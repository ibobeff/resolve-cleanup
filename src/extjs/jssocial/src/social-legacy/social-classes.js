/*
Copyright(c) 2013 Gen-E
*/
/**
 * Model to be used for comboboxes.  It defines a 'name' and 'value' field 
 */
Ext.define('RS.model.ComboBoxModel', {
	extend : 'Ext.data.Model',

	fields : ['name', 'value']
});

/**
 * Model for a comment.  Defines fields used to properly display a comment.
 * Belongs to a Post 
 */
Ext.define('RS.model.Comment', {
	extend : 'Ext.data.Model',

	fields : ['sys_id', '_ui_id', 'postId', 'author', 'date', 'comment'],
	belongsTo : 'RS.model.Post'
}); 
/**
 * Defines the model used to describe people who like a particular post.  Is displayed when the user clicks on the number of likes on a post
 */
Ext.define('RS.model.PeopleWhoLikeThis', {
	extend : 'Ext.data.Model',
	idProperty : 'sysid',
	fields : [{
		name : 'sysid',
		type : 'string'
	}, {
		name : 'photo'
	}, {
		name : 'name'
	}, {
		name : 'userEmail'
	}, {
		name : 'gotoLink'
	}, {
		name : 'description'
	}, {
		name : 'currentUser',
		type : 'boolean'
	}, {
		name : 'following',
		type : 'boolean'
	}]
});

/**
 * Model for a Post.  Has fields that are used to display the posts to the user. Has many comments. 
 */
Ext.define('RS.model.Post', {
	extend : 'Ext.data.Model',
	requires : ['RS.model.Comment', 'Ext.data.HasManyAssociation', 'Ext.data.BelongsToAssociation'],
	fields : [{
		name : 'sysId',
		type : 'string'
	}, {
		name : 'name',
		type : 'string'
	}, {
		name : 'postType',
		type : 'string'
	}, {
		name : 'targetEntity',
		type : 'string'
	}, {
		name : 'postAuthor',
		type : 'string'
	}, {
		name : 'postDate',
		type : 'string'
	}, {
		name : 'postContent',
		type : 'string'
	}, {
		name : 'likeCount',
		type : 'string'
	}, {
		name : 'isStarred',
		type : 'boolean',
		defaultValue : false
	}, {
		name : 'hasDeleteRights',
		type : 'boolean',
		defaultValue : false
	}, {
		name : '_ui_id',
		type : 'string'
	}],

	hasMany : {
		model : 'RS.model.Comment',
		name : 'comments',
		associationKey : 'comments'
	}

});

/**
 * Model for a stream that the user subscribes to in order to view the posts inside that stream.  For example, Runbook or Inbox 
 */
Ext.define('RS.model.Stream', {
	extend : 'Ext.data.Model',
	//idProperty : 'sys_id',
	fields : [{
		name : 'displayName',
		type : 'string'
	}, {
		name : 'user',
		type : 'string'
	}, {
		name : 'sys_id',
		type : 'string'
	}, {
		name : 'comptype',
		type : 'string'
	}, {
		name : 'grouptitle',
		type : 'string'
	}, {
		name : 'parentCompType',
		type : 'string'
	}, {
		name : 'parentSysId',
		type : 'string'
	}, {
		name : 'systemStream',
		type : 'boolean'
	}, 'records']

});

/**
 * Model used to display the user's image in the upper left corner of the screen 
 */
Ext.define('RS.model.UserImage', {
	extend : 'Ext.data.Model',
	fields : [{
		name : 'sysId',
		type : 'string'
	}, {
		name : 'title',
		type : 'string'
	}, {
		name : 'userName',
		type : 'string'
	}, {
		name : 'userFullName',
		type : 'string'
	}, {
		name : 'userHref',
		type : 'string'
	}, {
		name : 'imgSrc',
		type : 'string'
	}],
	proxy : {
		type : 'ajax',
		url : '/resolve/service/userinfo',
		reader : {
			root : 'data'
		}
	}
});

/**
 * Store to get the suggestions for when the user adds a link to a post before submitting the post
 */
Ext.define('RS.store.LinkStore', {
	extend : 'Ext.data.Store',
	requires : 'RS.model.ComboBoxModel',
	model : 'RS.model.ComboBoxModel',
	pageSize : 10,
	remoteSort : true,
	proxy : {
		type : 'ajax',
		//url: 'data/linkdata.json',
		url : '/resolve/service/social/getentitydata',
		extraParams : {
			type : '',
			limit : 10
		},
		reader : {
			type : 'json',
			totalProperty : 'totalCount',
			root : 'records'
		},
		// sends single sort as multi parameter
		simpleSortMode : true
	}

});

/**
 * Store to load the suggested mention/hash tags for the user while they type in the post textarea
 */
Ext.define('RS.store.MentionStore', {
	extend : 'Ext.data.Store',
	requires : 'RS.model.ComboBoxModel',
	model : 'RS.model.ComboBoxModel',
	pageSize : 0,
	proxy : {
		type : 'ajax',
		url : '/resolve/service/social/getmentiondata',
		extraParams : {
			query : ''
		},
		reader : {
			type : 'json',
			root : 'records'
		},
		simpleSortMode : true
	}

});

/**
 * Store to load the posts for a particular stream 
 */
Ext.define('RS.store.SocialStore', {
	extend : 'Ext.data.Store',
	requires : 'RS.model.Post',
	model : 'RS.model.Post',
	pageSize : pageSizeForAllTabs(),
	remoteSort : true,
	buffered : true,
	proxy : {
		type : 'ajax',
		url : '/resolve/service/getPost',
		extraParams : {
			displayName : 'Blog',
			sysid : '__sysid__',
			comptype : 'Blog',
			gridItemClick : false,
			infiniteScroll : false,
			start : 0,
			limit : pageSizeForAllTabs()
		},
		reader : {
			type : 'json',
			root : 'records'
		},
		// sends single sort as multi parameter
		simpleSortMode : true
	}

});

/**
 * Store to load the user image
 */
Ext.define('RS.store.UserImages', {
	extend : 'Ext.data.Store',
	requires : 'RS.model.UserImage',
	model : 'RS.model.UserImage',
	autoLoad : true
});

/**
 * Control to display a large image when the user clicks on a thumbnail image within a post.
 */
Ext.define('RS.view.control.ImageViewer', {
	extend: 'Ext.window.Window',

	layout: 'fit',
	frame: false,
	closeAction: 'hide',
	modal: true,
	constrain: true,
	onEsc: function() {
		this.close();
	},
	baseCls: 'x-panel',

	/**
	 * The source of the image tag to display in the large window
	 */
	src: '',

	initComponent: function() {
		this.items = {
			xtype: 'component',
			autoEl: {
				tag: 'img',
				style: 'height:auto;width:auto',
				src: this.src
			},
			listeners: {
				render: {
					scope: this,
					fn: function(cmp) {
						this.getImage().on('load', this.onImageLoad, this, {
							single: true
						});
					}
				}
			}
		};
		this.callParent(arguments);
	},

	onImageLoad: function() {
		this.getEl().unmask();
		var size = Ext.fly(Ext.getBody()).getSize(),
			bodyHeight = size.height,
			bodyWidth = size.width,
			image = this.getImage();
		image.setStyle({
			height: 'auto',
			width: 'auto'
		});
		var h = image.dom.offsetHeight,
			w = image.dom.offsetWidth,
			toConfig = {};
		
		//preserve scale
		var scaledSize = this.scaleSize(bodyWidth - 26, bodyHeight - 58, w, h);
		//10-25 to bring the image away from the edges
		var width = scaledSize[0] + 16;
		var height = scaledSize[1] + 38
		var x = Math.round((bodyWidth - width) / 2);
		var y = Math.round((bodyHeight - height) / 2);
		toConfig = {
			width: width,
			height: height,
			x: x,
			y: y
		};

		this.animate({
			to: toConfig,
			listeners: {
				afteranimate: {
					fn: function() {
						this.doComponentLayout();
					},
					scope: this
				}
			}
		});
	},

	/**
	 * Scales the image to the max height and width from the current height and width
	 */
	scaleSize: function(maxW, maxH, currW, currH) {

		var ratio = currH / currW;

		if(currW >= maxW && ratio <= 1) {
			currW = maxW;
			currH = currW * ratio;
		} else if(currH >= maxH) {
			currH = maxH;
			currW = currH / ratio;
		}

		return [currW, currH];
	},

	/**
	 * Sets the image source and resizes the window to the largest size available (the size of the image or the size of the browser window) maintaining scale of the image.
	 */
	setSrc: function(src) {
		this.getEl().mask('Loading...');
		var image = this.getImage();
		image.on('load', this.onImageLoad, this, {
			single: true
		});
		image.dom.src = src;
		image.dom.style.width = image.dom.style.height = 'auto';
	},
	getImage: function() {
		return this.items.items[0].getEl();
	},
	initEvents: function() {
		this.callParent(arguments);
		if(this.resizer) {
			this.resizer.preserveRatio = true;
		}
	}
});
/**
 * Panel to display the posts from a particular stream
 */
Ext.define('RS.view.panel.GenericPostPanel', {
	extend: 'Ext.Panel',
	alias: 'widget.genericpostpanel',
	border: false,
	loadMask: true,
	disableSelection: true,

	//use this for infinite scrolling calculations
	contentHeight: -1,
	calculateHeight: false,
	autoScroll: true,

	initComponent: function() {
		this.store = Ext.create('RS.store.SocialStore', {
			listeners: {
				beforeprefetch: this.beforePrefetch,
				prefetch: this.onPrefetch,
				scope: this
			}
		});

		this.tpl = postTemplate;

		this.callParent(arguments);

		this.on('render', function() {
			if (this.selectionDetails) {
				this.store.load();
			}
		}, this, {
			single: true
		});
	},
	beforePrefetch: function(store, operation, eOpts) {
		//This is going to be ugly, but we want to keep this post panel self-contained
		//We need to get the current selected item from the tree on the left, and populate the panel with the results from the store.
		//This would be good for the controller to do, but the controller doesn't know which store to link to which tab because they are all the "same"
		var tabpanel = this.up('panel');
		if (tabpanel) {
			var navigation = tabpanel.down('streamtree');
			if (navigation) {
				var selectionDetails = navigation.getSelectedDetails();
				if (selectionDetails.hadSelection) {
					var params = store.getProxy().extraParams;

					if (params.infiniteScroll) {
						params.start = (params.start + params.limit);
					} else {
						params.start = 0;
					}

					Ext.apply(params, selectionDetails);
				} else {
					//If there are no selections, then its because there are no records (we auto selected the first record already) which means just display blank information
					this.overwritePost([]);
					return false;
				}
			}
		} else if (this.selectionDetails) {
			var selectionDetails = this.selectionDetails;
			if (selectionDetails.hadSelection) {
				var params = store.getProxy().extraParams;

				if (params.infiniteScroll) {
					params.start = (params.start + params.limit);
				} else {
					params.start = 0;
				}

				Ext.apply(params, selectionDetails);
			} else {
				//If there are no selections, then its because there are no records (we auto selected the first record already) which means just display blank information
				this.overwritePost([]);
				return false;
			}
		}
	},
	onPrefetch: function(store, records, successful, operation, opts) {
		var params = store.getProxy().extraParams;

		//reset the flags
		params.infiniteScroll = false;
		if (records && records.length == 0) {
			//flag to indicate that there are no more recs to show
			params.endOfRecs = true;
		}

		var startRec = params.start;
		//if its the first page, then overwrite it
		var json = [];

		if (records) {
			Ext.each(records, function(record) {
				json.push(record.raw);
			})
		}

		if (startRec == 0) {
			this.overwritePost(json);
		} else {
			this.updatePosts(json);
		}

		//If forum, then initially show everything collapsed
		showingForum = false;
		if (operation.params.comptype === 'Forums' && !operation.params.permaLink) {
			var showHideCommentLinks = this.body.query('.showHideComments');
			Ext.each(showHideCommentLinks, function(showHideCommentLink) {
				hideCommentDisplay(showHideCommentLink);
			});
		}
	},

	/**
	 * Updates the current posts with the provided records without overwriting the existing posts (for infinite scrolling)
	 */
	updatePosts: function(records) {
		var html = this.tpl.apply(records);
		var elbody = Ext.get(this.body);

		if (elbody) {
			elbody.insertHtml('beforeEnd', html);
		} else {
			this.body.insertHtml('beforeEnd', html);
		}

		var elId = elbody.id;
		var panelId = elId.substring(0, elId.indexOf('-body'));

		this.calculateHeight = true;
	},

	/**
	 * Overwrites any existing posts with the provided records (used when switching streams or refreshing the latest from the server)
	 */
	overwritePost: function(records) {
		var html = this.tpl.apply(records);
		var elbody = Ext.get(this.body);
		if (elbody) {
			elbody.update(html);
		}

		this.calculateHeight = true;
	}
});

var commentTpl = new Ext.XTemplate('<div id="{sysId}" class="feeditemcomment cxfeedcomment " style="display:block">',
	'<span class="feeditemcommentbody">',
	'<span style="position:relative;float:right;color: #909090;">',
	'<span>{[this.formatDate(values)]}</span>',
	'<span {[this.showHideDeleteLink(values)]} class="cxfeeditemlikedot feeditemseparatingdot"> | </span>',
	'<a id={[this.getDeleteLinkId(values)]} href="JavaScript:void(0);" class="feeditemactionlink cxaddcommentaction" title="Delete this comment" {[this.showHideDeleteLink(values)]}><img src="../images/social/delete.png" /></a>',
	'</span>',
	'<span class="feedcommentuser">{author}</span>',
	'<br/>',
	'<span class="feedcommenttext">{[this.decodeComment(values)]}</span>',
	'</span>',
	'</div>', {
	compiled: true,


	decodeComment: function(values) {
		var decContent = values.comment;
		/*
		 if(decContent)
		 {
		 decContent = Ext.htmlDecode(decContent);
		 }
		 */
		return decContent;
	},

	getDeleteLinkId: function(values) {
		var id = Ext.id();
		var deleteLink = '"' + id + '" onclick="JavaScript:deleteComment(\'' + values.postId + '\',\'' + values.sysId + '\')"';
		return deleteLink;
	},

	showHideDeleteLink: function(values) {
		var cssStyle = '';
		if (values.hasDeleteRights) {

		} else {
			cssStyle = 'style="display:none"';
		}
		return cssStyle;
	},
	formatDate: function(values) {
		var now = new Date(),
			postDate = new Date(Number(values.date)),
			dateString = Ext.Date.format(postDate, 'g:i A'),
			duration = '',
			durationText = '';
		var diff = (now.getTime() - postDate.getTime());
		var seconds = Math.round(diff / 1000);
		if (seconds < 60) {
			duration = seconds;
			durationText = duration === 1 ? 'second' : 'seconds';
		} else if (seconds < (60 * 60)) {
			duration = Math.round(seconds / 60);
			durationText = duration === 1 ? 'minute' : 'minutes';
		} else if (seconds < (60 * 60 * 24)) {
			duration = Math.round(seconds / 60 / 60);
			durationText = duration === 1 ? 'hour' : 'hours';
		} else {
			dateString = Ext.Date.format(postDate, 'M j, Y')
			duration = Math.round(seconds / 60 / 60 / 24);
			durationText = duration === 1 ? 'day' : 'days';
		}

		dateString += Ext.String.format(' ({0} {1} ago)', duration, durationText);
		return dateString;
	}
});

var postTemplate = new Ext.XTemplate('<tpl for=".">',
	'<div id="{sysId}" class="feedcontainer actionsOnHoverEnabled cxfeedcontainer">',
	'<div class="cxfeeditem feeditem">',
	'<div class="feeditemcontent cxfeeditemcontent">',
	'<div class="feeditembody">',
	'<span style="padding-right:5px;">',
	'<a href="JavaScript:void(0);" {[this.addOnClickToStar(values)]} >',
	'<span style="display:none">{isStarred}</span>',
	'<img src="../images/social/{[this.showStarImg(values)]}" width="15" height="15"></img>',
	'</a>',
	'</span>',
	'<span class="feeditemtargetentity">{targetEntity} &#8212; </span>',
	'<span  class="feeditemsecondentity">',
	'<span class="feeditemauthorentity">{postAuthor}</span>',
	'<span style="float:right;color: #909090;">',
	'<span class="feeditemfooter">',
	'<a id={[this.getLinkId(values)]} href="JavaScript:void(0);" class="feeditemactionlink cxaddcommentaction cxaddcommentlink" title="Comment on this post">Add Comment</a>',
	'<span  class="cxfeeditemlikedot feeditemseparatingdot cxaddcommentlink"> | </span>',
	'<a id={[this.getPermLinkId(values)]} href="JavaScript:void(0);" class="feeditemactionlink cxaddcommentaction" title="A direct link to this post">Link</a>',
	'<span  class="cxfeeditemlikedot feeditemseparatingdot"> | </span>',
	'<span class="__likeLink__"><a id={[this.getLikeLinkId(values)]} href="JavaScript:void(0);" class="feeditemactionlink cxaddcommentaction" title="Like on this post">Like</a></span>  ',
	'<span  class="cxfeeditemlikedot feeditemseparatingdot"> | </span>',
	'<span class="likeCountContainer">',
	'<tpl if="likeCount &gt; 0"><a href="JavaScript:void(0);" onclick="displayLikeDialog(\'{sysId}\')"></tpl>',
	'<span class="__noOfPeopleLiked__">{likeCount}</span>&nbsp;&nbsp;',
	'<img src="../images/social/like.png" />',
	'<tpl if="likeCount &gt; 0"></a></tpl>',
	'</span> ',
/*'<span  class="cxfeeditemlikedot feeditemseparatingdot"> | </span>',
'<a href="JavaScript:void(0);" onclick="displayPostInNewWindow(\'{sysId}\')"><img src="../images/social/like.png" /></a>',
*/
'<span  {[this.showHideDeleteLink(values)]} class="cxfeeditemlikedot feeditemseparatingdot"> | </span>',
	'<a id={[this.getDeleteLinkId(values)]} href="JavaScript:void(0);" class="feeditemactionlink cxaddcommentaction" title="Delete this post" {[this.showHideDeleteLink(values)]} ><img src="../images/social/delete.png" /></a>',
	'</span>',
	'{[this.formatDate(values)]}',
	'</span>',
	'</span>',
	'<br/>',
	'<div class="feeditemtext">{[this.decodeContent(values)]}</div>',
	'<br style="clear:both;" />',
	'</div>',
	'<a class="showHideComments" href="JavaScript:void(0);" onclick="JavaScript:toggleCommentDisplay(this,false,\'{sysId}\')" style="visibility:<tpl if="comments.length &gt; 0">visible</tpl><tpl if="comments.length == 0">hidden</tpl>;"><i>Hide Comments</i></a>',
	'<div class="feeditemextras">',
	'<div class="cxcomments feeditemcomments">',
	'<div class="commentsCls">',
	'<tpl for="comments">',
	'{[this.renderComments(values)]}',
	'</tpl>',
	'</div>',
	'</div>',
	'<div class="cxnewcomment" style="display:none">',
	'<div class="feeditemcomment feeditemcommenttext">',
	'<div class="feeditemcommentnew">',
	'<form>',
	'<textarea name="tacomment" style="height: 100px; max-height: 100px;" class="rspost cxnewcommenttext  chatterMentionsEnabled chatterHashtagsEnabled" tabindex="0" role="textbox" title="Write a comment..."></textarea>',
	'<div class="cxvalidationmessage newcommenterrorcontainer" style="display:none"></div>',
	'<input id={[this.getCommentBtnId(values)]} class="" type="button" value="Comment" title="Comment"/>',
	'</form>',
	'<div class="feedclearfloat"></div>',
	'</div>',
	'</div>',
	'</div>',
	'</div>',
	'</div>',
	'</div>',
	'</div>',
	'</tpl>', {
	compiled: true,


	formatDate: function(values) {
		var now = new Date(),

			postDate = new Date(Number(values.postDate)),

			dateString = Ext.Date.format(postDate, 'g:i A'),
			duration = '',
			durationText = '';
		var diff = (now.getTime() - postDate.getTime());
		var seconds = Math.round(diff / 1000);
		if (seconds < 60) {
			duration = seconds;
			durationText = duration === 1 ? 'second' : 'seconds';
		} else if (seconds < (60 * 60)) {
			duration = Math.round(seconds / 60);
			durationText = duration === 1 ? 'minute' : 'minutes';
		} else if (seconds < (60 * 60 * 24)) {
			duration = Math.round(seconds / 60 / 60);
			durationText = duration === 1 ? 'hour' : 'hours';
		} else {
			dateString = Ext.Date.format(postDate, 'M j, Y')
			duration = Math.round(seconds / 60 / 60 / 24);
			durationText = duration === 1 ? 'day' : 'days';
		}

		dateString += Ext.String.format(' ({0} {1} ago)', duration, durationText);
		return dateString;
	},

	renderComments: function(values) {
		var comm = commentTpl.apply(values);
		return comm;
	},
	decodeContent: function(values) {
		var decContent = values.postContent;
		/*if(decContent)
		 {
		 decContent = Ext.htmlDecode(decContent);
		 }*/
		return decContent;
	},
	addOnClickToStar: function(values) {
		var id = Ext.id();
		var func = 'onclick="JavaScript:toggleStar(\'' + id + '\', \'' + values.sysId + '\')" id=' + id;
		return func;
	},
	showStarImg: function(values) {
		var imgname = 'starOff.png';
		if (values.isStarred) {
			imgname = 'starOn.png';
		}
		return imgname;
	},
	getLinkId: function(values) {
		var id = Ext.id();
		var linkId = '"' + id + '" onclick="JavaScript:showComment(\'' + id + '\', \'' + values.sysId + '\')"';
		return linkId;
	},
	getPermLinkId: function(values) {
		var id = Ext.id();
		var linkId = '"' + id + '" onclick="JavaScript:showPermaLink(\'' + id + '\', \'' + values.sysId + '\')"';
		return linkId;
	},
	getLikeLinkId: function(values) {
		var id = Ext.id();
		var likeLink = '"' + id + '" onclick="JavaScript:likeLinkOnClick(\'' + values.sysId + '\')"';
		return likeLink;
	},
	showHideDeleteLink: function(values) {
		var cssStyle = '';
		if (values.hasDeleteRights) {
			//do nothing
		} else {
			cssStyle = 'style="display:none"';
		}
		return cssStyle;
	},
	getDeleteLinkId: function(values) {
		var id = Ext.id();
		var deleteLink = '"' + id + '" onclick="JavaScript:deletePost(\'' + values.sysId + '\')"';
		return deleteLink;
	},
	getCommentBtnId: function(values) {
		var id = Ext.id();
		var buttonEl = '"' + id + '" onclick="JavaScript:submitComment(\'' + id + '\', \'' + values.sysId + '\')"';
		return buttonEl;
	}
});
/**
 * Refresh button to refresh the currently display posts.  Has a menu to turn on and off the auto-refresh of the posts
 */
Ext.define('RS.view.menu.RefreshButton', {
	extend: 'Ext.button.Split',
	alias: 'widget.refreshbutton',
	text: 'Refresh',
	hideRefreshStreams: false,

	initComponent: function() {
		this.menu = [{
			text: 'Auto Refresh',
			checked: Ext.state.Manager.get('socialAutoRefresh') || false,
			action: 'autorefresh'
		}, {
			hidden: this.hideRefreshStreams,
			text: 'Refresh Streams',
			action: 'refreshstreams'
		}];

		this.callParent();
	}
});
/*
 * Plugin, which can be used with any class, which inherits from Ext.form.field.Text (xtype: textfield).
 * The plugin requires a textarea.
 *
 * Note: This plugin is a generalisation of the combo-box auto-complete feature.
 */
Ext.define('RS.view.plugin.AutoComplete', {
	extend : 'Ext.AbstractPlugin',
	mixins : ['Ext.util.Observable'],
	requires : ['Ext.XTemplate', 'Ext.view.BoundList', 'Ext.view.BoundListKeyNav', 'Ext.data.Store'],
	alias : 'plugin.auto-complete',
	pageSize : 0,
	delimiter : ',',
	defaultListConfig : {
		emptyText : '',
		loadingText : 'Loading...',
		loadingHeight : 70,
		minWidth : 100,
		maxHeight : 300,
		shadow : 'sides'
	},
	pickerAlign : 'tl-bl?',
	selectOnTab : true,

	/**
	 * Regular expression to parse the @ or # tags in a post as the user types
	 */
	tagRegex : /(^|\s)[@|#]\[\w+\]/g,

	matchFieldWidth : true,

	init : function(component) {
		var me = this;

		// Custom events
		me.hasListeners = ['expand', 'collapse', 'select'];

		//initialize the store
		me.store = Ext.data.StoreManager.lookup(me.store);
		me.store.on({
			scope : me,
			load : me.onLoad,
			exception : me.collapse
		});

		if (!this.displayTpl) {
			this.displayTpl = Ext.create('Ext.XTemplate', '<tpl for=".">' + '{[typeof values === "string" ? values : values.' + this.valueField + '.substr(1)]}' + '<tpl if="xindex < xcount">' + this.delimiter + '</tpl>' + '</tpl>');
		} else if (Ext.isString(this.displayTpl)) {
			this.displayTpl = Ext.create('Ext.XTemplate', this.displayTpl);
		}

		// Setup key event when component is rendered, so we have 'inputEl'.
		// Note: We bypass the 'enableKeyEvents' config of text fields.
		component.on('render', function(cmp, obj) {
			this.mon(this.cmp.inputEl, {
				scope : this,
				keyup : {
					fn : this.onKeyUp,
					buffer : 500
				},
				keydown : {
					fn : this.onKeyDown
				}
			});
		}, this);
	},

	onLoad : function(store, records, successful, eOpts) {
		if (records.length == 0) {
			this.collapse();
		} else {
			//show the suggestions
			this.expand();
		}
	},

	onKeyDown : function(event, component, eOpts) {
		var me = this;
		if (event.getKey() == event.ESC) {
			me.collapse();
			return;
		}
	},

	onKeyUp : function(event, component, eOpts) {
		var me = this;

		if (event.getKey() == event.ESC) {
			return;
		}

		//if there is a new query from the component's getValue, then we should run the query
		var text = me.cmp.getValue();
		if (text) {
			var query;
			var textSplit = text.split(me.tagRegex);
			var hashText = '';
			for (var i = textSplit.length - 1; i >= 0; i--) {
				var s = textSplit[i];
				var index = s.indexOf('@');
				var hashIndex = s.indexOf('#');
				if (index > -1) {
					query = s.substr(index);
					break;
				} else if (hashIndex > -1) {
					query = s.substr(hashIndex);
					break;
				}
			}

			if (query && (query.charAt(0) == '@' || query.charAt(0) == '#')) {
				if (query.indexOf(' ') > -1) {
					me.collapse();
					// query = query.slice(0, query.indexOf(' '));
					return;
				}

				if (me.lastQuery != query) {
					me.lastQuery = query;
					me.store.load({
						params : {
							query : query
						}
					});
				} else {
					//show the suggestions
					//we already have the records already because the searches match
					me.expand();
				}
				//wait a bit so it doesn't react to the down arrow opening the picker
				me.cmp.inputEl.focus();
			}
		}
	},

	onItemClick : function(picker, record) {
		/*
		 * If we're doing single selection, the selection change events won't fire when
		 * clicking on the selected element. Detect it here.
		 */
		var me = this, lastSelection = me.lastSelection, valueField = me.valueField, selected;

		if (lastSelection) {
			selected = lastSelection[0];
			if (record.get(valueField) === selected.get(valueField)) {
				me.collapse();
			}
		}
		me.setValue(record);
	},

	onListRefresh : function() {
		this.alignPicker();
		this.syncSelection();
	},

	alignPicker : function() {
		var me = this, picker = me.picker, heightAbove = me.cmp.getPosition()[1] - Ext.getBody().getScroll().top, heightBelow = Ext.Element.getViewHeight() - heightAbove - me.cmp.getHeight(), space = Math.max(heightAbove, heightBelow);

		if (me.isExpanded) {
			picker = me.getPicker();
			if (me.matchFieldWidth) {
				// Auto the height (it will be constrained by min and max width) unless there are no records to display.
				picker.setSize(me.cmp.bodyEl.getWidth(), picker.store && picker.store.getCount() ? null : 0);
			}
			if (picker.isFloating()) {
				me.doAlign();
			}
		}

		if (picker.getHeight() > space) {
			picker.setHeight(space - 5);
			// have some leeway so we aren't flush against
			me.doAlign();
		}
	},

	/**
	 * Performs the alignment on the picker using the class defaults
	 * @private
	 */
	doAlign : function() {
		var me = this, picker = me.picker, aboveSfx = '-above', isAbove;

		me.picker.alignTo(me.cmp.inputEl, me.pickerAlign, me.pickerOffset);
		// add the {openCls}-above class if the picker was aligned above
		// the field due to hitting the bottom of the viewport
		isAbove = picker.el.getY() < me.cmp.inputEl.getY();
		me.cmp.bodyEl[isAbove ? 'addCls' : 'removeCls'](me.openCls + aboveSfx);
		picker[isAbove ? 'addCls' : 'removeCls'](picker.baseCls + aboveSfx);
	},

	onListSelectionChange : function(list, selectedRecords) {
		var me = this;
		// Only react to selection if it is not called from setValue, and if our list is
		// expanded (ignores changes to the selection model triggered elsewhere)
		if (!me.ignoreSelection && me.isExpanded) {
			Ext.defer(me.collapse, 1, me);
			me.setValue(selectedRecords, false);
			if (selectedRecords.length > 0) {
				me.fireEvent('select', me, selectedRecords);
			}
			me.cmp.inputEl.focus();
		}
	},

	/**
	 * Returns a reference to the picker component for this field, creating it if necessary by
	 * calling {@link #createPicker}.
	 * @return {Ext.Component} The picker component
	 */
	getPicker : function() {
		var me = this;
		return me.picker || (me.picker = me.createPicker());
	},

	createPicker : function() {
		var me = this, picker, menuCls = Ext.baseCSSPrefix + 'menu', opts = Ext.apply({
			selModel : {
				mode : 'SINGLE'
			},
			floating : true,
			hidden : true,
			ownerCt : me.ownerCt,
			cls : me.cmp.el.up('.' + menuCls) ? menuCls : '',
			store : me.store,
			displayField : me.displayField,
			focusOnToFront : false,
			pageSize : me.pageSize
		}, me.listConfig, me.defaultListConfig);

		picker = me.picker = Ext.create('Ext.view.BoundList', opts);
		me.mon(picker, {
			//itemclick : me.onItemClick,
			refresh : me.onListRefresh,
			scope : me
		});

		me.mon(picker.getSelectionModel(), {
			selectionChange : me.onListSelectionChange,
			scope : me
		});

		return picker;
	},

	expand : function() {
		var me = this, bodyEl, picker, collapseIf;

		if (!me.isExpanded && !me.isDestroyed) {
			bodyEl = me.cmp.bodyEl;
			picker = me.getPicker();
			collapseIf = me.collapseIf;

			// show the picker and set isExpanded flag
			picker.show();
			me.isExpanded = true;
			me.alignPicker();
			bodyEl.addCls(me.openCls);

			// monitor clicking and mousewheel
			me.mon(Ext.getDoc(), {
				mousewheel : collapseIf,
				mousedown : collapseIf,
				scope : me
			});
			Ext.EventManager.onWindowResize(me.alignPicker, me);
			me.fireEvent('expand', me);
			me.onExpand();
		}
	},

	/**
	 * @private
	 * Enables the key nav for the BoundList when it is expanded.
	 */
	onExpand : function() {
		var me = this, keyNav = me.listKeyNav, selectOnTab = me.selectOnTab, picker = me.getPicker();

		// Handle BoundList navigation from the input field. Insert a tab listener specially to enable selectOnTab.
		if (keyNav) {
			keyNav.enable();
		} else {
			keyNav = me.listKeyNav = Ext.create('Ext.view.BoundListKeyNav', this.cmp.inputEl, {
				boundList : picker,
				forceKeyDown : true,
				tab : function(e) {
					if (selectOnTab) {
						this.selectHighlighted(e);
						me.collapse();
					}
					// Tab key event is allowed to propagate to field
					return true;
				}
			});
		}

		// While list is expanded, stop tab monitoring from Ext.form.field.Trigger so it doesn't short-circuit selectOnTab
		if (selectOnTab) {
			me.ignoreMonitorTab = true;
		}

		Ext.defer(keyNav.enable, 1, keyNav);
		//wait a bit so it doesn't react to the down arrow opening the picker
		me.cmp.inputEl.focus();
	},

	/**
	 * Collapse this field's picker dropdown.
	 */
	collapse : function() {
		if (this.isExpanded && !this.isDestroyed) {
			var me = this, openCls = me.openCls, picker = me.picker, doc = Ext.getDoc(), collapseIf = me.collapseIf, aboveSfx = '-above';

			// hide the picker and set isExpanded flag
			picker.getSelectionModel().deselectAll();
			picker.hide();
			me.isExpanded = false;

			// remove the openCls
			me.cmp.bodyEl.removeCls([openCls, openCls + aboveSfx]);
			picker.el.removeCls(picker.baseCls + aboveSfx);

			// remove event listeners
			doc.un('mousewheel', collapseIf, me);
			doc.un('mousedown', collapseIf, me);
			Ext.EventManager.removeResizeListener(me.alignPicker, me);
			me.fireEvent('collapse', me);
			me.onCollapse();
		}
	},

	/**
	 * @private
	 * Disables the key nav for the BoundList when it is collapsed.
	 */
	onCollapse : function() {
		var me = this, keyNav = me.listKeyNav;
		if (keyNav) {
			keyNav.disable();
			me.ignoreMonitorTab = false;
		}
		Ext.Function.defer(me.cmp.inputEl.focus, 1, me.cmp.inputEl);
		// me.cmp.inputEl.focus();
	},

	/**
	 * @private
	 * Runs on mousewheel and mousedown of doc to check to see if we should collapse the picker
	 */
	collapseIf : function(e) {
		var me = this;
		if (!me.isDestroyed && !e.within(me.bodyEl, false, true) && !e.within(me.picker.el, false, true)) {
			me.collapse();
		}
	},

	setValue : function(value, doSelect) {
		var me = this, valueNotFoundText = me.valueNotFoundText, inputEl = me.cmp.inputEl, i, len, record, models = [], displayTplData = [], processedValue = [];

		if (me.store.loading) {
			// Called while the Store is loading. Ensure it is processed by the onLoad method.
			me.value = value;
			return me;
		}

		// This method processes multi-values, so ensure value is an array.
		value = Ext.Array.from(value);

		// Loop through values
		for ( i = 0, len = value.length; i < len; i += 1) {
			record = value[i];
			if (!record || !record.isModel) {
				record = me.findRecordByValue(record);
			}
			// record found, select it.
			if (record) {
				models.push(record);
				displayTplData.push(record.data);
				processedValue.push(record.get(me.valueField));
			}
			// record was not found, this could happen because
			// store is not loaded or they set a value not in the store
			else {
				// if valueNotFoundText is defined, display it, otherwise display nothing for this value
				if (Ext.isDefined(valueNotFoundText)) {
					displayTplData.push(valueNotFoundText);
				}
				processedValue.push(value[i]);
			}
		}

		// Set the value of this field. If we are multiselecting, then that is an array.
		me.value = me.multiSelect ? processedValue : processedValue[0];
		if (!Ext.isDefined(me.value)) {
			me.value = null;
		}
		me.displayTplData = displayTplData;
		// me.lastSelection = me.valueModels = models;

		if (inputEl && me.emptyText && !Ext.isEmpty(value)) {
			inputEl.removeCls(me.emptyCls);
		}

		if (me.value) {
			var text = me.cmp.getValue();
			var val = me.value;
			var tag = val.charAt(0);
			var textSplit = text.split(me.tagRegex);
			var oldText = ''
			var hashText = '';
			for (var i = textSplit.length - 1; i >= 0; i--) {
				var s = textSplit[i];
				var index = s.indexOf(tag);
				if (index > -1) {
					oldText = s.substr(0, index);
					hashText = s.substr(index);
					if (hashText.indexOf(' ') > -1)
						hashText = hashText.substr(0, hashText.indexOf(' '));
					break;
				}
			}
			var pre = text.substr(0, text.lastIndexOf(oldText + hashText) + oldText.length);
			var post = text.substr(text.lastIndexOf(oldText + hashText) + (oldText.length + hashText.length) + 1);
			var temp = pre + tag + '[' + val.substr(1) + ']' + post;
			me.cmp.setRawValue(temp);
			me.cmp.checkChange();
		}

		if (doSelect !== false) {
			me.syncSelection();
		}
		me.cmp.applyEmptyText();

		return me;
	},

	syncSelection : function() {
		var me = this, ExtArray = Ext.Array, picker = me.picker, selection, selModel;
		if (picker) {
			// From the value, find the Models that are in the store's current data
			selection = [];
			ExtArray.forEach(me.valueModels || [], function(value) {
				if (value && value.isModel && me.store.indexOf(value) >= 0) {
					selection.push(value);
				}
			});

			// Update the selection to match
			me.ignoreSelection += 1;
			selModel = picker.getSelectionModel();
			selModel.deselectAll();
			if (selection.length) {
				selModel.select(selection);
			}
			me.ignoreSelection -= 1;
		}
	},

	/**
	 * @private Generate the string value to be displayed in the text field for the currently stored value
	 */
	getDisplayValue : function() {
		return this.displayTpl.apply(this.displayTplData);
	},

	findRecordByValue : function(value) {
		return this.findRecord(this.valueField, value);
	},

	findRecord : function(field, value) {
		var ds = this.store, idx = ds.findExact(field, value);
		return idx !== -1 ? ds.getAt(idx) : false;
	}
});

/**
 * Form to collect information to post.  Collects information for files, links, html and posts that information to the selected stream
 */
Ext.define('RS.view.form.PostForm', {
	extend: 'Ext.form.Panel',
	alias: 'widget.postform',

	requires: ['RS.view.plugin.AutoComplete'],

	border: false,
	cls: 'rs-panel',

	dockedItems: [{
		xtype: 'toolbar',
		cls: 'rs-toolbar',
		dock: 'bottom',
		items: [{
			xtype: 'button',
			action: 'post',
			text: 'Post',
			itemId: 'postButton',
			listeners: {
				render: function(button) {
					//COMMENT-4.1: html doesn't work in 4.1 for some reason
					button.update('<font size="2"><b>&nbsp;&nbsp;Post&nbsp;&nbsp;</b></font>');
				}
			}
		}, {
			xtype: 'button',
			action: 'showfileupload',
			text: 'File'
		}, {
			xtype: 'button',
			action: 'showlink',
			text: 'Link'
		}, {
			xtype: 'button',
			action: 'showhtml',
			text: 'Html'
		}, '->',
		{
			xtype: 'splitbutton',
			iconCls: 'x-btn-icon x-tbar-loading',
			tooltip: 'Reload Posts',
			action: 'refreshPostPanel',
			menu: [{
				text: 'Auto Refresh',
				itemId: 'autoRefresh',
				checked: false,
				action: 'autorefresh'
			}]
		}]
	}],

	defaults: {
		anchor: '-50'
	},

	initComponent: function() {
		var initializeParams = Ext.apply({}, this.selectionDetails);

		this.items = [{
			xtype: 'displayfield',
			name: 'postTo',
			fieldLabel: 'Posting To',
			itemId: 'postingTo',
			labelWidth: 70,
			value: Ext.String.format('<b>{0}</b>', initializeParams.displayName || 'Blog')
		}, {
			xtype: 'textareafield',
			height: 84,
			name: 'postContents',
			enableKeyEvents: true,
			editable: true,
			hidden: false,
			plugins: [{
				ptype: 'auto-complete',
				store: 'MentionStore',
				displayField: 'name',
				valueField: 'value'
			}],
			setTextAreaValue: function(val) {
				this.setValue(val);
				this.plugins[0].lastQuery = null;
			}
		}, {
			xtype: 'hiddenfield',
			name: 'postSysId'
		}];

		this.callParent(arguments);
		
		if( Ext.state.Manager.get('socialAutoRefresh') ){
			this.down('#autoRefresh').setChecked(true);
		}
	},

	setForum: function(isForum, currentlySelectedName){
		if( isForum ){
			//set to Comment instead of post
			this.down('#postButton').update('<font size="2"><b>&nbsp;&nbsp;Comment&nbsp;&nbsp;</b></font>');
			this.down('#postingTo').setFieldLabel('Return To');
			this.down('#postingTo').setValue(Ext.String.format('<a style="color: blue;text-decoration:underline;cursor:pointer;cursor:hand;" onclick="returnForum()">{0}</a>', currentlySelectedName));
		}
		else{
			//set to post 
			this.down('#postButton').update('<font size="2"><b>&nbsp;&nbsp;Post&nbsp;&nbsp;</b></font>')
			this.down('#postingTo').setFieldLabel('Posting To');
			this.down('#postingTo').setValue(Ext.String.format('<b>{0}</b>', currentlySelectedName));
		}
	}
});
/**
 * Viewport displayed when the user views a component 
 */
Ext.define('RS.view.DocViewport', {
	extend : 'Ext.container.Viewport',

	requires : ['RS.view.form.PostForm', 'RS.view.panel.GenericPostPanel', 'RS.view.menu.RefreshButton'],
	cls : 'rs-panel',
	layout : {
		type : 'vbox',
		align : 'stretch'
	},

	defaults : {
		margins : '10'
	},
	
	/**
	 * @cfg selectionDetails {Object} Configuration to simulate the user clicking on a stream since no streams will be displayed on this view.  Configuration should be something like:
	 * 
	 * selectionDetails : {
	 * 		displayName : social.compFullName,
	 * 		sys_id : social.compSysId,
	 * 		comptype : social.compType,
	 * 		parent_comptype : '',
	 * 		parent_sysid : '',
	 * 		hadSelection : true
	 * 	}
	 * 	
	 */
	
	initComponent : function() {
		this.items = [{
			xtype : 'postform',
			border : false,
			height : 140,
			selectionDetails : this.selectionDetails
		}, {
			xtype : 'genericpostpanel',
			selectionDetails : this.selectionDetails,
			flex : 1
		}];
		this.callParent(arguments);
	}
});


/**
 * Form to display when the user needs to attach a file to a post 
 */
Ext.define('RS.view.form.FileUploadForm', {
	extend : 'Ext.form.Panel',
	alias : 'widget.fileuploadform',

	requires : ['Ext.form.Panel', 'Ext.form.field.File'],

	autoShow : true,

	bodyPadding : '10',

	initComponent : function() {
		this.callParent(arguments);
		this.form.url = '/resolve/service/post/attach';
		this.form.method = 'POST';
		//this.form.standardSubmit = true;
	},

	defaults : {
		anchor : '100%',
		msgTarget : 'side'
	},

	items : [{
		allowBlank : false,
		xtype : 'filefield',
		id : 'form-file',
		emptyText : 'Select a File',
		fieldLabel : 'File',
		name : 'file-path',
		buttonText : 'Select'
	}, {
		xtype : 'textfield',
		name : 'newfilename',
		fieldLabel : 'Description'
	}]

});

/**
 * Form to display when the user hovers over a component in social.  Displays details about the component and allows the user to follow/unfollow the component
 */
Ext.define('RS.view.form.FollowComponentForm', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.followcomponentform',

	border : false,

	/**
	 * Name of the component to display
	 */
	name : 'Name',

	/**
	 * Description of the component to display
	 */
	description : 'Description',

	/**
	 * True if the user is following the provided component, false otherwise
	 */
	following : true,

	/**
	 * True if the user should be given the option of following the component (possibly not because of permissions)
	 */
	canFollow : true,

	/**
	 * The component type to be displayed
	 */
	componentType : 'process',

	/**
	 * The text to display when the component isn't being followed
	 */
	followText : 'Follow',

	/**
	 * The icon to display when the component isn't being followed
	 */
	followIcon : '/resolve/images/plus.gif',

	/**
	 * The tooltip to display on the follow button when the component isn't being followed.  Will be passed the name of the component if a {0} variable is provided
	 */
	followTooltip : 'Click to follow {0}',

	/**
	 * The link to use when the user clicks on the name of the component.  This link should 'go to' the component in a new window
	 */
	gotoLink : '',

	/**
	 * The text to display when the component is being followed
	 */
	followingText : 'Unfollow',

	/**
	 * The icon to display when the component is being followed
	 */
	followingIcon : '/resolve/images/ok_st_obj.gif',

	/**
	 * The tooltip to display on the unfollow button when the component is being followed.  Will be passed the name of the component if a {0} variable is provided
	 */

	followingTooltip : 'Click to stop following {0}',

	initComponent : function() {
		switch( this.compType ) {
			case 'process':
				this.canFollow = false;
				this.followingText = 'Leave Process';
				this.followingTooltip = 'Click to leave the process {0}';
				break;
			case 'team':
				this.canFollow = false;
				this.followingText = 'Leave Team';
				this.followingTooltip = 'Click to leave the team {0}';
				break;
			case 'forum':
				this.followText = 'Join Forum';
				this.followingText = 'Leave Forum';
				this.followingTooltip = 'Click to leave the forum {0}';
				break;
			case 'document':
			case 'actiontask':
			case 'feed':
			case 'rss':
			case 'runbook':
				this.followingText = 'Unfollow';
				this.followingTooltip = 'Click to stop following {0}';
				break;
		}

		//@formatter:off
		var descriptionText = this.description ? ['<span class="popupTitle">',this.description,'</span>','<br/>'].join('') : '';

		this.html = [
			'<table>',
				'<tr>',
					'<td style="padding-left: 5px;font-size: 14px;">',
						this.gotoLink ? Ext.String.format('<span onclick="hidePopupOnMouseOut();window.open(\'{0}\');">',this.gotoLink):'',
						'<span class="popupName">',this.name,'</span>',
						this.gotoLink ? '</span>':'','<br/>',
						descriptionText,'<br/>',
					'</td>',
				'</tr>',
			'</table>'
		].join('');
		//@formatter:on

		this.buttons = this.buttons || [];

		if (this.following || this.canFollow) {
			this.buttons.push({
				text : this.following ? this.followingText : this.followText,
				icon : this.following ? this.followingIcon : this.followIcon,
				tooltip : this.following ? Ext.String.format(this.followingTooltip, this.name) : Ext.String.format(this.followTooltip, this.name),
				scope : this,
				handler : function(button) {
					var tooltip = Ext.tip.QuickTipManager.getQuickTip();
					if (tooltip)
						tooltip.hide();
					if (!this.canFollow)
						button.hide();
					this.following = !this.following;

					if (this.following) {
						button.setText(this.followingText);
						button.setIcon(this.followingIcon);
						button.setTooltip(Ext.String.format(this.followingTooltip, this.name));
					} else {
						button.setText(this.followText);
						button.setIcon(this.followIcon);
						button.setTooltip(Ext.String.format(this.followTooltip, this.name));
					}

					var win = button.up('window');
					Ext.Ajax.request({
						url : '/resolve/service/social/followComponent',
						params : {
							follow : this.following,
							comptype : win.compType,
							sysid : win.sysId
						},
						success : function(r) {
							var response = Ext.decode(r.responseText);
							if (response.success) {
								//do nothing
							} else {
								//display error
							}
						}
					});
				}
			});
		}

		this.callParent();
	}
});

/**
 * Form to display when the user hovers over a user in social.  Displays details about the user and allows the user to follow/unfollow the user
 */
Ext.define('RS.view.form.FollowUserForm', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.followuserform',

	border : false,

	/**
	 * Name of the user to display
	 */
	name : 'User Name',

	/**
	 * Title of the user to display
	 */
	description : 'Title',

	/**
	 * Email of the user to display
	 */
	userEmail : 'user@email.com',

	/**
	 * Phone number of the user to display
	 */
	phone : '555-1234',

	/**
	 * Mobile phone number of the user to display
	 */
	mobilePhone : '555-1234',

	/**
	 * Image to use when displaying the user
	 */
	photo : '/resolve/images/social/emptyUser.png',

	/**
	 * True if the user is currently following the provided user, false otherwise
	 */
	following : false,

	/**
	 * True if the user is able to follow the provided user.  They might not be able to because of permissions.
	 */
	canFollow : true,

	/**
	 * True if the passed user is the current user (so that they can't follow themselves)
	 */
	currentUser : false,

	/**
	 * The link to use when the user clicks on the name of the user.  This link should 'go to' the user in a new window
	 */
	gotoLink : '',

	/**
	 * The text to display when the component isn't being followed
	 */
	followText : 'Follow',

	/**
	 * The icon to display when the component isn't being followed
	 */
	followIcon : '/resolve/images/plus.gif',

	/**
	 * The tooltip to display on the follow button when the component isn't being followed.  Will be passed the name of the component if a {0} variable is provided
	 */
	followTooltip : 'Click to follow {0}',

	/**
	 * The text to display when the component is being followed
	 */
	followingText : 'Following',

	/**
	 * The icon to display when the component is being followed
	 */
	followingIcon : '/resolve/images/ok_st_obj.gif',

	/**
	 * The tooltip to display on the unfollow button when the component is being followed.  Will be passed the name of the component if a {0} variable is provided
	 */
	followingTooltip : 'Click to stop following {0}',

	initComponent : function() {
		//@formatter:off
		var descriptionText = this.description ? ['<span class="popupTitle">',this.description,'</span>','<br/>'].join('') : '';
		var emailText = this.userEmail ? [Ext.String.format('<a class="popupData" href="mailto:{0}">',this.userEmail),this.userEmail,'</a><br/>'].join('') : '';
		var phoneText = this.phone ? [Ext.String.format('<span class="popupData">Phone: {0}</span><br/>', this.phone)].join('') : '';
		var mobileText = this.mobilePhone ? [Ext.String.format('<span class="popupData">Mobile: {0}</span><br/>', this.mobilePhone)].join('') : '';
		this.html = [
			'<table>',
				'<tr>',
					'<td style="vertical-align: top;">',
						Ext.String.format('<img style="height:45px;width:45px;" src="{0}"/>', this.photo),
					'</td>',
					'<td style="padding-left: 5px; font-size: 14px;">',
						Ext.String.format('<span style="cursor:hand;cursor:pointer" class="popupName" onclick="hidePopupOnMouseOut();window.open(\'{0}\');">', this.gotoLink),this.name,'</span>','<br/>',
						descriptionText,
						emailText,
						phoneText,
						mobileText,
					'</td>',
				'</tr>',
			'</table>'
		].join('');
		//@formatter:on

		this.buttons = this.buttons || [];

		if (this.canFollow && !this.currentUser) {
			this.buttons.push({
				text : this.following ? this.followingText : this.followText,
				icon : this.following ? this.followingIcon : this.followIcon,
				tooltip : Ext.String.format(this.following ? this.followingTooltip : this.followTooltip, this.name),
				scope : this,
				handler : function(button) {
					var tooltip = Ext.tip.QuickTipManager.getQuickTip();
					if (tooltip)
						tooltip.hide();
					this.following = !this.following;
					if (this.following) {
						button.setText(this.followingText);
						button.setIcon(this.followingIcon);
						button.setTooltip(Ext.String.format(this.followingTooltip, this.name));
					} else {
						button.setText(this.followText);
						button.setIcon(this.followIcon);
						button.setTooltip(Ext.String.format(this.followTooltip, this.name));
					}

					var win = button.up('window');
					Ext.Ajax.request({
						url : '/resolve/service/social/followComponent',
						params : {
							follow : this.following,
							comptype : win.compType,
							sysid : win.sysId
						},
						success : function(r) {
							var response = Ext.decode(r.responseText);
							if (response.success) {
								//do nothing
							} else {
								//display error
							}
						}
					});
				}
			});
		}

		this.callParent();
	}
});

Ext.define('RS.view.panel.MenuSet', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.menuset',

	frame: false,
	closable: true,
	border: false,
	bodyBorder: false,

	initComponent: function() {
		this.loadMenuData();
		this.callParent(arguments);
	},
	loadMenuData: function() {
		Ext.Ajax.request({
			url: '/resolve/service/social/getMenuSet',
			params: {
				menuSetId: this.menuSetId
			},
			scope: this,
			success: this.loadedMenuData
		});
	},
	loadedMenuData: function(r) {
		var response = Ext.decode(r.responseText);
		if(response.success) {
			this.setTitle(response.data.name);
			var links = [],
				lastGroup = '',
				group = '<div style="padding: 3px 0px 3px 20px">{0}</div>',
				link = '<div style="padding: 3px 0px 3px 30px"><a onclick="{0}" href="javascript:void(0)" class="usefullinks">{1}</a></div>';

			Ext.each(response.data.menuItemModels, function(item) {
				if( item.group != lastGroup){
					lastGroup = item.group;
					links.push(Ext.String.format(group, item.group));
				}
				links.push(Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=' + encodeURIComponent(item.query) + '\', \'_top\')', item.name));
			});
			this.update(links.join(''));
		}
	}
});
/**
 * Panel to display the user image
 */
Ext.define('RS.view.panel.UserImagePanel', {
	extend : 'Ext.view.View',
	alias : 'widget.userimagepanel',
	id : 'userimagepanelId',
	border : false,
	store : 'UserImages',
	//@formatter:off
	itemTpl : [	'<div id="userImage" class="thumb">', 
					'<div><a href="{userHref}"><img src="{imgSrc}" border="0" alt="{title}" style="float: CONFIG" title="Edit Settings" /></a></div>', 
					'<div><h2>{userFullName}</h2></div>', 
				'</div>']
	//@formatter:on
});


/**
 * Panel to display links that will allow the user to quickly create components and follow other components
 */
Ext.define('RS.view.panel.WhatToDoWidget', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.whattodowidget',

	title: 'Useful Links',
	frame: false,
	closable: true,
	border: false,
	bodyBorder: false,

	initComponent: function() {
		var link = '<div style="padding: 3px 0px 3px 20px"><a onclick="{0}" href="javascript:void(0)" class="usefullinks">{1}</a></div>',

			createProcess = Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=' + encodeURIComponent('rsprofile.jsp?main=Process&action=create') + '\', \'_top\')', 'Create/Join Process'),
			createTeam = Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=' + encodeURIComponent('rsprofile.jsp?main=Teams&action=create') + '\', \'_top\')', 'Create/Join Team'),
			createForum = Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=' + encodeURIComponent('rsprofile.jsp?main=Forums&action=create') + '\', \'_top\')', 'Create/Join Forum'),
			createRss = Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=' + encodeURIComponent('rsprofile.jsp?main=RSS&action=create') + '\', \'_top\')', 'Create/Join RSS'),

			followUsers = Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=' + encodeURIComponent('rsprofile.jsp?main=User') + '\', \'_top\')', 'Follow Users'),
			followDocument = Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=' + encodeURIComponent('rsprofile.jsp?main=Document') + '\', \'_top\')', 'Follow Document'),
			followRunbook = Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=' + encodeURIComponent('rsprofile.jsp?main=Runbook') + '\', \'_top\')', 'Follow Runbook'),
			followActionTask = Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=' + encodeURIComponent('rsprofile.jsp?main=ActionTask') + '\', \'_top\')', 'Follow ActionTask'),

			createNewContentRequest = Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=/resolve/service/wiki/view/CR/New Content Request\', \'_blank\')', 'Create New Content Request');

		this.html = [createProcess, createTeam, createForum, createRss, followUsers, followDocument, followRunbook, followActionTask, createNewContentRequest].join('');
		this.callParent(arguments);
	}
});
/**
 * Viewport displayed when the user searches for a social component
 */
Ext.define('RS.view.SearchPostViewport', {
	extend: 'Ext.container.Viewport',

	requires: ['RS.view.form.PostForm', 'RS.view.panel.GenericPostPanel'],
	cls: 'rs-panel',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	defaults: {
		margins: '10'
	},

	/**
	 * @cfg selectionDetails {Object} Configuration to simulate the user clicking on a stream since no streams will be displayed on this view.  Configuration should be something like:
	 *
	 * selectionDetails : {
	 * 		displayName : social.compFullName,
	 * 		sys_id : social.compSysId,
	 * 		comptype : social.compType,
	 * 		parent_comptype : '',
	 * 		parent_sysid : '',
	 * 		hadSelection : true,
	 * 		searchPostKey: 'search from user',
	 * 		searchPostTime: 'ALL',
	 *      queryType: '',
	 *      resultsType: 'Post',
	 * 		doSearch: true
	 * 	}
	 *
	 */

	initComponent: function() {
		this.items = [{
			xtype: 'genericpostpanel',
			selectionDetails: this.selectionDetails,
			flex: 1
		}];
		this.callParent(arguments);
	}
});
/**
 * Tab to display a list of streams and the posts for the selected stream
 */
Ext.define('RS.view.tab.StreamTabPanel', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.streamtabpanel',

	requires : ['RS.view.panel.GenericPostPanel', 'RS.view.menu.RefreshButton'],

	border : false,
	cls : 'rs-panel',
	layout : {
		type : 'border',
		align : 'stretch'
	},

	initComponent : function() {
		var streamTree = {
			xtype : 'streamtree',
			split : true,
			region : 'west',
			border : false,
			flex : 1,
			streamName : this.name,
			minWidth: 150,
			maxWidth: 400
		};

		Ext.apply(streamTree, this.streamTreeConfig);
		this.items = [streamTree, {
			xtype : 'genericpostpanel',
			region : 'center',
			border : false,
			// style: 'padding-top: 22px;',
			flex : 5
		}];

		this.callParent();
	}
});

/**
 * Tree to display the Streams for a particular tab
 */
Ext.define('RS.view.tree.StreamTree', {
	extend: 'Ext.tree.Panel',
	alias: 'widget.streamtree',

	bodyPadding: '10px',
	requires: ['RS.model.Stream'],

	border: false,
	rootVisible: false,
	displayField: 'displayName',

	lines: false,
	cls: 'stream-tree',

	/**
	 * True to override leaf icon images with the @{RS.view.tree.StreamTree#nodeDisplayIconLeaf} property
	 */
	overrideLeafIconImages: true,

	/**
	 * True to override the folder icon images with the @{RS.view.tree.StreamTree#nodeDisplayIconFolder} property
	 */
	overrideFolderIconImages: true,

	/**
	 * Image to use when overriding the leaf icon in the tree
	 */
	nodeDisplayIconLeaf: '/resolve/js/extjs4/resources/themes/images/default/tree/s.gif',

	/**
	 * Image to use when overriding the folder icon in the tree
	 */
	nodeDisplayIconFolder: '/resolve/js/extjs4/resources/themes/images/default/tree/s.gif',

	/**
	 * Template string to use when displaying the folder in the tree
	 */
	nodeDisplayTemplate: '<span class="rs-stream-group-title">{0}</span>',

	leafDisplayTemplate: '{0}<div class="stream-menu" style="float: right"></div>',

	/**
	 * Css class to apply to the folder in the tree
	 */
	nodeDisplayCls: 'rs-stream-group-node',

	//State information
	stateful: true,
	stateEvents: ['itemcollapse', 'itemexpand'],

	initComponent: function() {
		var stream = this.streamName.toLowerCase();
		var url = Ext.String.format('/resolve/service/{0}cp', stream);

		this.store = Ext.create('Ext.data.TreeStore', {
			model: 'RS.model.Stream',
			proxy: {
				type: 'ajax',
				url: url,
				reader: {
					type: 'json',
					root: 'records'
				}
			},
			listeners: {
				scope: this,
				load: this.onTreeLoad,
				append: this.onNodeAppend
			}
		});

		this.tbar = {
			xtype: 'toolbar',
			cls: 'rs-toolbar',
			items: [{
				xtype: 'label',
				html: '<font size="2"><b>Streams</b></font>'
			}
			/*, '->',{
				xtype: 'button',
				icon: '/resolve/images/reload.png',
				tooltip: 'Reload Streams'
			}*/
			]
		};

		this.stateId = stream + 'Tree';

		this.callParent(arguments);

		if(Ext.isGecko) {
			//Firefox doesn't handle float:right elements properly, and because of that we have to manually go in and move the menu
			//div to the left of the entire tree structure so that it will properly be displayed with the menu floating on the right
			//instead of displaying on the next line.  This should happen for each of the nodes as they are added to the view so that it
			//happens immediately instead of relying on the expand/collapse of a particular node
			this.view.on('itemadd', function(records, index, node, eOpts) {
				Ext.each(records, function(record) {
					var node = this.getNode(record);
					var div = Ext.fly(node).down('div[class*=stream-menu]');
					if(div) {
						var owner = Ext.fly(div).parent();
						Ext.fly(div).remove();
						Ext.fly(owner).insertFirst(div);

					}
				}, this);
			});
		}
	},

	onTreeLoad: function(store, node, records, successful, eOpts) {
		//select the first leaf in the tree by default so that we always have a selection
		this.selectFirstNode(this.getRootNode().childNodes);
	},

	selectFirstNode: function(children) {
		for(var i = 0; i < children.length; i++) {
			if(children[i].isLeaf()) {
				this.getSelectionModel().select(children[i]);
				this.fireEvent('itemclick', this, []);
				return true;
			} else if(children[i].isExpanded()) {
				if(this.selectFirstNode(children[i].childNodes)) {
					return true;
				}
			}
		}
	},
	onNodeAppend: function(nodeInterface, node, index, eOpts) {
		if(node.data.leaf) { //] || !node.data.records || node.data.records.length == 0 ) {
			if(!node.data.systemStream) node.data.displayName = Ext.String.format(this.leafDisplayTemplate, node.data.displayName);
			if(this.overrideLeafIconImages) node.data.icon = this.nodeDisplayIconLeaf;
		} else {
			if(this.overrideFolderIconImages) node.data.icon = this.nodeDisplayIconFolder;
			node.data.displayName = Ext.String.format(this.nodeDisplayTemplate, node.data.displayName, node.data.records.length, node.data.records.length === 1 ? 'Item' : 'Items');
			node.data.cls = this.nodeDisplayCls;
		}
	},

	/**
	 * Gets the details about the currently selected item in the tree that we need when doing things like posting or refreshing the posts
	 */
	getSelectedDetails: function() {
		var params = {
			displayName: '',
			sys_id: '',
			comptype: '',
			parentCompType: '',
			parentSysId: '',
			permaLink: this.forumPermaLink
		}

		var selections = this.getSelectionModel().getSelection();
		if(selections.length > 0) {
			Ext.apply(params, selections[0].raw);
			params.hadSelection = true;
			if(selections[0].data.systemStream) {
				params.sys_id = '__sysId__';
			}
		}

		return params;
	},
	getState: function() {
		var nodes = [],
			state = this.callParent();

		this.getRootNode().eachChild(function(child) {
			// function to store state of tree recursively
			var storeTreeState = function(node, expandedNodes) {
					if(node.isExpanded() && node.childNodes.length > 0) {
						expandedNodes.push(node.getPath());

						node.eachChild(function(child) {
							storeTreeState(child, expandedNodes);
						});
					}
				};

			storeTreeState(child, nodes);
		});

		Ext.apply(state, {
			expandedNodes: nodes
		});

		return state;
	},
	applyState: function(state) {
		if(state) {
			var nodes = state.expandedNodes;
			if(nodes && nodes.length > 0) {
				for(var i = 0; i < nodes.length; i++) {
					if(typeof nodes[i] != 'undefined') {
						Ext.defer(this.expandPath, 100, this, [nodes[i]]);
					}
				}
			}
		}
		this.callParent(arguments);
	}
});
Ext.define('RS.view.Viewport', {
	extend: 'Ext.container.Viewport',
	layout: 'border',

	requires: ['RS.view.panel.UserImagePanel', 'RS.view.form.PostForm', 'RS.view.panel.WhatToDoWidget', 'RS.view.tab.StreamTabPanel', 'RS.view.tree.StreamTree', 'RS.view.panel.MenuSet'],

	border: false,
	cls: 'rs-panel',

	initComponent: function() {

		var tabpanel = {
			xtype: 'tabpanel',
			activeTab: 0,
			border: false,
			tabPosition: 'bottom',
			flex: 5,
			cls: 'rs-tab',
			items: [{
				title: 'Home',
				name: 'home',
				xtype: 'streamtabpanel'
			}]
		};

		if(social.socialuserProcessTab) tabpanel.items.push({
			title: 'Process',
			name: 'process',
			xtype: 'streamtabpanel'
		});

		if(social.socialuserTeamTab) tabpanel.items.push({
			title: 'Team',
			name: 'team',
			xtype: 'streamtabpanel'
		});

		if(social.socialuserForumTab) tabpanel.items.push({
			title: 'Forum',
			name: 'forum',
			xtype: 'streamtabpanel'
		});

		if(social.socialuserDocumentTab) tabpanel.items.push({
			title: 'Document',
			name: 'document',
			xtype: 'streamtabpanel'
		});

		if(social.socialuserRunbookTab) tabpanel.items.push({
			title: 'Runbook',
			name: 'runbook',
			xtype: 'streamtabpanel'
		});

		if(social.socialuserActionTaskTab) tabpanel.items.push({
			title: 'Action Task',
			name: 'actiontask',
			xtype: 'streamtabpanel'
		});

		if(social.socialuserUserTab) tabpanel.items.push({
			title: 'User',
			name: 'user',
			xtype: 'streamtabpanel'
		});

		if(social.socialuserFeedTab) tabpanel.items.push({
			title: 'Feed',
			name: 'feed',
			xtype: 'streamtabpanel'
		});

		if(social.socialuserAdvanceTab) tabpanel.items.push({
			title: 'Advanced',
			name: 'advanced',
			xtype: 'streamtabpanel',
			streamTreeConfig: {
				useLines: true,
				nodeDisplayTemplate: '{0}',
				nodeDisplayCls: '',
				overrideFolderIconImages: false
			}
		});

		this.items = [{
			region: 'center',
			xtype: 'panel',
			border: false,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'panel',
				border: false,
				height: 140,
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [{
					xtype: 'userimagepanel',
					border: false,
					flex: 1
				}, {
					xtype: 'postform',
					border: false,
					flex: 5
				}]
			},
			tabpanel]
		}];

		if(social.socialuserShowEastpanel) {
			var items = [];
			if(social.socialuserShowEastpanelWhatToDoWidget) {
				items.push({
					xtype: 'whattodowidget'
				})
			}

			if(social.menuSets) {
				var sets = social.menuSets.split(',');
				Ext.each(sets, function(set) {
					items.push({
						xtype: 'menuset',
						menuSetId: set
					});
				});
			}

			this.items.push({
				region: 'east',
				stateId: 'eastInfoPanel',
				title: 'Info',
				cls: 'rs-panel',
				collapsible: true,
				collapsed: true,
				split: true,
				border: false,
				width: 250,
				items: items
			})
		}

		this.callParent(arguments);
	}
});
/**
 * Window to display when the user clicks on the 'File' button when posting to a stream
 */
Ext.define('RS.view.window.FileUploadWindow', {
	extend: 'Ext.window.Window',
	alias: 'widget.fileuploadwindow',

	width: 400,
	title: 'File Upload',
	modal: true,
	layout: 'fit',

	items: [{
		xtype: 'form',
		border: false,
		url: '/resolve/service/post/attach',
		bodyPadding: '10',
		defaults: {
			anchor: '100%',
			msgTarget: 'side'
		},

		items: [{
			allowBlank: false,
			xtype: 'filefield',
			id: 'form-file',
			emptyText: 'Select a File',
			fieldLabel: 'File',
			name: 'file-path',
			buttonText: 'Select'
		}, {
			xtype: 'textfield',
			name: 'newfilename',
			fieldLabel: 'Description'
		}]
	}],
	buttonAlign: 'left',
	buttons: [{
		text: 'Upload',
		action: 'fileupload'
	}, {
		text: 'Reset',
		action: 'resetform'
	}, {
		text: 'Cancel',
		handler: function(button) {
			button.up('window').close();
		}
	}]

});
/**
 * Window to display when the user clicks on the 'Html' button when posting to a stream
 */
Ext.define('RS.view.window.HtmlWindow', {
	extend: 'Ext.window.Window',
	alias: 'widget.htmlwindow',

	modal: true,
	bodyBorder: false,
	border: false,

	autoShow: true,
	width: 650,
	height: 400,
	title: 'Html',
	layout: 'fit',
	items: [{
		xtype: 'htmleditor',
		enableLinks: false,
		cls: 'show-htmleditor-font',

		createToolbar: function(editor) {
			var me = this,
				items = [],
				i, tipsEnabled = Ext.tip.QuickTipManager && Ext.tip.QuickTipManager.isEnabled(),
				baseCSSPrefix = Ext.baseCSSPrefix,
				fontSelectItem, toolbar, undef;

			function btn(id, toggle, handler) {
				return {
					itemId: id,
					cls: baseCSSPrefix + 'btn-icon',
					iconCls: baseCSSPrefix + 'edit-' + id,
					enableToggle: toggle !== false,
					scope: editor,
					handler: handler || editor.relayBtnCmd,
					clickEvent: 'mousedown',
					tooltip: tipsEnabled ? editor.buttonTips[id] || undef : undef,
					overflowText: editor.buttonTips[id].title || undef,
					tabIndex: -1
				};
			}

			if(!Ext.isSafari2) {
				if(me.enableSourceEdit) {
					var source = btn('sourceedit', true, function(btn) {
						me.toggleSourceEdit(!me.sourceEditMode);
					});
					source.text = 'Source';
					delete source.cls;
					delete source.iconCls;
					items.push(source, '-');
				}
			}

			if(me.enableFont && !Ext.isSafari2) {
				fontSelectItem = Ext.widget('component', {
					renderTpl: ['<select id="{id}-selectEl" class="{cls}">', '<tpl for="fonts">', '<option value="{[values.toLowerCase()]}" style="font-family:{.}"<tpl if="values.toLowerCase()==parent.defaultFont"> selected</tpl>>{.}</option>', '</tpl>', '</select>'],
					renderData: {
						cls: baseCSSPrefix + 'font-select',
						fonts: me.fontFamilies,
						defaultFont: me.defaultFont
					},
					childEls: ['selectEl'],
					afterRender: function() {
						me.fontSelect = this.selectEl;
						Ext.Component.prototype.afterRender.apply(this, arguments);
					},
					onDisable: function() {
						var selectEl = this.selectEl;
						if(selectEl) {
							selectEl.dom.disabled = true;
						}
						Ext.Component.prototype.onDisable.apply(this, arguments);
					},
					onEnable: function() {
						var selectEl = this.selectEl;
						if(selectEl) {
							selectEl.dom.disabled = false;
						}
						Ext.Component.prototype.onEnable.apply(this, arguments);
					},
					listeners: {
						change: function() {
							me.relayCmd('fontname', me.fontSelect.dom.value);
							me.deferFocus();
						},
						element: 'selectEl'
					}
				});

				items.push(fontSelectItem, '-');
			}

			if(me.enableFontSize) {
				items.push(btn('increasefontsize', false, me.adjustFont), btn('decreasefontsize', false, me.adjustFont));
			}

			if(me.enableFormat) {
				items.push('-', btn('bold'), btn('italic'), btn('underline'));
			}

			if(me.enableAlignments) {
				items.push('-', btn('justifyleft'), btn('justifycenter'), btn('justifyright'));
			}

			if(me.enableColors) {
				items.push('-', {
					itemId: 'forecolor',
					cls: baseCSSPrefix + 'btn-icon',
					iconCls: baseCSSPrefix + 'edit-forecolor',
					overflowText: editor.buttonTips.forecolor.title,
					tooltip: tipsEnabled ? editor.buttonTips.forecolor || undef : undef,
					tabIndex: -1,
					menu: Ext.widget('menu', {
						plain: true,
						items: [{
							xtype: 'colorpicker',
							allowReselect: true,
							focus: Ext.emptyFn,
							value: '000000',
							plain: true,
							clickEvent: 'mousedown',
							handler: function(cp, color) {
								me.execCmd('forecolor', Ext.isWebKit || Ext.isIE ? '#' + color : color);
								me.deferFocus();
								this.up('menu').hide();
							}
						}]
					})
				}, {
					itemId: 'backcolor',
					cls: baseCSSPrefix + 'btn-icon',
					iconCls: baseCSSPrefix + 'edit-backcolor',
					overflowText: editor.buttonTips.backcolor.title,
					tooltip: tipsEnabled ? editor.buttonTips.backcolor || undef : undef,
					tabIndex: -1,
					menu: Ext.widget('menu', {
						plain: true,
						items: [{
							xtype: 'colorpicker',
							focus: Ext.emptyFn,
							value: 'FFFFFF',
							plain: true,
							allowReselect: true,
							clickEvent: 'mousedown',
							handler: function(cp, color) {
								if(Ext.isGecko) {
									me.execCmd('useCSS', false);
									me.execCmd('hilitecolor', color);
									me.execCmd('useCSS', true);
									me.deferFocus();
								} else {
									me.execCmd(Ext.isOpera ? 'hilitecolor' : 'backcolor', Ext.isWebKit || Ext.isIE ? '#' + color : color);
									me.deferFocus();
								}
								this.up('menu').hide();
							}
						}]
					})
				});
			}

			if(!Ext.isSafari2) {
				if(me.enableLinks) {
					items.push('-', btn('createlink', false, me.createLink));
				}

				if(me.enableLists) {
					items.push('-', btn('insertorderedlist'), btn('insertunorderedlist'));
				}
			}

			// Everything starts disabled.
			for(i = 0; i < items.length; i++) {
				if(items[i].itemId !== 'sourceedit') {
					items[i].disabled = true;
				}
			}

			// build the toolbar
			// Automatically rendered in AbstractComponent.afterRender's renderChildren call
			toolbar = Ext.widget('toolbar', {
				id: me.id + '-toolbar',
				ownerCt: me,
				cls: Ext.baseCSSPrefix + 'html-editor-tb',
				enableOverflow: true,
				items: items,
				ownerLayout: me.getComponentLayout(),

				// stop form submits
				listeners: {
					click: function(e) {
						e.preventDefault();
					},
					element: 'el'
				}
			});

			me.toolbar = toolbar;
		}
	}],
	buttonAlign: 'left',
	buttons: [{
		text: 'Add Html',
		action: 'addhtml'
	}, {
		text: 'Reset',
		action: 'resethtml'
	}, {
		text: 'Cancel',
		handler: function(button) {
			button.up('window').close();
		}
	}]

});
/**
 * Window to display that shows which users also liked the selected post.
 */
Ext.define('RS.view.window.LikeDialog', {
	extend : 'Ext.window.Window',
	alias : 'widget.likedialog',

	requires : ['RS.model.PeopleWhoLikeThis'],

	width : 500,
	height : 300,
	layout : 'fit',
	modal : true,
	title : 'People who like this',

	onEsc : function() {
		this.close();
	},

	initComponent : function() {

		this.items = [{
			xtype : 'gridpanel',
			border : false,
			bodyBorder : false,
			hideHeaders : true,
			buttonAlign : 'center',
			store : Ext.create('Ext.data.Store', {
				autoLoad : true,
				model : 'RS.model.PeopleWhoLikeThis',
				proxy : {
					type : 'ajax',
					url : '/resolve/service/social/getLikeData',
					reader : {
						root : 'records'
					},
					extraParams : {
						sysid : this.sys_id
					}
				}
			}),
			columns : [{
				text : 'Image',
				xtype : 'templatecolumn',
				tpl : '<div style="text-align:center"><img style="height:45px;width:45px" src="{photo}"/></div>'
			}, {
				text : 'Name',
				xtype : 'templatecolumn',
				tpl : '<span style="cursor:hand;cursor:pointer;font-size:14px" class="popupName" onclick="window.open(\'{gotoLink}\')">{name}</span><br/><span style="color: #888888;font-size:12px">{description}</span>',
				flex : 1
			}, {
				xtype : 'templatecolumn',
				//@formatting:off
				tpl : ['<tpl if="currentUser == true">', '<span style="text-align:center;font-size:14px">(You)</span>', '</tpl>', '<tpl if="currentUser == false &amp;&amp; following == true">', '<span style="font-size:14px;cursor:hand;cursor:pointer;" onclick="JavaScript:followUser(\'{sysid}\', false)"><image style="width:16px;height:16px;" src="/resolve/images/ok_st_obj.gif"/>Following</span>', '</tpl>', '<tpl if="currentUser == false &amp;&amp; following == false">', '<span style="font-size:14px;cursor:hand;cursor:pointer;" onclick="JavaScript:followUser(\'{sysid}\', true)"><image style="width:16px;height:16px;" src="/resolve/images/plus.gif"/>Follow</span>', '</tpl>'].join('')
				//@formatting:on
			}]
			// ,
			// buttons : [{
			// text : 'Done',
			// scope : this,
			// handler : function(button) {
			// this.close();
			// }
			// }]
		}];

		this.callParent(arguments);
	}
});

/**
 * Window to display when the user clicks the 'Link' button when posting to a stream
 */
Ext.define('RS.view.window.LinkWindow', {
	extend: 'Ext.window.Window',
	alias: 'widget.linkwindow',

	modal: true,

	layout: 'fit',
	width: 650,
	title: 'Add Link',

	items: [{
		xtype: 'form',
		bodyPadding: '10',
		border: false,
		defaults: {
			anchor: '100%',
			allowBlank: false,
			msgTarget: 'side'
		},
		items: [{
			xtype: 'radiogroup',
			fieldLabel: 'Select Type',
			//cls : 'x-check-group-alt',
			items: [{
				boxLabel: 'URL',
				name: 'type',
				inputValue: 'url',
				checked: true
			}, {
				boxLabel: 'Document',
				name: 'type',
				inputValue: 'document'
			}, {
				boxLabel: 'ActionTask',
				name: 'type',
				inputValue: 'actiontask'
			}, {
				boxLabel: 'Worksheet',
				name: 'type',
				inputValue: 'worksheet'
			}, {
				boxLabel: 'Forum',
				name: 'type',
				inputValue: 'forum'
			}, {
				boxLabel: 'RSS',
				name: 'type',
				inputValue: 'rss'
			}]
		}, {
			xtype: 'textfield',
			name: 'tfValue',
			fieldLabel: 'Value'
		}, {
			xtype: 'combo',
			name: 'cbValue',
			store: 'LinkStore',
			displayField: 'name',
			typeAhead: false,
			fieldLabel: 'Value',
			hideTrigger: true,
			anchor: '100%',
			minChars: 1,
			hidden: true,
			listConfig: {
				loadingText: 'Searching...',
				emptyText: 'No matching posts found.'
			}
		}, {
			xtype: 'textfield',
			name: 'tfName',
			fieldLabel: 'Description'
		}]
	}],
	buttonAlign: 'left',
	buttons: [{
		text: 'Add Link',
		action: 'addlink'
	}, {
		text: 'Reset',
		action: 'resetlink'
	}, {
		text: 'Cancel',
		handler: function(button) {
			button.up('window').close();
		}
	}]

});
/**
 * Social Controller is the main controller that deals with all the logic for the Social application
 * It handles the interaction between the different controls, as well as dealing with posting to and refreshing feeds and streams for users
 */
Ext.define('RS.controller.SocialController', {
	extend: 'Ext.app.Controller',

	requires: ['RS.view.form.FollowUserForm', 'RS.view.form.FollowComponentForm', 'RS.view.window.LikeDialog', 'RS.view.control.ImageViewer'],
	stores: ['LinkStore', 'MentionStore'],
	uses: ['RS.view.window.HtmlWindow', 'RS.view.window.LinkWindow', 'RS.view.window.FileUploadWindow'],

	refs: [{
		ref: 'streamTabs',
		selector: 'viewport > panel > tabpanel'
	}, {
		ref: 'postForm',
		selector: 'postform'
	}, {
		ref: 'fileUploadForm',
		selector: 'fileuploadwindow > form'
	}, {
		ref: 'linkFormRadioGroup',
		selector: 'linkwindow > form > radiogroup'
	}, {
		ref: 'linkFormTextfieldName',
		selector: 'linkwindow > form > textfield[name=tfName]'
	}, {
		ref: 'linkFormTextfieldValue',
		selector: 'linkwindow > form > textfield[name=tfValue]'
	}, {
		ref: 'linkFormCombobox',
		selector: 'linkwindow > form > combo'
	}, {
		ref: 'linkForm',
		selector: 'linkwindow > form'
	}, {
		ref: 'htmlEditor',
		selector: 'htmlwindow > htmleditor'
	}],

	init: function() {
		var me = this;

		me.isForum = false;

		me.control({
			'viewport > panel > tabpanel': {
				tabchange: me.tabChange
			},
			'viewport > panel > tabpanel > panel > streamtree': {
				beforeitemclick: me.beforeItemClick,
				itemclick: me.refreshButtonClicked
			},
			'viewport > panel > toolbar > refreshbutton': {
				click: me.refreshButtonClicked
			},

			'postform  button[action=post]': {
				click: me.submitPost
			},
			'postform  button[action=showfileupload]': {
				click: me.showFileUploadWindow
			},
			'postform  button[action=showlink]': {
				click: me.showLinkWindow
			},
			'postform  button[action=showhtml]': {
				click: me.showHtmlWindow
			},

			'postform  button[action=refreshPostPanel]': {
				click: me.refreshButtonClicked
			},
			'postform  menu > menuitem[action=autorefresh]': {
				click: me.toggleAutoRefresh
			},

			'fileuploadwindow button[action=fileupload]': {
				click: me.uploadFile
			},
			'fileuploadwindow button[action=resetform]': {
				click: me.resetUploadForm
			},

			'linkwindow > form > radiogroup': {
				change: me.rgSelectType
			},
			'linkwindow > form > textfield[name=tfName]': {
				focus: me.tfNameOnFocus
			},
			'linkwindow button[action=addlink]': {
				click: me.addLink
			},
			'linkwindow button[action=resetlink]': {
				click: me.resetLinkForm
			},

			'htmlwindow button[action=addhtml]': {
				click: me.addHtml
			},
			'htmlwindow button[action=resethtml]': {
				click: me.resetHtmlForm
			}
		});

		//this works when fireEvent done from RS.app
		this.application.on({
			'refreshPostPanelEvent': {
				fn: this.refreshPostPanel,
				scope: this
			},
			'toggleStarEvent': {
				fn: this.toggleStar,
				scope: this
			},
			'likeLinkOnClickEvent': {
				fn: this.likeLinkOnClick,
				scope: this
			},
			'submitCommentEvent': {
				fn: this.submitComment,
				scope: this
			},
			'showCommentEvent': {
				fn: this.showComment,
				scope: this
			},
			'showPopupOnMouseOverEvent': {
				fn: this.showPopupOnMouseOver,
				scope: this
			},
			'perhapsHidePopupOnMouseOutEvent': {
				fn: this.perhapsHidePopupOnMouseOut,
				scope: this
			},
			'hidePopupOnMouseOutEvent': {
				fn: this.hidePopupOnMouseOut,
				scope: this
			},
			'displayLikeDialog': {
				fn: this.displayLikeDialog,
				scope: this
			},
			'followUser': {
				fn: this.followUser,
				scope: this
			},
			'deletePostEvent': {
				fn: this.deletePost,
				scope: this
			},
			'deleteCommentEvent': {
				fn: this.deleteComment,
				scope: this
			},
			'displayLargeImage': {
				fn: this.displayLargeImage,
				scope: this
			},
			'showMoreForum': {
				fn: this.showMoreForum,
				scope: this
			},
			'returnForum': {
				fn: this.returnForum,
				scope: this
			}
		});

		//Start grid infinite scrolling poller to check when to get additional records
		Ext.TaskManager.start({
			scope: this,
			run: function() {
				var me = this;
				this.updatePostTimer();
			},
			interval: refreshIntervalForInfiniteRecords() //in milliseconds
		});

		me.getLinkStoreStore().on({
			beforeload: this.onBeforeLoadOfLinkStore,
			scope: this
		});

		me.refreshPostTask = Ext.TaskManager.newTask({
			scope: this,
			run: function() {
				this.refreshPostPanel();
			},
			interval: refreshIntervalForAutoRefreshPost() //in milliseconds
		});
	},

	getCurrentlySelectedStream: function() {
		var tabPanels = Ext.ComponentQuery.query('viewport > panel > tabpanel');
		var activeTab = tabPanels[0].getActiveTab();
		var streamTree = activeTab.down('streamtree');
		var selection = streamTree.getSelectionModel().getSelection();
		if (selection.length > 0) {
			return selection[0];
		}
		return null;
	},

	tabChange: function() {
		//Only get data from the server if there is no data currently loaded
		var tabPanels = Ext.ComponentQuery.query('viewport > panel > tabpanel');
		var activeTab = tabPanels[0].getActiveTab();
		var activePanel = activeTab.down('genericpostpanel');
		if (activePanel) {
			var store = activePanel.store;
			if (store && store.getCount() < 1) {
				this.refreshPostPanel();
			}
		}

		var currentNode = this.getCurrentlySelectedStream();

		if (currentNode && currentNode.isLeaf()) {
			//Update the 'post to' text with the proper display
			this.setPostToText(currentNode.data.displayName, currentNode.data.comptype);
		}
	},
	refreshStreams: function() {
		//get handle to current viewable tree and refresh it to get the latest streams
		var tabPanels = Ext.ComponentQuery.query('viewport > panel > tabpanel');
		var activeTab = tabPanels[0].getActiveTab();
		var streamTree = activeTab.down('streamtree');
		if (streamTree) {
			streamTree.getStore().load();
		}
	},
	beforeItemClick: function(view, record, item, index, e, eOpts) {
		var menuClicked = e.getTarget('.stream-menu');
		if (!record.isLeaf()) {
			if (record.data.displayName && record.data.comptype) {
				this.setPostToText(record.data.displayName, record.data.comptype);
			} else {
				if (record.isExpanded()) record.collapse();
				else record.expand();
				return false;
			}
		} else {
			if (!menuClicked) {
				//Update the 'post to' text with the proper display
				this.returnForum();
				this.setPostToText(record.data.displayName, record.data.comptype);
			} else {
				//display the "context" menu for the node instead of selecting the record
				//Now we need to display the proper context menu for this stream
				//if we're looking at the home tab, then obviously we are trying to remove from home
				var tabPanels = Ext.ComponentQuery.query('viewport > panel > tabpanel'),
					activeTab = tabPanels[0].getActiveTab(),
					menuItem;
				if (activeTab.name == 'home') {
					menuItem = {
						text: 'Remove from Home',
						remove: true
					};
				} else {
					//Otherwise we're trying to add to home, but should only add if its not already there
					var currentNode = record;
					var homeTab = tabPanels[0].items.items[0];

					var homeStreamTree = homeTab.down('streamtree');
					var homeRootNode = homeStreamTree.getRootNode();
					var foundNode = homeRootNode.findChild('sys_id', currentNode.get('sys_id'), true);
					if (foundNode) {
						menuItem = {
							text: 'Remove from Home',
							remove: true
						};
					} else {
						menuItem = {
							text: 'Add to Home',
							remove: false
						};
					}
				}

				if (menuItem) {
					var menuClickEl = Ext.get(menuClicked);
					menuClickEl.addCls('stream-menu-expanded');
					menuItem.scope = this;
					menuItem.handler = this.toggleDisplayOnHome;
					Ext.create('Ext.menu.Menu', {
						margin: '0 0 10 0',
						items: [menuItem, {
							text: 'Edit',
							handler: this.editMenuItem,
							scope: this
						}],
						listeners: {
							hide: function() {
								menuClickEl.removeCls('stream-menu-expanded');
							},
							close: function() {
								menuClickEl.removeCls('stream-menu-expanded');
							}
						}
					}).showBy(menuClickEl); //showAt(e.getX(), e.getY(), true);
				}
				return false;
			}
		}
	},
	toggleAutoRefresh: function() {
		var me = this;
		if (me.refreshPostTask.stopped) {
			me.refreshPostTask.start();
			Ext.state.Manager.set('socialAutoRefresh', true);
		} else {
			me.refreshPostTask.stop();
			Ext.state.Manager.set('socialAutoRefresh', false);
		}
	},
	refreshButtonClicked: function() {
		this.showedComment = false;
		this.refreshPostPanel();
	},
	refreshPostPanel: function() {
		if (this.showedComment) return;
		//get handle to the tab panel to find out the active tab
		var tabPanels = Ext.ComponentQuery.query('viewport > panel > tabpanel');
		if (tabPanels && tabPanels.length > 0) {
			var activeTab = tabPanels[0].getActiveTab();
			var activePanel = activeTab.down('genericpostpanel');

			if (activePanel) {
				var store = activePanel.store;

				if (store) {
					//reset the flag
					store.getProxy().extraParams.endOfRecs = false;

					//Defer the loading to allow the backend to process the new post fully
					Ext.Function.defer(store.load, 500, store);
				}
			}
		} else {
			//We don't have the tabs and we're looking at a one-off like view document or search
			var panels = Ext.ComponentQuery.query('viewport > genericpostpanel');
			if (panels && panels.length > 0) {
				var store = panels[0].store;
				if (store) {
					//reset the flag
					store.getProxy().extraParams.endOfRecs = false;

					//Defer the loading to allow the backend to process the new post fully
					Ext.Function.defer(store.load, 500, store);
				}
			}
		}
	},
	updatePostTimer: function() {
		//for calculations refer - http://net.tutsplus.com/tutorials/javascript-ajax/how-to-create-an-infinite-scroll-web-gallery/
		var postPanel;
		var tabs = this.getStreamTabs();
		if (tabs) {
			var activeTab = this.getStreamTabs().getActiveTab();
			if (activeTab) postPanel = activeTab.down('genericpostpanel');
		} else {
			postPanel = Ext.ComponentQuery.query('viewport > genericpostpanel')[0];
		}
		var postPanelEl = postPanel.getEl();

		//get the content height
		var contentHeight = 0;
		if (postPanelEl && (postPanel.calculateHeight || postPanel.contentHeight < 1)) {
			var feedContainers = postPanelEl.query('.feedcontainer');
			Ext.each(feedContainers, function(container) {
				contentHeight += Ext.fly(container).getHeight()
			});
			postPanel.contentHeight = contentHeight;
			postPanel.calculateHeight = false;
		} else {
			contentHeight = postPanel.contentHeight;
		}

		var scrollPosition = postPanel.body.dom.scrollTop;
		var pageHeight = document.documentElement.clientHeight;
		//same as viewport height
		var viewportHeight = Ext.getBody().getViewSize().height;
		var calculatedHeight = contentHeight - viewportHeight - scrollPosition;

		if (contentHeight > 500 && scrollPosition > 0 && calculatedHeight < 500) {
			//console.log(calculatedHeight + " - " + contentHeight + " - " + pageHeight + " - " + viewportHeight + " - " + scrollPosition + " -- scroll LESS than 500");
			var store = postPanel.store;
			var params = store.getProxy().extraParams;
			if (!params.endOfRecs) {
				params.infiniteScroll = true;
				store.load();
			}
		} else {
			//console.log(calculatedHeight + " - " + contentHeight + " - " + pageHeight + " - " + viewportHeight + " - " + scrollPosition + " -- scroll GREATER than 500");
		}
	},
	editMenuItem: function(menu, item, eventObj, eOpts) {
		var record = this.getCurrentlySelectedStream();
		if (record) {
			var type = record.data.comptype,
				sysId = record.data.sys_id,
				displayName = record.raw.displayName;
			//Now depending on the type of this record, we need to construct the link to edit the component properly
			var url = '';
			switch (type) {
				case 'Actiontasks':
					url = Ext.String.format('/resolve/jsp/rsclient.jsp?url=/resolve/jsp/rsactiontask.jsp?sys_id={0}', sysId);
					break;
				case 'Documents':
				case 'Runbooks':
					url = Ext.String.format('/resolve/jsp/rsclient.jsp?url=/resolve/jsp/rswiki.jsp?wiki={0}', displayName);
					break;
				case 'Users':
					//Get current username from cookie
					var cookies = document.cookie.split('; '),
						currentUserName = '';
					Ext.each(cookies, function(cookie) {
						var split = cookie.split('=');
						if (split[0] == 'RSTOKEN') {
							currentUserName = split[1].split('/')[0].substring(1);
							return false;
						}
					});
					url = Ext.String.format('/resolve/jsp/rsclient.jsp?url=/resolve/jsp/rsprofile.jsp?main=Profile{0}', (displayName == currentUserName ? '' : '&username=' + displayName));
					break;
				case 'Process':
				case 'RSS':
				case 'Forums':
				case 'Teams':
					url = Ext.String.format('/resolve/jsp/rsclient.jsp?url=rsprofile.jsp?main={0}&sysId={1}', type, sysId);
					break;
			}

			if (url) window.open(url);
		}

	},
	toggleDisplayOnHome: function(menu, item, eventObj, eOpts) {
		var record = this.getCurrentlySelectedStream();

		if (record.get('systemStream')) return;

		Ext.Ajax.request({
			url: '/resolve/service/social/managefav',
			params: {
				isAdd: !menu.remove,
				displayName: record.get('displayName'),
				sysid: record.get('sys_id'),
				comptype: record.get('comptype')
			},
			scope: this,
			success: function(r) {
				var response = Ext.decode(r.responseText);
				if (response.success) {
					//refresh the home streams
					var tabPanels = Ext.ComponentQuery.query('viewport > panel > tabpanel');
					var homeTab = tabPanels[0].items.items[0];
					var streamTree = homeTab.down('streamtree');
					if (streamTree) {
						streamTree.getStore().load();
					}
					menu.remove = !menu.remove;
					menu.setText(menu.remove ? 'Remove from Home' : 'Add to Home');
					RS.UserMessage.msg({
						success: true,
						title: 'Info',
						msg: response.data,
						duration: 4000,
						closeable: true
					});
				}
			}
		});
	},
	toggleStar: function(elId, postId) {
		var el = Ext.get(elId);
		var dataSpan = el.child('span');
		var isStarred = dataSpan.dom.innerHTML;

		Ext.Ajax.request({
			url: '/resolve/service/social/toggleStar',
			params: {
				postId: postId,
				isStarred: isStarred
			},
			success: function(r) {
				var response = Ext.decode(r.responseText);
				var starFlag = response.data || false;

				var imgEl = el.child('img');
				if (!starFlag) {
					imgEl.set({
						src: '../images/social/starOff.png'
					});
					dataSpan.update('false');
				} else {
					imgEl.set({
						src: '../images/social/starOn.png'
					});
					dataSpan.update('true');
				}
			}
		});
	},
	likeLinkOnClick: function(postId) {
		//Disable the like button
		var postDiv = Ext.get(postId),
			likeLink = postDiv.query('span[class=__likeLink__]'),
			likeLinkEl = Ext.get(likeLink[0]),
			match, result = "",
			regex = /<a.*>(.*?)<\/a>/ig;

		while (match = regex.exec(likeLinkEl.dom.innerHTML)) {
			result += match[1];
		}
		likeLinkEl.update(result);

		Ext.Ajax.request({
			url: '/resolve/service/social/likePost',
			params: {
				postId: postId
			},
			success: function(r) {
				var response = Ext.decode(r.responseText);
				if (response.success) {
					//update the count
					var likeCount = response.data || 0,
						postDiv = Ext.get(postId),
						noOfPeopleLiked = postDiv.query('span[class=__noOfPeopleLiked__]'),
						noOfPeopleLikedEl = Ext.get(noOfPeopleLiked[0]),
						oldCount = Number(noOfPeopleLikedEl.dom.innerHTML);
					if (oldCount == 0) {
						//add in the link to properly display the popup
						var likeCountContainer = Ext.get(postDiv.query('span[class=likeCountContainer]')[0]);
						likeCountContainer.update(Ext.String.format('<a href="JavaScript:void(0);" onclick="displayLikeDialog(\'{0}\')"><span class="__noOfPeopleLiked__">{1}</span>&nbsp;&nbsp;<img src="../images/social/like.png" /></a>', postId, likeCount));
					} else {
						noOfPeopleLikedEl.update(likeCount);
					}
				}
			}
		});

	},
	submitComment: function(elId, postId) {

		var buttonEl = Ext.get(elId);
		var formEl = buttonEl.parent();
		var comment = formEl.child('textarea');
		var commentValue = comment.getValue();
		this.showedComment = false;
		Ext.Ajax.request({
			url: '/resolve/service/submitComment',
			params: {
				postId: postId,
				comment: commentValue
			},
			scope: this,
			success: function(r) {
				var response = Ext.decode(r.responseText);
				if (response.success) {
					var newComment = commentTpl.applyTemplate(response.data);

					var postDiv = Ext.get(r.request.options.params.postId);
					var cDiv = postDiv.query("*[class=commentsCls]");
					var showHideCommentLinks = postDiv.query('.showHideComments');
					if (showHideCommentLinks.length > 0) {
						var showHideCommentLink = Ext.get(showHideCommentLinks[0]);
						showHideCommentLink.setVisible(true);
					}
					var cEl = Ext.get(cDiv[0]);
					cEl.insertHtml('beforeEnd', newComment);

					comment.dom.value = "";
					this.hideComment(postId);
				}
			}
		});
	},
	showComment: function(elId, _ui_id) {
		this.showedComment = true;
		var postDivs = Ext.query('[id=' + _ui_id + ']');
		var postDiv = null; //Ext.get(_ui_id);
		Ext.Array.each(postDivs, function(pDiv) {
			if (Ext.fly(pDiv).isVisible(true)) {
				postDiv = Ext.get(pDiv)
				return false
			}
		})
		if (postDiv) {
			var showHideCommentLinks = postDiv.query('.showHideComments');
			if (showHideCommentLinks.length > 0) {
				var showHideCommentLink = Ext.get(showHideCommentLinks[0]);
				showHideCommentLink.setVisible(true);
				toggleCommentDisplay(showHideCommentLinks[0], true);
			}

			//get the Post Div
			var cDiv = postDiv.query("*[class=cxnewcomment]");
			//grab the comments div
			var taDivEl = Ext.get(cDiv[0]);
			//convert it into an Element object
			taDivEl.show({
				duration: 500
			});
			//show it
			//stop the event propagation for this textarea as there are some issues otherwise
			var taArr = taDivEl.query('div textarea');
			//get the array of textarea, in this case there is only 1
			var elTA = Ext.get(taArr[0]);
			if (elTA) {
				//bring focus to the text area so users can immediately start typing
				elTA.focus();

				// IE, Chrome and Safari
				elTA.on('keydown', function(eventObj, htmlElementObj) {
					eventObj.stopPropagation();
				});

				// FF and Opera
				elTA.on('keypress', function(eventObj, htmlElementObj) {
					eventObj.stopPropagation();
				});
			}

			//**js for textarea -- in the textareaResizer.js file
			cleanForm();
		}
	},
	hideComment: function(sysId) {
		var postDiv = Ext.get(sysId);
		var cDiv = postDiv.query("*[class=cxnewcomment]");
		var taDivEl = Ext.get(cDiv[0]);
		taDivEl.setVisibilityMode(Ext.Element.DISPLAY);
		taDivEl.hide();
	},
	showPopupOnMouseOver: function(el, compType, sysId) {
		// console.log('mouse over event triggered - ' + compType + ' - ' + sysId);
		var viewport = Ext.getBody();
		var element = Ext.get(el);
		var compDisplayName = element.dom.innerHTML;

		//Cleanup the window here because the animations are sometimes still running for some reason when we're trying to close which causes errors
		if (this.popupWin) {
			this.popupWin.getEl().stopAnimation();
			this.popupWin.close();
			this.popupWin = null;
		}

		if (!this.popupWin) {
			var x = Math.round((element.getX() + element.getWidth() / 2)),
				y = element.getY() + element.getHeight();
			var height = 110,
				width = 300;
			//determine position of window to not go outside of visible area
			if (x + width > viewport.getWidth()) x = Math.round((element.getX() + element.getWidth() / 2)) - width;
			if (y + height > viewport.getHeight()) y = element.getY() - height;
			this.popupWin = Ext.create('Ext.window.Window', {
				layout: 'fit',
				border: false,
				width: width,
				height: height,
				style: 'border: 1px solid #888888',
				draggable: false,
				closable: false,
				preventHeader: true,
				baseCls: 'x-panel',
				resizable: false,
				x: x,
				y: y,
				compType: compType,
				compDisplayName: compDisplayName,
				sysId: sysId,
				anim: {
					from: {
						opacity: 0
					},
					duration: 500
				},
				listeners: {
					show: function(win) {
						win.getEl().fadeIn(win.anim);
						//win.getEl().shadow.el.fadeIn(win.anim);
						Ext.Ajax.request({
							url: '/resolve/service/social/getPopupData',
							params: {
								comptype: win.compType,
								compDisplayName: win.compDisplayName,
								sysid: win.sysId
							},
							scope: win,
							success: function(r) {
								var response = Ext.decode(r.responseText);
								if (response.success) {
									Ext.applyIf(response.data, {
										compType: this.compType,
										sysId: this.sysId
									});
									this.add(response.data);
								}
							}
						});
					},
					render: function(win) {
						win.mon(win.getEl(), {
							mouseout: {
								scope: win,
								fn: function(e, html, eOpts) {
									Ext.Function.defer(function() {
										perhapsHidePopupOnMouseOut(this.getEl(), e)
									}, 1000, this);
								}
							}
						})
					}
				}
			});
			this.popupWin.show();
		}
	},
	hidePopupOnMouseOut: function() {
		if (this.popupWin) {
			this.popupWin.getEl().fadeOut({
				duration: 500,
				scope: this.popupWin,
				callback: function() {
					this.hide();
				}
			});
		}
	},
	perhapsHidePopupOnMouseOut: function(el, e) {
		if (this.popupWin) {
			var e = Ext.isIE ? new Ext.EventObjectImpl(e) : Ext.EventObject;
			var win = this.popupWin;
			var position = win.getPosition(),
				winX = position[0],
				winY = position[1],
				height = win.getHeight(),
				width = win.getWidth();
			var eX = e.getPoint()[0],
				eY = e.getPoint()[1];
			if (eX <= winX || eX >= winX + width || eY <= winY - 5 || eY >= winY + height) {
				hidePopupOnMouseOut();
			}
		}
	},
	displayLikeDialog: function(sys_id) {
		if (!this.likeDialog) {
			this.likeDialog = Ext.create('RS.view.window.LikeDialog', {
				sys_id: sys_id,
				listeners: {
					close: {
						scope: this,
						fn: function(win) {
							this.likeDialog = null;
						}
					}
				}
			});
			this.likeDialog.show();
		}
	},
	followUser: function(sys_id, following) {
		Ext.Ajax.request({
			url: '/resolve/service/social/followUser',
			params: {
				sysid: sys_id,
				follow: following
			},
			scope: this,
			success: function(r) {
				var response = Ext.decode(r.responseText);
				if (response.success) {
					//refresh the grid in the dialog
					if (this.likeDialog) {
						var grid = this.likeDialog.down('grid');
						if (grid) {
							grid.store.load();
						}
					}
				} else {
					//display error
				}
			}
		});
	},
	deletePost: function(postId) {
		this.deleteComment(postId, '');
	},
	deleteComment: function(postId, commentId) {
		Ext.Ajax.request({
			url: '/resolve/service/social/deletepost',
			params: {
				postsysid: postId,
				commentsysid: commentId
			},
			scope: this,
			success: function(r) {
				var response = Ext.decode(r.responseText);
				if (response.success) {
					//Remove the post/comment element rather than refreshing the post panel information
					if (postId) {
						if (commentId) {
							//Remove comment
							Ext.get(commentId).remove();
						} else {
							//Removing post
							Ext.get(postId).remove();
						}
					}
					// Ext.Function.defer(function() {
					// 	this.refreshPostPanel();
					// }, 100, this);
				} else {
					//display error
				}
			}
		});
	},
	setPostToText: function(selectedName, comptype) {
		this.currentlySelectedName = selectedName;
		this.isForum = false;
		var targetLabel = this.getPostForm().down('displayfield');
		if (selectedName && selectedName != comptype) {
			targetLabel.setValue('<b>' + selectedName + '</b>');
		} else {
			//default to Blog
			targetLabel.setValue('<b>Blog</b>');
		}
	},

	displayLargeImage: function(url, displayName) {
		if (!this.imageWindow) {
			this.imageWindow = Ext.create('RS.view.control.ImageViewer', {
				src: url,
				title: displayName
			});
		} else {
			this.imageWindow.setTitle(displayName);
			this.imageWindow.setSrc(url);
		}
		this.imageWindow.show();
	},

	getCompToPost: function() {
		var params = {
			targetSysId: '__sysId__',
			//sysId of the currently selected item in the tree
			targetCompType: 'Blog' //comptype from selected item in tree
		};
		//Get the currently selected item in the tree and set the proper values
		var tabPanels = Ext.ComponentQuery.query('viewport > panel > tabpanel');
		if (tabPanels && tabPanels.length > 0) {
			var activeTab = tabPanels[0].getActiveTab();
			var streamTree = activeTab.down('streamtree');
			if (streamTree) {
				var selection = streamTree.getSelectionModel().getSelection();
				if (selection.length > 0) {
					if (!selection[0].data.systemStream) {
						params.targetCompType = selection[0].data.comptype;
						params.targetSysId = selection[0].data.sys_id;
					}
				}
			}
		} else {
			var panels = Ext.ComponentQuery.query('viewport > genericpostpanel');
			if (panels && panels.length > 0) {
				var selectionDetails = panels[0].selectionDetails;
				params.targetCompType = selectionDetails.comptype;
				params.targetSysId = selectionDetails.sys_id;
			}
		}
		return params;
	},

	submitPost: function() {
		var me = this;

		var params = this.getPostForm().getForm().getValues();
		Ext.apply(params, this.getCompToPost());

		if (me.isForum) {
			//really not posting, adding a comment
			var myMask = new Ext.LoadMask(this.getPostForm(), {
				msg: 'Commenting...'
			});
			myMask.show();
			Ext.Ajax.request({
				url: '/resolve/service/submitComment',
				params: {
					postId: this.currentForumPostId,
					comment: params.postContents
				},
				scope: this,
				success: function(r) {
					myMask.hide();
					var response = Ext.decode(r.responseText);
					if (response.success) {
						//Reset the individual fields so that we don't wipe out the 'posting to' text
						this.getPostForm().down('textarea').setValue('');
						this.getPostForm().down('hiddenfield').setValue('');
						this.refreshPostPanel();
					}
				}
			});
			return;
		}


		if (params.postContents) {
			//In case there is a delay posting something to the server (if a file is there or something), then display a "Posting..." message so the user knows what's going on.
			var myMask = new Ext.LoadMask(this.getPostForm(), {
				msg: 'Posting...'
			});
			myMask.show();
			Ext.Ajax.request({
				url: '/resolve/service/submitPost',
				params: params,
				scope: this,
				success: function(r) {
					myMask.hide();
					var response = Ext.decode(r.responseText);
					if (response.success) {
						//Reset the individual fields so that we don't wipe out the 'posting to' text
						this.getPostForm().down('textarea').setValue('');
						this.getPostForm().down('hiddenfield').setValue('');
						this.refreshPostPanel();
					}
				},
				failure: function() {
					myMask.hide();
				}
			});
		}
	},
	showFileUploadWindow: function() {
		Ext.widget('fileuploadwindow').show();
	},
	showLinkWindow: function() {
		Ext.widget('linkwindow').show();
	},
	showHtmlWindow: function() {
		Ext.widget('htmlwindow').show();
	},
	uploadFile: function() {
		var formToSubmit = this.getFileUploadForm().getForm();
		var values = formToSubmit.getValues();
		if (formToSubmit.isValid()) {
			formToSubmit.submit({
				clientValidation: true,
				waitMsg: 'Uploading your file...',
				params: {
					postid: values.postsysid
				},
				scope: this,
				success: function(form, action) {
					RS.UserMessage.msg({
						success: true,
						title: 'Success',
						msg: action.result.msg,
						duration: 4000,
						closeable: true
					});

					//update the post
					var textarea = this.getPostForm().down('textarea');
					var newValue = textarea.getValue() + ' ' + action.result.fileurl;
					textarea.setValue(newValue);

					//set the postid
					var postsysididHidden = this.getPostForm().down('hiddenfield');
					postsysididHidden.setValue(action.result.postid);

					//close the window
					var windowOwner = this.getFileUploadForm().up('window');
					windowOwner.close();
				},
				failure: function(form, action) {
					switch (action.failureType) {
						case Ext.form.action.Action.CLIENT_INVALID:
							RS.UserMessage.msg({
								success: false,
								title: 'Failure',
								msg: 'Form fields may not be submitted with invalid values',
								duration: 0,
								closeable: true
							});
							break;
						case Ext.form.action.Action.CONNECT_FAILURE:
							RS.UserMessage.msg({
								success: false,
								title: 'Failure',
								msg: 'Ajax communication failed',
								duration: 0,
								closeable: true
							});
							break;
						case Ext.form.action.Action.SERVER_INVALID:
							RS.UserMessage.msg({
								success: false,
								title: 'Failure',
								msg: action.result.msg,
								duration: 0,
								closeable: true
							});
							break;
					}

					var windowOwner = form.owner.ownerCt;
					windowOwner.close();
				}
			});
		}
	},
	rgSelectType: function(thisValue, newValue, oldValue, eOpts) {
		var values = thisValue.getValue();
		//making sure that it is a String and not an Array
		if (Ext.isString(values.type)) {
			this.getLinkFormTextfieldName().setValue('');
			//reset the name
			if (values.type == 'url' || values.type == 'worksheet') {
				//textfield
				this.getLinkFormTextfieldValue().show();
				this.getLinkFormCombobox().hide();
			} else {
				//combobox
				this.getLinkFormTextfieldValue().hide();
				this.getLinkFormCombobox().show();
			}
		}
	},
	tfNameOnFocus: function(thisField, opts) {

		thisField.setValue("");
		var cbValue = this.getLinkFormCombobox().getRawValue();
		var tfValue = this.getLinkFormTextfieldValue().getValue();
		var selectedRadio = this.getLinkFormRadioGroup().getValue().type;

		if (selectedRadio == 'url' || selectedRadio == 'worksheet') {
			if (tfValue) {
				thisField.setValue(tfValue);
			}
		} else {
			if (cbValue) {
				thisField.setValue(cbValue);
			}
		}

	},
	addLink: function() {
		var formToSubmit = this.getLinkForm().getForm();
		formToSubmit.isValid();
		//Do validation and mark the appropriate fields as invalid so the user knows what to correct
		var values = formToSubmit.getValues();
		var type = values.type;
		var tfValue = values.tfValue;
		var cbmodels = this.getLinkFormCombobox().valueModels;
		var model = null;
		var tfName = null;
		if (values.tfName) {
			tfName = values.tfName;
		} else {
			tfName = tfValue;
		}

		if (cbmodels) {
			model = cbmodels[0];
		}
		var newUrl = '';
		var baseUrl = '<a target="_blank" href="/resolve/service/social/showentity?showpost=false&type=' + type + '&sysId=';

		if (type.toLowerCase() == 'forum') {
			baseUrl = '<a target="_blank" href="/resolve/service/social/showentity?type=' + type + '&sysId=';
		}

		//console.log('This should add a link now');
		//prepare the url from the values
		if (values.type == 'url') {
			if (!tfValue || !tfName) {
				return;
			}
			var link = tfValue;
			if (tfValue.indexOf('http') == -1) {
				link = 'http://' + tfValue;
			}

			newUrl = '<a href="javascript:void(0)" onclick="window.open(\'' + link + '\',\'\',\'\')">';
			newUrl += tfName;
			newUrl += '</a>';
		} else if (values.type == 'worksheet') {
			if (!tfValue || !tfName) {
				return;
			}
			newUrl = baseUrl + '&name=' + tfValue + '">' + tfName + '</a>';
		} else if (values.type == 'document') {
			if (!tfName) {
				return;
			}
			var m_name = this.getLinkFormCombobox().getRawValue();
			var m_value = '';
			if (model) {
				m_name = model.get('name');
				m_value = model.get('value');
			}

			newUrl = baseUrl + m_value + '&name=' + m_name + '">' + tfName + '</a>';

		} else {
			if (!tfName) {
				return;
			}
			if (model) {
				var m_name = model.get('name');
				var m_value = model.get('value');

				/*newUrl = baseUrl + model.get('value') + '">' + model.get('name') + '</a>';*/
				newUrl = baseUrl + m_value + '&name=' + m_name + '">' + tfName + '</a>';
			} else {
				RS.UserMessage.msg({
					success: false,
					title: 'Alert',
					msg: 'This is not a valid selection. Please try again',
					duration: 10000,
					closeable: true
				});
			}
		}

		//update the post
		this.updatePostArea(newUrl);

		//close the window
		var windowOwner = this.getLinkForm().ownerCt;
		windowOwner.close();
	},
	addHtml: function() {
		var editor = this.getHtmlEditor();
		var htmlValue = editor.getValue();
		if (htmlValue) {
			//process html to make sure there isn't any links, and if there are make sure to have a target="_blank" for them
			//do a basic search and replace to deal with this
			htmlValue = htmlValue.replace(/<a href="/g, '<a target="_blank" href="');
			this.updatePostArea(htmlValue);
		}

		//close the window
		var windowOwner = editor.ownerCt;
		windowOwner.close();
	},
	updatePostArea: function(appendValue) {
		var textarea = this.getPostForm().down('textarea');
		var newValue = textarea.getValue();
		if (appendValue && appendValue.length > 0) {
			if (newValue) {
				newValue = newValue + ' ' + __htmlBegin__ + appendValue + __htmlEnd__;
			} else {
				newValue = __htmlBegin__ + appendValue + __htmlEnd__;
			}
		}
		textarea.setValue(newValue);
	},
	resetUploadForm: function() {
		this.getFileUploadForm().getForm().reset();
	},
	resetLinkForm: function() {
		this.getLinkForm().getForm().reset();
	},
	resetHtmlForm: function() {
		this.getHtmlEditor().reset();
	},
	onBeforeLoadOfLinkStore: function(store, operation, opts) {
		var selectedType = this.getLinkFormRadioGroup().getValue().type;
		var params = store.getProxy().extraParams;
		params.type = selectedType;

	},
	loadMentions: function(queryStr) {
		this.getMentionStoreStore().proxy.extraParams.query = queryStr;
		this.getMentionStoreStore().load();
	},
	showMoreForum: function(postId) {
		this.currentForumPostId = postId;
		this.isForum = true;
		this.getPostForm().setForum(true, this.currentlySelectedName);
		this.getStreamTabs().activeTab.down('streamtree').forumPermaLink = postId;
		this.refreshPostPanel();
	},
	returnForum: function() {
		this.isForum = false;
		this.getPostForm().setForum(false, this.currentlySelectedName);
		this.getStreamTabs().activeTab.down('streamtree').forumPermaLink = null;
		this.refreshPostPanel();
	}
});
