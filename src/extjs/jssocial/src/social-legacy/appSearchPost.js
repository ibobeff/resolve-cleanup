/**
 * Main entry point for Social application when displaying social results in a search
 */
Ext.application({
	name: 'RS',
	controllers: ['SocialController'],

	launch: function() {
		//create the viewport for this view
		Ext.create('RS.view.SearchPostViewport', {
			selectionDetails: {
				displayName: social.compFullName || social.atFullName,
				sys_id: social.compSysId || social.atSysId,
				comptype: social.compType,
				parent_comptype: '',
				parent_sysid: '',
				hadSelection: true,
				searchPostKey: social.searchKey,
				searchPostTime: social.searchTime,
				doSearch: true,
				//social.doSearch,
				queryType: social.queryType,
				resultsType: social.resultsType,
				permaLink: social.permaLink
			}
		});
		RS.app = this;
	}
});