Ext.Loader.setConfig({
	enabled: true
});

var __htmlBegin__ = '<!-- HTML_BEGIN -->';
var __htmlEnd__ = '<!-- HTML_END -->';

function refreshIntervalForInfiniteRecords() {
	return 2500;
}

function refreshIntervalForAutoRefreshPost() {
	var val = social.postAutoRefreshInterval;
	var result = 60 * 1000;
	//1 min , by default
	if(val > 1) {
		result = val * result;
	}
	return result;
}

function pageSizeForAllTabs() {
	return 20;
}

//methods to integrate raw html with the controller

function toggleStar(elId, postId) {
	this.RS.app.fireEvent('toggleStarEvent', elId, postId);
}

function likeLinkOnClick(elId, postId) {
	this.RS.app.fireEvent('likeLinkOnClickEvent', elId, postId);
}

function submitComment(elId, postId, _ui_id) {
	this.RS.app.fireEvent('submitCommentEvent', elId, postId, _ui_id);
}

function showComment(elId, postId) {
	if( showingForum ){
		this.RS.app.fireEvent('showMoreForum', postId);
		return;
	}
	this.RS.app.fireEvent('showCommentEvent', elId, postId);
}

function showPermaLink(elId, postId) {
	//open window to the new link using the permaLink param
	window.open(Ext.String.format('/resolve/social/rssocial.jsp?permaLink={0}', postId));
}

function showPopupOnMouseOver(el, compType, sysId) {
	//Ensure at least some hover before displaying the popup
	Ext.Function.defer(displayPopupOnMouseOver, 1000, this, [el, compType, sysId]);
}

function displayPopupOnMouseOver(el, compType, sysId) {
	//if the mouse is still over the element that fired the mouseover, then display the popup window
	var e = Ext.EventObject;
	if(e.within(el)) this.RS.app.fireEvent('showPopupOnMouseOverEvent', el, compType, sysId);
}

function perhapsHidePopupOnMouseOut(el) {
	if(Ext.isIE) {
		var eventCopy = {};
		for(var i in window.event) {
			eventCopy[i] = window.event[i];
		}
	}
	Ext.Function.defer(checkHidePopupOnMouseOut, 200, this, [el, Ext.isIE ? eventCopy : null]);
}

function checkHidePopupOnMouseOut(el, eventCopy) {
	var e = Ext.EventObject;
	if(!e.within(el)) this.RS.app.fireEvent('perhapsHidePopupOnMouseOutEvent', el, eventCopy);
}

function hidePopupOnMouseOut() {
	this.RS.app.fireEvent('hidePopupOnMouseOutEvent');
}

function displayLikeDialog(sys_id) {
	this.RS.app.fireEvent('displayLikeDialog', sys_id);
}

function followUser(sys_id, following) {
	this.RS.app.fireEvent('followUser', sys_id, following);
}

function deletePost(postId) {
	Ext.get(postId).mask();
	RS.UserMessage.msg({
		success: true,
		duration: 5000,
		title: 'Delete Post',
		msg: Ext.String.format('The post has been deleted, <a href="#" onclick="Ext.fly(this).up(\'div\').fadeOut({duration: 200,remove: true});Ext.get(\'{0}\').unmask();">click to undo</a>', postId),
		callback: reallyDeletePost,
		scope: this,
		postId: postId
	});
}

function reallyDeletePost(config) {
	config.scope.RS.app.fireEvent('deletePostEvent', config.postId);
}

function deleteComment(postId, commentId) {
	Ext.get(commentId).mask();
	RS.UserMessage.msg({
		success: true,
		duration: 5000,
		title: 'Delete Comment',
		msg: Ext.String.format('The comment has been deleted, <a href="#" onclick="Ext.fly(this).up(\'div\').fadeOut({duration: 200,remove: true});Ext.get(\'{0}\').unmask();">click to undo</a>', commentId),
		callback: reallyDeleteComment,
		scope: this,
		postId: postId,
		commentId: commentId
	});
}

function reallyDeleteComment(config) {
	config.scope.RS.app.fireEvent('deleteCommentEvent', config.postId, config.commentId);
}

function displayLargeImage(url, displayName) {
	this.RS.app.fireEvent('displayLargeImage', url, displayName);
}

function toggleCommentDisplay(element, forceShow, postId) {
	if( showingForum ){
		this.RS.app.fireEvent('showMoreForum', postId);
		return;
	}

	var el = Ext.get(element);
	if(el) {
		var sibling = el.next();
		//Backwards logic because its about to be toggled
		if(sibling.isVisible() && !forceShow) {
			// console.log('display show comments');
			el.update('<i>Show Comments</i>');
		} else {
			// console.log('display hide comments');
			el.update('<i>Hide Comments</i>');
		}
		sibling.setVisibilityMode(Ext.dom.AbstractElement.OFFSETS);
		if(forceShow) sibling.setVisible(true);
		else sibling.toggle(true);
	}
}

function hideCommentDisplay(element) {
	var el = Ext.get(element);
	if(el) {
		var sibling = el.next();
		//Backwards logic because its about to be toggled
		el.update('<i>Show More</i>');
		el.setVisible(true);
		sibling.setVisibilityMode(Ext.dom.AbstractElement.OFFSETS);
		sibling.setVisible(false);
		showingForum = true;

		//hide the addcomment link to not confuse users
		// var commentLinks = el.parent().query('.cxaddcommentlink');
		// Ext.each( commentLinks, function(commentLink){
		// 	Ext.fly(commentLink).hide();
		// })
	}
}

function returnForum(){
	this.RS.app.fireEvent('returnForum');
}