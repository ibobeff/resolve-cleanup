/**
 * Main entry point for the Social application when you are looking at one particular document/component in the system
 */
Ext.application({
	name : 'RS',
	controllers : ['SocialController'],

	launch : function() {
		//create the viewport for this view

		Ext.create('RS.view.DocViewport', {
			selectionDetails : {
				displayName : social.compFullName || social.atFullName,
				sys_id : social.compSysId || social.atSysId,
				comptype : social.compType,
				parent_comptype : '',
				parent_sysid : '',
				hadSelection : true,
				permaLink: social.permaLink
			}
		});

		RS.app = this;
	}
});
