/**
 * Main entry point for the social application if there is an error accessing the application 
 */
Ext.application({
	launch : function() {
		Ext.create('Ext.container.Viewport', {
			layout : 'fit',
			items : [{
				xtype : 'panel',
				html : '<font color=RED>User does not have Access Rights to View this app.</font>'
			}]
		});
	}
});
