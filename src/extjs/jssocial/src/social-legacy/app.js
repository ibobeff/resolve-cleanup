/**
 * Main entry point for the Social application
 */
Ext.application({
	name: 'RS',

	models: ['Post', 'UserImage', 'Stream'],
	stores: ['UserImages'],
	controllers: ['SocialController'],
	requires: ['RS.view.Viewport'],

	launch: function() {
		//setup tooltips
		Ext.tip.QuickTipManager.init();

		//setup state management
		if(Ext.supports.LocalStorage) Ext.state.Manager.setProvider(new Ext.state.LocalStorageProvider());
		else Ext.state.Manager.setProvider(new Ext.state.CookieProvider());

		//Create interceptor for all requests to handle responses from the server generically with proper error messages
		// Ext.data.Connection.prototype.request = Ext.Function.createInterceptor(Ext.data.Connection.prototype.request, function(options) {
		// options.callback = Ext.Function.createInterceptor(options.callback, function(options, success, response) {
		// if (success) {
		// var r = Ext.decode(response.responseText);
		// if (!r.success) {
		// Ext.Msg.alert('Error', r.message);
		// return false;
		// }
		// }
		// });
		// });
		RS.app = this;

		//Now create the viewport because things are properly setup
		Ext.create('RS.view.Viewport', {});

		//restore state of auto-refresh
		if(Ext.state.Manager.get('socialAutoRefresh')) {
			this.controllers.getAt(0).toggleAutoRefresh();
		}
	}
});