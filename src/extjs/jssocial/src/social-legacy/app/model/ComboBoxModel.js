/**
 * Model to be used for comboboxes.  It defines a 'name' and 'value' field 
 */
Ext.define('RS.model.ComboBoxModel', {
	extend : 'Ext.data.Model',

	fields : ['name', 'value']
});
