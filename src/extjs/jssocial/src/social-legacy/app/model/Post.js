/**
 * Model for a Post.  Has fields that are used to display the posts to the user. Has many comments. 
 */
Ext.define('RS.model.Post', {
	extend : 'Ext.data.Model',
	requires : ['RS.model.Comment', 'Ext.data.HasManyAssociation', 'Ext.data.BelongsToAssociation'],
	fields : [{
		name : 'sysId',
		type : 'string'
	}, {
		name : 'name',
		type : 'string'
	}, {
		name : 'postType',
		type : 'string'
	}, {
		name : 'targetEntity',
		type : 'string'
	}, {
		name : 'postAuthor',
		type : 'string'
	}, {
		name : 'postDate',
		type : 'string'
	}, {
		name : 'postContent',
		type : 'string'
	}, {
		name : 'likeCount',
		type : 'string'
	}, {
		name : 'isStarred',
		type : 'boolean',
		defaultValue : false
	}, {
		name : 'hasDeleteRights',
		type : 'boolean',
		defaultValue : false
	}, {
		name : '_ui_id',
		type : 'string'
	}],

	hasMany : {
		model : 'RS.model.Comment',
		name : 'comments',
		associationKey : 'comments'
	}

});
