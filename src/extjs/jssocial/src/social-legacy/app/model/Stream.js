/**
 * Model for a stream that the user subscribes to in order to view the posts inside that stream.  For example, Runbook or Inbox 
 */
Ext.define('RS.model.Stream', {
	extend : 'Ext.data.Model',
	//idProperty : 'sys_id',
	fields : [{
		name : 'displayName',
		type : 'string'
	}, {
		name : 'user',
		type : 'string'
	}, {
		name : 'sys_id',
		type : 'string'
	}, {
		name : 'comptype',
		type : 'string'
	}, {
		name : 'grouptitle',
		type : 'string'
	}, {
		name : 'parentCompType',
		type : 'string'
	}, {
		name : 'parentSysId',
		type : 'string'
	}, {
		name : 'systemStream',
		type : 'boolean'
	}, 'records']

});
