/**
 * Model used to display the user's image in the upper left corner of the screen 
 */
Ext.define('RS.model.UserImage', {
	extend : 'Ext.data.Model',
	fields : [{
		name : 'sysId',
		type : 'string'
	}, {
		name : 'title',
		type : 'string'
	}, {
		name : 'userName',
		type : 'string'
	}, {
		name : 'userFullName',
		type : 'string'
	}, {
		name : 'userHref',
		type : 'string'
	}, {
		name : 'imgSrc',
		type : 'string'
	}],
	proxy : {
		type : 'ajax',
		url : '/resolve/service/userinfo',
		reader : {
			root : 'data'
		}
	}
});
