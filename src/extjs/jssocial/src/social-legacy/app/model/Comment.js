/**
 * Model for a comment.  Defines fields used to properly display a comment.
 * Belongs to a Post 
 */
Ext.define('RS.model.Comment', {
	extend : 'Ext.data.Model',

	fields : ['sys_id', '_ui_id', 'postId', 'author', 'date', 'comment'],
	belongsTo : 'RS.model.Post'
}); 