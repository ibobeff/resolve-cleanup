/**
 * Defines the model used to describe people who like a particular post.  Is displayed when the user clicks on the number of likes on a post
 */
Ext.define('RS.model.PeopleWhoLikeThis', {
	extend : 'Ext.data.Model',
	idProperty : 'sysid',
	fields : [{
		name : 'sysid',
		type : 'string'
	}, {
		name : 'photo'
	}, {
		name : 'name'
	}, {
		name : 'userEmail'
	}, {
		name : 'gotoLink'
	}, {
		name : 'description'
	}, {
		name : 'currentUser',
		type : 'boolean'
	}, {
		name : 'following',
		type : 'boolean'
	}]
});
