Ext.define('RS.view.panel.MenuSet', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.menuset',

	frame: false,
	closable: true,
	border: false,
	bodyBorder: false,

	initComponent: function() {
		this.loadMenuData();
		this.callParent(arguments);
	},
	loadMenuData: function() {
		Ext.Ajax.request({
			url: '/resolve/service/social/getMenuSet',
			params: {
				menuSetId: this.menuSetId
			},
			scope: this,
			success: this.loadedMenuData
		});
	},
	loadedMenuData: function(r) {
		var response = RS.common.parsePayload(r);
		if(response.success) {
			this.setTitle(response.data.name);
			var links = [],
				lastGroup = '',
				group = '<div style="padding: 3px 0px 3px 20px">{0}</div>',
				link = '<div style="padding: 3px 0px 3px 30px"><a onclick="{0}" href="javascript:void(0)" class="usefullinks">{1}</a></div>';

			Ext.each(response.data.menuItemModels, function(item) {
				if( item.group != lastGroup){
					lastGroup = item.group;
					links.push(Ext.String.format(group, item.group));
				}
				links.push(Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=' + encodeURIComponent(item.query) + '\', \'_top\')', item.name));
			});
			this.update(links.join(''));
		}
	}
});