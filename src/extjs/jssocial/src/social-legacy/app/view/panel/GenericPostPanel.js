/**
 * Panel to display the posts from a particular stream
 */
Ext.define('RS.view.panel.GenericPostPanel', {
	extend: 'Ext.Panel',
	alias: 'widget.genericpostpanel',
	border: false,
	loadMask: true,
	disableSelection: true,

	//use this for infinite scrolling calculations
	contentHeight: -1,
	calculateHeight: false,
	autoScroll: true,

	initComponent: function() {
		this.store = Ext.create('RS.store.SocialStore', {
			listeners: {
				beforeprefetch: this.beforePrefetch,
				prefetch: this.onPrefetch,
				scope: this
			}
		});

		this.tpl = postTemplate;

		this.callParent(arguments);

		this.on('render', function() {
			if (this.selectionDetails) {
				this.store.load();
			}
		}, this, {
			single: true
		});
	},
	beforePrefetch: function(store, operation, eOpts) {
		//This is going to be ugly, but we want to keep this post panel self-contained
		//We need to get the current selected item from the tree on the left, and populate the panel with the results from the store.
		//This would be good for the controller to do, but the controller doesn't know which store to link to which tab because they are all the "same"
		var tabpanel = this.up('panel');
		if (tabpanel) {
			var navigation = tabpanel.down('streamtree');
			if (navigation) {
				var selectionDetails = navigation.getSelectedDetails();
				if (selectionDetails.hadSelection) {
					var params = store.getProxy().extraParams;

					if (params.infiniteScroll) {
						params.start = (params.start + params.limit);
					} else {
						params.start = 0;
					}

					Ext.apply(params, selectionDetails);
				} else {
					//If there are no selections, then its because there are no records (we auto selected the first record already) which means just display blank information
					this.overwritePost([]);
					return false;
				}
			}
		} else if (this.selectionDetails) {
			var selectionDetails = this.selectionDetails;
			if (selectionDetails.hadSelection) {
				var params = store.getProxy().extraParams;

				if (params.infiniteScroll) {
					params.start = (params.start + params.limit);
				} else {
					params.start = 0;
				}

				Ext.apply(params, selectionDetails);
			} else {
				//If there are no selections, then its because there are no records (we auto selected the first record already) which means just display blank information
				this.overwritePost([]);
				return false;
			}
		}
	},
	onPrefetch: function(store, records, successful, operation, opts) {
		var params = store.getProxy().extraParams;

		//reset the flags
		params.infiniteScroll = false;
		if (records && records.length == 0) {
			//flag to indicate that there are no more recs to show
			params.endOfRecs = true;
		}

		var startRec = params.start;
		//if its the first page, then overwrite it
		var json = [];

		if (records) {
			Ext.each(records, function(record) {
				json.push(record.raw);
			})
		}

		if (startRec == 0) {
			this.overwritePost(json);
		} else {
			this.updatePosts(json);
		}

		//If forum, then initially show everything collapsed
		showingForum = false;
		if (operation.params.comptype === 'Forums' && !operation.params.permaLink) {
			var showHideCommentLinks = this.body.query('.showHideComments');
			Ext.each(showHideCommentLinks, function(showHideCommentLink) {
				hideCommentDisplay(showHideCommentLink);
			});
		}
	},

	/**
	 * Updates the current posts with the provided records without overwriting the existing posts (for infinite scrolling)
	 */
	updatePosts: function(records) {
		var html = this.tpl.apply(records);
		var elbody = Ext.get(this.body);

		if (elbody) {
			elbody.insertHtml('beforeEnd', html);
		} else {
			this.body.insertHtml('beforeEnd', html);
		}

		var elId = elbody.id;
		var panelId = elId.substring(0, elId.indexOf('-body'));

		this.calculateHeight = true;
	},

	/**
	 * Overwrites any existing posts with the provided records (used when switching streams or refreshing the latest from the server)
	 */
	overwritePost: function(records) {
		var html = this.tpl.apply(records);
		var elbody = Ext.get(this.body);
		if (elbody) {
			elbody.update(html);
		}

		this.calculateHeight = true;
	}
});

var commentTpl = new Ext.XTemplate('<div id="{sysId}" class="feeditemcomment cxfeedcomment " style="display:block">',
	'<span class="feeditemcommentbody">',
	'<span style="position:relative;float:right;color: #909090;">',
	'<span>{[this.formatDate(values)]}</span>',
	'<span {[this.showHideDeleteLink(values)]} class="cxfeeditemlikedot feeditemseparatingdot"> | </span>',
	'<a id={[this.getDeleteLinkId(values)]} href="JavaScript:void(0);" class="feeditemactionlink cxaddcommentaction" title="Delete this comment" {[this.showHideDeleteLink(values)]}><img src="../images/social/delete.png" /></a>',
	'</span>',
	'<span class="feedcommentuser">{author}</span>',
	'<br/>',
	'<span class="feedcommenttext">{[this.decodeComment(values)]}</span>',
	'</span>',
	'</div>', {
	compiled: true,


	decodeComment: function(values) {
		var decContent = values.comment;
		/*
		 if(decContent)
		 {
		 decContent = Ext.htmlDecode(decContent);
		 }
		 */
		return decContent;
	},

	getDeleteLinkId: function(values) {
		var id = Ext.id();
		var deleteLink = '"' + id + '" onclick="JavaScript:deleteComment(\'' + values.postId + '\',\'' + values.sysId + '\')"';
		return deleteLink;
	},

	showHideDeleteLink: function(values) {
		var cssStyle = '';
		if (values.hasDeleteRights) {

		} else {
			cssStyle = 'style="display:none"';
		}
		return cssStyle;
	},
	formatDate: function(values) {
		var now = new Date(),
			postDate = new Date(Number(values.date)),
			dateString = Ext.Date.format(postDate, 'g:i A'),
			duration = '',
			durationText = '';
		var diff = (now.getTime() - postDate.getTime());
		var seconds = Math.round(diff / 1000);
		if (seconds < 60) {
			duration = seconds;
			durationText = duration === 1 ? 'second' : 'seconds';
		} else if (seconds < (60 * 60)) {
			duration = Math.round(seconds / 60);
			durationText = duration === 1 ? 'minute' : 'minutes';
		} else if (seconds < (60 * 60 * 24)) {
			duration = Math.round(seconds / 60 / 60);
			durationText = duration === 1 ? 'hour' : 'hours';
		} else {
			dateString = Ext.Date.format(postDate, 'M j, Y')
			duration = Math.round(seconds / 60 / 60 / 24);
			durationText = duration === 1 ? 'day' : 'days';
		}

		dateString += Ext.String.format(' ({0} {1} ago)', duration, durationText);
		return dateString;
	}
});

var postTemplate = new Ext.XTemplate('<tpl for=".">',
	'<div id="{sysId}" class="feedcontainer actionsOnHoverEnabled cxfeedcontainer">',
	'<div class="cxfeeditem feeditem">',
	'<div class="feeditemcontent cxfeeditemcontent">',
	'<div class="feeditembody">',
	'<span style="padding-right:5px;">',
	'<a href="JavaScript:void(0);" {[this.addOnClickToStar(values)]} >',
	'<span style="display:none">{isStarred}</span>',
	'<img src="../images/social/{[this.showStarImg(values)]}" width="15" height="15"></img>',
	'</a>',
	'</span>',
	'<span class="feeditemtargetentity">{targetEntity} &#8212; </span>',
	'<span  class="feeditemsecondentity">',
	'<span class="feeditemauthorentity">{postAuthor}</span>',
	'<span style="float:right;color: #909090;">',
	'<span class="feeditemfooter">',
	'<a id={[this.getLinkId(values)]} href="JavaScript:void(0);" class="feeditemactionlink cxaddcommentaction cxaddcommentlink" title="Comment on this post">Add Comment</a>',
	'<span  class="cxfeeditemlikedot feeditemseparatingdot cxaddcommentlink"> | </span>',
	'<a id={[this.getPermLinkId(values)]} href="JavaScript:void(0);" class="feeditemactionlink cxaddcommentaction" title="A direct link to this post">Link</a>',
	'<span  class="cxfeeditemlikedot feeditemseparatingdot"> | </span>',
	'<span class="__likeLink__"><a id={[this.getLikeLinkId(values)]} href="JavaScript:void(0);" class="feeditemactionlink cxaddcommentaction" title="Like on this post">Like</a></span>  ',
	'<span  class="cxfeeditemlikedot feeditemseparatingdot"> | </span>',
	'<span class="likeCountContainer">',
	'<tpl if="likeCount &gt; 0"><a href="JavaScript:void(0);" onclick="displayLikeDialog(\'{sysId}\')"></tpl>',
	'<span class="__noOfPeopleLiked__">{likeCount}</span>&nbsp;&nbsp;',
	'<img src="../images/social/like.png" />',
	'<tpl if="likeCount &gt; 0"></a></tpl>',
	'</span> ',
/*'<span  class="cxfeeditemlikedot feeditemseparatingdot"> | </span>',
'<a href="JavaScript:void(0);" onclick="displayPostInNewWindow(\'{sysId}\')"><img src="../images/social/like.png" /></a>',
*/
'<span  {[this.showHideDeleteLink(values)]} class="cxfeeditemlikedot feeditemseparatingdot"> | </span>',
	'<a id={[this.getDeleteLinkId(values)]} href="JavaScript:void(0);" class="feeditemactionlink cxaddcommentaction" title="Delete this post" {[this.showHideDeleteLink(values)]} ><img src="../images/social/delete.png" /></a>',
	'</span>',
	'{[this.formatDate(values)]}',
	'</span>',
	'</span>',
	'<br/>',
	'<div class="feeditemtext">{[this.decodeContent(values)]}</div>',
	'<br style="clear:both;" />',
	'</div>',
	'<a class="showHideComments" href="JavaScript:void(0);" onclick="JavaScript:toggleCommentDisplay(this,false,\'{sysId}\')" style="visibility:<tpl if="comments.length &gt; 0">visible</tpl><tpl if="comments.length == 0">hidden</tpl>;"><i>Hide Comments</i></a>',
	'<div class="feeditemextras">',
	'<div class="cxcomments feeditemcomments">',
	'<div class="commentsCls">',
	'<tpl for="comments">',
	'{[this.renderComments(values)]}',
	'</tpl>',
	'</div>',
	'</div>',
	'<div class="cxnewcomment" style="display:none">',
	'<div class="feeditemcomment feeditemcommenttext">',
	'<div class="feeditemcommentnew">',
	'<form>',
	'<textarea name="tacomment" style="height: 100px; max-height: 100px;" class="rspost cxnewcommenttext  chatterMentionsEnabled chatterHashtagsEnabled" tabindex="0" role="textbox" title="Write a comment..."></textarea>',
	'<div class="cxvalidationmessage newcommenterrorcontainer" style="display:none"></div>',
	'<input id={[this.getCommentBtnId(values)]} class="" type="button" value="Comment" title="Comment"/>',
	'</form>',
	'<div class="feedclearfloat"></div>',
	'</div>',
	'</div>',
	'</div>',
	'</div>',
	'</div>',
	'</div>',
	'</div>',
	'</tpl>', {
	compiled: true,


	formatDate: function(values) {
		var now = new Date(),

			postDate = new Date(Number(values.postDate)),

			dateString = Ext.Date.format(postDate, 'g:i A'),
			duration = '',
			durationText = '';
		var diff = (now.getTime() - postDate.getTime());
		var seconds = Math.round(diff / 1000);
		if (seconds < 60) {
			duration = seconds;
			durationText = duration === 1 ? 'second' : 'seconds';
		} else if (seconds < (60 * 60)) {
			duration = Math.round(seconds / 60);
			durationText = duration === 1 ? 'minute' : 'minutes';
		} else if (seconds < (60 * 60 * 24)) {
			duration = Math.round(seconds / 60 / 60);
			durationText = duration === 1 ? 'hour' : 'hours';
		} else {
			dateString = Ext.Date.format(postDate, 'M j, Y')
			duration = Math.round(seconds / 60 / 60 / 24);
			durationText = duration === 1 ? 'day' : 'days';
		}

		dateString += Ext.String.format(' ({0} {1} ago)', duration, durationText);
		return dateString;
	},

	renderComments: function(values) {
		var comm = commentTpl.apply(values);
		return comm;
	},
	decodeContent: function(values) {
		var decContent = values.postContent;
		/*if(decContent)
		 {
		 decContent = Ext.htmlDecode(decContent);
		 }*/
		return decContent;
	},
	addOnClickToStar: function(values) {
		var id = Ext.id();
		var func = 'onclick="JavaScript:toggleStar(\'' + id + '\', \'' + values.sysId + '\')" id=' + id;
		return func;
	},
	showStarImg: function(values) {
		var imgname = 'starOff.png';
		if (values.isStarred) {
			imgname = 'starOn.png';
		}
		return imgname;
	},
	getLinkId: function(values) {
		var id = Ext.id();
		var linkId = '"' + id + '" onclick="JavaScript:showComment(\'' + id + '\', \'' + values.sysId + '\')"';
		return linkId;
	},
	getPermLinkId: function(values) {
		var id = Ext.id();
		var linkId = '"' + id + '" onclick="JavaScript:showPermaLink(\'' + id + '\', \'' + values.sysId + '\')"';
		return linkId;
	},
	getLikeLinkId: function(values) {
		var id = Ext.id();
		var likeLink = '"' + id + '" onclick="JavaScript:likeLinkOnClick(\'' + values.sysId + '\')"';
		return likeLink;
	},
	showHideDeleteLink: function(values) {
		var cssStyle = '';
		if (values.hasDeleteRights) {
			//do nothing
		} else {
			cssStyle = 'style="display:none"';
		}
		return cssStyle;
	},
	getDeleteLinkId: function(values) {
		var id = Ext.id();
		var deleteLink = '"' + id + '" onclick="JavaScript:deletePost(\'' + values.sysId + '\')"';
		return deleteLink;
	},
	getCommentBtnId: function(values) {
		var id = Ext.id();
		var buttonEl = '"' + id + '" onclick="JavaScript:submitComment(\'' + id + '\', \'' + values.sysId + '\')"';
		return buttonEl;
	}
});