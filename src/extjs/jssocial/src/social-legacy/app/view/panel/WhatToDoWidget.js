/**
 * Panel to display links that will allow the user to quickly create components and follow other components
 */
Ext.define('RS.view.panel.WhatToDoWidget', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.whattodowidget',

	title: 'Useful Links',
	frame: false,
	closable: true,
	border: false,
	bodyBorder: false,

	initComponent: function() {
		var link = '<div style="padding: 3px 0px 3px 20px"><a onclick="{0}" href="javascript:void(0)" class="usefullinks">{1}</a></div>',

			createProcess = Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=' + encodeURIComponent('rsprofile.jsp?main=Process&action=create') + '\', \'_top\')', 'Create/Join Process'),
			createTeam = Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=' + encodeURIComponent('rsprofile.jsp?main=Teams&action=create') + '\', \'_top\')', 'Create/Join Team'),
			createForum = Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=' + encodeURIComponent('rsprofile.jsp?main=Forums&action=create') + '\', \'_top\')', 'Create/Join Forum'),
			createRss = Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=' + encodeURIComponent('rsprofile.jsp?main=RSS&action=create') + '\', \'_top\')', 'Create/Join RSS'),

			followUsers = Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=' + encodeURIComponent('rsprofile.jsp?main=User') + '\', \'_top\')', 'Follow Users'),
			followDocument = Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=' + encodeURIComponent('rsprofile.jsp?main=Document') + '\', \'_top\')', 'Follow Document'),
			followRunbook = Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=' + encodeURIComponent('rsprofile.jsp?main=Runbook') + '\', \'_top\')', 'Follow Runbook'),
			followActionTask = Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=' + encodeURIComponent('rsprofile.jsp?main=ActionTask') + '\', \'_top\')', 'Follow ActionTask'),

			createNewContentRequest = Ext.String.format(link, 'window.open(\'/resolve/jsp/rsclient.jsp?url=/resolve/service/wiki/view/CR/New Content Request\', \'_blank\')', 'Create New Content Request');

		this.html = [createProcess, createTeam, createForum, createRss, followUsers, followDocument, followRunbook, followActionTask, createNewContentRequest].join('');
		this.callParent(arguments);
	}
});