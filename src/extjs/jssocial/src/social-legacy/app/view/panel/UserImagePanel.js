/**
 * Panel to display the user image
 */
Ext.define('RS.view.panel.UserImagePanel', {
	extend : 'Ext.view.View',
	alias : 'widget.userimagepanel',
	id : 'userimagepanelId',
	border : false,
	store : 'UserImages',
	//@formatter:off
	itemTpl : [	'<div id="userImage" class="thumb">', 
					'<div><a href="{userHref}"><img src="{imgSrc}" border="0" alt="{title}" style="float: CONFIG" title="Edit Settings" /></a></div>', 
					'<div><h2>{userFullName}</h2></div>', 
				'</div>']
	//@formatter:on
});

