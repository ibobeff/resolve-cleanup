/**
 * Window to display when the user clicks on the 'File' button when posting to a stream
 */
Ext.define('RS.view.window.FileUploadWindow', {
	extend: 'Ext.window.Window',
	alias: 'widget.fileuploadwindow',

	width: 400,
	title: 'File Upload',
	modal: true,
	layout: 'fit',

	items: [{
		xtype: 'form',
		border: false,
		url: '/resolve/service/post/attach',
		bodyPadding: '10',
		defaults: {
			anchor: '100%',
			msgTarget: 'side'
		},

		items: [{
			allowBlank: false,
			xtype: 'filefield',
			id: 'form-file',
			emptyText: 'Select a File',
			fieldLabel: 'File',
			name: 'file-path',
			buttonText: 'Select'
		}, {
			xtype: 'textfield',
			name: 'newfilename',
			fieldLabel: 'Description'
		}]
	}],
	buttonAlign: 'left',
	buttons: [{
		text: 'Upload',
		action: 'fileupload'
	}, {
		text: 'Reset',
		action: 'resetform'
	}, {
		text: 'Cancel',
		handler: function(button) {
			button.up('window').close();
		}
	}]

});