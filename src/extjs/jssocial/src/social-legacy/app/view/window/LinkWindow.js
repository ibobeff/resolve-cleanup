/**
 * Window to display when the user clicks the 'Link' button when posting to a stream
 */
Ext.define('RS.view.window.LinkWindow', {
	extend: 'Ext.window.Window',
	alias: 'widget.linkwindow',

	modal: true,

	layout: 'fit',
	width: 650,
	title: 'Add Link',

	items: [{
		xtype: 'form',
		bodyPadding: '10',
		border: false,
		defaults: {
			anchor: '100%',
			allowBlank: false,
			msgTarget: 'side'
		},
		items: [{
			xtype: 'radiogroup',
			fieldLabel: 'Select Type',
			//cls : 'x-check-group-alt',
			items: [{
				boxLabel: 'URL',
				name: 'type',
				inputValue: 'url',
				checked: true
			}, {
				boxLabel: 'Document',
				name: 'type',
				inputValue: 'document'
			}, {
				boxLabel: 'ActionTask',
				name: 'type',
				inputValue: 'actiontask'
			}, {
				boxLabel: 'Worksheet',
				name: 'type',
				inputValue: 'worksheet'
			}, {
				boxLabel: 'Forum',
				name: 'type',
				inputValue: 'forum'
			}, {
				boxLabel: 'RSS',
				name: 'type',
				inputValue: 'rss'
			}]
		}, {
			xtype: 'textfield',
			name: 'tfValue',
			fieldLabel: 'Value'
		}, {
			xtype: 'combo',
			name: 'cbValue',
			store: 'LinkStore',
			displayField: 'name',
			typeAhead: false,
			fieldLabel: 'Value',
			hideTrigger: true,
			anchor: '100%',
			minChars: 1,
			hidden: true,
			listConfig: {
				loadingText: 'Searching...',
				emptyText: 'No matching posts found.'
			}
		}, {
			xtype: 'textfield',
			name: 'tfName',
			fieldLabel: 'Description'
		}]
	}],
	buttonAlign: 'left',
	buttons: [{
		text: 'Add Link',
		action: 'addlink'
	}, {
		text: 'Reset',
		action: 'resetlink'
	}, {
		text: 'Cancel',
		handler: function(button) {
			button.up('window').close();
		}
	}]

});