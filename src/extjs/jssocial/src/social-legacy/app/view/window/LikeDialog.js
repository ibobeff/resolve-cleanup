/**
 * Window to display that shows which users also liked the selected post.
 */
Ext.define('RS.view.window.LikeDialog', {
	extend : 'Ext.window.Window',
	alias : 'widget.likedialog',

	requires : ['RS.model.PeopleWhoLikeThis'],

	width : 500,
	height : 300,
	layout : 'fit',
	modal : true,
	title : 'People who like this',

	onEsc : function() {
		this.close();
	},

	initComponent : function() {

		this.items = [{
			xtype : 'gridpanel',
			border : false,
			bodyBorder : false,
			hideHeaders : true,
			buttonAlign : 'center',
			store : Ext.create('Ext.data.Store', {
				autoLoad : true,
				model : 'RS.model.PeopleWhoLikeThis',
				proxy : {
					type : 'ajax',
					url : '/resolve/service/social/getLikeData',
					reader : {
						root : 'records'
					},
					extraParams : {
						sysid : this.sys_id
					}
				}
			}),
			columns : [{
				text : 'Image',
				xtype : 'templatecolumn',
				tpl : '<div style="text-align:center"><img style="height:45px;width:45px" src="{photo}"/></div>'
			}, {
				text : 'Name',
				xtype : 'templatecolumn',
				tpl : '<span style="cursor:hand;cursor:pointer;font-size:14px" class="popupName" onclick="window.open(\'{gotoLink}\')">{name}</span><br/><span style="color: #888888;font-size:12px">{description}</span>',
				flex : 1
			}, {
				xtype : 'templatecolumn',
				//@formatting:off
				tpl : ['<tpl if="currentUser == true">', '<span style="text-align:center;font-size:14px">(You)</span>', '</tpl>', '<tpl if="currentUser == false &amp;&amp; following == true">', '<span style="font-size:14px;cursor:hand;cursor:pointer;" onclick="JavaScript:followUser(\'{sysid}\', false)"><image style="width:16px;height:16px;" src="/resolve/images/ok_st_obj.gif"/>Following</span>', '</tpl>', '<tpl if="currentUser == false &amp;&amp; following == false">', '<span style="font-size:14px;cursor:hand;cursor:pointer;" onclick="JavaScript:followUser(\'{sysid}\', true)"><image style="width:16px;height:16px;" src="/resolve/images/plus.gif"/>Follow</span>', '</tpl>'].join('')
				//@formatting:on
			}]
			// ,
			// buttons : [{
			// text : 'Done',
			// scope : this,
			// handler : function(button) {
			// this.close();
			// }
			// }]
		}];

		this.callParent(arguments);
	}
});
