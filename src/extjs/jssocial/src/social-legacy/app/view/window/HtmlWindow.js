/**
 * Window to display when the user clicks on the 'Html' button when posting to a stream
 */
Ext.define('RS.view.window.HtmlWindow', {
	extend: 'Ext.window.Window',
	alias: 'widget.htmlwindow',

	modal: true,
	bodyBorder: false,
	border: false,

	autoShow: true,
	width: 650,
	height: 400,
	title: 'Html',
	layout: 'fit',
	items: [{
		xtype: 'htmleditor',
		enableLinks: false,
		cls: 'show-htmleditor-font',

		createToolbar: function(editor) {
			var me = this,
				items = [],
				i, tipsEnabled = Ext.tip.QuickTipManager && Ext.tip.QuickTipManager.isEnabled(),
				baseCSSPrefix = Ext.baseCSSPrefix,
				fontSelectItem, toolbar, undef;

			function btn(id, toggle, handler) {
				return {
					itemId: id,
					cls: baseCSSPrefix + 'btn-icon',
					iconCls: baseCSSPrefix + 'edit-' + id,
					enableToggle: toggle !== false,
					scope: editor,
					handler: handler || editor.relayBtnCmd,
					clickEvent: 'mousedown',
					tooltip: tipsEnabled ? editor.buttonTips[id] || undef : undef,
					overflowText: editor.buttonTips[id].title || undef,
					tabIndex: -1
				};
			}

			if(!Ext.isSafari2) {
				if(me.enableSourceEdit) {
					var source = btn('sourceedit', true, function(btn) {
						me.toggleSourceEdit(!me.sourceEditMode);
					});
					source.text = 'Source';
					delete source.cls;
					delete source.iconCls;
					items.push(source, '-');
				}
			}

			if(me.enableFont && !Ext.isSafari2) {
				fontSelectItem = Ext.widget('component', {
					renderTpl: ['<select id="{id}-selectEl" class="{cls}">', '<tpl for="fonts">', '<option value="{[values.toLowerCase()]}" style="font-family:{.}"<tpl if="values.toLowerCase()==parent.defaultFont"> selected</tpl>>{.}</option>', '</tpl>', '</select>'],
					renderData: {
						cls: baseCSSPrefix + 'font-select',
						fonts: me.fontFamilies,
						defaultFont: me.defaultFont
					},
					childEls: ['selectEl'],
					afterRender: function() {
						me.fontSelect = this.selectEl;
						Ext.Component.prototype.afterRender.apply(this, arguments);
					},
					onDisable: function() {
						var selectEl = this.selectEl;
						if(selectEl) {
							selectEl.dom.disabled = true;
						}
						Ext.Component.prototype.onDisable.apply(this, arguments);
					},
					onEnable: function() {
						var selectEl = this.selectEl;
						if(selectEl) {
							selectEl.dom.disabled = false;
						}
						Ext.Component.prototype.onEnable.apply(this, arguments);
					},
					listeners: {
						change: function() {
							me.relayCmd('fontname', me.fontSelect.dom.value);
							me.deferFocus();
						},
						element: 'selectEl'
					}
				});

				items.push(fontSelectItem, '-');
			}

			if(me.enableFontSize) {
				items.push(btn('increasefontsize', false, me.adjustFont), btn('decreasefontsize', false, me.adjustFont));
			}

			if(me.enableFormat) {
				items.push('-', btn('bold'), btn('italic'), btn('underline'));
			}

			if(me.enableAlignments) {
				items.push('-', btn('justifyleft'), btn('justifycenter'), btn('justifyright'));
			}

			if(me.enableColors) {
				items.push('-', {
					itemId: 'forecolor',
					cls: baseCSSPrefix + 'btn-icon',
					iconCls: baseCSSPrefix + 'edit-forecolor',
					overflowText: editor.buttonTips.forecolor.title,
					tooltip: tipsEnabled ? editor.buttonTips.forecolor || undef : undef,
					tabIndex: -1,
					menu: Ext.widget('menu', {
						plain: true,
						items: [{
							xtype: 'colorpicker',
							allowReselect: true,
							focus: Ext.emptyFn,
							value: '000000',
							plain: true,
							clickEvent: 'mousedown',
							handler: function(cp, color) {
								me.execCmd('forecolor', Ext.isWebKit || Ext.isIE ? '#' + color : color);
								me.deferFocus();
								this.up('menu').hide();
							}
						}]
					})
				}, {
					itemId: 'backcolor',
					cls: baseCSSPrefix + 'btn-icon',
					iconCls: baseCSSPrefix + 'edit-backcolor',
					overflowText: editor.buttonTips.backcolor.title,
					tooltip: tipsEnabled ? editor.buttonTips.backcolor || undef : undef,
					tabIndex: -1,
					menu: Ext.widget('menu', {
						plain: true,
						items: [{
							xtype: 'colorpicker',
							focus: Ext.emptyFn,
							value: 'FFFFFF',
							plain: true,
							allowReselect: true,
							clickEvent: 'mousedown',
							handler: function(cp, color) {
								if(Ext.isGecko) {
									me.execCmd('useCSS', false);
									me.execCmd('hilitecolor', color);
									me.execCmd('useCSS', true);
									me.deferFocus();
								} else {
									me.execCmd(Ext.isOpera ? 'hilitecolor' : 'backcolor', Ext.isWebKit || Ext.isIE ? '#' + color : color);
									me.deferFocus();
								}
								this.up('menu').hide();
							}
						}]
					})
				});
			}

			if(!Ext.isSafari2) {
				if(me.enableLinks) {
					items.push('-', btn('createlink', false, me.createLink));
				}

				if(me.enableLists) {
					items.push('-', btn('insertorderedlist'), btn('insertunorderedlist'));
				}
			}

			// Everything starts disabled.
			for(i = 0; i < items.length; i++) {
				if(items[i].itemId !== 'sourceedit') {
					items[i].disabled = true;
				}
			}

			// build the toolbar
			// Automatically rendered in AbstractComponent.afterRender's renderChildren call
			toolbar = Ext.widget('toolbar', {
				id: me.id + '-toolbar',
				ownerCt: me,
				cls: Ext.baseCSSPrefix + 'html-editor-tb',
				enableOverflow: true,
				items: items,
				ownerLayout: me.getComponentLayout(),

				// stop form submits
				listeners: {
					click: function(e) {
						e.preventDefault();
					},
					element: 'el'
				}
			});

			me.toolbar = toolbar;
		}
	}],
	buttonAlign: 'left',
	buttons: [{
		text: 'Add Html',
		action: 'addhtml'
	}, {
		text: 'Reset',
		action: 'resethtml'
	}, {
		text: 'Cancel',
		handler: function(button) {
			button.up('window').close();
		}
	}]

});