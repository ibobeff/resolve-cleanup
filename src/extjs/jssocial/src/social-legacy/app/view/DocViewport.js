/**
 * Viewport displayed when the user views a component 
 */
Ext.define('RS.view.DocViewport', {
	extend : 'Ext.container.Viewport',

	requires : ['RS.view.form.PostForm', 'RS.view.panel.GenericPostPanel', 'RS.view.menu.RefreshButton'],
	cls : 'rs-panel',
	layout : {
		type : 'vbox',
		align : 'stretch'
	},

	defaults : {
		margins : '10'
	},
	
	/**
	 * @cfg selectionDetails {Object} Configuration to simulate the user clicking on a stream since no streams will be displayed on this view.  Configuration should be something like:
	 * 
	 * selectionDetails : {
	 * 		displayName : social.compFullName,
	 * 		sys_id : social.compSysId,
	 * 		comptype : social.compType,
	 * 		parent_comptype : '',
	 * 		parent_sysid : '',
	 * 		hadSelection : true
	 * 	}
	 * 	
	 */
	
	initComponent : function() {
		this.items = [{
			xtype : 'postform',
			border : false,
			height : 140,
			selectionDetails : this.selectionDetails
		}, {
			xtype : 'genericpostpanel',
			selectionDetails : this.selectionDetails,
			flex : 1
		}];
		this.callParent(arguments);
	}
});

