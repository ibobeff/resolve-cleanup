/**
 * Viewport displayed when the user searches for a social component
 */
Ext.define('RS.view.SearchPostViewport', {
	extend: 'Ext.container.Viewport',

	requires: ['RS.view.form.PostForm', 'RS.view.panel.GenericPostPanel'],
	cls: 'rs-panel',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	defaults: {
		margins: '10'
	},

	/**
	 * @cfg selectionDetails {Object} Configuration to simulate the user clicking on a stream since no streams will be displayed on this view.  Configuration should be something like:
	 *
	 * selectionDetails : {
	 * 		displayName : social.compFullName,
	 * 		sys_id : social.compSysId,
	 * 		comptype : social.compType,
	 * 		parent_comptype : '',
	 * 		parent_sysid : '',
	 * 		hadSelection : true,
	 * 		searchPostKey: 'search from user',
	 * 		searchPostTime: 'ALL',
	 *      queryType: '',
	 *      resultsType: 'Post',
	 * 		doSearch: true
	 * 	}
	 *
	 */

	initComponent: function() {
		this.items = [{
			xtype: 'genericpostpanel',
			selectionDetails: this.selectionDetails,
			flex: 1
		}];
		this.callParent(arguments);
	}
});