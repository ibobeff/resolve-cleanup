/**
 * Control to display a large image when the user clicks on a thumbnail image within a post.
 */
Ext.define('RS.view.control.ImageViewer', {
	extend: 'Ext.window.Window',

	layout: 'fit',
	frame: false,
	closeAction: 'hide',
	modal: true,
	constrain: true,
	onEsc: function() {
		this.close();
	},
	baseCls: 'x-panel',

	/**
	 * The source of the image tag to display in the large window
	 */
	src: '',

	initComponent: function() {
		this.items = {
			xtype: 'component',
			autoEl: {
				tag: 'img',
				style: 'height:auto;width:auto',
				src: this.src
			},
			listeners: {
				render: {
					scope: this,
					fn: function(cmp) {
						this.getImage().on('load', this.onImageLoad, this, {
							single: true
						});
					}
				}
			}
		};
		this.callParent(arguments);
	},

	onImageLoad: function() {
		this.getEl().unmask();
		var size = Ext.fly(Ext.getBody()).getSize(),
			bodyHeight = size.height,
			bodyWidth = size.width,
			image = this.getImage();
		image.setStyle({
			height: 'auto',
			width: 'auto'
		});
		var h = image.dom.offsetHeight,
			w = image.dom.offsetWidth,
			toConfig = {};
		
		//preserve scale
		var scaledSize = this.scaleSize(bodyWidth - 26, bodyHeight - 58, w, h);
		//10-25 to bring the image away from the edges
		var width = scaledSize[0] + 16;
		var height = scaledSize[1] + 38
		var x = Math.round((bodyWidth - width) / 2);
		var y = Math.round((bodyHeight - height) / 2);
		toConfig = {
			width: width,
			height: height,
			x: x,
			y: y
		};

		this.animate({
			to: toConfig,
			listeners: {
				afteranimate: {
					fn: function() {
						this.doComponentLayout();
					},
					scope: this
				}
			}
		});
	},

	/**
	 * Scales the image to the max height and width from the current height and width
	 */
	scaleSize: function(maxW, maxH, currW, currH) {

		var ratio = currH / currW;

		if(currW >= maxW && ratio <= 1) {
			currW = maxW;
			currH = currW * ratio;
		} else if(currH >= maxH) {
			currH = maxH;
			currW = currH / ratio;
		}

		return [currW, currH];
	},

	/**
	 * Sets the image source and resizes the window to the largest size available (the size of the image or the size of the browser window) maintaining scale of the image.
	 */
	setSrc: function(src) {
		this.getEl().mask('Loading...');
		var image = this.getImage();
		image.on('load', this.onImageLoad, this, {
			single: true
		});
		image.dom.src = src;
		image.dom.style.width = image.dom.style.height = 'auto';
	},
	getImage: function() {
		return this.items.items[0].getEl();
	},
	initEvents: function() {
		this.callParent(arguments);
		if(this.resizer) {
			this.resizer.preserveRatio = true;
		}
	}
});