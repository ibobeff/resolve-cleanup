/**
 * Form to collect information to post.  Collects information for files, links, html and posts that information to the selected stream
 */
Ext.define('RS.view.form.PostForm', {
	extend: 'Ext.form.Panel',
	alias: 'widget.postform',

	requires: ['RS.view.plugin.AutoComplete'],

	border: false,
	cls: 'rs-panel',

	dockedItems: [{
		xtype: 'toolbar',
		cls: 'rs-toolbar',
		dock: 'bottom',
		items: [{
			xtype: 'button',
			action: 'post',
			text: 'Post',
			itemId: 'postButton',
			listeners: {
				render: function(button) {
					//COMMENT-4.1: html doesn't work in 4.1 for some reason
					button.update('<font size="2"><b>&nbsp;&nbsp;Post&nbsp;&nbsp;</b></font>');
				}
			}
		}, {
			xtype: 'button',
			action: 'showfileupload',
			text: 'File'
		}, {
			xtype: 'button',
			action: 'showlink',
			text: 'Link'
		}, {
			xtype: 'button',
			action: 'showhtml',
			text: 'Html'
		}, '->',
		{
			xtype: 'splitbutton',
			iconCls: 'x-btn-icon x-tbar-loading',
			tooltip: 'Reload Posts',
			action: 'refreshPostPanel',
			menu: [{
				text: 'Auto Refresh',
				itemId: 'autoRefresh',
				checked: false,
				action: 'autorefresh'
			}]
		}]
	}],

	defaults: {
		anchor: '-50'
	},

	initComponent: function() {
		var initializeParams = Ext.apply({}, this.selectionDetails);

		this.items = [{
			xtype: 'displayfield',
			name: 'postTo',
			fieldLabel: 'Posting To',
			itemId: 'postingTo',
			labelWidth: 70,
			value: Ext.String.format('<b>{0}</b>', initializeParams.displayName || 'Blog')
		}, {
			xtype: 'textareafield',
			height: 84,
			name: 'postContents',
			enableKeyEvents: true,
			editable: true,
			hidden: false,
			plugins: [{
				ptype: 'auto-complete',
				store: 'MentionStore',
				displayField: 'name',
				valueField: 'value'
			}],
			setTextAreaValue: function(val) {
				this.setValue(val);
				this.plugins[0].lastQuery = null;
			}
		}, {
			xtype: 'hiddenfield',
			name: 'postSysId'
		}];

		this.callParent(arguments);
		
		if( Ext.state.Manager.get('socialAutoRefresh') ){
			this.down('#autoRefresh').setChecked(true);
		}
	},

	setForum: function(isForum, currentlySelectedName){
		if( isForum ){
			//set to Comment instead of post
			this.down('#postButton').update('<font size="2"><b>&nbsp;&nbsp;Comment&nbsp;&nbsp;</b></font>');
			this.down('#postingTo').setFieldLabel('Return To');
			this.down('#postingTo').setValue(Ext.String.format('<a style="color: blue;text-decoration:underline;cursor:pointer;cursor:hand;" onclick="returnForum()">{0}</a>', currentlySelectedName));
		}
		else{
			//set to post 
			this.down('#postButton').update('<font size="2"><b>&nbsp;&nbsp;Post&nbsp;&nbsp;</b></font>')
			this.down('#postingTo').setFieldLabel('Posting To');
			this.down('#postingTo').setValue(Ext.String.format('<b>{0}</b>', currentlySelectedName));
		}
	}
});