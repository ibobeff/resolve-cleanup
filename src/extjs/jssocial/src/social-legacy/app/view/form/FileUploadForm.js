/**
 * Form to display when the user needs to attach a file to a post 
 */
Ext.define('RS.view.form.FileUploadForm', {
	extend : 'Ext.form.Panel',
	alias : 'widget.fileuploadform',

	requires : ['Ext.form.Panel', 'Ext.form.field.File'],

	autoShow : true,

	bodyPadding : '10',

	initComponent : function() {
		this.callParent(arguments);
		this.form.url = '/resolve/service/post/attach';
		this.form.method = 'POST';
		//this.form.standardSubmit = true;
	},

	defaults : {
		anchor : '100%',
		msgTarget : 'side'
	},

	items : [{
		allowBlank : false,
		xtype : 'filefield',
		id : 'form-file',
		emptyText : 'Select a File',
		fieldLabel : 'File',
		name : 'file-path',
		buttonText : 'Select'
	}, {
		xtype : 'textfield',
		name : 'newfilename',
		fieldLabel : 'Description'
	}]

});
