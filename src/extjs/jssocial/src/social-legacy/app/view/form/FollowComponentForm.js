/**
 * Form to display when the user hovers over a component in social.  Displays details about the component and allows the user to follow/unfollow the component
 */
Ext.define('RS.view.form.FollowComponentForm', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.followcomponentform',

	border : false,

	/**
	 * Name of the component to display
	 */
	name : 'Name',

	/**
	 * Description of the component to display
	 */
	description : 'Description',

	/**
	 * True if the user is following the provided component, false otherwise
	 */
	following : true,

	/**
	 * True if the user should be given the option of following the component (possibly not because of permissions)
	 */
	canFollow : true,

	/**
	 * The component type to be displayed
	 */
	componentType : 'process',

	/**
	 * The text to display when the component isn't being followed
	 */
	followText : 'Follow',

	/**
	 * The icon to display when the component isn't being followed
	 */
	followIcon : '/resolve/images/plus.gif',

	/**
	 * The tooltip to display on the follow button when the component isn't being followed.  Will be passed the name of the component if a {0} variable is provided
	 */
	followTooltip : 'Click to follow {0}',

	/**
	 * The link to use when the user clicks on the name of the component.  This link should 'go to' the component in a new window
	 */
	gotoLink : '',

	/**
	 * The text to display when the component is being followed
	 */
	followingText : 'Unfollow',

	/**
	 * The icon to display when the component is being followed
	 */
	followingIcon : '/resolve/images/ok_st_obj.gif',

	/**
	 * The tooltip to display on the unfollow button when the component is being followed.  Will be passed the name of the component if a {0} variable is provided
	 */

	followingTooltip : 'Click to stop following {0}',

	initComponent : function() {
		switch( this.compType ) {
			case 'process':
				this.canFollow = false;
				this.followingText = 'Leave Process';
				this.followingTooltip = 'Click to leave the process {0}';
				break;
			case 'team':
				this.canFollow = false;
				this.followingText = 'Leave Team';
				this.followingTooltip = 'Click to leave the team {0}';
				break;
			case 'forum':
				this.followText = 'Join Forum';
				this.followingText = 'Leave Forum';
				this.followingTooltip = 'Click to leave the forum {0}';
				break;
			case 'document':
			case 'actiontask':
			case 'feed':
			case 'rss':
			case 'runbook':
				this.followingText = 'Unfollow';
				this.followingTooltip = 'Click to stop following {0}';
				break;
		}

		//@formatter:off
		var descriptionText = this.description ? ['<span class="popupTitle">',this.description,'</span>','<br/>'].join('') : '';

		this.html = [
			'<table>',
				'<tr>',
					'<td style="padding-left: 5px;font-size: 14px;">',
						this.gotoLink ? Ext.String.format('<span onclick="hidePopupOnMouseOut();window.open(\'{0}\');">',this.gotoLink):'',
						'<span class="popupName">',this.name,'</span>',
						this.gotoLink ? '</span>':'','<br/>',
						descriptionText,'<br/>',
					'</td>',
				'</tr>',
			'</table>'
		].join('');
		//@formatter:on

		this.buttons = this.buttons || [];

		if (this.following || this.canFollow) {
			this.buttons.push({
				text : this.following ? this.followingText : this.followText,
				icon : this.following ? this.followingIcon : this.followIcon,
				tooltip : this.following ? Ext.String.format(this.followingTooltip, this.name) : Ext.String.format(this.followTooltip, this.name),
				scope : this,
				handler : function(button) {
					var tooltip = Ext.tip.QuickTipManager.getQuickTip();
					if (tooltip)
						tooltip.hide();
					if (!this.canFollow)
						button.hide();
					this.following = !this.following;

					if (this.following) {
						button.setText(this.followingText);
						button.setIcon(this.followingIcon);
						button.setTooltip(Ext.String.format(this.followingTooltip, this.name));
					} else {
						button.setText(this.followText);
						button.setIcon(this.followIcon);
						button.setTooltip(Ext.String.format(this.followTooltip, this.name));
					}

					var win = button.up('window');
					Ext.Ajax.request({
						url : '/resolve/service/social/followComponent',
						params : {
							follow : this.following,
							comptype : win.compType,
							sysid : win.sysId
						},
						success : function(r) {
							var response = RS.common.parsePayload(r);
							if (response.success) {
								//do nothing
							} else {
								//display error
							}
						}
					});
				}
			});
		}

		this.callParent();
	}
});
