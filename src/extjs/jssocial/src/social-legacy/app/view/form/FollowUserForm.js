/**
 * Form to display when the user hovers over a user in social.  Displays details about the user and allows the user to follow/unfollow the user
 */
Ext.define('RS.view.form.FollowUserForm', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.followuserform',

	border : false,

	/**
	 * Name of the user to display
	 */
	name : 'User Name',

	/**
	 * Title of the user to display
	 */
	description : 'Title',

	/**
	 * Email of the user to display
	 */
	userEmail : 'user@email.com',

	/**
	 * Phone number of the user to display
	 */
	phone : '555-1234',

	/**
	 * Mobile phone number of the user to display
	 */
	mobilePhone : '555-1234',

	/**
	 * Image to use when displaying the user
	 */
	photo : '/resolve/images/social/emptyUser.png',

	/**
	 * True if the user is currently following the provided user, false otherwise
	 */
	following : false,

	/**
	 * True if the user is able to follow the provided user.  They might not be able to because of permissions.
	 */
	canFollow : true,

	/**
	 * True if the passed user is the current user (so that they can't follow themselves)
	 */
	currentUser : false,

	/**
	 * The link to use when the user clicks on the name of the user.  This link should 'go to' the user in a new window
	 */
	gotoLink : '',

	/**
	 * The text to display when the component isn't being followed
	 */
	followText : 'Follow',

	/**
	 * The icon to display when the component isn't being followed
	 */
	followIcon : '/resolve/images/plus.gif',

	/**
	 * The tooltip to display on the follow button when the component isn't being followed.  Will be passed the name of the component if a {0} variable is provided
	 */
	followTooltip : 'Click to follow {0}',

	/**
	 * The text to display when the component is being followed
	 */
	followingText : 'Following',

	/**
	 * The icon to display when the component is being followed
	 */
	followingIcon : '/resolve/images/ok_st_obj.gif',

	/**
	 * The tooltip to display on the unfollow button when the component is being followed.  Will be passed the name of the component if a {0} variable is provided
	 */
	followingTooltip : 'Click to stop following {0}',

	initComponent : function() {
		//@formatter:off
		var descriptionText = this.description ? ['<span class="popupTitle">',this.description,'</span>','<br/>'].join('') : '';
		var emailText = this.userEmail ? [Ext.String.format('<a class="popupData" href="mailto:{0}">',this.userEmail),this.userEmail,'</a><br/>'].join('') : '';
		var phoneText = this.phone ? [Ext.String.format('<span class="popupData">Phone: {0}</span><br/>', this.phone)].join('') : '';
		var mobileText = this.mobilePhone ? [Ext.String.format('<span class="popupData">Mobile: {0}</span><br/>', this.mobilePhone)].join('') : '';
		this.html = [
			'<table>',
				'<tr>',
					'<td style="vertical-align: top;">',
						Ext.String.format('<img style="height:45px;width:45px;" src="{0}"/>', this.photo),
					'</td>',
					'<td style="padding-left: 5px; font-size: 14px;">',
						Ext.String.format('<span style="cursor:hand;cursor:pointer" class="popupName" onclick="hidePopupOnMouseOut();window.open(\'{0}\');">', this.gotoLink),this.name,'</span>','<br/>',
						descriptionText,
						emailText,
						phoneText,
						mobileText,
					'</td>',
				'</tr>',
			'</table>'
		].join('');
		//@formatter:on

		this.buttons = this.buttons || [];

		if (this.canFollow && !this.currentUser) {
			this.buttons.push({
				text : this.following ? this.followingText : this.followText,
				icon : this.following ? this.followingIcon : this.followIcon,
				tooltip : Ext.String.format(this.following ? this.followingTooltip : this.followTooltip, this.name),
				scope : this,
				handler : function(button) {
					var tooltip = Ext.tip.QuickTipManager.getQuickTip();
					if (tooltip)
						tooltip.hide();
					this.following = !this.following;
					if (this.following) {
						button.setText(this.followingText);
						button.setIcon(this.followingIcon);
						button.setTooltip(Ext.String.format(this.followingTooltip, this.name));
					} else {
						button.setText(this.followText);
						button.setIcon(this.followIcon);
						button.setTooltip(Ext.String.format(this.followTooltip, this.name));
					}

					var win = button.up('window');
					Ext.Ajax.request({
						url : '/resolve/service/social/followComponent',
						params : {
							follow : this.following,
							comptype : win.compType,
							sysid : win.sysId
						},
						success : function(r) {
							var response = RS.common.parsePayload(r);
							if (response.success) {
								//do nothing
							} else {
								//display error
							}
						}
					});
				}
			});
		}

		this.callParent();
	}
});
