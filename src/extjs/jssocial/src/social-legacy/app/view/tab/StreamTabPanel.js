/**
 * Tab to display a list of streams and the posts for the selected stream
 */
Ext.define('RS.view.tab.StreamTabPanel', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.streamtabpanel',

	requires : ['RS.view.panel.GenericPostPanel', 'RS.view.menu.RefreshButton'],

	border : false,
	cls : 'rs-panel',
	layout : {
		type : 'border',
		align : 'stretch'
	},

	initComponent : function() {
		var streamTree = {
			xtype : 'streamtree',
			split : true,
			region : 'west',
			border : false,
			flex : 1,
			streamName : this.name,
			minWidth: 150,
			maxWidth: 400
		};

		Ext.apply(streamTree, this.streamTreeConfig);
		this.items = [streamTree, {
			xtype : 'genericpostpanel',
			region : 'center',
			border : false,
			// style: 'padding-top: 22px;',
			flex : 5
		}];

		this.callParent();
	}
});
