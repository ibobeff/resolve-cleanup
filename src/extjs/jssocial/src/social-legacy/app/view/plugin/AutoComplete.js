/*
 * Plugin, which can be used with any class, which inherits from Ext.form.field.Text (xtype: textfield).
 * The plugin requires a textarea.
 *
 * Note: This plugin is a generalisation of the combo-box auto-complete feature.
 */
Ext.define('RS.view.plugin.AutoComplete', {
	extend : 'Ext.AbstractPlugin',
	mixins : ['Ext.util.Observable'],
	requires : ['Ext.XTemplate', 'Ext.view.BoundList', 'Ext.view.BoundListKeyNav', 'Ext.data.Store'],
	alias : 'plugin.auto-complete',
	pageSize : 0,
	delimiter : ',',
	defaultListConfig : {
		emptyText : '',
		loadingText : 'Loading...',
		loadingHeight : 70,
		minWidth : 100,
		maxHeight : 300,
		shadow : 'sides'
	},
	pickerAlign : 'tl-bl?',
	selectOnTab : true,

	/**
	 * Regular expression to parse the @ or # tags in a post as the user types
	 */
	tagRegex : /(^|\s)[@|#]\[\w+\]/g,

	matchFieldWidth : true,

	init : function(component) {
		var me = this;

		// Custom events
		me.hasListeners = ['expand', 'collapse', 'select'];

		//initialize the store
		me.store = Ext.data.StoreManager.lookup(me.store);
		me.store.on({
			scope : me,
			load : me.onLoad,
			exception : me.collapse
		});

		if (!this.displayTpl) {
			this.displayTpl = Ext.create('Ext.XTemplate', '<tpl for=".">' + '{[typeof values === "string" ? values : values.' + this.valueField + '.substr(1)]}' + '<tpl if="xindex < xcount">' + this.delimiter + '</tpl>' + '</tpl>');
		} else if (Ext.isString(this.displayTpl)) {
			this.displayTpl = Ext.create('Ext.XTemplate', this.displayTpl);
		}

		// Setup key event when component is rendered, so we have 'inputEl'.
		// Note: We bypass the 'enableKeyEvents' config of text fields.
		component.on('render', function(cmp, obj) {
			this.mon(this.cmp.inputEl, {
				scope : this,
				keyup : {
					fn : this.onKeyUp,
					buffer : 500
				},
				keydown : {
					fn : this.onKeyDown
				}
			});
		}, this);
	},

	onLoad : function(store, records, successful, eOpts) {
		if (records.length == 0) {
			this.collapse();
		} else {
			//show the suggestions
			this.expand();
		}
	},

	onKeyDown : function(event, component, eOpts) {
		var me = this;
		if (event.getKey() == event.ESC) {
			me.collapse();
			return;
		}
	},

	onKeyUp : function(event, component, eOpts) {
		var me = this;

		if (event.getKey() == event.ESC) {
			return;
		}

		//if there is a new query from the component's getValue, then we should run the query
		var text = me.cmp.getValue();
		if (text) {
			var query;
			var textSplit = text.split(me.tagRegex);
			var hashText = '';
			for (var i = textSplit.length - 1; i >= 0; i--) {
				var s = textSplit[i];
				var index = s.indexOf('@');
				var hashIndex = s.indexOf('#');
				if (index > -1) {
					query = s.substr(index);
					break;
				} else if (hashIndex > -1) {
					query = s.substr(hashIndex);
					break;
				}
			}

			if (query && (query.charAt(0) == '@' || query.charAt(0) == '#')) {
				if (query.indexOf(' ') > -1) {
					me.collapse();
					// query = query.slice(0, query.indexOf(' '));
					return;
				}

				if (me.lastQuery != query) {
					me.lastQuery = query;
					me.store.load({
						params : {
							query : query
						}
					});
				} else {
					//show the suggestions
					//we already have the records already because the searches match
					me.expand();
				}
				//wait a bit so it doesn't react to the down arrow opening the picker
				me.cmp.inputEl.focus();
			}
		}
	},

	onItemClick : function(picker, record) {
		/*
		 * If we're doing single selection, the selection change events won't fire when
		 * clicking on the selected element. Detect it here.
		 */
		var me = this, lastSelection = me.lastSelection, valueField = me.valueField, selected;

		if (lastSelection) {
			selected = lastSelection[0];
			if (record.get(valueField) === selected.get(valueField)) {
				me.collapse();
			}
		}
		me.setValue(record);
	},

	onListRefresh : function() {
		this.alignPicker();
		this.syncSelection();
	},

	alignPicker : function() {
		var me = this, picker = me.picker, heightAbove = me.cmp.getPosition()[1] - Ext.getBody().getScroll().top, heightBelow = Ext.Element.getViewHeight() - heightAbove - me.cmp.getHeight(), space = Math.max(heightAbove, heightBelow);

		if (me.isExpanded) {
			picker = me.getPicker();
			if (me.matchFieldWidth) {
				// Auto the height (it will be constrained by min and max width) unless there are no records to display.
				picker.setSize(me.cmp.bodyEl.getWidth(), picker.store && picker.store.getCount() ? null : 0);
			}
			if (picker.isFloating()) {
				me.doAlign();
			}
		}

		if (picker.getHeight() > space) {
			picker.setHeight(space - 5);
			// have some leeway so we aren't flush against
			me.doAlign();
		}
	},

	/**
	 * Performs the alignment on the picker using the class defaults
	 * @private
	 */
	doAlign : function() {
		var me = this, picker = me.picker, aboveSfx = '-above', isAbove;

		me.picker.alignTo(me.cmp.inputEl, me.pickerAlign, me.pickerOffset);
		// add the {openCls}-above class if the picker was aligned above
		// the field due to hitting the bottom of the viewport
		isAbove = picker.el.getY() < me.cmp.inputEl.getY();
		me.cmp.bodyEl[isAbove ? 'addCls' : 'removeCls'](me.openCls + aboveSfx);
		picker[isAbove ? 'addCls' : 'removeCls'](picker.baseCls + aboveSfx);
	},

	onListSelectionChange : function(list, selectedRecords) {
		var me = this;
		// Only react to selection if it is not called from setValue, and if our list is
		// expanded (ignores changes to the selection model triggered elsewhere)
		if (!me.ignoreSelection && me.isExpanded) {
			Ext.defer(me.collapse, 1, me);
			me.setValue(selectedRecords, false);
			if (selectedRecords.length > 0) {
				me.fireEvent('select', me, selectedRecords);
			}
			me.cmp.inputEl.focus();
		}
	},

	/**
	 * Returns a reference to the picker component for this field, creating it if necessary by
	 * calling {@link #createPicker}.
	 * @return {Ext.Component} The picker component
	 */
	getPicker : function() {
		var me = this;
		return me.picker || (me.picker = me.createPicker());
	},

	createPicker : function() {
		var me = this, picker, menuCls = Ext.baseCSSPrefix + 'menu', opts = Ext.apply({
			selModel : {
				mode : 'SINGLE'
			},
			floating : true,
			hidden : true,
			ownerCt : me.ownerCt,
			cls : me.cmp.el.up('.' + menuCls) ? menuCls : '',
			store : me.store,
			displayField : me.displayField,
			focusOnToFront : false,
			pageSize : me.pageSize
		}, me.listConfig, me.defaultListConfig);

		picker = me.picker = Ext.create('Ext.view.BoundList', opts);
		me.mon(picker, {
			//itemclick : me.onItemClick,
			refresh : me.onListRefresh,
			scope : me
		});

		me.mon(picker.getSelectionModel(), {
			selectionChange : me.onListSelectionChange,
			scope : me
		});

		return picker;
	},

	expand : function() {
		var me = this, bodyEl, picker, collapseIf;

		if (!me.isExpanded && !me.isDestroyed) {
			bodyEl = me.cmp.bodyEl;
			picker = me.getPicker();
			collapseIf = me.collapseIf;

			// show the picker and set isExpanded flag
			picker.show();
			me.isExpanded = true;
			me.alignPicker();
			bodyEl.addCls(me.openCls);

			// monitor clicking and mousewheel
			me.mon(Ext.getDoc(), {
				mousewheel : collapseIf,
				mousedown : collapseIf,
				scope : me
			});
			Ext.EventManager.onWindowResize(me.alignPicker, me);
			me.fireEvent('expand', me);
			me.onExpand();
		}
	},

	/**
	 * @private
	 * Enables the key nav for the BoundList when it is expanded.
	 */
	onExpand : function() {
		var me = this, keyNav = me.listKeyNav, selectOnTab = me.selectOnTab, picker = me.getPicker();

		// Handle BoundList navigation from the input field. Insert a tab listener specially to enable selectOnTab.
		if (keyNav) {
			keyNav.enable();
		} else {
			keyNav = me.listKeyNav = Ext.create('Ext.view.BoundListKeyNav', this.cmp.inputEl, {
				boundList : picker,
				forceKeyDown : true,
				tab : function(e) {
					if (selectOnTab) {
						this.selectHighlighted(e);
						me.collapse();
					}
					// Tab key event is allowed to propagate to field
					return true;
				}
			});
		}

		// While list is expanded, stop tab monitoring from Ext.form.field.Trigger so it doesn't short-circuit selectOnTab
		if (selectOnTab) {
			me.ignoreMonitorTab = true;
		}

		Ext.defer(keyNav.enable, 1, keyNav);
		//wait a bit so it doesn't react to the down arrow opening the picker
		me.cmp.inputEl.focus();
	},

	/**
	 * Collapse this field's picker dropdown.
	 */
	collapse : function() {
		if (this.isExpanded && !this.isDestroyed) {
			var me = this, openCls = me.openCls, picker = me.picker, doc = Ext.getDoc(), collapseIf = me.collapseIf, aboveSfx = '-above';

			// hide the picker and set isExpanded flag
			picker.getSelectionModel().deselectAll();
			picker.hide();
			me.isExpanded = false;

			// remove the openCls
			me.cmp.bodyEl.removeCls([openCls, openCls + aboveSfx]);
			picker.el.removeCls(picker.baseCls + aboveSfx);

			// remove event listeners
			doc.un('mousewheel', collapseIf, me);
			doc.un('mousedown', collapseIf, me);
			Ext.EventManager.removeResizeListener(me.alignPicker, me);
			me.fireEvent('collapse', me);
			me.onCollapse();
		}
	},

	/**
	 * @private
	 * Disables the key nav for the BoundList when it is collapsed.
	 */
	onCollapse : function() {
		var me = this, keyNav = me.listKeyNav;
		if (keyNav) {
			keyNav.disable();
			me.ignoreMonitorTab = false;
		}
		Ext.Function.defer(me.cmp.inputEl.focus, 1, me.cmp.inputEl);
		// me.cmp.inputEl.focus();
	},

	/**
	 * @private
	 * Runs on mousewheel and mousedown of doc to check to see if we should collapse the picker
	 */
	collapseIf : function(e) {
		var me = this;
		if (!me.isDestroyed && !e.within(me.bodyEl, false, true) && !e.within(me.picker.el, false, true)) {
			me.collapse();
		}
	},

	setValue : function(value, doSelect) {
		var me = this, valueNotFoundText = me.valueNotFoundText, inputEl = me.cmp.inputEl, i, len, record, models = [], displayTplData = [], processedValue = [];

		if (me.store.loading) {
			// Called while the Store is loading. Ensure it is processed by the onLoad method.
			me.value = value;
			return me;
		}

		// This method processes multi-values, so ensure value is an array.
		value = Ext.Array.from(value);

		// Loop through values
		for ( i = 0, len = value.length; i < len; i += 1) {
			record = value[i];
			if (!record || !record.isModel) {
				record = me.findRecordByValue(record);
			}
			// record found, select it.
			if (record) {
				models.push(record);
				displayTplData.push(record.data);
				processedValue.push(record.get(me.valueField));
			}
			// record was not found, this could happen because
			// store is not loaded or they set a value not in the store
			else {
				// if valueNotFoundText is defined, display it, otherwise display nothing for this value
				if (Ext.isDefined(valueNotFoundText)) {
					displayTplData.push(valueNotFoundText);
				}
				processedValue.push(value[i]);
			}
		}

		// Set the value of this field. If we are multiselecting, then that is an array.
		me.value = me.multiSelect ? processedValue : processedValue[0];
		if (!Ext.isDefined(me.value)) {
			me.value = null;
		}
		me.displayTplData = displayTplData;
		// me.lastSelection = me.valueModels = models;

		if (inputEl && me.emptyText && !Ext.isEmpty(value)) {
			inputEl.removeCls(me.emptyCls);
		}

		if (me.value) {
			var text = me.cmp.getValue();
			var val = me.value;
			var tag = val.charAt(0);
			var textSplit = text.split(me.tagRegex);
			var oldText = ''
			var hashText = '';
			for (var i = textSplit.length - 1; i >= 0; i--) {
				var s = textSplit[i];
				var index = s.indexOf(tag);
				if (index > -1) {
					oldText = s.substr(0, index);
					hashText = s.substr(index);
					if (hashText.indexOf(' ') > -1)
						hashText = hashText.substr(0, hashText.indexOf(' '));
					break;
				}
			}
			var pre = text.substr(0, text.lastIndexOf(oldText + hashText) + oldText.length);
			var post = text.substr(text.lastIndexOf(oldText + hashText) + (oldText.length + hashText.length) + 1);
			var temp = pre + tag + '[' + val.substr(1) + ']' + post;
			me.cmp.setRawValue(temp);
			me.cmp.checkChange();
		}

		if (doSelect !== false) {
			me.syncSelection();
		}
		me.cmp.applyEmptyText();

		return me;
	},

	syncSelection : function() {
		var me = this, ExtArray = Ext.Array, picker = me.picker, selection, selModel;
		if (picker) {
			// From the value, find the Models that are in the store's current data
			selection = [];
			ExtArray.forEach(me.valueModels || [], function(value) {
				if (value && value.isModel && me.store.indexOf(value) >= 0) {
					selection.push(value);
				}
			});

			// Update the selection to match
			me.ignoreSelection += 1;
			selModel = picker.getSelectionModel();
			selModel.deselectAll();
			if (selection.length) {
				selModel.select(selection);
			}
			me.ignoreSelection -= 1;
		}
	},

	/**
	 * @private Generate the string value to be displayed in the text field for the currently stored value
	 */
	getDisplayValue : function() {
		return this.displayTpl.apply(this.displayTplData);
	},

	findRecordByValue : function(value) {
		return this.findRecord(this.valueField, value);
	},

	findRecord : function(field, value) {
		var ds = this.store, idx = ds.findExact(field, value);
		return idx !== -1 ? ds.getAt(idx) : false;
	}
});
