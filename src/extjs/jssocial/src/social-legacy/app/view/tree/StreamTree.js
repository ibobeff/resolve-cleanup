/**
 * Tree to display the Streams for a particular tab
 */
Ext.define('RS.view.tree.StreamTree', {
	extend: 'Ext.tree.Panel',
	alias: 'widget.streamtree',

	bodyPadding: '10px',
	requires: ['RS.model.Stream'],

	border: false,
	rootVisible: false,
	displayField: 'displayName',

	lines: false,
	cls: 'stream-tree',

	/**
	 * True to override leaf icon images with the @{RS.view.tree.StreamTree#nodeDisplayIconLeaf} property
	 */
	overrideLeafIconImages: true,

	/**
	 * True to override the folder icon images with the @{RS.view.tree.StreamTree#nodeDisplayIconFolder} property
	 */
	overrideFolderIconImages: true,

	/**
	 * Image to use when overriding the leaf icon in the tree
	 */
	nodeDisplayIconLeaf: '/resolve/js/extjs4/resources/themes/images/default/tree/s.gif',

	/**
	 * Image to use when overriding the folder icon in the tree
	 */
	nodeDisplayIconFolder: '/resolve/js/extjs4/resources/themes/images/default/tree/s.gif',

	/**
	 * Template string to use when displaying the folder in the tree
	 */
	nodeDisplayTemplate: '<span class="rs-stream-group-title">{0}</span>',

	leafDisplayTemplate: '{0}<div class="stream-menu" style="float: right"></div>',

	/**
	 * Css class to apply to the folder in the tree
	 */
	nodeDisplayCls: 'rs-stream-group-node',

	//State information
	stateful: true,
	stateEvents: ['itemcollapse', 'itemexpand'],

	initComponent: function() {
		var stream = this.streamName.toLowerCase();
		var url = Ext.String.format('/resolve/service/{0}cp', stream);

		this.store = Ext.create('Ext.data.TreeStore', {
			model: 'RS.model.Stream',
			proxy: {
				type: 'ajax',
				url: url,
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			},
			listeners: {
				scope: this,
				load: this.onTreeLoad,
				append: this.onNodeAppend
			}
		});

		this.tbar = {
			xtype: 'toolbar',
			cls: 'rs-toolbar',
			items: [{
				xtype: 'label',
				html: '<font size="2"><b>Streams</b></font>'
			}
			/*, '->',{
				xtype: 'button',
				icon: '/resolve/images/reload.png',
				tooltip: 'Reload Streams'
			}*/
			]
		};

		this.stateId = stream + 'Tree';

		this.callParent(arguments);

		if(Ext.isGecko) {
			//Firefox doesn't handle float:right elements properly, and because of that we have to manually go in and move the menu
			//div to the left of the entire tree structure so that it will properly be displayed with the menu floating on the right
			//instead of displaying on the next line.  This should happen for each of the nodes as they are added to the view so that it
			//happens immediately instead of relying on the expand/collapse of a particular node
			this.view.on('itemadd', function(records, index, node, eOpts) {
				Ext.each(records, function(record) {
					var node = this.getNode(record);
					var div = Ext.fly(node).down('div[class*=stream-menu]');
					if(div) {
						var owner = Ext.fly(div).parent();
						Ext.fly(div).remove();
						Ext.fly(owner).insertFirst(div);

					}
				}, this);
			});
		}
	},

	onTreeLoad: function(store, node, records, successful, eOpts) {
		//select the first leaf in the tree by default so that we always have a selection
		this.selectFirstNode(this.getRootNode().childNodes);
	},

	selectFirstNode: function(children) {
		for(var i = 0; i < children.length; i++) {
			if(children[i].isLeaf()) {
				this.getSelectionModel().select(children[i]);
				this.fireEvent('itemclick', this, []);
				return true;
			} else if(children[i].isExpanded()) {
				if(this.selectFirstNode(children[i].childNodes)) {
					return true;
				}
			}
		}
	},
	onNodeAppend: function(nodeInterface, node, index, eOpts) {
		if(node.data.leaf) { //] || !node.data.records || node.data.records.length == 0 ) {
			if(!node.data.systemStream) node.data.displayName = Ext.String.format(this.leafDisplayTemplate, node.data.displayName);
			if(this.overrideLeafIconImages) node.data.icon = this.nodeDisplayIconLeaf;
		} else {
			if(this.overrideFolderIconImages) node.data.icon = this.nodeDisplayIconFolder;
			node.data.displayName = Ext.String.format(this.nodeDisplayTemplate, node.data.displayName, node.data.records.length, node.data.records.length === 1 ? 'Item' : 'Items');
			node.data.cls = this.nodeDisplayCls;
		}
	},

	/**
	 * Gets the details about the currently selected item in the tree that we need when doing things like posting or refreshing the posts
	 */
	getSelectedDetails: function() {
		var params = {
			displayName: '',
			sys_id: '',
			comptype: '',
			parentCompType: '',
			parentSysId: '',
			permaLink: this.forumPermaLink
		}

		var selections = this.getSelectionModel().getSelection();
		if(selections.length > 0) {
			Ext.apply(params, selections[0].raw);
			params.hadSelection = true;
			if(selections[0].data.systemStream) {
				params.sys_id = '__sysId__';
			}
		}

		return params;
	},
	getState: function() {
		var nodes = [],
			state = this.callParent();

		this.getRootNode().eachChild(function(child) {
			// function to store state of tree recursively
			var storeTreeState = function(node, expandedNodes) {
					if(node.isExpanded() && node.childNodes.length > 0) {
						expandedNodes.push(node.getPath());

						node.eachChild(function(child) {
							storeTreeState(child, expandedNodes);
						});
					}
				};

			storeTreeState(child, nodes);
		});

		Ext.apply(state, {
			expandedNodes: nodes
		});

		return state;
	},
	applyState: function(state) {
		if(state) {
			var nodes = state.expandedNodes;
			if(nodes && nodes.length > 0) {
				for(var i = 0; i < nodes.length; i++) {
					if(typeof nodes[i] != 'undefined') {
						Ext.defer(this.expandPath, 100, this, [nodes[i]]);
					}
				}
			}
		}
		this.callParent(arguments);
	}
});