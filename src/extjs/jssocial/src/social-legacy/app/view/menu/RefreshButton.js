/**
 * Refresh button to refresh the currently display posts.  Has a menu to turn on and off the auto-refresh of the posts
 */
Ext.define('RS.view.menu.RefreshButton', {
	extend: 'Ext.button.Split',
	alias: 'widget.refreshbutton',
	text: 'Refresh',
	hideRefreshStreams: false,

	initComponent: function() {
		this.menu = [{
			text: 'Auto Refresh',
			checked: Ext.state.Manager.get('socialAutoRefresh') || false,
			action: 'autorefresh'
		}, {
			hidden: this.hideRefreshStreams,
			text: 'Refresh Streams',
			action: 'refreshstreams'
		}];

		this.callParent();
	}
});