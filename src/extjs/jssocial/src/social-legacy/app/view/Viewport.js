Ext.define('RS.view.Viewport', {
	extend: 'Ext.container.Viewport',
	layout: 'border',

	requires: ['RS.view.panel.UserImagePanel', 'RS.view.form.PostForm', 'RS.view.panel.WhatToDoWidget', 'RS.view.tab.StreamTabPanel', 'RS.view.tree.StreamTree', 'RS.view.panel.MenuSet'],

	border: false,
	cls: 'rs-panel',

	initComponent: function() {

		var tabpanel = {
			xtype: 'tabpanel',
			activeTab: 0,
			border: false,
			tabPosition: 'bottom',
			flex: 5,
			cls: 'rs-tab',
			items: [{
				title: 'Home',
				name: 'home',
				xtype: 'streamtabpanel'
			}]
		};

		if(social.socialuserProcessTab) tabpanel.items.push({
			title: 'Process',
			name: 'process',
			xtype: 'streamtabpanel'
		});

		if(social.socialuserTeamTab) tabpanel.items.push({
			title: 'Team',
			name: 'team',
			xtype: 'streamtabpanel'
		});

		if(social.socialuserForumTab) tabpanel.items.push({
			title: 'Forum',
			name: 'forum',
			xtype: 'streamtabpanel'
		});

		if(social.socialuserDocumentTab) tabpanel.items.push({
			title: 'Document',
			name: 'document',
			xtype: 'streamtabpanel'
		});

		if(social.socialuserRunbookTab) tabpanel.items.push({
			title: 'Runbook',
			name: 'runbook',
			xtype: 'streamtabpanel'
		});

		if(social.socialuserActionTaskTab) tabpanel.items.push({
			title: 'Action Task',
			name: 'actiontask',
			xtype: 'streamtabpanel'
		});

		if(social.socialuserUserTab) tabpanel.items.push({
			title: 'User',
			name: 'user',
			xtype: 'streamtabpanel'
		});

		if(social.socialuserFeedTab) tabpanel.items.push({
			title: 'Feed',
			name: 'feed',
			xtype: 'streamtabpanel'
		});

		if(social.socialuserAdvanceTab) tabpanel.items.push({
			title: 'Advanced',
			name: 'advanced',
			xtype: 'streamtabpanel',
			streamTreeConfig: {
				useLines: true,
				nodeDisplayTemplate: '{0}',
				nodeDisplayCls: '',
				overrideFolderIconImages: false
			}
		});

		this.items = [{
			region: 'center',
			xtype: 'panel',
			border: false,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'panel',
				border: false,
				height: 140,
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [{
					xtype: 'userimagepanel',
					border: false,
					flex: 1
				}, {
					xtype: 'postform',
					border: false,
					flex: 5
				}]
			},
			tabpanel]
		}];

		if(social.socialuserShowEastpanel) {
			var items = [];
			if(social.socialuserShowEastpanelWhatToDoWidget) {
				items.push({
					xtype: 'whattodowidget'
				})
			}

			if(social.menuSets) {
				var sets = social.menuSets.split(',');
				Ext.each(sets, function(set) {
					items.push({
						xtype: 'menuset',
						menuSetId: set
					});
				});
			}

			this.items.push({
				region: 'east',
				stateId: 'eastInfoPanel',
				title: 'Info',
				cls: 'rs-panel',
				collapsible: true,
				collapsed: true,
				split: true,
				border: false,
				width: 250,
				items: items
			})
		}

		this.callParent(arguments);
	}
});