/**
 * Social Controller is the main controller that deals with all the logic for the Social application
 * It handles the interaction between the different controls, as well as dealing with posting to and refreshing feeds and streams for users
 */
Ext.define('RS.controller.SocialController', {
	extend: 'Ext.app.Controller',

	requires: ['RS.view.form.FollowUserForm', 'RS.view.form.FollowComponentForm', 'RS.view.window.LikeDialog', 'RS.view.control.ImageViewer'],
	stores: ['LinkStore', 'MentionStore'],
	uses: ['RS.view.window.HtmlWindow', 'RS.view.window.LinkWindow', 'RS.view.window.FileUploadWindow'],

	refs: [{
		ref: 'streamTabs',
		selector: 'viewport > panel > tabpanel'
	}, {
		ref: 'postForm',
		selector: 'postform'
	}, {
		ref: 'fileUploadForm',
		selector: 'fileuploadwindow > form'
	}, {
		ref: 'linkFormRadioGroup',
		selector: 'linkwindow > form > radiogroup'
	}, {
		ref: 'linkFormTextfieldName',
		selector: 'linkwindow > form > textfield[name=tfName]'
	}, {
		ref: 'linkFormTextfieldValue',
		selector: 'linkwindow > form > textfield[name=tfValue]'
	}, {
		ref: 'linkFormCombobox',
		selector: 'linkwindow > form > combo'
	}, {
		ref: 'linkForm',
		selector: 'linkwindow > form'
	}, {
		ref: 'htmlEditor',
		selector: 'htmlwindow > htmleditor'
	}],

	init: function() {
		var me = this;

		me.isForum = false;

		me.control({
			'viewport > panel > tabpanel': {
				tabchange: me.tabChange
			},
			'viewport > panel > tabpanel > panel > streamtree': {
				beforeitemclick: me.beforeItemClick,
				itemclick: me.refreshButtonClicked
			},
			'viewport > panel > toolbar > refreshbutton': {
				click: me.refreshButtonClicked
			},

			'postform  button[action=post]': {
				click: me.submitPost
			},
			'postform  button[action=showfileupload]': {
				click: me.showFileUploadWindow
			},
			'postform  button[action=showlink]': {
				click: me.showLinkWindow
			},
			'postform  button[action=showhtml]': {
				click: me.showHtmlWindow
			},

			'postform  button[action=refreshPostPanel]': {
				click: me.refreshButtonClicked
			},
			'postform  menu > menuitem[action=autorefresh]': {
				click: me.toggleAutoRefresh
			},

			'fileuploadwindow button[action=fileupload]': {
				click: me.uploadFile
			},
			'fileuploadwindow button[action=resetform]': {
				click: me.resetUploadForm
			},

			'linkwindow > form > radiogroup': {
				change: me.rgSelectType
			},
			'linkwindow > form > textfield[name=tfName]': {
				focus: me.tfNameOnFocus
			},
			'linkwindow button[action=addlink]': {
				click: me.addLink
			},
			'linkwindow button[action=resetlink]': {
				click: me.resetLinkForm
			},

			'htmlwindow button[action=addhtml]': {
				click: me.addHtml
			},
			'htmlwindow button[action=resethtml]': {
				click: me.resetHtmlForm
			}
		});

		//this works when fireEvent done from RS.app
		this.application.on({
			'refreshPostPanelEvent': {
				fn: this.refreshPostPanel,
				scope: this
			},
			'toggleStarEvent': {
				fn: this.toggleStar,
				scope: this
			},
			'likeLinkOnClickEvent': {
				fn: this.likeLinkOnClick,
				scope: this
			},
			'submitCommentEvent': {
				fn: this.submitComment,
				scope: this
			},
			'showCommentEvent': {
				fn: this.showComment,
				scope: this
			},
			'showPopupOnMouseOverEvent': {
				fn: this.showPopupOnMouseOver,
				scope: this
			},
			'perhapsHidePopupOnMouseOutEvent': {
				fn: this.perhapsHidePopupOnMouseOut,
				scope: this
			},
			'hidePopupOnMouseOutEvent': {
				fn: this.hidePopupOnMouseOut,
				scope: this
			},
			'displayLikeDialog': {
				fn: this.displayLikeDialog,
				scope: this
			},
			'followUser': {
				fn: this.followUser,
				scope: this
			},
			'deletePostEvent': {
				fn: this.deletePost,
				scope: this
			},
			'deleteCommentEvent': {
				fn: this.deleteComment,
				scope: this
			},
			'displayLargeImage': {
				fn: this.displayLargeImage,
				scope: this
			},
			'showMoreForum': {
				fn: this.showMoreForum,
				scope: this
			},
			'returnForum': {
				fn: this.returnForum,
				scope: this
			}
		});

		//Start grid infinite scrolling poller to check when to get additional records
		Ext.TaskManager.start({
			scope: this,
			run: function() {
				var me = this;
				this.updatePostTimer();
			},
			interval: refreshIntervalForInfiniteRecords() //in milliseconds
		});

		me.getLinkStoreStore().on({
			beforeload: this.onBeforeLoadOfLinkStore,
			scope: this
		});

		me.refreshPostTask = Ext.TaskManager.newTask({
			scope: this,
			run: function() {
				this.refreshPostPanel();
			},
			interval: refreshIntervalForAutoRefreshPost() //in milliseconds
		});
	},

	getCurrentlySelectedStream: function() {
		var tabPanels = Ext.ComponentQuery.query('viewport > panel > tabpanel');
		var activeTab = tabPanels[0].getActiveTab();
		var streamTree = activeTab.down('streamtree');
		var selection = streamTree.getSelectionModel().getSelection();
		if (selection.length > 0) {
			return selection[0];
		}
		return null;
	},

	tabChange: function() {
		//Only get data from the server if there is no data currently loaded
		var tabPanels = Ext.ComponentQuery.query('viewport > panel > tabpanel');
		var activeTab = tabPanels[0].getActiveTab();
		var activePanel = activeTab.down('genericpostpanel');
		if (activePanel) {
			var store = activePanel.store;
			if (store && store.getCount() < 1) {
				this.refreshPostPanel();
			}
		}

		var currentNode = this.getCurrentlySelectedStream();

		if (currentNode && currentNode.isLeaf()) {
			//Update the 'post to' text with the proper display
			this.setPostToText(currentNode.data.displayName, currentNode.data.comptype);
		}
	},
	refreshStreams: function() {
		//get handle to current viewable tree and refresh it to get the latest streams
		var tabPanels = Ext.ComponentQuery.query('viewport > panel > tabpanel');
		var activeTab = tabPanels[0].getActiveTab();
		var streamTree = activeTab.down('streamtree');
		if (streamTree) {
			streamTree.getStore().load();
		}
	},
	beforeItemClick: function(view, record, item, index, e, eOpts) {
		var menuClicked = e.getTarget('.stream-menu');
		if (!record.isLeaf()) {
			if (record.data.displayName && record.data.comptype) {
				this.setPostToText(record.data.displayName, record.data.comptype);
			} else {
				if (record.isExpanded()) record.collapse();
				else record.expand();
				return false;
			}
		} else {
			if (!menuClicked) {
				//Update the 'post to' text with the proper display
				this.returnForum();
				this.setPostToText(record.data.displayName, record.data.comptype);
			} else {
				//display the "context" menu for the node instead of selecting the record
				//Now we need to display the proper context menu for this stream
				//if we're looking at the home tab, then obviously we are trying to remove from home
				var tabPanels = Ext.ComponentQuery.query('viewport > panel > tabpanel'),
					activeTab = tabPanels[0].getActiveTab(),
					menuItem;
				if (activeTab.name == 'home') {
					menuItem = {
						text: 'Remove from Home',
						remove: true
					};
				} else {
					//Otherwise we're trying to add to home, but should only add if its not already there
					var currentNode = record;
					var homeTab = tabPanels[0].items.items[0];

					var homeStreamTree = homeTab.down('streamtree');
					var homeRootNode = homeStreamTree.getRootNode();
					var foundNode = homeRootNode.findChild('sys_id', currentNode.get('sys_id'), true);
					if (foundNode) {
						menuItem = {
							text: 'Remove from Home',
							remove: true
						};
					} else {
						menuItem = {
							text: 'Add to Home',
							remove: false
						};
					}
				}

				if (menuItem) {
					var menuClickEl = Ext.get(menuClicked);
					menuClickEl.addCls('stream-menu-expanded');
					menuItem.scope = this;
					menuItem.handler = this.toggleDisplayOnHome;
					Ext.create('Ext.menu.Menu', {
						margin: '0 0 10 0',
						items: [menuItem, {
							text: 'Edit',
							handler: this.editMenuItem,
							scope: this
						}],
						listeners: {
							hide: function() {
								menuClickEl.removeCls('stream-menu-expanded');
							},
							close: function() {
								menuClickEl.removeCls('stream-menu-expanded');
							}
						}
					}).showBy(menuClickEl); //showAt(e.getX(), e.getY(), true);
				}
				return false;
			}
		}
	},
	toggleAutoRefresh: function() {
		var me = this;
		if (me.refreshPostTask.stopped) {
			me.refreshPostTask.start();
			Ext.state.Manager.set('socialAutoRefresh', true);
		} else {
			me.refreshPostTask.stop();
			Ext.state.Manager.set('socialAutoRefresh', false);
		}
	},
	refreshButtonClicked: function() {
		this.showedComment = false;
		this.refreshPostPanel();
	},
	refreshPostPanel: function() {
		if (this.showedComment) return;
		//get handle to the tab panel to find out the active tab
		var tabPanels = Ext.ComponentQuery.query('viewport > panel > tabpanel');
		if (tabPanels && tabPanels.length > 0) {
			var activeTab = tabPanels[0].getActiveTab();
			var activePanel = activeTab.down('genericpostpanel');

			if (activePanel) {
				var store = activePanel.store;

				if (store) {
					//reset the flag
					store.getProxy().extraParams.endOfRecs = false;

					//Defer the loading to allow the backend to process the new post fully
					Ext.Function.defer(store.load, 500, store);
				}
			}
		} else {
			//We don't have the tabs and we're looking at a one-off like view document or search
			var panels = Ext.ComponentQuery.query('viewport > genericpostpanel');
			if (panels && panels.length > 0) {
				var store = panels[0].store;
				if (store) {
					//reset the flag
					store.getProxy().extraParams.endOfRecs = false;

					//Defer the loading to allow the backend to process the new post fully
					Ext.Function.defer(store.load, 500, store);
				}
			}
		}
	},
	updatePostTimer: function() {
		//for calculations refer - http://net.tutsplus.com/tutorials/javascript-ajax/how-to-create-an-infinite-scroll-web-gallery/
		var postPanel;
		var tabs = this.getStreamTabs();
		if (tabs) {
			var activeTab = this.getStreamTabs().getActiveTab();
			if (activeTab) postPanel = activeTab.down('genericpostpanel');
		} else {
			postPanel = Ext.ComponentQuery.query('viewport > genericpostpanel')[0];
		}
		var postPanelEl = postPanel.getEl();

		//get the content height
		var contentHeight = 0;
		if (postPanelEl && (postPanel.calculateHeight || postPanel.contentHeight < 1)) {
			var feedContainers = postPanelEl.query('.feedcontainer');
			Ext.each(feedContainers, function(container) {
				contentHeight += Ext.fly(container).getHeight()
			});
			postPanel.contentHeight = contentHeight;
			postPanel.calculateHeight = false;
		} else {
			contentHeight = postPanel.contentHeight;
		}

		var scrollPosition = postPanel.body.dom.scrollTop;
		var pageHeight = document.documentElement.clientHeight;
		//same as viewport height
		var viewportHeight = Ext.getBody().getViewSize().height;
		var calculatedHeight = contentHeight - viewportHeight - scrollPosition;

		if (contentHeight > 500 && scrollPosition > 0 && calculatedHeight < 500) {
			//console.log(calculatedHeight + " - " + contentHeight + " - " + pageHeight + " - " + viewportHeight + " - " + scrollPosition + " -- scroll LESS than 500");
			var store = postPanel.store;
			var params = store.getProxy().extraParams;
			if (!params.endOfRecs) {
				params.infiniteScroll = true;
				store.load();
			}
		} else {
			//console.log(calculatedHeight + " - " + contentHeight + " - " + pageHeight + " - " + viewportHeight + " - " + scrollPosition + " -- scroll GREATER than 500");
		}
	},
	editMenuItem: function(menu, item, eventObj, eOpts) {
		var record = this.getCurrentlySelectedStream();
		if (record) {
			var type = record.data.comptype,
				sysId = record.data.sys_id,
				displayName = record.raw.displayName;
			//Now depending on the type of this record, we need to construct the link to edit the component properly
			var url = '';
			switch (type) {
				case 'Actiontasks':
					url = Ext.String.format('/resolve/jsp/rsclient.jsp?url=/resolve/jsp/rsactiontask.jsp?sys_id={0}', sysId);
					break;
				case 'Documents':
				case 'Runbooks':
					url = Ext.String.format('/resolve/jsp/rsclient.jsp?url=/resolve/jsp/rswiki.jsp?wiki={0}', displayName);
					break;
				case 'Users':
					//Get current username from cookie
					var cookies = document.cookie.split('; '),
						currentUserName = '';
					Ext.each(cookies, function(cookie) {
						var split = cookie.split('=');
						if (split[0] == 'RSTOKEN') {
							currentUserName = split[1].split('/')[0].substring(1);
							return false;
						}
					});
					url = Ext.String.format('/resolve/jsp/rsclient.jsp?url=/resolve/jsp/rsprofile.jsp?main=Profile{0}', (displayName == currentUserName ? '' : '&username=' + displayName));
					break;
				case 'Process':
				case 'RSS':
				case 'Forums':
				case 'Teams':
					url = Ext.String.format('/resolve/jsp/rsclient.jsp?url=rsprofile.jsp?main={0}&sysId={1}', type, sysId);
					break;
			}

			if (url) window.open(url);
		}

	},
	toggleDisplayOnHome: function(menu, item, eventObj, eOpts) {
		var record = this.getCurrentlySelectedStream();

		if (record.get('systemStream')) return;

		Ext.Ajax.request({
			url: '/resolve/service/social/managefav',
			params: {
				isAdd: !menu.remove,
				displayName: record.get('displayName'),
				sysid: record.get('sys_id'),
				comptype: record.get('comptype')
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					//refresh the home streams
					var tabPanels = Ext.ComponentQuery.query('viewport > panel > tabpanel');
					var homeTab = tabPanels[0].items.items[0];
					var streamTree = homeTab.down('streamtree');
					if (streamTree) {
						streamTree.getStore().load();
					}
					menu.remove = !menu.remove;
					menu.setText(menu.remove ? 'Remove from Home' : 'Add to Home');
					RS.UserMessage.msg({
						success: true,
						title: 'Info',
						msg: response.data,
						duration: 4000,
						closeable: true
					});
				}
			}
		});
	},
	toggleStar: function(elId, postId) {
		var el = Ext.get(elId);
		var dataSpan = el.child('span');
		var isStarred = dataSpan.dom.innerHTML;

		Ext.Ajax.request({
			url: '/resolve/service/social/toggleStar',
			params: {
				postId: postId,
				isStarred: isStarred
			},
			success: function(r) {
				var response = RS.common.parsePayload(r);
				var starFlag = response.data || false;

				var imgEl = el.child('img');
				if (!starFlag) {
					imgEl.set({
						src: '../images/social/starOff.png'
					});
					dataSpan.update('false');
				} else {
					imgEl.set({
						src: '../images/social/starOn.png'
					});
					dataSpan.update('true');
				}
			}
		});
	},
	likeLinkOnClick: function(postId) {
		//Disable the like button
		var postDiv = Ext.get(postId),
			likeLink = postDiv.query('span[class=__likeLink__]'),
			likeLinkEl = Ext.get(likeLink[0]),
			match, result = "",
			regex = /<a.*>(.*?)<\/a>/ig;

		while (match = regex.exec(likeLinkEl.dom.innerHTML)) {
			result += match[1];
		}
		likeLinkEl.update(result);

		Ext.Ajax.request({
			url: '/resolve/service/social/likePost',
			params: {
				postId: postId
			},
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					//update the count
					var likeCount = response.data || 0,
						postDiv = Ext.get(postId),
						noOfPeopleLiked = postDiv.query('span[class=__noOfPeopleLiked__]'),
						noOfPeopleLikedEl = Ext.get(noOfPeopleLiked[0]),
						oldCount = Number(noOfPeopleLikedEl.dom.innerHTML);
					if (oldCount == 0) {
						//add in the link to properly display the popup
						var likeCountContainer = Ext.get(postDiv.query('span[class=likeCountContainer]')[0]);
						likeCountContainer.update(Ext.String.format('<a href="JavaScript:void(0);" onclick="displayLikeDialog(\'{0}\')"><span class="__noOfPeopleLiked__">{1}</span>&nbsp;&nbsp;<img src="../images/social/like.png" /></a>', postId, likeCount));
					} else {
						noOfPeopleLikedEl.update(likeCount);
					}
				}
			}
		});

	},
	submitComment: function(elId, postId) {

		var buttonEl = Ext.get(elId);
		var formEl = buttonEl.parent();
		var comment = formEl.child('textarea');
		var commentValue = comment.getValue();
		this.showedComment = false;
		Ext.Ajax.request({
			url: '/resolve/service/submitComment',
			params: {
				postId: postId,
				comment: commentValue
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					var newComment = commentTpl.applyTemplate(response.data);

					var postDiv = Ext.get(r.request.options.params.postId);
					var cDiv = postDiv.query("*[class=commentsCls]");
					var showHideCommentLinks = postDiv.query('.showHideComments');
					if (showHideCommentLinks.length > 0) {
						var showHideCommentLink = Ext.get(showHideCommentLinks[0]);
						showHideCommentLink.setVisible(true);
					}
					var cEl = Ext.get(cDiv[0]);
					cEl.insertHtml('beforeEnd', newComment);

					comment.dom.value = "";
					this.hideComment(postId);
				}
			}
		});
	},
	showComment: function(elId, _ui_id) {
		this.showedComment = true;
		var postDivs = Ext.query('[id=' + _ui_id + ']');
		var postDiv = null; //Ext.get(_ui_id);
		Ext.Array.each(postDivs, function(pDiv) {
			if (Ext.fly(pDiv).isVisible(true)) {
				postDiv = Ext.get(pDiv)
				return false
			}
		})
		if (postDiv) {
			var showHideCommentLinks = postDiv.query('.showHideComments');
			if (showHideCommentLinks.length > 0) {
				var showHideCommentLink = Ext.get(showHideCommentLinks[0]);
				showHideCommentLink.setVisible(true);
				toggleCommentDisplay(showHideCommentLinks[0], true);
			}

			//get the Post Div
			var cDiv = postDiv.query("*[class=cxnewcomment]");
			//grab the comments div
			var taDivEl = Ext.get(cDiv[0]);
			//convert it into an Element object
			taDivEl.show({
				duration: 500
			});
			//show it
			//stop the event propagation for this textarea as there are some issues otherwise
			var taArr = taDivEl.query('div textarea');
			//get the array of textarea, in this case there is only 1
			var elTA = Ext.get(taArr[0]);
			if (elTA) {
				//bring focus to the text area so users can immediately start typing
				elTA.focus();

				// IE, Chrome and Safari
				elTA.on('keydown', function(eventObj, htmlElementObj) {
					eventObj.stopPropagation();
				});

				// FF and Opera
				elTA.on('keypress', function(eventObj, htmlElementObj) {
					eventObj.stopPropagation();
				});
			}

			//**js for textarea -- in the textareaResizer.js file
			cleanForm();
		}
	},
	hideComment: function(sysId) {
		var postDiv = Ext.get(sysId);
		var cDiv = postDiv.query("*[class=cxnewcomment]");
		var taDivEl = Ext.get(cDiv[0]);
		taDivEl.setVisibilityMode(Ext.Element.DISPLAY);
		taDivEl.hide();
	},
	showPopupOnMouseOver: function(el, compType, sysId) {
		// console.log('mouse over event triggered - ' + compType + ' - ' + sysId);
		var viewport = Ext.getBody();
		var element = Ext.get(el);
		var compDisplayName = element.dom.innerHTML;

		//Cleanup the window here because the animations are sometimes still running for some reason when we're trying to close which causes errors
		if (this.popupWin) {
			this.popupWin.getEl().stopAnimation();
			this.popupWin.close();
			this.popupWin = null;
		}

		if (!this.popupWin) {
			var x = Math.round((element.getX() + element.getWidth() / 2)),
				y = element.getY() + element.getHeight();
			var height = 110,
				width = 300;
			//determine position of window to not go outside of visible area
			if (x + width > viewport.getWidth()) x = Math.round((element.getX() + element.getWidth() / 2)) - width;
			if (y + height > viewport.getHeight()) y = element.getY() - height;
			this.popupWin = Ext.create('Ext.window.Window', {
				layout: 'fit',
				border: false,
				width: width,
				height: height,
				style: 'border: 1px solid #888888',
				draggable: false,
				closable: false,
				preventHeader: true,
				baseCls: 'x-panel',
				resizable: false,
				x: x,
				y: y,
				compType: compType,
				compDisplayName: compDisplayName,
				sysId: sysId,
				anim: {
					from: {
						opacity: 0
					},
					duration: 500
				},
				listeners: {
					show: function(win) {
						win.getEl().fadeIn(win.anim);
						//win.getEl().shadow.el.fadeIn(win.anim);
						Ext.Ajax.request({
							url: '/resolve/service/social/getPopupData',
							params: {
								comptype: win.compType,
								compDisplayName: win.compDisplayName,
								sysid: win.sysId
							},
							scope: win,
							success: function(r) {
								var response = RS.common.parsePayload(r);
								if (response.success) {
									Ext.applyIf(response.data, {
										compType: this.compType,
										sysId: this.sysId
									});
									this.add(response.data);
								}
							}
						});
					},
					render: function(win) {
						win.mon(win.getEl(), {
							mouseout: {
								scope: win,
								fn: function(e, html, eOpts) {
									Ext.Function.defer(function() {
										perhapsHidePopupOnMouseOut(this.getEl(), e)
									}, 1000, this);
								}
							}
						})
					}
				}
			});
			this.popupWin.show();
		}
	},
	hidePopupOnMouseOut: function() {
		if (this.popupWin) {
			this.popupWin.getEl().fadeOut({
				duration: 500,
				scope: this.popupWin,
				callback: function() {
					this.hide();
				}
			});
		}
	},
	perhapsHidePopupOnMouseOut: function(el, e) {
		if (this.popupWin) {
			var e = Ext.isIE ? new Ext.EventObjectImpl(e) : Ext.EventObject;
			var win = this.popupWin;
			var position = win.getPosition(),
				winX = position[0],
				winY = position[1],
				height = win.getHeight(),
				width = win.getWidth();
			var eX = e.getPoint()[0],
				eY = e.getPoint()[1];
			if (eX <= winX || eX >= winX + width || eY <= winY - 5 || eY >= winY + height) {
				hidePopupOnMouseOut();
			}
		}
	},
	displayLikeDialog: function(sys_id) {
		if (!this.likeDialog) {
			this.likeDialog = Ext.create('RS.view.window.LikeDialog', {
				sys_id: sys_id,
				listeners: {
					close: {
						scope: this,
						fn: function(win) {
							this.likeDialog = null;
						}
					}
				}
			});
			this.likeDialog.show();
		}
	},
	followUser: function(sys_id, following) {
		Ext.Ajax.request({
			url: '/resolve/service/social/followUser',
			params: {
				sysid: sys_id,
				follow: following
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					//refresh the grid in the dialog
					if (this.likeDialog) {
						var grid = this.likeDialog.down('grid');
						if (grid) {
							grid.store.load();
						}
					}
				} else {
					//display error
				}
			}
		});
	},
	deletePost: function(postId) {
		this.deleteComment(postId, '');
	},
	deleteComment: function(postId, commentId) {
		Ext.Ajax.request({
			url: '/resolve/service/social/deletepost',
			params: {
				postsysid: postId,
				commentsysid: commentId
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					//Remove the post/comment element rather than refreshing the post panel information
					if (postId) {
						if (commentId) {
							//Remove comment
							Ext.get(commentId).remove();
						} else {
							//Removing post
							Ext.get(postId).remove();
						}
					}
					// Ext.Function.defer(function() {
					// 	this.refreshPostPanel();
					// }, 100, this);
				} else {
					//display error
				}
			}
		});
	},
	setPostToText: function(selectedName, comptype) {
		this.currentlySelectedName = selectedName;
		this.isForum = false;
		var targetLabel = this.getPostForm().down('displayfield');
		if (selectedName && selectedName != comptype) {
			targetLabel.setValue('<b>' + selectedName + '</b>');
		} else {
			//default to Blog
			targetLabel.setValue('<b>Blog</b>');
		}
	},

	displayLargeImage: function(url, displayName) {
		if (!this.imageWindow) {
			this.imageWindow = Ext.create('RS.view.control.ImageViewer', {
				src: url,
				title: displayName
			});
		} else {
			this.imageWindow.setTitle(displayName);
			this.imageWindow.setSrc(url);
		}
		this.imageWindow.show();
	},

	getCompToPost: function() {
		var params = {
			targetSysId: '__sysId__',
			//sysId of the currently selected item in the tree
			targetCompType: 'Blog' //comptype from selected item in tree
		};
		//Get the currently selected item in the tree and set the proper values
		var tabPanels = Ext.ComponentQuery.query('viewport > panel > tabpanel');
		if (tabPanels && tabPanels.length > 0) {
			var activeTab = tabPanels[0].getActiveTab();
			var streamTree = activeTab.down('streamtree');
			if (streamTree) {
				var selection = streamTree.getSelectionModel().getSelection();
				if (selection.length > 0) {
					if (!selection[0].data.systemStream) {
						params.targetCompType = selection[0].data.comptype;
						params.targetSysId = selection[0].data.sys_id;
					}
				}
			}
		} else {
			var panels = Ext.ComponentQuery.query('viewport > genericpostpanel');
			if (panels && panels.length > 0) {
				var selectionDetails = panels[0].selectionDetails;
				params.targetCompType = selectionDetails.comptype;
				params.targetSysId = selectionDetails.sys_id;
			}
		}
		return params;
	},

	submitPost: function() {
		var me = this;

		var params = this.getPostForm().getForm().getValues();
		Ext.apply(params, this.getCompToPost());

		if (me.isForum) {
			//really not posting, adding a comment
			var myMask = new Ext.LoadMask(this.getPostForm(), {
				msg: 'Commenting...'
			});
			myMask.show();
			Ext.Ajax.request({
				url: '/resolve/service/submitComment',
				params: {
					postId: this.currentForumPostId,
					comment: params.postContents
				},
				scope: this,
				success: function(r) {
					myMask.hide();
					var response = RS.common.parsePayload(r);
					if (response.success) {
						//Reset the individual fields so that we don't wipe out the 'posting to' text
						this.getPostForm().down('textarea').setValue('');
						this.getPostForm().down('hiddenfield').setValue('');
						this.refreshPostPanel();
					}
				}
			});
			return;
		}


		if (params.postContents) {
			//In case there is a delay posting something to the server (if a file is there or something), then display a "Posting..." message so the user knows what's going on.
			var myMask = new Ext.LoadMask(this.getPostForm(), {
				msg: 'Posting...'
			});
			myMask.show();
			Ext.Ajax.request({
				url: '/resolve/service/submitPost',
				params: params,
				scope: this,
				success: function(r) {
					myMask.hide();
					var response = RS.common.parsePayload(r);
					if (response.success) {
						//Reset the individual fields so that we don't wipe out the 'posting to' text
						this.getPostForm().down('textarea').setValue('');
						this.getPostForm().down('hiddenfield').setValue('');
						this.refreshPostPanel();
					}
				},
				failure: function() {
					myMask.hide();
				}
			});
		}
	},
	showFileUploadWindow: function() {
		Ext.widget('fileuploadwindow').show();
	},
	showLinkWindow: function() {
		Ext.widget('linkwindow').show();
	},
	showHtmlWindow: function() {
		Ext.widget('htmlwindow').show();
	},
	uploadFile: function() {
		var formToSubmit = this.getFileUploadForm().getForm();
		var values = formToSubmit.getValues();
		if (formToSubmit.isValid()) {
			formToSubmit.submit({
				clientValidation: true,
				waitMsg: 'Uploading your file...',
				params: {
					postid: values.postsysid
				},
				scope: this,
				success: function(form, action) {
					RS.UserMessage.msg({
						success: true,
						title: 'Success',
						msg: action.result.msg,
						duration: 4000,
						closeable: true
					});

					//update the post
					var textarea = this.getPostForm().down('textarea');
					var newValue = textarea.getValue() + ' ' + action.result.fileurl;
					textarea.setValue(newValue);

					//set the postid
					var postsysididHidden = this.getPostForm().down('hiddenfield');
					postsysididHidden.setValue(action.result.postid);

					//close the window
					var windowOwner = this.getFileUploadForm().up('window');
					windowOwner.close();
				},
				failure: function(form, action) {
					switch (action.failureType) {
						case Ext.form.action.Action.CLIENT_INVALID:
							RS.UserMessage.msg({
								success: false,
								title: 'Failure',
								msg: 'Form fields may not be submitted with invalid values',
								duration: 0,
								closeable: true
							});
							break;
						case Ext.form.action.Action.CONNECT_FAILURE:
							RS.UserMessage.msg({
								success: false,
								title: 'Failure',
								msg: 'Ajax communication failed',
								duration: 0,
								closeable: true
							});
							break;
						case Ext.form.action.Action.SERVER_INVALID:
							RS.UserMessage.msg({
								success: false,
								title: 'Failure',
								msg: action.result.msg,
								duration: 0,
								closeable: true
							});
							break;
					}

					var windowOwner = form.owner.ownerCt;
					windowOwner.close();
				}
			});
		}
	},
	rgSelectType: function(thisValue, newValue, oldValue, eOpts) {
		var values = thisValue.getValue();
		//making sure that it is a String and not an Array
		if (Ext.isString(values.type)) {
			this.getLinkFormTextfieldName().setValue('');
			//reset the name
			if (values.type == 'url' || values.type == 'worksheet') {
				//textfield
				this.getLinkFormTextfieldValue().show();
				this.getLinkFormCombobox().hide();
			} else {
				//combobox
				this.getLinkFormTextfieldValue().hide();
				this.getLinkFormCombobox().show();
			}
		}
	},
	tfNameOnFocus: function(thisField, opts) {

		thisField.setValue("");
		var cbValue = this.getLinkFormCombobox().getRawValue();
		var tfValue = this.getLinkFormTextfieldValue().getValue();
		var selectedRadio = this.getLinkFormRadioGroup().getValue().type;

		if (selectedRadio == 'url' || selectedRadio == 'worksheet') {
			if (tfValue) {
				thisField.setValue(tfValue);
			}
		} else {
			if (cbValue) {
				thisField.setValue(cbValue);
			}
		}

	},
	addLink: function() {
		var formToSubmit = this.getLinkForm().getForm();
		formToSubmit.isValid();
		//Do validation and mark the appropriate fields as invalid so the user knows what to correct
		var values = formToSubmit.getValues();
		var type = values.type;
		var tfValue = values.tfValue;
		var cbmodels = this.getLinkFormCombobox().valueModels;
		var model = null;
		var tfName = null;
		if (values.tfName) {
			tfName = values.tfName;
		} else {
			tfName = tfValue;
		}

		if (cbmodels) {
			model = cbmodels[0];
		}
		var newUrl = '';
		var baseUrl = '<a target="_blank" href="/resolve/service/social/showentity?showpost=false&type=' + type + '&sysId=';

		if (type.toLowerCase() == 'forum') {
			baseUrl = '<a target="_blank" href="/resolve/service/social/showentity?type=' + type + '&sysId=';
		}

		//console.log('This should add a link now');
		//prepare the url from the values
		if (values.type == 'url') {
			if (!tfValue || !tfName) {
				return;
			}
			var link = tfValue;
			if (tfValue.indexOf('http') == -1) {
				link = 'http://' + tfValue;
			}

			newUrl = '<a href="javascript:void(0)" onclick="window.open(\'' + link + '\',\'\',\'\')">';
			newUrl += tfName;
			newUrl += '</a>';
		} else if (values.type == 'worksheet') {
			if (!tfValue || !tfName) {
				return;
			}
			newUrl = baseUrl + '&name=' + tfValue + '">' + tfName + '</a>';
		} else if (values.type == 'document') {
			if (!tfName) {
				return;
			}
			var m_name = this.getLinkFormCombobox().getRawValue();
			var m_value = '';
			if (model) {
				m_name = model.get('name');
				m_value = model.get('value');
			}

			newUrl = baseUrl + m_value + '&name=' + m_name + '">' + tfName + '</a>';

		} else {
			if (!tfName) {
				return;
			}
			if (model) {
				var m_name = model.get('name');
				var m_value = model.get('value');

				/*newUrl = baseUrl + model.get('value') + '">' + model.get('name') + '</a>';*/
				newUrl = baseUrl + m_value + '&name=' + m_name + '">' + tfName + '</a>';
			} else {
				RS.UserMessage.msg({
					success: false,
					title: 'Alert',
					msg: 'This is not a valid selection. Please try again',
					duration: 10000,
					closeable: true
				});
			}
		}

		//update the post
		this.updatePostArea(newUrl);

		//close the window
		var windowOwner = this.getLinkForm().ownerCt;
		windowOwner.close();
	},
	addHtml: function() {
		var editor = this.getHtmlEditor();
		var htmlValue = editor.getValue();
		if (htmlValue) {
			//process html to make sure there isn't any links, and if there are make sure to have a target="_blank" for them
			//do a basic search and replace to deal with this
			htmlValue = htmlValue.replace(/<a href="/g, '<a target="_blank" href="');
			this.updatePostArea(htmlValue);
		}

		//close the window
		var windowOwner = editor.ownerCt;
		windowOwner.close();
	},
	updatePostArea: function(appendValue) {
		var textarea = this.getPostForm().down('textarea');
		var newValue = textarea.getValue();
		if (appendValue && appendValue.length > 0) {
			if (newValue) {
				newValue = newValue + ' ' + __htmlBegin__ + appendValue + __htmlEnd__;
			} else {
				newValue = __htmlBegin__ + appendValue + __htmlEnd__;
			}
		}
		textarea.setValue(newValue);
	},
	resetUploadForm: function() {
		this.getFileUploadForm().getForm().reset();
	},
	resetLinkForm: function() {
		this.getLinkForm().getForm().reset();
	},
	resetHtmlForm: function() {
		this.getHtmlEditor().reset();
	},
	onBeforeLoadOfLinkStore: function(store, operation, opts) {
		var selectedType = this.getLinkFormRadioGroup().getValue().type;
		var params = store.getProxy().extraParams;
		params.type = selectedType;

	},
	loadMentions: function(queryStr) {
		this.getMentionStoreStore().proxy.extraParams.query = queryStr;
		this.getMentionStoreStore().load();
	},
	showMoreForum: function(postId) {
		this.currentForumPostId = postId;
		this.isForum = true;
		this.getPostForm().setForum(true, this.currentlySelectedName);
		this.getStreamTabs().activeTab.down('streamtree').forumPermaLink = postId;
		this.refreshPostPanel();
	},
	returnForum: function() {
		this.isForum = false;
		this.getPostForm().setForum(false, this.currentlySelectedName);
		this.getStreamTabs().activeTab.down('streamtree').forumPermaLink = null;
		this.refreshPostPanel();
	}
});