/**
 * Store to load the suggested mention/hash tags for the user while they type in the post textarea
 */
Ext.define('RS.store.MentionStore', {
	extend : 'Ext.data.Store',
	requires : 'RS.model.ComboBoxModel',
	model : 'RS.model.ComboBoxModel',
	pageSize : 0,
	proxy : {
		type : 'ajax',
		url : '/resolve/service/social/getmentiondata',
		extraParams : {
			query : ''
		},
		reader : {
			type : 'json',
			root : 'records'
		},
		simpleSortMode : true
	}

});
