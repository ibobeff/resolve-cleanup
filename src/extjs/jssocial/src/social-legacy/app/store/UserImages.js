/**
 * Store to load the user image
 */
Ext.define('RS.store.UserImages', {
	extend : 'Ext.data.Store',
	requires : 'RS.model.UserImage',
	model : 'RS.model.UserImage',
	autoLoad : true
});
