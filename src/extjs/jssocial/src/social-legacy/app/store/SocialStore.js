/**
 * Store to load the posts for a particular stream 
 */
Ext.define('RS.store.SocialStore', {
	extend : 'Ext.data.Store',
	requires : 'RS.model.Post',
	model : 'RS.model.Post',
	pageSize : pageSizeForAllTabs(),
	remoteSort : true,
	buffered : true,
	proxy : {
		type : 'ajax',
		url : '/resolve/service/getPost',
		extraParams : {
			displayName : 'Blog',
			sysid : '__sysid__',
			comptype : 'Blog',
			gridItemClick : false,
			infiniteScroll : false,
			start : 0,
			limit : pageSizeForAllTabs()
		},
		reader : {
			type : 'json',
			root : 'records'
		},
		// sends single sort as multi parameter
		simpleSortMode : true
	}

});
