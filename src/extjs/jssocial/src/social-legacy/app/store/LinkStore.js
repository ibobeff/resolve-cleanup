/**
 * Store to get the suggestions for when the user adds a link to a post before submitting the post
 */
Ext.define('RS.store.LinkStore', {
	extend : 'Ext.data.Store',
	requires : 'RS.model.ComboBoxModel',
	model : 'RS.model.ComboBoxModel',
	pageSize : 10,
	remoteSort : true,
	proxy : {
		type : 'ajax',
		//url: 'data/linkdata.json',
		url : '/resolve/service/social/getentitydata',
		extraParams : {
			type : '',
			limit : 10
		},
		reader : {
			type : 'json',
			totalProperty : 'totalCount',
			root : 'records'
		},
		// sends single sort as multi parameter
		simpleSortMode : true
	}

});
