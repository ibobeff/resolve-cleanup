<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<jsp:useBean id="social" scope="request" class="com.resolve.services.graph.social.model.SocialUIPreferences" />
<%
	boolean debug = false;
	String d = request.getParameter("debug");
	if( d != null && d.indexOf("t") > -1){
		debug = true;
	}
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title id="page-title">Resolve Social</title>

	<link rel="stylesheet" type="text/css" href="../js/extjs4/resources/css/ext-all-gray.css">
    <link href="rssocialfont.css" rel="stylesheet" type="text/css">
	<link href="rssocial.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="../css/messagebox.css">
    
	<%
		if( debug ){
	%>
		<script type="text/javascript" src="../js/extjs4/ext-all-debug-w-comments.js"></script>
    <%
		}else{
	%>
		<script type="text/javascript" src="../js/extjs4/ext-all.js"></script>
	<%
		}
	%>
	<script type="text/javascript" src="textareaResizer.js"></script>
	<script type="text/javascript" src="rssocial.js"></script>
	
	<!-- Resolve Library -->
	<script type="text/javascript" src="../js/resolve.js"></script>
	
	<script type="text/javascript">
		social = {
			postAutoRefreshInterval: ${social.postAutoRefreshInterval},
			userAutoRefreshFlag: ${social.userAutoRefreshFlag},
			socialuserActionTaskTab: ${social.socialuserActionTaskTab},
			socialuserAdvanceTab: ${social.socialuserAdvanceTab},
			socialuserProcessTab: ${social.socialuserProcessTab},
			socialuserTeamTab: ${social.socialuserTeamTab},
			socialuserForumTab: ${social.socialuserForumTab},
			socialuserDocumentTab: ${social.socialuserDocumentTab},
			socialuserRunbookTab: ${social.socialuserRunbookTab},
			socialuserUserTab: ${social.socialuserUserTab},
			socialuserFeedTab: ${social.socialuserFeedTab},
			socialuserShowEastpanel: ${social.socialuserShowEastpanel},
			socialuserShowEastpanelWhatToDoWidget: ${social.socialuserShowEastpanelWhatToDoWidget},
			menuSets: '${social.menuSets}'
			<c:choose>
				<c:when test="${social.compFullName != null}">
					,compFullName: '${social.compFullName}'
					,compSysId: '${social.compSysId}'
				</c:when>
				<c:when test="${social.atFullName != null}">
					,atFullName: '${social.atFullName}'
					,atSysId: '${social.atSysId}'
				</c:when>
				<c:when test="${social.searchKey != null}">
	                ,searchKey: '${social.searchKey}'
	                ,searchTime: '${social.searchTime}'
	                ,queryType: '${social.queryType}'
					,resultsType: '${social.resultsType}'
	                ,doSearch: ${social.doSearch}
	            </c:when>
	            <c:when test="${social.permaLink != null}">
	                ,permaLink: '${social.permaLink}'
	            </c:when>
				<c:otherwise>				
				</c:otherwise>
			</c:choose>
			
			,compType: '${social.compType}'
			,hasAccessRights: ${social.hasAccessRights}
		};
	</script>

	<%
		if( debug ){
	%>
		
    <%
		}else{
	%>
    	<script type="text/javascript" src="social-classes.js"></script>
	<%
		}
	%>
	<c:choose>
        <c:when test="${!social.hasAccessRights}">
            <script type="text/javascript" src="appError.js"></script>
        </c:when>
		<c:when test="${social.compFullName != null}">
			<script type="text/javascript" src="appViewDocument.js"></script>
		</c:when>
		<c:when test="${social.atFullName != null}">
			<script type="text/javascript" src="appViewDocument.js"></script>
		</c:when>
		<c:when test="${social.searchKey != null}">
            <script type="text/javascript" src="appSearchPost.js"></script>
        </c:when>
        <c:when test="${social.permaLink != null}">
            <script type="text/javascript" src="appSearchPost.js"></script>
        </c:when>
		<c:otherwise>
			<script type="text/javascript" src="app.js"></script>
		</c:otherwise>
	</c:choose>
	
</head>
<body>
</body>
</html>

