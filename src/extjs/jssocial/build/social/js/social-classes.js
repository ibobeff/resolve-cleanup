/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
Ext.define('RS.social.UserStream', {
    extend: 'Ext.data.Model',
	fields: [
		'id', 'sys_id', 'name', 'type', 'username', 'componentNotification', 'displayName',
		{
			name: 'following',
			type: 'bool'
		}, {
			name: 'lock',
			type: 'bool'
		}, {
			name: 'currentUser',
			type: 'bool'
		}, {
			name: 'unReadCount',
			type: 'numeric'
		}, {
			name: 'isMe',
			type: 'bool'
		}, {
			name: 'pinned',
			type: 'bool'
		}, {
			name: 'sysUpdatedOn',
			type: 'date',
			format: 'time',
			dateFormat: 'time'
		}]
});
/* Deprecated feature
glu.defModel('RS.social.AttachFile', {
	editor: null,

	attach: function() {
		var name = '',
			splitName = [];
		Ext.Array.forEach(this.imagepickerSelections, function(selection) {
			name = selection.get('name')
			splitName = name.split('.')
			if (splitName.length > 0 && Ext.Array.indexOf(['jpg', 'jpeg', 'gif', 'png'], splitName[splitName.length - 1].toLowerCase()) > -1) {
				this.editor.insertAtCursor(Ext.String.format('<img src="{0}" fullData="{1}" style="width:200px"/>', '/resolve/service/social/download?preview=true&id=' + selection.get('sys_id'), '/resolve/service/social/download?id=' + selection.get('sys_id')))
			} else {
				this.editor.insertAtCursor(Ext.String.format('<a href="" fullData="/resolve/service/social/download?id={0}" dataType="{2}">{1}</a>', selection.get('sys_id'), name, splitName[splitName.length - 1].toLowerCase() == 'pdf' ? 'pdf' : ''))
			}
		}, this)
		this.doClose()
	},
	attachIsEnabled$: function() {
		if (this.imagepickerSelections.length > 0) {
			var status = true
			Ext.Array.forEach(this.imagepickerSelections, function(selection) {
				if (selection.get('status') != '' && selection.get('status') != 5) status = false
			})
			return status
		}
		return false
	},
	cancel: function() {
		this.doClose()
	},

	imagepickerSelections: [],

	selectionChanged: function(selected, eOpts) {
		this.set('imagepickerSelections', selected)
	},
	selectRecord: function(record, item, index, e, eOpts) {
		this.set('imagepickerSelections', [record])
		this.attach()
	}
})
*/
glu.defModel('RS.social.Component', {
	target: null,
	type: '',
	following: false,
	name: '',
	username: '',
	componentId: '',
	currentUser: false,

	isAuthor$: function() {
		return this.type.toLowerCase() == 'user'
	},

	description: '',
	uemail: '',
	uhomePhone: '',
	umobilePhone: '',
	csrftoken: '',
	fields: ['description', 'uemail', 'uhomePhone', 'umobilePhone'],

	viewText$: function() {
		return '<i class="icon-large icon-play stream-menu-icon" style="text-decoration:none"></i> ' + this.localize('view')
	},

	init: function() {
		this.initToken();
		if (this.isAuthor) {
			this.ajax({
				url: '/resolve/service/user/getUser',
				params: {
					username: this.name
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.loadData(response.data || {})
						this.set('componentDescription', this.componentDescription$())
					}
				}
			})
		} else {

		}
	},

	activate: function() {
		this.initToken();
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/user/downloadProfile', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this));
	},

	close: function() {
		if (this.parentVM.componentDisplayed) this.parentVM.set('componentDisplayed', false)
		if (this.parentVM.authorDisplayed) this.parentVM.set('authorDisplayed', false)
		this.doClose()
	},

	followText$: function() {
		return Ext.String.format('<span class="{0} like-follow-text" style="background:transparent !important;"></span><span class="like-follow-text rs-link" style="background:transparent !important;">{1}</span>', this.followIcon, this.following ? this.localize('unfollow') : this.localize('follow'))
	},
	followIcon$: function() {
		return this.following ? 'icon-large icon-reply like-following' : 'icon-large icon-plus like-follow'
	},
	toggleFollowing: function() {
		this.set('following', !this.following)
		this.ajax({
			url: '/resolve/service/social/setFollowStreams',
			params: {
				ids: [this.componentTarget.sysId],
				follow: this.following
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) this.parentVM.parentVM.refreshStreams()
				else clientVM.displayError(response.message)
			}
		})
	},

	componentDescription$: function() {
		if (this.isAuthor) {
			var description = Ext.String.format('<font cls="rs-menu-icon">{0}</font>', this.description || ''),
			image = Ext.String.format('<img style="width:46px;height:46px" src="/resolve/service/user/downloadProfile?username={0}&{1}"/>', this.username, this.csrftoken),
			email = Ext.String.format('{0}: {1}', this.localize('email'), this.uemail || ''),
			phone = Ext.String.format('{0}: {1}', this.localize('phone'), this.uhomePhone || ''),
			mobile = Ext.String.format('{0}: {1}', this.localize('mobile'), this.umobilePhone || '');
			return Ext.String.format('<table><tr><td colspan=2>{0}</td></tr><tr><td>{1}</td><td style="padding-left:10px">{2}</td></tr></table>', description, image, [email, phone, mobile].join('<br/>'))
		} else {
			this.getComponentDescription()
		}
	},

	getComponentDescription: function() {
		var name = Ext.String.format('{0}: {1}', this.localize('name'), this.componentTarget.name || ''),
			type = Ext.String.format('{0}: {1}', this.localize('type'), this.localize(this.componentTarget.type.toLowerCase()) || ''),
			description = Ext.String.format('{0}: {1}', this.localize('description'), this.componentTarget.description || '');
		return Ext.String.format('<table><tr><td colspan=2>{0}</td></tr><tr><td>{1}</td></tr><tr><td>{2}</td></tr></table>', name, type, description)
	},

	view: function() {
		var modelName = RS.social.ComponentMap[(this.componentTarget.type || 'user').toLowerCase()]
		if (modelName) {
			if (modelName == 'RS.wiki.WikiAdmin') {
				clientVM.handleNavigation({
					modelName: modelName,
					params: {
						ufullname: 'startsWith~' + this.sys_id,
						social: true
					}
				})
			} else if (modelName == 'RS.user.User') {
				clientVM.handleNavigation({
					modelName: modelName,
					params: {
						username: this.username
					}
				})
			} else
				clientVM.handleNavigation({
					modelName: modelName,
					params: {
						id: this.componentTarget.sysId,
						social: true
					}
				})
		} else
			this.message({
				title: this.localize('unknownComponentTitle'),
				msg: this.localize('unknownComponentMessage', [this.type.toLowerCase()]),
				buttons: Ext.MessageBox.OK
			});
		this.doClose();
	}
})
glu.defModel('RS.social.ConfigureNotifications', {

	activeItem: 0,

	selectAllProcess: false,
	PROCESS_CREATE: false,
	PROCESS_UPDATE: false,
	PROCESS_PURGED: false,
	PROCESS_RUNBOOK_ADDED: false,
	PROCESS_RUNBOOK_REMOVED: false,
	PROCESS_DOCUMENT_ADDED: false,
	PROCESS_DOCUMENT_REMOVED: false,
	PROCESS_ACTIONTASK_ADDED: false,
	PROCESS_ACTIONTASK_REMOVED: false,
	PROCESS_RSS_ADDED: false,
	PROCESS_RSS_REMOVED: false,
	PROCESS_FORUM_ADDED: false,
	PROCESS_FORUM_REMOVED: false,
	PROCESS_TEAM_ADDED: false,
	PROCESS_TEAM_REMOVED: false,
	PROCESS_USER_ADDED: false,
	PROCESS_USER_REMOVED: false,
	PROCESS_DIGEST_EMAIL: false,
	PROCESS_FORWARD_EMAIL: false,
	when_selectAllProcess_changes_to_true_select_all_process: {
		on: ['selectAllProcessChanged'],
		action: function() {
			if (this.selectAllProcess) {
				this.set('PROCESS_CREATE', true)
				this.set('PROCESS_UPDATE', true)
				this.set('PROCESS_PURGED', true)
				this.set('PROCESS_RUNBOOK_ADDED', true)
				this.set('PROCESS_RUNBOOK_REMOVED', true)
				this.set('PROCESS_DOCUMENT_ADDED', true)
				this.set('PROCESS_DOCUMENT_REMOVED', true)
				this.set('PROCESS_ACTIONTASK_ADDED', true)
				this.set('PROCESS_ACTIONTASK_REMOVED', true)
				this.set('PROCESS_RSS_ADDED', true)
				this.set('PROCESS_RSS_REMOVED', true)
				this.set('PROCESS_FORUM_ADDED', true)
				this.set('PROCESS_FORUM_REMOVED', true)
				this.set('PROCESS_TEAM_ADDED', true)
				this.set('PROCESS_TEAM_REMOVED', true)
				this.set('PROCESS_USER_ADDED', true)
				this.set('PROCESS_USER_REMOVED', true)
				this.set('PROCESS_DIGEST_EMAIL', true)
				this.set('PROCESS_FORWARD_EMAIL', true)
			}
		}
	},
	when_process_unselected_uncheck_select_all_box: {
		on: [
			'PROCESS_CREATEChanged',
			'PROCESS_UPDATEChanged',
			'PROCESS_PURGEDChanged',
			'PROCESS_RUNBOOK_ADDEDChanged',
			'PROCESS_RUNBOOK_REMOVEDChanged',
			'PROCESS_DOCUMENT_ADDEDChanged',
			'PROCESS_DOCUMENT_REMOVEDChanged',
			'PROCESS_ACTIONTASK_ADDEDChanged',
			'PROCESS_ACTIONTASK_REMOVEDChanged',
			'PROCESS_RSS_ADDEDChanged',
			'PROCESS_RSS_REMOVEDChanged',
			'PROCESS_FORUM_ADDEDChanged',
			'PROCESS_FORUM_REMOVEDChanged',
			'PROCESS_TEAM_ADDEDChanged',
			'PROCESS_TEAM_REMOVEDChanged',
			'PROCESS_USER_ADDEDChanged',
			'PROCESS_USER_REMOVEDChanged',
			'PROCESS_DIGEST_EMAILChanged',
			'PROCESS_FORWARD_EMAILChanged'
		],
		action: function() {
			if (this.PROCESS_CREATE &&
				this.PROCESS_UPDATE &&
				this.PROCESS_PURGED &&
				this.PROCESS_RUNBOOK_ADDED &&
				this.PROCESS_RUNBOOK_REMOVED &&
				this.PROCESS_DOCUMENT_ADDED &&
				this.PROCESS_DOCUMENT_REMOVED &&
				this.PROCESS_ACTIONTASK_ADDED &&
				this.PROCESS_ACTIONTASK_REMOVED &&
				this.PROCESS_RSS_ADDED &&
				this.PROCESS_RSS_REMOVED &&
				this.PROCESS_FORUM_ADDED &&
				this.PROCESS_FORUM_REMOVED &&
				this.PROCESS_TEAM_ADDED &&
				this.PROCESS_TEAM_REMOVED &&
				this.PROCESS_USER_ADDED &&
				this.PROCESS_USER_REMOVED &&
				this.PROCESS_DIGEST_EMAIL &&
				this.PROCESS_FORWARD_EMAIL) this.set('selectAllProcess', true)
			else {
				this.set('selectAllProcess', false)
			}
		}
	},

	selectAllTeam: false,
	TEAM_CREATE: false,
	TEAM_UPDATE: false,
	TEAM_PURGED: false,
	TEAM_ADDED_TO_PROCESS: false,
	TEAM_REMOVED_FROM_PROCESS: false,
	TEAM_ADDED_TO_TEAM: false,
	TEAM_REMOVED_FROM_TEAM: false,
	TEAM_USER_ADDED: false,
	TEAM_USER_REMOVED: false,
	TEAM_DIGEST_EMAIL: false,
	TEAM_FORWARD_EMAIL: false,

	when_selectAllTeam_changes_to_true_select_all_Team: {
		on: ['selectAllTeamChanged'],
		action: function() {
			if (this.selectAllTeam) {
				this.set('TEAM_CREATE', true)
				this.set('TEAM_UPDATE', true)
				this.set('TEAM_PURGED', true)
				this.set('TEAM_ADDED_TO_PROCESS', true)
				this.set('TEAM_REMOVED_FROM_PROCESS', true)
				this.set('TEAM_ADDED_TO_TEAM', true)
				this.set('TEAM_REMOVED_FROM_TEAM', true)
				this.set('TEAM_USER_ADDED', true)
				this.set('TEAM_USER_REMOVED', true)
				this.set('TEAM_DIGEST_EMAIL', true)
				this.set('TEAM_FORWARD_EMAIL', true)
			}
		}
	},
	when_team_unselected_uncheck_select_all_box: {
		on: [
			'TEAM_CREATEChanged',
			'TEAM_UPDATEChanged',
			'TEAM_PURGEDChanged',
			'TEAM_ADDED_TO_PROCESSChanged',
			'TEAM_REMOVED_FROM_PROCESSChanged',
			'TEAM_ADDED_TO_TEAMChanged',
			'TEAM_REMOVED_FROM_TEAMChanged',
			'TEAM_USER_ADDEDChanged',
			'TEAM_USER_REMOVEDChanged',
			'TEAM_DIGEST_EMAILChanged',
			'TEAM_FORWARD_EMAILChanged'
		],
		action: function() {
			if (this.TEAM_CREATE &&
				this.TEAM_UPDATE &&
				this.TEAM_PURGED &&
				this.TEAM_ADDED_TO_PROCESS &&
				this.TEAM_REMOVED_FROM_PROCESS &&
				this.TEAM_ADDED_TO_TEAM &&
				this.TEAM_REMOVED_FROM_TEAM &&
				this.TEAM_USER_ADDED &&
				this.TEAM_USER_REMOVED &&
				this.TEAM_DIGEST_EMAIL &&
				this.TEAM_FORWARD_EMAIL) this.set('selectAllTeam', true)
			else {
				this.set('selectAllTeam', false)
			}
		}
	},

	selectAllForum: false,
	FORUM_CREATE: false,
	FORUM_UPDATE: false,
	FORUM_PURGED: false,
	FORUM_ADDED_TO_PROCESS: false,
	FORUM_REMOVED_FROM_PROCESS: false,
	FORUM_USER_ADDED: false,
	FORUM_USER_REMOVED: false,
	FORUM_DIGEST_EMAIL: false,
	FORUM_FORWARD_EMAIL: false,

	when_selectAllForum_changes_to_true_select_all_Forum: {
		on: ['selectAllForumChanged'],
		action: function() {
			if (this.selectAllForum) {
				this.set('FORUM_CREATE', true)
				this.set('FORUM_UPDATE', true)
				this.set('FORUM_PURGED', true)
				this.set('FORUM_ADDED_TO_PROCESS', true)
				this.set('FORUM_REMOVED_FROM_PROCESS', true)
				this.set('FORUM_USER_ADDED', true)
				this.set('FORUM_USER_REMOVED', true)
				this.set('FORUM_DIGEST_EMAIL', true)
				this.set('FORUM_FORWARD_EMAIL', true)
			}
		}
	},
	when_forum_unselected_uncheck_select_all_box: {
		on: [
			'FORUM_CREATEChanged',
			'FORUM_UPDATEChanged',
			'FORUM_PURGEDChanged',
			'FORUM_ADDED_TO_PROCESSChanged',
			'FORUM_REMOVED_FROM_PROCESSChanged',
			'FORUM_USER_ADDEDChanged',
			'FORUM_USER_REMOVEDChanged',
			'FORUM_DIGEST_EMAILChanged',
			'FORUM_FORWARD_EMAILChanged'
		],
		action: function() {
			if (this.FORUM_CREATE &&
				this.FORUM_UPDATE &&
				this.FORUM_PURGED &&
				this.FORUM_ADDED_TO_PROCESS &&
				this.FORUM_REMOVED_FROM_PROCESS &&
				this.FORUM_USER_ADDED &&
				this.FORUM_USER_REMOVED &&
				this.FORUM_DIGEST_EMAIL &&
				this.FORUM_FORWARD_EMAIL) this.set('selectAllForum', true)
			else {
				this.set('selectAllForum', false)
			}
		}
	},

	selectAllUser: false,
	USER_ADDED_TO_PROCESS: false,
	USER_ADDED_TO_TEAM: false,
	USER_FOLLOW_ME: false,
	USER_REMOVED_FROM_PROCESS: false,
	USER_REMOVED_FROM_TEAM: false,
	USER_UNFOLLOW_ME: false,
	USER_ADDED_TO_FORUM: false,
	USER_REMOVED_FROM_FORUM: false,
	USER_PROFILE_CHANGE: false,
	USER_DIGEST_EMAIL: false,
	USER_FORWARD_EMAIL: false,

	when_selectAllUser_changes_to_true_select_all_User: {
		on: ['selectAllUserChanged'],
		action: function() {
			if (this.selectAllUser) {
				this.set('USER_ADDED_TO_PROCESS', true)
				this.set('USER_ADDED_TO_TEAM', true)
				this.set('USER_FOLLOW_ME', true)
				this.set('USER_REMOVED_FROM_PROCESS', true)
				this.set('USER_REMOVED_FROM_TEAM', true)
				this.set('USER_UNFOLLOW_ME', true)
				this.set('USER_ADDED_TO_FORUM', true)
				this.set('USER_REMOVED_FROM_FORUM', true)
				this.set('USER_PROFILE_CHANGE', true)
				this.set('USER_DIGEST_EMAIL', true)
				this.set('USER_FORWARD_EMAIL', true)
			}
		}
	},
	when_user_unselected_uncheck_select_all_box: {
		on: [
			'USER_ADDED_TO_PROCESSChanged',
			'USER_ADDED_TO_TEAMChanged',
			'USER_FOLLOW_MEChanged',
			'USER_REMOVED_FROM_PROCESSChanged',
			'USER_REMOVED_FROM_TEAMChanged',
			'USER_UNFOLLOW_MEChanged',
			'USER_ADDED_TO_FORUMChanged',
			'USER_REMOVED_FROM_FORUMChanged',
			'USER_PROFILE_CHANGEChanged',
			'USER_DIGEST_EMAILChanged',
			'USER_FORWARD_EMAILChanged'
		],
		action: function() {
			if (this.USER_ADDED_TO_PROCESS &&
				this.USER_ADDED_TO_TEAM &&
				this.USER_FOLLOW_ME &&
				this.USER_REMOVED_FROM_PROCESS &&
				this.USER_REMOVED_FROM_TEAM &&
				this.USER_UNFOLLOW_ME &&
				this.USER_ADDED_TO_FORUM &&
				this.USER_REMOVED_FROM_FORUM &&
				this.USER_PROFILE_CHANGE &&
				this.USER_DIGEST_EMAIL &&
				this.USER_FORWARD_EMAIL) this.set('selectAllUser', true)
			else {
				this.set('selectAllUser', false)
			}
		}
	},

	selectAllActionTask: false,
	ACTIONTASK_CREATE: false,
	ACTIONTASK_UPDATE: false,
	ACTIONTASK_PURGED: false,
	ACTIONTASK_ADDED_TO_PROCESS: false,
	ACTIONTASK_REMOVED_FROM_PROCESS: false,
	ACTIONTASK_DIGEST_EMAIL: false,
	ACTIONTASK_FORWARD_EMAIL: false,

	when_selectAllActionTask_changes_to_true_select_all_ActionTask: {
		on: ['selectAllActionTaskChanged'],
		action: function() {
			if (this.selectAllActionTask) {
				this.set('ACTIONTASK_CREATE', true)
				this.set('ACTIONTASK_UPDATE', true)
				this.set('ACTIONTASK_PURGED', true)
				this.set('ACTIONTASK_ADDED_TO_PROCESS', true)
				this.set('ACTIONTASK_REMOVED_FROM_PROCESS', true)
				this.set('ACTIONTASK_DIGEST_EMAIL', true)
				this.set('ACTIONTASK_FORWARD_EMAIL', true)
			}
		}
	},
	when_actiontask_unselected_uncheck_select_all_box: {
		on: [
			'ACTIONTASK_CREATEChanged',
			'ACTIONTASK_UPDATEChanged',
			'ACTIONTASK_PURGEDChanged',
			'ACTIONTASK_ADDED_TO_PROCESSChanged',
			'ACTIONTASK_REMOVED_FROM_PROCESSChanged',
			'ACTIONTASK_DIGEST_EMAILChanged',
			'ACTIONTASK_FORWARD_EMAILChanged'
		],
		action: function() {
			if (this.ACTIONTASK_CREATE &&
				this.ACTIONTASK_UPDATE &&
				this.ACTIONTASK_PURGED &&
				this.ACTIONTASK_ADDED_TO_PROCESS &&
				this.ACTIONTASK_REMOVED_FROM_PROCESS &&
				this.ACTIONTASK_DIGEST_EMAIL &&
				this.ACTIONTASK_FORWARD_EMAIL) this.set('selectAllActionTask', true)
			else {
				this.set('selectAllActionTask', false)
			}
		}
	},

	selectAllRunbook: false,
	RUNBOOK_UPDATE: false,
	RUNBOOK_ADDED_TO_PROCESS: false,
	RUNBOOK_REMOVED_FROM_PROCESS: false,
	RUNBOOK_COMMIT: false,
	RUNBOOK_DIGEST_EMAIL: false,
	RUNBOOK_FORWARD_EMAIL: false,

	when_selectAllRunbook_changes_to_true_select_all_Runbook: {
		on: ['selectAllRunbookChanged'],
		action: function() {
			if (this.selectAllRunbook) {
				this.set('RUNBOOK_UPDATE', true)
				this.set('RUNBOOK_ADDED_TO_PROCESS', true)
				this.set('RUNBOOK_REMOVED_FROM_PROCESS', true)
				this.set('RUNBOOK_COMMIT', true)
				this.set('RUNBOOK_DIGEST_EMAIL', true)
				this.set('RUNBOOK_FORWARD_EMAIL', true)
			}
		}
	},
	when_runbook_unselected_uncheck_select_all_box: {
		on: [
			'RUNBOOK_UPDATEChanged',
			'RUNBOOK_ADDED_TO_PROCESSChanged',
			'RUNBOOK_REMOVED_FROM_PROCESSChanged',
			'RUNBOOK_COMMITChanged',
			'RUNBOOK_DIGEST_EMAILChanged',
			'RUNBOOK_FORWARD_EMAILChanged'
		],
		action: function() {
			if (this.RUNBOOK_UPDATE &&
				this.RUNBOOK_ADDED_TO_PROCESS &&
				this.RUNBOOK_REMOVED_FROM_PROCESS &&
				this.RUNBOOK_COMMIT &&
				this.RUNBOOK_DIGEST_EMAIL &&
				this.RUNBOOK_FORWARD_EMAIL) this.set('selectAllRunbook', true)
			else {
				this.set('selectAllRunbook', false)
			}
		}
	},

	selectAllDocument: false,
	DOCUMENT_CREATE: false,
	DOCUMENT_UPDATE: false,
	DOCUMENT_DELETE: false,
	DOCUMENT_PURGED: false,
	DOCUMENT_ADDED_TO_PROCESS: false,
	DOCUMENT_REMOVED_FROM_PROCESS: false,
	DOCUMENT_COMMIT: false,
	DOCUMENT_DIGEST_EMAIL: false,
	DOCUMENT_FORWARD_EMAIL: false,

	when_selectAllDocument_changes_to_true_select_all_Document: {
		on: ['selectAllDocumentChanged'],
		action: function() {
			if (this.selectAllDocument) {
				this.set('DOCUMENT_CREATE', true)
				this.set('DOCUMENT_UPDATE', true)
				this.set('DOCUMENT_DELETE', true)
				this.set('DOCUMENT_PURGED', true)
				this.set('DOCUMENT_ADDED_TO_PROCESS', true)
				this.set('DOCUMENT_REMOVED_FROM_PROCESS', true)
				this.set('DOCUMENT_COMMIT', true)
				this.set('DOCUMENT_DIGEST_EMAIL', true)
				this.set('DOCUMENT_FORWARD_EMAIL', true)
			}
		}
	},
	when_document_unselected_uncheck_select_all_box: {
		on: [
			'DOCUMENT_CREATEChanged',
			'DOCUMENT_UPDATEChanged',
			'DOCUMENT_DELETEChanged',
			'DOCUMENT_PURGEDChanged',
			'DOCUMENT_ADDED_TO_PROCESSChanged',
			'DOCUMENT_REMOVED_FROM_PROCESSChanged',
			'DOCUMENT_COMMITChanged',
			'DOCUMENT_DIGEST_EMAILChanged',
			'DOCUMENT_FORWARD_EMAILChanged'
		],
		action: function() {
			if (this.DOCUMENT_CREATE &&
				this.DOCUMENT_UPDATE &&
				this.DOCUMENT_DELETE &&
				this.DOCUMENT_PURGED &&
				this.DOCUMENT_ADDED_TO_PROCESS &&
				this.DOCUMENT_REMOVED_FROM_PROCESS &&
				this.DOCUMENT_COMMIT &&
				this.DOCUMENT_DIGEST_EMAIL &&
				this.DOCUMENT_FORWARD_EMAIL) this.set('selectAllDocument', true)
			else {
				this.set('selectAllDocument', false)
			}
		}
	},

	selectAllRSS: false,
	RSS_CREATE: false,
	RSS_UPDATE: false,
	RSS_PURGED: false,
	RSS_ADDED_TO_PROCESS: false,
	RSS_REMOVED_FROM_PROCESS: false,
	RSS_DIGEST_EMAIL: false,
	RSS_FORWARD_EMAIL: false,

	when_selectAllRSS_changes_to_true_select_all_RSS: {
		on: ['selectAllRSSChanged'],
		action: function() {
			if (this.selectAllRSS) {
				this.set('RSS_CREATE', true)
				this.set('RSS_UPDATE', true)
				this.set('RSS_PURGED', true)
				this.set('RSS_ADDED_TO_PROCESS', true)
				this.set('RSS_REMOVED_FROM_PROCESS', true)
				this.set('RSS_DIGEST_EMAIL', true)
				this.set('RSS_FORWARD_EMAIL', true)
			}
		}
	},
	when_rss_unselected_uncheck_select_all_box: {
		on: [
			'RSS_CREATEChanged',
			'RSS_UPDATEChanged',
			'RSS_PURGEDChanged',
			'RSS_ADDED_TO_PROCESSChanged',
			'RSS_REMOVED_FROM_PROCESSChanged',
			'RSS_DIGEST_EMAILChanged',
			'RSS_FORWARD_EMAILChanged'
		],
		action: function() {
			if (this.RSS_CREATE &&
				this.RSS_UPDATE &&
				this.RSS_PURGED &&
				this.RSS_ADDED_TO_PROCESS &&
				this.RSS_REMOVED_FROM_PROCESS &&
				this.RSS_DIGEST_EMAIL &&
				this.RSS_FORWARD_EMAIL) this.set('selectAllRSS', true)
			else {
				this.set('selectAllRSS', false)
			}
		}
	},

	fields: [
		'PROCESS_CREATE',
		'PROCESS_UPDATE',
		'PROCESS_PURGED',
		'PROCESS_RUNBOOK_ADDED',
		'PROCESS_RUNBOOK_REMOVED',
		'PROCESS_DOCUMENT_ADDED',
		'PROCESS_DOCUMENT_REMOVED',
		'PROCESS_ACTIONTASK_ADDED',
		'PROCESS_ACTIONTASK_REMOVED',
		'PROCESS_RSS_ADDED',
		'PROCESS_RSS_REMOVED',
		'PROCESS_FORUM_ADDED',
		'PROCESS_FORUM_REMOVED',
		'PROCESS_TEAM_ADDED',
		'PROCESS_TEAM_REMOVED',
		'PROCESS_USER_ADDED',
		'PROCESS_USER_REMOVED',
		'PROCESS_DIGEST_EMAIL',
		'PROCESS_FORWARD_EMAIL',
		'TEAM_CREATE',
		'TEAM_UPDATE',
		'TEAM_PURGED',
		'TEAM_ADDED_TO_PROCESS',
		'TEAM_REMOVED_FROM_PROCESS',
		'TEAM_ADDED_TO_TEAM',
		'TEAM_REMOVED_FROM_TEAM',
		'TEAM_USER_ADDED',
		'TEAM_USER_REMOVED',
		'TEAM_DIGEST_EMAIL',
		'TEAM_FORWARD_EMAIL',
		'FORUM_CREATE',
		'FORUM_UPDATE',
		'FORUM_PURGED',
		'FORUM_ADDED_TO_PROCESS',
		'FORUM_REMOVED_FROM_PROCESS',
		'FORUM_USER_ADDED',
		'FORUM_USER_REMOVED',
		'FORUM_DIGEST_EMAIL',
		'FORUM_FORWARD_EMAIL',
		'USER_ADDED_TO_PROCESS',
		'USER_ADDED_TO_TEAM',
		'USER_FOLLOW_ME',
		'USER_REMOVED_FROM_PROCESS',
		'USER_REMOVED_FROM_TEAM',
		'USER_UNFOLLOW_ME',
		'USER_ADDED_TO_FORUM',
		'USER_REMOVED_FROM_FORUM',
		'USER_PROFILE_CHANGE',
		'USER_DIGEST_EMAIL',
		'USER_FORWARD_EMAIL',
		'ACTIONTASK_CREATE',
		'ACTIONTASK_UPDATE',
		'ACTIONTASK_PURGED',
		'ACTIONTASK_ADDED_TO_PROCESS',
		'ACTIONTASK_REMOVED_FROM_PROCESS',
		'ACTIONTASK_DIGEST_EMAIL',
		'ACTIONTASK_FORWARD_EMAIL',
		'RUNBOOK_UPDATE',
		'RUNBOOK_ADDED_TO_PROCESS',
		'RUNBOOK_REMOVED_FROM_PROCESS',
		'RUNBOOK_COMMIT',
		'RUNBOOK_DIGEST_EMAIL',
		'RUNBOOK_FORWARD_EMAIL',
		'DOCUMENT_CREATE',
		'DOCUMENT_UPDATE',
		'DOCUMENT_DELETE',
		'DOCUMENT_PURGED',
		'DOCUMENT_ADDED_TO_PROCESS',
		'DOCUMENT_REMOVED_FROM_PROCESS',
		'DOCUMENT_COMMIT',
		'DOCUMENT_DIGEST_EMAIL',
		'DOCUMENT_FORWARD_EMAIL',
		'RSS_CREATE',
		'RSS_UPDATE',
		'RSS_PURGED',
		'RSS_ADDED_TO_PROCESS',
		'RSS_REMOVED_FROM_PROCESS',
		'RSS_DIGEST_EMAIL',
		'RSS_FORWARD_EMAIL'
	],

	typeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	type: '',

	when_type_changes_update_active_card: {
		on: ['typeChanged'],
		action: function() {
			switch (this.type) {
				case 'PROCESS':
					this.set('activeItem', 0)
					break;
				case 'TEAM':
					this.set('activeItem', 1)
					break;
				case 'FORUM':
					this.set('activeItem', 2)
					break;
				case 'USER':
					this.set('activeItem', 3)
					break;
				case 'ACTIONTASK':
					this.set('activeItem', 4)
					break;
				case 'RUNBOOK':
					this.set('activeItem', 5)
					break;
				case 'DOCUMENT':
					this.set('activeItem', 6)
					break;
				case 'DECISIONTREES':
					this.set('activeItem', 7)
					break;
				case 'RSS':
					this.set('activeItem', 8)
					break;
			}
		}
	},

	init: function() {
		this.loadNotifications()
	},

	loadNotifications: function() {
		this.ajax({
			url: '/resolve/service/social/global/notify/get',
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					Ext.Array.forEach(response.records || [], function(record) {
						if (record.notifyTypes.length > 0)
							this.typeStore.add({
								name: this.localize(record.compType),
								value: record.compType
							})
						Ext.Array.forEach(record.notifyTypes || [], function(type) {
							if (Ext.isDefined(this[type.type]))
								this.set(type.type, type.value === 'true' || type.value === true)
						}, this)
					}, this)

					if (this.typeStore.getCount() > 0) this.set('type', this.typeStore.getAt(0).get('value'))
				}
			}
		})
	},

	save: function() {
		var params = this.asObject(),
			selectedTypes = ['blank'];
		Ext.Object.each(params, function(param) {
			if (params[param] == 'true') selectedTypes.push(param)
		})

		this.ajax({
			url: '/resolve/service/social/global/notify/update',
			params: {
				selectedTypes: selectedTypes
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					displaySuccess(this.localize('notficationsSaved'))
					this.doClose()
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	cancel: function() {
		this.doClose()
	}
})
/* Deprecated feature
glu.defModel('RS.social.CreateLink', {

	editor: null,

	link: 'url',
	display: '',

	height$: function() {
		return this.link == 'component' ? 500 : 225
	},

	when_input_changes_update_display: {
		on: ['urlChanged'],
		action: function(value) {
			if (Ext.String.startsWith(value, this.display, true)) this.set('display', value)
		}
	},

	url: 'http://',
	urlIsVisible$: function() {
		return this.link == 'url'
	},

	searchQuery: '',
	search: function() {
		this.toGrid.load()
	},
	type: '',
	typeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listComponentTypes',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	toGridIsVisible$: function() {
		return this.link == 'component'
	},
	toGrid: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['name'],
		fields: ['id', 'name', 'type', 'username'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listAllStreams',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	when_filter_changes_reload_grid: {
		on: ['typeChanged', 'searchQueryChanged'],
		action: function() {
			this.toGrid.load()
		}
	},

	toGridSelections: [],

	lastSelections: [],
	when_selection_changes_update_text: {
		on: ['toGridSelectionsChanged'],
		action: function() {
			if (!this.display || this.lastSelections.length > 0 && (this.display == this.lastSelections[0].get('name'))) this.set('display', this.toGridSelections[0].get('name'))
			this.set('lastSelections', this.toGridSelections)
		}
	},

	toGridColumns: [{
		dataIndex: 'name',
		header: '~~name~~',
		flex: 1
	}, {
		dataIndex: 'type',
		header: '~~type~~'
	}],

	componentTypes: {
		mtype: 'list',
		autoParent: true
	},

	init: function() {
		this.componentTypes.add({
			mtype: 'RS.social.ComponentType',
			name: this.localize('url'),
			inputValue: 'url',
			value: true
		})
		this.componentTypes.add({
			mtype: 'RS.social.ComponentType',
			name: this.localize('component'),
			inputValue: 'component'
		})

		this.toGrid.on('beforeload', function(store, operation, eOpts) {
			var filters = [];

			if (this.type) filters.push({
				field: 'type',
				condition: 'equals',
				value: this.type
			})

			if (this.search) filters.push({
				field: 'name',
				condition: 'startsWith',
				value: this.searchQuery
			})

			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				filter: Ext.encode(filters)
			})
		}, this)


		this.typeStore.load()

		if (!this.type) this.typeStore.on('load', function(store, records) {
			if (records.length > 0) this.set('type', records[0].get('value'))
		}, this, {
			single: true
		})
		else this.toGrid.load()
	},

	hrefLink$: function() {
		if (this.link == 'url') return this.url
		if (this.link == 'component' && this.toGridSelections.length > 0) {
			var type = this.toGridSelections[0].get('type').toLowerCase();
			return Ext.String.format('#{0}/{1}={2}', (RS.social.ComponentMap[type.toLowerCase()] || ''), type == 'user' ? 'username' : 'id', type == 'user' ? this.toGridSelections[0].get('username') : this.toGridSelections[0].get('id'))
		}
	},

	createLink: function() {
		this.editor.insertAtCursor(Ext.String.format('<a href="{0}" target="_blank">{1}</a>', this.hrefLink, this.display))
		this.doClose()
	},

	createLinkIsEnabled$: function() {
		return this.display && (this.link == 'component' ? this.toGridSelections.length == 1 : this.url)
	},

	cancel: function() {
		this.doClose()
	}

})

glu.defModel('RS.social.ComponentType', {
	name: '',
	inputValue: '',
	value: false,
	when_value_changes_update_parent: {
		on: ['valueChanged'],
		action: function() {
			if (this.value) this.parentVM.set('link', this.inputValue)
		}
	}
})
*/

glu.defModel('RS.social.Followers', {
	target: null,

	streamId: '',

	removeFollowers: true,
	addFollowers: false,
	buttonWindow: false,
	csrftoken: '',
	baseCls$: function() {
		return this.buttonWindow && !Ext.isIE8m ? 'x-panel' : 'x-window'
	},
	ui$: function() {
		return this.buttonWindow && !Ext.isIE8m ? 'sysinfo-dialog' : 'default'
	},
	draggable$: function() {
		return !this.buttonWindow
	},

	followers: {
		mtype: 'list',
		autoParent: true
	},

	containsMe: false,

	init: function() {
		this.initToken();
		this.loadFollowers()
	},

	activate: function() {
		this.initToken();
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/user/downloadProfile', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this))
	},

	loadFollowers: function() {
		if (this.streamId || this.parentVM.id) {
			this.followers.removeAll();
			var followersModel = this;
			this.ajax({
				url: '/resolve/service/social/listFollowers',
				params: {
					streamId: this.streamId || this.parentVM.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.set('containsMe', false)
						Ext.Array.forEach(response.records || [], function(record) {
							if (record.currentUser || record.id == clientVM.user.id) this.set('containsMe', true)
							var me = this;
							this.followers.add(Ext.apply({
								mtype: 'viewmodel',
								followText$: function() {
									if (me.removeFollowers)
										return Ext.String.format('<span style="color:red" class="icon-large icon-remove like-follow-text"></span>&nbsp;<span class="like-follow-text">{0}</span>', this.parentVM.localize('remove'))
									return this.currentUser || this.id == clientVM.user.id ? this.parentVM.localize('you') : Ext.String.format('<span class="{0} like-follow-text"></span><span class="like-follow-text">{1}</span>', this.followIcon, this.following ? this.localize('unfollow') : this.localize('follow'))
								},
								followIcon$: function() {
									return this.following ? 'icon-large icon-reply like-following' : 'icon-large icon-plus like-follow'
								},
								followerProfileImage$: function() {
									return followersModel.csrftoken ? '/resolve/service/user/downloadProfile?username=' + this.username + '&' + followersModel.csrftoken : '';
								},
								toggleFollowing: function() {
									if (me.removeFollowers) {
										this.ajax({
											url: '/resolve/service/social/setFollowers',
											params: {
												streamIds: [this.parentVM.streamId || this.parentVM.parentVM.id],
												ids: [this.id],
												follow: false
											},
											success: function(r) {
												var response = RS.common.parsePayload(r)
												if (response.success) {
													this.parentVM.loadFollowers()
													if (this.rootVM.refreshStreams) this.rootVM.refreshStreams()
												} else clientVM.displayError(response.message)
											}
										})
									} else {
										this.set('following', !this.following)
										this.ajax({
											url: '/resolve/service/social/setFollowStreams',
											params: {
												ids: [this.id],
												follow: this.following
											},
											success: function(r) {
												var response = RS.common.parsePayload(r)
												if (response.success) {
													this.parentVM.loadFollowers()
													if (this.rootVM.refreshStreams) this.rootVM.refreshStreams()
												} else clientVM.displayError(response.message)
											}
										})
									}
								}
							}, record))
						}, this)

						if (this.followers.length == 0) {
							this.followers.add({
								mtype: 'viewmodel',
								name: this.localize('noFollowers'),
								followText$: function() {
									return ''
								},
								followIcon$: function() {
									return ''
								},
								followerProfileImage$: function() {
									return ''
								},
								toggleFollowing: function() {}
							})
						}
					} else clientVM.displayError(response.message)
				}
			})
		}
	},

	follow: function() {
		this.ajax({
			url: '/resolve/service/social/setFollowStreams',
			params: {
				ids: [this.streamId || this.parentVM.id],
				streamType: this.streamType || this.parentVM.streamType,
				follow: true
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				this.loadFollowers()
			}
		})
	},
	followIsVisible$: function() {
		return this.addFollowers && !this.containsMe
	},
	unfollow: function() {
		this.ajax({
			url: '/resolve/service/social/setFollowStreams',
			params: {
				ids: [this.streamId || this.parentVM.id],
				follow: false
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				this.loadFollowers()
			}
		})
	},
	unfollowIsVisible$: function() {
		return this.addFollowers && this.containsMe
	},
	addMoreFollowers: function() {
		var model = glu.model({
			mtype: 'RS.social.StreamPicker',
			pickerType: 2,
			streamId: this.streamId || this.parentVM.id,
			streamType: this.streamType || this.parentVM.streamType
		})
		model.init()
		glu.openWindow(model)
		this.doClose()
	},
	addMoreFollowersIsVisible$: function() {
		return this.addFollowers
	}
})
glu.defModel('RS.social.Likes', {
	target: null,
	csrftoken: '',

	likes: {
		mtype: 'list',
		autoParent: true
	},

	init: function() {
		this.initToken();
		if (this.parentVM.id) this.ajax({
			url: '/resolve/service/social/listLikes',
			params: {
				id: this.parentVM.id
			},
			success: function(r) {
				var response = RS.common.parsePayload(r),
					streamStore = this.parentVM.parentVM.streamStore || this.parentVM.parentVM.parentVM.streamStore;
				if (response.success) Ext.Array.forEach(response.records, function(record) {
					if (record.username == this.parentVM.username) record.currentUser = true
					record.following = false
					streamStore.each(function(stream) {
						if (stream.get('username') == record.username) {
							record.following = true
							return false
						}
					})
					this.likes.add(Ext.apply({
						mtype: 'viewmodel',
						followText$: function() {
							return this.currentUser ? this.parentVM.localize('you') : Ext.String.format('<span class="{0} like-follow-text"></span><span class="like-follow-text">{1}</span>', this.followIcon, this.following ? this.localize('unfollow') : this.localize('follow'))
						},
						followIcon$: function() {
							return this.following ? 'icon-large icon-reply like-following' : 'icon-large icon-plus like-follow'
						},
						likeProfileImage$: function() {
							return this.csrftoken ? '/resolve/service/user/downloadProfile?username=' + this.username + '&' + this.csrftoken : '';
						},
						displayUser: function() {
							clientVM.handleNavigation({
								modelName: 'RS.user.User',
								target: '_blank',
								params: {
									username: this.username
								}
							})
						},
						toggleFollowing: function() {
							this.set('following', !this.following)
							this.ajax({
								url: '/resolve/service/social/setFollowStreams',
								params: {
									ids: [this.id],
									follow: this.following
								},
								success: function(r) {
									var response = RS.common.parsePayload(r)
									if (response.success) this.rootVM.refreshStreams()
									else clientVM.displayError(response.message)
								}
							})
						}
					}, record))
				}, this)
				else clientVM.displayError(response.message)
			}
		})
	},

	activate: function() {
		this.initToken();
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/user/downloadProfile', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this));
	},

	close: function() {
		this.parentVM.set('likesDisplayed', false)
		this.doClose()
	}
})
glu.defModel('RS.social.Main', {

	mock: false,

	active: false,

	postId: '',

	displayPostProfilePictures: true,
	displayCommentProfilePictures: true,

	miniMode: false,

	mode: '',
	id: '',

	defaultItemsPerPage: 10,

	isNews$: function() {
		return this.mode == 'news'
	},

	isEmbed$: function() {
		return this.mode == 'embed'
	},

	showStreams$: function() {
		return !this.isNews && !this.isEmbed
	},

	activate: function(screen, params) {
		this.set('active', true)
		this.set('mode', params ? params.mode : '')
		if (params && params.streamId) this.setInitialStream(params.streamId)
		if (params && params.streamParent) this.set('streamParent', params.streamParent)
		this.updateScreen()
	},

	deactivate: function() {
		this.set('active', false)
	},

	checkNavigateAway: function() {
		this.set('active', false)
	},



	groups: {
		mtype: 'list',
		add: function(streamGroup) {
			// We display DOCUMENT and ACTIONTASK at the end
			if(streamGroup.type === 'DOCUMENT') {
				(this.length > 0 && this.getAt(this.length-1).type === 'ACTIONTASK')
					? this.insert(this.length-1, streamGroup)
					: this.insert(this.length, streamGroup);
			} else if(streamGroup.type === 'ACTIONTASK') {
				// Always insert at the end
				this.insert(this.length, streamGroup);
			} else {
				// Insert before Document or ActionTask. If none exists, insert at the end
				var i;
				for (i=0; i<this.length; i++) {
					var type = this.getAt(i).type;
					if (type === 'DOCUMENT' || type == 'ACTIONTASK') {
						break;
					}
				}
				this.insert(i, streamGroup);
			}
		}
	},

	streamToStore: {
		mtype: 'store',
		fields: ['id', 'name', 'type', 'displayName'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	streamStore: {
		mtype: 'store',
		model: 'RS.social.UserStream',
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listUserStreams',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	documentStreamStore : {
		mtype: 'store',
		model: 'RS.social.UserStream',
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listDocumentUserStreams',
			reader: {
				type: 'json',
				root: 'records',
				totalProperty : 'total'
			}
		},
		supportPaging: true
	},

	actionTaskStreamStore : {
		mtype: 'store',
		model: 'RS.social.UserStream',
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listActionTaskUserStreams',
			reader: {
				type: 'json',
				root: 'records',
				totalProperty : 'total'
			}
		},
		supportPaging: true
	},

	pageSize: 10,
	pageNumber: 1,
	when_pageNumber_changes_reload_posts: {
		on: ['pageNumberChanged'],
		action: function() {
			this.postFilterStore.load()
		}
	},
	totalCount: 0,

	postFilterStore: {
		remoteSort: true,
		mtype: 'store',
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		fields: [
			'id',
			'content',
			'title',
			'author',
			'sysUpdatedOn'
		],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listPosts',
			reader: {
				type: 'json',
				root: 'records',
				totalProperty: 'total'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
				
			}
		}
	},
	posts: {
		mtype: 'list'
	},

	switchingToPost: false,

	postYourMessageDisplay$: function() {
		if (this.streamParent) return this.localize('postYourMessageDisplay', this.streamParent)
		if (this.activeStream) {
			if (this.streamToStore.findExact('id', this.activeStream.sys_id) > -1)
				return this.localize('postYourMessageDisplay', this.activeStream.name)
			return this.localize('postYourMessageDisplay', this.localize('blog'))
		}
		return this.localize('postYourMessage')
	},

	activeStream: null,
	when_active_stream_changes_update_posts: {
		on: ['activeStreamChanged'],
		action: function() {
			//clear out any initial stream id too
			this.set('initialStreamId', '')
			this.posts.removeAll()
			this.set('pageNumber', 1)
			this.set('postId', '')
			this.postFilterStore.load()
			this.updateWindowTitle()
			this.updateMini()
			if (this.activeStream.lock) this.switchToFilter()
			else if (!this.switchingToPost) {
				this.set('activePostCard', 0)
			}
			this.set('switchingToPost', false)
			this.switchToPostDisplay()
		}
	},
	activePostCard: 0,

	userNameText: '',

	poller : null,
	pollInterval: 30,
	initialStreamId: '',
	initialStreamType: '',

	clientVM: null,
	authenticated: false,

	unreadOnly: false,
	when_unreadOnly_changes_update_posts: {
		on: 'unreadOnlyChanged',
		action: function() {
			this.postFilterStore.load()
		}
	},

	oneDayOld: false,
	oneWeekOld: false,

	when_postOld_changes_update_posts: {
		on: ['oneDayOldChanged'],
		action: function() {
			if (this.oneDayOld) {
				this.set('oneWeekOld', false)
			}
			this.postFilterStore.load()
		}
	},

	when_oneWeekOld_changes: {
		on: ['oneWeekOldChanged'],
		action: function() {
			if (this.oneWeekOld) {
				this.set('oneDayOld', false)
			}
			this.postFilterStore.load()
		}
	},

	postFilterStoreLoadingCount: 0,

	startPolling: function() {
		if (this.poller)
			this.poller.destroy();
		if (this.pollInterval < 5)
			this.set('pollInterval', 5);
		var resultRunner = new Ext.util.TaskRunner();	
		var task = resultRunner.newTask({
			run: function() {
				if (this.autoRefreshPosts)
					this.updateScreen();
			},
			scope: this,
			interval: this.pollInterval * 1000
		});
		this.set('poller', task);
		this.poller.start();
	},
	cleanGroup: function() {
		var wipedOut = [];
		this.groups.foreach(function(g) {
			if (g.streams.getCount() == 1 && g.streams.getAt(0).viewmodelName == 'Blank')
				wipedOut.push(g);
		});
		Ext.each(wipedOut, function(g) {
			this.groups.remove(g)
		}, this)
	},

	init: function() {
		var me = this;

		this.setDefaultPageSize();
		this.on('streamsUpdated', this.streamsUpdated);

		if (this.mock) this.backend = RS.social.createMockBackend(true)

		this.streamStore.on('load', function() {
			this.initUnreadRecords(),
			this.updateWindowTitle()
			this.updateStreams()
			this.updateShowFollow()
			this.cleanGroup();
		}, this)
		
		this.postFilterStore.on('beforeload', function(store, operation, eOpts) {
			if (!this.activeStream && !this.initialStreamId && !this.postId) return false
			if (this.mode == 'embed' && !this.initialStreamId) return false

			operation.params = operation.params || {}

			var newerThanDate = '';
			if (this.oneDayOld) newerThanDate = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -1), 'c')
			if (this.oneWeekOld) newerThanDate = Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -7), 'c')

			Ext.apply(operation.params, {
				streamId: this.initialStreamId || (this.activeStream ? this.activeStream.id : ''),
				streamType: this.initialStreamType || (this.activeStream ? this.activeStream.type : ''),
				page: this.pageNumber,
				start: (this.pageNumber - 1) * this.pageSize,
				limit: this.pageSize,
				unreadOnly: this.unreadOnly
			})

			if (newerThanDate)
				Ext.apply(operation.params, {
					filter: Ext.encode([{
						field: 'sysUpdatedOn',
						condition: 'greaterThan',
						type: 'date',
						value: newerThanDate
					}])
				})

			if (this.postId) {
				Ext.apply(operation.params, {
					postId: this.postId
				})
			}
			this.set('postFilterStoreLoadingCount', this.postFilterStoreLoadingCount + 1)
		}, this)

		this.streamStore.load();

		this.postFilterStore.on('load', function(store, records, successfull, eOpts) {
			this.set('postFilterStoreLoadingCount', this.postFilterStoreLoadingCount - 1)
			//this.streamStore.load();
			if (this.postFilterStoreLoadingCount == 0) this.updatePosts()
		}, this)

		if (this.id) this.set('initialStreamId', this.id)
		delete this.id

		//Start auto-timer to refresh posts automatically	
		this.startPolling();

		if (!window['clientVM']) { //&& window.opener && window.opener.clientVM) {
			this.set('miniMode', true)
			this.set('clientVM', this.getMiniClientVM())
			window['clientVM'] = this.clientVM
			this.set('authenticated', clientVM.authenticated)
			clientVM.on('authenticatedChanged', function() {
				me.set('authenticated', clientVM.authenticated)
			})
			this.set('active', true)
			this.updateScreen() //manually kick updatescreen from mini-mode
		}

		//Go get the user's name from the client
		if (clientVM.user) this.set('userNameText', clientVM.user.fullName)
		else {
			clientVM.on('userChanged', function() {
			if (clientVM.user) this.set('userNameText', clientVM.user.fullName)
		}, this)
		}

		if (this.postId) this.postFilterStore.load()

		var winParams = {};
		try {
			Ext.Object.fromQueryString(window.location.search)
		} catch (e) {
			clientVM.displayError(e)
		}
		if (winParams.streamId) {
			this.set('activeCard', 1)
			this.set('initialStreamId', winParams.streamId)
		}
		if (this.postId) this.set('activeCard', 2)
		if (this.isNews) this.set('activePostCard', 3)
	},
	beforeDestroyComponent : function(){
		if(this.poller)			
			this.poller.destroy();
	},
	loadPosts: function() {
		this.postFilterStore.load()
	},

	getMiniClientVM: function() {
		var vm = this;
		var model = glu.model({
			mtype: 'viewmodel',
			authenticated: true,

			setWindowTitle: function(title) {
				window.document.title = title
			},
			displayError: function(message) {
				RS.UserMessage.msg({
					success: false,
					title: vm.localize('error'),
					msg: message,
					closeable: true
				})
			},

			displayFailure: function() {
				RS.UserMessage.msg({
					success: false,
					title: vm.localize('error'),
					msg: vm.localize('failure'),
					closeable: true
				})
			},

			displaySuccess: function(message) {
				RS.UserMessage.msg({
					success: true,
					title: vm.localize('success'),
					msg: message,
					duration: 2000,
					closeable: true
				})
			},

			user: null,

			init: function() {
				this.ajax({
					url: '/resolve/service/client/getUser',
					params: {
						notificationsSince: Math.max(Ext.Date.format(Ext.Date.subtract(new Date(), Ext.Date.HOUR, 1), 'time'), Ext.state.Manager.get('notificationIgnore', 0))
					},
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) this.set('user', response.data)
					}
				})
			}
		});
		model.init()
		return model
	},

	navigateToProfile: function(username) {
		clientVM.handleNavigation({
			modelName: 'RS.user.User',
			params: {
				username: username
			}
		})
	},

	unreadRecords: null,
	when_unreadrecords_changed_update_counts: {
		on: ['unreadRecordsChanged'],
		action: function() {
			if (this.groups.length > 0)
				this.groups.getAt(0).streams.forEach(function(stream) {
					if (Ext.isDefined(this.unreadRecords[stream.id]))
						stream.set('unReadCount', this.unreadRecords[stream.id])
				}, this)
		}
	},

	updateExisting: function(streamStore, stream) {
		return (!!!streamStore.supportPaging && !stream.supportPaging());
	},

	updateStreams: function() {
		//go through each of the streams in the store and update the streams list appropriately by either:
		//showing, hiding, updating the count, adding, removing, or whatever else might have changed since the last refresh
		this.streamToStore.removeAll()

		var index = -1,
			group, i = 0,
			len, streams, lookupId, streamId, newStreamsForStreamStore = [];

		this.streamStore.sort([{
			property: 'name',
			direction: 'ASC'
		}])

		this.streamStore.each(function(stream) {
			if (stream.data.id) {
				index = -1
				if (!group || stream.data.type != group.type || stream.data.currentUser != group.isMe) {
					group = this.getStreamGroup(stream)
				}

				if (!group.isMe) {
					if (stream.get('type').toLowerCase() != 'namespace')
						newStreamsForStreamStore.push({
							name: stream.get('name'),
							type: stream.get('type'),
							id: stream.get('sys_id'),
							displayName: stream.get('displayName')
						})

					len = group.streams.length
					streams = group.streams
					lookupId = stream.data.id

					for (i = 0; i < len && index == -1; i++) {
						if (lookupId == streams.getAt(i).id)
							index = i
					}

					//Add anything that wasn't found
					if (index == -1)
						streams.add(group.model(Ext.apply({
							mtype: 'RS.social.Stream'
						}, stream.data)))
					else streams.getAt(index).updateStream(stream.data) //Update existing records
				} else
					this.updateUserStreams(group, stream)
			} else {
				//we don't have a stream, we have the aggregate of unread records.
				// Accumulate unread records with the current on.
				var unreadDocuments = this.getDocumentGroup(),
				    unreadActionTasks = this.getActionTaskGroup();
				for (key in stream.raw) {
					stream.raw[key] = stream.raw[key] + (this.unreadRecords ? this.unreadRecords[key]: 0) + 
		                  (unreadDocuments && unreadDocuments.priorUnreadRecords ? unreadDocuments.priorUnreadRecords[key]: 0) + 
		                  (unreadActionTasks && unreadActionTasks.priorUnreadRecords ? unreadActionTasks.priorUnreadRecords[key]: 0);					
				}
				this.set('unreadRecords', stream.raw);
			}
		}, this)

		//Remove any extra items that aren't there anymore
		for (var g = 0; g < this.groups.length; g++) {
			var group = this.groups.getAt(g)
			for (var i = 0; i < group.streams.length; i++) {
				var mStream = group.streams.getAt(i),
					found = this.streamStore.getById(mStream.id) != null;

				if (!found && Ext.Array.indexOf(['all', 'inbox', 'starred', 'blog', 'sent', 'system', 'activity'], mStream.id) == -1) {
					if (this.updateExisting(this.streamStore, mStream)) {
						group.streams.removeAt(i)
						i--;
					}
				}
			}
			if (group.streams.length == 0) {
				this.groups.removeAt(g)
				g--;
			} else group.refreshDisplay()
		}

		this.streamToStore.add(newStreamsForStreamStore)
		this.streamToStore.sort('name', 'ASC')

		//Sort each group's streams
		this.groups.forEach(function(group) {
			if (!group.isMe && group.type !== 'DOCUMENT' && group.type !== 'ACTIONTASK') {
				var sortStreams = []
				group.streams.forEach(function(stream) {
					sortStreams.push(stream)
				})
				sortStreams.sort(function(a, b) {
					var aTemp = a.name ? a.name.toLowerCase() : a.name,
						bTemp = b.name ? b.name.toLowerCase() : b.name;

					if (aTemp > bTemp) return 1
					if (aTemp < bTemp) return -1
					return 0
				})

				Ext.Array.forEach(sortStreams, function(sortStream, index) {
					if (group.streams.getAt(index) != sortStream)
						group.streams.transferFrom(group.streams, sortStream, index)
				})
			}
		})

		if ((!this.activeStream && !this.postId) || this.initialStreamChanged) {
			this.set('initialStreamChanged', false)
			if (this.initialStreamId) this.selectStreamById(this.initialStreamId)
			else this.selectFirstStream()
		}
		this.fireEvent('streamsUpdated');
	},

	setDefaultPageSize: function() {
		// Default configurable page size is 10 items
		this.documentStreamStore.pageSize = this.defaultItemsPerPage;
		this.actionTaskStreamStore.pageSize = this.defaultItemsPerPage;
	},

	getStreamGroup: function(stream) {
		var g = null,
			type = stream.data.type,
			isCurrentUser = stream.data.currentUser,
			i = 0,
			len = this.groups.length,
			temp;

		for (; i < len && !g; i++) {
			temp = this.groups.getAt(i)
			if (temp.type == type && temp.isMe == isCurrentUser) g = temp
		}

		if (!g) {
			var name;
			if (stream.data.currentUser) name = this.localize('defaultText')
			else name = this.localize((type || '').toLowerCase())

			g = this.model({
				mtype: 'RS.social.StreamGroup',
				id: Ext.id(),
				type: type,
				name: name,
				isMe: stream.data.currentUser
			})
			if (stream.data.currentUser) this.groups.insert(0, g)
			else this.groups.add(g)
		}
		return g;
	},

	updateUserStreams: function(group, stream) {
		this.streamToStore.add({
			name: this.localize('blog'),
			id: stream.get('id'),
			displayName: this.localize('blog')
		})

		if (hasPermission('admin,social_admin'))
			this.streamToStore.add({
				name: this.localize('system'),
				id: 'system',
				displayName: this.localize('system')
			})
			//All, Inbox, Starred, Blog, Sent, system
		var all = inbox = starred = blog = sent = system = activity = false;
		group.streams.forEach(function(s) {
			switch (s.id) {
				case 'all':
					all = true
					break;
				case 'inbox':
					inbox = true
					break;
				case 'starred':
					starred = true
					break;
				case 'blog':
					blog = true
					break;
				case 'sent':
					sent = true
					break;
				case 'system':
					system = true
					break;
				case 'activity':
					activity = true
					break;
			}
		})

		if (!all)
			group.streams.add(group.model({
				mtype: 'RS.social.Stream',
				id: 'all',
				sys_id: 'all',
				name: this.localize('all'),
				isMe: true,
				unReadCount: stream.raw.unReadCountOfAll,
				type: 'USER_ALL'
			}))

		if (!activity) group.streams.add(group.model({
			mtype: 'RS.social.Stream',
			id: 'activity',
			name: this.localize('activity'),
			isMe: true,
			unReadCount: stream.raw.unReadCountOfActivity,
			showDigest: false,
			showEmail: false
		}))

		if (!inbox) group.streams.add(group.model({
			mtype: 'RS.social.Stream',
			id: 'inbox',
			sys_id: 'inbox',
			name: this.localize('inbox'),
			isMe: true,
			unReadCount: stream.raw.unReadCountOfInbox,
			type: 'USER'
		}))

		if (!starred) group.streams.add(group.model({
			mtype: 'RS.social.Stream',
			id: 'starred',
			name: this.localize('starred'),
			isMe: true,
			unReadCount: stream.raw.unReadCountOfStarred,
			showDigest: false,
			showEmail: false
		}))

		if (!blog) group.streams.add(group.model({
			mtype: 'RS.social.Stream',
			id: 'blog',
			name: this.localize('blog'),
			isMe: true,
			showDigest: false,
			showEmail: false
		}))

		if (!sent) group.streams.add(group.model({
			mtype: 'RS.social.Stream',
			id: 'sent',
			name: this.localize('sent'),
			isMe: true,
			showDigest: false,
			showEmail: false
		}))

		if (!system) group.streams.add(group.model({
			mtype: 'RS.social.Stream',
			id: 'system',
			sys_id: 'system',
			// lock: true,
			name: this.localize('system'),
			isMe: true,
			type: 'system'
		}))
	},

	initUnreadRecords: function() {
		// Reset unreadRecords before every load.
		this.unreadRecords = null;
	},

	getGroup: function(name) {
		for (var i=0; i<this.groups.length; i++) {
			if (this.groups.getAt(i).type === name) {
				return this.groups.getAt(i);
			}
		}
		return null;
	},

	getDocumentGroup: function() {
		return this.getGroup('DOCUMENT');
	},

	getActionTaskGroup: function() {
		return this.getGroup('ACTIONTASK');
	},

	getStreamGroupWithPagination: function(type) {
		var streamGroup = null,
			i = 0;
		for(i=0; i<this.groups.length; i++) {
			var curStreamGroup = this.groups.getAt(i);
			if (curStreamGroup.type === type) {
				streamGroup = curStreamGroup;
				break;
			}
		}
		if(!streamGroup) {
			streamGroup = this.model({
				mtype: 'RS.social.StreamGroup',
				id: Ext.id(),
				type: type,
				name: this.localize((type || '').toLowerCase()),
				isMe: false
			});
			if (type === 'DOCUMENT') {
				streamGroup.set('store', this.documentStreamStore);
			} else if (type === 'ACTIONTASK') {
				streamGroup.set('store', this.actionTaskStreamStore);
			}
			this.groups.add(streamGroup);
		}
		return streamGroup;
	},

	updatesLocalsTreamStore: function(streams) {
		if (streams) {
			for (var i=0; i<streams.length; i++) {
				var curStream = streams[i];
				var curStreamToStoreRecord = this.streamToStore.getById(curStream.id);
				if (!!curStreamToStoreRecord) {
					this.streamToStore.remove(curStreamToStoreRecord);
				}
				this.streamToStore.add({
					name: curStream.get('name'),
					type: curStream.get('type'),
					id: curStream.get('sys_id'),
					displayName: curStream.get('displayName')
				});
			}
		}
	},

	removeStreamsFromLocalsTreamStore: function(streams) {
		if (streams) {
			for (var i=0; i<streams.length; i++) {
				var curStream = streams.getAt(i);
				var curStreamToStoreRecord = this.streamToStore.getById(curStream.id);
				if (!!curStreamToStoreRecord) {
					this.streamToStore.remove(curStreamToStoreRecord);
				}
			}
		}
	},

	updatePagination: function(type) {
		if(type === 'DOCUMENT') {
			this.documentStreamStore.reload();
		} else if (type === 'ACTIONTASK') {
			this.actionTaskStreamStore.reload();
		}
	},

	postYourMessageBoxReady: false,
	streamsUpdated: function() {
		this.updatesLocalsTreamStore(this.documentStreamStore.getRange());
		this.updatesLocalsTreamStore(this.actionTaskStreamStore.getRange());
		this.set('postYourMessageBoxReady', true);
	},

	// @param records: DOCUMENT stream records
	updateStreamGroup: function(records, type) {
		var streamGroup = this.getStreamGroupWithPagination(type);

		// Remove streamGroup's streams from 'streamToStore'
		this.removeStreamsFromLocalsTreamStore(streamGroup.streams);

		streamGroup.handleStoreLoaded(records);
		this.updatesLocalsTreamStore(records);
	},

	updateDocumentStreamGroupOnloaded: function(store, records, successful) {
		if (successful) {
			this.updateStreamGroup(records, 'DOCUMENT');
		}
	},

	updateActionTaskStreamGroupOnloaded: function(store, records, successful) {
		if (successful) {
			this.updateStreamGroup(records, 'ACTIONTASK');
		}
	},

	loadStreamGroupWithPagination: function() {
		//
		// Go through each store, load it and hook up pagination.
		//

		// DOCUMENT
		this.documentStreamStore.load({
			params: {
				start: (this.documentStreamStore.currentPage-1)*this.documentStreamStore.pageSize,
				limit: this.documentStreamStore.pageSize
			},
			scope: this,
			callback: function(records, operation, success) {
				this.updateStreamGroup(records, 'DOCUMENT');
				// Intercept Document's store load so that we can update the
				// Document StreamGroup every time a new document page is loaded.
				this.documentStreamStore.on('load', this.updateDocumentStreamGroupOnloaded, this);
			}
		});

		// ACTIONTASK
		this.actionTaskStreamStore.load({
			params: {
				start: (this.actionTaskStreamStore.currentPage-1)*this.actionTaskStreamStore.pageSize,
				limit: this.actionTaskStreamStore.pageSize
			},
			scope: this,
			callback: function(records, operation, success) {
				this.updateStreamGroup(records, 'ACTIONTASK');
				// Intercept Document's store load so that we can update the
				// Document StreamGroup every time a new document page is loaded.
				this.actionTaskStreamStore.on('load', this.updateActionTaskStreamGroupOnloaded, this);
			}
		});
	},

	updateScreen: function(config) {
		if (this.active) {
			//Update the window title immediately even though it will also be done after the store is loaded
			this.updateWindowTitle()

			//Update streams
			if (!this.miniMode || this.activeCard == 0)
				this.streamStore.load()

			//Update selected stream's posts
			if (this.activeStream)
				this.postFilterStore.load()

			// Load stream groups that support pagination.
			if (!this.miniMode || this.activeCard == 0) {
				this.loadStreamGroupWithPagination();
			}
		}
	},

	updatePosts: function() {
		this.set('totalCount', this.postFilterStore.totalCount);

		//Remove any old posts that weren't in the latest page of results
		for (var i = 0; i < this.posts.length; i++) {
			if (!this.postFilterStore.getById(this.posts.getAt(i).id)) {
				this.posts.removeAt(i)
				i--;
			}
		}

		var posts = this.postFilterStore.data.items;

		for(var i = posts.length - 1; i >= 0; i--) {
			var post = posts[i];
			var postIndex = -1;

			for(var j = 0; j < this.posts.length; j++) {
				if(this.posts.getAt(j).id === post.data.id) {
					postIndex = j;
					break;
				}
			}

			if(postIndex === -1) {
				var newPost = this.model(Ext.apply({
					mtype: 'RS.social.Post'
					}, post.raw));
				this.posts.insert(0,newPost);
			} else {
				var match = this.posts.getAt(postIndex);
				match.updatePost(post);

				if (this.activePost && this.activePost.id === match.id) {
					this.activePost.syncPostProperties(match);
				}
			}
		}
	},

	updateWindowTitle: function() {
		//Update window title with the proper count of unread social posts if there are any
		if (this.rootVM == this) {
			var winTitle = this.localize('windowTitle');
			if (this.activeStream) {
				winTitle = this.activeStream.name + ' - ' + winTitle
				if (this.activeStream.count) winTitle = '(' + this.activeStream.count + ') ' + winTitle
			}

			if (clientVM && clientVM.setWindowTitle) clientVM.setWindowTitle(winTitle)
			else window.document.title = winTitle
		}
	},


	to: [],
	toIsValid$: function() {
		if (Ext.Array.indexOf(this.to, 'system') != -1 && this.to.length > 1) return this.localize('invalidToSystem')
		return this.to.length > 0 ? true : this.localize('invalidTo')
	},
	toHidden: [],
	subject: '',
	content: '',
	contentIsValid$: function() {
		return this.content.length > 0
	},

	showAdvancedTo: function() {
		this.open({
			mtype: 'RS.social.StreamPicker'
		})
	},

	streamParent: '',

	switchToPostMessage: function() {
		this.set('activePostCard', 1);

		this.posts.forEach(function(post) {
			post.cancel();
		});

		this.set('toHidden', '');
		this.set('switchingToPost', true);

		if (this.initialStreamId && this.streamParent) {
			this.set('toHidden', [this.streamToStore.createModel({
				id: this.initialStreamId,
				name: this.initialStreamId == (clientVM.user ? clientVM.user.id : '') ? this.localize('blog') : this.streamParent,
				displayName: this.initialStreamId == (clientVM.user ? clientVM.user.id : '') ? this.localize('blog') : this.streamParent
			})]);
		} else if (this.activeStream && this.streamToStore.findExact('id', this.activeStream.sys_id) > -1) {
			this.set('toHidden', [this.streamToStore.getAt(this.streamToStore.findExact('id', this.activeStream.sys_id))]);
		} else if (this.activeStream && this.activeStream.id == 'system') {
			var systemStreamIndex = this.streamToStore.findExact('name', this.localize('system'));

			if (systemStreamIndex > -1) {
				this.set('toHidden', [this.streamToStore.getAt(systemStreamIndex)]);
			}
		} else {
			//default to Blog
			var blogStreamIndex = this.streamToStore.findExact('name', this.localize('blog'));

			if (blogStreamIndex > -1) {
				this.set('toHidden', [this.streamToStore.getAt(blogStreamIndex)]);
			}

			//Also, switch to the blog stream because Duke said to
			this.selectStreamById('blog');
		}
	},

	switchToFilter: function() {
		this.set('activePostCard', 2)
	},

	showPostToolbarButtons$: function() {
		return this.activePostCard != 1 && this.postDisplayItem != 1
	},

	showPostToolbarButtonsRefresh$: function() {
		return this.activePostCard != 1
	},

	showPostToolbarActions$: function() {
		return this.activePostCard != 1 && !this.isNews && this.postDisplayItem != 1
	},

	showPostActionToolbarActions$: function() {
		return this.activePostCard != 1 && !this.isNews && ((this.activeStream && !this.activeStream.lock && this.activeStream.type.toLowerCase() != 'namespace') || this.initialStreamId) && this.postDisplayItem != 1
	},

	showPostToolbarButton$: function() {
		return this.showPostToolbarButtons && this.activeStream && !this.activeStream.lock
	},

	post: function(form, post) {
		var p = this;
		if (post && post.id) p = post
		p.set('posting', true)
		var content = p.commentContent || p.content;
		content = content.replace('<span class="suggest"></span>', '');

		this.ajax({
			url: '/resolve/service/social/submit',
			params: {
				streams: p.to,
				subject: p.subject,
				content: content,
				postId: p.id,
				streamType: this.initialStreamType || (this.activeStream ? this.activeStream.type : '')
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.set('pageNumber', 1)
					this.postFilterStore.load()
					if (form && form.fireEvent) form.fireEvent('resetPostForm')
					p.resetPost()
					if (this.miniMode) this.set('activeCard', 1)
					if (!this.miniMode) clientVM.displaySuccess(this.localize('messageSent'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('posting', false);
			}
		})
	},
	posting: false,
	postIsEnabled$: function() {
		return this.isValid && !this.posting && this.content.indexOf('/resolve/js/extjs4/resources/ext-theme-neptune/images/loadmask/loading.gif') == -1
	},

	comment: function(form, post) {
		this.post(form, post)
	},
	commentIsVisible$: function() {
		return false
	},

	cancel: function() {
		this.resetPost()
	},

	resetPost: function() {
		this.set('activePostCard', 0)
		this.set('to', '')
		this.set('subject', '')
		this.set('content', '')
	},

	/* Deprecated feature
	createLink: function(editor) {
		this.open({
			mtype: 'RS.social.CreateLink',
			editor: editor
		})
	}, 
	*/ 

	/* Deprecated feature
	attachFile: function(editor) {
		this.open({
			mtype: 'RS.social.AttachFile',
			editor: editor
		})
	},
	*/

	markAllPostsRead: function() {
		if (this.activeStream)
			this.activeStream.markStreamRead(new Date())
		if (this.mode == 'embed')
			this.ajax({
				url: '/resolve/service/social/markStreamRead',
				params: {
					id: this.initialStreamId,
					streamType: this.initialStreamType,
					olderThanDate: Ext.Date.format(new Date(), 'c')
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					Ext.defer(this.refreshPosts, 800, this);
					// this.refreshPosts();
					if (response.success) clientVM.displaySuccess(this.localize('markedStreamRead'))
					else clientVM.displayError(response.message)
				}
			})
	},
	markPostsOlderThanOneDayRead: function() {
		if (this.activeStream) this.activeStream.markStreamRead(Ext.Date.add(new Date(), Ext.Date.DAY, -1))
	},
	markPostsOlderThanOneWeekRead: function() {
		if (this.activeStream) this.activeStream.markStreamRead(Ext.Date.add(new Date(), Ext.Date.DAY, -7))
	},
	markReadScroll: function(index) {
		if (index < this.posts.length && !this.posts.getAt(index).read) this.posts.getAt(index).toggleReadPost()
	},

	refreshPosts: function() {
		this.postFilterStore.load()
	},
	autoRefreshPosts: false,	
	refreshStreams: function() {
		this.streamStore.load();
	},
	toggleStreams: function(expand) {
		this.groups.forEach(function(group) {
			group.set('showAll', expand)
		})
	},
	showToggleStreams$: function() {
		return this.activeCard == 0
	},

	selectStream: function(stream) {
		this.groups.forEach(function(group) {
			group.streams.forEach(function(s) {
				if (Ext.isDefined(s.selected))
					s.set('selected', s == stream)
			})
		})
		this.switchToPostDisplay()
		if (this.miniMode) this.set('activeCard', 1)
	},

	findStream: function(stream) {
		var rStream = null;
		this.groups.forEach(function(group) {
			group.streams.forEach(function(s) {
				if (s == stream) rStream = stream
			})
		})
		return rStream
	},

	selectStreamById: function(streamId) {
		var found = false;
		this.groups.forEach(function(group) {
			group.streams.forEach(function(s) {
				if (s.id == streamId) found = true
				s.set('selected', s.id == streamId)
			})
		})
		if (this.miniMode) {
			if (found)
				this.set('activeCard', 1)
			else
				this.ajax({
					url: '/resolve/service/social/getStream',
					params: {
						id: streamId
					},
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							if (response.data) {
								this.set('activeStream', this.model(Ext.apply({
									mtype: 'RS.social.Stream'
								}, response.data)))
							}
						} else clientVM.displayError(response.message)
					}
				})
		}
	},

	selectFirstStream: function() {
		if (this.groups.length > 0) {
			var group = this.groups.getAt(0)
			if (group.streams.length > 0) this.selectStream(group.streams.getAt(1))
		}
	},

	hideLikes: function() {
		this.posts.forEach(function(p) {
			p.closePopups()
		})
	},

	removePost: function(post) {
		this.posts.remove(post)
	},

	joinComponent: function() {
		this.open({
			mtype: 'RS.social.StreamPicker',
			type: 'user',
			pickerType: 1
		})
	},

	/*Mini components*/
	activeCard: 0, //0 streams, 1 stream, 2 post, 3 post/comment

	when_activeCard_changes_update_data: {
		on: ['activeCardChanged'],
		action: function() {
			this.updateTitleText()
			this.updateBackText()
			this.updateShowFollow()
			this.refresh()
		}
	},

	initialStreamChanged: false,
	setInitialStream: function(id) {
		this.set('initialStreamChanged', true)
		this.set('initialStreamId', id)
		this.refreshPosts()
	},

	when_initialStreamId_changes_update_posts: {
		on: ['initialStreamIdChanged'],
		action: function() {
			if (this.initialStreamId) {
				this.set('pageNumber', 1)
				this.postFilterStore.load()
			}
		}
	},

	backText: '',
	updateBackText: function() {
		switch (this.activeCard) {
			case 0:
				this.set('backText', '')
				break;
			case 1:
				this.set('backText', this.localize('streams'))
				break;
			case 2:
				this.set('backText', this.activeStream ? this.activeStream.titleDisplay : '')
				break;
			case 3:
				this.set('backText', this.activeStream ? this.activeStream.titleDisplay : '')
				break;
			case 4:
				this.set('backText', this.activeStream ? this.activeStream.titleDisplay : '')
				break;
		}
	},

	titleText: '',
	titleTextTooltip: '',
	updateTitleText: function() {
		switch (this.activeCard) {
			case 0:
				this.set('titleText', this.localize('streams'))
				this.set('titleTextTooltip', this.localize('streams'))
				break;
			case 1:
				this.set('titleText', this.activeStream ? this.activeStream.titleDisplay : '')
				this.set('titleTextTooltip', this.activeStream ? this.activeStream.titleFullDisplay : '')
				break;
			case 2:
				this.set('titleText', this.activeStream ? this.activeStream.titleDisplay : '')
				this.set('titleTextTooltip', this.activeStream ? this.activeStream.titleFullDisplay : '')
				break;
			case 3:
				this.set('titleText', this.localize('newPost'))
				this.set('titleTextTooltip', this.localize('newPost'))
				break;
		}
	},


	refreshIsVisible$: function() {
		return this.activeCard < 3
	},

	showRefreshButton$: function() {
		return this.activeCard < 3
	},
	showPostButton$: function() {
		var stream = this.activeStream
		return stream && !stream.lock && (!stream.isMe || stream.id == 'blog') && (this.activeCard == 1 || this.activeCard == 2)
	},
	switchToPost: function() {
		var id = this.activeStream.id
		if (id == 'blog') {
			var index = this.streamToStore.findExact('name', this.localize('blog'))
			if (index > -1) {
				var blogStream = this.streamToStore.getAt(index)
				id = blogStream.data.id
			}
		}
		this.set('to', [id])
		this.set('activeCard', 3)
	},
	switchToForum: function() {
		this.set('activeCard', 4)
	},

	refresh: function() {
		switch (this.activeCard) {
			case 0:
				this.streamStore.load()
				break;
			case 1:
				this.postFilterStore.load()
				break;
			case 2:
				break;
		}
	},
	backIsVisible$: function() {
		return this.activeCard > 0
	},
	back: function() {
		if (this.activeCard == 3 || this.activeCard == 4) this.set('activeCard', 1)
		else if (this.activeCard > 0) this.set('activeCard', this.activeCard - 1)
	},

	updateMini: function() {
		this.updateBackText()
		this.updateTitleText()
		this.set('activeCard', 1)
	},

	cancelMini: function() {
		this.set('to', '')
		this.set('subject', '')
		this.set('content', '')
		this.set('activeCard', 1)
	},

	showPagingButtons$: function() {
		return this.activeCard == 1
	},

	updateShowFollow: function() {
		var temp = this.activeStream,
			stream = this.findStream(temp);
		if (temp)
			this.set('showFollow', stream == null && this.activeCard == 1)
	},

	showFollow: false,

	followStream: function() {
		this.ajax({
			url: '/resolve/service/social/setFollowStreams',
			params: {
				ids: [this.activeStream.id],
				follow: true
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) this.refreshStreams()
				else clientVM.displayError(response.message)
			}
		})
	},

	authenticateText$: function() {
		return this.authenticated ? false : this.localize('notAuthenticated')
	},

	previousPage: function() {
		this.set('pageNumber', this.pageNumber - 1)
	},
	previousPageIsEnabled$: function() {
		return this.pageNumber > 1
	},
	nextPage: function() {
		this.set('pageNumber', this.pageNumber + 1)
	},
	nextPageIsEnabled$: function() {
		return this.pageNumber * this.pageSize < this.totalCount
	},

	postDisplayItem: 0,
	activePost: {
		mtype: 'Post',
		viewMode: 'forum'
	},

	switchToForumDisplay: function(post) {
		this.activePost.syncPostProperties(post)
		if (this.miniMode) {
			this.switchToForum()
		} else {
			this.set('postDisplayItem', 1)
		}
	},

	switchToPostDisplay: function() {
		this.activePost.cancel()
		this.set('postDisplayItem', 0)
	},

	switchToPostDisplayIsVisible$: function() {
		return this.postDisplayItem == 1
	}
})
glu.defModel('RS.social.MovePost', {

	to: [],
	toIsValid$: function() {
		return this.to.length > 0 ? true : this.localize('invalidTo')
	},
	toHidden: [],

	streamToStore: {
		mtype: 'store',
		fields: ['id', 'name', 'type', 'displayName'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	init: function() {
		this.parentVM.parentVM.streamToStore.each(function(record) {
			this.streamToStore.add(record)
		}, this)
		var tos = []
		if (this.parentVM && this.parentVM.target) {
			Ext.Array.forEach(this.parentVM.target, function(target) {
				if (target.id) tos.push(target.id)
			})

			if (tos.length == 0) {
				var blogStreamIndex = this.rootVM.streamToStore.findExact('name', this.localize('blog'))
				if (blogStreamIndex > -1)
					tos.push(this.rootVM.streamToStore.getAt(blogStreamIndex))
			}
			this.set('to', tos)
		}
	},

	movePost: function() {
		this.ajax({
			url: '/resolve/service/social/move',
			params: {
				postId: this.parentVM.id,
				targets: this.to
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.rootVM.refreshPosts()
					this.doClose()
				} else
					clientVM.displayError(response.message)
			}
		})
	},

	movePostIsEnabled$: function() {
		return this.isValid
	},

	cancel: function() {
		this.doClose()
	}
})
glu.defModel('RS.social.Post', {
	id: '',
	authorUsername: '',
	authorDisplayName: '',
	authorSysId: '',
	commentAuthorUsername: '',
	commentAuthorDisplayName: '',
	authorId: '',
	title: '',
	uri: '',
	subject$: function() {
		return Ext.htmlEncode(this.title);
	},
	viewMode: '',
	component: '',
	componentType: '',
	content: '',
	csrftoken: '',
	contentDisplay$: function() {
		//hashtag ee.log(this.content);xtraction to auto-link
		var content = this.content,
			hashtags;
		//Weak logic to rely on string generated from backend to decide if this is our custom HTML content.
		var isRSContent = content.indexOf('<!-- HTML_BEGIN -->') != -1 || content.indexOf('Searched Wiki Documents') != -1;
		content = content.replace(/<span class="suggest"><\/span>/g, '').replace(/&lt;span class=&quot;suggest&quot;&gt;&lt;\/span&gt;/g, '')

		hashtags = content.match(/(^|\s)#([\w\d]*[\w\d])\s+/g)

		if (hashtags) {
			Ext.Array.forEach(hashtags, function(rawTag) {
				var tag = Ext.String.trim(rawTag);
				if (tag.indexOf('&quot;') == -1)
					content = content.replace(tag, tag.substring(0, tag.indexOf('#')) + Ext.String.format('<a href="#RS.search.Main/tag={0}">{1}</a>', tag.substring(tag.indexOf('#') + 1), tag.substring(tag.indexOf('#'))))
			})
		}

		if ((content.indexOf('localhost') != -1) || (content.indexOf(window.location.origin) != -1) && (content.indexOf('resolve/jsp/rsclient.jsp?') != -1)) {
			const urlData = content.split('href=');
			var tmpContent = '';
			if (urlData.length > 1) {
				for (var i=0; i < urlData.length; i++) {
					if ((urlData[i].indexOf('localhost') != -1) || (urlData[i].indexOf(window.location.origin) != -1) && (urlData[i].indexOf('resolve/jsp/rsclient.jsp?') != -1)) {
						var subUrlPart = urlData[i].split('resolve/jsp/rsclient.jsp?');
						if (subUrlPart.length > 1) {
							tmpContent += unescape('href=\'/resolve/jsp/rsclient.jsp?' + clientVM.rsclientToken + '%26' + subUrlPart[1]);
						} else {
							tmpContent += subUrlPart[0];
						}
					} else {
						tmpContent += urlData[i];
					}
				}
				if (tmpContent) {
					content = tmpContent;
				}
			}
		}

		return isRSContent ? Ext.util.Format.nl2br(content) : Ext.util.Format.nl2br(Ext.String.htmlEncode(content));
	},
	starred: false,
	numberOfLikes: 0,
	numberOfComments: 0,
	liked: false,
	sysCreatedOn: new Date(),
	sysUpdatedOn: new Date(),
	now: new Date(),
	comments: [],
	targets: [],
	beforeDestroyComponent : function(){
		if(this.poller)
			this.poller.destroy();
		if(this.runner)
			this.runner.destroy();
	},
	changeInterestedTarget: function(target) {
		this.set('component', target.name);
	},
	shouldBeLocked$: function() {
		var interestedTarget = null;
		Ext.each(this.targets, function(target) {
			if (target.name == this.component)
				interestedTarget = target;
		}, this);
		return interestedTarget && interestedTarget.locked || this.locked;
	},
	locked: false,
	read: true,
	hasDeleteRights: false,

	answer: false,

	commentsList: {
		mtype: 'list'
	},

	targetList: {
		mtype: 'list'
	},

	to$: function() {
		return this.id
	},

	commentContent: '',
	activePostCard: 0,
	switchToPostMessage: function() {
		if (this.getRootVM().cancel && this.getRootVM().posts) {
			this.getRootVM().cancel()
			this.getRootVM().posts.forEach(function(post) {
				post.cancel()
			})
		}
		this.set('activePostCard', 1)
	},

	cancel: function() {
		this.resetPost()
	},

	resetPost: function() {
		this.set('activePostCard', 0)
		this.set('commentContent', '')
	},
	comment: function(form) {
		this.getParentVM().comment(form, this)
	},
	commentIsEnabled$: function() {
		return this.postIsEnabled
	},
	posting: false,
	postIsEnabled$: function() {
		return !this.posting && this.commentContent.indexOf('/resolve/js/extjs4/resources/ext-theme-neptune/images/loadmask/loading.gif') == -1 && this.commentContent.length > 0
	},

	/* Deprecated feature
	createLink: function(editor) {
		this.open({
			mtype: 'RS.social.CreateLink',
			editor: editor
		})
	},
	*/

	/* Deprecated feature
	attachFile: function(editor) {
		this.open({
			mtype: 'RS.social.AttachFile',
			editor: editor
		})
	},
	*/

	isPost$: function() {
		return this.getParentVM().viewmodelName != 'Post'
	},

	imageSize$: function() {
		return this.isPost && !this.getRootVM().miniMode ? 46 : 28
	},

	imagePadding$: function() {
		return this.isPost ? '0px 10px 0px 0px' : '10px 10px 0px 0px'
	},

	imageAlignment$: function() {
		return this.isPost && this.getParentVM().displayPostProfilePictures ? 'middle' : 'top'
	},

	showAuthorSubject$: function() {
		return !this.subject
	},

	subjectDisplay$: function() {
		return this.getRootVM().miniMode ? Ext.util.Format.ellipsis(this.subject, 20) : Ext.util.Format.ellipsis(this.subject, 75)
	},

	subjectTooltip$: function() {
		return this.getRootVM().miniMode ? this.subject : ''
	},

	dateTooltip$: function() {
		return this.getRootVM().miniMode ? this.dateText : ''
	},

	contentPadding$: function() {
		return this.isPost && this.getParentVM().displayPostProfilePictures ? '10px 0px 0px 0px' : '10px 0px 0px 5px'
	},

	showStar$: function() {
		return true //this.type != 'system'
	},

	username: '',

	showCaret$: function() {
		return this.isPost && !this.getRootVM().miniMode && this.showAuthor && this.type != 'system' && this.type != 'manual'
	},

	showAuthor$: function() {
		var hasNonRSS = false
			/*
			 * To remove displaying system user
			 * from post uncomment the following
			 * // commented out lines
			 */
			//if(this.authorUsername.toLowerCase() != 'system'){
		Ext.Array.each(this.targets, function(t) {
				if (t.type != 'RSS') {
					hasNonRSS = true
					return false
				}
			})
			//}

		return hasNonRSS
	},

	displayComponent$: function() {
		return !this.getRootVM().miniMode && this.type != 'system' && this.type != 'manual'
	},

	displayAddComment$: function() {
		return !this.shouldBeLocked && (this.viewMode == 'forum' || !this.isForum) && (this.getParentVM().activeStream && !this.getParentVM().activeStream.lock) && this.type != 'system' && this.type != 'manual'
	},

	targetsLocked$: function() {
		var hasNonLocked = false
		Ext.Array.each(this.targets, function(t) {
			if (!t.lock) {
				hasNonLocked = true
				return false
			}
		})
		return !hasNonLocked
	},

	init: function() {
		this.initToken();
		this.set('now', new Date())
		if (!Ext.isDate(this.sysCreatedOn)) this.set('sysCreatedOn', Ext.Date.parse(this.sysCreatedOn, 'time'))
		if (!Ext.isNumber(this.numberOfLikes)) this.set('numberOfLikes', Number(this.numberOfLikes))

		if (!this.runner) this.runner = new Ext.util.TaskRunner()
		if (!this.poller) this.poller = this.runner.newTask({
			run: this.updateNow,
			scope: this,
			interval: 15000 //15 seconds
		})
		this.poller.start()

		this.targets = this.targets || []

		Ext.Array.forEach(this.targets, function(target) {
			if (target.sysId == this.authorSysId) target.name = this.localize('blog')
			if (this.getParentVM().activeStream && this.getParentVM().activeStream.id == 'system') target.name = this.localize('system')
			this.targetList.add(this.model(Ext.apply({
				mtype: 'viewmodel'
			}, target)))
			if (target.type && target.type.toLowerCase() == 'rss') this.set('componentType', 'rss')
			if (target.type && target.type.toLowerCase() == 'forum') this.set('componentType', 'forum')
		}, this)

		if (this.targetList.length > 0) {
			//determine best target to display
			var set = false
			this.targetList.forEach(function(target) {
				if (this.getParentVM().activeStream && target.name == this.getParentVM().activeStream.name) {
					this.set('component', target.type == 'USER' ? target.displayName : target.name)
					set = true
				}
			}, this)
			if (!set) this.set('component', this.targetList.getAt(0).type == 'USER' ? this.targetList.getAt(0).displayName : this.targetList.getAt(0).name)
		} else this.set('component', this.localize('blog'))

		if (this.isPost) {
			this.comments = this.comments || []
			Ext.Array.forEach(this.comments, function(child) {
				this.commentsList.add(this.model(Ext.apply({
					mtype: 'RS.social.Post'
				}, child)))

				if (child.answer) {
					this.set('answered', true)
					this.answersList.add(this.model(Ext.apply({
						mtype: 'RS.social.Post'
					}, child)))
				}
			}, this)
		} else {
			//determine delete rights
			this.set('hasDeleteRights', this.getParentVM().username == this.commentAuthorUsername || hasPermission('admin'))
		}
		this.determineFollowing()
	},
	activate: function() {
		this.initToken();
	},
	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/user/downloadProfile', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this));
	},
	getRootVM : function(){
		return this.rootVM;
	},
	getParentVM : function(){
		return this.parentVM;
	},
	updateNow: function() {
		this.set('now', new Date())
	},
	starCls$: function() {
		return 'post-star icon-large ' + (this.starred ? 'icon-star post-starred' : 'icon-star-empty');
	},
	starTooltip$: function() {
		return this.starred ? this.localize('starTooltip') : this.localize('starredTooltip')
	},

	numberOfLikesText$: function() {
		return Ext.String.format('({0})', this.numberOfLikes)
	},

	showImage$: function() {
		return this.isPost ? this.getParentVM().displayPostProfilePictures : this.getParentVM().parentVM.displayCommentProfilePictures
	},
	userProfilePicture$: function() {
		return this.csrftoken !== '' ? Ext.String.format('/resolve/service/user/downloadProfile?username={0}&{1}', this.commentAuthorUsername || this.authorUsername, this.csrftoken) : '';
	},
	collapsed: false,
	collapseTitle$: function() {
		var count = this.commentsList.length
		return this.collapsed ? this.localize(count == 1 ? 'expandComment' : 'expandComments', [count]) : this.localize(count == 1 ? 'collapseComment' : 'collapseComments', [count])
	},
	collapseCls$: function() {
		return this.collapsed ? 'icon-caret-down' : 'icon-caret-up'
	},
	displayComments$: function() {
		return this.commentsList.length > 0 && (this.viewMode == 'forum' || !this.isForum)
	},
	displayContent$: function() {
		return !this.isForum || this.viewMode == 'forum'
	},
	toggleCollapseComments: function() {
		this.set('collapsed', !this.collapsed)
	},

	toggleStarred: function() {
		this.set('starred', !this.starred)
		this.syncMainPostProperties()
		this.ajax({
			url: '/resolve/service/social/setStarred',
			params: {
				id: this.id,
				parentId: this.getParentVM().id,
				starred: this.starred
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (!response.success) {
					this.set('starred', !this.starred)
					this.syncMainPostProperties()
					clientVM.displayError(response.message)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	toggleLike: function() {
		this.set('liked', !this.liked)
		this.set('numberOfLikes', Number(this.numberOfLikes) + (this.liked ? 1 : -1))
		this.syncMainPostProperties()
		this.ajax({
			url: '/resolve/service/social/setLike',
			params: {
				id: this.id,
				parentId: this.getParentVM().id,
				like: this.liked
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (!response.success) {
					this.set('like', !this.like)
					this.set('numberOfLikes', this.numberOfLikes + (this.liked ? 1 : -1))
					this.syncMainPostProperties()
					clientVM.displayError(response.message)
				} else {
					//set the Post to be 'read' if user Likes it.
					this.set('read', true)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	likeCls$: function() {
		return 'post-hand icon-large ' + (this.liked ? 'icon-thumbs-up post-like-hand' : 'icon-thumbs-up-alt post-like-hand')
	},

	displayTargets$: function() {
		return this.targetList.length > 1 && !this.getRootVM().miniMode && this.type != 'system' && this.type != 'manual'
	},

	authorTooltip$: function() {
		if (this.getRootVM().miniMode && this.targetList.length > 0) {
			var list = [];
			this.targetList.foreach(function(target) {
				list.push(target.name)
			})
			return (list.length == 1 ? this.localize('target') : this.localize('targets')) + ' ' + list.join(', ')
		}
		return ''
	},

	dateText$: function() {
		var now = this.now,
			date = this.sysCreatedOn,
			diff;

		if (!Ext.isDate(date)) date = Ext.Date.parse(date, 'time')

		diff = (now - date) / 1000

		if (diff < 60) //1 minute
			return Ext.String.format('{0} {1} {2}', Math.max(Math.round(diff), 0), this.localize('seconds'), this.localize('ago'))
		if (60 < diff && diff < 60 * 2) // > 1 minute but < 2 minutes
			return Ext.String.format('{0} {1} {2}', 1, this.localize('minute'), this.localize('ago'))
		if (60 * 2 < diff && diff < 60 * 60) // > 2 minutes but < 60 minutes
			return Ext.String.format('{0} {1} {2}', Math.round(diff / 60), this.localize('minutes'), this.localize('ago'))
		if (60 * 60 < diff && diff < 2 * 60 * 60) // > 1 hour but < 2 hours
			return Ext.String.format('{0} {1} {2}', 1, this.localize('hour'), this.localize('ago'))
		if (2 * 60 * 60 < diff && diff < 24 * 60 * 60) // > 2 hours but < 1 day
			return Ext.String.format('{0} {1} {2}', Math.round(diff / (60 * 60)), this.localize('hours'), this.localize('ago'))
		if (24 * 60 * 60 < diff && diff < 2 * 24 * 60 * 60) // > 24 hours but < 48 hours
			return Ext.String.format('{0}', this.localize('yesterday'))
		if (2 * 24 * 60 * 60 < diff && diff < 7 * 24 * 60 * 60)
			return Ext.String.format('{0} {1} {2}', Math.round(diff / (24 * 60 * 60)), this.localize('days'), this.localize('ago'))

		return Ext.Date.format(date, clientVM.userDefaultDateFormat || Ext.state.Manager.get('userDefaultDateFormat', 'c'))
	},

	likesDisplayed: false,
	likesModel: null,
	toggleLikesDisplay: function(tbtext) {
		if (!this.getRootVM().miniMode) {
			if (this.likesDisplayed)
				this.likesModel.doClose()
			else {
				if (this.numberOfLikes > 0) {
					this.likesModel = this.open({
						mtype: 'RS.social.Likes',
						target: tbtext
					})
					this.set('likesDisplayed', !this.likesDisplayed)
				}
			}
		}
	},
	closeLikes: function() {
		if (!this.getRootVM().miniMode) {
			if (this.likesDisplayed) {
				this.likesModel.doClose()
				this.set('likesDisplayed', false)
			}

			if (this.authorDisplayed) {
				this.authorModel.doClose()
				this.set('authorDisplayed', false)
			}
		}
	},

	authorDisplayed: false,
	authorModel: null,
	toggleAuthorDisplay: function(tbtext) {
		if (!this.getRootVM().miniMode) {
			if (this.authorDisplayed)
				this.authorModel.doClose()
			else {
				this.closeComponent()
				this.authorModel = this.open({
					mtype: 'RS.social.Component',
					type: 'user',
					name: this.commentAuthorDisplayName || this.authorDisplayName,
					username: this.commentAuthorUsername || this.authorUsername,
					componentTarget: {
						sysId: this.authorSysId,
						type: 'user'
					},
					target: tbtext,
					currentUser: (this.commentAuthorUsername || this.authorUsername) == this.username,
					following: this.following
				})
			}
			this.set('authorDisplayed', !this.authorDisplayed)
		}
	},
	closeAuthor: function() {
		if (this.authorDisplayed) {
			this.authorModel.doClose()
			this.set('authorDisplayed', false)
		}
	},

	componentDisplayed: false,
	componentModel: null,
	toggleComponentDisplay: function(tbtext) {
		if (!this.getRootVM().miniMode) {
			if (this.componentDisplayed && this.componentModel)
				this.componentModel.doClose()
			else {
				this.closeAuthor()
				if (this.targetList.length > 0) {
					var following = false,
						currentUser = false;
					if (this.isPost) {
						var id = this.targetList.getAt(0).sysId;
						this.getParentVM().streamStore.each(function(stream) {
							if (stream.get('sys_id') == id) {
								if (stream.get('username') == this.username) currentUser = true
								following = true
								return false
							}
						}, this)
					}
					this.componentModel = this.open({
						mtype: 'RS.social.Component',
						type: this.componentType,
						following: following,
						currentUser: currentUser,
						name: this.targetList.getAt(0).name,
						username: this.targetList.getAt(0).name,
						componentTarget: this.targetList.getAt(0),
						target: tbtext
					})
					this.set('componentDisplayed', !this.componentDisplayed)
				}
			}
		}
	},
	closeComponent: function() {
		if (this.componentDisplayed) {
			this.componentModel.doClose()
			this.set('componentDisplayed', false)
		}
	},

	closePopups: function() {
		this.closeLikes()
		this.closeAuthor()
		this.closeComponent()

		this.commentsList.forEach(function(c) {
			c.closePopups()
		})
	},

	sharePost: function() {
		this.getParentVM().switchToPostMessage()
		this.getParentVM().set('content', Ext.String.format('{2} <a href="#RS.social.Main/postId={0}" target="_blank">{1}</a>', this.id, this.subject || Ext.util.Format.htmlEncode(Ext.String.format('<a href="#RS.social.Main/postId={0}" target="_blank">{1}</a>', this.id, this.authorUsername)), this.localize('postFrom')))
		this.getParentVM().set('to', [])
		this.getParentVM().set('toHidden', [])
	},

	showShareLink$: function() {
		return this.isPost && !this.getRootVM().miniMode
	},

	movePost: function() {
		this.open({
			mtype: 'RS.social.MovePost'
		})
	},
	deletePost: function() {
		this.message({
			title: this.localize('confirmPostDeleteTitle'),
			msg: this.localize('confirmPostDeleteBody'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deletePost'),
				no: this.localize('cancel')
			},
			fn: this.confirmDeletePost,
			scope: this
		})
	},
	confirmDeletePost: function(btn) {
		if (btn == 'yes') {
			this.ajax({
				url: '/resolve/service/social/delete',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						clientVM.displaySuccess(this.localize('postDeleted', [this.id]))
						this.getParentVM().removePost(this)
					} else clientVM.displayError(response.message)
				}
			})
		}
	},
	deleteComment: function() {
		this.message({
			title: this.localize('confirmCommentDeleteTitle'),
			msg: this.localize('confirmCommentDeleteBody'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteComment'),
				no: this.localize('cancel')
			},
			fn: this.confirmDeleteComment,
			scope: this
		})
	},
	confirmDeleteComment: function(btn) {
		if (btn == 'yes') {
			this.ajax({
				url: '/resolve/service/social/deleteComment',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						clientVM.displaySuccess(this.localize('commentDeleted', [this.id]))
						this.getParentVM().removePost(this)
					} else clientVM.displayError(response.message)
				}
			})
		}
	},
	toggleReadPost: function() {
		this.set('read', !this.read)
		this.syncMainPostProperties()
		this.ajax({
			url: '/resolve/service/social/setRead',
			params: {
				id: this.id,
				parentId: this.getParentVM().id,
				read: this.read
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (!response.success) {
					this.set('read', !this.read)
					this.syncMainPostProperties()
					clientVM.displayError(response.message)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	removePost: function(post) {
		this.commentsList.remove(post)
	},

	toggleLockPost: function() {
		this.set('locked', !this.shouldBeLocked)
		this.syncMainPostProperties()
		this.ajax({
			url: '/resolve/service/social/setLockPost',
			params: {
				id: this.id,
				parentId: this.getParentVM().id,
				lock: this.shouldBeLocked
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) this.getRootVM().refreshPosts()
				else {
					this.set('locked', !this.shouldBeLocked)
					this.syncMainPostProperties()
					clientVM.displayError(response.message)
				}
			}
		})
	},

	showMove$: function() {
		return !this.getRootVM().miniMode && this.hasDeleteRights && this.type != 'rss' && this.type != 'system' && this.getParentVM().streamStore && this.getParentVM().streamStore.getCount() > 0;
	},

	showLocked$: function() {
		return true //this.type != 'system'
	},

	currentUser$: function() {
		return this.authorUsername == this.username
	},

	following: false,
	toggleFollowAuthor: function() {
		this.ajax({
			url: '/resolve/service/user/getUser',
			params: {
				username: this.authorUsername
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.ajax({
						url: '/resolve/service/social/setFollowStreams',
						params: {
							ids: response.data.id,
							follow: !this.following
						},
						success: function(r) {
							var response = RS.common.parsePayload(r)
							if (response.success) {
								this.set('following', !this.following)
								this.getParentVM().streamStore.load()
							} else clientVM.displayError(response.message)
						},
						failure: function(r) {
							clientVM.displayFailure(r);
						}
					})
				}
			}
		})
	},

	readCls$: function() {
		return 'icon-large post-read ' + (this.read ? 'icon-ok' : 'icon-envelope')
	},
	toggleRead: function() {
		this.set('read', !this.read)
		this.syncMainPostProperties()
		this.ajax({
			url: '/resolve/service/social/setRead',
			params: {
				id: this.id,
				parentId: this.getParentVM().id,
				read: this.read
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (!response.success) {
					this.set('read', !this.read)
					this.syncMainPostProperties()
					clientVM.displayError(response.message)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	likeTooltip$: function() {
		return this.liked ? this.localize('unlikeTooltip') : this.localize('likeTooltip')
	},
	starredTooltip$: function() {
		return this.starred ? this.localize('unstarredTooltip') : this.localize('starredTooltip')
	},
	readTooltip$: function() {
		return this.read ? this.localize('unreadTooltip') : this.localize('readTooltip')
	},

	updatePost: function(post) {
		var realPost = post.raw ? post.raw : post,
			comments = realPost.comments || [],
			contains = false;

		this.set('liked', realPost.liked)
		this.set('read', realPost.read)
		this.set('starred', realPost.starred)
		this.set('numberOfLikes', realPost.numberOfLikes)
		this.set('answer', realPost.answer)

		this.set('targets', realPost.targets)
		this.targetList.removeAll()
		Ext.Array.forEach(this.targets || [], function(target) {
			if (target.sysId == this.authorSysId) target.name = this.localize('blog')
			if (this.getParentVM().activeStream && this.getParentVM().activeStream.id == 'system') target.name = this.localize('system')
			this.targetList.add(this.model(Ext.apply({
				mtype: 'viewmodel'
			}, target)))
			if (target.type && target.type.toLowerCase() == 'forum') this.set('componentType', 'forum')
		}, this)

		if (this.targetList.length > 0) {
			//determine best target to display
			var set = false
			this.targetList.forEach(function(target) {
				if (this.getParentVM().activeStream && target.name == this.getParentVM().activeStream.name) {
					this.set('component', target.type == 'USER' ? target.displayName : target.name)
					set = true
				}
			}, this)
			if (!set) this.set('component', this.targetList.getAt(0).type == 'USER' ? this.targetList.getAt(0).displayName : this.targetList.getAt(0).name)
		} else this.set('component', this.localize('blog'))

		Ext.Array.forEach(comments, function(comment) {
			contains = false
			this.commentsList.forEach(function(c) {
				if (c.id == comment.id) {
					contains = true
					c.updatePost(comment)
					return false
				}
			})

			if (!contains) {
				this.commentsList.add(this.model(Ext.apply({
					mtype: 'RS.social.Post'
				}, comment)))
				if (comment.answer) {
					this.set('answered', true)
					this.answersList.add(this.model(Ext.apply({
						mtype: 'RS.social.Post'
					}, comment)))
				}

			}
		}, this)

		var answered = false;
		this.commentsList.forEach(function(comment) {
			if (comment.answer) answered = true
		})
		this.set('answered', answered)
		this.determineFollowing()
	},

	determineFollowing: function() {
		if (!this.getParentVM().streamStore)
			return;
		var following = false
		if (this.isPost)
			this.getParentVM().streamStore.each(function(stream) {
				if (stream.get('username') == this.authorUsername) {
					following = true
					return false
				}
			}, this)
		this.set('following', following)
	},

	syncMainPostProperties: function() {
		if (this.getParentVM().posts)
			this.getParentVM().posts.forEach(function(post) {
				if (post.id == this.id)
					post.updatePost(this)
			}, this)
	},

	syncPostProperties: function(post) {
		this.set('id', post.id)
		this.set('authorUsername', post.authorUsername)
		this.set('username', post.username)
		this.set('title', post.title)
		this.set('content', post.content)
		this.set('starred', post.starred)
		this.set('numberOfLikes', post.numberOfLikes)
		this.set('liked', post.liked)
		this.set('sysCreatedOn', post.sysCreatedOn)
		this.set('componentType', post.componentType)

		this.targetList.removeAll()
		post.targetList.forEach(function(target) {
			this.targetList.add(target)
		}, this)

		if (this.targetList.length > 0) this.set('component', this.targetList.getAt(0).name)
		else this.set('component', this.localize('blog'))

		var answered = false;
		this.commentsList.removeAll()
		post.commentsList.forEach(function(comment) {
			this.commentsList.add(comment)
			if (comment.answer) answered = true
		}, this)
		this.set('answered', answered)

		this.answersList.removeAll()
		post.answersList.forEach(function(comment) {
			this.answersList.add(comment)
		}, this)
	},

	/*FORUM DISPLAY*/

	isForum$: function() {
		return this.getParentVM().activeStream && this.getParentVM().activeStream.type && this.getParentVM().activeStream.type.toLowerCase() == 'forum' //componentType == 'forum' //&& this.type != 'system'
	},

	showAnswer$: function() {
		return this.isForum
	},

	answered: false,

	answerText$: function() {
		return this.answered ? this.localize('answered') : this.localize('unanswered')
	},

	answerCls$: function() {
		return this.answered ? 'post-forum-answered' : 'post-forum-unanswered'
	},

	subjectClicked: function() {
		if (this.isForum && this.viewMode != 'forum')
			this.getRootVM().switchToForumDisplay(this)
		else
			this.getRootVM().switchToPostDisplay()

		if (this.componentType == 'rss' && this.uri)
			window.open(this.uri, '_blank')
	},

	postForumSubjectCls$: function() {
		return this.isForum || this.componentType == 'rss' ? 'post-forum-subject' : ''
	},

	isForumComment$: function() {
		return !this.isPost && this.getParentVM().isForum && this.getParentVM().currentUser
	},

	answerTextComment$: function() {
		return this.answer ? this.localize('answer') : this.localize('isAnswer')
	},

	commentAnswerCls$: function() {
		return this.answer ? 'post-forum-answered' : ''
	},

	calculateAnswer: function() {
		this.set('answered', false)
		this.answersList.removeAll()
		this.commentsList.forEach(function(comment) {
			if (comment.answer) {
				this.set('answered', true)
				this.answersList.add(comment)
			}
		}, this)
		if (this.getParentVM().activePost && this.getParentVM().activePost.id == this.id) this.getParentVM().activePost.syncPostProperties(this)
	},

	toggleAnswer: function() {
		this.set('answer', !this.answer)
		this.getParentVM().calculateAnswer()
		this.ajax({
			url: '/resolve/service/social/setAnswer',
			params: {
				id: this.id,
				parentId: this.getParentVM().id,
				answer: this.answer
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (!response.success) {
					this.set('answer', !this.answer)
					clientVM.displayError(response.message)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	answersList: {
		mtype: 'list'
	},
	displayAnswers$: function() {
		return this.viewMode == 'forum' && this.answered && this.answersList.length > 0
	},

	forumCountText$: function() {
		if (this.isForum) {
			return Ext.String.format('({0} {1})', this.commentsList.length, this.commentsList.length === 1 ? this.localize('response') : this.localize('responses'))
		}
		return ''
	},

	forumBorderCls$: function() {
		return this.isForum ? 'post-forum-border' : ''
	},

	navigateToProfile: function(username) {
		if (this.getParentVM().navigateToProfile) this.getParentVM().navigateToProfile(username)
		else if (this.getParentVM().parentVM.navigateToProfile) this.getParentVM().parentVM.navigateToProfile(username)
	},

	miniMode$: function() {
		return this.getParentVM().miniMode
	}
})
glu.defModel('RS.social.Stream', {
	id: '',
	name: '',
	displayName: '',
	type: '',
	unReadCount: 0,
	pinned: false,
	isMe: false,
	sysUpdatedOn: new Date(),
	lock: false,
	following: true,

	showDigest: true,
	showEmail: true,

	showTool: false,
	selected: false,
	when_selected_changes_update_root: {
		on: ['selectedChanged'],
		action: function() {
			if (this.selected) this.parentVM.parentVM.set('activeStream', this)
		}
	},
	allowUnfollow$: function() {
		return this.following
	},
	displayTool$: function() {
		return this.showTool || this.selected
	},

	init: function() {
		if (this.parentVM.parentVM.unreadRecords && this.parentVM.parentVM.unreadRecords[this.id])
			this.set('unReadCount', this.parentVM.parentVM.unreadRecords[this.id])

		if (this.componentNotification) {
			Ext.Array.forEach(this.componentNotification.notifyTypes, function(notifyType) {
				if (notifyType.type.indexOf('DIGEST_EMAIL') > -1) this.set('sendDigest', notifyType.value == 'true' || notifyType.value == 'True')
				if (notifyType.type.indexOf('FORWARD_EMAIL') > -1) this.set('sendEmail', notifyType.value == 'true' || notifyType.value == 'True')
			}, this)
		}
		//if (this.sysUpdatedOn > Ext.Date.add(new Date(), Ext.Date.DAY, -14) || this.sysCreatedOn > Ext.Date.add(new Date(), Ext.Date.DAY, -14)) //within the last 2 weeks)
			this.set('initialShow', true)
	},
	getNotification: function(nextAction) {
		if (this.type)
			this.ajax({
				url: '/resolve/service/social/specific/notify/get',
				params: {
					streamId: this.sys_id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						if (response.data) {
							Ext.Array.forEach(response.data.notifyTypes || [], function(notifyType) {
								if (notifyType.type.indexOf(this.type == 'system' ? 'USER_SYSTEM' : this.type) == 0) {
									if (this.type == 'USER') {
										if (notifyType.type.indexOf('USER_ALL') == -1) {
											if (notifyType.type.indexOf('DIGEST_EMAIL') > -1) this.set('sendDigest', notifyType.value == 'true' || notifyType.value == 'True')
											if (notifyType.type.indexOf('FORWARD_EMAIL') > -1) this.set('sendEmail', notifyType.value == 'true' || notifyType.value == 'True')
										}
									} else {
										if (notifyType.type.indexOf('DIGEST_EMAIL') > -1) this.set('sendDigest', notifyType.value == 'true' || notifyType.value == 'True')
										if (notifyType.type.indexOf('FORWARD_EMAIL') > -1) this.set('sendEmail', notifyType.value == 'true' || notifyType.value == 'True')
									}
								}
							}, this)
						}
					} else {
						clientVM.displayError(response.message);
					}

					nextAction();
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		else
			nextAction();
	},
	initialShow: false,
	selectedCss$: function() {
		return this.selected ? 'selected' : 'unselected'
	},
	displayStream$: function() {
		return this.parentVM.showAll || this.pinned || this.isMe || this.initialShow
	},

	displayView$: function() {
		return !this.isMe && this.type && Ext.Array.indexOf(['process', 'team', 'forum', 'rss'], this.type.toLowerCase()) == -1 && !this.parentVM.parentVM.miniMode
	},

	displaySettings$: function() {
		return !this.isMe && this.type && Ext.Array.indexOf(['process', 'team', 'forum', 'rss'], this.type.toLowerCase()) > -1 && !this.parentVM.parentVM.miniMode
	},

	display$: function() {
		var display = this.unReadCount > 0 ? '<b>{0}</b>' : '{0}'
		if (this.type.toLowerCase() == 'user' && this.id != 'inbox') {
			return Ext.String.format(display, Ext.util.Format.ellipsis(this.displayName, this.parentVM.parentVM.miniMode ? 42 : 28, false) + (this.unReadCount > 0 ? ' (' + this.unReadCount + ')' : ''))
		} else {
			return Ext.String.format(display, Ext.util.Format.ellipsis(this.name, this.parentVM.parentVM.miniMode ? 42 : 28, false) + (this.unReadCount > 0 ? ' (' + this.unReadCount + ')' : ''))
		}
	},

	titleDisplay$: function() {
		var display = this.unReadCount > 0 && !this.parentVM.parentVM.miniMode ? '<b>{0}</b>' : '{0}'
		if (this.type.toLowerCase() == 'user' && this.id != 'inbox') {
			return Ext.String.format(display, Ext.util.Format.ellipsis(this.displayName, 15, false) + (this.unReadCount > 0 ? ' (' + this.unReadCount + ')' : ''))
		} else {
			return Ext.String.format(display, Ext.util.Format.ellipsis(this.name, 15, false) + (this.unReadCount > 0 ? ' (' + this.unReadCount + ')' : ''))
		}
	},

	titleFullDisplay$: function() {
		var display = this.unReadCount > 0 ? '<b>{0}</b>' : '{0}'
		if (this.type.toLowerCase() == 'user' && this.id != 'inbox') {
			return Ext.String.format(display, this.displayName + (this.unReadCount > 0 ? ' (' + this.unReadCount + ')' : ''))
		} else {
			return Ext.String.format(display, this.name + (this.unReadCount > 0 ? ' (' + this.unReadCount + ')' : ''))
		}

	},

	isSystem$: function() {
		return this.id == 'system'
	},

	configureNotifications: function() {
		this.open({
			mtype: 'RS.social.ConfigureNotifications'
		})
	},

	viewStream: function() {
		var modelName = RS.social.ComponentMap[this.type.toLowerCase()]
		if (modelName) {
			if (modelName == 'RS.wiki.WikiAdmin') {
				clientVM.handleNavigation({
					modelName: modelName,
					params: {
						ufullname: 'startsWith~' + this.sys_id,
						social: true
					}
				})
			} else if (modelName == 'RS.user.User') {
				clientVM.handleNavigation({
					modelName: modelName,
					params: {
						username: this.username
					}
				})
			} else
				clientVM.handleNavigation({
					modelName: modelName,
					params: {
						id: this.sys_id,
						social: true
					}
				})
		} else
			this.message({
				title: this.localize('unknownComponentTitle'),
				msg: this.localize('unknownComponentMessage', [this.type.toLowerCase()]),
				buttons: Ext.MessageBox.OK
			})
	},
	configureStream: function() {
		var streamTypes = {
			rss: 'rss',
			team: 'teams',
			process: 'process',
			forum: 'forums'
		};
		clientVM.handleNavigation({
			modelName: 'RS.socialadmin.Component',
			params: {
				id: this.id,
				name: streamTypes[this.type.toLowerCase()]
			}
		})
	},
	togglePinned: function() {
		this.ajax({
			url: '/resolve/service/social/setPinned',
			params: {
				id: this.id,
				pinned: !this.pinned
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success)
					this.set('pinned', !this.pinned)
				else
					clientVM.displayError(response.message)
			}
		})
	},
	toggleLocked: function() {
		this.ajax({
			url: '/resolve/service/social/setLockStream',
			params: {
				id: this.id,
				lock: !this.lock
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.set('lock', !this.lock)
					this.parentVM.parentVM.fireEvent('activeStreamChanged')
				} else
					clientVM.displayError(response.message)
			}
		})
	},
	unfollowStream: function() {
		this.ajax({
			url: '/resolve/service/social/setFollowStreams',
			params: {
				ids: [this.id],
				follow: false
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				this.parentVM.parentVM.selectFirstStream()
				this.parentVM.parentVM.refreshStreams()
				if (!response.success) {
					clientVM.displayError(response.message);
				} else {
					this.parentVM.parentVM.updatePagination(this.type);
				}
			}
		})
	},
	markStreamRead: function(date) {
		this.ajax({
			url: '/resolve/service/social/markStreamRead',
			params: {
				id: this.id,
				streamType: this.type,
				olderThanDate: Ext.Date.format(date || new Date(), 'c')
			},
			success: function(r) {
				var response = RS.common.parsePayload(r);
				this.parentVM.parentVM.updateScreen();
				if (response.success) clientVM.displaySuccess(this.localize('markedStreamRead'))
				else clientVM.displayError(response.message)
			}
		})
	},

	updateStream: function(stream) {
		Ext.Object.each(stream, function(key) {
			this.set(key, stream[key])
		}, this)

		if (this.componentNotification) {
			Ext.Array.forEach(this.componentNotification.notifyTypes, function(notifyType) {
				if (notifyType.type.indexOf('DIGEST_EMAIL') > -1) this.set('sendDigest', notifyType.value == 'true' || notifyType.value == 'True')
				if (notifyType.type.indexOf('FORWARD_EMAIL') > -1) this.set('sendEmail', notifyType.value == 'true' || notifyType.value == 'True')
			}, this)
		}
	},

	mouseOver: function() {
		this.set('showTool', true)
	},
	mouseOut: function() {
		this.set('showTool', false)
	},
	select: function() {
		this.parentVM.selectStream(this)
	},
	doubleClick: function() {
		if (this.displayView) {
			this.viewStream()
		}
	},
	popout: function() {
		var params = Ext.Object.fromQueryString(window.location.search),
			debugParam = '';
		if (params.debug || params.debugSocial) debugParam = '&debug=true'
		var child = window.open(Ext.String.format('/resolve/social/social.jsp?v=m&streamId={0}{1}', this.id, debugParam), '_blank', 'height=325,width=400,menubar=no,status=no,toolbar=no');
		// clientVM.childWindows.push(child)
	},
	showPopout$: function() {
		return false;
		// return !this.parentVM.parentVM.miniMode
	},

	sendDigest: false,
	digestChanged: function(checked) {
		if (checked != this.sendDigest) {
			this.ajax({
				url: '/resolve/service/social/specific/notify/updateType',
				params: {
					streamIds: [this.sys_id],
					selectedType: Ext.String.format('{0}_{1}_{2}', this.type == 'system' ? 'USER_SYSTEM' : this.type, 'DIGEST', 'EMAIL'),
					value: checked
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.set('sendDigest', checked)
						clientVM.displaySuccess(this.localize('notificationSettingChanged'))
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},
	sendEmail: false,
	emailForwardChanged: function(checked) {
		if (checked != this.sendEmail) {
			this.ajax({
				url: '/resolve/service/social/specific/notify/updateType',
				params: {
					streamIds: [this.sys_id],
					selectedType: Ext.String.format('{0}_{1}_{2}', this.type == 'system' ? 'USER_SYSTEM' : this.type, 'FORWARD', 'EMAIL'),
					value: checked
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.set('sendEmail', checked)
						clientVM.displaySuccess(this.localize('notificationSettingChanged'))
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},
	supportPaging: function() {
		return (this.type === 'DOCUMENT' || this.type === 'ACTIONTASK');
	}
})

glu.defModel('RS.social.StreamGroup', {
	id: '',
	name: '',
	type: '',

	display: '',

	streams: {
		mtype: 'list'
	},

	store: undefined,

	showAll: false,

	isMe: false,

	hidePagingToolbar$: function () {
		// Currently only Document streams have pagination supported provided that total count is more than
		// default items per page.
		return !((this.type === 'DOCUMENT' && this.parentVM.documentStreamStore.getTotalCount() > this.parentVM.defaultItemsPerPage)
			|| (this.type === 'ACTIONTASK' && this.parentVM.actionTaskStreamStore.getTotalCount() > this.parentVM.defaultItemsPerPage));
	},

	canCollapse$: function() {
		return this.streams.indexOf(this.parentVM.activeStream) == -1
	},

	init: function() {
		this.streams.add(this.model({
			mtype: 'Blank'
		}));
	},

	toggleCollapse: function() {
		var currentlyShown = false;
		this.streams.forEach(function(s) {
			if (Ext.isDefined(s.initialShow)) {
				currentlyShown = currentlyShown || s.initialShow
				s.set('initialShow', false);
			}
		})
		if (currentlyShown)
			this.set('showAll', currentlyShown);
		this.set('showAll', !this.showAll)
	},

	refreshDisplay: function() {
		var count = 0;
		this.streams.forEach(function(stream) {
			count += Number(stream.unReadCount)
		})
		this.set('display', this.name + (count > 0 ? ' (' + count + ')' : ''))
	},

	joinComponent: function(tool) {
		this.open({
			mtype: 'RS.social.StreamPicker',
			type: this.type ? this.type.toLowerCase() : '',
			pickerType: 1
		})
	},

	openComponentSettings: function(tool) {
		debugger;
	},

	registerTool: function(tool) {
		if (this.type == 'Default') tool.hide()
	},

	selectStream: function(stream) {
		this.parentVM.selectStream(stream)
	},

	sendDigest: false,
	digestChanged: function(checked) {
		debugger;
	},
	sendEmail: false,
	emailForwardChanged: function(checked) {
		debugger;
	},

	digestApplied: function() {
		if (this.type == 'NAMESPACE') return
		var streams = [];
		this.streams.forEach(function(stream) {
			streams.push(stream.sys_id)
		})
		this.ajax({
			url: '/resolve/service/social/specific/notify/updateType',
			params: {
				streamIds: streams,
				selectedType: Ext.String.format('{0}_{1}_{2}', this.type, 'DIGEST', 'EMAIL'),
				value: true
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.parentVM.refreshStreams()
					clientVM.displaySuccess(this.localize('notificationSettingChanged'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	unapplyDigest: function() {
		if (this.type == 'NAMESPACE') return
		var streams = [];
		this.streams.forEach(function(stream) {
			streams.push(stream.sys_id)
		})
		this.ajax({
			url: '/resolve/service/social/specific/notify/updateType',
			params: {
				streamIds: streams,
				selectedType: Ext.String.format('{0}_{1}_{2}', this.type, 'DIGEST', 'EMAIL'),
				value: false
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.parentVM.refreshStreams()
					clientVM.displaySuccess(this.localize('notificationSettingChanged'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	emailApplied: function() {
		if (this.type == 'NAMESPACE') return
		var streams = [];
		this.streams.forEach(function(stream) {
			streams.push(stream.sys_id)
		})
		this.ajax({
			url: '/resolve/service/social/specific/notify/updateType',
			params: {
				streamIds: streams,
				selectedType: Ext.String.format('{0}_{1}_{2}', this.type, 'FORWARD', 'EMAIL'),
				value: true
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.parentVM.refreshStreams()
					clientVM.displaySuccess(this.localize('notificationSettingChanged'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	unapplyEmail: function() {
		if (this.type == 'NAMESPACE') return
		var streams = [];
		this.streams.forEach(function(stream) {
			streams.push(stream.sys_id)
		})
		this.ajax({
			url: '/resolve/service/social/specific/notify/updateType',
			params: {
				streamIds: streams,
				selectedType: Ext.String.format('{0}_{1}_{2}', this.type, 'FORWARD', 'EMAIL'),
				value: false
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.parentVM.refreshStreams()
					clientVM.displaySuccess(this.localize('notificationSettingChanged'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	supportPagination: function() {
		return (this.type === 'DOCUMENT' || this.type === 'ACTIONTASK');
	},

	//
	// This method is called after the streams are loaded.
	//
	handleStoreLoaded: function(records) {
		// Remove existing streams from document StreamGroup
		this.streams.removeAll();
		// Add new streams into document StreamGroup
		this.updateStreams(records);
	},

	//
	// This method gets data from the store, converts them into streams.
	//
	priorUnreadRecords: null,
	updateStreams: function(records) {
		if (records) {
			for(var i=0; i<records.length; i++) {
				var stream = records[i];
				if (stream.data.id) {
					this.streams.add(this.model(Ext.apply({
						mtype: 'RS.social.Stream'
					}, stream.data)));
	
				} else {
					// Update unread records by 
					// 1. subtracting what was added
					// 2. adding current unread record of this group
					var priorUnreadRecords = Ext.apply({}, stream.raw);
					if (this.parentVM.unreadRecords) {
						for (key in this.parentVM.unreadRecords) {
							stream.raw[key] = 
								this.parentVM.unreadRecords[key] + stream.raw[key] - (!this.priorUnreadRecords? 0: this.priorUnreadRecords[key]);
						}
					}
					this.parentVM.set('unreadRecords', stream.raw);
					// Update prior unread records
					this.priorUnreadRecords  =  Ext.apply({}, priorUnreadRecords);
				}
			}
		}
	}
});

glu.defModel('RS.social.Blank', {
	selected: false
})
glu.defModel('RS.social.StreamPicker', {

	windowTitle$: function() {
		switch (this.pickerType) {
			case 0:
				return this.localize('addressToDialogWindowTitle')
				break;
			case 1:
				return this.localize('followDialogWindowTitle')
				break;
			case 2:
				return this.localize('addFollowersWindowTitle')
				break;
		}
	},

	activeItem$: function() {
		return this.type == 'worksheet' ? 1 : 0
	},

	/**
		0 - Follow Dialog
		1 - Address Dialog
		2 - Add Followers Dialog
	*/
	pickerType: 0,

	streamId: '',

	toGrid: {
		mtype: 'store',
		sorters: ['displayName'],
		fields: ['id', 'name', 'type', 'displayName'],
		remoteSort: true,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listAllStreams',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	toGridSelections: [],

	toGridColumns: [{
			dataIndex: 'displayName',
			header: '~~name~~',
			flex: 1
		}
		/*, {
		dataIndex: 'type',
		header: '~~type~~'
	}*/
	],

	type: '',
	typeIsVisible: true,
	typeStore: {
		mtype: 'store',
		fields: ['name', {
			name: 'value',
			convert: function(v) {
				return (v || '').toLowerCase()
			}
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listComponentTypes',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	searchQuery: '',

	worksheets: {
		mtype: 'store',
		remoteSort: true,
		sorters: [{
			property: 'sysUpdatedOn',
			direction: 'DESC'
		}],
		fields: ['id', 'sysId', 'number', 'reference', 'correlationId', 'alertId', 'wsData_dt_dtList'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/worksheet/list',			
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	worksheetsSelections: [],
	worksheetsColumns: [],

	myWorksheetsIsPressed: false,
	myWorksheets: function() {
		this.set('myWorksheetsIsPressed', !this.myWorksheetsIsPressed)
	},
	myWorksheetsPressedCls$: function() {
		return this.myWorksheetsIsPressed ? 'rs-social-button-pressed' : ''
	},
	myWorksheetsTooltip$: function() {
		return this.myWorksheetsIsPressed ? this.localize('showAllWorksheets') : this.localize('showMyWorksheets')
	},

	searchText: '',
	when_searchText_or_my_worksheets_changes_update_worksheet_grid: {
		on: ['searchTextChanged', 'myWorksheetsIsPressedChanged'],
		action: function() {
			this.worksheets.load()
		}
	},

	init: function() {
		if (this.pickerType == 2) {
			this.type = 'user'
			this.set('typeIsVisible', false)
		}

		this.toGrid.on('beforeload', function(store, operation, eOpts) {
			var filters = [];

			if (this.type) filters.push({
				field: 'type',
				condition: 'equals',
				value: this.type
			})

			if (this.searchQuery) filters.push({
				field: 'name',
				condition: 'startsWith',
				value: this.searchQuery
			})

			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				filter: Ext.encode(filters)
			})
		}, this)

		this.typeStore.on('load', function(store, records) {
			Ext.Array.forEach(records || [], function(record) {
				record.set('name', this.localize(record.get('name').toLowerCase()))
			}, this)

			//Reorder the components so that our groups are on top
			for (var i = 0; i < records.length; i++) {
				if (records[i].get('value').toLowerCase() == 'forum')
					records.splice(0, 0, records.splice(i, 1)[0])
				if (records[i].get('value').toLowerCase() == 'process')
					records.splice(1, 0, records.splice(i, 1)[0])
				if (records[i].get('value').toLowerCase() == 'team')
					records.splice(2, 0, records.splice(i, 1)[0])
			}
			store.loadData(records)
		}, this)


		this.typeStore.load()

		this.set('worksheetsColumns', [{
			dataIndex: 'number',
			text: '~~number~~',
			filterable: false,
			sortable: true,
			flex: 1
		}, {
			dataIndex: 'reference',
			text: '~~reference~~',
			filterable: false,
			sortable: false,
			flex: 1,
			renderer: function(value, metaData, record) {
				return value || record.get('correlationId') || record.get('alertId') || ''
			}
		}, {
			text: RS.common.locale.sysUpdated,
			dataIndex: 'sysUpdatedOn',
			filterable: false,
			sortable: true,
			width: 200,
			renderer: Ext.util.Format.dateRenderer('M j Y, h:i:s.u')
		}])

		this.worksheets.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}

			var filter = [];
			if (this.searchText) {
				filter = [{
					field: 'alertId',
					type: 'auto',
					condition: 'contains',
					value: this.searchText
				}, {
					field: 'correlationId',
					type: 'auto',
					condition: 'contains',
					value: this.searchText
				}, {
					field: 'number',
					type: 'auto',
					condition: 'contains',
					value: this.searchText
				}, {
					field: 'reference',
					type: 'auto',
					condition: 'contains',
					value: this.searchText
				}]
			}

			if (this.myWorksheetsIsPressed) {
				filter.push({
					field: 'assignedToName',
					type: 'auto',
					condition: 'equals',
					value: clientVM.user.name
				})
			}

			Ext.apply(operation.params, {
				operator: 'or',
				filter: Ext.encode(filter),
				'RESOLVE.ORG_NAME' : clientVM.orgName,
				'RESOLVE.ORG_ID' : clientVM.orgId
			})
		}, this)

		this.worksheets.load()

		if (!this.type)
			this.typeStore.on('load', function(store, records) {
				if (records.length > 0) {
					Ext.Array.forEach(records || [], function(record) {
							if (record.get('value').toLowerCase() == 'team')
								this.set('type', record.get('value'))
						}, this)
						// this.set('type', records[0].get('value'))
				}
			}, this, {
				single: true
			})
		else
			this.toGrid.load()
	},

	when_filter_changes_reload_grid: {
		on: ['typeChanged', 'searchQueryChanged'],
		action: function() {
			this.toGrid.loadPage(1)
		}
	},

	search: function() {
		this.toGrid.load()
	},

	select: function() {
		var selections = [];
		if (this.parentVM.to)
			Ext.Array.forEach(this.parentVM.to, function(to) {
				selections.push(to)
			}, this)
		Ext.Array.forEach(this.type == 'worksheet' ? this.worksheetsSelections : this.toGridSelections, function(selection) {
			var contains = false
			Ext.Array.forEach(selections, function(sel) {
				if (sel == selection || (sel.get && sel.get('id') == selection)) contains = true
			})
			if (!contains) {
				if (this.type == 'worksheet') Ext.apply(selection.data, {
					name: selection.get('number'),
					displayName: selection.get('number'),
					type: 'worksheet'
				})
				selections.push(selection)
			}
		}, this)
		this.parentVM.set('toHidden', selections)
		this.doClose()
	},
	selectIsVisible$: function() {
		return this.pickerType == 0
	},
	selectIsEnabled$: function() {
		return this.type == 'worksheet' ? this.worksheetsSelections.length > 0 : this.toGridSelections.length > 0
	},

	follow: function() {
		//Call in to the server to add these streams to the user
		var selections = [];
		Ext.Array.forEach(this.type == 'worksheet' ? this.worksheetsSelections : this.toGridSelections, function(selection) {
			selections.push(selection.get('id'))
		})
		this.ajax({
			url: '/resolve/service/social/setFollowStreams',
			params: {
				ids: selections,
				follow: true,
				streamType: this.type
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.rootVM.refreshStreams()
					this.doClose()
				} else clientVM.displayError(response.message)
			}
		})
	},
	followIsVisible$: function() {
		return this.pickerType == 1
	},
	followIsEnabled$: function() {
		return this.selectIsEnabled
	},

	addFollowers: function() {
		//Call in to the server to add these streams to the user
		var selections = [];
		Ext.Array.forEach(this.type == 'worksheet' ? this.worksheetsSelections : this.toGridSelections, function(selection) {
			selections.push(selection.get('id'))
		})
		this.ajax({
			url: '/resolve/service/social/setFollowers',
			params: {
				streamIds: [this.streamId],
				streamType: this.streamType,
				ids: selections,
				follow: true
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.doClose()
				}
			}
		})
	},
	addFollowersIsVisible$: function() {
		return this.pickerType == 2
	},
	addFollowersIsEnabled$: function() {
		return this.selectIsEnabled
	},

	cancel: function() {
		this.doClose()
	},

	createComponent: function() {
		this.parentVM.open({
			mtype: 'RS.socialadmin.CreateComponent',
			type: this.type
		})
		this.doClose()
	},
	createComponentIsVisible$: function() {
		return this.followIsVisible && (hasPermission('admin') || hasPermission('socialadmin'));
	}
})
/* Deprecated feature
glu.defView('RS.social.AttachFile', {

	asWindow: {
		width: 600,
		height: 400,
		title: '~~attachFileWindowTitle~~'
	},

	layout: 'fit',
	items: [{
		xtype: 'uploadmanager',
		allowUpload: true,
		resizable: false,
		fileServiceListUrl: '/resolve/service/social/attachmentList',
		fileServiceUploadUrl: '/resolve/service/social/upload',
		fileUploadTableName: 'SocialAttachment',
		selType: 'rowmodel',
		preventLoading: true,
		selModel: {
			mode: 'MULTI'
		},
		recordId: true,
		uploader: {
			url: '/resolve/service/social/upload',
			uploadpath: 'dev',
			autoStart: true,
			max_file_size: '2020mb',
			flash_swf_url: '/resolve/js/plupload/Moxie.swf',
			urlstream_upload: true
		},
		listeners: {
			selectionchange: '@{selectionChanged}',
			itemdblclick: '@{selectRecord}',
			uploadcomplete: function(uploader, files) {
				if (files.length > 0) this.getSelectionModel().select(this.getStore().getAt(this.getStore().find('sys_id', files[files.length - 1].sys_id)))
			}
		}
	}],

	buttonAlign: 'left',
	buttons: ['attach', 'cancel']
})
*/

glu.defView('RS.social.Component', {
	asWindow: {
		height: 162,
		width: 350,
		title: '@{name}',
		draggable: false,
		baseCls: Ext.isIE8m ? 'x-window' : 'x-panel',
		ui: Ext.isIE8m ? 'default' : 'social-component-dialog',
		displayTarget: '@{target}',
		floatable: false,
		listeners: {
			afterrender: function(panel) {
				panel.getEl().on('mouseout', function(e) {
					Ext.defer(function() {
						if (!Ext.EventObject.within(panel.getEl()))
							panel.fireEvent('mouseClose', panel)
					}, 1000, panel)
				})
			},
			show: function(panel) {
				if (panel.displayTarget.getY() <= panel.getHeight())
					panel.alignTo(panel.displayTarget, 'tl-bl')
				else panel.alignTo(panel.displayTarget, 'bl-tl')
			},
			mouseClose: '@{close}'
		}
	},
	bodyPadding: '10px',
	bodyCls: 'component-description',
	html: '@{componentDescription}',
	buttons: {
		xtype: 'toolbar',
		cls: 'component-toolbar-footer',
		hidden: '@{currentUser}',
		items: ['->', {
			xtype: 'tbtext',
			text: '@{viewText}',
			cls: 'rs-link',
			listeners: {
				render: function(item) {
					item.getEl().on('click', function() {
						item.fireEvent('view', item)
					})
				},
				view: '@{view}'
			}
		}, {
			xtype: 'panel',
			html: '@{followText}',
			width: 90,
			style: 'background:transparent !important',
			bodyStyle: 'background:transparent !important',
			listeners: {
				render: function(item) {
					item.getEl().on('click', function() {
						item.fireEvent('toggleFollowing', item)
					})
				},
				toggleFollowing: '@{toggleFollowing}'
			}
		}]
	}
})
glu.defView('RS.social.ConfigureNotifications', {
	asWindow: {
		title: '~~configureNotificationsTitle~~',
		width: 500,
		height: 400,
		modal: true
	},
	buttonAlign: 'left',
	buttons: ['save', 'cancel'],
	bodyPadding: '10px',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'combo',
		name: 'type',
		store: '@{typeStore}',
		displayField: 'name',
		valueField: 'value',
		queryMode: 'local'
	}, {
		layout: 'card',
		activeItem: '@{activeItem}',
		flex: 1,
		items: [{
			title: '~~PROCESS~~',
			defaultType: 'checkbox',
			autoScroll: true,
			bodyPadding: '10px',
			defaults: {
				hideLabel: true
			},
			items: [{
					name: 'selectAllProcess',
					boxLabel: '~~selectAllProcess~~'
				}, {
					name: 'PROCESS_CREATE',
					boxLabel: '~~PROCESS_CREATE~~'
				}, {
					name: 'PROCESS_UPDATE',
					boxLabel: '~~PROCESS_UPDATE~~'
				}, {
					name: 'PROCESS_PURGED',
					boxLabel: '~~PROCESS_PURGED~~'
				}, {
					name: 'PROCESS_DOCUMENT_ADDED',
					boxLabel: '~~PROCESS_DOCUMENT_ADDED~~'
				}, {
					name: 'PROCESS_DOCUMENT_REMOVED',
					boxLabel: '~~PROCESS_DOCUMENT_REMOVED~~'
				}, {
					name: 'PROCESS_ACTIONTASK_ADDED',
					boxLabel: '~~PROCESS_ACTIONTASK_ADDED~~'
				}, {
					name: 'PROCESS_ACTIONTASK_REMOVED',
					boxLabel: '~~PROCESS_ACTIONTASK_REMOVED~~'
				}, {
					name: 'PROCESS_RSS_ADDED',
					boxLabel: '~~PROCESS_RSS_ADDED~~'
				}, {
					name: 'PROCESS_RSS_REMOVED',
					boxLabel: '~~PROCESS_RSS_REMOVED~~'
				}, {
					name: 'PROCESS_FORUM_ADDED',
					boxLabel: '~~PROCESS_FORUM_ADDED~~'
				}, {
					name: 'PROCESS_FORUM_REMOVED',
					boxLabel: '~~PROCESS_FORUM_REMOVED~~'
				}, {
					name: 'PROCESS_TEAM_ADDED',
					boxLabel: '~~PROCESS_TEAM_ADDED~~'
				}, {
					name: 'PROCESS_TEAM_REMOVED',
					boxLabel: '~~PROCESS_TEAM_REMOVED~~'
				}, {
					name: 'PROCESS_USER_ADDED',
					boxLabel: '~~PROCESS_USER_ADDED~~'
				}, {
					name: 'PROCESS_USER_REMOVED',
					boxLabel: '~~PROCESS_USER_REMOVED~~'
				}
				/*, {
				name: 'PROCESS_DIGEST_EMAIL',
				boxLabel: '~~PROCESS_DIGEST_EMAIL~~'
			}, {
				name: 'PROCESS_FORWARD_EMAIL',
				boxLabel: '~~PROCESS_FORWARD_EMAIL~~'
			}*/
			]
		}, {
			title: '~~TEAM~~',
			defaultType: 'checkbox',
			autoScroll: true,
			bodyPadding: '10px',
			defaults: {
				hideLabel: true
			},
			items: [{
					name: 'selectAllTeam',
					boxLabel: '~~selectAllTeam~~'
				}, {
					name: 'TEAM_CREATE',
					boxLabel: '~~TEAM_CREATE~~'
				}, {
					name: 'TEAM_UPDATE',
					boxLabel: '~~TEAM_UPDATE~~'
				}, {
					name: 'TEAM_PURGED',
					boxLabel: '~~TEAM_PURGED~~'
				}, {
					name: 'TEAM_ADDED_TO_PROCESS',
					boxLabel: '~~TEAM_ADDED_TO_PROCESS~~'
				}, {
					name: 'TEAM_REMOVED_FROM_PROCESS',
					boxLabel: '~~TEAM_REMOVED_FROM_PROCESS~~'
				}, {
					name: 'TEAM_ADDED_TO_TEAM',
					boxLabel: '~~TEAM_ADDED_TO_TEAM~~'
				}, {
					name: 'TEAM_REMOVED_FROM_TEAM',
					boxLabel: '~~TEAM_REMOVED_FROM_TEAM~~'
				}, {
					name: 'TEAM_USER_ADDED',
					boxLabel: '~~TEAM_USER_ADDED~~'
				}, {
					name: 'TEAM_USER_REMOVED',
					boxLabel: '~~TEAM_USER_REMOVED~~'
				}
				/*, {
				name: 'TEAM_DIGEST_EMAIL',
				boxLabel: '~~TEAM_DIGEST_EMAIL~~'
			}, {
				name: 'TEAM_FORWARD_EMAIL',
				boxLabel: '~~TEAM_FORWARD_EMAIL~~'
			}*/
			]
		}, {
			title: '~~FORUM~~',
			defaultType: 'checkbox',
			autoScroll: true,
			bodyPadding: '10px',
			defaults: {
				hideLabel: true
			},
			items: [{
					name: 'selectAllForum',
					boxLabel: '~~selectAllForum~~'
				}, {
					name: 'FORUM_CREATE',
					boxLabel: '~~FORUM_CREATE~~'
				}, {
					name: 'FORUM_UPDATE',
					boxLabel: '~~FORUM_UPDATE~~'
				}, {
					name: 'FORUM_PURGED',
					boxLabel: '~~FORUM_PURGED~~'
				}, {
					name: 'FORUM_ADDED_TO_PROCESS',
					boxLabel: '~~FORUM_ADDED_TO_PROCESS~~'
				}, {
					name: 'FORUM_REMOVED_FROM_PROCESS',
					boxLabel: '~~FORUM_REMOVED_FROM_PROCESS~~'
				}, {
					name: 'FORUM_USER_ADDED',
					boxLabel: '~~FORUM_USER_ADDED~~'
				}, {
					name: 'FORUM_USER_REMOVED',
					boxLabel: '~~FORUM_USER_REMOVED~~'
				}
				/*, {
				name: 'FORUM_DIGEST_EMAIL',
				boxLabel: '~~FORUM_DIGEST_EMAIL~~'
			}, {
				name: 'FORUM_FORWARD_EMAIL',
				boxLabel: '~~FORUM_FORWARD_EMAIL~~'
			}*/
			]
		}, {
			title: '~~USER~~',
			defaultType: 'checkbox',
			autoScroll: true,
			bodyPadding: '10px',
			defaults: {
				hideLabel: true
			},
			items: [{
					name: 'selectAllUser',
					boxLabel: '~~selectAllUser~~'
				}, {
					name: 'USER_ADDED_TO_PROCESS',
					boxLabel: '~~USER_ADDED_TO_PROCESS~~'
				}, {
					name: 'USER_ADDED_TO_TEAM',
					boxLabel: '~~USER_ADDED_TO_TEAM~~'
				}, {
					name: 'USER_FOLLOW_ME',
					boxLabel: '~~USER_FOLLOW_ME~~'
				}, {
					name: 'USER_REMOVED_FROM_PROCESS',
					boxLabel: '~~USER_REMOVED_FROM_PROCESS~~'
				}, {
					name: 'USER_REMOVED_FROM_TEAM',
					boxLabel: '~~USER_REMOVED_FROM_TEAM~~'
				}, {
					name: 'USER_UNFOLLOW_ME',
					boxLabel: '~~USER_UNFOLLOW_ME~~'
				}, {
					name: 'USER_ADDED_TO_FORUM',
					boxLabel: '~~USER_ADDED_TO_FORUM~~'
				}, {
					name: 'USER_REMOVED_FROM_FORUM',
					boxLabel: '~~USER_REMOVED_FROM_FORUM~~'
				}, {
					name: 'USER_PROFILE_CHANGE',
					boxLabel: '~~USER_PROFILE_CHANGE~~'
				}
				/*, {
				name: 'USER_DIGEST_EMAIL',
				boxLabel: '~~USER_DIGEST_EMAIL~~'
			}, {
				name: 'USER_FORWARD_EMAIL',
				boxLabel: '~~USER_FORWARD_EMAIL~~'
			}*/
			]
		}, {
			title: '~~ACTIONTASK~~',
			defaultType: 'checkbox',
			autoScroll: true,
			bodyPadding: '10px',
			defaults: {
				hideLabel: true
			},
			items: [{
					name: 'selectAllActionTask',
					boxLabel: '~~selectAllActionTask~~'
				}, {
					name: 'ACTIONTASK_CREATE',
					boxLabel: '~~ACTIONTASK_CREATE~~'
				}, {
					name: 'ACTIONTASK_UPDATE',
					boxLabel: '~~ACTIONTASK_UPDATE~~'
				}, {
					name: 'ACTIONTASK_PURGED',
					boxLabel: '~~ACTIONTASK_PURGED~~'
				}, {
					name: 'ACTIONTASK_ADDED_TO_PROCESS',
					boxLabel: '~~ACTIONTASK_ADDED_TO_PROCESS~~'
				}, {
					name: 'ACTIONTASK_REMOVED_FROM_PROCESS',
					boxLabel: '~~ACTIONTASK_REMOVED_FROM_PROCESS~~'
				}
				/*, {
				name: 'ACTIONTASK_DIGEST_EMAIL',
				boxLabel: '~~ACTIONTASK_DIGEST_EMAIL~~'
			}, {
				name: 'ACTIONTASK_FORWARD_EMAIL',
				boxLabel: '~~ACTIONTASK_FORWARD_EMAIL~~'
			}*/
			]
		}, {
			title: '~~RUNBOOK~~',
			defaultType: 'checkbox',
			autoScroll: true,
			bodyPadding: '10px',
			defaults: {
				hideLabel: true
			},
			items: [{
					name: 'selectAllRunbook',
					boxLabel: '~~selectAllRunbook~~'
				}, {
					name: 'RUNBOOK_UPDATE',
					boxLabel: '~~RUNBOOK_UPDATE~~'
				}, {
					name: 'RUNBOOK_ADDED_TO_PROCESS',
					boxLabel: '~~RUNBOOK_ADDED_TO_PROCESS~~'
				}, {
					name: 'RUNBOOK_REMOVED_FROM_PROCESS',
					boxLabel: '~~RUNBOOK_REMOVED_FROM_PROCESS~~'
				}, {
					name: 'RUNBOOK_COMMIT',
					boxLabel: '~~RUNBOOK_COMMIT~~'
				}
				/*, {
				name: 'RUNBOOK_DIGEST_EMAIL',
				boxLabel: '~~RUNBOOK_DIGEST_EMAIL~~'
			}, {
				name: 'RUNBOOK_FORWARD_EMAIL',
				boxLabel: '~~RUNBOOK_FORWARD_EMAIL~~'
			}*/
			]
		}, {
			title: '~~DOCUMENT~~',
			defaultType: 'checkbox',
			autoScroll: true,
			bodyPadding: '10px',
			defaults: {
				hideLabel: true
			},
			items: [{
					name: 'selectAllDocument',
					boxLabel: '~~selectAllDocument~~'
				}, {
					name: 'DOCUMENT_CREATE',
					boxLabel: '~~DOCUMENT_CREATE~~'
				}, {
					name: 'DOCUMENT_UPDATE',
					boxLabel: '~~DOCUMENT_UPDATE~~'
				}, {
					name: 'DOCUMENT_DELETE',
					boxLabel: '~~DOCUMENT_DELETE~~'
				}, {
					name: 'DOCUMENT_PURGED',
					boxLabel: '~~DOCUMENT_PURGED~~'
				}, {
					name: 'DOCUMENT_ADDED_TO_PROCESS',
					boxLabel: '~~DOCUMENT_ADDED_TO_PROCESS~~'
				}, {
					name: 'DOCUMENT_REMOVED_FROM_PROCESS',
					boxLabel: '~~DOCUMENT_REMOVED_FROM_PROCESS~~'
				}, {
					name: 'DOCUMENT_COMMIT',
					boxLabel: '~~DOCUMENT_COMMIT~~'
				}
				/*, {
				name: 'DOCUMENT_DIGEST_EMAIL',
				boxLabel: '~~DOCUMENT_DIGEST_EMAIL~~'
			}, {
				name: 'DOCUMENT_FORWARD_EMAIL',
				boxLabel: '~~DOCUMENT_FORWARD_EMAIL~~'
			}*/
			]
		}, {
			title: '~~DECISIONTREES~~',
			defaultType: 'checkbox',
			autoScroll: true,
			bodyPadding: '10px',
			defaults: {
				hideLabel: true
			},
			items: []
		}, {
			title: '~~RSS~~',
			defaultType: 'checkbox',
			autoScroll: true,
			bodyPadding: '10px',
			defaults: {
				hideLabel: true
			},
			items: [{
					name: 'selectAllRSS',
					boxLabel: '~~selectAllRSS~~'
				}, {
					name: 'RSS_CREATE',
					boxLabel: '~~RSS_CREATE~~'
				}, {
					name: 'RSS_UPDATE',
					boxLabel: '~~RSS_UPDATE~~'
				}, {
					name: 'RSS_PURGED',
					boxLabel: '~~RSS_PURGED~~'
				}, {
					name: 'RSS_ADDED_TO_PROCESS',
					boxLabel: '~~RSS_ADDED_TO_PROCESS~~'
				}, {
					name: 'RSS_REMOVED_FROM_PROCESS',
					boxLabel: '~~RSS_REMOVED_FROM_PROCESS~~'
				}
				/*, {
				name: 'RSS_DIGEST_EMAIL',
				boxLabel: '~~RSS_DIGEST_EMAIL~~'
			}, {
				name: 'RSS_FORWARD_EMAIL',
				boxLabel: '~~RSS_FORWARD_EMAIL~~'
			}*/
			]
		}]
	}]
})
/* Deprecated feature
glu.defView('RS.social.CreateLink', {
	asWindow: {
		title: '~~createLinkTitle~~',
		width: 850,
		height: '@{height}',
		modal: true,
		listeners: {
			resize: function(win) {
				win.center()
			}
		}
	},

	layout: 'fit',
	items: [{
		xtype: 'form',
		bodyPadding: '10px',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'textfield',
			name: 'display',
			labelWidth: 125
		}, {
			labelWidth: 125,
			xtype: 'fieldcontainer',
			fieldLabel: '~~linkTo~~',
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: '@{componentTypes}'
		}, {
			xtype: 'textfield',
			labelAlign: 'top',
			name: 'url',
			fieldLabel: '~~urlLabel~~',
			labelSeparator: '',
			keyDelay: 1
		}, {
			xtype: 'grid',
			name: 'toGrid',
			scroll: true,
			flex: 1,
			columns: '@{toGridColumns}',
			selType: 'rowmodel',
			dockedItems: [{
				name: 'actionBar',
				xtype: 'toolbar',
				items: [{
					xtype: 'combobox',
					name: 'type',
					emptyText: '~~type~~',
					hideLabel: true,
					displayField: 'name',
					valueField: 'value',
					queryMode: 'local'
				}, {
					xtype: 'textfield',
					name: 'searchQuery',
					hideLabel: true,
					flex: 1,
					minWidth: 250,
					emptyText: '~~searchQuery~~'
				}, {
					xtype: 'button',
					handler: '@{search}',
					iconCls: 'icon-search rs-icon-button'
				}]
			}],
			plugins: [{
				ptype: 'pager',
				showSysInfo: false
			}]
		}]
	}],
	buttonAlign: 'left',
	buttons: ['createLink', 'cancel']
})

glu.defView('RS.social.ComponentType', {
	xtype: 'radio',
	name: 'type',
	boxLabel: '@{name}',
	padding: '0px 20px 0px 0px',
	inputValue: '@{inputValue}',
	value: '@{value}'
})
*/


glu.defView('RS.social.Followers', {
	minHeight: 250,
	width: 600,
	modal : true,
	padding : 15,
	title: '~~followers~~',
	ui: '@{ui}',
	baseCls: '@{baseCls}',
	draggable: '@{draggable}',
	cls : 'rs-modal-popup',	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	autoScroll: true,
	items: '@{followers}',
	bodyPadding: '10px',
	itemTemplate: {
		layout: {
			type: 'hbox',
			align: 'middle'
		},
		padding: '0px 0px 10px 0px',
		items: [{
			xtype: 'image',
			width: 46,
			height: 46,
			margin: '0px 10px 0px 0px',
			src: '@{followerProfileImage}'
		}, {
			xtype: 'label',
			cls: 'like-username',
			text: '@{name}',
			flex: 1
		}, {
			html: '@{followText}',
			listeners: {
				render: function(item) {
					item.getEl().on('click', function() {
						item.fireEvent('toggleFollowing', item)
					})
				},
				toggleFollowing: '@{toggleFollowing}'
			}
		}]
	},	
	buttons: [/*'follow', 'unfollow', */
	{
		name :'addMoreFollowers',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		cls : 'rs-med-btn rs-btn-light',
		name : 'close'
	}]
})
glu.defView('RS.social.Likes', {
	asWindow: {
		baseCls: 'x-panel',
		ui: 'social-likes-dialog',
		height: 215,
		width: 325,
		title: '~~peopleWhoLikeThis~~',
		draggable: false,
		displayTarget: '@{target}',
		floatable: false,
		listeners: {
			afterrender: function(panel) {
				panel.getEl().on('mouseout', function(e) {
					Ext.defer(function() {
						if (!Ext.EventObject.within(panel.getEl()))
							panel.fireEvent('mouseClose', panel)
					}, 1000, panel)
				})
			},
			show: function(panel) {
				panel.alignTo(panel.displayTarget, 'tr-br')
			},
			mouseClose: '@{close}'
		}
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	autoScroll: true,
	items: '@{likes}',
	bodyPadding: '10px',
	itemTemplate: {
		layout: {
			type: 'hbox',
			align: 'middle'
		},
		padding: '0px 0px 10px 0px',
		items: [{
			xtype: 'image',
			width: 46,
			height: 46,
			margin: '0px 10px 0px 0px',
			src: '@{likeProfileImage}'
		}, {
			xtype: 'label',
			cls: 'like-username',
			text: '@{displayName}',
			flex: 1,
			listeners: {
				render: function(label) {
					label.getEl().on('click', function() {
						label.fireEvent('displayUser', label)
					})
				},
				displayUser: '@{displayUser}'
			}
		}, {
			html: '@{followText}',
			minWidth: 85,
			listeners: {
				render: function(item) {
					item.getEl().on('click', function() {
						item.fireEvent('toggleFollowing', item)
					})
				},
				toggleFollowing: '@{toggleFollowing}'
			}
		}]
	}
})
glu.defView('RS.social.Main', {
	layout: 'border',
	itemId: 'RSsocialMain',
	screenName: 'RS.social.Main',
	items: [{
		xtype: 'container',
		hidden: '@{!showStreams}',
		region: 'west',
		width: 275,
		split: true,
		layout: {
			type: 'vbox',
			align: 'center'
		},
		items: [{
			flex: 1,
			width: '100%',
			ui: 'social-navigation-header',
			title: '~~streams~~',
			items: '@{groups}',
			bodyPadding: '0px 0px 0px 5px',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			autoScroll: true,
			tools: [{
				type: 'up',
				style: 'top: 0px !important',
				handler: function() {
					this.getEl().select('img').toggleCls('x-tool-up')
					this.getEl().select('img').toggleCls('x-tool-down')
					this.up('panel').fireEvent('toggleStreams', this, !this.isUp)
					this.getEl().set({
						'data-qtip': this.isUp ? RS.social.locale.showAll : RS.social.locale.showActive
					})
					this.isUp = !this.isUp
				},
				listeners: {
					afterrender: function() {
						this.getEl().set({
							'data-qtip': RS.social.locale.showAll
						})
					}
				}
			}, {
				type: 'plus',
				cls: 'social-main-follow icon-share-alt',
				style: 'top:0px !important',
				handler: function() {
					this.up('panel').fireEvent('followClicked', this, this)
				},
				listeners: {
					render: function(tool) {
						Ext.create('Ext.tip.ToolTip', {
							html: RS.social.locale.followStream,
							target: tool.getEl()
						})
						// tool.up('panel').fireEvent('registerTool', tool, tool)
					}
				}
			}, {
				type: 'refresh',
				style: 'top: 0px !important',
				handler: function() {
					this.up('panel').fireEvent('refreshStreams', this)
				},
				listeners: {
					afterrender: function(button) {
						this.getEl().set({
							'data-qtip': RS.social.locale.refreshStreams
						});
						var view = this.up('#RSsocialMain');
						// Pass the screen name to fix social is rendered when wiki main is rendered.
						// This is the only place found so far having this issue.
						clientVM.updateRefreshButtons(button, view.screenName);
					}
				}
			}],
			listeners: {
				refreshStreams: '@{refreshStreams}',
				toggleStreams: '@{toggleStreams}',
				followClicked: '@{joinComponent}'
			}
		}]
	}, {
		xtype: 'container',
		region: 'center',
		itemId: 'centerRegion',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		autoScroll: true,
		minHeight: 200,
		items: [{
			xtype: 'container',

			layout: {
				type: 'hbox',
				align: 'top'
			},
			items: [{
				layout: {
					type: 'card',
					deferredRender: true
				},
				flex: 1,
				activeItem: '@{activePostCard}',
				items: [{
					xtype: 'container',
					padding: '10px',
					layout: {
						type: 'hbox'
					},
					height: 35,
					listeners: {
						activate: function(panel) {
							if (panel.ownerCt.originalHeight) panel.ownerCt.setHeight(panel.ownerCt.originalHeight)
						}
					},
					items: [{
						xtype: 'toolbar',
						items: [{
							xtype: 'button',
							iconCls: 'icon-large icon-eject rs-icon',
							name: 'switchToPostDisplay'
						}]
					}, {
						xtype: 'textfield',
						flex: 1,
						itemId: 'postYourMessageBox',
						emptyText: '@{postYourMessageDisplay}',
						setEmptyText: function(value) {
							this.emptyText = value
							this.applyEmptyText()
						},
						hidden: '@{!showPostActionToolbarActions}',
						disabled: '@{!postYourMessageBoxReady}',
						inputCls: 'post-message',
						resizeForPost: function() {
							this.ownerCt.ownerCt.originalHeight = this.ownerCt.ownerCt.getHeight()
							this.ownerCt.ownerCt.setHeight(Ext.getBody().getHeight() * 0.5)
						},
						listeners: {
							postMessage: '@{switchToPostMessage}',
							render: function(field) {
								field.getEl().on('click', function() {
									field.fireEvent('postMessage', field)
									// field.ownerCt.ownerCt.animate({
									// 	to: {
									// 		height: Ext.getBody().getHeight() * 0.5 //50% of the document's height
									// 	},
									// 	duration: 500,
									// 	easing: 'backOut',
									// 	dynamic: true
									// })
								})
							}
						}
					}]
				}, {
					xtype: 'form',
					itemId: 'postForm',
					bodyPadding: '10px',
					defaults: {
						anchor: '100%'
					},
					items: [{
						xtype: 'comboboxselect',
						forceSelection: false,
						hideTrigger: true,
						itemId: 'toField',
						store: '@{streamToStore}',
						displayField: 'displayName',
						valueField: 'id',
						name: 'to',
						queryMode: 'local',
						typeAhead: true,
						typeAheadDelay: 500,
						multiSelect: true,
						tpl: '<tpl for="."><div class="x-boundlist-item">{displayName} {[values["type"] ? "(" + values["type"] + ")" : ""]}</div></tpl>',
						listeners: {
							render: function(field) {
								field.labelEl.on({
									scope: field,
									mouseover: function() {
										this.labelEl.addCls('toLabelOver')
									},
									mouseout: function() {
										this.labelEl.removeCls('toLabelOver')
									},
									click: function() {
										field.fireEvent('showAdvancedTo', field)
									}
								})
							},
							select: function() {
								this.collapse()
							},
							showAdvancedTo: '@{showAdvancedTo}'
						}
					}, {
						xtype: 'hidden',
						name: 'toHidden',
						setValue: function(value) {
							var form = this.up('form');
							if (form) form.down('#toField').setValue(value)
						}
					}, {
						xtype: 'textfield',
						itemId: 'subject',
						name: 'subject'
					}, {
						xtype: 'posteditor',
						anchor: '100% -50',
						name: 'content',
						itemId: 'content',
						listeners: {
							//createLink: '@{createLink}',
							//attachFile: '@{attachFile}'
						}
					}],
					buttonAlign: 'left',
					buttons: {
						xtype: 'toolbar',
						cls: 'post-toolbar-footer',
						items: [{
							name: 'post',
							handler: function(button) {
								this.up().up().down('posteditor').closeMenu();
								button.up('form').fireEvent('post', button.up('form'), button.up('form'))
							}
						}, {
							name: 'comment',
							handler: function(button) {
								this.up().up().down('posteditor').closeMenu();
								button.up('form').fireEvent('comment', button.up('form'), button.up('form'))
							}
						}, {
							text: '~~cancel~~',
							handler: function(button) {
								this.up().up().down('posteditor').closeMenu();
								button.up('form').fireEvent('resetPostForm')
								button.up('form').fireEvent('cancel')
							}
						}]
					},
					listeners: {
						show: function(form) {
							Ext.defer(function() {
								form.down('#content').focus()
								var toField = form.down('#toField'),
									models = toField.valueModels;
								Ext.Array.forEach(models, function(model) {
									if (model.get('type').toLowerCase() == 'forum') {
										form.down('#subject').focus()
									}
								})
							}, 10)
						},
						resetPostForm: function() {
							this.down('#toField').clearValue() //manually clear the value because glu bindings are a little weak
						},
						cancel: '@{cancel}',
						post: '@{post}',
						comment: '@{comment}'
					}
				}, {
					bodyPadding: '10px',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: [{
						xtype: 'combobox',
						itemId: 'filter',
						emptyText: '~~filter~~',
						inputCls: 'post-message',
						filterStore: '@{postFilterStore}',					
						displayField: 'name',
						valueField: 'value',
						columns: [{
							dataIndex: 'content',
							filterable: true,
							text: '~~content~~'
						}, {
							dataIndex: 'authorUsername',
							filterable: true,
							text: 'Author'
						}, {
							dataIndex: 'title',
							filterable: true,
							text: 'Subject'
						}],
						/*plugins: [{
							ptype: 'searchfieldfilter',
							showComparisons: false,
							filterBarItemId: 'filterBar'
						}],*/
						listeners: {
							beforerender: function(combo) {
								Ext.Array.forEach(combo.columns, function(column) {
									switch (column.text) {
										case '~~content~~':
											column.text = RS.social.locale.content
											break;
										case '~~subject~~':
											column.text = RS.social.locale.subject
											break;
										case '~~author~~':
											column.text = RS.social.locale.author
											break;
									}
								})
							}
						}
					}, {
						xtype: 'filterbar',
						itemId: 'filterBar',
						flex: 1,
						allowPersistFilter: false,
						listeners: {
							add: {
								fn: function() {
									this.ownerCt.down('#filter').getPlugin().filterChanged()
								},
								buffer: 10
							},
							remove: {
								fn: function() {
									this.ownerCt.down('#filter').getPlugin().filterChanged()
								},
								buffer: 10
							},
							clearFilter: function() {
								this.ownerCt.down('#filter').clearValue()
							}
						}
					}],
					listeners: {
						activate: function(panel) {
							panel.down('#filter').focus()
						}
					}
				}, {
					html: '&nbsp;'
				}]
			}, {
				xtype: 'toolbar',
				padding: '10px 10px 0px 0px',
				items: [{
					name: 'switchToPostMessage',
					hidden: '@{!showPostActionToolbarActions}',
					text: '',
					iconCls: 'icon-comment-alt social-post-toolbar',
					tooltip: '~~postMessageTooltip~~'
				}, {
					name: 'switchToFilter',
					hidden: '@{!showPostToolbarActions}',
					text: '',
					iconCls: 'icon-search social-post-toolbar',
					tooltip: '~~filterTooltip~~'
				}, {
					xtype: 'splitbutton',
					name: 'markAllPostsRead',
					hidden: '@{!showPostToolbarActions}',
					text: '',
					iconCls: 'icon-ok social-post-toolbar',
					tooltip: '~~markAllPostsReadTooltip~~',
					menu: [{
							xtype: 'panel',
							plain: true,
							//style: 'font-weight: bold',
							cls: 'x-menu-item-header',
							html: '~~markAsRead~~'
						}, {
							name: 'markAllPostsRead'
						}, {
							name: 'markPostsOlderThanOneDayRead'
						}, {
							name: 'markPostsOlderThanOneWeekRead'
						}, {
							xtype: 'panel',
							plain: true,
							//style: 'font-weight: bold',
							cls: 'x-menu-item-header',
							html: '~~filterMenuHeader~~'
						}, {
							xtype: 'menucheckitem',
							checked: '@{unreadOnly}',
							text: '~~unreadOnly~~'
						}
						/*, {
						xtype: 'menucheckitem',
						checked: '@{oneDayOld}',
						text: '~~oneDayOld~~'
					}, {
						xtype: 'menucheckitem',
						checked: '@{oneWeekOld}',
						text: '~~oneWeekOld~~'
					}*/
					]
				}, {
					xtype: 'tbseparator',
					hidden: '@{!showPostToolbarActions}',
					cls: 'social-post-toolbar'
				}, {
					iconCls: 'icon-chevron-left social-post-toolbar-page',
					hidden: '@{!showPostToolbarButtons}',
					name: 'previousPage',
					tooltip: '~~previousPage~~',
					text: ''
				}, {
					iconCls: 'icon-chevron-right social-post-toolbar-page',
					hidden: '@{!showPostToolbarButtons}',
					name: 'nextPage',
					tooltip: '~~nextPage~~',
					text: ''
				}, {
					xtype: 'tbseparator',
					hidden: '@{!showPostToolbarButtons}',
					cls: 'social-post-toolbar'
				}, {
					xtype: 'splitbutton',
					name: 'refreshPosts',
					text: '',
					hidden: '@{!showPostToolbarButtonsRefresh}',
					iconCls: 'icon-repeat social-post-toolbar',
					tooltip: '~~refreshPostsTooltip~~',
					menu: {
						cls: 'auto-refresh-container',
						width: 200,
						plain: true,
						items: [{
							xtype: 'checkboxfield',
							name: 'autoRefreshPosts',
							hideLabel: true,
							boxLabel: '~~autoRefreshPosts~~'
						}, {
							xtype: 'fieldcontainer',
							hidden: '@{!autoRefreshPosts}',
							layout: {
								type: 'hbox',
								align: 'stretch'
							},
							items: [{
								xtype: 'numberfield',
								labelWidth: 60,
								name: 'pollInterval',
								minValue: 5,
								flex: 1								
							}, {
								xtype: 'tbtext',
								style: {
									marginTop: '2px',
									lineHeight: '20px'
								},
								text: '(sec.)'
							}]
						}],
						listeners: {
							hide: '@{startPolling}'
						}
					}
				}]
			}]
		}, {
			flex: 1,
			itemId: 'postPanel',
			layout: 'card',
			activeItem: '@{postDisplayItem}',
			items: [{
				bodyPadding: '10px',
				autoScroll: true,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: '@{posts}',
				listeners: {
					render: function(panel) {
						panel.body.on('scroll', function(e, el) {
							panel.fireEvent('hideLikes', panel)

							var thisHeight = [],
								prevHeight = [],
								total = 0,
								scroll = el.scrollTop,
								i = 0;
							panel.items.each(function(item, index) {
								total = total + item.getHeight()
								thisHeight[index] = item.getHeight()
								prevHeight[index] = total - item.getHeight()
							})

							for (; i < panel.items.length; i++) {
								if (prevHeight[i] < (scroll + panel.getHeight() / 4) && prevHeight[i + 1] >= (scroll + panel.getHeight() / 4)) {
									panel.prevItem = panel.currentItem

									panel.currentItem = i
									if (panel.currentItem != panel.prevItem) {
										panel.fireEvent('markReadScroll', panel, panel.prevItem)
									}
								}
							}
						})
					},
					add: function(panel, component, index) {
						if (index % 10 == 9)
							component.on('render', function() {
								panel.body.scrollTo('top', 0);
							});
					},
					hideLikes: '@{hideLikes}',
					markReadScroll: '@{markReadScroll}'
				}
			}, {
				bodyPadding: '10px',
				autoScroll: true,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [{
					xtype: '@{activePost}'
				}],
				listeners: {
					render: function(panel) {
						panel.body.on('scroll', function(e, el) {
							panel.fireEvent('hideLikes', panel, e, el, panel)
						})
					},
					hideLikes: '@{hideLikes}'
				}
			}]
		}]
	}],
	listeners: {
		afterrender: function() {
			var splitters = this.query('bordersplitter')
			Ext.Array.forEach(splitters, function(splitter) {
				splitter.setSize(1, 1)
				splitter.getEl().setStyle({
					backgroundColor: '#fbfbfb'
				})
			})
		},
		beforedestroy : '@{beforeDestroyComponent}'
	}
});
glu.defView('RS.social.Main', 'mini', {
	activeItem: '@{activeCard}',
	layout: 'card',
	displayMask: '@{authenticateText}',
	setDisplayMask: function(value) {
		this.setLoading(value)
	},
	dockedItems: [{
		dock: 'top',
		xtype: 'toolbar',
		cls: 'social-mini-toolbar-header',
		defaultButtonUI: 'social-mini-toolbar-button-small',
		items: [{
			text: '@{backText}',
			iconCls: 'icon-large icon-chevron-left',
			name: 'back'
		}, {
			xtype: 'tbtext',
			cls: 'social-mini-post-title',
			text: '@{titleText}',
			tooltip: '@{titleTextTooltip}',
			setTooltip: function(value) {
				this.tooltip = value
				if (this.tooltipControl) this.tooltipControl.update(value)
			},
			listeners: {
				render: function(tbtext) {
					tbtext.tooltipControl = Ext.create('Ext.tip.ToolTip', {
						html: tbtext.tooltip,
						target: tbtext.getEl()
					})
				}
			}
		}, {
			xtype: 'tbfill'
		}, {
			iconCls: 'icon-large icon-plus',
			hidden: '@{!showFollow}',
			handler: '@{followStream}',
			tooltip: '~~followStreamTooltip~~'
		}, {
			iconCls: 'icon-chevron-left',
			hidden: '@{!showPagingButtons}',
			name: 'previousPage',
			tooltip: '~~previousPage~~',
			text: ''
		}, {
			iconCls: 'icon-chevron-right',
			hidden: '@{!showPagingButtons}',
			name: 'nextPage',
			tooltip: '~~nextPage~~',
			text: ''
		}, {
			xtype: 'tbseparator',
			hidden: '@{!showPagingButtons}'
		}, {
			iconCls: 'icon-large icon-chevron-sign-down',
			hidden: '@{!showToggleStreams}',
			handler: function(button) {
				button.fireEvent('toggleStreams', button, !button.isUp)
				button.isUp = !button.isUp
				button.setIconCls('icon-large ' + (button.isUp ? 'icon-chevron-sign-up' : 'icon-chevron-sign-down'))
			},
			listeners: {
				toggleStreams: '@{toggleStreams}'
			}
		}, {
			iconCls: 'icon-large icon-repeat',
			hidden: '@{!showRefreshButton}',
			handler: '@{refresh}',
			tooltip: '~~refreshPostsTooltip~~',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	defaults: {
		listeners: {
			activate: function(panel) {
				panel.up('panel').dockedItems.each(function(docked) {
					docked.items.each(function(item) {
						item.doComponentLayout()
					})
					docked.doLayout()
				})
			}
		}
	},
	items: [{
		autoScroll: true,
		items: '@{groups}'
	}, {
		autoScroll: true,
		padding: '0px 0px 0px 10px',
		items: '@{posts}'
	}, {
		html: 'comments'
	}, {
		xtype: 'form',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		buttonAlign: 'left',
		buttons: ['post', 'cancelMini'],
		bodyPadding: '5px',
		items: [{
			xtype: 'textfield',
			emptyText: '~~subject~~',
			hideLabel: true,
			name: 'subject'
		}, {
			xtype: 'posteditor',
			displayLinkAndUpload: false,
			flex: 1,
			name: 'content',
			listeners: {
				//createLink: '@{createLink}',
				//attachFile: '@{attachFile}'
			}
		}]
	}, {
		xtype: '@{activePost}'
	}],
	bbar: {
		xtype: 'toolbar',
		hidden: '@{!showPostButton}',
		cls: 'social-mini-toolbar-footer',
		defaultButtonUI: 'social-mini-toolbar-button-small',
		items: [{
			xtype: 'textfield',
			inputCls: 'post-message',
			emptyText: '~~postYourMessage~~',
			readOnly: true,
			flex: 1,
			listeners: {
				render: function(field) {
					field.getEl().on('click', function() {
						field.fireEvent('switchToPost', field)
					})
				},
				switchToPost: '@{switchToPost}'
			}
		}, {
			iconCls: 'icon-large icon-comment-alt',
			hidden: '@{!showPostButton}',
			handler: '@{switchToPost}'
		}]
	},
	listeners: {
		render: function(panel) {
			if (panel.displayMask !== false)
				panel.setLoading(panel.displayMask)
		}
	}
})
glu.defView('RS.social.MovePost', {
	asWindow: {
		height: 200,
		width: 600,
		title: '~~movePost~~'
	},

	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	bodyPadding: '10px',
	items: [{
		xtype: 'comboboxselect',
		forceSelection: false,
		hideTrigger: true,
		itemId: 'toField',
		store: '@{streamToStore}',
		displayField: 'displayName',
		valueField: 'id',
		name: 'to',
		queryMode: 'local',
		typeAhead: true,
		multiSelect: true,
		tpl: '<tpl for="."><div class="x-boundlist-item">{displayName} {[values["type"] ? "(" + values["type"] + ")" : ""]}</div></tpl>'
	}, {
		xtype: 'hidden',
		name: 'toHidden',
		setValue: function(value) {
			var form = this.up('window');
			if (form) form.down('#toField').setValue(value)
		}
	}],
	buttonAlign: 'left',
	buttons: ['movePost', 'cancel']
})
glu.defView('RS.social.Post', {
	padding: '15px 0px',
	cls: '@{forumBorderCls}',
	items: [{
		xtype: 'container',
		layout: {
			type: 'hbox',
			align: 'middle'
		},
		items: [{
			xtype: 'image',
			cls: 'profile-pic',
			src: '@{userProfilePicture}',
			authorUsername: '@{authorUsername}',
			setAuthorUsername: function(authorUsername) {
				this.authorUsername = authorUsername;
			},
			hidden: '@{!showImage}',
			width: '@{imageSize}',
			height: '@{imageSize}',
			margin: '@{imagePadding}',
			listeners: {
				render: function(panel) {
					panel.getEl().on('click', function() {
						panel.fireEvent('navigateToProfile', panel, panel.authorUsername)
					})
				},
				navigateToProfile: '@{navigateToProfile}'
			}
		}, {
			flex: 1,
			xtype: 'container',
			items: [{
				xtype: 'toolbar',
				padding: '0px',
				items: [{
					xtype: 'tbtext',
					text: '@{subjectDisplay}',
					tooltip: '@{subjectTooltip}',
					hidden: '@{showAuthorSubject}',
					cls: 'post-subject @{postForumSubjectCls}',
					listeners: {
						render: function(tbtext) {
							if (tbtext.tooltip)
								tbtext.tooltipControl = Ext.create('Ext.tip.ToolTip', {
									target: tbtext.getEl(),
									html: tbtext.tooltip
								})

							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('subjectClicked', tbtext)
							})
						},
						subjectClicked: '@{subjectClicked}'
					}
				}, {
					xtype: 'tbtext',
					hidden: '@{!showAuthorSubject}',
					cls: 'post-author-subject',
					text: '@{authorDisplayName}',
					listeners: {
						render: function(tbtext) {
							if (tbtext.tooltip)
								tbtext.tooltipControl = Ext.create('Ext.tip.ToolTip', {
									target: tbtext.getEl(),
									html: tbtext.tooltip
								})

							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('subjectClicked', tbtext)
							})
						},
						subjectClicked: '@{subjectClicked}'
					}
				}, '->', {
					xtype: 'tbtext',
					text: '@{answerText}',
					cls: '@{answerCls}',
					hidden: '@{!showAnswer}'
				}, {
					xtype: 'tbseparator',
					hidden: '@{!showAnswer}'
				}, {
					xtype: 'tbtext',
					cls: 'post-link',
					text: '~~share~~',
					hidden: '@{!showShareLink}',
					listeners: {
						render: function(tbtext) {
							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('sharePost', tbtext, tbtext)
							})
						},
						sharePost: '@{sharePost}'
					}
				}, {
					xtype: 'tbseparator',
					hidden: '@{!isPost}'
				}, {
					xtype: 'tbtext',
					cls: '@{readCls}',
					tooltip: '@{readTooltip}',
					setTooltip: function(value) {
						if (this.tooltipControl) this.tooltipControl.update(value)
					},
					listeners: {
						afterrender: function(tbtext) {
							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('toggleRead', tbtext)
							})
							tbtext.tooltipControl = Ext.create('Ext.tip.ToolTip', {
								html: tbtext.tooltip,
								target: tbtext.getEl()
							})
						},
						toggleRead: '@{toggleRead}'
					}
				}, {
					xtype: 'tbtext',
					cls: '@{starCls}',
					tooltip: '@{starredTooltip}',
					hidden: '@{!showStar}',
					setTooltip: function(value) {
						if (this.tooltipControl) this.tooltipControl.update(value)
					},
					listeners: {
						afterrender: function(tbtext) {
							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('toggleStarred', tbtext)
							})
							tbtext.tooltipControl = Ext.create('Ext.tip.ToolTip', {
								html: tbtext.tooltip,
								target: tbtext.getEl()
							})
						},
						toggleStarred: '@{toggleStarred}'
					}
				}, {
					xtype: 'tbtext',
					cls: '@{likeCls}',
					tooltip: '@{likeTooltip}',
					setTooltip: function(value) {
						if (this.tooltipControl) this.tooltipControl.update(value)
					},
					listeners: {
						afterrender: function(tbtext) {
							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('toggleLike', tbtext)
							})
							tbtext.tooltipControl = Ext.create('Ext.tip.ToolTip', {
								html: tbtext.tooltip,
								target: tbtext.getEl()
							})
						},
						toggleLike: '@{toggleLike}'
					}
				}, {
					xtype: 'tbtext',
					cls: 'post-link',
					text: '@{numberOfLikesText}',
					listeners: {
						render: function(tbtext) {
							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('toggleLikesDisplay', tbtext, tbtext)
							})
							// tbtext.getEl().on('mouseover', function() {
							// 	Ext.defer(function() {
							// 		if (Ext.EventObject.within(tbtext.getEl())) tbtext.fireEvent('toggleLikesDisplay', tbtext, tbtext)
							// 	}, 1000)
							// })
						},
						toggleLikesDisplay: '@{toggleLikesDisplay}'
					}
				}, {
					xtype: 'tbtext',
					cls: 'icon-large icon-caret-down post-hand',
					read: '@{read}',
					setRead: function(value) {
						this.read = value
					},
					locked: '@{shouldBeLocked}',
					showLocked: '@{showLocked}',
					showMove: '@{showMove}',
					following: '@{following}',
					currentUser: '@{currentUser}',
					hasDeleteRights: '@{hasDeleteRights}',
					listeners: {
						render: function(tbtext) {
							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('showMenu', tbtext)
							})
						},
						showMenu: function(tbtext) {
							if (!tbtext.menu) tbtext.menu = Ext.create('Ext.menu.Menu', {
								items: [{
									text: RS.social.locale.move,
									hidden: !tbtext.showMove,
									iconCls: 'icon-large icon-exchange post-menu-icon',
									handler: function() {
										tbtext.fireEvent('movePost')
									}
								}, {
									text: RS.social.locale.deletePost,
									hidden: !tbtext.hasDeleteRights,
									iconCls: 'icon-large icon-remove post-menu-icon',
									handler: function() {
										tbtext.fireEvent('deletePost')
									}
								}, {
									itemId: 'readButton',
									text: tbtext.read ? RS.social.locale.unread : RS.social.locale.read,
									iconCls: tbtext.read ? 'icon-large icon-envelope post-menu-icon' : 'icon-large icon-ok post-menu-icon',
									handler: function() {
										tbtext.read = !tbtext.read
										this.setText(tbtext.read ? RS.social.locale.unread : RS.social.locale.read)
										this.setIconCls(tbtext.read ? 'icon-large icon-ok post-menu-icon' : 'icon-large icon-envelope post-menu-icon')
										tbtext.fireEvent('toggleReadPost')
									}
								}, {
									text: tbtext.locked ? RS.social.locale.unlock : RS.social.locale.lock,
									iconCls: tbtext.locked ? 'icon-large icon-lock post-menu-icon' : 'icon-large icon-unlock post-menu-icon',
									hidden: !tbtext.showLocked,
									handler: function() {
										tbtext.locked = !tbtext.locked
										this.setText(tbtext.locked ? RS.social.locale.unlock : RS.social.locale.lock)
										this.setIconCls(tbtext.locked ? 'icon-large icon-lock post-menu-icon' : 'icon-large icon-unlock post-menu-icon')
										tbtext.fireEvent('toggleLockPost')
									}
								}, {
									text: tbtext.following ? RS.social.locale.unfollowUser : RS.social.locale.followUser,
									iconCls: tbtext.following ? 'icon-large icon-reply post-menu-icon' : 'icon-large icon-share-alt post-menu-icon',
									hidden: tbtext.currentUser,
									handler: function() {
										tbtext.following = !tbtext.following
										this.setText(tbtext.following ? RS.social.locale.unfollowUser : RS.social.locale.followUser)
										this.setIconCls(tbtext.following ? 'icon-large icon-reply post-menu-icon' : 'icon-large icon-share-alt post-menu-icon')
										tbtext.fireEvent('toggleFollowAuthor')
									}
								}]
							})

							tbtext.menu.down('#readButton').setText(tbtext.read ? RS.social.locale.unread : RS.social.locale.read)
							tbtext.menu.down('#readButton').setIconCls(tbtext.read ? 'icon-large icon-envelope post-menu-icon' : 'icon-large icon-ok post-menu-icon')
							tbtext.menu.showBy(tbtext)
						},
						movePost: '@{movePost}',
						deletePost: '@{deletePost}',
						toggleReadPost: '@{toggleReadPost}',
						toggleLockPost: '@{toggleLockPost}',
						toggleFollowAuthor: '@{toggleFollowAuthor}'
					}
				}]
			}, {
				xtype: 'toolbar',
				padding: '0px',
				items: [{
					xtype: 'tbtext',
					cls: 'post-author',
					text: '@{authorDisplayName}',
					tooltip: '@{authorTooltip}',
					hidden: '@{!showAuthor}',
					setTooltip: function(value) {
						if (this.tooltipCtrl) this.tooltipCtrl.update(value)
						else this.tooltipCtrl = Ext.create('Ext.tip.ToolTip', {
							html: value,
							target: this.getEl()
						})
					},
					listeners: {
						render: function(tbtext) {
							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('toggleAuthorDisplay', tbtext, tbtext)
							})
							// tbtext.getEl().on('mouseover', function() {
							// 	Ext.defer(function() {
							// 		if (Ext.EventObject.within(tbtext.getEl())) tbtext.fireEvent('toggleAuthorDisplay', tbtext, tbtext)
							// 	}, 1000)
							// })
							if (tbtext.tooltip) {
								tbtext.tooltipCtrl = Ext.create('Ext.tip.ToolTip', {
									html: tbtext.tooltip,
									target: tbtext.getEl()
								})
							}
						},
						toggleAuthorDisplay: '@{toggleAuthorDisplay}'
					}
				}, {
					xtype: 'tbtext',
					hidden: '@{!showCaret}',
					cls: 'icon-angle-right icon-large post-arrow'
				}, {
					xtype: 'tbtext',
					cls: 'post-component',
					text: '@{component}',
					hidden: '@{!displayComponent}',
					listeners: {
						render: function(tbtext) {
							tbtext.getEl().on('click', function() {
								tbtext.fireEvent('toggleComponentDisplay', tbtext, tbtext)
							})
						},
						toggleComponentDisplay: '@{toggleComponentDisplay}'
					}
				}, {
					xtype: 'tbtext',
					hidden: '@{!displayTargets}',
					targets: '@{targetList}',
					cls: 'icon-caret-down post-hand',
					listeners: {
						render: function(tbtext) {
							var me = this;
							tbtext.getEl().on('click', function() {
								if (!tbtext.menu) {
									var targetItems = [];
									tbtext.targets.forEach(function(target) {
										targetItems.push({
											text: target.name,
											socialTarget: target,
											listeners: {
												click: function() {
													me.fireEvent('changeInterestedTarget', this, this.socialTarget);
												}
											}
										})
									})
									tbtext.menu = Ext.create('Ext.menu.Menu', {
										items: targetItems
									})
								}
								tbtext.menu.showBy(tbtext)
							})
						},
						changeInterestedTarget: '@{changeInterestedTarget}'
					}
				}, '->', {
					xtype: 'tbtext',
					text: '@{forumCountText}',
					cls: 'post-date'
				}, {
					xtype: 'tbtext',
					cls: 'post-date',
					text: '@{dateText}'
				}]
			}]
		}]
	}, {
		xtype: 'component',
		padding: '@{contentPadding}',
		hidden: '@{!displayContent}',
		style: 'word-wrap:break-word',
		html: '@{contentDisplay}',
		isMini: '@{miniMode}',
		setHtml: function(value) {
			this.update(value)
		},
		tooltip: '@{dateTooltip}',
		listeners: {
			render: function(panel) {
				if (panel.tooltip) panel.tooltipControl = Ext.create('Ext.tip.ToolTip', {
					html: panel.tooltip,
					target: panel.getEl()
				})

				var links = panel.getEl().query('a'),
					images = panel.getEl().query('img');

				Ext.Array.forEach(links, function(link) {
					var data = link.getAttribute('fullData'),
						dataType = link.getAttribute('dataType'),
						currentLocation = window.location.protocol + '//' + window.location.host + window.location.pathname;
					if (data) {
						if (dataType == 'pdf') {
							link.target = '_blank'
							link.href = data
						} else
							Ext.fly(link).on('click', function(e) {
								e.stopEvent()
								downloadFile(data)
							})
					} else if (link.href && link.href.indexOf(currentLocation) == -1) {
						link.target = '_blank'
					}

					if (link.href.indexOf('social.jsp') > -1) {
						var temp = link.href.split('#')[1];
						link.href = '/resolve/jsp/rsclient.jsp' + (temp ? '#' + temp : '')
					}

					//Correct legacy links to be displayed properly
					if (link.href.indexOf('/showentity?') > -1) {
						//grab type
						var params = Ext.Object.fromQueryString(link.href.split('?')[1])
						if (params.type) {
							switch (params.type.toLowerCase()) {
								case 'document':
									link.href = '#RS.wiki.Main/name=' + params.name
									break;
								case 'actiontask':
									link.href = '#RS.actiontask.ActionTask/id=' + params.sysId
									break;
								case 'worksheet':
									link.href = '#RS.worksheet.Worksheet/id=' + params.sysId
									break;
								case 'forum':
									link.href = '#RS.socialadmin.Component/name=forum&id=' + params.sysId
									break;
								case 'rss':
									link.href = '#RS.socialadmin.Component/name=rss&id=' + params.sysId
									break;
								case 'process':
									link.href = '#RS.socialadmin.Component/name=process&id=' + params.sysId
									break;
								case 'team':
									link.href = '#RS.socialadmin.Component/name=team&id=' + params.sysId
									break;
								case 'user':
									link.href = '#RS.user.User/username=' + params.username
									break;
							}
						}
					}

					if (link.href.indexOf('/post/download') > -1 || Ext.isFunction(link.onclick)) {
						var img = Ext.fly(link).query('img')[0];
						var href = link.href;
						if (img) href = img.src;
						if (href.indexOf('?') > -1) {
							var params = Ext.Object.fromQueryString(href.split('?')[1])
							if (params.fileid) {
								link.onclick = null
								Ext.fly(link).on('click', function(e) {
									e.stopEvent()
									downloadFile('/resolve/service/social/download?id=' + params.fileid)
								})
							}
						}
					}

					if (panel.isMini) {
						link.target = '_blank'
						if (link.href.indexOf('social.jsp') > -1) link.href = '/resolve/jsp/rsclient.jsp' + link.hash
					}
				})

				Ext.Array.forEach(images, function(image) {
					var image = Ext.get(image),
						fullData = image.getAttribute('fullData');

					//convert legacy images
					var imageSrc = Ext.fly(image).dom.src;
					if (imageSrc.indexOf('/post/download') > -1) {
						var params = Ext.Object.fromQueryString(imageSrc.split('?')[1])
						if (params.fileid) {
							var fileid = params.fileid
							if (Ext.String.endsWith(fileid, '"')) fileid = fileid.substring(0, fileid.length - 1)
							Ext.fly(image).dom.src = '/resolve/service/social/download?preview=true&id=' + fileid
							fullData = '/resolve/service/social/download?id=' + fileid
						}
					}

					if (fullData) image.setWidth(200) //force only internal images to 200px (rss images should appear as they are+)
					image.on('click', function() {
						displayLargeImage(fullData || image.src, '')
					})
					image.on('load', function() {
						//force refresh of parent container because this image will load an change the size of the content after being loaded
						panel.ownerCt.doLayout()
					})

				})
			}
		}
	}, {
		hidden: '@{!displayAnswers}',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		padding: '20px 0px 30px 18px',
		items: '@{answersList}',
		defaults: {
			viewMode: 'comment'
		}
	}, {
		xtype: 'container',
		hidden: '@{!displayComments}',
		layout: {
			type: 'hbox',
			align: 'middle'
		},
		items: [{
			xtype: 'label',
			cls: 'post-display-comments',
			text: '@{collapseTitle}',
			listeners: {
				render: function(label) {
					label.getEl().on('click', function() {
						label.fireEvent('toggleCollapseComments')
					})
				},
				toggleCollapseComments: '@{toggleCollapseComments}'
			}
		}, {
			xtype: 'label',
			cls: '@{collapseCls} icon-large post-display-comments-caret',
			listeners: {
				render: function(label) {
					label.getEl().on('click', function() {
						label.fireEvent('toggleCollapseComments')
					})
				},
				toggleCollapseComments: '@{toggleCollapseComments}'
			}
		}]
	}, {
		xtype: 'panel',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		hidden: '@{!displayComments}',
		collapsible: true,
		hideCollapseTool: true,
		collapsed: '@{collapsed}',
		header: false,
		padding: '0px 0px 0px 18px',
		items: '@{commentsList}',
		defaults: {
			viewMode: 'comment'
		}
	}, {
		hidden: '@{!displayAddComment}',
		layout: {
			type: 'card',
			deferredRender: true
		},
		activeItem: '@{activePostCard}',
		items: [{
			bodyPadding: '10px',
			layout: 'hbox',
			height: 35,
			listeners: {
				activate: function(panel) {
					if (this.ownerCt.originalHeight) this.ownerCt.setHeight(this.ownerCt.originalHeight)
				}
			},
			items: [{
				xtype: 'textfield',
				flex: 1,
				emptyText: '~~addAComment~~',
				inputCls: 'comment-message',
				listeners: {
					postMessage: '@{switchToPostMessage}',
					render: function(field) {
						field.getEl().on('click', function() {
							field.ownerCt.ownerCt.originalHeight = field.ownerCt.ownerCt.getHeight()
							field.fireEvent('postMessage', field)
							// field.ownerCt.ownerCt.animate({
							// 	to: {
							// 		height: 200
							// 	},
							// 	duration: 750,
							// 	easing: 'backOut',
							// 	dynamic: true
							// })
							field.ownerCt.ownerCt.setHeight(200)
							var editor = field.up('panel').up('panel').down('posteditor').ownerCt
							Ext.defer(function() {
								if (Ext.getBody().getHeight() - editor.getY() < 200)
									field.up('#postPanel').scrollBy({
										x: 0,
										y: 200
									}, true)
							}, 100)
						})
					}
				}
			}]
		}, {
			xtype: 'form',
			bodyPadding: '10px',
			defaults: {
				anchor: '100%'
			},
			items: [{
				xtype: 'posteditor',
				displayLinkAndUpload: '@{!miniMode}',
				padding: '10 0 0 0',
				height: 140,
				name: 'commentContent',
				itemId: 'commentContent',
				hideLabel: true,
				listeners: {
					//createLink: '@{createLink}',
					//attachFile: '@{attachFile}',
					render: function(ctrl) {
						ctrl.focus()
					}
				}
			}],
			buttonAlign: 'left',
			buttons: {
				xtype: 'toolbar',
				cls: 'post-toolbar-footer',
				items: [{
					name: 'comment',
					handler: function(button) {
						button.up('form').fireEvent('comment', button.up('form'), button.up('form'))
					}
				}, {
					text: '~~cancel~~',
					handler: function(button) {
						button.up('form').fireEvent('resetPostForm')
						button.up('form').fireEvent('cancel')
					}
				}]
			},
			listeners: {
				render: function() {},
				show: function(form) {
					var ctrl = form.down('#commentContent')
					if (ctrl) ctrl.focus()
				},
				resetPostForm: function() {
					// this.ownerCt.animate({
					// 	to: {
					// 		height: this.ownerCt.originalHeight
					// 	},
					// 	duration: 250,
					// 	dynamic: true
					// })
					this.ownerCt.setHeight(this.ownerCt.originalHeight)
				},
				cancel: '@{cancel}',
				comment: '@{comment}',
				//createLink: '@{createLink}',
				//attachFile: '@{attachFile}'
			}
		}]
	}],
	listeners : {
		beforedestroy : '@{beforeDestroyComponent}'
	}
})

glu.defView('RS.social.Post', 'comment', {
	xtype: 'container',
	anchor: '-20',
	padding: '0px 0px',
	layout: {
		type: 'hbox',
		align: 'top'
	},
	items: [{
		xtype: 'image',
		cls: 'profile-pic',
		src: '@{userProfilePicture}',
		authorUsername: '@{commentAuthorUsername}',
		hidden: '@{!showImage}',
		width: '@{imageSize}',
		height: '@{imageSize}',
		margin: '@{imagePadding}',
		listeners: {
			render: function(panel) {
				panel.getEl().on('click', function() {
					panel.fireEvent('navigateToProfile', panel, panel.authorUsername)
				})
			},
			navigateToProfile: '@{navigateToProfile}'
		}
	}, {
		xtype: 'container',
		flex: 1,
		items: [{
			xtype: 'toolbar',
			padding: '0px',
			items: ['->', {
				xtype: 'tbtext',
				hidden: '@{!isForumComment}',
				cls: '@{commentAnswerCls} post-forum-answer',
				text: '@{answerTextComment}',
				listeners: {
					afterrender: function(tbtext) {
						tbtext.getEl().on('click', function() {
							tbtext.fireEvent('toggleAnswer', tbtext)
						})
					},
					toggleAnswer: '@{toggleAnswer}'
				}
			}, {
				xtype: 'tbseparator',
				hidden: '@{!isForumComment}'
			}, {
				xtype: 'tbtext',
				cls: '@{likeCls}',
				tooltip: '@{likeTooltip}',
				setTooltip: function(value) {
					if (this.tooltipControl) this.tooltipControl.update(value)
				},
				listeners: {
					afterrender: function(tbtext) {
						tbtext.getEl().on('click', function() {
							tbtext.fireEvent('toggleLike', tbtext)
						})
						tbtext.tooltipControl = Ext.create('Ext.tip.ToolTip', {
							html: tbtext.tooltip,
							target: tbtext.getEl()
						})
					},
					toggleLike: '@{toggleLike}'
				}
			}, {
				xtype: 'tbtext',
				cls: 'post-link',
				text: '@{numberOfLikesText}',
				listeners: {
					render: function(tbtext) {
						tbtext.getEl().on('click', function() {
							tbtext.fireEvent('toggleLikesDisplay', tbtext, tbtext)
						})
						// tbtext.getEl().on('mouseover', function() {
						// 	Ext.defer(function() {
						// 		if (Ext.EventObject.within(tbtext.getEl())) tbtext.fireEvent('toggleLikesDisplay', tbtext, tbtext)
						// 	}, 1000)
						// })
					},
					toggleLikesDisplay: '@{toggleLikesDisplay}'
				}
			}, {
				xtype: 'tbtext',
				cls: 'icon-large icon-caret-down post-hand',
				hidden: '@{!hasDeleteRights}',
				listeners: {
					render: function(tbtext) {
						tbtext.getEl().on('click', function() {
							tbtext.fireEvent('showMenu', tbtext)
						})
					},
					showMenu: function(tbtext) {
						if (!tbtext.menu) tbtext.menu = Ext.create('Ext.menu.Menu', {
							items: [{
								text: RS.social.locale.deletePost,
								iconCls: 'icon-large icon-remove post-menu-icon',
								handler: function() {
									tbtext.fireEvent('deleteComment')
								}
							}]
						})

						tbtext.menu.showBy(tbtext)
					},
					deleteComment: '@{deleteComment}'
				}
			}]
		}, {
			xtype: 'toolbar',
			padding: '0px',
			items: [{
				xtype: 'tbtext',
				cls: 'comment-author',
				text: '@{commentAuthorDisplayName}',
				listeners: {
					render: function(tbtext) {
						tbtext.getEl().on('click', function() {
							tbtext.fireEvent('toggleAuthorDisplay', tbtext, tbtext)
						})
					},
					toggleAuthorDisplay: '@{toggleAuthorDisplay}'
				}
			}, '->', {
				xtype: 'tbtext',
				cls: 'post-date',
				text: '@{dateText}'
			}]
		}, {
			xtype: 'container',
			padding: '@{contentPadding}',
			style: 'word-wrap:break-word',
			html: '@{contentDisplay}',
			isMini: '@{miniMode}',
			listeners: {
				render: function(panel) {
					var links = panel.getEl().query('a'),
						images = panel.getEl().query('img');

					Ext.Array.forEach(links, function(link) {
						var data = link.getAttribute('fullData'),
							dataType = link.getAttribute('dataType'),
							currentLocation = window.location.protocol + '//' + window.location.host + window.location.pathname;
						if (data) {
							if (dataType == 'pdf') {
								link.target = '_blank'
								link.href = data
							} else
								Ext.fly(link).on('click', function(e) {
									e.stopEvent()
									downloadFile(data)
								})
						} else if (link.href && link.href.indexOf(currentLocation) == -1) {
							link.target = '_blank'
						}

						if (link.href.indexOf('social.jsp') > -1) {
							var temp = link.href.split('#')[1];
							link.href = '/resolve/jsp/rsclient.jsp' + (temp ? '#' + temp : '')
						}

						//Correct legacy links to be displayed properly
						if (link.href.indexOf('/showentity?') > -1) {
							//grab type
							var params = Ext.Object.fromQueryString(link.href.split('?')[1])
							if (params.type) {
								switch (params.type.toLowerCase()) {
									case 'document':
										link.href = '#RS.wiki.Main/name=' + params.name
										break;
									case 'actiontask':
										link.href = '#RS.actiontask.ActionTask/id=' + params.sysId
										break;
									case 'worksheet':
										link.href = '#RS.worksheet.Worksheet/id=' + params.sysId
										break;
									case 'forum':
										link.href = '#RS.socialadmin.Component/name=forum&id=' + params.sysId
										break;
									case 'rss':
										link.href = '#RS.socialadmin.Component/name=rss&id=' + params.sysId
										break;
									case 'process':
										link.href = '#RS.socialadmin.Component/name=process&id=' + params.sysId
										break;
									case 'team':
										link.href = '#RS.socialadmin.Component/name=team&id=' + params.sysId
										break;
									case 'user':
										link.href = '#RS.user.User/username=' + params.username
										break;
								}
							}
						}

						if (link.href.indexOf('/post/download') > -1 || Ext.isFunction(link.onclick)) {
							var img = Ext.fly(link).query('img')[0];
							var href = link.href;
							if (img) href = img.src;
							if (href.indexOf('?') > -1) {
								var params = Ext.Object.fromQueryString(href.split('?')[1])
								if (params.fileid) {
									link.onclick = null
									Ext.fly(link).on('click', function(e) {
										e.stopEvent()
										downloadFile('/resolve/service/social/download?id=' + params.fileid)
									})
								}
							}
						}

						if (panel.isMini) {
							link.target = '_blank'
							if (link.href.indexOf('social.jsp') > -1) link.href = '/resolve/jsp/rsclient.jsp' + link.hash
						}
					})

					Ext.Array.forEach(images, function(image) {
						var image = Ext.get(image),
							fullData = image.getAttribute('fullData');

						//convert legacy images
						var imageSrc = Ext.fly(image).dom.src;
						if (imageSrc.indexOf('/post/download') > -1) {
							var params = Ext.Object.fromQueryString(imageSrc.split('?')[1])
							if (params.fileid) {
								var fileid = params.fileid
								if (Ext.String.endsWith(fileid, '"')) fileid = fileid.substring(0, fileid.length - 1)
								Ext.fly(image).dom.src = '/resolve/service/social/download?preview=true&id=' + fileid
								fullData = '/resolve/service/social/download?id=' + fileid
							}
						}

						if (fullData) image.setWidth(200) //force only internal images to 200px (rss images should appear as they are+)

						image.on('click', function() {
							displayLargeImage(fullData || image.src, '')
						})

						image.on('load', function() {
							//force refresh of parent container because this image will load an change the size of the content after being loaded
							panel.ownerCt.doLayout()
						})
					})
				}
			}
		}]
	}]
})
glu.defView('RS.social.Stream', {
	xtype: 'container',
	hidden: '@{!displayStream}',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	ui: 'social-navigation-stream',
	cls: 'stream-@{selectedCss}',
	overCls: 'stream-over',
	bodyStyle: 'background:transparent',
	height: 22,
	items: [{
		ui: 'social-navigation-stream',
		html: '@{display}',
		tooltip: '@{displayName}',
		bodyStyle: 'background:transparent',
		flex: 1,
		listeners: {
			render: function(panel) {
				var tip = Ext.create('Ext.tip.ToolTip', {
					target: panel.getEl(),
					html: panel.tooltip
				});
			}
		}
	}, {
		xtype: 'tool',
		hidden: '@{!displayTool}',
		pinned: '@{pinned}',
		locked: '@{lock}',
		showPopout: '@{showPopout}',
		type: 'toggle-collapsed',
		displayView: '@{displayView}',
		displaySettings: '@{displaySettings}',
		style: 'margin-right: 10px',
		isMe: '@{isMe}',
		isSystem: '@{isSystem}',
		allowUnfollow: '@{allowUnfollow}',
		sendDigest: '@{sendDigest}',
		sendEmail: '@{sendEmail}',
		showDigest: '@{showDigest}',
		showEmail: '@{showEmail}',
		handler: function() {
			var me = this;
			if (!me.menu)
				me._vm.getNotification(function() {
					me.menu = Ext.create('Ext.menu.Menu', {
						items: [{
							text: RS.social.locale.view,
							iconCls: 'icon-play icon-large stream-menu-icon',
							hidden: !me.displayView,
							handler: function() {
								me.fireEvent('view', me)
							}
						}, {
							text: RS.social.locale.settings,
							iconCls: 'icon-cog icon-large stream-menu-icon',
							hidden: !me.displaySettings,
							handler: function() {
								me.fireEvent('configure', me)
							}
						}, {
							text: RS.social.locale.configureNotifications,
							hidden: !me.isSystem,
							iconCls: 'icon-large icon-cog stream-menu-icon',
							handler: function() {
								me.fireEvent('configureNotifications', me)
							}
						}, {
							itemId: 'pinMenu',
							text: '',
							iconCls: 'icon-pushpin icon-large stream-menu-icon',
							handler: function() {
								me.fireEvent('togglePinned', me)
							}
						}, {
							text: RS.social.locale.markAllRead,
							iconCls: 'icon-ok icon-large stream-menu-icon',
							handler: function() {
								me.fireEvent('markStreamRead', me)
							}
						}, {
							text: RS.social.locale.unfollow,
							iconCls: 'icon-reply icon-large stream-menu-icon',
							hidden: me.isMe || !me.allowUnfollow,
							handler: function() {
								me.fireEvent('unfollowStream', me)
							}
						}, {
							itemId: 'lockMenu',
							hidden: me.isMe,
							text: '',
							iconCls: 'icon-large',
							handler: function() {
								me.fireEvent('toggleLocked', me)
							}
						}, {
							text: RS.social.locale.popout,
							hidden: !me.showPopout,
							iconCls: 'icon-external-link icon-large stream-menu-icon',
							handler: function() {
								me.fireEvent('popout', me)
							}
						}, '-', {
							itemId: 'digest',
							hidden: !me.showDigest,
							text: RS.social.locale.digest,
							checked: me.sendDigest,
							listeners: {
								checkchange: function(item, checked) {
									me.fireEvent('digestChanged', item, checked)
								}
							}
						}, {
							itemId: 'email',
							hidden: !me.showEmail,
							text: RS.social.locale.forward,
							checked: me.sendEmail,
							listeners: {
								checkchange: function(item, checked) {
									me.fireEvent('emailForwardChanged', item, checked)
								}
							}
						}],

						listeners: {
							beforehide: function() {
								if (Ext.isIE8m)
									Ext.each(this.query('menuitem[iconCls*="stream-menu-icon"]'), function(item) {
										if (!item.isVisible())
											return;
										var icons = item.getEl().query('*[id*="iconEl"]')
										Ext.each(icons, function(icon) {
											// alert(Ext.fly(icon).getVisibilityMode())
											// Ext.fly(icon).hide();
											icon.style.display = 'none';
										});
									});
							},
							show: function() {
								if (Ext.isIE8m)
									Ext.each(this.query('menuitem[iconCls*="stream-menu-icon"]'), function(item) {
										if (!item.isVisible())
											return;
										var icons = item.getEl().query('*[id*="iconEl"]')
										Ext.each(icons, function(icon) {
											icon.style.display = 'block';
										});
									});
							},
							afterLayout: function() {
								this.alignTo(me.getEl(), 'br');
							}
						}
					})
					me.menu.down('#pinMenu').setIconCls(me.pinned ? 'icon-pushpin icon-large stream-menu-icon icon-rotate-90' : 'icon-pushpin icon-large stream-menu-icon')
					me.menu.down('#pinMenu').setText(me.pinned ? RS.social.locale.unpin : RS.social.locale.pin)
					me.menu.down('#lockMenu').setIconCls(me.locked ? 'icon-lock icon-large stream-menu-icon' : 'icon-unlock icon-large stream-menu-icon')
					me.menu.down('#lockMenu').setText(me.locked ? RS.social.locale.unlock : RS.social.locale.lock)
					me.menu.down('#digest').setChecked(me.sendDigest)
					me.menu.down('#email').setChecked(me.sendEmail)
					me.menu.show();
				});
			else
				me.menu.showBy(me)
		},
		setPinned: function(value) {
			this.pinned = value
		},
		setLocked: function(value) {
			this.locked = value
		},
		setShowPopout: function(value) {
			this.showPopout = value
		},
		setSendDigest: function(value) {
			this.sendDigest = value
		},
		setSendEmail: function(value) {
			this.sendEmail = value
		},
		listeners: {
			view: '@{viewStream}',
			configure: '@{configureStream}',
			togglePinned: '@{togglePinned}',
			markStreamRead: '@{markStreamRead}',
			unfollowStream: '@{unfollowStream}',
			toggleLocked: '@{toggleLocked}',
			popout: '@{popout}',
			digestChanged: '@{digestChanged}',
			emailForwardChanged: '@{emailForwardChanged}',
			configureNotifications: '@{configureNotifications}'
		}
	}],
	listeners: {
		afterrender: function(panel) {
			panel.getEl().on('mouseover', function() {
				panel.fireEvent('mouseOver', panel)
			})
			panel.getEl().on('mouseout', function(e) {
				if (!e.within(panel.getEl(), true))
					panel.fireEvent('mouseOut', panel)
			})
			panel.getEl().on('click', function() {
				panel.fireEvent('select', panel, panel)
			})
			panel.getEl().on('dblclick', function() {
				panel.fireEvent('doubleClick', panel, panel)
			})
		},
		mouseOver: '@{mouseOver}',
		mouseOut: '@{mouseOut}',
		select: '@{select}',
		doubleClick: '@{doubleClick}'
	}
})
glu.defView('RS.social.StreamGroup', {
	title: '@{display}',
	ui: 'social-navigation-group',
	titleCollapse: true,
	collapsible: true,
	hideCollapseTool: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	isMe: '@{isMe}',
	items: '@{streams}',
	headerOverCls: 'stream-over',
	canCollapse: '@{canCollapse}',
	sendDigest: '@{sendDigest}',
	sendEmail: '@{sendEmail}',
	setCanCollapse: function(value) {
		this.canCollapse = value
	},

	dockedItems: [{
		xtype: 'pagingtoolbar',
		dock: 'bottom',
		store: '@{store}',
		hidden: '@{hidePagingToolbar}',
		displayInfo: true,
		items: {
			xtype: 'numberfield',
			width: 180,
			name: 'itemsPerPage',
			fieldLabel: 'Items per page',
			allowDecimals: false,
			minValue: 1,
			maxValue: 40,
			value: 10,
			step: 5,
			listeners: {
				specialkey: function(field, e) {
					if (e.getKey() == e.ENTER) {
						var	value = field.getValue();
						if (value > 40) {
							return;
						}
						var store = this.up('pagingtoolbar').store;
						store.pageSize = value;
						store.load();
					}
				}
			}
		}
	}],

	tools: [{
		xtype: 'tool',
		type: 'plus',
		cls: 'social-follow icon-share-alt',
		style: 'top:0px !important',
		handler: function() {
			this.up('panel').fireEvent('followClicked', this, this)
		},
		listeners: {
			render: function(tool) {
				Ext.create('Ext.tip.ToolTip', {
					html: RS.social.locale.followStream,
					target: tool.getEl()
				})
				tool.up('panel').fireEvent('registerTool', tool, tool)
			}
		}
	}, {
		xtype: 'tool',
		type: 'toggle-collapsed',
		cls: 'social-group-follow',
		handler: function() {
			var me = this;
			if (!me.menu) me.menu = Ext.create('Ext.menu.Menu', {
				items: [
					/*{
					itemId: 'digest',
					text: RS.social.locale.digest,
					checked: me.up('panel').sendDigest,
					listeners: {
						checkchange: function(item, checked) {
							me.up('panel').fireEvent('digestChanged', item, checked)
						}
					}
				},
					{
						itemId: 'email',
						text: RS.social.locale.forward,
						checked: me.up('panel').sendEmail,
						listeners: {
							checkchange: function(item, checked) {
								me.up('panel').fireEvent('digestChanged', item, checked)
							}
						}
					}, */
					{
						text: RS.social.locale.applyDigest,
						handler: function(item) {
							me.up('panel').fireEvent('digestApplied', item)
						}
					}, {
						text: RS.social.locale.unapplyDigest,
						handler: function() {
							me.up('panel').fireEvent('unapplyDigest')
						}
					}, {
						text: RS.social.locale.applyEmail,
						handler: function(item) {
							me.up('panel').fireEvent('emailApplied', item)
						}
					}, {
						text: RS.social.locale.unapplyEmail,
						handler: function(item) {
							me.up('panel').fireEvent('unapplyEmail', item)
						}
					}
				]
			})
			// me.menu.down('#digest').setChecked(me.up('panel').sendDigest)
			// me.menu.down('#email').setChecked(me.up('panel').sendEmail)
			me.menu.showBy(me)
		}
	}],
	listeners: {
		followClicked: '@{joinComponent}',
		gearClicked: '@{openComponentSettings}',
		registerTool: '@{registerTool}',
		digestChanged: '@{digestChanged}',
		emailForwardChanged: '@{emailForwardChanged}',
		digestApplied: '@{digestApplied}',
		unapplyDigest: '@{unapplyDigest}',
		emailApplied: '@{emailApplied}',
		unapplyEmail: '@{unapplyEmail}',
		toggleCollapse: '@{toggleCollapse}',
		beforeCollapse: function(panel) {
			if (panel.canCollapse)
				panel.fireEvent('toggleCollapse')
				// return panel.canCollapse
			return false
		},
		afterrender: function( panel, eOpts ) {
			// NOTE
			// To get around the issue with glupagingtoolbar that registers 'beforechange' with
			// a method returning 'false' that prevents the paging actions from working.
			//
			panel.down('pagingtoolbar').hasListeners['beforechange'] = 0;
		}
	}
});

glu.defView('RS.social.Blank', {
	html: '&nbsp;',
	height: 1
});
glu.defView('RS.social.StreamPicker', {
	padding : 15,
	title: '@{windowTitle}',
	modal: true,
	width: 900,
	height: 500,
	layout: 'card',
	activeItem: '@{activeItem}',
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'toGrid',
		columns: '@{toGridColumns}',
		selType: 'checkboxmodel',
		dockedItems: [{
			name: 'actionBar',
			xtype: 'toolbar',
			cls : 'rs-dockedtoolbar',
			items: [{
				xtype: 'combobox',
				name: 'type',
				emptyText: '~~type~~',
				hideLabel: true,
				displayField: 'name',
				valueField: 'value',
				queryMode: 'local',
				editable: false,
				listConfig: {
					tpl: '<tpl for="."><div class="x-boundlist-item">{name}</div><tpl if="xindex == 3"><hr /></tpl></tpl>'
				},
				listeners: {
					render: function(combo) {
						combo.store.on('load', function(store, records) {
							if (combo.getValue())
								combo.select(combo.store.getAt(combo.store.findExact('value', combo.getValue())))
						}, combo, {
							single: true
						})
					}
				}
			}, {
				xtype: 'textfield',
				name: 'searchQuery',
				hideLabel: true,
				minWidth: 300,
				flex: 1,
				emptyText: '~~searchQuery~~'
			}, {
				xtype: 'button',
				handler: '@{search}',
				iconCls: 'icon-search rs-icon-button'
			}]
		}],
		plugins: [{
			ptype: 'pager',
			showSysInfo: false
		}]
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'worksheets',
		flex: 1,
		columns: '@{worksheetsColumns}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: [{
				xtype: 'combobox',
				name: 'type',
				emptyText: '~~type~~',
				hideLabel: true,
				displayField: 'name',
				valueField: 'value',
				queryMode: 'local',
				listeners: {
					render: function(combo) {
						combo.store.on('load', function(store, records) {
							if (combo.getValue())
								combo.select(combo.store.getAt(combo.store.findExact('value', combo.getValue())))
						}, combo, {
							single: true
						})
					}
				}
			}, {
				xtype: 'textfield',
				name: 'searchText',
				hideLabel: true,
				width: 250,
				emptyText: '~~searchWorksheet~~'
			}, {
				iconCls: 'icon-large icon-user rs-icon @{myWorksheetsPressedCls}',
				name: 'myWorksheets',
				text: '',
				tooltip: '@{myWorksheetsTooltip}'
			}]
		}],
		plugins: [{
			ptype: 'pager',
			showSysInfo: false,
			allowAutoRefresh: false
		}]
	}],	
	buttons: [{
		name :'select', 
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name :'follow', 
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name :'addFollowers', 
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name: 'createComponent',
		cls : 'rs-med-btn rs-btn-dark',
		roleHide: true,
		roles: 'social_admin'
	},{
		name :'cancel', 
		cls : 'rs-med-btn rs-btn-light'
	}]
})
/**
 * BoxSelect for ExtJS 4.1, a combo box improved for multiple value querying, selection and management.
 *
 * A friendlier combo box for multiple selections that creates easily individually
 * removable labels for each selection, as seen on facebook and other sites. Querying
 * and type-ahead support are also improved for multiple selections.
 *
 * Options and usage mostly remain consistent with the standard
 * [ComboBox](http://docs.sencha.com/ext-js/4-1/#!/api/Ext.form.field.ComboBox) control.
 * Some default configuration options have changed, but most should still work properly
 * if overridden unless otherwise noted.
 *
 * Please note, this component does not support versions of ExtJS earlier than 4.1.
 *
 * Inspired by the [SuperBoxSelect component for ExtJS 3](http://technomedia.co.uk/SuperBoxSelect/examples3.html),
 * which in turn was inspired by the [BoxSelect component for ExtJS 2](http://efattal.fr/en/extjs/extuxboxselect/).
 *
 * Various contributions and suggestions made by many members of the ExtJS community which can be seen
 * in the [official user extension forum post](http://www.sencha.com/forum/showthread.php?134751-Ext.ux.form.field.BoxSelect).
 *
 * Many thanks go out to all of those who have contributed, this extension would not be
 * possible without your help.
 *
 * See [AUTHORS.txt](../AUTHORS.TXT) for a list of major contributors
 *
 * @author kvee_iv http://www.sencha.com/forum/member.php?29437-kveeiv
 * @version 2.0.3
 * @requires BoxSelect.css
 * @xtype boxselect
 *
 */
Ext.define('Ext.ux.form.field.BoxSelect', {
    extend: 'Ext.form.field.ComboBox',
    alias: ['widget.comboboxselect', 'widget.boxselect'],
    requires: ['Ext.selection.Model', 'Ext.data.Store'],

    //
    // Begin configuration options related to the underlying store
    //

    /**
     * @cfg {String} valueParam
     * The name of the parameter used to load unknown records into the store. If left unspecified, {@link #valueField}
     * will be used.
     */

    //
    // End of configuration options related to the underlying store
    //



    //
    // Begin configuration options related to selected values
    //

    /**
     * @cfg {Boolean}
     * If set to `true`, allows the combo field to hold more than one value at a time, and allows selecting multiple
     * items from the dropdown list. The combo's text field will show all selected values using the template
     * defined by {@link #labelTpl}.
     *
     
     */
    multiSelect: true,

    /**
     * @cfg {String/Ext.XTemplate} labelTpl
     * The [XTemplate](http://docs.sencha.com/ext-js/4-1/#!/api/Ext.XTemplate) to use for the inner
     * markup of the labelled items. Defaults to the configured {@link #displayField}
     */

    /**
     * @cfg
     * @inheritdoc
     *
     * When {@link #forceSelection} is `false`, new records can be created by the user as they
     * are typed. These records are **not** added to the combo's store. This creation
     * is triggered by typing the configured 'delimiter', and can be further configured using the
     * {@link #createNewOnEnter} and {@link #createNewOnBlur} configuration options.
     *
     * This functionality is primarily useful with BoxSelect components for things
     * such as an email address.
     */
    forceSelection: true,

    /**
     * @cfg {Boolean}
     * Has no effect if {@link #forceSelection} is `true`.
     *
     * With {@link #createNewOnEnter} set to `true`, the creation described in
     * {@link #forceSelection} will also be triggered by the 'enter' key.
     */
    createNewOnEnter: false,

    /**
     * @cfg {Boolean}
     * Has no effect if {@link #forceSelection} is `true`.
     *
     * With {@link #createNewOnBlur} set to `true`, the creation described in
     * {@link #forceSelection} will also be triggered when the field loses focus.
     *
     * Please note that this behavior is also affected by the configuration options
     * {@link #autoSelect} and {@link #selectOnTab}. If those are true and an existing
     * item would have been selected as a result, the partial text the user has entered will
     * be discarded and the existing item will be added to the selection.
     */
    createNewOnBlur: false,

    /**
     * @cfg {Boolean}
     * Has no effect if {@link #multiSelect} is `false`.
     *
     * Controls the formatting of the form submit value of the field as returned by {@link #getSubmitValue}
     *
     * - `true` for the field value to submit as a json encoded array in a single GET/POST variable
     * - `false` for the field to submit as an array of GET/POST variables
     */
    encodeSubmitValue: false,

    //
    // End of configuration options related to selected values
    //



    //
    // Configuration options related to pick list behavior
    //

    /**
     * @cfg {Boolean}
     * `true` to activate the trigger when clicking in empty space in the field. Note that the
     * subsequent behavior of this is controlled by the field's {@link #triggerAction}.
     * This behavior is similar to that of a basic ComboBox with {@link #editable} `false`.
     */
    triggerOnClick: true,

    /**
     * @cfg {Boolean}
     * - `true` to have each selected value fill to the width of the form field
     * - `false to have each selected value size to its displayed contents
     */
    stacked: false,

    /**
     * @cfg {Boolean}
     * Has no effect if {@link #multiSelect} is `false`
     *
     * `true` to keep the pick list expanded after each selection from the pick list
     * `false` to automatically collapse the pick list after a selection is made
     */
    pinList: true,

    /**
     * @cfg {Boolean}
     * True to hide the currently selected values from the drop down list. These items are hidden via
     * css to maintain simplicity in store and filter management.
     *
     * - `true` to hide currently selected values from the drop down pick list
     * - `false` to keep the item in the pick list as a selected item
     */
    filterPickList: false,

    //
    // End of configuration options related to pick list behavior
    //



    //
    // Configuration options related to text field behavior
    //

    /**
     * @cfg
     * @inheritdoc
     */
    selectOnFocus: true,

    /**
     * @cfg {Boolean}
     *
     * `true` if this field should automatically grow and shrink vertically to its content.
     * Note that this overrides the natural trigger grow functionality, which is used to size
     * the field horizontally.
     */
    grow: true,

    /**
     * @cfg {Number/Boolean}
     * Has no effect if {@link #grow} is `false`
     *
     * The minimum height to allow when {@link #grow} is `true`, or `false` to allow for
     * natural vertical growth based on the current selected values. See also {@link #growMax}.
     */
    growMin: false,

    /**
     * @cfg {Number/Boolean}
     * Has no effect if {@link #grow} is `false`
     *
     * The maximum height to allow when {@link #grow} is `true`, or `false` to allow for
     * natural vertical growth based on the current selected values. See also {@link #growMin}.
     */
    growMax: false,

    /**
     * @cfg growAppend
     * @hide
     * Currently unsupported by BoxSelect since this is used for horizontal growth and
     * BoxSelect only supports vertical growth.
     */
    /**
     * @cfg growToLongestValue
     * @hide
     * Currently unsupported by BoxSelect since this is used for horizontal growth and
     * BoxSelect only supports vertical growth.
     */

    //
    // End of configuration options related to text field behavior
    //


    //
    // Event signatures
    //

    /**
     * @event autosize
     * Fires when the **{@link #autoSize}** function is triggered and the field is resized according to the
     * {@link #grow}/{@link #growMin}/{@link #growMax} configs as a result. This event provides a hook for the
     * developer to apply additional logic at runtime to resize the field if needed.
     * @param {Ext.ux.form.field.BoxSelect} this This BoxSelect field
     * @param {Number} height The new field height
     */

    //
    // End of event signatures
    //



    //
    // Configuration options that will break things if messed with
    //

    /**
     * @private
     */
    fieldSubTpl: [
        '<div id="{cmpId}-listWrapper" class="x-boxselect {fieldCls} {typeCls}">',
        '<ul id="{cmpId}-itemList" class="x-boxselect-list">',
        '<li id="{cmpId}-inputElCt" class="x-boxselect-input">',
        '<div id="{cmpId}-emptyEl" class="{emptyCls}">{emptyText}</div>',
        '<input id="{cmpId}-inputEl" type="{type}" ',
        '<tpl if="name">name="{name}" </tpl>',
        '<tpl if="value"> value="{[Ext.util.Format.htmlEncode(values.value)]}"</tpl>',
        '<tpl if="size">size="{size}" </tpl>',
        '<tpl if="tabIdx">tabIndex="{tabIdx}" </tpl>',
        '<tpl if="disabled"> disabled="disabled"</tpl>',
        'class="x-boxselect-input-field {inputElCls}" autocomplete="off">',
        '</li>',
        '</ul>',
        '</div>', {
            compiled: true,
            disableFormats: true
        }
    ],

    /**
     * @private
     */
    childEls: ['listWrapper', 'itemList', 'inputEl', 'inputElCt', 'emptyEl'],

    /**
     * @private
     */
    componentLayout: 'boxselectfield',

    /**
     * @private
     */
    emptyInputCls: 'x-boxselect-emptyinput',

    /**
     * @inheritdoc
     *
     * Initialize additional settings and enable simultaneous typeAhead and multiSelect support
     * @protected
     */
    initComponent: function() {
        var me = this,
            typeAhead = me.typeAhead;

        if (typeAhead && !me.editable) {
            Ext.Error.raise('If typeAhead is enabled the combo must be editable: true -- please change one of those settings.');
        }

        Ext.apply(me, {
            typeAhead: false
        });

        me.callParent();

        me.typeAhead = typeAhead;

        me.selectionModel = new Ext.selection.Model({
            store: me.valueStore,
            mode: 'MULTI',
            lastFocused: null,
            onSelectChange: function(record, isSelected, suppressEvent, commitFn) {
                commitFn();
            }
        });

        if (!Ext.isEmpty(me.delimiter) && me.multiSelect) {
            me.delimiterRegexp = new RegExp(String(me.delimiter).replace(/[$%()*+.?\[\\\]{|}]/g, "\\$&"));
        }
    },

    /**
     * Register events for management controls of labelled items
     * @protected
     */
    initEvents: function() {
        var me = this;

        me.callParent(arguments);

        if (!me.enableKeyEvents) {
            me.mon(me.inputEl, 'keydown', me.onKeyDown, me);
        }
        me.mon(me.inputEl, 'paste', me.onPaste, me);
        me.mon(me.listWrapper, 'click', me.onItemListClick, me);

        // I would prefer to use relayEvents here to forward these events on, but I want
        // to pass the field instead of exposing the underlying selection model
        me.mon(me.selectionModel, {
            'selectionchange': function(selModel, selectedRecs) {
                me.applyMultiselectItemMarkup();
                me.fireEvent('valueselectionchange', me, selectedRecs);
            },
            'focuschange': function(selectionModel, oldFocused, newFocused) {
                me.fireEvent('valuefocuschange', me, oldFocused, newFocused);
            },
            scope: me
        });
    },

    /**
     * @inheritdoc
     *
     * Create a store for the records of our current value based on the main store's model
     * @protected
     */
    onBindStore: function(store, initial) {
        var me = this;

        if (store) {
            me.valueStore = new Ext.data.Store({
                model: store.model,
                proxy: {
                    type: 'memory'
                }
            });
            me.mon(me.valueStore, 'datachanged', me.applyMultiselectItemMarkup, me);
            if (me.selectionModel) {
                me.selectionModel.bindStore(me.valueStore);
            }
        }
    },

    /**
     * @inheritdoc
     *
     * Remove the selected value store and associated listeners
     * @protected
     */
    onUnbindStore: function(store) {
        var me = this,
            valueStore = me.valueStore;

        if (valueStore) {
            if (me.selectionModel) {
                me.selectionModel.setLastFocused(null);
                me.selectionModel.deselectAll();
                me.selectionModel.bindStore(null);
            }
            me.mun(valueStore, 'datachanged', me.applyMultiselectItemMarkup, me);
            valueStore.destroy();
            me.valueStore = null;
        }

        me.callParent(arguments);
    },

    /**
     * @inheritdoc
     *
     * Add refresh tracking to the picker for selection management
     * @protected
     */
    createPicker: function() {
        var me = this,
            picker = me.callParent(arguments);

        me.mon(picker, {
            'beforerefresh': me.onBeforeListRefresh,
            scope: me
        });

        if (me.filterPickList) {
            picker.addCls('x-boxselect-hideselections');
        }

        return picker;
    },

    /**
     * @inheritdoc
     *
     * Clean up selected values management controls
     * @protected
     */
    onDestroy: function() {
        var me = this;

        Ext.destroyMembers(me, 'valueStore', 'selectionModel');

        me.callParent(arguments);
    },

    /**
     * Add empty text support to initial render.
     * @protected
     */
    getSubTplData: function() {
        var me = this,
            data = me.callParent(),
            isEmpty = me.emptyText && data.value.length < 1;

        data.value = '';
        if (isEmpty) {
            data.emptyText = me.emptyText;
            data.emptyCls = me.emptyCls;
            data.inputElCls = me.emptyInputCls;
        } else {
            data.emptyText = '';
            data.emptyCls = me.emptyInputCls;
            data.inputElCls = '';
        }

        return data;
    },

    /**
     * @inheritdoc
     *
     * Overridden to avoid use of placeholder, as our main input field is often empty
     * @protected
     */
    afterRender: function() {
        var me = this;

        if (Ext.supports.Placeholder && me.inputEl && me.emptyText) {
            delete me.inputEl.dom.placeholder;
        }

        me.bodyEl.applyStyles('vertical-align:top');

        if (me.grow) {
            if (Ext.isNumber(me.growMin) && (me.growMin > 0)) {
                me.listWrapper.applyStyles('min-height:' + me.growMin + 'px');
            }
            if (Ext.isNumber(me.growMax) && (me.growMax > 0)) {
                me.listWrapper.applyStyles('max-height:' + me.growMax + 'px');
            }
        }

        if (me.stacked === true) {
            me.itemList.addCls('x-boxselect-stacked');
        }

        if (!me.multiSelect) {
            me.itemList.addCls('x-boxselect-singleselect');
        }

        me.applyMultiselectItemMarkup();

        me.callParent(arguments);
    },

    /**
     * Overridden to search entire unfiltered store since already selected values
     * can span across multiple store page loads and other filtering. Overlaps
     * some with {@link #isFilteredRecord}, but findRecord is used by the base component
     * for various logic so this logic is applied here as well.
     * @protected
     */
    findRecord: function(field, value) {
        var ds = this.store,
            matches;

        if (!ds) {
            return false;
        }

        matches = ds.queryBy(function(rec, id) {
            return rec.isEqual(rec.get(field), value);
        });

        return (matches.getCount() > 0) ? matches.first() : false;
    },

    /**
     * Overridden to map previously selected records to the "new" versions of the records
     * based on value field, if they are part of the new store load
     * @protected
     */
    onLoad: function() {
        var me = this,
            valueField = me.valueField,
            valueStore = me.valueStore,
            changed = false;

        if (valueStore) {
            if (!Ext.isEmpty(me.value) && (valueStore.getCount() == 0)) {
                me.setValue(me.value, false, true);
            }

            valueStore.suspendEvents();
            valueStore.each(function(rec) {
                var r = me.findRecord(valueField, rec.get(valueField)),
                    i = r ? valueStore.indexOf(rec) : -1;
                if (i >= 0) {
                    valueStore.removeAt(i);
                    valueStore.insert(i, r);
                    changed = true;
                }
            });
            valueStore.resumeEvents();
            if (changed) {
                valueStore.fireEvent('datachanged', valueStore);
            }
        }

        me.callParent(arguments);
    },

    /**
     * Used to determine if a record is filtered out of the current store's data set,
     * for determining if a currently selected value should be retained.
     *
     * Slightly complicated logic. A record is considered filtered and should be retained if:
     *
     * - It is not in the combo store and the store has no filter or it is in the filtered data set
     *   (Happens when our selected value is just part of a different load, page or query)
     * - It is not in the combo store and forceSelection is false and it is in the value store
     *   (Happens when our selected value was created manually)
     *
     * @private
     */
    isFilteredRecord: function(record) {
        var me = this,
            store = me.store,
            valueField = me.valueField,
            storeRecord,
            filtered = false;

        storeRecord = store.findExact(valueField, record.get(valueField));

        filtered = ((storeRecord === -1) && (!store.snapshot || (me.findRecord(valueField, record.get(valueField)) !== false)));

        filtered = filtered || (!filtered && (storeRecord === -1) && (me.forceSelection !== true) &&
            (me.valueStore.findExact(valueField, record.get(valueField)) >= 0));

        return filtered;
    },

    /**
     * @inheritdoc
     *
     * Overridden to allow for continued querying with multiSelect selections already made
     * @protected
     */
    doRawQuery: function() {
        var me = this,
            rawValue = me.inputEl.dom.value;

        if (me.multiSelect) {
            rawValue = rawValue.split(me.delimiter).pop();
        }

        this.doQuery(rawValue, false, true);
    },

    /**
     * When the picker is refreshing, we should ignore selection changes. Otherwise
     * the value of our field will be changing just because our view of the choices is.
     * @protected
     */
    onBeforeListRefresh: function() {
        this.ignoreSelection++;
    },

    /**
     * When the picker is refreshing, we should ignore selection changes. Otherwise
     * the value of our field will be changing just because our view of the choices is.
     * @protected
     */
    onListRefresh: function() {
        this.callParent(arguments);
        if (this.ignoreSelection > 0) {
            --this.ignoreSelection;
        }
    },

    /**
     * Overridden to preserve current labelled items when list is filtered/paged/loaded
     * and does not include our current value. See {@link #isFilteredRecord}
     * @private
     */
    onListSelectionChange: function(list, selectedRecords) {
        var me = this,
            valueStore = me.valueStore,
            mergedRecords = [],
            i;

        // Only react to selection if it is not called from setValue, and if our list is
        // expanded (ignores changes to the selection model triggered elsewhere)
        if ((me.ignoreSelection <= 0) && me.isExpanded) {
            // Pull forward records that were already selected or are now filtered out of the store
            valueStore.each(function(rec) {
                if (Ext.Array.contains(selectedRecords, rec) || me.isFilteredRecord(rec)) {
                    mergedRecords.push(rec);
                }
            });
            mergedRecords = Ext.Array.merge(mergedRecords, selectedRecords);

            i = Ext.Array.intersect(mergedRecords, valueStore.getRange()).length;
            if ((i != mergedRecords.length) || (i != me.valueStore.getCount())) {
                me.setValue(mergedRecords, false);
                if (!me.multiSelect || !me.pinList) {
                    Ext.defer(me.collapse, 1, me);
                }
                if (valueStore.getCount() > 0) {
                    me.fireEvent('select', me, valueStore.getRange());
                }
            }
            me.inputEl.focus();
            if (!me.pinList) {
                me.inputEl.dom.value = '';
            }
            if (me.selectOnFocus) {
                me.inputEl.dom.select();
            }
        }
    },

    /**
     * Overridden to use valueStore instead of valueModels, for inclusion of
     * filtered records. See {@link #isFilteredRecord}
     * @private
     */
    syncSelection: function() {
        var me = this,
            picker = me.picker,
            valueField = me.valueField,
            pickStore, selection, selModel;

        if (picker) {
            pickStore = picker.store;

            // From the value, find the Models that are in the store's current data
            selection = [];
            if (me.valueStore) {
                me.valueStore.each(function(rec) {
                    var i = pickStore.findExact(valueField, rec.get(valueField));
                    if (i >= 0) {
                        selection.push(pickStore.getAt(i));
                    }
                });
            }

            // Update the selection to match
            me.ignoreSelection++;
            selModel = picker.getSelectionModel();
            selModel.deselectAll();
            if (selection.length > 0) {
                selModel.select(selection);
            }
            if (me.ignoreSelection > 0) {
                --me.ignoreSelection;
            }
        }
    },

    /**
     * Overridden to align to itemList size instead of inputEl
     */
    doAlign: function() {
        var me = this,
            picker = me.picker,
            aboveSfx = '-above',
            isAbove;

        me.picker.alignTo(me.listWrapper, me.pickerAlign, me.pickerOffset);
        // add the {openCls}-above class if the picker was aligned above
        // the field due to hitting the bottom of the viewport
        isAbove = picker.el.getY() < me.inputEl.getY();
        me.bodyEl[isAbove ? 'addCls' : 'removeCls'](me.openCls + aboveSfx);
        picker[isAbove ? 'addCls' : 'removeCls'](picker.baseCls + aboveSfx);
    },

    /**
     * Overridden to preserve scroll position of pick list when list is realigned
     */
    alignPicker: function() {
        var me = this,
            picker = me.picker,
            pickerScrollPos = picker.getTargetEl().dom.scrollTop;

        me.callParent(arguments);

        if (me.isExpanded) {
            if (me.matchFieldWidth) {
                // Auto the height (it will be constrained by min and max width) unless there are no records to display.
                picker.setWidth(me.listWrapper.getWidth());
            }

            picker.getTargetEl().dom.scrollTop = pickerScrollPos;
        }
    },

    /**
     * Get the current cursor position in the input field, for key-based navigation
     * @private
     */
    getCursorPosition: function() {
        var cursorPos;
        if (Ext.isIE) {
            cursorPos = document.selection.createRange();
            cursorPos.collapse(true);
            cursorPos.moveStart("character", -this.inputEl.dom.value.length);
            cursorPos = cursorPos.text.length;
        } else {
            cursorPos = this.inputEl.dom.selectionStart;
        }
        return cursorPos;
    },

    /**
     * Check to see if the input field has selected text, for key-based navigation
     * @private
     */
    hasSelectedText: function() {
        var sel, range;
        if (Ext.isIE) {
            sel = document.selection;
            range = sel.createRange();
            return (range.parentElement() == this.inputEl.dom);
        } else {
            return this.inputEl.dom.selectionStart != this.inputEl.dom.selectionEnd;
        }
    },

    /**
     * Handles keyDown processing of key-based selection of labelled items.
     * Supported keyboard controls:
     *
     * - If pick list is expanded
     *
     *     - `CTRL-A` will select all the items in the pick list
     *
     * - If the cursor is at the beginning of the input field and there are values present
     *
     *     - `CTRL-A` will highlight all the currently selected values
     *     - `BACKSPACE` and `DELETE` will remove any currently highlighted selected values
     *     - `RIGHT` and `LEFT` will move the current highlight in the appropriate direction
     *     - `SHIFT-RIGHT` and `SHIFT-LEFT` will add to the current highlight in the appropriate direction
     *
     * @protected
     */
    onKeyDown: function(e, t) {
        var me = this,
            key = e.getKey(),
            rawValue = me.inputEl.dom.value,
            valueStore = me.valueStore,
            selModel = me.selectionModel,
            stopEvent = false;

        if (me.readOnly || me.disabled || !me.editable) {
            return;
        }

        if (me.isExpanded && (key == e.A && e.ctrlKey)) {
            // CTRL-A when picker is expanded - add all items in current picker store page to current value
            me.select(me.getStore().getRange());
            selModel.setLastFocused(null);
            selModel.deselectAll();
            me.collapse();
            me.inputEl.focus();
            stopEvent = true;
        } else if ((valueStore.getCount() > 0) &&
            ((rawValue == '') || ((me.getCursorPosition() === 0) && !me.hasSelectedText()))) {
            // Keyboard navigation of current values
            var lastSelectionIndex = (selModel.getCount() > 0) ? valueStore.indexOf(selModel.getLastSelected() || selModel.getLastFocused()) : -1;

            if ((key == e.BACKSPACE) || (key == e.DELETE)) {
                if (lastSelectionIndex > -1) {
                    if (selModel.getCount() > 1) {
                        lastSelectionIndex = -1;
                    }
                    me.valueStore.remove(selModel.getSelection());
                } else {
                    me.valueStore.remove(me.valueStore.last());
                }
                selModel.clearSelections();
                me.setValue(me.valueStore.getRange());
                if (lastSelectionIndex > 0) {
                    selModel.select(lastSelectionIndex - 1);
                }
                stopEvent = true;
            } else if ((key == e.RIGHT) || (key == e.LEFT)) {
                if ((lastSelectionIndex == -1) && (key == e.LEFT)) {
                    selModel.select(valueStore.last());
                    stopEvent = true;
                } else if (lastSelectionIndex > -1) {
                    if (key == e.RIGHT) {
                        if (lastSelectionIndex < (valueStore.getCount() - 1)) {
                            selModel.select(lastSelectionIndex + 1, e.shiftKey);
                            stopEvent = true;
                        } else if (!e.shiftKey) {
                            selModel.setLastFocused(null);
                            selModel.deselectAll();
                            stopEvent = true;
                        }
                    } else if ((key == e.LEFT) && (lastSelectionIndex > 0)) {
                        selModel.select(lastSelectionIndex - 1, e.shiftKey);
                        stopEvent = true;
                    }
                }
            } else if (key == e.A && e.ctrlKey) {
                selModel.selectAll();
                stopEvent = e.A;
            }
            me.inputEl.focus();
        }

        if (stopEvent) {
            me.preventKeyUpEvent = stopEvent;
            e.stopEvent();
            return;
        }

        // Prevent key up processing for enter if it is being handled by the picker
        if (me.isExpanded && (key == e.ENTER) && me.picker.highlightedItem) {
            me.preventKeyUpEvent = true;
        }

        if (me.enableKeyEvents) {
            me.callParent(arguments);
        }

        if (!e.isSpecialKey() && !e.hasModifier()) {
            me.selectionModel.setLastFocused(null);
            me.selectionModel.deselectAll();
            me.inputEl.focus();
        }
    },

    /**
     * Handles auto-selection and creation of labelled items based on this field's
     * delimiter, as well as the keyUp processing of key-based selection of labelled items.
     * @protected
     */
    onKeyUp: function(e, t) {
        var me = this,
            rawValue = me.inputEl.dom.value;

        if (me.preventKeyUpEvent) {
            e.stopEvent();
            if ((me.preventKeyUpEvent === true) || (e.getKey() === me.preventKeyUpEvent)) {
                delete me.preventKeyUpEvent;
            }
            return;
        }

        if (me.multiSelect && (me.delimiterRegexp && me.delimiterRegexp.test(rawValue)) ||
            ((me.createNewOnEnter === true) && e.getKey() == e.ENTER)) {
            rawValue = Ext.Array.clean(rawValue.split(me.delimiterRegexp));
            me.inputEl.dom.value = '';
            me.setValue(me.valueStore.getRange().concat(rawValue));
            me.inputEl.focus();
        }

        me.callParent([e, t]);
    },

    /**
     * Handles auto-selection of labelled items based on this field's delimiter when pasting
     * a list of values in to the field (e.g., for email addresses)
     * @protected
     */
    onPaste: function(e, t) {
        var me = this,
            rawValue = me.inputEl.dom.value,
            clipboard = (e && e.browserEvent && e.browserEvent.clipboardData) ? e.browserEvent.clipboardData : false;

        if (me.multiSelect && (me.delimiterRegexp && me.delimiterRegexp.test(rawValue))) {
            if (clipboard && clipboard.getData) {
                if (/text\/plain/.test(clipboard.types)) {
                    rawValue = clipboard.getData('text/plain');
                } else if (/text\/html/.test(clipboard.types)) {
                    rawValue = clipboard.getData('text/html');
                }
            }

            rawValue = Ext.Array.clean(rawValue.split(me.delimiterRegexp));
            me.inputEl.dom.value = '';
            me.setValue(me.valueStore.getRange().concat(rawValue));
            me.inputEl.focus();
        }
    },

    /**
     * Overridden to handle key navigation of pick list when list is filtered. Because we
     * want to avoid complexity that could be introduced by modifying the store's contents,
     * (e.g., always having to search back through and remove values when they might
     * be re-sent by the server, adding the values back in their previous position when
     * they are removed from the current selection, etc.), we handle this filtering
     * via a simple css rule. However, for the moment since those DOM nodes still exist
     * in the list we have to hijack the highlighting methods for the picker's BoundListKeyNav
     * to appropriately skip over these hidden nodes. This is a less than ideal solution,
     * but it centralizes all of the complexity of this problem in to this one method.
     * @protected
     */
    onExpand: function() {
        var me = this,
            keyNav = me.listKeyNav;

        me.callParent(arguments);

        if (keyNav || !me.filterPickList) {
            return;
        }
        keyNav = me.listKeyNav;
        keyNav.highlightAt = function(index) {
            var boundList = this.boundList,
                item = boundList.all.item(index),
                len = boundList.all.getCount(),
                direction;

            if (item && item.hasCls('x-boundlist-selected')) {
                if ((index == 0) || !boundList.highlightedItem || (boundList.indexOf(boundList.highlightedItem) < index)) {
                    direction = 1;
                } else {
                    direction = -1;
                }
                do {
                    index = index + direction;
                    item = boundList.all.item(index);
                } while ((index > 0) && (index < len) && item.hasCls('x-boundlist-selected'));

                if (item.hasCls('x-boundlist-selected')) {
                    return;
                }
            }

            if (item) {
                item = item.dom;
                boundList.highlightItem(item);
                boundList.getTargetEl().scrollChildIntoView(item, false);
            }
        };
    },

    /**
     * Overridden to get and set the DOM value directly for type-ahead suggestion (bypassing get/setRawValue)
     * @protected
     */
    onTypeAhead: function() {
        var me = this,
            displayField = me.displayField,
            inputElDom = me.inputEl.dom,
            valueStore = me.valueStore,
            boundList = me.getPicker(),
            record, newValue, len, selStart;

        if (me.filterPickList) {
            var fn = this.createFilterFn(displayField, inputElDom.value);
            record = me.store.findBy(function(rec) {
                return ((valueStore.indexOfId(rec.getId()) === -1) && fn(rec));
            });
            record = (record === -1) ? false : me.store.getAt(record);
        } else {
            record = me.store.findRecord(displayField, inputElDom.value);
        }

        if (record) {
            newValue = record.get(displayField);
            len = newValue.length;
            selStart = inputElDom.value.length;
            boundList.highlightItem(boundList.getNode(record));
            if (selStart !== 0 && selStart !== len) {
                inputElDom.value = newValue;
                me.selectText(selStart, newValue.length);
            }
        }
    },

    /**
     * Delegation control for selecting and removing labelled items or triggering list collapse/expansion
     * @protected
     */
    onItemListClick: function(evt, el, o) {
        var me = this,
            itemEl = evt.getTarget('.x-boxselect-item'),
            closeEl = itemEl ? evt.getTarget('.x-boxselect-item-close') : false;

        if (me.readOnly || me.disabled) {
            return;
        }

        evt.stopPropagation();

        if (itemEl) {
            if (closeEl) {
                me.removeByListItemNode(itemEl);
                if (me.valueStore.getCount() > 0) {
                    me.fireEvent('select', me, me.valueStore.getRange());
                }
            } else {
                me.toggleSelectionByListItemNode(itemEl, evt.shiftKey);
            }
            me.inputEl.focus();
        } else {
            if (me.selectionModel.getCount() > 0) {
                me.selectionModel.setLastFocused(null);
                me.selectionModel.deselectAll();
            }
            if (me.triggerOnClick) {
                me.onTriggerClick();
            }
        }
    },

    /**
     * Build the markup for the labelled items. Template must be built on demand due to ComboBox initComponent
     * lifecycle for the creation of on-demand stores (to account for automatic valueField/displayField setting)
     * @private
     */
    getMultiSelectItemMarkup: function() {
        var me = this;

        if (!me.multiSelectItemTpl) {
            if (!me.labelTpl) {
                me.labelTpl = Ext.create('Ext.XTemplate',
                    '{[values.' + me.displayField + ']}');
            } else if (Ext.isString(me.labelTpl) || Ext.isArray(me.labelTpl)) {
                me.labelTpl = Ext.create('Ext.XTemplate', me.labelTpl);
            }

            me.multiSelectItemTpl = [
                '<tpl for=".">',
                '<li class="x-boxselect-item ',
                '<tpl if="this.isSelected(values.' + me.valueField + ')">',
                ' selected',
                '</tpl>',
                '" qtip="{[typeof values === "string" ? values : values.' + me.displayField + ']}">',
                '<div class="x-boxselect-item-text">{[typeof values === "string" ? values : this.getItemLabel(values)]}</div>',
                '<div class="x-tab-close-btn x-boxselect-item-close"></div>',
                '</li>',
                '</tpl>', {
                    compile: true,
                    disableFormats: true,
                    isSelected: function(value) {
                        var i = me.valueStore.findExact(me.valueField, value);
                        if (i >= 0) {
                            return me.selectionModel.isSelected(me.valueStore.getAt(i));
                        }
                        return false;
                    },
                    getItemLabel: function(values) {
                        return me.getTpl('labelTpl').apply(values);
                    }
                }
            ];
        }

        return this.getTpl('multiSelectItemTpl').apply(Ext.Array.pluck(this.valueStore.getRange(), 'data'));
    },

    /**
     * Update the labelled items rendering
     * @private
     */
    applyMultiselectItemMarkup: function() {
        var me = this,
            itemList = me.itemList,
            item;

        if (itemList) {
            while ((item = me.inputElCt.prev()) != null) {
                item.remove();
            }
            me.inputElCt.insertHtml('beforeBegin', me.getMultiSelectItemMarkup());
        }

        Ext.Function.defer(function() {
            if (me.picker && me.isExpanded) {
                me.alignPicker();
            }
            if (me.hasFocus && me.inputElCt && me.listWrapper) {
                me.inputElCt.scrollIntoView(me.listWrapper);
            }
        }, 15);
    },

    /**
     * Returns the record from valueStore for the labelled item node
     */
    getRecordByListItemNode: function(itemEl) {
        var me = this,
            itemIdx = 0,
            searchEl = me.itemList.dom.firstChild;

        while (searchEl && searchEl.nextSibling) {
            if (searchEl == itemEl) {
                break;
            }
            itemIdx++;
            searchEl = searchEl.nextSibling;
        }
        itemIdx = (searchEl == itemEl) ? itemIdx : false;

        if (itemIdx === false) {
            return false;
        }

        return me.valueStore.getAt(itemIdx);
    },

    /**
     * Toggle of labelled item selection by node reference
     */
    toggleSelectionByListItemNode: function(itemEl, keepExisting) {
        var me = this,
            rec = me.getRecordByListItemNode(itemEl),
            selModel = me.selectionModel;

        if (rec) {
            if (selModel.isSelected(rec)) {
                if (selModel.isFocused(rec)) {
                    selModel.setLastFocused(null);
                }
                selModel.deselect(rec);
            } else {
                selModel.select(rec, keepExisting);
            }
        }
    },

    /**
     * Removal of labelled item by node reference
     */
    removeByListItemNode: function(itemEl) {
        var me = this,
            rec = me.getRecordByListItemNode(itemEl);

        if (rec) {
            me.valueStore.remove(rec);
            me.setValue(me.valueStore.getRange());
        }
    },

    /**
     * @inheritdoc
     * Intercept calls to getRawValue to pretend there is no inputEl for rawValue handling,
     * so that we can use inputEl for user input of just the current value.
     */
    getRawValue: function() {
        var me = this,
            inputEl = me.inputEl,
            result;
        me.inputEl = false;
        result = me.callParent(arguments);
        me.inputEl = inputEl;
        return result;
    },

    /**
     * @inheritdoc
     * Intercept calls to setRawValue to pretend there is no inputEl for rawValue handling,
     * so that we can use inputEl for user input of just the current value.
     */
    setRawValue: function(value) {
        var me = this,
            inputEl = me.inputEl,
            result;

        me.inputEl = false;
        result = me.callParent([value]);
        me.inputEl = inputEl;

        return result;
    },

    /**
     * Adds a value or values to the current value of the field
     * @param {Mixed} value The value or values to add to the current value, see {@link #setValue}
     */
    addValue: function(value) {
        var me = this;
        if (value) {
            me.setValue(Ext.Array.merge(me.value, Ext.Array.from(value)));
        }
    },

    /**
     * Removes a value or values from the current value of the field
     * @param {Mixed} value The value or values to remove from the current value, see {@link #setValue}
     */
    removeValue: function(value) {
        var me = this;

        if (value) {
            me.setValue(Ext.Array.difference(me.value, Ext.Array.from(value)));
        }
    },

    /**
     * Sets the specified value(s) into the field. The following value formats are recognised:
     *
     * - Single Values
     *
     *     - A string associated to this field's configured {@link #valueField}
     *     - A record containing at least this field's configured {@link #valueField} and {@link #displayField}
     *
     * - Multiple Values
     *
     *     - If {@link #multiSelect} is `true`, a string containing multiple strings as
     *       specified in the Single Values section above, concatenated in to one string
     *       with each entry separated by this field's configured {@link #delimiter}
     *     - An array of strings as specified in the Single Values section above
     *     - An array of records as specified in the Single Values section above
     *
     * In any of the string formats above, the following occurs if an associated record cannot be found:
     *
     * 1. If {@link #forceSelection} is `false`, a new record of the {@link #store}'s configured model type
     *    will be created using the given value as the {@link #displayField} and {@link #valueField}.
     *    This record will be added to the current value, but it will **not** be added to the store.
     * 2. If {@link #forceSelection} is `true` and {@link #queryMode} is `remote`, the list of unknown
     *    values will be submitted as a call to the {@link #store}'s load as a parameter named by
     *    the {@link #valueParam} with values separated by the configured {@link #delimiter}.
     *    ** This process will cause setValue to asynchronously process. ** This will only be attempted
     *    once. Any unknown values that the server does not return records for will be removed.
     * 3. Otherwise, unknown values will be removed.
     *
     * @param {Mixed} value The value(s) to be set, see method documentation for details
     * @return {Ext.form.field.Field/Boolean} this, or `false` if asynchronously querying for unknown values
     */
    setValue: function(value, doSelect, skipLoad) {
        var me = this,
            valueStore = me.valueStore,
            valueField = me.valueField,
            record, len, i, valueRecord, h,
            unknownValues = [];

        if (Ext.isEmpty(value)) {
            value = null;
        }
        if (Ext.isString(value) && me.multiSelect) {
            value = value.split(me.delimiter);
        }
        value = Ext.Array.from(value, true);

        for (i = 0, len = value.length; i < len; i++) {
            record = value[i];
            if (!record || !record.isModel) {
                valueRecord = valueStore.findExact(valueField, record);
                if (valueRecord >= 0) {
                    value[i] = valueStore.getAt(valueRecord);
                } else {
                    valueRecord = me.findRecord(valueField, record);
                    if (!valueRecord) {
                        if (me.forceSelection) {
                            unknownValues.push(record);
                        } else {
                            valueRecord = {};
                            valueRecord[me.valueField] = record;
                            valueRecord[me.displayField] = record;
                            valueRecord = new me.valueStore.model(valueRecord);
                        }
                    }
                    if (valueRecord) {
                        value[i] = valueRecord;
                    }
                }
            }
        }

        if ((skipLoad !== true) && (unknownValues.length > 0) && (me.queryMode === 'remote')) {
            var params = {};
            params[me.valueParam || me.valueField] = unknownValues.join(me.delimiter);
            me.store.load({
                params: params,
                callback: function() {
                    if (me.itemList) {
                        me.itemList.unmask();
                    }
                    me.setValue(value, doSelect, true);
                    me.autoSize();
                    me.lastQuery = false;
                }
            });
            return false;
        }

        // For single-select boxes, use the last good (formal record) value if possible
        if (!me.multiSelect && (value.length > 0)) {
            for (i = value.length - 1; i >= 0; i--) {
                if (value[i].isModel) {
                    value = value[i];
                    break;
                }
            }
            if (Ext.isArray(value)) {
                value = value[value.length - 1];
            }
        }

        // return me.callParent([value, doSelect]);
        return me.reallySetValue(value, doSelect);
    },

    //Override setValue extjs Ext.form.field.ComboBox. RBA-12389
    reallySetValue: function(value, doSelect) {
        var me = this,
            valueNotFoundText = me.valueNotFoundText,
            inputEl = me.inputEl,
            i, len, record, dataObj,
            matchedRecords = [],
            displayTplData = [],
            processedValue = [];
        if (me.store.loading ) {
            me.value = value;
            // Called while the Store is loading. Ensure it is processed by the onLoad method.
            me.setHiddenValue(value);
            me.setRawValue('');
            return me;
        }
        // This method processes multi-values, so ensure value is an array.
        value = Ext.Array.from(value);
        // Loop through values, matching each from the Store, and collecting matched records
        for (i = 0 , len = value.length; i < len; i++) {
            record = value[i];
            if (!record || !record.isModel) {
                record = me.findRecordByValue(record);
            }
            // record found, select it.
            if (record) {
                matchedRecords.push(record);
                displayTplData.push(record.data);
                processedValue.push(record.get(me.valueField));
            } else // record was not found, this could happen because
            // store is not loaded or they set a value not in the store
            {
                // If we are allowing insertion of values not represented in the Store, then push the value and
                // create a fake record data object to push as a display value for use by the displayTpl
                if (!me.forceSelection) {
                    processedValue.push(value[i]);
                    dataObj = {};
                    dataObj[me.displayField] = value[i];
                    displayTplData.push(dataObj);
                }
                // TODO: Add config to create new records on selection of a value that has no match in the Store
                // Else, if valueNotFoundText is defined, display it, otherwise display nothing for this value
                else if (Ext.isDefined(valueNotFoundText)) {
                    displayTplData.push(valueNotFoundText);
                }
            }
        }
        // Set the value of this field. If we are multiselecting, then that is an array.
        me.setHiddenValue(processedValue);
        me.value = me.multiSelect ? processedValue : processedValue[0];
        if (!Ext.isDefined(me.value)) {
            me.value = null;
        }
        me.displayTplData = displayTplData;
        //store for getDisplayValue method
        me.lastSelection = me.valueModels = matchedRecords;
        if (inputEl && me.emptyText && !Ext.isEmpty(value)) {
            inputEl.removeCls(me.emptyCls);
        }
        // Calculate raw value from the collection of Model data
        me.setRawValue(me.getDisplayValue());
        me.checkChange();
        if (doSelect !== false) {
            me.syncSelection();
        }
        me.applyEmptyText();
        return me;
    },
    /**
     * Returns the records for the field's current value
     * @return {Array} The records for the field's current value
     */
    getValueRecords: function() {
        return this.valueStore.getRange();
    },

    /**
     * @inheritdoc
     * Overridden to optionally allow for submitting the field as a json encoded array.
     */
    getSubmitData: function() {
        var me = this,
            val = me.callParent(arguments);

        if (me.multiSelect && me.encodeSubmitValue && val && val[me.name]) {
            val[me.name] = Ext.encode(val[me.name]);
        }

        return val;
    },

    /**
     * Overridden to clear the input field if we are auto-setting a value as we blur.
     * @protected
     */
    mimicBlur: function() {
        var me = this;

        if (me.selectOnTab && me.picker && me.picker.highlightedItem) {
            me.inputEl.dom.value = '';
        }

        me.callParent(arguments);
    },

    /**
     * Overridden to handle partial-input selections more directly
     */
    assertValue: function() {
        var me = this,
            rawValue = me.inputEl.dom.value,
            rec = !Ext.isEmpty(rawValue) ? me.findRecordByDisplay(rawValue) : false,
            value = false;

        if (!rec && !me.forceSelection && me.createNewOnBlur && !Ext.isEmpty(rawValue)) {
            value = rawValue;
        } else if (rec) {
            value = rec;
        }

        if (value) {
            me.addValue(value);
        }

        me.inputEl.dom.value = '';

        me.collapse();
    },

    /**
     * Expand record values for evaluating change and fire change events for UI to respond to
     */
    checkChange: function() {
        if (!this.suspendCheckChange && !this.isDestroyed) {
            var me = this,
                valueStore = me.valueStore,
                lastValue = me.lastValue || '',
                valueField = me.valueField,
                newValue = Ext.Array.map(Ext.Array.from(me.value), function(val) {
                    if (val.isModel) {
                        return val.get(valueField);
                    }
                    return val;
                }, this).join(this.delimiter),
                isEqual = me.isEqual(newValue, lastValue);

            if (!isEqual || ((newValue.length > 0 && valueStore.getCount() < newValue.length))) {
                valueStore.suspendEvents();
                valueStore.removeAll();
                if (Ext.isArray(me.valueModels)) {
                    valueStore.add(me.valueModels);
                }
                valueStore.resumeEvents();
                valueStore.fireEvent('datachanged', valueStore);

                if (!isEqual) {
                    me.lastValue = newValue;
                    me.fireEvent('change', me, newValue, lastValue);
                    me.onChange(newValue, lastValue);
                }
            }
        }
    },

    /**
     * Overridden to be more accepting of varied value types
     */
    isEqual: function(v1, v2) {
        var fromArray = Ext.Array.from,
            valueField = this.valueField,
            i, len, t1, t2;

        v1 = fromArray(v1);
        v2 = fromArray(v2);
        len = v1.length;

        if (len !== v2.length) {
            return false;
        }

        for (i = 0; i < len; i++) {
            t1 = v1[i].isModel ? v1[i].get(valueField) : v1[i];
            t2 = v2[i].isModel ? v2[i].get(valueField) : v2[i];
            if (t1 !== t2) {
                return false;
            }
        }

        return true;
    },

    /**
     * Overridden to use value (selection) instead of raw value and to avoid the use of placeholder
     */
    applyEmptyText: function() {
        var me = this,
            emptyText = me.emptyText,
            inputEl, isEmpty;

        if (me.rendered && emptyText) {
            isEmpty = Ext.isEmpty(me.value) && !me.hasFocus;
            inputEl = me.inputEl;
            if (isEmpty) {
                inputEl.dom.value = '';
                me.emptyEl.update(emptyText);
                me.emptyEl.addCls(me.emptyCls);
                me.emptyEl.removeCls(me.emptyInputCls);
                me.listWrapper.addCls(me.emptyCls);
                me.inputEl.addCls(me.emptyInputCls);
            } else {
                me.emptyEl.addCls(me.emptyInputCls);
                me.emptyEl.removeCls(me.emptyCls);
                me.listWrapper.removeCls(me.emptyCls);
                me.inputEl.removeCls(me.emptyInputCls);
            }
            me.autoSize();
        }
    },

    /**
     * Overridden to use inputEl instead of raw value and to avoid the use of placeholder
     */
    preFocus: function() {
        var me = this,
            inputEl = me.inputEl,
            emptyText = me.emptyText,
            isEmpty = (inputEl.dom.value == '');

        me.emptyEl.addCls(me.emptyInputCls);
        me.emptyEl.removeCls(me.emptyCls);
        me.listWrapper.removeCls(me.emptyCls);
        me.inputEl.removeCls(me.emptyInputCls);

        if (me.selectOnFocus || isEmpty) {
            inputEl.dom.select();
        }
    },

    /**
     * Intercept calls to onFocus to add focusCls, because the base field
     * classes assume this should be applied to inputEl
     */
    onFocus: function() {
        var me = this,
            focusCls = me.focusCls,
            itemList = me.itemList;

        if (focusCls && itemList) {
            itemList.addCls(focusCls);
        }

        me.callParent(arguments);
    },

    /**
     * Intercept calls to onBlur to remove focusCls, because the base field
     * classes assume this should be applied to inputEl
     */
    onBlur: function() {
        var me = this,
            focusCls = me.focusCls,
            itemList = me.itemList;

        if (focusCls && itemList) {
            itemList.removeCls(focusCls);
        }

        me.callParent(arguments);
    },

    /**
     * Intercept calls to renderActiveError to add invalidCls, because the base
     * field classes assume this should be applied to inputEl
     */
    renderActiveError: function() {
        var me = this,
            invalidCls = me.invalidCls,
            itemList = me.itemList,
            hasError = me.hasActiveError();

        if (invalidCls && itemList) {
            itemList[hasError ? 'addCls' : 'removeCls'](me.invalidCls + '-field');
        }

        me.callParent(arguments);
    },

    /**
     * Initiate auto-sizing for height based on {@link #grow}, if applicable.
     */
    autoSize: function() {
        var me = this,
            height;

        if (me.grow && me.rendered) {
            me.autoSizing = true;
            me.updateLayout();
        }

        return me;
    },

    /**
     * Track height change to fire {@link #event-autosize} event, when applicable.
     */
    afterComponentLayout: function() {
        var me = this,
            width;

        if (me.autoSizing) {
            height = me.getHeight();
            if (height !== me.lastInputHeight) {
                if (me.isExpanded) {
                    me.alignPicker();
                }
                me.fireEvent('autosize', me, height);
                me.lastInputHeight = height;
                delete me.autoSizing;
            }
        }
    }
});

/**
 * Ensures the input element takes up the maximum amount of remaining list width,
 * or the entirety of the list width if too little space remains. In this case,
 * the list height will be automatically increased to accomodate the new line. This
 * growth will not occur if {@link Ext.ux.form.field.BoxSelect#multiSelect} or
 * {@link Ext.ux.form.field.BoxSelect#grow} is false.
 */
Ext.define('Ext.ux.layout.component.field.BoxSelectField', {
    /* Begin Definitions */
    alias: ['layout.boxselectfield'],
    extend: 'Ext.layout.component.field.Trigger',

    /* End Definitions */

    type: 'boxselectfield',

    /*For proper calculations we need our field to be sized.*/
    waitForOuterWidthInDom: true,

    beginLayout: function(ownerContext) {
        var me = this,
            owner = me.owner;

        me.callParent(arguments);

        ownerContext.inputElCtContext = ownerContext.getEl('inputElCt');
        owner.inputElCt.setStyle('width', '');

        me.skipInputGrowth = !owner.grow || !owner.multiSelect;
    },

    beginLayoutFixed: function(ownerContext, width, suffix) {
        var me = this,
            owner = ownerContext.target;

        owner.triggerEl.setStyle('height', '24px');

        me.callParent(arguments);

        if (ownerContext.heightModel.fixed && ownerContext.lastBox) {
            owner.listWrapper.setStyle('height', ownerContext.lastBox.height + 'px');
            owner.itemList.setStyle('height', '100%');
        }
        /*No inputElCt calculations here!*/
    },

    /*Calculate and cache value of input container.*/
    publishInnerWidth: function(ownerContext) {
        var me = this,
            owner = me.owner,
            width = owner.itemList.getWidth(true) - 10,
            lastEntry = owner.inputElCt.prev(null, true);

        if (lastEntry && !owner.stacked) {
            lastEntry = Ext.fly(lastEntry);
            width = width - lastEntry.getOffsetsTo(lastEntry.up(''))[0] - lastEntry.getWidth();
        }

        if (!me.skipInputGrowth && (width < 35)) {
            width = width - 10;
        } else if (width < 1) {
            width = 1;
        }
        ownerContext.inputElCtContext.setWidth(width);
    }
});

glu.regAdapter('comboboxselect', {
    extend: 'combo'
});

glu.regAdapter('boxselect', {
    extend: 'combo'
});
Ext.namespace('RS.social.ComponentMap')
Ext.applyIf(RS.social.ComponentMap, {
	actiontask: 'RS.actiontask.ActionTask',
	decisiontree: 'RS.wiki.Main',
	document: 'RS.wiki.Main',
	forum: 'RS.social.Main',
	namespace: 'RS.wiki.WikiAdmin',
	process: 'RS.social.Main',
	rss: 'RS.social.Main',
	runbook: 'RS.wiki.Main',
	team: 'RS.social.Main',
	user: 'RS.user.User',
	worksheet: 'RS.worksheet.Worksheet'

})
Ext.define('RS.social.PostEditor', {
	extend: 'RS.common.HtmlLintEditor',
	alias: 'widget.posteditor',
	displayLinkAndUpload: true,
	searchString: '',
	cursorIndex: 0,
	hideLabel: true,
	name: 'content',
	enableAlignments: false,
	enableFont: false,
	enableFontSize: false,
	enableFormat: false,
	enableLists: false,
	enableSourceEdit: false,
	enableColors: false,
	enableLinks: false,
	enableUpload: false,

	getDocMarkup: function() {
		//WARNING: This is a copy from extjs to override the style sheet loaded in the htmleditor font.
		var me = this,
			h = me.iframeEl.getHeight() - me.iframePad * 2,
			oldIE = Ext.isIE8m;

		// - IE9+ require a strict doctype otherwise text outside visible area can't be selected.
		// - Opera inserts <P> tags on Return key, so P margins must be removed to avoid double line-height.
		// - On browsers other than IE, the font is not inherited by the IFRAME so it must be specified.
		return Ext.String.format((oldIE ? '' : '<!DOCTYPE html>') + '<html><head><style type="text/css">' + (Ext.isOpera ? 'p{margin:0}' : '') + 'body{border:0;margin:0;padding:{0}px;direction:' + (me.rtl ? 'rtl;' : 'ltr;') + (oldIE ? Ext.emptyString : 'min-') + 'height:{1}px;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:text;background-color:white;' + (Ext.isIE ? '' : 'font-size:12px;font-family:{2}') + '}</style><link rel="stylesheet" type="text/css" href="/resolve/client/css/client-all.css"/></head><body></body></html>', me.iframePad, h, me.defaultFont);
	},

	getSelection: function() {
		var me = this,
			val = me.getValue(),
			end = val.indexOf('<span class="suggest"></span>'),
			temp = val.substring(0, val.indexOf('<span class="suggest"></span>')),
			start = temp.lastIndexOf(me.menu.magicKey),
			selection = val.substring(start, end);
		return selection;
	},

	click: function(e) {
		this.closeMenu();
	},

	closeMenu: function() {
		if (this.menu) {
			this.menu.close();
			this.focus();
			this.menu = null;
			this.store = null;
		}
	},

	removeText: function(el, text, magicKey) {
		var nodes = this.getTextNodesIn(el),
			textString = '',
			start = -1,
			end = -1,
			smallText = text.toLowerCase().substring(1),
			node;

		for (var i = 0; i < nodes.length; i++) {
			node = nodes[i];
			textString += (node.textContent || node.nodeValue || '');

			if (node.mark) {
				start = textString.lastIndexOf(magicKey);
				end = textString.length;
			}
		}

		// console.info(nodes)
		if (start > -1) {
			this.setSelectionRange(el, start, end);
			this.execCmd('delete');

			if (Ext.isGecko) {
				this.execCmd('delete');
			}
		}
	},

	getTextNodesIn: function(node) {
		var textNodes = [];

		if (node.nodeType == 3) {
			textNodes.push(node);
		} else {
			var children = node.childNodes;

			for (var i = 0; i < children.length; i++) {
				var current = children[i];

				if (Ext.fly(current).hasCls('suggest')) {
					textNodes.push({
						mark: 'fromHere'
					});
				}

				textNodes.push.apply(textNodes, this.getTextNodesIn(current));
			}
		}

		return textNodes;
	},

	setSelectionRange: function(el, start, end) {
		var doc = this.getDoc() || document;
		var win = this.getWin();
		var hasCreateFn = typeof doc.createRange === "function";
		var hasGetSelectionFn = win && typeof win.getSelection === "function";

		if (hasCreateFn && hasGetSelectionFn) {
			var range = doc.createRange();
			range.selectNodeContents(el);
			var textNodes = this.getTextNodesIn(el);
			var foundStart = false;
			var charCount = 0,
				endCharCount;

			for (var i = 0; i < textNodes.length; i++) {
				var textNode = textNodes[i++];

				if (textNode.mark) {
					continue;
				}

				endCharCount = charCount + textNode.length;
				range.selectNodeContents(textNode);
				var hasMoreChars = start >= charCount && start <= endCharCount;

				if (!foundStart && hasMoreChars) {
					range.setStart(textNode, start - charCount);
					foundStart = true;
				}

				if (foundStart && end <= endCharCount) {
					range.setEnd(textNode, end - charCount);
					break;
				}

				charCount = endCharCount;
			}

			var sel = this.getWin().getSelection();
			sel.removeAllRanges();
			sel.addRange(range);
		} else if (document.selection && document.body.createTextRange) {
			var textRange = document.body.createTextRange();
			textRange.moveToElementText(el);
			textRange.collapse(true);
			textRange.moveEnd("character", end);
			textRange.moveStart("character", start);
			textRange.select();
		}
	}
});
/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.social').locale = {

	success: 'Success',
	error: 'Error',
	failure: 'An error has occurred on the server, please contact your administrator for assistance',

	windowTitle: 'Resolve Social',

	postYourMessage: 'Post your message…',
	postYourMessageDisplay: 'Post your message to {0} …',
	addAComment: 'Add a comment…',
	messageSent: 'Message Sent',

	//Actions
	post: 'Post',
	newPost: 'New Post',
	comment: 'Comment',
	cancel: 'Cancel',
	cancelMini: 'Cancel',
	switchToPostDisplay: 'Back',

	notificationSettingChanged: 'Notification setting changed',
	applyDigest: 'Send a daily email digest',
	unapplyDigest: 'Stop sending daily email digest',
	applyEmail: 'Forward messages to email',
	unapplyEmail: 'Stop forwarding messages to email',

	postMessageTooltip: 'Post your message…',
	filterTooltip: 'Filter posts',
	refreshPostsTooltip: 'Refresh Posts',
	autoRefreshPosts: 'Auto Refresh',
	pollInterval: 'Interval',
	followStreamTooltip: 'Click to follow this stream',

	nextPage: 'Next Page',
	previousPage: 'Previous Page',

	notAuthenticated: 'You are no longer authenticated.  Please login from the main window.',

	//Fields
	to: 'To',
	invalidTo: 'Please provide a stream to post to',
	invalidToSystem: 'You cannot post to other streams when you are posting to the system stream.  Please remove system or remove the other streams.',
	subject: 'Subject',
	content: 'Content',
	author: 'Author',

	//Filter
	filter: 'Filter your posts…',

	//Grid columns
	name: 'Name',
	type: 'Type',

	// Component Types
	defaultText: 'Default',
	user: 'User',
	document: 'Document',

	//User Default hard coded streams
	all: 'All',
	inbox: 'Inbox',
	starred: 'Starred',
	blog: 'Blog',
	sent: 'Sent',
	notification: 'System',
	activity: 'Activity',

	//Streams
	streams: 'Streams',
	showAll: 'Show all streams that you are following',
	showActive: 'Show only the active streams that you are following',
	refreshStreams: 'Refresh Streams',

	//Stream Group Actions
	followStream: 'Follow a stream',

	//Stream Actions
	edit: 'Edit',
	view: 'View',
	settings: 'Settings',
	configure: 'Configure',
	markAllRead: 'Mark all as read',
	markedStreamRead: 'Marked all posts as read',
	markAllPostsReadTooltip: 'Mark all as read',
	markAllPostsRead: 'All',
	markPostsOlderThanOneDayRead: 'Older than 1 day',
	markPostsOlderThanOneWeekRead: 'Older than 1 week',
	filterMenuHeader: 'Filter',
	markAsRead: 'Mark As Read',
	unreadOnly: 'New Only',
	oneDayOld: '1 day old',
	oneWeekOld: '1 week old',
	unpin: 'Unpin',
	pin: 'Pin',
	lock: 'Lock',
	unlock: 'Unlock',
	unfollow: 'Unfollow',
	follow: 'Follow',
	unfollow: 'Unfollow',
	popout: 'Popout',
	followUser: 'Follow User',
	unfollowUser: 'Unfollow User',

	//Post Actions
	share: 'Share',
	like: 'Like',
	move: 'Move',
	deletePost: 'Delete',
	deleteComment: 'Delete',
	read: 'Mark Read',
	unread: 'Mark Unread',

	//Time
	seconds: 'seconds',
	minute: 'minute',
	minutes: 'minutes',
	hour: 'hour',
	hours: 'hours',
	yesterday: 'Yesterday',
	days: 'days',
	ago: 'ago',

	//Post Editor
	createLink: 'Create Link',
	attach: 'Attach Files',

	//Notifications
	forward: 'Forward to Email',
	digest: 'Send Digest Email',
	configureNotifications: 'Configure System Messages',

	//Unknown Component Navigation
	unknownComponentTitle: 'Unknown Component',
	unknownComponentMessage: 'The component type "{0}" is not registered with the Social Component Map',

	number: 'Number',
	reference: 'Reference',

	StreamPicker: {
		addressToDialogWindowTitle: 'Address your post',
		followDialogWindowTitle: 'Follow a stream',
		addFollowersWindowTitle: 'Add Followers',
		type: 'Filter By…',
		searchQuery: 'Search…',
		select: 'Select',
		follow: 'Follow',
		addFollowers: 'Add Followers',
		createComponent: 'Create',

		searchWorksheet: 'Search worksheets...',

		showMyWorksheets: 'Click to show my worksheets',
		showAllWorksheets: 'Click to show all worksheets'
	},

	CreateLink: {
		createLinkTitle: 'Create Link',
		createLink: 'Create Link',
		display: 'Text to display',
		linkTo: 'Link to',

		url: 'URL',
		urlLabel: 'To what url should this link go?',
		docLabel: 'To what document should this link go?',
		actionTaskLabel: 'To what action task should this link go?',
		worksheetLabel: 'To what worksheet should this link go?',
		forumLabel: 'To what forum should this link go?',
		rssLabel: 'To what rss should this link go?',

		searchQuery: 'Search…'
	},

	//Common commponent types
	component: 'Component',
	doc: 'Document',
	actionTask: 'Action Task',
	forum: 'Forum',
	rss: 'RSS',
	process: 'Process',
	team: 'Team',
	actiontask: 'Action Task',
	runbook: 'Runbook',
	worksheet: 'Worksheet',
	decisiontree: 'Decision Tree',
	decisiontrees: 'Decision Tree',
	namespace: 'Namespace',
	system: 'System',

	AttachFile: {
		attachFileWindowTitle: 'Attach File',
		attach: 'Attach'
	},

	Likes: {
		peopleWhoLikeThis: 'People who like this',
		you: 'You'
	},

	Post: {
		expandComments: 'Show {0} comments',
		expandComment: 'Show 1 comment',
		collapseComments: 'Hide {0} comments',
		collapseComment: 'Hide 1 comment',

		confirmPostDeleteTitle: 'Confirm Post Deletion',
		confirmPostDeleteBody: 'Are you sure you want to delete this post?',
		confirmCommentDeleteTitle: 'Confirm Comment Deletion',
		confirmCommentDeleteBody: 'Are you sure you want to delete this comment?',
		//postDeleted: 'Post deleted, <a onclick="Ext.Ajax.request({url: \'/resolve/service/social/undoDelete\', params: {id: \'{0}\'}});Ext.get(\'msg-div\').select(\'div\').fadeOut({duration: 200,remove: true});" style="cursor:pointer;cursor:hand;">click to undo</a>'
		postDeleted: 'Post deleted',
		commentDeleted: 'Comment deleted',

		likeTooltip: 'Click to like',
		unlikeTooltip: 'Click to un-like',
		starredTooltip: 'Click to star',
		unstarredTooltip: 'Click to un-star',
		readTooltip: 'Click to mark read',
		unreadTooltip: 'Click to mark un-read',

		answered: 'Answered',
		unanswered: 'Unanswered',
		answer: 'Answer',
		isAnswer: 'Is Answer?',
		response: 'response',
		responses: 'responses',

		target: 'Target:',
		targets: 'Targets:',

		postFrom: 'Post from:'
	},

	MovePost: {
		movePost: 'Move Post',
		invalidTo: 'Please provide a stream to move the post to'
	},

	Component: {
		email: 'Email',
		phone: 'Phone',
		mobile: 'Mobile',
		description: 'Description'
	},

	Followers: {
		followers: 'Followers',
		close: 'Close',
		addMoreFollowers: 'Add Followers',
		noFollowers: 'No followers at this time',
		you: 'You',
		remove: 'Remove'
	},

	ConfigureNotifications: {
		configureNotificationsTitle: 'Configure System Messages',
		save: 'Save',
		cancel: 'Cancel',
		notficationsSaved: 'System Messages preferences saved',
		selectAllProcess: 'Select All',
		PROCESS_CREATE: 'Create',
		PROCESS_UPDATE: 'Update',
		PROCESS_PURGED: 'Purged',
		PROCESS_RUNBOOK_ADDED: 'Runbook added',
		PROCESS_RUNBOOK_REMOVED: 'Runbook removed',
		PROCESS_DOCUMENT_ADDED: 'Document added',
		PROCESS_DOCUMENT_REMOVED: 'Document removed',
		PROCESS_ACTIONTASK_ADDED: 'ActionTask added',
		PROCESS_ACTIONTASK_REMOVED: 'ActionTask removed',
		PROCESS_RSS_ADDED: 'Rss added',
		PROCESS_RSS_REMOVED: 'Rss removed',
		PROCESS_FORUM_ADDED: 'Forum added',
		PROCESS_FORUM_REMOVED: 'Forum removed',
		PROCESS_TEAM_ADDED: 'Team added',
		PROCESS_TEAM_REMOVED: 'Team removed',
		PROCESS_USER_ADDED: 'User added',
		PROCESS_USER_REMOVED: 'User removed',
		PROCESS_DIGEST_EMAIL: 'Digest Email',
		PROCESS_FORWARD_EMAIL: 'Forward Email',
		selectAllTeam: 'Select All',
		TEAM_CREATE: 'Create',
		TEAM_UPDATE: 'Update',
		TEAM_PURGED: 'Purged',
		TEAM_ADDED_TO_PROCESS: 'Added to Process',
		TEAM_REMOVED_FROM_PROCESS: 'Removed from Process',
		TEAM_ADDED_TO_TEAM: 'Added to Team',
		TEAM_REMOVED_FROM_TEAM: 'Removed from Team',
		TEAM_USER_ADDED: 'User added',
		TEAM_USER_REMOVED: 'User removed',
		TEAM_DIGEST_EMAIL: 'Digest Email',
		TEAM_FORWARD_EMAIL: 'Forward Email',
		selectAllForum: 'Select All',
		FORUM_CREATE: 'Create',
		FORUM_UPDATE: 'Update',
		FORUM_PURGED: 'Purged',
		FORUM_ADDED_TO_PROCESS: 'Added to Process',
		FORUM_REMOVED_FROM_PROCESS: 'Removed from Process',
		FORUM_USER_ADDED: 'User added',
		FORUM_USER_REMOVED: 'User removed',
		FORUM_DIGEST_EMAIL: 'Digest Email',
		FORUM_FORWARD_EMAIL: 'Forward Email',
		selectAllUser: 'Select All',
		USER_ADDED_TO_PROCESS: 'Added to Process',
		USER_ADDED_TO_TEAM: 'Added to Team',
		USER_FOLLOW_ME: 'User following me',
		USER_REMOVED_FROM_PROCESS: 'Removed from Process',
		USER_REMOVED_FROM_TEAM: 'Removed from Team',
		USER_UNFOLLOW_ME: 'User unfollow me',
		USER_ADDED_TO_FORUM: 'Added to Forum',
		USER_REMOVED_FROM_FORUM: 'Removed from Forum',
		USER_PROFILE_CHANGE: 'User profile change',
		USER_DIGEST_EMAIL: 'Digest Email',
		USER_FORWARD_EMAIL: 'Inbox Forward Email',
		selectAllActionTask: 'Select All',
		ACTIONTASK_CREATE: 'Create',
		ACTIONTASK_UPDATE: 'Update',
		ACTIONTASK_PURGED: 'Purged',
		ACTIONTASK_ADDED_TO_PROCESS: 'Added to Process',
		ACTIONTASK_REMOVED_FROM_PROCESS: 'Removed from Process',
		ACTIONTASK_DIGEST_EMAIL: 'Digest Email',
		ACTIONTASK_FORWARD_EMAIL: 'Forward Email',
		selectAllRunbook: 'Select All',
		RUNBOOK_UPDATE: 'Update',
		RUNBOOK_ADDED_TO_PROCESS: 'Added to Process',
		RUNBOOK_REMOVED_FROM_PROCESS: 'Removed from Process',
		RUNBOOK_COMMIT: 'Commit',
		RUNBOOK_DIGEST_EMAIL: 'Digest Email',
		RUNBOOK_FORWARD_EMAIL: 'Forward Email',
		selectAllDocument: 'Select All',
		DOCUMENT_CREATE: 'Create',
		DOCUMENT_UPDATE: 'Update',
		DOCUMENT_DELETE: 'Delete',
		DOCUMENT_PURGED: 'Purged',
		DOCUMENT_ADDED_TO_PROCESS: 'Added to Process',
		DOCUMENT_REMOVED_FROM_PROCESS: 'Removed from Process',
		DOCUMENT_COMMIT: 'Commit',
		DOCUMENT_DIGEST_EMAIL: 'Digest Email',
		DOCUMENT_FORWARD_EMAIL: 'Forward Email',
		selectAllRSS: 'Select All',
		RSS_CREATE: 'Create',
		RSS_UPDATE: 'Update',
		RSS_PURGED: 'Purged',
		RSS_ADDED_TO_PROCESS: 'Added to Process',
		RSS_REMOVED_FROM_PROCESS: 'Removed from Process',
		RSS_DIGEST_EMAIL: 'Digest Email',
		RSS_FORWARD_EMAIL: 'Forward Email',

		PROCESS: 'Process',
		TEAM: 'Team',
		FORUM: 'Forum',
		USER: 'User',
		ACTIONTASK: 'Actiontask',
		RUNBOOK: 'Runbook',
		DOCUMENT: 'Document',
		DECISIONTREES: 'Decision Tree',
		RSS: 'RSS'
	}
}
