/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.jsb3builder;

import java.util.Set;

public class JSB3Data
{
    private String className;
    private String extend;
    private String jsFileName;
    private String path;
    private Set<String> requires;
    private Set<String> uses;
    
    public JSB3Data(String className,
                    String extend,
                    String jsFileName, 
                    String path, 
                    Set<String> requires,
                    Set<String> uses)
    {
        this.className = className;
        this.extend = extend;
        this.jsFileName = jsFileName;
        this.path = path.replace('\\', '/');
        this.requires = requires;
        this.uses = uses;
    }
    
    public String buildJSString()
    {
        StringBuffer sf = new StringBuffer();
        
//        if((className != null && !className.equals("")) 
//             && (extend != null && !extend.equals(""))
//             && (jsFileName != null && !jsFileName.equals(""))
//             && (path != null && !path.equals("")))
//        {
            sf.append("{" + "\n");
            sf.append("\"clsName\": \"" + className.trim() + "\"" + ",\n");
            sf.append("\"extend\": \"" + extend.trim() + "\"" + ",\n");
            sf.append("\"name\": \"" + jsFileName.trim() + "\"" + ",\n");
            sf.append("\"path\": \"" + path.trim() + "\"" +"\n");
            sf.append("}");
//        }
        
        return sf.toString();
    }
    
    public Set<String> getRequires()
    {
        return requires;
    }
    
    public Set<String> getUses()
    {
        return uses;
    }
    
    public String getClassName()
    {
        return className;
    }
    
    public String getExtend()
    {
        return this.extend;
    }
}
