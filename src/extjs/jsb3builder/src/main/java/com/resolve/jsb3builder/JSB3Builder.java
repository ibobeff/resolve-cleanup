/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.jsb3builder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JSB3Builder
{
    public static boolean osISWindows = System.getProperty("os.name").startsWith("Windows");
    public static String moduleName = "all";
    public static String fileName = "rssocial.jsb3";
    public static String rootdir = (osISWindows) ? ("C:\\Project\\resolve\\src\\rssocial\\src\\social") : "/opt/resolve/tomcat/webapps/resolve/social";
    public static File directory = new File(".");
    public static String separator = (osISWindows) ? "\\" : "/";
    public static Pattern CLASSNAME_PATTERN = Pattern.compile("Ext.define\\s*[(]\\s*(['\"]\\w.+['\"])");
    public static Pattern EXTEND_PATTERN = Pattern.compile("extend\\s*:\\s*(['\"]\\w.+['\"])");
    // public static Pattern REQ_PATTERN =
    // Pattern.compile("[,\\s*requires\\s*:\\s*](\\[?\\s*['\"]\\w.+['\"]\\s*\\]?)");
    public static Pattern REQ_PATTERN = Pattern.compile("\\s*requires\\s*:\\s*(\\[?\\s*['\"]\\w.+['\"]\\s*\\]?)");

    public static int allFilesCount = 0;
    public static int processFilesCount = 0;
    public static int addFilesCount = 0;

    public static void main(String[] args)
    {
        try
        {
            if (args.length < 1)
            {
                System.out.println("");
                System.out.println("Usage: java -jar jsb3builder.jar -n <name> -f <rssocial.jsb3> -d <c:\\project\\resolve\\src\\rssocial\\src\\social");
                System.exit(0);
            }

            if (args[0] != null && !args[0].equals("") && args[1] != null && !args[1].equals(""))
            {
                moduleName = args[1];
            }

            if (args[2] != null && !args[2].equals("") && args[3] != null && !args[3].equals(""))
            {
                fileName = args[3];
            }

            if (args.length >= 5)
            {
                if (args[4] != null && !args[4].equals(""))
                {
                    if (args[5] != null && !args[5].equals(""))
                    {
                        rootdir = args[5];
                    }
                    else
                    {
                        rootdir = directory.getCanonicalPath();
                    }
                }
            }
            else
            {
                rootdir = directory.getCanonicalPath();
            }

            buildJSB3();

            System.out.println("===== the file " + fileName + " successful build");
        }
        catch (Throwable t)
        {
            t.printStackTrace();
        }
    }

    public static void buildJSB3()
    {
        File jsb3File = null;
        FileWriter fw = null;

        try
        {
            jsb3File = new File(fileName);

            if (jsb3File.exists())
            {
                jsb3File.delete();
            }
            else
            {
                jsb3File.createNewFile();
                System.out.println("File is created");
            }

            if (jsb3File != null)
            {
                String jsb3Content = buildJSB3Content();

                fw = new FileWriter(jsb3File);
                fw.write(jsb3Content);
            }
        }
        catch (Throwable t)
        {
            t.printStackTrace();
        }
        finally
        {
            if (fw != null)
            {
                try
                {
                    fw.flush();
                    fw.close();
                }
                catch (Exception exp)
                {
                }
            }
        }
    }

    public static String buildJSB3Content()
    {
        StringBuffer jsb3String = new StringBuffer();

        jsb3String.append(buildFirstPart());
        jsb3String.append("\n");

        // jsb3String.append("\\\\------ start pass the string from js --------");
        jsb3String.append("\n");
        jsb3String.append("\n");

        jsb3String.append(buildMiddlePart());

        jsb3String.append("\n");
        jsb3String.append("\n");
        // jsb3String.append("\\\\------ end pass the string from js --------");

        jsb3String.append("\n");
        jsb3String.append(buildLastPart());

        return jsb3String.toString();
    }

    public static String buildMiddlePart()
    {
        StringBuffer stringBuffer = new StringBuffer();

        List<File> allModelFiles = new ArrayList<File>();
        List<File> allStoreFiles = new ArrayList<File>();
        List<File> allViewFiles = new ArrayList<File>();
        List<File> allCtrlFiles = new ArrayList<File>();
        List<File> allOtherFiles = new ArrayList<File>();

        File dir = new File(rootdir);

        System.out.println("Including files:");
        getAllJSFiles(allModelFiles, allStoreFiles, allViewFiles, allCtrlFiles, allOtherFiles, dir);
        System.out.println("Included files count: " + allFilesCount);

        System.out.println("\n");
        System.out.println("Processing files: ");
        List<JSB3Data> modelDataList = buildDataList(allModelFiles);
        List<JSB3Data> storeDataList = buildDataList(allStoreFiles);
        List<JSB3Data> viewDataList = buildDataList(allViewFiles);
        List<JSB3Data> ctrlDataList = buildDataList(allCtrlFiles);
        List<JSB3Data> otherDataList = buildDataList(allOtherFiles);
        System.out.println("Processed files count: " + processFilesCount);

        System.out.println("\n");
        System.out.println("Adding files:");
        stringBuffer.append(buildOrderedList(modelDataList));
        if (!storeDataList.isEmpty())
        {
            if( stringBuffer.length() > 0 )stringBuffer.append(",\n");
            stringBuffer.append(buildOrderedList(storeDataList));
        }
        if (!viewDataList.isEmpty())
        {
            if( stringBuffer.length() > 0 )stringBuffer.append(",\n");
            stringBuffer.append(buildOrderedList(viewDataList));
        }
        if (!ctrlDataList.isEmpty())
        {
            if( stringBuffer.length() > 0 )stringBuffer.append(",\n");
            stringBuffer.append(buildOrderedList(ctrlDataList));
        }
        if (!otherDataList.isEmpty())
        {
            if( stringBuffer.length() > 0 )stringBuffer.append(",\n");
            stringBuffer.append(buildOrderedList(otherDataList));
        }
        System.out.println("Added files count: " + addFilesCount);

        return stringBuffer.toString();

    }

    public static String buildOrderedList(List<JSB3Data> modelDataList)
    {
        StringBuffer result = new StringBuffer();

        List<String> orderedList = new ArrayList<String>();

        for (JSB3Data data : modelDataList)
        {
            String className = data.getClassName();
            className = className.trim();

            if (className.equals("DocViewport"))
            {
                System.out.println();
            }

            Set<String> reqs = data.getRequires();
            if (reqs != null && !reqs.isEmpty())
            {
                buildAllRequiresList(reqs, orderedList, modelDataList);
            }

            Set<String> uses = data.getUses();
            if (uses != null && !uses.isEmpty())
            {
                buildAllUsesList(uses, orderedList, modelDataList);
            }

            String extend = data.getExtend();
            if (extend != null && !extend.isEmpty())
            {
                buildAllExtendList(extend, orderedList, modelDataList);
            }

            String jsString = data.buildJSString();
            if (!orderedList.contains(jsString))
            {
                orderedList.add(jsString);
            }
        }
        
        for (Iterator i = orderedList.iterator(); i.hasNext();)
        {
            String jsString = (String) i.next();

            //System.out.println(jsString);
            addFilesCount++;

            result.append(jsString);
            result.append("\n");
            if (i.hasNext())
            {
                result.append(",\n");
            }
        }

        return result.toString();
    }

    public static void buildAllExtendList(String extend, List<String> orderedList, List<JSB3Data> modelDataList)
    {
        if (extend != null && !extend.equals(""))
        {
            JSB3Data reqsData = findData(extend, modelDataList);

            if (reqsData != null)
            {
                String extendLocal = reqsData.getExtend();

                if (extendLocal != null && !extendLocal.equals(""))
                {
                    buildAllExtendList(extendLocal, orderedList, modelDataList);
                }

                String reqsString = reqsData.buildJSString();

                if (!orderedList.contains(reqsString))
                {
                    orderedList.add(reqsString);
                }
            }

        }
    }

    public static void buildAllRequiresList(Set<String> reqs, List<String> orderedList, List<JSB3Data> modelDataList)
    {
        for (String str : reqs)
        {
            if (!orderedList.contains(str))
            {
                JSB3Data reqsData = findData(str, modelDataList);

                if (reqsData != null)
                {
                    Set<String> reqsLocal = reqsData.getRequires();

                    if (reqsLocal != null && !reqsLocal.isEmpty())
                    {
                        buildAllRequiresList(reqsLocal, orderedList, modelDataList);
                    }

                    String reqsString = reqsData.buildJSString();

                    if (!orderedList.contains(reqsString))
                    {
                        orderedList.add(reqsString);
                    }
                }
            }
        }
    }

    public static void buildAllUsesList(Set<String> uses, List<String> orderedList, List<JSB3Data> modelDataList)
    {
        for (String str : uses)
        {
            if (!orderedList.contains(str))
            {
                JSB3Data usesData = findData(str, modelDataList);

                if (usesData != null)
                {
                    Set<String> reqsLocal = usesData.getUses();

                    if (reqsLocal != null && !reqsLocal.isEmpty())
                    {
                        buildAllUsesList(reqsLocal, orderedList, modelDataList);
                    }

                    String reqsString = usesData.buildJSString();

                    if (!orderedList.contains(reqsString))
                    {
                        orderedList.add(reqsString);
                    }
                }
            }
        }
    }

    public static JSB3Data findData(String classname, List<JSB3Data> modelDataList)
    {
        JSB3Data dataLocal = null;

        for (JSB3Data data : modelDataList)
        {
            if (data.getClassName().trim().equalsIgnoreCase(classname.trim()))
            {
                dataLocal = data;
                break;
            }
        }

        return dataLocal;
    }

    public static List<JSB3Data> buildDataList(List<File> allFiles)
    {
        List<JSB3Data> dataList = new ArrayList<JSB3Data>();
        JSB3Data jsb3Data = null;

        for (File file : allFiles)
        {
            System.out.println("  " + file);
            jsb3Data = getJSB3Data(file);

            if (jsb3Data != null)
            {
                dataList.add(jsb3Data);
                processFilesCount++;
            }
            else
            {
                System.out.println("WARNING file not included: " + file);
            }
        }

        return dataList;
    }

    public static JSB3Data getJSB3Data(File file)
    {
        JSB3Data jsb3Data = null;
        StringBuilder contents = new StringBuilder();
        BufferedReader input = null;
        FileReader fileReader = null;
        String className = null;
        String extend = null;
        String fileNameLocal = null;
        String path = null;
        Set<String> requiresFileNames = new HashSet<String>();
        Set<String> usesFileNames = new HashSet<String>();

        try
        {
            if (file != null)
            {
                fileReader = new FileReader(file);
                input = new BufferedReader(fileReader);
                String line = null;

                while ((line = input.readLine()) != null)
                {
                    contents.append(line);
                    contents.append(System.getProperty("line.separator"));
                }

                String test = "";

                if (contents != null)
                {
                    String contentLocal = contents.toString();
                    className = getClassName(contentLocal);
                    fileNameLocal = file.getName();

                    path = file.getAbsolutePath();

                    if (path.contains("app" + separator))
                    {
                        path = path.substring(path.indexOf("app"), path.length());
                        path = path.substring(0, path.lastIndexOf(separator) + 1);
                        String originalContent = contents.toString();
                        extend = getExtendName(originalContent);

                        // requires
                        if (originalContent.contains("requires:") || originalContent.contains("requires :"))
                        {
                            if (originalContent.contains("requires:"))
                            {
                                originalContent = originalContent.substring(originalContent.indexOf("requires:"), originalContent.length());
                            }
                            else
                            {
                                originalContent = originalContent.substring(originalContent.indexOf("requires :"), originalContent.length());
                            }

                            requiresFileNames = getReqClassNames(originalContent);
                        }

                        // uses
                        if (originalContent.contains("uses:") || originalContent.contains("uses :"))
                        {
                            if (originalContent.contains("uses:"))
                            {
                                originalContent = originalContent.substring(originalContent.indexOf("uses:"), originalContent.length());
                            }
                            else
                            {
                                originalContent = originalContent.substring(originalContent.indexOf("uses :"), originalContent.length());
                            }

                            usesFileNames = getReqClassNames(originalContent);
                        }

                        jsb3Data = new JSB3Data(className, extend, fileNameLocal, path, requiresFileNames, usesFileNames);
                    }
                    else
                    {
                        System.out.println("Excluding file: " + path);
                    }

                }
            }
        }
        catch (Throwable t)
        {
            t.printStackTrace();
        }
        finally
        {
            try
            {
                if (fileReader != null)
                {
                    fileReader.close();
                }

                if (input != null)
                {
                    input.close();
                }
            }
            catch (Throwable t)
            {
            }
        }

        return jsb3Data;
    }

    public static Set<String> getReqClassNames(String content)
    {
        String reqClassString = "";
        Set<String> reqClassNames = new HashSet<String>();

        if (content != null && !content.equals(""))
        {
            Matcher matcher = REQ_PATTERN.matcher(content);

            while (matcher.find())
            {
                reqClassString = matcher.group();
                break;
            }

            if (reqClassString != null && !reqClassString.equals(""))
            {
                reqClassString = reqClassString.replace("requires", "");
                reqClassString = reqClassString.replace(":", "");

                reqClassString = reqClassString.trim();

                if (reqClassString.startsWith("["))
                {
                    reqClassString = reqClassString.replace("[", " ");
                    reqClassString = reqClassString.replace("]", " ");
                    // reqClassString = reqClassString.substring(1,
                    // reqClassString.indexOf("],"));
                    buildRequiresFileName(reqClassString, reqClassNames);
                }
                else if (reqClassString.startsWith("'"))
                {
                    reqClassString = reqClassString.replace("'", " ");
                    reqClassString = reqClassString.trim();
                    reqClassNames.add(reqClassString);
                }
            }
        }

        return reqClassNames;
    }

    public static String getExtendName(String content)
    {
        String match = "";
        Matcher matcher = EXTEND_PATTERN.matcher(content);

        while (matcher.find())
        {
            match = matcher.group();

            if (match != null && !match.equals(""))
            {
                match = match.replace("extend", "");
                match = match.replace(":", "");
                match = match.replace("'", "");
                match = match.replace("\"", "");
                match = match.trim();
            }

            break;
        }

        return match;
    }

    public static String getPathName(String contentDesc)
    {
        StringBuffer sb = new StringBuffer();

        if (contentDesc != null && !contentDesc.equals(""))
        {
            Matcher matcher = CLASSNAME_PATTERN.matcher(contentDesc);

            while (matcher.find())
            {
                String match = matcher.group();

                if (match != null && !match.equals("") && match.length() >= 2)
                {
                    match = match.substring(2, match.length() - 1);
                    sb.append(match + " ");
                }
            }
        }

        return sb.toString();
    }

    public static String getClassName(String contentDesc)
    {
        String match = "";

        if (contentDesc != null && !contentDesc.equals(""))
        {
            Matcher matcher = CLASSNAME_PATTERN.matcher(contentDesc);

            while (matcher.find())
            {
                match = matcher.group();

                if (match != null && !match.equals(""))
                {
                    match = match.replace("Ext.define", "");
                    match = match.replace("(", "");
                    match = match.replace("'", "");
                    match = match.replace("\"", "");
                    match = match.trim();
                }
            }
        }

        return match;
    }

    public static void buildRequiresFileName(String reqFileNames, Set<String> requiresFileNames)
    {
        try
        {
            if (reqFileNames.equalsIgnoreCase("'RS.view.common.RSMenu' 'RS.view.common.RefreshButton' 'RS.view.ActiontaskTabCP'  'RS.view.ActiontaskPostPanel'"))
            {
                String stop = "";
                System.out.println("");
            }

            if (reqFileNames != null && !reqFileNames.equals(""))
            {
                reqFileNames = reqFileNames.replace(",", " ");
                reqFileNames = reqFileNames.trim();

                String[] reqs = reqFileNames.split(" ");

                if (reqs != null && reqs.length >= 0)
                {
                    for (int i = 0; i < reqs.length; i++)
                    {
                        String reqsName = reqs[i];

                        if (reqsName != null && !reqsName.equals("") && reqsName.length() >= 1)
                        {
                            reqsName = reqsName.substring(1, reqsName.length() - 1);
                            requiresFileNames.add(reqsName);
                        }
                    }
                }
            }
        }
        catch (Exception exp)
        {
            System.out.println(exp);
        }

    }

    public static void getAllJSFiles(List<File> allModelFiles, List<File> allStoreFiles, List<File> allViewFiles, List<File> allCtrFiles, List<File> allOtherFiles, File dir)
    {
        if (dir.isFile() && dir.getName().endsWith(".js"))
        {
            System.out.println("  " + dir);
            if (dir.getAbsolutePath().contains("app" + separator + "model"))
            {
                allModelFiles.add(dir);
            }
            else if (dir.getAbsolutePath().contains("app" + separator + "store"))
            {
                allStoreFiles.add(dir);
            }
            else if (dir.getAbsolutePath().contains("app" + separator + "view"))
            {
                allViewFiles.add(dir);
            }
            else if (dir.getAbsolutePath().contains("app" + separator + "controller"))
            {
                allCtrFiles.add(dir);
            }
            else
            {
                allOtherFiles.add(dir);
            }
            allFilesCount++;
        }
        else if (dir.isDirectory())
        {
            File[] listFiles = dir.listFiles();
            if (listFiles != null)
            {
                Arrays.sort(listFiles);
                for (int i = 0; i < listFiles.length; i++)
                {
                    getAllJSFiles(allModelFiles, allStoreFiles, allViewFiles, allCtrFiles, allOtherFiles, listFiles[i]);
                }
            }
        }
    }

    public static String buildFirstPart()
    {
        StringBuffer firstPart = new StringBuffer();

        firstPart.append("{ " + "\n");
        firstPart.append("\"projectName\": \"Resolve\"," + "\n");
        firstPart.append("\"licenseText\": \"******************************************************************************\n (C) Copyright 2016\n\n Resolve Systems, LLC\n\n All rights reserved\n This software is distributed under license and may not be used, copied,\n modified, or distributed without the express written permission of\n Resolve Systems, LLC.\n\n******************************************************************************\"," + "\n");
        firstPart.append("\"builds\": [" + "\n");
        firstPart.append("{" + "\n");
        firstPart.append("\"name\": \""+moduleName+" classes\"," + "\n");
        firstPart.append("\"target\": \""+moduleName+"-classes.js\"," + "\n");
//        firstPart.append("\"compress\": true," + "\n");
        firstPart.append("\"options\": {" + "\n");
//        firstPart.append("\"debug\": true" + "\n");
        firstPart.append("}," + "\n");
        firstPart.append("\"files\": [" + "\n");

        return firstPart.toString();
    }

    public static String buildLastPart()
    {
        StringBuffer lastPart = new StringBuffer();

        lastPart.append("]" + "\n");
        lastPart.append("}" + "\n");
//        lastPart.append("}," + "\n");
//        lastPart.append("{" + "\n");
//        lastPart.append("\"name\": \"Application - Production\"," + "\n");
//        lastPart.append("\"target\": \"app-"+moduleName+".js\"," + "\n");
//        lastPart.append("\"compress\": true," + "\n");
//        lastPart.append("\"files\": [" + "\n");
//        lastPart.append("{" + "\n");
//        lastPart.append("\"path\": \"\"," + "\n");
//        lastPart.append("\"name\": \""+moduleName+"-classes.js\"" + "\n");
//        lastPart.append("}" + "\n");
        /*
         * lastPart.append("}," + "\n"); lastPart.append("{" + "\n");
         * lastPart.append("\"path\": \"\"," + "\n");
         * lastPart.append("\"name\": \"app.js\"" + "\n"); lastPart.append("}" +
         * "\n");
         */
//        lastPart.append("]" + "\n");
//        lastPart.append("}" + "\n");
        lastPart.append("]," + "\n");
        lastPart.append("\"resources\": []" + "\n");
        lastPart.append("}" + "\n");

        return lastPart.toString();
    }
}
