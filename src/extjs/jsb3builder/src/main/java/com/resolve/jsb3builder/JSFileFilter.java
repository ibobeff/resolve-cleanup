/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.jsb3builder;

import java.io.File;
import java.io.FileFilter;

public class JSFileFilter implements FileFilter 
{
    public boolean accept(File pathname) 
    {
      if(pathname.getName().endsWith(".js"))
      {
        return true;
      }
      else
      {
          return false;
      }
    }
}