glu.defModel('RS.socialadmin.AddComponent', {

	formDisplayName: '',
	stateId: 'AddComponentStateId',

	AddComponentColumns: [],
	addComponentGridStore: {
		xtype: 'store',
		fields: ['sys_id'],
		remoteSort: true
	},

	addComponentGridStoreSelections: [],

	init: function() {
		// alert (this.parentComp)
		if (this.comp == 'users') {
			this.selectUserColumns()
		} else if (this.comp == 'document' || this.comp == 'runbook' || this.comp == 'actiontask') {
			this.selectDocumentColumns();
		} else if (this.comp == 'team' || this.comp == 'forum') {
			this.selectTeamOrForumColumns()
		} else if (this.comp == 'rss') {
			this.selectRSSColumns();
		}
	},

	selectComponent: function() {

	},

	selectComponentIsEnabled$: function() {
		return this.addComponentGridStoreSelections.length > 0
	},

	cancel: function() {
		this.close()
	},

	selectUserColumns: function() {
		this.set('AddComponentColumns', [{
			header: 'id',
			dataIndex: 'sys_id',
			hidden: true
		}, {
			header: 'User Id',
			dataIndex: 'userId',
			filterable: true,
			sortable: true,
			width: 120
		}, {
			header: 'First Name',
			dataIndex: 'firstName',
			filterable: true,
			sortable: true,
			width: 120
		}, {
			header: 'Last Name',
			dataIndex: 'lastName',
			filterable: true,
			sortable: true,
			flex: 1
		}])
	},

	selectDocumentColumns: function() {
		this.set('AddComponentColumns', [{
			header: 'id',
			dataIndex: 'sys_id',
			hidden: true
		}, {
			header: 'Name',
			dataIndex: 'u_name',
			filterable: true,
			sortable: true,
			width: 120
		}, {
			header: 'Namespace',
			dataIndex: 'u_namespace',
			filterable: true,
			sortable: true,
			width: 120
		}, {
			header: 'summary',
			dataIndex: 'u_summary',
			filterable: true,
			sortable: true,
			flex: 1
		}])
	},

	selectTeamOrForumColumns: function() {
		this.set('AddComponentColumns', [{
			header: 'id',
			dataIndex: 'sys_id',
			hidden: true
		}, {
			header: 'Name',
			dataIndex: 'u_name',
			filterable: true,
			sortable: true,
			width: 120
		}, {
			header: 'Owner',
			dataIndex: 'u_owner',
			filterable: true,
			sortable: true,
			width: 120
		}, {
			header: 'Description',
			dataIndex: 'u_description',
			filterable: true,
			sortable: true,
			flex: 1
		}])
	},

	selectRSSColumns: function() {
		this.set('AddComponentColumns', [{
			header: 'id',
			dataIndex: 'sys_id',
			hidden: true
		}, {
			header: 'Name',
			dataIndex: 'u_name',
			filterable: true,
			sortable: true,
			width: 120
		}, {
			header: 'Owner',
			dataIndex: 'u_owner',
			filterable: true,
			sortable: true,
			width: 120
		}, {
			header: 'URL',
			dataIndex: 'u_url',
			filterable: true,
			sortable: true,
			flex: 1
		}])
	}
})