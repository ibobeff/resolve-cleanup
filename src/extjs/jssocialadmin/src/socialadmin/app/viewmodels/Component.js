glu.defModel('RS.socialadmin.Component', {
    wait: false,

    activate: function(screen, params) {
        this.resetForm()
        this.set('id', params && params.id != null ? params.id : '');
        this.set('name', params.name);
        this.set('wait', false);
        this.set('activeTab', 0)

        if (this.name == 'process') {
            this.set('componentTabName', this.localize('components'))
        } else if (this.name == 'teams' || this.name == 'forums') {
            this.set('componentTabName', this.localize('members'))
        }

        if (!this.name)
            clientVM.displayError(this.localize('noComponentNameProvided'))

        this.loadComponent()
    },

    title$: function() {
        var name = this.name,
            display = ''
        switch (name) {
            case 'process':
                display = this.localize('process')
                break;
            case 'teams':
                display = this.localize('team')
                break;
            case 'forums':
                display = this.localize('forum')
                break;
            case 'rss':
                display = this.localize('rss')
                break;
        }

        return display + (this.u_display_name ? ' - ' + this.u_display_name : '')
    },

    loadDefaultAccessRights: function() {
		this.set('wait', true);
        this.ajax({
            url: '/resolve/service/socialadmin/defaultAccessRights/get',
            params: {
                compType: this.name
            },
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					var data = respData.data;
					this.processAccessRights(data)
				} else {
					clientVM.displayError(respData.message);
				}
            },
            failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
        })
    },

    componentFormName: '',
    name: '',
    componentTabName: '',

    defaultData: {
        u_display_name: '',
        u_owner: '',
        u_user_display_name: '',
        u_is_private: false,
        u_is_locked: false,
        u_description: '',
        u_module: '',
        u_read_roles: '',
        u_edit_roles: '',
        u_post_roles: '',
        u_im: '',
        u_im_pwd: '',
        u_im_display_name: '',
        u_email: '',
        u_send_to: '',
        u_receive_from: '',
        u_receive_pwd: '',
        u_email_display_name: '',
        u_schedule: '0 0 * * * ?',

        //sys field
        id: '',
        sysCreatedOn: '',
        sysUpdatedOn: '',
        sysCreatedBy: '',
        sysUpdatedBy: '',
        sysOrg: ''
    },

    u_display_name: '',
    u_owner: '',
    u_user_display_name: '',
    u_is_private: false,
    u_description: '',
    u_module: '',
    u_read_roles: '',
    u_edit_roles: '',
    u_post_roles: '',
    u_im: '',
    u_im_pwd: '',
    u_im_display_name: '',
    u_email: '',
    u_send_to: '',
    u_receive_from: '',
    u_receive_pwd: '',
    u_email_display_name: '',

    //sys field
    id: '',
    sysCreatedOn: '',
    sysUpdatedOn: '',
    sysCreatedBy: '',
    sysUpdatedBy: '',
    sysOrg: '',

    u_editable: true,
    u_is_locked: false,

    u_schedule: '0 0 * * * ?',
    u_scheduleOptionsList: {
        mtype: 'store',
        fields: ['text', 'value'],
        data: [{
            text: '5 mins',
            value: '0 0/5 * * * ?'
        }, {
            text: '10 mins',
            value: '0 0/10 * * * ?'
        }, {
            text: '15 mins',
            value: '0 0/15 * * * ?'
        }, {
            text: '30 mins',
            value: '0 0/30 * * * ?'
        }, {
            text: '1 hr',
            value: '0 0 * * * ?'
        }, {
            text: '2 hr',
            value: '0 0 0/2 * * ?'
        }, {
            text: '4 hr',
            value: '0 0 0/4 * * ?'
        }, {
            text: '8 hr',
            value: '0 0 0/8 * * ?'
        }, {
            text: '24 hr',
            value: '0 0 0 * * ?'
        }]
    },

    isRSS$: function() {
        return this.name == 'rss'
    },

    u_urlIsVisible$: function() {
        return this.isRSS
    },

    u_ownerIsVisible$: function() {
        return !this.isRSS
    },

    u_rss_ownerIsVisible$: function() {
        return this.isRSS
    },

    u_scheduleIsVisible$: function() {
        return this.isRSS
    },

    mailingListValue: true,
    directValue: false,
    emailRadio: '',

    u_display_nameIsVisible$: function() {
        return !this.id || !this.isRSS
    },
    uDisplayName$: function() {
        return this.u_display_name
    },
    uDisplayNameIsVisible$: function() {
        return this.id && this.isRSS
    },

    hasGroup: false,

    fields: ['id', 'u_display_name', 'u_owner', 'u_description', 'u_is_private', 'u_is_locked',
        'u_im', 'u_im_pwd', 'u_im_display_name', 'u_send_to', 'u_receive_from', 'u_receive_pwd',
        'u_email_display_name', 'u_rss_owner', 'u_url', 'u_feed_updated', 'u_schedule', 'u_user_display_name', {
            name: 'u_editable',
            type: 'boolean'
        }, {
            name: 'hasGroup',
            type: 'boolean'
        },
        'sys_created_by', 'u_read_roles', 'u_edit_roles', 'u_post_roles', {
            name: 'sys_created_on',
            type: 'long'
        }, {
            name: 'sys_updated_on',
            type: 'long'
        }, 'sys_updated_by'
    ],

    init: function() {
        if (this.mock) this.backend = RS.socialadmin.createMockBackend(true);


        this.allComponentsGrid.on('beforeload', function(store, operation, eOpts) {
            operation.params = operation.params || {};
            Ext.apply(operation.params, {
                id: this.id,
                compType: this.name
            })
        }, this)
    },


    loadComponent: function() {
        this.allComponentsGrid.removeAll()
        if (this.id) {
            this.allComponentsGrid.load()
            this.set('wait', true);
            this.ajax({
                url: '/resolve/service/socialadmin/component/get',
                params: {
                    compType: this.name,
                    id: this.id
                },
				scope: this,
                success: function(resp) {
                    var respData = RS.common.parsePayload(resp);
                    if (!respData.success) {
                        clientVM.displayError(respData.message)
                        return
                    }
                    var data = respData.data;
                    this.loadData(data)
                    this.set('u_editable', data.u_editable)
                    RS.common.grid.updateSysFields(this, data)
                    this.processAccessRights(data)
                    this.accessRights.commitChanges()
                },
                failure: function(resp) {
					clientVM.displayFailure(resp);
				},
				callback: function() {
					this.set('wait', false);
				}
            })
        } else this.loadDefaultAccessRights()
    },

    resetForm: function() {
        this.accessRights.removeAll()
        this.loadData(this.defaultData)
        RS.common.grid.updateSysFields(this, this.defaultData)
        this.set('u_owner', clientVM.user.name)
        this.set('u_rss_owner', clientVM.user.name)
    },

    accessRights: {
        mtype: 'store',
        sorters: ['name'],
        fields: ['name', 'value', {
            name: 'edit',
            type: 'bool'
        }, {
            name: 'view',
            type: 'bool'
        }, {
            name: 'post',
            type: 'bool'
        }],
        proxy: {
            type: 'memory',
            reader: {
                type: 'json'
            }
        }
    },

    accessRightsSelections: [],

    addAccessRight: function() {
        var rights = [];

        this.accessRights.each(function(right) {
            rights.push(right.data);
        })

        this.open({
            mtype: 'AccessRightsPicker',
            currentRights: rights
        })
    },

    addRealAccessRights: function(accessRights) {
        Ext.each(accessRights, function(accessRight) {
            this.accessRights.add(Ext.applyIf(accessRight.data, {
                view: true,
                edit: false,
                post: true
            }))
        }, this)
    },

    removeAccessRightIsEnabled$: function() {
        return this.accessRightsSelections.length > 0
    },

    removeAccessRight: function() {
        for (var i = 0; i < this.accessRightsSelections.length; i++) {
            if (this.accessRightsSelections[i].data.name != 'admin') {
                this.accessRights.remove(this.accessRightsSelections[i])
            }
        }
    },

    processAccessRights: function(rsForm) {
        var viewRoles = rsForm.u_read_roles ? rsForm.u_read_roles.split(',') : [],
            editRoles = rsForm.u_edit_roles ? rsForm.u_edit_roles.split(',') : [],
            postRoles = rsForm.u_post_roles ? rsForm.u_post_roles.split(',') : [],
            accessRights = [],
            i;

        for (i = 0; i < viewRoles.length; i++)
            this.addToAccessRights(accessRights, viewRoles[i], 'view', true)
        for (i = 0; i < editRoles.length; i++)
            this.addToAccessRights(accessRights, editRoles[i], 'edit', true)
        for (i = 0; i < postRoles.length; i++)
            this.addToAccessRights(accessRights, postRoles[i], 'post', true)

        this.accessRights.removeAll()
        this.accessRights.add(accessRights)
    },

    pullAccessRightsToViewmodel: function() {
        //Add in access rights
        var viewRoles = [],
            editRoles = [],
            postRoles = [];
        this.accessRights.each(function(accessRight) {
            if (accessRight.data.view) viewRoles.push(accessRight.data.name)
            if (accessRight.data.edit) editRoles.push(accessRight.data.name)
            if (accessRight.data.post) postRoles.push(accessRight.data.name)
        })

        this.set('u_read_roles', viewRoles.join(','))
        this.set('u_edit_roles', editRoles.join(','))
        this.set('u_post_roles', postRoles.join(','))
    },

    pushAccessRightsToViewmodel: function() {
        this.processAccessRights({
            u_read_roles: this.u_read_roles,
            u_edit_roles: this.u_edit_roles,
            u_post_roles: this.u_post_roles
        })
    },

    addToAccessRights: function(accessRights, name, right, value) {
        if (Ext.String.trim(name).length > 0) {
            var i, len = accessRights.length;
            for (i = 0; i < len; i++) {
                if (accessRights[i].name == name) {
                    accessRights[i][right] = value
                    return
                }
            }

            var newName = {
                name: name,
                value: name
            }
            newName[right] = value
            accessRights.push(newName)
        }
    },

    setOwner: function(e) {
        this.open({
            mtype: 'RS.user.UserPicker',
            pick: true,
            callback: this.reallySetOwner
        })
    },
    reallySetOwner: function(selection) {
        if (selection.length > 0) {
            this.set('u_user_display_name', selection[0].get('udisplayName'))
            this.set('u_rss_owner', selection[0].get('uuserName'))
            this.set('u_owner', selection[0].get('uuserName'))
        }
    },

    userDateFormat$: function() {
        return clientVM.getUserDefaultDateFormat()
    },

    // This variable is used to validate GraphDB and SQLDB are in sync.
    validateSave: true,
    saveAndExitFlag: false,

    save: function() {
        if (this.task) this.task.cancel()
        this.task = new Ext.util.DelayedTask(this.saveComponent, this)
        this.task.delay(500)
        this.set('wait', true)
    },

    saveAndExit: function() {
        if (!this.saveIsEnabled)
            return;
        if (this.task) this.task.cancel()
        this.set('saveAndExitFlag', true)
        this.save()
    },

    refresh: function() {
        this.loadComponent()
    },

    saveComponent: function() {
        var socialDTO = {
            u_display_name: this.u_display_name,
            u_owner: this.u_owner,
            u_is_private: this.u_is_private,
            u_is_locked: this.u_is_locked,
            u_description: this.u_description,
            u_module: this.u_module,
            u_is_active: this.u_is_active,
            u_im: this.u_im,
            u_im_pwd: this.u_im_pwd,
            u_im_display_name: this.u_im_display_name,
            u_email: this.u_email,
            u_send_to: this.u_send_to,
            u_receive_from: this.u_receive_from,
            u_receive_pwd: this.u_receive_pwd,
            u_email_display_name: this.u_email_display_name,
            sys_created_on: this.sysCreatedOn,
            sys_created_by: this.sysCreatedBy,
            sys_updated_by: this.sysUpdatedBy,
            sys_updated_on: this.sysUpdatedOn,
            sys_id: this.id
        }

        if (this.isRSS) {
            socialDTO.u_schedule = this.u_schedule
            socialDTO.u_url = this.u_url
            socialDTO.u_rss_owner = this.u_rss_owner
        }

        if (socialDTO.sys_id == '') socialDTO.sys_id = null

        //Add in access rights
        var viewRoles = [],
            editRoles = [],
            postRoles = [];
        this.accessRights.each(function(accessRight) {
            if (accessRight.data.view) viewRoles.push(accessRight.data.name)
            if (accessRight.data.edit) editRoles.push(accessRight.data.name)
            if (accessRight.data.post) postRoles.push(accessRight.data.name)
        });

        socialDTO.u_read_roles = viewRoles.join(',')
        socialDTO.u_edit_roles = editRoles.join(',')
        socialDTO.u_post_roles = postRoles.join(',')

        this.set('wait', true)

        this.ajax({
            url: '/resolve/service/socialadmin/component/save',
            jsonData: socialDTO,
            params: {
                compType: this.name,
                validate: this.validateSave
            },
			scope: this,
            success: function(resp) {
                this.validateSave = true

                var respData = RS.common.parsePayload(resp);

                if (respData.success) {
                    var data = respData.data,
                        messageString;
                    this.loadData(data)
                    this.allComponentsGrid.load()
                    RS.common.grid.updateSysFields(this, data)
                    this.accessRights.commitChanges()

                    switch (this.name.toLowerCase()) {
                        case 'teams':
                            messageString = this.localize('teamSaved')
                            break;
                        case 'process':
                            messageString = this.localize('processSaved')
                            break;
                        case 'forums':
                            messageString = this.localize('forumSaved')
                            break;
                        case 'rss':
                            messageString = this.localize('rssSaved')
                            break;
                    }

                    clientVM.displaySuccess(messageString)

                    if (this.saveAndExitFlag) {
                        this.set('saveAndExitFlag', false)
                        clientVM.handleNavigation({
                            modelName: 'RS.socialadmin.Components',
                            params: {
                                name: this.name
                            }
                        })
                    }
                } else {
                    clientVM.displayError(respData.message)
                        // if (respData.message && respData.message.indexOf('unique') > 0) {
                        //     clientVM.displayError(respData.message);
                        // } else {
                        //     this.message({
                        //         title: '~~confirm~~',
                        //         // msg: 'Graph DB does not contain node for this component. Create a new node and save this component anyway?',
                        //         msg: respData.message,
                        //         buttons: Ext.MessageBox.YESNO,
                        //         buttonText: {
                        //             yes: '~~create~~',
                        //             no: '~~cancel~~'
                        //         },
                        //         scope: this,
                        //         fn: this.saveComponentWithNoValidation
                        //     })
                        // }
                }
            },
            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('wait', false);
			}
        }, this)
    },

    saveComponentWithNoValidation: function(button) {
        if (button === 'yes') {
            this.validateSave = false
            this.save()
        }
    },

    saveIsEnabled$: function() {
        var result = false
        if (!this.isRSS) {
            result = Ext.String.trim(this.u_display_name).length > 0 && (this.u_editable || !this.id)
        } else {
            result = Ext.String.trim(this.u_display_name).length > 0 &&
                Ext.String.trim(this.u_url).length > 0 &&
                Ext.String.trim(this.u_schedule).length > 0 && (this.u_editable || !this.id)
        }

        return result && this.isValid;
    },

    u_display_nameIsValid$: function() {
        var re = new RegExp(/^[\w -]+$/);
        return re.test(this.u_display_name) ? true : this.localize('invalidName')
    },

    u_urlIsValid$: function() {
        return !this.isRSS || (Ext.form.VTypes.url(this.u_url) && this.isRSS) ? true : Ext.form.VTypes.urlText
    },

    back: function() {
        clientVM.handleNavigation({
            modelName: 'RS.socialadmin.Components',
            params: {
                name: this.name
            }
        })
    },

    compTabPanelIsVisible$: function() {
        return !this.isRSS
    },

    emailConfigFormIsVisible$: function() {
        return !this.isRSS
    },

    instandMsgConfigFormIsVisible$: function() {
        return !this.isRSS
    },

    allComponentsGrid: {
        mtype: 'store',
        fields: ['id', 'compType', 'u_display_name', 'u_description'],
        groupField: 'compType',
        // remoteSort: true,
        sorters: [{
            property: 'u_display_name',
            direction: 'ASC'
        }],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/socialadmin/container/components/get',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    allComponentsGridSelections: [],

    addComponent: function() {
        this.pullAccessRightsToViewmodel()
        this.open({
            mtype: 'RS.socialadmin.SelectComponents',
            name: this.name,
            id: this.id
        })
    },
    addComponentIsVisible$: function() {
        return this.activeTab == 1
    },

    addComponentIsEnabled$: function() {
        return this.id && this.u_editable
    },

    removeComponent: function() {
        // Removes components from container.
        var message = message = this.localize(this.allComponentsGridSelections.length == 1 ? 'RemoveCompMsg' : 'RemoveCompsMsg', {
                num: this.allComponentsGridSelections.length
            }),
            removeUser = false,
            removeOtherComps = false,
            userCount = 0;

        Ext.Array.forEach(this.allComponentsGridSelections, function(selection) {
            if (selection.data.compType == 'User') {
                removeUser = true
                userCount++
            } else
                removeOtherComps = true
        })

        if (this.hasGroup) {
            if (removeUser && removeOtherComps) {
                message += '<br/>' + this.localize(userCount === 1 ? 'removeUserAndComponentMessage' : 'removeUsersAndComponentMessage')
            } else if (removeUser) {
                message += '<br/>' + this.localize(userCount === 1 ? 'removeUserMessage' : 'removeUsersMessage')
            } else {
                message += '<br/>' + this.localize(this.allComponentsGridSelections.length == 1 ? 'RemoveCompMsg' : 'RemoveCompsMsg', {
                    num: this.allComponentsGridSelections.length
                })
            }
        }

        this.message({
            title: this.localize('removeComponents'),
            msg: message,
            buttons: Ext.MessageBox.YESNO,
            buttonText: {
                yes: this.localize('remove'),
                no: this.localize('cancel')
            },
            scope: this,
            fn: this.reallyRemoveComponents
        })
    },

    removeComponentsIsVisible$: function() {
        return this.activeTab == 1
    },

    removeComponentIsVisible$: function() {
        return this.activeTab == 1
    },

    reallyRemoveComponents: function(button) {
        if (button === 'yes') {
            var compsToRemove = [];

            Ext.Array.forEach(this.allComponentsGridSelections, function(selection) {
                // if (selection.data.u_display_name != this.u_owner) {
                var component = {
                    compType: selection.data.compType.replace(/ /, ''),
                    id: selection.data.id,
                    u_display_name: selection.data.u_display_name
                }
                compsToRemove.push(component)
                    // }
            }, this)

            this.ajax({
                url: '/resolve/service/socialadmin/container/components/remove',
                jsonData: compsToRemove,
                params: {
                    containerType: this.name,
                    containerId: this.id
                },
                success: function(response) {
                    var respData = RS.common.parsePayload(response);

                    if (respData.success) {
                        if(respData.message)
                            clientVM.displayError(respData.message)
                        else 
                            clientVM.displaySuccess(this.allComponentsGridSelections.length === 1 ? this.localize('componentRemoved') : this.localize('componentsRemoved'))
                        this.allComponentsGrid.load()
                    } else
                        clientVM.displayError(respData.message)
                },
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
            }, this)
        }
    },

    removeComponentIsEnabled$: function() {
        return this.allComponentsGridSelections.length > 0 && this.u_editable
    },

    activeTab: 0,

    generalInfoTab: function() {
        this.set('activeTab', 0)
    },

    generalInfoTabIsPressed$: function() {
        return this.activeTab == 0
    },

    componentTab: function() {
        this.set('activeTab', 1)
    },

    componentTabIsPressed$: function() {
        return this.activeTab == 1
    },

    componentTabIsVisible$: function() {
        return !this.isRSS
    },

    generalInfoTabIsVisible$: function() {
        return !this.isRSS
    },

    allComponentsGridIsVisible$: function() {
        return !this.isRSS
    }
})