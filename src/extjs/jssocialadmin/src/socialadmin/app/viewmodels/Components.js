glu.defModel('RS.socialadmin.Components', {

    name: '',
    mock: false,
    socialTableName: '',

    wait: false,

    activate: function(screens, params) {
        this.set('name', params ? params.name : '')
        switch (this.name) {
            case 'process':
                this.set('socialTableName', 'social_process')
                this.set('displayName', this.localize('processes'))
                clientVM.setWindowTitle(this.localize('socialProcesses'))
                break;
            case 'teams':
                this.set('socialTableName', 'social_team')
                this.set('displayName', this.localize('teams'))
                clientVM.setWindowTitle(this.localize('socialTeams'))
                break;
            case 'forums':
                this.set('socialTableName', 'social_forum')
                this.set('displayName', this.localize('forums'))
                clientVM.setWindowTitle(this.localize('socialForums'))
                break;
            default:
                this.set('socialTableName', 'rss_subscription')
                this.set('displayName', this.localize('rss'))
                clientVM.setWindowTitle(this.localize('socialRSS'))
                break;
        }
        this.components.load();
    },

    displayName: '',
    rssDisplayName: 'RSS',

    componentStateId: 'ComponentsList',
    rssStateId: 'RSSList',

    components: {
        mtype: 'store',
        remoteSort: true,
        sorters: [{
            property: 'u_display_name',
            direction: 'ASC'
        }],
        fields: ['u_display_name', {
            name: 'u_editable',
            type: 'boolean'
        }, 'u_user_display_name', 'u_description', 'u_owner', {
            name: 'u_is_locked',
            type: 'boolean'
        }].concat(RS.common.grid.getSysFields()),

        proxy: {
            type: 'ajax',
            url: '/resolve/service/socialadmin/component/list',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    rssComponentsStore: {
        mtype: 'store',
        remoteSort: true,
        sorters: [{
            property: 'u_display_name',
            direction: 'DESC'
        }],
        fields: ['u_display_name', {
            name: 'u_is_locked',
            type: 'boolean'
        }, 'u_user_display_name', 'u_description', 'u_url', 'u_schedule', 'u_rss_owner', {
            name: 'u_feed_updated',
            type: 'date',
            format: 'time',
            dateFormat: 'time'
        }].concat(RS.common.grid.getSysFields()),

        proxy: {
            type: 'ajax',
            url: '/resolve/service/socialadmin/component/list',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    componentsGridSelections: [],
    rssComponentsGridSelections: [],

    componentColumns: [],
    rssComponentColumns: [],

    allSelected: false,

    cronTimeConversionMap: {
        '0 0/5 * * * ?': '5 mins',
        '0 0/10 * * * ?': '10 mins',
        '0 0/15 * * * ?': '15 mins',
        '0 0/30 * * * ?': '30 mins',
        '0 0 * * * ?': '1 hr',
        '0 0 0/2 * * ?': '2 hr',
        '0 0 0/4 * * ?': '4 hr',
        '0 0 0/8 * * ?': '8 hr',
        '0 0 0 * * ?': '24 hr'
    },

    init: function() {
        if (this.mock)
            this.backend = RS.socialadmin.createMockBackend(true)

        var me = this;

        this.set('componentColumns', [{
            header: '~~name~~',
            dataIndex: 'u_display_name',
            filterable: true,
            sortable: true,
            width: 220
        }, {
            header: '~~locked~~',
            dataIndex: 'u_is_locked',
            filterable: false,
            sortable: false,
            width: 60,
            align: 'center',
            renderer: RS.common.grid.booleanRenderer()
        }, {
            header: '~~owner~~',
            dataIndex: 'u_owner',
            filterable: true,
            sortable: true,
            width: 120,
            renderer:function(v,meta,r){
                return r.get('u_user_display_name');
            }
        }, {
            header: '~~description~~',
            dataIndex: 'u_description',
            filterable: true,
            sortable: true,
            flex: 1
        }].concat(RS.common.grid.getSysColumns()))

        this.set('rssComponentColumns', [{
            header: '~~name~~',
            dataIndex: 'u_display_name',
            filterable: true,
            sortable: true,
            width: 220
        }, {
            header: '~~u_url~~',
            dataIndex: 'u_url',
            filterable: true,
            sortable: true,
            width: 80,
            flex: 1
        }, {
            header: '~~locked~~',
            dataIndex: 'u_is_locked',
            filterable: false,
            sortable: false,
            width: 60,
            align: 'center',
            renderer: RS.common.grid.booleanRenderer()
        }, {
            header: '~~u_schedule~~',
            dataIndex: 'u_schedule',
            filterable: true,
            sortable: true,
            width: 120,
            renderer: function(value, metaData, record, row, col, store, gridView) {
                return me.cronTimeConversionMap[value]
            }
        }, {
            header: '~~u_rss_owner~~',
            dataIndex: 'u_rss_owner',
            filterable: true,
            sortable: true,
            width: 120,
            renderer:function(v,meta,r) {
                return r.get('u_user_display_name');
            }
        }, {
            header: '~~u_feed_updated~~',
            dataIndex: 'u_feed_updated',
            filterable: true,
            sortable: true,
            width: 160,
            renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
        }].concat(RS.common.grid.getSysColumns()))

        this.components.on('beforeload', function(store, operation, eOpts) {
            this.set('wait', true);
            operation.params = operation.params || {}
            Ext.apply(operation.params, {
                socialTableName: this.socialTableName
            })
        }, this)

        this.components.on('load', function(store, records, suc) {
            this.set('wait', false)
            if (!suc)
                clientVM.displayError(this.localize('ListErrMsg'));
        }, this)

        this.rssComponentsStore.on('beforeload', function(store, operation, eOpts) {
            this.set('wait', true);
            operation.params = operation.params || {};
            Ext.apply(operation.params, {
                socialTableName: this.socialTableName
            })
        }, this);

        this.rssComponentsStore.on('load', function(store, records, suc) {
            this.set('wait', false)
            if (!suc)
                clientVM.displayError(this.localize('ListErrMsg'))
        }, this);
    },

    componentsGridIsVisible$: function() {
        return Ext.Array.indexOf(['process', 'teams', 'forums'], this.name) > -1
    },

    rssComponentsGridIsVisible$: function() {
        return this.name == 'rss'
    },

    selectAllAcrossPages: function(selectAll) {
        this.set('allSelected', selectAll)
    },
    selectionChanged: function() {
        this.selectAllAcrossPages(false)
    },

    deleteComponents: function() {
        //Delete the selected record(s), but first confirm the action
        this.message({
            title: this.localize('deleteTitle'),
            msg: this.localize(this.componentsGridSelections.length == 1 ? 'DeleteCompMsg' : 'DeleteCompsMsg', {
                num: this.componentsGridSelections.length
            }),
            buttons: Ext.MessageBox.YESNO,
            buttonText: {
                yes: this.localize('deleteAction'),
                no: this.localize('cancel')
            },
            scope: this,
            fn: this.reallyDeleteRecords
        })
    },

    deleteRssComponents: function() {
        //Delete the selected record(s), but first confirm the action
        this.message({
            title: this.localize('deleteTitle'),
            msg: this.localize(this.rssComponentsGridSelections.length == 1 ? 'DeleteCompMsg' : 'DeleteCompsMsg', {
                num: this.rssComponentsGridSelections.length
            }),
            buttons: Ext.MessageBox.YESNO,
            buttonText: {
                yes: this.localize('deleteAction'),
                no: this.localize('cancel')
            },
            scope: this,
            fn: this.reallyDeleteRecords
        })
    },

    deleteComponentsIsEnabled$: function() {
        return this.componentsGridSelections.length > 0 && !this.wait;
    },

    deleteRssComponentsIsEnabled$: function() {
        return this.rssComponentsGridSelections.length > 0 && !this.wait;
    },

    reallyDeleteRecords: function(button) {
        //Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
        if (button == 'yes') {
            this.set('wait', true)
            var selectedComponents = [];

            if (this.name == 'rss')
                selectedComponents = this.rssComponentsGridSelections
            else
                selectedComponents = this.componentsGridSelections

            if (this.allSelected) {
                this.ajax({
                    url: '/resolve/service/socialadmin/component/delete',
                    params: {
                        ids: '',
                        whereClause: (this.name == 'rss' ? this.rssComponentsStore.lastWhereClause : this.components.lastWhereClause) || '1=1',
                        compType: this.name
                    },
                    scope: this,
                    success: function(r) {
                        this.handleDeleteSuccessResponse(r)
                    },
                    failure: function(resp) {
                        clientVM.displayFailure(resp);
                    },
					callback: function() {
						this.set('wait', false)
					}
                })
            } else {
                var ids = []
                Ext.each(selectedComponents, function(selection) {
                    ids.push(selection.get('id'))
                })
                this.ajax({
                    url: '/resolve/service/socialadmin/component/delete',
                    params: {
                        ids: ids.join(','),
                        compType: this.name
                    },
                    scope: this,
                    success: function(r) {
                        this.handleDeleteSuccessResponse(r)
                    },
                    failure: function(r) {
                        clientVM.displayFailure(r);
                    },
					callback: function() {
						this.set('wait', false)
					}
                })
            }
        }
    },

    handleDeleteSuccessResponse: function(r) {
        var response = RS.common.parsePayload(r);
        if (response.success)
            clientVM.displaySuccess(this.localize('deleteSuccessMessage'))
        else
            clientVM.displayError(response.message)

        if (this.name == 'rss')
            this.rssComponentsStore.load()
        else
            this.components.load()
    },

    createComponent: function() {
        clientVM.handleNavigation({
            modelName: 'RS.socialadmin.Component',
            params: {
                name: this.name
            }
        })
    },

    editComponent: function(id) {
        clientVM.handleNavigation({
            modelName: 'RS.socialadmin.Component',
            params: {
                id: id,
                name: this.name
            }
        })
    }
})