glu.defModel('RS.socialadmin.CreateComponent', {

	wait: false,

	title$: function() {
		if (this.type == 'forum') return this.localize('createForum')
		if (this.type == 'rss') return this.localize('createRss')
		if (this.type == 'process') return this.localize('createProcess')
		if (this.type == 'team') return this.localize('createTeam')
		return this.localize('createComponentTitle')
	},

	type: '',
	typeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: 'json'
		}
	},

	socialTypes: {
		rss: 'rss',
		team: 'teams',
		forum: 'forums',
		process: 'process'
	},

	name: '',
	nameIsValid$: function() {
		var re = new RegExp(/^[\w -]+$/);
		return re.test(this.name) || this.localize('invalidName')
	},

	description: '',
	url: '',
	urlIsVisible$: function() {
		return this.type == 'rss'
	},
	urlIsValid$: function() {
		return Ext.form.VTypes.url(this.url) || Ext.form.VTypes.urlText
	},

	accessRights: {
		mtype: 'store',
		sorters: ['name'],
		fields: ['name', 'value', {
			name: 'edit',
			type: 'bool'
		}, {
			name: 'view',
			type: 'bool'
		}, {
			name: 'post',
			type: 'bool'
		}],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	componentsGrid: {
		mtype: 'store',
		fields: ['id', 'uuserName', 'u_description'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	init: function() {
		this.typeStore.add([{
			name: this.localize('rss'),
			value: 'rss'
		}, {
			name: this.localize('forum'),
			value: 'forum'
		}, {
			name: this.localize('team'),
			value: 'team'
		}, {
			name: this.localize('process'),
			value: 'process'
		}])
		this.typeStore.sort('name')

		if (Ext.Array.indexOf(['rss', 'forum', 'team', 'process'], this.type) == -1)
			this.set('type', this.typeStore.getAt(0).get('value'))

		this.loadDefaultAccessRights()

		this.memberStore.on('beforeload', function(store, operation) {
			operation.params = operation.params || {}

			Ext.apply(operation.params, {
				parentName: this.socialTypes[this.type],
				parentId: '',
				tableName: 'Users',
				modelName: 'Users',
				whereClause: this.memberSearch
			})
		}, this)
	},

	loadDefaultAccessRights: function() {
		this.set('wait', true)
		this.ajax({
			url: '/resolve/service/socialadmin/defaultAccessRights/get',
			params: {
				compType: this.socialTypes[this.type]
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				this.processAccessRights(response.data)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false)
			}
		})
	},

	processAccessRights: function(rsForm) {
		var viewRoles = rsForm.u_read_roles ? rsForm.u_read_roles.split(',') : [],
			editRoles = rsForm.u_edit_roles ? rsForm.u_edit_roles.split(',') : [],
			postRoles = rsForm.u_post_roles ? rsForm.u_post_roles.split(',') : [],
			accessRights = [],
			i;

		for (i = 0; i < viewRoles.length; i++)
			this.addToAccessRights(accessRights, viewRoles[i], 'view', true)
		for (i = 0; i < editRoles.length; i++)
			this.addToAccessRights(accessRights, editRoles[i], 'edit', true)
		for (i = 0; i < postRoles.length; i++)
			this.addToAccessRights(accessRights, postRoles[i], 'post', true)

		this.accessRights.removeAll()
		this.accessRights.add(accessRights)
	},

	addToAccessRights: function(accessRights, name, right, value) {
		var i, len = accessRights.length;
		for (i = 0; i < len; i++) {
			if (accessRights[i].name == name) {
				accessRights[i][right] = value
				return
			}
		}

		var newName = {
			name: name,
			value: name
		}
		newName[right] = value
		accessRights.push(newName)
	},

	create: function() {
		var socialDTO = {
			u_display_name: this.name,
			u_owner: clientVM.user.name,
			u_is_locked: false,
			u_description: this.description,
			sys_id: null
		};

		if (this.type == 'rss') {
			socialDTO.u_schedule = '0 0 * * * ?' //default 1hr
			socialDTO.u_url = this.url
			socialDTO.u_rss_owner = clientVM.user.name
		}

		//Add in access rights
		var viewRoles = [],
			editRoles = [],
			postRoles = [];
		this.accessRights.each(function(accessRight) {
			if (accessRight.data.view) viewRoles.push(accessRight.data.name)
			if (accessRight.data.edit) editRoles.push(accessRight.data.name)
			if (accessRight.data.post) postRoles.push(accessRight.data.name)
		})

		socialDTO.u_read_roles = viewRoles.join(',')
		socialDTO.u_edit_roles = editRoles.join(',')
		socialDTO.u_post_roles = postRoles.join(',')

		this.set('wait', true)

		this.ajax({
			url: '/resolve/service/socialadmin/component/save',
			jsonData: socialDTO,
			params: {
				compType: this.socialTypes[this.type],
				validate: false
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r),
					messageString;

				if (response.success) {
					switch (this.type.toLowerCase()) {
						case 'team':
							messageString = this.localize('teamSaved')
							break;
						case 'process':
							messageString = this.localize('processSaved')
							break;
						case 'forum':
							messageString = this.localize('forumSaved')
							break;
						case 'rss':
							messageString = this.localize('rssSaved')
							break;
					}

					if (this.componentsGrid.getCount() > 0) {
						this.persistMembers(response.data, messageString)
					} else {
						clientVM.displaySuccess(messageString)
						this.doClose()
					}
				} else {
					clientVM.displayError(response.message)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false)
			}
		}, this)
	},
	createIsEnabled$: function() {
		return !this.wait && this.nameIsValid === true
	},
	cancel: function() {
		this.doClose()
	},

	memberSearch: '',
	memberStore: {
		mtype: 'store',
		fields: ['id', 'sys_id', 'uname', 'uuserName', 'ufirstName', 'ulastName', 'unamespace', 'u_description', 'usummary', 'u_display_name', 'u_read_roles'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/socialadmin/addcomponent/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	persistMembers: function(data, msg) {
		var compsToAdd = [];
		this.componentsGrid.each(function(selection) {
			compsToAdd.push({
				compType: 'Users',
				sys_id: selection.get('id'),
				u_display_name: selection.get('uuserName')
			})
		}, this)

		this.ajax({
			url: '/resolve/service/socialadmin/container/components/add',
			jsonData: compsToAdd,
			params: {
				containerType: this.socialTypes[this.type],
				containerId: data.id,
				addUsersToGroup: false,
				validate: false
			},

			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					clientVM.displaySuccess(msg)
					this.doClose()
				} else {
					clientVM.displayError(response.message)
					return
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		}, this)
	},

	addUser: function() {
		//Add the highlighted user to the list of members
		var record = this.memberStore.getById(this.memberSearch)
		if (record) {
			this.componentsGrid.add({
				id: record.get('id'),
				uuserName: record.get('uuserName'),
				u_description: record.get('ufirstName') + ' ' + record.get('ulastName')
			})
		}
	},
	addUserIsEnabled$: function() {
		return this.memberSearch && this.memberStore.getById(this.memberSearch)
	}
})