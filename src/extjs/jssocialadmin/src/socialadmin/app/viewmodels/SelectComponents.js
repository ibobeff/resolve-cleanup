glu.defModel('RS.socialadmin.SelectComponents', {

	activate: function() {},

	windowTitle$: function() {
		return this.localize('addComponents')
	},

	name: '',

	componentsGrid: {
		mtype: 'store',
		sorters: ['uname'],
		fields: ['id', 'sys_id', 'name', 'uname', 'uuserName', 'ufirstName', 'ulastName', 'unamespace', 'u_description', 'usummary', 'u_display_name', 'u_read_roles', 'ufullname'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/socialadmin/addcomponent/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	componentsGridSelections: [],

	componentsGridColumns: [],

	type: 'Users',
	typeOptionsList: {
		mtype: 'store',
		sorters: ['componentName'],
		fields: ['componentName', 'componentValue'],
		proxy: {
			type: 'memory'
		}
	},

	searchQuery: '',

	allSelected: false,
	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},

	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	init: function() {
		this.typeOptionsList.removeAll()

		if (this.name == 'process') {
			this.typeOptionsList.add([{
				componentName: this.localize('users'),
				componentValue: 'Users',
				type: 'String'
			}, {
				componentName: this.localize('documents'),
				componentValue: 'WikiDocument',
				type: 'String'
			}, {
				componentName: this.localize('runbooks'),
				componentValue: 'Runbook',
				type: 'String'
			}, {
				componentName: this.localize('actionTasks'),
				componentValue: 'ResolveActionTask',
				type: 'String'
			}, {
				componentName: this.localize('teams'),
				componentValue: 'social_team',
				type: 'String'
			}, {
				componentName: this.localize('forums'),
				componentValue: 'social_forum',
				type: 'String'
			}, {
				componentName: this.localize('rss'),
				componentValue: 'rss_subscription',
				type: 'String'
			}, {
				componentName: 'Namespace',
				componentValue: 'namespace',
				type: 'String'
			}])
		} else if (this.name == 'teams') {
			this.typeOptionsList.add([{
				componentName: this.localize('users'),
				componentValue: 'Users',
				type: 'String'
			}, {
				componentName: this.localize('teams'),
				componentValue: 'social_team',
				type: 'String'
			}])
		} else if (this.name == 'forums') {
			this.typeOptionsList.add([{
				componentName: this.localize('users'),
				componentValue: 'Users',
				type: 'String'
			}])
		}

		this.componentsGrid.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};

			Ext.apply(operation.params, {
				tableName: this.type,
				modelName: this.type,
				whereClause: this.searchQuery,
				parentName: this.name,
				parentId: this.id
			})
		}, this)

		this.componentsGrid.load()
	},

	when_filter_changes_reload_grid: {
		on: ['typeChanged', 'searchQueryChanged'],
		action: function() {
			this.componentsGrid.loadPage(1)
		}
	},

	search: function() {
		this.componentsGrid.load()
	},

	compsToAdd: [],
	add: function() {
		/*
        	Check if there's a group whose name is same as that of this Team.
        	hasGroup flag will be true if this Team is linked to the group.
        	So, before adding a user to team check if above condition is true.
        	If yes, prompt current user to add the selected users to group as well.
        */
		if (this.parentVM.hasGroup && this.name == 'teams' && this.type == 'Users') {
			this.message({
				title: this.componentsGridSelections.length === 1 ? this.localize('addUserToGroup') : this.localize('addUsersToGroup'),
				msg: this.componentsGridSelections.length === 1 ? this.localize('addUserToGroupMessage') : this.localize('addUsersToGroupMessage'),
				buttons: Ext.MessageBox.YESNO,
				buttonText: {
					yes: this.componentsGridSelections.length === 1 ? this.localize('addUserToGroup') : this.localize('addUsersToGroup'),
					no: this.localize('cancel')
				},
				scope: this,
				fn: this.addUsers
			})
		} else {
			this.addComponents()
		}

		this.doClose()
	},

	addUsersToGroup: false,

	addUsers: function(button) {
		if (button == 'yes') {
			this.set('addUsersToGroup', true)
			this.addComponents()
		}
	},

	validateComponent: true,

	addComponents: function() {
		var compsToAdd = [];
		Ext.Array.forEach(this.componentsGridSelections, function(selection) {
			compsToAdd.push({
				compType: this.type,
				sys_id: selection.data.sys_id,
				u_display_name: selection.data.uuserName || selection.data.u_display_name ||  selection.data.ufullname || selection.data.name
			})
		}, this)

		this.ajax({
			url: '/resolve/service/socialadmin/container/components/add',
			jsonData: compsToAdd,
			params: {
				containerType: this.name,
				containerId: this.parentVM.id,
				addUsersToGroup: this.addUsersToGroup,
				validate: this.validateComponent
			},

			success: function(r) {
				var response = RS.common.parsePayload(r);

				if (response.success) {
					clientVM.displaySuccess(compsToAdd.length === 1 ? this.localize('componentAdded') : this.localize('componentsAdded'))
					this.parentVM.allComponentsGrid.load()
					this.validateComponent = true
				} else {
					// this.message({
					// 	title: 'Confirm',
					// 	// msg: 'Graph DB does not contain node for this component. Create a new node and add this component anyway',
					// 	msg: response.message,
					// 	buttons: Ext.MessageBox.YESNO,
					// 	buttonText: {
					// 		yes: 'Yes',
					// 		no: 'No'
					// 	},
					// 	scope: this,
					// 	fn: this.addCompsWithNoValidation
					// })
					clientVM.displayError(response.message)
					return
				}

				// If the component which is getting added is Team(s), then only check for roles.
				if (compsToAdd[0].compType == 'social_team') {
					this.checkTeamReadRoles()
				} else this.doClose()
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}

		}, this)
	},

	addCompsWithNoValidation: function(button) {
		if (button === 'yes') {
			this.validateComponent = false;
			this.addComponents()
		}
	},

	checkTeamReadRoles: function() {
		var showDialog = false,
			role, parentRoles = this.parentVM.u_read_roles.split(',');
		Ext.Array.each(this.componentsGridSelections, function(selection) {
			roles = selection.get('u_read_roles').split(',')
			Ext.Array.each(roles, function(role) {
				if (Ext.Array.indexOf(parentRoles, role) == -1) {
					showDialog = true
					return false
				}
			}, this)
			if (showDialog) return false
		}, this)

		if (showDialog) {
			this.message({
				title: this.localize('addRoleConfirmation'),
				msg: this.name == 'process' ? this.localize('addRolesToProcess') : this.localize('addRolesToTeam'),
				buttons: Ext.MessageBox.YESNO,
				buttonText: {
					yes: this.localize('addRoles'),
					no: this.localize('cancel')
				},
				scope: this,
				fn: this.updateCurrentTeamsRoles
			})
		} else this.doClose()
	},

	updateCurrentTeamsRoles: function(button) {
		if (button === 'yes') {
			var parentRoles = this.parentVM.u_read_roles.split(',');
			Ext.Array.forEach(this.componentsGridSelections, function(selection) {
				roles = selection.get('u_read_roles').split(',')
				Ext.Array.forEach(roles, function(role) {
					if (Ext.Array.indexOf(parentRoles, role) == -1) {
						parentRoles.push(role)
					}
				}, this)
			}, this)
			this.parentVM.set('u_read_roles', parentRoles.join(','))
			this.parentVM.pushAccessRightsToViewmodel()
			this.parentVM.saveComponent()
		}
		this.doClose()
	},

	addIsEnabled$: function() {
		return this.componentsGridSelections.length > 0
	},

	cancel: function() {
		this.doClose()
	}
})