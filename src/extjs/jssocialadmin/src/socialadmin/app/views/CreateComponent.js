glu.defView('RS.socialadmin.CreateComponent', {
	asWindow: {
		title: '@{title}',
		height: 400,
		width: 600,
		modal: true
	},
	bodyPadding: '10px',
	buttonAlign: 'left',
	buttons: ['create', 'cancel'],
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'combobox',
		name: 'type',
		store: '@{typeStore}',
		displayField: 'name',
		valueField: 'value',
		queryMode: 'local'
	}, {
		xtype: 'textfield',
		name: 'name'
	}, {
		xtype: 'textarea',
		name: 'description'
	}, {
		xtype: 'textfield',
		name: 'url'
	}, {
		xtype: 'grid',
		name: 'componentsGrid',
		title: '~~members~~',
		flex: 1,
		minHeight: 200,
		tbar: [{
			xtype: 'combobox',
			name: 'memberSearch',
			hideLabel: true,
			store: '@{memberStore}',
			displayField: 'uuserName',
			valueField: 'id',
			minChars: 1
		}, {
			iconCls: 'icon-plus icon-large rs-icon',
			name: 'addUser',
			tooltip: '~~addUser~~',
			text: ''
		}],
		columns: [{
			text: '~~name~~',
			dataIndex: 'uuserName',
			flex: 1
		}, {
			text: '~~description~~',
			dataIndex: 'u_description',
			flex: 1
		}, {
			xtype: 'actioncolumn',
			width: 50,
			items: [{
				icon: '/resolve/images/closex.gif',
				// iconCls: 'icon-large icon-remove rs-icon',
				tooltip: 'Remove',
				handler: function(grid, rowIndex, colIndex) {
					grid.getStore().removeAt(rowIndex)
				}
			}]
		}]
	}]
})