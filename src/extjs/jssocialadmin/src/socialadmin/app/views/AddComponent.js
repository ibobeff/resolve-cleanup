glu.defView('RS.socialadmin.AddComponent', {
    layout: 'fit',
    items: [{
        xtype: 'grid',
        name: 'addComponentGridStore',
        border: false,
        stateId: '@{stateId}',
        displayName: '@{formDisplayName}',
        stateful: true,
        plugins: [{
            ptype: 'searchfilter',
            allowPersistFilter: false,
            hideMenu: true
        }, {
            ptype: 'pager'
        }],
        selModel: {
            selType: 'resolvecheckboxmodel',
            columnTooltip: RS.common.locale.editColumnTooltip,
            columnTarget: '_self',
            columnEventName: 'editAction',
            columnIdField: 'sys_id',
            childLinkIdProperty: 'sys_id'
        },
        columns: '@{AddComponentColumns}',
        viewConfig: {
            enableTextSelection: true
        }
        // ,
        // listeners: {
        //     editAction: '@{editComponent}',
        //     selectAllAcrossPages: '@{selectAllAcrossPages}',
        //     selectionChange: '@{selectionChanged}'
        // }
    }],
    buttonAlign: 'left',
    buttons: [{
        name: 'selectComponent',
        text: 'Select'
    }, {
        name: 'cancel',
        text: 'Cancel'
    }],
    asWindow: {
        width: 800,
        height: 480
        //title:'@{title}'
    }
});