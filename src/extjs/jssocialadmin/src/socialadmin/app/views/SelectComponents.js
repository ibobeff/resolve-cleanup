glu.defView('RS.socialadmin.SelectComponents', {
	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'componentsGrid',
		columns: '@{componentsGridColumns}',
		selType: 'checkboxmodel',
		listeners: {
			selectionChange: '@{selectionChanged}'
		},
		plugins: [{
			ptype: 'pager',
			showSysInfo: false
		}],
		dockedItems: [{
			name: 'actionBar',
			xtype: 'toolbar',
			items: [{
				xtype: 'combobox',
				name: 'type',
				emptyText: '~~type~~',
				hideLabel: true,
				displayField: 'componentName',
				valueField: 'componentValue',
				queryMode: 'local',
				store: '@{typeOptionsList}',
				width: 160,
				correctColumns: function(newValue) {
					var parentGrid = this.up('grid'),
						gridColumns = [];
					parentGrid.findPlugin('pager').store.currentPage = 1
					if (newValue == 'Users') {
						gridColumns.push({
							text: RS.socialadmin.locale.name,
							dataIndex: 'uuserName',
							filterable: 'true',
							width: 200
						}, {
							text: RS.socialadmin.locale.firstName,
							dataIndex: 'ufirstName',
							filterable: 'true',
							width: 300
						}, {
							text: RS.socialadmin.locale.lastName,
							dataIndex: 'ulastName',
							filterable: 'true',
							flex: 1
						})
					} else if (newValue == 'WikiDocument' || newValue == 'Runbook') {
						gridColumns.push({
							text: RS.socialadmin.locale.name,
							dataIndex: 'uname',
							filterable: 'true',
							width: 200
						}, {
							text: RS.socialadmin.locale.namespace,
							dataIndex: 'unamespace',
							filterable: 'true',
							width: 200
						}, {
							text: RS.socialadmin.locale.summary,
							dataIndex: 'usummary',
							filterable: 'true',
							flex: 1,
							renderer: 'htmlEncode'
						})
					} else if (newValue == 'ResolveActionTask') {
						gridColumns.push({
							text: RS.socialadmin.locale.name,
							dataIndex: 'uname',
							filterable: 'true',
							width: 200
						}, {
							text: RS.socialadmin.locale.namespace,
							dataIndex: 'unamespace',
							filterable: 'true',
							width: 200
						}, {
							text: RS.socialadmin.locale.summary,
							dataIndex: 'usummary',
							filterable: 'true',
							flex: 1
						})
					} else if (newValue == 'social_team' || newValue == 'social_forum' || newValue == 'rss_subscription') {
						gridColumns.push({
							text: RS.socialadmin.locale.name,
							dataIndex: 'u_display_name',
							filterable: 'true',
							width: 200
						}, {
							text: RS.socialadmin.locale.description,
							dataIndex: 'u_description',
							filterable: 'true',
							flex: 1
						})
					} else if (newValue == 'namespace') {
						gridColumns.push({
							text: RS.socialadmin.locale.name,
							dataIndex: 'name',
							filterable: 'true',
							flex: 1
						})
					}

					parentGrid.reconfigure(null, gridColumns)
				},
				listeners: {
					change: function(combo, newValue) {
						combo.correctColumns(newValue)
					},
					render: function(combo) {
						combo.correctColumns(combo.getValue())
					}
				}
			}, {
				xtype: 'triggerfield',
				name: 'searchQuery',
				hideLabel: true,
				flex: 1,
				emptyText: '~~searchQuery~~',
				triggerCls: 'x-form-search-trigger',
				onTriggerClick: function() {
					this.fireEvent('search', this)
				},
				listeners: {
					search: '@{search}'
				}
			}]
		}]
	}],
	buttonAlign: 'left',
	buttons: ['add', 'cancel'],

	asWindow: {
		title: '@{windowTitle}',
		modal: true,
		width: 800,
		height: 400
	}
})