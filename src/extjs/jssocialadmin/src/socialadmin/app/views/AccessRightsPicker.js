glu.defView('RS.socialadmin.AccessRightsPicker', {
	height: 300,
	width: 500,

	title: '~~addAccessRights~~',

	layout: 'fit',
	items: [{
		xtype: 'grid',
		forceFit: true,
		name: 'roles',
		columns: [{
			header: 'Name',
			dataIndex: 'name'
		}]
	}],
	buttonAlign: 'left',
	buttons: [{
		name: 'addAccessRightsAction'
	}, {
		name: 'cancel'
	}]
});