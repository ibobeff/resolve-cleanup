glu.defView('RS.socialadmin.Components', {
    layout: 'fit',
    padding: 15,
	cls : 'rs-grid-dark',
    items: [{
        xtype: 'grid',
        name: 'componentsGrid',
        store: '@{components}',
        stateId: '@{componentStateId}',
        columns: '@{componentColumns}',
        displayName: '@{displayName}',
        setDisplayName: function(value) {
            this.getPlugin('searchfilter').toolbar.down('#tableMenu').setText(value)
        },
        stateful: true,
        plugins: [{
            ptype: 'searchfilter',
            pluginId: 'searchfilter',
            allowPersistFilter: false,
            hideMenu: true
        }, {
            ptype: 'pager'
        }],
        selModel: {
            selType: 'resolvecheckboxmodel',
            columnTooltip: RS.common.locale.editColumnTooltip,
            columnTarget: '_self',
            columnEventName: 'editAction',
            columnIdField: 'id'
        },
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            name: 'actionBar',
			cls: 'actionBar rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light',
			},
            items: ['createComponent', 'deleteComponents']
        }],

        viewConfig: {
            enableTextSelection: true
        },
        listeners: {
            editAction: '@{editComponent}',
            selectAllAcrossPages: '@{selectAllAcrossPages}',
            selectionChange: '@{selectionChanged}'
        }
    }, {
        xtype: 'grid',
        name: 'rssComponentsGrid',
        store: '@{rssComponentsStore}',
        stateId: '@{rssStateId}',
        displayName: '@{rssDisplayName}',
        columns: '@{rssComponentColumns}',
        stateful: true,
        plugins: [{
            ptype: 'searchfilter',
            allowPersistFilter: false,
            hideMenu: true
        }, {
            ptype: 'pager',
            notActivateCtrl_R: true
        }],
        selModel: {
            selType: 'resolvecheckboxmodel',
            columnTooltip: RS.common.locale.editColumnTooltip,
            columnTarget: '_self',
            columnEventName: 'editAction',
            columnIdField: 'id',
            childLinkIdProperty: 'id'
        },
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            cls: 'actionBar',
            name: 'actionBar',
			cls: 'actionBar rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light',
			},
            items: ['createComponent', 'deleteRssComponents']
        }],

        viewConfig: {
            enableTextSelection: true
        },
        listeners: {
            editAction: '@{editComponent}',
            selectAllAcrossPages: '@{selectAllAcrossPages}',
            selectionChange: '@{selectionChanged}'
        }
    }]
});