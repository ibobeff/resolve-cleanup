glu.defView('RS.socialadmin.Component', {
	xtype: 'panel',
	layout: 'card',
	padding: 15,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls : 'rs-dockedtoolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{title}'
		}, '->', {
			name: 'generalInfoTab'
		}, {
			name: 'componentTab',
			text: '@{componentTabName}'
		}],
		margin : '0 0 15 0'
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light',
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, {
			xtype: 'tbseparator',
			hidden: '@{!addComponentIsVisible}'
		}, 'addComponent', 'removeComponent', '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrg}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	autoScroll: true,
	activeItem: '@{activeTab}',
	items: [{
		layout: 'border',
		minWidth: 650,
		minHeight: 500,
		items: [{
			region: 'center',
			minWidth: 350,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				bodyPadding: '10 5 0 0'
			},
			items: [{
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaults: {
					labelWidth: 135
				},
				items: [{
					xtype: 'textfield',
					name: 'u_display_name'
				}, {
					xtype: 'displayfield',
					name: 'uDisplayName'
				}, {
					xtype: 'triggerfield',
					triggerCls: 'x-form-search-trigger',
					name: 'u_user_display_name',
					onTriggerClick: function() {
						this.fireEvent('focus', this)
					},
					listeners: {
						focus: '@{setOwner}'
					}
				}, {
					xtype: 'textfield',
					name: 'u_url'
				}, {
					xtype: 'combobox',
					name: 'u_schedule',
					editable: false,
					queryMode: 'local',
					displayField: 'text',
					valueField: 'value',
					store: '@{u_scheduleOptionsList}'
				}, {
					xtype: 'textarea',
					name: 'u_description'
				}, {
					xtype: 'checkbox',
					name: 'u_is_locked'
				}]
			}, {
				title: '~~emailConfiguration~~',
				defaultType: 'textfield',
				hidden: '@{isRSS}',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaults: {
					labelWidth: 135
				},
				items: ['u_send_to', 'u_receive_from', {
					name: 'u_receive_pwd',
					inputType: 'password'
				}, 'u_email_display_name', {
					xtype: 'radiogroup',
					anchor: 'none',
					layout: {
						autoFlex: false
					},
					defaults: {
						name: 'emailRadio'
					},
					items: [{
						xtype: 'radio',
						boxLabel: '~~mailingList~~',
						value: '@{mailingListValue}'
					}, {
						xtype: 'radio',
						boxLabel: '~~direct~~',
						value: '@{directValue}',
						padding: '0px 0px 0px 100px'
					}]
				}]
			}, {
				border: false,
				title: '~~instantMessagingConfiguration~~',
				defaultType: 'textfield',
				hidden: '@{isRSS}',
				defaults: {
					labelWidth: 135
				},
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: ['u_im', {
					name: 'u_im_pwd',
					inputType: 'password'
				}, 'u_im_display_name']
			}]
		}, {
			xtype: 'grid',
			region: 'east',
			name: 'accessRights',
			width: 300,
			padding: '0 0 0 5',

			title: '~~accessRights~~',
			tbar: [{
				name: 'addAccessRight'
			}, {
				name: 'removeAccessRight'
			}],
			columns: [{
				header: '~~name~~',
				dataIndex: 'name',
				hideable: false,
				flex: 1,
				editor: {
					allowBlank: false
				}
			}, {
				xtype: 'checkcolumn',
				stopSelection: false,
				header: '~~editRight~~',
				dataIndex: 'edit',
				hideable: false,
				width: 70
			}, {
				xtype: 'checkcolumn',
				stopSelection: false,
				header: '~~viewRight~~',
				dataIndex: 'view',
				hideable: false,
				width: 70
			}, {
				xtype: 'checkcolumn',
				stopSelection: false,
				header: '~~postRight~~',
				dataIndex: 'post',
				hideable: false,
				width: 70
			}]
		}]
	}, {
		xtype: 'grid',
		name: 'allComponentsGrid',
		columns: [{
			text: '~~compType~~',
			dataIndex: 'compType'
		}, {
			text: '~~name~~',
			dataIndex: 'u_display_name',
			width: 300
		}, {
			text: '~~description~~',
			dataIndex: 'u_description',
			flex: 1,
			renderer: 'htmlEncode'
		}],

		features: [{
			ftype: 'grouping',
			groupHeaderTpl: '{name} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})',
			hideGroupedHeader: true,
			startCollapsed: false
		}],
		listeners: {
			groupclick: function(grid, field, value, e) {
				if (e.getTarget().tagName == 'INPUT') {
					e.stopPropagation();
					e.stopEvent()
				}
				var t = e.getTarget('.grpCheckbox');
				if (t) {
					var checked = t.checked;
					grid.store.data.items.forEach(function(record, index) {
						if (record.data.compType == value) {
							if (checked) {
								grid.getSelectionModel().select(index, true);
							} else {
								grid.getSelectionModel().deselectRow(index);
							}
						}
					})
				}
			}
		}
	}]
})
