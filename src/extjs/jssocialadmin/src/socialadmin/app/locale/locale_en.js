/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.socialadmin').locale = {

    cancel: 'Cancel',
    save: 'Save',
    back: 'Back',

    name: 'Name',
    editable: 'Editable',
    locked: 'Locked',
    owner: 'Owner',
    description: 'Description',

    userId: 'User Id',
    firstName: 'First Name',
    lastName: 'Last Name',
    users: 'Users',

    AccessRights: 'Access Rights',
    addAccessRights: 'Add Access Rights',
    addAccessRightsAction: 'Add',
    addAccessRight: 'Add',
    removeAccessRight: 'Remove',
    viewRight: 'View',
    postRight: 'Post',
    editRight: 'Admin',

    addComponent: 'Add',
    removeComponent: 'Remove',

    processes: 'Processes',
    socialProcesses: 'Social Processes',
    forums: 'Forums',
    socialForums: 'Social Forums',
    teams: 'Teams',
    socialTeams: 'Social Teams',
    rss: 'RSS',
    socialRSS: 'Social RSS',

    accessRightsTab: 'Access Rights',
    messagingTab: 'Messaging',

    u_url: 'URL',
    u_schedule: 'Schedule',
    u_feed_updated: 'Feed Updated On',
    u_rss_owner: 'Owner',
    u_active: 'Active',
    u_user_display_name: 'Owner',

    addComponents: 'Add Components',

    deleteTitle: 'Delete Components',
    deleteAction: 'Delete',
    DeleteCompsMsg: 'Are you sure you want to delete the {num} selected items?',
    DeleteCompMsg: 'Are you sure you want to delete the selected item?',
    DeleteSucMsg: 'The selected items have been deleted',
    createComponent: 'New',
    deleteComponents: 'Delete',
    deleteRssComponents: 'Delete',

    u_owner: 'Owner',
    u_display_name: 'Name',
    uDisplayName: 'Name',
    u_description: 'Description',
    RemoveCompsMsg: 'Are you sure you want to remove the {num} selected items?',
    RemoveCompMsg: 'Are you sure you want to remove the selected item?',
    u_is_private: 'Is Private',
    u_is_locked: 'Is Locked',

    u_send_to: 'Send To',
    u_receive_from: 'Receive From',
    u_receive_pwd: 'Receive Password',
    u_email_display_name: 'Email Display Name',

    u_im: 'IM Account',
    u_im_pwd: 'IM Password',
    u_im_display_name: 'IM Display Name',

    emailConfiguration: 'Email Configuration',
    accessRights: 'Access Rights',
    mailingList: 'Mailing List',
    direct: 'Direct',
    instantMessagingConfiguration: 'Instant Messaging Configuration',
    invalidName: 'The name is required and should only contains alphabets and digits.',
    actionTasks: 'Action Tasks',
    documents: 'Documents',
    runbooks: 'Runbooks',

    addUser: 'Add User',
    addDuplicateUser: 'Add Duplicate User',

    deleteSuccessMessage: 'Records successfully deleted',

    teamSaved: 'Team successfully saved',
    processSaved: 'Process successfully saved',
    forumSaved: 'Forum successfully saved',
    rssSaved: 'RSS successfully saved',
    generalInfoTab: 'Properties',
    components: 'Components',
    members: 'Members',
    team: 'Team',
    Forum: 'Forum',
    process: 'Process',

    type: 'Select Type',
    searchQuery: 'Search...',
    add: 'Add',
    close: 'Close',

    rssSaved: 'RSS successfully saved',

    user: 'User',
    namespace: 'Namespace',
    summary: 'Summary',

    addUserToGroupMessage: 'This team is linked to a group, add the selected user to the group as well?',
    addUsersToGroupMessage: 'This team is linked to a group, add the selected users to the group as well?',
    addUsersToGroup: 'Add users to Group',
    addUserToGroup: 'Add user to Group',

    addRoleConfirmation: 'Confirm add role',
    addRolesToProcess: 'Would you like to add the roles from the selected teams to the current Process?',
    addRolesToTeam: 'Would you like to add roles from the selected teams to the current Team?',
    addRoles: 'Add Roles',

    forum: 'Forum',

    componentAdded: 'Component successfully added',
    componentsAdded: 'Components successfully added',

    componentsRemoved: 'Components successfully removed',
    componentRemoved: 'Component successfully removed',

    removeUserAndComponentMessage: 'This team is linked to a group. Selected user will also be removed from group.',
    removeUsersAndComponentMessage: 'This team is linked to a group. Selected users will also be removed from group.',


    removeComponents: 'Remove Components',
    remove: 'Remove',

    removeUserMessage: 'This team is linked to a group. Removed the user from group as well?',
    removeUsersMessage: 'This team is linked to a group. Removed the users from group as well?',

    ListErrMsg: 'Unable to retrieve components',

    CreateComponent: {
        createComponentTitle: 'Create Component',
        createRss: 'Create RSS',
        createForum: 'Create Forum',
        createProcess: 'Create Process',
        createTeam: 'Create Team',

        create: 'Create',

        url: 'URL',
        invalidName: 'Please provide a name for this component',

        teamSaved: 'Team successfully saved',
        processSaved: 'Process successfully saved',
        forumSaved: 'Forum successfully saved',
        rssSaved: 'RSS successfully saved',

        addUser: 'Add the user to the list of members'
    },

    addRoleConfirmation: 'Confirm add role',
    addRolesToProcess: 'Would you like to add the roles from the selected teams to the current Process?',
    addRolesToTeam: 'Would you like to add roles from the selected teams to the current Team?',
    addRoles: 'Add Roles',
    cancel: 'Cancel',
    compType: 'Component Type'

}