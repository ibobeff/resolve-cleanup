/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
glu.defModel('RS.socialadmin.AccessRightsPicker', {

	init: function() {
		this.roles.load({
			callback: this.rolesLoaded,
			scope: this
		})
	},

	rolesLoaded: function(records, operation, success) {
		for (var i = 0; i < this.currentRights.length; i++) {
			var currentRight = this.currentRights[i],
				role = this.roles.findBy(function(role) {
					if (role.data.value == currentRight.value) return true;
					return false;
				})
				this.roles.removeAt(role)
		}
	},

	currentRights: [],

	roles: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getRoles',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	rolesSelections: [],

	cancel: function() {
		this.doClose()
	},
	addAccessRightsAction: function() {
		this.parentVM.addRealAccessRights(this.rolesSelections)
		this.doClose()
	},
	addAccessRightsActionIsEnabled$: function() {
		return this.rolesSelections.length > 0
	}
});
glu.defModel('RS.socialadmin.AddComponent', {

	formDisplayName: '',
	stateId: 'AddComponentStateId',

	AddComponentColumns: [],
	addComponentGridStore: {
		xtype: 'store',
		fields: ['sys_id'],
		remoteSort: true
	},

	addComponentGridStoreSelections: [],

	init: function() {
		// alert (this.parentComp)
		if (this.comp == 'users') {
			this.selectUserColumns()
		} else if (this.comp == 'document' || this.comp == 'runbook' || this.comp == 'actiontask') {
			this.selectDocumentColumns();
		} else if (this.comp == 'team' || this.comp == 'forum') {
			this.selectTeamOrForumColumns()
		} else if (this.comp == 'rss') {
			this.selectRSSColumns();
		}
	},

	selectComponent: function() {

	},

	selectComponentIsEnabled$: function() {
		return this.addComponentGridStoreSelections.length > 0
	},

	cancel: function() {
		this.close()
	},

	selectUserColumns: function() {
		this.set('AddComponentColumns', [{
			header: 'id',
			dataIndex: 'sys_id',
			hidden: true
		}, {
			header: 'User Id',
			dataIndex: 'userId',
			filterable: true,
			sortable: true,
			width: 120
		}, {
			header: 'First Name',
			dataIndex: 'firstName',
			filterable: true,
			sortable: true,
			width: 120
		}, {
			header: 'Last Name',
			dataIndex: 'lastName',
			filterable: true,
			sortable: true,
			flex: 1
		}])
	},

	selectDocumentColumns: function() {
		this.set('AddComponentColumns', [{
			header: 'id',
			dataIndex: 'sys_id',
			hidden: true
		}, {
			header: 'Name',
			dataIndex: 'u_name',
			filterable: true,
			sortable: true,
			width: 120
		}, {
			header: 'Namespace',
			dataIndex: 'u_namespace',
			filterable: true,
			sortable: true,
			width: 120
		}, {
			header: 'summary',
			dataIndex: 'u_summary',
			filterable: true,
			sortable: true,
			flex: 1
		}])
	},

	selectTeamOrForumColumns: function() {
		this.set('AddComponentColumns', [{
			header: 'id',
			dataIndex: 'sys_id',
			hidden: true
		}, {
			header: 'Name',
			dataIndex: 'u_name',
			filterable: true,
			sortable: true,
			width: 120
		}, {
			header: 'Owner',
			dataIndex: 'u_owner',
			filterable: true,
			sortable: true,
			width: 120
		}, {
			header: 'Description',
			dataIndex: 'u_description',
			filterable: true,
			sortable: true,
			flex: 1
		}])
	},

	selectRSSColumns: function() {
		this.set('AddComponentColumns', [{
			header: 'id',
			dataIndex: 'sys_id',
			hidden: true
		}, {
			header: 'Name',
			dataIndex: 'u_name',
			filterable: true,
			sortable: true,
			width: 120
		}, {
			header: 'Owner',
			dataIndex: 'u_owner',
			filterable: true,
			sortable: true,
			width: 120
		}, {
			header: 'URL',
			dataIndex: 'u_url',
			filterable: true,
			sortable: true,
			flex: 1
		}])
	}
})
glu.defModel('RS.socialadmin.Component', {
    wait: false,

    activate: function(screen, params) {
        this.resetForm()
        this.set('id', params && params.id != null ? params.id : '');
        this.set('name', params.name);
        this.set('wait', false);
        this.set('activeTab', 0)

        if (this.name == 'process') {
            this.set('componentTabName', this.localize('components'))
        } else if (this.name == 'teams' || this.name == 'forums') {
            this.set('componentTabName', this.localize('members'))
        }

        if (!this.name)
            clientVM.displayError(this.localize('noComponentNameProvided'))

        this.loadComponent()
    },

    title$: function() {
        var name = this.name,
            display = ''
        switch (name) {
            case 'process':
                display = this.localize('process')
                break;
            case 'teams':
                display = this.localize('team')
                break;
            case 'forums':
                display = this.localize('forum')
                break;
            case 'rss':
                display = this.localize('rss')
                break;
        }

        return display + (this.u_display_name ? ' - ' + this.u_display_name : '')
    },

    loadDefaultAccessRights: function() {
		this.set('wait', true);
        this.ajax({
            url: '/resolve/service/socialadmin/defaultAccessRights/get',
            params: {
                compType: this.name
            },
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					var data = respData.data;
					this.processAccessRights(data)
				} else {
					clientVM.displayError(respData.message);
				}
            },
            failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
        })
    },

    componentFormName: '',
    name: '',
    componentTabName: '',

    defaultData: {
        u_display_name: '',
        u_owner: '',
        u_user_display_name: '',
        u_is_private: false,
        u_is_locked: false,
        u_description: '',
        u_module: '',
        u_read_roles: '',
        u_edit_roles: '',
        u_post_roles: '',
        u_im: '',
        u_im_pwd: '',
        u_im_display_name: '',
        u_email: '',
        u_send_to: '',
        u_receive_from: '',
        u_receive_pwd: '',
        u_email_display_name: '',
        u_schedule: '0 0 * * * ?',

        //sys field
        id: '',
        sysCreatedOn: '',
        sysUpdatedOn: '',
        sysCreatedBy: '',
        sysUpdatedBy: '',
        sysOrg: ''
    },

    u_display_name: '',
    u_owner: '',
    u_user_display_name: '',
    u_is_private: false,
    u_description: '',
    u_module: '',
    u_read_roles: '',
    u_edit_roles: '',
    u_post_roles: '',
    u_im: '',
    u_im_pwd: '',
    u_im_display_name: '',
    u_email: '',
    u_send_to: '',
    u_receive_from: '',
    u_receive_pwd: '',
    u_email_display_name: '',

    //sys field
    id: '',
    sysCreatedOn: '',
    sysUpdatedOn: '',
    sysCreatedBy: '',
    sysUpdatedBy: '',
    sysOrg: '',

    u_editable: true,
    u_is_locked: false,

    u_schedule: '0 0 * * * ?',
    u_scheduleOptionsList: {
        mtype: 'store',
        fields: ['text', 'value'],
        data: [{
            text: '5 mins',
            value: '0 0/5 * * * ?'
        }, {
            text: '10 mins',
            value: '0 0/10 * * * ?'
        }, {
            text: '15 mins',
            value: '0 0/15 * * * ?'
        }, {
            text: '30 mins',
            value: '0 0/30 * * * ?'
        }, {
            text: '1 hr',
            value: '0 0 * * * ?'
        }, {
            text: '2 hr',
            value: '0 0 0/2 * * ?'
        }, {
            text: '4 hr',
            value: '0 0 0/4 * * ?'
        }, {
            text: '8 hr',
            value: '0 0 0/8 * * ?'
        }, {
            text: '24 hr',
            value: '0 0 0 * * ?'
        }]
    },

    isRSS$: function() {
        return this.name == 'rss'
    },

    u_urlIsVisible$: function() {
        return this.isRSS
    },

    u_ownerIsVisible$: function() {
        return !this.isRSS
    },

    u_rss_ownerIsVisible$: function() {
        return this.isRSS
    },

    u_scheduleIsVisible$: function() {
        return this.isRSS
    },

    mailingListValue: true,
    directValue: false,
    emailRadio: '',

    u_display_nameIsVisible$: function() {
        return !this.id || !this.isRSS
    },
    uDisplayName$: function() {
        return this.u_display_name
    },
    uDisplayNameIsVisible$: function() {
        return this.id && this.isRSS
    },

    hasGroup: false,

    fields: ['id', 'u_display_name', 'u_owner', 'u_description', 'u_is_private', 'u_is_locked',
        'u_im', 'u_im_pwd', 'u_im_display_name', 'u_send_to', 'u_receive_from', 'u_receive_pwd',
        'u_email_display_name', 'u_rss_owner', 'u_url', 'u_feed_updated', 'u_schedule', 'u_user_display_name', {
            name: 'u_editable',
            type: 'boolean'
        }, {
            name: 'hasGroup',
            type: 'boolean'
        },
        'sys_created_by', 'u_read_roles', 'u_edit_roles', 'u_post_roles', {
            name: 'sys_created_on',
            type: 'long'
        }, {
            name: 'sys_updated_on',
            type: 'long'
        }, 'sys_updated_by'
    ],

    init: function() {
        if (this.mock) this.backend = RS.socialadmin.createMockBackend(true);


        this.allComponentsGrid.on('beforeload', function(store, operation, eOpts) {
            operation.params = operation.params || {};
            Ext.apply(operation.params, {
                id: this.id,
                compType: this.name
            })
        }, this)
    },


    loadComponent: function() {
        this.allComponentsGrid.removeAll()
        if (this.id) {
            this.allComponentsGrid.load()
            this.set('wait', true);
            this.ajax({
                url: '/resolve/service/socialadmin/component/get',
                params: {
                    compType: this.name,
                    id: this.id
                },
				scope: this,
                success: function(resp) {
                    var respData = RS.common.parsePayload(resp);
                    if (!respData.success) {
                        clientVM.displayError(respData.message)
                        return
                    }
                    var data = respData.data;
                    this.loadData(data)
                    this.set('u_editable', data.u_editable)
                    RS.common.grid.updateSysFields(this, data)
                    this.processAccessRights(data)
                    this.accessRights.commitChanges()
                },
                failure: function(resp) {
					clientVM.displayFailure(resp);
				},
				callback: function() {
					this.set('wait', false);
				}
            })
        } else this.loadDefaultAccessRights()
    },

    resetForm: function() {
        this.accessRights.removeAll()
        this.loadData(this.defaultData)
        RS.common.grid.updateSysFields(this, this.defaultData)
        this.set('u_owner', clientVM.user.name)
        this.set('u_rss_owner', clientVM.user.name)
    },

    accessRights: {
        mtype: 'store',
        sorters: ['name'],
        fields: ['name', 'value', {
            name: 'edit',
            type: 'bool'
        }, {
            name: 'view',
            type: 'bool'
        }, {
            name: 'post',
            type: 'bool'
        }],
        proxy: {
            type: 'memory',
            reader: {
                type: 'json'
            }
        }
    },

    accessRightsSelections: [],

    addAccessRight: function() {
        var rights = [];

        this.accessRights.each(function(right) {
            rights.push(right.data);
        })

        this.open({
            mtype: 'AccessRightsPicker',
            currentRights: rights
        })
    },

    addRealAccessRights: function(accessRights) {
        Ext.each(accessRights, function(accessRight) {
            this.accessRights.add(Ext.applyIf(accessRight.data, {
                view: true,
                edit: false,
                post: true
            }))
        }, this)
    },

    removeAccessRightIsEnabled$: function() {
        return this.accessRightsSelections.length > 0
    },

    removeAccessRight: function() {
        for (var i = 0; i < this.accessRightsSelections.length; i++) {
            if (this.accessRightsSelections[i].data.name != 'admin') {
                this.accessRights.remove(this.accessRightsSelections[i])
            }
        }
    },

    processAccessRights: function(rsForm) {
        var viewRoles = rsForm.u_read_roles ? rsForm.u_read_roles.split(',') : [],
            editRoles = rsForm.u_edit_roles ? rsForm.u_edit_roles.split(',') : [],
            postRoles = rsForm.u_post_roles ? rsForm.u_post_roles.split(',') : [],
            accessRights = [],
            i;

        for (i = 0; i < viewRoles.length; i++)
            this.addToAccessRights(accessRights, viewRoles[i], 'view', true)
        for (i = 0; i < editRoles.length; i++)
            this.addToAccessRights(accessRights, editRoles[i], 'edit', true)
        for (i = 0; i < postRoles.length; i++)
            this.addToAccessRights(accessRights, postRoles[i], 'post', true)

        this.accessRights.removeAll()
        this.accessRights.add(accessRights)
    },

    pullAccessRightsToViewmodel: function() {
        //Add in access rights
        var viewRoles = [],
            editRoles = [],
            postRoles = [];
        this.accessRights.each(function(accessRight) {
            if (accessRight.data.view) viewRoles.push(accessRight.data.name)
            if (accessRight.data.edit) editRoles.push(accessRight.data.name)
            if (accessRight.data.post) postRoles.push(accessRight.data.name)
        })

        this.set('u_read_roles', viewRoles.join(','))
        this.set('u_edit_roles', editRoles.join(','))
        this.set('u_post_roles', postRoles.join(','))
    },

    pushAccessRightsToViewmodel: function() {
        this.processAccessRights({
            u_read_roles: this.u_read_roles,
            u_edit_roles: this.u_edit_roles,
            u_post_roles: this.u_post_roles
        })
    },

    addToAccessRights: function(accessRights, name, right, value) {
        if (Ext.String.trim(name).length > 0) {
            var i, len = accessRights.length;
            for (i = 0; i < len; i++) {
                if (accessRights[i].name == name) {
                    accessRights[i][right] = value
                    return
                }
            }

            var newName = {
                name: name,
                value: name
            }
            newName[right] = value
            accessRights.push(newName)
        }
    },

    setOwner: function(e) {
        this.open({
            mtype: 'RS.user.UserPicker',
            pick: true,
            callback: this.reallySetOwner
        })
    },
    reallySetOwner: function(selection) {
        if (selection.length > 0) {
            this.set('u_user_display_name', selection[0].get('udisplayName'))
            this.set('u_rss_owner', selection[0].get('uuserName'))
            this.set('u_owner', selection[0].get('uuserName'))
        }
    },

    userDateFormat$: function() {
        return clientVM.getUserDefaultDateFormat()
    },

    // This variable is used to validate GraphDB and SQLDB are in sync.
    validateSave: true,
    saveAndExitFlag: false,

    save: function() {
        if (this.task) this.task.cancel()
        this.task = new Ext.util.DelayedTask(this.saveComponent, this)
        this.task.delay(500)
        this.set('wait', true)
    },

    saveAndExit: function() {
        if (!this.saveIsEnabled)
            return;
        if (this.task) this.task.cancel()
        this.set('saveAndExitFlag', true)
        this.save()
    },

    refresh: function() {
        this.loadComponent()
    },

    saveComponent: function() {
        var socialDTO = {
            u_display_name: this.u_display_name,
            u_owner: this.u_owner,
            u_is_private: this.u_is_private,
            u_is_locked: this.u_is_locked,
            u_description: this.u_description,
            u_module: this.u_module,
            u_is_active: this.u_is_active,
            u_im: this.u_im,
            u_im_pwd: this.u_im_pwd,
            u_im_display_name: this.u_im_display_name,
            u_email: this.u_email,
            u_send_to: this.u_send_to,
            u_receive_from: this.u_receive_from,
            u_receive_pwd: this.u_receive_pwd,
            u_email_display_name: this.u_email_display_name,
            sys_created_on: this.sysCreatedOn,
            sys_created_by: this.sysCreatedBy,
            sys_updated_by: this.sysUpdatedBy,
            sys_updated_on: this.sysUpdatedOn,
            sys_id: this.id
        }

        if (this.isRSS) {
            socialDTO.u_schedule = this.u_schedule
            socialDTO.u_url = this.u_url
            socialDTO.u_rss_owner = this.u_rss_owner
        }

        if (socialDTO.sys_id == '') socialDTO.sys_id = null

        //Add in access rights
        var viewRoles = [],
            editRoles = [],
            postRoles = [];
        this.accessRights.each(function(accessRight) {
            if (accessRight.data.view) viewRoles.push(accessRight.data.name)
            if (accessRight.data.edit) editRoles.push(accessRight.data.name)
            if (accessRight.data.post) postRoles.push(accessRight.data.name)
        });

        socialDTO.u_read_roles = viewRoles.join(',')
        socialDTO.u_edit_roles = editRoles.join(',')
        socialDTO.u_post_roles = postRoles.join(',')

        this.set('wait', true)

        this.ajax({
            url: '/resolve/service/socialadmin/component/save',
            jsonData: socialDTO,
            params: {
                compType: this.name,
                validate: this.validateSave
            },
			scope: this,
            success: function(resp) {
                this.validateSave = true

                var respData = RS.common.parsePayload(resp);

                if (respData.success) {
                    var data = respData.data,
                        messageString;
                    this.loadData(data)
                    this.allComponentsGrid.load()
                    RS.common.grid.updateSysFields(this, data)
                    this.accessRights.commitChanges()

                    switch (this.name.toLowerCase()) {
                        case 'teams':
                            messageString = this.localize('teamSaved')
                            break;
                        case 'process':
                            messageString = this.localize('processSaved')
                            break;
                        case 'forums':
                            messageString = this.localize('forumSaved')
                            break;
                        case 'rss':
                            messageString = this.localize('rssSaved')
                            break;
                    }

                    clientVM.displaySuccess(messageString)

                    if (this.saveAndExitFlag) {
                        this.set('saveAndExitFlag', false)
                        clientVM.handleNavigation({
                            modelName: 'RS.socialadmin.Components',
                            params: {
                                name: this.name
                            }
                        })
                    }
                } else {
                    clientVM.displayError(respData.message)
                        // if (respData.message && respData.message.indexOf('unique') > 0) {
                        //     clientVM.displayError(respData.message);
                        // } else {
                        //     this.message({
                        //         title: '~~confirm~~',
                        //         // msg: 'Graph DB does not contain node for this component. Create a new node and save this component anyway?',
                        //         msg: respData.message,
                        //         buttons: Ext.MessageBox.YESNO,
                        //         buttonText: {
                        //             yes: '~~create~~',
                        //             no: '~~cancel~~'
                        //         },
                        //         scope: this,
                        //         fn: this.saveComponentWithNoValidation
                        //     })
                        // }
                }
            },
            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('wait', false);
			}
        }, this)
    },

    saveComponentWithNoValidation: function(button) {
        if (button === 'yes') {
            this.validateSave = false
            this.save()
        }
    },

    saveIsEnabled$: function() {
        var result = false
        if (!this.isRSS) {
            result = Ext.String.trim(this.u_display_name).length > 0 && (this.u_editable || !this.id)
        } else {
            result = Ext.String.trim(this.u_display_name).length > 0 &&
                Ext.String.trim(this.u_url).length > 0 &&
                Ext.String.trim(this.u_schedule).length > 0 && (this.u_editable || !this.id)
        }

        return result && this.isValid;
    },

    u_display_nameIsValid$: function() {
        var re = new RegExp(/^[\w -]+$/);
        return re.test(this.u_display_name) ? true : this.localize('invalidName')
    },

    u_urlIsValid$: function() {
        return !this.isRSS || (Ext.form.VTypes.url(this.u_url) && this.isRSS) ? true : Ext.form.VTypes.urlText
    },

    back: function() {
        clientVM.handleNavigation({
            modelName: 'RS.socialadmin.Components',
            params: {
                name: this.name
            }
        })
    },

    compTabPanelIsVisible$: function() {
        return !this.isRSS
    },

    emailConfigFormIsVisible$: function() {
        return !this.isRSS
    },

    instandMsgConfigFormIsVisible$: function() {
        return !this.isRSS
    },

    allComponentsGrid: {
        mtype: 'store',
        fields: ['id', 'compType', 'u_display_name', 'u_description'],
        groupField: 'compType',
        // remoteSort: true,
        sorters: [{
            property: 'u_display_name',
            direction: 'ASC'
        }],
        proxy: {
            type: 'ajax',
            url: '/resolve/service/socialadmin/container/components/get',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    allComponentsGridSelections: [],

    addComponent: function() {
        this.pullAccessRightsToViewmodel()
        this.open({
            mtype: 'RS.socialadmin.SelectComponents',
            name: this.name,
            id: this.id
        })
    },
    addComponentIsVisible$: function() {
        return this.activeTab == 1
    },

    addComponentIsEnabled$: function() {
        return this.id && this.u_editable
    },

    removeComponent: function() {
        // Removes components from container.
        var message = message = this.localize(this.allComponentsGridSelections.length == 1 ? 'RemoveCompMsg' : 'RemoveCompsMsg', {
                num: this.allComponentsGridSelections.length
            }),
            removeUser = false,
            removeOtherComps = false,
            userCount = 0;

        Ext.Array.forEach(this.allComponentsGridSelections, function(selection) {
            if (selection.data.compType == 'User') {
                removeUser = true
                userCount++
            } else
                removeOtherComps = true
        })

        if (this.hasGroup) {
            if (removeUser && removeOtherComps) {
                message += '<br/>' + this.localize(userCount === 1 ? 'removeUserAndComponentMessage' : 'removeUsersAndComponentMessage')
            } else if (removeUser) {
                message += '<br/>' + this.localize(userCount === 1 ? 'removeUserMessage' : 'removeUsersMessage')
            } else {
                message += '<br/>' + this.localize(this.allComponentsGridSelections.length == 1 ? 'RemoveCompMsg' : 'RemoveCompsMsg', {
                    num: this.allComponentsGridSelections.length
                })
            }
        }

        this.message({
            title: this.localize('removeComponents'),
            msg: message,
            buttons: Ext.MessageBox.YESNO,
            buttonText: {
                yes: this.localize('remove'),
                no: this.localize('cancel')
            },
            scope: this,
            fn: this.reallyRemoveComponents
        })
    },

    removeComponentsIsVisible$: function() {
        return this.activeTab == 1
    },

    removeComponentIsVisible$: function() {
        return this.activeTab == 1
    },

    reallyRemoveComponents: function(button) {
        if (button === 'yes') {
            var compsToRemove = [];

            Ext.Array.forEach(this.allComponentsGridSelections, function(selection) {
                // if (selection.data.u_display_name != this.u_owner) {
                var component = {
                    compType: selection.data.compType.replace(/ /, ''),
                    id: selection.data.id,
                    u_display_name: selection.data.u_display_name
                }
                compsToRemove.push(component)
                    // }
            }, this)

            this.ajax({
                url: '/resolve/service/socialadmin/container/components/remove',
                jsonData: compsToRemove,
                params: {
                    containerType: this.name,
                    containerId: this.id
                },
                success: function(response) {
                    var respData = RS.common.parsePayload(response);

                    if (respData.success) {
                        if(respData.message)
                            clientVM.displayError(respData.message)
                        else 
                            clientVM.displaySuccess(this.allComponentsGridSelections.length === 1 ? this.localize('componentRemoved') : this.localize('componentsRemoved'))
                        this.allComponentsGrid.load()
                    } else
                        clientVM.displayError(respData.message)
                },
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
            }, this)
        }
    },

    removeComponentIsEnabled$: function() {
        return this.allComponentsGridSelections.length > 0 && this.u_editable
    },

    activeTab: 0,

    generalInfoTab: function() {
        this.set('activeTab', 0)
    },

    generalInfoTabIsPressed$: function() {
        return this.activeTab == 0
    },

    componentTab: function() {
        this.set('activeTab', 1)
    },

    componentTabIsPressed$: function() {
        return this.activeTab == 1
    },

    componentTabIsVisible$: function() {
        return !this.isRSS
    },

    generalInfoTabIsVisible$: function() {
        return !this.isRSS
    },

    allComponentsGridIsVisible$: function() {
        return !this.isRSS
    }
})
glu.defModel('RS.socialadmin.Components', {

    name: '',
    mock: false,
    socialTableName: '',

    wait: false,

    activate: function(screens, params) {
        this.set('name', params ? params.name : '')
        switch (this.name) {
            case 'process':
                this.set('socialTableName', 'social_process')
                this.set('displayName', this.localize('processes'))
                clientVM.setWindowTitle(this.localize('socialProcesses'))
                break;
            case 'teams':
                this.set('socialTableName', 'social_team')
                this.set('displayName', this.localize('teams'))
                clientVM.setWindowTitle(this.localize('socialTeams'))
                break;
            case 'forums':
                this.set('socialTableName', 'social_forum')
                this.set('displayName', this.localize('forums'))
                clientVM.setWindowTitle(this.localize('socialForums'))
                break;
            default:
                this.set('socialTableName', 'rss_subscription')
                this.set('displayName', this.localize('rss'))
                clientVM.setWindowTitle(this.localize('socialRSS'))
                break;
        }
        this.components.load();
    },

    displayName: '',
    rssDisplayName: 'RSS',

    componentStateId: 'ComponentsList',
    rssStateId: 'RSSList',

    components: {
        mtype: 'store',
        remoteSort: true,
        sorters: [{
            property: 'u_display_name',
            direction: 'ASC'
        }],
        fields: ['u_display_name', {
            name: 'u_editable',
            type: 'boolean'
        }, 'u_user_display_name', 'u_description', 'u_owner', {
            name: 'u_is_locked',
            type: 'boolean'
        }].concat(RS.common.grid.getSysFields()),

        proxy: {
            type: 'ajax',
            url: '/resolve/service/socialadmin/component/list',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    rssComponentsStore: {
        mtype: 'store',
        remoteSort: true,
        sorters: [{
            property: 'u_display_name',
            direction: 'DESC'
        }],
        fields: ['u_display_name', {
            name: 'u_is_locked',
            type: 'boolean'
        }, 'u_user_display_name', 'u_description', 'u_url', 'u_schedule', 'u_rss_owner', {
            name: 'u_feed_updated',
            type: 'date',
            format: 'time',
            dateFormat: 'time'
        }].concat(RS.common.grid.getSysFields()),

        proxy: {
            type: 'ajax',
            url: '/resolve/service/socialadmin/component/list',
            reader: {
                type: 'json',
                root: 'records'
            },
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
        }
    },

    componentsGridSelections: [],
    rssComponentsGridSelections: [],

    componentColumns: [],
    rssComponentColumns: [],

    allSelected: false,

    cronTimeConversionMap: {
        '0 0/5 * * * ?': '5 mins',
        '0 0/10 * * * ?': '10 mins',
        '0 0/15 * * * ?': '15 mins',
        '0 0/30 * * * ?': '30 mins',
        '0 0 * * * ?': '1 hr',
        '0 0 0/2 * * ?': '2 hr',
        '0 0 0/4 * * ?': '4 hr',
        '0 0 0/8 * * ?': '8 hr',
        '0 0 0 * * ?': '24 hr'
    },

    init: function() {
        if (this.mock)
            this.backend = RS.socialadmin.createMockBackend(true)

        var me = this;

        this.set('componentColumns', [{
            header: '~~name~~',
            dataIndex: 'u_display_name',
            filterable: true,
            sortable: true,
            width: 220
        }, {
            header: '~~locked~~',
            dataIndex: 'u_is_locked',
            filterable: false,
            sortable: false,
            width: 60,
            align: 'center',
            renderer: RS.common.grid.booleanRenderer()
        }, {
            header: '~~owner~~',
            dataIndex: 'u_owner',
            filterable: true,
            sortable: true,
            width: 120,
            renderer:function(v,meta,r){
                return r.get('u_user_display_name');
            }
        }, {
            header: '~~description~~',
            dataIndex: 'u_description',
            filterable: true,
            sortable: true,
            flex: 1
        }].concat(RS.common.grid.getSysColumns()))

        this.set('rssComponentColumns', [{
            header: '~~name~~',
            dataIndex: 'u_display_name',
            filterable: true,
            sortable: true,
            width: 220
        }, {
            header: '~~u_url~~',
            dataIndex: 'u_url',
            filterable: true,
            sortable: true,
            width: 80,
            flex: 1
        }, {
            header: '~~locked~~',
            dataIndex: 'u_is_locked',
            filterable: false,
            sortable: false,
            width: 60,
            align: 'center',
            renderer: RS.common.grid.booleanRenderer()
        }, {
            header: '~~u_schedule~~',
            dataIndex: 'u_schedule',
            filterable: true,
            sortable: true,
            width: 120,
            renderer: function(value, metaData, record, row, col, store, gridView) {
                return me.cronTimeConversionMap[value]
            }
        }, {
            header: '~~u_rss_owner~~',
            dataIndex: 'u_rss_owner',
            filterable: true,
            sortable: true,
            width: 120,
            renderer:function(v,meta,r) {
                return r.get('u_user_display_name');
            }
        }, {
            header: '~~u_feed_updated~~',
            dataIndex: 'u_feed_updated',
            filterable: true,
            sortable: true,
            width: 160,
            renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
        }].concat(RS.common.grid.getSysColumns()))

        this.components.on('beforeload', function(store, operation, eOpts) {
            this.set('wait', true);
            operation.params = operation.params || {}
            Ext.apply(operation.params, {
                socialTableName: this.socialTableName
            })
        }, this)

        this.components.on('load', function(store, records, suc) {
            this.set('wait', false)
            if (!suc)
                clientVM.displayError(this.localize('ListErrMsg'));
        }, this)

        this.rssComponentsStore.on('beforeload', function(store, operation, eOpts) {
            this.set('wait', true);
            operation.params = operation.params || {};
            Ext.apply(operation.params, {
                socialTableName: this.socialTableName
            })
        }, this);

        this.rssComponentsStore.on('load', function(store, records, suc) {
            this.set('wait', false)
            if (!suc)
                clientVM.displayError(this.localize('ListErrMsg'))
        }, this);
    },

    componentsGridIsVisible$: function() {
        return Ext.Array.indexOf(['process', 'teams', 'forums'], this.name) > -1
    },

    rssComponentsGridIsVisible$: function() {
        return this.name == 'rss'
    },

    selectAllAcrossPages: function(selectAll) {
        this.set('allSelected', selectAll)
    },
    selectionChanged: function() {
        this.selectAllAcrossPages(false)
    },

    deleteComponents: function() {
        //Delete the selected record(s), but first confirm the action
        this.message({
            title: this.localize('deleteTitle'),
            msg: this.localize(this.componentsGridSelections.length == 1 ? 'DeleteCompMsg' : 'DeleteCompsMsg', {
                num: this.componentsGridSelections.length
            }),
            buttons: Ext.MessageBox.YESNO,
            buttonText: {
                yes: this.localize('deleteAction'),
                no: this.localize('cancel')
            },
            scope: this,
            fn: this.reallyDeleteRecords
        })
    },

    deleteRssComponents: function() {
        //Delete the selected record(s), but first confirm the action
        this.message({
            title: this.localize('deleteTitle'),
            msg: this.localize(this.rssComponentsGridSelections.length == 1 ? 'DeleteCompMsg' : 'DeleteCompsMsg', {
                num: this.rssComponentsGridSelections.length
            }),
            buttons: Ext.MessageBox.YESNO,
            buttonText: {
                yes: this.localize('deleteAction'),
                no: this.localize('cancel')
            },
            scope: this,
            fn: this.reallyDeleteRecords
        })
    },

    deleteComponentsIsEnabled$: function() {
        return this.componentsGridSelections.length > 0 && !this.wait;
    },

    deleteRssComponentsIsEnabled$: function() {
        return this.rssComponentsGridSelections.length > 0 && !this.wait;
    },

    reallyDeleteRecords: function(button) {
        //Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
        if (button == 'yes') {
            this.set('wait', true)
            var selectedComponents = [];

            if (this.name == 'rss')
                selectedComponents = this.rssComponentsGridSelections
            else
                selectedComponents = this.componentsGridSelections

            if (this.allSelected) {
                this.ajax({
                    url: '/resolve/service/socialadmin/component/delete',
                    params: {
                        ids: '',
                        whereClause: (this.name == 'rss' ? this.rssComponentsStore.lastWhereClause : this.components.lastWhereClause) || '1=1',
                        compType: this.name
                    },
                    scope: this,
                    success: function(r) {
                        this.handleDeleteSuccessResponse(r)
                    },
                    failure: function(resp) {
                        clientVM.displayFailure(resp);
                    },
					callback: function() {
						this.set('wait', false)
					}
                })
            } else {
                var ids = []
                Ext.each(selectedComponents, function(selection) {
                    ids.push(selection.get('id'))
                })
                this.ajax({
                    url: '/resolve/service/socialadmin/component/delete',
                    params: {
                        ids: ids.join(','),
                        compType: this.name
                    },
                    scope: this,
                    success: function(r) {
                        this.handleDeleteSuccessResponse(r)
                    },
                    failure: function(r) {
                        clientVM.displayFailure(r);
                    },
					callback: function() {
						this.set('wait', false)
					}
                })
            }
        }
    },

    handleDeleteSuccessResponse: function(r) {
        var response = RS.common.parsePayload(r);
        if (response.success)
            clientVM.displaySuccess(this.localize('deleteSuccessMessage'))
        else
            clientVM.displayError(response.message)

        if (this.name == 'rss')
            this.rssComponentsStore.load()
        else
            this.components.load()
    },

    createComponent: function() {
        clientVM.handleNavigation({
            modelName: 'RS.socialadmin.Component',
            params: {
                name: this.name
            }
        })
    },

    editComponent: function(id) {
        clientVM.handleNavigation({
            modelName: 'RS.socialadmin.Component',
            params: {
                id: id,
                name: this.name
            }
        })
    }
})
glu.defModel('RS.socialadmin.CreateComponent', {

	wait: false,

	title$: function() {
		if (this.type == 'forum') return this.localize('createForum')
		if (this.type == 'rss') return this.localize('createRss')
		if (this.type == 'process') return this.localize('createProcess')
		if (this.type == 'team') return this.localize('createTeam')
		return this.localize('createComponentTitle')
	},

	type: '',
	typeStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: 'json'
		}
	},

	socialTypes: {
		rss: 'rss',
		team: 'teams',
		forum: 'forums',
		process: 'process'
	},

	name: '',
	nameIsValid$: function() {
		var re = new RegExp(/^[\w -]+$/);
		return re.test(this.name) || this.localize('invalidName')
	},

	description: '',
	url: '',
	urlIsVisible$: function() {
		return this.type == 'rss'
	},
	urlIsValid$: function() {
		return Ext.form.VTypes.url(this.url) || Ext.form.VTypes.urlText
	},

	accessRights: {
		mtype: 'store',
		sorters: ['name'],
		fields: ['name', 'value', {
			name: 'edit',
			type: 'bool'
		}, {
			name: 'view',
			type: 'bool'
		}, {
			name: 'post',
			type: 'bool'
		}],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	componentsGrid: {
		mtype: 'store',
		fields: ['id', 'uuserName', 'u_description'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	init: function() {
		this.typeStore.add([{
			name: this.localize('rss'),
			value: 'rss'
		}, {
			name: this.localize('forum'),
			value: 'forum'
		}, {
			name: this.localize('team'),
			value: 'team'
		}, {
			name: this.localize('process'),
			value: 'process'
		}])
		this.typeStore.sort('name')

		if (Ext.Array.indexOf(['rss', 'forum', 'team', 'process'], this.type) == -1)
			this.set('type', this.typeStore.getAt(0).get('value'))

		this.loadDefaultAccessRights()

		this.memberStore.on('beforeload', function(store, operation) {
			operation.params = operation.params || {}

			Ext.apply(operation.params, {
				parentName: this.socialTypes[this.type],
				parentId: '',
				tableName: 'Users',
				modelName: 'Users',
				whereClause: this.memberSearch
			})
		}, this)
	},

	loadDefaultAccessRights: function() {
		this.set('wait', true)
		this.ajax({
			url: '/resolve/service/socialadmin/defaultAccessRights/get',
			params: {
				compType: this.socialTypes[this.type]
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				this.processAccessRights(response.data)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false)
			}
		})
	},

	processAccessRights: function(rsForm) {
		var viewRoles = rsForm.u_read_roles ? rsForm.u_read_roles.split(',') : [],
			editRoles = rsForm.u_edit_roles ? rsForm.u_edit_roles.split(',') : [],
			postRoles = rsForm.u_post_roles ? rsForm.u_post_roles.split(',') : [],
			accessRights = [],
			i;

		for (i = 0; i < viewRoles.length; i++)
			this.addToAccessRights(accessRights, viewRoles[i], 'view', true)
		for (i = 0; i < editRoles.length; i++)
			this.addToAccessRights(accessRights, editRoles[i], 'edit', true)
		for (i = 0; i < postRoles.length; i++)
			this.addToAccessRights(accessRights, postRoles[i], 'post', true)

		this.accessRights.removeAll()
		this.accessRights.add(accessRights)
	},

	addToAccessRights: function(accessRights, name, right, value) {
		var i, len = accessRights.length;
		for (i = 0; i < len; i++) {
			if (accessRights[i].name == name) {
				accessRights[i][right] = value
				return
			}
		}

		var newName = {
			name: name,
			value: name
		}
		newName[right] = value
		accessRights.push(newName)
	},

	create: function() {
		var socialDTO = {
			u_display_name: this.name,
			u_owner: clientVM.user.name,
			u_is_locked: false,
			u_description: this.description,
			sys_id: null
		};

		if (this.type == 'rss') {
			socialDTO.u_schedule = '0 0 * * * ?' //default 1hr
			socialDTO.u_url = this.url
			socialDTO.u_rss_owner = clientVM.user.name
		}

		//Add in access rights
		var viewRoles = [],
			editRoles = [],
			postRoles = [];
		this.accessRights.each(function(accessRight) {
			if (accessRight.data.view) viewRoles.push(accessRight.data.name)
			if (accessRight.data.edit) editRoles.push(accessRight.data.name)
			if (accessRight.data.post) postRoles.push(accessRight.data.name)
		})

		socialDTO.u_read_roles = viewRoles.join(',')
		socialDTO.u_edit_roles = editRoles.join(',')
		socialDTO.u_post_roles = postRoles.join(',')

		this.set('wait', true)

		this.ajax({
			url: '/resolve/service/socialadmin/component/save',
			jsonData: socialDTO,
			params: {
				compType: this.socialTypes[this.type],
				validate: false
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r),
					messageString;

				if (response.success) {
					switch (this.type.toLowerCase()) {
						case 'team':
							messageString = this.localize('teamSaved')
							break;
						case 'process':
							messageString = this.localize('processSaved')
							break;
						case 'forum':
							messageString = this.localize('forumSaved')
							break;
						case 'rss':
							messageString = this.localize('rssSaved')
							break;
					}

					if (this.componentsGrid.getCount() > 0) {
						this.persistMembers(response.data, messageString)
					} else {
						clientVM.displaySuccess(messageString)
						this.doClose()
					}
				} else {
					clientVM.displayError(response.message)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false)
			}
		}, this)
	},
	createIsEnabled$: function() {
		return !this.wait && this.nameIsValid === true
	},
	cancel: function() {
		this.doClose()
	},

	memberSearch: '',
	memberStore: {
		mtype: 'store',
		fields: ['id', 'sys_id', 'uname', 'uuserName', 'ufirstName', 'ulastName', 'unamespace', 'u_description', 'usummary', 'u_display_name', 'u_read_roles'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/socialadmin/addcomponent/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	persistMembers: function(data, msg) {
		var compsToAdd = [];
		this.componentsGrid.each(function(selection) {
			compsToAdd.push({
				compType: 'Users',
				sys_id: selection.get('id'),
				u_display_name: selection.get('uuserName')
			})
		}, this)

		this.ajax({
			url: '/resolve/service/socialadmin/container/components/add',
			jsonData: compsToAdd,
			params: {
				containerType: this.socialTypes[this.type],
				containerId: data.id,
				addUsersToGroup: false,
				validate: false
			},

			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					clientVM.displaySuccess(msg)
					this.doClose()
				} else {
					clientVM.displayError(response.message)
					return
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		}, this)
	},

	addUser: function() {
		//Add the highlighted user to the list of members
		var record = this.memberStore.getById(this.memberSearch)
		if (record) {
			this.componentsGrid.add({
				id: record.get('id'),
				uuserName: record.get('uuserName'),
				u_description: record.get('ufirstName') + ' ' + record.get('ulastName')
			})
		}
	},
	addUserIsEnabled$: function() {
		return this.memberSearch && this.memberStore.getById(this.memberSearch)
	}
})
glu.defModel('RS.socialadmin.SelectComponents', {

	activate: function() {},

	windowTitle$: function() {
		return this.localize('addComponents')
	},

	name: '',

	componentsGrid: {
		mtype: 'store',
		sorters: ['uname'],
		fields: ['id', 'sys_id', 'name', 'uname', 'uuserName', 'ufirstName', 'ulastName', 'unamespace', 'u_description', 'usummary', 'u_display_name', 'u_read_roles', 'ufullname'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/socialadmin/addcomponent/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	componentsGridSelections: [],

	componentsGridColumns: [],

	type: 'Users',
	typeOptionsList: {
		mtype: 'store',
		sorters: ['componentName'],
		fields: ['componentName', 'componentValue'],
		proxy: {
			type: 'memory'
		}
	},

	searchQuery: '',

	allSelected: false,
	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},

	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	init: function() {
		this.typeOptionsList.removeAll()

		if (this.name == 'process') {
			this.typeOptionsList.add([{
				componentName: this.localize('users'),
				componentValue: 'Users',
				type: 'String'
			}, {
				componentName: this.localize('documents'),
				componentValue: 'WikiDocument',
				type: 'String'
			}, {
				componentName: this.localize('runbooks'),
				componentValue: 'Runbook',
				type: 'String'
			}, {
				componentName: this.localize('actionTasks'),
				componentValue: 'ResolveActionTask',
				type: 'String'
			}, {
				componentName: this.localize('teams'),
				componentValue: 'social_team',
				type: 'String'
			}, {
				componentName: this.localize('forums'),
				componentValue: 'social_forum',
				type: 'String'
			}, {
				componentName: this.localize('rss'),
				componentValue: 'rss_subscription',
				type: 'String'
			}, {
				componentName: 'Namespace',
				componentValue: 'namespace',
				type: 'String'
			}])
		} else if (this.name == 'teams') {
			this.typeOptionsList.add([{
				componentName: this.localize('users'),
				componentValue: 'Users',
				type: 'String'
			}, {
				componentName: this.localize('teams'),
				componentValue: 'social_team',
				type: 'String'
			}])
		} else if (this.name == 'forums') {
			this.typeOptionsList.add([{
				componentName: this.localize('users'),
				componentValue: 'Users',
				type: 'String'
			}])
		}

		this.componentsGrid.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};

			Ext.apply(operation.params, {
				tableName: this.type,
				modelName: this.type,
				whereClause: this.searchQuery,
				parentName: this.name,
				parentId: this.id
			})
		}, this)

		this.componentsGrid.load()
	},

	when_filter_changes_reload_grid: {
		on: ['typeChanged', 'searchQueryChanged'],
		action: function() {
			this.componentsGrid.loadPage(1)
		}
	},

	search: function() {
		this.componentsGrid.load()
	},

	compsToAdd: [],
	add: function() {
		/*
        	Check if there's a group whose name is same as that of this Team.
        	hasGroup flag will be true if this Team is linked to the group.
        	So, before adding a user to team check if above condition is true.
        	If yes, prompt current user to add the selected users to group as well.
        */
		if (this.parentVM.hasGroup && this.name == 'teams' && this.type == 'Users') {
			this.message({
				title: this.componentsGridSelections.length === 1 ? this.localize('addUserToGroup') : this.localize('addUsersToGroup'),
				msg: this.componentsGridSelections.length === 1 ? this.localize('addUserToGroupMessage') : this.localize('addUsersToGroupMessage'),
				buttons: Ext.MessageBox.YESNO,
				buttonText: {
					yes: this.componentsGridSelections.length === 1 ? this.localize('addUserToGroup') : this.localize('addUsersToGroup'),
					no: this.localize('cancel')
				},
				scope: this,
				fn: this.addUsers
			})
		} else {
			this.addComponents()
		}

		this.doClose()
	},

	addUsersToGroup: false,

	addUsers: function(button) {
		if (button == 'yes') {
			this.set('addUsersToGroup', true)
			this.addComponents()
		}
	},

	validateComponent: true,

	addComponents: function() {
		var compsToAdd = [];
		Ext.Array.forEach(this.componentsGridSelections, function(selection) {
			compsToAdd.push({
				compType: this.type,
				sys_id: selection.data.sys_id,
				u_display_name: selection.data.uuserName || selection.data.u_display_name ||  selection.data.ufullname || selection.data.name
			})
		}, this)

		this.ajax({
			url: '/resolve/service/socialadmin/container/components/add',
			jsonData: compsToAdd,
			params: {
				containerType: this.name,
				containerId: this.parentVM.id,
				addUsersToGroup: this.addUsersToGroup,
				validate: this.validateComponent
			},

			success: function(r) {
				var response = RS.common.parsePayload(r);

				if (response.success) {
					clientVM.displaySuccess(compsToAdd.length === 1 ? this.localize('componentAdded') : this.localize('componentsAdded'))
					this.parentVM.allComponentsGrid.load()
					this.validateComponent = true
				} else {
					// this.message({
					// 	title: 'Confirm',
					// 	// msg: 'Graph DB does not contain node for this component. Create a new node and add this component anyway',
					// 	msg: response.message,
					// 	buttons: Ext.MessageBox.YESNO,
					// 	buttonText: {
					// 		yes: 'Yes',
					// 		no: 'No'
					// 	},
					// 	scope: this,
					// 	fn: this.addCompsWithNoValidation
					// })
					clientVM.displayError(response.message)
					return
				}

				// If the component which is getting added is Team(s), then only check for roles.
				if (compsToAdd[0].compType == 'social_team') {
					this.checkTeamReadRoles()
				} else this.doClose()
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}

		}, this)
	},

	addCompsWithNoValidation: function(button) {
		if (button === 'yes') {
			this.validateComponent = false;
			this.addComponents()
		}
	},

	checkTeamReadRoles: function() {
		var showDialog = false,
			role, parentRoles = this.parentVM.u_read_roles.split(',');
		Ext.Array.each(this.componentsGridSelections, function(selection) {
			roles = selection.get('u_read_roles').split(',')
			Ext.Array.each(roles, function(role) {
				if (Ext.Array.indexOf(parentRoles, role) == -1) {
					showDialog = true
					return false
				}
			}, this)
			if (showDialog) return false
		}, this)

		if (showDialog) {
			this.message({
				title: this.localize('addRoleConfirmation'),
				msg: this.name == 'process' ? this.localize('addRolesToProcess') : this.localize('addRolesToTeam'),
				buttons: Ext.MessageBox.YESNO,
				buttonText: {
					yes: this.localize('addRoles'),
					no: this.localize('cancel')
				},
				scope: this,
				fn: this.updateCurrentTeamsRoles
			})
		} else this.doClose()
	},

	updateCurrentTeamsRoles: function(button) {
		if (button === 'yes') {
			var parentRoles = this.parentVM.u_read_roles.split(',');
			Ext.Array.forEach(this.componentsGridSelections, function(selection) {
				roles = selection.get('u_read_roles').split(',')
				Ext.Array.forEach(roles, function(role) {
					if (Ext.Array.indexOf(parentRoles, role) == -1) {
						parentRoles.push(role)
					}
				}, this)
			}, this)
			this.parentVM.set('u_read_roles', parentRoles.join(','))
			this.parentVM.pushAccessRightsToViewmodel()
			this.parentVM.saveComponent()
		}
		this.doClose()
	},

	addIsEnabled$: function() {
		return this.componentsGridSelections.length > 0
	},

	cancel: function() {
		this.doClose()
	}
})
glu.defView('RS.socialadmin.AccessRightsPicker', {
	height: 300,
	width: 500,

	title: '~~addAccessRights~~',

	layout: 'fit',
	items: [{
		xtype: 'grid',
		forceFit: true,
		name: 'roles',
		columns: [{
			header: 'Name',
			dataIndex: 'name'
		}]
	}],
	buttonAlign: 'left',
	buttons: [{
		name: 'addAccessRightsAction'
	}, {
		name: 'cancel'
	}]
});
glu.defView('RS.socialadmin.AddComponent', {
    layout: 'fit',
    items: [{
        xtype: 'grid',
        name: 'addComponentGridStore',
        border: false,
        stateId: '@{stateId}',
        displayName: '@{formDisplayName}',
        stateful: true,
        plugins: [{
            ptype: 'searchfilter',
            allowPersistFilter: false,
            hideMenu: true
        }, {
            ptype: 'pager'
        }],
        selModel: {
            selType: 'resolvecheckboxmodel',
            columnTooltip: RS.common.locale.editColumnTooltip,
            columnTarget: '_self',
            columnEventName: 'editAction',
            columnIdField: 'sys_id',
            childLinkIdProperty: 'sys_id'
        },
        columns: '@{AddComponentColumns}',
        viewConfig: {
            enableTextSelection: true
        }
        // ,
        // listeners: {
        //     editAction: '@{editComponent}',
        //     selectAllAcrossPages: '@{selectAllAcrossPages}',
        //     selectionChange: '@{selectionChanged}'
        // }
    }],
    buttonAlign: 'left',
    buttons: [{
        name: 'selectComponent',
        text: 'Select'
    }, {
        name: 'cancel',
        text: 'Cancel'
    }],
    asWindow: {
        width: 800,
        height: 480
        //title:'@{title}'
    }
});
glu.defView('RS.socialadmin.Component', {
	xtype: 'panel',
	layout: 'card',
	padding: 15,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls : 'rs-dockedtoolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{title}'
		}, '->', {
			name: 'generalInfoTab'
		}, {
			name: 'componentTab',
			text: '@{componentTabName}'
		}],
		margin : '0 0 15 0'
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light',
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, {
			xtype: 'tbseparator',
			hidden: '@{!addComponentIsVisible}'
		}, 'addComponent', 'removeComponent', '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrg}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	autoScroll: true,
	activeItem: '@{activeTab}',
	items: [{
		layout: 'border',
		minWidth: 650,
		minHeight: 500,
		items: [{
			region: 'center',
			minWidth: 350,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				bodyPadding: '10 5 0 0'
			},
			items: [{
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaults: {
					labelWidth: 135
				},
				items: [{
					xtype: 'textfield',
					name: 'u_display_name'
				}, {
					xtype: 'displayfield',
					name: 'uDisplayName'
				}, {
					xtype: 'triggerfield',
					triggerCls: 'x-form-search-trigger',
					name: 'u_user_display_name',
					onTriggerClick: function() {
						this.fireEvent('focus', this)
					},
					listeners: {
						focus: '@{setOwner}'
					}
				}, {
					xtype: 'textfield',
					name: 'u_url'
				}, {
					xtype: 'combobox',
					name: 'u_schedule',
					editable: false,
					queryMode: 'local',
					displayField: 'text',
					valueField: 'value',
					store: '@{u_scheduleOptionsList}'
				}, {
					xtype: 'textarea',
					name: 'u_description'
				}, {
					xtype: 'checkbox',
					name: 'u_is_locked'
				}]
			}, {
				title: '~~emailConfiguration~~',
				defaultType: 'textfield',
				hidden: '@{isRSS}',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaults: {
					labelWidth: 135
				},
				items: ['u_send_to', 'u_receive_from', {
					name: 'u_receive_pwd',
					inputType: 'password'
				}, 'u_email_display_name', {
					xtype: 'radiogroup',
					anchor: 'none',
					layout: {
						autoFlex: false
					},
					defaults: {
						name: 'emailRadio'
					},
					items: [{
						xtype: 'radio',
						boxLabel: '~~mailingList~~',
						value: '@{mailingListValue}'
					}, {
						xtype: 'radio',
						boxLabel: '~~direct~~',
						value: '@{directValue}',
						padding: '0px 0px 0px 100px'
					}]
				}]
			}, {
				border: false,
				title: '~~instantMessagingConfiguration~~',
				defaultType: 'textfield',
				hidden: '@{isRSS}',
				defaults: {
					labelWidth: 135
				},
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: ['u_im', {
					name: 'u_im_pwd',
					inputType: 'password'
				}, 'u_im_display_name']
			}]
		}, {
			xtype: 'grid',
			region: 'east',
			name: 'accessRights',
			width: 300,
			padding: '0 0 0 5',

			title: '~~accessRights~~',
			tbar: [{
				name: 'addAccessRight'
			}, {
				name: 'removeAccessRight'
			}],
			columns: [{
				header: '~~name~~',
				dataIndex: 'name',
				hideable: false,
				flex: 1,
				editor: {
					allowBlank: false
				}
			}, {
				xtype: 'checkcolumn',
				stopSelection: false,
				header: '~~editRight~~',
				dataIndex: 'edit',
				hideable: false,
				width: 70
			}, {
				xtype: 'checkcolumn',
				stopSelection: false,
				header: '~~viewRight~~',
				dataIndex: 'view',
				hideable: false,
				width: 70
			}, {
				xtype: 'checkcolumn',
				stopSelection: false,
				header: '~~postRight~~',
				dataIndex: 'post',
				hideable: false,
				width: 70
			}]
		}]
	}, {
		xtype: 'grid',
		name: 'allComponentsGrid',
		columns: [{
			text: '~~compType~~',
			dataIndex: 'compType'
		}, {
			text: '~~name~~',
			dataIndex: 'u_display_name',
			width: 300
		}, {
			text: '~~description~~',
			dataIndex: 'u_description',
			flex: 1,
			renderer: 'htmlEncode'
		}],

		features: [{
			ftype: 'grouping',
			groupHeaderTpl: '{name} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})',
			hideGroupedHeader: true,
			startCollapsed: false
		}],
		listeners: {
			groupclick: function(grid, field, value, e) {
				if (e.getTarget().tagName == 'INPUT') {
					e.stopPropagation();
					e.stopEvent()
				}
				var t = e.getTarget('.grpCheckbox');
				if (t) {
					var checked = t.checked;
					grid.store.data.items.forEach(function(record, index) {
						if (record.data.compType == value) {
							if (checked) {
								grid.getSelectionModel().select(index, true);
							} else {
								grid.getSelectionModel().deselectRow(index);
							}
						}
					})
				}
			}
		}
	}]
})

glu.defView('RS.socialadmin.Components', {
    layout: 'fit',
    padding: 15,
	cls : 'rs-grid-dark',
    items: [{
        xtype: 'grid',
        name: 'componentsGrid',
        store: '@{components}',
        stateId: '@{componentStateId}',
        columns: '@{componentColumns}',
        displayName: '@{displayName}',
        setDisplayName: function(value) {
            this.getPlugin('searchfilter').toolbar.down('#tableMenu').setText(value)
        },
        stateful: true,
        plugins: [{
            ptype: 'searchfilter',
            pluginId: 'searchfilter',
            allowPersistFilter: false,
            hideMenu: true
        }, {
            ptype: 'pager'
        }],
        selModel: {
            selType: 'resolvecheckboxmodel',
            columnTooltip: RS.common.locale.editColumnTooltip,
            columnTarget: '_self',
            columnEventName: 'editAction',
            columnIdField: 'id'
        },
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            name: 'actionBar',
			cls: 'actionBar rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light',
			},
            items: ['createComponent', 'deleteComponents']
        }],

        viewConfig: {
            enableTextSelection: true
        },
        listeners: {
            editAction: '@{editComponent}',
            selectAllAcrossPages: '@{selectAllAcrossPages}',
            selectionChange: '@{selectionChanged}'
        }
    }, {
        xtype: 'grid',
        name: 'rssComponentsGrid',
        store: '@{rssComponentsStore}',
        stateId: '@{rssStateId}',
        displayName: '@{rssDisplayName}',
        columns: '@{rssComponentColumns}',
        stateful: true,
        plugins: [{
            ptype: 'searchfilter',
            allowPersistFilter: false,
            hideMenu: true
        }, {
            ptype: 'pager',
            notActivateCtrl_R: true
        }],
        selModel: {
            selType: 'resolvecheckboxmodel',
            columnTooltip: RS.common.locale.editColumnTooltip,
            columnTarget: '_self',
            columnEventName: 'editAction',
            columnIdField: 'id',
            childLinkIdProperty: 'id'
        },
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            cls: 'actionBar',
            name: 'actionBar',
			cls: 'actionBar rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light',
			},
            items: ['createComponent', 'deleteRssComponents']
        }],

        viewConfig: {
            enableTextSelection: true
        },
        listeners: {
            editAction: '@{editComponent}',
            selectAllAcrossPages: '@{selectAllAcrossPages}',
            selectionChange: '@{selectionChanged}'
        }
    }]
});
glu.defView('RS.socialadmin.CreateComponent', {
	asWindow: {
		title: '@{title}',
		height: 400,
		width: 600,
		modal: true
	},
	bodyPadding: '10px',
	buttonAlign: 'left',
	buttons: ['create', 'cancel'],
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'combobox',
		name: 'type',
		store: '@{typeStore}',
		displayField: 'name',
		valueField: 'value',
		queryMode: 'local'
	}, {
		xtype: 'textfield',
		name: 'name'
	}, {
		xtype: 'textarea',
		name: 'description'
	}, {
		xtype: 'textfield',
		name: 'url'
	}, {
		xtype: 'grid',
		name: 'componentsGrid',
		title: '~~members~~',
		flex: 1,
		minHeight: 200,
		tbar: [{
			xtype: 'combobox',
			name: 'memberSearch',
			hideLabel: true,
			store: '@{memberStore}',
			displayField: 'uuserName',
			valueField: 'id',
			minChars: 1
		}, {
			iconCls: 'icon-plus icon-large rs-icon',
			name: 'addUser',
			tooltip: '~~addUser~~',
			text: ''
		}],
		columns: [{
			text: '~~name~~',
			dataIndex: 'uuserName',
			flex: 1
		}, {
			text: '~~description~~',
			dataIndex: 'u_description',
			flex: 1
		}, {
			xtype: 'actioncolumn',
			width: 50,
			items: [{
				icon: '/resolve/images/closex.gif',
				// iconCls: 'icon-large icon-remove rs-icon',
				tooltip: 'Remove',
				handler: function(grid, rowIndex, colIndex) {
					grid.getStore().removeAt(rowIndex)
				}
			}]
		}]
	}]
})
glu.defView('RS.socialadmin.SelectComponents', {
	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'componentsGrid',
		columns: '@{componentsGridColumns}',
		selType: 'checkboxmodel',
		listeners: {
			selectionChange: '@{selectionChanged}'
		},
		plugins: [{
			ptype: 'pager',
			showSysInfo: false
		}],
		dockedItems: [{
			name: 'actionBar',
			xtype: 'toolbar',
			items: [{
				xtype: 'combobox',
				name: 'type',
				emptyText: '~~type~~',
				hideLabel: true,
				displayField: 'componentName',
				valueField: 'componentValue',
				queryMode: 'local',
				store: '@{typeOptionsList}',
				width: 160,
				correctColumns: function(newValue) {
					var parentGrid = this.up('grid'),
						gridColumns = [];
					parentGrid.findPlugin('pager').store.currentPage = 1
					if (newValue == 'Users') {
						gridColumns.push({
							text: RS.socialadmin.locale.name,
							dataIndex: 'uuserName',
							filterable: 'true',
							width: 200
						}, {
							text: RS.socialadmin.locale.firstName,
							dataIndex: 'ufirstName',
							filterable: 'true',
							width: 300
						}, {
							text: RS.socialadmin.locale.lastName,
							dataIndex: 'ulastName',
							filterable: 'true',
							flex: 1
						})
					} else if (newValue == 'WikiDocument' || newValue == 'Runbook') {
						gridColumns.push({
							text: RS.socialadmin.locale.name,
							dataIndex: 'uname',
							filterable: 'true',
							width: 200
						}, {
							text: RS.socialadmin.locale.namespace,
							dataIndex: 'unamespace',
							filterable: 'true',
							width: 200
						}, {
							text: RS.socialadmin.locale.summary,
							dataIndex: 'usummary',
							filterable: 'true',
							flex: 1,
							renderer: 'htmlEncode'
						})
					} else if (newValue == 'ResolveActionTask') {
						gridColumns.push({
							text: RS.socialadmin.locale.name,
							dataIndex: 'uname',
							filterable: 'true',
							width: 200
						}, {
							text: RS.socialadmin.locale.namespace,
							dataIndex: 'unamespace',
							filterable: 'true',
							width: 200
						}, {
							text: RS.socialadmin.locale.summary,
							dataIndex: 'usummary',
							filterable: 'true',
							flex: 1
						})
					} else if (newValue == 'social_team' || newValue == 'social_forum' || newValue == 'rss_subscription') {
						gridColumns.push({
							text: RS.socialadmin.locale.name,
							dataIndex: 'u_display_name',
							filterable: 'true',
							width: 200
						}, {
							text: RS.socialadmin.locale.description,
							dataIndex: 'u_description',
							filterable: 'true',
							flex: 1
						})
					} else if (newValue == 'namespace') {
						gridColumns.push({
							text: RS.socialadmin.locale.name,
							dataIndex: 'name',
							filterable: 'true',
							flex: 1
						})
					}

					parentGrid.reconfigure(null, gridColumns)
				},
				listeners: {
					change: function(combo, newValue) {
						combo.correctColumns(newValue)
					},
					render: function(combo) {
						combo.correctColumns(combo.getValue())
					}
				}
			}, {
				xtype: 'triggerfield',
				name: 'searchQuery',
				hideLabel: true,
				flex: 1,
				emptyText: '~~searchQuery~~',
				triggerCls: 'x-form-search-trigger',
				onTriggerClick: function() {
					this.fireEvent('search', this)
				},
				listeners: {
					search: '@{search}'
				}
			}]
		}]
	}],
	buttonAlign: 'left',
	buttons: ['add', 'cancel'],

	asWindow: {
		title: '@{windowTitle}',
		modal: true,
		width: 800,
		height: 400
	}
})
/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.socialadmin').locale = {

    cancel: 'Cancel',
    save: 'Save',
    back: 'Back',

    name: 'Name',
    editable: 'Editable',
    locked: 'Locked',
    owner: 'Owner',
    description: 'Description',

    userId: 'User Id',
    firstName: 'First Name',
    lastName: 'Last Name',
    users: 'Users',

    AccessRights: 'Access Rights',
    addAccessRights: 'Add Access Rights',
    addAccessRightsAction: 'Add',
    addAccessRight: 'Add',
    removeAccessRight: 'Remove',
    viewRight: 'View',
    postRight: 'Post',
    editRight: 'Admin',

    addComponent: 'Add',
    removeComponent: 'Remove',

    processes: 'Processes',
    socialProcesses: 'Social Processes',
    forums: 'Forums',
    socialForums: 'Social Forums',
    teams: 'Teams',
    socialTeams: 'Social Teams',
    rss: 'RSS',
    socialRSS: 'Social RSS',

    accessRightsTab: 'Access Rights',
    messagingTab: 'Messaging',

    u_url: 'URL',
    u_schedule: 'Schedule',
    u_feed_updated: 'Feed Updated On',
    u_rss_owner: 'Owner',
    u_active: 'Active',
    u_user_display_name: 'Owner',

    addComponents: 'Add Components',

    deleteTitle: 'Delete Components',
    deleteAction: 'Delete',
    DeleteCompsMsg: 'Are you sure you want to delete the {num} selected items?',
    DeleteCompMsg: 'Are you sure you want to delete the selected item?',
    DeleteSucMsg: 'The selected items have been deleted',
    createComponent: 'New',
    deleteComponents: 'Delete',
    deleteRssComponents: 'Delete',

    u_owner: 'Owner',
    u_display_name: 'Name',
    uDisplayName: 'Name',
    u_description: 'Description',
    RemoveCompsMsg: 'Are you sure you want to remove the {num} selected items?',
    RemoveCompMsg: 'Are you sure you want to remove the selected item?',
    u_is_private: 'Is Private',
    u_is_locked: 'Is Locked',

    u_send_to: 'Send To',
    u_receive_from: 'Receive From',
    u_receive_pwd: 'Receive Password',
    u_email_display_name: 'Email Display Name',

    u_im: 'IM Account',
    u_im_pwd: 'IM Password',
    u_im_display_name: 'IM Display Name',

    emailConfiguration: 'Email Configuration',
    accessRights: 'Access Rights',
    mailingList: 'Mailing List',
    direct: 'Direct',
    instantMessagingConfiguration: 'Instant Messaging Configuration',
    invalidName: 'The name is required and should only contains alphabets and digits.',
    actionTasks: 'Action Tasks',
    documents: 'Documents',
    runbooks: 'Runbooks',

    addUser: 'Add User',
    addDuplicateUser: 'Add Duplicate User',

    deleteSuccessMessage: 'Records successfully deleted',

    teamSaved: 'Team successfully saved',
    processSaved: 'Process successfully saved',
    forumSaved: 'Forum successfully saved',
    rssSaved: 'RSS successfully saved',
    generalInfoTab: 'Properties',
    components: 'Components',
    members: 'Members',
    team: 'Team',
    Forum: 'Forum',
    process: 'Process',

    type: 'Select Type',
    searchQuery: 'Search...',
    add: 'Add',
    close: 'Close',

    rssSaved: 'RSS successfully saved',

    user: 'User',
    namespace: 'Namespace',
    summary: 'Summary',

    addUserToGroupMessage: 'This team is linked to a group, add the selected user to the group as well?',
    addUsersToGroupMessage: 'This team is linked to a group, add the selected users to the group as well?',
    addUsersToGroup: 'Add users to Group',
    addUserToGroup: 'Add user to Group',

    addRoleConfirmation: 'Confirm add role',
    addRolesToProcess: 'Would you like to add the roles from the selected teams to the current Process?',
    addRolesToTeam: 'Would you like to add roles from the selected teams to the current Team?',
    addRoles: 'Add Roles',

    forum: 'Forum',

    componentAdded: 'Component successfully added',
    componentsAdded: 'Components successfully added',

    componentsRemoved: 'Components successfully removed',
    componentRemoved: 'Component successfully removed',

    removeUserAndComponentMessage: 'This team is linked to a group. Selected user will also be removed from group.',
    removeUsersAndComponentMessage: 'This team is linked to a group. Selected users will also be removed from group.',


    removeComponents: 'Remove Components',
    remove: 'Remove',

    removeUserMessage: 'This team is linked to a group. Removed the user from group as well?',
    removeUsersMessage: 'This team is linked to a group. Removed the users from group as well?',

    ListErrMsg: 'Unable to retrieve components',

    CreateComponent: {
        createComponentTitle: 'Create Component',
        createRss: 'Create RSS',
        createForum: 'Create Forum',
        createProcess: 'Create Process',
        createTeam: 'Create Team',

        create: 'Create',

        url: 'URL',
        invalidName: 'Please provide a name for this component',

        teamSaved: 'Team successfully saved',
        processSaved: 'Process successfully saved',
        forumSaved: 'Forum successfully saved',
        rssSaved: 'RSS successfully saved',

        addUser: 'Add the user to the list of members'
    },

    addRoleConfirmation: 'Confirm add role',
    addRolesToProcess: 'Would you like to add the roles from the selected teams to the current Process?',
    addRolesToTeam: 'Would you like to add roles from the selected teams to the current Team?',
    addRoles: 'Add Roles',
    cancel: 'Cancel',
    compType: 'Component Type'

}
