glu.defView('RS.user.Organizations', {
	xtype: 'grid',
	padding: 15,
	cls : 'rs-grid-dark',
	displayName: '~~organizationDisplayName~~',
	store : '@{organizations}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		childLink: 'RS.user.Organization',
		childLinkIdProperty: 'id'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls :'rs-small-btn rs-btn-light'
		},
		items: ['createOrganization']
	}],
	columns: [{
			header: '~~uname~~',
			dataIndex: 'uname',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~udescription~~',
			dataIndex: 'udescription',
			filterable: false,
			sortable: false,
			flex: 1
		}, {
			header: '~~groups~~',
			dataIndex: 'groups',
			filterable: false,
			sortable: false,
			flex: 1
	}].concat(RS.common.grid.getSysColumns())	
});