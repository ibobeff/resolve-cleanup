glu.defView('RS.user.Group', {
	autoScroll: true,
	padding: 15,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		margin : '0 0 15 0',
		cls: 'title-toolbar rs-dockedtoolbar',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{groupTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',		
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],

	items: [{
		style : 'border-top:1px solid silver',
		padding : '8 0 0 0',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},		
		items: [{
			flex: 1,
			margin : {
				right : 10
			},
			defaults : {
				margin : '4 0'
			},
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				name: 'uname',
				xtype: 'textfield'
			}, {
				name: 'udescription',
				xtype: 'textarea',
				height : 150
			}]
		}, {
			flex: 1,		
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				name: 'uisLinkToTeam',
				xtype: 'checkbox',
				hideLabel: true,
				boxLabel: '~~linkToTeam~~',
				padding: '0px 0px 0px 105px',
				tooltip: '~~linkToTeamTooltip~~',
				listeners: {
					render: function(checkbox) {
						Ext.create('Ext.tip.ToolTip', {
							html: checkbox.tooltip,
							target: checkbox.getEl()
						})
					}
				}
			}, {
				name: 'uhasExternalLink',
				xtype: 'checkbox',
				hideLabel: true,
				boxLabel: '~~linkToExternalGroup~~',
				padding: '0px 0px 0px 105px',
				tooltip: '~~linkToExternalGroupTooltip~~',
				listeners: {
					render: function(checkbox) {
						Ext.create('Ext.tip.ToolTip', {
							html: checkbox.tooltip,
							target: checkbox.getEl()
						})
					}
				}
			}]
		}]
	},{
		xtype : 'container',
		flex : 1,
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		defaults : {
			maxHeight : 300
		},
		items : [{
			title: '~~roles~~',
			xtype: 'grid',
			flex : 1,
			cls: '@{rolesCls}',
			name: 'roles',
			columns:  [{
				header: '~~sysId~~',
				dataIndex: 'id',
				hidden: true,
				autoWidth: true,
				sortable: false,
				filterable: false
			}, {
				header: '~~uname~~',
				dataIndex: 'uname',
				filterable: true,
				sortable: true,
				flex: 1
			}],
			dockedItems : [{
				xtype : 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items : ['addRole', 'removeRole']
			}],
			qtip: '@{noRolesError}',
			listeners: {
				afterrender: function(form) {
					//Configure proper tooltip on the title
					if (form.getHeader()) {
						var headerElem = form.getHeader().getEl();
						var toolTipElem = headerElem.down('.x-header-text.x-panel-header-text');
						Ext.create('Ext.tip.ToolTip', {
							cls: 'group-roles-tip',
							target: toolTipElem,
							html: this.qtip,
							autoShow: true,
							listeners: {
								boxready: function() {
									this.hide();
								}
							}
						})
					}
				},
			}
		}, {
			title: '~~users~~',
			xtype: 'grid',
			flex : 1,
			margin : '0 10',
			cls : 'rs-grid-dark',		
			name: 'users',		
			dockedItems : [{
				xtype : 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items : ['addUser', 'removeUser']
			}],
			columns: [{
				header: '~~userId~~',
				dataIndex: 'uuserName',
				filterable: true,
				sortable: true,
				flex: 1
			}, {
				header: '~~uname~~',
				dataIndex: 'ufirstName',
				filterable: true,
				sortable: true,
				flex: 1,
				renderer: function(value, meta, record) {
					return (value || '') + ' ' + (record.get('ulastName') || '')
				}
			}, {
				header: '~~email~~',
				dataIndex: 'uemail',
				filterable: true,
				sortable: true,
				flex: 1
			}]	
		},{
			title: '~~organizations~~',
			xtype: 'grid',
			flex : 1,		
			cls : 'rs-grid-dark',		
			name: 'organizations',
			dockedItems : [{
				xtype : 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items : ['addOrganization', 'removeOrganization']
			}],
			columns: [{
				header: '~~uname~~',
				dataIndex: 'uname',
				filterable: true,
				sortable: true,
				flex: 1
			}]	
		}]
	}]
});
