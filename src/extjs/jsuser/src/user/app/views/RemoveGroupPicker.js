glu.defView('RS.user.RemoveGroupPicker', {
	title: '~~removeGroupPickerTitle~~',
	width: 600,
	height: 400,
	padding : 15,
	modal : true,
	cls : 'rs-modal-popup',
	layout: 'fit',
	items: [{
		xtype: 'grid',
		margin : {
			bottom : 10
		},
		cls : 'rs-grid-dark',
		name: 'groups',
		columns: '@{columns}',
		selModel: {
			mode: 'MULTI'
		}
	}],	
	buttons: [{
		name : 'remove',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name :'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})