glu.defView('RS.user.Groups', {
	xtype: 'grid',
	padding: 15,
	name: 'groups',
	cls : 'rs-grid-dark',
	border: false,
	stateId: '@{stateId}',
	displayName: '@{displayName}',
	stateful: true,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		childLink: 'RS.user.Group',
		childLinkIdProperty: 'id'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls :'rs-small-btn rs-btn-light'
		},
		items: ['createGroup', 'copyGroup', 'deleteGroup']
	}],
	columns: '@{columns}',
	viewConfig: {
		enableTextSelection: true
	},
	listeners: {
		editAction: '@{editGroup}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});