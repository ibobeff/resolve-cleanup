glu.defView('RS.user.User', {
	padding: 15,
	layout: 'card',
	activeItem: '@{activeTab}',
	dockedItems: [{
		xtype: 'toolbar',
		margin : '0 0 10 0',
		ui: 'display-toolbar',
		cls: 'rs-dockedtoolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{userTitle}'
		}, '->', {
			name: 'blogTab',
			pressed: '@{blogTabIsPressed}'
		}, {
			name: 'adminTab',
			pressed: '@{adminTabIsPressed}'
		}, {
			name: 'settingsTab',
			pressed: '@{settingsTabIsPressed}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls :'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, {
			xtype: 'tbseparator',
			hidden: '@{!resetBrowserPreferencesIsVisible}'
		}, 'resetBrowserPreferences', '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			disabled: '@{!refreshIsEnabled}',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	defaults: {
		overflowY: 'auto',
	},
	items: [{
		style : 'border-top:1px solid silver',
		layout: 'border',
		items: [{
			region: 'west',
			split: true,
			width: 210,
			autoScroll: true,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				layout: {
					type: 'vbox',
					align: 'center'
				},
				items: [{
					xtype: 'image',
					cls: 'profile-pic @{isMeCls}',
					src: '@{profilePicUrl}',
					maxWidth: 100,
					maxHeight: 100,
					tooltip: '~~clickToChangeProfile~~',
					isMe: '@{isMe}',
					setIsMe: function(value) {
						this.isMe = value
						if (this.isMe) {
							if (!this.tooltipControl)
								this.tooltipControl = Ext.create('Ext.tip.ToolTip', {
									html: this.tooltip,
									target: this.getEl()
								})
						} else {
							if (this.tooltipControl) {
								this.tooltipControl.remove()
								this.tooltipControl.destroy()
								this.tooltipControl = null
							}
						}
					},
					listeners: {
						render: function(img) {
							img.getEl().on('click', function() {
								img.fireEvent('imageClicked', img)
							})
							img.getEl().on('load', function() {
								img.ownerCt.doLayout()
							})

							if (img.isMe)
								img.tooltipControl = Ext.create('Ext.tip.ToolTip', {
									html: img.tooltip,
									target: img.getEl()
								})
						},
						imageClicked: '@{imageClicked}'
					}
				}]
			}, {
				// html: '10 posts & comments | 20 comments received | 100 likes received',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [{
					html: '@{contributionsText}',
					bodyStyle: {
						'text-align': 'center'
					},
					flex: 1
				}, {
					html: '@{commentsReceivedText}',
					bodyStyle: {
						'text-align': 'center'
					},
					flex: 1
				}, {
					html: '@{likesReceivedText}',
					bodyStyle: {
						'text-align': 'center'
					},
					flex: 1
				}]
			}, {
				title: '~~contact~~',
				bodyPadding: '10px',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [{
					xtype: 'displayfield',
					htmlEncode: false,
					value: '@{uemailDisplay}',
					style: {
						overflow: 'ellipsis'
					},
					hidden: '@{!uemailHasValue}'
				}, {
					xtype: 'displayfield',
					htmlEncode: false,
					value: '@{uhomePhoneDisplay}',
					hidden: '@{!uhomePhoneHasValue}'
				}, {
					xtype: 'displayfield',
					htmlEncode: false,
					value: '@{umobilePhoneDisplay}',
					hidden: '@{!umobilePhoneHasValue}'
				}, {
					xtype: 'displayfield',
					htmlEncode: false,
					value: '@{ufaxDisplay}',
					hidden: '@{!ufaxHasValue}'
				}, {
					xtype: 'displayfield',
					htmlEncode: false,
					value: '@{uimDisplay}',
					hidden: '@{!uimHasValue}'
				}]
			}, {
				title: '~~summary~~',
				bodyPadding: '10px',
				html: '@{usummary}',
				style: 'word-wrap:break-word;',
				listeners: {
					render: function(panel) {
						panel.fireEvent('registerSummary', panel, panel)
					},
					registerSummary: '@{registerSummaryPanel}'
				}

			}]
		}, {
			region: 'center',
			xtype: '@{socialDetail}'
		}, {
			region: 'east',
			width: 200,
			split: true,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				title: '~~followers~~',
				flex: 1,
				layout: 'fit',
				bodyPadding: '10px',
				items: [{
					autoScroll: true,
					xtype: 'dataview',
					tpl: '@{followersTpl}',
					itemSelector: 'div.thumb-wrap',
					emptyText: '~~noFollowers~~',
					store: '@{followers}'
				}]
			}, {
				title: '~~following~~',
				flex: 1,
				layout: 'fit',
				bodyPadding: '10px',
				items: [{
					autoScroll: true,
					xtype: 'dataview',
					tpl: '@{followingTpl}',
					itemSelector: 'div.thumb-wrap',
					emptyText: '~~noFollowing~~',
					store: '@{following}'
				}]
			}]
		}]
	}, {
		style : 'border-top:1px solid silver',
		padding : '8 0 0 0',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaults : {
				margin : {
					right : 10
				}
			},
			items: [{
				flex: 1,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaultType: 'textfield',
				defaults: {
					labelWidth: 130,
					margin : '4 0'
				},
				items: [{
					name: 'uuserName',
					hidden: '@{isEdit}'
				}, {
					xtype: 'displayfield',
					cls :'rs-displayfield-value',
					value: '@{uuserName}',
					fieldLabel: '~~uuserName~~',
					hidden: '@{!isEdit}',
					htmlEncode : true
				},{
					xtype : 'fieldcontainer',
					fieldLabel : '~~fullName~~',
					labelWidth : 130,
					layout : {
						type : 'hbox',
						align : 'stretch'
					},
					defaultType :'textfield',
					defaults : {
						hideLabel : true,
						flex : 1
					},
					items : [{
						name :'ufirstName',
						emptyText : '~~ufirstName~~',
						margin : '0 8 0 0'
					},{
						name : 'ulastName',
						emptyText : '~~ulastName~~'
					}]
				},{
					name: 'utitle'
				}, {
					name: 'uuserPassword',
					inputType: 'password',
					selectOnFocus: true,
					hidden: '@{isEdit}'
				}, {
					name: 'uuserPasswordConfirm',
					inputType: 'password',
					selectOnFocus: true,
					hidden: '@{isEdit}'
				}, {
					xtype: 'checkbox',
					name: 'upasswordNeedsReset',
					margin: '4 0 4 135',
					hideLabel: true,
					boxLabel: '~~upasswordNeedsReset~~'
				}, {
					xtype: 'checkbox',
					name: 'ulockedOut',
					margin: '0 0 0 135',
					hideLabel: true,
					boxLabel: '~~ulockedOut~~'
				}, {
					xtype: 'fieldcontainer',
					hidden: '@{!isEdit}',
					fieldLabel: ' ',
					labelSeparator: '',
					items: [{
						xtype: 'button',
						cls: 'rs-btn-dark rs-small-btn',
						name: 'changePassword'
					}]
				}]
			}, {
				flex: 1,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaults : {
					labelWidth : 130,
					margin : '4 0'
				},
				defaultType: 'textfield',
				items: [{
					xtype: 'htmleditor',
					name: 'usummary',
					enableSourceEdit: false
				}, {
					xtype: 'combo',
					editable: false,
					forceSelection: true,
					name: 'ustartPage',
					store: '@{startPageStore}',
					displayField: 'name',
					valueField: 'value',
					queryMode: 'local'
				}, {
					name: 'uhomePage'
				}]
			}]
		}, {
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaults : {
				margin : {
					right : 10
				}
			},
			items: [{
				title: '~~contactInfo~~',
				bodyPadding : '4 0 0 0',
				flex: 1,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaultType: 'textfield',
				defaults: {
					labelWidth: 130,
					margin : '4 0'
				},
				items: [{
					name: 'uemail'
				}, {
					name: 'uhomePhone'
				}, {
					name: 'umobilePhone'
				}, {
					name: 'ufax'
				}, {
					name: 'uim'
				}, {
					xtype: 'fieldcontainer',
					fieldLabel: '~~address~~',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					margin : 0,
					defaults : {
						margin : '4 0'
					},
					items: [{
						xtype: 'textfield',
						name: 'ustreet',
						emptyText: '~~ustreet~~',
						hideLabel: true
					}, {
						xtype: 'fieldcontainer',
						layout: {
							type: 'hbox',
							align: 'stretch'
						},
						items: [{
							xtype: 'textfield',
							hideLabel: true,
							flex: 1,
							name: 'ucity',
							emptyText: '~~ucity~~',
							padding: '0px 8px 0px 0px'
						}, {
							xtype: 'textfield',
							hideLabel: true,
							flex: 1,
							name: 'ustate',
							emptyText: '~~ustate~~'
						}]
					}, {
						xtype: 'fieldcontainer',
						layout: {
							type: 'hbox',
							align: 'stretch'
						},
						items: [{
							xtype: 'textfield',
							hideLabel: true,
							flex: 1,
							name: 'uzip',
							emptyText: '~~uzip~~',
							padding: '0px 8px 0px 0px'
						}, {
							xtype: 'combo',
							hideLabel: true,
							flex: 1,
							name: 'ucountry',
							store: '@{countryStore}',
							displayField: 'name',
							valueField: 'value',
							queryMode: 'local',
							emptyText: '~~ucountry~~'
						}]
					}]
				}]
			}, {
				title: '~~recoveryOptions~~',
				bodyPadding : '4 0 0 0',
				hidden: '@{!showRecoveryOptions}',
				flex: 1,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaultType: 'textfield',
				defaults : {
					margin : '4 0',
					labelWidth : 130
				},
				items: [{
					itemId: 'uquestion1',
					name: 'uquestion1',
					readOnly: '@{recoveryOptionsRO}'
				}, {
					name: 'uanswer1',
					readOnly: '@{recoveryOptionsRO}'
				}, {
					name: 'uquestion2',
					readOnly: '@{recoveryOptionsRO}'
				}, {
					name: 'uanswer2',
					readOnly: '@{recoveryOptionsRO}'
				}, {
					name: 'uquestion3',
					readOnly: '@{recoveryOptionsRO}'
				}, {
					name: 'uanswer3',
					readOnly: '@{recoveryOptionsRO}'
				}, {
					xtype: 'fieldcontainer',
					hidden: '@{!recoveryOptionsRO}',
					fieldLabel: ' ',
					labelSeparator: '',
					items: [{
						xtype: 'button',
						cls: 'rs-btn-dark rs-small-btn',
						name: 'resetRecoveryOptions',
						listeners: {
							render: function() {
								var uquestion1 = this.up().up().down('#uquestion1');
								this.getEl().on('click', function() {
									uquestion1.focus();
								});
							}
						}
					}]
				}]
			}, {
				hidden: '@{showRecoveryOptions}',
				flex: 1
			}]
		},
			// DISABLING THE USER CUSTOM FIELDS AS THERE WILL BE MORE OPERATIONS INVOLVED IN DOING THAT.
			// {
			// 	title: '~~customFields~~',
			// 	hidden: '@{!showCustomFields}',
			// 	layout: 'fit',
			// 	items: [{
			// 		xtype: 'rsform',
			// 		formName: 'RS_USERCUSTOMFIELDS',
			// 		embed: true,
			// 		hideToolbar: true,
			// 		showLoading: false,
			// 		recordId: '@{id}',
			// 		listeners: {
			// 			formLoaded: '@{formLoaded}'
			// 		}
			// 	}]
			// },
		{
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			minHeight: 200,
			flex : 1,
			defaults : {
				cls : 'rs-grid-dark',
				flex : 1,
				autoScroll : true,
				margin : '0 10 0 0',
			},
			items : [{
				xtype: 'grid',
				title: '~~roles~~',
				cls: '@{rolesGroupsCls}',
				name: 'roles',
				roleHide: true,
				roles: ['admin'],
				columns: [{
					text: '~~name~~',
					dataIndex: 'uname',
					flex: 1
				}],
				dockedItems : [{
					xtype : 'toolbar',
					cls : 'rs-dockedtoolbar',
					defaults : {
						cls :'rs-small-btn rs-btn-light'
					},
					items : ['addRole', 'removeRole']
				}],
				qtip: '@{noRoleOrGroupError}',
				listeners: {
					afterrender: function(form) {
						//Configure proper tooltip on the title
						if (form.getHeader()) {
							var headerElem = form.getHeader().getEl();
							var toolTipElem = headerElem.down('.x-header-text.x-panel-header-text');
							Ext.create('Ext.tip.ToolTip', {
								cls: 'user-roles-groups-tip',
								target: toolTipElem,
								html: this.qtip,
								autoShow: true,
								listeners: {
									boxready: function() {
										this.hide();
									}
								}
							})
						}
					},
				}
			}, {
				xtype: 'grid',
				title: '~~groups~~',
				name: 'groups',
				roleHide: true,
				roles: ['admin'],
				cls: '@{rolesGroupsCls}',
				columns: [{
					text: '~~name~~',
					dataIndex: 'uname',
					flex: 1
				}],
				dockedItems : [{
					xtype : 'toolbar',
					cls : 'rs-dockedtoolbar',
					defaults : {
						cls :'rs-small-btn rs-btn-light'
					},
					items :  ['addGroup', 'removeGroup']
				}],
				qtip: '@{noRoleOrGroupError}',
				listeners: {
					afterrender: function(form) {
						//Configure proper tooltip on the title
						if (form.getHeader()) {
							var headerElem = form.getHeader().getEl();
							var toolTipElem = headerElem.down('.x-header-text.x-panel-header-text');
							Ext.create('Ext.tip.ToolTip', {
								cls: 'user-roles-groups-tip',
								target: toolTipElem,
								html: this.qtip,
								autoShow: true,
								listeners: {
									boxready: function() {
										this.hide();
									}
								}
							})
						}
					},
				}
			},{
				xtype: 'grid',
				title: '~~organizations~~',
				name: 'organizations',
				roleHide: true,
				roles: ['admin'],
				margin : 0,
				viewConfig : {
					markDirty : false
				},
				selModel : {
					mode : 'SINGLE'
				},
				columns: [{
					text: '~~name~~',
					dataIndex: 'uname',
					flex: 1
				},{
					text : '~~default~~',
					dataIndex : 'isDefaultOrg',
					width : 80,
					align : 'center',
					renderer: RS.common.grid.booleanRenderer()
				}],
				dockedItems : [{
					xtype : 'toolbar',
					cls : 'rs-dockedtoolbar',
					defaults : {
						cls :'rs-small-btn rs-btn-light'
					},
					items :  ['setDefaultOrg']
				}]
			}]
		}]
	}, {
		cls : 'rs-panel-header',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		autoScroll: true,
		style : 'border-top:1px solid silver',
		defaults : {
			bodyPadding : '5 0 0 0',
			margin : '25 0 0 0'
		},
		items: [{
			title: '~~timeSettings~~',
			defaults: {
				labelWidth: 210
			},
			items: [{
				xtype: 'combobox',
				width: 500,
				name: 'userDefaultDateFormat',
				autoSelect: false,
				store: '@{userDefaultDateFormatStore}',
				displayField: 'name',
				valueField: 'value',
				queryMode: 'local'
			}]
		}, {
			title: '~~gridSettings~~',
			defaults: {
				labelWidth: 210
			},
			items: [{
				xtype: 'checkbox',
				name: 'autoRefreshEnabled',
			}, {
				xtype: 'numberfield',
				name: 'autoRefreshInterval',
				width: 500,
				minValue: 10
			}]
		}, {
			title: '~~editorSettings~~',
			defaults: {
				labelWidth: 210
			},
			items: [{
				xtype: 'combobox',
				editable: false,
				name: 'aceKeybinding',
				store: '@{aceKeybindingStore}',
				displayField: 'name',
				valueField: 'value',
				queryMode: 'local',
				width: 500
			}]
		}, {
			title: '~~menuSettings~~',
			defaults: {
				labelWidth: 210,
				width: 500
			},
			items: [{
				xtype: 'numberfield',
				name: 'maxRecentlyUsed',
				minValue: -1
			},{
				xtype: 'combo',
				name: 'sideMenuDisplay',
				store : '@{menuDisplayOptionStore}',
				displayField : 'text',
				valueField : 'value',
				editable : false,
			}]
		}, {
			title: '~~displaySettings~~',
			defaults: {
				labelWidth: 210
			},
			items: [{
				xtype: 'checkbox',
				name: 'bannerIsVisible'
			}]
		}]
	}]
});

glu.defView('RS.user.ChangePasswordForm', {
	xtype: 'form',
	title: '~~changePassword~~',
	cls : 'rs-modal-popup',
	padding : 15,
	modal: true,
	width: 550,
	height: 220,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaultType: 'textfield',
	defaults: {
		labelWidth: 150,
		allowBlank: false
	},
	items: [{
		name: 'uuserOldPassword',
		hidden: '@{!currentUser}',
		inputType: 'password',
		selectOnFocus: true
	},{
		name: 'uuserPassword',
		itemId: 'uuserPassword',
		fieldLabel: '~~newPassword~~',
		inputType: 'password',
		selectOnFocus: true
	}, {
		name: 'uuserPasswordConfirm',
		itemId: 'uuserPasswordConfirm',
		inputType: 'password',
		selectOnFocus: true
	}],
	buttons: [{
		name :'apply',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});