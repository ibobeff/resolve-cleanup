glu.defView('RS.user.Organization', {
	autoScroll: true,
	padding: 15,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		margin : '0 0 15 0',
		cls: 'title-toolbar rs-dockedtoolbar',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{organizationTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',		
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],

	items: [{
		style : 'border-top:1px solid silver',
		padding : '8 0 0 0',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			flex: 1,
			margin : {
				right : 10
			},
			defaults : {
				margin : '4 0',
				labelWidth : 150
			},
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				name: 'uname',
				xtype: 'textfield',
				maxLength: 200,
				enforceMaxLength: true
			}, {
				name: 'udescription',
				xtype: 'textarea',
				height : 150
			},{
				name : 'parentOrgId',
				store : '@{parentOrgStore}',
				maxWidth : 550,
				defaultValue : 'None',
				editable : false,
				xtype : 'combo',
				displayField : 'uname',
				valueField: 'id',
				queryMode: 'local'
			},{
				name : 'uhasNoOrgAccess',
				xtype : 'checkbox',
				hideLabel : true,
				boxLabel : '~~uhasNoOrgAccess~~',
				margin : '4 0 4 155'
			}]
		}]
	}, {
		title: '~~groups~~',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		minHeight: 400,		
		name: 'groups',
		columns: [{
			header: '~~sysId~~',
			dataIndex: 'id',
			hidden: true,
			autoWidth: true,
			sortable: false,
			filterable: false
		}, {
			header: '~~uname~~',
			dataIndex: 'uname',
			filterable: true,
			sortable: true,
			flex: 1
		}],
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items : ['addGroup', 'removeGroup']
		}]	
	}]
});
