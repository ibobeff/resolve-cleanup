glu.defView('RS.user.ProfileUpload', {
	asWindow: {
		//height: '@{windowHeight}',
		//width: '@{windowWidth}',
		height: 500,
		width: 800,
		modal: true,
		padding : 15,
		cls : 'rs-modal-popup',
		resizable: false,
		draggable: false,
		title: '~~uploadProfilePicTitle~~',
		closable: true,
		listeners: {
			show: function(win) {
				win.fireEvent('setupSizes', win, win)
			},
			setupSizes: '@{setupSizes}'
		}
	},

	layout: 'card',
	activeItem: '@{activeItem}',
	items: [{
		margin: '0 0 10 0',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			html: '@{intro}',
			margin: '0 0 10 0',
		}, {
			itemId: 'fineUploaderTemplate',
			html: '@{fineUploaderTemplate}',
			height: '@{fineUploaderTemplateHeight}',
			hidden: '@{fineUploaderTemplateHidden}',
		}, {
			id: 'attachments_grid',
			hidden: true
		}]
	},{
		style: {
			'background-color': '#f9f9f9'
		},
		bodyStyle: {
			'margin-left': 'auto',
			'margin-right': 'auto'
		},
		layout: {
			type: 'vbox',
			align: 'center'
		},
		src: '@{previewUrl}',
		setSrc: function(url) {
			if (!url) return
			var me = this,
				ic = new ICropper(me.body.el.dom, {
					keepSquare: true,
					image: url,
					onChange: function(info) {
						me.fireEvent('changeCrop', me, info)
					}
				});
		},
		listeners: {
			changeCrop: '@{changeCrop}'
		}
	}],
	buttons: [{
		xtype: 'button',
		name: 'browse',
		preventDefault: false,
		id: 'upload_button',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});

/*
glu.defView('RS.user.ProfileUpload', {
	asWindow: {
		height: '@{windowHeight}',
		width: '@{windowWidth}',
		modal: true,
		padding : 15,
		cls : 'rs-modal-popup',
		resizable: false,
		draggable: false,
		title: '~~uploadProfilePicTitle~~',
		listeners: {
			show: function(win) {
				win.fireEvent('setupSizes', win, win)
			},
			setupSizes: '@{setupSizes}'
		}
	},

	layout: 'card',
	activeItem: '@{activeItem}',
	items: [{
		padding: '80px 0px 0px 0px',
		layout: {
			type: 'vbox',
			align: 'center'
		},
		items: [{
			html: '<i style="color: #666" class="icon-picture icon-5x"></i>'
		}, {
			padding: '10px',
			html: '@{dragText}'
		}, {
			xtype: 'button',
			text: '~~clickToUpload~~',
			cls : 'rs-small-btn rs-btn-light',
			itemId: 'clickToUpload'
		}, {
			xtype: 'progressbar',
			margin: '20px 0px 0px 0px',
			hidden: '@{!showProgress}',
			flex: 1
		}],
		uploader: {
			autoStart: true,
			runtimes: '',
			url: '/resolve/service/user/upload',
			browse_button: 'getAtRuntime',
			max_file_size: '128mb',
			resize: '',
			flash_swf_url: '/resolve/js/plupload/plupload.flash.swf',
			silverlight_xap_url: '',
			filters: [{
				title: "Image files",
				extensions: "jpg,jpeg,png,gif"
			}],
			//chunk_size: '1mb', // @see http://www.plupload.com/punbb/viewtopic.php?id=1259
			chunk_size: null,
			unique_names: true,
			multipart: true,
			multipart_params: {},
			multi_selection: false,
			// drop_element: null,
			required_features: null
		},

		uploaderListeners: {
			beforestart: function(uploader, data) {
				// console.log('beforestart')
			},
			uploadready: function(uploader, data) {
				// console.log('uploadready')
			},
			uploadstarted: function(uploader, data) {
				uploader.uploader.disableBrowse(true);
				var owner = this.owner,
					progressBar = owner.down('#progress');
				owner.percent = 0;
				Ext.TaskManager.start({
					run: function() {
						progressBar.updateProgress(owner.percent / 100, owner.percent + '%');
						if (owner.percent == 100) {
							return false;
						}
					},
					interval: 100
				})
			},
			uploadcomplete: function(uploader, data) {
				this.owner.fireEvent('uploadComplete', this.owner, uploader, data);
				var btnId = uploader.browse_button;
				uploader.uploader.disableBrowse(false);
			},
			uploaderror: function(uploader, data) {
				this.owner.fireEvent('uploadError', this.owner, uploader, data);
			},
			filesadded: function(uploader, data) {
				this.owner.fireEvent('filesAdded', this.owner, uploader, data);
			},
			beforeupload: function(uploader, data) {
				// console.log('beforeupload')
			},
			fileuploaded: function(uploader, data) {
				// console.log('fileuploaded')
			},
			uploadprogress: function(uploader, file, name, size, percent) {
				this.owner.percent = file.percent;
			},
			storeempty: function(uploader, data) {
				// console.log('storeempty')
			}
		},
		listeners: {
			uploadError: '@{uploadError}',
			filesAdded: '@{filesAdded}',
			uploadComplete: '@{uploadComplete}',
			afterrender: function(panel) {
				Ext.defer(function() {
					panel.myUploader = Ext.create('RS.common.SinglePlupUpload', panel)
					var browseBtnName = panel.uploader.browse_button,
						browseBtn = panel.down('#clickToUpload');
					panel.uploader.browse_button = browseBtn.id
					panel.uploader.container = browseBtn.ownerCt.id
					panel.uploader.drop_element = panel.body.el.id
					panel.myUploader.initialize(panel)
				}, 100, this)
			}
		}
	}, {
		style: {
			'background-color': '#f9f9f9'
		},
		bodyStyle: {
			'margin-left': 'auto',
			'margin-right': 'auto'
		},
		layout: {
			type: 'vbox',
			align: 'center'
		},
		src: '@{previewUrl}',
		setSrc: function(url) {
			if (!url) return
			var me = this,
				ic = new ICropper(me.body.el.dom, {
					keepSquare: true,
					image: url,
					onChange: function(info) {
						me.fireEvent('changeCrop', me, info)
					}
				});
		},
		listeners: {
			changeCrop: '@{changeCrop}'
		}
	}],
	buttons: [{
		name : 'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});
*/