glu.defView('RS.user.UserPicker', {
	title: '@{pickerTitle}',
	modal: true,	
	layout: 'fit',
	cls : 'rs-modal-popup',
	padding : 15,
	width: 800,
	height: 450,
	items: [{
		xtype: 'grid',
		cls :'rs-grid-dark',
		name: 'users',
		columns: [{
			header: '~~userId~~',
			dataIndex: 'uuserName',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~uname~~',
			dataIndex: 'ufirstName',
			filterable: true,
			sortable: true,
			flex: 1,
			renderer: function(value, meta, record) {
				return (value || '') + ' ' + (record.get('ulastName') || '')
			}
		}, {
			header: '~~email~~',
			dataIndex: 'uemail',
			filterable: true,
			sortable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()),
		plugins: [{
			ptype: 'pager'
		}],
		selModel: {
			mode: 'MULTI'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'keyword',
				width : 400				
			}]
		}],
		margin : {
			bottom : 10
		}
	}],	
	buttons: [{
		name : 'add',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'select',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name :'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]	
});