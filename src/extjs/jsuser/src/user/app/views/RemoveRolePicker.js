glu.defView('RS.user.RemoveRolePicker', {
	title: '~~removeRolePickerTitle~~',
	width: 600,	
	height: 400,
	padding : 15,
	layout: 'fit',
	cls : 'rs-modal-popup',
	modal : true,
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'roles',
		columns: '@{columns}',
		selModel: {
			mode: 'MULTI'
		},
		margin : {
			bottom : 10
		}
	}],
	buttons: [{
		name : 'remove',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name :'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})