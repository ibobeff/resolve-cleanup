glu.defView('RS.user.OrganizationPicker',{
	title: '~~addOrganization~~',
	padding : 15,	
	modal: true,
	cls : 'rs-modal-popup',
	layout: 'fit',	
	width: 800,
	height: 450,
	items : [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'organizations',
		columns: [{
			header : '~~uname~~',
			dataIndex : 'uname',
			width : 300,
		},{
			header : '~~udescription~~',
			dataIndex : 'udescription',
			flex : 1
		}],
		plugins: [{
			ptype: 'pager'
		}],
		selModel: {
			mode: 'MULTI'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'keyword',
				width : 400
			}]
		}],	
	}],	
	buttons: [{
		name :'add',
		cls : 'rs-med-btn rs-btn-dark'
	},{ 
		name :  'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})