glu.defView('RS.user.Analytics', {
	xtype: 'container',
	hidden: '@{hidden}',
	items: [{
		xtype: 'container',
		items: '@{permissionList}'
	}]
});

glu.defView('RS.user.AnalyticsCheckboxGroup', {
	xtype: 'checkboxgroup',
	cls: '@{labelStyle}',
	fieldLabel: '@{label}',
	labelWidth: 150,
	items: '@{groupPermissions}',
	defaults: {
	},
});

glu.defView('RS.user.AnalyticsCheckbox', {
	xtype: 'checkbox',
	cls: 'rs-rbac-checkbox',
	boxLabel: '@{label}',
	checked: '@{checked}',
	listeners: {
		change: function (checkbox, newValue) {
			this.fireEvent('handlePermissionChange', null, newValue);
		},
		handlePermissionChange: '@{handlePermissionChange}',
	}
});