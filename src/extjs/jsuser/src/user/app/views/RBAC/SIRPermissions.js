glu.defView('RS.user.SIRPermissions', {
	xtype: 'container',
	hidden: '@{hidden}',
	items: [{
		xtype: 'container',
		items: '@{permissionList}'
	}]
});

glu.defView('RS.user.SIRCheckboxGroup', {
	xtype: 'checkboxgroup',
	cls: '@{labelStyle}',
	fieldLabel: '@{label}',
	labelWidth: 150,
	items: '@{groupPermissions}',
	defaults: {
	},
});

glu.defView('RS.user.SIRCheckbox', {
	xtype: 'checkbox',
	cls: 'rs-rbac-checkbox',
	boxLabel: '@{label}',
	checked: '@{checked}',
	listeners: {
		change: function (checkbox, newValue) {
			this.fireEvent('handlePermissionChange', null, newValue);
		},
		handlePermissionChange: '@{handlePermissionChange}',
	}
});