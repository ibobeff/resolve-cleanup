glu.defView('RS.user.Role', {
	title: '@{windowTitle}',
	width: 800,
	padding : 15,
	modal: true,
	resizable: false,
	layout : 'fit',
	cls : 'rs-modal-popup',
	dock: 'top',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'textfield',
		name: 'uname'
	}, {
		xtype: 'textarea',
		name: 'udescription',
	}, {
		title: '~~permissions~~',
		id: 'rs-user-permissions',
		height: 500,
		overflowY: 'auto',
		dock: 'top',
		dockedItems: [{
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				margin: '5 0 0 0',
				items: [{
					xtype: 'combobox',
					fieldLabel: '~~app~~',
					queryMode: 'local',
					store: '@{appStore}',
					displayField: 'name',
					valueField: 'value',
					value: '@{selectedApp}',
					editable: false,
				}, {
					xtype: 'button',
					text: '~~applyPredefined~~',
					hidden: true
				}]
			}]
		}],
		items: [{
			xtype: '@{SIRPermissions}',
		},
		{
			xtype: '@{Analytics}',
		}]
	}],
	buttons: [{
		name : 'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});