glu.defView('RS.user.GroupPicker', {
	title: '@{title}',
	padding : 15,
	cls : 'rs-modal-popup',
	modal: true,	
	layout: 'fit',
	width: 800,
	height: 450,
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		margin : {
			bottom : 10
		},
		name: 'groups',
		columns: '@{columns}',
		plugins: [{
			ptype: 'pager'
		}],
		selModel: {
			mode: 'MULTI'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'keyword',
				width : 400				
			}]
		}],
	}],	
	buttons: [{
		name :'add',
		cls : 'rs-med-btn rs-btn-dark'
	},{ 
		name :  'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});