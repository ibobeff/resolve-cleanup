glu.defModel('RS.user.ProfileUpload', {
	mixins: ['FileUploadManager'],
	api: {
		upload: '/resolve/service/user/upload',
		list: ''
	},

	intro: '',
	activeItem: 0,

	allowedExtensions: ['png', 'jpg', 'jpeg', 'gif'],
	fineUploaderTemplateHidden: false,
	checkForDuplicates: false,
	allowMultipleFiles: false,

	imageUploaded$: function() {
		return this.activeItem == 1
	},

	bodyHeight: 0,
	bodyWidth: 0,

	cropInfo: null,

	browse: function() {},

	browseIsVisible$: function() {
		return this.activeItem == 0;
	},

	init: function() {
		this.initToken();
		this.set('intro', this.localize('profileIntro'));
		this.set('dragHereText', this.localize('dragText'));
	},

	activate: function() {
		this.initToken();
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/user/previewProfile', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this));
	},

	cancel: function() {
		this.doClose()
	},

	saveIsEnabled$: function() {
		return this.imageUploaded
	},

	saveIsVisible$: function() {
		return this.activeItem == 1;
	},

	save: function() {
		this.ajax({
			url: '/resolve/service/user/crop',
			params: {
				scaledWidth: this.bodyWidth,
				scaledHeight: this.bodyHeight,
				x: this.cropInfo.l,
				y: this.cropInfo.t,
				width: this.cropInfo.w,
				height: this.cropInfo.h
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.parentVM.loadUser()
					this.doClose()
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	fineUploaderClass: 'profile',

	uploadedOn: new Date(),

	csrftoken: '',
	fileOnCompleteCallback: function(manager, filename, response, xhr) {
		this.set('uploadedOn', new Date());
		this.set('activeItem', 1)
	},

	previewUrl$: function() {
		if (this.win && this.activeItem == 1) {
			var bh = this.win.items.items[0].getHeight(),
				bw = this.win.items.items[0].getWidth();
			this.set('bodyHeight', bh)
			this.set('bodyWidth', bw)
			return Ext.String.format('/resolve/service/user/previewProfile?d={0}&height={1}&width={2}&{3}', Ext.Date.format(this.uploadedOn, 'time'), bh, bw, this.csrftoken)
		}
		return ''
	},

	changeCrop: function(crop) {
		this.set('cropInfo', crop)
	},

	win: null,

	setupSizes: function(win) {
		this.set('win', win)
	}
});

glu.defModel('RS.user.FileUploadManagerFactory', function(cfg) {
	Ext.applyIf(cfg, RS.common.viewmodels.FileUploadManager);
	return cfg;
});

/*
glu.defModel('RS.user.ProfileUpload', {

	showProgress: false,
	activeItem: 0,
	imageUploaded$: function() {
		return this.activeItem == 1
	},

	windowWidth$: function() {
		return Ext.getBody().getWidth() - 100
	},
	windowHeight$: function() {
		return Ext.getBody().getHeight() - 100
	},

	bodyHeight: 0,
	bodyWidth: 0,

	cropInfo: null,

	dragText$: function() {
		return Ext.String.format('<div style="text-align:center">{0}<br/>- {1} -<br/></div>', this.localize('dragText'), this.localize('or'))
	},

	cancel: function() {
		this.doClose()
	},

	saveIsEnabled$: function() {
		return this.imageUploaded
	},

	save: function() {
		this.ajax({
			url: '/resolve/service/user/crop',
			params: {
				scaledWidth: this.bodyWidth,
				scaledHeight: this.bodyHeight,
				x: this.cropInfo.l,
				y: this.cropInfo.t,
				width: this.cropInfo.w,
				height: this.cropInfo.h
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.parentVM.loadUser()
					this.doClose()
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	uploadError: function() {
		debugger;
	},
	filesAdded: function() {
		this.set('showProgress', true)
	},
	uploadComplete: function() {
		this.set('uploadedOn', new Date())
		this.set('showProgress', false)
		this.set('activeItem', 1)
	},

	uploadedOn: new Date(),

	previewUrl$: function() {
		if (this.win && this.activeItem == 1) {
			var bh = this.win.items.items[0].getHeight(),
				bw = this.win.items.items[0].getWidth();
			this.set('bodyHeight', bh)
			this.set('bodyWidth', bw)
			return Ext.String.format('/resolve/service/user/previewProfile?d={0}&height={1}&width={2}', Ext.Date.format(this.uploadedOn, 'time'), bh, bw)
		}
		return ''
	},

	changeCrop: function(crop) {
		this.set('cropInfo', crop)
	},

	win: null,

	setupSizes: function(win) {
		this.set('win', win)
	}
});
*/