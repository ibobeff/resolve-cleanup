glu.defModel('RS.user.Users', {

	mock: false,

	activate: function() {
		this.users.load()
		clientVM.setWindowTitle(this.localize('allUsers'))
	},

	columns: [],
	usersSelections: [],
	allSelected: false,

	displayName: '',
	stateId: 'users',

	users: {
		mtype: 'store',
		sorters: ['uuserName'],
		remoteSort: true,
		fields: ['id', 'uuserName', 'ufirstName', 'ulastName', {
			name: 'ulockedOut',
			type: 'bool'
		}, 'roles', 'groups', 'utitle', 'uemail', 'uphone', 'umobilePhone', {
			name: 'ulastLogin',
			type: 'date',
			dateFormat: 'c',
			format: 'c'
		}, 'ulastLoginDevice'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/user/listUsers',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.user.createMockBackend(true)

		this.set('columns', [{
			text: '~~userId~~',
			dataIndex: 'uuserName',
			filterable: true,
			sortable: true,
			width: 200
		}, {
			text: '~~uname~~',
			dataIndex: 'ufirstName',
			filterable: true,
			sortable: true,
			width: 200,
			renderer: function(value, meta, record) {
				return (value || '') + ' ' + (record.get('ulastName') || '')
			}
		}, {
			text: '~~locked~~',
			dataIndex: 'ulockedOut',
			filterable: true,
			sortable: true,
			width: 65,
			align: 'center',
			renderer: RS.common.grid.booleanRenderer()
		}, {
			text: '~~roles~~',
			dataIndex: 'roles',
			filterable: false,
			sortable: false,
			flex: 1
		}, {
			text: '~~groups~~',
			dataIndex: 'groups',
			filterable: false,
			sortable: false,
			flex: 1
		}, {
			text: '~~title~~',
			dataIndex: 'utitle',
			filterable: true,
			sortable: true,
			hidden: true
		}, {
			text: '~~email~~',
			dataIndex: 'uemail',
			filterable: true,
			sortable: true,
			hidden: true
		}, {
			text: '~~phone~~',
			dataIndex: 'uphone',
			filterable: true,
			sortable: true,
			hidden: true
		}, {
			text: '~~mobilePhone~~',
			dataIndex: 'umobilePhone',
			filterable: true,
			sortable: true,
			hidden: true
		}, {
			text: '~~lastLogin~~',
			dataIndex: 'ulastLogin',
			filterable: true,
			sortable: true,
			width: 200,
			hidden: true,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}, {
			text: '~~ipAddress~~',
			dataIndex: 'ulastLoginDevice',
			filterable: true,
			sortable: true,
			hidden: true
		}].concat(RS.common.grid.getSysColumns()))

		this.set('displayName', this.localize('userDisplayName'))
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createUser: function() {
		clientVM.handleNavigation({
			modelName: 'RS.user.User',
			params: {
				activeTab: 1,
				create: true
			}
		})
	},
	cloneUser: function() {
		clientVM.handleNavigation({
			modelName: 'RS.user.User',
			params: {
				username: this.usersSelections[0].get('uuserName'),
				copy: true
			}
		})
	},
	cloneUserIsEnabled$: function() {
		return this.usersSelections.length == 1
	},
	editUser: function(username) {
		clientVM.handleNavigation({
			modelName: 'RS.user.User',
			params: {
				username: username,
				activeTab: 1
			}
		})
	},

	assignRoles: function() {
		this.open({
			mtype: 'RS.sysadmin.RolePicker',
			callback: this.reallyAssignRoles,
			includePublic : "false"
		})
	},
	reallyAssignRoles: function(roles) {
		var userIds = [],
			roleIds = [];

		Ext.Array.forEach(this.usersSelections, function(user) {
			userIds.push(user.get('id'))
		})

		Ext.Array.forEach(Ext.Array.from(roles), function(role) {
			roleIds.push(role.get('id'))
		})

		this.ajax({
			url: '/resolve/service/user/assignRoles',
			params: {
				userSysIds: userIds,
				rolesSysIds: roleIds
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.users.load()
					clientVM.displaySuccess(this.localize('rolesAdded'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	assignRolesIsEnabled$: function() {
		return this.usersSelections.length > 0
	},
	removeRoles: function() {
		var roleNames = [];
		Ext.Array.forEach(this.usersSelections || [], function(selection) {
			var record = this.users.getById(selection.get('id'));
			var roles = record.get('roles');
			Ext.Array.forEach(roles || [], function(item) {
				roleNames.push(Ext.String.trim(item))
			})
		}, this)
		if (roleNames.length) {
			this.open({
				mtype: 'RS.user.RemoveRolePicker',
				callback: this.reallyRemoveRoles,
				isRemove: true,
				roleNames: roleNames
			})
		} else {
			clientVM.displayError(this.localize('noRoles'));
		}
	},
	reallyRemoveRoles: function(roles) {
		var userIds = [],
			roleNames = [];

		Ext.Array.forEach(this.usersSelections, function(user) {
			userIds.push(user.get('id'))
		})

		Ext.Array.forEach(Ext.Array.from(roles), function(role) {
			roleNames.push(role.get('name'))
		})

		this.ajax({
			url: '/resolve/service/user/removeRoles',
			params: {
				userSysIds: userIds,
				roles: roleNames
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.users.load()
					clientVM.displaySuccess(this.localize('rolesRemoved'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	removeRolesIsEnabled$: function() {
		return this.usersSelections.length > 0
	},
	assignGroups: function() {
		this.open({
			mtype: 'RS.user.GroupPicker',
			callback: this.reallyAssignGroups
		})
	},
	reallyAssignGroups: function(groups) {
		var userIds = [],
			groupIds = [];

		Ext.Array.forEach(this.usersSelections, function(user) {
			userIds.push(user.get('id'))
		})
		Ext.Array.forEach(Ext.Array.from(groups), function(group) {
			groupIds.push(group.get('id'))
		})

		this.ajax({
			url: '/resolve/service/user/assignGroups',
			params: {
				userSysIds: userIds,
				groupSysIds: groupIds
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.users.load()
					clientVM.displaySuccess(this.localize('groupsAdded'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	assignGroupsIsEnabled$: function() {
		return this.usersSelections.length > 0
	},
	removeGroups: function() {
		var groupNames = [];
		Ext.Array.forEach(this.usersSelections || [], function(selection) {
			var record = this.users.getById(selection.get('id'));
			var groups = record.get('groups');
			Ext.Array.forEach(groups || [], function(item) {
				if (Ext.Array.indexOf(groupNames, Ext.String.trim(item)) == -1)
					groupNames.push(Ext.String.trim(item))
			})
		}, this)
		if (groupNames.length) {
			this.open({
				mtype: 'RS.user.RemoveGroupPicker',
				callback: this.reallyRemoveGroups,
				isRemove: true,
				groupNames: groupNames
			})
		} else {
			clientVM.displayError(this.localize('noGroups'));
		}
	},
	reallyRemoveGroups: function(groups) {
		var userIds = [],
			groupNames = [];

		Ext.Array.forEach(this.usersSelections, function(user) {
			userIds.push(user.get('id'))
		})
		Ext.Array.forEach(Ext.Array.from(groups), function(group) {
			groupNames.push(group.get('name'))
		})

		this.ajax({
			url: '/resolve/service/user/removeGroups',
			params: {
				userSysIds: userIds,
				groups: groupNames
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.users.load()
					clientVM.displaySuccess(this.localize('groupsRemoved'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	removeGroupsIsEnabled$: function() {
		return this.usersSelections.length > 0
	},
	deleteUserIsEnabled$: function() {
		return this.usersSelections.length > 0
	},
	deleteUser: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.usersSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.usersSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/user/deleteUser',
					params: {
						ids: [],
						whereClause: this.user.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('usersSelections', [])
							this.users.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			} else {
				var ids = []
				Ext.each(this.usersSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/user/deleteUser',
					params: {
						ids: ids
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('usersSelections', [])
							this.users.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	}
});