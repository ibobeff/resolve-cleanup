glu.defModel('RS.user.UserPicker', {
	pick: false,
	keyword : '',
	delayedTask : null,
	when_keyword_changed: {
		on: ['keywordChanged'],
		action: function() {
			if(this.delayedTask)
				clearTimeout(this.delayedTask);
			this.delayedTask = setTimeout(function(){
				this.users.loadPage(1);
			}.bind(this),300)		
		}
	},
	pickerTitle$: function() {
		return this.pick ? this.localize('selectUserTitle') : this.localize('addUserTitle')
	},
	usersSelections : [],
	users: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uuserName'],
		fields: ['id', 'uuserName', 'ufirstName', 'ulastName', 'uemail', 'udisplayName'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/user/listUsers',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		},
		autoLoad : true
	},
	init: function() {	
		this.users.on('beforeload', function(store, operation, opts) {
			operation.params = operation.params || {};		
			if (this.keyword) {
				Ext.apply(operation.params, {
					filter: Ext.encode([{
						field: 'uuserName',
						type: 'auto',
						condition: 'contains',
						value: this.keyword
					}])			
				})
			}
		}, this);		
	},

	cancel: function() {
		this.doClose()
	},
	add: function() {
		if (this.callback)
			this.callback.apply(this.parentVM, [this.usersSelections])		
		this.doClose();
	},
	addIsEnabled$: function() {
		return this.usersSelections.length > 0
	},
	addIsVisible$: function() {
		return !this.pick
	},
	select: function() {
		this.callback.apply(this.parentVM, [this.usersSelections])
		this.doClose();
	},
	selectIsEnabled$: function() {
		return this.addIsEnabled
	},
	selectIsVisible$: function() {
		return this.pick
	}
});