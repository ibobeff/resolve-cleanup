glu.defModel('RS.user.Organization', {
	fields: ['id', 'uname', 'udescription',{
		name : 'uhasNoOrgAccess',
		type : 'bool'
	}].concat(RS.common.grid.getSysFields()),
	defaultData: {
		uname: '',
		udescription: '',
		sysCreatedOn: '',
		sysUpdatedOn: '',
		sysCreatedBy: '',
		sysUpdatedBy: '',
		sysOrganizationName: '',
		uhasNoOrgAccess: false,
	},
	API : {
		getOrg : '/resolve/service/user/getOrg',
		saveOrg : '/resolve/service/user/saveOrg',
		getParentOrg : '/resolve/service/user/getValidParentOrgsByName'
	},
	sysOrganizationName :'',
	uparentOrg : '',	
	persisting: false,
	parentOrgId : -1,
	parentOrgStore : {
		mtype : 'store',
		fields : ['id','uname'],
		data : [{id : -1, uname : 'No Parent'}]	
	},
	groups: {
		mtype: 'store',
		sorters: ['uname'],
		fields: ['id', 'uname']		
	},	
	groupsSelections : [],

	init : function(){
		this.parentOrgStore.on('beforeload', function(store, operation, opts) {
			operation.params = { childOrgName : this.uname } 			
		}, this);

	},
	activate: function(screen, params) {
		this.resetForm();
		this.set('id', params ? params.id : '');	
		this.loadOrganization();	
		clientVM.setWindowTitle(this.localize('organization'))	
	},
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},
	organizationTitle$: function() {
		return this.localize('organizationTitle', this.uname || this.localize('newOrganzition'));
	},	
	resetForm: function() {
		this.loadData(this.defaultData);
		this.groups.removeAll();
		this.set('parentOrgId', -1);
		this.set('isPristine', true);
		this.set('uname', ''); // set after isPristine so uname field has red exclamation
	},		
	unameIsValid$: function() {
		return this.uname && this.uname.toLowerCase() != 'none' ? true : this.localize('nameInvalid');
	},
	sysCreatedOnDisplay$: function() {
		return Ext.isDate(this.sysCreatedOn) ? Ext.Date.format(this.sysCreatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysUpdatedOnDisplay$: function() {
		return Ext.isDate(this.sysUpdatedOn) ? Ext.Date.format(this.sysUpdatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	loadOrganization: function() {
		if (this.id) {
			this.groups.removeAll();		
			this.ajax({
				url: this.API['getOrg'],
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.loadData(response.data);

						//Load Group
						if (Ext.isArray(response.data.orgGroups)) Ext.Array.forEach(response.data.orgGroups, function(group) {
								this.groups.add(group)
							}, this)
						
						//Load Parent Orgs					
						this.set('uparentOrg', response.data.uparentOrg);
						this.getParentOrg(this.uname);
						
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
		else {
			//Load Parent Orgs
			this.getParentOrg();
		}
	},
	getParentOrg : function(childOrg){
		this.ajax({
			url : this.API['getParentOrg'],
			params : {
				childOrgName : childOrg
			},
			success : function(resp){
				var response = RS.common.parsePayload(resp);
					if(response.success){
						var defaultParent = [{id : -1, uname : 'No Parent'}];
						var parentOrgData = (response.records || []).sort(function(a,b){ return a.uname < b.uname ? -1 : 1});
						this.parentOrgStore.loadData(defaultParent.concat(parentOrgData));
						this.set('parentOrgId', this.uparentOrg ? this.uparentOrg.id : -1);
					}
					else
						clientVM.displayError(response.message);
			},
			failure: function(r) {
					clientVM.displayFailure(r);
				}
		})
	},
	refresh: function() {
		if (this.id) {
			this.loadOrganization();
		} else {
			this.resetForm();
		}
	},

	save: function(exitAfterSave) {
		if(this.persisting || !this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.persistOrganization, this, [exitAfterSave])
		this.task.delay(500)
		this.set('persisting', true)
	},	
	saveIsEnabled$: function() {
		return this.isValid;
	},
	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},
	
	persistOrganization: function(exitAfterSave) {
		var groups = [];		

		this.groups.each(function(group) {
			groups.push(group.data)
		})		
	
		var data = Ext.apply(this.asObject(), {
			orgGroups: groups,
			uparentOrg : this.parentOrgId == -1 ? null : this.parentOrgStore.findRecord('id', this.parentOrgId).raw
		});		

		this.ajax({
			url: this.API['saveOrg'],
			jsonData: data,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize('orgSaved'))
					if (exitAfterSave === true) clientVM.handleNavigation({
						modelName: 'RS.user.Organizations'
					})
					else {
						this.set('id', response.data.id)						
					}
				} else 
					clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('persisting', false)
			}
		})
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.user.Organizations'
		})
	},

	addGroup: function() {
		this.open({
			mtype: 'RS.user.GroupPicker',
			callback : function(groupsSelections){
				Ext.Array.forEach(groupsSelections, function(r) {
					if (this.groups.indexOf(r) == -1)
						this.groups.add(r.data)
				}, this)
			}
		})
	},
	removeGroup: function() {
		Ext.Array.forEach(this.groupsSelections, function(r) {
			this.groups.remove(r)
		}, this)
	},
	removeGroupIsEnabled$: function() {
		return this.groupsSelections.length > 0
	}	
});
