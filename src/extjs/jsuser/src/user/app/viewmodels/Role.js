glu.defModel('RS.user.Role', {
	isNew: false,

	SIRPermissions: {
		mtype: 'RS.user.SIRPermissions',
	},

	Analytics: {
		mtype: 'RS.user.Analytics',
	},

	selectedApp: 'securityOperations',
	appStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [
			{name: 'Security Operations', value: 'securityOperations'},
			{name: 'Analytics', value: 'analytics'},
		]
	},

	predefinedRolesStore: {
		mtype: 'store',
		fields: ['name', 'value'],
	},

	applyPredefined: function() {
		return null
	},
	
    permissionState: {
        /* SIR */
        'sir': false,
        /* SIR Analytics */
        'sir.analytics.graphs.view': false,
        /* SIR Dashboard */
        'sir.dashboard.caseCreation.create': false,
        'sir.dashboard.incidentList.viewAll': false,
        /* SIR Incidents */
        'sir.incident.title.change': false,
        'sir.incident.classification.change': false,
        'sir.incident.type.change': false,
        'sir.incident.dataCompromised.change': false,
        'sir.incident.severity.change': false,
        'sir.incident.status.change': false,
        'sir.incident.priority.change': false,
        'sir.incident.playbook.change': false,
        'sir.incident.owner.change': false,
        'sir.incident.dueDate.change': false,
        'sir.incident.riskScore.change': false,
        'sir.incident.incidentFlags.change': false,
        'sir.incident.markIncidents.asDuplicate': false,
        /* SIR Incident Viewer */
        'sir.incidentViewer.activity.create': false,
        'sir.incidentViewer.activity.status.change': false,
        'sir.incidentViewer.activity.assignee.change': false,
        'sir.incidentViewer.note.create': false,
        'sir.incidentViewer.artifact.create': false,
        'sir.incidentViewer.artifact.executeAutomation': false,
        'sir.incidentViewer.artifact.delete': false,
        'sir.incidentViewer.attachment.upload': false,
        'sir.incidentViewer.attachment.download': false,
        'sir.incidentViewer.attachment.downloadMalicious': false,
        'sir.incidentViewer.attachment.unsetMalicious': false,
        'sir.incidentViewer.attachment.delete': false,
        'sir.incidentViewer.team.change': false,
        'sir.incidentViewer.activityTimeline.view': false,
        /* Playbook Templates */
        'sir.playbookTemplates.templateBuilder.view': false,
        'sir.playbookTemplates.templateBuilder.create': false,
        'sir.playbookTemplates.templateBuilder.change': false,
        /* Settings */
        'sir.settings.change': false,
        'analytics.reporting.view': false,
    },

	permissions: {},

	updatePermissions: function(permissionsObj) {
		this.set('permissions', Object.assign({}, this.permissions, permissionsObj));
	},

	loadPermissions: function() {
		this.SIRPermissions.loadPermissions(this.permissions);
		this.Analytics.loadPermissions(this.permissions);
	},

	showSIR: false,
	toggleSIR$: function() {
		if (this.selectedApp === 'securityOperations') {
			this.SIRPermissions.set('hidden', false);
			this.Analytics.set('hidden', true);
		} else if (this.selectedApp === 'analytics') {
			this.SIRPermissions.set('hidden', true);
			this.Analytics.set('hidden', false);
		} else {
			this.SIRPermissions.set('hidden', true);
			this.Analytics.set('hidden', true);
		}
	},

	windowTitle$: function() {
		return this.id ? this.localize('editRole') : this.localize('newRole')
	},

	fields: ['id', 'uname', 'udescription'],
	unameIsValid$: function() {
		if (!this.uname) return this.localize('nameInvalid')
		return true
	},
	udescription: '',

	init: function() {
		if (this.id) {
			this.loadRole()
		}
	},

	loadRole: function() {
		this.ajax({
			url: '/resolve/service/user/getRole',
			scope: this,
			params: {
				id: this.id
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.loadData(response.data);
					this.updatePermissions(response.data.upermissions || {});
					this.loadPermissions();
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.persistRole, this, [exitAfterSave]);
		this.task.delay(500);
		this.set('persisting', true);
	},
	saveIsEnabled$: function() {
		return !this.persisting && this.isValid
	},

	persisting: false,
	persistRole: function() {
		var roleObj = this.asObject();
		roleObj.upermissions = this.permissions;

		this.ajax({
			url: '/resolve/service/user/saveRole',
			jsonData: roleObj,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				this.doClose();
				if (response.success) {
					clientVM.displaySuccess(this.localize('roleSaved'))
					this.parentVM.roles.load();
					//this.doClose()
				} else clientVM.displayError(response.message);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('persisting', false)
			}
		})
	},

	cancel: function() {
		this.doClose()
	}
});