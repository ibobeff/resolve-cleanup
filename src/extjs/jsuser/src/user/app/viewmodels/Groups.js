glu.defModel('RS.user.Groups', {

	mock: false,

	activate: function() {
		this.groups.load()
		clientVM.setWindowTitle(this.localize('allGroups'))
	},

	columns: [],
	groupsSelections: [],
	allSelected: false,

	displayName: '',
	stateId: 'groups',

	groups: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uname'],
		fields: ['id', 'uname', 'udescription', 'roles'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/user/listGroups',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.user.createMockBackend(true)

		this.set('columns', [{
			header: '~~uname~~',
			dataIndex: 'uname',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~udescription~~',
			dataIndex: 'udescription',
			filterable: false,
			sortable: false,
			flex: 1
		}, {
			header: '~~roles~~',
			dataIndex: 'roles',
			filterable: false,
			sortable: false,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))

		this.set('displayName', this.localize('groupDisplayName'))
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createGroup: function() {
		clientVM.handleNavigation({
			modelName: 'RS.user.Group'
		})
	},
	editGroup: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.user.Group',
			params: {
				id: id
			}
		})
	},
	copyGroup: function() {
		clientVM.handleNavigation({
			modelName: 'RS.user.Group',
			params: {
				id: this.groupsSelections[0].get('id'),
				copy: true
			}
		})
	},
	copyGroupIsEnabled$: function() {
		return this.groupsSelections.length == 1
	},
	deleteGroupIsEnabled$: function() {
		return this.groupsSelections.length > 0
	},
	deleteGroup: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.groupsSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.groupsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/user/deleteGroup',
					params: {
						ids: [],
						whereClause: this.user.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('groupsSelections', [])
							this.groups.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			} else {
				var ids = []
				Ext.each(this.groupsSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/user/deleteGroup',
					params: {
						ids: ids
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('groupsSelections', [])
							this.groups.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	}
});