glu.defModel('RS.user.RemoveRolePicker', {
	roles: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uname'],
		fields: ['name'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	rolesSelections: [],
	columns: [{
		header: '~~name~~',
		dataIndex: 'name',
		flex: 1
	}],

	remove: function() {
		if (this.callback) {
			this.callback.apply(this.parentVM, this.rolesSelections)
		}
		this.doClose()
	},
	cancel: function() {
		this.doClose()
	},

	init: function() {
		Ext.Array.forEach(this.roleNames, function(name) {
			this.roles.add({
				name: name
			})
		}, this)
	}

})