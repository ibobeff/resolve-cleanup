glu.defModel('RS.user.Organizations', {
	organizations: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uname'],
		fields: ['id', 'uname', 'udescription', 'groups'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/user/listOrgs',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	activate: function() {
		this.organizations.load();	
		clientVM.setWindowTitle(this.localize('organizations'))		
	},	
	init: function() {
	
	},
	createOrganization: function() {
		clientVM.handleNavigation({
			modelName: 'RS.user.Organization'
		})
	},
	editOrganization: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.user.Organization',
			params: {
				id: id
			}
		})
	}	
});