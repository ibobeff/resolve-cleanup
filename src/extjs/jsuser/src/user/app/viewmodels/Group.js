glu.defModel('RS.user.Group', {
	copy: false,
	groupTitle$: function() {
		return this.localize('groupTitle', this.uname || this.localize('newGroup'));
	},
	defaultData: {
		uname: '',
		udescription: '',
		uisLinkToTeam: false,
		uhasExternalLink: false,
		sysCreatedOn: '',
		sysUpdatedOn: '',
		sysCreatedBy: '',
		sysUpdatedBy: '',
		sysOrganizationName: ''
	},

	fields: ['id', 'uname', 'udescription', 'uhasExternalLink', 'uisLinkToTeam','sysOrganizationName'].concat(RS.common.grid.getSysFields()),	
	unameIsValid$: function() {
		return this.uname ? true : this.localize('nameInvalid');
	},	
	sysCreatedOnDisplay$: function() {
		return Ext.isDate(this.sysCreatedOn) ? Ext.Date.format(this.sysCreatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},	
	sysUpdatedOnDisplay$: function() {
		return Ext.isDate(this.sysUpdatedOn) ? Ext.Date.format(this.sysUpdatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},	
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat();
	},	
	init: function() {	
		 this.set('noRolesError', this.localize('noRolesError'));
		 this.roles.on('datachanged', this.updateRoles, this);
	},
	activate: function(screen, params) {
		this.resetForm();
		this.set('id', params ? params.id : '');
		this.set('copy', params ? params.copy == 'true' : false);
		this.loadGroup();
		clientVM.setWindowTitle(this.localize('group'));
	},
	resetForm: function() {
		this.loadData(this.defaultData);	
		this.roles.removeAll();
		this.users.removeAll();
		this.organizations.removeAll();
		this.resetRoles();
		this.set('isPristine', true);
		this.set('uname', ''); // set after isPristine so uname field has red exclamation
	},
	loadGroup: function() {
		if (this.id) {
			this.roles.removeAll();
			this.users.removeAll();
			this.organizations.removeAll();
			this.ajax({
				url: '/resolve/service/user/getGroup',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.set('id', response.data.id)
						this.set('uname', response.data.uname)
						this.set('udescription', response.data.udescription)
						this.set('uisLinkToTeam', response.data.uisLinkToTeam)
						this.set('uhasExternalLink', response.data.uhasExternalLink)
						this.set('sysCreatedOn', Ext.Date.parse(response.data.sysCreatedOn, 'time'))
						this.set('sysUpdatedOn', Ext.Date.parse(response.data.sysUpdatedOn, 'time'))
						this.set('sysCreatedBy', response.data.sysCreatedBy)
						this.set('sysUpdatedBy', response.data.sysUpdatedBy)
						// this.set('sys_id', response.data.sys_id)

						//load roles
						if (Ext.isArray(response.data.groupRoles)) Ext.Array.forEach(response.data.groupRoles, function(role) {
								this.roles.add(role)
							}, this)
						//load users
						if (Ext.isArray(response.data.groupUsers)) Ext.Array.forEach(response.data.groupUsers, function(user) {
							this.users.add(user)
						}, this)
						//load organizations
						if (Ext.isArray(response.data.groupOrgs)) Ext.Array.forEach(response.data.groupOrgs, function(org) {
							this.organizations.add(org)
						}, this)

						if (this.copy) {
							this.set('copy', false)
							this.set('id', '')
							// this.set('sys_id', '')
							this.set('uname', 'COPY_' + this.uname)
						}
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},
	refresh: function() {
		if (this.id) {
			this.loadGroup();
		} else {
			this.resetForm();
		}
	},
	saveIsEnabled$: function() {
		return this.isValid && this.rolesValid;
	},
	save: function(exitAfterSave) {
		if(this.persisting || !this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.persistGroup, this, [exitAfterSave])
		this.task.delay(500)
		this.set('persisting', true)
	},	
	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},
	persisting: false,
	persistGroup: function(exitAfterSave) {
		var roles = [],
			users = [],
			orgs = [];

		this.roles.each(function(role) {
			roles.push(role.data)
		})

		this.users.each(function(user) {
			users.push(user.data)
		})

		this.organizations.each(function(org) {
			orgs.push(org.data)
		})

		var data = Ext.apply(this.asObject(), {
			groupRoles: roles,
			groupUsers: users,
			groupOrgs : orgs
		});

		data.uhasExternalLink = this.uhasExternalLink
		data.uisLinkToTeam = this.uisLinkToTeam

		this.ajax({
			url: '/resolve/service/user/saveGroup',
			jsonData: data,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize('groupSaved'))
					if (exitAfterSave === true) clientVM.handleNavigation({
						modelName: 'RS.user.Groups'
					})
					else {
						this.set('id', response.data.id)
						// this.set('sys_id', response.data.sys_id)
					}
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('persisting', false)
			}
		})
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.user.Groups'
		})
	},

	rolesValid: false,
	noRolesError: '',
	rolesCls: 'rs-grid-dark',

	updateRoles: function() {
		var rolesTipElements = document.getElementsByClassName('group-roles-tip');
		if (this.roles.data.length) {
			this.set('rolesValid', true);
			this.set('rolesCls', 'rs-grid-dark');
			for (var i=0; i<rolesTipElements.length; i++) {
				rolesTipElements[i].classList.add('tooltip-hidden');
			}
		} else {
			this.resetRoles(rolesTipElements);
		}
	},

	resetRoles: function(elements) {
		this.set('rolesValid', false);
		this.set('rolesCls', 'rs-grid-dark group-no-roles');
		var rolesTipElements = elements || document.getElementsByClassName('group-roles-tip');
		for (var i=0; i<rolesTipElements.length; i++) {
			rolesTipElements[i].classList.remove('tooltip-hidden');
		}
	},

	//Roles
	rolesSelections: [],
	roles: {
		mtype: 'store',
		sorters: ['uname'],
		fields: ['id', 'uname'],
		proxy: {
			type: 'memory'
		}
	},	
	addRole: function() {
		this.open({
			mtype: 'RS.sysadmin.RolePicker'
		})
	},
	removeRole: function() {
		Ext.Array.forEach(this.rolesSelections, function(r) {
			this.roles.remove(r)
		}, this)
	},
	removeRoleIsEnabled$: function() {
		return this.rolesSelections.length > 0
	},

	//Users
	usersSelections: [],	
	users: {
		mtype: 'store',
		sorters: ['uuserName'],
		fields: ['id', 'uuserName', 'ufirstName', 'ulastName', 'uemail'],
		proxy: {
			type: 'memory'
		}
	},
	addUser: function() {
		this.open({
			mtype: 'RS.user.UserPicker',
			callback : function(usersSelections){
				Ext.Array.forEach(usersSelections, function(r) {
					if (this.users.indexOf(r) == -1)
						this.users.add(r.data)
				}, this)
			}
		})
	},
	removeUser: function() {
		Ext.Array.forEach(this.usersSelections, function(c) {
			this.users.remove(c)
		}, this)
	},
	removeUserIsEnabled$: function() {
		return this.usersSelections.length > 0
	},

	//Organizations	
	organizationsSelection : [],
	organizations : {
		mtype: 'store',
		sorters: ['uname'],
		fields: ['id', 'uname']	
	},
	addOrganization : function(){
		this.open({
			mtype: 'RS.user.OrganizationPicker',
			callback : function(organizationsSelection){
				Ext.Array.forEach(organizationsSelection, function(r) {
					if (this.organizations.indexOf(r) == -1)
						this.organizations.add(r.data)
				}, this)
			}
		})
	},
	removeOrganization : function(){
		Ext.Array.forEach(this.organizationsSelection, function(r) {		
			this.organizations.remove(r);
		}, this)	
	},
	removeOrganizationIsEnabled$ : function(){
		return this.organizationsSelection.length > 0;
	},
});