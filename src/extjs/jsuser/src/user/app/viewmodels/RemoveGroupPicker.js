glu.defModel('RS.user.RemoveGroupPicker', {
	groupNames: [],
	groups: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uname'],
		fields: ['name'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	groupsSelections: [],
	columns: [{
		header: '~~name~~',
		dataIndex: 'name',
		flex: 1
	}],

	remove: function() {
		if (this.callback) {
			this.callback.apply(this.parentVM, [this.groupsSelections])
		}
		this.doClose()
	},
	cancel: function() {
		this.doClose()
	},

	init: function() {
		Ext.Array.forEach(this.groupNames, function(name) {
			this.groups.add({
				name: name
			})
		}, this)
	}
})