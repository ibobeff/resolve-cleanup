glu.defModel('RS.user.Analytics', {
	hidden: true,
	isNew: false,
	
	config: [
		{id: 'analytics', name: 'analytics', permissions: [], format: {section: 1, bold: true}},
		{id: 'analytics.reporting', name: 'analyticsReporting', permissions: ['view'], format: {section: 2, bold: false}},
	],

	permissionList: {
		mtype: 'list',
	},

	loadPermissions: function (permissions) {
		this.permissionList.removeAll();
		var mergedPermissions = Object.assign({}, this.parentVM.permissionState, permissions);
		this.parentVM.set('permissionState', mergedPermissions);
		this.generatePermissions();
	},

	updatePermissions: function(key, value) {
		this.parentVM.permissionState[key] = value;
		this.parentVM.updatePermissions(this.parentVM.permissionState);
	},

	generatePermissions: function() {
		/* Generate Permissions Checkbox array */
		for (var i = 0; i < this.config.length; i++) {
			var modelObject = this.model({
				mtype: 'RS.user.AnalyticsCheckboxGroup',
				id: this.config[i].id || '',
				label: this.localize(this.config[i].name),
				bold: this.config[i].format.bold,
				section: this.config[i].format.section,
				config: this.config[i].permissions || [],
			});
			this.permissionList.add(modelObject);
		}
	},

	init: function() {
		this.loadPermissions({});
	}
});

glu.defModel('RS.user.AnalyticsCheckboxGroup', {
	id: '',
	label: '',
	section: 0,
	config: [],
	groupPermissions: {
		mtype: 'list',
	},

	labelStyle: '',
	applyLabelStyle$: function () {
		this.set('labelStyle', (this.bold) ? 'rs-user-section' + this.section + ' rs-user-header' : 'rs-user-section' + this.section);
	},

	generateGroupPermissions: function() {
		var perm = this.config.forEach(function(permission) {
			var path = (permission === '') ? this.id : this.id + '.' + permission;
			this.groupPermissions.add(this.model({
				mtype: 'RS.user.AnalyticsCheckbox',
				checked: this.parentVM.parentVM.permissionState[path],
				label: (permission) ? this.localize(permission) : '',
				path: path,
			}));
		}, this);
	},

	init: function() {
		this.generateGroupPermissions();
	}
});

glu.defModel('RS.user.AnalyticsCheckbox', {
	checked: false,
	label: '',
	path: '',
	handlePermissionChange: function(value) {
		this.parentVM.parentVM.updatePermissions(this.path, value);
	}
});
