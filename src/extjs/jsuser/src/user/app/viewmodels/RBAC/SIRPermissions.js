glu.defModel('RS.user.SIRPermissions', {
	hidden: true,
	isNew: false,
	

	config: [
		/* SIR */
		{id: 'sir', name: 'sir', permissions: [''], format: {section: 1, bold: true}},
		/* Analytics */
		// {id: 'sir.analytics', name: 'sirAnalytics', permissions: [], format: {section: 1, bold: true}},
		// {id: 'sir.analytics.graphs', name: 'sirAnalyticsGraph', permissions: ['view'], format: {section: 2, bold: false}},
		/* Dashboard */
		{id: 'sir.dashboard', name: 'sirDashboard', permissions: [], format: {section: 1, bold: true}},
		{id: 'sir.dashboard.caseCreation', name: 'sirDashboardCaseCreation', permissions: ['create'], format: {section: 2, bold: false}},
		{id: 'sir.dashboard.incidentList', name: 'sirDashboardIncidentList', permissions: ['viewAll'], format: {section: 2, bold: false}},
		/* Incident */
		{id: 'sir.incident', name: 'sirIncident', permissions: [], format: {section: 1, bold: true}},
		{id: 'sir.incident.title', name: 'sirIncidentTitle', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.classification', name: 'sirIncidentClassification', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.type', name: 'sirIncidentType', permissions: ['change'], format: {section: 2, bold: false }},
		{id: 'sir.incident.dataCompromised', name: 'sirIncidentDataCompromised', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.severity', name: 'sirIncidentSeverity', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.status', name: 'sirIncidentStatus', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.priority', name: 'sirIncidentPriority', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.playbook', name: 'sirIncidentPlaybook', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.owner', name: 'sirIncidentOwner', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.dueDate', name: 'sirIncidentDueDate', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.riskScore', name: 'sirIncidentRiskScore', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.incidentFlags', name: 'sirIncidentIncidentFlags', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.markIncident', name: 'sirIncidentMarkIncident', permissions: ['asDuplicate'], format: {section: 2, bold: false}},
		/* Incident Viewer */
		{id: 'sir.incidentViewer', name: 'sirIncidentViewer', permissions: [], format: {section: 1, bold: true}},
		{id: 'sir.incidentViewer.activity', name: 'sirIncidentViewerActivity', permissions: ['create', 'assignee.change', 'status.change'], format: {section: 2, bold: false}},
		{id: 'sir.incidentViewer.note', name: 'sirIncidentViewerNote', permissions: ['create'], format: {section: 2, bold: false}},
		{id: 'sir.incidentViewer.artifact', name: 'sirIncidentViewerArtifact', permissions: ['create', 'executeAutomation', 'delete'], format: {section: 2, bold: false}},
		{id: 'sir.incidentViewer.attachment', name: 'sirIncidentViewerAttachment', permissions: ['upload', 'download', 'downloadMalicious', 'unsetMalicious', 'delete'], format: {section: 2, bold: false}},
		{id: 'sir.incidentViewer.team', name: 'sirIncidentViewerTeam', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incidentViewer.activityTimeline', name: 'sirIncidentViewerActivityTimeline', permissions: ['view'], format: {section: 2, bold: false}},
		/* Templates */
		{id: 'sir.playbookTemplates', name: 'sirPlaybookTemplates', permissions: [], format: {section: 1, bold: true}},
		{id: 'sir.playbookTemplates.templateBuilder', name: 'sirPlaybookTemplatesTemplateBuilder', permissions: ['create', 'change'], format: {section: 2, bold: false}},
		/* Settings */
		{id: 'sir.settings', name: 'sirSettings', permissions: ['change'], format: {section: 1, bold: true}},
	],

	dependencies: {
		'sir.incidentViewer.attachment.downloadMalicious': 'sir.incidentViewer.attachment.download',
	},

	permissionList: {
		mtype: 'list',
	},

	loadPermissions: function (permissions) {
		this.permissionList.removeAll();
		var mergedPermissions = Object.assign({}, this.parentVM.permissionState, permissions);
		this.parentVM.set('permissionState', mergedPermissions);
		this.generatePermissions();
	},

	updatePermissions: function(key, value) {
		this.parentVM.permissionState[key] = value;
		this.parentVM.updatePermissions(this.parentVM.permissionState);
	},

	generatePermissions: function() {
		/* Generate Permissions Checkbox array */
		for (var i = 0; i < this.config.length; i++) {
			var modelObject = this.model({
				mtype: 'RS.user.SIRCheckboxGroup',
				id: this.config[i].id || '',
				label: this.localize(this.config[i].name),
				bold: this.config[i].format.bold,
				section: this.config[i].format.section,
				config: this.config[i].permissions || [],
			});
			this.permissionList.add(modelObject);
		}
	},

	init: function() {
		this.loadPermissions({});
	}
});

glu.defModel('RS.user.SIRCheckboxGroup', {
	id: '',
	label: '',
	section: 0,
	config: [],
	groupPermissions: {
		mtype: 'list',
	},

	labelStyle: '',
	applyLabelStyle$: function () {
		this.set('labelStyle', (this.bold) ? 'rs-user-section' + this.section + ' rs-user-header' : 'rs-user-section' + this.section);
	},

	generateGroupPermissions: function() {
		var perm = this.config.forEach(function(permission) {
			var path = (permission === '') ? this.id : this.id + '.' + permission;
			this.groupPermissions.add(this.model({
				mtype: 'RS.user.SIRCheckbox',
				checked: this.parentVM.parentVM.permissionState[path],
				label: (permission) ? this.localize(permission) : '',
				path: path,
			}));
		}, this);
	},

	init: function() {
		this.generateGroupPermissions();
	}
});

glu.defModel('RS.user.SIRCheckbox', {
	checked: false,
	label: '',
	path: '',
	handlePermissionChange: function(value) {
		this.parentVM.parentVM.updatePermissions(this.path, value);
	}
});
