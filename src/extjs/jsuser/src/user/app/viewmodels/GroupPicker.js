glu.defModel('RS.user.GroupPicker', {
	isRemove: false,
	title$: function() {
		return this.isRemove ? this.localize('removeGroupTitle') : this.localize('addGroupTitle')
	},
	keyword : '',
	delayedTask : null,
	when_keyword_changed: {
		on: ['keywordChanged'],
		action: function() {
			if(this.delayedTask)
				clearTimeout(this.delayedTask);
			this.delayedTask = setTimeout(function(){
				this.groups.loadPage(1);
			}.bind(this),300)		
		}
	},
	groups: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uname'],
		fields: ['id', 'uname', 'udescription', 'roles'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/user/listGroups',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	groupsSelections: [],
	columns: [],

	init: function() {
		this.groups.on('beforeload', function(store, operation, opts) {
			operation.params = operation.params || {};		
			if (this.keyword) {
				Ext.apply(operation.params, {
					filter: Ext.encode([{
						field: 'uname',
						type: 'auto',
						condition: 'contains',
						value: this.keyword
					}])			
				})
			}
		}, this);		
		this.set('columns', [{
			header: '~~uname~~',
			dataIndex: 'uname',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~udescription~~',
			dataIndex: 'udescription',
			filterable: false,
			sortable: false,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))
		this.groups.load()
	},

	cancel: function() {
		this.doClose()
	},

	add: function() {
		if (this.callback)
			this.callback.apply(this.parentVM, [this.groupsSelections])		
		this.doClose();
	},
	addIsEnabled$: function() {
		return this.groupsSelections.length > 0
	},
	addIsVisible$: function() {
		return !this.isRemove
	},
	remove: function() {
		this.add()
	},
	removeIsEnabled$: function() {
		return this.addIsEnabled
	},
	removeIsVisible$: function() {
		return !this.addIsVisible
	}
});