glu.defModel('RS.user.OrganizationPicker',{
	keyword : '',
	delayedTask : null,
	organizationsSelections : [],
	when_keyword_changed: {
		on: ['keywordChanged'],
		action: function() {
			if(this.delayedTask)
				clearTimeout(this.delayedTask);
			this.delayedTask = setTimeout(function(){
				this.organizations.loadPage(1);
			}.bind(this),300)		
		}
	},
	organizations : {
		mtype : 'store',
		fields : ['uname', 'udescription'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/user/listOrgs',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		},
		autoLoad : true
	},
	init : function(){
		this.organizations.on('beforeload', function(store, operation, opts) {
			operation.params = operation.params || {};		
			if (this.keyword) {
				Ext.apply(operation.params, {			
					filter: Ext.encode([{
						field: 'uname',
						type: 'auto',
						condition: 'contains',
						value: this.keyword
					}])
				})			
			}			
		}, this);
	},
	add: function() {
		if (this.callback)
			this.callback.apply(this.parentVM, [this.organizationsSelections])		
		this.doClose();
	},
	addIsEnabled$: function() {
		return this.organizationsSelections.length > 0
	},
	cancel : function(){
		this.doClose();
	}
})