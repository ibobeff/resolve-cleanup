glu.defModel('RS.user.PasswordsValidation', {
	uuserPassword: '',
	csrftoken: '',
	uuserPasswordIsValid$: function() {
		if (!this.uuserPassword) {
			return false;
		} else if (this.uuserPassword != '**********') {
			if (this.uuserPassword.length < 8) return this.localize('passwordMinLengthInvalid')
			if (!this.uuserPassword.match(/[0-9]+/g)) return this.localize('passwordNumberRequiredInvalid')
			if (!this.uuserPassword.match(/[A-Z]+/g)) return this.localize('passwordCapitalLetterRequiredInvalid')
			if (!this.uuserPassword.match(/[a-z]+/g)) return this.localize('passwordLowercaseLetterRequiredInvalid')
		}
		return true;
	},
	uuserPasswordConfirm: '',
	uuserPasswordConfirmIsValid$: function() {
		if (this.uuserPassword && this.uuserPassword != '**********')
			return this.uuserPassword == this.uuserPasswordConfirm ? true : this.localize('passwordConfirmMismatchInvalid')
		return true;
	}
});

glu.defModel('RS.user.User', {
	mixins: ['PasswordsValidation'],
	mock: false,

	copy: false,

	create: false,

	userTitle$: function() {
		return this.localize('user') + (this.uuserName ? ' - ' + this.uuserName : '')
	},

	lastLoaded: null,
	profilePicUrl$: function() {
		return this.lastLoaded && this.csrftoken !== '' ? Ext.String.format('/resolve/service/user/downloadProfile?username={0}&d={1}&{2}',
			this.uuserName, Ext.Date.format(this.lastLoaded, 'time'), this.csrftoken) : '';
	},
	doActivate: function(screen, params) {
		this.initToken();
		this.resetForm()
		this.set('username', params ? params.username : '');
		this.set('copy', params ? params.copy == 'true' : false)
		if (params && params.activeTab)
			this.set('activeTab', Number(params.activeTab))
		if (params)
			this.set('create', params.create === true || params.create == 'true')

		if (this.copy && this.blogTabIsPressed) this.adminTab()

		if (this.activeTab > 0 && !this.adminTabIsVisible) this.set('activeTab', 0)
		this.set('isEdit', !this.copy && !this.create);
		this.loadUser()
		clientVM.setWindowTitle(this.localize('user'))
	},
	activate: function(screen, params) {
		if (this.id == clientVM.user.id) {
			this.set('adminTabIsVisible', true);
			this.doActivate(screen, params);
		} else {
			this.ajax({
				url: '/resolve/service/apps/hasPermission',
				method: 'GET',
				params: {
					controller: '/user'
				},
				scope: this,
				success: function(resp) {
					//load roles
					var r = RS.common.parsePayload(resp);
					this.set('adminTabIsVisible', r.data ? r.data.hasPermission : false);
					this.doActivate(screen, params);
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			});
 		}
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/user/downloadProfile', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
			this.set('followersTpl', Ext.create('Ext.XTemplate',
				'<tpl for=".">',
				'<div style="margin-bottom: 10px;margin-right: 5px;float:left" class="thumb-wrap">',
				'<img src="/resolve/service/user/downloadProfile?username={username}&type=comment&' + token_pair[0] + '='+ token_pair[1] +'" style="width:28px;height:28px;" data-qtip="{displayName}" />',
				'</div>',
				'</tpl>'
			));
			this.set('followingTpl', Ext.create('Ext.XTemplate',
				'<tpl for=".">',
				'<div style="margin-bottom: 10px;margin-right: 5px;float:left" class="thumb-wrap">',
				'<img src="/resolve/service/user/downloadProfile?username={username}&type=comment&' + token_pair[0] + '='+ token_pair[1] +'" style="width:28px;height:28px;" data-qtip="{displayName}" />',
				'</div>',
				'</tpl>'
			));
		}.bind(this));
	},

	resetForm: function() {
		this.loadData(this.defaultData)
		this.resetBrowserDefaultValues()
		this.resetRolesGroups();
		this.roles.removeAll()
		this.groups.removeAll()
		this.organizations.removeAll();
		this.set('passwordConfirmUnnecessary', false);
	},

	userDefaultDateFormat: '',
	when_userDefaultDateFormat_changes_update_state: {
		on: ['userDefaultDateFormatChanged'],
		action: function() {
			//Ext.state.Manager.set('userDefaultDateFormat', this.userDefaultDateFormat)
		}
	},
	autoRefreshEnabled: false,
	when_autoRefreshEnabled_changes_update_state: {
		on: ['autoRefreshEnabledChanged'],
		action: function() {
			Ext.state.Manager.set('autoRefreshEnabled', this.autoRefreshEnabled)
		}
	},
	autoRefreshInterval: 0,
	when_autoRefreshInterval_changes_update_state: {
		on: ['autoRefreshIntervalChanged'],
		action: function() {
			Ext.state.Manager.set('autoRefreshInterval', this.autoRefreshInterval)
		}
	},
	useAce: true,
	when_useAce_changes_update_state: {
		on: ['useAceChanged'],
		action: function() {
			Ext.state.Manager.set('useAce', this.useAce)
		}
	},
	aceKeybinding: 'default',
	when_aceKeybinding_changes_update_state: {
		on: ['aceKeybindingChanged'],
		action: function() {
			Ext.state.Manager.set('aceKeybinding', this.aceKeybinding)
		}
	},

	aceLanguage: 'groovy',
	when_aceLanguage_changes_update_state: {
		on: ['aceLanguageChanged'],
		action: function() {
			Ext.state.Manager.set('aceLanguage', this.aceLanguage)
		}
	},

	maxRecentlyUsed: 10,
	when_maxRecentlyUsed_changes_update_state: {
		on: ['maxRecentlyUsedChanged'],
		action: function() {
			Ext.state.Manager.set('maxRecentlyUsed', this.maxRecentlyUsed)
		}
	},

	sideMenuDisplay: false,
	menuDisplayOptionStore : {
		mtype : 'store',
		fields : ['text', 'value'],
		data : [{
			text : 'Side',
			value : true
		},{
			text : 'Tile',
			value : false
		}]
	},

	bannerIsVisible: true,
	when_bannerIsVisible_changes_update_state: {
		on: ['bannerIsVisibleChanged'],
		action: function() {
			Ext.state.Manager.set('bannerIsVisible', this.bannerIsVisible)
			clientVM.set('bannerIsVisible', this.bannerIsVisible)
		}
	},


	userDefaultDateFormatStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [{
			name: Ext.Date.format(new Date(), 'Y-m-d G:i:s'),
			value: 'Y-m-d G:i:s'
		}, {
			name: Ext.Date.format(new Date(), 'Y-m-d g:i:s A'),
			value: 'Y-m-d g:i:s A'
		}]
	},

	aceKeybindingStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [{
			name: 'Default',
			value: 'default'
		}, {
			name: 'Vim',
			value: 'vim'
		}, {
			name: 'Emacs',
			value: 'emacs'
		}]
	},

	aceLanguageStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [{
			name: 'Groovy',
			value: 'groovy'
		}, {
			name: 'Go',
			value: 'golang'
		}, {
			name: 'Perl',
			value: 'perl'
		}, {
			name: 'Power Shell',
			value: 'powershell'
		}, {
			name: 'Ruby',
			value: 'ruby'
		}, {
			name: 'SQL',
			value: 'sql'
		}, {
			name: 'Text',
			value: 'text'
		}, {
			name: 'XML',
			value: 'xml'
		}]
	},

	followers: {
		mtype: 'store',
		sorters: ['displayName'],
		fields: ['id', 'displayName', 'username'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listFollowers',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		},
		sortData: function(f, direction) {
			direction = direction || 'ASC';
			var st = this.fields.get(f).sortType;
			var fn = function(r1, r2) {
				var v1 = st(r1.data[f]),
					v2 = st(r2.data[f]);
				// ADDED THIS FOR CASE INSENSITIVE SORT
				if (v1.toLowerCase) {
					v1 = v1.toLowerCase();
					v2 = v2.toLowerCase();
				}
				return v1 > v2 ? 1 : (v1 < v2 ? -1 : 0);
			}
			this.data.sort(direction, fn);
			if (this.snapshot && this.snapshot != this.data) {
				this.snapshot.sort(direction, fn);
			}
		}
	},

	following: {
		mtype: 'store',
		sorters: ['displayName'],
		fields: ['id', 'displayName', 'username'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listFollowing',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		},
		sortData: function(f, direction) {
			direction = direction || 'ASC';
			var st = this.fields.get(f).sortType;
			var fn = function(r1, r2) {
				var v1 = st(r1.data[f]),
					v2 = st(r2.data[f]);
				// ADDED THIS FOR CASE INSENSITIVE SORT
				if (v1.toLowerCase) {
					v1 = v1.toLowerCase();
					v2 = v2.toLowerCase();
				}
				return v1 > v2 ? 1 : (v1 < v2 ? -1 : 0);
			}
			this.data.sort(direction, fn);
			if (this.snapshot && this.snapshot != this.data) {
				this.snapshot.sort(direction, fn);
			}
		}
	},

	startPageStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	resetBrowserDefaultValues: function() {
		this.set('userDefaultDateFormat', Ext.state.Manager.get('userDefaultDateFormat', 'Y-m-d G:i:s'))
		this.set('autoRefreshEnabled', Ext.state.Manager.get('autoRefreshEnabled', false))
		this.set('autoRefreshInterval', Ext.state.Manager.get('autoRefreshInterval', 30))
		this.set('useAce', Ext.state.Manager.get('useAce', true))
		this.set('aceKeybinding', Ext.state.Manager.get('aceKeybinding', 'default'))
		this.set('aceLanguage', Ext.state.Manager.get('aceLanguage', 'groovy'))
		this.set('maxRecentlyUsed', Ext.state.Manager.get('maxRecentlyUsed', 10))
		this.set('sideMenuDisplay', Ext.state.Manager.get('sideMenuDisplay', false))
		this.set('bannerIsVisible', Ext.state.Manager.get('bannerIsVisible', true))
	},

	defaultData: {
		cloneUsername: '',
		uuserName: '',
		ufirstName: '',
		ulastName: '',
		uuserPassword: '',
		uuserPasswordConfirm: '',
		upasswordNeedsReset: false,
		ulockedOut: false,
		utitle: '',
		uemail: '',
		uhomePhone: '',
		umobilePhone: '',
		ufax: '',
		uim: '',

		ustreet: '',
		ucity: '',
		ustate: '',
		uzip: '',
		ucountry: '',

		sysCreatedOn: '',
		sysUpdatedOn: '',
		sysCreatedBy: '',
		sysUpdatedBy: '',
		sysOrganizationName: '',

		usummary: '',

		uquestion1: '',
		uquestion2: '',
		uquestion3: '',
		uanswer1: '',
		uanswer2: '',
		uanswer3: '',

		ustartPage: '',
		uhomePage: ''
	},

	activeTab: 0,

	fields: ['id',
		'uuserName',
		'ufirstName',
		'ulastName',
		'uuserPassword',
		'uuserPasswordConfirm',
		'upasswordNeedsReset',
		'ulockedOut',
		'utitle',
		'uemail',
		'uhomePhone',
		'umobilePhone',
		'ufax',
		'uim',
		'ustreet',
		'ucity',
		'ustate',
		'uzip',
		'ucountry',
		'usummary',
		'uquestion1',
		'uquestion2',
		'uquestion3',
		'uanswer1',
		'uanswer2',
		'uanswer3',
		'ustartPage',
		'uhomePage'
	].concat(RS.common.grid.getSysFields()),
	isEdit: true,
	id: '',
	cloneUsername: '',
	username: '',
	uuserName: '',
	uuserNameIsValid$: function() {
		if (this.isEdit) {	
			// if we are editing a user, the userID will have already been deemed "valid" even if it fails the regex below, e.g. LDAP user
			this.set('passwordConfirmUnnecessary', true);
			return true;
		} else {
			return (this.uuserName && /^(?=[a-zA-Z0-9])[a-zA-Z0-9_.@]{1,40}$/.test(this.uuserName)) ? true : this.localize('invalidUserName')
		}
	},
	ufirstName: '',
	ulastName: '',
	upasswordNeedsReset: false,
	ulockedOut: false,
	utitle: '',
	uemail: '',
	uhomePhone: '',
	umobilePhone: '',
	ufax: '',
	uim: '',
	usummary: '',

	passwordConfirmUnnecessary: false,

	uemailDisplay$: function() {
		return Ext.String.format('<i class="icon-envelope rs-user-icon" data-qtip="{1}"></i> {0}', Ext.String.htmlEncode(this.uemail), this.localize('uemail'))
	},
	uhomePhoneDisplay$: function() {
		return Ext.String.format('<i class="icon-phone rs-user-icon" data-qtip="{1}"></i> {0}', Ext.String.htmlEncode(this.uhomePhone), this.localize('uhomePhone'))
	},
	umobilePhoneDisplay$: function() {
		return Ext.String.format('<i class="icon-mobile-phone rs-user-icon" data-qtip="{1}"></i> {0}', Ext.String.htmlEncode(this.umobilePhone), this.localize('umobilePhone'))
	},
	ufaxDisplay$: function() {
		return Ext.String.format('<i class="icon-phone-sign rs-user-icon" data-qtip="{1}"></i> {0}', Ext.String.htmlEncode(this.ufax), this.localize('ufax'))
	},
	uimDisplay$: function() {
		return Ext.String.format('<i class="icon-user rs-user-icon" data-qtip="{1}"></i> {0}', Ext.String.htmlEncode(this.uim), this.localize('uim'))
	},

	ustreet: '',
	ucity: '',
	ustate: '',
	uzip: '',
	ucountry: '',
	countryStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: RS.common.data.countries
	},

	uquestion1: '',
	uquestion2: '',
	uquestion3: '',
	uanswer1: '',
	uanswer2: '',
	uanswer3: '',

	ustartPage: '',
	uhomePage: '',

	sysCreatedOn: '',
	sysCreatedOnDisplay$: function() {
		return Ext.isDate(this.sysCreatedOn) ? Ext.Date.format(this.sysCreatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysUpdatedOn: '',
	sysUpdatedOnDisplay$: function() {
		return Ext.isDate(this.sysUpdatedOn) ? Ext.Date.format(this.sysUpdatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysCreatedBy: '',
	sysUpdatedBy: '',

	sysOrganizationName: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	contributions: 0,
	commentsReceived: 0,
	likesReceived: 0,

	contributionsText$: function() {
		return Ext.String.format('<font style="font-size: 18px">{0}</font><br/><span style="text-align:center">{1}</span>', this.contributions, this.localize('contributions'))
	},
	commentsReceivedText$: function() {
		return Ext.String.format('<font style="font-size: 18px">{0}</font><br/><span style="text-align:center">{1}</span>', this.commentsReceived, this.localize('commentsReceived'))
	},
	likesReceivedText$: function() {
		return Ext.String.format('<font style="font-size: 18px">{0}</font><br/><span style="text-align:center">{1}</span>', this.likesReceived, this.localize('likesReceived'))
	},

	uemailHasValue$: function() {
		return this.uemail
	},
	uhomePhoneHasValue$: function() {
		return this.uhomePhone
	},
	umobilePhoneHasValue$: function() {
		return this.umobilePhone
	},
	ufaxHasValue$: function() {
		return this.ufax
	},
	uimHasValue$: function() {
		return this.uim
	},

	blogTab: function() {
		this.set('activeTab', 0)
	},
	blogTabIsPressed$: function() {
		return this.activeTab == 0
	},
	blogTabIsVisible$: function() {
		return !this.copy
	},
	adminTab: function() {
		this.set('activeTab', 1)
	},
	adminTabIsPressed$: function() {
		return this.activeTab == 1
	},
	settingsTab: function() {
		this.set('activeTab', 2)
	},
	settingsTabIsPressed$: function() {
		return this.activeTab == 2
	},
	settingsTabIsVisible$: function() {
		return this.isMe
	},
	adminTabIsVisible$: function() {
		return this.isMe
	},

	isMe$: function() {
		return this.id == clientVM.user.id
	},
	isMeCls$: function() {
		return this.isMe ? '' : 'user-isNotMe'
	},
	showRecoveryOptions$: function() {
		return this.isMe
	},

	uhomePageIsVisible$ : function(){
		return this.ustartPage == 'wiki';
	},
	adminTabIsVisible: false,
	init: function() {
		this.set('followersTpl', Ext.create('Ext.XTemplate','<tpl for="."></tpl>'));
		this.set('followingTpl', Ext.create('Ext.XTemplate','<tpl for="."></tpl>'));
		this.initToken();
		if (this.mock) this.backend = RS.user.createMockBackend(true)

		this.startPageStore.add([{
			name: this.localize('menu'),
			value: 'menu'
		}, {
			name: this.localize('uhomePage'),
			value: 'wiki'
		}, {
			name: this.localize('sirDashboard'),
			value: 'sirDashboard'
		}, {
			name: this.localize('social'),
			value: 'social'
		}, {
			name: this.localize('defaultText'),
			value: 'default'
		}])

		this.startPageStore.sort('name')

		this.aceLanguageStore.sort(function(a, b) {
			if (a.name < b.name) return -1
			if (a.name > b.name) return 1
			return 0
		})

		this.aceKeybindingStore.sort(function(a, b) {
			if (a.name < b.name) return -1
			if (a.name > b.name) return 1
			return 0
		})

		this.userDefaultDateFormatStore.insert(0, {
			name: this.localize('defaultDate'),
			value: 'Y-m-d g:i:s A'
		})

		this.followers.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}

			Ext.apply(operation.params, {
				streamId: this.id
			})
		}, this)
		/*
		this.following.on('load', function(store, records, successfull) {
			debugger;
		}, this)
		*/

		this.set('noRoleOrGroupError', this.localize('noRoleOrGroupError'));
		this.roles.on('datachanged', this.updateRolesGroupsCls, this);
		this.groups.on('datachanged', this.updateRolesGroupsCls, this);
	},

	followersTpl: '',
	followingTpl: '',

	loadUser: function() {
		this.roles.removeAll()
		this.groups.removeAll()
		this.organizations.removeAll();
		if ((this.create === true || this.create === 'true') && !this.id) return

		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/user/getUser',
			params: {
				username: this.username || this.uuserName
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.loadData(response.data)
					this.checkRecoveryOptions();
					this.set('lastLoaded', new Date())
					this.socialDetail.setInitialStream(this.id)
					this.socialDetail.set('initialStreamType', 'user')
					this.socialDetail.set('streamParent', response.data.udisplayName)

					//Now load the followers since we'll have the user id
					this.followers.load()
					this.following.load()

					this.ajax({
						url: '/resolve/service/social/userStats',
						params: {
							username: this.uuserName
						},
						scope: this,
						success: function(r) {
							var response = RS.common.parsePayload(r)
							if (response.success) {
								this.set('contributions', response.data.postContributions + response.data.commentContributions)
								this.set('commentsReceived', response.data.commentsReceived)
								this.set('likesReceived', response.data.likesReceivedForPost + response.data.likesReceivedForComment)
							} else clientVM.displayError(response.message)

						},
						failure: function(r) {
							clientVM.displayFailure(r);
						},
						callback: function() {
							this.set('wait', false);
						}
					})

					if (this.copy) {
						this.set('cloneUsername', this.uuserName)
						this.set('id', '')
						this.set('uuserName', '')
						this.set('ufirstName', '')
						this.set('ulastName', '')

						this.set('sysCreatedBy', '')
						this.set('sysCreatedOn', '')
						this.set('sysUpdatedBy', '')
						this.set('sysUpdatedOn', '')
					}

					//correct password
					this.set('uuserPassword', '**********')

					//correct booleans
					this.set('upasswordNeedsReset', response.data.upasswordNeedsReset)
					this.set('ulockedOut', response.data.ulockedOut)

					//load roles and groups grids
					this.roles.loadData(response.data.userRoles || []);
					this.groups.loadData(response.data.userGroups || []);
					this.organizations.loadData(response.data.userOrgs || []);

					//Load user setting
					this.loadUserSetting(response.data.userSettings);
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},

	recoveryOptionsRO: false,

	checkRecoveryOptions: function(data) {
		if (this.uquestion1 || this.uanswer1 || this.uquestion2 || this.uanswer2 || this.uquestion3 || this.uanswer3) {
			this.set('recoveryOptionsRO', true);
			if (data) {
				if (data.uquestion1) this.set('uquestion1', data.uquestion1);
				if (data.uanswer1) this.set('uanswer1', data.uanswer1);
				if (data.uquestion2) this.set('uquestion2', data.uquestion2);
				if (data.uanswer2) this.set('uanswer2', data.uanswer2);
				if (data.uquestion3) this.set('uquestion3', data.uquestion3);
				if (data.uanswer3) this.set('uanswer3', data.uanswer3);
			}
		} else {
			this.set('recoveryOptionsRO', false);
		}
	},

	resetRecoveryOptions: function() {
		this.set('uquestion1', '');
		this.set('uquestion2', '');
		this.set('uquestion3', '');
		this.set('uanswer1', '');
		this.set('uanswer2', '');
		this.set('uanswer3', '');
		this.set('recoveryOptionsRO', false);
	},

	refresh: function() {
		if (this.id) {
			this.loadUser();
		} else {
			this.resetForm();
		}
	},
	refreshIsEnabled$: function() {
		return !this.wait;
	},
	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.persistUser, this, [exitAfterSave, function() {
			if (this.create) {
				clientVM.handleNavigation({
					modelName: 'RS.user.User',
					params: {
						username: this.uuserName,
						activeTab: 1
					}
				});
			}
			clientVM.displaySuccess(this.localize('UserSaved'));
		}]);
		this.task.delay(500)
		this.set('wait', true)
	},
	saveIsEnabled$: function() {
		var namePasswordValid = this.isValid || (this.uuserNameIsValid == true && this.uuserPasswordIsValid == true && (this.uuserPasswordConfirmIsValid == true || this.passwordConfirmUnnecessary));
		return !this.wait && namePasswordValid && this.rolesGroupsValid;
	},
	saveIsVisible$: function() {
		return this.activeTab > 0
	},
	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	wait: false,
	userSettingList : ['userDefaultDateFormat','autoRefreshEnabled','autoRefreshInterval','aceKeybinding','maxRecentlyUsed','sideMenuDisplay','bannerIsVisible'],
	loadUserSetting : function(settings){
		try {
			var settingObj = settings ? JSON.parse(settings) : {};
			for(var i = 0; i < this.userSettingList.length; i++){
				var setting = this.userSettingList[i];
				this.set(setting, settingObj[setting]);
			}
		}catch (e) {
			clientVM.displayError(this.localize('invalidUserSettings', this.username));
		}
	},
	getUserSetting : function(){
		var settingObj = {};
		for(var i = 0; i < this.userSettingList.length; i++){
			var setting = this.userSettingList[i];
			settingObj[setting] = this[setting];
		}
		return settingObj;
	},
	persistUser: function(exitAfterSave, onSuccessCallback) {

		//save the actual user
		var roles = [],
			groups = [];

		this.roles.each(function(role) {
			roles.push(role.data)
		})

		this.groups.each(function(group) {
			groups.push(group.data)
		})

		if (roles.length == 0 && groups.length == 0) {
			clientVM.displayError(this.localize('noRoleOrGroupError'))
			this.set('wait', false)
			return
		}

		//if we have custom fields, then store those to the db as well
		if (this.customFieldsForm)
			this.customFieldsForm.saveForm();

		var orgs = [];
		this.organizations.each(function(org){
			orgs.push(Ext.apply(org.raw,{
				isDefaultOrg : org.get('isDefaultOrg')
			}))
		})
		var userObj = Ext.apply(this.asObject(), {
			userRoles: roles,
			userGroups: groups,
			userSettings : JSON.stringify(this.getUserSetting()),
			userOrgs : orgs
		});

		//configure password information
		delete userObj.uuserPasswordConfirm
		if (userObj.uuserPassword == '**********') delete userObj.uuserPassword
		if (userObj.uuserPassword) {
			var pass = this.uuserPassword;
			if (pass[0] == '_') pass = pass.substring(1)
			// RBA-12838 Password will be hashed on server from Corona (5.4) onwards
			//userObj.uuserPassword = CryptoJS.enc.Base64.stringify(CryptoJS.SHA512(pass))
			userObj.uuserPassword = pass
		}

		if (this.cloneUsername) userObj.cloneUsername = this.cloneUsername

		this.ajax({
			url: '/resolve/service/user/saveUser',
			jsonData: userObj,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					if (exitAfterSave === true) {
						clientVM.handleNavigation({
							modelName: 'RS.user.Users'
						});
					} else if (Ext.isFunction(onSuccessCallback)) {
						onSuccessCallback.call(this);
					}
					if (!this.create) {
						this.checkRecoveryOptions(response.data);
					}
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.user.Users'
		})
	},

	backIsVisible$: function() {
		return this.adminTabIsVisible
	},

	rolesGroupsValid: false,
	noRoleOrGroupError: '',
	rolesGroupsCls: 'rs-grid-dark',

	updateRolesGroupsCls: function() {
		var rolesGroupsTipElements = document.getElementsByClassName('user-roles-groups-tip');
		if (this.roles.data.length || this.groups.data.length) {
			this.set('rolesGroupsValid', true);
			this.set('rolesGroupsCls', 'rs-grid-dark');
			for (var i=0; i<rolesGroupsTipElements.length; i++) {
				rolesGroupsTipElements[i].classList.add('tooltip-hidden');
			}
		} else {
			this.resetRolesGroups(rolesGroupsTipElements);
		}
	},

	resetRolesGroups: function(elements) {
		this.set('rolesGroupsValid', false);
		this.set('rolesGroupsCls', 'rs-grid-dark users-no-roles-no-groups');
		var rolesGroupsTipElements = elements || document.getElementsByClassName('user-roles-groups-tip');
		for (var i=0; i<rolesGroupsTipElements.length; i++) {
			rolesGroupsTipElements[i].classList.remove('tooltip-hidden');
		}
	},

	roles: {
		mtype: 'store',
		sorters: ['uname'],
		fields: ['id', 'uname'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	rolesSelections: [],
	addRole: function() {
		this.open({
			mtype: 'RS.sysadmin.RolePicker',
			includePublic : "false"
		})
	},
	removeRole: function() {
		Ext.Array.forEach(this.rolesSelections, function(r) {
			this.roles.remove(r)
		}, this)
	},
	removeRoleIsEnabled$: function() {
		return this.rolesSelections.length > 0
	},

	groups: {
		mtype: 'store',
		sorters: ['uname'],
		fields: ['id', 'uname'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	groupsSelections: [],
	addGroup: function() {
		this.open({
			mtype: 'RS.user.GroupPicker',
			callback : function(groupsSelections){
				Ext.Array.forEach(groupsSelections, function(r) {
					if (this.groups.indexOf(r) == -1)
						this.groups.add(r.data)
				}, this)
			}
		})
	},
	removeGroup: function() {
		Ext.Array.forEach(this.groupsSelections, function(r) {
			this.groups.remove(r)
		}, this)
	},
	removeGroupIsEnabled$: function() {
		return this.groupsSelections.length > 0
	},

	resetBrowserPreferences: function() {
		this.message({
			title: this.localize('resetTitle'),
			msg: this.localize('resetMessage'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('resetBrowserPreferences'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyResetBrowserPreferences
		})
	},
	reallyResetBrowserPreferences: function(btn) {
		if (btn == 'yes') {
			Ext.state.Manager.getProvider().getStorageObject().clear()
			if (Ext.supports.LocalStorage) Ext.state.Manager.setProvider(new Ext.state.LocalStorageProvider())
			else Ext.state.Manager.setProvider(new Ext.state.CookieProvider())
			this.resetBrowserDefaultValues()
		}
	},
	resetBrowserPreferencesIsVisible$: function() {
		return this.activeTab == 2
	},

	imageClicked: function() {
		if (this.isMe)
			this.open({
				mtype: 'RS.user.ProfileUpload'
			})
	},


	summaryPanel: null,
	registerSummaryPanel: function(panel) {
		this.set('summaryPanel', panel)
	},

	when_summary_changes_ensure_links_are_with_blank_target: {
		on: ['usummaryChanged'],
		action: function() {
			Ext.defer(function() {
				if (this.summaryPanel && this.summaryPanel.getEl()) {
					var links = this.summaryPanel.getEl().query('a'),
						currentLocation = window.location.protocol + '//' + window.location.host + window.location.pathname;
					Ext.Array.forEach(links, function(link) {
						if (link.href && link.href.indexOf(currentLocation) == -1) {
							link.target = '_blank'
						}
					})
				}
			}, 10, this)
		}
	},

	socialDetail: {
		mtype: 'RS.social.Main',
		mode: 'embed'
	},

	showCustomFields: false,
	customFieldsForm: null,
	formLoaded: function(form) {
		this.set('showCustomFields', true)
		this.set('customFieldsForm', form)
	},
	organizationsSelection : [],
	organizations: {
		mtype: 'store',
		sorters: ['uname'],
		fields: ['uname','isDefaultOrg']	,
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	setDefaultOrgIsEnabled$ : function(){
		return this.organizationsSelection.length > 0;
	},
	setDefaultOrg : function(){
		var record = this.organizationsSelection[0];
		if(record.get('isDefaultOrg'))
			record.set('isDefaultOrg', false);
		else {
			this.organizations.each(function(r){
				r.set('isDefaultOrg', r == record);
			})
		}
	},
	changePassword: function() {
		this.open({
			mtype: 'RS.user.ChangePasswordForm'
		});
	}
});

glu.defModel('RS.user.ChangePasswordForm', {
	mixins: ['PasswordsValidation'],
	uuserOldPassword: '',
	currentUser: true,
	init: function() {
		this.set('currentUser', this.parentVM.uuserName ===clientVM.user.name);
	},
	applyIsEnabled$: function() {
		return ((this.currentUser && this.uuserOldPassword) || !this.currentUser) && this.uuserPassword
			&& (this.uuserPasswordIsValid$() == true) && this.uuserPasswordConfirm == this.uuserPassword;
	},
	apply: function() {
		this.ajax({
			url: '/resolve/service/user/changePassword',
			jsonData: {
				username:  this.parentVM.uuserName,
				oldPassword: this.uuserOldPassword,
				newPassword: this.uuserPassword
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					clientVM.displaySuccess(this.localize('successfully_Changed_Pas_sword'));
					this.close();
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	cancel: function() {
		this.close();
	}
});
