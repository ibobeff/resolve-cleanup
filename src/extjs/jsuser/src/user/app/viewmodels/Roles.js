glu.defModel('RS.user.Roles', {

	mock: false,

	activate: function() {
		this.roles.load()
		clientVM.setWindowTitle(this.localize('allRoles'))
	},

	columns: [],
	rolesSelections: [],
	allSelected: false,

	displayName: '',
	stateId: 'roles',

	roles: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uname'],
		fields: ['id', 'uname', 'udescription'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/user/listRoles',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.user.createMockBackend(true)

		this.set('columns', [{
			header: '~~uname~~',
			dataIndex: 'uname',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~udescription~~',
			dataIndex: 'udescription',
			filterable: false,
			sortable: false,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))

		this.set('displayName', this.localize('roleDisplayName'))
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createRole: function() {
		this.open({
			mtype: 'RS.user.Role',
			isNew: true
		})
	},
	editRole: function(id) {
		this.open({
			mtype: 'RS.user.Role',
			isNew: false,
			id: id
		})
	},
	deleteRoleIsEnabled$: function() {
		return this.rolesSelections.length > 0
	},
	deleteRole: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.rolesSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.rolesSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/user/deleteRole',
					params: {
						ids: [],
						whereClause: this.user.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('rolesSelections', [])
							this.roles.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			} else {
				var ids = [];
				Ext.each(this.rolesSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/user/deleteRole',
					params: {
						ids: ids
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('rolesSelections', [])
							this.roles.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	}
});