/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
glu.defModel('RS.user.Group', {
	copy: false,
	groupTitle$: function() {
		return this.localize('groupTitle', this.uname || this.localize('newGroup'));
	},
	defaultData: {
		uname: '',
		udescription: '',
		uisLinkToTeam: false,
		uhasExternalLink: false,
		sysCreatedOn: '',
		sysUpdatedOn: '',
		sysCreatedBy: '',
		sysUpdatedBy: '',
		sysOrganizationName: ''
	},

	fields: ['id', 'uname', 'udescription', 'uhasExternalLink', 'uisLinkToTeam','sysOrganizationName'].concat(RS.common.grid.getSysFields()),	
	unameIsValid$: function() {
		return this.uname ? true : this.localize('nameInvalid');
	},	
	sysCreatedOnDisplay$: function() {
		return Ext.isDate(this.sysCreatedOn) ? Ext.Date.format(this.sysCreatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},	
	sysUpdatedOnDisplay$: function() {
		return Ext.isDate(this.sysUpdatedOn) ? Ext.Date.format(this.sysUpdatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},	
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat();
	},	
	init: function() {	
		 this.set('noRolesError', this.localize('noRolesError'));
		 this.roles.on('datachanged', this.updateRoles, this);
	},
	activate: function(screen, params) {
		this.resetForm();
		this.set('id', params ? params.id : '');
		this.set('copy', params ? params.copy == 'true' : false);
		this.loadGroup();
		clientVM.setWindowTitle(this.localize('group'));
	},
	resetForm: function() {
		this.loadData(this.defaultData);	
		this.roles.removeAll();
		this.users.removeAll();
		this.organizations.removeAll();
		this.resetRoles();
		this.set('isPristine', true);
		this.set('uname', ''); // set after isPristine so uname field has red exclamation
	},
	loadGroup: function() {
		if (this.id) {
			this.roles.removeAll();
			this.users.removeAll();
			this.organizations.removeAll();
			this.ajax({
				url: '/resolve/service/user/getGroup',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.set('id', response.data.id)
						this.set('uname', response.data.uname)
						this.set('udescription', response.data.udescription)
						this.set('uisLinkToTeam', response.data.uisLinkToTeam)
						this.set('uhasExternalLink', response.data.uhasExternalLink)
						this.set('sysCreatedOn', Ext.Date.parse(response.data.sysCreatedOn, 'time'))
						this.set('sysUpdatedOn', Ext.Date.parse(response.data.sysUpdatedOn, 'time'))
						this.set('sysCreatedBy', response.data.sysCreatedBy)
						this.set('sysUpdatedBy', response.data.sysUpdatedBy)
						// this.set('sys_id', response.data.sys_id)

						//load roles
						if (Ext.isArray(response.data.groupRoles)) Ext.Array.forEach(response.data.groupRoles, function(role) {
								this.roles.add(role)
							}, this)
						//load users
						if (Ext.isArray(response.data.groupUsers)) Ext.Array.forEach(response.data.groupUsers, function(user) {
							this.users.add(user)
						}, this)
						//load organizations
						if (Ext.isArray(response.data.groupOrgs)) Ext.Array.forEach(response.data.groupOrgs, function(org) {
							this.organizations.add(org)
						}, this)

						if (this.copy) {
							this.set('copy', false)
							this.set('id', '')
							// this.set('sys_id', '')
							this.set('uname', 'COPY_' + this.uname)
						}
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},
	refresh: function() {
		if (this.id) {
			this.loadGroup();
		} else {
			this.resetForm();
		}
	},
	saveIsEnabled$: function() {
		return this.isValid && this.rolesValid;
	},
	save: function(exitAfterSave) {
		if(this.persisting || !this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.persistGroup, this, [exitAfterSave])
		this.task.delay(500)
		this.set('persisting', true)
	},	
	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},
	persisting: false,
	persistGroup: function(exitAfterSave) {
		var roles = [],
			users = [],
			orgs = [];

		this.roles.each(function(role) {
			roles.push(role.data)
		})

		this.users.each(function(user) {
			users.push(user.data)
		})

		this.organizations.each(function(org) {
			orgs.push(org.data)
		})

		var data = Ext.apply(this.asObject(), {
			groupRoles: roles,
			groupUsers: users,
			groupOrgs : orgs
		});

		data.uhasExternalLink = this.uhasExternalLink
		data.uisLinkToTeam = this.uisLinkToTeam

		this.ajax({
			url: '/resolve/service/user/saveGroup',
			jsonData: data,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize('groupSaved'))
					if (exitAfterSave === true) clientVM.handleNavigation({
						modelName: 'RS.user.Groups'
					})
					else {
						this.set('id', response.data.id)
						// this.set('sys_id', response.data.sys_id)
					}
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('persisting', false)
			}
		})
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.user.Groups'
		})
	},

	rolesValid: false,
	noRolesError: '',
	rolesCls: 'rs-grid-dark',

	updateRoles: function() {
		var rolesTipElements = document.getElementsByClassName('group-roles-tip');
		if (this.roles.data.length) {
			this.set('rolesValid', true);
			this.set('rolesCls', 'rs-grid-dark');
			for (var i=0; i<rolesTipElements.length; i++) {
				rolesTipElements[i].classList.add('tooltip-hidden');
			}
		} else {
			this.resetRoles(rolesTipElements);
		}
	},

	resetRoles: function(elements) {
		this.set('rolesValid', false);
		this.set('rolesCls', 'rs-grid-dark group-no-roles');
		var rolesTipElements = elements || document.getElementsByClassName('group-roles-tip');
		for (var i=0; i<rolesTipElements.length; i++) {
			rolesTipElements[i].classList.remove('tooltip-hidden');
		}
	},

	//Roles
	rolesSelections: [],
	roles: {
		mtype: 'store',
		sorters: ['uname'],
		fields: ['id', 'uname'],
		proxy: {
			type: 'memory'
		}
	},	
	addRole: function() {
		this.open({
			mtype: 'RS.sysadmin.RolePicker'
		})
	},
	removeRole: function() {
		Ext.Array.forEach(this.rolesSelections, function(r) {
			this.roles.remove(r)
		}, this)
	},
	removeRoleIsEnabled$: function() {
		return this.rolesSelections.length > 0
	},

	//Users
	usersSelections: [],	
	users: {
		mtype: 'store',
		sorters: ['uuserName'],
		fields: ['id', 'uuserName', 'ufirstName', 'ulastName', 'uemail'],
		proxy: {
			type: 'memory'
		}
	},
	addUser: function() {
		this.open({
			mtype: 'RS.user.UserPicker',
			callback : function(usersSelections){
				Ext.Array.forEach(usersSelections, function(r) {
					if (this.users.indexOf(r) == -1)
						this.users.add(r.data)
				}, this)
			}
		})
	},
	removeUser: function() {
		Ext.Array.forEach(this.usersSelections, function(c) {
			this.users.remove(c)
		}, this)
	},
	removeUserIsEnabled$: function() {
		return this.usersSelections.length > 0
	},

	//Organizations	
	organizationsSelection : [],
	organizations : {
		mtype: 'store',
		sorters: ['uname'],
		fields: ['id', 'uname']	
	},
	addOrganization : function(){
		this.open({
			mtype: 'RS.user.OrganizationPicker',
			callback : function(organizationsSelection){
				Ext.Array.forEach(organizationsSelection, function(r) {
					if (this.organizations.indexOf(r) == -1)
						this.organizations.add(r.data)
				}, this)
			}
		})
	},
	removeOrganization : function(){
		Ext.Array.forEach(this.organizationsSelection, function(r) {		
			this.organizations.remove(r);
		}, this)	
	},
	removeOrganizationIsEnabled$ : function(){
		return this.organizationsSelection.length > 0;
	},
});
glu.defModel('RS.user.GroupPicker', {
	isRemove: false,
	title$: function() {
		return this.isRemove ? this.localize('removeGroupTitle') : this.localize('addGroupTitle')
	},
	keyword : '',
	delayedTask : null,
	when_keyword_changed: {
		on: ['keywordChanged'],
		action: function() {
			if(this.delayedTask)
				clearTimeout(this.delayedTask);
			this.delayedTask = setTimeout(function(){
				this.groups.loadPage(1);
			}.bind(this),300)		
		}
	},
	groups: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uname'],
		fields: ['id', 'uname', 'udescription', 'roles'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/user/listGroups',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	groupsSelections: [],
	columns: [],

	init: function() {
		this.groups.on('beforeload', function(store, operation, opts) {
			operation.params = operation.params || {};		
			if (this.keyword) {
				Ext.apply(operation.params, {
					filter: Ext.encode([{
						field: 'uname',
						type: 'auto',
						condition: 'contains',
						value: this.keyword
					}])			
				})
			}
		}, this);		
		this.set('columns', [{
			header: '~~uname~~',
			dataIndex: 'uname',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~udescription~~',
			dataIndex: 'udescription',
			filterable: false,
			sortable: false,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))
		this.groups.load()
	},

	cancel: function() {
		this.doClose()
	},

	add: function() {
		if (this.callback)
			this.callback.apply(this.parentVM, [this.groupsSelections])		
		this.doClose();
	},
	addIsEnabled$: function() {
		return this.groupsSelections.length > 0
	},
	addIsVisible$: function() {
		return !this.isRemove
	},
	remove: function() {
		this.add()
	},
	removeIsEnabled$: function() {
		return this.addIsEnabled
	},
	removeIsVisible$: function() {
		return !this.addIsVisible
	}
});
glu.defModel('RS.user.Groups', {

	mock: false,

	activate: function() {
		this.groups.load()
		clientVM.setWindowTitle(this.localize('allGroups'))
	},

	columns: [],
	groupsSelections: [],
	allSelected: false,

	displayName: '',
	stateId: 'groups',

	groups: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uname'],
		fields: ['id', 'uname', 'udescription', 'roles'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/user/listGroups',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.user.createMockBackend(true)

		this.set('columns', [{
			header: '~~uname~~',
			dataIndex: 'uname',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~udescription~~',
			dataIndex: 'udescription',
			filterable: false,
			sortable: false,
			flex: 1
		}, {
			header: '~~roles~~',
			dataIndex: 'roles',
			filterable: false,
			sortable: false,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))

		this.set('displayName', this.localize('groupDisplayName'))
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createGroup: function() {
		clientVM.handleNavigation({
			modelName: 'RS.user.Group'
		})
	},
	editGroup: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.user.Group',
			params: {
				id: id
			}
		})
	},
	copyGroup: function() {
		clientVM.handleNavigation({
			modelName: 'RS.user.Group',
			params: {
				id: this.groupsSelections[0].get('id'),
				copy: true
			}
		})
	},
	copyGroupIsEnabled$: function() {
		return this.groupsSelections.length == 1
	},
	deleteGroupIsEnabled$: function() {
		return this.groupsSelections.length > 0
	},
	deleteGroup: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.groupsSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.groupsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/user/deleteGroup',
					params: {
						ids: [],
						whereClause: this.user.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('groupsSelections', [])
							this.groups.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			} else {
				var ids = []
				Ext.each(this.groupsSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/user/deleteGroup',
					params: {
						ids: ids
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('groupsSelections', [])
							this.groups.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	}
});
glu.defModel('RS.user.Organizations', {
	organizations: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uname'],
		fields: ['id', 'uname', 'udescription', 'groups'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/user/listOrgs',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	activate: function() {
		this.organizations.load();	
		clientVM.setWindowTitle(this.localize('organizations'))		
	},	
	init: function() {
	
	},
	createOrganization: function() {
		clientVM.handleNavigation({
			modelName: 'RS.user.Organization'
		})
	},
	editOrganization: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.user.Organization',
			params: {
				id: id
			}
		})
	}	
});
glu.defModel('RS.user.Organization', {
	fields: ['id', 'uname', 'udescription',{
		name : 'uhasNoOrgAccess',
		type : 'bool'
	}].concat(RS.common.grid.getSysFields()),
	defaultData: {
		uname: '',
		udescription: '',
		sysCreatedOn: '',
		sysUpdatedOn: '',
		sysCreatedBy: '',
		sysUpdatedBy: '',
		sysOrganizationName: '',
		uhasNoOrgAccess: false,
	},
	API : {
		getOrg : '/resolve/service/user/getOrg',
		saveOrg : '/resolve/service/user/saveOrg',
		getParentOrg : '/resolve/service/user/getValidParentOrgsByName'
	},
	sysOrganizationName :'',
	uparentOrg : '',	
	persisting: false,
	parentOrgId : -1,
	parentOrgStore : {
		mtype : 'store',
		fields : ['id','uname'],
		data : [{id : -1, uname : 'No Parent'}]	
	},
	groups: {
		mtype: 'store',
		sorters: ['uname'],
		fields: ['id', 'uname']		
	},	
	groupsSelections : [],

	init : function(){
		this.parentOrgStore.on('beforeload', function(store, operation, opts) {
			operation.params = { childOrgName : this.uname } 			
		}, this);

	},
	activate: function(screen, params) {
		this.resetForm();
		this.set('id', params ? params.id : '');	
		this.loadOrganization();	
		clientVM.setWindowTitle(this.localize('organization'))	
	},
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},
	organizationTitle$: function() {
		return this.localize('organizationTitle', this.uname || this.localize('newOrganzition'));
	},	
	resetForm: function() {
		this.loadData(this.defaultData);
		this.groups.removeAll();
		this.set('parentOrgId', -1);
		this.set('isPristine', true);
		this.set('uname', ''); // set after isPristine so uname field has red exclamation
	},		
	unameIsValid$: function() {
		return this.uname && this.uname.toLowerCase() != 'none' ? true : this.localize('nameInvalid');
	},
	sysCreatedOnDisplay$: function() {
		return Ext.isDate(this.sysCreatedOn) ? Ext.Date.format(this.sysCreatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysUpdatedOnDisplay$: function() {
		return Ext.isDate(this.sysUpdatedOn) ? Ext.Date.format(this.sysUpdatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	loadOrganization: function() {
		if (this.id) {
			this.groups.removeAll();		
			this.ajax({
				url: this.API['getOrg'],
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.loadData(response.data);

						//Load Group
						if (Ext.isArray(response.data.orgGroups)) Ext.Array.forEach(response.data.orgGroups, function(group) {
								this.groups.add(group)
							}, this)
						
						//Load Parent Orgs					
						this.set('uparentOrg', response.data.uparentOrg);
						this.getParentOrg(this.uname);
						
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
		else {
			//Load Parent Orgs
			this.getParentOrg();
		}
	},
	getParentOrg : function(childOrg){
		this.ajax({
			url : this.API['getParentOrg'],
			params : {
				childOrgName : childOrg
			},
			success : function(resp){
				var response = RS.common.parsePayload(resp);
					if(response.success){
						var defaultParent = [{id : -1, uname : 'No Parent'}];
						var parentOrgData = (response.records || []).sort(function(a,b){ return a.uname < b.uname ? -1 : 1});
						this.parentOrgStore.loadData(defaultParent.concat(parentOrgData));
						this.set('parentOrgId', this.uparentOrg ? this.uparentOrg.id : -1);
					}
					else
						clientVM.displayError(response.message);
			},
			failure: function(r) {
					clientVM.displayFailure(r);
				}
		})
	},
	refresh: function() {
		if (this.id) {
			this.loadOrganization();
		} else {
			this.resetForm();
		}
	},

	save: function(exitAfterSave) {
		if(this.persisting || !this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.persistOrganization, this, [exitAfterSave])
		this.task.delay(500)
		this.set('persisting', true)
	},	
	saveIsEnabled$: function() {
		return this.isValid;
	},
	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},
	
	persistOrganization: function(exitAfterSave) {
		var groups = [];		

		this.groups.each(function(group) {
			groups.push(group.data)
		})		
	
		var data = Ext.apply(this.asObject(), {
			orgGroups: groups,
			uparentOrg : this.parentOrgId == -1 ? null : this.parentOrgStore.findRecord('id', this.parentOrgId).raw
		});		

		this.ajax({
			url: this.API['saveOrg'],
			jsonData: data,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize('orgSaved'))
					if (exitAfterSave === true) clientVM.handleNavigation({
						modelName: 'RS.user.Organizations'
					})
					else {
						this.set('id', response.data.id)						
					}
				} else 
					clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('persisting', false)
			}
		})
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.user.Organizations'
		})
	},

	addGroup: function() {
		this.open({
			mtype: 'RS.user.GroupPicker',
			callback : function(groupsSelections){
				Ext.Array.forEach(groupsSelections, function(r) {
					if (this.groups.indexOf(r) == -1)
						this.groups.add(r.data)
				}, this)
			}
		})
	},
	removeGroup: function() {
		Ext.Array.forEach(this.groupsSelections, function(r) {
			this.groups.remove(r)
		}, this)
	},
	removeGroupIsEnabled$: function() {
		return this.groupsSelections.length > 0
	}	
});

glu.defModel('RS.user.OrganizationPicker',{
	keyword : '',
	delayedTask : null,
	organizationsSelections : [],
	when_keyword_changed: {
		on: ['keywordChanged'],
		action: function() {
			if(this.delayedTask)
				clearTimeout(this.delayedTask);
			this.delayedTask = setTimeout(function(){
				this.organizations.loadPage(1);
			}.bind(this),300)		
		}
	},
	organizations : {
		mtype : 'store',
		fields : ['uname', 'udescription'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/user/listOrgs',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		},
		autoLoad : true
	},
	init : function(){
		this.organizations.on('beforeload', function(store, operation, opts) {
			operation.params = operation.params || {};		
			if (this.keyword) {
				Ext.apply(operation.params, {			
					filter: Ext.encode([{
						field: 'uname',
						type: 'auto',
						condition: 'contains',
						value: this.keyword
					}])
				})			
			}			
		}, this);
	},
	add: function() {
		if (this.callback)
			this.callback.apply(this.parentVM, [this.organizationsSelections])		
		this.doClose();
	},
	addIsEnabled$: function() {
		return this.organizationsSelections.length > 0
	},
	cancel : function(){
		this.doClose();
	}
})
glu.defModel('RS.user.ProfileUpload', {
	mixins: ['FileUploadManager'],
	api: {
		upload: '/resolve/service/user/upload',
		list: ''
	},

	intro: '',
	activeItem: 0,

	allowedExtensions: ['png', 'jpg', 'jpeg', 'gif'],
	fineUploaderTemplateHidden: false,
	checkForDuplicates: false,
	allowMultipleFiles: false,

	imageUploaded$: function() {
		return this.activeItem == 1
	},

	bodyHeight: 0,
	bodyWidth: 0,

	cropInfo: null,

	browse: function() {},

	browseIsVisible$: function() {
		return this.activeItem == 0;
	},

	init: function() {
		this.initToken();
		this.set('intro', this.localize('profileIntro'));
		this.set('dragHereText', this.localize('dragText'));
	},

	activate: function() {
		this.initToken();
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/user/previewProfile', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this));
	},

	cancel: function() {
		this.doClose()
	},

	saveIsEnabled$: function() {
		return this.imageUploaded
	},

	saveIsVisible$: function() {
		return this.activeItem == 1;
	},

	save: function() {
		this.ajax({
			url: '/resolve/service/user/crop',
			params: {
				scaledWidth: this.bodyWidth,
				scaledHeight: this.bodyHeight,
				x: this.cropInfo.l,
				y: this.cropInfo.t,
				width: this.cropInfo.w,
				height: this.cropInfo.h
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.parentVM.loadUser()
					this.doClose()
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	fineUploaderClass: 'profile',

	uploadedOn: new Date(),

	csrftoken: '',
	fileOnCompleteCallback: function(manager, filename, response, xhr) {
		this.set('uploadedOn', new Date());
		this.set('activeItem', 1)
	},

	previewUrl$: function() {
		if (this.win && this.activeItem == 1) {
			var bh = this.win.items.items[0].getHeight(),
				bw = this.win.items.items[0].getWidth();
			this.set('bodyHeight', bh)
			this.set('bodyWidth', bw)
			return Ext.String.format('/resolve/service/user/previewProfile?d={0}&height={1}&width={2}&{3}', Ext.Date.format(this.uploadedOn, 'time'), bh, bw, this.csrftoken)
		}
		return ''
	},

	changeCrop: function(crop) {
		this.set('cropInfo', crop)
	},

	win: null,

	setupSizes: function(win) {
		this.set('win', win)
	}
});

glu.defModel('RS.user.FileUploadManagerFactory', function(cfg) {
	Ext.applyIf(cfg, RS.common.viewmodels.FileUploadManager);
	return cfg;
});

/*
glu.defModel('RS.user.ProfileUpload', {

	showProgress: false,
	activeItem: 0,
	imageUploaded$: function() {
		return this.activeItem == 1
	},

	windowWidth$: function() {
		return Ext.getBody().getWidth() - 100
	},
	windowHeight$: function() {
		return Ext.getBody().getHeight() - 100
	},

	bodyHeight: 0,
	bodyWidth: 0,

	cropInfo: null,

	dragText$: function() {
		return Ext.String.format('<div style="text-align:center">{0}<br/>- {1} -<br/></div>', this.localize('dragText'), this.localize('or'))
	},

	cancel: function() {
		this.doClose()
	},

	saveIsEnabled$: function() {
		return this.imageUploaded
	},

	save: function() {
		this.ajax({
			url: '/resolve/service/user/crop',
			params: {
				scaledWidth: this.bodyWidth,
				scaledHeight: this.bodyHeight,
				x: this.cropInfo.l,
				y: this.cropInfo.t,
				width: this.cropInfo.w,
				height: this.cropInfo.h
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.parentVM.loadUser()
					this.doClose()
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	uploadError: function() {
		debugger;
	},
	filesAdded: function() {
		this.set('showProgress', true)
	},
	uploadComplete: function() {
		this.set('uploadedOn', new Date())
		this.set('showProgress', false)
		this.set('activeItem', 1)
	},

	uploadedOn: new Date(),

	previewUrl$: function() {
		if (this.win && this.activeItem == 1) {
			var bh = this.win.items.items[0].getHeight(),
				bw = this.win.items.items[0].getWidth();
			this.set('bodyHeight', bh)
			this.set('bodyWidth', bw)
			return Ext.String.format('/resolve/service/user/previewProfile?d={0}&height={1}&width={2}', Ext.Date.format(this.uploadedOn, 'time'), bh, bw)
		}
		return ''
	},

	changeCrop: function(crop) {
		this.set('cropInfo', crop)
	},

	win: null,

	setupSizes: function(win) {
		this.set('win', win)
	}
});
*/
glu.defModel('RS.user.Analytics', {
	hidden: true,
	isNew: false,
	
	config: [
		{id: 'analytics', name: 'analytics', permissions: [], format: {section: 1, bold: true}},
		{id: 'analytics.reporting', name: 'analyticsReporting', permissions: ['view'], format: {section: 2, bold: false}},
	],

	permissionList: {
		mtype: 'list',
	},

	loadPermissions: function (permissions) {
		this.permissionList.removeAll();
		var mergedPermissions = Object.assign({}, this.parentVM.permissionState, permissions);
		this.parentVM.set('permissionState', mergedPermissions);
		this.generatePermissions();
	},

	updatePermissions: function(key, value) {
		this.parentVM.permissionState[key] = value;
		this.parentVM.updatePermissions(this.parentVM.permissionState);
	},

	generatePermissions: function() {
		/* Generate Permissions Checkbox array */
		for (var i = 0; i < this.config.length; i++) {
			var modelObject = this.model({
				mtype: 'RS.user.AnalyticsCheckboxGroup',
				id: this.config[i].id || '',
				label: this.localize(this.config[i].name),
				bold: this.config[i].format.bold,
				section: this.config[i].format.section,
				config: this.config[i].permissions || [],
			});
			this.permissionList.add(modelObject);
		}
	},

	init: function() {
		this.loadPermissions({});
	}
});

glu.defModel('RS.user.AnalyticsCheckboxGroup', {
	id: '',
	label: '',
	section: 0,
	config: [],
	groupPermissions: {
		mtype: 'list',
	},

	labelStyle: '',
	applyLabelStyle$: function () {
		this.set('labelStyle', (this.bold) ? 'rs-user-section' + this.section + ' rs-user-header' : 'rs-user-section' + this.section);
	},

	generateGroupPermissions: function() {
		var perm = this.config.forEach(function(permission) {
			var path = (permission === '') ? this.id : this.id + '.' + permission;
			this.groupPermissions.add(this.model({
				mtype: 'RS.user.AnalyticsCheckbox',
				checked: this.parentVM.parentVM.permissionState[path],
				label: (permission) ? this.localize(permission) : '',
				path: path,
			}));
		}, this);
	},

	init: function() {
		this.generateGroupPermissions();
	}
});

glu.defModel('RS.user.AnalyticsCheckbox', {
	checked: false,
	label: '',
	path: '',
	handlePermissionChange: function(value) {
		this.parentVM.parentVM.updatePermissions(this.path, value);
	}
});

glu.defModel('RS.user.SIRPermissions', {
	hidden: true,
	isNew: false,
	

	config: [
		/* SIR */
		{id: 'sir', name: 'sir', permissions: [''], format: {section: 1, bold: true}},
		/* Analytics */
		// {id: 'sir.analytics', name: 'sirAnalytics', permissions: [], format: {section: 1, bold: true}},
		// {id: 'sir.analytics.graphs', name: 'sirAnalyticsGraph', permissions: ['view'], format: {section: 2, bold: false}},
		/* Dashboard */
		{id: 'sir.dashboard', name: 'sirDashboard', permissions: [], format: {section: 1, bold: true}},
		{id: 'sir.dashboard.caseCreation', name: 'sirDashboardCaseCreation', permissions: ['create'], format: {section: 2, bold: false}},
		{id: 'sir.dashboard.incidentList', name: 'sirDashboardIncidentList', permissions: ['viewAll'], format: {section: 2, bold: false}},
		/* Incident */
		{id: 'sir.incident', name: 'sirIncident', permissions: [], format: {section: 1, bold: true}},
		{id: 'sir.incident.title', name: 'sirIncidentTitle', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.classification', name: 'sirIncidentClassification', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.type', name: 'sirIncidentType', permissions: ['change'], format: {section: 2, bold: false }},
		{id: 'sir.incident.dataCompromised', name: 'sirIncidentDataCompromised', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.severity', name: 'sirIncidentSeverity', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.status', name: 'sirIncidentStatus', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.priority', name: 'sirIncidentPriority', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.playbook', name: 'sirIncidentPlaybook', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.owner', name: 'sirIncidentOwner', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.dueDate', name: 'sirIncidentDueDate', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.riskScore', name: 'sirIncidentRiskScore', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.incidentFlags', name: 'sirIncidentIncidentFlags', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incident.markIncident', name: 'sirIncidentMarkIncident', permissions: ['asDuplicate'], format: {section: 2, bold: false}},
		/* Incident Viewer */
		{id: 'sir.incidentViewer', name: 'sirIncidentViewer', permissions: [], format: {section: 1, bold: true}},
		{id: 'sir.incidentViewer.activity', name: 'sirIncidentViewerActivity', permissions: ['create', 'assignee.change', 'status.change'], format: {section: 2, bold: false}},
		{id: 'sir.incidentViewer.note', name: 'sirIncidentViewerNote', permissions: ['create'], format: {section: 2, bold: false}},
		{id: 'sir.incidentViewer.artifact', name: 'sirIncidentViewerArtifact', permissions: ['create', 'executeAutomation', 'delete'], format: {section: 2, bold: false}},
		{id: 'sir.incidentViewer.attachment', name: 'sirIncidentViewerAttachment', permissions: ['upload', 'download', 'downloadMalicious', 'unsetMalicious', 'delete'], format: {section: 2, bold: false}},
		{id: 'sir.incidentViewer.team', name: 'sirIncidentViewerTeam', permissions: ['change'], format: {section: 2, bold: false}},
		{id: 'sir.incidentViewer.activityTimeline', name: 'sirIncidentViewerActivityTimeline', permissions: ['view'], format: {section: 2, bold: false}},
		/* Templates */
		{id: 'sir.playbookTemplates', name: 'sirPlaybookTemplates', permissions: [], format: {section: 1, bold: true}},
		{id: 'sir.playbookTemplates.templateBuilder', name: 'sirPlaybookTemplatesTemplateBuilder', permissions: ['create', 'change'], format: {section: 2, bold: false}},
		/* Settings */
		{id: 'sir.settings', name: 'sirSettings', permissions: ['change'], format: {section: 1, bold: true}},
	],

	dependencies: {
		'sir.incidentViewer.attachment.downloadMalicious': 'sir.incidentViewer.attachment.download',
	},

	permissionList: {
		mtype: 'list',
	},

	loadPermissions: function (permissions) {
		this.permissionList.removeAll();
		var mergedPermissions = Object.assign({}, this.parentVM.permissionState, permissions);
		this.parentVM.set('permissionState', mergedPermissions);
		this.generatePermissions();
	},

	updatePermissions: function(key, value) {
		this.parentVM.permissionState[key] = value;
		this.parentVM.updatePermissions(this.parentVM.permissionState);
	},

	generatePermissions: function() {
		/* Generate Permissions Checkbox array */
		for (var i = 0; i < this.config.length; i++) {
			var modelObject = this.model({
				mtype: 'RS.user.SIRCheckboxGroup',
				id: this.config[i].id || '',
				label: this.localize(this.config[i].name),
				bold: this.config[i].format.bold,
				section: this.config[i].format.section,
				config: this.config[i].permissions || [],
			});
			this.permissionList.add(modelObject);
		}
	},

	init: function() {
		this.loadPermissions({});
	}
});

glu.defModel('RS.user.SIRCheckboxGroup', {
	id: '',
	label: '',
	section: 0,
	config: [],
	groupPermissions: {
		mtype: 'list',
	},

	labelStyle: '',
	applyLabelStyle$: function () {
		this.set('labelStyle', (this.bold) ? 'rs-user-section' + this.section + ' rs-user-header' : 'rs-user-section' + this.section);
	},

	generateGroupPermissions: function() {
		var perm = this.config.forEach(function(permission) {
			var path = (permission === '') ? this.id : this.id + '.' + permission;
			this.groupPermissions.add(this.model({
				mtype: 'RS.user.SIRCheckbox',
				checked: this.parentVM.parentVM.permissionState[path],
				label: (permission) ? this.localize(permission) : '',
				path: path,
			}));
		}, this);
	},

	init: function() {
		this.generateGroupPermissions();
	}
});

glu.defModel('RS.user.SIRCheckbox', {
	checked: false,
	label: '',
	path: '',
	handlePermissionChange: function(value) {
		this.parentVM.parentVM.updatePermissions(this.path, value);
	}
});

glu.defModel('RS.user.RemoveGroupPicker', {
	groupNames: [],
	groups: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uname'],
		fields: ['name'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	groupsSelections: [],
	columns: [{
		header: '~~name~~',
		dataIndex: 'name',
		flex: 1
	}],

	remove: function() {
		if (this.callback) {
			this.callback.apply(this.parentVM, [this.groupsSelections])
		}
		this.doClose()
	},
	cancel: function() {
		this.doClose()
	},

	init: function() {
		Ext.Array.forEach(this.groupNames, function(name) {
			this.groups.add({
				name: name
			})
		}, this)
	}
})
glu.defModel('RS.user.RemoveRolePicker', {
	roles: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uname'],
		fields: ['name'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	rolesSelections: [],
	columns: [{
		header: '~~name~~',
		dataIndex: 'name',
		flex: 1
	}],

	remove: function() {
		if (this.callback) {
			this.callback.apply(this.parentVM, this.rolesSelections)
		}
		this.doClose()
	},
	cancel: function() {
		this.doClose()
	},

	init: function() {
		Ext.Array.forEach(this.roleNames, function(name) {
			this.roles.add({
				name: name
			})
		}, this)
	}

})
glu.defModel('RS.user.Role', {
	isNew: false,

	SIRPermissions: {
		mtype: 'RS.user.SIRPermissions',
	},

	Analytics: {
		mtype: 'RS.user.Analytics',
	},

	selectedApp: 'securityOperations',
	appStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [
			{name: 'Security Operations', value: 'securityOperations'},
			{name: 'Analytics', value: 'analytics'},
		]
	},

	predefinedRolesStore: {
		mtype: 'store',
		fields: ['name', 'value'],
	},

	applyPredefined: function() {
		return null
	},
	
    permissionState: {
        /* SIR */
        'sir': false,
        /* SIR Analytics */
        'sir.analytics.graphs.view': false,
        /* SIR Dashboard */
        'sir.dashboard.caseCreation.create': false,
        'sir.dashboard.incidentList.viewAll': false,
        /* SIR Incidents */
        'sir.incident.title.change': false,
        'sir.incident.classification.change': false,
        'sir.incident.type.change': false,
        'sir.incident.dataCompromised.change': false,
        'sir.incident.severity.change': false,
        'sir.incident.status.change': false,
        'sir.incident.priority.change': false,
        'sir.incident.playbook.change': false,
        'sir.incident.owner.change': false,
        'sir.incident.dueDate.change': false,
        'sir.incident.riskScore.change': false,
        'sir.incident.incidentFlags.change': false,
        'sir.incident.markIncidents.asDuplicate': false,
        /* SIR Incident Viewer */
        'sir.incidentViewer.activity.create': false,
        'sir.incidentViewer.activity.status.change': false,
        'sir.incidentViewer.activity.assignee.change': false,
        'sir.incidentViewer.note.create': false,
        'sir.incidentViewer.artifact.create': false,
        'sir.incidentViewer.artifact.executeAutomation': false,
        'sir.incidentViewer.artifact.delete': false,
        'sir.incidentViewer.attachment.upload': false,
        'sir.incidentViewer.attachment.download': false,
        'sir.incidentViewer.attachment.downloadMalicious': false,
        'sir.incidentViewer.attachment.unsetMalicious': false,
        'sir.incidentViewer.attachment.delete': false,
        'sir.incidentViewer.team.change': false,
        'sir.incidentViewer.activityTimeline.view': false,
        /* Playbook Templates */
        'sir.playbookTemplates.templateBuilder.view': false,
        'sir.playbookTemplates.templateBuilder.create': false,
        'sir.playbookTemplates.templateBuilder.change': false,
        /* Settings */
        'sir.settings.change': false,
        'analytics.reporting.view': false,
    },

	permissions: {},

	updatePermissions: function(permissionsObj) {
		this.set('permissions', Object.assign({}, this.permissions, permissionsObj));
	},

	loadPermissions: function() {
		this.SIRPermissions.loadPermissions(this.permissions);
		this.Analytics.loadPermissions(this.permissions);
	},

	showSIR: false,
	toggleSIR$: function() {
		if (this.selectedApp === 'securityOperations') {
			this.SIRPermissions.set('hidden', false);
			this.Analytics.set('hidden', true);
		} else if (this.selectedApp === 'analytics') {
			this.SIRPermissions.set('hidden', true);
			this.Analytics.set('hidden', false);
		} else {
			this.SIRPermissions.set('hidden', true);
			this.Analytics.set('hidden', true);
		}
	},

	windowTitle$: function() {
		return this.id ? this.localize('editRole') : this.localize('newRole')
	},

	fields: ['id', 'uname', 'udescription'],
	unameIsValid$: function() {
		if (!this.uname) return this.localize('nameInvalid')
		return true
	},
	udescription: '',

	init: function() {
		if (this.id) {
			this.loadRole()
		}
	},

	loadRole: function() {
		this.ajax({
			url: '/resolve/service/user/getRole',
			scope: this,
			params: {
				id: this.id
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.loadData(response.data);
					this.updatePermissions(response.data.upermissions || {});
					this.loadPermissions();
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.persistRole, this, [exitAfterSave]);
		this.task.delay(500);
		this.set('persisting', true);
	},
	saveIsEnabled$: function() {
		return !this.persisting && this.isValid
	},

	persisting: false,
	persistRole: function() {
		var roleObj = this.asObject();
		roleObj.upermissions = this.permissions;

		this.ajax({
			url: '/resolve/service/user/saveRole',
			jsonData: roleObj,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				this.doClose();
				if (response.success) {
					clientVM.displaySuccess(this.localize('roleSaved'))
					this.parentVM.roles.load();
					//this.doClose()
				} else clientVM.displayError(response.message);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('persisting', false)
			}
		})
	},

	cancel: function() {
		this.doClose()
	}
});
glu.defModel('RS.user.Roles', {

	mock: false,

	activate: function() {
		this.roles.load()
		clientVM.setWindowTitle(this.localize('allRoles'))
	},

	columns: [],
	rolesSelections: [],
	allSelected: false,

	displayName: '',
	stateId: 'roles',

	roles: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uname'],
		fields: ['id', 'uname', 'udescription'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/user/listRoles',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.user.createMockBackend(true)

		this.set('columns', [{
			header: '~~uname~~',
			dataIndex: 'uname',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~udescription~~',
			dataIndex: 'udescription',
			filterable: false,
			sortable: false,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))

		this.set('displayName', this.localize('roleDisplayName'))
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createRole: function() {
		this.open({
			mtype: 'RS.user.Role',
			isNew: true
		})
	},
	editRole: function(id) {
		this.open({
			mtype: 'RS.user.Role',
			isNew: false,
			id: id
		})
	},
	deleteRoleIsEnabled$: function() {
		return this.rolesSelections.length > 0
	},
	deleteRole: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.rolesSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.rolesSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/user/deleteRole',
					params: {
						ids: [],
						whereClause: this.user.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('rolesSelections', [])
							this.roles.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			} else {
				var ids = [];
				Ext.each(this.rolesSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/user/deleteRole',
					params: {
						ids: ids
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('rolesSelections', [])
							this.roles.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	}
});
glu.defModel('RS.user.PasswordsValidation', {
	uuserPassword: '',
	csrftoken: '',
	uuserPasswordIsValid$: function() {
		if (!this.uuserPassword) {
			return false;
		} else if (this.uuserPassword != '**********') {
			if (this.uuserPassword.length < 8) return this.localize('passwordMinLengthInvalid')
			if (!this.uuserPassword.match(/[0-9]+/g)) return this.localize('passwordNumberRequiredInvalid')
			if (!this.uuserPassword.match(/[A-Z]+/g)) return this.localize('passwordCapitalLetterRequiredInvalid')
			if (!this.uuserPassword.match(/[a-z]+/g)) return this.localize('passwordLowercaseLetterRequiredInvalid')
		}
		return true;
	},
	uuserPasswordConfirm: '',
	uuserPasswordConfirmIsValid$: function() {
		if (this.uuserPassword && this.uuserPassword != '**********')
			return this.uuserPassword == this.uuserPasswordConfirm ? true : this.localize('passwordConfirmMismatchInvalid')
		return true;
	}
});

glu.defModel('RS.user.User', {
	mixins: ['PasswordsValidation'],
	mock: false,

	copy: false,

	create: false,

	userTitle$: function() {
		return this.localize('user') + (this.uuserName ? ' - ' + this.uuserName : '')
	},

	lastLoaded: null,
	profilePicUrl$: function() {
		return this.lastLoaded && this.csrftoken !== '' ? Ext.String.format('/resolve/service/user/downloadProfile?username={0}&d={1}&{2}',
			this.uuserName, Ext.Date.format(this.lastLoaded, 'time'), this.csrftoken) : '';
	},
	doActivate: function(screen, params) {
		this.initToken();
		this.resetForm()
		this.set('username', params ? params.username : '');
		this.set('copy', params ? params.copy == 'true' : false)
		if (params && params.activeTab)
			this.set('activeTab', Number(params.activeTab))
		if (params)
			this.set('create', params.create === true || params.create == 'true')

		if (this.copy && this.blogTabIsPressed) this.adminTab()

		if (this.activeTab > 0 && !this.adminTabIsVisible) this.set('activeTab', 0)
		this.set('isEdit', !this.copy && !this.create);
		this.loadUser()
		clientVM.setWindowTitle(this.localize('user'))
	},
	activate: function(screen, params) {
		if (this.id == clientVM.user.id) {
			this.set('adminTabIsVisible', true);
			this.doActivate(screen, params);
		} else {
			this.ajax({
				url: '/resolve/service/apps/hasPermission',
				method: 'GET',
				params: {
					controller: '/user'
				},
				scope: this,
				success: function(resp) {
					//load roles
					var r = RS.common.parsePayload(resp);
					this.set('adminTabIsVisible', r.data ? r.data.hasPermission : false);
					this.doActivate(screen, params);
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			});
 		}
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/user/downloadProfile', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
			this.set('followersTpl', Ext.create('Ext.XTemplate',
				'<tpl for=".">',
				'<div style="margin-bottom: 10px;margin-right: 5px;float:left" class="thumb-wrap">',
				'<img src="/resolve/service/user/downloadProfile?username={username}&type=comment&OWASP_CSRFTOKEN='+ token_pair[1] +'" style="width:28px;height:28px;" data-qtip="{displayName}" />',
				'</div>',
				'</tpl>'
			));
			this.set('followingTpl', Ext.create('Ext.XTemplate',
				'<tpl for=".">',
				'<div style="margin-bottom: 10px;margin-right: 5px;float:left" class="thumb-wrap">',
				'<img src="/resolve/service/user/downloadProfile?username={username}&type=comment&OWASP_CSRFTOKEN='+ token_pair[1] +'" style="width:28px;height:28px;" data-qtip="{displayName}" />',
				'</div>',
				'</tpl>'
			));
		}.bind(this));
	},

	resetForm: function() {
		this.loadData(this.defaultData)
		this.resetBrowserDefaultValues()
		this.resetRolesGroups();
		this.roles.removeAll()
		this.groups.removeAll()
		this.organizations.removeAll();
	},

	userDefaultDateFormat: '',
	when_userDefaultDateFormat_changes_update_state: {
		on: ['userDefaultDateFormatChanged'],
		action: function() {
			//Ext.state.Manager.set('userDefaultDateFormat', this.userDefaultDateFormat)
		}
	},
	autoRefreshEnabled: false,
	when_autoRefreshEnabled_changes_update_state: {
		on: ['autoRefreshEnabledChanged'],
		action: function() {
			Ext.state.Manager.set('autoRefreshEnabled', this.autoRefreshEnabled)
		}
	},
	autoRefreshInterval: 0,
	when_autoRefreshInterval_changes_update_state: {
		on: ['autoRefreshIntervalChanged'],
		action: function() {
			Ext.state.Manager.set('autoRefreshInterval', this.autoRefreshInterval)
		}
	},
	useAce: true,
	when_useAce_changes_update_state: {
		on: ['useAceChanged'],
		action: function() {
			Ext.state.Manager.set('useAce', this.useAce)
		}
	},
	aceKeybinding: 'default',
	when_aceKeybinding_changes_update_state: {
		on: ['aceKeybindingChanged'],
		action: function() {
			Ext.state.Manager.set('aceKeybinding', this.aceKeybinding)
		}
	},

	aceLanguage: 'groovy',
	when_aceLanguage_changes_update_state: {
		on: ['aceLanguageChanged'],
		action: function() {
			Ext.state.Manager.set('aceLanguage', this.aceLanguage)
		}
	},

	maxRecentlyUsed: 10,
	when_maxRecentlyUsed_changes_update_state: {
		on: ['maxRecentlyUsedChanged'],
		action: function() {
			Ext.state.Manager.set('maxRecentlyUsed', this.maxRecentlyUsed)
		}
	},

	sideMenuDisplay: false,
	menuDisplayOptionStore : {
		mtype : 'store',
		fields : ['text', 'value'],
		data : [{
			text : 'Side',
			value : true
		},{
			text : 'Tile',
			value : false
		}]
	},

	bannerIsVisible: true,
	when_bannerIsVisible_changes_update_state: {
		on: ['bannerIsVisibleChanged'],
		action: function() {
			Ext.state.Manager.set('bannerIsVisible', this.bannerIsVisible)
			clientVM.set('bannerIsVisible', this.bannerIsVisible)
		}
	},


	userDefaultDateFormatStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [{
			name: Ext.Date.format(new Date(), 'Y-m-d G:i:s'),
			value: 'Y-m-d G:i:s'
		}, {
			name: Ext.Date.format(new Date(), 'Y-m-d g:i:s A'),
			value: 'Y-m-d g:i:s A'
		}]
	},

	aceKeybindingStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [{
			name: 'Default',
			value: 'default'
		}, {
			name: 'Vim',
			value: 'vim'
		}, {
			name: 'Emacs',
			value: 'emacs'
		}]
	},

	aceLanguageStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: [{
			name: 'Groovy',
			value: 'groovy'
		}, {
			name: 'Go',
			value: 'golang'
		}, {
			name: 'Perl',
			value: 'perl'
		}, {
			name: 'Power Shell',
			value: 'powershell'
		}, {
			name: 'Ruby',
			value: 'ruby'
		}, {
			name: 'SQL',
			value: 'sql'
		}, {
			name: 'Text',
			value: 'text'
		}, {
			name: 'XML',
			value: 'xml'
		}]
	},

	followers: {
		mtype: 'store',
		sorters: ['displayName'],
		fields: ['id', 'displayName', 'username'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listFollowers',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		},
		sortData: function(f, direction) {
			direction = direction || 'ASC';
			var st = this.fields.get(f).sortType;
			var fn = function(r1, r2) {
				var v1 = st(r1.data[f]),
					v2 = st(r2.data[f]);
				// ADDED THIS FOR CASE INSENSITIVE SORT
				if (v1.toLowerCase) {
					v1 = v1.toLowerCase();
					v2 = v2.toLowerCase();
				}
				return v1 > v2 ? 1 : (v1 < v2 ? -1 : 0);
			}
			this.data.sort(direction, fn);
			if (this.snapshot && this.snapshot != this.data) {
				this.snapshot.sort(direction, fn);
			}
		}
	},

	following: {
		mtype: 'store',
		sorters: ['displayName'],
		fields: ['id', 'displayName', 'username'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/social/listFollowing',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		},
		sortData: function(f, direction) {
			direction = direction || 'ASC';
			var st = this.fields.get(f).sortType;
			var fn = function(r1, r2) {
				var v1 = st(r1.data[f]),
					v2 = st(r2.data[f]);
				// ADDED THIS FOR CASE INSENSITIVE SORT
				if (v1.toLowerCase) {
					v1 = v1.toLowerCase();
					v2 = v2.toLowerCase();
				}
				return v1 > v2 ? 1 : (v1 < v2 ? -1 : 0);
			}
			this.data.sort(direction, fn);
			if (this.snapshot && this.snapshot != this.data) {
				this.snapshot.sort(direction, fn);
			}
		}
	},

	startPageStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},

	resetBrowserDefaultValues: function() {
		this.set('userDefaultDateFormat', Ext.state.Manager.get('userDefaultDateFormat', 'Y-m-d G:i:s'))
		this.set('autoRefreshEnabled', Ext.state.Manager.get('autoRefreshEnabled', false))
		this.set('autoRefreshInterval', Ext.state.Manager.get('autoRefreshInterval', 30))
		this.set('useAce', Ext.state.Manager.get('useAce', true))
		this.set('aceKeybinding', Ext.state.Manager.get('aceKeybinding', 'default'))
		this.set('aceLanguage', Ext.state.Manager.get('aceLanguage', 'groovy'))
		this.set('maxRecentlyUsed', Ext.state.Manager.get('maxRecentlyUsed', 10))
		this.set('sideMenuDisplay', Ext.state.Manager.get('sideMenuDisplay', false))
		this.set('bannerIsVisible', Ext.state.Manager.get('bannerIsVisible', true))
	},

	defaultData: {
		cloneUsername: '',
		uuserName: '',
		ufirstName: '',
		ulastName: '',
		uuserPassword: '',
		uuserPasswordConfirm: '',
		upasswordNeedsReset: false,
		ulockedOut: false,
		utitle: '',
		uemail: '',
		uhomePhone: '',
		umobilePhone: '',
		ufax: '',
		uim: '',

		ustreet: '',
		ucity: '',
		ustate: '',
		uzip: '',
		ucountry: '',

		sysCreatedOn: '',
		sysUpdatedOn: '',
		sysCreatedBy: '',
		sysUpdatedBy: '',
		sysOrganizationName: '',

		usummary: '',

		uquestion1: '',
		uquestion2: '',
		uquestion3: '',
		uanswer1: '',
		uanswer2: '',
		uanswer3: '',

		ustartPage: '',
		uhomePage: ''
	},

	activeTab: 0,

	fields: ['id',
		'uuserName',
		'ufirstName',
		'ulastName',
		'uuserPassword',
		'uuserPasswordConfirm',
		'upasswordNeedsReset',
		'ulockedOut',
		'utitle',
		'uemail',
		'uhomePhone',
		'umobilePhone',
		'ufax',
		'uim',
		'ustreet',
		'ucity',
		'ustate',
		'uzip',
		'ucountry',
		'usummary',
		'uquestion1',
		'uquestion2',
		'uquestion3',
		'uanswer1',
		'uanswer2',
		'uanswer3',
		'ustartPage',
		'uhomePage'
	].concat(RS.common.grid.getSysFields()),
	isEdit: true,
	id: '',
	cloneUsername: '',
	username: '',
	uuserName: '',
	uuserNameIsValid$: function() {
		if (this.isEdit) {	
			// if we are editing a user, the userID will have already been deemed "valid" even if it fails the regex below, e.g. LDAP user
			return true;
		} else {
			return (this.uuserName && /^(?=[a-zA-Z0-9])[a-zA-Z0-9_.@]{1,40}$/.test(this.uuserName)) ? true : this.localize('invalidUserName')
		}
	},
	ufirstName: '',
	ulastName: '',
	upasswordNeedsReset: false,
	ulockedOut: false,
	utitle: '',
	uemail: '',
	uhomePhone: '',
	umobilePhone: '',
	ufax: '',
	uim: '',
	usummary: '',

	uemailDisplay$: function() {
		return Ext.String.format('<i class="icon-envelope rs-user-icon" data-qtip="{1}"></i> {0}', Ext.String.htmlEncode(this.uemail), this.localize('uemail'))
	},
	uhomePhoneDisplay$: function() {
		return Ext.String.format('<i class="icon-phone rs-user-icon" data-qtip="{1}"></i> {0}', Ext.String.htmlEncode(this.uhomePhone), this.localize('uhomePhone'))
	},
	umobilePhoneDisplay$: function() {
		return Ext.String.format('<i class="icon-mobile-phone rs-user-icon" data-qtip="{1}"></i> {0}', Ext.String.htmlEncode(this.umobilePhone), this.localize('umobilePhone'))
	},
	ufaxDisplay$: function() {
		return Ext.String.format('<i class="icon-phone-sign rs-user-icon" data-qtip="{1}"></i> {0}', Ext.String.htmlEncode(this.ufax), this.localize('ufax'))
	},
	uimDisplay$: function() {
		return Ext.String.format('<i class="icon-user rs-user-icon" data-qtip="{1}"></i> {0}', Ext.String.htmlEncode(this.uim), this.localize('uim'))
	},

	ustreet: '',
	ucity: '',
	ustate: '',
	uzip: '',
	ucountry: '',
	countryStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		data: RS.common.data.countries
	},

	uquestion1: '',
	uquestion2: '',
	uquestion3: '',
	uanswer1: '',
	uanswer2: '',
	uanswer3: '',

	ustartPage: '',
	uhomePage: '',

	sysCreatedOn: '',
	sysCreatedOnDisplay$: function() {
		return Ext.isDate(this.sysCreatedOn) ? Ext.Date.format(this.sysCreatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysUpdatedOn: '',
	sysUpdatedOnDisplay$: function() {
		return Ext.isDate(this.sysUpdatedOn) ? Ext.Date.format(this.sysUpdatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysCreatedBy: '',
	sysUpdatedBy: '',

	sysOrganizationName: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	contributions: 0,
	commentsReceived: 0,
	likesReceived: 0,

	contributionsText$: function() {
		return Ext.String.format('<font style="font-size: 18px">{0}</font><br/><span style="text-align:center">{1}</span>', this.contributions, this.localize('contributions'))
	},
	commentsReceivedText$: function() {
		return Ext.String.format('<font style="font-size: 18px">{0}</font><br/><span style="text-align:center">{1}</span>', this.commentsReceived, this.localize('commentsReceived'))
	},
	likesReceivedText$: function() {
		return Ext.String.format('<font style="font-size: 18px">{0}</font><br/><span style="text-align:center">{1}</span>', this.likesReceived, this.localize('likesReceived'))
	},

	uemailHasValue$: function() {
		return this.uemail
	},
	uhomePhoneHasValue$: function() {
		return this.uhomePhone
	},
	umobilePhoneHasValue$: function() {
		return this.umobilePhone
	},
	ufaxHasValue$: function() {
		return this.ufax
	},
	uimHasValue$: function() {
		return this.uim
	},

	blogTab: function() {
		this.set('activeTab', 0)
	},
	blogTabIsPressed$: function() {
		return this.activeTab == 0
	},
	blogTabIsVisible$: function() {
		return !this.copy
	},
	adminTab: function() {
		this.set('activeTab', 1)
	},
	adminTabIsPressed$: function() {
		return this.activeTab == 1
	},
	settingsTab: function() {
		this.set('activeTab', 2)
	},
	settingsTabIsPressed$: function() {
		return this.activeTab == 2
	},
	settingsTabIsVisible$: function() {
		return this.isMe
	},

	isMe$: function() {
		return this.id == clientVM.user.id
	},
	isMeCls$: function() {
		return this.isMe ? '' : 'user-isNotMe'
	},
	showRecoveryOptions$: function() {
		return this.isMe
	},

	uhomePageIsVisible$ : function(){
		return this.ustartPage == 'wiki';
	},
	adminTabIsVisible: false,
	init: function() {
		this.set('followersTpl', Ext.create('Ext.XTemplate','<tpl for="."></tpl>'));
		this.set('followingTpl', Ext.create('Ext.XTemplate','<tpl for="."></tpl>'));
		this.initToken();
		if (this.mock) this.backend = RS.user.createMockBackend(true)

		this.startPageStore.add([{
			name: this.localize('menu'),
			value: 'menu'
		}, {
			name: this.localize('uhomePage'),
			value: 'wiki'
		}, {
			name: this.localize('sirDashboard'),
			value: 'sirDashboard'
		}, {
			name: this.localize('social'),
			value: 'social'
		}, {
			name: this.localize('defaultText'),
			value: 'default'
		}])

		this.startPageStore.sort('name')

		this.aceLanguageStore.sort(function(a, b) {
			if (a.name < b.name) return -1
			if (a.name > b.name) return 1
			return 0
		})

		this.aceKeybindingStore.sort(function(a, b) {
			if (a.name < b.name) return -1
			if (a.name > b.name) return 1
			return 0
		})

		this.userDefaultDateFormatStore.insert(0, {
			name: this.localize('defaultDate'),
			value: 'Y-m-d g:i:s A'
		})

		this.followers.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}

			Ext.apply(operation.params, {
				streamId: this.id
			})
		}, this)
		/*
		this.following.on('load', function(store, records, successfull) {
			debugger;
		}, this)
		*/

		this.set('noRoleOrGroupError', this.localize('noRoleOrGroupError'));
		this.roles.on('datachanged', this.updateRolesGroupsCls, this);
		this.groups.on('datachanged', this.updateRolesGroupsCls, this);
	},

	followersTpl: '',
	followingTpl: '',

	loadUser: function() {
		this.roles.removeAll()
		this.groups.removeAll()
		this.organizations.removeAll();
		if ((this.create === true || this.create === 'true') && !this.id) return

		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/user/getUser',
			params: {
				username: this.username || this.uuserName
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.loadData(response.data)
					this.checkRecoveryOptions();
					this.set('lastLoaded', new Date())
					this.socialDetail.setInitialStream(this.id)
					this.socialDetail.set('initialStreamType', 'user')
					this.socialDetail.set('streamParent', response.data.udisplayName)

					//Now load the followers since we'll have the user id
					this.followers.load()
					this.following.load()

					this.ajax({
						url: '/resolve/service/social/userStats',
						params: {
							username: this.uuserName
						},
						scope: this,
						success: function(r) {
							var response = RS.common.parsePayload(r)
							if (response.success) {
								this.set('contributions', response.data.postContributions + response.data.commentContributions)
								this.set('commentsReceived', response.data.commentsReceived)
								this.set('likesReceived', response.data.likesReceivedForPost + response.data.likesReceivedForComment)
							} else clientVM.displayError(response.message)

						},
						failure: function(r) {
							clientVM.displayFailure(r);
						},
						callback: function() {
							this.set('wait', false);
						}
					})

					if (this.copy) {
						this.set('cloneUsername', this.uuserName)
						this.set('id', '')
						this.set('uuserName', '')
						this.set('ufirstName', '')
						this.set('ulastName', '')

						this.set('sysCreatedBy', '')
						this.set('sysCreatedOn', '')
						this.set('sysUpdatedBy', '')
						this.set('sysUpdatedOn', '')
					}

					//correct password
					this.set('uuserPassword', '**********')

					//correct booleans
					this.set('upasswordNeedsReset', response.data.upasswordNeedsReset)
					this.set('ulockedOut', response.data.ulockedOut)

					//load roles and groups grids
					this.roles.loadData(response.data.userRoles || []);
					this.groups.loadData(response.data.userGroups || []);
					this.organizations.loadData(response.data.userOrgs || []);

					//Load user setting
					this.loadUserSetting(response.data.userSettings);
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},

	recoveryOptionsRO: false,

	checkRecoveryOptions: function(data) {
		if (this.uquestion1 || this.uanswer1 || this.uquestion2 || this.uanswer2 || this.uquestion3 || this.uanswer3) {
			this.set('recoveryOptionsRO', true);
			if (data) {
				if (data.uquestion1) this.set('uquestion1', data.uquestion1);
				if (data.uanswer1) this.set('uanswer1', data.uanswer1);
				if (data.uquestion2) this.set('uquestion2', data.uquestion2);
				if (data.uanswer2) this.set('uanswer2', data.uanswer2);
				if (data.uquestion3) this.set('uquestion3', data.uquestion3);
				if (data.uanswer3) this.set('uanswer3', data.uanswer3);
			}
		} else {
			this.set('recoveryOptionsRO', false);
		}
	},

	resetRecoveryOptions: function() {
		this.set('uquestion1', '');
		this.set('uquestion2', '');
		this.set('uquestion3', '');
		this.set('uanswer1', '');
		this.set('uanswer2', '');
		this.set('uanswer3', '');
		this.set('recoveryOptionsRO', false);
	},

	refresh: function() {
		if (this.id) {
			this.loadUser();
		} else {
			this.resetForm();
		}
	},
	refreshIsEnabled$: function() {
		return !this.wait;
	},
	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.persistUser, this, [exitAfterSave, function() {
			if (this.create) {
				clientVM.handleNavigation({
					modelName: 'RS.user.User',
					params: {
						username: this.uuserName,
						activeTab: 1
					}
				});
			}
			clientVM.displaySuccess(this.localize('UserSaved'));
		}]);
		this.task.delay(500)
		this.set('wait', true)
	},
	saveIsEnabled$: function() {
		return !this.wait && this.isValid && this.rolesGroupsValid;
	},
	saveIsVisible$: function() {
		return this.activeTab > 0
	},
	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	wait: false,
	userSettingList : ['userDefaultDateFormat','autoRefreshEnabled','autoRefreshInterval','aceKeybinding','maxRecentlyUsed','sideMenuDisplay','bannerIsVisible'],
	loadUserSetting : function(settings){
		try {
			var settingObj = settings ? JSON.parse(settings) : {};
			for(var i = 0; i < this.userSettingList.length; i++){
				var setting = this.userSettingList[i];
				this.set(setting, settingObj[setting]);
			}
		}catch (e) {
			clientVM.displayError(this.localize('invalidUserSettings', this.username));
		}
	},
	getUserSetting : function(){
		var settingObj = {};
		for(var i = 0; i < this.userSettingList.length; i++){
			var setting = this.userSettingList[i];
			settingObj[setting] = this[setting];
		}
		return settingObj;
	},
	persistUser: function(exitAfterSave, onSuccessCallback) {

		//save the actual user
		var roles = [],
			groups = [];

		this.roles.each(function(role) {
			roles.push(role.data)
		})

		this.groups.each(function(group) {
			groups.push(group.data)
		})

		if (roles.length == 0 && groups.length == 0) {
			clientVM.displayError(this.localize('noRoleOrGroupError'))
			this.set('wait', false)
			return
		}

		//if we have custom fields, then store those to the db as well
		if (this.customFieldsForm)
			this.customFieldsForm.saveForm();

		var orgs = [];
		this.organizations.each(function(org){
			orgs.push(Ext.apply(org.raw,{
				isDefaultOrg : org.get('isDefaultOrg')
			}))
		})
		var userObj = Ext.apply(this.asObject(), {
			userRoles: roles,
			userGroups: groups,
			userSettings : JSON.stringify(this.getUserSetting()),
			userOrgs : orgs
		});

		//configure password information
		delete userObj.uuserPasswordConfirm
		if (userObj.uuserPassword == '**********') delete userObj.uuserPassword
		if (userObj.uuserPassword) {
			var pass = this.uuserPassword;
			if (pass[0] == '_') pass = pass.substring(1)
			// RBA-12838 Password will be hashed on server from Corona (5.4) onwards
			//userObj.uuserPassword = CryptoJS.enc.Base64.stringify(CryptoJS.SHA512(pass))
			userObj.uuserPassword = pass
		}

		if (this.cloneUsername) userObj.cloneUsername = this.cloneUsername

		this.ajax({
			url: '/resolve/service/user/saveUser',
			jsonData: userObj,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					if (exitAfterSave === true) {
						clientVM.handleNavigation({
							modelName: 'RS.user.Users'
						});
					} else if (Ext.isFunction(onSuccessCallback)) {
						onSuccessCallback.call(this);
					}
					if (!this.create) {
						this.checkRecoveryOptions(response.data);
					}
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.user.Users'
		})
	},

	backIsVisible$: function() {
		return this.adminTabIsVisible
	},

	rolesGroupsValid: false,
	noRoleOrGroupError: '',
	rolesGroupsCls: 'rs-grid-dark',

	updateRolesGroupsCls: function() {
		var rolesGroupsTipElements = document.getElementsByClassName('user-roles-groups-tip');
		if (this.roles.data.length || this.groups.data.length) {
			this.set('rolesGroupsValid', true);
			this.set('rolesGroupsCls', 'rs-grid-dark');
			for (var i=0; i<rolesGroupsTipElements.length; i++) {
				rolesGroupsTipElements[i].classList.add('tooltip-hidden');
			}
		} else {
			this.resetRolesGroups(rolesGroupsTipElements);
		}
	},

	resetRolesGroups: function(elements) {
		this.set('rolesGroupsValid', false);
		this.set('rolesGroupsCls', 'rs-grid-dark users-no-roles-no-groups');
		var rolesGroupsTipElements = elements || document.getElementsByClassName('user-roles-groups-tip');
		for (var i=0; i<rolesGroupsTipElements.length; i++) {
			rolesGroupsTipElements[i].classList.remove('tooltip-hidden');
		}
	},

	roles: {
		mtype: 'store',
		sorters: ['uname'],
		fields: ['id', 'uname'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	rolesSelections: [],
	addRole: function() {
		this.open({
			mtype: 'RS.sysadmin.RolePicker',
			includePublic : "false"
		})
	},
	removeRole: function() {
		Ext.Array.forEach(this.rolesSelections, function(r) {
			this.roles.remove(r)
		}, this)
	},
	removeRoleIsEnabled$: function() {
		return this.rolesSelections.length > 0
	},

	groups: {
		mtype: 'store',
		sorters: ['uname'],
		fields: ['id', 'uname'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	groupsSelections: [],
	addGroup: function() {
		this.open({
			mtype: 'RS.user.GroupPicker',
			callback : function(groupsSelections){
				Ext.Array.forEach(groupsSelections, function(r) {
					if (this.groups.indexOf(r) == -1)
						this.groups.add(r.data)
				}, this)
			}
		})
	},
	removeGroup: function() {
		Ext.Array.forEach(this.groupsSelections, function(r) {
			this.groups.remove(r)
		}, this)
	},
	removeGroupIsEnabled$: function() {
		return this.groupsSelections.length > 0
	},

	resetBrowserPreferences: function() {
		this.message({
			title: this.localize('resetTitle'),
			msg: this.localize('resetMessage'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('resetBrowserPreferences'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyResetBrowserPreferences
		})
	},
	reallyResetBrowserPreferences: function(btn) {
		if (btn == 'yes') {
			Ext.state.Manager.getProvider().getStorageObject().clear()
			if (Ext.supports.LocalStorage) Ext.state.Manager.setProvider(new Ext.state.LocalStorageProvider())
			else Ext.state.Manager.setProvider(new Ext.state.CookieProvider())
			this.resetBrowserDefaultValues()
		}
	},
	resetBrowserPreferencesIsVisible$: function() {
		return this.activeTab == 2
	},

	imageClicked: function() {
		if (this.isMe)
			this.open({
				mtype: 'RS.user.ProfileUpload'
			})
	},


	summaryPanel: null,
	registerSummaryPanel: function(panel) {
		this.set('summaryPanel', panel)
	},

	when_summary_changes_ensure_links_are_with_blank_target: {
		on: ['usummaryChanged'],
		action: function() {
			Ext.defer(function() {
				if (this.summaryPanel && this.summaryPanel.getEl()) {
					var links = this.summaryPanel.getEl().query('a'),
						currentLocation = window.location.protocol + '//' + window.location.host + window.location.pathname;
					Ext.Array.forEach(links, function(link) {
						if (link.href && link.href.indexOf(currentLocation) == -1) {
							link.target = '_blank'
						}
					})
				}
			}, 10, this)
		}
	},

	socialDetail: {
		mtype: 'RS.social.Main',
		mode: 'embed'
	},

	showCustomFields: false,
	customFieldsForm: null,
	formLoaded: function(form) {
		this.set('showCustomFields', true)
		this.set('customFieldsForm', form)
	},
	organizationsSelection : [],
	organizations: {
		mtype: 'store',
		sorters: ['uname'],
		fields: ['uname','isDefaultOrg']	,
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	setDefaultOrgIsEnabled$ : function(){
		return this.organizationsSelection.length > 0;
	},
	setDefaultOrg : function(){
		var record = this.organizationsSelection[0];
		if(record.get('isDefaultOrg'))
			record.set('isDefaultOrg', false);
		else {
			this.organizations.each(function(r){
				r.set('isDefaultOrg', r == record);
			})
		}
	},
	changePassword: function() {
		this.open({
			mtype: 'RS.user.ChangePasswordForm'
		});
	}
});

glu.defModel('RS.user.ChangePasswordForm', {
	mixins: ['PasswordsValidation'],
	uuserOldPassword: '',
	currentUser: true,
	init: function() {
		this.set('currentUser', this.parentVM.uuserName ===clientVM.user.name);
	},
	applyIsEnabled$: function() {
		return ((this.currentUser && this.uuserOldPassword) || !this.currentUser) && this.uuserPassword
			&& (this.uuserPasswordIsValid$() == true) && this.uuserPasswordConfirm == this.uuserPassword;
	},
	apply: function() {
		this.ajax({
			url: '/resolve/service/user/changePassword',
			jsonData: {
				username:  this.parentVM.uuserName,
				oldPassword: this.uuserOldPassword,
				newPassword: this.uuserPassword
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					clientVM.displaySuccess(this.localize('successfully_Changed_Pas_sword'));
					this.close();
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	cancel: function() {
		this.close();
	}
});

glu.defModel('RS.user.UserPicker', {
	pick: false,
	keyword : '',
	delayedTask : null,
	when_keyword_changed: {
		on: ['keywordChanged'],
		action: function() {
			if(this.delayedTask)
				clearTimeout(this.delayedTask);
			this.delayedTask = setTimeout(function(){
				this.users.loadPage(1);
			}.bind(this),300)		
		}
	},
	pickerTitle$: function() {
		return this.pick ? this.localize('selectUserTitle') : this.localize('addUserTitle')
	},
	usersSelections : [],
	users: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uuserName'],
		fields: ['id', 'uuserName', 'ufirstName', 'ulastName', 'uemail', 'udisplayName'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/user/listUsers',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		},
		autoLoad : true
	},
	init: function() {	
		this.users.on('beforeload', function(store, operation, opts) {
			operation.params = operation.params || {};		
			if (this.keyword) {
				Ext.apply(operation.params, {
					filter: Ext.encode([{
						field: 'uuserName',
						type: 'auto',
						condition: 'contains',
						value: this.keyword
					}])			
				})
			}
		}, this);		
	},

	cancel: function() {
		this.doClose()
	},
	add: function() {
		if (this.callback)
			this.callback.apply(this.parentVM, [this.usersSelections])		
		this.doClose();
	},
	addIsEnabled$: function() {
		return this.usersSelections.length > 0
	},
	addIsVisible$: function() {
		return !this.pick
	},
	select: function() {
		this.callback.apply(this.parentVM, [this.usersSelections])
		this.doClose();
	},
	selectIsEnabled$: function() {
		return this.addIsEnabled
	},
	selectIsVisible$: function() {
		return this.pick
	}
});
glu.defModel('RS.user.Users', {

	mock: false,

	activate: function() {
		this.users.load()
		clientVM.setWindowTitle(this.localize('allUsers'))
	},

	columns: [],
	usersSelections: [],
	allSelected: false,

	displayName: '',
	stateId: 'users',

	users: {
		mtype: 'store',
		sorters: ['uuserName'],
		remoteSort: true,
		fields: ['id', 'uuserName', 'ufirstName', 'ulastName', {
			name: 'ulockedOut',
			type: 'bool'
		}, 'roles', 'groups', 'utitle', 'uemail', 'uphone', 'umobilePhone', {
			name: 'ulastLogin',
			type: 'date',
			dateFormat: 'c',
			format: 'c'
		}, 'ulastLoginDevice'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/user/listUsers',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.user.createMockBackend(true)

		this.set('columns', [{
			text: '~~userId~~',
			dataIndex: 'uuserName',
			filterable: true,
			sortable: true,
			width: 200
		}, {
			text: '~~uname~~',
			dataIndex: 'ufirstName',
			filterable: true,
			sortable: true,
			width: 200,
			renderer: function(value, meta, record) {
				return (value || '') + ' ' + (record.get('ulastName') || '')
			}
		}, {
			text: '~~locked~~',
			dataIndex: 'ulockedOut',
			filterable: true,
			sortable: true,
			width: 65,
			align: 'center',
			renderer: RS.common.grid.booleanRenderer()
		}, {
			text: '~~roles~~',
			dataIndex: 'roles',
			filterable: false,
			sortable: false,
			flex: 1
		}, {
			text: '~~groups~~',
			dataIndex: 'groups',
			filterable: false,
			sortable: false,
			flex: 1
		}, {
			text: '~~title~~',
			dataIndex: 'utitle',
			filterable: true,
			sortable: true,
			hidden: true
		}, {
			text: '~~email~~',
			dataIndex: 'uemail',
			filterable: true,
			sortable: true,
			hidden: true
		}, {
			text: '~~phone~~',
			dataIndex: 'uphone',
			filterable: true,
			sortable: true,
			hidden: true
		}, {
			text: '~~mobilePhone~~',
			dataIndex: 'umobilePhone',
			filterable: true,
			sortable: true,
			hidden: true
		}, {
			text: '~~lastLogin~~',
			dataIndex: 'ulastLogin',
			filterable: true,
			sortable: true,
			width: 200,
			hidden: true,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}, {
			text: '~~ipAddress~~',
			dataIndex: 'ulastLoginDevice',
			filterable: true,
			sortable: true,
			hidden: true
		}].concat(RS.common.grid.getSysColumns()))

		this.set('displayName', this.localize('userDisplayName'))
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createUser: function() {
		clientVM.handleNavigation({
			modelName: 'RS.user.User',
			params: {
				activeTab: 1,
				create: true
			}
		})
	},
	cloneUser: function() {
		clientVM.handleNavigation({
			modelName: 'RS.user.User',
			params: {
				username: this.usersSelections[0].get('uuserName'),
				copy: true
			}
		})
	},
	cloneUserIsEnabled$: function() {
		return this.usersSelections.length == 1
	},
	editUser: function(username) {
		clientVM.handleNavigation({
			modelName: 'RS.user.User',
			params: {
				username: username,
				activeTab: 1
			}
		})
	},

	assignRoles: function() {
		this.open({
			mtype: 'RS.sysadmin.RolePicker',
			callback: this.reallyAssignRoles,
			includePublic : "false"
		})
	},
	reallyAssignRoles: function(roles) {
		var userIds = [],
			roleIds = [];

		Ext.Array.forEach(this.usersSelections, function(user) {
			userIds.push(user.get('id'))
		})

		Ext.Array.forEach(Ext.Array.from(roles), function(role) {
			roleIds.push(role.get('id'))
		})

		this.ajax({
			url: '/resolve/service/user/assignRoles',
			params: {
				userSysIds: userIds,
				rolesSysIds: roleIds
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.users.load()
					clientVM.displaySuccess(this.localize('rolesAdded'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	assignRolesIsEnabled$: function() {
		return this.usersSelections.length > 0
	},
	removeRoles: function() {
		var roleNames = [];
		Ext.Array.forEach(this.usersSelections || [], function(selection) {
			var record = this.users.getById(selection.get('id'));
			var roles = record.get('roles');
			Ext.Array.forEach(roles || [], function(item) {
				roleNames.push(Ext.String.trim(item))
			})
		}, this)
		if (roleNames.length) {
			this.open({
				mtype: 'RS.user.RemoveRolePicker',
				callback: this.reallyRemoveRoles,
				isRemove: true,
				roleNames: roleNames
			})
		} else {
			clientVM.displayError(this.localize('noRoles'));
		}
	},
	reallyRemoveRoles: function(roles) {
		var userIds = [],
			roleNames = [];

		Ext.Array.forEach(this.usersSelections, function(user) {
			userIds.push(user.get('id'))
		})

		Ext.Array.forEach(Ext.Array.from(roles), function(role) {
			roleNames.push(role.get('name'))
		})

		this.ajax({
			url: '/resolve/service/user/removeRoles',
			params: {
				userSysIds: userIds,
				roles: roleNames
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.users.load()
					clientVM.displaySuccess(this.localize('rolesRemoved'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	removeRolesIsEnabled$: function() {
		return this.usersSelections.length > 0
	},
	assignGroups: function() {
		this.open({
			mtype: 'RS.user.GroupPicker',
			callback: this.reallyAssignGroups
		})
	},
	reallyAssignGroups: function(groups) {
		var userIds = [],
			groupIds = [];

		Ext.Array.forEach(this.usersSelections, function(user) {
			userIds.push(user.get('id'))
		})
		Ext.Array.forEach(Ext.Array.from(groups), function(group) {
			groupIds.push(group.get('id'))
		})

		this.ajax({
			url: '/resolve/service/user/assignGroups',
			params: {
				userSysIds: userIds,
				groupSysIds: groupIds
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.users.load()
					clientVM.displaySuccess(this.localize('groupsAdded'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	assignGroupsIsEnabled$: function() {
		return this.usersSelections.length > 0
	},
	removeGroups: function() {
		var groupNames = [];
		Ext.Array.forEach(this.usersSelections || [], function(selection) {
			var record = this.users.getById(selection.get('id'));
			var groups = record.get('groups');
			Ext.Array.forEach(groups || [], function(item) {
				if (Ext.Array.indexOf(groupNames, Ext.String.trim(item)) == -1)
					groupNames.push(Ext.String.trim(item))
			})
		}, this)
		if (groupNames.length) {
			this.open({
				mtype: 'RS.user.RemoveGroupPicker',
				callback: this.reallyRemoveGroups,
				isRemove: true,
				groupNames: groupNames
			})
		} else {
			clientVM.displayError(this.localize('noGroups'));
		}
	},
	reallyRemoveGroups: function(groups) {
		var userIds = [],
			groupNames = [];

		Ext.Array.forEach(this.usersSelections, function(user) {
			userIds.push(user.get('id'))
		})
		Ext.Array.forEach(Ext.Array.from(groups), function(group) {
			groupNames.push(group.get('name'))
		})

		this.ajax({
			url: '/resolve/service/user/removeGroups',
			params: {
				userSysIds: userIds,
				groups: groupNames
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.users.load()
					clientVM.displaySuccess(this.localize('groupsRemoved'))
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},
	removeGroupsIsEnabled$: function() {
		return this.usersSelections.length > 0
	},
	deleteUserIsEnabled$: function() {
		return this.usersSelections.length > 0
	},
	deleteUser: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.usersSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.usersSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/user/deleteUser',
					params: {
						ids: [],
						whereClause: this.user.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('usersSelections', [])
							this.users.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			} else {
				var ids = []
				Ext.each(this.usersSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/user/deleteUser',
					params: {
						ids: ids
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('usersSelections', [])
							this.users.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	}
});
glu.defView('RS.user.Group', {
	autoScroll: true,
	padding: 15,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		margin : '0 0 15 0',
		cls: 'title-toolbar rs-dockedtoolbar',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{groupTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',		
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],

	items: [{
		style : 'border-top:1px solid silver',
		padding : '8 0 0 0',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},		
		items: [{
			flex: 1,
			margin : {
				right : 10
			},
			defaults : {
				margin : '4 0'
			},
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				name: 'uname',
				xtype: 'textfield'
			}, {
				name: 'udescription',
				xtype: 'textarea',
				height : 150
			}]
		}, {
			flex: 1,		
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				name: 'uisLinkToTeam',
				xtype: 'checkbox',
				hideLabel: true,
				boxLabel: '~~linkToTeam~~',
				padding: '0px 0px 0px 105px',
				tooltip: '~~linkToTeamTooltip~~',
				listeners: {
					render: function(checkbox) {
						Ext.create('Ext.tip.ToolTip', {
							html: checkbox.tooltip,
							target: checkbox.getEl()
						})
					}
				}
			}, {
				name: 'uhasExternalLink',
				xtype: 'checkbox',
				hideLabel: true,
				boxLabel: '~~linkToExternalGroup~~',
				padding: '0px 0px 0px 105px',
				tooltip: '~~linkToExternalGroupTooltip~~',
				listeners: {
					render: function(checkbox) {
						Ext.create('Ext.tip.ToolTip', {
							html: checkbox.tooltip,
							target: checkbox.getEl()
						})
					}
				}
			}]
		}]
	},{
		xtype : 'container',
		flex : 1,
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		defaults : {
			maxHeight : 300
		},
		items : [{
			title: '~~roles~~',
			xtype: 'grid',
			flex : 1,
			cls: '@{rolesCls}',
			name: 'roles',
			columns:  [{
				header: '~~sysId~~',
				dataIndex: 'id',
				hidden: true,
				autoWidth: true,
				sortable: false,
				filterable: false
			}, {
				header: '~~uname~~',
				dataIndex: 'uname',
				filterable: true,
				sortable: true,
				flex: 1
			}],
			dockedItems : [{
				xtype : 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items : ['addRole', 'removeRole']
			}],
			qtip: '@{noRolesError}',
			listeners: {
				afterrender: function(form) {
					//Configure proper tooltip on the title
					if (form.getHeader()) {
						var headerElem = form.getHeader().getEl();
						var toolTipElem = headerElem.down('.x-header-text.x-panel-header-text');
						Ext.create('Ext.tip.ToolTip', {
							cls: 'group-roles-tip',
							target: toolTipElem,
							html: this.qtip,
							autoShow: true,
							listeners: {
								boxready: function() {
									this.hide();
								}
							}
						})
					}
				},
			}
		}, {
			title: '~~users~~',
			xtype: 'grid',
			flex : 1,
			margin : '0 10',
			cls : 'rs-grid-dark',		
			name: 'users',		
			dockedItems : [{
				xtype : 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items : ['addUser', 'removeUser']
			}],
			columns: [{
				header: '~~userId~~',
				dataIndex: 'uuserName',
				filterable: true,
				sortable: true,
				flex: 1
			}, {
				header: '~~uname~~',
				dataIndex: 'ufirstName',
				filterable: true,
				sortable: true,
				flex: 1,
				renderer: function(value, meta, record) {
					return (value || '') + ' ' + (record.get('ulastName') || '')
				}
			}, {
				header: '~~email~~',
				dataIndex: 'uemail',
				filterable: true,
				sortable: true,
				flex: 1
			}]	
		},{
			title: '~~organizations~~',
			xtype: 'grid',
			flex : 1,		
			cls : 'rs-grid-dark',		
			name: 'organizations',
			dockedItems : [{
				xtype : 'toolbar',
				cls : 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items : ['addOrganization', 'removeOrganization']
			}],
			columns: [{
				header: '~~uname~~',
				dataIndex: 'uname',
				filterable: true,
				sortable: true,
				flex: 1
			}]	
		}]
	}]
});

glu.defView('RS.user.GroupPicker', {
	title: '@{title}',
	padding : 15,
	cls : 'rs-modal-popup',
	modal: true,	
	layout: 'fit',
	width: 800,
	height: 450,
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		margin : {
			bottom : 10
		},
		name: 'groups',
		columns: '@{columns}',
		plugins: [{
			ptype: 'pager'
		}],
		selModel: {
			mode: 'MULTI'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'keyword',
				width : 400				
			}]
		}],
	}],	
	buttons: [{
		name :'add',
		cls : 'rs-med-btn rs-btn-dark'
	},{ 
		name :  'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});
glu.defView('RS.user.Groups', {
	xtype: 'grid',
	padding: 15,
	name: 'groups',
	cls : 'rs-grid-dark',
	border: false,
	stateId: '@{stateId}',
	displayName: '@{displayName}',
	stateful: true,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		childLink: 'RS.user.Group',
		childLinkIdProperty: 'id'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls :'rs-small-btn rs-btn-light'
		},
		items: ['createGroup', 'copyGroup', 'deleteGroup']
	}],
	columns: '@{columns}',
	viewConfig: {
		enableTextSelection: true
	},
	listeners: {
		editAction: '@{editGroup}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});
glu.defView('RS.user.Organization', {
	autoScroll: true,
	padding: 15,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		margin : '0 0 15 0',
		cls: 'title-toolbar rs-dockedtoolbar',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{organizationTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',		
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],

	items: [{
		style : 'border-top:1px solid silver',
		padding : '8 0 0 0',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			flex: 1,
			margin : {
				right : 10
			},
			defaults : {
				margin : '4 0',
				labelWidth : 150
			},
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				name: 'uname',
				xtype: 'textfield',
				maxLength: 200,
				enforceMaxLength: true
			}, {
				name: 'udescription',
				xtype: 'textarea',
				height : 150
			},{
				name : 'parentOrgId',
				store : '@{parentOrgStore}',
				maxWidth : 550,
				defaultValue : 'None',
				editable : false,
				xtype : 'combo',
				displayField : 'uname',
				valueField: 'id',
				queryMode: 'local'
			},{
				name : 'uhasNoOrgAccess',
				xtype : 'checkbox',
				hideLabel : true,
				boxLabel : '~~uhasNoOrgAccess~~',
				margin : '4 0 4 155'
			}]
		}]
	}, {
		title: '~~groups~~',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		minHeight: 400,		
		name: 'groups',
		columns: [{
			header: '~~sysId~~',
			dataIndex: 'id',
			hidden: true,
			autoWidth: true,
			sortable: false,
			filterable: false
		}, {
			header: '~~uname~~',
			dataIndex: 'uname',
			filterable: true,
			sortable: true,
			flex: 1
		}],
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items : ['addGroup', 'removeGroup']
		}]	
	}]
});

glu.defView('RS.user.OrganizationPicker',{
	title: '~~addOrganization~~',
	padding : 15,	
	modal: true,
	cls : 'rs-modal-popup',
	layout: 'fit',	
	width: 800,
	height: 450,
	items : [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'organizations',
		columns: [{
			header : '~~uname~~',
			dataIndex : 'uname',
			width : 300,
		},{
			header : '~~udescription~~',
			dataIndex : 'udescription',
			flex : 1
		}],
		plugins: [{
			ptype: 'pager'
		}],
		selModel: {
			mode: 'MULTI'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'keyword',
				width : 400
			}]
		}],	
	}],	
	buttons: [{
		name :'add',
		cls : 'rs-med-btn rs-btn-dark'
	},{ 
		name :  'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})
glu.defView('RS.user.Organizations', {
	xtype: 'grid',
	padding: 15,
	cls : 'rs-grid-dark',
	displayName: '~~organizationDisplayName~~',
	store : '@{organizations}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		childLink: 'RS.user.Organization',
		childLinkIdProperty: 'id'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls :'rs-small-btn rs-btn-light'
		},
		items: ['createOrganization']
	}],
	columns: [{
			header: '~~uname~~',
			dataIndex: 'uname',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~udescription~~',
			dataIndex: 'udescription',
			filterable: false,
			sortable: false,
			flex: 1
		}, {
			header: '~~groups~~',
			dataIndex: 'groups',
			filterable: false,
			sortable: false,
			flex: 1
	}].concat(RS.common.grid.getSysColumns())	
});
glu.defView('RS.user.ProfileUpload', {
	asWindow: {
		//height: '@{windowHeight}',
		//width: '@{windowWidth}',
		height: 500,
		width: 800,
		modal: true,
		padding : 15,
		cls : 'rs-modal-popup',
		resizable: false,
		draggable: false,
		title: '~~uploadProfilePicTitle~~',
		closable: true,
		listeners: {
			show: function(win) {
				win.fireEvent('setupSizes', win, win)
			},
			setupSizes: '@{setupSizes}'
		}
	},

	layout: 'card',
	activeItem: '@{activeItem}',
	items: [{
		margin: '0 0 10 0',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			html: '@{intro}',
			margin: '0 0 10 0',
		}, {
			itemId: 'fineUploaderTemplate',
			html: '@{fineUploaderTemplate}',
			height: '@{fineUploaderTemplateHeight}',
			hidden: '@{fineUploaderTemplateHidden}',
		}, {
			id: 'attachments_grid',
			hidden: true
		}]
	},{
		style: {
			'background-color': '#f9f9f9'
		},
		bodyStyle: {
			'margin-left': 'auto',
			'margin-right': 'auto'
		},
		layout: {
			type: 'vbox',
			align: 'center'
		},
		src: '@{previewUrl}',
		setSrc: function(url) {
			if (!url) return
			var me = this,
				ic = new ICropper(me.body.el.dom, {
					keepSquare: true,
					image: url,
					onChange: function(info) {
						me.fireEvent('changeCrop', me, info)
					}
				});
		},
		listeners: {
			changeCrop: '@{changeCrop}'
		}
	}],
	buttons: [{
		xtype: 'button',
		name: 'browse',
		preventDefault: false,
		id: 'upload_button',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});

/*
glu.defView('RS.user.ProfileUpload', {
	asWindow: {
		height: '@{windowHeight}',
		width: '@{windowWidth}',
		modal: true,
		padding : 15,
		cls : 'rs-modal-popup',
		resizable: false,
		draggable: false,
		title: '~~uploadProfilePicTitle~~',
		listeners: {
			show: function(win) {
				win.fireEvent('setupSizes', win, win)
			},
			setupSizes: '@{setupSizes}'
		}
	},

	layout: 'card',
	activeItem: '@{activeItem}',
	items: [{
		padding: '80px 0px 0px 0px',
		layout: {
			type: 'vbox',
			align: 'center'
		},
		items: [{
			html: '<i style="color: #666" class="icon-picture icon-5x"></i>'
		}, {
			padding: '10px',
			html: '@{dragText}'
		}, {
			xtype: 'button',
			text: '~~clickToUpload~~',
			cls : 'rs-small-btn rs-btn-light',
			itemId: 'clickToUpload'
		}, {
			xtype: 'progressbar',
			margin: '20px 0px 0px 0px',
			hidden: '@{!showProgress}',
			flex: 1
		}],
		uploader: {
			autoStart: true,
			runtimes: '',
			url: '/resolve/service/user/upload',
			browse_button: 'getAtRuntime',
			max_file_size: '128mb',
			resize: '',
			flash_swf_url: '/resolve/js/plupload/plupload.flash.swf',
			silverlight_xap_url: '',
			filters: [{
				title: "Image files",
				extensions: "jpg,jpeg,png,gif"
			}],
			//chunk_size: '1mb', // @see http://www.plupload.com/punbb/viewtopic.php?id=1259
			chunk_size: null,
			unique_names: true,
			multipart: true,
			multipart_params: {},
			multi_selection: false,
			// drop_element: null,
			required_features: null
		},

		uploaderListeners: {
			beforestart: function(uploader, data) {
				// console.log('beforestart')
			},
			uploadready: function(uploader, data) {
				// console.log('uploadready')
			},
			uploadstarted: function(uploader, data) {
				uploader.uploader.disableBrowse(true);
				var owner = this.owner,
					progressBar = owner.down('#progress');
				owner.percent = 0;
				Ext.TaskManager.start({
					run: function() {
						progressBar.updateProgress(owner.percent / 100, owner.percent + '%');
						if (owner.percent == 100) {
							return false;
						}
					},
					interval: 100
				})
			},
			uploadcomplete: function(uploader, data) {
				this.owner.fireEvent('uploadComplete', this.owner, uploader, data);
				var btnId = uploader.browse_button;
				uploader.uploader.disableBrowse(false);
			},
			uploaderror: function(uploader, data) {
				this.owner.fireEvent('uploadError', this.owner, uploader, data);
			},
			filesadded: function(uploader, data) {
				this.owner.fireEvent('filesAdded', this.owner, uploader, data);
			},
			beforeupload: function(uploader, data) {
				// console.log('beforeupload')
			},
			fileuploaded: function(uploader, data) {
				// console.log('fileuploaded')
			},
			uploadprogress: function(uploader, file, name, size, percent) {
				this.owner.percent = file.percent;
			},
			storeempty: function(uploader, data) {
				// console.log('storeempty')
			}
		},
		listeners: {
			uploadError: '@{uploadError}',
			filesAdded: '@{filesAdded}',
			uploadComplete: '@{uploadComplete}',
			afterrender: function(panel) {
				Ext.defer(function() {
					panel.myUploader = Ext.create('RS.common.SinglePlupUpload', panel)
					var browseBtnName = panel.uploader.browse_button,
						browseBtn = panel.down('#clickToUpload');
					panel.uploader.browse_button = browseBtn.id
					panel.uploader.container = browseBtn.ownerCt.id
					panel.uploader.drop_element = panel.body.el.id
					panel.myUploader.initialize(panel)
				}, 100, this)
			}
		}
	}, {
		style: {
			'background-color': '#f9f9f9'
		},
		bodyStyle: {
			'margin-left': 'auto',
			'margin-right': 'auto'
		},
		layout: {
			type: 'vbox',
			align: 'center'
		},
		src: '@{previewUrl}',
		setSrc: function(url) {
			if (!url) return
			var me = this,
				ic = new ICropper(me.body.el.dom, {
					keepSquare: true,
					image: url,
					onChange: function(info) {
						me.fireEvent('changeCrop', me, info)
					}
				});
		},
		listeners: {
			changeCrop: '@{changeCrop}'
		}
	}],
	buttons: [{
		name : 'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});
*/
glu.defView('RS.user.Analytics', {
	xtype: 'container',
	hidden: '@{hidden}',
	items: [{
		xtype: 'container',
		items: '@{permissionList}'
	}]
});

glu.defView('RS.user.AnalyticsCheckboxGroup', {
	xtype: 'checkboxgroup',
	cls: '@{labelStyle}',
	fieldLabel: '@{label}',
	labelWidth: 150,
	items: '@{groupPermissions}',
	defaults: {
	},
});

glu.defView('RS.user.AnalyticsCheckbox', {
	xtype: 'checkbox',
	cls: 'rs-rbac-checkbox',
	boxLabel: '@{label}',
	checked: '@{checked}',
	listeners: {
		change: function (checkbox, newValue) {
			this.fireEvent('handlePermissionChange', null, newValue);
		},
		handlePermissionChange: '@{handlePermissionChange}',
	}
});
glu.defView('RS.user.SIRPermissions', {
	xtype: 'container',
	hidden: '@{hidden}',
	items: [{
		xtype: 'container',
		items: '@{permissionList}'
	}]
});

glu.defView('RS.user.SIRCheckboxGroup', {
	xtype: 'checkboxgroup',
	cls: '@{labelStyle}',
	fieldLabel: '@{label}',
	labelWidth: 150,
	items: '@{groupPermissions}',
	defaults: {
	},
});

glu.defView('RS.user.SIRCheckbox', {
	xtype: 'checkbox',
	cls: 'rs-rbac-checkbox',
	boxLabel: '@{label}',
	checked: '@{checked}',
	listeners: {
		change: function (checkbox, newValue) {
			this.fireEvent('handlePermissionChange', null, newValue);
		},
		handlePermissionChange: '@{handlePermissionChange}',
	}
});
glu.defView('RS.user.RemoveGroupPicker', {
	title: '~~removeGroupPickerTitle~~',
	width: 600,
	height: 400,
	padding : 15,
	modal : true,
	cls : 'rs-modal-popup',
	layout: 'fit',
	items: [{
		xtype: 'grid',
		margin : {
			bottom : 10
		},
		cls : 'rs-grid-dark',
		name: 'groups',
		columns: '@{columns}',
		selModel: {
			mode: 'MULTI'
		}
	}],	
	buttons: [{
		name : 'remove',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name :'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})
glu.defView('RS.user.RemoveRolePicker', {
	title: '~~removeRolePickerTitle~~',
	width: 600,	
	height: 400,
	padding : 15,
	layout: 'fit',
	cls : 'rs-modal-popup',
	modal : true,
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'roles',
		columns: '@{columns}',
		selModel: {
			mode: 'MULTI'
		},
		margin : {
			bottom : 10
		}
	}],
	buttons: [{
		name : 'remove',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name :'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})
glu.defView('RS.user.Role', {
	title: '@{windowTitle}',
	width: 800,
	padding : 15,
	modal: true,
	resizable: false,
	layout : 'fit',
	cls : 'rs-modal-popup',
	dock: 'top',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'textfield',
		name: 'uname'
	}, {
		xtype: 'textarea',
		name: 'udescription',
	}, {
		title: '~~permissions~~',
		id: 'rs-user-permissions',
		height: 500,
		overflowY: 'auto',
		dock: 'top',
		dockedItems: [{
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				margin: '5 0 0 0',
				items: [{
					xtype: 'combobox',
					fieldLabel: '~~app~~',
					queryMode: 'local',
					store: '@{appStore}',
					displayField: 'name',
					valueField: 'value',
					value: '@{selectedApp}',
					editable: false,
				}, {
					xtype: 'button',
					text: '~~applyPredefined~~',
					hidden: true
				}]
			}]
		}],
		items: [{
			xtype: '@{SIRPermissions}',
		},
		{
			xtype: '@{Analytics}',
		}]
	}],
	buttons: [{
		name : 'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});
glu.defView('RS.user.Roles', {
	xtype: 'grid',
	cls : 'rs-grid-dark',
	padding: 15,
	name: 'roles',
	border: false,
	stateId: '@{stateId}',
	displayName: '@{displayName}',
	stateful: true,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',	
		columnTooltip: RS.common.locale.editColumnTooltip,
		glyph : 0xF044,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		childLinkIdProperty: 'id'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['createRole', 'deleteRole']
	}],
	columns: '@{columns}',
	viewConfig: {
		enableTextSelection: true
	},
	listeners: {
		editAction: '@{editRole}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});
glu.defView('RS.user.User', {
	padding: 15,
	layout: 'card',
	activeItem: '@{activeTab}',
	dockedItems: [{
		xtype: 'toolbar',
		margin : '0 0 10 0',
		ui: 'display-toolbar',
		cls: 'rs-dockedtoolbar',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{userTitle}'
		}, '->', {
			name: 'blogTab',
			pressed: '@{blogTabIsPressed}'
		}, {
			name: 'adminTab',
			pressed: '@{adminTabIsPressed}'
		}, {
			name: 'settingsTab',
			pressed: '@{settingsTabIsPressed}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls :'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, {
			xtype: 'tbseparator',
			hidden: '@{!resetBrowserPreferencesIsVisible}'
		}, 'resetBrowserPreferences', '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			disabled: '@{!refreshIsEnabled}',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	defaults: {
		overflowY: 'auto',
	},
	items: [{
		style : 'border-top:1px solid silver',
		layout: 'border',
		items: [{
			region: 'west',
			split: true,
			width: 210,
			autoScroll: true,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				layout: {
					type: 'vbox',
					align: 'center'
				},
				items: [{
					xtype: 'image',
					cls: 'profile-pic @{isMeCls}',
					src: '@{profilePicUrl}',
					maxWidth: 100,
					maxHeight: 100,
					tooltip: '~~clickToChangeProfile~~',
					isMe: '@{isMe}',
					setIsMe: function(value) {
						this.isMe = value
						if (this.isMe) {
							if (!this.tooltipControl)
								this.tooltipControl = Ext.create('Ext.tip.ToolTip', {
									html: this.tooltip,
									target: this.getEl()
								})
						} else {
							if (this.tooltipControl) {
								this.tooltipControl.remove()
								this.tooltipControl.destroy()
								this.tooltipControl = null
							}
						}
					},
					listeners: {
						render: function(img) {
							img.getEl().on('click', function() {
								img.fireEvent('imageClicked', img)
							})
							img.getEl().on('load', function() {
								img.ownerCt.doLayout()
							})

							if (img.isMe)
								img.tooltipControl = Ext.create('Ext.tip.ToolTip', {
									html: img.tooltip,
									target: img.getEl()
								})
						},
						imageClicked: '@{imageClicked}'
					}
				}]
			}, {
				// html: '10 posts & comments | 20 comments received | 100 likes received',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [{
					html: '@{contributionsText}',
					bodyStyle: {
						'text-align': 'center'
					},
					flex: 1
				}, {
					html: '@{commentsReceivedText}',
					bodyStyle: {
						'text-align': 'center'
					},
					flex: 1
				}, {
					html: '@{likesReceivedText}',
					bodyStyle: {
						'text-align': 'center'
					},
					flex: 1
				}]
			}, {
				title: '~~contact~~',
				bodyPadding: '10px',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [{
					xtype: 'displayfield',
					htmlEncode: false,
					value: '@{uemailDisplay}',
					style: {
						overflow: 'ellipsis'
					},
					hidden: '@{!uemailHasValue}'
				}, {
					xtype: 'displayfield',
					htmlEncode: false,
					value: '@{uhomePhoneDisplay}',
					hidden: '@{!uhomePhoneHasValue}'
				}, {
					xtype: 'displayfield',
					htmlEncode: false,
					value: '@{umobilePhoneDisplay}',
					hidden: '@{!umobilePhoneHasValue}'
				}, {
					xtype: 'displayfield',
					htmlEncode: false,
					value: '@{ufaxDisplay}',
					hidden: '@{!ufaxHasValue}'
				}, {
					xtype: 'displayfield',
					htmlEncode: false,
					value: '@{uimDisplay}',
					hidden: '@{!uimHasValue}'
				}]
			}, {
				title: '~~summary~~',
				bodyPadding: '10px',
				html: '@{usummary}',
				style: 'word-wrap:break-word;',
				listeners: {
					render: function(panel) {
						panel.fireEvent('registerSummary', panel, panel)
					},
					registerSummary: '@{registerSummaryPanel}'
				}

			}]
		}, {
			region: 'center',
			xtype: '@{socialDetail}'
		}, {
			region: 'east',
			width: 200,
			split: true,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				title: '~~followers~~',
				flex: 1,
				layout: 'fit',
				bodyPadding: '10px',
				items: [{
					autoScroll: true,
					xtype: 'dataview',
					tpl: '@{followersTpl}',
					itemSelector: 'div.thumb-wrap',
					emptyText: '~~noFollowers~~',
					store: '@{followers}'
				}]
			}, {
				title: '~~following~~',
				flex: 1,
				layout: 'fit',
				bodyPadding: '10px',
				items: [{
					autoScroll: true,
					xtype: 'dataview',
					tpl: '@{followingTpl}',
					itemSelector: 'div.thumb-wrap',
					emptyText: '~~noFollowing~~',
					store: '@{following}'
				}]
			}]
		}]
	}, {
		style : 'border-top:1px solid silver',
		padding : '8 0 0 0',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaults : {
				margin : {
					right : 10
				}
			},
			items: [{
				flex: 1,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaultType: 'textfield',
				defaults: {
					labelWidth: 130,
					margin : '4 0'
				},
				items: [{
					name: 'uuserName',
					hidden: '@{isEdit}'
				}, {
					xtype: 'displayfield',
					cls :'rs-displayfield-value',
					value: '@{uuserName}',
					fieldLabel: '~~uuserName~~',
					hidden: '@{!isEdit}',
					htmlEncode : true
				},{
					xtype : 'fieldcontainer',
					fieldLabel : '~~fullName~~',
					labelWidth : 130,
					layout : {
						type : 'hbox',
						align : 'stretch'
					},
					defaultType :'textfield',
					defaults : {
						hideLabel : true,
						flex : 1
					},
					items : [{
						name :'ufirstName',
						emptyText : '~~ufirstName~~',
						margin : '0 8 0 0'
					},{
						name : 'ulastName',
						emptyText : '~~ulastName~~'
					}]
				},{
					name: 'utitle'
				}, {
					name: 'uuserPassword',
					inputType: 'password',
					selectOnFocus: true,
					hidden: '@{isEdit}'
				}, {
					name: 'uuserPasswordConfirm',
					inputType: 'password',
					selectOnFocus: true,
					hidden: '@{isEdit}'
				}, {
					xtype: 'checkbox',
					name: 'upasswordNeedsReset',
					margin: '4 0 4 135',
					hideLabel: true,
					boxLabel: '~~upasswordNeedsReset~~'
				}, {
					xtype: 'checkbox',
					name: 'ulockedOut',
					margin: '0 0 0 135',
					hideLabel: true,
					boxLabel: '~~ulockedOut~~'
				}, {
					xtype: 'fieldcontainer',
					hidden: '@{!isEdit}',
					fieldLabel: ' ',
					labelSeparator: '',
					items: [{
						xtype: 'button',
						cls: 'rs-btn-dark rs-small-btn',
						name: 'changePassword'
					}]
				}]
			}, {
				flex: 1,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaults : {
					labelWidth : 130,
					margin : '4 0'
				},
				defaultType: 'textfield',
				items: [{
					xtype: 'htmleditor',
					name: 'usummary',
					enableSourceEdit: false
				}, {
					xtype: 'combo',
					editable: false,
					forceSelection: true,
					name: 'ustartPage',
					store: '@{startPageStore}',
					displayField: 'name',
					valueField: 'value',
					queryMode: 'local'
				}, {
					name: 'uhomePage'
				}]
			}]
		}, {
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			defaults : {
				margin : {
					right : 10
				}
			},
			items: [{
				title: '~~contactInfo~~',
				bodyPadding : '4 0 0 0',
				flex: 1,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaultType: 'textfield',
				defaults: {
					labelWidth: 130,
					margin : '4 0'
				},
				items: [{
					name: 'uemail'
				}, {
					name: 'uhomePhone'
				}, {
					name: 'umobilePhone'
				}, {
					name: 'ufax'
				}, {
					name: 'uim'
				}, {
					xtype: 'fieldcontainer',
					fieldLabel: '~~address~~',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					margin : 0,
					defaults : {
						margin : '4 0'
					},
					items: [{
						xtype: 'textfield',
						name: 'ustreet',
						emptyText: '~~ustreet~~',
						hideLabel: true
					}, {
						xtype: 'fieldcontainer',
						layout: {
							type: 'hbox',
							align: 'stretch'
						},
						items: [{
							xtype: 'textfield',
							hideLabel: true,
							flex: 1,
							name: 'ucity',
							emptyText: '~~ucity~~',
							padding: '0px 8px 0px 0px'
						}, {
							xtype: 'textfield',
							hideLabel: true,
							flex: 1,
							name: 'ustate',
							emptyText: '~~ustate~~'
						}]
					}, {
						xtype: 'fieldcontainer',
						layout: {
							type: 'hbox',
							align: 'stretch'
						},
						items: [{
							xtype: 'textfield',
							hideLabel: true,
							flex: 1,
							name: 'uzip',
							emptyText: '~~uzip~~',
							padding: '0px 8px 0px 0px'
						}, {
							xtype: 'combo',
							hideLabel: true,
							flex: 1,
							name: 'ucountry',
							store: '@{countryStore}',
							displayField: 'name',
							valueField: 'value',
							queryMode: 'local',
							emptyText: '~~ucountry~~'
						}]
					}]
				}]
			}, {
				title: '~~recoveryOptions~~',
				bodyPadding : '4 0 0 0',
				hidden: '@{!showRecoveryOptions}',
				flex: 1,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaultType: 'textfield',
				defaults : {
					margin : '4 0',
					labelWidth : 130
				},
				items: [{
					itemId: 'uquestion1',
					name: 'uquestion1',
					readOnly: '@{recoveryOptionsRO}'
				}, {
					name: 'uanswer1',
					readOnly: '@{recoveryOptionsRO}'
				}, {
					name: 'uquestion2',
					readOnly: '@{recoveryOptionsRO}'
				}, {
					name: 'uanswer2',
					readOnly: '@{recoveryOptionsRO}'
				}, {
					name: 'uquestion3',
					readOnly: '@{recoveryOptionsRO}'
				}, {
					name: 'uanswer3',
					readOnly: '@{recoveryOptionsRO}'
				}, {
					xtype: 'fieldcontainer',
					hidden: '@{!recoveryOptionsRO}',
					fieldLabel: ' ',
					labelSeparator: '',
					items: [{
						xtype: 'button',
						cls: 'rs-btn-dark rs-small-btn',
						name: 'resetRecoveryOptions',
						listeners: {
							render: function() {
								var uquestion1 = this.up().up().down('#uquestion1');
								this.getEl().on('click', function() {
									uquestion1.focus();
								});
							}
						}
					}]
				}]
			}, {
				hidden: '@{showRecoveryOptions}',
				flex: 1
			}]
		},
			// DISABLING THE USER CUSTOM FIELDS AS THERE WILL BE MORE OPERATIONS INVOLVED IN DOING THAT.
			// {
			// 	title: '~~customFields~~',
			// 	hidden: '@{!showCustomFields}',
			// 	layout: 'fit',
			// 	items: [{
			// 		xtype: 'rsform',
			// 		formName: 'RS_USERCUSTOMFIELDS',
			// 		embed: true,
			// 		hideToolbar: true,
			// 		showLoading: false,
			// 		recordId: '@{id}',
			// 		listeners: {
			// 			formLoaded: '@{formLoaded}'
			// 		}
			// 	}]
			// },
		{
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			minHeight: 200,
			flex : 1,
			defaults : {
				cls : 'rs-grid-dark',
				flex : 1,
				autoScroll : true,
				margin : '0 10 0 0',
			},
			items : [{
				xtype: 'grid',
				title: '~~roles~~',
				cls: '@{rolesGroupsCls}',
				name: 'roles',
				roleHide: true,
				roles: ['admin'],
				columns: [{
					text: '~~name~~',
					dataIndex: 'uname',
					flex: 1
				}],
				dockedItems : [{
					xtype : 'toolbar',
					cls : 'rs-dockedtoolbar',
					defaults : {
						cls :'rs-small-btn rs-btn-light'
					},
					items : ['addRole', 'removeRole']
				}],
				qtip: '@{noRoleOrGroupError}',
				listeners: {
					afterrender: function(form) {
						//Configure proper tooltip on the title
						if (form.getHeader()) {
							var headerElem = form.getHeader().getEl();
							var toolTipElem = headerElem.down('.x-header-text.x-panel-header-text');
							Ext.create('Ext.tip.ToolTip', {
								cls: 'user-roles-groups-tip',
								target: toolTipElem,
								html: this.qtip,
								autoShow: true,
								listeners: {
									boxready: function() {
										this.hide();
									}
								}
							})
						}
					},
				}
			}, {
				xtype: 'grid',
				title: '~~groups~~',
				name: 'groups',
				roleHide: true,
				roles: ['admin'],
				cls: '@{rolesGroupsCls}',
				columns: [{
					text: '~~name~~',
					dataIndex: 'uname',
					flex: 1
				}],
				dockedItems : [{
					xtype : 'toolbar',
					cls : 'rs-dockedtoolbar',
					defaults : {
						cls :'rs-small-btn rs-btn-light'
					},
					items :  ['addGroup', 'removeGroup']
				}],
				qtip: '@{noRoleOrGroupError}',
				listeners: {
					afterrender: function(form) {
						//Configure proper tooltip on the title
						if (form.getHeader()) {
							var headerElem = form.getHeader().getEl();
							var toolTipElem = headerElem.down('.x-header-text.x-panel-header-text');
							Ext.create('Ext.tip.ToolTip', {
								cls: 'user-roles-groups-tip',
								target: toolTipElem,
								html: this.qtip,
								autoShow: true,
								listeners: {
									boxready: function() {
										this.hide();
									}
								}
							})
						}
					},
				}
			},{
				xtype: 'grid',
				title: '~~organizations~~',
				name: 'organizations',
				roleHide: true,
				roles: ['admin'],
				margin : 0,
				viewConfig : {
					markDirty : false
				},
				selModel : {
					mode : 'SINGLE'
				},
				columns: [{
					text: '~~name~~',
					dataIndex: 'uname',
					flex: 1
				},{
					text : '~~default~~',
					dataIndex : 'isDefaultOrg',
					width : 80,
					align : 'center',
					renderer: RS.common.grid.booleanRenderer()
				}],
				dockedItems : [{
					xtype : 'toolbar',
					cls : 'rs-dockedtoolbar',
					defaults : {
						cls :'rs-small-btn rs-btn-light'
					},
					items :  ['setDefaultOrg']
				}]
			}]
		}]
	}, {
		cls : 'rs-panel-header',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		autoScroll: true,
		style : 'border-top:1px solid silver',
		defaults : {
			bodyPadding : '5 0 0 0',
			margin : '25 0 0 0'
		},
		items: [{
			title: '~~timeSettings~~',
			defaults: {
				labelWidth: 210
			},
			items: [{
				xtype: 'combobox',
				width: 500,
				name: 'userDefaultDateFormat',
				autoSelect: false,
				store: '@{userDefaultDateFormatStore}',
				displayField: 'name',
				valueField: 'value',
				queryMode: 'local'
			}]
		}, {
			title: '~~gridSettings~~',
			defaults: {
				labelWidth: 210
			},
			items: [{
				xtype: 'checkbox',
				name: 'autoRefreshEnabled',
			}, {
				xtype: 'numberfield',
				name: 'autoRefreshInterval',
				width: 500,
				minValue: 10
			}]
		}, {
			title: '~~editorSettings~~',
			defaults: {
				labelWidth: 210
			},
			items: [{
				xtype: 'combobox',
				editable: false,
				name: 'aceKeybinding',
				store: '@{aceKeybindingStore}',
				displayField: 'name',
				valueField: 'value',
				queryMode: 'local',
				width: 500
			}]
		}, {
			title: '~~menuSettings~~',
			defaults: {
				labelWidth: 210,
				width: 500
			},
			items: [{
				xtype: 'numberfield',
				name: 'maxRecentlyUsed',
				minValue: -1
			},{
				xtype: 'combo',
				name: 'sideMenuDisplay',
				store : '@{menuDisplayOptionStore}',
				displayField : 'text',
				valueField : 'value',
				editable : false,
			}]
		}, {
			title: '~~displaySettings~~',
			defaults: {
				labelWidth: 210
			},
			items: [{
				xtype: 'checkbox',
				name: 'bannerIsVisible'
			}]
		}]
	}]
});

glu.defView('RS.user.ChangePasswordForm', {
	xtype: 'form',
	title: '~~changePassword~~',
	cls : 'rs-modal-popup',
	padding : 15,
	modal: true,
	width: 550,
	height: 220,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaultType: 'textfield',
	defaults: {
		labelWidth: 150,
		allowBlank: false
	},
	items: [{
		name: 'uuserOldPassword',
		hidden: '@{!currentUser}',
		inputType: 'password',
		selectOnFocus: true
	},{
		name: 'uuserPassword',
		itemId: 'uuserPassword',
		fieldLabel: '~~newPassword~~',
		inputType: 'password',
		selectOnFocus: true
	}, {
		name: 'uuserPasswordConfirm',
		itemId: 'uuserPasswordConfirm',
		inputType: 'password',
		selectOnFocus: true
	}],
	buttons: [{
		name :'apply',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});
glu.defView('RS.user.UserPicker', {
	title: '@{pickerTitle}',
	modal: true,	
	layout: 'fit',
	cls : 'rs-modal-popup',
	padding : 15,
	width: 800,
	height: 450,
	items: [{
		xtype: 'grid',
		cls :'rs-grid-dark',
		name: 'users',
		columns: [{
			header: '~~userId~~',
			dataIndex: 'uuserName',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~uname~~',
			dataIndex: 'ufirstName',
			filterable: true,
			sortable: true,
			flex: 1,
			renderer: function(value, meta, record) {
				return (value || '') + ' ' + (record.get('ulastName') || '')
			}
		}, {
			header: '~~email~~',
			dataIndex: 'uemail',
			filterable: true,
			sortable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()),
		plugins: [{
			ptype: 'pager'
		}],
		selModel: {
			mode: 'MULTI'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'keyword',
				width : 400				
			}]
		}],
		margin : {
			bottom : 10
		}
	}],	
	buttons: [{
		name : 'add',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'select',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name :'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]	
});
glu.defView('RS.user.Users', {
	xtype: 'grid',
	padding: 15,
	cls :'rs-grid-dark',
	name: 'users',
	border: false,
	stateId: '@{stateId}',
	displayName: '@{displayName}',
	stateful: true,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',	
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'uuserName'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls :'rs-small-btn rs-btn-light'
		},
		items: ['createUser', 'cloneUser', 'assignRoles', 'removeRoles', 'assignGroups', 'removeGroups', 'deleteUser']
	}],
	columns: '@{columns}',
	viewConfig: {
		enableTextSelection: true
	},
	listeners: {
		editAction: '@{editUser}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});
(function() {
    //Some common utility functions
    var util = {
        mixin: function(dest, src) {
            for (var p in src) dest[p] = src[p];
        },
        byId: function(id) {
            if (typeof id == 'string') return document.getElementById(id);
            else return id;
        },
        create: function(tag, attrs) {
            var node = document.createElement(tag);
            this.mixin(node, attrs);
            return node;
        },
        connect: function(node, evt, context, callback) {
            //TODO: use event listeners instead
            var self = this;
            node[evt] = function(evt) {
                evt = self.fixEvent(evt);
                context[callback](evt);
            }
        },
        style: function(node, args) {
            if (typeof args == 'string') {
                var value = node.style[args];
                if (!value) {
                    s = window.getComputedStyle ? getComputedStyle(node) : node.currentStyle;
                    value = s[args];
                }
                return value;
            } else this.mixin(node.style, args);
        },
        each: function(arr, callback) {
            for (var i = 0; i < arr.length; i++)
                callback(arr[i], i);
        },
        indexOf: function(arr, value) {
            for (var i = 0; i < arr.length; i++)
                if (value == arr[i]) return i;
            return -1;
        },
        addCss: function(node, css) {
            if (!node) return;
            var cn = node.className || '',
                arr = cn.split(' '),
                i = util.indexOf(arr, css);
            if (i < 0) arr.push(css);
            node.className = arr.join(' ');
        },
        rmCss: function(node, css) {
            if (!node) return;
            var cn = node.className || '',
                arr = cn.split(' '),
                i = util.indexOf(arr, css);
            if (i >= 0) arr.splice(i, 1);
            node.className = arr.join(' ');
        },
        fixEvent: function(evt) {
            evt = evt || event;
            if (!evt.target) evt.target = evt.srcElement;
            if (!evt.keyCode) evt.keyCode = evt.which || evt.charCode;
            if (!evt.pageX) { //only for IE
                evt.pageX = evt.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                evt.pageY = evt.clientY + document.body.scrollTop + document.documentElement.scrollTop;
            }
            return evt;
        }
    };



    window.ICropper = function(container, options) {
        // summary:
        //  Constructor of the Image Cropper, the container could be a dom node or id.

        container = util.byId(container);
        for (var p in options) {
            if (options[p]) this[p] = options[p];
        }
        this.domNode = container || util.create('div');
        this._init();
    }

    ICropper.prototype = {

        //The image url
        image: ''

        //The minimal size of the cropping area
        ,
        minWidth: 20,
        minHeight: 20

        //The default gap between crop region border and container border
        ,
        gap: 50

        //the initial crop region width and height
        ,
        initialSize: 0

        //whether to keep crop region as a square
        ,
        keepSquare: false

        //array: the nodes to show previews of cropped image
        ,
        preview: null

        ,
        domNode: null,
        cropNode: null,
        imageNode: null

        //Public APIs
        //------------------------------------------------------------
        ,
        setImage: function(url) {
            // summary:
            //  Set the image to be cropped. The container size will fit the image.
            var img = new Image();
            img.src = url;
            this.image = url;
            if (!this.imageNode) {
                this.imageNode = util.create('img');
                this.domNode.appendChild(this.imageNode);
                var self = this;
                //TODO: onerror?
                this.imageNode.onload = function() {
                    self._setSize(this.offsetWidth, this.offsetHeight);
                }
            }
            this.imageNode.src = url;
        }

        ,
        bindPreview: function(node) {
            // summary:
            //  Bind a node as the preview area. e.g: a real size avatar
            node = util.byId(node);
            util.style(node, {
                overflow: 'hidden'
            });
            var width = parseInt(util.style(node, 'width')),
                height = parseInt(util.style(node, 'height'));
            var previewImage = util.create('img', {
                src: this.image
            });
            node.appendChild(previewImage);

            var _oldOnChange = this.onChange;
            this.onChange = function(info) {
                _oldOnChange.call(this, info);
                var rateX = width / info.w,
                    rateY = height / info.h;
                util.style(previewImage, {
                    width: info.cw * rateX + 'px',
                    height: info.ch * rateY + 'px',
                    marginLeft: -info.l * rateX + 'px',
                    marginTop: -info.t * rateY + 'px'
                });
            }
        }

        ,
        getInfo: function() {
            // summary:
            //  Get the cropping infomation. Such as being used by server side for real cropping.
            return {
                w: this.cropNode.offsetWidth - 2 //2 is hard code border width
                ,
                h: this.cropNode.offsetHeight - 2,
                l: parseInt(util.style(this.cropNode, 'left')),
                t: parseInt(util.style(this.cropNode, 'top')),
                cw: this.domNode.offsetWidth //container width
                ,
                ch: this.domNode.offsetHeight //container height
            };
        }

        ,
        onChange: function() {
            //Event:
            //    When the cropping size is changed.
        },
        onComplete: function() {
            //Event:
            //    When mouseup.
        }

        //Private APIs
        //------------------------------------------------------------
        ,
        _init: function() {
            util.addCss(this.domNode, 'icropper');
            this._buildRendering();
            this._updateUI();
            util.connect(this.cropNode, 'onmousedown', this, '_onMouseDown');
            util.connect(document, 'onmouseup', this, '_onMouseUp');
            util.connect(document, 'onmousemove', this, '_onMouseMove');
            this.image && this.setImage(this.image);

            if (this.preview) {
                var self = this;
                util.each(this.preview, function(node) {
                    self.bindPreview(node);
                });
            }
        }

        ,
        _buildRendering: function() {
            this._archors = {};
            this._blockNodes = {};

            this.cropNode = util.create('div', {
                className: 'crop-node no-select'
            });
            this.domNode.appendChild(this.cropNode);

            //Create archors
            var arr = ['lt', 't', 'rt', 'r', 'rb', 'b', 'lb', 'l'];
            for (var i = 0; i < 8; i++) {
                var n = util.create('div', {
                    className: 'archor archor-' + arr[i]
                });
                this.cropNode.appendChild(n);
                this._archors[arr[i]] = n;
            }

            //Create blocks for showing dark areas
            arr = ['l', 't', 'r', 'b'];
            for (var i = 0; i < 4; i++) {
                var n = util.create('div', {
                    className: 'block block-' + arr[i]
                });
                this.domNode.appendChild(n);
                this._blockNodes[arr[i]] = n;
            }
        }

        ,
        _setSize: function(w, h) {

            this.domNode.style.width = w + 'px';
            this.domNode.style.height = h + 'px';

            var w2, h2;
            if (this.initialSize) {
                var m = Math.min(w, h, this.initialSize);
                w2 = h2 = m - 2 + 'px';
            } else {
                w2 = w - this.gap * 2 - 2;
                h2 = h - this.gap * 2 - 2;
                if (this.keepSquare) {
                    w2 = h2 = Math.min(w2, h2);
                }
                w2 += 'px';
                h2 += 'px';
            }

            var s = this.cropNode.style;
            s.width = w2;
            s.height = h2;

            var l = (w - this.cropNode.offsetWidth) / 2,
                t = (h - this.cropNode.offsetHeight) / 2;

            if (l < 0) l = 0;
            if (t < 0) t = 0;

            s.left = l + 'px';
            s.top = t + 'px';

            this._posArchors();
            this._posBlocks();
            this.onChange(this.getInfo());
        }

        ,
        _updateUI: function() {
            this._posArchors();
            this._posBlocks();
        }

        ,
        _posArchors: function() {
            var a = this._archors,
                w = this.cropNode.offsetWidth,
                h = this.cropNode.offsetHeight;
            w = w / 2 - 4 + 'px';
            h = h / 2 - 4 + 'px';
            a.t.style.left = a.b.style.left = w;
            a.l.style.top = a.r.style.top = h;
        }

        ,
        _posBlocks: function() {
            var p = this.startedPos,
                b = this._blockNodes;
            var l = parseInt(util.style(this.cropNode, 'left'));
            var t = parseInt(util.style(this.cropNode, 'top'));
            var w = this.cropNode.offsetWidth;
            var ww = this.domNode.offsetWidth;
            var h = this.cropNode.offsetHeight;
            var hh = this.domNode.offsetHeight;

            b = this._blockNodes;
            b.t.style.height = b.l.style.top = b.r.style.top = t + 'px';

            b.l.style.height = b.r.style.height = h + 'px';
            b.l.style.width = l + 'px';


            w = ww - w - l;
            h = hh - h - t;

            //fix IE
            if (w < 0) w = 0;
            if (h < 0) h = 0;

            b.r.style.width = w + 'px';
            b.b.style.height = h + 'px';
        }

        ,
        _onMouseDown: function(e) {
            var n = this.cropNode,
                s = n.style;
            this.dragging = (e.target == n) ? 'move' : e.target.className;
            if (this.dragging != 'move') {
                var arr = this.dragging.split(' ');
                this.dragging = arr.pop().split('-')[1];
            }

            this.startedPos = {
                x: e.pageX,
                y: e.pageY,
                h: n.offsetHeight - 2 //2 is border width
                ,
                w: n.offsetWidth - 2,
                l: parseInt(util.style(n, 'left')),
                t: parseInt(util.style(n, 'top'))
            }
            var c = util.style(e.target, 'cursor');
            util.style(document.body, {
                cursor: c
            });
            util.style(this.cropNode, {
                cursor: c
            });
            util.addCss(document.body, 'no-select');
            util.addCss(document.body, 'unselectable'); //for IE
        }

        ,
        _onMouseUp: function(e) {
            this.dragging = false;
            util.style(document.body, {
                cursor: 'default'
            });
            util.style(this.cropNode, {
                cursor: 'move'
            });
            util.rmCss(document.body, 'no-select');
            util.rmCss(document.body, 'unselectable');
            this.onComplete && this.onComplete(this.getInfo());
        }

        ,
        _onMouseMove: function(e) {
            if (!this.dragging) return;

            if (this.dragging == 'move') this._doMove(e);
            else this._doResize(e);
            this._updateUI();
            this.onChange && this.onChange(this.getInfo());
        }

        ,
        _doMove: function(e) {
            var s = this.cropNode.style,
                p0 = this.startedPos;
            var l = p0.l + e.pageX - p0.x;
            var t = p0.t + e.pageY - p0.y;
            if (l < 0) l = 0;
            if (t < 0) t = 0;
            var maxL = this.domNode.offsetWidth - this.cropNode.offsetWidth;
            var maxT = this.domNode.offsetHeight - this.cropNode.offsetHeight;
            if (l > maxL) l = maxL;
            if (t > maxT) t = maxT;
            s.left = l + 'px';
            s.top = t + 'px'
        }

        ,
        _doResize: function(e) {
            var m = this.dragging,
                s = this.cropNode.style,
                cw = this.cropNode.offsetWidth,
                ch = this.cropNode.offsetHeight,
                p0 = this.startedPos;
            //delta x and delta y
            var dx = e.pageX - p0.x,
                dy = e.pageY - p0.y;

            if (this.keepSquare || e.shiftKey) {
                if (m == 'l') {
                    dy = dx;
                    if (p0.l + dx < 0) dx = dy = -p0.l;
                    if (p0.t + dy < 0) dx = dy = -p0.t;
                    m = 'lt';
                } else if (m == 'r') {
                    dy = dx;
                    m = 'rb';
                } else if (m == 'b') {
                    dx = dy;
                    m = 'rb';
                } else if (m == 'lt') {
                    dx = dy = Math.abs(dx) > Math.abs(dy) ? dx : dy;
                    if (p0.l + dx < 0) dx = dy = -p0.l;
                    if (p0.t + dy < 0) dx = dy = -p0.t;
                } else if (m == 'lb') {
                    dy = -dx;
                    if (p0.l + dx < 0) {
                        dx = -p0.l;
                        dy = p0.l;
                    }
                } else if (m == 'rt' || m == 't') {
                    dx = -dy;
                    m = 'rt';
                    if (p0.t + dy < 0) {
                        dy = -p0.t;
                        dx = -dy;
                    }
                }
            }
            if (/l/.test(m)) {
                dx = Math.min(dx, p0.w - this.minWidth);
                if (p0.l + dx >= 0) {

                    s.left = p0.l + dx + 'px';
                    s.width = p0.w - dx + 'px';

                } else {
                    s.left = 0;
                    s.width = p0.l + p0.w + 'px';
                }
            }
            if (/t/.test(m)) {
                dy = Math.min(dy, p0.h - this.minHeight);
                if (p0.t + dy >= 0) {
                    s.top = p0.t + dy + 'px';
                    s.height = p0.h - dy + 'px';
                } else {
                    s.top = 0;
                    s.height = p0.t + p0.h + 'px';
                }
            }
            if (/r/.test(m)) {
                dx = Math.max(dx, this.minWidth - p0.w);
                if (p0.l + p0.w + dx <= this.domNode.offsetWidth) {
                    s.width = p0.w + dx + 'px';
                } else {
                    s.width = this.domNode.offsetWidth - p0.l - 2 + 'px';
                }
            }
            if (/b/.test(m)) {
                dy = Math.max(dy, this.minHeight - p0.h);
                if (p0.t + p0.h + dy <= this.domNode.offsetHeight) {
                    s.height = p0.h + dy + 'px';
                } else {
                    s.height = this.domNode.offsetHeight - p0.t - 2 + 'px';
                }
            }

            if (this.keepSquare || e.shiftKey) {
                var min = Math.min(parseInt(s.width), parseInt(s.height));
                s.height = s.width = min + 'px';
            }
        }

        ,
        destroy: function() {
            //TODO: destroy self to release memory
        }
    }
})();
/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.user').locale = {

	//Columns
	id: 'Sys ID',
	sysId: 'Sys ID',
	uname: 'Name',
	uuserName: 'Name',
	udescription : 'Description',
	sysCreatedOn: 'Created On',
	sysCreatedBy: 'Created By',
	sysUpdatedOn: 'Updated On',
	sysUpdatedBy: 'Updated By',
	organization: 'Organization',
	udescription: 'Description',
	default : 'Default',
	roles: 'Roles',
	users: 'Users',
	userId: 'User ID',
	locked: 'Locked',
	groups: 'Groups',
	email: 'Email',
	title: 'Title',
	phone: 'Phone',
	mobilePhone: 'Mobile Phone',
	lastLogin: 'Last Login',
	ipAddress: 'IP Address',

	name: 'Name',
	back: 'Back',
	save: 'Save',
	add : 'Add',
	cancel : 'Cancel',
	keyword : 'Keyword',
	Users: {
		allUsers: 'Users',
		userDisplayName: 'Users',

		createUser: 'New',
		deleteUser: 'Delete',
		cloneUser: 'Clone',
		assignGroups: 'Assign Groups',
		removeGroups: 'Remove Groups',
		assignRoles: 'Assign Roles',
		removeRoles: 'Remove Roles',

		groupsAdded: 'Groups added successfully',
		groupsRemoved: 'Groups removed successfully',
		rolesAdded: 'Roles added successfully',
		rolesRemoved: 'Roles removed successfully',
		noGroups: 'Selected user(s) have no groups',
		noRoles: 'Selected user(s) have no roles',

		deleteTitle: 'Confirm Deletion',
		deleteMessage: 'Are you sure you want to delete the selected user?',
		deletesMessage: 'Are you sure you want to delete the {0} selected users?',
		deleteAction: 'Delete'
	},

	UserPicker: {
		selectUserTitle: 'Select User',
		addUserTitle: 'Add User',
		select: 'Select'
	},

	User: {
		windowTitle: 'User Profile',
		user: 'User',
		blogTab: 'Blog',
		adminTab: 'Admin',
		settingsTab: 'Settings',

		contact: 'Contact',
		summary: 'Summary',

		addRole: 'Add Role',
		removeRole: 'Remove Role',

		addGroup: 'Add Group',
		removeGroup: 'Remove Group',

		uuserName: 'User ID',
		fullName : 'Full Name',
		ufirstName: 'First Name',
		ulastName: 'Last Name',
		uuserPassword: 'Password',
		uuserPasswordConfirm: 'Confirm Password',
		upasswordNeedsReset: 'Password needs resetting',
		ulockedOut: 'Locked Out',
		utitle: 'Title',
		uemail: 'Email',
		uhomePhone: 'Phone',
		umobilePhone: 'Mobile Phone',
		ufax: 'Fax',
		mobile: 'Mobile',
		fax: 'Fax',
		uim: 'IM',
		usummary: 'Summary',

		address: 'Address',

		ustreet: 'Street',
		ucity: 'City',
		ustate: 'State',
		uzip: 'Postal / Zip Code',
		ucountry: 'Country',

		contactInfo: 'Contact Information',
		recoveryOptions: 'Recovery Options',
		customFields: 'Custom Fields',

		uquestion1: 'Question 1',
		uquestion2: 'Question 2',
		uquestion3: 'Question 3',
		uanswer1: 'Answer 1',
		uanswer2: 'Answer 2',
		uanswer3: 'Answer 3',
		invalidUserName: 'The User ID is limited to 40 characters, must begin with a letter or number, and can only contain letters, numbers, underscores, periods or the @ symbol.',

		UserSaved: 'User saved successfully',

		resetBrowserPreferences: 'Restore Default Preferences',
		resetTitle: 'Confirm Restore Default Preferences',
		resetMessage: 'Are you sure you want to clear your browser preferences?',


		timeSettings: 'Time Settings',
		editorSettings: 'Editor Settings',
		gridSettings: 'Grid Settings',
		menuSettings: 'Menu Settings',
		displaySettings: 'Display Settings',

		clickToChangeProfile: 'Click to change your profile picture',

		userDefaultDateFormat: 'Date format',
		defaultDate: 'Default',
		autoRefreshEnabled: 'Auto refresh',
		autoRefreshInterval: 'Auto refresh interval',
		useAce: 'Use code editor',
		aceKeybinding: 'Editor keybinding',
		aceLanguage: 'Editor syntax highlighting',
		maxRecentlyUsed: 'Display recently used entries',
		sideMenuDisplay : 'Display options',
		bannerIsVisible: 'Display Banner',

		followers: 'Followers',
		following: 'Following',
		noFollowers: 'No followers yet',
		noFollowing: 'You are not following anyone yet',


		contributions: 'Posts & Comments',
		commentsReceived: 'Comments Received',
		likesReceived: 'Likes Received',

		ustartPage: 'Start page',
		uhomePage: 'Home page',
		sirDashboard : 'Investigation Dashboard',

		menu: 'Menu',
		wiki: 'Wiki',
		social: 'Social',
		defaultText: 'Default',

		noRoleOrGroupError: 'Please provide a role or group before saving the user',
		organizations : 'Organizations',
		setDefaultOrg : 'Set as Default Organization',
		changePassword: 'Change Password',
		resetRecoveryOptions: 'Reset Recovery Options',

		invalidUserSettings: 'Invalid user settings. <br><br>Please click <i>Save</i> to correct any invalid settings for user <b>{0}</b>.'
	},
	ChangePasswordForm: {
		uuserOldPassword: 'Old Password',
		newPassword: 'New Password',
		uuserPasswordConfirm: 'Confirm Password',
		apply: 'Apply',
		cancel: 'Cancel',
		changePassword: 'Change Password'
	},
	Organizations : {
		organizationDisplayName : 'Organizations',
		createOrganization : 'New',
		organizations : 'Organizations'
	},
	Organization : {
		organizationTitle : 'Organization - {0}',
		nameInvalid: 'Please provide a valid name for this organization. The name "None" is reserved.',
		addGroup : 'Add Group',
		removeGroup : 'Remove Group',
		groups : 'Groups',
		newOrganzition : 'New Organization',
		orgSaved: 'Organization saved successfully',
		parentOrgId : 'Parent Organization',
		uhasNoOrgAccess : "Has Access to No Org's Contents."
	},
	OrganizationPicker : {
		addOrganization : 'Add Organization'
	},
	Groups: {
		allGroups: 'Groups',
		groupDisplayName: 'Groups',

		createGroup: 'New',
		copyGroup: 'Copy',
		deleteGroup: 'Delete',

		deleteTitle: 'Confirm Deletion',
		deleteMessage: 'Are you sure you want to delete the selected group?',
		deletesMessage: 'Are you sure you want to delete the {0} selected groups?',
		deleteAction: 'Delete'
	},
	Group: {
		group: 'Group',
		groupTitle: 'Group - {0}',
		newGroup: 'New Group',

		groupSaved: 'Group saved successfully',

		linkToTeam: 'Link to Team',
		linkToTeamTooltip: 'Users added to this group are also added to the team with the same name',
		linkToExternalGroup: 'Link to external group',
		linkToExternalGroupTooltip: 'Users from LDAP/AD group of the same name are automatically assigned to this group',

		nameInvalid: 'Please provide a name for this group',
		noRolesError: 'Please provide a role for this group',

		addRole: 'Add Role',
		removeRole: 'Remove Role',

		addUser: 'Add User',
		removeUser: 'Remove User',

		sysUpdatedOnDisplay: 'Updated On',
		sysCreatedOnDisplay: 'Created On',
		organizations : 'Organizations',
		addOrganization : 'Add Organization',
		removeOrganization : 'Remove Organization'
	},

	CopyGroup: {
		copyGroupTitle: 'Copy Group',
		copy: 'Copy',
		name: 'Name',
		nameInvalidBlank: 'Please provide a name for the new group',
		nameInvalidDuplicate: 'A group with the name \'{0}\' already exists, please enter another name'
	},

	Roles: {
		allRoles: 'Roles',
		roleDisplayName: 'Roles',

		createRole: 'New',
		deleteRole: 'Delete',

		deleteTitle: 'Confirm Deletion',
		deleteMessage: 'Are you sure you want to delete the selected role?',
		deletesMessage: 'Are you sure you want to delete the {0} selected roles?',
		deleteAction: 'Delete',
	},

	Role: {
		newRole: 'New Role',
		editRole: 'Edit Role',
		roleSaved: 'Role saved successfully',
		unameInvalid: 'Please provide a name for the role',

		view: 'View',
		create: 'Create',
		modify: 'Modify',
		delete: 'Delete',
		execute: 'Execute',

		roleName: 'Name',
		roleDescription: 'Description',
		permissions: 'Permissions',
		app: 'Application',
		applyPredefined: 'Apply Template'
	},

	CloneUser: {
		cloneUserTitle: 'Copy User',
		clone: 'Copy',
		name: 'Name',
		nameInvalidBlank: 'Please provide a name for the new user',
		nameInvalidDuplicate: 'A user with the name \'{0}\' already exists, please enter another name'
	},

	GroupPicker: {
		addGroupTitle: 'Add Group',
		removeGroupTitle: 'Remove Group',
		remove: 'Remove'
	},

	ProfileUpload: {
		save: 'Set as profile photo',
		clickToUpload: 'Select a photo from your computer',
		dragText: 'DRAG A PHOTO HERE',
		or: 'or',
		browse: 'Browse',
		profileIntro: 'Click "Browse" to select a photo for uploading or drag a photo to the area below to upload it.',

		uploadProfilePicTitle: 'Upload Profile Picture'
	},

	RemoveGroupPicker: {
		removeGroupPickerTitle: 'Remove Group',
		remove: 'Remove'
	},

	RemoveRolePicker: {
		removeRolePickerTitle: 'Remove Role',
		remove: 'Remove'
	},

	SIRPermissions: {
		// Keys
		sir: 'SIR Enabled',
		sirAnalytics: 'Analytics',
		sirAnalyticsGraph: 'Graphs',
		sirDashboard: 'Dashboard',
		sirDashboardCaseCreation: 'Case Creation',
		sirDashboardIncidentList: 'Incident List',
		sirIncident: 'Incidents',
		sirIncidentTitle: 'Title',
		sirIncidentClassification: 'Classification',
		sirIncidentType: 'Type',
		sirIncidentDataCompromised: 'Data Compromised',
		sirIncidentSeverity: 'Severity',
		sirIncidentStatus: 'Status',
		sirIncidentPriority: 'Priority',
		sirIncidentPlaybook: 'Playbook',
		sirIncidentOwner: 'Owner',
		sirIncidentTeam: 'Team',
		sirIncidentDueDate: 'Due Date',
		sirIncidentRiskScore: 'Risk Score',
		sirIncidentIncidentFlags: 'Incident Flags',
		sirIncidentMarkIncident: 'Mark Incident',

		sirIncidentViewer: 'Incident Viewer',
		sirIncidentViewerActivity: 'Task',
		sirIncidentViewerNote: 'Note',
		sirIncidentViewerArtifact: 'Artifact',
		sirIncidentViewerAttachment: 'Attachment',
		sirIncidentViewerTeam: 'Team',
		sirIncidentViewerActivityTimeline: 'Activity Timeline',

		sirPlaybookTemplates: 'Playbook Templates',
		sirPlaybookTemplatesTemplateBuilder: 'Template Builder',

		sirSettings: 'SIR Settings',

		analytics: 'Analytics',
		analyticsReporting: 'Reporting',

		investigationViewer: 'Investigation Viewer',
		dataCompromised: 'Data Compromised',
		team: 'Incident Team',
		activities: 'Activities',
		notes: 'Notes',
		artifacts: 'Artifacts',
		attachments: 'Attachments',

		playbookTemplates: 'Playbook Templates',
		templateBuilder: 'Template Builder',
		templateActivities: 'Activities',
		templateSections: 'Sections',
	},

	Analytics: {
		analytics: 'Analytics',
		analyticsReporting: 'Reporting'
	},

	SIRCheckboxGroup: {
		// Permissions
		enabled: 'Enabled',
		create: 'Create',
		view: 'View',
		viewAll: 'View All',
		change: 'Change',
		'assignee.change': 'Change Assignee',
		'status.change': 'Change Status',
		'status.change': 'Change Status',
		executeAutomation: 'Execute Automation',
		upload: 'Upload',
		download: 'Download',
		downloadMalicious: 'Download Malicious',
		unsetMalicious: 'Unset Malicious',
		delete: 'Delete',
		asMaster: 'As Master',
		asDuplicate: 'As Duplicate',
	},

	AnalyticsCheckboxGroup: {
		view: 'View',
	},

	UARCheckboxGroup: {
		view: 'View',
		edit: 'Edit',
		create: 'Create',
		close: 'Close',
		add: 'Add',
		delete: 'Delete',
		execute: 'Execute Wikis',
	},
	passwordMinLengthInvalid: 'Password must be a minimum of 8 characters long',
	passwordNumberRequiredInvalid: 'Password must contain a number',
	passwordCapitalLetterRequiredInvalid: 'Password must contain a capital letter',
	passwordLowercaseLetterRequiredInvalid: 'Password must contain a lower case letter',
	passwordConfirmMismatchInvalid: 'Passwords do not match',
	successfully_Changed_Pas_sword: 'Successfully change password.'

}
