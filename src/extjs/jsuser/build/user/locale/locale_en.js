/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.user').locale = {

	//Columns
	id: 'Sys ID',
	sysId: 'Sys ID',
	uname: 'Name',
	uuserName: 'Name',
	udescription : 'Description',
	sysCreatedOn: 'Created On',
	sysCreatedBy: 'Created By',
	sysUpdatedOn: 'Updated On',
	sysUpdatedBy: 'Updated By',
	organization: 'Organization',
	udescription: 'Description',
	default : 'Default',
	roles: 'Roles',
	users: 'Users',
	userId: 'User ID',
	locked: 'Locked',
	groups: 'Groups',
	email: 'Email',
	title: 'Title',
	phone: 'Phone',
	mobilePhone: 'Mobile Phone',
	lastLogin: 'Last Login',
	ipAddress: 'IP Address',

	name: 'Name',
	back: 'Back',
	save: 'Save',
	add : 'Add',
	cancel : 'Cancel',
	keyword : 'Keyword',
	Users: {
		allUsers: 'Users',
		userDisplayName: 'Users',

		createUser: 'New',
		deleteUser: 'Delete',
		cloneUser: 'Clone',
		assignGroups: 'Assign Groups',
		removeGroups: 'Remove Groups',
		assignRoles: 'Assign Roles',
		removeRoles: 'Remove Roles',

		groupsAdded: 'Groups added successfully',
		groupsRemoved: 'Groups removed successfully',
		rolesAdded: 'Roles added successfully',
		rolesRemoved: 'Roles removed successfully',
		noGroups: 'Selected user(s) have no groups',
		noRoles: 'Selected user(s) have no roles',

		deleteTitle: 'Confirm Deletion',
		deleteMessage: 'Are you sure you want to delete the selected user?',
		deletesMessage: 'Are you sure you want to delete the {0} selected users?',
		deleteAction: 'Delete'
	},

	UserPicker: {
		selectUserTitle: 'Select User',
		addUserTitle: 'Add User',
		select: 'Select'
	},

	User: {
		windowTitle: 'User Profile',
		user: 'User',
		blogTab: 'Blog',
		adminTab: 'Admin',
		settingsTab: 'Settings',

		contact: 'Contact',
		summary: 'Summary',

		addRole: 'Add Role',
		removeRole: 'Remove Role',

		addGroup: 'Add Group',
		removeGroup: 'Remove Group',

		uuserName: 'User ID',
		fullName : 'Full Name',
		ufirstName: 'First Name',
		ulastName: 'Last Name',
		uuserPassword: 'Password',
		uuserPasswordConfirm: 'Confirm Password',
		upasswordNeedsReset: 'Password needs resetting',
		ulockedOut: 'Locked Out',
		utitle: 'Title',
		uemail: 'Email',
		uhomePhone: 'Phone',
		umobilePhone: 'Mobile Phone',
		ufax: 'Fax',
		mobile: 'Mobile',
		fax: 'Fax',
		uim: 'IM',
		usummary: 'Summary',

		address: 'Address',

		ustreet: 'Street',
		ucity: 'City',
		ustate: 'State',
		uzip: 'Postal / Zip Code',
		ucountry: 'Country',

		contactInfo: 'Contact Information',
		recoveryOptions: 'Recovery Options',
		customFields: 'Custom Fields',

		uquestion1: 'Question 1',
		uquestion2: 'Question 2',
		uquestion3: 'Question 3',
		uanswer1: 'Answer 1',
		uanswer2: 'Answer 2',
		uanswer3: 'Answer 3',
		invalidUserName: 'The User ID is limited to 40 characters, must begin with a letter or number, and can only contain letters, numbers, underscores, periods or the @ symbol.',

		UserSaved: 'User saved successfully',

		resetBrowserPreferences: 'Restore Default Preferences',
		resetTitle: 'Confirm Restore Default Preferences',
		resetMessage: 'Are you sure you want to clear your browser preferences?',


		timeSettings: 'Time Settings',
		editorSettings: 'Editor Settings',
		gridSettings: 'Grid Settings',
		menuSettings: 'Menu Settings',
		displaySettings: 'Display Settings',

		clickToChangeProfile: 'Click to change your profile picture',

		userDefaultDateFormat: 'Date format',
		defaultDate: 'Default',
		autoRefreshEnabled: 'Auto refresh',
		autoRefreshInterval: 'Auto refresh interval',
		useAce: 'Use code editor',
		aceKeybinding: 'Editor keybinding',
		aceLanguage: 'Editor syntax highlighting',
		maxRecentlyUsed: 'Display recently used entries',
		sideMenuDisplay : 'Display options',
		bannerIsVisible: 'Display Banner',

		followers: 'Followers',
		following: 'Following',
		noFollowers: 'No followers yet',
		noFollowing: 'You are not following anyone yet',


		contributions: 'Posts & Comments',
		commentsReceived: 'Comments Received',
		likesReceived: 'Likes Received',

		ustartPage: 'Start page',
		uhomePage: 'Home page',
		sirDashboard : 'Investigation Dashboard',

		menu: 'Menu',
		wiki: 'Wiki',
		social: 'Social',
		defaultText: 'Default',

		noRoleOrGroupError: 'Please provide a role or group before saving the user',
		organizations : 'Organizations',
		setDefaultOrg : 'Set as Default Organization',
		changePassword: 'Change Password',
		resetRecoveryOptions: 'Reset Recovery Options',

		invalidUserSettings: 'Invalid user settings. <br><br>Please click <i>Save</i> to correct any invalid settings for user <b>{0}</b>.'
	},
	ChangePasswordForm: {
		uuserOldPassword: 'Old Password',
		newPassword: 'New Password',
		uuserPasswordConfirm: 'Confirm Password',
		apply: 'Apply',
		cancel: 'Cancel',
		changePassword: 'Change Password'
	},
	Organizations : {
		organizationDisplayName : 'Organizations',
		createOrganization : 'New',
		organizations : 'Organizations'
	},
	Organization : {
		organizationTitle : 'Organization - {0}',
		nameInvalid: 'Please provide a valid name for this organization. The name "None" is reserved.',
		addGroup : 'Add Group',
		removeGroup : 'Remove Group',
		groups : 'Groups',
		newOrganzition : 'New Organization',
		orgSaved: 'Organization saved successfully',
		parentOrgId : 'Parent Organization',
		uhasNoOrgAccess : "Has Access to No Org's Contents."
	},
	OrganizationPicker : {
		addOrganization : 'Add Organization'
	},
	Groups: {
		allGroups: 'Groups',
		groupDisplayName: 'Groups',

		createGroup: 'New',
		copyGroup: 'Copy',
		deleteGroup: 'Delete',

		deleteTitle: 'Confirm Deletion',
		deleteMessage: 'Are you sure you want to delete the selected group?',
		deletesMessage: 'Are you sure you want to delete the {0} selected groups?',
		deleteAction: 'Delete'
	},
	Group: {
		group: 'Group',
		groupTitle: 'Group - {0}',
		newGroup: 'New Group',

		groupSaved: 'Group saved successfully',

		linkToTeam: 'Link to Team',
		linkToTeamTooltip: 'Users added to this group are also added to the team with the same name',
		linkToExternalGroup: 'Link to external group',
		linkToExternalGroupTooltip: 'Users from LDAP/AD group of the same name are automatically assigned to this group',

		nameInvalid: 'Please provide a name for this group',
		noRolesError: 'Please provide a role for this group',

		addRole: 'Add Role',
		removeRole: 'Remove Role',

		addUser: 'Add User',
		removeUser: 'Remove User',

		sysUpdatedOnDisplay: 'Updated On',
		sysCreatedOnDisplay: 'Created On',
		organizations : 'Organizations',
		addOrganization : 'Add Organization',
		removeOrganization : 'Remove Organization'
	},

	CopyGroup: {
		copyGroupTitle: 'Copy Group',
		copy: 'Copy',
		name: 'Name',
		nameInvalidBlank: 'Please provide a name for the new group',
		nameInvalidDuplicate: 'A group with the name \'{0}\' already exists, please enter another name'
	},

	Roles: {
		allRoles: 'Roles',
		roleDisplayName: 'Roles',

		createRole: 'New',
		deleteRole: 'Delete',

		deleteTitle: 'Confirm Deletion',
		deleteMessage: 'Are you sure you want to delete the selected role?',
		deletesMessage: 'Are you sure you want to delete the {0} selected roles?',
		deleteAction: 'Delete',
	},

	Role: {
		newRole: 'New Role',
		editRole: 'Edit Role',
		roleSaved: 'Role saved successfully',
		unameInvalid: 'Please provide a name for the role',

		view: 'View',
		create: 'Create',
		modify: 'Modify',
		delete: 'Delete',
		execute: 'Execute',

		roleName: 'Name',
		roleDescription: 'Description',
		permissions: 'Permissions',
		app: 'Application',
		applyPredefined: 'Apply Template'
	},

	CloneUser: {
		cloneUserTitle: 'Copy User',
		clone: 'Copy',
		name: 'Name',
		nameInvalidBlank: 'Please provide a name for the new user',
		nameInvalidDuplicate: 'A user with the name \'{0}\' already exists, please enter another name'
	},

	GroupPicker: {
		addGroupTitle: 'Add Group',
		removeGroupTitle: 'Remove Group',
		remove: 'Remove'
	},

	ProfileUpload: {
		save: 'Set as profile photo',
		clickToUpload: 'Select a photo from your computer',
		dragText: 'DRAG A PHOTO HERE',
		or: 'or',
		browse: 'Browse',
		profileIntro: 'Click "Browse" to select a photo for uploading or drag a photo to the area below to upload it.',

		uploadProfilePicTitle: 'Upload Profile Picture'
	},

	RemoveGroupPicker: {
		removeGroupPickerTitle: 'Remove Group',
		remove: 'Remove'
	},

	RemoveRolePicker: {
		removeRolePickerTitle: 'Remove Role',
		remove: 'Remove'
	},

	SIRPermissions: {
		// Keys
		sir: 'SIR Enabled',
		sirAnalytics: 'Analytics',
		sirAnalyticsGraph: 'Graphs',
		sirDashboard: 'Dashboard',
		sirDashboardCaseCreation: 'Case Creation',
		sirDashboardIncidentList: 'Incident List',
		sirIncident: 'Incidents',
		sirIncidentTitle: 'Title',
		sirIncidentClassification: 'Classification',
		sirIncidentType: 'Type',
		sirIncidentDataCompromised: 'Data Compromised',
		sirIncidentSeverity: 'Severity',
		sirIncidentStatus: 'Status',
		sirIncidentPriority: 'Priority',
		sirIncidentPlaybook: 'Playbook',
		sirIncidentOwner: 'Owner',
		sirIncidentTeam: 'Team',
		sirIncidentDueDate: 'Due Date',
		sirIncidentRiskScore: 'Risk Score',
		sirIncidentIncidentFlags: 'Incident Flags',
		sirIncidentMarkIncident: 'Mark Incident',

		sirIncidentViewer: 'Incident Viewer',
		sirIncidentViewerActivity: 'Task',
		sirIncidentViewerNote: 'Note',
		sirIncidentViewerArtifact: 'Artifact',
		sirIncidentViewerAttachment: 'Attachment',
		sirIncidentViewerTeam: 'Team',
		sirIncidentViewerActivityTimeline: 'Activity Timeline',

		sirPlaybookTemplates: 'Playbook Templates',
		sirPlaybookTemplatesTemplateBuilder: 'Template Builder',

		sirSettings: 'SIR Settings',

		analytics: 'Analytics',
		analyticsReporting: 'Reporting',

		investigationViewer: 'Investigation Viewer',
		dataCompromised: 'Data Compromised',
		team: 'Incident Team',
		activities: 'Activities',
		notes: 'Notes',
		artifacts: 'Artifacts',
		attachments: 'Attachments',

		playbookTemplates: 'Playbook Templates',
		templateBuilder: 'Template Builder',
		templateActivities: 'Activities',
		templateSections: 'Sections',
	},

	Analytics: {
		analytics: 'Analytics',
		analyticsReporting: 'Reporting'
	},

	SIRCheckboxGroup: {
		// Permissions
		enabled: 'Enabled',
		create: 'Create',
		view: 'View',
		viewAll: 'View All',
		change: 'Change',
		'assignee.change': 'Change Assignee',
		'status.change': 'Change Status',
		'status.change': 'Change Status',
		executeAutomation: 'Execute Automation',
		upload: 'Upload',
		download: 'Download',
		downloadMalicious: 'Download Malicious',
		unsetMalicious: 'Unset Malicious',
		delete: 'Delete',
		asMaster: 'As Master',
		asDuplicate: 'As Duplicate',
	},

	AnalyticsCheckboxGroup: {
		view: 'View',
	},

	UARCheckboxGroup: {
		view: 'View',
		edit: 'Edit',
		create: 'Create',
		close: 'Close',
		add: 'Add',
		delete: 'Delete',
		execute: 'Execute Wikis',
	},
	passwordMinLengthInvalid: 'Password must be a minimum of 8 characters long',
	passwordNumberRequiredInvalid: 'Password must contain a number',
	passwordCapitalLetterRequiredInvalid: 'Password must contain a capital letter',
	passwordLowercaseLetterRequiredInvalid: 'Password must contain a lower case letter',
	passwordConfirmMismatchInvalid: 'Passwords do not match',
	successfully_Changed_Pas_sword: 'Successfully change password.'

}