<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.resolve.util.JspUtils" %>
<%@page import="org.owasp.esapi.ESAPI"%>
<%
    String ver = ESAPI.encoder().encodeForURL(JspUtils.getResolveVersion());

    boolean debug = false;
    String d = request.getParameter("debug");
    if( d != null && d.indexOf("t") > -1){
        debug = true;
    }

    boolean debugSysAdmin = false;
    String dT = request.getParameter("debugSysAdmin");
    if( dT != null && dT.indexOf("t") > -1){
        debugSysAdmin = true;
    }

    boolean mock = false;
    String m = request.getParameter("mock");
    if( m != null && m.indexOf("t") > -1){
        mock = true;
    }
%>

<%
    if( debug || debugSysAdmin ){
%>
<script type="text/javascript" src="/resolve/sysadmin/js/sysadmin-classes.js?_v=<%=ver%>"></script>
<%
    }else{
%>
<script type="text/javascript" src="/resolve/sysadmin/js/sysadmin-classes.js?_v=<%=ver%>"></script>
<%
    }
%>

