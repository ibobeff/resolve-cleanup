/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.sysadmin').locale = {
	windowTitle: 'SysAdmin Project',
	uname: 'Name',
	name: 'Name',
	value: 'Value',
	uvalue: 'Value',
	sysId: 'Sys ID',

	ServerErrMsg: 'Server Error.',
	DeleteErrMsg: 'Cannot delete the selected records.',
	DeleteSucMsg: 'The selected records have been deleted.',
	save: 'Save',
	cancel: 'Cancel',
	back: 'Back',
	udescription: 'Description',
	sysCreatedOn: 'Created On',
	sysCreatedBy: 'Created By',
	sysUpdatedOn: 'Updated On',
	sysUpdatedBy: 'Updated By',
	invalidNameErr: 'The name should only contain characters,digits,dot and underscore and the length should be between 1 and 300',
	invalidDescriptionErr: 'The maximum length of the description is 4000',
	invalidValueErr: 'The maximum length of the value is 300',

	//grid headers - Domains
	sysOrganizationName: 'Domain',
	description: 'Description',
	disable: 'Active',
	//grid headers - configADs
	domainName: 'Domain Name',
	ipAddress: 'IP Address',
	uidAttribute: 'ID Attribute',
	mode: 'Mode',
	bindDn: 'Bind DN',
	baseDNList: 'Base DN List',
	gateway: 'Gateway',
	active: 'Active',
	isDefault: 'Default',
	ssl: 'SSL',
	fallback: 'Fallback',
	groupRequired : 'Group Required',
	version: 'Version',
	port: 'Port',
	// organizationName: 'Domain',

	roles: 'Roles',

	//Business Rules
	Name: 'Name',
	Table: 'Table',
	NonBlocking: 'Non-Blocking',
	Active: 'Active',
	Before: 'Before',
	Type: 'Type',
	Order: 'Order',
	Types: 'Types',
	CreatedOn: 'Created On',
	CreatedBy: 'Created By',
	UpdatedOn: 'Updated On',
	UpdatedBy: 'Updated By',
	script: 'Script',

	//System Scripts
	Description: 'Description',
	LastUpdated: 'LastUpdated',

	//ListRegistration
	ustatus: 'Status',
	uguid: 'Guid',
	utype: 'Type',
	uipaddress: 'Reg Address',
	uversion: 'Version',
	uconfig: 'Config',

	//menu sets
	applications: 'Applications',

	//system information
	user: 'User',
	host: 'Host',
	duration: 'Duration',

	//organizations
	organizationName: 'Name',

	//job schedules
	runbook: 'Runbook',
	expression: 'Expression',
	module: 'Module',

	active: 'Active',
	group: 'Group',
	query: 'Query',
	queryOrUrl: 'Query or Fully Qualified URL',
	queryOrUrlTooltip: 'Wiki Name or URL link, i.e. http://www.resolvesys.com',
	//toolbar config
	openAs: 'Open As',
	tooltip: 'Tooltip',

	SystemProperties: {
		NoSelectionErr: 'No item is selected.',
		createProperty: 'New',
		editProperty: 'Edit',
		deleteProperties: 'Delete',
		help: 'Help',
		systemPropertiesTitle: 'System Properties',
		systemProperties: 'System Properties',
		deleteTitle: 'Delete System Properties',
		deletesMessage: 'Are you sure you want to delete the selected system properties?({len} properties selected)',
		deleteMessage: 'Are you sure you want to delete the selected system properties?',
		deleteAction: 'Delete',
		getListError: 'Get System Property Error',
		getListErrorMsg: 'Cannot get system properties from server.',
		expandAll: 'Expand All',
		collapseAll: 'Collapse All',
		deleteSucMsg: 'The selected records have been deleted.'
	},
	SystemProperty: {
		createSystemPropertyTitle: 'Create System Property',
		editSystemPropertyTitle: 'Edit System Property',
		save: 'Save',
		deleteProperty: 'Delete',
		saveTitle: 'Save System Property',
		saveAction: 'Save',
		saveSuccess: 'The system property has been saved.',
		saveFailure: 'Failed to save the system property',
		ok: 'OK',
		organization: 'Domain',
		organizations: 'Domains',
		toParent: 'OK',
		GetSysPropErrMsg: 'Cannot get the system property.',
		SaveSysPropErrMsg: 'Cannot save the system property.',
		deleteFailureMsg: 'Cannot delete the system properties.'
	},
	Organizations: {
		organizationsDisplayName: 'Domains',
		createOrganization: 'New',
		disableOrganization: 'Disable',
		disableTitle: 'Disable Domains',
		disablesMessage: 'Are you sure you want to disable the {0} selected domains?',
		disableMessage: 'Are you sure you want to disable the selected domain?',
		disableAction: 'Disable',

		enableOrganization: 'Enable',
		enableTitle: 'Enable Domains',
		enablesMessage: 'Are you sure you want to enable the {0} selected domains?',
		enableMessage: 'Are you sure you want to enable the selected domain?',
		enableAction: 'Enable',

		deleteOrganization: 'Delete',

		getListError: 'Get Domains Error',
		getListErrorMsg: 'Cannot get Domains from server.',

		cancel: 'Cancel'
	},
	Organization: {
		createOrganizationTitle: 'Create Domain',
		editOrganizationTitle: 'Edit Domain',
		saveOrg: 'Save',
		deleteOrg: 'Delete',
		cancel: 'Cancel',
		saveTitle: 'Save Domain',
		saveAction: 'Save',
		organizationSaved: 'Domain saved',
		orgNameRequired: 'Please provide an domain name',
		orgNameMaxLength: 'Domain name cannot be longer than 300 characters',
		orgDescriptionMaxLength: 'Domain description cannot be longer than 4000 characters',
		uorganizationName: 'Name',
		udescription: 'Description'
	},
	ConfigADs: {
		configADsTitle: 'Config Active Directory',
		createConfigAD: 'New',
		deleteConfigAD: 'Delete',
		deleteConfigADMessage: 'Are you sure you want to delete the selected Active Directory?',
		deleteConfigADsMessage: 'Are you sure you want to delete the {len} selected Active Directories?',
		deleteAction: 'Delete',

		getListError: 'Got Config AD Error',
		getListErrorMsg: 'Cannot get Active Directory from server.'
	},
	//--------------for Config AD/LDAP/RADIUS----------------//
	umode: 'Mode',
	uipAddress: 'IP Address',
	ucryptType: 'Crypt Type',
	ucryptPrefix: 'Crypt Prefix',
	ubindDn: 'Bind DN',
	ubindPassword: 'Bind Password',
	uversion: 'Version',
	uactive: 'Active',
	ussl: 'SSL',
	ufallback: 'Fallback',
	uisDefault: 'Default',
	udefaultDomain: 'DefaultDomain',
	uuidAttribute: 'User ID Attribute',
	upasswordAttribute: 'Password Attribute',
	udomain: 'Domain Name',
	ubaseDNList: 'Base DN List',
	uproperties: 'Properties',
	ugateway: 'Gateway',
	uport: 'Port',
	uprimaryHost: 'Primary Host',
	usecondaryHost: 'Secondary Host',
	uauthProtocol: 'Authentication Protocol',
	uauthPort: 'Authentication Port',
	uacctPort: 'Accounting Port',
	usharedSecret: 'Shared Secret',

	propertiesTitle: 'Properties',
	addProperties: 'Add',
	deleteProperty: 'Delete',

	addBaseDn: 'Add',
	deleteBaseDn: 'Delete',

	invalidDomain: 'The Domain field should not be empty.',
	baseDNListTitle: 'Base DN List',

	//---------------------------------------------------//
	ConfigAD: {
		configADTitle: 'Active Directory Config',
		configADTitle: 'Active Directory Config',

		saveADConfig: 'Save',
		belongsToOrganization: 'Resolve Domain',
		SaveSucTitle: 'Config Saved',
		SaveErrMsg: 'Cannot save the cofiguration.[{0}]',
		SaveSucMsg: 'The Configuration has been successfully saved.',
		confirmSuc: 'OK',
		LoadAuthGatewayErr: 'Cannot get gateways from the server.'
	},
	BusinessRules: {
		createBusinessRule: 'New',
		deleteBusinessRules: 'Delete',
		displayName: 'Business Rules',
		DeleteBusinessRulesTitle: 'Delete Business Rules',
		DeleteBusinessRulesMsg: 'Are you sure you want to delete the {0} selected items?',
		DeleteBusinessRuleMsg: 'Are you sure you want to delete the selected item?',
		confirmDelete: 'OK',
		ListBusinessRulesErrMsg: 'Cannot get business rules from the server',
		DeleteBusinessRulesErrMsg: 'Cannot delete the business rules.',
		DeleteBusinessRulesSucMsg: 'The selected records has been deleted.'
	},
	BusinessRule: {
		saveBusinessRule: 'Save',
		save: 'Save',
		SaveSucMsg: 'The business rule has been successfully saved.',
		SaveErrMsg: 'Cannot save the business rule.',
		invalidName: 'The name should only contain A-Z,a-z,0,dot,space and the first and last character cannot be dot and space',
		invalidOrder: 'The order should be integer.',
		invalidType: 'The type should not be empty.',
		businessRuleTitle: 'Business Rule',
		invalidTable: 'Please provide a table for this business rule',
		GetErrMsg: 'Cannot get the business rule from the server.[{0}]'
	},
	SystemScripts: {
		systemScriptsTitle: 'System Scripts',
		createScript: 'New',
		deleteScripts: 'Delete',
		DeleteScriptsTitle: 'Delete System Scripts',
		DeleteScriptsMsg: 'Are you sure you want to delete the {0} selected items?',
		DeleteScriptMsg: 'Are you sure you want to delete the selected item?',
		confirmDelete: 'OK',
		LoadErrMsg: 'Cannot get system scripts from the server.'
	},
	SystemScript: {
		saveScript: 'Save',
		scriptTitle: 'System Script Information',
		udescription: 'Description',
		SaveSucTitle: 'System Script Saved',
		SaveErrMsg: 'Cannot save the system script.',
		SaveSucMsg: 'The system script has been successfully saved.',
		confirmSaveSuc: 'OK',
		execute: 'Execute',
		ExeSucTitle: 'Script Executed',
		ExeSucMsg: 'Succssfully executed the system script.',
		confirmSuc: 'OK',
		invalidName: 'the name should only contain A-Z,a-z,0,dot,space and the first and last character cannot be dot and space',
		sysScriptTitle: 'System Script',
		LoadErrMsg: 'Cannot get the system script from the server.'
	},

	ApplicationRights: {
		applicationRightsDisplayName: 'Application Rights',
		createApplicationRight: 'New',
		deleteApplicationRight: 'Delete',

		deleteTitle: 'Confirm Deletion',
		deleteMessage: 'Are you sure you want to delete the selected application right?',
		deletesMessage: 'Are you sure you want to delete the {0} selected application rights?',
		deleteAction: 'Delete'
	},

	ApplicationRight: {
		applicationRight: 'Application Right',
		back: 'Back',
		save: 'Save',
		uappName: 'Name',
		ApplicationRightSaved: 'Application Right successfully saved',
		roles: 'Roles',
		controllers: 'Controllers',
		organizations: 'Domain',

		addOrganization: 'Add',
		removeOrganization: 'Remove',

		addRole: 'Add',
		removeRole: 'Remove',

		addController: 'Add',
		removeController: 'Remove',

		nameInvalid: 'Please provide a name for the application right'
	},

	RolePicker: {
		addRoleTitle: 'Add Role',
		keyword: 'Search',
		removeRoleTitle: 'Remove Role',
		add: 'Add',
		remove: 'Remove',
		cancel: 'Cancel'
	},

	ControllerPicker: {
		addControllerTitle: 'Add Controller',
		add: 'Add',
		cancel: 'Cancel'
	},

	OrganizationPicker: {
		addOrganizationTitle: 'Add Domain',
		add: 'Add',
		cancel: 'Cancel'
	},

	ConfigLDAPs: {
		invalidJSON: 'The server replies with a invalid JSON string',
		displayName: 'LDAP',
		createRecord: 'New',
		deleteRecords: 'Delete',
		DeleteTitle: 'Delete LDAP Properties',
		DeleteMsg: 'Are you sure you want to delete the selected item?',
		DeletesMsg: 'Are you sure you want to delete the selected items?({num} items selected)',
		confirmDelete: 'OK',
		ListRecordsErr: 'Cannot get ldaps from the server'
	},

	ConfigLDAP: {
		LdapTitle: 'LDAP',
		saveLdap: 'Save',
		GetErrMsg: 'Cannot get LDAP from server.',
		SaveSucTitle: 'LDAP Proerty Saved',
		SaveSucMsg: 'The LDAP property has been successfully saved.',
		SaveErrMsg: 'Cannot save the LDAP property.[{0}]',
		confirmSuc: 'OK',
		belongsToOrganization: 'Resolve Domain',
		LoadAuthGatewayErr: 'Cannot get gateways from the server.'
	},

	ConfigRADIUSs: {
		invalidJSON: 'The server replies with a invalid JSON string',
		displayName: 'RADIUS',
		createRecord: 'New',
		deleteRecords: 'Delete',
		DeleteTitle: 'Delete RADIUS Properties',
		DeleteMsg: 'Are you sure you want to delete the selected item?',
		DeletesMsg: 'Are you sure you want to delete the selected items?({num} items selected)',
		confirmDelete: 'OK',
		ListRecordsErr: 'Cannot get radius from the server'
	},

	ConfigRADIUS: {
		RadiusTitle: 'RADIUS',
		saveRadius: 'Save',
		GetErrMsg: 'Cannot get RADIUS from server.',
		SaveSucTitle: 'RADIUS Proerty Saved',
		SaveSucMsg: 'The RADIUS property has been successfully saved.',
		SaveErrMsg: 'Cannot save the RADIUS property.[{0}]',
		confirmSuc: 'OK',
		belongsToOrganization: 'Resolve Domain',
		LoadAuthGatewayErr: 'Cannot get gateways from the server.',
		invalidPrimaryHost: 'The Primary Host field should not be empty.',
		invalidSharedSecret: 'The Shared Secret field should not be empty.'
	},

	JobSchedules: {
		jobSchedulesDisplayName: 'Job Schedules',
		createJobSchedule: 'New',
		deleteJobSchedule: 'Delete',
		executeJobSchedule: 'Execute',
		executeJobScheduleTooltip: 'Click to execute this runbook immediately',

		activateJobSchedule: 'Activate',
		deactivateJobSchedule: 'Deactivate',

		activateTitle: 'Confirm Activation',
		activateMessage: 'Are you sure you want to activate the selected job schedule?',
		activatesMessage: 'Are you sure you want to activate the {0} selected job schedules?',
		activateAction: 'Activate',

		deactivateTitle: 'Confirm Deactivation',
		deactivateMessage: 'Are you sure you want to deactivate the selected job schedule?',
		deactivatesMessage: 'Are you sure you want to deactivate the {0} selected job schedules?',
		deactivateAction: 'Deactivate',

		deleteTitle: 'Confirm Deletion',
		deleteMessage: 'Are you sure you want to delete the selected job schedule?',
		deletesMessage: 'Are you sure you want to delete the {0} selected job schedules?',
		deleteAction: 'Delete',

		executeTitle: 'Confirm Execution',
		executeMessage: 'Are you sure you want to execute the selected job schedule?',
		executesMessage: 'Are you sure you want to execute the {0} selected job schedules?',
		executeAction: 'Execute',
		executeSuccess: 'Job Schedule executed successfully',
		executesSuccess: 'Job Schedules executed successfully'
	},
	JobSchedule: {
		executeJobSchedule: 'Execute',
		executeJobScheduleTooltip: 'Click to execute this runbook immediately',
		executeSuccess: 'Job Schedule executed successfully',

		jobSchedule: 'Job Schedule',
		JobScheduleSaved: 'Job Schedule saved',
		schedule: 'Schedule',
		seconds: 'Seconds',
		minutes: 'Minutes',
		hours: 'Hours',
		month: 'Month',
		dayType: 'Day Type',
		dayOfMonthRadio: 'Day of month',
		dayOfWeekRadio: 'Day of week',
		dayOfMonth: 'Day of Month',
		dayOfWeek: 'Day of Week',
		parameters: 'Parameters',

		timeRange: 'Time Range',
		startTime: 'Start Time',
		endTime: 'End Time',

		uname: 'Name',
		unameDisplay: 'Name',
		umodule: 'Module',
		urunbook: 'Runbook',
		value: 'Value',

		addParam: 'Add',
		removeParam: 'Remove',

		secondsTooltip: 'Seconds: Allowed values (0-59); Allowed Special Characters (,-*/)',
		minutesTooltip: 'Minutes: Allowed values (0-59); Allowed Special Characters (,-*/)',
		hoursTooltip: 'Hours: Allowed values (0-23); Allowed Special Characters (,-*/)',
		monthTooltip: 'Month: Allowed values (1-12 or JAN-DEC); Allowed Special Characters (,-*/)',
		dayOfMonthTooltip: 'Day of month: Allowed values (1-31); Allowed Special Characters (,-*/LW)',
		dayOfWeekTooltip: 'Day of week: Allowed values (1-7 or SUN-SAT); Allowed Special Characters (.-*/L#)',

		executeTitle: 'Confirm Execution',
		executeMessage: 'Are you sure you want to execute this job schedule?',
		executeAction: 'Execute',

		nameInvalid: 'Please provide a name for the job schedule',
		secondsInvalid: 'Please provide a value for seconds, * is the default',
		minutesInvalid: 'Please provide a value for minutes, * is the default',
		hoursInvalid: 'Please provide a value for hours, * is the default',
		monthInvalid: 'Please provide a value for month, * is the default'
	},

	ListRegistrations: {
		listRegistrationsDisplayName: 'List Registrations',
		deleteListRegistration: 'Delete',

		deleteTitle: 'Confirm Deletion',
		deleteMessage: 'Are you sure you want to delete the selected list registration?',
		deletesMessage: 'Are you sure you want to delete the {0} selected list registrations?',
		deleteAction: 'Delete'
	},
	ListRegistration: {
		title: 'Registration'
	},

	MenuDefinitions: {
		menuDefinitionsDisplayName: 'Menu Definitions',
		createMenuDefinition: 'New',
		deleteMenuDefinition: 'Delete',

		deleteTitle: 'Confirm Deletion',
		deleteMessage: 'Are you sure you want to delete the selected menu definition?',
		deletesMessage: 'Are you sure you want to delete the {0} selected menu definitions?',
		deleteAction: 'Delete',
		draganddroptip : '<i>* Drag and drop re-order menu definition.</i>'
	},
	MenuDefinition: {
		menuDefTitle: 'Menu Definition',
		addRole: 'Add',
		addMenuItem: 'Add Item',
		addGroup: 'Add Group',
		newGroup: 'New Group',
		removeMenuItem: 'Remove',
		removeRole: 'Remove',
		menuItems: 'Menu Items',
		fontColor: 'Font:',
		backgroundColor: 'Background:',
		MenuDefinitionSaved: 'Menu Definition successfully saved',
		titleInvalid: 'Please provide a title for this menu definition'
	},

	MenuSets: {
		menuSetsDisplayName: 'Menu Sets',
		createMenuSet: 'New',
		deleteMenuSet: 'Delete',

		deleteTitle: 'Confirm Deletion',
		deleteMessage: 'Are you sure you want to delete the selected menu set?',
		deletesMessage: 'Are you sure you want to delete the {0} selected menu sets?',
		deleteAction: 'Delete',
		draganddroptip : '<i>* Drag and drop re-order menu set.</i>'
	},
	MenuSet: {
		title: 'Menu Set',
		addRole: 'Add',
		removeRole: 'Remove',
		menus: 'Menus',
		addMenu: 'Add',
		removeMenu: 'Remove',
		MenuSetSaved: 'Menu Set successfully saved',
		nameInvalid: 'Please provide a name for this menu set'
	},

	MenuPicker: {
		addMenuTitle: 'Menus',
		add: 'Add'
	},

	MenuItem: {
		menuItem: 'Menu Item',
		addMenuItem: 'Add',
		applyMenuItem: 'Apply',
		addRole: 'Add',
		removeRole: 'Remove',
		ItemAlreadyExist: 'MenuItem:{title} already exists.'
	},

	ServerInformation: {
		systemInformationTitle: 'System Information',

		version: 'Version',
		build: 'Build',
		server: 'Server',
		osArchitecture: 'Architecture',
		osName: 'OS',
		osVersion: 'Version',
		address: 'Address',
		osProcessor: 'Processor',
		jvmVendor: 'JVM Vendor',
		jvmName: 'JVM Name',
		jvmVersion: 'JVM Version',
		memory: 'Memory',
		memoryMax: 'Max Memory',
		memoryTotal: 'Total Memory',
		memoryFreePercentage: 'Free Memory',
		thread: 'Thread',
		threadPeak: 'Peak Thread',
		threadLive: 'Live Thread',
		threadDeamon: 'Daemon Thread',

		sessions: 'Sessions'
	},

	ToolbarConfiguration: {
		tooltip: 'Tooltip',
		toolbarConfTitle: 'Toolbar Definition',
		addToolbarItem: 'Add Item',
		removeToolbarItem: 'Remove',
		addGroup: 'Add Group',
		newGroup: 'New Group',
		newItem: 'New Item',
		addRoles: 'Add Roles',
		removeRoles: 'Remove Roles',
		SaveSucMsg: 'The toolbar configuration is saved.',
		roleDisplayName: 'Roles',
		reset: 'Reset',
		newText: 'New',
		menuText: 'Menu',
		socialText: 'Social',
		worksheetText: 'Worksheet',
		documentText: 'Document',
		activeWorksheetText: 'Active Worksheet',
		newWorksheetText: 'New Worksheet',
		allWorksheetsText: 'All Worksheets',
		defaultDocumentText: 'HOMEPAGE',
		addDocumentText: 'Add Document',
		ResetTitle: 'Reset Toolbar',
		ResetConfirmation: 'Are you sure you want to reset? All your changes will be lost.',
		yes: 'Yes',
		no: 'No',
		dump: 'Add',
		dumpRemove: 'Remove',
		asWindowTitle: 'Roles',
		noMenuTitle: 'Warning',
		noMenu: 'Menu is not in the current toolbar configuration.',
		GetErrMsg: 'Cannot get toolbar configuration from the server.'
	},

	username: 'Username',
	createTime: 'Create Time',
	updateTime: 'Update Time',
	durationInSecs: 'Duration(secs)',
	sessionDurationInMins: 'Remaining (min)',
	ssoType: 'SSO Type',
	ssoSessionId: 'SSO Session Id',
	lastSSOValidationTime: 'Session Validation Time',
	iframeId: 'iframe Id',
	sessionId: 'iframe Session Id',

	SessionInformation: {
		userInfoTitle: 'User Session Information',
		removeSessions: 'Remove',
		sessionSummary: 'Session Summary',
		activeSessions: 'Active Sessions',
		maxActiveSessions: 'Max Active Sessions:',
		maxSessions: 'Max Sessions',
		timeout: 'Timeout',
		sessionDetails: 'Session Details',
		LoadUserSessionInfoErrMsg: 'Cannot load user session information.',

		RemoveSessionTitle: 'Confirm Remove Session',
		RemoveSessionMsg: 'Are you sure you want to remove the selected session?',
		RemoveSessionMsgs: 'Are you sure you want to remove {0} selected sessions?',
		remove: 'Remove',
		cancel: 'Cancel'
	},

	propertiesTab : 'Properties',
	sourceTab : 'Source',
	ruleTab : 'Rule',
	scriptTab : 'Script'
}