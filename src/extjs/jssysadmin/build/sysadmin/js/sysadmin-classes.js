/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
Ext.define('RS.sysadmin.model.SystemProperty', {
	extend: 'Ext.data.Model',
	fields: [
		'id',
		'uname',
		'udescription',
		'uvalue',{
			name: 'group',
			convert: function(newValue, model) {
				return RS.common.grid.htmlRenderer()((model.get('uname').split('\.'))[0]);
			}
		}
	].concat(RS.common.grid.getSysFields())
});
glu.defModel('RS.sysadmin.ApplicationRight', {

	mock: false,

	applicationRightTitle$: function() {
		return this.localize('applicationRight') + (this.uappName ? ' - ' + this.uappName : '')
	},

	activate: function(screen, params) {
		this.resetForm()
		this.set('id', params ? params.id : '')
		this.loadApplicationRight()
	},

	resetForm: function() {
		this.loadData(this.defaultData)
		this.roles.removeAll()
		this.controllers.removeAll()
		this.organizations.removeAll()
	},

	defaultData: {
		uappName: ''
	},

	fields: ['id', 'uappName'].concat(RS.common.grid.getSysFields()),
	id: '',
	uappName: '',
	uappNameIsValid$: function() {
		if (!this.uappName) return this.localize('nameInvalid')
		return true
	},
	sysCreatedOn: '',
	sysCreatedOnDisplay$: function() {
		return Ext.isDate(this.sysCreatedOn) ? Ext.Date.format(this.sysCreatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysUpdatedOn: '',
	sysUpdatedOnDisplay$: function() {
		return Ext.isDate(this.sysUpdatedOn) ? Ext.Date.format(this.sysUpdatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysCreatedBy: '',
	sysUpdatedBy: '',

	sysOrganizationName: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)

		this.set('rolesColumns', [{
			header: '~~name~~',
			dataIndex: 'uname',
			filterable: true,
			sortable: true,
			flex: 1
		}])

		this.set('controllersColumns', [{
			header: '~~name~~',
			dataIndex: 'ucontrollerName',
			filterable: true,
			sortable: true,
			flex: 1
		}])

		this.set('organizationsColumns', [{
			header: '~~name~~',
			dataIndex: 'uorganizationName',
			filterable: true,
			sortable: true,
			flex: 1
		}])
	},

	loadApplicationRight: function() {
		if (this.id) {
			this.roles.removeAll()
			this.controllers.removeAll()
			this.organizations.removeAll()
			this.ajax({
				url: '/resolve/service/apps/get',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.loadData(response.data)
						this.originalResponse = response.data
						//load roles
						if (Ext.isArray(response.data.appRoles)) Ext.Array.forEach(response.data.appRoles, function(role) {
							this.roles.add(role)
						}, this)

						//load organizations
						if (Ext.isArray(response.data.appOrgs)) Ext.Array.forEach(response.data.appOrgs, function(organization) {
							this.organizations.add(organization)
						}, this)

						//load controllers
						if (Ext.isArray(response.data.appControllers)) Ext.Array.forEach(response.data.appControllers, function(controller) {
							this.controllers.add(controller)
						}, this)
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},

	refresh: function() {
		this.loadApplicationRight()
	},

	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.persistApplicationRight, this, [exitAfterSave])
		this.task.delay(500)
		this.set('persisting', true)
	},
	saveIsEnabled$: function() {
		return !this.persisting && this.isValid
	},
	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	persisting: false,
	persistApplicationRight: function(exitAfterSave) {
		var roles = [],
			controllers = [],
			organizations = [];

		this.roles.each(function(role) {
			roles.push(role.data)
		}, this)

		this.controllers.each(function(controller) {
			controllers.push(controller.data)
		}, this)

		this.organizations.each(function(organization) {
			organizations.push(organization.data)
		}, this)

		this.ajax({
			url: '/resolve/service/apps/save',
			jsonData: {
				id: this.id,
				uappName: this.uappName,
				appRoles: roles,
				appControllers: controllers,
				appOrgs: organizations
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize('ApplicationRightSaved'))
					if (exitAfterSave === true) clientVM.handleNavigation({
						modelName: 'RS.sysadmin.ApplicationRights'
					})
					else {
						this.set('id', response.data.id)
					}
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('persisting', false);
			}
		})
	},

	back: function() {
		clientVM.handleNavigationBack()
	},

	roles: {
		mtype: 'store',
		sorters: ['uname'],
		fields: ['id', 'uname'],
		proxy: {
			type: 'memory'
		}
	},
	rolesSelections: [],
	rolesColumns: [],

	addRole: function() {
		this.open({
			mtype: 'RS.sysadmin.RolePicker'
		})
	},
	removeRole: function() {
		Ext.Array.forEach(this.rolesSelections, function(r) {
			this.roles.remove(r)
		}, this)
	},
	removeRoleIsEnabled$: function() {
		return this.rolesSelections.length > 0
	},

	controllers: {
		mtype: 'store',
		sorters: ['ucontrollerName'],
		fields: ['id', 'ucontrollerName'],
		proxy: {
			type: 'memory'
		}
	},
	controllersSelections: [],
	controllersColumns: [],

	addController: function() {
		this.open({
			mtype: 'RS.sysadmin.ControllerPicker'
		})
	},
	removeController: function() {
		Ext.Array.forEach(this.controllersSelections, function(c) {
			this.controllers.remove(c)
		}, this)
	},
	removeControllerIsEnabled$: function() {
		return this.controllersSelections.length > 0
	},

	organizations: {
		mtype: 'store',
		sorters: ['uorganizationName'],
		fields: ['id', 'uorganizationName'],
		proxy: {
			type: 'memory'
		}
	},
	organizationsSelections: [],
	organizationsColumns: [],

	addOrganization: function() {
		this.open({
			mtype: 'RS.sysadmin.OrganizationPicker'
		})
	},
	removeOrganization: function() {
		Ext.Array.forEach(this.organizationsSelections, function(c) {
			this.organizations.remove(c)
		}, this)
	},
	removeOrganizationIsEnabled$: function() {
		return this.organizationsSelections.length > 0
	}
})
glu.defModel('RS.sysadmin.ApplicationRights', {

	mock: false,

	activate: function() {
		this.applicationRights.load()
	},

	columns: [],
	applicationRightsSelections: [],
	allSelected: false,

	displayName: '',
	stateId: 'applicationRights',

	applicationRights: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uappName'],
		fields: ['id', 'uappName', 'roles'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/apps/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)

		this.applicationRights.proxy.on('exception', function(e, resp, op) {
			clientVM.displayExceptionError(e, resp, op);
		}, this);

		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'uappName',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~roles~~',
			dataIndex: 'roles',
			filterable: false,
			sortable: false,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))

		this.set('displayName', this.localize('applicationRightsDisplayName'))
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createApplicationRight: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ApplicationRight'
		})
	},
	editApplicationRight: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ApplicationRight',
			params: {
				id: id
			}
		})
	},
	deleteApplicationRightIsEnabled$: function() {
		return this.applicationRightsSelections.length > 0
	},
	deleteApplicationRight: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.applicationRightsSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.applicationRightsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/apps/delete',
					params: {
						ids: '',
						whereClause: this.applicationRights.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('applicationRightsSelections', [])
							this.applicationRights.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.applicationRightsSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/apps/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('applicationRightsSelections', [])
							this.applicationRights.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	}
})
glu.defModel('RS.sysadmin.BusinessRule', {
	mock: false,
	id: '',
	uname: '',
	uorder: 0,
	utable: '',
	uactive: false,
	uasync: false,
	ubefore: false,
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrganizationName: '',
	utype: [],
	fields: ['id', 'uname', {
		name: 'uorder',
		type: 'long'
	}, 'utable', {
		name: 'uactive',
		type: 'boolean'
	}, {
		name: 'uasync',
		type: 'boolean'
	}, {
		name: 'ubefore',
		type: 'boolean'
	}, 'utype', 'uscript', {
		name: 'sysCreatedOn',
		type: 'long'
	}, 'sysCreatedBy', {
		name: 'sysUpdatedOn',
		type: 'long'
	}, 'sysUpdatedBy', 'sysOrganizationName'],

	defaultData: {
		id: '',
		uname: '',
		uorder: 0,
		utable: '',
		uactive: false,
		uasync: false,
		ubefore: false,
		utype: [],
		uscript: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrganizationName: ''
	},


	tableStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/common/resolveTables',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	typeStore: {
		mtype: 'store',
		fields: ['name', {
			name: 'value',
			type: 'auto'
		}],
		data: [{
			"name": "insert",
			"value": "insert"
		}, {
			"name": "delete",
			"value": "delete"
		}, {
			"name": "update",
			"value": "update"
		}, {
			"name": "select",
			"value": "select"
		}]
	},

	businessRuleTitle: '',

	uscript: '',

	state: '',

	propCollapsed: false,
	rows: 47,
	activeItem : 0,
	ruleTabIsPressed$ : function(){
		return this.activeItem == 0;
	},
	scriptTabIsPressed$ : function(){
		return this.activeItem == 1;
	},
	ruleTab : function(){
		this.set('activeItem', 0);
	},
	scriptTab : function(){
		this.set('activeItem', 1);
	},
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},
	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.set('propCollapsed', false);
		var id = this.id || '';
		this.resetForm()
		this.set('id', params && params.id != null ? params.id : id);
		this.set('state', 'ready');
		if (this.id != '') {
			this.loadBusinessRule();
			return;
		}
		this.set('activeItem',0);
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true);
		this.loadTables();
		this.set('businessRuleTitle', this.localize('businessRuleTitle'));
	},

	loadTables: function() {
		this.tableStore.load({
			scope: this,
			callback: function(recs, op, suc) {
				/*
				if (!suc) {
					clientVM.displayError(this.localize('LoadTableErrMsg'));
				}
				*/
			}
		})
	},

	loadBusinessRule: function() {
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/businessrules/get',
			params: {
				id: this.id
			},
			scope: this,
			success: function(resp) {
				this.handleLoadSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	handleLoadSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('GetErrMsg', respData.message));
			return;
		}
		var data = respData.data;
		this.setData(data);
	},

	setData: function(data) {
		this.set('id', data.id);
		this.set('uname', data.uname);
		this.set('uorder', data.uorder);
		this.set('utable', data.utable);
		this.set('uactive', data.uactive);
		this.set('uasync', data.uasync);
		this.set('ubefore', data.ubefore);
		this.set('uscript', data.uscript);
		this.set('sysCreatedOn', data.sysCreatedOn);
		this.set('sysCreatedBy', data.sysCreatedBy);
		this.set('sysUpdatedOn', data.sysUpdatedOn);
		this.set('sysUpdatedBy', data.sysUpdatedBy);
		this.set('utype', data.utype.split(','));
		this.commit();
	},

	saveBusinessRule: function(exitAfterSave) {
		this.set('state', 'wait');
		var data = {
			id: this.id,
			UName: this.uname,
			UOrder: this.uorder,
			UTable: this.utable,
			UActive: this.uactive,
			UAsync: this.uasync,
			UBefore: this.ubefore,
			UType: this.utype,
			UScript: this.uscript
		};
		data.UType = this.utype.join(',');

		this.ajax({
			url: '/resolve/service/businessrules/save',
			jsonData: data,
			scope: this,
			success: function(resp) {
				this.handleSaveSucResp(resp);
				if (exitAfterSave === true)
					clientVM.handleNavigation({
						modelName: 'RS.sysadmin.BusinessRules'
					});
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	save: function(exitAfterSave) {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveBusinessRule, this, [exitAfterSave])
		this.task.delay(500)
		this.set('state', 'wait')
	},
	saveIsEnabled$: function() {
		return !this.state != 'wait' && this.isValid;
	},

	saveAndExit: function() {
		if (!this.saveIsEnabled)
			return;
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	handleSaveSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']')
			return;
		}

		var data = respData.data;
		this.setData(data);
		clientVM.displaySuccess(this.localize('SaveSucMsg'));
	},

	resetForm: function() {
		this.loadData(this.defaultData);
		this.set('isPristine', true);
		this.set('uname', ''); // set after isPristine so uname field has red exclamation
		this.set('utable', '');
		this.set('utype', []);
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.BusinessRules'
		});
		this.resetForm();
	},

	refresh: function() {
		if (this.id) {
			this.loadBusinessRule();
		} else {
			this.resetForm();
		}
	},
	saveBusinessRuleIsEnabled$: function() {
		return this.isValid && this.state != 'wait' && this.isDirty;
	},

	unameIsValid$: function() {
		var rexp1 = /^[A-Za-z0-9_ ]+[\\.]([A-Za-z0-9_ ]+[\\.])*[A-Za-z0-9_ ]+$/;
		var rexp2 = /^[A-Za-z0-9_ ]+$/;
		return this.uname != null &&
			(rexp1.test(this.uname) || rexp2.test(this.uname)) &&
			this.uname != '' ? true : this.localize('invalidName');
	},

	utableIsValid$: function() {
		return this.utable != null && this.utable != '' ? true : this.localize('invalidTable');
	},
	uorderIsValid$: function() {
		var rexp = /^\d*$/;
		return this.uorder != null && this.uorder !== '' &&
			rexp.test(this.uorder) ? true : this.localize('invalidOrder');
	},
	utypeIsValid$: function() {
		return this.utype.length > 0 ? true : this.localize('invalidType');
	}
});
glu.defModel('RS.sysadmin.BusinessRules', {

	mock: false,

	state: '',
	stateId: 'rssysadminbussinessrules',

	displayName: '',
	store: {
		mtype: 'store',
		fields: ['id', 'sys_id', 'uname', 'utable', {
				name: 'uasync',
				type: 'bool'
			}, {
				name: 'uactive',
				type: 'bool'
			}, {
				name: 'ubefore',
				type: 'bool'
			}, 'utype', {
				name: 'uorder',
				type: 'float'
			}, {
				name: 'sysCreatedOn',
				type: 'date',
				format: 'time',
				dateFormat: 'time'
			}, 'sysOrganizationName',
			'sysCreatedBy', {
				name: 'sysUpdatedOn',
				type: 'date',
				format: 'time',
				dateFormat: 'time'
			}, 'sysUpdatedBy'
		],
		remoteSort: true,
		sorters: [{
			property: 'uname',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/businessrules/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	columns: null,
	businessRulesSelections: [],
	activate: function() {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.loadRecs();
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true);
		this.set('displayName', this.localize('displayName'));
		this.set('columns', [{
			header: '~~Name~~',
			dataIndex: 'uname',
			sortable: true,
			filterable: true,
			width: 190,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~Table~~',
			dataIndex: 'utable',
			filterable: true,
			flex: 1
		}, {
			header: '~~NonBlocking~~',
			dataIndex: 'uasync',
			filterable: true,
			width: 105,
			renderer: RS.common.grid.booleanRenderer()
		}, {
			header: '~~Active~~',
			dataIndex: 'uactive',
			filterable: true,
			width: 105,
			renderer: RS.common.grid.booleanRenderer()
		}, {
			header: '~~Before~~',
			dataIndex: 'ubefore',
			filterable: true,
			width: 105,
			renderer: RS.common.grid.booleanRenderer()
		}, {
			header: '~~Type~~',
			dataIndex: 'utype',
			filterable: true,
			width: 200
		}, {
			header: '~~Order~~',
			dataIndex: 'uorder',
			filterable: true,
			width: 60
		}, {
			header: '~~sysCreatedOn~~',
			dataIndex: 'sysCreatedOn',
			hidden: true,
			filterable: true,
			width: 200,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}, {
			header: '~~sysCreatedBy~~',
			dataIndex: 'sysCreatedBy',
			hidden: true,
			filterable: true,
			width: 120
		}, {
			header: '~~sysUpdatedOn~~',
			dataIndex: 'sysUpdatedOn',
			filterable: 1,
			hidden: true,
			width: 200,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}, {
			header: '~~sysUpdatedBy~~',
			dataIndex: 'sysUpdatedBy',
			hidden: true,
			filterable: true,
			width: 120
		}, {
			header: '~~sysId~~',
			dataIndex: 'sys_id',
			hidden: true,
			filterable: true,
			width: 300
		}]);
	},

	loadRecs: function() {
		this.set('state', 'wait');
		this.store.load({
			scope: this,
			callback: function(recs, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('ListBusinessRulesErrMsg'));
					return;
				}

				this.set('state', 'ready');
			}
		});
	},

	createBusinessRule: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.BusinessRule',
			params: {
				id: ''
			}
		});
	},

	deleteBusinessRules: function() {
		this.message({
			title: this.localize('DeleteBusinessRulesTitle'),
			msg: this.localize(this.businessRulesSelections.length > 0 ? 'DeleteBusinessRulesMsg' : 'DeleteBusinessRuleMsg', this.businessRulesSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmDelete'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.sendDeleteReq
		});
	},

	sendDeleteReq: function(btn) {
		if (btn != 'yes')
			return;
		this.set('state', 'wait');

		var ids = [];

		Ext.each(this.businessRulesSelections, function(r) {
			ids.push(r.get('id'));
		}, this)

		this.ajax({
			url: '/resolve/service/businessrules/delete',
			jsonData: ids,
			scope: this,
			success: this.handleSucDeleteResp,
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		})
	},

	handleSucDeleteResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('DeleteBusinessRulesErrMsg') + '[' + respData.message + ']');
		} else
			clientVM.displaySuccess(this.localize('DeleteBusinessRulesSucMsg'));
		this.loadRecs();
	},

	editBusinessRule: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.BusinessRule',
			params: {
				id: id
			}
		});
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createBusinessRuleIsEnabled$: function() {
		return this.state != 'wait';
	},

	deleteBusinessRulesIsEnabled$: function() {
		return this.state == 'itemSelected';
	},

	when_businessRulesSelections_changed: {
		on: ['businessRulesSelectionsChanged'],
		action: function() {
			if (this.businessRulesSelections.length > 0)
				this.set('state', 'itemSelected');
			else
				this.set('state', 'ready');
		}
	}
});
glu.defModel('RS.sysadmin.ConfigAD', {

	waitingResponse: false,
	title: '',

	state: '',

	//fields 
	fields: ['uipAddress', 'uuidAttribute', 'belongsToOrganization', 'umode', 'ubindDn', 'ubindPassword', 'udomain', 'ucryptType', 'ucryptPrefix', 'upasswordAttribute', 'belongsToOrganization',
		'ugateway', {
			name: 'ussl',
			type: 'bool'
		},  {

			name: 'groupRequired',
			type: 'bool'
		}, {

			name: 'ufallback',
			type: 'bool'
		}, {
			name: 'uversion',
			type: 'numeric'
		}, {
			name: 'uport',
			type: 'numeric'
		}
	].concat(RS.common.grid.getSysFields()),

	//local variables same as field names

	id: '',
	uipAddress: '',
	uuidAttribute: '',
	umode: '',
	ubindDn: '',
	ubindPassword: '',
	udomain: '',
	ucryptType: '',
	ucryptPrefix: '',
	upasswordAttribute: '',
	ubaseDNList: '',
	uproperties: '',
	ugateway: '',
	ussl: false,
	ufallback: false,
	uversion: 0,
	uport: 0,
	belongsToOrganization: '',

	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrganizationName: '',

	defaultData: {
		id: '',
		uipAddress: '',
		uuidAttribute: 'sAMAccountName',
		umode: 'BIND',
		ubindDn: '',
		ubindPassword: '',
		udomain: '',
		ubaseDNList: '',
		uproperties: '',
		ugateway: '',
		ussl: false,
		ufallback: true,
		groupRequired : false,
		uversion: 3,
		uport: 389,
		belongsToOrganization: '',

		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrganizationName: ''
	},

	organizations: {
		mtype: 'store',
		fields: ['sys_id', 'uorganizationName'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/common/organizationList',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	propertiesTitle: '',
	baseDNListTitle: '',

	properties: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	baseDNList: {
		mtype: 'store',
		fields: ['value'],
		proxy: {
			type: 'memory'
		}
	},

	baseDNColumns: [{
		header: '~~value~~',
		dataIndex: 'value',
		filterable: true,
		flex: 1,
		editor: {
			xtype: 'textfield',
			allowBlank: false
		}
	}],

	propertiesColumns: [{
		header: '~~name~~',
		dataIndex: 'name',
		filterable: true,
		flex: 1,
		editor: {
			xtype: 'textfield',
			allowBlank: false
		}
	}, {
		header: '~~value~~',
		dataIndex: 'value',
		filterable: true,
		flex: 1,
		editor: {
			xtype: 'textfield',
			allowBlank: false
		}
	}],

	selectedBaseDN: null,
	selectedProp: null,
	authGatewayStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.loadAuthGateway();
		this.resetForm()
		this.set('id', params ? params.id : '');
		this.set('state', 'ready');
		if (this.id != '')
			this.loadConfigAD()
	},
	init: function() {
		this.loadOrg();
		this.set('propertiesTitle', this.localize('propertiesTitle'));
		this.set('baseDNListTitle', this.localize('baseDNListTitle'));
		this.set('title', this.localize('configADTitle'));
		this.cellEditing = new Ext.grid.plugin.CellEditing({
			clicksToEdit: 1
		});
	},

	loadOrg: function() {
		this.set('state', 'wait');
		this.organizations.load({
			scope: this,
			callback: function(recs, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('GetOrgErrMsg'));
					return;
				}
				this.set('state', 'ready')
			}
		});
	},

	loadConfigAD: function() {
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/configad/get',
			params: {
				id: this.id
			},
			scope: this,
			success: function(resp) {
				this.handleLoadSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	loadAuthGateway: function() {
		this.ajax({
			url: '/resolve/service/configad/listAuthGateways',
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success)
					clientVM.displayError(this.localize('LoadAuthGatewayErr') + '[' + respData.message + ']');
				var records = respData.records;
				this.authGatewayStore.removeAll();
				Ext.each(records, function(r) {
					this.authGatewayStore.add({
						name: r,
						value: r
					});
				}, this);
				if (this.authGatewayStore.getCount() > 0)
					this.set('ugateway', this.authGatewayStore.first().get('value'));
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	handleLoadSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('GetConfigADErrMsg') + '[' + respData.message + ']');
			return;
		}

		this.setData(respData.data);
	},

	getBaseDNCSV: function() {
		var records = [];
		this.baseDNList.each(function(rec) {
			records.push(rec.data.value);
		});

		var csv = records.join('|&|');
		return csv;
	},

	getPropertiesCSV: function() {
		var records = [];
		this.properties.each(function(rec) {
			records.push(rec.data.name + "=" + rec.data.value);
		});

		var csv = records.join('\n');
		return csv;
	},

	loadBaseDN: function(csv) {
		if (csv == null)
			return;
		var values = csv.split('|&|');
		if (values == '')
			return;
		for (var idx = 0; idx < values.length; idx++) {
			this.baseDNList.add({
				value: values[idx]
			});
		}
	},

	loadProperties: function(csv) {
		if (csv == null)
			return;
		var pair = csv.split('\n');
		if (pair == '')
			return;
		var kv = [];
		for (var idx = 0; idx < pair.length; idx++) {
			kv = pair[idx].split('=');
			this.properties.add({
				name: kv[0],
				value: kv[1]
			});
		}
	},

	saveADConfig: function(exitAfterSave) {
		var baseDNListCSV = this.getBaseDNCSV();
		var propertiesCSV = this.getPropertiesCSV();
		if (this.uport == null || this.uport == '')
			this.uport = 0;
		var data = {
			id: this.id,
			UVersion: this.uversion,
			UIpAddress: this.uipAddress,
			UPort: this.uport,
			USsl: this.ussl,
			groupRequired : this.groupRequired,
			UUidAttribute: this.uuidAttribute,
			UMode: this.umode,
			UBindDn: this.ubindDn,
			UBindPassword: this.ubindPassword,
			UFallback: this.ufallback,
			UDomain: this.udomain,
			UBaseDNList: baseDNListCSV,
			UProperties: propertiesCSV,
			UGateway: this.ugateway,
			UPassWordAttribute: this.upasswordAttribute,
			UCryptType: this.ucryptType,
			UCryptPrefix: this.ucryptPrefix,
			belongsToOrganization: {
				sys_id: this.belongsToOrganization
			}
		}
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/configad/save',
			jsonData: data,
			scope: this,
			success: function(resp) {
				if (exitAfterSave === true)
					clientVM.handleNavigation({
						modelName: 'RS.sysadmin.ConfigADs'
					});
				this.handleSaveSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		})
	},

	handleSaveSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('SaveErrMsg', respData.message))
			return;
		}
		clientVM.displaySuccess(this.localize('SaveSucMsg'));
		this.setData(respData.data);
	},

	setData: function(data) {
		this.loadData(data);
		if (data.belongsToOrganization != null)
			this.set('belongsToOrganization', data.belongsToOrganization.id);
		this.baseDNList.removeAll();
		this.properties.removeAll();
		this.loadBaseDN(data.ubaseDNList);
		this.loadProperties(data.uproperties);
	},

	resetForm: function() {
		this.loadData(this.defaultData);
		this.baseDNList.removeAll();
		this.properties.removeAll();
		this.properties.add([{
			name: 'firstname',
			value: 'givenname'
		}, {
			name: 'lastname',
			value: 'sn'
		}, {
			name: 'title',
			value: 'title'
		}, {
			name: 'email',
			value: 'mail'
		}, {
			name: 'phone',
			value: 'telephonenumber'
		}, {
			name: 'mobile',
			value: 'mobile'
		}]);
		this.set('isPristine', true);
		this.set('udomain', ''); // set after isPristine so udomain field has red exclamation
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ConfigADs'
		});
	},

	save: function(exitAfterSave) {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveADConfig, this, [exitAfterSave])
		this.task.delay(500)
		this.set('state', 'wait')
	},
	saveIsEnabled$: function() {
		return this.state != 'wait' && this.isValid;
	},
	saveAndExit: function() {
		if (!this.saveIsEnabled)
			return;
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	refresh: function() {
		if (this.id) {
			this.loadConfigAD();
		} else {
			this.resetForm();
		}
	},

	addProperties: function() {
		this.properties.add({
			name: 'NewPropertyName',
			value: 'NewPropertyValue'
		});
	},

	deleteProperty: function() {
		this.properties.remove(this.selectedProp);
	},

	addBaseDn: function() {
		this.baseDNList.add({
			value: 'NewBaseDnValue'
		});

	},

	deleteBaseDn: function() {
		this.baseDNList.remove(this.selectedBaseDN);
	},

	saveADConfigIsEnabled$: function() {
		return this.state != 'wait' && this.isValid;
	},

	deletePropertyIsEnabled$: function() {
		return this.selectedProp != null;
	},

	deleteBaseDnIsEnabled$: function() {
		return this.selectedBaseDN != null;
	},

	udomainIsValid$: function() {
		return this.udomain != null &&
			this.udomain != '' ? true : this.localize('invalidDomain');
	}
})
glu.defModel('RS.sysadmin.ConfigADs', {

	mock: false,

	waitResponse: false,
	stateId: 'sysadminconfigad',

	configADStore: {
		mtype: 'store',
		fields: ['udomain', 'uipAddress', 'uuidAttribute', 'umode', 'ubindDn', 'ubaseDNList', 'ugateway', {
			name: 'groupRequired',
			type: 'bool'
		}, {
			name: 'uisDefault',
			type: 'bool'
		}, {
			name: 'ussl',
			type: 'bool'
		}, {
			name: 'ufallback',
			type: 'bool'
		}, {
			name: 'uversion',
			type: 'float'
		}, {
			name: 'uport',
			type: 'float'
		}].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'udomain',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/configad/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	configADSelections: [],

	columns: null,

	displayName: '',

	activate: function() {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.loadRecs();
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)
		this.set('displayName', this.localize('configADsTitle'))
		this.set('columns', [{
			header: '~~domainName~~',
			dataIndex: 'udomain',
			filterable: true,
			flex: 1
		}, {
			header: '~~ipAddress~~',
			dataIndex: 'uipAddress',
			filterable: true,
			width: 150
		}, {
			header: '~~mode~~',
			dataIndex: 'umode',
			filterable: true,
			width: 100
		}, {
			header: '~~baseDNList~~',
			dataIndex: 'ubaseDNList',
			filterable: true,
			flex: 1
		}, {
			header: '~~fallback~~',
			dataIndex: 'ufallback',
			filterable: true,
			renderer: RS.common.grid.plainRenderer(),
			width: 100
		}].concat(RS.common.grid.getSysColumns()))
	},

	loadRecs: function() {
		this.configADStore.load({
			scope: this,
			callback: function(records, op, success) {
				if (!success) {
					//clientVM.displayError(this.localize('getListErrorMsg'));
				}
				this.set('waitResponse', false);
			}
		});
		this.set('waitResponse', true);
	},

	createConfigAD: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ConfigAD',
			params: {
				id: ''
			}
		});
	},

	editConfigAD: function(id) {

		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ConfigAD',
			params: {
				id: id
			}
		})

	},

	deleteConfigAD: function() {

		this.message({
			title: this.localize('deleteConfigAD'),
			msg: this.localize(this.configADSelections.length == 1 ? 'deleteConfigADMessage' : 'deleteConfigADsMessage', {
				len: this.configADSelections.length
			}) + '<br/>',
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteConfigADs
		});
	},

	reallyDeleteConfigADs: function(btn) {
		if (btn != 'yes')
			return;
		var ids = [];
		for (var i = 0; i < this.configADSelections.length; i++) {
			ids.push(this.configADSelections[i].data.id);
		}

		this.set('waitResponse', true);
		this.ajax({
			url: '/resolve/service/configad/delete',
			jsonData: ids,
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success)
					clientVM.displayError(this.localize('disableFailureMsg') + '[' + respData.message + ']');
				else
					clientVM.displaySuccess(this.localize('DeleteSucMsg'));
				this.loadRecs();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('waitResponse', false);
			}
		});
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createConfigADIsEnabled$: function() {
		return !this.waitResponse;
	},

	deleteConfigADIsEnabled$: function() {
		return this.configADSelections.length > 0 && !this.waitResponse;
	},

	updateConfigAD: function() {
		this.configADStore.load({
			scope: this,
			callback: function() {
				this.set('waitResponse', false);
			}
		});
		this.set('waitResponse', true);
	}

})
glu.defModel('RS.sysadmin.ConfigLDAP', {

	title: '',

	state: '',

	//fields 
	fields: ['belongsToOrganization', 'udomain', 'ugateway', 'uipAddress', 'umode',
		'ubindDn', 'ubindPassword', 'uuidAttribute', 'ucryptType', 'ucryptPrefix', 'upasswordAttribute', 'ucryptType', 'ucryptPrefix', 'upasswordAttribute', {
			name: 'ussl',
			type: 'bool'
		}, {
			name: 'ufallback',
			type: 'bool'
		}, {
			name: 'groupRequired',
			type: 'bool'
		}, {
			name: 'uversion',
			type: 'numeric'
		}, {
			name: 'uport',
			type: 'numeric'
		},
		'sys_id',
		'sysCreatedOn',
		'sysCreatedBy',
		'sysUpdatedOn',
		'sysUpdatedBy',
		'sysOrganizationName'
	],

	//local variables same as field names

	sys_id: '',
	sysOrganizationName: '',
	udomain: '',
	ugateway: '',
	uipAddress: '',
	uport: 0,
	umode: '',
	uversion: 0,

	ubindDn: '',
	ubindPassword: '',
	upasswordAttribute: '',
	uuidAttribute: '',
	ucryptType: '',
	ucryptPrefix: '',

	ussl: false,
	ufallback: false,

	belongsToOrganization: '',

	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',

	ubaseDNList: '',
	uproperties: '',

	defaultData: {
		sys_id: '',
		sysOrganizationName: '',
		udomain: '',
		// ugateway: 'LDAP',
		uipAddress: '',
		uport: 389,
		umode: 'BIND',
		uversion: 3,

		ubindDn: '',
		ubindPassword: '',
		upasswordAttribute: 'userPassword',
		uuidAttribute: 'uid',
		ucryptType: 'CLEAR',
		ucryptPrefix: 'DEFAULT',

		ussl: false,
		ufallback: true,
		groupRequired : false,
		belongsToOrganization: '',

		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: ''
	},

	//this is for the drop down
	organizations: {
		mtype: 'store',
		fields: ['sys_id', 'uorganizationName'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/common/organizationList',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	propertiesTitle: '',
	baseDNListTitle: '',

	modeStore: {
		mtype: 'store',
		fields: ['type'],
		data: [{
			type: 'BIND'
		}, {
			type: 'BIND_UID'
		}, {
			type: 'COMPARE'
		}, {
			type: 'COMPARE_LDAP'
		}, {
			type: 'USERNAME'
		}]
	},

	cryptTypeStore: {
		mtype: 'store',
		fields: ['type'],
		data: [{
			type: 'CLEAR'
		}, {
			type: 'MD5'
		}, {
			type: 'CRYPT'
		}]
	},

	properties: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	baseDNList: {
		mtype: 'store',
		fields: ['value'],
		proxy: {
			type: 'memory'
		}
	},

	baseDNColumns: [{
		header: '~~value~~',
		dataIndex: 'value',
		filterable: true,
		flex: 1,
		editor: {
			xtype: 'textfield',
			allowBlank: false
		}
	}],

	propertiesColumns: [{
		header: '~~name~~',
		dataIndex: 'name',
		filterable: true,
		flex: 1,
		editor: {
			xtype: 'textfield',
			allowBlank: false
		}
	}, {
		header: '~~value~~',
		dataIndex: 'value',
		filterable: true,
		flex: 1,
		editor: {
			xtype: 'textfield',
			allowBlank: false
		}
	}],

	selectedBaseDN: null,
	selectedProp: null,

	authGatewayStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		var sys_id = this.sys_id || ''
		this.loadAuthGateway();
		this.resetForm()
		this.set('sys_id', params && params.sys_id != null ? params.sys_id : sys_id);
		this.set('state', 'ready');
		if (this.sys_id != '') {
			this.loadLdaps();
			return;
		}
	},
	//init function
	init: function() {
		this.loadOrg();
		this.set('propertiesTitle', this.localize('propertiesTitle'));
		this.set('baseDNListTitle', this.localize('baseDNListTitle'));
		this.set('title', this.localize('LdapTitle'));
		this.cellEditing = new Ext.grid.plugin.CellEditing({
			clicksToEdit: 1
		});
	},

	//load the list of organizations first as it needs to be pre-selected
	loadOrg: function() {
		this.set('state', 'wait');
		this.organizations.load({
			scope: this,
			callback: function(recs, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('GetOrgErrMsg'));
					return;
				}

				this.set('state', 'ready');
			}
		})
	},

	loadLdaps: function() {
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/configldap/get',
			params: {
				id: this.sys_id
			},
			scope: this,
			success: function(resp) {
				this.handleLoadSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	loadAuthGateway: function() {
		this.ajax({
			url: '/resolve/service/configldap/listAuthGateways',
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success)
					clientVM.displayError(this.localize('LoadAuthGatewayErr') + '[' + respData.message + ']');
				var records = respData.records;
				this.authGatewayStore.removeAll();
				Ext.each(records, function(r) {
					this.authGatewayStore.add({
						name: r,
						value: r
					});
				}, this);
				if (this.authGatewayStore.getCount() > 0)
					this.set('ugateway', this.authGatewayStore.first().get('value'));
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	handleLoadSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('GetErrMsg') + '[' + respData.message + ']');
			return;
		}

		this.setData(respData.data);
	},

	getBaseDNCSV: function() {
		var records = [];
		this.baseDNList.each(function(rec) {
			records.push(rec.data.value);
		});

		var csv = records.join('|&|');
		return csv;
	},

	getPropertiesCSV: function() {
		var records = [];
		this.properties.each(function(rec) {
			records.push(rec.data.name + "=" + rec.data.value);
		});

		var csv = records.join('\n');
		return csv;
	},

	loadBaseDN: function(csv) {
		if (csv == null)
			return;
		var values = csv.split('|&|');
		if (values == '')
			return;
		for (var idx = 0; idx < values.length; idx++) {
			this.baseDNList.add({
				value: values[idx]
			});
		}
	},

	loadProperties: function(csv) {
		if (csv == null)
			return;
		var pair = csv.split('\n');
		if (pair == '')
			return;
		var kv = [];
		for (var idx = 0; idx < pair.length; idx++) {
			kv = (pair[idx]).split('=');
			this.properties.add({
				name: kv[0],
				value: kv[1]
			});
		}
	},

	saveLdap: function(exitAfterSave) {
		var baseDNListCSV = this.getBaseDNCSV();
		var propertiesCSV = this.getPropertiesCSV();
		if (this.uport == null || this.uport == '')
			this.uport = 0;
		var data = {
			id: this.sys_id,
			UVersion: this.uversion,
			UIpAddress: this.uipAddress,
			UPort: this.uport,
			USsl: this.ussl,
			groupRequired : this.groupRequired,
			UUidAttribute: this.uuidAttribute,
			UMode: this.umode,
			UBindDn: this.ubindDn,
			UBindPassword: this.ubindPassword,
			UFallback: this.ufallback,
			UDomain: this.udomain,
			UBaseDNList: baseDNListCSV,
			UProperties: propertiesCSV,
			UGateway: this.ugateway,
			UPasswordAttribute: this.upasswordAttribute,
			UCryptType: this.ucryptType,
			UCryptPrefix: this.ucryptPrefix,
			belongsToOrganization: {
				sys_id: this.belongsToOrganization
			}
		}
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/configldap/save',
			jsonData: data,
			scope: this,
			success: function(resp) {
				if (exitAfterSave === true)
					clientVM.handleNavigation({
						modelName: 'RS.sysadmin.ConfigLDAPs'
					});
				this.handleSaveSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		})
	},

	handleSaveSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('SaveErrMsg', respData.message));
			return;
		}
		this.set('state', 'ready');
		clientVM.displaySuccess(this.localize('SaveSucMsg'));
		this.setData(respData.data);
	},

	setData: function(data) {
		this.loadData(data);
		if (data.belongsToOrganization != null)
			this.set('belongsToOrganization', data.belongsToOrganization.sys_id);
		this.baseDNList.removeAll();
		this.properties.removeAll();
		this.loadBaseDN(data.ubaseDNList);
		this.loadProperties(data.uproperties);
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ConfigLDAPs'
		});

		this.loadData(this.defaultData);
	},


	save: function(exitAfterSave) {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveLdap, this, [exitAfterSave])
		this.task.delay(500)
		this.set('state', 'wait')
	},
	saveIsEnabled$: function() {
		return this.state != 'wait' && this.isValid;
	},
	saveAndExit: function() {
		if (!this.saveIsEnabled)
			return;
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	refresh: function() {
		if (this.id) {
			this.loadLdaps();
		} else {
			this.resetForm();
		}
	},

	selectedProp: null,

	selectedBaseDN: null,

	addProperties: function() {
		this.properties.add({
			name: 'NewPropertyName',
			value: 'NewPropertyValue'
		});
	},

	deleteProperty: function() {
		this.properties.remove(this.selectedProp);
	},

	addBaseDn: function() {
		this.baseDNList.add({
			value: 'NewBaseDnValue'
		});

	},

	deleteBaseDn: function() {
		this.baseDNList.remove(this.selectedBaseDN);
	},


	resetForm: function() {
		this.loadData(this.defaultData);
		this.baseDNList.removeAll();
		this.properties.removeAll();
		this.properties.add([{
			name: 'firstname',
			value: 'givenname'
		}, {
			name: 'lastname',
			value: 'sn'
		}, {
			name: 'title',
			value: 'title'
		}, {
			name: 'email',
			value: 'mail'
		}, {
			name: 'phone',
			value: 'telephonenumber'
		}, {
			name: 'mobile',
			value: 'mobile'
		}]);
		this.set('isPristine', true);
		this.set('udomain', ''); // set after isPristine so udomain field has red exclamation
	},

	saveLdapIsEnabled$: function() {
		return this.state != 'wait' && this.isValid;
	},

	deletePropertyIsEnabled$: function() {
		return this.selectedProp != null;
	},

	deleteBaseDnIsEnabled$: function() {
		return this.selectedBaseDN != null;
	},

	udomainIsValid$: function() {
		return this.udomain != null &&
			this.udomain != '' ? true : this.localize('invalidDomain');
	},

	// ubindDnIsEnabled$:function(){
	// 	return this.umode!='BIND'&&this.umode!='BIND_UID';
	// },
	// ubindPasswordIsEnabled$:function(){
	// 	return this.umode!='BIND'&&this.umode!='BIND_UID';
	// },
	uuidAttributeIsEnabled$: function() {
		return this.umode != 'BIND';
	},
	upasswordAttributeIsEnabled$: function() {
		return this.umode != 'BIND' && this.umode != 'USERNAME';
	},
	ucryptTypeIsEnabled$: function() {
		return this.umode == 'COMPARE';
	},
	ucryptPrefixIsEnabled$: function() {
		return this.umode == 'COMPARE';
	}
})
glu.defModel('RS.sysadmin.ConfigLDAPs', {
	displayName: '',

	columns: null,

	stateId: 'sysadminladapproperties',

	state: '',

	store: {
		mtype: 'store',

		fields: ['id', 'udomain', 'uipAddress', 'uuidAttribute', 'umode', 'ubindDn', 'ubaseDNList', 'ugateway', {
			name: 'uisDefault',
			type: 'bool'
		}, {
			name: 'ussl',
			type: 'bool'
		}, {
			name: 'ufallback',
			type: 'bool'
		}, {
			name: 'groupRequired',
			type: 'bool'
		}, {
			name: 'uversion',
			type: 'float'
		}, {
			name: 'uport',
			type: 'float'
		}].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'udomain',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/configldap/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	recordsSelections: [],

	activate: function() {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.loadRecords();
	},

	init: function() {
		this.set('displayName', this.localize('displayName'));

		this.set('columns', [{
			header: '~~domainName~~',
			dataIndex: 'udomain',
			filterable: true,
			flex: 1
		}, {
			header: '~~ipAddress~~',
			dataIndex: 'uipAddress',
			filterable: true,
			width: 150
		}, {
			header: '~~mode~~',
			dataIndex: 'umode',
			filterable: true,
			width: 100
		}, {
			header: '~~baseDNList~~',
			dataIndex: 'ubaseDNList',
			filterable: true,
			flex: 1
		}, {
			header: '~~fallback~~',
			dataIndex: 'ufallback',
			filterable: true,
			renderer: RS.common.grid.booleanRenderer(),
			width: 100
		}].concat(RS.common.grid.getSysColumns()))
	},

	loadRecords: function() {
		this.set('state', 'wait');
		this.store.load({
			scope: this,
			callback: function(rec, op, suc) {
				/*
				if (!suc) {
					clientVM.displayError(this.localize('ListRecordsErr'))
				}
				*/
				this.set('state', 'ready')
			}
		})
	},

	editRecord: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ConfigLDAP',
			params: {
				sys_id: id
			}
		});
	},

	createRecord: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ConfigLDAP',
			params: {
				sys_id: ''
			}
		});
	},

	deleteRecords: function() {
		this.message({
			title: this.localize('DeleteTitle'),
			msg: this.localize(this.recordsSelections.length > 1 ? 'DeletesMsg' : 'DeleteMsg', {
				num: this.recordsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmDelete'),
				no: this.localize('cancel')
			},

			fn: this.sendDeleteReq
		})
	},

	sendDeleteReq: function(btn) {
		if (btn != 'yes')
			return;

		this.set('state', 'wait');

		var ids = [];

		Ext.each(this.recordsSelections,function(r){
			ids.push(r.get('id'));
		},this);

		this.ajax({
			url: '/resolve/service/configldap/delete',
			jsonData: ids,
			scope: this,
			success: function(resp) {
				this.handleDeleteSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		})
	},

	handleDeleteSucResp: function(resp) {
		var respData = null;

		try {
			respData = RS.common.parsePayload(resp);
		} catch (e) {
			clientVM.displayError(this.localize('invalidJSON'));
		}

		if (!respData.success) {
			clientVM.displayError(this.localize('DeleteErr') + '[' + respData.message + ']');
		} else
			clientVM.displaySuccess(this.localize('DeleteSucMsg'));

		this.loadRecords();
	},

	createRecordIsEnabled$: function() {
		return this.state != 'wait';
	},

	deleteRecordsIsEnabled$: function() {
		return this.state == 'itemSelected';
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	when_selections_chagned: {
		on: ['recordsSelectionsChanged'],
		action: function() {
			if (this.recordsSelections.length > 0)
				this.set('state', 'itemSelected');
			else
				this.set('state', 'ready');
		}
	}
});
glu.defModel('RS.sysadmin.ConfigRADIUS', {

	title: '',

	state: '',

	//fields 
	fields: ['belongsToOrganization', 'udomain', 'ugateway', 'uprimaryHost', 'usecondaryHost', 'uauthProtocol', 'usharedSecret',
		{
			name: 'ufallback',
			type: 'bool'
		}, {
			name: 'uauthPort',
			type: 'numeric'
		}, {
			name: 'uacctPort',
			type: 'numeric'
		},
		'sys_id',
		'sysCreatedOn',
		'sysCreatedBy',
		'sysUpdatedOn',
		'sysUpdatedBy',
		'sysOrganizationName'
	],

	//local variables same as field names

	sys_id: '',
	sysOrganizationName: '',
	udomain: '',
	ugateway: '',
	uprimaryHost: '',
	usecondaryHost: '',
	uacctPort: 0,
	uauthProtocol: '',
	uauthPort: 0,
	usharedSecret: undefined,

	ufallback: false,

	belongsToOrganization: '',

	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
		
	defaultData: {
		sys_id: '',
		sysOrganizationName: '',
		udomain: '',
		// ugateway: 'RADIUS',
		uprimaryHost: '',
		usecondaryHost: '',
		uacctPort: 1813,
		uauthProtocol: 'PAP',
		uauthPort: 1812,

		ufallback: true,

		belongsToOrganization: '',

		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: ''
	},

	//this is for the drop down
	organizations: {
		mtype: 'store',
		fields: ['sys_id', 'uorganizationName'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/common/organizationList',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	propertiesTitle: '',

	authprotocolStore: {
		mtype: 'store',
		fields: ['type'],
		data: [{
			type: 'PAP'
		}, {
			type: 'CHAP'
		}, {
			type: 'MS-CHAP V1'
		}, {
			type: 'MS-CHAP V2'
		}]
	},

	authGatewayStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		var sys_id = this.sys_id || ''
		this.loadAuthGateway();
		this.resetForm();
		this.set('sys_id', params && params.sys_id != null ? params.sys_id : sys_id);
		this.set('state', 'ready');
		if (this.sys_id != '') {
			this.loadRadiuss();
			return;
		} else {
			this.set('usharedSecret', 'resolve');
		}
	},
	//init function
	init: function() {
		this.loadOrg();
		this.set('title', this.localize('RadiusTitle'));
		this.cellEditing = new Ext.grid.plugin.CellEditing({
			clicksToEdit: 1
		});
	},

	//load the list of organizations first as it needs to be pre-selected
	loadOrg: function() {
		this.set('state', 'wait');
		this.organizations.load({
			scope: this,
			callback: function(recs, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('GetOrgErrMsg'));
					return;
				}

				this.set('state', 'ready');
			}
		})
	},

	loadRadiuss: function() {
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/configradius/get',
			params: {
				id: this.sys_id
			},
			scope: this,
			success: function(resp) {
				this.handleLoadSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	loadAuthGateway: function() {
		this.ajax({
			url: '/resolve/service/configradius/listAuthGateways',
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success)
					clientVM.displayError(this.localize('LoadAuthGatewayErr') + '[' + respData.message + ']');
				var records = respData.records;
				this.authGatewayStore.removeAll();
				Ext.each(records, function(r) {
					this.authGatewayStore.add({
						name: r,
						value: r
					});
				}, this);
				if (this.authGatewayStore.getCount() > 0)
					this.set('ugateway', this.authGatewayStore.first().get('value'));
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	handleLoadSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('GetErrMsg') + '[' + respData.message + ']');
			return;
		}

		this.setData(respData.data);
	},

	saveRadius: function(exitAfterSave) {
		if (this.uauthPort == null || this.uathPort == '')
			this.uauthPort = 1812;
		if (this.uacctPort == null || this.uacctPort == '')
			this.uacctPort = 1813;
		var data = {
			id: this.sys_id,
			UAuthPort: this.uauthPort,
			UPrimaryHost: this.uprimaryHost,
			USecondaryHost: this.usecondaryHost,
			UAcctPort: this.uacctPort,
			UAuthProtocol: this.uauthProtocol,
			UFallback: this.ufallback,
			UDomain: this.udomain,
			UGateway: this.ugateway,
			USharedSecret: this.usharedSecret,
			belongsToOrganization: {
				sys_id: this.belongsToOrganization
			}
		}
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/configradius/save',
			jsonData: data,
			scope: this,
			success: function(resp) {
				if (exitAfterSave === true)
					clientVM.handleNavigation({
						modelName: 'RS.sysadmin.ConfigRADIUSs'
					});
				this.handleSaveSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		})
	},

	handleSaveSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('SaveErrMsg', respData.message));
			return;
		}
		this.set('state', 'ready');
		clientVM.displaySuccess(this.localize('SaveSucMsg'));
		this.setData(respData.data);
	},

	setData: function(data) {
		this.loadData(data);
		if (data.belongsToOrganization != null)
			this.set('belongsToOrganization', data.belongsToOrganization.sys_id);
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ConfigRADIUSs'
		});

		this.loadData(this.defaultData);
	},


	save: function(exitAfterSave) {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveRadius, this, [exitAfterSave])
		this.task.delay(500)
		this.set('state', 'wait')
	},
	saveIsEnabled$: function() {
		return this.state != 'wait' && this.isValid;
	},
	saveAndExit: function() {
		if (!this.saveIsEnabled)
			return;
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	refresh: function() {
		if (this.id) {
			this.loadRadiuss();
		} else {
			this.resetForm();
		}
	},

	resetForm: function() {
		this.loadData(this.defaultData);
		this.set('isPristine', true);
		this.set('udomain', ''); // set after isPristine so udomain field has red exclamation
		this.set('uprimaryHost', '');
	},

	saveRadiusIsEnabled$: function() {
		return this.state != 'wait' && this.isValid;
	},

	udomainIsValid$: function() {
		return this.udomain != null &&
			this.udomain != '' ? true : this.localize('invalidDomain');
	},
	
	uprimaryHostIsValid$: function() {
		return this.uprimaryHost != null &&
			this.uprimaryHost != '' ? true : this.localize('invalidPrimaryHost');
	},
	
	usharedSecretIsValid$: function() {
		return this.usharedSecret != null &&
			this.usharedSecret != '' ? true : this.localize('invalidSharedSecret');
	}
})

glu.defModel('RS.sysadmin.ConfigRADIUSs', {
	displayName: '',

	columns: null,

	stateId: 'sysadminradiusproperties',

	state: '',

	store: {
		mtype: 'store',

		fields: ['id', 'udomain', 'uprimaryHost', 'usecondaryHost', 'uauthProtocol', 'ugateway', 'usharedSecret', {
			name: 'uisDefault',
			type: 'bool'
		}, {
			name: 'ufallback',
			type: 'bool'
		}, {
			name: 'uauthPort',
			type: 'numeric'
		}, {
			name: 'uacctPort',
			type: 'numeric'
		}].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'udomain',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/configradius/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	recordsSelections: [],

	activate: function() {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.loadRecords();
	},

	init: function() {
		this.set('displayName', this.localize('displayName'));

		this.set('columns', [{
			header: '~~domainName~~',
			dataIndex: 'udomain',
			filterable: true,
			flex: 1
		}, {
			header: '~~uprimaryHost~~',
			dataIndex: 'uprimaryHost',
			filterable: true,
			width: 150
		}, {
			header: '~~usecondaryHost~~',
			dataIndex: 'usecondaryHost',
			filterable: true,
			width: 150
		}, {
			header: '~~uauthProtocol~~',
			dataIndex: 'uauthProtocol',
			filterable: true,
			width: 200
		}, {
			header: '~~fallback~~',
			dataIndex: 'ufallback',
			filterable: true,
			renderer: RS.common.grid.booleanRenderer(),
			width: 100
		}, {
			header: '~~usharedSecret~~',
			dataIndex: 'usharedSecret',
			filterable: true,
			width: 180
		}].concat(RS.common.grid.getSysColumns()))
	},

	loadRecords: function() {
		this.set('state', 'wait');
		this.store.load({
			scope: this,
			callback: function(rec, op, suc) {
				/*
				if (!suc) {
					clientVM.displayError(this.localize('ListRecordsErr'))
				}
				*/
				this.set('state', 'ready')
			}
		})
	},

	editRecord: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ConfigRADIUS',
			params: {
				sys_id: id
			}
		});
	},

	createRecord: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ConfigRADIUS',
			params: {
				sys_id: ''
			}
		});
	},

	deleteRecords: function() {
		this.message({
			title: this.localize('DeleteTitle'),
			msg: this.localize(this.recordsSelections.length > 1 ? 'DeletesMsg' : 'DeleteMsg', {
				num: this.recordsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmDelete'),
				no: this.localize('cancel')
			},

			fn: this.sendDeleteReq
		})
	},

	sendDeleteReq: function(btn) {
		if (btn != 'yes')
			return;

		this.set('state', 'wait');

		var ids = [];

		Ext.each(this.recordsSelections,function(r){
			ids.push(r.get('id'));
		},this);

		this.ajax({
			url: '/resolve/service/configradius/delete',
			jsonData: ids,
			scope: this,
			success: function(resp) {
				this.handleDeleteSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		})
	},

	handleDeleteSucResp: function(resp) {
		var respData = null;

		try {
			respData = RS.common.parsePayload(resp);
		} catch (e) {
			clientVM.displayError(this.localize('invalidJSON'));
		}

		if (!respData.success) {
			clientVM.displayError(this.localize('DeleteErr') + '[' + respData.message + ']');
		} else
			clientVM.displaySuccess(this.localize('DeleteSucMsg'));

		this.loadRecords();
	},

	createRecordIsEnabled$: function() {
		return this.state != 'wait';
	},

	deleteRecordsIsEnabled$: function() {
		return this.state == 'itemSelected';
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	when_selections_chagned: {
		on: ['recordsSelectionsChanged'],
		action: function() {
			if (this.recordsSelections.length > 0)
				this.set('state', 'itemSelected');
			else
				this.set('state', 'ready');
		}
	}
});
glu.defModel('RS.sysadmin.ControllerPicker', {

	mock: false,

	controllers: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['ucontrollerName'],
		fields: ['id', 'ucontrollerName'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/common/controllers/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	controllersSelections: [],
	columns: [],

	init: function() {
		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'ucontrollerName',
			filterable: true,
			sortable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))
		this.controllers.load()
	},

	cancel: function() {
		this.doClose()
	},

	add: function() {
		Ext.Array.forEach(this.controllersSelections, function(r) {
			if (this.parentVM.controllers.indexOf(r) == -1)
				this.parentVM.controllers.add(r.data)
		}, this)
		this.doClose()
	},
	addIsEnabled$: function() {
		return this.controllersSelections.length > 0
	}
})
glu.defModel('RS.sysadmin.JobSchedule', {

	mock: false,

	jobScheduleTitle$: function() {
		return this.localize('jobSchedule') + (this.name ? ' - ' + this.name : '')
	},

	activate: function(screen, params) {
		this.resetForm()
		this.set('id', params ? params.id : '')
		this.loadJobSchedule()
	},

	resetForm: function() {
		this.loadData(this.defaultData)
		this.set('dayType', this.defaultData.dayType)
		this.parametersStore.removeAll()
	},

	defaultData: {
		uname: '',
		umodule: '',
		urunbook: '',
		uactive: true,
		uexpression: '',
		seconds: '*',
		minutes: '*',
		hours: '*',
		month: '*',
		dayType: false,
		dayOfMonth: '*',
		dayOfWeek: '*',
		uparams: '',

		ustartTime: '',
		uendTime: ''
	},

	fields: ['id', 'uname', 'umodule', 'urunbook', 'uactive', 'uexpression', 'seconds', 'minutes', 'hours', 'month', 'dayOfMonth', 'dayOfWeek', 'uparams', 'ustartTime', 'uendTime'].concat(RS.common.grid.getSysFields()),
	id: '',
	uname: '',
	umodule: '',
	urunbook: '',
	uactive: true,
	uexpression: '',
	seconds: '',
	secondsIsValid$: function() {
		return this.seconds ? true : this.localize('secondsInvalid')
	},
	minutes: '',
	minutesIsValid$: function() {
		return this.minutes ? true : this.localize('minutesInvalid')
	},
	hours: '',
	hoursIsValid$: function() {
		return this.hours ? true : this.localize('hoursInvalid')
	},
	month: '',
	monthIsValid$: function() {
		return this.month ? true : this.localize('monthInvalid')
	},
	dayType: false,
	dayOfMonth: '*',
	dayOfMonthIsVisible$: function() {
		return this.dayOfMonthRadio
	},
	dayOfWeek: '*',
	dayOfWeekIsVisible$: function() {
		return this.dayOfWeekRadio
	},
	dayOfWeekRadio: false,
	dayOfMonthRadio: true,
	when_radios_change_update_dayType: {
		on: ['dayOfWeekRadioChanged', 'dayOfMonthRadioChanged'],
		action: function() {
			if (this.dayOfWeekRadio) this.set('dayType', true)
			else this.set('dayType', false)
		}
	},
	when_dayType_change_update_radios: {
		on: ['dayTypeChanged'],
		action: function() {
			if (this.dayType) {
				this.set('dayOfMonthRadio', false)
				this.set('dayOfWeekRadio', true)
			} else {
				this.set('dayOfMonthRadio', true)
				this.set('dayOfWeekRadio', false)
			}
		}
	},
	uparams: '',

	ustartTime: new Date(),
	uendTime: new Date(),

	sysCreatedOn: '',
	sysCreatedOnDisplay$: function() {
		return Ext.isDate(this.sysCreatedOn) ? Ext.Date.format(this.sysCreatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysUpdatedOn: '',
	sysUpdatedOnDisplay$: function() {
		return Ext.isDate(this.sysUpdatedOn) ? Ext.Date.format(this.sysUpdatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysCreatedBy: '',
	sysUpdatedBy: '',

	sysOrganizationName: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	unameIsValid$: function() {
		if (!this.uname) return this.localize('nameInvalid')
		return true
	},

	unameIsVisible$: function() {
		return !this.id
	},

	unameDisplay$: function() {
		return this.uname
	},
	unameDisplayIsVisible$: function() {
		return this.id
	},

	parametersStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	parametersSelections: [],

	parametersColumns: [{
		dataIndex: 'name',
		header: '~~name~~',
		flex: 1,
		editor: {}
	}, {
		dataIndex: 'value',
		header: '~~value~~',
		flex: 1,
		editor: {}
	}],

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)
	},

	loadJobSchedule: function() {
		if (this.id)
			this.ajax({
				url: '/resolve/service/jobscheduler/get',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.loadData(response.data)
						this.set('uactive', response.data.uactive)
						this.set('ustartTime', response.data.ustartTime ? Ext.Date.parse(response.data.ustartTime, 'time') : '')
						this.set('uendTime', response.data.uendTime ? Ext.Date.parse(response.data.uendTime, 'time') : '')
						var params = Ext.Object.fromQueryString(this.uparams)
						Ext.Object.each(params, function(key) {
							this.parametersStore.add({
								name: key,
								value: params[key]
							})
						}, this)

						var expressions = Ext.String.trim(this.uexpression).split(' ')
						if (expressions.length == 6) {
							this.set('seconds', expressions[0])
							this.set('minutes', expressions[1])
							this.set('hours', expressions[2])
							this.set('dayOfMonth', expressions[3])
							this.set('month', expressions[4])
							this.set('dayOfWeek', expressions[5])

							if (this.dayOfMonth == '?') {
								this.set('dayType', true)
								this.set('dayOfMonth', '*')
							}
							if (this.dayOfWeek == '?') {
								this.set('dayType', false)
								this.set('dayOfWeek', '*')
							}
						}

					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
	},

	refresh: function() {
		this.parametersStore.removeAll();
		this.loadJobSchedule();
	},

	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.persistJobSchedule, this, [exitAfterSave])
		this.task.delay(500)
		this.set('persisting', true)
	},
	saveIsEnabled$: function() {
		return !this.persisting && this.isValid
	},
	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	persisting: false,
	persistJobSchedule: function(exitAfterSave) {
		var params = {}
		this.parametersStore.each(function(record) {
			if (record.get('name'))
				params[record.get('name')] = record.get('value')
		})
		this.set('uparams', Ext.Object.toQueryString(params))

		//calculate expression
		var expression = []
		expression.push(this.seconds)
		expression.push(this.minutes)
		expression.push(this.hours)
		expression.push(this.dayType ? '?' : this.dayOfMonth)
		expression.push(this.month)
		expression.push(this.dayType ? this.dayOfWeek : '?')
		this.set('uexpression', expression.join(' '))

		var data = {
			id: this.id,
			uname: this.uname,
			uactive: this.uactive,
			umodule: this.umodule,
			urunbook: this.urunbook,
			active: this.active,
			uexpression: this.uexpression,
			ustartTime: this.ustartTime,
			uendTime: this.uendTime,
			uparams: this.uparams
		}
		this.ajax({
			url: '/resolve/service/jobscheduler/save',
			jsonData: data,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize('JobScheduleSaved'))
					if (exitAfterSave === true) clientVM.handleNavigation({
						modelName: 'RS.sysadmin.JobSchedules'
					})
					else {
						this.set('id', response.data.id)
						//this.loadData(response.data)
					}
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('persisting', false);
			}
		})
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.JobSchedules'
		})
	},

	addParam: function() {
		var max = 0;
		var reg = /^NewParam(\d*)$/;
		var newParam = 'NewParam';

		// determine a new unique "NewParam" name
		this.parametersStore.each(function(param) {
			if (!reg.test(param.get('name')))
				return;
			var m = reg.exec(param.get('name'));
			var idx = parseInt(m[1]);
			if (idx > max)
				max = idx;
		}, this);
		newParam += ((max + 1) || '');

		// add "NewParam" to Parameters list
		this.parametersStore.add({
			name: newParam,
			value: ''
		});

		// clear out all previously selected items
		this.parametersStore.grid.getSelectionModel().deselectAll();

		// determine row of newly added "NewParam"
		var row;
		for (row = this.parametersStore.data.items.length-1; row > 0; row--) {
			if (this.parametersStore.data.items[row].data.name == newParam) {
				break;
			}
		}
		// start editing the newly selected item
		this.parametersStore.grid.getPlugin().startEditByPosition({
			row: row,
			column: 0
		});
	},
	removeParam: function() {
		Ext.Array.forEach(this.parametersSelections, function(selection) {
			this.parametersStore.remove(selection)
		}, this)
	},
	removeParamIsEnabled$: function() {
		return this.parametersSelections.length > 0
	},

	executeJobSchedule: function() {
		this.message({
			title: this.localize('executeTitle'),
			msg: this.localize('executeMessage'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('executeAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyExecuteRecords
		})
	},
	executeJobScheduleIsEnabled$: function() {
		return this.id
	},
	reallyExecuteRecords: function(btn) {
		if (btn == 'yes') {
			this.ajax({
				url: '/resolve/service/jobscheduler/execute',
				params: {
					ids: [this.id]
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						clientVM.displaySuccess(this.localize('executeSuccess'))
					} else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
	}
})
glu.defModel('RS.sysadmin.JobSchedules', {

	mock: false,

	activate: function() {
		this.jobSchedules.load()
		clientVM.setWindowTitle(this.localize('jobSchedulesDisplayName'))
	},

	columns: [],
	jobSchedulesSelections: [],
	allSelected: false,

	displayName: '',
	stateId: 'JobSchedules',

	jobSchedules: {
		mtype: 'store',
		fields: ['id', 'uname', 'urunbook', 'uexpression', 'umodule', {
			name: 'uactive',
			type: 'bool'
		}].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/jobscheduler/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)

		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'uname',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~active~~',
			dataIndex: 'uactive',
			filterable: true,
			sortable: true,
			align: 'center',
			width: 60,
			renderer: RS.common.grid.booleanRenderer()
		}, {
			header: '~~runbook~~',
			dataIndex: 'urunbook',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~expression~~',
			dataIndex: 'uexpression',
			filterable: true,
			sortable: true
		}, {
			header: '~~module~~',
			dataIndex: 'umodule',
			filterable: true,
			sortable: true
		}].concat(RS.common.grid.getSysColumns()))

		this.set('displayName', this.localize('jobSchedulesDisplayName'))
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createJobSchedule: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.JobSchedule'
		})
	},
	editJobSchedule: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.JobSchedule',
			params: {
				id: id
			}
		})
	},
	deleteJobScheduleIsEnabled$: function() {
		return this.jobSchedulesSelections.length > 0
	},
	deleteJobSchedule: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.jobSchedulesSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.jobSchedulesSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/jobscheduler/delete',
					params: {
						ids: '',
						whereClause: this.jobSchedules.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('jobSchedulesSelections', [])
							this.jobSchedules.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.jobSchedulesSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/jobscheduler/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('jobSchedulesSelections', [])
							this.jobSchedules.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	},

	activateJobScheduleIsEnabled$: function() {
		var deactivated = false;
		Ext.Array.each(this.jobSchedulesSelections, function(selection) {
			if (!selection.get('uactive')) {
				deactivated = true
				return false
			}
		})
		return this.jobSchedulesSelections.length > 0 && deactivated
	},
	activateJobSchedule: function() {
		this.message({
			title: this.localize('activateTitle'),
			msg: this.localize(this.jobSchedulesSelections.length == 1 ? 'activateMessage' : 'activatesMessage', this.jobSchedulesSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('activateAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyActivateRecords
		})
	},
	reallyActivateRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/jobscheduler/activateDeactivate',
					params: {
						ids: '',
						whereClause: this.jobSchedules.lastWhereClause || '1=1',
						activateJob: true
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('jobSchedulesSelections', [])
							this.jobSchedules.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.jobSchedulesSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/jobscheduler/activateDeactivate',
					params: {
						ids: ids.join(','),
						activateJob: true
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('jobSchedulesSelections', [])
							this.jobSchedules.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	},

	deactivateJobScheduleIsEnabled$: function() {
		var activated = false;
		Ext.Array.each(this.jobSchedulesSelections, function(selection) {
			if (selection.get('uactive')) {
				activated = true
				return false
			}
		})
		return this.jobSchedulesSelections.length > 0 && activated
	},
	deactivateJobSchedule: function() {
		this.message({
			title: this.localize('deactivateTitle'),
			msg: this.localize(this.jobSchedulesSelections.length == 1 ? 'deactivateMessage' : 'deactivatesMessage', this.jobSchedulesSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deactivateAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeactivateRecords
		})
	},
	reallyDeactivateRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/jobscheduler/activateDeactivate',
					params: {
						ids: '',
						whereClause: this.jobSchedules.lastWhereClause || '1=1',
						activateJob: false
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('jobSchedulesSelections', [])
							this.jobSchedules.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.jobSchedulesSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/jobscheduler/activateDeactivate',
					params: {
						ids: ids.join(','),
						activateJob: false
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('jobSchedulesSelections', [])
							this.jobSchedules.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	},

	executeJobScheduleIsEnabled$: function() {
		return this.jobSchedulesSelections.length > 0
	},
	executeJobSchedule: function() {
		this.message({
			title: this.localize('executeTitle'),
			msg: this.localize(this.jobSchedulesSelections.length == 1 ? 'executeMessage' : 'executesMessage', this.jobSchedulesSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('executeAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyExecuteRecords
		})
	},
	reallyExecuteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/jobscheduler/execute',
					params: {
						ids: '',
						whereClause: this.jobSchedules.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							clientVM.displaySuccess(this.jobSchedulesSelections.length === 1 ? this.localize('executeSuccess') : this.localize('executesSuccess'))
							this.set('jobSchedulesSelections', [])
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.jobSchedulesSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/jobscheduler/execute',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							clientVM.displaySuccess(this.jobSchedulesSelections.length === 1 ? this.localize('executeSuccess') : this.localize('executesSuccess'))
							this.set('jobSchedulesSelections', [])
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	}
})
glu.defModel('RS.sysadmin.ListRegistration', {

	mock: false,

	title: '',

	stateId: 'listRegistrationProperties',
	displayName: 'Properties',
	columns: [{
		header: '~~uname~~',
		dataIndex: 'uname',
		filterable: true,
		sortable: true,
		flex: 1
	}, {
		header: '~~utype~~',
		dataIndex: 'utype',
		filterable: true,
		sortable: true,
		flex: 1
	}, {
		header: '~~uvalue~~',
		dataIndex: 'uvalue',
		filterable: true,
		sortable: true,
		flex: 1
	}],

	properties: {
		mtype: 'store',
		fields: ['uname', 'utype', 'uvalue'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		},
		data: [{
			uname: 'test'
		}]
	},

	listRegistrationTitle$: function() {
		return this.localize('listRegistration') + (this.name ? ' - ' + this.name : '')
	},

	activate: function(screen, params) {
		this.resetForm()
		this.set('id', params ? params.id : '')
		this.loadListRegistration()
		clientVM.setWindowTitle(this.localize('title'))
	},

	resetForm: function() {
		this.loadData(this.defaultData)
	},

	fields: [
		'id',
		'uname',
		'uguid',
		'utype',
		'ustatus',
		'uipaddress',
		'uconfig',
		'uversion',
		'sys_id',
		'sysCreatedOn',
		'sysCreatedBy',
		'sysUpdatedOn',
		'sysUpdatedBy',
		'sysOrganizationName'
	],

	id: '',
	uname: '',
	uguid: '',
	utype: '',
	ustatus: '',
	uipaddress: '',
	uconfig: '',
	uversion: '',
	sys_id: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrganizationName: '',


	defaultData: {
		uname: '',
		uguid: '',
		utype: '',
		ustatus: '',
		uipaddress: '',
		uconfig: '',
		uversion: '',
		sys_id: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrganizationName: ''
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true);
		this.set('title', this.localize('title'));
	},

	loadListRegistration: function() {
		if (this.id) {
			// this.properties.removeAll()
			this.ajax({
				url: '/resolve/service/registration/get',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.loadData(response.data)
						// this.properties.add(response.data.resolveRegistrationProperties)
						Ext.Array.forEach(response.data.resolveRegistrationProperties, function(property) {
							this.properties.add(property)
						}, this)
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},

	refresh: function() {
		this.loadListRegistration()
	},

	saveListRegistration: function(exitAfterSave) {
		this.ajax({
			url: '/resolve/service/registration/save',
			jsonData: this.asObject(),
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize('ListRegistrationSaved'))
					if (exitAfterSave === true) clientVM.handleNavigation({
						modelName: 'RS.sysadmin.ListRegistrations'
					})
					else {
						this.set('id', response.data.id)
					}
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('persisting', false);
			}
		})
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ListRegistrations'
		})
	}
})
glu.defModel('RS.sysadmin.ListRegistrations', {

	mock: false,

	activate: function() {
		this.listRegistrations.load()
		clientVM.setWindowTitle(this.localize('listRegistrationsDisplayName'))
	},

	columns: [],
	listRegistrationsSelections: [],
	allSelected: false,

	displayName: '',
	stateId: 'ListRegistrations',

	listRegistrations: {
		mtype: 'store',
		fields: ['id', 'uname', 'ustatus', 'uguid', 'utype', 'uversion', 'uipaddress'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/registration/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)

		var columns = [{
			header: '~~ustatus~~',
			dataIndex: 'ustatus',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.statusRenderer('rs-listregistration-status')
		}, {
			header: '~~uname~~',
			dataIndex: 'uname',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~uguid~~',
			dataIndex: 'uguid',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~utype~~',
			dataIndex: 'utype',
			filterable: true,
			sortable: true
		}, {
			header: '~~uipaddress~~',
			dataIndex: 'uipaddress',
			filterable: true,
			sortable: true,
			width: 200
		}, {
			header: '~~uversion~~',
			dataIndex: 'uversion',
			filterable: true,
			sortable: true
		}].concat(RS.common.grid.getSysColumns());

		var col, index = -1;
		for (var i = 0; i < columns.length; i++) {
			col = columns[i];
			if (col.dataIndex == 'sysUpdatedOn') {
				col.initialShow = true
				col.hidden = false
				index = i
			}
		}
		//move updatedOn column to be the first column in the grid
		if (index > -1) columns.splice(0, 0, columns.splice(index, 1)[0])
		this.set('columns', columns)

		this.set('displayName', this.localize('listRegistrationsDisplayName'))
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	editListRegistration: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ListRegistration',
			params: {
				id: id
			}
		})
	},
	deleteListRegistrationIsEnabled$: function() {
		return this.listRegistrationsSelections.length > 0
	},

	editListRegistration: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ListRegistration',
			params: {
				id: id
			}
		})
	},

	deleteListRegistration: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.listRegistrationsSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.listRegistrationsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/registration/delete',
					params: {
						ids: '',
						whereClause: this.listRegistrations.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('listRegistrationsSelections', [])
							this.listRegistrations.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.listRegistrationsSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/registration/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('listRegistrationsSelections', [])
							this.listRegistrations.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	}
})
glu.defModel('RS.sysadmin.MenuDefinition', {
	mock: false,
	fields: [
		'id',
		'title', {
			name: 'active',
			type: 'bool'
		},
		'color',
		'textColor'
	].concat(RS.common.grid.getSysFields()),
	menuDefinitionTitle$: function() {
		return this.localize('menuDefTitle') + (this.title ? ' - ' + this.title : '')
	},
	activate: function(screen, params) {
		this.set('persisting', false);
		this.resetForm()
		this.set('id', params ? params.id : '')
		this.loadMenuDefinition()
	},
	resetForm: function() {
		this.loadData(this.defaultData)
		this.roles.removeAll()
		this.menuItems.setRootNode({
			children: []
		});
	},	
	bgStyle$: function() {
		var demo = 'border:1px solid #000000;background-color:#' + (this.color || '0088CC');
		return '<div style="height:15px;width:15px;' + demo + '"></div>'
	},
	selectBgColor: function(color) {
		this.set('color', color);
	},
	textColor: 'FFFFFF',
	txtStyle$: function() {
		var demo = 'border:1px solid #000000;background-color:#' + (this.textColor || 'FFFFFF');
		return '<div style="height:15px;width:15px;' + demo + '"></div>'
	},
	selectTxtColor: function(color) {
		this.set('textColor', color)
	},
	active: false,
	sysCreatedOn: '',
	sysCreatedOnDisplay$: function() {
		return Ext.isDate(this.sysCreatedOn) ? Ext.Date.format(this.sysCreatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysUpdatedOn: '',
	sysUpdatedOnDisplay$: function() {
		return Ext.isDate(this.sysUpdatedOn) ? Ext.Date.format(this.sysUpdatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysCreatedBy: '',
	sysUpdatedBy: '',

	sysOrganizationName: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	defaultData: {
		title: '',
		active: true,
		sys_id: '',
		color: '00CCFF',
		textColor: 'FFFFFF',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrganizationName: ''
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	titleIsValid$: function() {
		if (!this.title) return this.localize('titleInvalid')
		return true
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)
		this.set('rolesColumns', [{
			header: '~~name~~',
			dataIndex: 'name',
			filterable: true,
			sortable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))

		this.set('menuItemsColumns', [{
			xtype: 'treecolumn',
			header: '~~name~~',
			dataIndex: 'title',
			filterable: false,
			sortable: false,
			flex: 1,
			editor: {
				xtype: 'textfield',
				allowBlank: false
			}
		}, {
			header: '~~active~~',
			dataIndex: 'active',
			filterable: false,
			sortable: false,
			width: 60,
			align: 'center',
			renderer: RS.common.grid.booleanRenderer()
		}, {
			header: '~~query~~',
			dataIndex: 'query',
			filterable: false,
			sortable: false,
			flex: 1,
			editor: {}
		}, {
			header: '~~roles~~',
			dataIndex: 'rolesString',
			filterable: false,
			sortable: false,
			flex: 1
		}])
	},

	loadMenuDefinition: function() {
		if (this.id) {
			this.roles.removeAll()
			this.ajax({
				url: '/resolve/service/menudefinition/get',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.loadData(response.data)
						var groups = {},
							rootChildren = [];
						Ext.each(response.data.sysAppModules, function(r) {
							r.leaf = true;
							r.group = r.appModuleGroup;
							if (r.group && !groups[r.group])
								groups[r.group] = {
									id: r.group,
									isGroup: true,
									title: r.group,
									group: r.group,
									expanded: true,
									children: []
								}
							r.rolesString = (r.roles || []).join(', ')
							if (r.group)
								groups[r.group].children.push(r)
							else rootChildren.push(r)
						})

						Ext.Object.each(groups, function(group) {
							rootChildren.push(groups[group])
						})

						this.menuItems.setRootNode({
							children: rootChildren
						})

						this.menuItems.getRootNode().expand()

						var responseRoles = [];
						Ext.Array.forEach(response.data.roles || [], function(role) {
							responseRoles.push({
								name: role
							})
						})
						this.roles.add(responseRoles)
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},

	refresh: function() {
		this.loadMenuDefinition()
	},

	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.persistMenuDefinition, this, [exitAfterSave])
		this.task.delay(500)
		this.set('persisting', true)
	},
	saveIsEnabled$: function() {
		return !this.persisting && this.isValid
	},
	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	persisting: false,
	persistMenuDefinition: function(exitAfterSave) {
		var roles = [],
			menuItems = [],
			organizations = [],
			items = [],
			index = 0;

		this.roles.each(function(role) {
			roles.push(role.get('name'))
		})

		this.menuItems.getRootNode().eachChild(function(group) {
			if (group.data.isGroup && group.childNodes && group.childNodes.length > 0)
				group.eachChild(function(item) {
					item.set('order', index)
					item.set('group', group.get('title'))
					index++
					items.push(this.convertNode(item.data))
				}, this)
			else {
				if (!group.data.isGroup) {
					group.set('order', index)
					index++
					items.push(this.convertNode(group.data))
				}
			}
		}, this)

		this.ajax({
			url: '/resolve/service/menudefinition/save',
			jsonData: {
				id: this.id,
				title: this.title,
				color: this.color == '0088CC' ? '' : this.color,
				textColor: this.textColor == 'FFFFFF' ? '' : this.textColor,
				active: this.active,
				roles: roles,
				sysAppModules: items
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.getUser()
					clientVM.displaySuccess(this.localize('MenuDefinitionSaved'))
					if (exitAfterSave === true) clientVM.handleNavigation({
						modelName: 'RS.sysadmin.MenuDefinitions'
					})
					else {
						this.set('id', response.data.id)
						RS.common.grid.updateSysFields(this, response.data)
					}
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('persisting', false);
			}
		})
	},
	convertNode: function(node) {
		return {
			id: node.id && node.id.indexOf('ext-gen') == -1 ? node.id : '',
			title: node.title,
			roles: node.roles,
			query: node.query,
			active: node.active,
			appModuleGroup: node.group
		}
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.MenuDefinitions'
		})
	},

	roles: {
		mtype: 'store',
		fields: ['name'],
		proxy: {
			type: 'memory'
		}
	},
	rolesSelections: [],
	rolesColumns: [],

	addRole: function() {
		this.open({
			mtype: 'RS.sysadmin.RolePicker',
			callback: this.addNewRole
		})
	},
	addNewRole: function(newRoles) {
		Ext.Array.forEach(newRoles, function(role) {
			if (this.roles.findExact('name', role.get('uname')) == -1) this.roles.add({
				name: role.get('uname')
			})
		}, this)
	},
	removeRole: function() {
		Ext.Array.forEach(this.rolesSelections, function(r) {
			this.roles.remove(r)
		}, this)
	},
	removeRoleIsEnabled$: function() {
		return this.rolesSelections.length > 0
	},

	menuItems: {
		mtype: 'treestore',
		fields: ['id', 'title', 'query', 'group', 'rolesString', 'roles', {
			name: 'active',
			type: 'bool'
		}, {
			name: 'isGroup',
			type: 'bool'
		}],
		proxy: {
			type: 'memory'
		}
	},
	menuItemsSelections: [],
	menuItemsColumns: [],

	addMenuItem: function() {
		this.open({
			mtype: 'RS.sysadmin.MenuItem'
		})
	},
	addGroup: function() {
		this.menuItems.getRootNode().appendChild({
			title: this.localize('newGroup'),
			isGroup: 'true',
			expanded: true
		})
	},
	editMenuItem: function(id) {
		var menuDef = this.menuItems.getNodeById(id) || {},
			editModel = this.open({
				mtype: 'RS.sysadmin.MenuItem',
				editMode: true
			});

		if (menuDef) {
			editModel.set('originalItem', menuDef)
			editModel.loadData(menuDef.data)
			editModel.roles.removeAll()
			var newRoles = []
			Ext.Array.forEach(menuDef.get('roles') || [], function(newRole) {
				newRoles.push({
					name: newRole
				})
			})
			editModel.roles.add(newRoles)
		}
	},
	checkDrop: function(data, overModel, dropPosition, dropHandlers) {
		var group = overModel.parentNode.get('group'),
			records = data.records;

		var siblings = overModel.parentNode.childNodes;
		if (dropPosition === 'append') {
			group = overModel.get('group');
			siblings = overModel.childNodes;
		}
		for (var idx = 0; idx < records.length; idx++) {
			var r = records[idx];
			if (!r.isLeaf() && dropPosition === 'append') {
				dropHandlers.cancelDrop();
				return;
			}
			// for (sidx in siblings) {
			// 	if (siblings[sidx].get('title') == r.get('title')) {
			// 		dropHandlers.cancelDrop();
			// 		return;
			// 	}
			// }
		}

		Ext.each(records, function(r) {
			if (r.get('title'))
				r.set('group', group);
		});
	},
	removeMenuItem: function() {
		var root = this.menuItems.getRootNode(),
			l1 = root.childNodes;

		Ext.Array.forEach(this.menuItemsSelections, function(c) {
			Ext.each(l1, function(node) {
				if (node.childNodes) {
					node.removeChild(c)
				}
			}, this)
			root.removeChild(c)
		}, this)
	},
	removeMenuItemIsEnabled$: function() {
		return this.menuItemsSelections.length > 0
	}
})
glu.defModel('RS.sysadmin.MenuDefinitions', {

	mock: false,

	wait: false,

	activate: function() {
		this.set('wait', false);
		this.menuDefinitions.load()
		clientVM.setWindowTitle(this.localize('menuDefinitionsDisplayName'))
	},

	columns: [],
	menuDefinitionsSelections: [],
	allSelected: false,

	displayName: '',

	menuDefinitions: {
		mtype: 'store',
		sorters: ['order'],
		fields: ['id', 'title', 'roles', {
			name: 'order',
			type: 'int'
		}, {
			name: 'active',
			type: 'bool'
		}, {
			name: 'order',
			type: 'int'
		}].concat(RS.common.grid.getSysFields()),
		sorters: [{
			property: 'order',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/menudefinition/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)

		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'title',
			filterable: true,
			sortable: false,
			flex: 1
		}, {
			header: '~~active~~',
			dataIndex: 'active',
			filterable: true,
			sortable: false,
			align: 'center',
			width: 60,
			renderer: RS.common.grid.booleanRenderer()
		}, {
			header: '~~roles~~',
			dataIndex: 'roles',
			filterable: false,
			sortable: false,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))

		this.set('displayName', this.localize('menuDefinitionsDisplayName'))
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createMenuDefinition: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.MenuDefinition'
		})
	},
	editMenuDefinition: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.MenuDefinition',
			params: {
				id: id
			}
		})
	},
	deleteMenuDefinitionIsEnabled$: function() {
		return this.menuDefinitionsSelections.length > 0
	},
	deleteMenuDefinition: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.menuDefinitionsSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.menuDefinitionsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},

	reallyDeleteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/menudefinition/delete',
					params: {
						ids: '',
						whereClause: this.menuDefinitions.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							clientVM.getUser()
							this.set('menuDefinitionsSelections', [])
							this.menuDefinitions.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.menuDefinitionsSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/menudefinition/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							clientVM.getUser()
							this.set('menuDefinitionsSelections', [])
							this.menuDefinitions.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	},

	definitionDrop: function() {
		//Go through the new list of records, setting up their sequence, and then send the list of ids to the server
		var ids = []
		this.menuDefinitions.each(function(record) {
			ids.push(record.get('id'))
		})

		this.ajax({
			url: '/resolve/service/menudefinition/sequence',
			params: {
				ids: ids
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (!response.success) {
					this.menuDefinitions.load()
					clientVM.displayError(response.message)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	}
})
glu.defModel('RS.sysadmin.MenuItem', {
	id: '',
	name: '',
	title: '',
	group: '',
	active: true,
	query: '',
	editMode: false,
	originalItem: null,

	fields: ['title', 'group', {
		name: 'active',
		type: 'bool'
	}, 'query'],

	roles: {
		mtype: 'store',
		fields: ['name'],
		proxy: {
			type: 'memory'
		}
	},
	rolesSelections: [],
	rolesColumns: [],

	init: function() {
		this.set('rolesColumns', [{
			header: '~~uname~~',
			dataIndex: 'name',
			filterable: true,
			sortable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))
	},

	addRole: function() {
		this.open({
			mtype: 'RS.sysadmin.RolePicker',
			callback: this.addNewRole
		})
	},
	addNewRole: function(newRoles) {
		Ext.Array.forEach(newRoles, function(role) {
			if (this.roles.findExact('name', role.get('uname')) == -1) this.roles.add({
				name: role.get('uname')
			})
		}, this)
	},
	removeRole: function() {
		Ext.Array.forEach(this.rolesSelections, function(r) {
			this.roles.remove(r)
		}, this)
	},
	removeRoleIsEnabled$: function() {
		return this.rolesSelections.length > 0
	},
	applyMenuItem: function() {
		if (this.originalItem) {
			var newValues = this.asObject()
			Ext.Object.each(newValues, function(key) {
				this.originalItem.set(key, newValues[key])
			}, this)
			var newRoles = [];
			this.roles.each(function(role) {
				newRoles.push(role.get('name'))
			})
			this.originalItem.set('roles', newRoles)
			this.originalItem.set('rolesString', newRoles.join(', '))
		}
		this.doClose()
	},
	applyMenuItemIsVisible$: function() {
		return this.editMode
	},
	addMenuItem: function() {
		var menuRoles = []

		this.roles.each(function(role) {
			menuRoles.push(role.get('name'))
		})

		var selection = this.parentVM.menuItemsSelections,
			menuConfig = {
				id: Ext.id(),
				title: this.title,
				query: this.query,
				group: this.group,
				active: this.active,
				query: this.query,
				roles: menuRoles,
				rolesString: menuRoles.join(', '),
				leaf: true
			};
		var parentNode = this.parentVM.menuItems.getRootNode();
		if (selection.length > 0 && selection[0].raw.isGroup)
			parentNode = selection[0];

		var children = parentNode.childNodes;
		for (var idx=0;idx<children.length;idx++) {
			var node = children[idx];
			if (node.get('title') == menuConfig.title) {
				clientVM.displayError(this.localize('ItemAlreadyExist', {
					title: menuConfig.title
				}));
				return;
			}
		}
		parentNode.appendChild(menuConfig);
		this.doClose()
	},
	addMenuItemIsVisible$: function() {
		return !this.editMode
	},

	cancel: function() {
		this.doClose();
	}
});
glu.defModel('RS.sysadmin.MenuPicker', {
	mock: false,

	menus: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['title'],
		fields: ['id', 'title', 'name'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/common/menudefinition/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	menusSelections: [],
	columns: [],

	init: function() {
		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'title',
			filterable: true,
			sortable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))
		this.menus.load()
	},

	cancel: function() {
		this.doClose()
	},

	add: function() {
		Ext.Array.forEach(this.menusSelections, function(r) {
			var contains = false;
			this.parentVM.menus.each(function(menu) {
				if (menu.get('displayName') == r.get('title')) contains = true
			})
			if (!contains) {
				r.set('displayName', r.get('title'))
				r.set('value', r.get('name'))
				delete r.data.id
				this.parentVM.menus.add(r.data)
			}
		}, this)
		this.doClose()
	},
	addIsEnabled$: function() {
		return this.menusSelections.length > 0
	}
});
glu.defModel('RS.sysadmin.MenuSet', {

	mock: false,

	MenuSetTitle$: function() {
		return this.localize('title') + (this.name ? ' - ' + this.name : '')
	},

	activate: function(screen, params) {
		this.resetForm()
		this.set('id', params ? params.id : '')
		this.loadMenuSet()
		clientVM.setWindowTitle(this.localize('title'))
	},

	resetForm: function() {
		this.loadData(this.defaultData)
		this.roles.removeAll()
		this.menus.removeAll()
	},

	fields: [
		'id',
		'name', {
			name: 'active',
			type: 'bool'
		}
	].concat(RS.common.grid.getSysFields()),

	id: '',
	name: '',
	active: false,
	sysCreatedOn: '',
	sysCreatedOnDisplay$: function() {
		return Ext.isDate(this.sysCreatedOn) ? Ext.Date.format(this.sysCreatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysUpdatedOn: '',
	sysUpdatedOnDisplay$: function() {
		return Ext.isDate(this.sysUpdatedOn) ? Ext.Date.format(this.sysUpdatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysCreatedBy: '',
	sysUpdatedBy: '',

	sysOrganizationName: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	defaultData: {
		name: '',
		active: false,
		sys_id: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrganizationName: ''
	},

	menusColumns: [],
	rolesColumns: [],

	nameIsValid$: function() {
		if (!this.name) return this.localize('nameInvalid')
		return true
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)

		this.set('rolesColumns', [{
			header: '~~name~~',
			dataIndex: 'value',
			filterable: true,
			sortable: true,
			flex: 1
		}]);

		this.set('menusColumns', [{
			header: '~~name~~',
			dataIndex: 'displayName',
			filterable: true,
			sortable: false,
			flex: 1
		}]);
	},

	loadMenuSet: function() {
		if (this.id) {
			this.roles.removeAll()
			this.menus.removeAll()
			this.ajax({
				url: '/resolve/service/menuset/get',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.loadData(response.data)
						this.set('active', response.data.active)
						this.roles.add(response.data.sysPerspectiveroles || [])
						this.menus.add(response.data.sysPerspectiveapplications || [])
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},

	refresh: function() {
		this.loadMenuSet()
	},

	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.persistMenuSet, this, [exitAfterSave])
		this.task.delay(500)
		this.set('persisting', true)
	},
	saveIsEnabled$: function() {
		return !this.persisting && this.isValid
	},
	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	persisting: false,
	persistMenuSet: function(exitAfterSave) {
		var roles = [],
			menus = [],
			organizations = [];

		this.roles.each(function(role) {
			roles.push(role.get('value'))
		})

		this.menus.each(function(menu, index) {
			// menu.data.sequence = index
			delete menu.data.id
			menus.push(menu.data)
		})

		this.ajax({
			url: '/resolve/service/menuset/save',
			jsonData: {
				id: this.id,
				name: this.name,
				roles: roles,
				active: this.active,
				sysPerspectiveapplications: menus
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.getUser()
					clientVM.displaySuccess(this.localize('MenuSetSaved'))
					if (exitAfterSave === true) clientVM.handleNavigation({
						modelName: 'RS.sysadmin.MenuSets'
					})
					else {
						this.set('id', response.data.id)
						RS.common.grid.updateSysFields(this, response.data)
					}
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('persisting', false);
			}
		})
	},

	back: function() {
		clientVM.handleNavigationBack()
	},

	roles: {
		mtype: 'store',
		fields: ['id', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	rolesSelections: [],
	rolesColumns: [],

	addRole: function() {
		this.open({
			mtype: 'RS.sysadmin.RolePicker',
			callback: this.addNewRole
		})
	},
	addNewRole: function(newRoles) {
		Ext.Array.forEach(newRoles, function(role) {
			if (this.roles.findExact('value', role.get('uname')) == -1) this.roles.add({
				value: role.get('uname')
			})
		}, this)
	},
	removeRole: function() {
		Ext.Array.forEach(this.rolesSelections, function(r) {
			this.roles.remove(r)
		}, this)
	},
	removeRoleIsEnabled$: function() {
		return this.rolesSelections.length > 0
	},

	menus: {
		mtype: 'store',
		sorters: ['sequence'],
		fields: ['id', 'displayName', 'value', {
			name: 'sequence',
			type: 'int'
		}],
		proxy: {
			type: 'memory'
		}
	},
	menusSelections: [],
	menusColumns: [],

	addMenu: function() {
		this.open({
			mtype: 'RS.sysadmin.MenuPicker'
		})
	},
	removeMenu: function() {
		Ext.Array.forEach(this.menusSelections, function(c) {
			this.menus.remove(c)
		}, this)
	},
	removeMenuIsEnabled$: function() {
		return this.menusSelections.length > 0
	}
})
glu.defModel('RS.sysadmin.MenuSets', {

	mock: false,


	activate: function() {
		this.menuSets.load()
		clientVM.setWindowTitle(this.localize('menuSetsDisplayName'))
	},

	columns: [],
	menuSetsSelections: [],
	allSelected: false,

	displayName: '',
	stateId: 'MenuSets',

	menuSets: {
		mtype: 'store',
		sorters: ['sequence'],
		fields: ['id', 'name', 'roles', 'applications', {
			name: 'sequence',
			type: 'int'
		}, {
			name: 'active',
			type: 'bool'
		}].concat(RS.common.grid.getSysFields()),
		sorters: [{
			property: 'sequence',
			direction: 'ASC'
		}],
		proxy: {

			type: 'ajax',
			url: '/resolve/service/menuset/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)

		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'name',
			filterable: true,
			sortable: false,
			flex: 1
		}, {
			header: '~~active~~',
			dataIndex: 'active',
			filterable: true,
			sortable: false,
			align: 'center',
			width: 60,
			renderer: RS.common.grid.booleanRenderer()
		}, {
			header: '~~applications~~',
			dataIndex: 'applications',
			filterable: false,
			sortable: false,
			flex: 1
		}, {
			header: '~~roles~~',
			dataIndex: 'roles',
			filterable: false,
			sortable: false,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))

		this.set('displayName', this.localize('menuSetsDisplayName'))
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createMenuSet: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.MenuSet'
		})
	},
	editMenuSet: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.MenuSet',
			params: {
				id: id
			}
		})
	},
	deleteMenuSetIsEnabled$: function() {
		return this.menuSetsSelections.length > 0
	},
	deleteMenuSet: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.menuSetsSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.menuSetsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	arrange: function(data, dropRec, dropPosition, dropHandlers) {
		var ids = [];
		this.menuSets.each(function(r, idx) {
			//console.log(r.get('name'));
			ids.push(r.get('id'));
		});
		this.ajax({
			url: '/resolve/service/menuset/sequence',
			params: {
				ids: ids
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.menuSets.load();
				} else {
					clientVM.displayError(this.localize('orderErrMsg'));
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})

	},
	reallyDeleteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/menuset/delete',
					params: {
						ids: '',
						whereClause: this.menuSets.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							clientVM.getUser()
							this.set('menuSetsSelections', [])
							this.menuSets.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			} else {
				var ids = []
				Ext.each(this.menuSetsSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/menuset/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							clientVM.getUser()
							this.set('menuSetsSelections', [])
							this.menuSets.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	}
})
glu.defModel('RS.sysadmin.Organization', {
	id: '',

	uorganizationName: '',
	description: '',

	sysCreatedOn: '',
	sysCreatedOnDisplay$: function() {
		return Ext.isDate(this.sysCreatedOn) ? Ext.Date.format(this.sysCreatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysUpdatedOn: '',
	sysUpdatedOnDisplay$: function() {
		return Ext.isDate(this.sysUpdatedOn) ? Ext.Date.format(this.sysUpdatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysCreatedBy: '',
	sysUpdatedBy: '',

	sysOrganizationName: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	title: '',

	waitingResponse: false,

	fields: ['uorganizationName', 'udescription', 'udisable'].concat(RS.common.grid.getSysFields()),

	init: function() {
		if (this.id) {
			this.set('title', this.localize('editOrganizationTitle'));
			this.ajax({
				url: '/resolve/service/organization/get',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success)
						this.loadData(response.data)
					else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		} else
			this.set('title', this.localize('createOrganizationTitle'))

	},

	nameIsValid$: function() {
		if (!this.uorganizationName) return this.localize('orgNameRequired')
		if (this.uorganizationName.length > 300) this.localize('orgNameMaxLength')
		return true
	},
	descriptionIsValid$: function() {
		return this.udescription.length > 4000 ? this.localize('orgDescriptionMaxLength') : true
	},

	saveIsEnabled$: function() {
		return this.isValid && !this.waitingResponse
	},
	save: function() {
		this.set('waitingResponse', true)
		this.ajax({
			url: '/resolve/service/organization/save',
			jsonData: this.asObject(),
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					clientVM.displaySuccess(this.localize('organizationSaved'))
					this.parentVM.loadOrganizations()
					this.doClose()
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('waitingResponse', false)
			}
		})
	},

	cancel: function() {
		this.doClose();
	}


});
glu.defModel('RS.sysadmin.OrganizationPicker', {

	mock: false,

	organizations: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uorganizationName'],
		fields: ['id', 'uorganizationName'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/common/organizationList',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	organizationsSelections: [],
	columns: [],

	init: function() {
		var cols = [{
			header: '~~name~~',
			dataIndex: 'uorganizationName',
			filterable: true,
			sortable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()),
			sysOrgCol = null;
		Ext.Array.forEach(cols, function(col) {
			if (col.dataIndex == 'sysOrganizationName') sysOrgCol = col
		})
		if (sysOrgCol) cols.splice(cols.indexOf(sysOrgCol), 1)
		this.set('columns', cols)
		this.organizations.load()
	},

	cancel: function() {
		this.doClose()
	},

	add: function() {
		Ext.Array.forEach(this.organizationsSelections, function(r) {
			if (this.parentVM.organizations.indexOf(r) == -1)
				this.parentVM.organizations.add(r.data)
		}, this)
		this.doClose()
	},
	addIsEnabled$: function() {
		return this.organizationsSelections.length > 0
	}
})
glu.defModel('RS.sysadmin.Organizations', {

	mock: false,

	activate: function() {
		clientVM.setWindowTitle(this.localize('organizationsDisplayName'))
		this.loadOrganizations()
	},

	columns: [],
	organizationsSelections: [],
	allSelected: false,

	displayName: '',
	stateId: 'organizations',

	organizations: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uorganizationName'],
		fields: ['id', 'uorganizationName', 'udescription', {
			name: 'udisable',
			type: 'boolean'
		}].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/organization/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)

		var cols = [{
			header: '~~organizationName~~',
			dataIndex: 'uorganizationName',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~description~~',
			dataIndex: 'udescription',
			filterable: true,
			sortable: true,
			flex: 2
		}, {
			header: '~~disable~~',
			dataIndex: 'udisable',
			filterable: true,
			sortable: true,
			width: 65,
			align: 'center',
			renderer: RS.common.grid.booleanRenderer()
		}].concat(RS.common.grid.getSysColumns()),
			sysOrgCol = null;
		Ext.Array.forEach(cols, function(col) {
			if (col.dataIndex == 'sysOrganizationName') sysOrgCol = col
		})
		if (sysOrgCol) cols.splice(cols.indexOf(sysOrgCol), 1)
		this.set('columns', cols)

		this.set('displayName', this.localize('organizationsDisplayName'))
	},

	loadOrganizations: function() {
		this.organizations.load()
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createOrganization: function() {
		this.open({
			mtype: 'RS.sysadmin.Organization'
		})
	},
	editOrganization: function(id) {
		this.open({
			mtype: 'RS.sysadmin.Organization',
			id: id
		})
	},
	deleteOrganizationIsEnabled$: function() {
		return this.organizationsSelections.length > 0
	},
	deleteOrganization: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.organizationsSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.organizationsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/organization/delete',
					params: {
						ids: '',
						whereClause: this.organizations.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('organizationsSelections', [])
							this.organizations.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			} else {
				var ids = []
				Ext.each(this.organizationsSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/organization/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('organizationsSelections', [])
							this.organizations.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	},

	enableOrganizationIsEnabled$: function() {
		return this.organizationsSelections.length > 0
	},
	enableOrganization: function() {
		this.message({
			title: this.localize('enableTitle'),
			msg: this.localize(this.organizationsSelections.length == 1 ? 'enableMessage' : 'enablesMessage', this.organizationsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('enableAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyEnableRecords
		})
	},
	reallyEnableRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/organization/enable',
					params: {
						ids: '',
						whereClause: this.organizations.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('organizationsSelections', [])
							this.organizations.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			} else {
				var ids = []
				Ext.each(this.organizationsSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/organization/enable',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('organizationsSelections', [])
							this.organizations.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	},

	disableOrganizationIsEnabled$: function() {
		return this.organizationsSelections.length > 0
	},
	disableOrganization: function() {
		this.message({
			title: this.localize('disableTitle'),
			msg: this.localize(this.organizationsSelections.length == 1 ? 'disableMessage' : 'disablesMessage', this.organizationsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('disableAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDisableRecords
		})
	},
	reallyDisableRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/organization/disable',
					params: {
						ids: '',
						whereClause: this.organizations.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('organizationsSelections', [])
							this.organizations.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			} else {
				var ids = []
				Ext.each(this.organizationsSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/organization/disable',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('organizationsSelections', [])
							this.organizations.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	}
})
glu.defModel('RS.sysadmin.RolePicker', {

	mock: false,

	isRemove: false,
	includePublic : "true",
	title$: function() {
		return this.isRemove ? this.localize('removeRoleTitle') : this.localize('addRoleTitle')
	},

	roles: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uname'],
		fields: ['id', 'uname'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/common/roles/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	rolesSelections: [],
	columns: [],

	keyword: '',
	delayedTask : null,
	when_keyword_changed: {
		on: ['keywordChanged'],
		action: function() {
			if(this.delayedTask)
				clearTimeout(this.delayedTask);
			this.delayedTask = setTimeout(function(){
				this.roles.loadPage(1);
			}.bind(this),300)		
		}
	},

	init: function() {
		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'uname',
			filterable: true,
			sortable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))
		this.roles.on('beforeload', function(store, operation, opts) {
			operation.params = operation.params || {};
			var filter = [];
			if (this.keyword) {
				filter = [{
					field: 'uname',
					type: 'auto',
					condition: 'contains',
					value: this.keyword
				}]
			}
			Ext.apply(operation.params, {
				operator: 'or',
				includePublic : this.includePublic,
				filter: Ext.encode(filter)
			})
		}, this);

		this.roles.load()
	},

	cancel: function() {
		this.doClose()
	},

	add: function() {
		if (this.callback)
			this.callback.apply(this.parentVM, [this.rolesSelections])
		else
			Ext.Array.forEach(this.rolesSelections, function(r) {
				if (this.parentVM.roles.indexOf(r) == -1)
					this.parentVM.roles.add(r.data)
			}, this)
		this.doClose()
	},
	addIsEnabled$: function() {
		return this.rolesSelections.length > 0
	},
	addIsVisible$: function() {
		return !this.isRemove
	},
	remove: function() {
		this.add()
	},
	removeIsEnabled$: function() {
		return this.addIsEnabled
	},
	removeIsVisible$: function() {
		return !this.addIsVisible
	}
})
glu.defModel('RS.sysadmin.ServerInformation', {
	mock: false,
	activate: function() {
		this.loadSystemInformation()
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)
		this.set('sessionColumns', [{
			header: '~~user~~',
			dataIndex: 'user',
			sortable: true,
			filterable: true,
			flex: 1
		}, {
			header: '~~host~~',
			dataIndex: 'host',
			sortable: true,
			filterable: true,
			flex: 1
		}, {
			header: '~~sysCreatedOn~~',
			dataIndex: 'sysCreatedOn',
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat()),
			width: 200,
			sortable: true,
			filterable: true
		}, {
			header: '~~sysUpdatedOn~~',
			dataIndex: 'sysUpdatedOn',
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat()),
			width: 200,
			sortable: true,
			filterable: true
		}, {
			header: '~~duration~~',
			dataIndex: 'duration',
			sortable: true,
			filterable: true,
			flex: 1
		}])
	},

	fields: [
		'version',
		'build',

		'osArchitecture',
		'osName',
		'osVersion',
		'address',
		'osProcessor',
		'jvmVendor',
		'jvmName',
		'jvmVersion',

		'memoryMax',
		'memoryTotal',
		'memoryFreePercentage',

		'threadPeak',
		'threadLive',
		'threadDeamon'
	],

	version: '',
	build: '',

	osArchitecture: '',
	osName: '',
	osVersion: '',
	address: '',
	serverProcessor: 0,
	jvmVendor: '',
	jvmName: '',
	jvmVersion: '',

	memoryMax: 0,
	memoryTotal: 0,
	memoryFreePercentage: '',

	threadPeak: 0,
	threadLive: 0,
	threadDeamon: 0,

	sessions: {
		mtype: 'store',
		fields: ['id', 'user', 'host', {
			name: 'duration',
			type: 'int'
		}].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	sessionColumns: [],

	loadSystemInformation: function() {
		this.ajax({
			url: '/resolve/service/server/get',
			method: 'POST',
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.loadData(response.data)
					if (Ext.isArray(response.data.sessions)) Ext.Array.forEach(response.data.sessions, function(session) {
						this.sessions.add(session)
					}, this)
				} else clientVM.displayError(response.message)

			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	refresh: function() {
		this.loadSystemInformation()
	},

	back: function() {
		clientVM.handleNavigationBack()
	}
})
glu.defModel('RS.sysadmin.SessionInformation', {
	activeSessions: '',
	maxActiveSessions: '',
	maxSessions: '',
	timeout: '',
	wait: false,

	sessionColumns: null,
	sessionsSelections: [],
	sessionStore: {
		mtype: 'store',
		fields: ['username', 'host', {
			name: 'createTime',
			type: 'date',
			format: 'time',
			dateFormat: 'time'
		}, {
			name: 'updateTime',
			type: 'date',
			format: 'time',
			dateFormat: 'time'
		}, 'durationInSecs', 'sessionDurationInMin', 'ssoType', 'ssoSessionId', {
			name: 'lastSSOValidationTime',
			type: 'date',
			format: 'time',
			dateFormat: 'time'
		}, 'iframeId', 'sessionId'
		],
		proxy: {
			type: 'memory'
		}
	},
	fields: ['activeSessions', 'maxActiveSessions', 'maxSessions', 'timeout'],

	activate: function() {},
	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true);
		this.set('sessionColumns', [{
			header: '~~username~~',
			dataIndex: 'username',
			flex: 1
		}, {
			header: '~~host~~',
			dataIndex: 'host',
			flex: 1
		}, {
			header: '~~createTime~~',
			dataIndex: 'createTime',
			flex: 1,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}, {
			header: '~~updateTime~~',
			dataIndex: 'updateTime',
			flex: 1,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}, {
			header: '~~durationInSecs~~',
			dataIndex: 'durationInSecs',
			flex: 1
		}, {
			header: '~~sessionDurationInMins~~',
			dataIndex: 'sessionDurationInMin',
			flex: 1
		}, {
			header: '~~ssoType~~',
			dataIndex: 'ssoType',
			flex: 1
		}, {
			header: '~~ssoSessionId~~',
			dataIndex: 'ssoSessionId',
			flex: 1
		}, {
			header: '~~lastSSOValidationTime~~',
			dataIndex: 'lastSSOValidationTime',
			flex: 1,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}, {
			header: '~~iframeId~~',
			dataIndex: 'iframeId',
			flex: 1
		}, {
			header: '~~sessionId~~',
			dataIndex: 'sessionId',
			flex: 1
		}]);

		this.loadUserSessionInformation();
	},
	loadUserSessionInformation: function() {
		this.sessionStore.removeAll()
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/user/sessioninfo/get',
			success: function(resp) {
				this.set('wait', false);
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('LoadUserSessionInfoErrMsg'), respData.message);
					return;
				}
				var info = respData.data;
				this.loadData(info);
				this.sessionStore.add(info.sessionDetails);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		});
	},
	removeSessions: function() {
		this.message({
			title: this.localize('RemoveSessionTitle'),
			msg: this.sessionsSelections.length === 1 ? this.localize('RemoveSessionMsg') : this.localize('RemoveSessionMsgs', this.sessionsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('remove'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn == 'no')
					return;
				var ids = [];
				this.set('wait', true);
				Ext.each(this.sessionsSelections, function(s) {
					ids.push(s.get('id'));
				}, this)
				this.ajax({
					url: '/resolve/service/user/sessioninfo/delete',
					params: {
						ids: ids
					},
					scope: this,
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						if (!respData.success) {
							clientVM.displayError(this.localize('DeleteErrMsg') + '[' + respData.message + ']');
							return;
						}

						clientVM.displaySuccess(this.localize('DeleteSucMsg'));
						this.loadUserSessionInformation();
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					},
					callback: function() {
						this.set('wait', false);
					}
				});
			}
		})
	},

	removeSessionsIsEnabled$: function() {
		return !this.wait && this.sessionsSelections.length > 0;
	},

	refresh: function() {
		this.loadUserSessionInformation();
	}
});
glu.defModel('RS.sysadmin.SystemProperties', {

	mock: false,

	stateId: 'sysadminsystemproperties',
	waitResponse: false,

	propertiesStore: {
		mtype: 'store',
		model: 'RS.sysadmin.model.SystemProperty',
		pageSize: -1,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/sysproperties/getAllProperties',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	propertiesSelections: [],

	columns: null,

	displayName: '',

	isRoot$: function() {
		return clientVM.isRootUser
	},

	activate: function() {
		clientVM.setWindowTitle(this.localize('windowTitle'))

	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true);
		this.loadRecs();
		this.set('displayName', this.localize('systemPropertiesTitle'));
		this.set('columns', [{
			header: '~~uname~~',
			dataIndex: 'uname',
			filterable: true,
			width: 230,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~uvalue~~',
			dataIndex: 'uvalue',
			sortable: false,
			filterable: true,
			groupable: false,
			width: 230,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~udescription~~',
			dataIndex: 'udescription',
			filterable: false,
			sortable: false,
			groupable: false,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}].concat(RS.common.grid.getSysColumns()));
	},

	loadRecs: function() {
		delete this.propertiesStore.sortInfo;
		this.propertiesStore.load({
			scope: this,
			callback: function(records, op, success) {
				if (!success) {
					//clientVM.displayError(this.localize('getListErrorMsg'));
				} else {
					this.propertiesStore.sort([{
						property: 'uname'
					}]);
					this.propertiesStore.group([{
						property: 'group'
					}]);
				}

				this.set('waitResponse', false);
			}
		});
		this.set('waitResponse', true);
	},

	attachGroup: function(records) {
		var name = null;
		var tokens = null;
		for (var idx = 0; idx < records.length; idx++) {
			name = records[idx].name;
			tokens = name.split('\.');
			records.group = tokens;
		}
	},

	createProperty: function() {
		this.open({
			mtype: 'RS.sysadmin.SystemProperty',
			id: ''
		});
	},

	editProperty: function(id) {
		this.open({
			mtype: 'RS.sysadmin.SystemProperty',
			id: id
		});
	},

	deleteProperties: function() {

		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.propertiesSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', {
				len: this.propertiesSelections.length
			}) + '<br/>',
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteProperties
		});
	},

	reallyDeleteProperties: function(btn) {
		if (btn != 'yes')
			return;
		var ids = [];
		for (var i = 0; i < this.propertiesSelections.length; i++) {
			ids.push(this.propertiesSelections[i].data.id);
		}
		// ids = ids.join(',');
		this.set('waitResponse', true);
		this.ajax({
			url: '/resolve/service/sysproperties/deleteProperties',
			jsonData: ids,
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('deleteFailureMsg') + '[' + respData.message + ']');
				} else
					clientVM.displaySuccess(this.localize('deleteSucMsg'));
				this.loadRecs();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('waitResponse', false);
			}
		});
	},

	createPropertyIsEnabled$: function() {
		return !this.waitResponse;
	},

	deletePropertiesIsEnabled$: function() {
		return this.propertiesSelections.length > 0 && !this.waitResponse;
	},

	updateSystemProperties: function() {
		this.loadRecs();
	}
});
glu.defModel('RS.sysadmin.SystemProperty', {
	id: '',
	sys_id: '',
	uname: '',
	udescription: '',
	uvalue: '',
	title: '',
	sysOrganizationName: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	userDateFormat: null,

	defaultData: {
		id: '',
		uname: '',
		sys_id: '',
		udescription: '',
		uvalue: '',
		sysOrganizationName: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: ''
	},



	fields: ['id', 'uname', 'uvalue', 'udescription', 'sysOrganizationName', 'sysCreatedOn', 'sysCreatedBy', 'sysUpdatedOn', 'sysUpdatedBy'],

	waitingResponse: false,
	dockedItems: [{
		xtype: 'toolbar',
		items: ['saveProperty', 'deleteProperty', 'cancel']
	}],

	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		var id = this.id || ''
		this.resetForm()
		this.set('id', params && params.id != null ? params.id : id);
		if (this.id != '') {
			this.loadSysProp();
			return;
		}
	},

	init: function() {
		if ( !! this.id) {
			this.set('title', this.localize('editSystemPropertyTitle'));
			this.loadSysProp();
		} else {
			this.resetForm();
			this.set('title', this.localize('createSystemPropertyTitle'));
		}

	},

	loadSysProp: function() {
		this.ajax({
			url: '/resolve/service/sysproperties/getSystemProperty',
			params: {
				id: this.id
			},
			scope: this,
			success: function(resp, opt) {
				var respData = RS.common.parsePayload(resp);
				var data = respData.data;
				if (!respData.success) {
					clientVM.displayError(this.localize('GetSysPropErrMsg') + '[' + respData.message + ']');
					return;
				}
				this.setData(data);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('waitingResponse', false);
			}
		});
		this.set('waitingResponse', true);
	},

	unameIsValid$: function() {
		var rexp1 = /^[A-Za-z0-9_ ]+[\\.]([A-Za-z0-9_ ]+[\\.])*[A-Za-z0-9_ ]+$/;
		var rexp2 = /^[A-Za-z0-9_ ]+$/;
		return this.uname != null &&
			this.uname.length > 0 &&
			(rexp1.test(this.uname) || rexp2.test(this.uname)) &&
			this.uname.length <= 333 ? true : this.localize('invalidNameErr');
	},
	udescriptionIsValid$: function() {
		return this.udescription.length <= 4000 ? true : this.localize('invalidDescriptionErr');
	},

	uvalueIsValid$: function() {
		return true //this.uvalue.length <= 333 ? true : this.localize('invalidValueErr');
	},

	save: function(exitAfterSave) {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveProperty, this, [exitAfterSave])
		this.task.delay(500)
		this.set('waitingResponse', true)
	},
	saveIsEnabled$: function() {
		return !this.waitingResponse;
	},
	saveAndExit: function() {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.save(true)
	},

	saveProperty: function(exitAfterSave) {
		var systemProperty = {
			sys_id: this.id,
			UName: this.uname,
			UDescription: this.udescription,
			UValue: this.uvalue
		};
		if (this.parentVM.isRoot)
			systemProperty.sysOrg = this.sysOrg;
		this.ajax({
			url: '/resolve/service/sysproperties/saveSystemProperty',
			jsonData: systemProperty,
			scope: this,
			success: function(resp, opt) {
				var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					this.setData(respData.data);
					this.parentVM.updateSystemProperties();
					clientVM.displaySuccess(this.localize('saveSuccess'));
					// if (exitAfterSave === true)
					this.doClose();
				} else
					clientVM.displayError(this.localize('SaveSysPropErrMsg') + '[' + respData.message + ']');
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('waitingResponse', false);
			}
		});
		this.set('waitingResponse', true);
	},

	resetForm: function() {
		this.loadData(this.defaultData);
		this.set('isPristine', true);
	},

	cancel: function() {
		this.doClose();
	},

	toParent: function() {
		this.doClose();
	},

	deletePropertyIsVisible$: function() {
		if (this.id === '')
			return false;
		else
			return true;
	},

	setData: function(data) {
		this.loadData(data);
		this.commit();
	},

	refresh: function() {
		if (this.id != null && this.id != '')
			this.loadSysProp();
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	savePropertyIsEnabled$: function() {
		return this.isValid && !this.waitingResponse && this.isDirty;
	},

	savePropertyIsVisible$: function() {
		return this.parentVM.isRoot;
	},

	cancelIsVisible$: function() {
		return this.parentVM.isRoot;
	},

	toParentIsVisible$: function() {
		return !this.parentVM.isRoot;
	},

	editable$: function() {
		return !this.parentVM.isRoot;
	}
});
glu.defModel('RS.sysadmin.SystemScript', {
	mock: false,
	wait: false,
	sysScriptTitle: '',
	id: '',
	sys_id: '',
	uname: '',
	udescription: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrganizationName: '',
	uscript: '',
	scriptTitle: '',

	defaultData: {
		id: '',
		sys_id: '',
		uname: '',
		udescription: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrganizationName: '',
		uscript: ''
	},

	fields: ['id', 'sys_id', 'uname', 'udescription', {
		name: 'sysCreatedOn',
		type: 'long'
	}, 'sysCreatedBy', {
		name: 'sysUpdatedOn',
		type: 'long'
	}, 'sysUpdatedBy', 'uscript', 'sysOrganizationName'],
	activeItem : 0,
	propertiesTab : function(){
		this.set('activeItem', 0);
	},
	propertiesTabIsPressed$ : function(){
		return this.activeItem == 0;
	},
	sourceTab : function(){
		this.set('activeItem', 1);
	},
	sourceTabIsPressed$ : function(){
		return this.activeItem == 1;
	},
	title$: function(){
		if(this.uname.trim() != ""){
			this.set('sysScriptTitle', this.localize('sysScriptTitle') + " - " + this.uname);
		}
		else 
			this.set('sysScriptTitle', this.localize('sysScriptTitle')); 
	},
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	activate: function(screen, params) {
		var id = this.id || '';
		this.resetForm();
		this.set('id', params && params.id != null ? params.id : id);
		if (this.id != '') {
			this.loadScript();
		}
		this.set('activeItem', 0);
	},
	init: function() {
		this.set('scriptTitle', this.localize('scriptTitle'));
		this.set('sysScriptTitle', this.localize('sysScriptTitle'));
	},
	saveScript: function(exitAfterSave) {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		var data = {
			id: this.id,
			UName: this.uname,
			UDescription: this.udescription,
			UScript: this.uscript
		}
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/sysscript/save',
			jsonData: data,
			scope: this,
			success: function(resp) {
				if (this.handleSaveSucResp(resp) && exitAfterSave === true)
					clientVM.handleNavigation({
						modelName: 'RS.sysadmin.SystemScripts'
					});
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	save: function(exitAfterSave) {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveScript, this, [exitAfterSave])
		this.task.delay(500)
	},
	saveIsEnabled$: function() {
		return !this.wait && this.isValid;
	},	
	refresh: function() {
		if (this.id) {
			this.loadScript();
		} else {
			this.resetForm();
		}
	},

	loadScript: function() {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/sysscript/get',
			params: {
				id: this.id
			},
			scope: this,
			success: this.handleLoadSucResp,
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	handleLoadSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('LoadErrMsg') + '[' + respData.message + ']');
			return false;
		}
		var data = respData.data;
		this.setData(data);
		return true;
	},

	setData: function(data) {
		this.loadData(data);
	},

	handleSaveSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']');
			return;
		}
		var data = respData.data;
		this.setData(data);
		clientVM.displaySuccess(this.localize('SaveSucMsg'));
		return true;
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.SystemScripts'
		})
	},
	execute: function() {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/sysscript/executeScript',
			params: {
				name: this.uname
			},
			scope: this,
			success: function(resp) {
				this.handleExeSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	handleExeSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('ExeErrMsg') + '[' + respData.message + ']');
			return;
		}

		clientVM.displaySuccess(this.localize('ExeSucMsg'));
	},

	resetForm: function() {
		this.loadData(this.defaultData);
		this.set('isPristine', true);
		this.set('uname', ''); // set after isPristine so uname field has red exclamation
	},
	unameIsValid$: function() {
		var rexp1 = /^[A-Za-z0-9_ ]+[\\.]([A-Za-z0-9_ ]+[\\.])*[A-Za-z0-9_ ]+$/;
		var rexp2 = /^[A-Za-z0-9_ ]+$/;
		return this.uname != null &&
			(rexp1.test(this.uname) || rexp2.test(this.uname)) &&
			this.uname != '' ? true : this.localize('invalidName');
	},
	saveScriptIsEnabled$: function() {
		return !this.wait && this.isValid && this.isDirty;
	},
	executeIsVisible$: function() {
		return this.id && !/^\s*$/.test(this.id) && !/^\s*$/.test(this.uscript);
	},
	executeIsEnabled$: function() {
		return !this.wait;
	}
});
glu.defModel('RS.sysadmin.SystemScripts', {
	mock: false,

	displayName: '',

	state: '',

	stateId: 'sysadminsysscript',

	store: {
		mtype: 'store',
		fields: ['id', 'uname', 'udescription'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'uname',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/sysscript/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	columns: null,

	scriptsSelections: [],

	activate: function() {
		clientVM.setWindowTitle(this.localize('windowTitle'))

		this.loadScripts();
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true);
		this.set('displayName', this.localize('systemScriptsTitle'));
		this.set('columns', [{
			header: '~~Name~~',
			dataIndex: 'uname',
			filterable: true,
			width: 200,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~Description~~',
			dataIndex: 'udescription',
			filterable: true,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}].concat(RS.common.grid.getSysColumns()));
	},

	loadScripts: function() {
		this.set('state', 'wait');
		this.store.load({
			scope: this,
			callback: function(rec, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('LoadErrMsg'));
					return;
				}
				this.set('state', 'ready');
			}
		});
	},

	editScript: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.SystemScript',
			params: {
				id: id
			}
		});
	},

	createScript: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.SystemScript',
			params: {
				id: ''
			}
		});
	},

	deleteScripts: function() {
		this.message({
			title: this.localize('DeleteScriptsTitle'),
			msg: this.localize(this.scriptsSelections.length > 1 ? 'DeleteScriptsMsg' : 'DeleteScriptMsg', this.scriptsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmDelete'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.sendDeleteReq
		});
	},

	sendDeleteReq: function(btn) {
		if (btn != 'yes') {
			this.set('state', 'itemSelected');
			return;
		}

		this.set('state', 'wait');

		var ids = [];

		Ext.each(this.scriptsSelections, function(r) {
			ids.push(r.get('id'));
		})

		this.ajax({
			url: '/resolve/service/sysscript/delete',
			params: {
				ids: ids,
				all: false
			},
			scope: this,
			success: this.handleDeleteSucResp,
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	handleDeleteSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('DeleteErrMsg') + '[' + respData.message + ']');
		} else
			clientVM.displaySuccess(this.localize('DeleteSucMsg'));
		this.loadScripts();
	},

	createScriptIsEnabled$: function() {
		return this.state != 'wait';
	},

	deleteScriptsIsEnabled$: function() {
		return this.state === 'itemSelected';
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	when_scriptsSelections_changed: {
		on: ['scriptsSelectionsChanged'],
		action: function() {
			if (this.scriptsSelections.length > 0)
				this.set('state', 'itemSelected')
			else
				this.set('state', 'ready');
		}
	}
});
glu.defModel('RS.sysadmin.ToolbarConfiguration', {

	mock: false,
	wait: false,
	toolbarConfTitle: '',
	toolbarConfigurations: {
		mtype: 'treestore',
		fields: ['id', 'name', 'query', 'group', 'roles', 'tooltip', {
			name: 'openAs'
		}, {
			name: 'active',
			type: 'bool'
		}],
		// , {
		// 	name: 'isGroup',
		// 	type: 'bool'
		// }],
		proxy: {
			type: 'memory'
		}
	},
	comboStore: {
		mtype: 'store',
		fields: ['value', 'name'],
		data: [{
			value: 'normal',
			name: 'Normal'
		}, {
			value: 'tab',
			name: 'New Tab'
		}, {
			value: 'window',
			name: 'Popup'
		}]
	},
	queryComboStore: null,
	columns: null,
	toolbarConfigurationsSelections: [],

	sys_id: '',
	uname: 'menu.toolbar',
	udescription: 'Toolbar configuration',
	uvalue: '',
	fields: ['sys_id', 'uname', 'udescription', 'uvalue'],
	activate: function() {
		this.loadConfig();
	},

	init: function() {
		var me = this;

		var handleChanges = function(combo, newQuery) {
			if (me.toolbarConfigurationsSelections.length == 0)
				return;
			var r = me.toolbarConfigurationsSelections[0];
			if (newQuery == 'RS.wiki.AddDocument')
				r.set('openAs', 'window');
		}
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true);
		this.set('toolbarConfTitle', this.localize('toolbarConfTitle'));
		this.set('columns', [{
			xtype: 'treecolumn',
			header: '~~name~~',
			dataIndex: 'name',
			filterable: false,
			sortable: false,
			editor: {},
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~openAs~~',
			dataIndex: 'openAs',
			filterable: false,
			sortable: false,
			editor: {
				xtype: 'combobox',
				editable: false,
				queryMode: 'local',
				displayField: 'name',
				valueField: 'value',
				store: '@{comboStore}',
				listeners: {
					focus: function() {
						if (me.toolbarConfigurationsSelections.length == 0)
							return;
						var r = me.toolbarConfigurationsSelections[0];
						if (r.get('query') == 'RS.wiki.AddDocument') {
							r.set('value', 'window');
							this.setReadOnly(true);
						} else
							this.setReadOnly(false);
					}
				}
			},
			renderer: function(value) {
				switch (value) {
					case 'normal':
						return 'Normal';
					case 'tab':
						return 'New Tab';
					case 'window':
						return 'Popup';
				}
			}
		}, {
			xtype: 'checkcolumn',
			header: '~~active~~',
			dataIndex: 'active',
			filterable: false,
			sortable: false,
			width: 60,
			align: 'center'
		}, {
			header: '~~queryOrUrl~~',
			tooltip: this.localize('queryOrUrlTooltip'),
			tooltipType: 'title',
			dataIndex: 'query',
			filterable: false,
			sortable: false,
			editor: {
				xtype: 'combobox',
				queryMode: 'local',
				displayField: 'name',
				valueField: 'value',
				store: '@{queryComboStore}',
				listeners: {
					change: handleChanges
				}
			},
			flex: 1,
			renderer: (function(self) {
				return function(value) {
					var value0 = value ? value.replace(/\s+/g, '') : '';
					switch (value0) {
						case 'RS.client.Menu':
							return self.localize('menuText');
						case 'RS.social.Main':
							return self.localize('socialText');
						case '/resolve/jsp/rsworksheet.jsp?sys_id=ACTIVE':
							return self.localize('activeWorksheetText');
						case '/resolve/jsp/rsworksheet.jsp?main=WorkSheet':
							return self.localize('allWorksheetsText');
						case 'RS.wiki.Main/name=HOMEPAGE':
							return self.localize('defaultDocumentText');
						case 'RS.wiki.AddDocument':
							return self.localize('addDocumentText');
						default:
							return Ext.String.htmlEncode(value);
					}
				}
			})(this)
		}, {
			header: '~~roles~~',
			dataIndex: 'roles',
			filterable: false,
			sortable: false,
			flex: 1,
			renderer: function(value, metaData, record) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			}
		}, {
			text: '~~tooltip~~',
			dataIndex: 'tooltip',
			filterable: false,
			sortable: false,
			editor: {},
			flex: 1
		}]);

		this.set('queryComboStore', Ext.create('Ext.data.Store', {
			mtype: 'store',
			fields: ['value', 'name'],
			data: [{
				value: 'RS.client.Menu',
				name: this.localize('menuText')
			}, {
				value: 'RS.social.Main',
				name: this.localize('socialText')
			}, {
				value: 'RS.worksheet.Worksheet/id=ACTIVE',
				name: this.localize('activeWorksheetText')
			}, {
				value: 'RS.worksheet.Worksheets',
				name: this.localize('allWorksheetsText')
			}, {
				value: 'RS.wiki.Main/name=HOMEPAGE',
				name: this.localize('defaultDocumentText')
			}, {
				value: 'RS.wiki.AddDocument',
				name: this.localize('addDocumentText')
			}]
		}))
	},

	loadConfig: function() {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/sysproperties/getSystemPropertyByName',
			params: {
				name: 'menu.toolbar'
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('GetErrMsg') + '[' + respData.message + ']');
					return;
				}
				this.loadData(respData.data);
				var json = this.uvalue,
					jsonData = {};
				try {
					jsonData = Ext.JSON.decode(json);
				} catch (e) {
					var jsonData = {
							children: []
						},
						old = clientVM.parseLegacyConfiguration(json);
					Ext.each(old, function(i) {
						jsonData.children.push(this.itemToConfig(i));
					}, this)
				}
				var massage = {};
				Ext.apply(massage, jsonData);

				function m(item) {
					for (key in item) {
						var obj = item[key];
						if (key == 'query' && obj.indexOf('RS.wiki.Main/name=') != -1)
							item[key] = obj.substring(18, obj.length);
						else if (Ext.isArray(obj))
							for (var i = 0; i < obj.length; i++)
								m(obj);
						else if (Ext.isObject(obj))
							m(obj);
					}
				}

				m(massage);
				this.toolbarConfigurations.setRootNode(jsonData);
			},
			failure: function(resp) {
				clientVM.displayError(this.localize('GetErrMsg'));
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	save: function() {
		var root = this.toolbarConfigurations.getRootNode();
		var jsonObject = this.convertNode(root);
		var hasMenu = false;
		Ext.each(jsonObject.children, function(item) {
			if (item.query == "RS.client.Menu")
				hasMenu = true;
		});
		if (!hasMenu)
			this.message({
				title: this.localize('noMenuTitle'),
				msg: this.localize('noMenu'),
				buttons: Ext.MessageBox.OK
			});
		this.uvalue = ' ' + Ext.JSON.encode(jsonObject);

		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/sysproperties/saveSystemProperty',
			jsonData: {
				sys_id: this.sys_id,
				UName: this.uname,
				UDescription: this.udescription,
				UValue: this.uvalue
			},
			scope: this,
			success: function(resp) {
				clientVM.displaySuccess(this.localize('SaveSucMsg'))
				clientVM.getUser()
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},

	saveIsEnabled$: function() {
		return !this.wait;
	},

	addRoles: function() {
		this.open({
			mtype: 'RS.common.GridPicker',
			displayName: this.localize('roleDisplayName'),
			asWindowTitle: this.localize('asWindowTitle'),
			dumperText: this.localize('dump'),
			columns: [{
				header: this.localize('uname'),
				dataIndex: 'uname',
				filterable: true,
				flex: 1
			}],
			storeConfig: {
				fields: ['id', 'uname', 'uhomepage'],
				baseParams : {
					includePublic : "true"
				},
				proxy: {
					type: 'ajax',
					url: '/resolve/service/common/roles/list',
					reader: {
						type: 'json',
						root: 'records'
					},
					listeners: {
						exception: function(e, resp, op) {
							clientVM.displayExceptionError(e, resp, op);
						}
					}
				}
			},
			dumper: (function(self) {
				return function(records) {
					var names = [];
					Ext.each(records, function(r, idx) {
						names.push(r.get('uname'));
					});
					Ext.each(self.toolbarConfigurationsSelections, function(r) {
						var rr = r.get('roles').split(', ');
						if (rr[0] == '')
							rr = [];
						rr = rr.concat(names);
						var map = {};
						Ext.each(rr, function(n) {
							map[n] = n;
						});
						rr = [];
						for (attr in map)
							rr.push(attr);
						r.set('roles', rr.join(', '));
					});
					self.set('toolbarConfigurationsSelections', []);
					return true;
				}
			})(this)
		});
	},

	addRolesIsEnabled$: function() {
		return this.toolbarConfigurationsSelections.length > 0 && !this.wait;
	},

	removeRoles: function() {

		this.open({
			mtype: 'RS.common.GridPicker',
			displayName: this.localize('roleDisplayName'),
			asWindowTitle: this.localize('asWindowTitle'),
			dumperText: this.localize('dumpRemove'),
			columns: [{
				header: this.localize('uname'),
				dataIndex: 'uname',
				filterable: true,
				flex: 1
			}],
			storeConfig: {
				fields: ['id', 'uname', 'uhomepage'],
				baseParams : {
					includePublic : "true"
				},
				proxy: {
					type: 'ajax',
					url: '/resolve/service/common/roles/list',
					reader: {
						type: 'json',
						root: 'records'
					},
					listeners: {
						exception: function(e, resp, op) {
							clientVM.displayExceptionError(e, resp, op);
						}
					}
				}
			},
			loader: (function(self) {
				return function() {
					var roles = {};
					Ext.each(self.toolbarConfigurationsSelections, function(r) {
						if (r.get('roles') == '')
							return;
						var rs = r.get('roles').split(', ');
						Ext.each(rs, function(ro) {
							roles[ro] = {
								uname: ro
							};
						});
					}, self);
					for (name in roles)
						this.store.add(roles[name]);
				}
			})(this),
			dumper: (function(self) {
				return function(records) {
					Ext.each(self.toolbarConfigurationsSelections, function(t) {
						var names = [];
						var roles = t.get('roles').split(', ');
						Ext.each(roles, function(role) {
							for (idx in records) {
								if (records[idx].get('uname') == role)
									return;
							}
							names.push(role);
						});
						t.set('roles', names.join(', '));
					});
					self.set('toolbarConfigurationsSelections', []);
					return true;
				}
			})(this)
		});
	},

	removeRolesIsEnabled$: function() {
		return this.toolbarConfigurationsSelections.length > 0 && !this.wait;
	},

	convertNode: function(node, parent) {
		var node0 = {};
		node0.name = node.get('name');
		if (node.get('query')) {
			if (/^RS\.\w+\.\w+/.test(node.get('query')) || Ext.form.field.VTypes.url(node.get('query')))
				node0.query = node.get('query');
			else if (!/^\//.test(node.get('query')))
				node0.query = 'RS.wiki.Main/name=' + node.get('query');
			else node0.query = node.get('query');
		}
		// node0.isGroup = node.get('isGroup');
		node0.active = node.get('active');
		node0.roles = node.get('roles');
		node0.openAs = node.get('openAs');
		node0.tooltip = node.get('tooltip');
		node0.leaf = node.isLeaf();
		node0.firstLayer = parent && parent.get('id') == 'root';
		if (node.isLeaf())
			return node0;
		node0.query = '';
		node0.children = [];
		node0.expanded = true;
		Ext.each(node.childNodes, function(n) {
			node0.children.push(this.convertNode(n, node));
		}, this);
		return node0;
	},

	checkDrop: function(data, overModel, dropPosition, dropHandlers) {
		var group = overModel.parentNode.get('group'),
			records = data.records,
			allow = true;
		if (dropPosition === 'append')
			group = overModel.get('group');
		Ext.Array.forEach(records || [], function(r, idx) {
			if (!r.isLeaf() && overModel.parentNode.get('group') != '') {
				dropHandlers.cancelDrop();
				allow = false;
				return false;
			}
		}, this)

		if (!allow)
			return;
		Ext.each(records, function(r) {
			if (r.get('name'))
				r.set('group', group);
		});
	},

	reset: function() {
		this.message({
			title: this.localize('ResetTitle'),
			msg: this.localize('ResetConfirmation'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('yes'),
				no: this.localize('no')
			},
			scope: this,
			fn: function(btn) {
				if (btn == 'no')
					return;
				var defaults = clientVM.getDefaultToolbar();
				var root = {
					children: []
				};
				Ext.each(defaults, function(i) {
					root.children.push(this.itemToConfig(i));
				}, this);
				this.toolbarConfigurations.setRootNode(root);
			}
		});

	},
	resetIsEnabled$: function() {
		return !this.wait
	},

	itemToConfig: function(item) {
		var query = item.modelName
		if (query && item.params)
			query += '/' + Ext.Object.toQueryString(item.params)
		query = query || item.location
		var conf = {
			name: item.text || item.tooltip,
			query: query,
			active: !item.hidden,
			roles: '',
			openAs: 'normal',
			leaf: true
		};

		if (item.target == '_blank')
			conf.openAs = 'tab';
		if (item.action == 'WINDOW')
			conf.openAs = 'window';
		if (item.menu) {
			conf.children = [];
			conf.leaf = false;
			conf.expanded = true;
			// conf.isGroup = true;
			Ext.each(item.menu.items, function(i) {
				conf.children.push(this.itemToConfig(i))
			}, this)
		}
		return conf;
	},

	refresh: function() {
		this.loadConfig();
	},

	addToolbarItem: function() {
		if (this.toolbarConfigurationsSelections.length == 1 && !this.toolbarConfigurationsSelections[0].isLeaf()) {
			this.toolbarConfigurationsSelections[0].appendChild({
				name: this.localize('newItem'),
				leaf: true,
				openAs: 'normal',
				active: true
			});

			return;
		}
		this.toolbarConfigurations.getRootNode().appendChild({
			name: this.localize('newItem'),
			leaf: true,
			openAs: 'normal',
			active: true
		})
	},

	addToolbarItemIsEnabled$: function() {
		return !this.wait;
	},

	addGroup: function() {
		this.toolbarConfigurations.getRootNode().appendChild({
			name: this.localize('newGroup'),
			active: true,
			openAs: 'normal',
			expanded: true
				// isGroup: true
		})
	},

	addGroupIsEnabled$: function() {
		return !this.wait;
	},

	removeToolbarItem: function() {
		Ext.Array.forEach(this.toolbarConfigurationsSelections, function(c) {
			c.remove();
		}, this)
	},

	removeToolbarItemIsEnabled$: function() {
		return this.toolbarConfigurationsSelections.length > 0 && !this.wait;
	}
})
glu.defView('RS.sysadmin.ApplicationRight', {
	xtype: 'form',
	padding: 15,	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls : 'rs-dockedtoolbar',
		defaultButtonUI: 'display-toolbar-button',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{applicationRightTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name : 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'
		},{
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],

	items: [{
		style : 'border-top:1px solid silver',
		padding : '8 0 4 0',
		items : [{
			name: 'uappName',
			xtype: 'textfield',
			width : 500
		}]	
	}, /*{ //DOMAINS SECTION REMOVED
		title: '~~organizations~~',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		flex: 1,
		name: 'organizations',
		columns: '@{organizationsColumns}',
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items : ['addOrganization', 'removeOrganization']
		}]		
	}, */{
		title: '~~roles~~',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		flex: 1,
		name: 'roles',
		columns: '@{rolesColumns}',
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items : ['addRole', 'removeRole']
		}]	
	}, {
		title: '~~controllers~~',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		flex: 1,
		name: 'controllers',
		columns: '@{controllersColumns}',
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items : ['addController', 'removeController']
		}]	
	}]
})

glu.defView('RS.sysadmin.ApplicationRights', {
	padding: 15,
	xtype: 'grid',
	cls : 'rs-grid-dark',
	name: 'applicationRights',
	stateId: '@{stateId}',
	displayName: '@{displayName}',
	stateful: true,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',	
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		childLink: 'RS.sysadmin.ApplicationRight',
		childLinkIdProperty: 'id'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['createApplicationRight', 'deleteApplicationRight']
	}],
	columns: '@{columns}',
	viewConfig: {
		enableTextSelection: true
	},
	listeners: {
		editAction: '@{editApplicationRight}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
})
glu.defView('RS.sysadmin.BusinessRule', {
	padding: 15,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls : 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{businessRuleTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'			
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '-',{
			name : 'ruleTab',
			cls : 'rs-small-btn rs-toggle-btn'
		},{
			name : 'scriptTab',
			cls : 'rs-small-btn rs-toggle-btn'
		},'->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	layout : 'card',
	activeItem : '@{activeItem}',
	items: [{
		style : 'border-top:1px solid silver',
		padding : '8 0',		
		layout: 'hbox',
		items: [{
			xtype: 'panel',
			margin : {
				right : 15
			},
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {				
				margin : '4 0',
				labelWidth : 120
			},
			flex: 1,
			items: [{
				xtype: 'textfield',
				fieldLabel: '~~Name~~',
				name: 'uname'
			}, {
				xtype: 'textfield',
				fieldLabel: '~~Order~~',
				name: 'uorder'
			}, {
				xtype: 'panel',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaults : {
					hideLabel : true,
					margin : '0 0 0 125'
				},
				items: [{
					xtype: 'checkbox',
					boxLabel: '~~Active~~',
					name: 'uactive',
					flex: 1
				}, {
					xtype: 'checkbox',
					boxLabel: '~~NonBlocking~~',
					name: 'uasync',
					flex: 1
				}, {
					xtype: 'checkbox',
					boxLabel: '~~Before~~',
					name: 'ubefore',
					flex: 1
				}]
			}]
		}, {
			xtype: 'panel',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				margin : '4 0',
				labelWidth : 120
			},
			flex: 1,
			items: [{
				xtype: 'combo',
				editable: false,
				fieldLabel: '~~Table~~',
				store: '@{tableStore}',
				displayField: 'name',
				valueField: 'value',
				name: 'utable'
			}, {
				xtype: 'combo',
				editable: false,
				fieldLabel: '~~Types~~',
				multiSelect: true,
				store: '@{typeStore}',
				displayField: 'name',
				valueField: 'value',
				name: 'utype',
				flex: 1
			}]
		}]
	}, {
		xtype: 'AceEditor',
		style : 'border-top:1px solid silver',
		padding : '8 0',
		flex: 1,
		width: '100%',
		name: 'uscript',
		parser: 'groovy'		
	}]

});

glu.defView('RS.sysadmin.BusinessRules', {
	xtype: 'grid',
	cls : 'rs-grid-dark',
	padding: 15,
	stateId: '@{stateId}',
	stateful: true,
	layout: 'fit',
	displayName: '@{displayName}',
	name: 'businessRules',
	store: '@{store}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['createBusinessRule', 'deleteBusinessRules']
	}],
	columns: '@{columns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},

	listeners: {
		editAction: '@{editBusinessRule}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});
glu.defView('RS.sysadmin.ConfigAD', {
	xtype: 'form',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},	
	autoScroll: true,
	padding : 15,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls: 'rs-dockedtoolbar',
		defaultButtonUI: 'display-toolbar-button',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{title}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',		
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],

	//layout all the fields for the active directory
	items: [{
		xtype: 'container',
		style : 'border-top:1px solid silver',
		padding : '8 0',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},

		items: [{
			xtype: 'container',
			defaultType: 'textfield',
			flex: 1,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				labelWidth : 130,
				margin : '4 0'
			},			
			items: [
				'udomain', {
					name: 'belongsToOrganization',
					xtype: 'combo',
					editable: false,
					store: '@{organizations}',
					displayField: 'uorganizationName',
					valueField: 'sys_id'
				}, {
					xtype: 'combobox',
					editable: false,
					store: '@{authGatewayStore}',
					displayField: 'name',
					valueField: 'value',
					queryMode: 'local',
					name: 'ugateway'
				}, {
					xtype: 'container',
					layout: {
						type: 'hbox',
						align: 'stretch'
					},					
					defaultType: 'textfield',
					items: [{
						name : 'uipAddress',
						flex : 1,
						labelWidth : 130,
						margin : {
							right : 10
						}
					},{
						name : 'uport',
						width : 250,
						labelWidth : 50
					}]
				}, {
					name: 'ussl',
					xtype : 'checkbox',
					hideLabel: true,
					boxLabel: '~~ussl~~',
					margin: '0px 0px 0px 135px'
				}, {
					name: 'ufallback',
					xtype : 'checkbox',
					hideLabel: true,
					boxLabel: '~~ufallback~~',
					margin: '0px 0px 0px 135px'
				},{
					name: 'groupRequired',
					xtype : 'checkbox',
					hideLabel: true,
					boxLabel: '~~groupRequired~~',
					margin: '0px 0px 0px 135px'
				}
			]
		}, {
			margin : {
				left : 15
			},
			xtype: 'container',
			defaultType: 'textfield',
			flex: 1,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				labelWidth : 130,
				margin : '4 0'
			},
			items: [{
				xtype: 'container',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				defaults: {
					flex: 1
				},
				items: [{
					xtype: 'container',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					defaultType: 'textfield',
					defaults : {
						margin : '4 0',
						labelWidth : 130
					},
					items: ['umode', 'ubindDn', 'uuidAttribute']
				}, {
					xtype: 'container',
					margin : {
						left : 15
					},
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					defaultType: 'textfield',
					defaults : {
						margin : '4 0',
						labelWidth : 130
					},
					items: ['uversion', {
						name: 'ubindPassword',
						inputType: 'password'
					}]
				}]
			}]
		}]
	}, {
		xtype: 'container',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'grid',
			cls : 'rs-grid-dark',
			flex: 1,
			viewConfig: {
				markDirty: false
			},
			name: 'baseDN',
			title: '@{baseDNListTitle}',
			store: '@{baseDNList}',		
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar rs-dockedtoolbar',
				name: 'actionBar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items: ['addBaseDn', 'deleteBaseDn']
			}],
			selected: '@{selectedBaseDN}',
			columns: '@{baseDNColumns}',
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],
			margin : {
				right : 15
			}
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			flex: 1,
			viewConfig: {
				markDirty: false
			},
			name: 'properties',
			title: '@{propertiesTitle}',
			store: '@{properties}',		
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar rs-dockedtoolbar',
				name: 'actionBar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items: ['addProperties', 'deleteProperty']
			}],
			selected: '@{selectedProp}',
			columns: '@{propertiesColumns}',
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}]
		}]
	}]
});

glu.defView('RS.sysadmin.ConfigADs', {
	xtype: 'grid',
	cls : 'rs-grid-dark',
	stateId: '@{stateId}',
	stateful: true,
	padding: 15,
	layout: 'fit',
	displayName: '@{displayName}',
	name: 'configAD',
	store: '@{configADStore}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['createConfigAD', 'deleteConfigAD']
	}],
	columns: '@{columns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},

	listeners: {
		editAction: '@{editConfigAD}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});
glu.defView('RS.sysadmin.ConfigLDAP', {
	xtype: 'form',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	autoScroll: true,	
	padding : 15,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls: 'rs-dockedtoolbar',
		defaultButtonUI: 'display-toolbar-button',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{title}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'		
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{sys_id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],

	//layout all the fields for the active directory
	items: [{
		xtype: 'container',
		style : 'border-top:1px solid silver',
		padding : '8 0',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},

		items: [{
			xtype: 'container',
			defaultType: 'textfield',
			flex: 1,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				labelWidth : 130,
				margin : '4 0'
			},	
			items: [
				'udomain', {
					name: 'belongsToOrganization',
					xtype: 'combo',
					editable: false,
					store: '@{organizations}',
					displayField: 'uorganizationName',
					valueField: 'sys_id'
				}, {
					xtype: 'combobox',
					editable: false,
					store: '@{authGatewayStore}',
					displayField: 'name',
					valueField: 'value',
					queryMode: 'local',
					name: 'ugateway'
				},{
					xtype: 'container',
					layout: {
						type: 'hbox',
						align: 'stretch'
					},					
					defaultType: 'textfield',
					items: [{
						name : 'uipAddress',
						flex : 1,
						labelWidth : 130,
						margin : {
							right : 10
						}
					},{
						name : 'uport',
						width : 250,
						labelWidth : 50
					}]
				},{
					name: 'ussl',
					xtype : 'checkbox',
					hideLabel: true,
					boxLabel: '~~ussl~~',
					margin: '0px 0px 0px 135px'
				}, {
					name: 'ufallback',
					xtype : 'checkbox',
					hideLabel: true,
					boxLabel: '~~ufallback~~',
					margin: '0px 0px 0px 135px'
				},{
					name: 'groupRequired',
					xtype : 'checkbox',
					hideLabel: true,
					boxLabel: '~~groupRequired~~',
					margin: '0px 0px 0px 135px'
				}
			]
		}, {
			margin : {
				left : 15
			},
			xtype: 'container',
			defaultType: 'textfield',
			flex: 1,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				labelWidth : 130,
				margin : '4 0'
			},
			items: [{
				xtype: 'container',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				defaults: {
					flex: 1
				},
				items: [{
					xtype: 'container',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					defaultType: 'textfield',
					defaults : {
						margin : '4 0',
						labelWidth : 130
					},
					items: [{
						xtype: 'combobox',
						name: 'umode',
						editable: false,
						queryMode: 'local',
						displayField: 'type',
						valueField: 'type',
						store: '@{modeStore}'
					}, 'ubindDn', 'uuidAttribute', {
						xtype: 'combobox',
						name: 'ucryptType',
						editable: false,
						queryMode: 'local',
						displayField: 'type',
						valueField: 'type',
						store: '@{cryptTypeStore}'
					}]
				}, {
					xtype: 'container',
					margin : {
						left : 15
					},
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					defaultType: 'textfield',
					defaults : {
						margin : '4 0',
						labelWidth : 135
					},
					items: ['uversion', {
						name: 'ubindPassword',
						inputType: 'password'
					}, 'upasswordAttribute', 'ucryptPrefix']
				}]
			}]
		}]
	}, {
		xtype: 'container',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'grid',
			cls : 'rs-grid-dark',
			flex: 1,
			viewConfig: {
				markDirty: false
			},
			name: 'properties',
			title: '@{baseDNListTitle}',
			store: '@{baseDNList}',
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar rs-dockedtoolbar',
				name: 'actionBar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items: ['addBaseDn', 'deleteBaseDn']
			}],	
			selected: '@{selectedBaseDN}',
			columns: '@{baseDNColumns}',
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],
			margin : {
				right : 15
			}
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			flex: 1,
			viewConfig: {
				markDirty: false
			},
			name: 'properties',
			title: '@{propertiesTitle}',
			store: '@{properties}',		
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar rs-dockedtoolbar',
				name: 'actionBar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items:['addProperties', 'deleteProperty']
			}],			
			selected: '@{selectedProp}',
			columns: '@{propertiesColumns}',
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}]
		}]
	}]
});

glu.defView('RS.sysadmin.ConfigLDAPs', {
	xtype: 'grid',
	layout: 'fit',
	cls : 'rs-grid-dark',
	stateId: '@{stateId}',
	stateful: true,
	padding : 15,
	displayName: '@{displayName}',
	name: 'records',
	store: '@{store}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['createRecord', 'deleteRecords']
	}],
	columns: '@{columns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',	
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},

	listeners: {
		editAction: '@{editRecord}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});
glu.defView('RS.sysadmin.ConfigRADIUS', {
	xtype: 'form',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	padding : 15,
	//default configs
	autoScroll: true,
	//toolbar
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls: 'rs-dockedtoolbar',
		defaultButtonUI: 'display-toolbar-button',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{title}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'		
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{sys_id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],

	//layout all the fields for the RADIUS
	items: [{
		xtype: 'container',
		style : 'border-top:1px solid silver',
		padding : '8 0',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'container',
			defaultType: 'textfield',
			flex: 1,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				labelWidth: 130,
				margin : '4 0'
			},
			items: [
				'udomain', {
					name: 'belongsToOrganization',
					xtype: 'combo',
					editable: false,
					store: '@{organizations}',
					displayField: 'uorganizationName',
					valueField: 'sys_id'
				}, {
					xtype: 'combobox',
					editable: false,
					store: '@{authGatewayStore}',
					displayField: 'name',
					valueField: 'value',
					queryMode: 'local',
					name: 'ugateway'
				}, 'uprimaryHost', {
					xtype: 'container',
					defaultType: 'checkbox',
					layout: {
						type: 'hbox',
						align: 'stretch'
					},
					items: [{
						name: 'ufallback',
						hideLabel: true,
						boxLabel: '~~ufallback~~',
						padding: '0px 0px 0px 135px'
					}]
				}
			]
		}, {
			xtype: 'container',
			margin : {
				left : 15
			},
			defaultType: 'textfield',
			flex: 1,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				labelWidth: 180,
				margin : '4 0'
			},
			items: [{
				xtype: 'combobox',
				name: 'uauthProtocol',
				editable: false,
				queryMode: 'local',
				displayField: 'type',
				valueField: 'type',
				store: '@{authprotocolStore}',			
			},
			'uauthPort','uacctPort','usecondaryHost','usharedSecret']
		}]
	}]
});

glu.defView('RS.sysadmin.ConfigRADIUSs', {
	xtype: 'grid',
	layout: 'fit',
	cls : 'rs-grid-dark',
	padding : 15,
	stateId: '@{stateId}',
	stateful: true,
	displayName: '@{displayName}',
	name: 'records',
	store: '@{store}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['createRecord', 'deleteRecords']
	}],
	columns: '@{columns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',	
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},

	listeners: {
		editAction: '@{editRecord}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});
glu.defView('RS.sysadmin.ControllerPicker', {
	title: '~~addControllerTitle~~',
	modal: true,
	padding : 15,
	listeners: {
		beforeshow: function() {
			clientVM.setPopupSizeToEightyPercent(this);
		}
	},
	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'controllers',
		columns: '@{columns}',
		plugins: [{
			ptype: 'pager'
		}],
		selModel: {
			mode: 'MULTI'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			items: []
		}]
	}],
	buttons: [{
		name : 'add',
		cls : 'rs-small-btn rs-btn-light'
	},{ 
		name : 'cancel',
		cls : 'rs-small-btn rs-btn-light'
	}]
})
glu.defView('RS.sysadmin.JobSchedule', {
	xtype: 'form',
	layout : 'fit',
	padding: 15,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls : 'rs-dockedtoolbar',
		defaultButtonUI: 'display-toolbar-button',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{jobScheduleTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, {
			name: 'executeJobSchedule',
			tooltip: '~~executeJobScheduleTooltip~~'
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	defaults : {
		defaults : {
			labelWidth : 130,
			margin : '4 0'
		}
	},	
	items: [{
		xtype : 'panel',
		style : 'border-top:1px solid silver',
		padding : '8 0',
		defaultType: 'textfield',
		layout : {
			type : 'vbox',
			align : 'stretch'
		},
		items : ['uname',
		{
			xtype: 'displayfield',
			cls : 'rs-displayfield-value',
			name: 'unameDisplay',
			htmlEncode : true
		}, 'umodule', 'urunbook', {
			xtype: 'checkbox',
			name: 'uactive',
			boxLabel : '~~uactive~~',
			hideLabel : true,
			margin : '0 0 0 135'
		}]
	},{
		xtype: 'panel',	
		bodyPadding : '8 0 0 0',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		title: '~~schedule~~',
		items: [{
			xtype: 'textfield',
			name: 'seconds',
			tooltip: '~~secondsTooltip~~',
			keyDelay: 1,
			listeners: {
				render: function(field) {
					Ext.tip.QuickTipManager.register({
						target: field.getEl().dom.id,
						text: field.tooltip
					})
				}
			}
		}, {
			xtype: 'textfield',
			name: 'minutes',
			tooltip: '~~minutesTooltip~~',
			keyDelay: 1,
			listeners: {
				render: function(field) {
					Ext.tip.QuickTipManager.register({
						target: field.getEl().dom.id,
						text: field.tooltip
					})
				}
			}
		}, {
			xtype: 'textfield',
			name: 'hours',
			tooltip: '~~hoursTooltip~~',
			keyDelay: 1,
			listeners: {
				render: function(field) {
					Ext.tip.QuickTipManager.register({
						target: field.getEl().dom.id,
						text: field.tooltip
					})
				}
			}
		}, {
			xtype: 'textfield',
			name: 'month',
			tooltip: '~~monthTooltip~~',
			keyDelay: 1,
			listeners: {
				render: function(field) {
					Ext.tip.QuickTipManager.register({
						target: field.getEl().dom.id,
						text: field.tooltip
					})
				}
			}
		}, {
			xtype: 'fieldcontainer',
			fieldLabel: '~~dayType~~',
			layout: 'hbox',
			items: [{
				xtype: 'radio',
				name: 'dayType',
				boxLabel: '~~dayOfMonthRadio~~',
				value: '@{dayOfMonthRadio}'
			}, {
				xtype: 'radio',
				name: 'dayType',
				boxLabel: '~~dayOfWeekRadio~~',
				value: '@{dayOfWeekRadio}',
				padding: '0px 0px 0px 10px'
			}]
		}, {
			xtype: 'textfield',
			name: 'dayOfMonth',
			tooltip: '~~dayOfMonthTooltip~~',
			keyDelay: 1,
			listeners: {
				render: function(field) {
					Ext.tip.QuickTipManager.register({
						target: field.getEl().dom.id,
						text: field.tooltip
					})
				}
			}
		}, {
			xtype: 'textfield',
			name: 'dayOfWeek',
			tooltip: '~~dayOfWeekTooltip~~',
			keyDelay: 1,
			listeners: {
				render: function(field) {
					Ext.tip.QuickTipManager.register({
						target: field.getEl().dom.id,
						text: field.tooltip
					})
				}
			}
		}]
	}, {
		xtype: 'panel',	
		bodyPadding : '8 0 0 0'	,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		title: '~~timeRange~~',
		items: [{
			xtype: 'xdatetimefield',
			fieldLabel: '~~startTime~~',
			format: 'c',
			name: 'ustartTime'
		}, {
			xtype: 'xdatetimefield',
			fieldLabel: '~~endTime~~',
			name: 'uendTime'
		}]
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		flex : 1,
		name: 'parameters',
		store: '@{parametersStore}',
		title: '~~parameters~~',
		columns: '@{parametersColumns}',
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 2
		}],
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items : ['addParam', 'removeParam']
		}],	
		listeners: {
			render: function(grid) {
				grid.store.grid = grid;
			}
		}
	}]
})

glu.defView('RS.sysadmin.JobSchedules', {
	padding: 15,
	xtype: 'grid',
	cls : 'rs-grid-dark',
	name: 'jobSchedules',
	border: false,
	stateId: '@{stateId}',
	displayName: '@{displayName}',
	stateful: true,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		childLink: 'RS.sysadmin.JobSchedule',
		childLinkIdProperty: 'id'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		name: 'actionBar',
		items: ['createJobSchedule', 'activateJobSchedule', 'deactivateJobSchedule', 'deleteJobSchedule', {
			name: 'executeJobSchedule',
			tooltip: '~~executeJobScheduleTooltip~~'
		}]
	}],
	columns: '@{columns}',
	viewConfig: {
		enableTextSelection: true
	},
	listeners: {
		editAction: '@{editJobSchedule}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
})
glu.defView('RS.sysadmin.ListRegistration', {
	padding: 15,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls : 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{title}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',			
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',
			cls : 'rs-small-btn rs-btn-light'	
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],	
	items: [{		
		style : 'border-top:1px solid silver',		
		padding : '5 0',
		defaults : {
			margin : '4 0'
		},
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaultType: 'displayfield',
		items : ['uname', 'uguid', 'utype', 'ustatus', 'uipaddress', 'uversion', {
			xtype: 'textarea',
			name: 'uconfig',
			height: 200,
			overflowY : 'scroll',
			readOnly: true
		}]
	},{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'properties',
		title: '@{displayName}',
		columns: '@{columns}',
		autoScroll : true,
		flex : 1,
		viewConfig: {
			enableTextSelection: true
		}
	}]
});
glu.defView('RS.sysadmin.ListRegistrations', {
	padding: 15,
	xtype: 'grid',
	cls : 'rs-grid-dark',
	name: 'listRegistrations',
	border: false,
	stateId: '@{stateId}',
	displayName: '@{displayName}',
	stateful: true,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		childLink: 'RS.sysadmin.ListRegistration',
		childLinkIdProperty: 'id'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults :{
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['deleteListRegistration']
	}],
	columns: '@{columns}',
	viewConfig: {
		enableTextSelection: true
	},
	listeners: {
		editAction: '@{editListRegistration}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
})
glu.defView('RS.sysadmin.MenuDefinition', {
	padding: 15,	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls: 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{menuDefinitionTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'		
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		xtype: 'panel',
		style : 'border-top:1px solid silver',
		padding : '8 0',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},		
		items: [{
			xtype: 'textfield',
			fieldLabel: '~~name~~',
			name: 'title',
			width: '50%',
		}, {
			xtype: 'checkbox',
			padding: '0 0 0 50',
			hideLabel: true,
			name: 'active',		
			boxLabel: '~~active~~'
		} /*,{		
			padding: '0 0 0 50',
			xtype: 'toolbar',		
			items: [{
				xtype: 'label',
				text: '~~backgroundColor~~'
			}, {
				xtype: 'button',
				text: '@{bgStyle}',
				padding: '0 0 0 5',
				menu: {
					id: 'bgColor',
					xtype: 'colormenu',
					handler: '@{selectBgColor}'					
				}
			}, {
				xtype: 'label',
				text: '~~fontColor~~',
				margin : '0 0 0 50'
			}, {
				xtype: 'button',
				text: '@{txtStyle}',
				padding: '0 0 0 5',
				menu: {
					id: 'txtColor',
					xtype: 'colormenu',
					handler: '@{selectTxtColor}'
				}
			}]
		}, {
			xtype: 'displayfield',
			value: '@{color}',
			hidden: true,
			listeners: {
				change: function() {
					if (!this.value)
						return;
					this.ownerCt.down('#bgColor').items.get(0).select(this.value);
				}
			}
		}, {
			xtype: 'displayfield',
			value: '@{textColor}',
			hidden: true,
			listeners: {
				change: function() {
					if (!this.value)
						return;
					this.ownerCt.down('#txtColor').items.get(0).select(this.value);
				}
			}
		}*/]
	}, {
		xtype: 'treepanel',
		title: '~~menuItems~~',
		cls : 'rs-grid-dark',
		flex: 1,
		name: 'menuItems',
		useArrows: true,
		store: '@{menuItems}',
		split: true,
		columns: '@{menuItemsColumns}',
		rootVisible: false,
		displayField: 'group',
		selModel: {
			selType: 'resolvecheckboxmodel',	
			glyph : 0xF044,
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id'
		},
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		dockedItems: [{
			xtype : 'toolbar',
			cls: 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light',
			},
			items : ['addMenuItem', 'addGroup', 'removeMenuItem']
		}],
		viewConfig: {
			markDirty: false,
			plugins: {
				ptype: 'treeviewdragdrop'
			},
			listeners: {
				beforedrop: function(node, data, overModel, dropPosition, dropHandlers) {
					this.ownerCt.fireEvent('beforeDropRec', node, data, overModel, dropPosition, dropHandlers);
				}
			}
		},
		listeners: {
			beforeDropRec: '@{checkDrop}',
			editAction: '@{editMenuItem}',
			beforecellclick: function(treeview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
				if (!record.isLeaf()) {
					if (treeview.ownerCt.columns[cellIndex + 1].dataIndex != 'query') return false //cell index + 1 because of the tree column
				}
			}
		}	
	}, {
		title: '~~roles~~',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		flex: 1,
		name: 'roles',
		columns: '@{rolesColumns}',
		dockedItems: [{
			xtype : 'toolbar',
			cls: 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light',
			},
			items : ['addRole', 'removeRole']
		}]
	}]
})

glu.defView('RS.sysadmin.MenuDefinitions', {
	padding: 15,
	xtype: 'grid',
	cls : 'rs-grid-dark',
	name: 'menuDefinitions',
	border: false,
	displayName: '@{displayName}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager',
		pageable: false
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.gotoDetailsColumnTooltip,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		childLink: 'RS.sysadmin.MenuDefinition',
		childLinkIdProperty: 'id'
	},
	dockedItems: [		
		{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: ['createMenuDefinition', 'deleteMenuDefinition']
		},{
			xtype : 'displayfield',
			dock : 'bottom',
			margin : '5 0 0 0',
			hidden : true,
			value : '~~draganddroptip~~'
		}
	],
	columns: '@{columns}',
	viewConfig: {
		plugins: [{
			ptype: 'gridviewdragdrop',
			dragText: 'Drag and drop to reorganize'
		}],
		listeners: {
			drop: function(node, data, overModel, dropPosition, eOpts) {
				this.ownerCt.fireEvent('definitionDrop', this)
			}
		}
	},

	listeners: {
		render: function() {
			var filter = null;
			var dd = this.getView().plugins[0];

			Ext.each(this.plugins, function(p) {
				if (p.ptype == 'searchfilter')
					filter = p;
				if (p.ptype == 'gridviewdragdrop')
					dd = p;
			});

			this.getStore().on("load", function() {
				if (filter.filterBar.getFilters().length > 0)
					dd.disable();
				else
					dd.enable();
			}, this);
		},
		definitionDrop: '@{definitionDrop}',
		editAction: '@{editMenuDefinition}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
})
glu.defView('RS.sysadmin.MenuItem', {
	title: '~~menuItem~~',
	cls : 'rs-modal-popup',
	width: 600,
	height: 400,
	modal: true,
	padding : 15,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults : {
		labelWidth : 120,
		margin : '4 0'
	},
	items: [{
		xtype: 'textfield',
		fieldLabel: '~~name~~',
		name: 'title'
	},{
		xtype: 'textfield',
		fieldLabel: '~~query~~',
		name: 'query'
	},{
		xtype: 'checkbox',
		boxLabel: '~~active~~',
		margin: '0 0 0 125',
		hideLabel : true,
		name: 'active'
	},{
		title: '~~roles~~',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'roles',
		columns: '@{rolesColumns}',
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items : ['addRole', 'removeRole']
		}],	
		flex: 1,
		margin : '0 0 10 0'
	}],
	buttons: [{
		name :'addMenuItem', 
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'applyMenuItem',
			cls : 'rs-med-btn rs-btn-dark'
	},{ 
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]	
});
glu.defView('RS.sysadmin.MenuPicker', {
	title: '~~addMenuTitle~~',
	width: 600,
	height: 400,
	padding : 15,
	modal : true,
	cls : 'rs-modal-popup',
	layout: 'fit',
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'menus',
		columns: '@{columns}',
		plugins: [{
			ptype: 'pager'
		}],
		selModel: {
			mode: 'MULTI'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: []
		}],
		margin : {
			bottom : 8
		}
	}],
	buttons: [{
		name : 'add',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})
glu.defView('RS.sysadmin.MenuSet', {
	autoScroll: true,
	padding: 15,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{MenuSetTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'			
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		xtype: 'panel',
		style : 'border-top:1px solid silver',
		padding : '8 0',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'textfield',
			fieldLabel: '~~name~~',
			name: 'name',
			width: '50%'
		}, {
			xtype: 'checkbox',
			padding: '0px 0px 0px 50px',
			hideLabel: true,
			name: 'active',
			hideLabel: true,
			boxLabel: '~~active~~'
		}]
	}, {
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			title: '~~roles~~',
			xtype: 'grid',
			dockedItems: [{
				xtype: 'toolbar',	
				cls: 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},			
				items: ['addRole', 'removeRole']
			}],
			flex: 1,
			cls : 'rs-grid-dark',
			name: 'roles',
			columns: '@{rolesColumns}'		
		}, {
			padding: '0px 0px 0px 20px',
			dockedItems: [{
				xtype: 'toolbar',	
				cls: 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},			
				items: ['addMenu', 'removeMenu']
			}],
			title: '~~menus~~',
			xtype: 'grid',
			cls : 'rs-grid-dark',
			flex: 1,
			name: 'menus',
			columns: '@{menusColumns}'		
		}]
	}]
});

glu.defView('RS.sysadmin.MenuSets', {
	padding: 15,
	xtype: 'grid',
	cls : 'rs-grid-dark',
	name: 'menuSets',
	border: false,
	stateId: '@{stateId}',
	displayName: '@{displayName}',
	stateful: true,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager',
		pageable: false
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',		
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		childLink: 'RS.sysadmin.MenuSet',
		childLinkIdProperty: 'id'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['createMenuSet', 'deleteMenuSet']
	},{
		xtype : 'displayfield',
		margin : '5 0 0 0',
		dock : 'bottom',
		hidden : true,
		value : '~~draganddroptip~~'
	}],
	columns: '@{columns}',
	viewConfig: {
		enableTextSelection: true,
		plugins: {
			ptype: 'gridviewdragdrop',
			dragText: 'Drag and drop to reorganize'
		},
		listeners: {
			drop: function(node, data, dropRec, dropPosition, dropHandlers) {
				this.ownerCt.fireEvent('dropRec', node, data, dropRec, dropPosition, dropHandlers);
			}
		}
	},
	listeners: {
		editAction: '@{editMenuSet}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}',
		dropRec: '@{arrange}'
	}
})
glu.defView('RS.sysadmin.Organization', {
	xtype: 'form',
	autoScroll: true,
	bodyPadding: '10px',
	buttonAlign: 'left',
	items: [{
		xtype: 'textfield',
		name: 'uorganizationName'
	}, {
		xtype: 'textarea',
		name: 'udescription'
	}],

	buttons: ['save', 'cancel'],
	asWindow: {
		width: 500,
		height: 400,
		title: '@{title}'
	}
});
glu.defView('RS.sysadmin.OrganizationPicker', {
	title: '~~addOrganizationTitle~~',
	width: 600,
	height: 400,
	cls : 'rs-modal-popup',
	layout: 'fit',
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'organizations',
		columns: '@{columns}',
		plugins: [{
			ptype: 'pager'
		}],
		selModel: {
			mode: 'MULTI'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: []
		}]
	}],
	buttons: [{
		name : 'add',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})
glu.defView('RS.sysadmin.Organizations', {
	padding: '10px',
	xtype: 'grid',
	displayName: '@{displayName}',
	stateful: true,
	stateId: '@{stateId}',
	name: 'organizations',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar',
		name: 'actionBar',
		items: ['createOrganization', 'enableOrganization', 'disableOrganization']
	}],
	columns: '@{columns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',	
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		childLinkIdProperty: 'id'
	},
	listeners: {
		editAction: '@{editOrganization}'
	}
});
glu.defView('RS.sysadmin.RolePicker', {	
	title: '@{title}',
	padding : 15,
	modal: true,
	cls : 'rs-modal-popup',
	width: 800,
	height: 450,
	layout: 'fit',
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'roles',
		columns: '@{columns}',
		plugins: [{
			ptype: 'pager'
		}],
		selModel: {
			mode: 'MULTI'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'keyword',
				width : 400
			}]
		}],
		margin : {
			bottom : 10
		}
	}],	
	buttons: [{
		name :'add', 
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'remove',
			cls : 'rs-med-btn rs-btn-dark'
	},{ 
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})
glu.defView('RS.sysadmin.ServerInformation', {
	autoScroll: true,
	padding: '10px',
	bodyPadding: '10px',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls: 'actionBar actionBar-form',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '~~systemInformationTitle~~'
		}, '->', {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			flex: 1,
			layout: {
				type: 'vbox',
				align: 'stretch',
				padding: '0px 30px 0px 0px'
			},
			items: [{
				title: '~~version~~',
				bodyPadding: '10px',
				defaultType: 'displayfield',
				defaults: {
					labelWidth: 150
				},
				items: ['version', 'build']
			}, {
				bodyPadding: '10px',
				defaultType: 'displayfield',
				defaults: {
					labelWidth: 150
				},
				title: '~~memory~~',
				items: ['memoryMax',
					'memoryTotal',
					'memoryFreePercentage'
				]
			}, {
				bodyPadding: '10px',
				defaultType: 'displayfield',
				defaults: {
					labelWidth: 150
				},
				title: '~~thread~~',
				items: ['threadPeak',
					'threadLive',
					'threadDeamon'
				]
			}]
		}, {
			flex: 1,
			bodyPadding: '10px',
			defaultType: 'displayfield',
			defaults: {
				labelWidth: 150
			},
			title: '~~server~~',
			items: ['osArchitecture',
				'osName',
				'osVersion',
				'address',
				'osProcessor',
				'jvmVendor',
				'jvmName',
				'jvmVersion'
			]
		}]
	}]
})
glu.defView('RS.sysadmin.SessionInformation', {
	xtype: 'form',
	padding: 15,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar actionBar-form rs-dockedtoolbar',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '~~userInfoTitle~~'
		}, '->', {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	items: [{
		title: '~~sessionSummary~~',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		defaults: {
			flex: 1
		},
		defaultType: 'container',
		items: [{
			defaultType: 'displayfield',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			padding: '10px 0px 0px 0px',
			defaults: {
				labelWidth: 180
			},
			items: ['activeSessions', 'maxActiveSessions']
		}, {
			defaultType: 'displayfield',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				labelWidth: 180
			},
			padding: '10px 0px 0px 0px',
			items: ['maxSessions', 'timeout']
		}]
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		flex: 1,
		viewConfig: {
			markDirty: false
		},
		autoScroll : true,
		name: 'sessions',
		title: '~~sessionDetails~~',
		store: '@{sessionStore}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			items: [{
				name : 'removeSessions',
				cls : 'rs-small-btn rs-btn-light'
			}]
		}],
		columns: '@{sessionColumns}',
		selModel: {
			selType: 'checkboxmodel',
			mode: 'MULTI'
		}
	}]
});
glu.defView('RS.sysadmin.SystemProperties', {
	xtype: 'grid',
	layout: 'fit',
	cls : 'rs-grid-dark',
	displayName: '@{displayName}',
	padding: 15,
	stateId: '@{stateId}',
	stateful: true,
	name: 'properties',
	store: '@{propertiesStore}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		name: 'actionBar',
		items: ['createProperty', 'deleteProperties']
	}],
	columns: '@{columns}',
	features: [{
		ftype: 'grouping',
		id: 'group'
	}],
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true,
		clobCheckURL : '/resolve/service/sysproperties/getClobColumnNames'
	}, {
		ptype: 'columnautowidth'
	}, {
		ptype: 'pager',
		pageable: false
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.editColumnTooltip,
		glyph : 0xF044,	
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		toggleUiHeader: function(anyCollapsed) {
			var view = this.views[0],
				headerCt = view.headerCt,
				checkHd = headerCt.child('gridcolumn[isCheckerHd]');
			checkHd[anyCollapsed ? 'removeCls' : 'addCls']('x-grid-group-hd-collapsed');
		},
		getHeaderConfig: function() {
			var me = this;

			return {
				xtype: 'viewcolumn',
				isCheckerHd: true,
				text: '<div class="x-grid-group-title">&#160;</div>',
				width: me.headerWidth,
				sortable: false,
				glyph : me.glyph,
				draggable: false,
				resizable: false,
				hideable: false,
				menuDisabled: true,
				dataIndex: '',
				viewTooltip: me.columnTooltip,
				target: me.columnTarget,
				eventName: me.columnEventName,
				idField: me.columnIdField || 'sys_id',
				// cls: showCheck ? Ext.baseCSSPrefix + 'column-header-checkbox ' : '',
				cls: 'x-grid-group-hd-collapsible',
				editRenderer: me.editRenderer || me.renderEmpty,
				locked: me.hasLockedHeader(),
				childLink: me.childLink,
				childLinkIdProperty: me.childLinkIdProperty,
				listeners: {
					render: function(column, eOpts) {
						var header = column.getEl(),
							hoverCls = Ext.baseCSSPrefix + 'column-header-over';
						header.on('mouseover', function() {
							column.titleEl.addCls(hoverCls);
						});
						header.on('mouseout', function() {
							column.titleEl.removeCls(hoverCls);
						});
					}
				}
			};
		},

		onHeaderClick: function(headerCt, header, e) {
			if (header.dataIndex)
				return;
			var group = this.view.getFeature('group');
			var cache = group.groupCache;
			var anyCollapsed = false;
			for (name in cache) {
				if (cache.hasOwnProperty(name)) {
					anyCollapsed = anyCollapsed || cache[name].isCollapsed
				}
				if (anyCollapsed)
					break;
			}
			group[anyCollapsed ? 'expandAll' : 'collapseAll']();
			this.toggleUiHeader(anyCollapsed);
		}
	},

	listeners: {
		editAction: '@{editProperty}',
		render: function(grid) {
			var store = grid.getStore();
			store.on('load', function() {
				var g = grid;
			});
		}
	}
});
glu.defView('RS.sysadmin.SystemProperty', {
	xtype: 'form',
	autoScroll: true,
	width: 600,
	modal:true,
	title: '@{title}',
	cls : 'rs-modal-popup',
	padding : 15,
	layout:{
		type:'vbox',
		align:'stretch'
	},
	defaults : {
		margin : '4 0'
	},
	items: [{
		xtype: 'textfield',
		fieldLabel: '~~uname~~',
		name: 'uname',
		maxLength: 300,
		readOnly: '@{editable}'
	}, {
		xtype: 'textarea',
		fieldLabel: '~~udescription~~',
		name: 'udescription',
		height: 200,
		maxLength: 4000,
		readOnly: '@{editable}'
	}, {
		xtype: 'textfield',
		fieldLabel: '~~uvalue~~',
		name: 'uvalue',
		readOnly: '@{editable}',
		margin : '4 0 8 0'
	}],

	buttons: [{
		name: 'save',
		cls : 'rs-med-btn rs-btn-dark',
		listeners: {
			doubleClickHandler: '@{saveAndExit}',
			render: function(button) {
				button.getEl().on('dblclick', function() {
					button.fireEvent('doubleClickHandler')
				});
			}
		}
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	},{
		name: 'toParent',
		cls : 'rs-med-btn rs-btn-light'
	}]	
});

glu.defView('RS.sysadmin.SystemScript', {
	padding: 15,
	xtype : 'panel',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls : 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{sysScriptTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'		
		},'save','execute', '-',
		{
			name : 'propertiesTab',
			cls : 'rs-small-btn rs-toggle-btn'
		},{
			name : 'sourceTab',
			cls : 'rs-small-btn rs-toggle-btn'
		},'->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	layout: {
		type: 'card',
		align: 'stretch',	
	},
	activeItem : '@{activeItem}',
	items: [{
		style : 'border-top:1px solid silver',
		padding : '10 0',
		xtype: 'panel',	
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults : {
			margin : '4 0'
		},
		items : [
		{
			xtype: 'textfield',
			fieldLabel: '~~Name~~',
			name: 'uname'
		},{
			xtype: 'textarea',
			fieldName: '~~Description~~',
			name: 'udescription',
			minHeight: 200,
		}]
	},{
		style : 'border-top:1px solid silver',
		padding : '10 0',	
		xtype: 'AceEditor',
		flex: 1,
		width: '100%',
		name: 'uscript',
		parser: 'groovy'
	}]
});

glu.defView('RS.sysadmin.SystemScripts', {
	xtype: 'grid',
	stateId: '@{stateId}',
	stateful: true,
	padding: 15,
	cls :'rs-grid-dark',
	layout: 'fit',
	displayName: '@{displayName}',
	name: 'scripts',
	store: '@{store}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls :'rs-small-btn rs-btn-light'
		},
		items: ['createScript', 'deleteScripts']
	}],
	columns: '@{columns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},

	listeners: {
		editAction: '@{editScript}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});
glu.defView('RS.sysadmin.ToolbarConfiguration', {
	padding: 15,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls: 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{toolbarConfTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'save',
			listeners: {
				render: function(button) {
					clientVM.updateSaveButtons(button);
				}
			}
		}, 'addToolbarItem', 'addGroup', {
			name: 'addRoles',
			handler: '@{addRoles}'
		}, 'removeRoles', 'removeToolbarItem', 'reset', '->', {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	xtype: 'treepanel',
	cls : 'rs-grid-dark',
	autoScroll: true,
	name: 'toolbarConfigurations',
	useArrows: true,
	store: '@{toolbarConfigurations}',
	columns: '@{columns}',
	rootVisible: false,
	displayField: 'group',
	selModel: {
		selType: 'checkboxmodel',
		checkOnly: true,
		mode: 'SIMPLE'
	},
	plugins: [{
		ptype: 'cellediting',
		clicksToEdit: 1
	}],
	viewConfig: {
		markDirty: false,
		plugins: {
			ptype: 'treeviewdragdrop'
		},
		listeners: {
			beforedrop: function(node, data, overModel, dropPosition, dropHandlers) {
				this.ownerCt.fireEvent('beforeDropRec', node, data, overModel, dropPosition, dropHandlers);
			}
		}
	},
	listeners: {
		beforeDropRec: '@{checkDrop}',
		beforeEdit: function(editor, e, eOpts) {
			if (!e.record.isLeaf() && e.column.dataIndex == 'query') return false
		}
	}
})

/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.sysadmin').locale = {
	windowTitle: 'SysAdmin Project',
	uname: 'Name',
	name: 'Name',
	value: 'Value',
	uvalue: 'Value',
	sysId: 'Sys ID',

	ServerErrMsg: 'Server Error.',
	DeleteErrMsg: 'Cannot delete the selected records.',
	DeleteSucMsg: 'The selected records have been deleted.',
	save: 'Save',
	cancel: 'Cancel',
	back: 'Back',
	udescription: 'Description',
	sysCreatedOn: 'Created On',
	sysCreatedBy: 'Created By',
	sysUpdatedOn: 'Updated On',
	sysUpdatedBy: 'Updated By',
	invalidNameErr: 'The name should only contain characters,digits,dot and underscore and the length should be between 1 and 300',
	invalidDescriptionErr: 'The maximum length of the description is 4000',
	invalidValueErr: 'The maximum length of the value is 300',

	//grid headers - Domains
	sysOrganizationName: 'Domain',
	description: 'Description',
	disable: 'Active',
	//grid headers - configADs
	domainName: 'Domain Name',
	ipAddress: 'IP Address',
	uidAttribute: 'ID Attribute',
	mode: 'Mode',
	bindDn: 'Bind DN',
	baseDNList: 'Base DN List',
	gateway: 'Gateway',
	active: 'Active',
	isDefault: 'Default',
	ssl: 'SSL',
	fallback: 'Fallback',
	groupRequired : 'Group Required',
	version: 'Version',
	port: 'Port',
	// organizationName: 'Domain',

	roles: 'Roles',

	//Business Rules
	Name: 'Name',
	Table: 'Table',
	NonBlocking: 'Non-Blocking',
	Active: 'Active',
	Before: 'Before',
	Type: 'Type',
	Order: 'Order',
	Types: 'Types',
	CreatedOn: 'Created On',
	CreatedBy: 'Created By',
	UpdatedOn: 'Updated On',
	UpdatedBy: 'Updated By',
	script: 'Script',

	//System Scripts
	Description: 'Description',
	LastUpdated: 'LastUpdated',

	//ListRegistration
	ustatus: 'Status',
	uguid: 'Guid',
	utype: 'Type',
	uipaddress: 'Reg Address',
	uversion: 'Version',
	uconfig: 'Config',

	//menu sets
	applications: 'Applications',

	//system information
	user: 'User',
	host: 'Host',
	duration: 'Duration',

	//organizations
	organizationName: 'Name',

	//job schedules
	runbook: 'Runbook',
	expression: 'Expression',
	module: 'Module',

	active: 'Active',
	group: 'Group',
	query: 'Query',
	queryOrUrl: 'Query or Fully Qualified URL',
	queryOrUrlTooltip: 'Wiki Name or URL link, i.e. http://www.resolvesys.com',
	//toolbar config
	openAs: 'Open As',
	tooltip: 'Tooltip',

	SystemProperties: {
		NoSelectionErr: 'No item is selected.',
		createProperty: 'New',
		editProperty: 'Edit',
		deleteProperties: 'Delete',
		help: 'Help',
		systemPropertiesTitle: 'System Properties',
		systemProperties: 'System Properties',
		deleteTitle: 'Delete System Properties',
		deletesMessage: 'Are you sure you want to delete the selected system properties?({len} properties selected)',
		deleteMessage: 'Are you sure you want to delete the selected system properties?',
		deleteAction: 'Delete',
		getListError: 'Get System Property Error',
		getListErrorMsg: 'Cannot get system properties from server.',
		expandAll: 'Expand All',
		collapseAll: 'Collapse All',
		deleteSucMsg: 'The selected records have been deleted.'
	},
	SystemProperty: {
		createSystemPropertyTitle: 'Create System Property',
		editSystemPropertyTitle: 'Edit System Property',
		save: 'Save',
		deleteProperty: 'Delete',
		saveTitle: 'Save System Property',
		saveAction: 'Save',
		saveSuccess: 'The system property has been saved.',
		saveFailure: 'Failed to save the system property',
		ok: 'OK',
		organization: 'Domain',
		organizations: 'Domains',
		toParent: 'OK',
		GetSysPropErrMsg: 'Cannot get the system property.',
		SaveSysPropErrMsg: 'Cannot save the system property.',
		deleteFailureMsg: 'Cannot delete the system properties.'
	},
	Organizations: {
		organizationsDisplayName: 'Domains',
		createOrganization: 'New',
		disableOrganization: 'Disable',
		disableTitle: 'Disable Domains',
		disablesMessage: 'Are you sure you want to disable the {0} selected domains?',
		disableMessage: 'Are you sure you want to disable the selected domain?',
		disableAction: 'Disable',

		enableOrganization: 'Enable',
		enableTitle: 'Enable Domains',
		enablesMessage: 'Are you sure you want to enable the {0} selected domains?',
		enableMessage: 'Are you sure you want to enable the selected domain?',
		enableAction: 'Enable',

		deleteOrganization: 'Delete',

		getListError: 'Get Domains Error',
		getListErrorMsg: 'Cannot get Domains from server.',

		cancel: 'Cancel'
	},
	Organization: {
		createOrganizationTitle: 'Create Domain',
		editOrganizationTitle: 'Edit Domain',
		saveOrg: 'Save',
		deleteOrg: 'Delete',
		cancel: 'Cancel',
		saveTitle: 'Save Domain',
		saveAction: 'Save',
		organizationSaved: 'Domain saved',
		orgNameRequired: 'Please provide an domain name',
		orgNameMaxLength: 'Domain name cannot be longer than 300 characters',
		orgDescriptionMaxLength: 'Domain description cannot be longer than 4000 characters',
		uorganizationName: 'Name',
		udescription: 'Description'
	},
	ConfigADs: {
		configADsTitle: 'Config Active Directory',
		createConfigAD: 'New',
		deleteConfigAD: 'Delete',
		deleteConfigADMessage: 'Are you sure you want to delete the selected Active Directory?',
		deleteConfigADsMessage: 'Are you sure you want to delete the {len} selected Active Directories?',
		deleteAction: 'Delete',

		getListError: 'Got Config AD Error',
		getListErrorMsg: 'Cannot get Active Directory from server.'
	},
	//--------------for Config AD/LDAP/RADIUS----------------//
	umode: 'Mode',
	uipAddress: 'IP Address',
	ucryptType: 'Crypt Type',
	ucryptPrefix: 'Crypt Prefix',
	ubindDn: 'Bind DN',
	ubindPassword: 'Bind Password',
	uversion: 'Version',
	uactive: 'Active',
	ussl: 'SSL',
	ufallback: 'Fallback',
	uisDefault: 'Default',
	udefaultDomain: 'DefaultDomain',
	uuidAttribute: 'User ID Attribute',
	upasswordAttribute: 'Password Attribute',
	udomain: 'Domain Name',
	ubaseDNList: 'Base DN List',
	uproperties: 'Properties',
	ugateway: 'Gateway',
	uport: 'Port',
	uprimaryHost: 'Primary Host',
	usecondaryHost: 'Secondary Host',
	uauthProtocol: 'Authentication Protocol',
	uauthPort: 'Authentication Port',
	uacctPort: 'Accounting Port',
	usharedSecret: 'Shared Secret',

	propertiesTitle: 'Properties',
	addProperties: 'Add',
	deleteProperty: 'Delete',

	addBaseDn: 'Add',
	deleteBaseDn: 'Delete',

	invalidDomain: 'The Domain field should not be empty.',
	baseDNListTitle: 'Base DN List',

	//---------------------------------------------------//
	ConfigAD: {
		configADTitle: 'Active Directory Config',
		configADTitle: 'Active Directory Config',

		saveADConfig: 'Save',
		belongsToOrganization: 'Resolve Domain',
		SaveSucTitle: 'Config Saved',
		SaveErrMsg: 'Cannot save the cofiguration.[{0}]',
		SaveSucMsg: 'The Configuration has been successfully saved.',
		confirmSuc: 'OK',
		LoadAuthGatewayErr: 'Cannot get gateways from the server.'
	},
	BusinessRules: {
		createBusinessRule: 'New',
		deleteBusinessRules: 'Delete',
		displayName: 'Business Rules',
		DeleteBusinessRulesTitle: 'Delete Business Rules',
		DeleteBusinessRulesMsg: 'Are you sure you want to delete the {0} selected items?',
		DeleteBusinessRuleMsg: 'Are you sure you want to delete the selected item?',
		confirmDelete: 'OK',
		ListBusinessRulesErrMsg: 'Cannot get business rules from the server',
		DeleteBusinessRulesErrMsg: 'Cannot delete the business rules.',
		DeleteBusinessRulesSucMsg: 'The selected records has been deleted.'
	},
	BusinessRule: {
		saveBusinessRule: 'Save',
		save: 'Save',
		SaveSucMsg: 'The business rule has been successfully saved.',
		SaveErrMsg: 'Cannot save the business rule.',
		invalidName: 'The name should only contain A-Z,a-z,0,dot,space and the first and last character cannot be dot and space',
		invalidOrder: 'The order should be integer.',
		invalidType: 'The type should not be empty.',
		businessRuleTitle: 'Business Rule',
		invalidTable: 'Please provide a table for this business rule',
		GetErrMsg: 'Cannot get the business rule from the server.[{0}]'
	},
	SystemScripts: {
		systemScriptsTitle: 'System Scripts',
		createScript: 'New',
		deleteScripts: 'Delete',
		DeleteScriptsTitle: 'Delete System Scripts',
		DeleteScriptsMsg: 'Are you sure you want to delete the {0} selected items?',
		DeleteScriptMsg: 'Are you sure you want to delete the selected item?',
		confirmDelete: 'OK',
		LoadErrMsg: 'Cannot get system scripts from the server.'
	},
	SystemScript: {
		saveScript: 'Save',
		scriptTitle: 'System Script Information',
		udescription: 'Description',
		SaveSucTitle: 'System Script Saved',
		SaveErrMsg: 'Cannot save the system script.',
		SaveSucMsg: 'The system script has been successfully saved.',
		confirmSaveSuc: 'OK',
		execute: 'Execute',
		ExeSucTitle: 'Script Executed',
		ExeSucMsg: 'Succssfully executed the system script.',
		confirmSuc: 'OK',
		invalidName: 'the name should only contain A-Z,a-z,0,dot,space and the first and last character cannot be dot and space',
		sysScriptTitle: 'System Script',
		LoadErrMsg: 'Cannot get the system script from the server.'
	},

	ApplicationRights: {
		applicationRightsDisplayName: 'Application Rights',
		createApplicationRight: 'New',
		deleteApplicationRight: 'Delete',

		deleteTitle: 'Confirm Deletion',
		deleteMessage: 'Are you sure you want to delete the selected application right?',
		deletesMessage: 'Are you sure you want to delete the {0} selected application rights?',
		deleteAction: 'Delete'
	},

	ApplicationRight: {
		applicationRight: 'Application Right',
		back: 'Back',
		save: 'Save',
		uappName: 'Name',
		ApplicationRightSaved: 'Application Right successfully saved',
		roles: 'Roles',
		controllers: 'Controllers',
		organizations: 'Domain',

		addOrganization: 'Add',
		removeOrganization: 'Remove',

		addRole: 'Add',
		removeRole: 'Remove',

		addController: 'Add',
		removeController: 'Remove',

		nameInvalid: 'Please provide a name for the application right'
	},

	RolePicker: {
		addRoleTitle: 'Add Role',
		keyword: 'Search',
		removeRoleTitle: 'Remove Role',
		add: 'Add',
		remove: 'Remove',
		cancel: 'Cancel'
	},

	ControllerPicker: {
		addControllerTitle: 'Add Controller',
		add: 'Add',
		cancel: 'Cancel'
	},

	OrganizationPicker: {
		addOrganizationTitle: 'Add Domain',
		add: 'Add',
		cancel: 'Cancel'
	},

	ConfigLDAPs: {
		invalidJSON: 'The server replies with a invalid JSON string',
		displayName: 'LDAP',
		createRecord: 'New',
		deleteRecords: 'Delete',
		DeleteTitle: 'Delete LDAP Properties',
		DeleteMsg: 'Are you sure you want to delete the selected item?',
		DeletesMsg: 'Are you sure you want to delete the selected items?({num} items selected)',
		confirmDelete: 'OK',
		ListRecordsErr: 'Cannot get ldaps from the server'
	},

	ConfigLDAP: {
		LdapTitle: 'LDAP',
		saveLdap: 'Save',
		GetErrMsg: 'Cannot get LDAP from server.',
		SaveSucTitle: 'LDAP Proerty Saved',
		SaveSucMsg: 'The LDAP property has been successfully saved.',
		SaveErrMsg: 'Cannot save the LDAP property.[{0}]',
		confirmSuc: 'OK',
		belongsToOrganization: 'Resolve Domain',
		LoadAuthGatewayErr: 'Cannot get gateways from the server.'
	},

	ConfigRADIUSs: {
		invalidJSON: 'The server replies with a invalid JSON string',
		displayName: 'RADIUS',
		createRecord: 'New',
		deleteRecords: 'Delete',
		DeleteTitle: 'Delete RADIUS Properties',
		DeleteMsg: 'Are you sure you want to delete the selected item?',
		DeletesMsg: 'Are you sure you want to delete the selected items?({num} items selected)',
		confirmDelete: 'OK',
		ListRecordsErr: 'Cannot get radius from the server'
	},

	ConfigRADIUS: {
		RadiusTitle: 'RADIUS',
		saveRadius: 'Save',
		GetErrMsg: 'Cannot get RADIUS from server.',
		SaveSucTitle: 'RADIUS Proerty Saved',
		SaveSucMsg: 'The RADIUS property has been successfully saved.',
		SaveErrMsg: 'Cannot save the RADIUS property.[{0}]',
		confirmSuc: 'OK',
		belongsToOrganization: 'Resolve Domain',
		LoadAuthGatewayErr: 'Cannot get gateways from the server.',
		invalidPrimaryHost: 'The Primary Host field should not be empty.',
		invalidSharedSecret: 'The Shared Secret field should not be empty.'
	},

	JobSchedules: {
		jobSchedulesDisplayName: 'Job Schedules',
		createJobSchedule: 'New',
		deleteJobSchedule: 'Delete',
		executeJobSchedule: 'Execute',
		executeJobScheduleTooltip: 'Click to execute this runbook immediately',

		activateJobSchedule: 'Activate',
		deactivateJobSchedule: 'Deactivate',

		activateTitle: 'Confirm Activation',
		activateMessage: 'Are you sure you want to activate the selected job schedule?',
		activatesMessage: 'Are you sure you want to activate the {0} selected job schedules?',
		activateAction: 'Activate',

		deactivateTitle: 'Confirm Deactivation',
		deactivateMessage: 'Are you sure you want to deactivate the selected job schedule?',
		deactivatesMessage: 'Are you sure you want to deactivate the {0} selected job schedules?',
		deactivateAction: 'Deactivate',

		deleteTitle: 'Confirm Deletion',
		deleteMessage: 'Are you sure you want to delete the selected job schedule?',
		deletesMessage: 'Are you sure you want to delete the {0} selected job schedules?',
		deleteAction: 'Delete',

		executeTitle: 'Confirm Execution',
		executeMessage: 'Are you sure you want to execute the selected job schedule?',
		executesMessage: 'Are you sure you want to execute the {0} selected job schedules?',
		executeAction: 'Execute',
		executeSuccess: 'Job Schedule executed successfully',
		executesSuccess: 'Job Schedules executed successfully'
	},
	JobSchedule: {
		executeJobSchedule: 'Execute',
		executeJobScheduleTooltip: 'Click to execute this runbook immediately',
		executeSuccess: 'Job Schedule executed successfully',

		jobSchedule: 'Job Schedule',
		JobScheduleSaved: 'Job Schedule saved',
		schedule: 'Schedule',
		seconds: 'Seconds',
		minutes: 'Minutes',
		hours: 'Hours',
		month: 'Month',
		dayType: 'Day Type',
		dayOfMonthRadio: 'Day of month',
		dayOfWeekRadio: 'Day of week',
		dayOfMonth: 'Day of Month',
		dayOfWeek: 'Day of Week',
		parameters: 'Parameters',

		timeRange: 'Time Range',
		startTime: 'Start Time',
		endTime: 'End Time',

		uname: 'Name',
		unameDisplay: 'Name',
		umodule: 'Module',
		urunbook: 'Runbook',
		value: 'Value',

		addParam: 'Add',
		removeParam: 'Remove',

		secondsTooltip: 'Seconds: Allowed values (0-59); Allowed Special Characters (,-*/)',
		minutesTooltip: 'Minutes: Allowed values (0-59); Allowed Special Characters (,-*/)',
		hoursTooltip: 'Hours: Allowed values (0-23); Allowed Special Characters (,-*/)',
		monthTooltip: 'Month: Allowed values (1-12 or JAN-DEC); Allowed Special Characters (,-*/)',
		dayOfMonthTooltip: 'Day of month: Allowed values (1-31); Allowed Special Characters (,-*/LW)',
		dayOfWeekTooltip: 'Day of week: Allowed values (1-7 or SUN-SAT); Allowed Special Characters (.-*/L#)',

		executeTitle: 'Confirm Execution',
		executeMessage: 'Are you sure you want to execute this job schedule?',
		executeAction: 'Execute',

		nameInvalid: 'Please provide a name for the job schedule',
		secondsInvalid: 'Please provide a value for seconds, * is the default',
		minutesInvalid: 'Please provide a value for minutes, * is the default',
		hoursInvalid: 'Please provide a value for hours, * is the default',
		monthInvalid: 'Please provide a value for month, * is the default'
	},

	ListRegistrations: {
		listRegistrationsDisplayName: 'List Registrations',
		deleteListRegistration: 'Delete',

		deleteTitle: 'Confirm Deletion',
		deleteMessage: 'Are you sure you want to delete the selected list registration?',
		deletesMessage: 'Are you sure you want to delete the {0} selected list registrations?',
		deleteAction: 'Delete'
	},
	ListRegistration: {
		title: 'Registration'
	},

	MenuDefinitions: {
		menuDefinitionsDisplayName: 'Menu Definitions',
		createMenuDefinition: 'New',
		deleteMenuDefinition: 'Delete',

		deleteTitle: 'Confirm Deletion',
		deleteMessage: 'Are you sure you want to delete the selected menu definition?',
		deletesMessage: 'Are you sure you want to delete the {0} selected menu definitions?',
		deleteAction: 'Delete',
		draganddroptip : '<i>* Drag and drop re-order menu definition.</i>'
	},
	MenuDefinition: {
		menuDefTitle: 'Menu Definition',
		addRole: 'Add',
		addMenuItem: 'Add Item',
		addGroup: 'Add Group',
		newGroup: 'New Group',
		removeMenuItem: 'Remove',
		removeRole: 'Remove',
		menuItems: 'Menu Items',
		fontColor: 'Font:',
		backgroundColor: 'Background:',
		MenuDefinitionSaved: 'Menu Definition successfully saved',
		titleInvalid: 'Please provide a title for this menu definition'
	},

	MenuSets: {
		menuSetsDisplayName: 'Menu Sets',
		createMenuSet: 'New',
		deleteMenuSet: 'Delete',

		deleteTitle: 'Confirm Deletion',
		deleteMessage: 'Are you sure you want to delete the selected menu set?',
		deletesMessage: 'Are you sure you want to delete the {0} selected menu sets?',
		deleteAction: 'Delete',
		draganddroptip : '<i>* Drag and drop re-order menu set.</i>'
	},
	MenuSet: {
		title: 'Menu Set',
		addRole: 'Add',
		removeRole: 'Remove',
		menus: 'Menus',
		addMenu: 'Add',
		removeMenu: 'Remove',
		MenuSetSaved: 'Menu Set successfully saved',
		nameInvalid: 'Please provide a name for this menu set'
	},

	MenuPicker: {
		addMenuTitle: 'Menus',
		add: 'Add'
	},

	MenuItem: {
		menuItem: 'Menu Item',
		addMenuItem: 'Add',
		applyMenuItem: 'Apply',
		addRole: 'Add',
		removeRole: 'Remove',
		ItemAlreadyExist: 'MenuItem:{title} already exists.'
	},

	ServerInformation: {
		systemInformationTitle: 'System Information',

		version: 'Version',
		build: 'Build',
		server: 'Server',
		osArchitecture: 'Architecture',
		osName: 'OS',
		osVersion: 'Version',
		address: 'Address',
		osProcessor: 'Processor',
		jvmVendor: 'JVM Vendor',
		jvmName: 'JVM Name',
		jvmVersion: 'JVM Version',
		memory: 'Memory',
		memoryMax: 'Max Memory',
		memoryTotal: 'Total Memory',
		memoryFreePercentage: 'Free Memory',
		thread: 'Thread',
		threadPeak: 'Peak Thread',
		threadLive: 'Live Thread',
		threadDeamon: 'Daemon Thread',

		sessions: 'Sessions'
	},

	ToolbarConfiguration: {
		tooltip: 'Tooltip',
		toolbarConfTitle: 'Toolbar Definition',
		addToolbarItem: 'Add Item',
		removeToolbarItem: 'Remove',
		addGroup: 'Add Group',
		newGroup: 'New Group',
		newItem: 'New Item',
		addRoles: 'Add Roles',
		removeRoles: 'Remove Roles',
		SaveSucMsg: 'The toolbar configuration is saved.',
		roleDisplayName: 'Roles',
		reset: 'Reset',
		newText: 'New',
		menuText: 'Menu',
		socialText: 'Social',
		worksheetText: 'Worksheet',
		documentText: 'Document',
		activeWorksheetText: 'Active Worksheet',
		newWorksheetText: 'New Worksheet',
		allWorksheetsText: 'All Worksheets',
		defaultDocumentText: 'HOMEPAGE',
		addDocumentText: 'Add Document',
		ResetTitle: 'Reset Toolbar',
		ResetConfirmation: 'Are you sure you want to reset? All your changes will be lost.',
		yes: 'Yes',
		no: 'No',
		dump: 'Add',
		dumpRemove: 'Remove',
		asWindowTitle: 'Roles',
		noMenuTitle: 'Warning',
		noMenu: 'Menu is not in the current toolbar configuration.',
		GetErrMsg: 'Cannot get toolbar configuration from the server.'
	},

	username: 'Username',
	createTime: 'Create Time',
	updateTime: 'Update Time',
	durationInSecs: 'Duration(secs)',
	sessionDurationInMins: 'Remaining (min)',
	ssoType: 'SSO Type',
	ssoSessionId: 'SSO Session Id',
	lastSSOValidationTime: 'Session Validation Time',
	iframeId: 'iframe Id',
	sessionId: 'iframe Session Id',

	SessionInformation: {
		userInfoTitle: 'User Session Information',
		removeSessions: 'Remove',
		sessionSummary: 'Session Summary',
		activeSessions: 'Active Sessions',
		maxActiveSessions: 'Max Active Sessions:',
		maxSessions: 'Max Sessions',
		timeout: 'Timeout',
		sessionDetails: 'Session Details',
		LoadUserSessionInfoErrMsg: 'Cannot load user session information.',

		RemoveSessionTitle: 'Confirm Remove Session',
		RemoveSessionMsg: 'Are you sure you want to remove the selected session?',
		RemoveSessionMsgs: 'Are you sure you want to remove {0} selected sessions?',
		remove: 'Remove',
		cancel: 'Cancel'
	},

	propertiesTab : 'Properties',
	sourceTab : 'Source',
	ruleTab : 'Rule',
	scriptTab : 'Script'
}
