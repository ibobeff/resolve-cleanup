glu.defView('RS.sysadmin.SystemScript', {
	padding: 15,
	xtype : 'panel',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls : 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{sysScriptTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'		
		},'save','execute', '-',
		{
			name : 'propertiesTab',
			cls : 'rs-small-btn rs-toggle-btn'
		},{
			name : 'sourceTab',
			cls : 'rs-small-btn rs-toggle-btn'
		},'->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	layout: {
		type: 'card',
		align: 'stretch',	
	},
	activeItem : '@{activeItem}',
	items: [{
		style : 'border-top:1px solid silver',
		padding : '10 0',
		xtype: 'panel',	
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults : {
			margin : '4 0'
		},
		items : [
		{
			xtype: 'textfield',
			fieldLabel: '~~Name~~',
			name: 'uname'
		},{
			xtype: 'textarea',
			fieldName: '~~Description~~',
			name: 'udescription',
			minHeight: 200,
		}]
	},{
		style : 'border-top:1px solid silver',
		padding : '10 0',	
		xtype: 'AceEditor',
		flex: 1,
		width: '100%',
		name: 'uscript',
		parser: 'groovy'
	}]
});
