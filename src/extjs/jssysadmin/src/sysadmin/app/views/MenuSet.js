glu.defView('RS.sysadmin.MenuSet', {
	autoScroll: true,
	padding: 15,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{MenuSetTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'			
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		xtype: 'panel',
		style : 'border-top:1px solid silver',
		padding : '8 0',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'textfield',
			fieldLabel: '~~name~~',
			name: 'name',
			width: '50%'
		}, {
			xtype: 'checkbox',
			padding: '0px 0px 0px 50px',
			hideLabel: true,
			name: 'active',
			hideLabel: true,
			boxLabel: '~~active~~'
		}]
	}, {
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			title: '~~roles~~',
			xtype: 'grid',
			dockedItems: [{
				xtype: 'toolbar',	
				cls: 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},			
				items: ['addRole', 'removeRole']
			}],
			flex: 1,
			cls : 'rs-grid-dark',
			name: 'roles',
			columns: '@{rolesColumns}'		
		}, {
			padding: '0px 0px 0px 20px',
			dockedItems: [{
				xtype: 'toolbar',	
				cls: 'rs-dockedtoolbar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},			
				items: ['addMenu', 'removeMenu']
			}],
			title: '~~menus~~',
			xtype: 'grid',
			cls : 'rs-grid-dark',
			flex: 1,
			name: 'menus',
			columns: '@{menusColumns}'		
		}]
	}]
});
