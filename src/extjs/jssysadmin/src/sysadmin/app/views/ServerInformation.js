glu.defView('RS.sysadmin.ServerInformation', {
	autoScroll: true,
	padding: '10px',
	bodyPadding: '10px',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls: 'actionBar actionBar-form',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '~~systemInformationTitle~~'
		}, '->', {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			flex: 1,
			layout: {
				type: 'vbox',
				align: 'stretch',
				padding: '0px 30px 0px 0px'
			},
			items: [{
				title: '~~version~~',
				bodyPadding: '10px',
				defaultType: 'displayfield',
				defaults: {
					labelWidth: 150
				},
				items: ['version', 'build']
			}, {
				bodyPadding: '10px',
				defaultType: 'displayfield',
				defaults: {
					labelWidth: 150
				},
				title: '~~memory~~',
				items: ['memoryMax',
					'memoryTotal',
					'memoryFreePercentage'
				]
			}, {
				bodyPadding: '10px',
				defaultType: 'displayfield',
				defaults: {
					labelWidth: 150
				},
				title: '~~thread~~',
				items: ['threadPeak',
					'threadLive',
					'threadDeamon'
				]
			}]
		}, {
			flex: 1,
			bodyPadding: '10px',
			defaultType: 'displayfield',
			defaults: {
				labelWidth: 150
			},
			title: '~~server~~',
			items: ['osArchitecture',
				'osName',
				'osVersion',
				'address',
				'osProcessor',
				'jvmVendor',
				'jvmName',
				'jvmVersion'
			]
		}]
	}]
})