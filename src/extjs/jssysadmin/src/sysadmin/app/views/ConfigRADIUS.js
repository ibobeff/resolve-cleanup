glu.defView('RS.sysadmin.ConfigRADIUS', {
	xtype: 'form',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	padding : 15,
	//default configs
	autoScroll: true,
	//toolbar
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls: 'rs-dockedtoolbar',
		defaultButtonUI: 'display-toolbar-button',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{title}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'		
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{sys_id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],

	//layout all the fields for the RADIUS
	items: [{
		xtype: 'container',
		style : 'border-top:1px solid silver',
		padding : '8 0',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'container',
			defaultType: 'textfield',
			flex: 1,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				labelWidth: 130,
				margin : '4 0'
			},
			items: [
				'udomain', {
					name: 'belongsToOrganization',
					xtype: 'combo',
					editable: false,
					store: '@{organizations}',
					displayField: 'uorganizationName',
					valueField: 'sys_id'
				}, {
					xtype: 'combobox',
					editable: false,
					store: '@{authGatewayStore}',
					displayField: 'name',
					valueField: 'value',
					queryMode: 'local',
					name: 'ugateway'
				}, 'uprimaryHost', {
					xtype: 'container',
					defaultType: 'checkbox',
					layout: {
						type: 'hbox',
						align: 'stretch'
					},
					items: [{
						name: 'ufallback',
						hideLabel: true,
						boxLabel: '~~ufallback~~',
						padding: '0px 0px 0px 135px'
					}]
				}
			]
		}, {
			xtype: 'container',
			margin : {
				left : 15
			},
			defaultType: 'textfield',
			flex: 1,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				labelWidth: 180,
				margin : '4 0'
			},
			items: [{
				xtype: 'combobox',
				name: 'uauthProtocol',
				editable: false,
				queryMode: 'local',
				displayField: 'type',
				valueField: 'type',
				store: '@{authprotocolStore}',			
			},
			'uauthPort','uacctPort','usecondaryHost','usharedSecret']
		}]
	}]
});
