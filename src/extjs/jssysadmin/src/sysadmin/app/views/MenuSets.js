glu.defView('RS.sysadmin.MenuSets', {
	padding: 15,
	xtype: 'grid',
	cls : 'rs-grid-dark',
	name: 'menuSets',
	border: false,
	stateId: '@{stateId}',
	displayName: '@{displayName}',
	stateful: true,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager',
		pageable: false
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',		
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		childLink: 'RS.sysadmin.MenuSet',
		childLinkIdProperty: 'id'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['createMenuSet', 'deleteMenuSet']
	},{
		xtype : 'displayfield',
		margin : '5 0 0 0',
		dock : 'bottom',
		hidden : true,
		value : '~~draganddroptip~~'
	}],
	columns: '@{columns}',
	viewConfig: {
		enableTextSelection: true,
		plugins: {
			ptype: 'gridviewdragdrop',
			dragText: 'Drag and drop to reorganize'
		},
		listeners: {
			drop: function(node, data, dropRec, dropPosition, dropHandlers) {
				this.ownerCt.fireEvent('dropRec', node, data, dropRec, dropPosition, dropHandlers);
			}
		}
	},
	listeners: {
		editAction: '@{editMenuSet}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}',
		dropRec: '@{arrange}'
	}
})