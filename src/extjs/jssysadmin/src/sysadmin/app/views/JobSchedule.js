glu.defView('RS.sysadmin.JobSchedule', {
	xtype: 'form',
	layout : 'fit',
	padding: 15,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls : 'rs-dockedtoolbar',
		defaultButtonUI: 'display-toolbar-button',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{jobScheduleTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, {
			name: 'executeJobSchedule',
			tooltip: '~~executeJobScheduleTooltip~~'
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	defaults : {
		defaults : {
			labelWidth : 130,
			margin : '4 0'
		}
	},	
	items: [{
		xtype : 'panel',
		style : 'border-top:1px solid silver',
		padding : '8 0',
		defaultType: 'textfield',
		layout : {
			type : 'vbox',
			align : 'stretch'
		},
		items : ['uname',
		{
			xtype: 'displayfield',
			cls : 'rs-displayfield-value',
			name: 'unameDisplay',
			htmlEncode : true
		}, 'umodule', 'urunbook', {
			xtype: 'checkbox',
			name: 'uactive',
			boxLabel : '~~uactive~~',
			hideLabel : true,
			margin : '0 0 0 135'
		}]
	},{
		xtype: 'panel',	
		bodyPadding : '8 0 0 0',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		title: '~~schedule~~',
		items: [{
			xtype: 'textfield',
			name: 'seconds',
			tooltip: '~~secondsTooltip~~',
			keyDelay: 1,
			listeners: {
				render: function(field) {
					Ext.tip.QuickTipManager.register({
						target: field.getEl().dom.id,
						text: field.tooltip
					})
				}
			}
		}, {
			xtype: 'textfield',
			name: 'minutes',
			tooltip: '~~minutesTooltip~~',
			keyDelay: 1,
			listeners: {
				render: function(field) {
					Ext.tip.QuickTipManager.register({
						target: field.getEl().dom.id,
						text: field.tooltip
					})
				}
			}
		}, {
			xtype: 'textfield',
			name: 'hours',
			tooltip: '~~hoursTooltip~~',
			keyDelay: 1,
			listeners: {
				render: function(field) {
					Ext.tip.QuickTipManager.register({
						target: field.getEl().dom.id,
						text: field.tooltip
					})
				}
			}
		}, {
			xtype: 'textfield',
			name: 'month',
			tooltip: '~~monthTooltip~~',
			keyDelay: 1,
			listeners: {
				render: function(field) {
					Ext.tip.QuickTipManager.register({
						target: field.getEl().dom.id,
						text: field.tooltip
					})
				}
			}
		}, {
			xtype: 'fieldcontainer',
			fieldLabel: '~~dayType~~',
			layout: 'hbox',
			items: [{
				xtype: 'radio',
				name: 'dayType',
				boxLabel: '~~dayOfMonthRadio~~',
				value: '@{dayOfMonthRadio}'
			}, {
				xtype: 'radio',
				name: 'dayType',
				boxLabel: '~~dayOfWeekRadio~~',
				value: '@{dayOfWeekRadio}',
				padding: '0px 0px 0px 10px'
			}]
		}, {
			xtype: 'textfield',
			name: 'dayOfMonth',
			tooltip: '~~dayOfMonthTooltip~~',
			keyDelay: 1,
			listeners: {
				render: function(field) {
					Ext.tip.QuickTipManager.register({
						target: field.getEl().dom.id,
						text: field.tooltip
					})
				}
			}
		}, {
			xtype: 'textfield',
			name: 'dayOfWeek',
			tooltip: '~~dayOfWeekTooltip~~',
			keyDelay: 1,
			listeners: {
				render: function(field) {
					Ext.tip.QuickTipManager.register({
						target: field.getEl().dom.id,
						text: field.tooltip
					})
				}
			}
		}]
	}, {
		xtype: 'panel',	
		bodyPadding : '8 0 0 0'	,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		title: '~~timeRange~~',
		items: [{
			xtype: 'xdatetimefield',
			fieldLabel: '~~startTime~~',
			format: 'c',
			name: 'ustartTime'
		}, {
			xtype: 'xdatetimefield',
			fieldLabel: '~~endTime~~',
			name: 'uendTime'
		}]
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		flex : 1,
		name: 'parameters',
		store: '@{parametersStore}',
		title: '~~parameters~~',
		columns: '@{parametersColumns}',
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 2
		}],
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items : ['addParam', 'removeParam']
		}],	
		listeners: {
			render: function(grid) {
				grid.store.grid = grid;
			}
		}
	}]
})
