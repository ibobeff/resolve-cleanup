glu.defView('RS.sysadmin.MenuPicker', {
	title: '~~addMenuTitle~~',
	width: 600,
	height: 400,
	padding : 15,
	modal : true,
	cls : 'rs-modal-popup',
	layout: 'fit',
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'menus',
		columns: '@{columns}',
		plugins: [{
			ptype: 'pager'
		}],
		selModel: {
			mode: 'MULTI'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: []
		}],
		margin : {
			bottom : 8
		}
	}],
	buttons: [{
		name : 'add',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})