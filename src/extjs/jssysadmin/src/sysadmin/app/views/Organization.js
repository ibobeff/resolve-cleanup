glu.defView('RS.sysadmin.Organization', {
	xtype: 'form',
	autoScroll: true,
	bodyPadding: '10px',
	buttonAlign: 'left',
	items: [{
		xtype: 'textfield',
		name: 'uorganizationName'
	}, {
		xtype: 'textarea',
		name: 'udescription'
	}],

	buttons: ['save', 'cancel'],
	asWindow: {
		width: 500,
		height: 400,
		title: '@{title}'
	}
});