glu.defView('RS.sysadmin.RolePicker', {	
	title: '@{title}',
	padding : 15,
	modal: true,
	cls : 'rs-modal-popup',
	width: 800,
	height: 450,
	layout: 'fit',
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'roles',
		columns: '@{columns}',
		plugins: [{
			ptype: 'pager'
		}],
		selModel: {
			mode: 'MULTI'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			items: [{
				xtype: 'textfield',
				name: 'keyword',
				width : 400
			}]
		}],
		margin : {
			bottom : 10
		}
	}],	
	buttons: [{
		name :'add', 
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'remove',
			cls : 'rs-med-btn rs-btn-dark'
	},{ 
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})