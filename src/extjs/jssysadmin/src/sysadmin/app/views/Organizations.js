glu.defView('RS.sysadmin.Organizations', {
	padding: '10px',
	xtype: 'grid',
	displayName: '@{displayName}',
	stateful: true,
	stateId: '@{stateId}',
	name: 'organizations',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar',
		name: 'actionBar',
		items: ['createOrganization', 'enableOrganization', 'disableOrganization']
	}],
	columns: '@{columns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',	
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		childLinkIdProperty: 'id'
	},
	listeners: {
		editAction: '@{editOrganization}'
	}
});