glu.defView('RS.sysadmin.SystemProperty', {
	xtype: 'form',
	autoScroll: true,
	width: 600,
	modal:true,
	title: '@{title}',
	cls : 'rs-modal-popup',
	padding : 15,
	layout:{
		type:'vbox',
		align:'stretch'
	},
	defaults : {
		margin : '4 0'
	},
	items: [{
		xtype: 'textfield',
		fieldLabel: '~~uname~~',
		name: 'uname',
		maxLength: 300,
		readOnly: '@{editable}'
	}, {
		xtype: 'textarea',
		fieldLabel: '~~udescription~~',
		name: 'udescription',
		height: 200,
		maxLength: 4000,
		readOnly: '@{editable}'
	}, {
		xtype: 'textfield',
		fieldLabel: '~~uvalue~~',
		name: 'uvalue',
		readOnly: '@{editable}',
		margin : '4 0 8 0'
	}],

	buttons: [{
		name: 'save',
		cls : 'rs-med-btn rs-btn-dark',
		listeners: {
			doubleClickHandler: '@{saveAndExit}',
			render: function(button) {
				button.getEl().on('dblclick', function() {
					button.fireEvent('doubleClickHandler')
				});
			}
		}
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	},{
		name: 'toParent',
		cls : 'rs-med-btn rs-btn-light'
	}]	
});
