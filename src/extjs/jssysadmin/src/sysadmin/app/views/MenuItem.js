glu.defView('RS.sysadmin.MenuItem', {
	title: '~~menuItem~~',
	cls : 'rs-modal-popup',
	width: 600,
	height: 400,
	modal: true,
	padding : 15,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults : {
		labelWidth : 120,
		margin : '4 0'
	},
	items: [{
		xtype: 'textfield',
		fieldLabel: '~~name~~',
		name: 'title'
	},{
		xtype: 'textfield',
		fieldLabel: '~~query~~',
		name: 'query'
	},{
		xtype: 'checkbox',
		boxLabel: '~~active~~',
		margin: '0 0 0 125',
		hideLabel : true,
		name: 'active'
	},{
		title: '~~roles~~',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'roles',
		columns: '@{rolesColumns}',
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items : ['addRole', 'removeRole']
		}],	
		flex: 1,
		margin : '0 0 10 0'
	}],
	buttons: [{
		name :'addMenuItem', 
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'applyMenuItem',
			cls : 'rs-med-btn rs-btn-dark'
	},{ 
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]	
});