glu.defView('RS.sysadmin.ApplicationRights', {
	padding: 15,
	xtype: 'grid',
	cls : 'rs-grid-dark',
	name: 'applicationRights',
	stateId: '@{stateId}',
	displayName: '@{displayName}',
	stateful: true,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',	
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		childLink: 'RS.sysadmin.ApplicationRight',
		childLinkIdProperty: 'id'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['createApplicationRight', 'deleteApplicationRight']
	}],
	columns: '@{columns}',
	viewConfig: {
		enableTextSelection: true
	},
	listeners: {
		editAction: '@{editApplicationRight}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
})