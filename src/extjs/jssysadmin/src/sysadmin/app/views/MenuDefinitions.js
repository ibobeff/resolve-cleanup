glu.defView('RS.sysadmin.MenuDefinitions', {
	padding: 15,
	xtype: 'grid',
	cls : 'rs-grid-dark',
	name: 'menuDefinitions',
	border: false,
	displayName: '@{displayName}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager',
		pageable: false
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.gotoDetailsColumnTooltip,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		childLink: 'RS.sysadmin.MenuDefinition',
		childLinkIdProperty: 'id'
	},
	dockedItems: [		
		{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: ['createMenuDefinition', 'deleteMenuDefinition']
		},{
			xtype : 'displayfield',
			dock : 'bottom',
			margin : '5 0 0 0',
			hidden : true,
			value : '~~draganddroptip~~'
		}
	],
	columns: '@{columns}',
	viewConfig: {
		plugins: [{
			ptype: 'gridviewdragdrop',
			dragText: 'Drag and drop to reorganize'
		}],
		listeners: {
			drop: function(node, data, overModel, dropPosition, eOpts) {
				this.ownerCt.fireEvent('definitionDrop', this)
			}
		}
	},

	listeners: {
		render: function() {
			var filter = null;
			var dd = this.getView().plugins[0];

			Ext.each(this.plugins, function(p) {
				if (p.ptype == 'searchfilter')
					filter = p;
				if (p.ptype == 'gridviewdragdrop')
					dd = p;
			});

			this.getStore().on("load", function() {
				if (filter.filterBar.getFilters().length > 0)
					dd.disable();
				else
					dd.enable();
			}, this);
		},
		definitionDrop: '@{definitionDrop}',
		editAction: '@{editMenuDefinition}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
})