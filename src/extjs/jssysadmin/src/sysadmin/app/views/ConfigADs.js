glu.defView('RS.sysadmin.ConfigADs', {
	xtype: 'grid',
	cls : 'rs-grid-dark',
	stateId: '@{stateId}',
	stateful: true,
	padding: 15,
	layout: 'fit',
	displayName: '@{displayName}',
	name: 'configAD',
	store: '@{configADStore}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['createConfigAD', 'deleteConfigAD']
	}],
	columns: '@{columns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},

	listeners: {
		editAction: '@{editConfigAD}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});