glu.defView('RS.sysadmin.ConfigAD', {
	xtype: 'form',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},	
	autoScroll: true,
	padding : 15,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls: 'rs-dockedtoolbar',
		defaultButtonUI: 'display-toolbar-button',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{title}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',		
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],

	//layout all the fields for the active directory
	items: [{
		xtype: 'container',
		style : 'border-top:1px solid silver',
		padding : '8 0',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},

		items: [{
			xtype: 'container',
			defaultType: 'textfield',
			flex: 1,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				labelWidth : 130,
				margin : '4 0'
			},			
			items: [
				'udomain', {
					name: 'belongsToOrganization',
					xtype: 'combo',
					editable: false,
					store: '@{organizations}',
					displayField: 'uorganizationName',
					valueField: 'sys_id'
				}, {
					xtype: 'combobox',
					editable: false,
					store: '@{authGatewayStore}',
					displayField: 'name',
					valueField: 'value',
					queryMode: 'local',
					name: 'ugateway'
				}, {
					xtype: 'container',
					layout: {
						type: 'hbox',
						align: 'stretch'
					},					
					defaultType: 'textfield',
					items: [{
						name : 'uipAddress',
						flex : 1,
						labelWidth : 130,
						margin : {
							right : 10
						}
					},{
						name : 'uport',
						width : 250,
						labelWidth : 50
					}]
				}, {
					name: 'ussl',
					xtype : 'checkbox',
					hideLabel: true,
					boxLabel: '~~ussl~~',
					margin: '0px 0px 0px 135px'
				}, {
					name: 'ufallback',
					xtype : 'checkbox',
					hideLabel: true,
					boxLabel: '~~ufallback~~',
					margin: '0px 0px 0px 135px'
				},{
					name: 'groupRequired',
					xtype : 'checkbox',
					hideLabel: true,
					boxLabel: '~~groupRequired~~',
					margin: '0px 0px 0px 135px'
				}
			]
		}, {
			margin : {
				left : 15
			},
			xtype: 'container',
			defaultType: 'textfield',
			flex: 1,
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				labelWidth : 130,
				margin : '4 0'
			},
			items: [{
				xtype: 'container',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				defaults: {
					flex: 1
				},
				items: [{
					xtype: 'container',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					defaultType: 'textfield',
					defaults : {
						margin : '4 0',
						labelWidth : 130
					},
					items: ['umode', 'ubindDn', 'uuidAttribute']
				}, {
					xtype: 'container',
					margin : {
						left : 15
					},
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					defaultType: 'textfield',
					defaults : {
						margin : '4 0',
						labelWidth : 130
					},
					items: ['uversion', {
						name: 'ubindPassword',
						inputType: 'password'
					}]
				}]
			}]
		}]
	}, {
		xtype: 'container',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'grid',
			cls : 'rs-grid-dark',
			flex: 1,
			viewConfig: {
				markDirty: false
			},
			name: 'baseDN',
			title: '@{baseDNListTitle}',
			store: '@{baseDNList}',		
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar rs-dockedtoolbar',
				name: 'actionBar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items: ['addBaseDn', 'deleteBaseDn']
			}],
			selected: '@{selectedBaseDN}',
			columns: '@{baseDNColumns}',
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}],
			margin : {
				right : 15
			}
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			flex: 1,
			viewConfig: {
				markDirty: false
			},
			name: 'properties',
			title: '@{propertiesTitle}',
			store: '@{properties}',		
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar rs-dockedtoolbar',
				name: 'actionBar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items: ['addProperties', 'deleteProperty']
			}],
			selected: '@{selectedProp}',
			columns: '@{propertiesColumns}',
			plugins: [{
				ptype: 'cellediting',
				clicksToEdit: 1
			}]
		}]
	}]
});
