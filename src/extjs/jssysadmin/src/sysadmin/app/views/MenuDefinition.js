glu.defView('RS.sysadmin.MenuDefinition', {
	padding: 15,	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls: 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{menuDefinitionTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'		
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	items: [{
		xtype: 'panel',
		style : 'border-top:1px solid silver',
		padding : '8 0',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},		
		items: [{
			xtype: 'textfield',
			fieldLabel: '~~name~~',
			name: 'title',
			width: '50%',
		}, {
			xtype: 'checkbox',
			padding: '0 0 0 50',
			hideLabel: true,
			name: 'active',		
			boxLabel: '~~active~~'
		} /*,{		
			padding: '0 0 0 50',
			xtype: 'toolbar',		
			items: [{
				xtype: 'label',
				text: '~~backgroundColor~~'
			}, {
				xtype: 'button',
				text: '@{bgStyle}',
				padding: '0 0 0 5',
				menu: {
					id: 'bgColor',
					xtype: 'colormenu',
					handler: '@{selectBgColor}'					
				}
			}, {
				xtype: 'label',
				text: '~~fontColor~~',
				margin : '0 0 0 50'
			}, {
				xtype: 'button',
				text: '@{txtStyle}',
				padding: '0 0 0 5',
				menu: {
					id: 'txtColor',
					xtype: 'colormenu',
					handler: '@{selectTxtColor}'
				}
			}]
		}, {
			xtype: 'displayfield',
			value: '@{color}',
			hidden: true,
			listeners: {
				change: function() {
					if (!this.value)
						return;
					this.ownerCt.down('#bgColor').items.get(0).select(this.value);
				}
			}
		}, {
			xtype: 'displayfield',
			value: '@{textColor}',
			hidden: true,
			listeners: {
				change: function() {
					if (!this.value)
						return;
					this.ownerCt.down('#txtColor').items.get(0).select(this.value);
				}
			}
		}*/]
	}, {
		xtype: 'treepanel',
		title: '~~menuItems~~',
		cls : 'rs-grid-dark',
		flex: 1,
		name: 'menuItems',
		useArrows: true,
		store: '@{menuItems}',
		split: true,
		columns: '@{menuItemsColumns}',
		rootVisible: false,
		displayField: 'group',
		selModel: {
			selType: 'resolvecheckboxmodel',	
			glyph : 0xF044,
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id'
		},
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 1
		}],
		dockedItems: [{
			xtype : 'toolbar',
			cls: 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light',
			},
			items : ['addMenuItem', 'addGroup', 'removeMenuItem']
		}],
		viewConfig: {
			markDirty: false,
			plugins: {
				ptype: 'treeviewdragdrop'
			},
			listeners: {
				beforedrop: function(node, data, overModel, dropPosition, dropHandlers) {
					this.ownerCt.fireEvent('beforeDropRec', node, data, overModel, dropPosition, dropHandlers);
				}
			}
		},
		listeners: {
			beforeDropRec: '@{checkDrop}',
			editAction: '@{editMenuItem}',
			beforecellclick: function(treeview, td, cellIndex, record, tr, rowIndex, e, eOpts) {
				if (!record.isLeaf()) {
					if (treeview.ownerCt.columns[cellIndex + 1].dataIndex != 'query') return false //cell index + 1 because of the tree column
				}
			}
		}	
	}, {
		title: '~~roles~~',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		flex: 1,
		name: 'roles',
		columns: '@{rolesColumns}',
		dockedItems: [{
			xtype : 'toolbar',
			cls: 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light',
			},
			items : ['addRole', 'removeRole']
		}]
	}]
})
