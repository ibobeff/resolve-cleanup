glu.defView('RS.sysadmin.BusinessRule', {
	padding: 15,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls : 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{businessRuleTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'			
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '-',{
			name : 'ruleTab',
			cls : 'rs-small-btn rs-toggle-btn'
		},{
			name : 'scriptTab',
			cls : 'rs-small-btn rs-toggle-btn'
		},'->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	layout : 'card',
	activeItem : '@{activeItem}',
	items: [{
		style : 'border-top:1px solid silver',
		padding : '8 0',		
		layout: 'hbox',
		items: [{
			xtype: 'panel',
			margin : {
				right : 15
			},
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {				
				margin : '4 0',
				labelWidth : 120
			},
			flex: 1,
			items: [{
				xtype: 'textfield',
				fieldLabel: '~~Name~~',
				name: 'uname'
			}, {
				xtype: 'textfield',
				fieldLabel: '~~Order~~',
				name: 'uorder'
			}, {
				xtype: 'panel',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				defaults : {
					hideLabel : true,
					margin : '0 0 0 125'
				},
				items: [{
					xtype: 'checkbox',
					boxLabel: '~~Active~~',
					name: 'uactive',
					flex: 1
				}, {
					xtype: 'checkbox',
					boxLabel: '~~NonBlocking~~',
					name: 'uasync',
					flex: 1
				}, {
					xtype: 'checkbox',
					boxLabel: '~~Before~~',
					name: 'ubefore',
					flex: 1
				}]
			}]
		}, {
			xtype: 'panel',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				margin : '4 0',
				labelWidth : 120
			},
			flex: 1,
			items: [{
				xtype: 'combo',
				editable: false,
				fieldLabel: '~~Table~~',
				store: '@{tableStore}',
				displayField: 'name',
				valueField: 'value',
				name: 'utable'
			}, {
				xtype: 'combo',
				editable: false,
				fieldLabel: '~~Types~~',
				multiSelect: true,
				store: '@{typeStore}',
				displayField: 'name',
				valueField: 'value',
				name: 'utype',
				flex: 1
			}]
		}]
	}, {
		xtype: 'AceEditor',
		style : 'border-top:1px solid silver',
		padding : '8 0',
		flex: 1,
		width: '100%',
		name: 'uscript',
		parser: 'groovy'		
	}]

});
