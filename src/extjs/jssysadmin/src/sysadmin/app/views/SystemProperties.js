glu.defView('RS.sysadmin.SystemProperties', {
	xtype: 'grid',
	layout: 'fit',
	cls : 'rs-grid-dark',
	displayName: '@{displayName}',
	padding: 15,
	stateId: '@{stateId}',
	stateful: true,
	name: 'properties',
	store: '@{propertiesStore}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		name: 'actionBar',
		items: ['createProperty', 'deleteProperties']
	}],
	columns: '@{columns}',
	features: [{
		ftype: 'grouping',
		id: 'group'
	}],
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true,
		clobCheckURL : '/resolve/service/sysproperties/getClobColumnNames'
	}, {
		ptype: 'columnautowidth'
	}, {
		ptype: 'pager',
		pageable: false
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.editColumnTooltip,
		glyph : 0xF044,	
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		toggleUiHeader: function(anyCollapsed) {
			var view = this.views[0],
				headerCt = view.headerCt,
				checkHd = headerCt.child('gridcolumn[isCheckerHd]');
			checkHd[anyCollapsed ? 'removeCls' : 'addCls']('x-grid-group-hd-collapsed');
		},
		getHeaderConfig: function() {
			var me = this;

			return {
				xtype: 'viewcolumn',
				isCheckerHd: true,
				text: '<div class="x-grid-group-title">&#160;</div>',
				width: me.headerWidth,
				sortable: false,
				glyph : me.glyph,
				draggable: false,
				resizable: false,
				hideable: false,
				menuDisabled: true,
				dataIndex: '',
				viewTooltip: me.columnTooltip,
				target: me.columnTarget,
				eventName: me.columnEventName,
				idField: me.columnIdField || 'sys_id',
				// cls: showCheck ? Ext.baseCSSPrefix + 'column-header-checkbox ' : '',
				cls: 'x-grid-group-hd-collapsible',
				editRenderer: me.editRenderer || me.renderEmpty,
				locked: me.hasLockedHeader(),
				childLink: me.childLink,
				childLinkIdProperty: me.childLinkIdProperty,
				listeners: {
					render: function(column, eOpts) {
						var header = column.getEl(),
							hoverCls = Ext.baseCSSPrefix + 'column-header-over';
						header.on('mouseover', function() {
							column.titleEl.addCls(hoverCls);
						});
						header.on('mouseout', function() {
							column.titleEl.removeCls(hoverCls);
						});
					}
				}
			};
		},

		onHeaderClick: function(headerCt, header, e) {
			if (header.dataIndex)
				return;
			var group = this.view.getFeature('group');
			var cache = group.groupCache;
			var anyCollapsed = false;
			for (name in cache) {
				if (cache.hasOwnProperty(name)) {
					anyCollapsed = anyCollapsed || cache[name].isCollapsed
				}
				if (anyCollapsed)
					break;
			}
			group[anyCollapsed ? 'expandAll' : 'collapseAll']();
			this.toggleUiHeader(anyCollapsed);
		}
	},

	listeners: {
		editAction: '@{editProperty}',
		render: function(grid) {
			var store = grid.getStore();
			store.on('load', function() {
				var g = grid;
			});
		}
	}
});