glu.defView('RS.sysadmin.ListRegistration', {
	padding: 15,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls : 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{title}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',			
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon',
			cls : 'rs-small-btn rs-btn-light'	
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],	
	items: [{		
		style : 'border-top:1px solid silver',		
		padding : '5 0',
		defaults : {
			margin : '4 0'
		},
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaultType: 'displayfield',
		items : ['uname', 'uguid', 'utype', 'ustatus', 'uipaddress', 'uversion', {
			xtype: 'textarea',
			name: 'uconfig',
			height: 200,
			overflowY : 'scroll',
			readOnly: true
		}]
	},{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'properties',
		title: '@{displayName}',
		columns: '@{columns}',
		autoScroll : true,
		flex : 1,
		viewConfig: {
			enableTextSelection: true
		}
	}]
});