glu.defView('RS.sysadmin.OrganizationPicker', {
	title: '~~addOrganizationTitle~~',
	width: 600,
	height: 400,
	cls : 'rs-modal-popup',
	layout: 'fit',
	items: [{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'organizations',
		columns: '@{columns}',
		plugins: [{
			ptype: 'pager'
		}],
		selModel: {
			mode: 'MULTI'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar',
			name: 'actionBar',
			items: []
		}]
	}],
	buttons: [{
		name : 'add',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})