glu.defView('RS.sysadmin.ToolbarConfiguration', {
	padding: 15,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls: 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		defaultButtonUI: 'display-toolbar-button',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{toolbarConfTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'save',
			listeners: {
				render: function(button) {
					clientVM.updateSaveButtons(button);
				}
			}
		}, 'addToolbarItem', 'addGroup', {
			name: 'addRoles',
			handler: '@{addRoles}'
		}, 'removeRoles', 'removeToolbarItem', 'reset', '->', {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	xtype: 'treepanel',
	cls : 'rs-grid-dark',
	autoScroll: true,
	name: 'toolbarConfigurations',
	useArrows: true,
	store: '@{toolbarConfigurations}',
	columns: '@{columns}',
	rootVisible: false,
	displayField: 'group',
	selModel: {
		selType: 'checkboxmodel',
		checkOnly: true,
		mode: 'SIMPLE'
	},
	plugins: [{
		ptype: 'cellediting',
		clicksToEdit: 1
	}],
	viewConfig: {
		markDirty: false,
		plugins: {
			ptype: 'treeviewdragdrop'
		},
		listeners: {
			beforedrop: function(node, data, overModel, dropPosition, dropHandlers) {
				this.ownerCt.fireEvent('beforeDropRec', node, data, overModel, dropPosition, dropHandlers);
			}
		}
	},
	listeners: {
		beforeDropRec: '@{checkDrop}',
		beforeEdit: function(editor, e, eOpts) {
			if (!e.record.isLeaf() && e.column.dataIndex == 'query') return false
		}
	}
})
