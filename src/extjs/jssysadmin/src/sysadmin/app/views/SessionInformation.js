glu.defView('RS.sysadmin.SessionInformation', {
	xtype: 'form',
	padding: 15,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar actionBar-form rs-dockedtoolbar',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '~~userInfoTitle~~'
		}, '->', {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	items: [{
		title: '~~sessionSummary~~',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		defaults: {
			flex: 1
		},
		defaultType: 'container',
		items: [{
			defaultType: 'displayfield',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			padding: '10px 0px 0px 0px',
			defaults: {
				labelWidth: 180
			},
			items: ['activeSessions', 'maxActiveSessions']
		}, {
			defaultType: 'displayfield',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults: {
				labelWidth: 180
			},
			padding: '10px 0px 0px 0px',
			items: ['maxSessions', 'timeout']
		}]
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		flex: 1,
		viewConfig: {
			markDirty: false
		},
		autoScroll : true,
		name: 'sessions',
		title: '~~sessionDetails~~',
		store: '@{sessionStore}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			items: [{
				name : 'removeSessions',
				cls : 'rs-small-btn rs-btn-light'
			}]
		}],
		columns: '@{sessionColumns}',
		selModel: {
			selType: 'checkboxmodel',
			mode: 'MULTI'
		}
	}]
});