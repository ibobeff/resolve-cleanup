glu.defView('RS.sysadmin.ControllerPicker', {
	title: '~~addControllerTitle~~',
	modal: true,
	padding : 15,
	listeners: {
		beforeshow: function() {
			clientVM.setPopupSizeToEightyPercent(this);
		}
	},
	layout: 'fit',
	items: [{
		xtype: 'grid',
		name: 'controllers',
		columns: '@{columns}',
		plugins: [{
			ptype: 'pager'
		}],
		selModel: {
			mode: 'MULTI'
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			items: []
		}]
	}],
	buttons: [{
		name : 'add',
		cls : 'rs-small-btn rs-btn-light'
	},{ 
		name : 'cancel',
		cls : 'rs-small-btn rs-btn-light'
	}]
})