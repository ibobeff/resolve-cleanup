glu.defView('RS.sysadmin.ApplicationRight', {
	xtype: 'form',
	padding: 15,	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		cls : 'rs-dockedtoolbar',
		defaultButtonUI: 'display-toolbar-button',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{applicationRightTitle}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name : 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'
		},{
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],

	items: [{
		style : 'border-top:1px solid silver',
		padding : '8 0 4 0',
		items : [{
			name: 'uappName',
			xtype: 'textfield',
			width : 500
		}]	
	}, /*{ //DOMAINS SECTION REMOVED
		title: '~~organizations~~',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		flex: 1,
		name: 'organizations',
		columns: '@{organizationsColumns}',
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items : ['addOrganization', 'removeOrganization']
		}]		
	}, */{
		title: '~~roles~~',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		flex: 1,
		name: 'roles',
		columns: '@{rolesColumns}',
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items : ['addRole', 'removeRole']
		}]	
	}, {
		title: '~~controllers~~',
		xtype: 'grid',
		cls : 'rs-grid-dark',
		flex: 1,
		name: 'controllers',
		columns: '@{controllersColumns}',
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items : ['addController', 'removeController']
		}]	
	}]
})
