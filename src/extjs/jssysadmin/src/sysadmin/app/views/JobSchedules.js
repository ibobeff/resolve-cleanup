glu.defView('RS.sysadmin.JobSchedules', {
	padding: 15,
	xtype: 'grid',
	cls : 'rs-grid-dark',
	name: 'jobSchedules',
	border: false,
	stateId: '@{stateId}',
	displayName: '@{displayName}',
	stateful: true,
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id',
		childLink: 'RS.sysadmin.JobSchedule',
		childLinkIdProperty: 'id'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		name: 'actionBar',
		items: ['createJobSchedule', 'activateJobSchedule', 'deactivateJobSchedule', 'deleteJobSchedule', {
			name: 'executeJobSchedule',
			tooltip: '~~executeJobScheduleTooltip~~'
		}]
	}],
	columns: '@{columns}',
	viewConfig: {
		enableTextSelection: true
	},
	listeners: {
		editAction: '@{editJobSchedule}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
})