Ext.define('RS.sysadmin.model.SystemProperty', {
	extend: 'Ext.data.Model',
	fields: [
		'id',
		'uname',
		'udescription',
		'uvalue',{
			name: 'group',
			convert: function(newValue, model) {
				return RS.common.grid.htmlRenderer()((model.get('uname').split('\.'))[0]);
			}
		}
	].concat(RS.common.grid.getSysFields())
});