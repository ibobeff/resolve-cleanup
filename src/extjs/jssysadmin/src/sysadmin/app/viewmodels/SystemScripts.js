glu.defModel('RS.sysadmin.SystemScripts', {
	mock: false,

	displayName: '',

	state: '',

	stateId: 'sysadminsysscript',

	store: {
		mtype: 'store',
		fields: ['id', 'uname', 'udescription'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'uname',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/sysscript/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	columns: null,

	scriptsSelections: [],

	activate: function() {
		clientVM.setWindowTitle(this.localize('windowTitle'))

		this.loadScripts();
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true);
		this.set('displayName', this.localize('systemScriptsTitle'));
		this.set('columns', [{
			header: '~~Name~~',
			dataIndex: 'uname',
			filterable: true,
			width: 200,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~Description~~',
			dataIndex: 'udescription',
			filterable: true,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}].concat(RS.common.grid.getSysColumns()));
	},

	loadScripts: function() {
		this.set('state', 'wait');
		this.store.load({
			scope: this,
			callback: function(rec, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('LoadErrMsg'));
					return;
				}
				this.set('state', 'ready');
			}
		});
	},

	editScript: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.SystemScript',
			params: {
				id: id
			}
		});
	},

	createScript: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.SystemScript',
			params: {
				id: ''
			}
		});
	},

	deleteScripts: function() {
		this.message({
			title: this.localize('DeleteScriptsTitle'),
			msg: this.localize(this.scriptsSelections.length > 1 ? 'DeleteScriptsMsg' : 'DeleteScriptMsg', this.scriptsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmDelete'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.sendDeleteReq
		});
	},

	sendDeleteReq: function(btn) {
		if (btn != 'yes') {
			this.set('state', 'itemSelected');
			return;
		}

		this.set('state', 'wait');

		var ids = [];

		Ext.each(this.scriptsSelections, function(r) {
			ids.push(r.get('id'));
		})

		this.ajax({
			url: '/resolve/service/sysscript/delete',
			params: {
				ids: ids,
				all: false
			},
			scope: this,
			success: this.handleDeleteSucResp,
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	handleDeleteSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('DeleteErrMsg') + '[' + respData.message + ']');
		} else
			clientVM.displaySuccess(this.localize('DeleteSucMsg'));
		this.loadScripts();
	},

	createScriptIsEnabled$: function() {
		return this.state != 'wait';
	},

	deleteScriptsIsEnabled$: function() {
		return this.state === 'itemSelected';
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	when_scriptsSelections_changed: {
		on: ['scriptsSelectionsChanged'],
		action: function() {
			if (this.scriptsSelections.length > 0)
				this.set('state', 'itemSelected')
			else
				this.set('state', 'ready');
		}
	}
});