glu.defModel('RS.sysadmin.MenuDefinitions', {

	mock: false,

	wait: false,

	activate: function() {
		this.set('wait', false);
		this.menuDefinitions.load()
		clientVM.setWindowTitle(this.localize('menuDefinitionsDisplayName'))
	},

	columns: [],
	menuDefinitionsSelections: [],
	allSelected: false,

	displayName: '',

	menuDefinitions: {
		mtype: 'store',
		sorters: ['order'],
		fields: ['id', 'title', 'roles', {
			name: 'order',
			type: 'int'
		}, {
			name: 'active',
			type: 'bool'
		}, {
			name: 'order',
			type: 'int'
		}].concat(RS.common.grid.getSysFields()),
		sorters: [{
			property: 'order',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/menudefinition/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)

		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'title',
			filterable: true,
			sortable: false,
			flex: 1
		}, {
			header: '~~active~~',
			dataIndex: 'active',
			filterable: true,
			sortable: false,
			align: 'center',
			width: 60,
			renderer: RS.common.grid.booleanRenderer()
		}, {
			header: '~~roles~~',
			dataIndex: 'roles',
			filterable: false,
			sortable: false,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))

		this.set('displayName', this.localize('menuDefinitionsDisplayName'))
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createMenuDefinition: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.MenuDefinition'
		})
	},
	editMenuDefinition: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.MenuDefinition',
			params: {
				id: id
			}
		})
	},
	deleteMenuDefinitionIsEnabled$: function() {
		return this.menuDefinitionsSelections.length > 0
	},
	deleteMenuDefinition: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.menuDefinitionsSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.menuDefinitionsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},

	reallyDeleteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/menudefinition/delete',
					params: {
						ids: '',
						whereClause: this.menuDefinitions.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							clientVM.getUser()
							this.set('menuDefinitionsSelections', [])
							this.menuDefinitions.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.menuDefinitionsSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/menudefinition/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							clientVM.getUser()
							this.set('menuDefinitionsSelections', [])
							this.menuDefinitions.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	},

	definitionDrop: function() {
		//Go through the new list of records, setting up their sequence, and then send the list of ids to the server
		var ids = []
		this.menuDefinitions.each(function(record) {
			ids.push(record.get('id'))
		})

		this.ajax({
			url: '/resolve/service/menudefinition/sequence',
			params: {
				ids: ids
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (!response.success) {
					this.menuDefinitions.load()
					clientVM.displayError(response.message)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	}
})