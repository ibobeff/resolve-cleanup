glu.defModel('RS.sysadmin.ListRegistrations', {

	mock: false,

	activate: function() {
		this.listRegistrations.load()
		clientVM.setWindowTitle(this.localize('listRegistrationsDisplayName'))
	},

	columns: [],
	listRegistrationsSelections: [],
	allSelected: false,

	displayName: '',
	stateId: 'ListRegistrations',

	listRegistrations: {
		mtype: 'store',
		fields: ['id', 'uname', 'ustatus', 'uguid', 'utype', 'uversion', 'uipaddress'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/registration/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)

		var columns = [{
			header: '~~ustatus~~',
			dataIndex: 'ustatus',
			filterable: true,
			sortable: true,
			renderer: RS.common.grid.statusRenderer('rs-listregistration-status')
		}, {
			header: '~~uname~~',
			dataIndex: 'uname',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~uguid~~',
			dataIndex: 'uguid',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~utype~~',
			dataIndex: 'utype',
			filterable: true,
			sortable: true
		}, {
			header: '~~uipaddress~~',
			dataIndex: 'uipaddress',
			filterable: true,
			sortable: true,
			width: 200
		}, {
			header: '~~uversion~~',
			dataIndex: 'uversion',
			filterable: true,
			sortable: true
		}].concat(RS.common.grid.getSysColumns());

		var col, index = -1;
		for (var i = 0; i < columns.length; i++) {
			col = columns[i];
			if (col.dataIndex == 'sysUpdatedOn') {
				col.initialShow = true
				col.hidden = false
				index = i
			}
		}
		//move updatedOn column to be the first column in the grid
		if (index > -1) columns.splice(0, 0, columns.splice(index, 1)[0])
		this.set('columns', columns)

		this.set('displayName', this.localize('listRegistrationsDisplayName'))
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	editListRegistration: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ListRegistration',
			params: {
				id: id
			}
		})
	},
	deleteListRegistrationIsEnabled$: function() {
		return this.listRegistrationsSelections.length > 0
	},

	editListRegistration: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ListRegistration',
			params: {
				id: id
			}
		})
	},

	deleteListRegistration: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.listRegistrationsSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.listRegistrationsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/registration/delete',
					params: {
						ids: '',
						whereClause: this.listRegistrations.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('listRegistrationsSelections', [])
							this.listRegistrations.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.listRegistrationsSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/registration/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('listRegistrationsSelections', [])
							this.listRegistrations.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	}
})