glu.defModel('RS.sysadmin.JobSchedules', {

	mock: false,

	activate: function() {
		this.jobSchedules.load()
		clientVM.setWindowTitle(this.localize('jobSchedulesDisplayName'))
	},

	columns: [],
	jobSchedulesSelections: [],
	allSelected: false,

	displayName: '',
	stateId: 'JobSchedules',

	jobSchedules: {
		mtype: 'store',
		fields: ['id', 'uname', 'urunbook', 'uexpression', 'umodule', {
			name: 'uactive',
			type: 'bool'
		}].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/jobscheduler/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)

		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'uname',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~active~~',
			dataIndex: 'uactive',
			filterable: true,
			sortable: true,
			align: 'center',
			width: 60,
			renderer: RS.common.grid.booleanRenderer()
		}, {
			header: '~~runbook~~',
			dataIndex: 'urunbook',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~expression~~',
			dataIndex: 'uexpression',
			filterable: true,
			sortable: true
		}, {
			header: '~~module~~',
			dataIndex: 'umodule',
			filterable: true,
			sortable: true
		}].concat(RS.common.grid.getSysColumns()))

		this.set('displayName', this.localize('jobSchedulesDisplayName'))
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createJobSchedule: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.JobSchedule'
		})
	},
	editJobSchedule: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.JobSchedule',
			params: {
				id: id
			}
		})
	},
	deleteJobScheduleIsEnabled$: function() {
		return this.jobSchedulesSelections.length > 0
	},
	deleteJobSchedule: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.jobSchedulesSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.jobSchedulesSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/jobscheduler/delete',
					params: {
						ids: '',
						whereClause: this.jobSchedules.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('jobSchedulesSelections', [])
							this.jobSchedules.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.jobSchedulesSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/jobscheduler/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('jobSchedulesSelections', [])
							this.jobSchedules.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	},

	activateJobScheduleIsEnabled$: function() {
		var deactivated = false;
		Ext.Array.each(this.jobSchedulesSelections, function(selection) {
			if (!selection.get('uactive')) {
				deactivated = true
				return false
			}
		})
		return this.jobSchedulesSelections.length > 0 && deactivated
	},
	activateJobSchedule: function() {
		this.message({
			title: this.localize('activateTitle'),
			msg: this.localize(this.jobSchedulesSelections.length == 1 ? 'activateMessage' : 'activatesMessage', this.jobSchedulesSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('activateAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyActivateRecords
		})
	},
	reallyActivateRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/jobscheduler/activateDeactivate',
					params: {
						ids: '',
						whereClause: this.jobSchedules.lastWhereClause || '1=1',
						activateJob: true
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('jobSchedulesSelections', [])
							this.jobSchedules.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.jobSchedulesSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/jobscheduler/activateDeactivate',
					params: {
						ids: ids.join(','),
						activateJob: true
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('jobSchedulesSelections', [])
							this.jobSchedules.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	},

	deactivateJobScheduleIsEnabled$: function() {
		var activated = false;
		Ext.Array.each(this.jobSchedulesSelections, function(selection) {
			if (selection.get('uactive')) {
				activated = true
				return false
			}
		})
		return this.jobSchedulesSelections.length > 0 && activated
	},
	deactivateJobSchedule: function() {
		this.message({
			title: this.localize('deactivateTitle'),
			msg: this.localize(this.jobSchedulesSelections.length == 1 ? 'deactivateMessage' : 'deactivatesMessage', this.jobSchedulesSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deactivateAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeactivateRecords
		})
	},
	reallyDeactivateRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/jobscheduler/activateDeactivate',
					params: {
						ids: '',
						whereClause: this.jobSchedules.lastWhereClause || '1=1',
						activateJob: false
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('jobSchedulesSelections', [])
							this.jobSchedules.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.jobSchedulesSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/jobscheduler/activateDeactivate',
					params: {
						ids: ids.join(','),
						activateJob: false
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('jobSchedulesSelections', [])
							this.jobSchedules.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	},

	executeJobScheduleIsEnabled$: function() {
		return this.jobSchedulesSelections.length > 0
	},
	executeJobSchedule: function() {
		this.message({
			title: this.localize('executeTitle'),
			msg: this.localize(this.jobSchedulesSelections.length == 1 ? 'executeMessage' : 'executesMessage', this.jobSchedulesSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('executeAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyExecuteRecords
		})
	},
	reallyExecuteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/jobscheduler/execute',
					params: {
						ids: '',
						whereClause: this.jobSchedules.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							clientVM.displaySuccess(this.jobSchedulesSelections.length === 1 ? this.localize('executeSuccess') : this.localize('executesSuccess'))
							this.set('jobSchedulesSelections', [])
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.jobSchedulesSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/jobscheduler/execute',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							clientVM.displaySuccess(this.jobSchedulesSelections.length === 1 ? this.localize('executeSuccess') : this.localize('executesSuccess'))
							this.set('jobSchedulesSelections', [])
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	}
})