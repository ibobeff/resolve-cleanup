glu.defModel('RS.sysadmin.RolePicker', {

	mock: false,

	isRemove: false,
	includePublic : "true",
	title$: function() {
		return this.isRemove ? this.localize('removeRoleTitle') : this.localize('addRoleTitle')
	},

	roles: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uname'],
		fields: ['id', 'uname'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/common/roles/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	rolesSelections: [],
	columns: [],

	keyword: '',
	delayedTask : null,
	when_keyword_changed: {
		on: ['keywordChanged'],
		action: function() {
			if(this.delayedTask)
				clearTimeout(this.delayedTask);
			this.delayedTask = setTimeout(function(){
				this.roles.loadPage(1);
			}.bind(this),300)		
		}
	},

	init: function() {
		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'uname',
			filterable: true,
			sortable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))
		this.roles.on('beforeload', function(store, operation, opts) {
			operation.params = operation.params || {};
			var filter = [];
			if (this.keyword) {
				filter = [{
					field: 'uname',
					type: 'auto',
					condition: 'contains',
					value: this.keyword
				}]
			}
			Ext.apply(operation.params, {
				operator: 'or',
				includePublic : this.includePublic,
				filter: Ext.encode(filter)
			})
		}, this);

		this.roles.load()
	},

	cancel: function() {
		this.doClose()
	},

	add: function() {
		if (this.callback)
			this.callback.apply(this.parentVM, [this.rolesSelections])
		else
			Ext.Array.forEach(this.rolesSelections, function(r) {
				if (this.parentVM.roles.indexOf(r) == -1)
					this.parentVM.roles.add(r.data)
			}, this)
		this.doClose()
	},
	addIsEnabled$: function() {
		return this.rolesSelections.length > 0
	},
	addIsVisible$: function() {
		return !this.isRemove
	},
	remove: function() {
		this.add()
	},
	removeIsEnabled$: function() {
		return this.addIsEnabled
	},
	removeIsVisible$: function() {
		return !this.addIsVisible
	}
})