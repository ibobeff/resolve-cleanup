glu.defModel('RS.sysadmin.ControllerPicker', {

	mock: false,

	controllers: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['ucontrollerName'],
		fields: ['id', 'ucontrollerName'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/common/controllers/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	controllersSelections: [],
	columns: [],

	init: function() {
		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'ucontrollerName',
			filterable: true,
			sortable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))
		this.controllers.load()
	},

	cancel: function() {
		this.doClose()
	},

	add: function() {
		Ext.Array.forEach(this.controllersSelections, function(r) {
			if (this.parentVM.controllers.indexOf(r) == -1)
				this.parentVM.controllers.add(r.data)
		}, this)
		this.doClose()
	},
	addIsEnabled$: function() {
		return this.controllersSelections.length > 0
	}
})