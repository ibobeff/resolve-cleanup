glu.defModel('RS.sysadmin.OrganizationPicker', {

	mock: false,

	organizations: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uorganizationName'],
		fields: ['id', 'uorganizationName'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/common/organizationList',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	organizationsSelections: [],
	columns: [],

	init: function() {
		var cols = [{
			header: '~~name~~',
			dataIndex: 'uorganizationName',
			filterable: true,
			sortable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()),
			sysOrgCol = null;
		Ext.Array.forEach(cols, function(col) {
			if (col.dataIndex == 'sysOrganizationName') sysOrgCol = col
		})
		if (sysOrgCol) cols.splice(cols.indexOf(sysOrgCol), 1)
		this.set('columns', cols)
		this.organizations.load()
	},

	cancel: function() {
		this.doClose()
	},

	add: function() {
		Ext.Array.forEach(this.organizationsSelections, function(r) {
			if (this.parentVM.organizations.indexOf(r) == -1)
				this.parentVM.organizations.add(r.data)
		}, this)
		this.doClose()
	},
	addIsEnabled$: function() {
		return this.organizationsSelections.length > 0
	}
})