glu.defModel('RS.sysadmin.SessionInformation', {
	activeSessions: '',
	maxActiveSessions: '',
	maxSessions: '',
	timeout: '',
	wait: false,

	sessionColumns: null,
	sessionsSelections: [],
	sessionStore: {
		mtype: 'store',
		fields: ['username', 'host', {
			name: 'createTime',
			type: 'date',
			format: 'time',
			dateFormat: 'time'
		}, {
			name: 'updateTime',
			type: 'date',
			format: 'time',
			dateFormat: 'time'
		}, 'durationInSecs', 'sessionDurationInMin', 'ssoType', 'ssoSessionId', {
			name: 'lastSSOValidationTime',
			type: 'date',
			format: 'time',
			dateFormat: 'time'
		}, 'iframeId', 'sessionId'
		],
		proxy: {
			type: 'memory'
		}
	},
	fields: ['activeSessions', 'maxActiveSessions', 'maxSessions', 'timeout'],

	activate: function() {},
	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true);
		this.set('sessionColumns', [{
			header: '~~username~~',
			dataIndex: 'username',
			flex: 1
		}, {
			header: '~~host~~',
			dataIndex: 'host',
			flex: 1
		}, {
			header: '~~createTime~~',
			dataIndex: 'createTime',
			flex: 1,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}, {
			header: '~~updateTime~~',
			dataIndex: 'updateTime',
			flex: 1,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}, {
			header: '~~durationInSecs~~',
			dataIndex: 'durationInSecs',
			flex: 1
		}, {
			header: '~~sessionDurationInMins~~',
			dataIndex: 'sessionDurationInMin',
			flex: 1
		}, {
			header: '~~ssoType~~',
			dataIndex: 'ssoType',
			flex: 1
		}, {
			header: '~~ssoSessionId~~',
			dataIndex: 'ssoSessionId',
			flex: 1
		}, {
			header: '~~lastSSOValidationTime~~',
			dataIndex: 'lastSSOValidationTime',
			flex: 1,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}, {
			header: '~~iframeId~~',
			dataIndex: 'iframeId',
			flex: 1
		}, {
			header: '~~sessionId~~',
			dataIndex: 'sessionId',
			flex: 1
		}]);

		this.loadUserSessionInformation();
	},
	loadUserSessionInformation: function() {
		this.sessionStore.removeAll()
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/user/sessioninfo/get',
			success: function(resp) {
				this.set('wait', false);
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('LoadUserSessionInfoErrMsg'), respData.message);
					return;
				}
				var info = respData.data;
				this.loadData(info);
				this.sessionStore.add(info.sessionDetails);
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		});
	},
	removeSessions: function() {
		this.message({
			title: this.localize('RemoveSessionTitle'),
			msg: this.sessionsSelections.length === 1 ? this.localize('RemoveSessionMsg') : this.localize('RemoveSessionMsgs', this.sessionsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('remove'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn == 'no')
					return;
				var ids = [];
				this.set('wait', true);
				Ext.each(this.sessionsSelections, function(s) {
					ids.push(s.get('id'));
				}, this)
				this.ajax({
					url: '/resolve/service/user/sessioninfo/delete',
					params: {
						ids: ids
					},
					scope: this,
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						if (!respData.success) {
							clientVM.displayError(this.localize('DeleteErrMsg') + '[' + respData.message + ']');
							return;
						}

						clientVM.displaySuccess(this.localize('DeleteSucMsg'));
						this.loadUserSessionInformation();
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					},
					callback: function() {
						this.set('wait', false);
					}
				});
			}
		})
	},

	removeSessionsIsEnabled$: function() {
		return !this.wait && this.sessionsSelections.length > 0;
	},

	refresh: function() {
		this.loadUserSessionInformation();
	}
});