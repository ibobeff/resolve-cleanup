glu.defModel('RS.sysadmin.ListRegistration', {

	mock: false,

	title: '',

	stateId: 'listRegistrationProperties',
	displayName: 'Properties',
	columns: [{
		header: '~~uname~~',
		dataIndex: 'uname',
		filterable: true,
		sortable: true,
		flex: 1
	}, {
		header: '~~utype~~',
		dataIndex: 'utype',
		filterable: true,
		sortable: true,
		flex: 1
	}, {
		header: '~~uvalue~~',
		dataIndex: 'uvalue',
		filterable: true,
		sortable: true,
		flex: 1
	}],

	properties: {
		mtype: 'store',
		fields: ['uname', 'utype', 'uvalue'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		},
		data: [{
			uname: 'test'
		}]
	},

	listRegistrationTitle$: function() {
		return this.localize('listRegistration') + (this.name ? ' - ' + this.name : '')
	},

	activate: function(screen, params) {
		this.resetForm()
		this.set('id', params ? params.id : '')
		this.loadListRegistration()
		clientVM.setWindowTitle(this.localize('title'))
	},

	resetForm: function() {
		this.loadData(this.defaultData)
	},

	fields: [
		'id',
		'uname',
		'uguid',
		'utype',
		'ustatus',
		'uipaddress',
		'uconfig',
		'uversion',
		'sys_id',
		'sysCreatedOn',
		'sysCreatedBy',
		'sysUpdatedOn',
		'sysUpdatedBy',
		'sysOrganizationName'
	],

	id: '',
	uname: '',
	uguid: '',
	utype: '',
	ustatus: '',
	uipaddress: '',
	uconfig: '',
	uversion: '',
	sys_id: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrganizationName: '',


	defaultData: {
		uname: '',
		uguid: '',
		utype: '',
		ustatus: '',
		uipaddress: '',
		uconfig: '',
		uversion: '',
		sys_id: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrganizationName: ''
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true);
		this.set('title', this.localize('title'));
	},

	loadListRegistration: function() {
		if (this.id) {
			// this.properties.removeAll()
			this.ajax({
				url: '/resolve/service/registration/get',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.loadData(response.data)
						// this.properties.add(response.data.resolveRegistrationProperties)
						Ext.Array.forEach(response.data.resolveRegistrationProperties, function(property) {
							this.properties.add(property)
						}, this)
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},

	refresh: function() {
		this.loadListRegistration()
	},

	saveListRegistration: function(exitAfterSave) {
		this.ajax({
			url: '/resolve/service/registration/save',
			jsonData: this.asObject(),
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize('ListRegistrationSaved'))
					if (exitAfterSave === true) clientVM.handleNavigation({
						modelName: 'RS.sysadmin.ListRegistrations'
					})
					else {
						this.set('id', response.data.id)
					}
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('persisting', false);
			}
		})
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ListRegistrations'
		})
	}
})