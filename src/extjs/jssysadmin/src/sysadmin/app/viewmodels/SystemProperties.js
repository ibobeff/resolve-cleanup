glu.defModel('RS.sysadmin.SystemProperties', {

	mock: false,

	stateId: 'sysadminsystemproperties',
	waitResponse: false,

	propertiesStore: {
		mtype: 'store',
		model: 'RS.sysadmin.model.SystemProperty',
		pageSize: -1,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/sysproperties/getAllProperties',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	propertiesSelections: [],

	columns: null,

	displayName: '',

	isRoot$: function() {
		return clientVM.isRootUser
	},

	activate: function() {
		clientVM.setWindowTitle(this.localize('windowTitle'))

	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true);
		this.loadRecs();
		this.set('displayName', this.localize('systemPropertiesTitle'));
		this.set('columns', [{
			header: '~~uname~~',
			dataIndex: 'uname',
			filterable: true,
			width: 230,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~uvalue~~',
			dataIndex: 'uvalue',
			sortable: false,
			filterable: true,
			groupable: false,
			width: 230,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~udescription~~',
			dataIndex: 'udescription',
			filterable: false,
			sortable: false,
			groupable: false,
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}].concat(RS.common.grid.getSysColumns()));
	},

	loadRecs: function() {
		delete this.propertiesStore.sortInfo;
		this.propertiesStore.load({
			scope: this,
			callback: function(records, op, success) {
				if (!success) {
					//clientVM.displayError(this.localize('getListErrorMsg'));
				} else {
					this.propertiesStore.sort([{
						property: 'uname'
					}]);
					this.propertiesStore.group([{
						property: 'group'
					}]);
				}

				this.set('waitResponse', false);
			}
		});
		this.set('waitResponse', true);
	},

	attachGroup: function(records) {
		var name = null;
		var tokens = null;
		for (var idx = 0; idx < records.length; idx++) {
			name = records[idx].name;
			tokens = name.split('\.');
			records.group = tokens;
		}
	},

	createProperty: function() {
		this.open({
			mtype: 'RS.sysadmin.SystemProperty',
			id: ''
		});
	},

	editProperty: function(id) {
		this.open({
			mtype: 'RS.sysadmin.SystemProperty',
			id: id
		});
	},

	deleteProperties: function() {

		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.propertiesSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', {
				len: this.propertiesSelections.length
			}) + '<br/>',
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteProperties
		});
	},

	reallyDeleteProperties: function(btn) {
		if (btn != 'yes')
			return;
		var ids = [];
		for (var i = 0; i < this.propertiesSelections.length; i++) {
			ids.push(this.propertiesSelections[i].data.id);
		}
		// ids = ids.join(',');
		this.set('waitResponse', true);
		this.ajax({
			url: '/resolve/service/sysproperties/deleteProperties',
			jsonData: ids,
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('deleteFailureMsg') + '[' + respData.message + ']');
				} else
					clientVM.displaySuccess(this.localize('deleteSucMsg'));
				this.loadRecs();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('waitResponse', false);
			}
		});
	},

	createPropertyIsEnabled$: function() {
		return !this.waitResponse;
	},

	deletePropertiesIsEnabled$: function() {
		return this.propertiesSelections.length > 0 && !this.waitResponse;
	},

	updateSystemProperties: function() {
		this.loadRecs();
	}
});