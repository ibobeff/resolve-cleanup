glu.defModel('RS.sysadmin.BusinessRules', {

	mock: false,

	state: '',
	stateId: 'rssysadminbussinessrules',

	displayName: '',
	store: {
		mtype: 'store',
		fields: ['id', 'sys_id', 'uname', 'utable', {
				name: 'uasync',
				type: 'bool'
			}, {
				name: 'uactive',
				type: 'bool'
			}, {
				name: 'ubefore',
				type: 'bool'
			}, 'utype', {
				name: 'uorder',
				type: 'float'
			}, {
				name: 'sysCreatedOn',
				type: 'date',
				format: 'time',
				dateFormat: 'time'
			}, 'sysOrganizationName',
			'sysCreatedBy', {
				name: 'sysUpdatedOn',
				type: 'date',
				format: 'time',
				dateFormat: 'time'
			}, 'sysUpdatedBy'
		],
		remoteSort: true,
		sorters: [{
			property: 'uname',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/businessrules/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	columns: null,
	businessRulesSelections: [],
	activate: function() {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.loadRecs();
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true);
		this.set('displayName', this.localize('displayName'));
		this.set('columns', [{
			header: '~~Name~~',
			dataIndex: 'uname',
			sortable: true,
			filterable: true,
			width: 190,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~Table~~',
			dataIndex: 'utable',
			filterable: true,
			flex: 1
		}, {
			header: '~~NonBlocking~~',
			dataIndex: 'uasync',
			filterable: true,
			width: 105,
			renderer: RS.common.grid.booleanRenderer()
		}, {
			header: '~~Active~~',
			dataIndex: 'uactive',
			filterable: true,
			width: 105,
			renderer: RS.common.grid.booleanRenderer()
		}, {
			header: '~~Before~~',
			dataIndex: 'ubefore',
			filterable: true,
			width: 105,
			renderer: RS.common.grid.booleanRenderer()
		}, {
			header: '~~Type~~',
			dataIndex: 'utype',
			filterable: true,
			width: 200
		}, {
			header: '~~Order~~',
			dataIndex: 'uorder',
			filterable: true,
			width: 60
		}, {
			header: '~~sysCreatedOn~~',
			dataIndex: 'sysCreatedOn',
			hidden: true,
			filterable: true,
			width: 200,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}, {
			header: '~~sysCreatedBy~~',
			dataIndex: 'sysCreatedBy',
			hidden: true,
			filterable: true,
			width: 120
		}, {
			header: '~~sysUpdatedOn~~',
			dataIndex: 'sysUpdatedOn',
			filterable: 1,
			hidden: true,
			width: 200,
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat())
		}, {
			header: '~~sysUpdatedBy~~',
			dataIndex: 'sysUpdatedBy',
			hidden: true,
			filterable: true,
			width: 120
		}, {
			header: '~~sysId~~',
			dataIndex: 'sys_id',
			hidden: true,
			filterable: true,
			width: 300
		}]);
	},

	loadRecs: function() {
		this.set('state', 'wait');
		this.store.load({
			scope: this,
			callback: function(recs, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('ListBusinessRulesErrMsg'));
					return;
				}

				this.set('state', 'ready');
			}
		});
	},

	createBusinessRule: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.BusinessRule',
			params: {
				id: ''
			}
		});
	},

	deleteBusinessRules: function() {
		this.message({
			title: this.localize('DeleteBusinessRulesTitle'),
			msg: this.localize(this.businessRulesSelections.length > 0 ? 'DeleteBusinessRulesMsg' : 'DeleteBusinessRuleMsg', this.businessRulesSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmDelete'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.sendDeleteReq
		});
	},

	sendDeleteReq: function(btn) {
		if (btn != 'yes')
			return;
		this.set('state', 'wait');

		var ids = [];

		Ext.each(this.businessRulesSelections, function(r) {
			ids.push(r.get('id'));
		}, this)

		this.ajax({
			url: '/resolve/service/businessrules/delete',
			jsonData: ids,
			scope: this,
			success: this.handleSucDeleteResp,
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		})
	},

	handleSucDeleteResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('DeleteBusinessRulesErrMsg') + '[' + respData.message + ']');
		} else
			clientVM.displaySuccess(this.localize('DeleteBusinessRulesSucMsg'));
		this.loadRecs();
	},

	editBusinessRule: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.BusinessRule',
			params: {
				id: id
			}
		});
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createBusinessRuleIsEnabled$: function() {
		return this.state != 'wait';
	},

	deleteBusinessRulesIsEnabled$: function() {
		return this.state == 'itemSelected';
	},

	when_businessRulesSelections_changed: {
		on: ['businessRulesSelectionsChanged'],
		action: function() {
			if (this.businessRulesSelections.length > 0)
				this.set('state', 'itemSelected');
			else
				this.set('state', 'ready');
		}
	}
});