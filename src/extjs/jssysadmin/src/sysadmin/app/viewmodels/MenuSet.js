glu.defModel('RS.sysadmin.MenuSet', {

	mock: false,

	MenuSetTitle$: function() {
		return this.localize('title') + (this.name ? ' - ' + this.name : '')
	},

	activate: function(screen, params) {
		this.resetForm()
		this.set('id', params ? params.id : '')
		this.loadMenuSet()
		clientVM.setWindowTitle(this.localize('title'))
	},

	resetForm: function() {
		this.loadData(this.defaultData)
		this.roles.removeAll()
		this.menus.removeAll()
	},

	fields: [
		'id',
		'name', {
			name: 'active',
			type: 'bool'
		}
	].concat(RS.common.grid.getSysFields()),

	id: '',
	name: '',
	active: false,
	sysCreatedOn: '',
	sysCreatedOnDisplay$: function() {
		return Ext.isDate(this.sysCreatedOn) ? Ext.Date.format(this.sysCreatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysUpdatedOn: '',
	sysUpdatedOnDisplay$: function() {
		return Ext.isDate(this.sysUpdatedOn) ? Ext.Date.format(this.sysUpdatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysCreatedBy: '',
	sysUpdatedBy: '',

	sysOrganizationName: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	defaultData: {
		name: '',
		active: false,
		sys_id: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrganizationName: ''
	},

	menusColumns: [],
	rolesColumns: [],

	nameIsValid$: function() {
		if (!this.name) return this.localize('nameInvalid')
		return true
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)

		this.set('rolesColumns', [{
			header: '~~name~~',
			dataIndex: 'value',
			filterable: true,
			sortable: true,
			flex: 1
		}]);

		this.set('menusColumns', [{
			header: '~~name~~',
			dataIndex: 'displayName',
			filterable: true,
			sortable: false,
			flex: 1
		}]);
	},

	loadMenuSet: function() {
		if (this.id) {
			this.roles.removeAll()
			this.menus.removeAll()
			this.ajax({
				url: '/resolve/service/menuset/get',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.loadData(response.data)
						this.set('active', response.data.active)
						this.roles.add(response.data.sysPerspectiveroles || [])
						this.menus.add(response.data.sysPerspectiveapplications || [])
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},

	refresh: function() {
		this.loadMenuSet()
	},

	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.persistMenuSet, this, [exitAfterSave])
		this.task.delay(500)
		this.set('persisting', true)
	},
	saveIsEnabled$: function() {
		return !this.persisting && this.isValid
	},
	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	persisting: false,
	persistMenuSet: function(exitAfterSave) {
		var roles = [],
			menus = [],
			organizations = [];

		this.roles.each(function(role) {
			roles.push(role.get('value'))
		})

		this.menus.each(function(menu, index) {
			// menu.data.sequence = index
			delete menu.data.id
			menus.push(menu.data)
		})

		this.ajax({
			url: '/resolve/service/menuset/save',
			jsonData: {
				id: this.id,
				name: this.name,
				roles: roles,
				active: this.active,
				sysPerspectiveapplications: menus
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.getUser()
					clientVM.displaySuccess(this.localize('MenuSetSaved'))
					if (exitAfterSave === true) clientVM.handleNavigation({
						modelName: 'RS.sysadmin.MenuSets'
					})
					else {
						this.set('id', response.data.id)
						RS.common.grid.updateSysFields(this, response.data)
					}
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('persisting', false);
			}
		})
	},

	back: function() {
		clientVM.handleNavigationBack()
	},

	roles: {
		mtype: 'store',
		fields: ['id', 'value'],
		proxy: {
			type: 'memory'
		}
	},
	rolesSelections: [],
	rolesColumns: [],

	addRole: function() {
		this.open({
			mtype: 'RS.sysadmin.RolePicker',
			callback: this.addNewRole
		})
	},
	addNewRole: function(newRoles) {
		Ext.Array.forEach(newRoles, function(role) {
			if (this.roles.findExact('value', role.get('uname')) == -1) this.roles.add({
				value: role.get('uname')
			})
		}, this)
	},
	removeRole: function() {
		Ext.Array.forEach(this.rolesSelections, function(r) {
			this.roles.remove(r)
		}, this)
	},
	removeRoleIsEnabled$: function() {
		return this.rolesSelections.length > 0
	},

	menus: {
		mtype: 'store',
		sorters: ['sequence'],
		fields: ['id', 'displayName', 'value', {
			name: 'sequence',
			type: 'int'
		}],
		proxy: {
			type: 'memory'
		}
	},
	menusSelections: [],
	menusColumns: [],

	addMenu: function() {
		this.open({
			mtype: 'RS.sysadmin.MenuPicker'
		})
	},
	removeMenu: function() {
		Ext.Array.forEach(this.menusSelections, function(c) {
			this.menus.remove(c)
		}, this)
	},
	removeMenuIsEnabled$: function() {
		return this.menusSelections.length > 0
	}
})