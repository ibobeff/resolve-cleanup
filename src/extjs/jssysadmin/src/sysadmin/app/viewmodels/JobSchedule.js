glu.defModel('RS.sysadmin.JobSchedule', {

	mock: false,

	jobScheduleTitle$: function() {
		return this.localize('jobSchedule') + (this.name ? ' - ' + this.name : '')
	},

	activate: function(screen, params) {
		this.resetForm()
		this.set('id', params ? params.id : '')
		this.loadJobSchedule()
	},

	resetForm: function() {
		this.loadData(this.defaultData)
		this.set('dayType', this.defaultData.dayType)
		this.parametersStore.removeAll()
	},

	defaultData: {
		uname: '',
		umodule: '',
		urunbook: '',
		uactive: true,
		uexpression: '',
		seconds: '*',
		minutes: '*',
		hours: '*',
		month: '*',
		dayType: false,
		dayOfMonth: '*',
		dayOfWeek: '*',
		uparams: '',

		ustartTime: '',
		uendTime: ''
	},

	fields: ['id', 'uname', 'umodule', 'urunbook', 'uactive', 'uexpression', 'seconds', 'minutes', 'hours', 'month', 'dayOfMonth', 'dayOfWeek', 'uparams', 'ustartTime', 'uendTime'].concat(RS.common.grid.getSysFields()),
	id: '',
	uname: '',
	umodule: '',
	urunbook: '',
	uactive: true,
	uexpression: '',
	seconds: '',
	secondsIsValid$: function() {
		return this.seconds ? true : this.localize('secondsInvalid')
	},
	minutes: '',
	minutesIsValid$: function() {
		return this.minutes ? true : this.localize('minutesInvalid')
	},
	hours: '',
	hoursIsValid$: function() {
		return this.hours ? true : this.localize('hoursInvalid')
	},
	month: '',
	monthIsValid$: function() {
		return this.month ? true : this.localize('monthInvalid')
	},
	dayType: false,
	dayOfMonth: '*',
	dayOfMonthIsVisible$: function() {
		return this.dayOfMonthRadio
	},
	dayOfWeek: '*',
	dayOfWeekIsVisible$: function() {
		return this.dayOfWeekRadio
	},
	dayOfWeekRadio: false,
	dayOfMonthRadio: true,
	when_radios_change_update_dayType: {
		on: ['dayOfWeekRadioChanged', 'dayOfMonthRadioChanged'],
		action: function() {
			if (this.dayOfWeekRadio) this.set('dayType', true)
			else this.set('dayType', false)
		}
	},
	when_dayType_change_update_radios: {
		on: ['dayTypeChanged'],
		action: function() {
			if (this.dayType) {
				this.set('dayOfMonthRadio', false)
				this.set('dayOfWeekRadio', true)
			} else {
				this.set('dayOfMonthRadio', true)
				this.set('dayOfWeekRadio', false)
			}
		}
	},
	uparams: '',

	ustartTime: new Date(),
	uendTime: new Date(),

	sysCreatedOn: '',
	sysCreatedOnDisplay$: function() {
		return Ext.isDate(this.sysCreatedOn) ? Ext.Date.format(this.sysCreatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysUpdatedOn: '',
	sysUpdatedOnDisplay$: function() {
		return Ext.isDate(this.sysUpdatedOn) ? Ext.Date.format(this.sysUpdatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysCreatedBy: '',
	sysUpdatedBy: '',

	sysOrganizationName: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	unameIsValid$: function() {
		if (!this.uname) return this.localize('nameInvalid')
		return true
	},

	unameIsVisible$: function() {
		return !this.id
	},

	unameDisplay$: function() {
		return this.uname
	},
	unameDisplayIsVisible$: function() {
		return this.id
	},

	parametersStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	parametersSelections: [],

	parametersColumns: [{
		dataIndex: 'name',
		header: '~~name~~',
		flex: 1,
		editor: {}
	}, {
		dataIndex: 'value',
		header: '~~value~~',
		flex: 1,
		editor: {}
	}],

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)
	},

	loadJobSchedule: function() {
		if (this.id)
			this.ajax({
				url: '/resolve/service/jobscheduler/get',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.loadData(response.data)
						this.set('uactive', response.data.uactive)
						this.set('ustartTime', response.data.ustartTime ? Ext.Date.parse(response.data.ustartTime, 'time') : '')
						this.set('uendTime', response.data.uendTime ? Ext.Date.parse(response.data.uendTime, 'time') : '')
						var params = Ext.Object.fromQueryString(this.uparams)
						Ext.Object.each(params, function(key) {
							this.parametersStore.add({
								name: key,
								value: params[key]
							})
						}, this)

						var expressions = Ext.String.trim(this.uexpression).split(' ')
						if (expressions.length == 6) {
							this.set('seconds', expressions[0])
							this.set('minutes', expressions[1])
							this.set('hours', expressions[2])
							this.set('dayOfMonth', expressions[3])
							this.set('month', expressions[4])
							this.set('dayOfWeek', expressions[5])

							if (this.dayOfMonth == '?') {
								this.set('dayType', true)
								this.set('dayOfMonth', '*')
							}
							if (this.dayOfWeek == '?') {
								this.set('dayType', false)
								this.set('dayOfWeek', '*')
							}
						}

					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
	},

	refresh: function() {
		this.parametersStore.removeAll();
		this.loadJobSchedule();
	},

	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.persistJobSchedule, this, [exitAfterSave])
		this.task.delay(500)
		this.set('persisting', true)
	},
	saveIsEnabled$: function() {
		return !this.persisting && this.isValid
	},
	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	persisting: false,
	persistJobSchedule: function(exitAfterSave) {
		var params = {}
		this.parametersStore.each(function(record) {
			if (record.get('name'))
				params[record.get('name')] = record.get('value')
		})
		this.set('uparams', Ext.Object.toQueryString(params))

		//calculate expression
		var expression = []
		expression.push(this.seconds)
		expression.push(this.minutes)
		expression.push(this.hours)
		expression.push(this.dayType ? '?' : this.dayOfMonth)
		expression.push(this.month)
		expression.push(this.dayType ? this.dayOfWeek : '?')
		this.set('uexpression', expression.join(' '))

		var data = {
			id: this.id,
			uname: this.uname,
			uactive: this.uactive,
			umodule: this.umodule,
			urunbook: this.urunbook,
			active: this.active,
			uexpression: this.uexpression,
			ustartTime: this.ustartTime,
			uendTime: this.uendTime,
			uparams: this.uparams
		}
		this.ajax({
			url: '/resolve/service/jobscheduler/save',
			jsonData: data,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize('JobScheduleSaved'))
					if (exitAfterSave === true) clientVM.handleNavigation({
						modelName: 'RS.sysadmin.JobSchedules'
					})
					else {
						this.set('id', response.data.id)
						//this.loadData(response.data)
					}
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('persisting', false);
			}
		})
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.JobSchedules'
		})
	},

	addParam: function() {
		var max = 0;
		var reg = /^NewParam(\d*)$/;
		var newParam = 'NewParam';

		// determine a new unique "NewParam" name
		this.parametersStore.each(function(param) {
			if (!reg.test(param.get('name')))
				return;
			var m = reg.exec(param.get('name'));
			var idx = parseInt(m[1]);
			if (idx > max)
				max = idx;
		}, this);
		newParam += ((max + 1) || '');

		// add "NewParam" to Parameters list
		this.parametersStore.add({
			name: newParam,
			value: ''
		});

		// clear out all previously selected items
		this.parametersStore.grid.getSelectionModel().deselectAll();

		// determine row of newly added "NewParam"
		var row;
		for (row = this.parametersStore.data.items.length-1; row > 0; row--) {
			if (this.parametersStore.data.items[row].data.name == newParam) {
				break;
			}
		}
		// start editing the newly selected item
		this.parametersStore.grid.getPlugin().startEditByPosition({
			row: row,
			column: 0
		});
	},
	removeParam: function() {
		Ext.Array.forEach(this.parametersSelections, function(selection) {
			this.parametersStore.remove(selection)
		}, this)
	},
	removeParamIsEnabled$: function() {
		return this.parametersSelections.length > 0
	},

	executeJobSchedule: function() {
		this.message({
			title: this.localize('executeTitle'),
			msg: this.localize('executeMessage'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('executeAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyExecuteRecords
		})
	},
	executeJobScheduleIsEnabled$: function() {
		return this.id
	},
	reallyExecuteRecords: function(btn) {
		if (btn == 'yes') {
			this.ajax({
				url: '/resolve/service/jobscheduler/execute',
				params: {
					ids: [this.id]
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						clientVM.displaySuccess(this.localize('executeSuccess'))
					} else clientVM.displayError(response.message)
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
	}
})