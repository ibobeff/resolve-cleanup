glu.defModel('RS.sysadmin.SystemProperty', {
	id: '',
	sys_id: '',
	uname: '',
	udescription: '',
	uvalue: '',
	title: '',
	sysOrganizationName: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	userDateFormat: null,

	defaultData: {
		id: '',
		uname: '',
		sys_id: '',
		udescription: '',
		uvalue: '',
		sysOrganizationName: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: ''
	},



	fields: ['id', 'uname', 'uvalue', 'udescription', 'sysOrganizationName', 'sysCreatedOn', 'sysCreatedBy', 'sysUpdatedOn', 'sysUpdatedBy'],

	waitingResponse: false,
	dockedItems: [{
		xtype: 'toolbar',
		items: ['saveProperty', 'deleteProperty', 'cancel']
	}],

	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		var id = this.id || ''
		this.resetForm()
		this.set('id', params && params.id != null ? params.id : id);
		if (this.id != '') {
			this.loadSysProp();
			return;
		}
	},

	init: function() {
		if ( !! this.id) {
			this.set('title', this.localize('editSystemPropertyTitle'));
			this.loadSysProp();
		} else {
			this.resetForm();
			this.set('title', this.localize('createSystemPropertyTitle'));
		}

	},

	loadSysProp: function() {
		this.ajax({
			url: '/resolve/service/sysproperties/getSystemProperty',
			params: {
				id: this.id
			},
			scope: this,
			success: function(resp, opt) {
				var respData = RS.common.parsePayload(resp);
				var data = respData.data;
				if (!respData.success) {
					clientVM.displayError(this.localize('GetSysPropErrMsg') + '[' + respData.message + ']');
					return;
				}
				this.setData(data);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('waitingResponse', false);
			}
		});
		this.set('waitingResponse', true);
	},

	unameIsValid$: function() {
		var rexp1 = /^[A-Za-z0-9_ ]+[\\.]([A-Za-z0-9_ ]+[\\.])*[A-Za-z0-9_ ]+$/;
		var rexp2 = /^[A-Za-z0-9_ ]+$/;
		return this.uname != null &&
			this.uname.length > 0 &&
			(rexp1.test(this.uname) || rexp2.test(this.uname)) &&
			this.uname.length <= 333 ? true : this.localize('invalidNameErr');
	},
	udescriptionIsValid$: function() {
		return this.udescription.length <= 4000 ? true : this.localize('invalidDescriptionErr');
	},

	uvalueIsValid$: function() {
		return true //this.uvalue.length <= 333 ? true : this.localize('invalidValueErr');
	},

	save: function(exitAfterSave) {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveProperty, this, [exitAfterSave])
		this.task.delay(500)
		this.set('waitingResponse', true)
	},
	saveIsEnabled$: function() {
		return !this.waitingResponse;
	},
	saveAndExit: function() {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.save(true)
	},

	saveProperty: function(exitAfterSave) {
		var systemProperty = {
			sys_id: this.id,
			UName: this.uname,
			UDescription: this.udescription,
			UValue: this.uvalue
		};
		if (this.parentVM.isRoot)
			systemProperty.sysOrg = this.sysOrg;
		this.ajax({
			url: '/resolve/service/sysproperties/saveSystemProperty',
			jsonData: systemProperty,
			scope: this,
			success: function(resp, opt) {
				var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					this.setData(respData.data);
					this.parentVM.updateSystemProperties();
					clientVM.displaySuccess(this.localize('saveSuccess'));
					// if (exitAfterSave === true)
					this.doClose();
				} else
					clientVM.displayError(this.localize('SaveSysPropErrMsg') + '[' + respData.message + ']');
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('waitingResponse', false);
			}
		});
		this.set('waitingResponse', true);
	},

	resetForm: function() {
		this.loadData(this.defaultData);
		this.set('isPristine', true);
	},

	cancel: function() {
		this.doClose();
	},

	toParent: function() {
		this.doClose();
	},

	deletePropertyIsVisible$: function() {
		if (this.id === '')
			return false;
		else
			return true;
	},

	setData: function(data) {
		this.loadData(data);
		this.commit();
	},

	refresh: function() {
		if (this.id != null && this.id != '')
			this.loadSysProp();
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	savePropertyIsEnabled$: function() {
		return this.isValid && !this.waitingResponse && this.isDirty;
	},

	savePropertyIsVisible$: function() {
		return this.parentVM.isRoot;
	},

	cancelIsVisible$: function() {
		return this.parentVM.isRoot;
	},

	toParentIsVisible$: function() {
		return !this.parentVM.isRoot;
	},

	editable$: function() {
		return !this.parentVM.isRoot;
	}
});