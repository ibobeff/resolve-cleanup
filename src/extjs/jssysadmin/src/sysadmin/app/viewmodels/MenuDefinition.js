glu.defModel('RS.sysadmin.MenuDefinition', {
	mock: false,
	fields: [
		'id',
		'title', {
			name: 'active',
			type: 'bool'
		},
		'color',
		'textColor'
	].concat(RS.common.grid.getSysFields()),
	menuDefinitionTitle$: function() {
		return this.localize('menuDefTitle') + (this.title ? ' - ' + this.title : '')
	},
	activate: function(screen, params) {
		this.set('persisting', false);
		this.resetForm()
		this.set('id', params ? params.id : '')
		this.loadMenuDefinition()
	},
	resetForm: function() {
		this.loadData(this.defaultData)
		this.roles.removeAll()
		this.menuItems.setRootNode({
			children: []
		});
	},	
	bgStyle$: function() {
		var demo = 'border:1px solid #000000;background-color:#' + (this.color || '0088CC');
		return '<div style="height:15px;width:15px;' + demo + '"></div>'
	},
	selectBgColor: function(color) {
		this.set('color', color);
	},
	textColor: 'FFFFFF',
	txtStyle$: function() {
		var demo = 'border:1px solid #000000;background-color:#' + (this.textColor || 'FFFFFF');
		return '<div style="height:15px;width:15px;' + demo + '"></div>'
	},
	selectTxtColor: function(color) {
		this.set('textColor', color)
	},
	active: false,
	sysCreatedOn: '',
	sysCreatedOnDisplay$: function() {
		return Ext.isDate(this.sysCreatedOn) ? Ext.Date.format(this.sysCreatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysUpdatedOn: '',
	sysUpdatedOnDisplay$: function() {
		return Ext.isDate(this.sysUpdatedOn) ? Ext.Date.format(this.sysUpdatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysCreatedBy: '',
	sysUpdatedBy: '',

	sysOrganizationName: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	defaultData: {
		title: '',
		active: true,
		sys_id: '',
		color: '00CCFF',
		textColor: 'FFFFFF',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrganizationName: ''
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	titleIsValid$: function() {
		if (!this.title) return this.localize('titleInvalid')
		return true
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)
		this.set('rolesColumns', [{
			header: '~~name~~',
			dataIndex: 'name',
			filterable: true,
			sortable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))

		this.set('menuItemsColumns', [{
			xtype: 'treecolumn',
			header: '~~name~~',
			dataIndex: 'title',
			filterable: false,
			sortable: false,
			flex: 1,
			editor: {
				xtype: 'textfield',
				allowBlank: false
			}
		}, {
			header: '~~active~~',
			dataIndex: 'active',
			filterable: false,
			sortable: false,
			width: 60,
			align: 'center',
			renderer: RS.common.grid.booleanRenderer()
		}, {
			header: '~~query~~',
			dataIndex: 'query',
			filterable: false,
			sortable: false,
			flex: 1,
			editor: {}
		}, {
			header: '~~roles~~',
			dataIndex: 'rolesString',
			filterable: false,
			sortable: false,
			flex: 1
		}])
	},

	loadMenuDefinition: function() {
		if (this.id) {
			this.roles.removeAll()
			this.ajax({
				url: '/resolve/service/menudefinition/get',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.loadData(response.data)
						var groups = {},
							rootChildren = [];
						Ext.each(response.data.sysAppModules, function(r) {
							r.leaf = true;
							r.group = r.appModuleGroup;
							if (r.group && !groups[r.group])
								groups[r.group] = {
									id: r.group,
									isGroup: true,
									title: r.group,
									group: r.group,
									expanded: true,
									children: []
								}
							r.rolesString = (r.roles || []).join(', ')
							if (r.group)
								groups[r.group].children.push(r)
							else rootChildren.push(r)
						})

						Ext.Object.each(groups, function(group) {
							rootChildren.push(groups[group])
						})

						this.menuItems.setRootNode({
							children: rootChildren
						})

						this.menuItems.getRootNode().expand()

						var responseRoles = [];
						Ext.Array.forEach(response.data.roles || [], function(role) {
							responseRoles.push({
								name: role
							})
						})
						this.roles.add(responseRoles)
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},

	refresh: function() {
		this.loadMenuDefinition()
	},

	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.persistMenuDefinition, this, [exitAfterSave])
		this.task.delay(500)
		this.set('persisting', true)
	},
	saveIsEnabled$: function() {
		return !this.persisting && this.isValid
	},
	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	persisting: false,
	persistMenuDefinition: function(exitAfterSave) {
		var roles = [],
			menuItems = [],
			organizations = [],
			items = [],
			index = 0;

		this.roles.each(function(role) {
			roles.push(role.get('name'))
		})

		this.menuItems.getRootNode().eachChild(function(group) {
			if (group.data.isGroup && group.childNodes && group.childNodes.length > 0)
				group.eachChild(function(item) {
					item.set('order', index)
					item.set('group', group.get('title'))
					index++
					items.push(this.convertNode(item.data))
				}, this)
			else {
				if (!group.data.isGroup) {
					group.set('order', index)
					index++
					items.push(this.convertNode(group.data))
				}
			}
		}, this)

		this.ajax({
			url: '/resolve/service/menudefinition/save',
			jsonData: {
				id: this.id,
				title: this.title,
				color: this.color == '0088CC' ? '' : this.color,
				textColor: this.textColor == 'FFFFFF' ? '' : this.textColor,
				active: this.active,
				roles: roles,
				sysAppModules: items
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.getUser()
					clientVM.displaySuccess(this.localize('MenuDefinitionSaved'))
					if (exitAfterSave === true) clientVM.handleNavigation({
						modelName: 'RS.sysadmin.MenuDefinitions'
					})
					else {
						this.set('id', response.data.id)
						RS.common.grid.updateSysFields(this, response.data)
					}
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('persisting', false);
			}
		})
	},
	convertNode: function(node) {
		return {
			id: node.id && node.id.indexOf('ext-gen') == -1 ? node.id : '',
			title: node.title,
			roles: node.roles,
			query: node.query,
			active: node.active,
			appModuleGroup: node.group
		}
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.MenuDefinitions'
		})
	},

	roles: {
		mtype: 'store',
		fields: ['name'],
		proxy: {
			type: 'memory'
		}
	},
	rolesSelections: [],
	rolesColumns: [],

	addRole: function() {
		this.open({
			mtype: 'RS.sysadmin.RolePicker',
			callback: this.addNewRole
		})
	},
	addNewRole: function(newRoles) {
		Ext.Array.forEach(newRoles, function(role) {
			if (this.roles.findExact('name', role.get('uname')) == -1) this.roles.add({
				name: role.get('uname')
			})
		}, this)
	},
	removeRole: function() {
		Ext.Array.forEach(this.rolesSelections, function(r) {
			this.roles.remove(r)
		}, this)
	},
	removeRoleIsEnabled$: function() {
		return this.rolesSelections.length > 0
	},

	menuItems: {
		mtype: 'treestore',
		fields: ['id', 'title', 'query', 'group', 'rolesString', 'roles', {
			name: 'active',
			type: 'bool'
		}, {
			name: 'isGroup',
			type: 'bool'
		}],
		proxy: {
			type: 'memory'
		}
	},
	menuItemsSelections: [],
	menuItemsColumns: [],

	addMenuItem: function() {
		this.open({
			mtype: 'RS.sysadmin.MenuItem'
		})
	},
	addGroup: function() {
		this.menuItems.getRootNode().appendChild({
			title: this.localize('newGroup'),
			isGroup: 'true',
			expanded: true
		})
	},
	editMenuItem: function(id) {
		var menuDef = this.menuItems.getNodeById(id) || {},
			editModel = this.open({
				mtype: 'RS.sysadmin.MenuItem',
				editMode: true
			});

		if (menuDef) {
			editModel.set('originalItem', menuDef)
			editModel.loadData(menuDef.data)
			editModel.roles.removeAll()
			var newRoles = []
			Ext.Array.forEach(menuDef.get('roles') || [], function(newRole) {
				newRoles.push({
					name: newRole
				})
			})
			editModel.roles.add(newRoles)
		}
	},
	checkDrop: function(data, overModel, dropPosition, dropHandlers) {
		var group = overModel.parentNode.get('group'),
			records = data.records;

		var siblings = overModel.parentNode.childNodes;
		if (dropPosition === 'append') {
			group = overModel.get('group');
			siblings = overModel.childNodes;
		}
		for (var idx = 0; idx < records.length; idx++) {
			var r = records[idx];
			if (!r.isLeaf() && dropPosition === 'append') {
				dropHandlers.cancelDrop();
				return;
			}
			// for (sidx in siblings) {
			// 	if (siblings[sidx].get('title') == r.get('title')) {
			// 		dropHandlers.cancelDrop();
			// 		return;
			// 	}
			// }
		}

		Ext.each(records, function(r) {
			if (r.get('title'))
				r.set('group', group);
		});
	},
	removeMenuItem: function() {
		var root = this.menuItems.getRootNode(),
			l1 = root.childNodes;

		Ext.Array.forEach(this.menuItemsSelections, function(c) {
			Ext.each(l1, function(node) {
				if (node.childNodes) {
					node.removeChild(c)
				}
			}, this)
			root.removeChild(c)
		}, this)
	},
	removeMenuItemIsEnabled$: function() {
		return this.menuItemsSelections.length > 0
	}
})