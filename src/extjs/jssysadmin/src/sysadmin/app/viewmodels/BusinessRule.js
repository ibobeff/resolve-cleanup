glu.defModel('RS.sysadmin.BusinessRule', {
	mock: false,
	id: '',
	uname: '',
	uorder: 0,
	utable: '',
	uactive: false,
	uasync: false,
	ubefore: false,
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrganizationName: '',
	utype: [],
	fields: ['id', 'uname', {
		name: 'uorder',
		type: 'long'
	}, 'utable', {
		name: 'uactive',
		type: 'boolean'
	}, {
		name: 'uasync',
		type: 'boolean'
	}, {
		name: 'ubefore',
		type: 'boolean'
	}, 'utype', 'uscript', {
		name: 'sysCreatedOn',
		type: 'long'
	}, 'sysCreatedBy', {
		name: 'sysUpdatedOn',
		type: 'long'
	}, 'sysUpdatedBy', 'sysOrganizationName'],

	defaultData: {
		id: '',
		uname: '',
		uorder: 0,
		utable: '',
		uactive: false,
		uasync: false,
		ubefore: false,
		utype: [],
		uscript: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrganizationName: ''
	},


	tableStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/common/resolveTables',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	typeStore: {
		mtype: 'store',
		fields: ['name', {
			name: 'value',
			type: 'auto'
		}],
		data: [{
			"name": "insert",
			"value": "insert"
		}, {
			"name": "delete",
			"value": "delete"
		}, {
			"name": "update",
			"value": "update"
		}, {
			"name": "select",
			"value": "select"
		}]
	},

	businessRuleTitle: '',

	uscript: '',

	state: '',

	propCollapsed: false,
	rows: 47,
	activeItem : 0,
	ruleTabIsPressed$ : function(){
		return this.activeItem == 0;
	},
	scriptTabIsPressed$ : function(){
		return this.activeItem == 1;
	},
	ruleTab : function(){
		this.set('activeItem', 0);
	},
	scriptTab : function(){
		this.set('activeItem', 1);
	},
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},
	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.set('propCollapsed', false);
		var id = this.id || '';
		this.resetForm()
		this.set('id', params && params.id != null ? params.id : id);
		this.set('state', 'ready');
		if (this.id != '') {
			this.loadBusinessRule();
			return;
		}
		this.set('activeItem',0);
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true);
		this.loadTables();
		this.set('businessRuleTitle', this.localize('businessRuleTitle'));
	},

	loadTables: function() {
		this.tableStore.load({
			scope: this,
			callback: function(recs, op, suc) {
				/*
				if (!suc) {
					clientVM.displayError(this.localize('LoadTableErrMsg'));
				}
				*/
			}
		})
	},

	loadBusinessRule: function() {
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/businessrules/get',
			params: {
				id: this.id
			},
			scope: this,
			success: function(resp) {
				this.handleLoadSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	handleLoadSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('GetErrMsg', respData.message));
			return;
		}
		var data = respData.data;
		this.setData(data);
	},

	setData: function(data) {
		this.set('id', data.id);
		this.set('uname', data.uname);
		this.set('uorder', data.uorder);
		this.set('utable', data.utable);
		this.set('uactive', data.uactive);
		this.set('uasync', data.uasync);
		this.set('ubefore', data.ubefore);
		this.set('uscript', data.uscript);
		this.set('sysCreatedOn', data.sysCreatedOn);
		this.set('sysCreatedBy', data.sysCreatedBy);
		this.set('sysUpdatedOn', data.sysUpdatedOn);
		this.set('sysUpdatedBy', data.sysUpdatedBy);
		this.set('utype', data.utype.split(','));
		this.commit();
	},

	saveBusinessRule: function(exitAfterSave) {
		this.set('state', 'wait');
		var data = {
			id: this.id,
			UName: this.uname,
			UOrder: this.uorder,
			UTable: this.utable,
			UActive: this.uactive,
			UAsync: this.uasync,
			UBefore: this.ubefore,
			UType: this.utype,
			UScript: this.uscript
		};
		data.UType = this.utype.join(',');

		this.ajax({
			url: '/resolve/service/businessrules/save',
			jsonData: data,
			scope: this,
			success: function(resp) {
				this.handleSaveSucResp(resp);
				if (exitAfterSave === true)
					clientVM.handleNavigation({
						modelName: 'RS.sysadmin.BusinessRules'
					});
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	save: function(exitAfterSave) {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveBusinessRule, this, [exitAfterSave])
		this.task.delay(500)
		this.set('state', 'wait')
	},
	saveIsEnabled$: function() {
		return !this.state != 'wait' && this.isValid;
	},

	saveAndExit: function() {
		if (!this.saveIsEnabled)
			return;
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	handleSaveSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']')
			return;
		}

		var data = respData.data;
		this.setData(data);
		clientVM.displaySuccess(this.localize('SaveSucMsg'));
	},

	resetForm: function() {
		this.loadData(this.defaultData);
		this.set('isPristine', true);
		this.set('uname', ''); // set after isPristine so uname field has red exclamation
		this.set('utable', '');
		this.set('utype', []);
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.BusinessRules'
		});
		this.resetForm();
	},

	refresh: function() {
		if (this.id) {
			this.loadBusinessRule();
		} else {
			this.resetForm();
		}
	},
	saveBusinessRuleIsEnabled$: function() {
		return this.isValid && this.state != 'wait' && this.isDirty;
	},

	unameIsValid$: function() {
		var rexp1 = /^[A-Za-z0-9_ ]+[\\.]([A-Za-z0-9_ ]+[\\.])*[A-Za-z0-9_ ]+$/;
		var rexp2 = /^[A-Za-z0-9_ ]+$/;
		return this.uname != null &&
			(rexp1.test(this.uname) || rexp2.test(this.uname)) &&
			this.uname != '' ? true : this.localize('invalidName');
	},

	utableIsValid$: function() {
		return this.utable != null && this.utable != '' ? true : this.localize('invalidTable');
	},
	uorderIsValid$: function() {
		var rexp = /^\d*$/;
		return this.uorder != null && this.uorder !== '' &&
			rexp.test(this.uorder) ? true : this.localize('invalidOrder');
	},
	utypeIsValid$: function() {
		return this.utype.length > 0 ? true : this.localize('invalidType');
	}
});