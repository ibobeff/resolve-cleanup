glu.defModel('RS.sysadmin.ApplicationRights', {

	mock: false,

	activate: function() {
		this.applicationRights.load()
	},

	columns: [],
	applicationRightsSelections: [],
	allSelected: false,

	displayName: '',
	stateId: 'applicationRights',

	applicationRights: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uappName'],
		fields: ['id', 'uappName', 'roles'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/apps/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)

		this.applicationRights.proxy.on('exception', function(e, resp, op) {
			clientVM.displayExceptionError(e, resp, op);
		}, this);

		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'uappName',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~roles~~',
			dataIndex: 'roles',
			filterable: false,
			sortable: false,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))

		this.set('displayName', this.localize('applicationRightsDisplayName'))
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createApplicationRight: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ApplicationRight'
		})
	},
	editApplicationRight: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ApplicationRight',
			params: {
				id: id
			}
		})
	},
	deleteApplicationRightIsEnabled$: function() {
		return this.applicationRightsSelections.length > 0
	},
	deleteApplicationRight: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.applicationRightsSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.applicationRightsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/apps/delete',
					params: {
						ids: '',
						whereClause: this.applicationRights.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('applicationRightsSelections', [])
							this.applicationRights.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			} else {
				var ids = []
				Ext.each(this.applicationRightsSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/apps/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('applicationRightsSelections', [])
							this.applicationRights.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}
		}
	}
})