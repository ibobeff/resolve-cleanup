glu.defModel('RS.sysadmin.ConfigRADIUSs', {
	displayName: '',

	columns: null,

	stateId: 'sysadminradiusproperties',

	state: '',

	store: {
		mtype: 'store',

		fields: ['id', 'udomain', 'uprimaryHost', 'usecondaryHost', 'uauthProtocol', 'ugateway', 'usharedSecret', {
			name: 'uisDefault',
			type: 'bool'
		}, {
			name: 'ufallback',
			type: 'bool'
		}, {
			name: 'uauthPort',
			type: 'numeric'
		}, {
			name: 'uacctPort',
			type: 'numeric'
		}].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'udomain',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/configradius/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	recordsSelections: [],

	activate: function() {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.loadRecords();
	},

	init: function() {
		this.set('displayName', this.localize('displayName'));

		this.set('columns', [{
			header: '~~domainName~~',
			dataIndex: 'udomain',
			filterable: true,
			flex: 1
		}, {
			header: '~~uprimaryHost~~',
			dataIndex: 'uprimaryHost',
			filterable: true,
			width: 150
		}, {
			header: '~~usecondaryHost~~',
			dataIndex: 'usecondaryHost',
			filterable: true,
			width: 150
		}, {
			header: '~~uauthProtocol~~',
			dataIndex: 'uauthProtocol',
			filterable: true,
			width: 200
		}, {
			header: '~~fallback~~',
			dataIndex: 'ufallback',
			filterable: true,
			renderer: RS.common.grid.booleanRenderer(),
			width: 100
		}, {
			header: '~~usharedSecret~~',
			dataIndex: 'usharedSecret',
			filterable: true,
			width: 180
		}].concat(RS.common.grid.getSysColumns()))
	},

	loadRecords: function() {
		this.set('state', 'wait');
		this.store.load({
			scope: this,
			callback: function(rec, op, suc) {
				/*
				if (!suc) {
					clientVM.displayError(this.localize('ListRecordsErr'))
				}
				*/
				this.set('state', 'ready')
			}
		})
	},

	editRecord: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ConfigRADIUS',
			params: {
				sys_id: id
			}
		});
	},

	createRecord: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ConfigRADIUS',
			params: {
				sys_id: ''
			}
		});
	},

	deleteRecords: function() {
		this.message({
			title: this.localize('DeleteTitle'),
			msg: this.localize(this.recordsSelections.length > 1 ? 'DeletesMsg' : 'DeleteMsg', {
				num: this.recordsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirmDelete'),
				no: this.localize('cancel')
			},

			fn: this.sendDeleteReq
		})
	},

	sendDeleteReq: function(btn) {
		if (btn != 'yes')
			return;

		this.set('state', 'wait');

		var ids = [];

		Ext.each(this.recordsSelections,function(r){
			ids.push(r.get('id'));
		},this);

		this.ajax({
			url: '/resolve/service/configradius/delete',
			jsonData: ids,
			scope: this,
			success: function(resp) {
				this.handleDeleteSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		})
	},

	handleDeleteSucResp: function(resp) {
		var respData = null;

		try {
			respData = RS.common.parsePayload(resp);
		} catch (e) {
			clientVM.displayError(this.localize('invalidJSON'));
		}

		if (!respData.success) {
			clientVM.displayError(this.localize('DeleteErr') + '[' + respData.message + ']');
		} else
			clientVM.displaySuccess(this.localize('DeleteSucMsg'));

		this.loadRecords();
	},

	createRecordIsEnabled$: function() {
		return this.state != 'wait';
	},

	deleteRecordsIsEnabled$: function() {
		return this.state == 'itemSelected';
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	when_selections_chagned: {
		on: ['recordsSelectionsChanged'],
		action: function() {
			if (this.recordsSelections.length > 0)
				this.set('state', 'itemSelected');
			else
				this.set('state', 'ready');
		}
	}
});