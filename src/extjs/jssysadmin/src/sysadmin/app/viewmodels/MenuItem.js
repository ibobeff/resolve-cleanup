glu.defModel('RS.sysadmin.MenuItem', {
	id: '',
	name: '',
	title: '',
	group: '',
	active: true,
	query: '',
	editMode: false,
	originalItem: null,

	fields: ['title', 'group', {
		name: 'active',
		type: 'bool'
	}, 'query'],

	roles: {
		mtype: 'store',
		fields: ['name'],
		proxy: {
			type: 'memory'
		}
	},
	rolesSelections: [],
	rolesColumns: [],

	init: function() {
		this.set('rolesColumns', [{
			header: '~~uname~~',
			dataIndex: 'name',
			filterable: true,
			sortable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))
	},

	addRole: function() {
		this.open({
			mtype: 'RS.sysadmin.RolePicker',
			callback: this.addNewRole
		})
	},
	addNewRole: function(newRoles) {
		Ext.Array.forEach(newRoles, function(role) {
			if (this.roles.findExact('name', role.get('uname')) == -1) this.roles.add({
				name: role.get('uname')
			})
		}, this)
	},
	removeRole: function() {
		Ext.Array.forEach(this.rolesSelections, function(r) {
			this.roles.remove(r)
		}, this)
	},
	removeRoleIsEnabled$: function() {
		return this.rolesSelections.length > 0
	},
	applyMenuItem: function() {
		if (this.originalItem) {
			var newValues = this.asObject()
			Ext.Object.each(newValues, function(key) {
				this.originalItem.set(key, newValues[key])
			}, this)
			var newRoles = [];
			this.roles.each(function(role) {
				newRoles.push(role.get('name'))
			})
			this.originalItem.set('roles', newRoles)
			this.originalItem.set('rolesString', newRoles.join(', '))
		}
		this.doClose()
	},
	applyMenuItemIsVisible$: function() {
		return this.editMode
	},
	addMenuItem: function() {
		var menuRoles = []

		this.roles.each(function(role) {
			menuRoles.push(role.get('name'))
		})

		var selection = this.parentVM.menuItemsSelections,
			menuConfig = {
				id: Ext.id(),
				title: this.title,
				query: this.query,
				group: this.group,
				active: this.active,
				query: this.query,
				roles: menuRoles,
				rolesString: menuRoles.join(', '),
				leaf: true
			};
		var parentNode = this.parentVM.menuItems.getRootNode();
		if (selection.length > 0 && selection[0].raw.isGroup)
			parentNode = selection[0];

		var children = parentNode.childNodes;
		for (var idx=0;idx<children.length;idx++) {
			var node = children[idx];
			if (node.get('title') == menuConfig.title) {
				clientVM.displayError(this.localize('ItemAlreadyExist', {
					title: menuConfig.title
				}));
				return;
			}
		}
		parentNode.appendChild(menuConfig);
		this.doClose()
	},
	addMenuItemIsVisible$: function() {
		return !this.editMode
	},

	cancel: function() {
		this.doClose();
	}
});