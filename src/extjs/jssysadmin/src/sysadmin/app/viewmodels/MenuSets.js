glu.defModel('RS.sysadmin.MenuSets', {

	mock: false,


	activate: function() {
		this.menuSets.load()
		clientVM.setWindowTitle(this.localize('menuSetsDisplayName'))
	},

	columns: [],
	menuSetsSelections: [],
	allSelected: false,

	displayName: '',
	stateId: 'MenuSets',

	menuSets: {
		mtype: 'store',
		sorters: ['sequence'],
		fields: ['id', 'name', 'roles', 'applications', {
			name: 'sequence',
			type: 'int'
		}, {
			name: 'active',
			type: 'bool'
		}].concat(RS.common.grid.getSysFields()),
		sorters: [{
			property: 'sequence',
			direction: 'ASC'
		}],
		proxy: {

			type: 'ajax',
			url: '/resolve/service/menuset/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)

		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'name',
			filterable: true,
			sortable: false,
			flex: 1
		}, {
			header: '~~active~~',
			dataIndex: 'active',
			filterable: true,
			sortable: false,
			align: 'center',
			width: 60,
			renderer: RS.common.grid.booleanRenderer()
		}, {
			header: '~~applications~~',
			dataIndex: 'applications',
			filterable: false,
			sortable: false,
			flex: 1
		}, {
			header: '~~roles~~',
			dataIndex: 'roles',
			filterable: false,
			sortable: false,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))

		this.set('displayName', this.localize('menuSetsDisplayName'))
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createMenuSet: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.MenuSet'
		})
	},
	editMenuSet: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.MenuSet',
			params: {
				id: id
			}
		})
	},
	deleteMenuSetIsEnabled$: function() {
		return this.menuSetsSelections.length > 0
	},
	deleteMenuSet: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.menuSetsSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.menuSetsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	arrange: function(data, dropRec, dropPosition, dropHandlers) {
		var ids = [];
		this.menuSets.each(function(r, idx) {
			//console.log(r.get('name'));
			ids.push(r.get('id'));
		});
		this.ajax({
			url: '/resolve/service/menuset/sequence',
			params: {
				ids: ids
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.menuSets.load();
				} else {
					clientVM.displayError(this.localize('orderErrMsg'));
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})

	},
	reallyDeleteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/menuset/delete',
					params: {
						ids: '',
						whereClause: this.menuSets.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							clientVM.getUser()
							this.set('menuSetsSelections', [])
							this.menuSets.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			} else {
				var ids = []
				Ext.each(this.menuSetsSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/menuset/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							clientVM.getUser()
							this.set('menuSetsSelections', [])
							this.menuSets.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	}
})