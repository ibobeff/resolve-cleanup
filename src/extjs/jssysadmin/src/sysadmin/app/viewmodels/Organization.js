glu.defModel('RS.sysadmin.Organization', {
	id: '',

	uorganizationName: '',
	description: '',

	sysCreatedOn: '',
	sysCreatedOnDisplay$: function() {
		return Ext.isDate(this.sysCreatedOn) ? Ext.Date.format(this.sysCreatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysUpdatedOn: '',
	sysUpdatedOnDisplay$: function() {
		return Ext.isDate(this.sysUpdatedOn) ? Ext.Date.format(this.sysUpdatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysCreatedBy: '',
	sysUpdatedBy: '',

	sysOrganizationName: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	title: '',

	waitingResponse: false,

	fields: ['uorganizationName', 'udescription', 'udisable'].concat(RS.common.grid.getSysFields()),

	init: function() {
		if (this.id) {
			this.set('title', this.localize('editOrganizationTitle'));
			this.ajax({
				url: '/resolve/service/organization/get',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success)
						this.loadData(response.data)
					else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		} else
			this.set('title', this.localize('createOrganizationTitle'))

	},

	nameIsValid$: function() {
		if (!this.uorganizationName) return this.localize('orgNameRequired')
		if (this.uorganizationName.length > 300) this.localize('orgNameMaxLength')
		return true
	},
	descriptionIsValid$: function() {
		return this.udescription.length > 4000 ? this.localize('orgDescriptionMaxLength') : true
	},

	saveIsEnabled$: function() {
		return this.isValid && !this.waitingResponse
	},
	save: function() {
		this.set('waitingResponse', true)
		this.ajax({
			url: '/resolve/service/organization/save',
			jsonData: this.asObject(),
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					clientVM.displaySuccess(this.localize('organizationSaved'))
					this.parentVM.loadOrganizations()
					this.doClose()
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('waitingResponse', false)
			}
		})
	},

	cancel: function() {
		this.doClose();
	}


});