glu.defModel('RS.sysadmin.ConfigLDAP', {

	title: '',

	state: '',

	//fields 
	fields: ['belongsToOrganization', 'udomain', 'ugateway', 'uipAddress', 'umode',
		'ubindDn', 'ubindPassword', 'uuidAttribute', 'ucryptType', 'ucryptPrefix', 'upasswordAttribute', 'ucryptType', 'ucryptPrefix', 'upasswordAttribute', {
			name: 'ussl',
			type: 'bool'
		}, {
			name: 'ufallback',
			type: 'bool'
		}, {
			name: 'groupRequired',
			type: 'bool'
		}, {
			name: 'uversion',
			type: 'numeric'
		}, {
			name: 'uport',
			type: 'numeric'
		},
		'sys_id',
		'sysCreatedOn',
		'sysCreatedBy',
		'sysUpdatedOn',
		'sysUpdatedBy',
		'sysOrganizationName'
	],

	//local variables same as field names

	sys_id: '',
	sysOrganizationName: '',
	udomain: '',
	ugateway: '',
	uipAddress: '',
	uport: 0,
	umode: '',
	uversion: 0,

	ubindDn: '',
	ubindPassword: '',
	upasswordAttribute: '',
	uuidAttribute: '',
	ucryptType: '',
	ucryptPrefix: '',

	ussl: false,
	ufallback: false,

	belongsToOrganization: '',

	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',

	ubaseDNList: '',
	uproperties: '',

	defaultData: {
		sys_id: '',
		sysOrganizationName: '',
		udomain: '',
		// ugateway: 'LDAP',
		uipAddress: '',
		uport: 389,
		umode: 'BIND',
		uversion: 3,

		ubindDn: '',
		ubindPassword: '',
		upasswordAttribute: 'userPassword',
		uuidAttribute: 'uid',
		ucryptType: 'CLEAR',
		ucryptPrefix: 'DEFAULT',

		ussl: false,
		ufallback: true,
		groupRequired : false,
		belongsToOrganization: '',

		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: ''
	},

	//this is for the drop down
	organizations: {
		mtype: 'store',
		fields: ['sys_id', 'uorganizationName'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/common/organizationList',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	propertiesTitle: '',
	baseDNListTitle: '',

	modeStore: {
		mtype: 'store',
		fields: ['type'],
		data: [{
			type: 'BIND'
		}, {
			type: 'BIND_UID'
		}, {
			type: 'COMPARE'
		}, {
			type: 'COMPARE_LDAP'
		}, {
			type: 'USERNAME'
		}]
	},

	cryptTypeStore: {
		mtype: 'store',
		fields: ['type'],
		data: [{
			type: 'CLEAR'
		}, {
			type: 'MD5'
		}, {
			type: 'CRYPT'
		}]
	},

	properties: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	baseDNList: {
		mtype: 'store',
		fields: ['value'],
		proxy: {
			type: 'memory'
		}
	},

	baseDNColumns: [{
		header: '~~value~~',
		dataIndex: 'value',
		filterable: true,
		flex: 1,
		editor: {
			xtype: 'textfield',
			allowBlank: false
		}
	}],

	propertiesColumns: [{
		header: '~~name~~',
		dataIndex: 'name',
		filterable: true,
		flex: 1,
		editor: {
			xtype: 'textfield',
			allowBlank: false
		}
	}, {
		header: '~~value~~',
		dataIndex: 'value',
		filterable: true,
		flex: 1,
		editor: {
			xtype: 'textfield',
			allowBlank: false
		}
	}],

	selectedBaseDN: null,
	selectedProp: null,

	authGatewayStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		var sys_id = this.sys_id || ''
		this.loadAuthGateway();
		this.resetForm()
		this.set('sys_id', params && params.sys_id != null ? params.sys_id : sys_id);
		this.set('state', 'ready');
		if (this.sys_id != '') {
			this.loadLdaps();
			return;
		}
	},
	//init function
	init: function() {
		this.loadOrg();
		this.set('propertiesTitle', this.localize('propertiesTitle'));
		this.set('baseDNListTitle', this.localize('baseDNListTitle'));
		this.set('title', this.localize('LdapTitle'));
		this.cellEditing = new Ext.grid.plugin.CellEditing({
			clicksToEdit: 1
		});
	},

	//load the list of organizations first as it needs to be pre-selected
	loadOrg: function() {
		this.set('state', 'wait');
		this.organizations.load({
			scope: this,
			callback: function(recs, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('GetOrgErrMsg'));
					return;
				}

				this.set('state', 'ready');
			}
		})
	},

	loadLdaps: function() {
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/configldap/get',
			params: {
				id: this.sys_id
			},
			scope: this,
			success: function(resp) {
				this.handleLoadSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	loadAuthGateway: function() {
		this.ajax({
			url: '/resolve/service/configldap/listAuthGateways',
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success)
					clientVM.displayError(this.localize('LoadAuthGatewayErr') + '[' + respData.message + ']');
				var records = respData.records;
				this.authGatewayStore.removeAll();
				Ext.each(records, function(r) {
					this.authGatewayStore.add({
						name: r,
						value: r
					});
				}, this);
				if (this.authGatewayStore.getCount() > 0)
					this.set('ugateway', this.authGatewayStore.first().get('value'));
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	handleLoadSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('GetErrMsg') + '[' + respData.message + ']');
			return;
		}

		this.setData(respData.data);
	},

	getBaseDNCSV: function() {
		var records = [];
		this.baseDNList.each(function(rec) {
			records.push(rec.data.value);
		});

		var csv = records.join('|&|');
		return csv;
	},

	getPropertiesCSV: function() {
		var records = [];
		this.properties.each(function(rec) {
			records.push(rec.data.name + "=" + rec.data.value);
		});

		var csv = records.join('\n');
		return csv;
	},

	loadBaseDN: function(csv) {
		if (csv == null)
			return;
		var values = csv.split('|&|');
		if (values == '')
			return;
		for (var idx = 0; idx < values.length; idx++) {
			this.baseDNList.add({
				value: values[idx]
			});
		}
	},

	loadProperties: function(csv) {
		if (csv == null)
			return;
		var pair = csv.split('\n');
		if (pair == '')
			return;
		var kv = [];
		for (var idx = 0; idx < pair.length; idx++) {
			kv = (pair[idx]).split('=');
			this.properties.add({
				name: kv[0],
				value: kv[1]
			});
		}
	},

	saveLdap: function(exitAfterSave) {
		var baseDNListCSV = this.getBaseDNCSV();
		var propertiesCSV = this.getPropertiesCSV();
		if (this.uport == null || this.uport == '')
			this.uport = 0;
		var data = {
			id: this.sys_id,
			UVersion: this.uversion,
			UIpAddress: this.uipAddress,
			UPort: this.uport,
			USsl: this.ussl,
			groupRequired : this.groupRequired,
			UUidAttribute: this.uuidAttribute,
			UMode: this.umode,
			UBindDn: this.ubindDn,
			UBindPassword: this.ubindPassword,
			UFallback: this.ufallback,
			UDomain: this.udomain,
			UBaseDNList: baseDNListCSV,
			UProperties: propertiesCSV,
			UGateway: this.ugateway,
			UPasswordAttribute: this.upasswordAttribute,
			UCryptType: this.ucryptType,
			UCryptPrefix: this.ucryptPrefix,
			belongsToOrganization: {
				sys_id: this.belongsToOrganization
			}
		}
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/configldap/save',
			jsonData: data,
			scope: this,
			success: function(resp) {
				if (exitAfterSave === true)
					clientVM.handleNavigation({
						modelName: 'RS.sysadmin.ConfigLDAPs'
					});
				this.handleSaveSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		})
	},

	handleSaveSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('SaveErrMsg', respData.message));
			return;
		}
		this.set('state', 'ready');
		clientVM.displaySuccess(this.localize('SaveSucMsg'));
		this.setData(respData.data);
	},

	setData: function(data) {
		this.loadData(data);
		if (data.belongsToOrganization != null)
			this.set('belongsToOrganization', data.belongsToOrganization.sys_id);
		this.baseDNList.removeAll();
		this.properties.removeAll();
		this.loadBaseDN(data.ubaseDNList);
		this.loadProperties(data.uproperties);
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ConfigLDAPs'
		});

		this.loadData(this.defaultData);
	},


	save: function(exitAfterSave) {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveLdap, this, [exitAfterSave])
		this.task.delay(500)
		this.set('state', 'wait')
	},
	saveIsEnabled$: function() {
		return this.state != 'wait' && this.isValid;
	},
	saveAndExit: function() {
		if (!this.saveIsEnabled)
			return;
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	refresh: function() {
		if (this.id) {
			this.loadLdaps();
		} else {
			this.resetForm();
		}
	},

	selectedProp: null,

	selectedBaseDN: null,

	addProperties: function() {
		this.properties.add({
			name: 'NewPropertyName',
			value: 'NewPropertyValue'
		});
	},

	deleteProperty: function() {
		this.properties.remove(this.selectedProp);
	},

	addBaseDn: function() {
		this.baseDNList.add({
			value: 'NewBaseDnValue'
		});

	},

	deleteBaseDn: function() {
		this.baseDNList.remove(this.selectedBaseDN);
	},


	resetForm: function() {
		this.loadData(this.defaultData);
		this.baseDNList.removeAll();
		this.properties.removeAll();
		this.properties.add([{
			name: 'firstname',
			value: 'givenname'
		}, {
			name: 'lastname',
			value: 'sn'
		}, {
			name: 'title',
			value: 'title'
		}, {
			name: 'email',
			value: 'mail'
		}, {
			name: 'phone',
			value: 'telephonenumber'
		}, {
			name: 'mobile',
			value: 'mobile'
		}]);
		this.set('isPristine', true);
		this.set('udomain', ''); // set after isPristine so udomain field has red exclamation
	},

	saveLdapIsEnabled$: function() {
		return this.state != 'wait' && this.isValid;
	},

	deletePropertyIsEnabled$: function() {
		return this.selectedProp != null;
	},

	deleteBaseDnIsEnabled$: function() {
		return this.selectedBaseDN != null;
	},

	udomainIsValid$: function() {
		return this.udomain != null &&
			this.udomain != '' ? true : this.localize('invalidDomain');
	},

	// ubindDnIsEnabled$:function(){
	// 	return this.umode!='BIND'&&this.umode!='BIND_UID';
	// },
	// ubindPasswordIsEnabled$:function(){
	// 	return this.umode!='BIND'&&this.umode!='BIND_UID';
	// },
	uuidAttributeIsEnabled$: function() {
		return this.umode != 'BIND';
	},
	upasswordAttributeIsEnabled$: function() {
		return this.umode != 'BIND' && this.umode != 'USERNAME';
	},
	ucryptTypeIsEnabled$: function() {
		return this.umode == 'COMPARE';
	},
	ucryptPrefixIsEnabled$: function() {
		return this.umode == 'COMPARE';
	}
})