glu.defModel('RS.sysadmin.SystemScript', {
	mock: false,
	wait: false,
	sysScriptTitle: '',
	id: '',
	sys_id: '',
	uname: '',
	udescription: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrganizationName: '',
	uscript: '',
	scriptTitle: '',

	defaultData: {
		id: '',
		sys_id: '',
		uname: '',
		udescription: '',
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrganizationName: '',
		uscript: ''
	},

	fields: ['id', 'sys_id', 'uname', 'udescription', {
		name: 'sysCreatedOn',
		type: 'long'
	}, 'sysCreatedBy', {
		name: 'sysUpdatedOn',
		type: 'long'
	}, 'sysUpdatedBy', 'uscript', 'sysOrganizationName'],
	activeItem : 0,
	propertiesTab : function(){
		this.set('activeItem', 0);
	},
	propertiesTabIsPressed$ : function(){
		return this.activeItem == 0;
	},
	sourceTab : function(){
		this.set('activeItem', 1);
	},
	sourceTabIsPressed$ : function(){
		return this.activeItem == 1;
	},
	title$: function(){
		if(this.uname.trim() != ""){
			this.set('sysScriptTitle', this.localize('sysScriptTitle') + " - " + this.uname);
		}
		else 
			this.set('sysScriptTitle', this.localize('sysScriptTitle')); 
	},
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	activate: function(screen, params) {
		var id = this.id || '';
		this.resetForm();
		this.set('id', params && params.id != null ? params.id : id);
		if (this.id != '') {
			this.loadScript();
		}
		this.set('activeItem', 0);
	},
	init: function() {
		this.set('scriptTitle', this.localize('scriptTitle'));
		this.set('sysScriptTitle', this.localize('sysScriptTitle'));
	},
	saveScript: function(exitAfterSave) {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		var data = {
			id: this.id,
			UName: this.uname,
			UDescription: this.udescription,
			UScript: this.uscript
		}
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/sysscript/save',
			jsonData: data,
			scope: this,
			success: function(resp) {
				if (this.handleSaveSucResp(resp) && exitAfterSave === true)
					clientVM.handleNavigation({
						modelName: 'RS.sysadmin.SystemScripts'
					});
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	save: function(exitAfterSave) {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveScript, this, [exitAfterSave])
		this.task.delay(500)
	},
	saveIsEnabled$: function() {
		return !this.wait && this.isValid;
	},	
	refresh: function() {
		if (this.id) {
			this.loadScript();
		} else {
			this.resetForm();
		}
	},

	loadScript: function() {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/sysscript/get',
			params: {
				id: this.id
			},
			scope: this,
			success: this.handleLoadSucResp,
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	handleLoadSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('LoadErrMsg') + '[' + respData.message + ']');
			return false;
		}
		var data = respData.data;
		this.setData(data);
		return true;
	},

	setData: function(data) {
		this.loadData(data);
	},

	handleSaveSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('SaveErrMsg') + '[' + respData.message + ']');
			return;
		}
		var data = respData.data;
		this.setData(data);
		clientVM.displaySuccess(this.localize('SaveSucMsg'));
		return true;
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.SystemScripts'
		})
	},
	execute: function() {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/sysscript/executeScript',
			params: {
				name: this.uname
			},
			scope: this,
			success: function(resp) {
				this.handleExeSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	handleExeSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('ExeErrMsg') + '[' + respData.message + ']');
			return;
		}

		clientVM.displaySuccess(this.localize('ExeSucMsg'));
	},

	resetForm: function() {
		this.loadData(this.defaultData);
		this.set('isPristine', true);
		this.set('uname', ''); // set after isPristine so uname field has red exclamation
	},
	unameIsValid$: function() {
		var rexp1 = /^[A-Za-z0-9_ ]+[\\.]([A-Za-z0-9_ ]+[\\.])*[A-Za-z0-9_ ]+$/;
		var rexp2 = /^[A-Za-z0-9_ ]+$/;
		return this.uname != null &&
			(rexp1.test(this.uname) || rexp2.test(this.uname)) &&
			this.uname != '' ? true : this.localize('invalidName');
	},
	saveScriptIsEnabled$: function() {
		return !this.wait && this.isValid && this.isDirty;
	},
	executeIsVisible$: function() {
		return this.id && !/^\s*$/.test(this.id) && !/^\s*$/.test(this.uscript);
	},
	executeIsEnabled$: function() {
		return !this.wait;
	}
});