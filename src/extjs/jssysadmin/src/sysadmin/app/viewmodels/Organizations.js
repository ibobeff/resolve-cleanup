glu.defModel('RS.sysadmin.Organizations', {

	mock: false,

	activate: function() {
		clientVM.setWindowTitle(this.localize('organizationsDisplayName'))
		this.loadOrganizations()
	},

	columns: [],
	organizationsSelections: [],
	allSelected: false,

	displayName: '',
	stateId: 'organizations',

	organizations: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['uorganizationName'],
		fields: ['id', 'uorganizationName', 'udescription', {
			name: 'udisable',
			type: 'boolean'
		}].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/organization/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)

		var cols = [{
			header: '~~organizationName~~',
			dataIndex: 'uorganizationName',
			filterable: true,
			sortable: true,
			flex: 1
		}, {
			header: '~~description~~',
			dataIndex: 'udescription',
			filterable: true,
			sortable: true,
			flex: 2
		}, {
			header: '~~disable~~',
			dataIndex: 'udisable',
			filterable: true,
			sortable: true,
			width: 65,
			align: 'center',
			renderer: RS.common.grid.booleanRenderer()
		}].concat(RS.common.grid.getSysColumns()),
			sysOrgCol = null;
		Ext.Array.forEach(cols, function(col) {
			if (col.dataIndex == 'sysOrganizationName') sysOrgCol = col
		})
		if (sysOrgCol) cols.splice(cols.indexOf(sysOrgCol), 1)
		this.set('columns', cols)

		this.set('displayName', this.localize('organizationsDisplayName'))
	},

	loadOrganizations: function() {
		this.organizations.load()
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createOrganization: function() {
		this.open({
			mtype: 'RS.sysadmin.Organization'
		})
	},
	editOrganization: function(id) {
		this.open({
			mtype: 'RS.sysadmin.Organization',
			id: id
		})
	},
	deleteOrganizationIsEnabled$: function() {
		return this.organizationsSelections.length > 0
	},
	deleteOrganization: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.organizationsSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.organizationsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		})
	},
	reallyDeleteRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/organization/delete',
					params: {
						ids: '',
						whereClause: this.organizations.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('organizationsSelections', [])
							this.organizations.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			} else {
				var ids = []
				Ext.each(this.organizationsSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/organization/delete',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('organizationsSelections', [])
							this.organizations.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	},

	enableOrganizationIsEnabled$: function() {
		return this.organizationsSelections.length > 0
	},
	enableOrganization: function() {
		this.message({
			title: this.localize('enableTitle'),
			msg: this.localize(this.organizationsSelections.length == 1 ? 'enableMessage' : 'enablesMessage', this.organizationsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('enableAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyEnableRecords
		})
	},
	reallyEnableRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/organization/enable',
					params: {
						ids: '',
						whereClause: this.organizations.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('organizationsSelections', [])
							this.organizations.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			} else {
				var ids = []
				Ext.each(this.organizationsSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/organization/enable',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('organizationsSelections', [])
							this.organizations.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	},

	disableOrganizationIsEnabled$: function() {
		return this.organizationsSelections.length > 0
	},
	disableOrganization: function() {
		this.message({
			title: this.localize('disableTitle'),
			msg: this.localize(this.organizationsSelections.length == 1 ? 'disableMessage' : 'disablesMessage', this.organizationsSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('disableAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDisableRecords
		})
	},
	reallyDisableRecords: function(btn) {
		if (btn == 'yes') {
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/organization/disable',
					params: {
						ids: '',
						whereClause: this.organizations.lastWhereClause || '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('organizationsSelections', [])
							this.organizations.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			} else {
				var ids = []
				Ext.each(this.organizationsSelections, function(selection) {
					ids.push(selection.get('id'))
				})
				this.ajax({
					url: '/resolve/service/organization/disable',
					params: {
						ids: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r)
						if (response.success) {
							this.set('organizationsSelections', [])
							this.organizations.load()
						} else clientVM.displayError(response.message)
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				})
			}
		}
	}
})