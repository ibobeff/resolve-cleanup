glu.defModel('RS.sysadmin.ConfigADs', {

	mock: false,

	waitResponse: false,
	stateId: 'sysadminconfigad',

	configADStore: {
		mtype: 'store',
		fields: ['udomain', 'uipAddress', 'uuidAttribute', 'umode', 'ubindDn', 'ubaseDNList', 'ugateway', {
			name: 'groupRequired',
			type: 'bool'
		}, {
			name: 'uisDefault',
			type: 'bool'
		}, {
			name: 'ussl',
			type: 'bool'
		}, {
			name: 'ufallback',
			type: 'bool'
		}, {
			name: 'uversion',
			type: 'float'
		}, {
			name: 'uport',
			type: 'float'
		}].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'udomain',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/configad/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	configADSelections: [],

	columns: null,

	displayName: '',

	activate: function() {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.loadRecs();
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)
		this.set('displayName', this.localize('configADsTitle'))
		this.set('columns', [{
			header: '~~domainName~~',
			dataIndex: 'udomain',
			filterable: true,
			flex: 1
		}, {
			header: '~~ipAddress~~',
			dataIndex: 'uipAddress',
			filterable: true,
			width: 150
		}, {
			header: '~~mode~~',
			dataIndex: 'umode',
			filterable: true,
			width: 100
		}, {
			header: '~~baseDNList~~',
			dataIndex: 'ubaseDNList',
			filterable: true,
			flex: 1
		}, {
			header: '~~fallback~~',
			dataIndex: 'ufallback',
			filterable: true,
			renderer: RS.common.grid.plainRenderer(),
			width: 100
		}].concat(RS.common.grid.getSysColumns()))
	},

	loadRecs: function() {
		this.configADStore.load({
			scope: this,
			callback: function(records, op, success) {
				if (!success) {
					//clientVM.displayError(this.localize('getListErrorMsg'));
				}
				this.set('waitResponse', false);
			}
		});
		this.set('waitResponse', true);
	},

	createConfigAD: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ConfigAD',
			params: {
				id: ''
			}
		});
	},

	editConfigAD: function(id) {

		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ConfigAD',
			params: {
				id: id
			}
		})

	},

	deleteConfigAD: function() {

		this.message({
			title: this.localize('deleteConfigAD'),
			msg: this.localize(this.configADSelections.length == 1 ? 'deleteConfigADMessage' : 'deleteConfigADsMessage', {
				len: this.configADSelections.length
			}) + '<br/>',
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteConfigADs
		});
	},

	reallyDeleteConfigADs: function(btn) {
		if (btn != 'yes')
			return;
		var ids = [];
		for (var i = 0; i < this.configADSelections.length; i++) {
			ids.push(this.configADSelections[i].data.id);
		}

		this.set('waitResponse', true);
		this.ajax({
			url: '/resolve/service/configad/delete',
			jsonData: ids,
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success)
					clientVM.displayError(this.localize('disableFailureMsg') + '[' + respData.message + ']');
				else
					clientVM.displaySuccess(this.localize('DeleteSucMsg'));
				this.loadRecs();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('waitResponse', false);
			}
		});
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	createConfigADIsEnabled$: function() {
		return !this.waitResponse;
	},

	deleteConfigADIsEnabled$: function() {
		return this.configADSelections.length > 0 && !this.waitResponse;
	},

	updateConfigAD: function() {
		this.configADStore.load({
			scope: this,
			callback: function() {
				this.set('waitResponse', false);
			}
		});
		this.set('waitResponse', true);
	}

})