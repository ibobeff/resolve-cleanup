glu.defModel('RS.sysadmin.ApplicationRight', {

	mock: false,

	applicationRightTitle$: function() {
		return this.localize('applicationRight') + (this.uappName ? ' - ' + this.uappName : '')
	},

	activate: function(screen, params) {
		this.resetForm()
		this.set('id', params ? params.id : '')
		this.loadApplicationRight()
	},

	resetForm: function() {
		this.loadData(this.defaultData)
		this.roles.removeAll()
		this.controllers.removeAll()
		this.organizations.removeAll()
	},

	defaultData: {
		uappName: ''
	},

	fields: ['id', 'uappName'].concat(RS.common.grid.getSysFields()),
	id: '',
	uappName: '',
	uappNameIsValid$: function() {
		if (!this.uappName) return this.localize('nameInvalid')
		return true
	},
	sysCreatedOn: '',
	sysCreatedOnDisplay$: function() {
		return Ext.isDate(this.sysCreatedOn) ? Ext.Date.format(this.sysCreatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysUpdatedOn: '',
	sysUpdatedOnDisplay$: function() {
		return Ext.isDate(this.sysUpdatedOn) ? Ext.Date.format(this.sysUpdatedOn, clientVM.getUserDefaultDateFormat()) : ''
	},
	sysCreatedBy: '',
	sysUpdatedBy: '',

	sysOrganizationName: '',
	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)

		this.set('rolesColumns', [{
			header: '~~name~~',
			dataIndex: 'uname',
			filterable: true,
			sortable: true,
			flex: 1
		}])

		this.set('controllersColumns', [{
			header: '~~name~~',
			dataIndex: 'ucontrollerName',
			filterable: true,
			sortable: true,
			flex: 1
		}])

		this.set('organizationsColumns', [{
			header: '~~name~~',
			dataIndex: 'uorganizationName',
			filterable: true,
			sortable: true,
			flex: 1
		}])
	},

	loadApplicationRight: function() {
		if (this.id) {
			this.roles.removeAll()
			this.controllers.removeAll()
			this.organizations.removeAll()
			this.ajax({
				url: '/resolve/service/apps/get',
				params: {
					id: this.id
				},
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.loadData(response.data)
						this.originalResponse = response.data
						//load roles
						if (Ext.isArray(response.data.appRoles)) Ext.Array.forEach(response.data.appRoles, function(role) {
							this.roles.add(role)
						}, this)

						//load organizations
						if (Ext.isArray(response.data.appOrgs)) Ext.Array.forEach(response.data.appOrgs, function(organization) {
							this.organizations.add(organization)
						}, this)

						//load controllers
						if (Ext.isArray(response.data.appControllers)) Ext.Array.forEach(response.data.appControllers, function(controller) {
							this.controllers.add(controller)
						}, this)
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			})
		}
	},

	refresh: function() {
		this.loadApplicationRight()
	},

	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.persistApplicationRight, this, [exitAfterSave])
		this.task.delay(500)
		this.set('persisting', true)
	},
	saveIsEnabled$: function() {
		return !this.persisting && this.isValid
	},
	saveAndExit: function() {
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	persisting: false,
	persistApplicationRight: function(exitAfterSave) {
		var roles = [],
			controllers = [],
			organizations = [];

		this.roles.each(function(role) {
			roles.push(role.data)
		}, this)

		this.controllers.each(function(controller) {
			controllers.push(controller.data)
		}, this)

		this.organizations.each(function(organization) {
			organizations.push(organization.data)
		}, this)

		this.ajax({
			url: '/resolve/service/apps/save',
			jsonData: {
				id: this.id,
				uappName: this.uappName,
				appRoles: roles,
				appControllers: controllers,
				appOrgs: organizations
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					clientVM.displaySuccess(this.localize('ApplicationRightSaved'))
					if (exitAfterSave === true) clientVM.handleNavigation({
						modelName: 'RS.sysadmin.ApplicationRights'
					})
					else {
						this.set('id', response.data.id)
					}
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('persisting', false);
			}
		})
	},

	back: function() {
		clientVM.handleNavigationBack()
	},

	roles: {
		mtype: 'store',
		sorters: ['uname'],
		fields: ['id', 'uname'],
		proxy: {
			type: 'memory'
		}
	},
	rolesSelections: [],
	rolesColumns: [],

	addRole: function() {
		this.open({
			mtype: 'RS.sysadmin.RolePicker'
		})
	},
	removeRole: function() {
		Ext.Array.forEach(this.rolesSelections, function(r) {
			this.roles.remove(r)
		}, this)
	},
	removeRoleIsEnabled$: function() {
		return this.rolesSelections.length > 0
	},

	controllers: {
		mtype: 'store',
		sorters: ['ucontrollerName'],
		fields: ['id', 'ucontrollerName'],
		proxy: {
			type: 'memory'
		}
	},
	controllersSelections: [],
	controllersColumns: [],

	addController: function() {
		this.open({
			mtype: 'RS.sysadmin.ControllerPicker'
		})
	},
	removeController: function() {
		Ext.Array.forEach(this.controllersSelections, function(c) {
			this.controllers.remove(c)
		}, this)
	},
	removeControllerIsEnabled$: function() {
		return this.controllersSelections.length > 0
	},

	organizations: {
		mtype: 'store',
		sorters: ['uorganizationName'],
		fields: ['id', 'uorganizationName'],
		proxy: {
			type: 'memory'
		}
	},
	organizationsSelections: [],
	organizationsColumns: [],

	addOrganization: function() {
		this.open({
			mtype: 'RS.sysadmin.OrganizationPicker'
		})
	},
	removeOrganization: function() {
		Ext.Array.forEach(this.organizationsSelections, function(c) {
			this.organizations.remove(c)
		}, this)
	},
	removeOrganizationIsEnabled$: function() {
		return this.organizationsSelections.length > 0
	}
})