glu.defModel('RS.sysadmin.MenuPicker', {
	mock: false,

	menus: {
		mtype: 'store',
		remoteSort: true,
		sorters: ['title'],
		fields: ['id', 'title', 'name'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/common/menudefinition/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	menusSelections: [],
	columns: [],

	init: function() {
		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'title',
			filterable: true,
			sortable: true,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))
		this.menus.load()
	},

	cancel: function() {
		this.doClose()
	},

	add: function() {
		Ext.Array.forEach(this.menusSelections, function(r) {
			var contains = false;
			this.parentVM.menus.each(function(menu) {
				if (menu.get('displayName') == r.get('title')) contains = true
			})
			if (!contains) {
				r.set('displayName', r.get('title'))
				r.set('value', r.get('name'))
				delete r.data.id
				this.parentVM.menus.add(r.data)
			}
		}, this)
		this.doClose()
	},
	addIsEnabled$: function() {
		return this.menusSelections.length > 0
	}
});