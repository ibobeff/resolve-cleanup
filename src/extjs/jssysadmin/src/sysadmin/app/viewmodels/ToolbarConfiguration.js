glu.defModel('RS.sysadmin.ToolbarConfiguration', {

	mock: false,
	wait: false,
	toolbarConfTitle: '',
	toolbarConfigurations: {
		mtype: 'treestore',
		fields: ['id', 'name', 'query', 'group', 'roles', 'tooltip', {
			name: 'openAs'
		}, {
			name: 'active',
			type: 'bool'
		}],
		// , {
		// 	name: 'isGroup',
		// 	type: 'bool'
		// }],
		proxy: {
			type: 'memory'
		}
	},
	comboStore: {
		mtype: 'store',
		fields: ['value', 'name'],
		data: [{
			value: 'normal',
			name: 'Normal'
		}, {
			value: 'tab',
			name: 'New Tab'
		}, {
			value: 'window',
			name: 'Popup'
		}]
	},
	queryComboStore: null,
	columns: null,
	toolbarConfigurationsSelections: [],

	sys_id: '',
	uname: 'menu.toolbar',
	udescription: 'Toolbar configuration',
	uvalue: '',
	fields: ['sys_id', 'uname', 'udescription', 'uvalue'],
	activate: function() {
		this.loadConfig();
	},

	init: function() {
		var me = this;

		var handleChanges = function(combo, newQuery) {
			if (me.toolbarConfigurationsSelections.length == 0)
				return;
			var r = me.toolbarConfigurationsSelections[0];
			if (newQuery == 'RS.wiki.AddDocument')
				r.set('openAs', 'window');
		}
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true);
		this.set('toolbarConfTitle', this.localize('toolbarConfTitle'));
		this.set('columns', [{
			xtype: 'treecolumn',
			header: '~~name~~',
			dataIndex: 'name',
			filterable: false,
			sortable: false,
			editor: {},
			flex: 1,
			renderer: RS.common.grid.plainRenderer()
		}, {
			header: '~~openAs~~',
			dataIndex: 'openAs',
			filterable: false,
			sortable: false,
			editor: {
				xtype: 'combobox',
				editable: false,
				queryMode: 'local',
				displayField: 'name',
				valueField: 'value',
				store: '@{comboStore}',
				listeners: {
					focus: function() {
						if (me.toolbarConfigurationsSelections.length == 0)
							return;
						var r = me.toolbarConfigurationsSelections[0];
						if (r.get('query') == 'RS.wiki.AddDocument') {
							r.set('value', 'window');
							this.setReadOnly(true);
						} else
							this.setReadOnly(false);
					}
				}
			},
			renderer: function(value) {
				switch (value) {
					case 'normal':
						return 'Normal';
					case 'tab':
						return 'New Tab';
					case 'window':
						return 'Popup';
				}
			}
		}, {
			xtype: 'checkcolumn',
			header: '~~active~~',
			dataIndex: 'active',
			filterable: false,
			sortable: false,
			width: 60,
			align: 'center'
		}, {
			header: '~~queryOrUrl~~',
			tooltip: this.localize('queryOrUrlTooltip'),
			tooltipType: 'title',
			dataIndex: 'query',
			filterable: false,
			sortable: false,
			editor: {
				xtype: 'combobox',
				queryMode: 'local',
				displayField: 'name',
				valueField: 'value',
				store: '@{queryComboStore}',
				listeners: {
					change: handleChanges
				}
			},
			flex: 1,
			renderer: (function(self) {
				return function(value) {
					var value0 = value ? value.replace(/\s+/g, '') : '';
					switch (value0) {
						case 'RS.client.Menu':
							return self.localize('menuText');
						case 'RS.social.Main':
							return self.localize('socialText');
						case '/resolve/jsp/rsworksheet.jsp?sys_id=ACTIVE':
							return self.localize('activeWorksheetText');
						case '/resolve/jsp/rsworksheet.jsp?main=WorkSheet':
							return self.localize('allWorksheetsText');
						case 'RS.wiki.Main/name=HOMEPAGE':
							return self.localize('defaultDocumentText');
						case 'RS.wiki.AddDocument':
							return self.localize('addDocumentText');
						default:
							return Ext.String.htmlEncode(value);
					}
				}
			})(this)
		}, {
			header: '~~roles~~',
			dataIndex: 'roles',
			filterable: false,
			sortable: false,
			flex: 1,
			renderer: function(value, metaData, record) {
				metaData.tdAttr = 'data-qtip="' + value + '"';
				return value;
			}
		}, {
			text: '~~tooltip~~',
			dataIndex: 'tooltip',
			filterable: false,
			sortable: false,
			editor: {},
			flex: 1
		}]);

		this.set('queryComboStore', Ext.create('Ext.data.Store', {
			mtype: 'store',
			fields: ['value', 'name'],
			data: [{
				value: 'RS.client.Menu',
				name: this.localize('menuText')
			}, {
				value: 'RS.social.Main',
				name: this.localize('socialText')
			}, {
				value: 'RS.worksheet.Worksheet/id=ACTIVE',
				name: this.localize('activeWorksheetText')
			}, {
				value: 'RS.worksheet.Worksheets',
				name: this.localize('allWorksheetsText')
			}, {
				value: 'RS.wiki.Main/name=HOMEPAGE',
				name: this.localize('defaultDocumentText')
			}, {
				value: 'RS.wiki.AddDocument',
				name: this.localize('addDocumentText')
			}]
		}))
	},

	loadConfig: function() {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/sysproperties/getSystemPropertyByName',
			params: {
				name: 'menu.toolbar'
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('GetErrMsg') + '[' + respData.message + ']');
					return;
				}
				this.loadData(respData.data);
				var json = this.uvalue,
					jsonData = {};
				try {
					jsonData = Ext.JSON.decode(json);
				} catch (e) {
					var jsonData = {
							children: []
						},
						old = clientVM.parseLegacyConfiguration(json);
					Ext.each(old, function(i) {
						jsonData.children.push(this.itemToConfig(i));
					}, this)
				}
				var massage = {};
				Ext.apply(massage, jsonData);

				function m(item) {
					for (key in item) {
						var obj = item[key];
						if (key == 'query' && obj.indexOf('RS.wiki.Main/name=') != -1)
							item[key] = obj.substring(18, obj.length);
						else if (Ext.isArray(obj))
							for (var i = 0; i < obj.length; i++)
								m(obj);
						else if (Ext.isObject(obj))
							m(obj);
					}
				}

				m(massage);
				this.toolbarConfigurations.setRootNode(jsonData);
			},
			failure: function(resp) {
				clientVM.displayError(this.localize('GetErrMsg'));
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	save: function() {
		var root = this.toolbarConfigurations.getRootNode();
		var jsonObject = this.convertNode(root);
		var hasMenu = false;
		Ext.each(jsonObject.children, function(item) {
			if (item.query == "RS.client.Menu")
				hasMenu = true;
		});
		if (!hasMenu)
			this.message({
				title: this.localize('noMenuTitle'),
				msg: this.localize('noMenu'),
				buttons: Ext.MessageBox.OK
			});
		this.uvalue = ' ' + Ext.JSON.encode(jsonObject);

		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/sysproperties/saveSystemProperty',
			jsonData: {
				sys_id: this.sys_id,
				UName: this.uname,
				UDescription: this.udescription,
				UValue: this.uvalue
			},
			scope: this,
			success: function(resp) {
				clientVM.displaySuccess(this.localize('SaveSucMsg'))
				clientVM.getUser()
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},

	saveIsEnabled$: function() {
		return !this.wait;
	},

	addRoles: function() {
		this.open({
			mtype: 'RS.common.GridPicker',
			displayName: this.localize('roleDisplayName'),
			asWindowTitle: this.localize('asWindowTitle'),
			dumperText: this.localize('dump'),
			columns: [{
				header: this.localize('uname'),
				dataIndex: 'uname',
				filterable: true,
				flex: 1
			}],
			storeConfig: {
				fields: ['id', 'uname', 'uhomepage'],
				baseParams : {
					includePublic : "true"
				},
				proxy: {
					type: 'ajax',
					url: '/resolve/service/common/roles/list',
					reader: {
						type: 'json',
						root: 'records'
					},
					listeners: {
						exception: function(e, resp, op) {
							clientVM.displayExceptionError(e, resp, op);
						}
					}
				}
			},
			dumper: (function(self) {
				return function(records) {
					var names = [];
					Ext.each(records, function(r, idx) {
						names.push(r.get('uname'));
					});
					Ext.each(self.toolbarConfigurationsSelections, function(r) {
						var rr = r.get('roles').split(', ');
						if (rr[0] == '')
							rr = [];
						rr = rr.concat(names);
						var map = {};
						Ext.each(rr, function(n) {
							map[n] = n;
						});
						rr = [];
						for (attr in map)
							rr.push(attr);
						r.set('roles', rr.join(', '));
					});
					self.set('toolbarConfigurationsSelections', []);
					return true;
				}
			})(this)
		});
	},

	addRolesIsEnabled$: function() {
		return this.toolbarConfigurationsSelections.length > 0 && !this.wait;
	},

	removeRoles: function() {

		this.open({
			mtype: 'RS.common.GridPicker',
			displayName: this.localize('roleDisplayName'),
			asWindowTitle: this.localize('asWindowTitle'),
			dumperText: this.localize('dumpRemove'),
			columns: [{
				header: this.localize('uname'),
				dataIndex: 'uname',
				filterable: true,
				flex: 1
			}],
			storeConfig: {
				fields: ['id', 'uname', 'uhomepage'],
				baseParams : {
					includePublic : "true"
				},
				proxy: {
					type: 'ajax',
					url: '/resolve/service/common/roles/list',
					reader: {
						type: 'json',
						root: 'records'
					},
					listeners: {
						exception: function(e, resp, op) {
							clientVM.displayExceptionError(e, resp, op);
						}
					}
				}
			},
			loader: (function(self) {
				return function() {
					var roles = {};
					Ext.each(self.toolbarConfigurationsSelections, function(r) {
						if (r.get('roles') == '')
							return;
						var rs = r.get('roles').split(', ');
						Ext.each(rs, function(ro) {
							roles[ro] = {
								uname: ro
							};
						});
					}, self);
					for (name in roles)
						this.store.add(roles[name]);
				}
			})(this),
			dumper: (function(self) {
				return function(records) {
					Ext.each(self.toolbarConfigurationsSelections, function(t) {
						var names = [];
						var roles = t.get('roles').split(', ');
						Ext.each(roles, function(role) {
							for (idx in records) {
								if (records[idx].get('uname') == role)
									return;
							}
							names.push(role);
						});
						t.set('roles', names.join(', '));
					});
					self.set('toolbarConfigurationsSelections', []);
					return true;
				}
			})(this)
		});
	},

	removeRolesIsEnabled$: function() {
		return this.toolbarConfigurationsSelections.length > 0 && !this.wait;
	},

	convertNode: function(node, parent) {
		var node0 = {};
		node0.name = node.get('name');
		if (node.get('query')) {
			if (/^RS\.\w+\.\w+/.test(node.get('query')) || Ext.form.field.VTypes.url(node.get('query')))
				node0.query = node.get('query');
			else if (!/^\//.test(node.get('query')))
				node0.query = 'RS.wiki.Main/name=' + node.get('query');
			else node0.query = node.get('query');
		}
		// node0.isGroup = node.get('isGroup');
		node0.active = node.get('active');
		node0.roles = node.get('roles');
		node0.openAs = node.get('openAs');
		node0.tooltip = node.get('tooltip');
		node0.leaf = node.isLeaf();
		node0.firstLayer = parent && parent.get('id') == 'root';
		if (node.isLeaf())
			return node0;
		node0.query = '';
		node0.children = [];
		node0.expanded = true;
		Ext.each(node.childNodes, function(n) {
			node0.children.push(this.convertNode(n, node));
		}, this);
		return node0;
	},

	checkDrop: function(data, overModel, dropPosition, dropHandlers) {
		var group = overModel.parentNode.get('group'),
			records = data.records,
			allow = true;
		if (dropPosition === 'append')
			group = overModel.get('group');
		Ext.Array.forEach(records || [], function(r, idx) {
			if (!r.isLeaf() && overModel.parentNode.get('group') != '') {
				dropHandlers.cancelDrop();
				allow = false;
				return false;
			}
		}, this)

		if (!allow)
			return;
		Ext.each(records, function(r) {
			if (r.get('name'))
				r.set('group', group);
		});
	},

	reset: function() {
		this.message({
			title: this.localize('ResetTitle'),
			msg: this.localize('ResetConfirmation'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('yes'),
				no: this.localize('no')
			},
			scope: this,
			fn: function(btn) {
				if (btn == 'no')
					return;
				var defaults = clientVM.getDefaultToolbar();
				var root = {
					children: []
				};
				Ext.each(defaults, function(i) {
					root.children.push(this.itemToConfig(i));
				}, this);
				this.toolbarConfigurations.setRootNode(root);
			}
		});

	},
	resetIsEnabled$: function() {
		return !this.wait
	},

	itemToConfig: function(item) {
		var query = item.modelName
		if (query && item.params)
			query += '/' + Ext.Object.toQueryString(item.params)
		query = query || item.location
		var conf = {
			name: item.text || item.tooltip,
			query: query,
			active: !item.hidden,
			roles: '',
			openAs: 'normal',
			leaf: true
		};

		if (item.target == '_blank')
			conf.openAs = 'tab';
		if (item.action == 'WINDOW')
			conf.openAs = 'window';
		if (item.menu) {
			conf.children = [];
			conf.leaf = false;
			conf.expanded = true;
			// conf.isGroup = true;
			Ext.each(item.menu.items, function(i) {
				conf.children.push(this.itemToConfig(i))
			}, this)
		}
		return conf;
	},

	refresh: function() {
		this.loadConfig();
	},

	addToolbarItem: function() {
		if (this.toolbarConfigurationsSelections.length == 1 && !this.toolbarConfigurationsSelections[0].isLeaf()) {
			this.toolbarConfigurationsSelections[0].appendChild({
				name: this.localize('newItem'),
				leaf: true,
				openAs: 'normal',
				active: true
			});

			return;
		}
		this.toolbarConfigurations.getRootNode().appendChild({
			name: this.localize('newItem'),
			leaf: true,
			openAs: 'normal',
			active: true
		})
	},

	addToolbarItemIsEnabled$: function() {
		return !this.wait;
	},

	addGroup: function() {
		this.toolbarConfigurations.getRootNode().appendChild({
			name: this.localize('newGroup'),
			active: true,
			openAs: 'normal',
			expanded: true
				// isGroup: true
		})
	},

	addGroupIsEnabled$: function() {
		return !this.wait;
	},

	removeToolbarItem: function() {
		Ext.Array.forEach(this.toolbarConfigurationsSelections, function(c) {
			c.remove();
		}, this)
	},

	removeToolbarItemIsEnabled$: function() {
		return this.toolbarConfigurationsSelections.length > 0 && !this.wait;
	}
})