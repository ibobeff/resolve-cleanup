glu.defModel('RS.sysadmin.ConfigRADIUS', {

	title: '',

	state: '',

	//fields 
	fields: ['belongsToOrganization', 'udomain', 'ugateway', 'uprimaryHost', 'usecondaryHost', 'uauthProtocol', 'usharedSecret',
		{
			name: 'ufallback',
			type: 'bool'
		}, {
			name: 'uauthPort',
			type: 'numeric'
		}, {
			name: 'uacctPort',
			type: 'numeric'
		},
		'sys_id',
		'sysCreatedOn',
		'sysCreatedBy',
		'sysUpdatedOn',
		'sysUpdatedBy',
		'sysOrganizationName'
	],

	//local variables same as field names

	sys_id: '',
	sysOrganizationName: '',
	udomain: '',
	ugateway: '',
	uprimaryHost: '',
	usecondaryHost: '',
	uacctPort: 0,
	uauthProtocol: '',
	uauthPort: 0,
	usharedSecret: undefined,

	ufallback: false,

	belongsToOrganization: '',

	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
		
	defaultData: {
		sys_id: '',
		sysOrganizationName: '',
		udomain: '',
		// ugateway: 'RADIUS',
		uprimaryHost: '',
		usecondaryHost: '',
		uacctPort: 1813,
		uauthProtocol: 'PAP',
		uauthPort: 1812,

		ufallback: true,

		belongsToOrganization: '',

		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: ''
	},

	//this is for the drop down
	organizations: {
		mtype: 'store',
		fields: ['sys_id', 'uorganizationName'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/common/organizationList',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	propertiesTitle: '',

	authprotocolStore: {
		mtype: 'store',
		fields: ['type'],
		data: [{
			type: 'PAP'
		}, {
			type: 'CHAP'
		}, {
			type: 'MS-CHAP V1'
		}, {
			type: 'MS-CHAP V2'
		}]
	},

	authGatewayStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		var sys_id = this.sys_id || ''
		this.loadAuthGateway();
		this.resetForm();
		this.set('sys_id', params && params.sys_id != null ? params.sys_id : sys_id);
		this.set('state', 'ready');
		if (this.sys_id != '') {
			this.loadRadiuss();
			return;
		} else {
			this.set('usharedSecret', 'resolve');
		}
	},
	//init function
	init: function() {
		this.loadOrg();
		this.set('title', this.localize('RadiusTitle'));
		this.cellEditing = new Ext.grid.plugin.CellEditing({
			clicksToEdit: 1
		});
	},

	//load the list of organizations first as it needs to be pre-selected
	loadOrg: function() {
		this.set('state', 'wait');
		this.organizations.load({
			scope: this,
			callback: function(recs, op, suc) {
				if (!suc) {
					//clientVM.displayError(this.localize('GetOrgErrMsg'));
					return;
				}

				this.set('state', 'ready');
			}
		})
	},

	loadRadiuss: function() {
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/configradius/get',
			params: {
				id: this.sys_id
			},
			scope: this,
			success: function(resp) {
				this.handleLoadSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		});
	},

	loadAuthGateway: function() {
		this.ajax({
			url: '/resolve/service/configradius/listAuthGateways',
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success)
					clientVM.displayError(this.localize('LoadAuthGatewayErr') + '[' + respData.message + ']');
				var records = respData.records;
				this.authGatewayStore.removeAll();
				Ext.each(records, function(r) {
					this.authGatewayStore.add({
						name: r,
						value: r
					});
				}, this);
				if (this.authGatewayStore.getCount() > 0)
					this.set('ugateway', this.authGatewayStore.first().get('value'));
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	handleLoadSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('GetErrMsg') + '[' + respData.message + ']');
			return;
		}

		this.setData(respData.data);
	},

	saveRadius: function(exitAfterSave) {
		if (this.uauthPort == null || this.uathPort == '')
			this.uauthPort = 1812;
		if (this.uacctPort == null || this.uacctPort == '')
			this.uacctPort = 1813;
		var data = {
			id: this.sys_id,
			UAuthPort: this.uauthPort,
			UPrimaryHost: this.uprimaryHost,
			USecondaryHost: this.usecondaryHost,
			UAcctPort: this.uacctPort,
			UAuthProtocol: this.uauthProtocol,
			UFallback: this.ufallback,
			UDomain: this.udomain,
			UGateway: this.ugateway,
			USharedSecret: this.usharedSecret,
			belongsToOrganization: {
				sys_id: this.belongsToOrganization
			}
		}
		this.set('state', 'wait');
		this.ajax({
			url: '/resolve/service/configradius/save',
			jsonData: data,
			scope: this,
			success: function(resp) {
				if (exitAfterSave === true)
					clientVM.handleNavigation({
						modelName: 'RS.sysadmin.ConfigRADIUSs'
					});
				this.handleSaveSucResp(resp);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('state', 'ready');
			}
		})
	},

	handleSaveSucResp: function(resp) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize('SaveErrMsg', respData.message));
			return;
		}
		this.set('state', 'ready');
		clientVM.displaySuccess(this.localize('SaveSucMsg'));
		this.setData(respData.data);
	},

	setData: function(data) {
		this.loadData(data);
		if (data.belongsToOrganization != null)
			this.set('belongsToOrganization', data.belongsToOrganization.sys_id);
	},

	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.sysadmin.ConfigRADIUSs'
		});

		this.loadData(this.defaultData);
	},


	save: function(exitAfterSave) {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveRadius, this, [exitAfterSave])
		this.task.delay(500)
		this.set('state', 'wait')
	},
	saveIsEnabled$: function() {
		return this.state != 'wait' && this.isValid;
	},
	saveAndExit: function() {
		if (!this.saveIsEnabled)
			return;
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveAndExitIsEnabled$: function() {
		return this.saveIsEnabled
	},

	refresh: function() {
		if (this.id) {
			this.loadRadiuss();
		} else {
			this.resetForm();
		}
	},

	resetForm: function() {
		this.loadData(this.defaultData);
		this.set('isPristine', true);
		this.set('udomain', ''); // set after isPristine so udomain field has red exclamation
		this.set('uprimaryHost', '');
	},

	saveRadiusIsEnabled$: function() {
		return this.state != 'wait' && this.isValid;
	},

	udomainIsValid$: function() {
		return this.udomain != null &&
			this.udomain != '' ? true : this.localize('invalidDomain');
	},
	
	uprimaryHostIsValid$: function() {
		return this.uprimaryHost != null &&
			this.uprimaryHost != '' ? true : this.localize('invalidPrimaryHost');
	},
	
	usharedSecretIsValid$: function() {
		return this.usharedSecret != null &&
			this.usharedSecret != '' ? true : this.localize('invalidSharedSecret');
	}
})
