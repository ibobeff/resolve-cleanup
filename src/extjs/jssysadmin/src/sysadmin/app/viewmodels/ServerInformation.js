glu.defModel('RS.sysadmin.ServerInformation', {
	mock: false,
	activate: function() {
		this.loadSystemInformation()
	},

	init: function() {
		if (this.mock) this.backend = RS.sysadmin.createMockBackend(true)
		this.set('sessionColumns', [{
			header: '~~user~~',
			dataIndex: 'user',
			sortable: true,
			filterable: true,
			flex: 1
		}, {
			header: '~~host~~',
			dataIndex: 'host',
			sortable: true,
			filterable: true,
			flex: 1
		}, {
			header: '~~sysCreatedOn~~',
			dataIndex: 'sysCreatedOn',
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat()),
			width: 200,
			sortable: true,
			filterable: true
		}, {
			header: '~~sysUpdatedOn~~',
			dataIndex: 'sysUpdatedOn',
			renderer: Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat()),
			width: 200,
			sortable: true,
			filterable: true
		}, {
			header: '~~duration~~',
			dataIndex: 'duration',
			sortable: true,
			filterable: true,
			flex: 1
		}])
	},

	fields: [
		'version',
		'build',

		'osArchitecture',
		'osName',
		'osVersion',
		'address',
		'osProcessor',
		'jvmVendor',
		'jvmName',
		'jvmVersion',

		'memoryMax',
		'memoryTotal',
		'memoryFreePercentage',

		'threadPeak',
		'threadLive',
		'threadDeamon'
	],

	version: '',
	build: '',

	osArchitecture: '',
	osName: '',
	osVersion: '',
	address: '',
	serverProcessor: 0,
	jvmVendor: '',
	jvmName: '',
	jvmVersion: '',

	memoryMax: 0,
	memoryTotal: 0,
	memoryFreePercentage: '',

	threadPeak: 0,
	threadLive: 0,
	threadDeamon: 0,

	sessions: {
		mtype: 'store',
		fields: ['id', 'user', 'host', {
			name: 'duration',
			type: 'int'
		}].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	sessionColumns: [],

	loadSystemInformation: function() {
		this.ajax({
			url: '/resolve/service/server/get',
			method: 'POST',
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.loadData(response.data)
					if (Ext.isArray(response.data.sessions)) Ext.Array.forEach(response.data.sessions, function(session) {
						this.sessions.add(session)
					}, this)
				} else clientVM.displayError(response.message)

			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	},

	refresh: function() {
		this.loadSystemInformation()
	},

	back: function() {
		clientVM.handleNavigationBack()
	}
})