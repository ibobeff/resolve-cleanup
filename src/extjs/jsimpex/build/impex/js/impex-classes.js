/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
glu.defModel('RS.impex.Component', {
	mixins: ['ComponentSearch'],
	name: '',
	namespace: '',
	component: '',
	type: '',
	typeText: '',
	dumper: null,
	options: null,
	init: function() {
		this.set('typeText', this.localize(this.type));
		this.set('component', this.namespace + (this.name ? '.' + this.name : ''));
		for (opt in this.options) {
			if (this[opt] != null || opt == 'startDate' || opt == 'endDate')
				this.set(opt, (opt == 'startDate' || opt == 'endDate') && this.options[opt] ? new Date(this.options[opt]) : this.options[opt]);
		}
	},
	setOptions: function() {
		this.dumper();
		this.close();
	},
	close: function() {
		this.doClose();
	}
});
function defineImpexDefinitionMetaVMIfNotExist() {
	if (RS.impex.definitionMetaDefined)
		return;
	RS.impex.definitionMetaDefined = true;
	Ext.Object.each(RS.impex.metaConfigMap, function(metaType, meta) {
		var columns = [];
		Ext.each(meta.fields, function(field) {
			columns.push({
				header: '~~' + (field.header || field) + '~~',
				dataIndex: field.name || field,
				renderer: RS.common.grid.plainRenderer()
			});
		});
		if (columns.length > 0) {
			if (columns.length > 1) {
				columns[0].flex = 1;
				columns[columns.length - 1].flex = 2;
			} else
				columns[0].flex = 1;
		}
		Ext.each(RS.common.grid.getSysColumns(), function(col) {
			if (col.dataIndex == 'sysUpdatedBy' || col.dataIndex == 'sysUpdatedOn')
				columns.push(Ext.applyIf({
					hidden: false
				}, col))
		});
		var optionsList = [];
		Ext.Object.each(meta.options, function(k, v) {
			if (k.indexOf('$') == -1)
				optionsList.push(k);
		});
		if ((!meta.glide || !meta.glide.unique) && metaType != 'empty')
			throw 'Glide unique key of type:' + metaType + ' not defined!';
		var gridVMConfig = {
			statId: 'impexMeta_' + Ext.id(),
			type: metaType,
			componentStore: {
				mtype: 'store',
				fields: meta.fields.concat(['tmpBrowserId', 'sys_id']).concat(RS.common.grid.getSysFields()), //I add a temporary UI id so it decouples the ui id and impex definition sys_id
				proxy: {
					type: 'memory',
					reader: {
						type: 'json',
						idProperty: 'tmpBrowserId'
					}
				}
			},
			attached: false,
			glide: meta.glide,
			init: function() {
				this.componentStore.load();
				this.componentStore.on('datachanged', function() {
					this.parentVM.fireEvent('cmpStoreChanged', this);
				}, this);
			},
			componentsSelections: [],
			componentColumns: columns,
			componentsIsVisible$: function() {
				return !this.showOption;
			},
			editOption: function(browserId) {
				var me = this;
				var r = this.componentStore.getAt(this.componentStore.find('tmpBrowserId', browserId));
				this.open({
					mtype: 'RS.impex.' + metaType + 'DefinitionMetaOption',
					impexDef: r.raw,
					dumper: function() {
						r.raw.options = this.getOptions();
					}
				});
			},
			addOrUpdateCmp: function(rawCmp, optionsRef) {
				var cmpInStore = this.returnCmpInStoreIfContains(rawCmp);
				if (!cmpInStore) {
					//we want each cmp has its own option, so this we need to clone this options obj
					if (optionsRef)
						rawCmp.options = Ext.apply({}, optionsRef);
					rawCmp.tmpBrowserId = Ext.data.IdGenerator.get('uuid').generate();
					this.componentStore.add(rawCmp);
					return;
				}
				cmpInStore.raw.options = optionsRef;
			},
			addOrUpdateCmps: function(rawCmps, optionsRef) {
				Ext.each(rawCmps, function(rawCmp) {
					this.addOrUpdateCmp(rawCmp, optionsRef);
				}, this)
			},
			removeSelectedCmps: function() {
				this.componentStore.remove(this.componentsSelections);
			},
			returnCmpInStoreIfContains: function(cmp) {
				var contains = null;
				this.componentStore.each(function(cmpInStore) {
					if (this.isEqual(cmpInStore, cmp)) {
						contains = cmpInStore;
						return;
					}
				}, this)
				return contains;
			},
			isEqual: function(cmp1, cmp2) {
				var equal = true;
				if (cmp1.store)
					cmp1 = cmp1.raw;
				if (cmp2.store)
					cmp2 = cmp2.raw;
				Ext.each(this.glide.unique, function(partOfUnique) {
					equal = equal && (cmp1[partOfUnique] == cmp2[partOfUnique]);
				}, this);
				return equal;
			},
			when_componentsSelections_changed: {
				on: ['componentsSelectionsChanged'],
				action: function() {
					if (this.parentVM.activeItem != this)
						return;
					this.parentVM.fireEvent('activeCmpSelChange', this);
				}
			},
			getCmpImpexDefinitions: function() {
				var defs = [];
				this.componentStore.each(function(cmp) {
					var def = {};
					def['namespace'] = cmp.get(this.glide.namespace);
					if (this.glide.name)
						def['name'] = cmp.get(this.glide.name)
					var uiInfo = {};
					Ext.each(RS.impex.metaConfigMap[this.type].fields, function(f) {
						uiInfo[f.name || f] = cmp.get(f.name || f);
					}, this);
					def['description'] = Ext.encode(uiInfo) + ' ';
					def['type'] = this.type;
					var options = cmp.raw.options;
					//Convert start/end Date to timestap before save.
					if(options.startDate)
						options.startDate = new Date(options.startDate).getTime();
					if(options.endDate){
						//Add extra 24hr to cover all docs updated on that date.
						options.endDate = new Date(options.endDate).getTime() + 86399999;
					}
					def['options'] = cmp.raw.options;
					def['sys_id'] = cmp.get('sys_id');
					defs.push(def);
				}, this)
				return defs;
			}
		};
		Ext.apply(gridVMConfig, meta.options);
		glu.defModel('RS.impex.' + metaType + 'DefinitionMeta', gridVMConfig);
		glu.defModel('RS.impex.' + metaType + 'DefinitionMetaOption', Ext.apply({
			impexDef: null,
			typeText$: function() {
				return this.localize(this.parentVM.type);
			},
			component$: function() {
				var cmp = this.impexDef[this.parentVM.glide.namespace];
				if (this.parentVM.glide.name)
					cmp += '.' + this.impexDef[this.parentVM.glide.name];
				return cmp;
			},
			init: function() {
				this.setOptions(this.impexDef.options);
			},
			update: function() {
				this.dumper();
				this.cancel();
			},
			setOptions: function(options) {
				Ext.each(optionsList, function(o) {
					if (o == 'startDate' || o == 'endDate'){
						var date = null; //Avoid new Date(null) return unknown date.
						if(options[o] != null)
							date = new Date(options[o]);
						this.set(o, date);
					}else
						this.set(o, options[o]);
				}, this);
			},
			getOptions: function() {
				var options = {};
				Ext.each(optionsList, function(o) {
					options[o] = this[o]
				}, this);
				return options;
			},
			cancel: function() {
				this.doClose();
			},
			startDateChanged: function(newDate) {
				this.set('startDate', newDate);
			},
			endDateChanged: function(newDate) {
				this.set('endDate', newDate);
			}
		}, meta.options));
	});
}
glu.ns('RS.impex');
RS.impex.metaConfigMap = {
	'empty': {
		fields: []
	},
	'actionTask': {
		fields: ['uname', 'unamespace', 'usummary'],
		glide: {
			unique: ['unamespace', 'uname'],
			name: 'uname',
			namespace: 'unamespace'
		},
		options: {
			atProperties: true,
			excludeRefAssessor: false,
			excludeRefParser: false,
			excludeRefPreprocessor: false
		},
		showSysColumn : true
	},
	'actionTaskModule': {
		fields: ['unamespace'],
		glide: {
			unique: ['unamespace'],
			namespace: 'unamespace'
		},
		options: {
			atNsProperties: true,
			excludeNsRefAssessor: false,
			excludeNsRefParser: false,
			excludeNsRefPreprocessor: false
		}
	},
	'wiki': {
		fields: ['ufullname', 'usummary'],
		glide: {
			unique: ['ufullname'],
			namespace: 'ufullname'
		},
		options: {
			wikiSubRB: true,
			wikiTags: true,
			wikiForms: false,
			wikiRefATs: true,
			wikiOverride: true,
			wikiCatalogs: false
		},
		showSysColumn : true
	},
	'securityTemplate': {
		fields: ['ufullname', 'usummary'],
		glide: {
			unique: ['ufullname'],
			namespace: 'ufullname'
		},
		options: {
			wikiSubRB: true,
			wikiTags: true,
			wikiForms: true,
			wikiRefATs: true,
			wikiOverride: true,
			wikiCatalogs: false
		},
		showSysColumn : true
	},
	'runbook': {
		fields: ['ufullname', 'usummary'],
		glide: {
			unique: ['ufullname'],
			namespace: 'ufullname'
		},
		options: {
			wikiSubRB: true,
			wikiTags: true,
			wikiForms: false,
			wikiRefATs: true,
			wikiOverride: true,
			wikiCatalogs: false
		},
		showSysColumn : true
	},
	'wikiNamespace': {
		fields: ['unamespace'],
		glide: {
			unique: ['unamespace'],
			namespace: 'unamespace'
		},
		options: {
			startDate: null,
			endDate: null,
			wikiNsRefATs: true,
			wikiNsSubRB: true,
			wikiNsTags: true,
			wikiNsForms: false,
			wikiNsOverride: true,
			wikiDeleteIfSameSysId: false,
			wikiDeleteIfSameName: false
		}
	},
	'cns': {
		fields: ['uname', 'umodule', 'udescription'],
		glide: {
			unique: ['uname'],
			name: 'uname',
			namespace: 'umodule'
		}
	},
	'tag': {
		fields: ['name'],
		glide: {
			unique: ['name'],
			namespace: 'name'
		}
	},
	'wikiLookup': {
		fields: ['uregex', 'uwiki', 'uorder'],
		glide: {
			unique: ['uregex', 'uwiki', 'uorder'],
			name: 'uregex',
			namespace: 'umodule'
		}
	},
	'scheduler': {
		fields: ['uname', 'urunbook', 'umodule', 'uexpression'],
		glide: {
			unique: ['umodule', 'uname'],
			name: 'uname',
			namespace: 'umodule'
		}
	},
	'metricThreshold' : {
		fields: ['uruleName', 'uruleModule'],
		glide: {
			unique: ['uruleName', 'uruleModule'],
			name: 'uruleName',
			namespace: 'uruleModule'
		},
		options: {
			includeAllVersions: true
		}
	},
	'mtNamespace': {
		fields: ['uruleModule'],
		glide: {
			unique: ['uruleModule'],
			namespace: 'uruleModule'
		}
	},
	'menuSet': {
		fields: ['name'],
		glide: {
			unique: ['name'],
			namespace: 'name'
		},
		options: {
			mSetMenuSection: false,
			mSetMenuItem: false,
			mSetMenuItemIsVisible$: function() {
				return this.mSetMenuSection;
			}
		}
	},
	'menuDefinition': {
		fields: [{
			name: 'title',
			header: 'menuDefTitle'
		}],
		glide: {
			unique: ['title'],
			namespace: 'title'
		},
		options: {
			mSetMenuItem: true
		}
	},
	'menuItem': {
		fields: [{
			header: 'title',
			name: 'title'
		}, {
			header: 'menuItemTitle',
			name: 'name'
		}],
		glide: {
			unique: ['title', 'name'],
			namespace: 'title',
			name: 'name'
		}
	},
	'process': {
		fields: ['u_display_name', 'u_description'],
		glide: {
			unique: ['u_display_name'],
			namespace: 'u_display_name'
		},
		options: {
			processWiki: true,
			processUsers: true,
			processTeams: true,
			processForums: true,
			processRSS: true,
			processATs: true
		}
	},
	'team': {
		fields: ['u_display_name'],
		glide: {
			unique: ['u_display_name'],
			namespace: 'u_display_name'
		},
		options: {
			teamUsers: true,
			teamTeams: false
		}
	},
	'user': {
		fields: ['uuserName', 'ufirstName', 'ulastName'],
		glide: {
			unique: ['uuserName'],
			namespace: 'uuserName'
		},
		options: {
			userRoleRel: false,
			userGroupRel: false,
			userInsert: false
		}
	},
	'group': {
		fields: ['uname', 'udescription'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		},
		options: {
			groupUsers: false,
			groupRoleRel: false
		}
	},
	'role': {
		fields: ['uname', 'udescription'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		},
		options: {
			roleUsers: false,
			roleGroups: false
		}
	},
	'customTable': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			// name: 'uname',
			namespace: 'uname'
		},
		options: {
			tableForms: true, // Custom Forms
			tableData: false
		}
	},
	'form': {
		fields: ['uformName', 'udisplayName'],
		glide: {
			unique: ['uformName'],
			namespace: 'uformName'
		},
		options: {
			formTables: true, // Form Custom tables
			formRBs: true,
			formSS: true, // Form System Scripts
			formATs: true // Form ActionTasks
		}
	},
	'customDB': {
		fields: ['utableName', 'uquery'],
		glide: {
			unique: ['utableName', 'uquery'],
			name: 'uquery',
			namespace: 'utableName'
		}
	},
	'businessRule': {
		fields: ['uname', 'udescription'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'systemScript': {
		fields: ['uname', 'udescription'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'systemProperty': {
		fields: ['uname', 'udescription'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'forum': {
		fields: ['u_display_name', 'u_description'],
		glide: {
			unique: ['u_display_name'],
			namespace: 'u_display_name'
		},
		options: {
			forumUsers: true
		}
	},
	'rss': {
		fields: ['u_display_name', 'u_description'],
		glide: {
			unique: ['u_display_name'],
			namespace: 'u_display_name'
		}
	},
	'catalog': {
		fields: ['name'],
		glide: {
			unique: ['name'],
			namespace: 'name'
		},
		options: {
			catalogWikis: true
		}
	},
	'properties': {
		fields: ['uname', 'umodule', 'uvalue'],
		glide: {
			unique: ['umodule', 'uname'],
			name: 'uname',
			namespace: 'umodule'
		}
	},
	'wikiTemplate': {
		fields: ['uname'],
		glide: {
			unique: ['unamespace', 'uname'],
			name: 'uname',
			namespace: 'unamespace'
		},
		options: {
			templateForm: true,
			templateWiki: true
		}
	},
	'TCPFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'AmqpFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'DatabaseFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		},
		options: {
			exportDBPool: true
		}
	},
	'NetcoolFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'XMPPFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'ServiceNowFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'SNMPFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'HPOMFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'TIBCOBespokeFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'SalesforceFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'EWSFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		},
		options: {
			ewsAddress: true
		}
	},
	'ExchangeserverFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'EmailFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		},
		options: {
			emailAddress: true
		}
	},
	'TSRMFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'RemedyxFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		},
		options: {
			exportRemedyxForm: true
		}
	},
	'HPSMFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'SSHPool': {
		fields: ['usubnetMask'],
		glide: {
			unique: ['usubnetMask'],
			namespace: 'usubnetMask'
		}
	},
	'HTTPFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'CASpectrumFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'automationBuilder': {
		fields: ['ufullname', 'usummary'],
		glide: {
			unique: ['ufullname'],
			namespace: 'ufullname'
		},
		options: {
			wikiSubRB: true,
			wikiTags: true,
			wikiForms: true,
			wikiRefATs: true,
			wikiOverride: true,
			wikiCatalogs: false
		},
		showSysColumn : true
	},
	'ridMapping': {
		fields: ['rid', 'module',  'schema.name'],
		glide: {
			unique: ['rid', 'schema.name'],
			name: 'rid',
			namespace: 'schema.name'
		},
		options: {
			includeUIDisplay: true,
			includeUIAutomation: true,
    		includeGWAutomation: true,
			includeSirPlaybook: true
		}
	},
	'rrModule': {
		fields: ['module'],
		glide: {
			unique: ['module'],
			namespace: 'module'
		},
		options: {
			includeUIDisplay: true,
			includeUIAutomation: true,
    		includeGWAutomation: true,
			includeSirPlaybook: true
		}
	},
	'rrSchema': {
		fields: ['name'],
		glide: {
			unique: ['name'],
			name: 'name'
		},
		options: {
			includeMapping: false
		}
	}
};

glu.defModel('RS.impex.ComponentSearch', (function() {
      var tabs = {
            types: ['actionTask', 'actionTaskModule', 'wiki', 'wikiNamespace', 'cns', 'tag', 'wikiLookup', 'scheduler',
                  'menuSet', 'menuDefinition', 'menuItem', 'process', 'team', 'forum', 'rss', 'user', 'group', 'role', 'customTable', 'form',
                  'customDB', 'businessRule', 'systemScript', 'systemProperty', 'catalog', 'properties', 'wikiTemplate'
            ].sort(function(a, b) {
                  return a.toLowerCase() > b.toLowerCase() ? 1 : -1
            }),
            actionTaskStoreConf: ['uname', 'unamespace', 'usummary'],
            actionTaskModuleStoreConf: [{
                  name: 'unamespace',
                  header: 'umodule'
            }],
            wikiStoreConf: ['ufullname', 'usummary'],
            wikiNamespaceStoreConf: ['unamespace'],
            cnsStoreConf: ['uname', 'umodule', 'udescription'],
            tagStoreConf: ['name'],
            wikiLookupStoreConf: ['uregex', 'uwiki', 'uorder'],
            schedulerStoreConf: ['uname', 'urunbook', 'umodule', 'uexpression'],
            menuSetStoreConf: ['name'],
            menuDefinitionStoreConf: [{
                  name: 'title',
                  header: 'menuDefTitle'
            }, 'name'],
            menuItemStoreConf: ['title', {
                  header: 'menuItemTitle',
                  name: 'name'
            }],
            processStoreConf: ['u_display_name', 'u_description'],
            teamStoreConf: ['u_display_name'],
            userStoreConf: ['uuserName', 'ufirstName', 'ulastName'],
            groupStoreConf: ['uname', 'udescription'],
            roleStoreConf: ['uname', 'udescription'],
            customTableStoreConf: ['uname'],
            formStoreConf: ['uformName', 'udisplayName'],
            customDBStoreConf: ['utableName', 'uquery'],
            businessRuleStoreConf: ['uname', 'udescription'],
            systemScriptStoreConf: ['uname', 'udescription'],
            systemPropertyStoreConf: ['uname', 'udescription'],
            forumStoreConf: ['u_display_name', 'u_description'],
            rssStoreConf: ['u_display_name', 'u_description'],
            catalogStoreConf: ['name'],
            worksheetStoreConf: ['name'],
            propertiesStoreConf: ['uname', 'umodule', 'uvalue'],
            wikiTemplateStoreConf: ['uname'],
            changes: [],

            shared: {
                  menuDefinition: {
                        menuSet: 'mSetMenuItem'
                  }
            },
            //ActionTask opts
            actionTaskOpt: {
                  atProperties: true,
                  excludeRefAssessor: false,
                  excludeRefParser: false,
                  excludeRefPreprocessor: false
            },

            // ActionTask Namespace options
            actionTaskModuleOpt: {
                  atNsProperties: true,
                  excludeNsRefAssessor: false,
                  excludeNsRefParser: false,
                  excludeNsRefPreprocessor: false
            },

            // Wiki/Runbook options
            wikiOpt: {
                  wikiSubRB: true,
                  wikiTags: true,
                  // wikiSubDT: true,
                  wikiForms: false,
                  wikiRefATs: true,
                  wikiOverride: true,
                  wikiCatalogs: false
            },

            // Wiki/Runbook Namespace options
            wikiNamespaceOpt: {
                  startDate: null,
                  endDate: null,
                  wikiNsRefATs: true,
                  wikiNsSubRB: true,
                  wikiNsTags: true,
                  // wikiNsSubDT: true,
                  wikiNsForms: false,
                  wikiNsOverride: true,
                  wikiDeleteIfSameSysId: false,
                  wikiDeleteIfSameName: false
            },

            userDateFormat$: function() {
                  return clientVM.getUserDefaultDateFormat()
            },

            startDateChanged: function(newDate) {
                  this.set('startDate', newDate);
            },

            endDateChanged: function(newDate) {
                  this.set('endDate', newDate);
            },

            // Menus
            menuSetOpt: {
                  mSetMenuSection: false,
                  mSetMenuItem: false
            },

            // Social Components - Process
            processOpt: {
                  processWiki: true,
                  processUsers: true,
                  processTeams: true,
                  processForums: true,
                  processRSS: true,
                  processATs: true
            },

            // Team
            teamOpt: {
                  teamUsers: true,
                  teamTeams: false
            },

            // Forum
            forumOpt: {
                  forumUsers: true
            },

            // User
            userOpt: {
                  userRoleRel: false,
                  userGroupRel: false,
                  userInsert: false
            },

            // Role
            roleOpt: {
                  roleUsers: false,
                  roleGroups: false
            },

            // Group
            groupOpt: {
                  groupUsers: false,
                  groupRoleRel: false
            },

            // Form
            formOpt: {
                  formTables: false, // Form Custom tables
                  formRBs: true,
                  formSS: true, // Form System Scripts
                  formATs: true // Form ActionTasks
            },

            // Table
            customTableOpt: {
                  tableForms: true, // Custom Forms
                  tableData: false
            },

            // Catalog
            catalogOpt: {
                  catalogWikis: false,
                  catalogRefCatalogs: true
                  // catalogTags: false
            },

            // Worksheet
            worksheetOpt: {
                  //wsPosts: false
            },

            //Wiki Template
            wikiTemplateOpt: {
                  templateForm: true,
                  templateWiki: true
            }
      };

      function process(type) {
            tabs[type + 'Columns'] = [];
            var config = tabs[type + 'StoreConf'];
            Ext.each(config, function(f, idx) {
                  tabs[type + 'Columns'].push({
                        header: '~~' + (f.header || f) + '~~',
                        dataIndex: f.name || f,
                        filterable: idx == 0 || (type != 'menuItem'),
                        sortable: idx == 0 || (type != 'menuItem'),
                        flex: f.flex || 1,
                        hidden: (f.name || f) == 'name' && type == 'menuDefinition',
                        renderer: RS.common.grid.plainRenderer()
                  });
                  tabs[type + 'Store'] = null;
                  tabs[type + 'Selections'] = [];
                  tabs.changes.push(type + 'SelectionsChanged');
            });
            var currentOptObj = tabs[type + 'Opt'];
            var refType = null;
            for (opt in currentOptObj) {
                  tabs[opt] = currentOptObj[opt];
                  tabs[opt + 'IsVisible$'] = function() {
                        return this.type == type;
                  }
            }
      }

      Ext.each(tabs.types, function(t) {
            process(t);
      });
      //these formulas is just too special to process in a generic way
      tabs.mSetMenuItemIsVisible$ = function() {
            return (this.type == 'menuSet' && this.mSetMenuSection) || this.type == 'menuDefinition';
      };

      tabs.mSetMenuSectionChanged$ = function() {
            if (!this.mSetMenuSection && this.type != 'menuItem')
                  this.set('mSetMenuItem', false);
      };

      tabs.wikiOverrideChanged$ = function() {
            if (this.wikiOverride == false) {
                  this.set('wikiDeleteIfSameName', false);
                  this.set('wikiDeleteIfSameSysId', false);
            }
      };

      tabs.wikiDeleteIfSameNameIsVisible$ = function() {
            return (this.type == 'wiki' || this.type == 'wikiNamespace') &&
                  (this.wikiOverride && this.wikiOverrideIsVisible || this.wikiNsOverride && this.wikiNsOverrideIsVisible);
      };

      tabs.wikiDeleteIfSameSysIdIsVisible$ = function() {
            return (this.type == 'wiki' || this.type == 'wikiNamespace') &&
                  (this.wikiOverride && this.wikiOverrideIsVisible || this.wikiNsOverride && this.wikiNsOverrideIsVisible);
      };

      tabs.wikiDeleteIfSameNameChanged$ = function() {
            if (this.wikiDeleteIfSameName)
                  this.set('wikiOverride', true)
      };

      var base = {
            asWindowTitle: '',
            stateId: 'impexcomponentsearch',
            displayName: '',
            wait: false,
            typeStore: {
                  mtype: 'store',
                  fields: ['name', 'value'],
                  proxy: {
                        type: 'memory'
                  }
            },

            type: '',
            columns: [],
            activeItem: '',
            utableName: '',
            utableNameIsValid$: function() {
                  return this.utableName ? true : this.localize('invalidTableName');
            },
            uquery: '',

            when_custom_db_def_changed: {
                  on: ['utableNameChanged', 'uqueryChanged'],
                  action: function() {
                        if (this.utableNameIsValid)
                              this.set('chooseIsEnabled', true);
                  }
            },
            activate: function() {},
            init: function() {
                  this.set('type', 'none');
                  this.createStores();
                  this.set('activeItem', 'selectionTab');
                  this.set('utableName', '');
                  this.set('uquery', '');
            },

            createStores: function() {
                  this.typeStore.add({
                        name: this.localize('none'),
                        value: 'none'
                  });
                  Ext.each(this.types, function(t) {
                        this.typeStore.add({
                              name: this.localize(t),
                              value: t
                        });
                        this.createStore(t);
                  }, this);
            },

            createStore: function(t) {
                  var store = Ext.create('Ext.data.Store', {
                        fields: this[t + 'StoreConf'].concat('id'),
                        remoteSort: true,
                        sorters: [{
                              property: this[t + 'StoreConf'][0].name || this[t + 'StoreConf'][0],
                              direction: 'ASC'
                        }],
                        proxy: {
                              type: 'ajax',
                              url: '/resolve/service/impex/addcomponent/list',
                              extraParams: {
                                    compType: t
                              },
                              reader: {
                                    type: 'json',
                                    root: 'records'
                              },
                              listeners: {
                                    exception: function(e, resp, op) {
										clientVM.displayExceptionError(e, resp, op);
									}
                              }
                        }
                  });
                  store.on('beforeload', function(store, op) {
                        this.set('wait', true);
                  }, this);

                  store.on('load', function(recores, op, suc) {
                        this.set('wait', false);
						/*
                        if (!suc) {
                              clientVM.displayError(this.localize('ListCompErr'));
                        }
						*/
                  }, this);
                  this.set(t + 'Store', store);
            },

            when_type_changed: {
                  on: ['typeChanged'],
                  action: function(now, before) {
                        if (this.type == 'none') return;
                        this[this.type + 'Store'].load();
                        if (before == 'none') {
                              var idx = this.typeStore.find('value', 'none');
                              this.typeStore.removeAt(idx);
                              return;
                        }
                        this.set(before + 'Selections', []);
                  }
            },
            dumper: null,
            chooseIsEnabled: false,
            choose: function() {
                  if (!this.dumper) {
                        this.close();
                        return;
                  }
                  if (this.type == 'customDB') {
                        var store = this[this.type + 'Store'];
                        store.loadData([], false);
                        store.add({
                              utableName: this.utableName,
                              uquery: this.uquery
                        });
                        this.set(this.type + 'Selections', [store.getAt(0)]);
                  }
                  this.dumper(this[this.type + 'Selections'], this.type);
                  this.close();
            },

            close: function() {
                  this.doClose();
            },

            allSelected: false,

            selectAllAcrossPages: function(selectAll) {
                  this.set('allSelected', selectAll)
            },

            selectionChanged: function() {
                  this.selectAllAcrossPages(false)
            },

            when_selection_changed: {
                  on: tabs.changes,
                  action: function() {
                        if (this.type == 'customDB') {
                              if (!/^\s*$/.test(this.utableName))
                                    this.set('chooseIsEnabled', true);
                              else
                                    this.set('chooseIsEnabled', false);
                        } else
                              this.set('chooseIsEnabled', this[this.type + 'Selections'].length > 0);
                  }
            },

            selectionTab: function() {
                  this.set('activeItem', 'selectionTab');
            },

            selectionTabIsPressed$: function() {
                  return this.activeItem == 'selectionTab';
            },

            optionTab: function() {
                  this.set('activeItem', 'optionTab');
            },

            optionTabIsPressed$: function() {
                  return this.activeItem == 'optionTab';
            },

            optionIsVisible$: function() {
                  return this.activeItem == 'optionTab';
            }
      }

      Ext.apply(base, tabs);
      return base;
})());
function defineImpexSearchMetaVMIfNotExist() {
	if (RS.impex.searchMetaDefined)
		return;
	RS.impex.searchMetaDefined = true;
	Ext.Object.each(RS.impex.metaConfigMap, function(metaType, meta) {
		if (metaType == 'customDB')
			return;
		var columns = [];
		Ext.each(meta.fields, function(field, idx) {
			var flexVal = (field == 'usummary' || field.name == 'usummary') ? 2 : 1;
			columns.push({
				header: '~~' + (field.header || field.name || field) + '~~',
				dataIndex: field.name || field,
				filterable: idx == 0 && field.filterable !== false,
				renderer: RS.common.grid.plainRenderer(),
				flex : flexVal
			});
		});
		if(meta.showSysColumn){
			Ext.each(RS.common.grid.getSysColumns(), function(col) {
				if (col.dataIndex == 'sysUpdatedOn'){
					columns.push(Ext.applyIf({
						hidden: false
					}, col))
					meta.fields.push({
						dateFormat : "time",
						format: "time",
						name : "sysUpdatedOn",
						type : "date"
					});
				}
			});
		}	
		var optionsList = [];
		Ext.Object.each(meta.options, function(k, v) {
			if (k.indexOf('$') == -1)
				optionsList.push(k);
		});
		var gridVMConfig = {
			statId: 'impexMeta_' + Ext.id(),
			type: metaType,
			componentStore: {
				mtype: 'store',
				fields: meta.fields,
				remoteSort: true,
				proxy: {
					type: 'ajax',
					url: '/resolve/service/impex/addcomponent/list',
					reader: {
						type: 'json',
						root: 'records'
					},
					listeners: {
						exception: function(e, resp, op) {
							clientVM.displayExceptionError(e, resp, op);
						}
					},
					extraParams: {
						compType: metaType
					}
				}
			},
			resetTime: function() {
				if (this['startDate'])
					this.set('startDate', null);
				if (this['endDate'])
					this.set('endDate', null);
			},
			init: function() {
				this.componentStore.load();
			},
			componentsSelections: [],
			componentColumns: columns,
			componentsIsVisible$: function() {
				return !this.showOption;
			},
			showOption: false,
			selectionTab: function() {
				this.set('showOption', false);
			},
			selectionTabIsPressed$: function() {
				return !this.showOption;
			},
			optionTab: function() {
				this.set('showOption', true);
			},
			optionTabIsPressed$: function() {
				return this.showOption;
			},
			componentsSelectionsChanged$: function() {
				if (this.parentVM.activeItem != this)
					return;
				if (this.componentsSelections && this.componentsSelections.length > 0)
					this.parentVM.set('chooseIsEnabled', true);
				else
					this.parentVM.set('chooseIsEnabled', false);
			},
			getOptions: function() {
				var options = {};
				Ext.each(optionsList, function(o) {
					options[o] = this[o];
				}, this);
				return options;
			},
			getSelectedRawCmps: function() {
				return this.getRawCmps(this.componentsSelections);
			},
			getRawCmps: function(storeRecords) {
				var rawCmps = [];
				Ext.each(storeRecords, function(r) {
					var obj = {};
					Ext.each(RS.impex.metaConfigMap[this.type].fields, function(f) {
						obj[f.name || f] = r.get(f.name || f);
					}, this);
					obj['type'] = this.type;
					rawCmps.push(obj);
				}, this);
				return rawCmps;
			},
			startDateChanged: function(newDate) {
				this.set('startDate', newDate);
			},

			endDateChanged: function(newDate) {
				this.set('endDate', newDate);
			}
		};
		Ext.apply(gridVMConfig, meta.options);
		glu.defModel('RS.impex.' + metaType + 'SearchMeta', gridVMConfig);
	});
	glu.defModel('RS.impex.customDBSearchMeta', {
		utableName: '',
		utableNameIsValid$: function() {
			return this.utableName ? true : this.localize('invalidTableName');
		},
		uquery: '',
		when_custom_db_def_changed: {
			on: ['utableNameChanged', 'uqueryChanged'],
			action: function() {
				if (this.utableNameIsValid)
					this.parentVM.set('chooseIsEnabled', true);
			}
		},
		getOptions: function() {
			return {};
		},
		getSelectedRawCmps: function() {
			return [{
				utableName: this.utableName,
				uquery: this.uquery,
				type: 'customDB'
			}]
		}
	});
}
glu.defModel('RS.impex.ComponentSearchV2', {
	mixins: ['ImpexMetaContainer'],
	optionIsVisible: false,
	dumper: null,
	init: function() {
		this.figureOutTypeAndCat();
		//Set default category when this is loaded for the 1st time.
		var defaultCat = this.typeCatStore.getAt(0);
		this.set('typeCategory', defaultCat ? defaultCat.get('value') : '');
	},
	choose: function() {
		if (this.dumper)
			this.dumper(this.activeItem.getSelectedRawCmps(), this.activeItem.getOptions());
	},
	chooseIsEnabled: false,
	initialSearch: true
});
glu.ns('RS.impex');
RS.impex.componentTypeCategoryConfig = {
	automation: ['actionTask', 'actionTaskModule', 'runbook', 'automationBuilder', 'properties'],
	filters: ['TCPFilter', 'AmqpFilter', 'DatabaseFilter', 'NetcoolFilter', 'XMPPFilter', 'ServiceNowFilter',
		'SNMPFilter', 'HPOMFilter', 'SalesforceFilter', 'EWSFilter', 'ExchangeserverFilter', 'EmailFilter', 'TSRMFilter', 'RemedyxFilter', 'HPSMFilter', 'SSHPool', 'HTTPFilter', 'CASpectrumFilter', 'TIBCOBespokeFilter'],
	actionTask: ['actionTask', 'actionTaskModule', 'cns', 'properties'],
	wiki: ['wiki', 'wikiNamespace', 'wikiTemplate', 'catalog', 'wikiLookup', 'securityTemplate'],
	menus: ['menuSet', 'menuDefinition', 'menuItem'],
	socialAdmin: ['process', 'team', 'forum', 'rss'],
	system: ['systemProperty', 'systemScript', 'tag', 'businessRule', 'customDB', 'scheduler', 'metricThreshold', 'mtNamespace'],
	form: ['form', 'customTable', 'wikiTemplate'],
	user: ['user', 'role', 'group'],
	resolutionRouting: ['ridMapping', 'rrModule', 'rrSchema']
}
glu.defModel('RS.impex.Definition', (function() {
	var tabs = {
		types: ['actionTask', 'actionTaskModule', 'wiki', 'wikiNamespace', 'cns', 'tag', 'wikiLookup', 'scheduler',
			'menuSet', 'menuDefinition', 'menuItem', 'process', 'team', 'forum', 'rss', 'user', 'group', 'role', 'customTable', 'form',
			'customDB', 'businessRule', 'systemScript', 'systemProperty', 'catalog', 'properties', 'wikiTemplate'
		].sort(function(a, b) {
			return a.toLowerCase() > b.toLowerCase() ? 1 : -1
		}),

		actionTaskStoreConf: ['uname', 'unamespace', 'usummary'],
		actionTaskModuleStoreConf: [{
			header: 'umodule',
			name: 'unamespace'
		}],
		wikiStoreConf: ['ufullname', 'usummary'],
		wikiNamespaceStoreConf: ['unamespace'],
		cnsStoreConf: ['uname', 'umodule', 'udescription'],
		tagStoreConf: ['name'],
		wikiLookupStoreConf: ['uregex', 'uwiki', 'uorder'],
		schedulerStoreConf: ['uname', 'urunbook', 'umodule', 'uexpression'],
		menuSetStoreConf: ['name'],
		menuDefinitionStoreConf: [{
			header: 'menuDefTitle',
			name: 'title'
		}, 'name'],
		menuItemStoreConf: ['title', {
			header: 'menuItemTitle',
			name: 'name'
		}],
		processStoreConf: ['u_display_name', 'u_description'],
		teamStoreConf: ['u_display_name'],
		userStoreConf: ['uuserName', 'ufirstName', 'ulastName'],
		groupStoreConf: ['uname', 'udescription'],
		roleStoreConf: ['uname', 'udescription'],
		customTableStoreConf: ['uname'],
		formStoreConf: ['uformName', 'udisplayName'],
		customDBStoreConf: ['utableName', 'uquery'],
		businessRuleStoreConf: ['uname', 'udescription'],
		systemScriptStoreConf: ['uname', 'udescription'],
		systemPropertyStoreConf: ['uname', 'udescription'],
		forumStoreConf: ['u_display_name', 'u_description'],
		rssStoreConf: ['u_display_name', 'u_description'],
		catalogStoreConf: ['name'],
		worksheetStoreConf: ['name'],
		propertiesStoreConf: ['uname', 'umodule', 'uvalue'],
		wikiTemplateStoreConf: ['uname', 'unamespace'],
		changes: [],

		activeItem: ''
	};

	function process(type) {
		tabs[type + 'Columns'] = [];
		tabs[type + 'TabIsPressed$'] = function() {
			return this.activeItem == this.activeItemName(type);
		};
		tabs[type + 'Tab'] = function() {
			this.set('activeItem', this.activeItemName(type))
		};
		var countName = type + 'Count';
		tabs[countName] = 0;
		tabs[type + 'TabIsVisible$'] = new Function("return this." + countName + ">0");
		tabs[type + 'IsVisible$'] = new Function("return this." + countName + ">0&&this.activeItem==this.activeItemName('" + type + "')");
		tabs[type + 'Selections'] = [];
		var config = tabs[type + 'StoreConf'];
		Ext.each(config, function(f) {
			tabs[type + 'Columns'].push({
				header: '~~' + (f.header || f) + '~~',
				dataIndex: f.name || f,
				filterable: true,
				flex: f.flex || 1,
				hidden: (f.name || f) == 'name' && type == 'menuDefinition',
				renderer: RS.common.grid.plainRenderer()
			});
		});
		tabs[type + 'StoreConf'].push('id');
		tabs[type + 'StoreConf'].push('sysUpdatedBy');
		tabs[type + 'StoreConf'].push({
			name: 'sysUpdatedOn',
			type: 'date',
			dateFormat: 'time',
			format: 'time'
		});
		Ext.each(RS.common.grid.getSysColumns(), function(col) {
			if (col.dataIndex == 'sysUpdatedBy' || col.dataIndex == 'sysUpdatedOn')
				tabs[type + 'Columns'].push(Ext.applyIf({
					hidden: false
				}, col))
		});
	}

	var removeEnabled = [];
	tabs.typeMap = {};
	Ext.each(tabs.types, function(t) {
		process(t);
		tabs.typeMap[t.toUpperCase()] = t;
		removeEnabled.push('this.' + t + 'Selections.length>0');
	})

	tabs.removeIsEnabled$ = new Function('return ' + removeEnabled.join('||'));

	var base = {
		wait: false,
		uname: '',
		uforwardWikiDocument: '',
		uscriptName: '',
		udescription: '',
		id: '',
		sysCreatedBy: '',
		sysCreatedOn: '',
		sysUpdatedBy: '',
		sysUpdatedOn: '',
		sysOrganizationName: '',

		rendered: false,
		screenRendered: function() {
			this.set('rendered', true)
		},

		hideAndShow: function(type, visibility) {
			if (this.btnTable)
				this.btnTable[type + 'Tab'][visibility ? 'show' : 'hide']();
			return visibility;
		},

		defaultData: {
			uname: '',
			uforwardWikiDocument: '',
			udescription: '',
			id: '',
			sysCreatedBy: '',
			sysCreatedOn: '',
			sysUpdatedBy: '',
			sysUpdatedOn: '',
			sysOrganizationName: ''
		},

		unameIsValid$: function() {
			return this.uname && (/^([a-zA-Z0-9_\-]+[a-zA-Z0-9_\-\s\.])*[a-zA-Z0-9_\-\s]*[a-zA-Z0-9_\-]$/.test(this.uname) || /^[a-zA-Z0-9_\-]+$/.test(this.uname)) && !/.*\s{2,}.*/.test(this.uname) ? true : this.localize('invalidName');
		},

		uforwardWikiDocumentIsValid$: function() {
			if (!this.uforwardWikiDocument)
				return true;
			if (/[^a-zA-Z0-9_ \-.]+/.test(this.uforwardWikiDocument))
				return this.localize('invalidWikiChars');
			if (!/^[\w]+[.]{1}[\w]+$/.test(this.uforwardWikiDocument))
				return this.localize('invalidWikiFormat');
			return true;
		},
		userDateFormat$: function() {
			return clientVM.getUserDefaultDateFormat()
		},

		activate: function(screen, params) {
			window.document.title = this.localize('windowTitle')
			this.resetForm()
			this.set('id', params ? params.id : '');
			this.set('wait', false);
			if (!!this.id)
				this.loadDefinition()
			var flag = false;
			for (btnName in this.btnTable) {
				flag = this[btnName + 'IsVisibleOverflowWorkAround'];
				this.btnTable[btnName][flag ? 'show' : 'hide']();
			}
		},

		fields: ['uname', 'uforwardWikiDocument', 'uscriptName', 'udescription'].concat(RS.common.grid.getSysFields()),

		init: function() {
			if (this.mock)
				this.backend = RS.impex.createMockBackend(true)
			this.gridFunctionalityBuilder();
		},

		loadDefinition: function() {
			this.set('wait', true);
			this.ajax({
				url: '/resolve/service/impex/get',
				params: {
					moduleId: this.id
				},
				scope: this,
				success: function(response) {
					var respData = RS.common.parsePayload(response);
					if (!respData.success) {
						clientVM.displayError(this.localize('LoadDefinitionErr'), respData.message);
						return;
					}
					var data = respData.data;
					this.populateEverything(data);
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				},
				callback: function() {
					this.set('wait', false);
				}
			});
		},

		populateEverything: function(data) {
			this.loadData(data);
			this.dispatchRecords(data.impexDefinition);
		},

		dispatchRecords: function(records) {
			Ext.each(this.types, function(t) {
				var store = this[t + 'Store'];
				store.removeAll();
			}, this);
			var addedTypes = [];
			Ext.each(records, function(r) {
				var type = this.typeMap[r.type.toUpperCase()];
				var currentStore = this[type + 'Store'];
				if (!r.description)
					return;
				var description = r.description.substring(0, r.description.length - 1);

				description = Ext.JSON.decode(description);
				description.sys_id = r.sys_id; //for sysCreatedOn used in the backend ...don't remove
				var id = '';
				Ext.each(this.glidesMap[type].unique, function(f) {
					id += description[f];
				});
				description.id = id;
				description.sysUpdatedOn = r.sysUpdatedOn;
				description.sysUpdatedBy = r.sysUpdatedBy;
				currentStore.add(description);
				(currentStore.getAt(currentStore.find('id', description.id))).options = r.options;
				addedTypes.push(type);
			}, this);
			addedTypes.sort(function(a, b) {
				return a.toLowerCase() > b.toLowerCase() ? 1 : -1
			});
			if (addedTypes.length > 0)
				this.set('activeItem', this.activeItemName(addedTypes[0]));
			this.updateCountAndSortData();
		},

		addComponents: function() {
			this.open({
				mtype: 'RS.impex.ComponentSearch',
				dumper: (function(self) {
					return function(records, type) {
						var options = {};
						for (opt in this[this.type + 'Opt']) {
							var optName = opt;
							options[optName] = this[opt];
						}
						var sharedWith = this.shared[this.type];
						if (sharedWith) {
							for (type in sharedWith) {
								var opt = sharedWith[type];
								options[opt] = this[opt];
							}
						}
						self.addCompsToList(this.type, records, options);
					}
				})(this)
			});
		},

		remove: function() {
			Ext.each(this.types, function(type) {
				this[type + 'Store'].remove(this[type + 'Selections']);
			}, this)
			this.updateCountAndSortData();
		},

		glidesMap: {
			actionTask: {
				unique: ['unamespace', 'uname'],
				name: 'uname',
				namespace: 'unamespace'
			},
			actionTaskModule: {
				unique: ['unamespace'],
				namespace: 'unamespace'
			},
			wiki: {
				unique: ['ufullname'],
				namespace: 'ufullname'
			},
			wikiNamespace: {
				unique: ['unamespace'],
				namespace: 'unamespace'
			},
			cns: {
				unique: ['uname'],
				name: 'uname',
				namespace: 'umodule'
			},
			tag: {
				unique: ['name'],
				namespace: 'name'
			},
			wikiLookup: {
				unique: ['uregex', 'uwiki', 'uorder'],
				name: 'uregex',
				namespace: 'umodule'
			},
			scheduler: {
				unique: ['umodule', 'uname'],
				name: 'uname',
				namespace: 'umodule'
			},
			menuSet: {
				unique: ['name'],
				namespace: 'name'
			},
			menuDefinition: {
				unique: ['name'],
				namespace: 'name'
			},
			menuItem: {
				unique: ['title', 'name'],
				namespace: 'title',
				name: 'name'
			},
			process: {
				unique: ['u_display_name'],
				namespace: 'u_display_name'
			},
			team: {
				unique: ['u_display_name'],
				namespace: 'u_display_name'
			},
			user: {
				unique: ['uuserName'],
				namespace: 'uuserName'
			},
			group: {
				unique: ['uname'],
				namespace: 'uname'
			},
			role: {
				unique: ['uname'],
				namespace: 'uname'
			},
			customTable: {
				unique: ['uname'],
				// name: 'uname',
				namespace: 'uname'
			},
			form: {
				unique: ['uformName'],
				namespace: 'uformName'
			},
			customDB: {
				unique: ['utableName', 'uquery'],
				name: 'uquery',
				namespace: 'utableName'
			},
			businessRule: {
				unique: ['uname'],
				namespace: 'uname'
			},
			systemScript: {
				unique: ['uname'],
				namespace: 'uname'
			},
			systemProperty: {
				unique: ['uname'],
				namespace: 'uname'
			},
			forum: {
				unique: ['u_display_name'],
				namespace: 'u_display_name'
			},
			rss: {
				unique: ['u_display_name'],
				namespace: 'u_display_name'
			},
			catalog: {
				unique: ['name'],
				namespace: 'name'
			},
			// worksheet: {
			// 	unique: ['name'],
			// 	namespace: 'name'
			// },
			properties: {
				unique: ['umodule', 'uname'],
				name: 'uname',
				namespace: 'umodule'
			},
			wikiTemplate: {
				unique: ['unamespace', 'uname'],
				name: 'uname',
				namespace: 'unamespace'
			}
		},
		updateCountAndSortData: function() {
			Ext.each(this.types, function(type) {
				var store = this[type + 'Store'];
				var firstField = store.model.getFields()[0];
				store.sort(firstField.name, 'ASC');
				this.set(type + 'Count', store.getCount());
			}, this);
		},
		addCompsToList: function(type, records, options) {
			if (!this.glidesMap[type].unique) {
				alert('unique key for:' + type + 'not assigned');
				return;
			}
			var store = this[type + 'Store'];
			this.set('activeItem', this.activeItemName(type));
			Ext.each(records, function(r) {
				var targetToUpdate = null;
				store.each(function(rInStore) {
					var duplicate = true;
					Ext.each(this.glidesMap[type].unique, function(partOfUnique) {
						duplicate = duplicate && r.get(partOfUnique) == rInStore.get(partOfUnique);
					}, this);
					if (duplicate)
						targetToUpdate = rInStore;
				}, this);
				if (!targetToUpdate) {
					r.options = options;
					store.add(r);
				} else {
					targetToUpdate.options = options;
				}
			}, this)
			this.updateCountAndSortData();
		},

		addComponentsIsEnabled$: function() {
			return !this.wait;
		},

		save: function(exitAfterSave) {
			if (this.task) this.task.cancel()
			this.task = new Ext.util.DelayedTask(this.saveDefinition, this, [exitAfterSave])
			this.task.delay(500)
		},
		saveIsEnabled$: function() {
			return !this.wait && this.isValid;
		},
		saveAndExit: function() {
			if (!this.saveIsEnabled)
				return;
			if (this.task) this.task.cancel()
			this.save(true)
		},

		toData: function() {
			var data = this.asObject();
			var glides = [];
			Ext.each(this.types, function(t) {
				var store = this[t + 'Store'];
				var name = this.glidesMap[t].name;
				var namespace = this.glidesMap[t].namespace;

				store.each(function(r) {
					var d = {};
					Ext.each(store.model.getFields(), function(f) {
						d[f.name] = r.get(f.name);
					});
					var def = {
						sys_id: r.get('sys_id'),
						type: t,
						name: r.get(name),
						namespace: r.get(namespace),
						description: Ext.JSON.encode(d) + ' ',
						options: {}
					};
					for (attr in r.options) {
						def.options[attr] = r.options[attr];
					}
					glides.push(def);
				});
			}, this);
			data.impexDefinition = glides;
			return data;
		},
		saveDefinition: function(exitAfterSave) {
			this.set('wait', true);
			var data = this.toData();
			data.uname = Ext.String.trim(data.uname);
			this.ajax({
				url: '/resolve/service/impex/save',
				jsonData: data,
				scope: this,
				success: function(resp) {
					var respData = RS.common.parsePayload(resp);
					if (!respData.success) {
						clientVM.displayError(this.localize('SaveErr') + '[' + respData.message + ']');
						return;
					} else
						clientVM.displaySuccess(this.localize('SaveSuc'));
					var data = respData.data;
					this.populateEverything(data);
					this.set('id', data.id);
					if (exitAfterSave === true)
						this.back();
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				},
				callback: function() {
					this.set('wait', false);
				}
			})
		},
		saveDefinitionIsEnabled$: function() {
			return !this.wait;
		},
		back: function() {
			clientVM.handleNavigation({
				modelName: 'RS.impex.Main'
			});
		},

		refresh: function() {
			if (!this.id)
				return;
			this.loadDefinition();
		},

		resetForm: function() {
			this.loadData(this.defaultData);
			this.set('activeItem', '');
			Ext.each(this.types, function(t) {
				var store = this[t + 'Store'];
				store.loadData([], false);
				this.set(t + 'Count', 0);
			}, this)
		},

		gridFunctionalityBuilder: function() {
			Ext.each(this.types, function(type) {
				this.buildGridFunctionality(type);
			}, this);
		},

		buildGridFunctionality: function(type) {
			var config = this[type + 'StoreConf'];
			this[type + 'Store'] = Ext.create('Ext.data.Store', {
				fields: config.concat(['sys_id', 'id']),
				proxy: {
					type: 'memory'
				}
			});
		},

		activeItemName: function(type) {
			return type + 'Card';
		},

		editComponentOpt: function(id) {
			var type = this.activeItem.substring(0, this.activeItem.length - 4)
			var store = this[type + 'Store'];
			var idx = store.find('id', id);
			var r = store.getAt(idx);
			this.open(Ext.applyIf({
				mtype: 'RS.impex.Component',
				type: type,
				name: this.glidesMap[type].name ? r.get(this.glidesMap[type].name) : '',
				namespace: r.get(this.glidesMap[type].namespace),
				options: r.options,
				dumper: function() {
					for (key in r.options) {
						r.options[key] = this[key];
					}
				}
			}, r.raw))
		}
	};

	Ext.apply(base, tabs);
	return base;
})());
glu.defModel('RS.impex.DefinitionV2', {
	mixins: ['ImpexMetaContainer'],
	wait: false,
	id: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrg: '',

	uname: '',
	unameIsValid$: function() {
		return this.uname && (/^([a-zA-Z0-9_\-]+[a-zA-Z0-9_\-\s\.])*[a-zA-Z0-9_\-\s]*[a-zA-Z0-9_\-]$/.test(this.uname) || /^[a-zA-Z0-9_\-]+$/.test(this.uname)) && !/.*\s{2,}.*/.test(this.uname) ? true : this.localize('invalidName');
	},
	uforwardWikiDocument: '',
	uforwardWikiDocumentIsValid$: function() {
		if (!this.uforwardWikiDocument)
			return true;
		if (/[^a-zA-Z0-9_ \-.]+/.test(this.uforwardWikiDocument))
			return this.localize('invalidWikiChars');
		if (!/^[\w]+[.]{1}[\w]+$/.test(this.uforwardWikiDocument))
			return this.localize('invalidWikiFormat');
		return true;
	},
	uscriptName: '',
	udescription: '',

	defaultData: {
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrg: '',
		uname: '',
		uforwardWikiDocument: ''
	},

	metaVMs: {
		mtype: 'activatorlist',
		focusProperty: 'activeItem',
		autoParent: true
	},
	fields: ['uname', 'uforwardWikiDocument', 'uscriptName', 'udescription'].concat(RS.common.grid.getSysFields()),
	activeItem: '',
	componentsGridIsVisible: false,
	reverseCatTypeMap: {},
	figureOutReverseCatTypeMap: function() {
		Ext.Object.each(RS.impex.componentTypeCategoryConfig, function(k, v) {
			Ext.each(v, function(type) {
				if (!this.reverseCatTypeMap[type])
					this.reverseCatTypeMap[type] = [];
				this.reverseCatTypeMap[type].push(k);
			}, this)
		}, this);
		Ext.Object.each(RS.impex.metaConfigMap, function(k, v) {
			if (!this.reverseCatTypeMap[k])
				this.reverseCatTypeMap[k] = ['uncategorized'];
		}, this);
	},
	filterCategory: function(cat) {
		var contains = false;
		this.metaVMs.forEach(function(metaVM) {
			if (metaVM.type == 'empty')
				return;
			if (this.reverseCatTypeMap[metaVM.type].indexOf(cat) != -1)
				contains = true;
		}, this);
		return contains;
	},
	filterType: function(type) {
		var contains = false;
		this.metaVMs.forEach(function(metaVM) {
			if (metaVM.type == type)
				contains = true;
		});
		return contains;
	},
	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'));
		this.resetForm();
		this.set('id', params && params.id || '');
		if (params && params.id)
			Ext.Function.defer(this.loadDefinition, 500, this);
	},
	batch: false,
	init: function() {
		this.on('activeCmpSelChange', function(vm) {
			if (vm != this.activeItem)
				return false;
			this.set('removeComponentsIsEnbled', vm.componentsSelections.length > 0);
		});
		this.on('cmpStoreChanged', function(vm) {
			if (this.batch)
				return;
			if (vm.componentStore.getCount() == 0) {
				this.metaVMs.remove(vm);
				vm.set('attached', false);
			} else {
				if (!vm.attached) {
					vm.set('attached', true);
					this.metaVMs.add(vm)
				}
			}
			this.set('componentsGridIsVisible', this.metaVMs.length > 1);
			this.figureOutTypeAndCat();
			if (vm.attached) {
				this.set('typeCategory', this.reverseCatTypeMap[vm.type][0]);
				this.selectType(vm.type)
			} else {
				if (this.metaVMs.length > 1) {
					this.set('typeCategory', this.reverseCatTypeMap[this.metaVMs.getAt(1).type][0]);
					this.selectType(this.metaVMs.getAt(1).type)
				}
			}
		}, this);
		this.figureOutReverseCatTypeMap();
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.impex.Main'
		});
	},
	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveDefinition, this, [exitAfterSave])
		this.task.delay(500)
	},
	saveIsEnabled$: function() {
		return !this.wait && this.isValid;
	},
	saveAndExit: function() {
		if (!this.saveIsEnabled)
			return;
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveDefinition: function(exitAfterSave) {
		var defs = [];
		this.metaVMs.forEach(function(vm) {
			defs = defs.concat(vm.getCmpImpexDefinitions());
		});
		var data = this.asObject();
		data.impexDefinition = defs;
		this.set('wait', true);
		data.uname = Ext.String.trim(data.uname);
		this.ajax({
			url: '/resolve/service/impex/save',
			jsonData: data,
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('saveError', respData.message || ''));
					return;
				}
				clientVM.displaySuccess(this.localize('SaveSuc'));
				var data = respData.data;
				this.resetForm();
				this.populateEverything(data);
				this.set('id', data.id);
				if (exitAfterSave === true)
					this.back();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
		return data;
	},
	populateEverything: function(data) {
		this.loadData(data);
		this.dispatchRecords(data.impexDefinition);
	},

	getRawCmpsFromServerResp: function(record) {
		var rawCmps = [];
		var obj = {};
		var desp = Ext.decode(record.description);
		Ext.each(RS.impex.metaConfigMap[record.type].fields, function(f) {
			obj[f.name || f] = desp[f.name || f];
		}, this);
		obj['options'] = record.options;
		obj['sys_id'] = record.sys_id;
		obj['sysUpdatedOn'] = record.sysUpdatedOn;
		obj['sysUpdatedBy'] = record.sysUpdatedBy;
		return obj;
	},
	processAdd: function(r, massager, options) {
		var vm = null;
		var m = null;
		var o = null;
		if (Ext.isFunction(massager)) {
			m = massager;
			o = options;
		} else
			o = massager;
		if (!this.cachedMetaVM[r.type]) {
			var modelName = r.type + (this.viewmodelName.indexOf('Definition') != -1 ? 'Definition' : 'Search') + 'Meta';
			if(!glu.namespace('RS.impex.viewmodels')[modelName]){
			clientVM.displayError(this.localize('invalidDefinitionMsg', r.type));	
				return false;
			}			
			else{
				vm = this.model('RS.impex.' + modelName);
				this.cachedMetaVM[r.type] = vm;
			}			
		}
		vm = this.cachedMetaVM[r.type];
		if (!vm.attached) {
			vm.set('attached', true);
			this.metaVMs.add(vm);
		}
		vm.addOrUpdateCmp(m ? m(r) : r, o);
		return true;
	},
	dispatchRecords: function(records) {
		var invalidRecord = [];
		if (records.length == 0)
			return;
		this.set('batch', true);
		for (var i = 0; i < records.length - 1; i++){
			var added = this.processAdd.apply(this, [records[i], this.getRawCmpsFromServerResp]);
			if(!added){
				records.splice(i, 1);
				i--;
			}
		}	

		this.set('batch', false);
		this.processAdd.apply(this, [records[records.length - 1], this.getRawCmpsFromServerResp]);
		if (this.metaVMs.length > 1) {
			this.set('typeCategory', this.reverseCatTypeMap[records[0].type][0]);
			this.selectType(records[0].type);
		}
	},
	loadDefinition: function() {
		if(!this.id)
			return;
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/impex/get',
			method: 'GET',
			params: {
				moduleId: this.id
			},
			scope: this,
			success: function(response) {
				var respData = RS.common.parsePayload(response);
				if (!respData.success) {
					clientVM.displayError(this.localize('LoadDefinitionErr'), respData.message);
					return;
				}
				var data = respData.data;
				this.populateEverything(data);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},
	resetForm: function() {
		var id = this.id;
		this.loadData(this.defaultData);
		while (this.metaVMs.length > 1)
			this.metaVMs.forEach(function(vm) {
				if (vm.type == 'empty')
					return;
				vm.componentStore.removeAll();
			});
		this.set('id', id);
	},
	refresh: function() {
		this.resetForm();
		this.loadDefinition();
	},	
	addComponents: function() {
		var me = this;
		this.open({
			mtype: 'RS.impex.ComponentSearchV2',
			dumper: function(rawCmps, options) {
				Ext.each(rawCmps, function(rawCmp) {
					me.processAdd(rawCmp, options);
				});
			}
		});
	},
	removeComponentsIsEnbled: false,
	removeComponents: function() {
		this.activeItem.removeSelectedCmps();
	},
	typeSelectionChanged: function(selections) {
		this.set('selectedType', selections.length > 0 ? selections[0] : null);
	}
})

glu.defModel('RS.impex.Impexable', {
	impexing: false,
	startImport: function(module, ids, errorHandler, progressHandler, successHandler, warningHandler) {
		this.startImpex(module, ids, 'import', errorHandler, progressHandler, successHandler, warningHandler);
	},
	startExport: function(module, ids, errorHandler, progressHandler, successHandler, warningHandler) {
		this.startImpex(module, ids, 'export', errorHandler, progressHandler, successHandler, warningHandler);
	},
	startImpex: function(module, ids, operation, errorHandler, progressHandler, successHandler, warningHandler) {
		if (!ids)
			ids = null;
		this.set('impexing', true);
		this.ajax({
			url: Ext.String.format('/resolve/service/impex/{0}', this.operation),
			params: {
				moduleName: module,
				excludeIds: ids
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					this.set('impexing', false);
					errorHandler('beforepolling', respData);
					return;
				}
				if (!respData.data) {
					warningHandler('beforepolling', respData);
					return;
				}
				this.impexPolling(module, operation, errorHandler, progressHandler, successHandler);
			},

			failure: function(resp) {
				this.set('impexing', false);
				errorHandler('serverError');
			}
		});
	},

	impexPolling: function(module, operation, errorHandler, progressHandler, successHandler) {
		Ext.TaskManager.start({
			run: function() {
				this.ajax({
					url: '/resolve/service/impex/impexStatus',
					timeout: 300000,
					params: {
						operation: operation
					},
					scope: this,
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						if (!respData.success && respData.errorCode !==  "IMPEX-105") {
							this.set('impexing', false);
							errorHandler('polling', respData);
							return;
						}
						var data = respData.data;
						if (!(data.module == module && data.operation == operation && data.finished) && this.impexing) {
							setTimeout(function() {
                                this.impexPolling(module, operation, errorHandler, progressHandler, successHandler);
                            }.bind(this), 500);
							progressHandler(respData);
							return;
						}
						this.set('impexing', false);
						successHandler(data);
					},
					failure: function(resp) {
						this.set('impexing', false);
						errorHandler('serverError');
					}
				})
			},
			interval: 1500,
			scope: this,
			repeat: 1
		})
	}
})
glu.defModel('RS.impex.ImpexMetaContainer', {
	metaVMs: {
		mtype: 'activatorlist',
		focusProperty: 'activeItem',
		autoParent: true
	},
	cachedMetaVM: {},
	type: 'empty',
	typeStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	selectedType: null,
	selectedTypeChanged$: function() {
		if (!this.selectedType)
			return;
		this.set('type', this.selectedType.get('value'));
	},
	typeCatStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	selectType: function(type) {
		var t = null;
		this.typeStore.each(function(r) {
			if (r.get('value') == type)
				t = r;
		}, this);
		if (!t)
			return;
		this.set('selectedType', t);
	},
	typeCategory: '',
	updateTypeStore: function() {
		this.typeStore.removeAll();
		if (this.typeCategory == 'uncategorized') {
			Ext.each(this.uncategorizedTypes, function(type) {
				if (type == 'empty' || !this.filterType(type))
					return;
				this.typeStore.add({
					name: this.localize(type),
					value: type
				});
			}, this);
			return;
		}
		if (!RS.impex.componentTypeCategoryConfig[this.typeCategory])
			return;
		Ext.each(RS.impex.componentTypeCategoryConfig[this.typeCategory], function(type) {
			if (type == 'empty' || !this.filterType(type))
				return;
			this.typeStore.add({
				name: this.localize(type),
				value: type
			});
		}, this);
		this.typeStore.sort({
			property: 'name',
			direction: 'ASC'
		});
		this.selectType(this.typeStore.getAt(0).get('value'));
	},
	when_category_changed: {
		on: ['typeCategoryChanged'],
		action: function() {
			this.updateTypeStore();
		}
	},
	filterType: function() {
		return true;
	},
	activeItem: null,
	updateActiveComponentType: function() {
		if (!this.initialSearch)
			this.cachedMetaVM[this.type].set('type', this.type);
		else
			this.set('initialSearch', false);
	},
	typeChanged$: function() {
		defineImpexDefinitionMetaVMIfNotExist();
		defineImpexSearchMetaVMIfNotExist();
		if (!this.type || !RS.impex.metaConfigMap[this.type])
			return;
		if (!this.cachedMetaVM[this.type]) {
			var vm = this.model('RS.impex.' + this.type + (this.viewmodelName.indexOf('Definition') != -1 ? 'Definition' : 'Search') + 'Meta');
			this.metaVMs.add(vm);
			this.cachedMetaVM[this.type] = vm;
		}
		this.set('activeItem', this.cachedMetaVM[this.type]);
	},
	uncategorizedTypes: [],
	filterCategory: function() {
		return true;
	},
	//we can do this kind of naive filtering coz the category is small,if in the future there will be a lot of category then we need to improve the algorithm
	figureOutTypeAndCat: function() {
		var categorizedTypes = {};
		this.set('typeCategory', '');
		this.set('type', '');
		this.typeCatStore.removeAll();
		Ext.Object.each(RS.impex.componentTypeCategoryConfig, function(k, v) {
			if (this.filterCategory(k)) {
				this.typeCatStore.add({
					name: this.localize(k),
					value: k
				});
				Ext.each(v, function(type) {
					categorizedTypes[type] = true;
				})
			}
		}, this);
		var uncategorizedTypes = [];
		Ext.Object.each(RS.impex.metaConfigMap, function(k, v) {
			if (!categorizedTypes[k])
				uncategorizedTypes.push(k)
		}, this);
		if (uncategorizedTypes && this.filterCategory('uncategorized')) {
			// this.typeCatStore.add({
			// 	name: this.localize('uncategorized'),
			// 	value: 'uncategorized'
			// });
			this.set('uncategorizedTypes', uncategorizedTypes);
		}
		this.typeCatStore.sort({
			property: 'name',
			direction: 'ASC'
		});
	}
})
glu.defModel('RS.impex.ImpexOperations', {
    mixins: ['Impexable'],
    title: '',
    moduleName: '',
    wait: false,
    wikiName: null,
    init: function() {
        clientVM.getCSRFToken_ForURI('/resolve/service/wiki/impex/download', function(token_pair) {
            this.csrftoken = token_pair[0] + '=' + token_pair[1];
        }.bind(this))

        this.set('busyText', this.localize('ChooseOperation'));
    },

    busyText: '',
    progress: '',
    progressData: null,
    csrftoken: '',
    progressDataChanged$: function() {
        if (!this.progressData)
            return;
        this.set('progress', this.progressData.data.percent);
        var isMine = this.operation == this.progressData.data.operation && this.progressData.data.module == this.moduleName;
        //this.set('busyText', this.progressData.message);
        if (this.progressData.data.percent == 0)
            this.set('busyText', this.localize(Ext.String.format('validate{0}', isMine ? 'Mine' : 'Other'), {
                name: this.progressData.data.module
            }));
        if (this.progressData.data.percent > 0)
            this.set('busyText', this.localize(Ext.String.format('{0}{1}', this.progressData.data.operation, isMine ? 'Mine' : 'Other'), {
                name: this.progressData.data.module
            }));
        if (this.progressData.data.percent == 100)
            this.set('busyText', this.localize(Ext.String.format('post{0}{1}', this.progressData.data.operation, isMine ? 'Mine' : 'Other'), {
                name: this.progressData.data.module
            }));
    },
    checkRefTable: function(nextAction) {
        this.set('wait', true);
        this.ajax({
            url: '/resolve/service/impex/checkRefCustomTable',
            params: {
                moduleName: this.moduleName
            },
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    //clientVM.displayError(this.localize('checkRefTableError', respData.message));
					clientVM.displayError(respData.message);
                    return;
                }

                if (respData.data) {
                    this.message({
                        title: this.localize('refTableDetected'),
                        msg: this.localize('exportWithRefTable'),
                        buttons: Ext.MessageBox.YESNO,
                        buttonText: {
                            yes: this.localize('exportModule'),
                            no: this.localize('cancel')
                        },
                        fn: function(btn) {
                            if (btn != 'yes')
                                return;
                            nextAction();
                        }
                    })
                } else
                    nextAction();
            },
            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('wait', false);
			}
        })
    },
    checkCustomTable: function(nextAction) {
        this.set('wait', true);
        this.ajax({
            url: '/resolve/service/impex/checkForCustomTables',
            params: {
                moduleName: this.moduleName
            },
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    //clientVM.displayError(this.localize('checkCustomTableError', respData.message));
					clientVM.displayError(respData.message);
                    return;
                }

                if (respData.data) {
                    this.message({
                        title: this.localize('customTableDetected'),
                        msg: this.localize('exportWithCustomTable'),
                        buttons: Ext.MessageBox.YESNO,
                        buttonText: {
                            yes: this.localize('importModule'),
                            no: this.localize('cancel')
                        },
                        fn: function(btn) {
                            if (btn != 'yes')
                                return
                            nextAction();
                        }
                    })
                } else
                    nextAction();
            },
            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('wait', false);
			}
        })
    },
    doImpex: function(ids, operation) {
        var me = this;
        if (operation == 'export')
            this.checkRefTable(function() {
                me.impexAction(ids, operation);
            });
        else
            this.checkCustomTable(function() {
                me.impexAction(ids, operation);
            });
    },
    impexAction: function(ids, operation) {
        var me = this;
        this.set('busyText', this.localize('waitingResponse'));
        this.startImpex(this.moduleName, ids, operation, function(when, data) {
            if (when == 'polling' || when == 'beforepolling') {
                var msg = me.localize(when, {
                    msg: data.message
                });
			} else {
                var msg = me.localize(when);
			}
            clientVM.displayError(msg);
            me.set('busyText', msg);
        }, function(data) {
            me.set('progressData', data);
        }, function(data) {
            me.parentVM.impexGridStore.load();
            me.set('wait', false);
            var status = '.'
            if(data.status) {
                status = ' With ' + data.status + '.'
                me.getLastImpexResult();
            }
            me.set('busyText', me.localize(me.operation + 'Completed') + status);
            clientVM.displaySuccess(me.localize(me.operation + 'Completed') + status);
            var wikiName = data.wikiName || '';
            if (!/^\s*$/.test(wikiName) && me.operation != 'export')
                me.set('wikiName', wikiName);
            else
                me.set('wikiName', 'none');
        }, function(when, data) {
            clientVM.displaySuccess(data.message)
            me.set('busyText', data.message);
        });
    },
    importModule: function() {
        this.doImpex(null, this.operation);
    },
    importModuleIsVisible$: function() {
        return this.wikiName == null && !this.wait && this.operation == 'import' && !this.impexing;
    },
    importModuleIsEnabled$: function() {
        return !this.wait;
    },
    download: function() {
        downloadFile(Ext.String.format('/resolve/service/wiki/impex/download?{0}&filename={1}.zip&moduleId=', this.csrftoken, this.moduleName));
    },
    downloadIsVisible$: function() {
        return this.getLastImpexResultIsVisible && this.operation == 'export';
    },
    exportModule: function() {
        this.doImpex(null, this.operation);
    },
    exportModuleIsVisible$: function() {
        return this.wikiName == null && !this.wait && this.operation == 'export';
    },
    exportModuleIsEnabled$: function() {
        return !this.wait && !this.impexing;
    },
    getLastImpexResult: function() {
        this.set('wait', true);
        this.set('busyText', this.localize(this.operation + 'Result'));
        this.ajax({
            url: '/resolve/service/impex/getImpexResult',
            params: {
                moduleName: this.moduleName
            },
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    clientVM.displayError(this.localize('GetImpexResultErr'), respData.message);
                    return;
                }
                var title = null;
                if (respData.message == 'ERROR')
                    title = this.localize(this.operation + 'Error');
                else
                    title = this.localize(this.operation + 'Suc');
                this.open({
                    mtype: 'RS.impex.ImpexResult',
                    title: title,
                    result: respData.data
                });
            },
            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('wait', false);
			}
        })
    },

    getLastImpexResultIsVisible$: function() {
        return this.wikiName != null;
    },

    startWiki: function() {
        clientVM.handleNavigation({
            modelName: 'RS.wiki.Main',
            params: {
                name: this.wikiName
            },
            target: '_blank'
        });
    },

    startWikiIsVisible$: function() {
        return this.wikiName && this.wikiName != 'none';
    },

    manifest: function() {
        this.open({
            mtype: 'RS.impex.Manifest',
            moduleName: this.moduleName,
            operation: this.operation
        });
    },

    manifestIsEnabled$: function() {
        return !this.wait && !this.impexing;
    },

    manifestIsVisible$: function() {
        return this.wikiName === null && !this.impexing;
    },

    close: function() {
        this.set('impexing', false);
        this.parentVM.impexGridStore.load();
        this.doClose();
    }
});
glu.defModel('RS.impex.Main', {

    activate: function() {
        clientVM.setWindowTitle(this.localize('displayName'))
        this.set('impexGridStoreSelections', []);
        this.impexGridStore.load()
    },

    displayName: '',

    mock: false,

    wait: false,

    impexGridStore: null,

    columns: [],

    init: function() {
        if (this.mock)
            this.backend = RS.impex.createMockBackend(true)
        this.set('impexGridStore', Ext.create('Ext.data.Store', {
            fields: ['uname', 'ulocation', 'uzipFileName'].concat(RS.common.grid.getSysFields()),
            remoteSort: true,
            sorters: [{
                property: 'sysUpdatedOn',
                direction: 'DESC'
            }],
            proxy: {
                type: 'ajax',
                url: '/resolve/service/impex/list',
                reader: {
                    type: 'json',
                    root: 'records'
                },
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
            }
        }));
        this.impexGridStore.parentVM = this;
        this.set('displayName', this.localize('displayName'));
        var sysCols = RS.common.grid.getSysColumns();
        Ext.each(sysCols, function(col) {
            if (col.dataIndex == 'sysUpdatedOn' || col.dataIndex == 'sysUpdatedBy') {
                col.hidden = false;
                col.initialShow = true;
            }
        });
        this.set('columns', [{
            header: '~~uname~~',
            dataIndex: 'uname',
            filterable: 'true',
            width: 350
        }, {
            header: '~~uzipFileName~~',
            dataIndex: 'uzipFileName',
            filterable: 'true',
            flex: 1,
            renderer: RS.common.grid.downloadLinkRenderer('ulocation')
        }].concat(sysCols))
        this.impexGridStore.on('beforeload', function() {
            this.set('wait', true);
        }, this)
        this.impexGridStore.on('load', function(rec, op, suc) {
            this.set('wait', false);
            if (!suc)
                clientVM.displayError(this.localize('ListDefErrMsg'));
        }, this)

        this.loadSDKCustomFilters();
    },

    impexGridStoreSelections: [],


    allSelected: false,

    selectAllAcrossPages: function(selectAll) {
        this.set('allSelected', selectAll)
    },

    selectionChanged: function() {
        this.selectAllAcrossPages(false)
    },

    createDefinition: function() {
        clientVM.handleNavigation({
            modelName: 'RS.impex.DefinitionV2',
            params: {
                id: null
            }
        })
    },

    createDefinitionIsEnabled$: function() {
        return !this.wait;
    },

    deleteDefinitions: function() {
        this.message({
            title: this.localize('DeleteTitle'),
            msg: this.localize(this.impexGridStoreSelections.length > 1 ? 'DeletesMsg' : 'DeleteMsg', {
                num: this.impexGridStoreSelections.length
            }),
            buttons: Ext.MessageBox.YESNO,
            buttonText: {
                yes: this.localize('deleteDefinitions'),
                no: this.localize('no')
            },
            scope: this,
            fn: function(btn) {
                if (btn == 'no')
                    return;
                this.sendReq('/resolve/service/impex/delete', 'DeleteErr', 'DeleteSuc')
            }
        })
    },

    sendReq: function(url, err, suc, params) {
        var ids = [];
        Ext.each(this.impexGridStoreSelections, function(i) {
            ids.push(i.get('id'));
        }, this);
        this.set('wait', true);
        if (!params)
            this.ajax({
                url: url,
                jsonData: ids,
				scope: this,
                success: function(resp) {
                    var respData = RS.common.parsePayload(resp);
                    if (!respData.success)
                        clientVM.displayError(this.localize(err) + '[' + respData.message + ']');
                    else
                        clientVM.displaySuccess(this.localize(suc));
                    this.impexGridStore.load();
                },
                failure: function(resp) {
                    clientVM.displayFailure(resp);
                },
				callback: function() {
					this.set('wait', false);
				}
            });
        else
            this.ajax({
                url: url,
                params: params,
				scope: this,
                success: function(resp) {
                    var respData = RS.common.parsePayload(resp);
                    if (!respData.success)
                        clientVM.displayError(this.localize(err) + '[' + respData.message + ']');
                    else
                        clientVM.displaySuccess(this.localize(suc));
                    this.impexGridStore.load();
                },
                failure: function(resp) {
                    clientVM.displayFailure(resp);
                },
				callback: function() {
					this.set('wait', false);
				}
            });
    },

    deleteDefinitionsIsEnabled$: function() {
        return !this.wait && this.impexGridStoreSelections.length > 0;
    },

    uploadDefinition: function() {
        this.open({
            mtype: 'RS.impex.UploadModule'
        });
    },

    uploadDefinitionIsEnabled$: function() {
        return !this.wait;
    },

    reimportDefinitions: function() {
        var moduleName = this.impexGridStoreSelections[0].get('uname');
        this.open({
            mtype: 'RS.impex.ImpexOperations',
            title: this.localize('importTitle'),
            moduleName: moduleName,
            operation: 'import'
        });

    },

    reimportDefinitionsIsEnabled$: function() {
        return this.impexGridStoreSelections.length == 1 && !this.wait && this.impexGridStoreSelections[0].get('uzipFileName');
    },

    exportDefinitions: function() {
        var moduleName = this.impexGridStoreSelections[0].get('uname');
        this.open({
            mtype: 'RS.impex.ImpexOperations',
            title: this.localize('exportTitle'),
            moduleName: moduleName,
            operation: 'export'
        });
    },

    exportDefinitionsIsEnabled$: function() {
        return this.impexGridStoreSelections.length == 1 && !this.wait;
    },

    editDefinition: function(id) {
        var record = this.impexGridStore.findRecord('id', id);
        if (record && record.raw.isNew) {
            clientVM.message({
                title: this.localize('RedirectToPackageManagerTitle'),
                msg: this.localize('RedirectToPackageManagerMsg'),
                buttons: Ext.MessageBox.OK,
                buttonText: {
                    ok: this.localize('ok'),
                    cancel : this.localize('cancel')
                },
                scope: this,
                fn: function(btn) {
                    if (btn == "ok") {
                        clientVM.handleNavigation({
                            modelName: 'RS.client.URLScreen',
                            params: {
                                location: '/resolve/sir/index.html#/imex'
                            }                           
                        })
                    }
                }
            })
        }
        else {
            clientVM.handleNavigation({
                modelName: 'RS.impex.DefinitionV2',
                params: {
                    id: id
                }
            });
        }        
    },

    loadSDKCustomFilters : function(){
        this.set('wait', true);
        this.ajax({
            url: '/resolve/service/gateway/getCustomGatewayNames',
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (respData.success){
                    var customFilterMap = respData.data;
                    for(var key in customFilterMap){
                        var filterName = customFilterMap[key] + "Filter";

                        RS.impex.componentTypeCategoryConfig.filters.push(filterName);
                        RS.impex.metaConfigMap[filterName] = {
                            fields: ['uname'],
                            glide: {
                                unique: ['uname'],
                                namespace: 'uname'
                            }
                        }
                        RS.impex.locale[filterName] = key;

                        //Define view config for each new custom filter
                        var options = [];
                        defineMetaViewConfig(filterName, options);
                        defineSearchMetaViewConfig(filterName, options);
                    }
                }
            },
            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('wait', false);
			}
        });
    }
});

glu.defModel('RS.impex.ImpexResult', {
    result: '',
    title: '',
    ok: function() {
        this.doClose();
    }
});
glu.defModel('RS.impex.Manifest', {
	componentStore: {
		mtype: 'store',
		fields: ['sys_id', 'uname', 'ufullName', 'uoperation', 'ustatus', 'udisplayType', 'ucompSysId'],
		groupField: 'udisplayType',
		sorters: [{
			property: 'ufullName',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/impex/getManifest',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	moduleName: '',
	operation: '',
	immediateParentVM: null,
	wait: false,
	busyText: '',
	progress: 0,
	ready: false,
	componentColumns: [{
		header: '~~ufullName~~',
		dataIndex: 'ufullName',
		filterable: true,
		sortable: false,
		flex: 1
	}, {
		header: '~~uoperation~~',
		dataIndex: 'uoperation',
		filterable: true,
		sortable: false
	}, {
		header: '~~ustatus~~',
		dataIndex: 'ustatus',
		filterable: false,
		sortable: false
	}, {
		header: '~~udisplayType~~',
		dataIndex: 'udisplayType',
		filterable: true,
		hideable: false,
		hidden: true,
		sortable: false
	}],

	componentsSelections: [],
	when_componentsSelections_changed: {
		on: ['componentsSelectionsChanged'],
		action: function() {
			this.componentStore.each(function(r) {
				for (var idx=0; idx < this.componentsSelections.length; idx++) {
					if (this.componentsSelections[idx].get('ucompSysId') == r.get('ucompSysId')) {
						delete this.excludeList[r.get('ucompSysId')];
						return;
					}
					this.excludeList[r.get('ucompSysId')] = true;
				}
			}, this);
		}
	},
	excludeList: {},
	init: function() {
		this.componentStore.on('beforeload', function(store, op) {
			op.params = op.params || {};
			Ext.applyIf(op.params, {
				moduleName: this.moduleName,
				operation: this.operation
			})
			this.set('wait', true);
		}, this);
		this.componentStore.on('load', function(store) {
			this.set('wait', false);
			var filteredSelections = [];
			this.componentStore.each(function(r) {
				if (this.excludeList[r.get('ucompSysId')])
					return;
				filteredSelections.push(r);
			}, this);
			this.set('componentsSelections', filteredSelections);
		}, this);
		this.prepare();
		this.set('immediateParentVM', this.parentVM);
		this.parentVM = this.parentVM.parentVM || this.parentVM;
	},

	importModule: function() {
		var excludedIds = [];
		for (ucompSysId in this.excludeList)
			if (this.excludeList[ucompSysId])
				excludedIds.push(ucompSysId);
		if (excludedIds.length == 0)
			excludedIds = null;
		this.immediateParentVM.doImpex(excludedIds, 'import');
		this.close();
	},

	importModuleIsVisible$: function() {
		return this.operation == 'import' && this.immediateParentVM && this.immediateParentVM.wikiName == null;
	},

	exportModule: function() {
		var excludedIds = [];
		for (ucompSysId in this.excludeList)
			if (this.excludeList[ucompSysId])
				excludedIds.push(ucompSysId);
		if (excludedIds.length == 0)
			excludedIds = null;
		this.immediateParentVM.doImpex(excludedIds, 'export');
		this.close();
	},

	exportModuleIsVisible$: function() {
		return this.operation == 'export' && this.immediateParentVM;
	},

	prepare: function() {
		this.set('wait', true);
		this.set('busyText', this.localize('ManifestPolling'));
		this.ajax({
			url: '/resolve/service/impex/prepareManifest',
			timeout: 300000,
			params: {
				operation: this.operation,
				moduleName: this.moduleName
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					this.set('wait', false);
					this.set('busyText', this.localize('PrepareManifestErr', {
						msg: respData.message
					}));
					clientVM.displayError(this.localize('PrepareManifestErr', {
						msg: respData.message
					}));
					return;
				}
				if (!respData.data) {
					this.manifestPolling();
					return;
				}
				this.set('wait', false);
				this.set('ready', true);
				this.componentStore.loadPage(1);

			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
				this.set('wait', false);
			}
		})
	},

	manifestPolling: function() {
		Ext.TaskManager.start({
			run: function() {
				this.ajax({
					url: '/resolve/service/impex/manifestStatus',
					timeout: 300000,
					params: {
						operation: this.operation,
						moduleName: this.moduleName
					},
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						if (!respData.success) {
							this.set('wait', false);
							clientVM.displayError('ManifestPollingErr', {
								msg: respData.message
							});
							return;
						}
						if (respData.data.module == this.moduleName && !respData.data.finished && this.wait) {
							setTimeout(function() {
								this.manifestPolling();
							}.bind(this), 500);
							return;
						}
						this.set('wait', false);
						this.set('ready', true);
						this.componentStore.loadPage(1);
					}
				})
			},
			interval: 1500,
			scope: this,
			repeat: 1
		})
	},

	verify: function() {
		this.set('wait', true);
		this.set('busyText', this.localize('verifyInProgress'));
		this.ajax({
			url: '/resolve/service/impex/validateModule',
			timeout: 300000,
			params: {
				moduleName: this.moduleName
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('VerificatioErr') + '[' + respData.message + ']');
					return;
				}
				this.componentStore.load();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},

	verifyIsEnabled$: function() {
		return !this.wait;
	},

	importModuleIsEnabled$: function() {
		return !this.wait;
	},

	verifyIsVisible$: function() {
		return this.operation == 'import'
	},

	close: function() {
		this.set('wait', false);
		this.doClose();
	}
});
glu.defModel('RS.impex.UploadModule', {
	mixins: ['FileUploadManager', 'Impexable', 'ImpexOperations'],
	api: {
		upload: '/resolve/service/impex/uploadFile',
		list: '/resolve/service/impex/list'
	},

	file: true,
	url: false,
	wait: false,

	allowedExtensions: ['zip'],
	fineUploaderTemplateHidden: false,
	checkForDuplicates: false,
	allowMultipleFiles: false,

	uploaderAdditionalClass: 'impex',

	init: function() {
        this.set('wait', false);
        this.operation = 'import';
		this.set('dragHereText', this.localize('dragHere'));
		this.set('busyText', this.localize(Ext.isIE9m ? 'noDragFileIntro' : 'fileIntro'));
	},

	fileOnUploadCallback: function(manager, filename) {
		this.set('wait', true);
	},

	fileOnCompleteCallback: function(manager, filename) {
		this.set('moduleName', filename.substring(0, filename.length - 4));
		this.set('busyText', this.localize('uploadComplete', {
			zipName: ' ' + filename
		}));

        this.set('wait', false);
        this.parentVM.impexGridStore.load();

		this.view.fireEvent('uploadComplete');
	},

	getLastImpexResultIsVisible$: function() {
        return this.wikiName != null;
    },

    getLastImpexResultIsEnabled$: function() {
        return !this.wait;
    },

	importModuleIsVisible$: function() {
		return this.moduleName && this.wikiName == null && !this.wait && this.operation == 'import' && !this.impexing;
    },

    browse: function() {},
    browseIsVisible$: function() {
        return this.file && !this.moduleName;
    },
    manifest: function() {
        this.open({
            mtype: 'RS.impex.Manifest',
            moduleName: this.moduleName,
            operation: 'import'
        });
    },
    manifestIsVisible$: function() {
        return !!this.moduleName && this.wikiName === null && !this.uploading && !this.importing;
    },

	view: null,
	viewRenderCompleted: function(view) {
		this.set('view', view);
	}
});

glu.defModel('RS.impex.FileUploadManagerFactory', function(cfg) {
	Ext.applyIf(cfg, RS.common.viewmodels.FileUploadManager);
	return cfg;
});

/*
glu.defModel('RS.impex.UploadModule', {
    mixins: ['Impexable', 'ImpexOperations'],
    file: true,
    url: false,
    wait: false,
    filePath: '',
    moduleURL: null,
    runtime: '',
    moduleURLIsValid$: function() {
        return this.moduleURL && Ext.form.field.VTypes.url(this.moduleURL) ? true : this.localize('invalidURL');
    },

    init: function() {
        this.set('wait', false);
        this.operation = 'import';
    },
    submit: function() {
        this.set('busyText', this.localize('urlUploadBusy'));
        this.set('wait', true);
        this.ajax({
            url: '/resolve/service/impex/urlUpload',
            params: {
                moduleURL: this.moduleURL
            },
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    clientVM.displayError(this.localize('urlUploadErr'), respData.message);
                    return;
                }
                this.set('moduleName', respData.data);
                this.set('uploading', true);
                this.uploadPolling();
            },
            failure: function(resp) {
                this.set('wait', false);
                clientVM.displayFailure(resp);
            }
        })
    },
    submitIsVisible$: function() {
        return this.url && !this.moduleName;
    },
    submitIsEnabled$: function() {
        return this.moduleURLIsValid === true;
    },
    uploading: false,
    uploadPolling: function() {
        Ext.TaskManager.start({
            run: function() {
                this.uploadPollingAction();
            },
            interval: 1000,
            scope: this,
            repeat: 1
        })
    },

    uploadPollingAction: function() {
        this.ajax({
            url: '/resolve/service/impex/urlUploadStatus',
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    this.set('uploading', false);
                    clientVM.displayError(this.localize('pollingErr'), respData.message);
                    return;
                }

                if (!respData.message && this.wait) {
                    this.uploadPolling();
                    return;
                }
                this.set('uploading', false);
                if (respData.data == '-1') {
                    clientVM.displayError(this.localize('pollingErr'));
                    this.set('busyText', this.localize('pollingErr'));
                    this.message({
                        title: this.localize('urlUploadErr'),
                        msg: respData.message,
                        buttons: Ext.MessageBox.OK,
                        buttonText: {
                            ok: this.localize('ok')
                        },
                        scope: this,
                        fn: function() {
                            this.close();
                        }
                    });
                    return;
                }
                this.set('moduleName', respData.message);
                this.set('busyText', this.localize('uploadComplete', {
                    zipName: ' ' + this.moduleName
                }));
            },
            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('wait', false);
			}
        });
    },

    getLastImpexResultIsVisible$: function() {
        return this.wikiName != null;
    },

    getLastImpexResultIsEnabled$: function() {
        return !this.wait;
    },

    browse: function() {},
    browseIsVisible$: function() {
        return this.file && !this.moduleName;
    },
    manifest: function() {
        this.open({
            mtype: 'RS.impex.Manifest',
            moduleName: this.moduleName,
            operation: 'import'
        });
    },

    manifestIsVisible$: function() {
        return !!this.moduleName && this.wikiName === null && !this.uploading && !this.importing;
    },
    close: function() {
        this.set('wait', false);
        this.doClose();
    },
    fileChanged$: function() {
        if (this.file)
            this.set('busyText', this.localize(Ext.isIE9m ? 'noDragFileIntro' : 'fileIntro'));
        else
            this.set('busyText', this.localize('urlIntro'));
    },

    fileIsEnabled$: function() {
        return !this.wait;
    },
    fileIsVisible$: function() {
        return !this.moduleName;
    },
    urlIsEnabled$: function() {
        return !this.wait;
    },
    urlIsVisible$: function() {
        return !this.moduleName;
    },
    browseIsEnabled$: function() {
        return !this.wait;
    },
    moduleURLIsVisible$: function() {
        return this.url && !this.importing && this.wikiName == null;
    },
    dropAreaIsVisible$: function() {
        return !this.wait && this.file && this.wikiName == null && !this.moduleName && !Ext.isIE9m;
    },
    uploadError: function(uploader, data) {
        clientVM.displayError(this.localize('UploadError') + '[' + data.file.msg + ']');
    },
    filesAdded: function(uploader, data) {
        this.set('busyText', this.localize('fileUploadBusy'));
        this.set('wait', true);
        this.set('uploading', true);
        var name = data[0].name;
        this.set('moduleName', name.substring(0, name.length - 4));
    },
    uploadComplete: function(uploader, data) {
        this.set('busyText', this.localize('uploadComplete', {
            zipName: this.file ? ' "' + this.moduleName + '.zip"' : ''
        }));
        this.set('wait', false);
        this.set('uploading', false);
        this.parentVM.impexGridStore.load();
    },
    runtimeDecided: function(runtime) {
        if (!runtime) {
            alert('no uploader runtime!!')
            return;
        }
        this.set('runtime', runtime.type);
    }
})
*/
glu.defView('RS.impex.Component', function(args) {
	var vm = args.vm;
	var options = [];
	Ext.each(vm.types, function(type) {
		if (type == 'menuDefinition')
			return;
		for (opt in vm[type + 'Opt']) {
			if (opt == 'startDate' || opt == 'endDate')
				continue;

			options.push({
				name: opt,
				boxLabel: '~~' + opt + '~~'
			});
		}
	});
	var config = {
		title: '~~component~~',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		bodyPadding: '10px',
		defaultType: 'displayfield',
		defaults: {
			labelWidth: 100
		},

		items: ['component', 'typeText', {
			xtype: 'panel',
			layout: 'vbox',
			defaultType: 'checkbox',
			title: 'Options',
			defaults: {
				hideLabel: 150
			},
			items: options.concat([{
				xtype: 'datefield',
				hideLabel: false,
				width: 350,
				name: 'startDate',
				format: 'm/d/Y',
				maxValue: '@{endDate}',
				labelWidth: 50,
				listeners: {
					change: function(datefield, newDate, oldDate) {
						datefield.fireEvent('startDateChanged', datefield, newDate);
					},
					startDateChanged: '@{startDateChanged}'
				}
			}, {
				xtype: 'datefield',
				hideLabel: false,
				width: 350,
				name: 'endDate',
				minValue: '@{startDate}',
				format: 'm/d/Y',
				labelWidth: 50,
				listeners: {
					change: function(datefield, newDate, oldDate) {
						datefield.fireEvent('endDateChanged', datefield, newDate);
					},
					endDateChanged: '@{endDateChanged}'
				}
			}])
		}],
		asWindow: {
			title: '~~componentOptions~~',
			modal: true,
			cls : 'rs-modal-popup',
			padding : 15,
			width: 400
		},	
		buttons: [{
			cls : 'rs-med-btn rs-btn-dark',
			name : 'setOptions',
		}, {
			cls : 'rs-med-btn rs-btn-light',
			name :  'close'
		}]
	}

	return config;
});
Ext.Object.each(RS.impex.metaConfigMap, function(metaType, meta) {
	var t = metaType;
	var options = [];
	Ext.Object.each(meta.options, function(k, v) {
		if (k.indexOf('$') != -1)
			return;
		if (k == 'startDate') {
			options.push({
				xtype: 'datefield',
				hideLabel: false,
				width: 350,
				name: 'startDate',
				format: 'm/d/Y',
				maxValue: '@{endDate}',
				value: '@{startDate}',
				labelWidth: 50,
				listeners: {
					change: function(datefield, newDate, oldDate) {
						datefield.fireEvent('startDateChanged', datefield, newDate);
					},
					startDateChanged: '@{startDateChanged}'
				}
			})
			return
		}
		if (k == 'endDate') {
			options.push({
				xtype: 'datefield',
				hideLabel: false,
				width: 350,
				name: 'endDate',
				minValue : '@{startDate}',
				format: 'm/d/Y',
				labelWidth: 50,
				listeners: {
					change: function(datefield, newDate, oldDate) {
						datefield.fireEvent('endDateChanged', datefield, newDate);
					},
					endDateChanged: '@{endDateChanged}'
				}
			})
			return
		}
		options.push({
			name: k,
			boxLabel: '~~' + k + '~~'
		});
	});
	if (options.length > 0)
		options[0].padding = '10 0 0 0';
	defineMetaViewConfig(metaType, options);	
});

function defineMetaViewConfig(metaType, options){
	var grid = {
		layout: 'fit',		
		items: [{
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar rs-dockedtoolbar'
			}],
			name: 'components',
			xtype: 'grid',
			cls : 'rs-grid-dark',
			layout: 'fit',
			store: '@{componentStore}',
			columns: '@{componentColumns}',
			selModel: {
				selType: 'resolvecheckboxmodel',
				columnTooltip: RS.common.locale.editColumnTooltip,
				glyph : 0xF044,
				columnTarget: '_self',
				columnEventName: 'editAction',
				columnIdField: 'tmpBrowserId'
			},
			viewConfig: {
				enableTextSelection: true
			},
			listeners: {
				editAction: '@{editOption}'
			}
		}]
	};

	var optionPanel = {
		layout: 'vbox',
		defaultType: 'checkboxfield',
		defaults: {
			hideLabel: true
		},
		items: options
	};
	glu.defView('RS.impex.' + metaType + 'DefinitionMeta', grid);
	glu.defView('RS.impex.' + metaType + 'DefinitionMetaOption', {		
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			title: '~~component~~',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'displayfield',
				name: 'component'
			}, {
				xtype: 'displayfield',
				name: 'typeText'
			}]
		}, {
			title: '~~options~~',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaultType: 'checkboxfield',
			defaults: {
				hideLabel: true
			},
			items: options
		}],
		asWindow: {
			title: '~~componentOptions~~',
			cls : 'rs-modal-popup',
			width: 400,
			padding : 15,
			modal: true
		},	
		buttons: [{
			name : 'update', 
			cls : 'rs-med-btn rs-btn-dark',
		},{
			name : 'cancel',
			cls : 'rs-med-btn rs-btn-light'
		}]
	});
}
glu.defView('RS.impex.ComponentSearch', function(args) {
	var vm = args.vm;
	var grid = null;
	var panels = [];
	Ext.each(vm.types, function(t) {
		if (t != 'customDB')
			grid = {
				layout: 'fit',
				id: t,
				buttonAlign: 'left',
				dockedItems: [{
					xtype: 'toolbar',
					dock: 'top',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: [{
						xtype: 'toolbar',
						items: [{
							xtype: 'combobox',
							editable: false,
							store: '@{typeStore}',
							displayField: 'name',
							valueField: 'value',
							queryMode: 'local',
							name: 'type',
							width: 230,
							labelWidth: 50
						}, {
							minWidth: 300,
							xtype: 'combobox',
							itemId: t + 'filter',
							emptyText: '~~filter~~',
							inputCls: 'post-message',
							filterStore: '@{' + t + 'Store}',
							displayField: 'name',
							valueField: 'value',
							autoSelect: false,
							margins: {
								left: 5
							},
							getFilterBar: function() {
								return this.ownerCt.ownerCt.ownerCt.down('#' + t + 'filterBar');
							},
							columns: '@{' + t + 'Columns}',
							plugins: [{
								ptype: 'searchfieldfilter'
							}],
							listeners: {
								beforerender: function(combo) {
									Ext.Array.forEach(combo.columns, function(column) {
										column.text = column.header;
									})
								}
							}
						}, '->', 'selectionTab', 'optionTab']
					}]
				}, {
					xtype: 'filterbar',
					itemId: t + 'filterBar',
					allowPersistFilter: false,
					listeners: {
						add: {
							fn: function() {
								this.ownerCt.down('#' + t + 'filter').getPlugin().filterChanged()
							},
							buffer: 10
						},
						remove: {
							fn: function() {
								this.ownerCt.down('#' + t + 'filter').getPlugin().filterChanged()
							},
							buffer: 10
						},
						clearFilter: function() {
							this.ownerCt.down('#' + t + 'filter').clearValue()
						}
					}
				}],
				items: [{
					dockedItems: [{
						xtype: 'toolbar',
						dock: 'top',
						cls: 'actionBar',
						name: 'actionBar',
						items: []
					}],
					name: t,
					xtype: 'grid',
					cls : 'rs-grid-dark',
					layout: 'fit',
					store: '@{' + t + 'Store}',
					columns: '@{' + t + 'Columns}',
					selModel: {
						selType: 'checkboxmodel',
						mode: 'MULTI'
					},
					plugins: [{
						ptype: 'pager',
						showSysInfo: false
					}, {
						ptype: 'columnautowidth'
					}]
				}]
			};
		else
			grid = {
				id: t,
				xtype: 'panel',
				dockedItems: [{
					xtype: 'toolbar',
					dock: 'top',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: [{
						xtype: 'toolbar',
						items: [{
							xtype: 'combobox',
							editable: false,
							store: '@{typeStore}',
							displayField: 'name',
							valueField: 'value',
							queryMode: 'local',
							name: 'type',
							width: 230,
							labelWidth: 50
						}]
					}]
				}],
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [{
					flex: 1,
					xtype: 'panel',
					title: '~~definition~~',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					bodyPadding: '10px',
					items: [{
						xtype: 'textfield',
						name: 'utableName'
					}, {
						flex: 1,
						xtype: 'textarea',
						name: 'uquery'
					}]
				}]
			};
		panels.push(grid);
	});
	var none = {
		layout: 'fit',
		id: 'none',
		buttonAlign: 'left',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'toolbar',
				items: [{
					xtype: 'combobox',
					editable: false,
					store: '@{typeStore}',
					displayField: 'name',
					valueField: 'value',
					queryMode: 'local',
					name: 'type',
					width: 230,
					labelWidth: 50
				}, {
					minWidth: 300,
					xtype: 'combobox',
					disabled: true,
					itemId: 'nonefilter',
					emptyText: '~~filter~~',
					inputCls: 'post-message',
					filterStore: Ext.create('Ext.data.Store', {
						fields: [],
						proxy: {
							type: 'memory'
						}
					}),
					margins: {
						left: 5
					},
					getFilterBar: function() {
						return null;
					},
					columns: [],
					plugins: [{
						ptype: 'searchfieldfilter'
					}]
				}, '->', 'selectionTab', 'optionTab']
			}]
		}]
	};
	panels.push(none);
	var options = [];
	Ext.each(vm.types, function(type) {
		if (type == 'menuDefinition')
			return;
		for (opt in vm[type + 'Opt']) {
			if (opt == 'startDate' || opt == 'endDate')
				continue;

			options.push({
				name: opt,
				boxLabel: '~~' + opt + '~~'
			});
		}
	});

	var base = {
		xtype: 'form',
		autoScroll: true,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		bodyPadding: '10px',
		items: [{
			flex: 1,
			xtype: 'panel',
			layout: {
				type: 'card'
			},
			activeItem: '@{type}',
			hidden: '@{optionIsVisible}',
			items: panels
		}, {
			xtype: 'panel',
			hidden: '@{!optionIsVisible}',
			layout: 'vbox',
			defaultType: 'checkbox',
			defaults: {
				hideLabel: true,
				padding: '0px 15px 0px 0px'
			},
			dockedItems: [{
				xtype: 'toolbar',
				cls: 'actionBar actionBar-form',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [{
					flex: 1,
					xtype: 'toolbar',
					items: [{
						xtype: 'tbtext',
						cls: 'rs-display-name',
						text: '~~options~~'
					}, '->', 'selectionTab', 'optionTab']
				}],
				margin: '0px 0px 30px 0px'
			}],
			items: options.concat([{
				xtype: 'datefield',
				hideLabel: false,
				width: 350,
				name: 'startDate',
				format: 'm/d/Y',
				maxValue: '@{endDate}',
				labelWidth: 50,
				listeners: {
					change: function(datefield, newDate, oldDate) {
						datefield.fireEvent('startDateChanged', datefield, newDate);
					},
					startDateChanged: '@{startDateChanged}'
				}
			}, {
				xtype: 'datefield',
				hideLabel: false,
				width: 350,
				name: 'endDate',
				minValue : '@{startDate}',
				format: 'm/d/Y',
				labelWidth: 50,
				listeners: {
					change: function(datefield, newDate, oldDate) {
						datefield.fireEvent('endDateChanged', datefield, newDate);
					},
					endDateChanged: '@{endDateChanged}'
				}
			}])
		}],
		buttons: [{
			name: 'choose',
			cls : 'rs-med-btn rs-btn-dark',
			enabled: '@{chooseIsEnabled}'
		},{
			cls : 'rs-med-btn rs-btn-dark',
			name : 'close'
		}],
		asWindow: {
			title: '~~searchComponentTitle~~',
			padding : 15,
			cls : 'rs-modal-popup',
			modal: true,
			height: 480,
			width: 760
		}
	};
	return base;
});
Ext.Object.each(RS.impex.metaConfigMap, function(metaType, meta) {
	if (metaType == 'customDB')
		return;
	var t = metaType;
	var options = [];
	Ext.Object.each(meta.options, function(k, v) {
		if (k.indexOf('$') != -1)
			return;
		if (k == 'startDate')
			options.push({
				xtype: 'datefield',
				hideLabel: false,
				width: 350,
				name: k,
				format: 'm/d/Y',
				maxValue: '@{endDate}',
				labelWidth: 50,
				listeners: {
					change: function(datefield, newDate, oldDate) {
						datefield.fireEvent('startDateChanged', datefield, newDate);
					},
					startDateChanged: '@{startDateChanged}'
				}
			});
		else if (k == 'endDate')
			options.push({
				xtype: 'datefield',
				hideLabel: false,
				width: 350,
				name: 'endDate',
				minValue : '@{startDate}',
				format: 'm/d/Y',
				labelWidth: 50,
				listeners: {
					change: function(datefield, newDate, oldDate) {
						datefield.fireEvent('endDateChanged', datefield, newDate);
					},
					endDateChanged: '@{endDateChanged}'
				}
			});
		else
			options.push({
				name: k,
				boxLabel: '~~' + k + '~~'
			});
	});
	defineSearchMetaViewConfig(metaType, options);
});
function defineSearchMetaViewConfig(metaType, options){
	var grid = {
		layout: 'fit',
		buttonAlign: 'left',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'toolbar',
				layout: 'hbox',
				items: [{
					xtype: 'tbtext',
					cls: 'rs-display-name',
					text: '~~options~~',
					hidden: '@{!showOption}'
				}, {
					minWidth: 300,
					xtype: 'combobox',
					itemId: metaType + 'filter',
					emptyText: '~~filter~~',
					inputCls: 'post-message',
					filterStore: '@{componentStore}',
					hidden: '@{showOption}',
					displayField: 'name',
					valueField: 'value',
					flex: 1,
					margins: {
						left: 5
					},
					plugins: [{
						ptype: 'searchfieldfilter'
					}],
					getFilterBar: function() {
						return this.ownerCt.ownerCt.ownerCt.down('#' + metaType + 'filterBar');
					},
					columns: '@{componentColumns}',
					plugins: [{
						ptype: 'searchfieldfilter'
					}],
					listeners: {
						beforerender: function(combo) {
							Ext.Array.forEach(combo.columns, function(column) {
								column.text = column.header;
							});
						}
					}
				}, '->', 'selectionTab', 'optionTab']
			}]
		}, {
			xtype: 'filterbar',
			itemId: metaType + 'filterBar',
			hidden: '@{showOption}',
			allowPersistFilter: false,
			listeners: {
				add: {
					fn: function() {
						this.ownerCt.down('#' + metaType + 'filter').getPlugin().filterChanged()
					},
					buffer: 10
				},
				remove: {
					fn: function() {
						this.ownerCt.down('#' + metaType + 'filter').getPlugin().filterChanged()
					},
					buffer: 10
				},
				clearFilter: function() {
					this.ownerCt.down('#' + metaType + 'filter').clearValue()
				}
			}
		}],
		items: [{
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar rs-dockedtoolbar',
				name: 'actionBar',
				items: []
			}],
			name: 'components',
			xtype: 'grid',
			cls : 'rs-grid-dark',
			layout: 'fit',
			store: '@{componentStore}',
			columns: '@{componentColumns}',
			plugins: [{
				ptype: 'pager',
				showSysInfo: false
			}, {
				ptype: 'columnautowidth'
			}]
		}, {
			hidden: '@{!showOption}',
			defaultType: 'checkboxfield',
			defaults: {
				hideLabel: true
			},
			items: options
		}],
		listeners: {
			activate: '@{resetTime}'
		}
	};
	glu.defView('RS.impex.' + metaType + 'SearchMeta', grid);
}
glu.defView('RS.impex.customDBSearchMeta', {
	title: '',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'textfield',
		name: 'utableName'
	}, {
		xtype: 'textarea',
		flex: 1,
		name: 'uquery'
	}]
});
glu.defView('RS.impex.ComponentSearchV2', {
	bodyPadding: '0 0 8 0',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	items: [{
		width: 200,
		minWidth: 200,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			padding: '3 0 0 0',
			xtype: 'combo',
			hideLabel: true,
			name: 'typeCategory',
			queryMode: 'local',
			displayField: 'name',
			emptyText: '~~selectCategory~~',
			editable: false,
			valueField: 'value',
			store: '@{typeCatStore}'
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			flex: 1,
			store: '@{typeStore}',
			selected: '@{selectedType}',
			hideHeaders: true,
			columns: [{
				header: '~~name~~',
				dataIndex: 'name',
				flex: 1
			}]
		}]
	}, {
		xtype: 'splitter'
	}, {
		flex: 1,
		xtype: 'panel',
		layout: 'card',
		activeItem: '@{activeItem}',
		hidden: '@{optionIsVisible}',
		items: '@{metaVMs}'
	}],	
	buttons: [{
		name: 'choose',
		cls  :'rs-med-btn rs-btn-dark',
		disabled: '@{!chooseIsEnabled}'
	}, {
		name : 'close',
		cls : 'rs-btn-light rs-med-btn'
	}],
	asWindow: {
		title: '~~searchComponentTitle~~',
		modal: true,
		padding : 15,
		cls : 'rs-modal-popup',
		height: 700,
		width: 900,
		listeners: {
			show: function() {
				this.setHeight(700 > Ext.fly(document.body).getHeight() ? Ext.fly(document.body).getHeight() : 700);
				this.setWidth(900 > Ext.fly(document.body).getWidth() ? Ext.fly(document.body).getWidth() : 900);
			}
		}
	}
});
glu.defView('RS.impex.Definition', function(arg) {
	var vm = arg.vm;
	var tabs = [];
	var grids = [];
	Ext.each(vm.types, function(attr) {
		tabs.push({
			xtype: 'button',
			name: attr + 'Tab',
			listeners: {
				show: function() {
					if (!vm[attr + 'TabIsVisible'])
						this.hide();
				}
			}
		});

		grids.push({
			xtype: 'grid',
			cls : 'rs-grid-dark',
			minHeight: 300,
			title: '~~' + attr + '~~',
			id: attr + 'Card',
			name: attr,
			columns: '@{' + attr + 'Columns}',
			store: '@{' + attr + 'Store}',
			selModel: {
				selType: 'resolvecheckboxmodel',
				columnTooltip: RS.common.locale.editColumnTooltip,
				columnTarget: '_self',
				columnEventName: 'editAction',
				columnIdField: 'id'
			},
			listeners: {
				editAction: '@{editComponentOpt}'
			}
		});
	});
	return {
		xtype: 'panel',
		autoScroll: true,
		bodyPadding: '10px',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			ui: 'display-toolbar',
			defaultButtonUI: 'display-toolbar-button',
			items: [{
				xtype: 'tbtext',
				cls: 'rs-display-name',
				text: '~~impexDefinitionTitle~~'
			}]
		}, {
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar actionBar-form',
			items: [{
				name: 'back',
				iconCls: 'icon-large icon-eject rs-icon',
				text: ''
			}, {
				name: 'save',
				listeners: {
					doubleClickHandler: '@{saveAndExit}',
					render: function(button) {
						button.getEl().on('dblclick', function() {
							button.fireEvent('doubleClickHandler')
						});
						clientVM.updateSaveButtons(button);
					}
				}
			}, '->', {
				xtype: 'sysinfobutton',
				sysId: '@{id}',
				sysCreated: '@{sysCreatedOn}',
				sysCreatedBy: '@{sysCreatedBy}',
				sysUpdated: '@{sysUpdatedOn}',
				sysUpdatedBy: '@{sysUpdatedBy}',
				sysOrg: '@{sysOrganizationName}',
				dateFormat: '@{userDateFormat}'
			}, {
				iconCls: 'x-tbar-loading',
				handler: '@{refresh}',
				listeners: {
					render: function(button) {
						clientVM.updateRefreshButtons(button);
					}
				}
			}]
		}],
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'panel',
			layout: 'hbox',
			items: [{
				xtype: 'panel',
				flex: 1,
				defaultType: 'textfield',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: ['uname', {
					name: 'uforwardWikiDocument',
					tooltip: '~~forwardTooltip~~',
					listeners: {
						render: function(p) {
							p.getEl().down('input').set({
								'data-qtip': p.tooltip
							});
						}
					}
				}, 'uscriptName', {
					xtype: 'textarea',
					name: 'udescription'
				}]
			}]
		}, {
			xtype: 'panel',
			title: '~~Components~~',
			flex: 1,
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar',
				name: 'actionBar',
				items: ['addComponents', 'remove', '-'].concat(tabs),
				listeners: {
					overflowchange: function(lastHiddenCount, hiddenCount, hiddenItems) {
						Ext.each(this.items.items, function(tab) {
							if (/.*Tab$/.test(tab.name))
								tab.initialConfig.hidden = false;
						})
					}
				}
			}],
			layout: 'fit',
			activeItem: '@{activeItem}',
			items: grids
		}]
	}
})

glu.defView('RS.impex.DefinitionV2', {
	padding: 15,
	xtype: 'panel',
	autoScroll: true,	
	dockedItems: [{
		xtype: 'toolbar',	
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls : 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '~~impexDefinitionTitle~~'
		}]
	}, {
		xtype: 'toolbar',
		cls: 'actionBar rs-dockedtoolbar',	
		defaults : {
			cls :'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'	
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'panel',
		style : 'border-top:1px solid silver',
		padding : '5 0',	
		layout: 'hbox',		
		items: [{
			xtype: 'panel',
			flex: 1,
			defaultType: 'textfield',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults : {
				margin : '4 0',
			},
			items: ['uname', {
				name: 'uforwardWikiDocument',
				tooltip: '~~forwardTooltip~~',
				listeners: {
					render: function(p) {
						p.getEl().down('input').set({
							'data-qtip': p.tooltip
						});
					}
				}
			}, 'uscriptName', {
				xtype: 'textarea',
				name: 'udescription'
			}]
		}]
	}, {
		flex: 1,
		xtype: 'panel',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		minHeight: 100,
		title: '~~Components~~',
		dockedItems: [{
			xtype: 'toolbar',
			margin : '8 0 0 0',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light',
			},
			items: ['addComponents', {
				name: 'removeComponents',
				disabled: '@{!removeComponentsIsEnbled}'
			}]
		}],
		items: [{
			hidden: '@{!componentsGridIsVisible}',
			width: 200,
			minWidth: 200,
			margin : '8 0 0 0',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'combo',
				hideLabel: true,
				name: 'typeCategory',
				queryMode: 'local',
				editable: false,
				displayField: 'name',
				emptyString: '~~selectCategory~~',
				valueField: 'value',
				store: '@{typeCatStore}'
			}, {
				xtype: 'grid',
				cls : 'rs-grid-dark',
				store: '@{typeStore}',
				hideHeaders: true,
				selectedType: "@{selectedType}",
				setSelectedType: function(type) {
					var sm = this.getSelectionModel();
					sm.select(type || []);
				},
				flex: 1,
				columns: [{
					header: '~~name~~',
					dataIndex: 'name',
					flex: 1
				}],
				listeners: {
					selectionschanged: '@{typeSelectionChanged}'
				}
			}]
		}, {
			hidden: '@{!componentsGridIsVisible}',
			xtype: 'splitter'
		}, {
			hidden: '@{!componentsGridIsVisible}',
			flex: 1,
			xtype: 'panel',
			layout: 'card',
			activeItem: '@{activeItem}',
			items: '@{metaVMs}'
		}]
	}]
});

glu.defView('RS.impex.ImpexOperations', {
    xtype: 'panel',  
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [{
        // /xtype: 'text',
        html: '@{busyText}'
    },
    //  {
    //     xtype: 'container',
    //     layout: 'hbox',
    //     items: [{
    //         // /xtype: 'text',
    //         html: '@{busyText}'
    //     }, {
    //         hidden: '@{!wait}',
    //         html: '<i class="icon-spinner icon-spin icon-large"></i>'
    //     }]
    // },
     {
        id: 'pg',
        xtype: 'progressbar',
        hidden: '@{!impexing}'
    }, {
        xtype: 'displayfield',
        hidden: true,
        value: '@{progress}',
        listeners: {
            change: function(field, newData) {
                var progressBar = this.ownerCt.down('#pg');
                progressBar.updateProgress(newData / 100, newData + '%');
            }
        }
    }], 
    buttons: [{
        name :'importModule', 
        cls : 'rs-med-btn rs-btn-dark',
    },{
        name : 'exportModule', 
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'manifest',
         cls : 'rs-med-btn rs-btn-dark',
    },{
        name : 'startWiki',
         cls : 'rs-med-btn rs-btn-dark',
    },{ 
        name : 'getLastImpexResult',
        cls : 'rs-med-btn rs-btn-dark',
    },{
        name : 'download',
        cls : 'rs-med-btn rs-btn-dark',
    },{
        name : 'close',
        cls : 'rs-med-btn rs-btn-light',
    }],
    asWindow: {
        title: '@{title}',
        width: 460,
        height: 180,
        cls : 'rs-modal-popup',
        padding : 15,
        modal: true,
        closable: false
    }
});
glu.defView('RS.impex.Main', {
    padding : 15,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [{
        flex: 1,
        xtype: 'grid',
        cls : 'rs-grid-dark',
        name: 'impexGridStore',
        columns: '@{columns}',
        displayName: '@{displayName}',
        plugins: [{
            ptype: 'searchfilter',
            allowPersistFilter: false,
            hideMenu: true
        }, {
            ptype: 'pager'
        }],
        selModel: {
            selType: 'resolvecheckboxmodel',         
            columnTarget: '_self',
            columnEventName: 'editAction',
            columnIdField: 'id'
        },
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            cls: 'actionBar rs-dockedtoolbar',
            name: 'actionBar',
            defaults : {
                cls : 'rs-small-btn rs-btn-light',
            },
            items: ['createDefinition', 'deleteDefinitions', '-', 'uploadDefinition', 'reimportDefinitions', 'exportDefinitions']
        }],
        viewConfig: {
            enableTextSelection: true
        },
        listeners: {
            editAction: '@{editDefinition}',
            selectAllAcrossPages: '@{selectAllAcrossPages}',
            selectionChange: '@{selectionChanged}'
        }
    }]
});

glu.defView('RS.impex.ImpexResult', {
    xtype: 'panel',
    align: 'fit',
    items: [{
        html: '@{..result}',
        listeners: {
            afterrender: function(panel, eOpt) {
                clientVM.makeCtrlASelectAllOnView(panel);
            }
        }
    }],
    // items:[{
    //     flex:1,
    //     xtype:'displayfield',
    //     name:'result',
    //     hideLabel:true
    // }],
    autoScroll: true,   
    buttons: [{
        name : 'ok',
        cls : 'rs-med-btn rs-btn-dark'
    }],
    asWindow: {
        modal: true,
        height: 600,
        padding : 15,
        cls : 'rs-modal-popup',
        width: 600,
        title: '@{title}'
    }
});
glu.defView('RS.impex.Manifest', function(args) {
	var vm = args.vm;
	return {
		xtype: 'panel',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			hidden: '@{ready}',
			xtype: 'panel',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'label',
						text: '@{busyText}'
					}, {
						html: '<i class="icon-spinner icon-spin icon-large"></i>'
					}]
				}
				// {
				// 	id: 'pg1',
				// 	hidden: '@{!importing}',
				// 	xtype: 'progressbar',
				// }, {
				// 	xtype: 'displayfield',
				// 	hidden: true,
				// 	value: '@{progress}',
				// 	listeners: {
				// 		change: function(field, newData) {
				// 			var progressBar = this.ownerCt.down('#pg1');
				// 			progressBar.updateProgress(newData / 100, newData + '%');
				// 		}
				// 	}
				// }
			]
		}, {
			flex: 1,
			cls: 'manifest-component',
			xtype: 'grid',
			cls : 'rs-grid-dark',
			displayName: '~~Components~~',
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar rs-dockedtoolbar',
				name: 'actionBar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items: [{
					xtpye: 'button',
					text: '~~expandAll~~',
					handler: function() {
						var feature = this.ownerCt.ownerCt.view.getFeature('group');
						feature.expandAll();
					}
				}, {
					xtype: 'button',
					text: '~~collapseAll~~',
					handler: function() {
						var feature = this.ownerCt.ownerCt.view.getFeature('group');
						feature.collapseAll();
					}
				}]
			}],
			plugins: [{
				ptype: 'searchfilter',
				allowPersistFilter: false,
				hideMenu: true
			}, {
				ptype: 'pager'
			}],
			hidden: '@{!ready}',
			name: 'components',
			store: '@{componentStore}',
			columns: [{
				header: '~~ufullName~~',
				dataIndex: 'ufullName',
				filterable: true,
				sortable: false,
				flex: 1
			}, {
				header: '~~uoperation~~',
				dataIndex: 'uoperation',
				filterable: true,
				sortable: false
			}, {
				header: '~~udisplayType~~',
				dataIndex: 'udisplayType',
				filterable: true,
				hideable: false,
				hidden: true,
				sortable: false
			}].concat(vm.operation == 'EXPORT' ? [] : [{
				header: '~~ustatus~~',
				dataIndex: 'ustatus',
				filterable: false,
				sortable: false
			}]),

			selModel: {
				selType: 'checkboxmodel',
				onRowMouseDown: true,  // Dirty trick to fix the M -> V binding issue
				checkOnly: true,
				mode: 'MULTI',
				listeners: {
					select: function(item, record, index, eOpts) {
						var view = item.view,
							feature = view.getFeature('group'),
							el = view.getEl();

						if (el) {
							var groupField = feature.getGroupField(),
								group = record.get(groupField),
								groupId = view.id + '-' + 'hd' + '-' + group,
								selModel = view.getSelectionModel(),
								grpCheckboxList = el.query('.grpCheckbox'),
								isChecked = true;

							for (var i = 0; i < view.store.data.items.length; i++) {
								var rec = view.store.data.items[i];
								if (rec.get(groupField) == group) {
									if (!selModel.isSelected(rec)) {
										isChecked = false;
										break;
									}
								}
							}

							for (var i = 0; i < grpCheckboxList.length; i++) {
								var grpCheckbox = grpCheckboxList[i],
									currentGroupId = grpCheckbox.parentElement.parentElement.id;

								if (currentGroupId == groupId) {
									var groupHeader = document.getElementById(currentGroupId),
										groupHeaderCheckbox = groupHeader.getElementsByClassName('grpCheckbox');

									if (isChecked) {
										groupHeaderCheckbox[0].setAttribute('class', 'grpCheckbox checked');
									} else {
										groupHeaderCheckbox[0].setAttribute('class', 'grpCheckbox');
									}
									break;
								}
							}
						}
					},
					deselect: function(item, record, index, eOpts) {
						var view = item.view,
							feature = view.getFeature('group'),
							el = view.getEl();

						if (el) {
							var groupField = feature.getGroupField(),
								groupId = view.id + '-' + 'hd' + '-' + record.get(groupField),
								grpCheckboxList = el.query('.grpCheckbox');

							for (var i = 0; i < grpCheckboxList.length; i++) {
								var grpCheckbox = grpCheckboxList[i],
									currentGroupId = grpCheckbox.parentElement.parentElement.id;

								if (currentGroupId == groupId) {
									var groupHeader = document.getElementById(currentGroupId),
										groupHeaderCheckbox = groupHeader.getElementsByClassName('grpCheckbox');

									groupHeaderCheckbox[0].setAttribute('class', 'grpCheckbox');
									break;
								}
							}
						}
					}
				}
			},
			features: [{
				id: 'group',
				ftype: 'grouping',
				collapsible: false,
				groupHeaderTpl: '<span class="grpCheckbox"></span>Type: {name}'
			}],
			listeners: {
				sortchange: function(ct) {
					var view = ct.view,
						feature = view.getFeature('group'),
						el = view.getEl();

					if (el) {
						var groupField = feature.getGroupField(),
							grpCheckboxList = el.query('.grpCheckbox'),
							selModel = view.getSelectionModel(),
							selectedRec = selModel.getSelection(),
							groupIdList = [];

						for (var groupname in feature.groupCache) {
							var group = feature.groupCache[groupname];
							var isChecked = true;

							for (var i = 0; i < group.children.length; i++) {
								var rec = group.children[i];
								if (!selModel.isSelected(rec)) {
									isChecked = false;
									break;
								}
							}

							if (isChecked) {
								var groupId = view.id + '-' + 'hd' + '-' + groupname;
								groupIdList.push(groupId);
							}
						}

						for (var i = 0; i < groupIdList.length; i++) {
							var groupId = groupIdList[i];
							for (var j = 0; j < grpCheckboxList.length; j++) {
								var grpCheckbox = grpCheckboxList[j],
									currentGroupId = grpCheckbox.parentElement.parentElement.id;

								if (currentGroupId == groupId) {
									var groupHeader = document.getElementById(currentGroupId),
										groupHeaderCheckbox = groupHeader.getElementsByClassName('grpCheckbox');

									groupHeaderCheckbox[0].setAttribute('class', 'grpCheckbox checked');
									break;
								}
							}
						}
					}
				},
				groupclick: function(view, node, group, e, eOpts) {
					var t = e.getTarget('.grpCheckbox'),
						feature = view.getFeature('group');

					if (t) {
						var groupField = feature.getGroupField(),
							isChecked = t.classList.contains('checked') ? true : false,
							selModel = view.getSelectionModel();

						if (isChecked) {
							t.setAttribute('class', 'grpCheckbox');
						} else {
							t.setAttribute('class', 'grpCheckbox checked');
						}

						for (var i = 0; i < view.store.data.items.length; i++) {
							var rec = view.store.data.items[i];
							if(rec.get(groupField) == group){
								if(isChecked) {
									selModel.deselect(i, true, true);
								} else {
									selModel.select(i, true, true);
								}
							}
						}
					}
					else {
						var groupHeader = document.getElementById(node.id),
							groupHeaderCheckbox = groupHeader.getElementsByClassName('grpCheckbox'),
							isChecked = groupHeaderCheckbox[0].classList.contains('checked') ? true : false;

						if (feature.isExpanded(group)) {
							feature.collapse(group);
						} else {
							feature.expand(group);
						}

						if (isChecked) {
							groupHeader = document.getElementById(node.id);
							groupHeaderCheckbox = groupHeader.getElementsByClassName('grpCheckbox');
							groupHeaderCheckbox[0].setAttribute('class', 'grpCheckbox checked');
						}
					}
				}
			}
		}],

		buttons: [{
			name :'importModule',
			cls : 'rs-med-btn rs-btn-dark',
		},{
			name : 'exportModule',
			cls : 'rs-med-btn rs-btn-dark'
		},{
			name : 'verify',
			cls : 'rs-med-btn rs-btn-dark'
		},{
			name :'close',
			cls : 'rs-med-btn rs-btn-light',
		}],
		asWindow: {
			title: '~~manifest~~',
			width: 800,
			height: 500,
			padding : 15,
			cls : 'rs-modal-popup',
			modal: true
		}
	}
});
glu.defView('RS.impex.UploadModule', {
	asWindow: {
		title: '~~uploadModuleWindowTitle~~',
		cls : 'rs-modal-popup',
		height: 500,
		width: 800,
		modal: true,
		padding : 15,
		closable: true
	},

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	
	items: [{
		html: '@{busyText}',
		margin: '0 0 10 0'
	}, {
        xtype: 'fieldcontainer',
        hidden: '@{!impexing}',
        items: [{
            itemId: 'pg',
            xtype: 'progressbar'
        }, {
            xtype: 'displayfield',
            hidden: true,
            value: '@{progress}',
            listeners: {
                change: function(field, newData) {
                    var progressBar = this.ownerCt.down('#pg');
                    progressBar.updateProgress(newData / 100, newData + '%');
                }
            }
        }]
    }, {
		itemId: 'fineUploaderTemplate',
		html: '@{fineUploaderTemplate}',
		height: '@{fineUploaderTemplateHeight}',
		hidden: '@{fineUploaderTemplateHidden}',
	}, {
		id: 'attachments_grid',
		hidden: true
	}],

	listeners: {
		afterrender: function() {
			this.fireEvent('afterRendered', this, this);
		},
		afterRendered: '@{viewRenderCompleted}',
		uploadComplete: function() {
			var fineuploader = this.down('#fineUploaderTemplate');
			fineuploader.hide();
		}
	},

	buttons: [{
        itemId: 'browse',
        cls : 'rs-med-btn rs-btn-dark',
        text: '~~browse~~',
        hidden: '@{!browseIsVisible}',
		id: 'upload_button',
		preventDefault: false,
    },{
        name : 'importModule',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'manifest',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'startWiki', 
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'getLastImpexResult',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name :'close',
        cls : 'rs-med-btn rs-btn-light'
	}]
})

/*
glu.defView('RS.impex.UploadModule', {
    xtype: 'panel',
    getConfig: function() {
        return {
            autoStart: true,
            url: '/resolve/service/impex/uploadFile',
            filters: [{
                title: "Zip files",
                extensions: "zip"
            }],
            browse_button: this.getBrowseButton(),
            drop_element: this.getDropArea(),
            container: this.getContainer(),
            chunk_size: null,
            runtimes: "html5,flash,silverlight,html4",
            flash_swf_url: '/resolve/js/plupload/Moxie.swf',
            unique_names: true,
            multipart: true,
            multipart_params: {},
            multi_selection: false,
            required_features: null,
            ownerCt: this
        }
    },
    getBrowseButton: function() {
        return this.down('#browse').btnEl.dom.id;
    },
    getDropArea: function() {
        return this.down('#dropzone').getEl().dom.id;
    },
    getContainer: function() {
        var btn = this.down('#browse');
        return btn.ownerCt.id;
    },
    uploaderListeners: {
        beforestart: function(uploader, data) {
            //console.log('beforestart')
        },
        // uploadready: function(uploader, data) {
        //     var me = uploader.uploaderConfig.ownerCt;
        //     var inputs = Ext.query('#' + me.getEl().id + ' input[type="file"]');
        //     var file = inputs[0];
        //     var runtime = mOxie.Runtime.getRuntime(file.id);
        //     me.fireEvent('runtimedecided', null, runtime);
        // },
        uploadstarted: function(uploader, data) {},
        uploadcomplete: function(uploader, data) {
            this.owner.fireEvent('uploadComplete', this.owner, uploader, data);
        },
        // uploaderror: function(uploader, data) {
        //     this.owner.fireEvent('uploadError', this.owner, uploader, data);
        // },
        filesadded: function(uploader, data) {
            this.owner.fireEvent('filesAdded', this.owner, uploader, data);
        },
        beforeupload: function(uploader, data) {
            //console.log('beforeupload')
        },
        // fileuploaded:function(uploader,data,response){this.owner.fireEvent('fileUploaded',this.owner,uploader,data,response);},
        uploadprogress: function(uploader, file, name, size, percent) {
            this.owner.percent = file.percent;
        },
        storeempty: function(uploader, data) {
            //console.log('storeempty')
        }
    },

    listeners: {
        uploadError: '@{uploadError}',
        filesAdded: '@{filesAdded}',
        uploadComplete: '@{uploadComplete}',
        runtimedecided: '@{runtimeDecided}',
        afterrender: function() {
            this.myUploader = Ext.create('RS.common.SinglePlupUpload', this);
            this.uploader = this.getConfig();
            this.myUploader.initialize(this);
        }
    },
  
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults : {
        margin : '4 0'
    },
    dockedItems: [{
        xtype: 'toolbar',
        cls : 'rs-dockedtoolbar',      
        items: [/ *{ 
            xtype: 'radiofield',
            name: 'type',
            hideLabel: true,
            boxLabel: '~~file~~',
            disabled: '@{!fileIsEnabled}',
            hidden: '@{!fileIsVisible}',
            value: '@{file}'
        }, {
            xtype: 'radiofield',
            name: 'type',
            hideLabel: true,
            boxLabel: '~~url~~',
            disabled: '@{!urlIsEnabled}',
            hidden: '@{!urlIsVisible}',
            value: '@{url}'
        }* /]
    }],
    items: [{
        html: '@{busyText}'
    }, {
        xtype: 'form',
        itemId: 'dropzone',
        style: 'border: 2px dashed #ccc;margin: 10px 0px 0px 0px',
        hidden: '@{!dropAreaIsVisible}',
        flex: 1,
        layout: {
            type: 'vbox',
            pack: 'center',
            align: 'center'
        },
        items: [{
            xtype: 'label',
            text: '~~dragHere~~',
            style: {
                'font-size': '30px',
                'color': '#CCCCCC'
            }
        }]
    }, {
        xtype: 'fieldcontainer',
        margin: '20px 0px 0px 0px',
        hidden: '@{!moduleURLIsVisible}',
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        items: [{
            xtype: 'textfield',
            labelWidth: 100,
            name: 'moduleURL',
            flex: 1
        }]
    }, {
        xtype: 'fieldcontainer',
        hidden: '@{!impexing}',
        items: [{
            itemId: 'pg',
            xtype: 'progressbar'
        }, {
            xtype: 'displayfield',
            hidden: true,
            value: '@{progress}',
            listeners: {
                change: function(field, newData) {
                    var progressBar = this.ownerCt.down('#pg');
                    progressBar.updateProgress(newData / 100, newData + '%');
                }
            }
        }]
    }],

    asWindow: {
        width: 600,
        height: 350,
        padding : 15,
        title: '~~uploadModuleWindowTitle~~',
        cls : 'rs-modal-popup',
        modal: true,
        closable: false
    },  
    buttons: [{
        itemId: 'browse',
        cls : 'rs-med-btn rs-btn-dark',
        text: '~~browse~~',
        hidden: '@{!browseIsVisible}',
        listeners: {
            hide: function() {
                this.up().el.query('.moxie-shim')[0].style.width = '0px';
                // this.up().el.query('.moxie-shim')[0].style.display = 'none'
            },
            show: function() {
                this.up().el.query('.moxie-shim')[0].style.width = '100%'
                    // this.up().el.query('.moxie-shim')[0].style.display = 'inline'
            }
        }
    }, {
        name : 'submit',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'importModule',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'manifest',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'startWiki', 
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'getLastImpexResult',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name :'close',
        cls : 'rs-med-btn rs-btn-light'
   }]
})
*/
/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.impex').locale = {
    ServerErr: 'Server Error',
    serverError: 'Server Error',
    //main
    windowTitle: 'Import / Export',
    displayName: 'Import / Export',
    ListDefErrMsg: 'Cannot get definitions from the server.',
    uzipFileName: 'File',
    sysOrganizationName: 'Organization',
    sysId: 'Sys ID',
    name: 'Name',
    active: 'Active',
    lastUpdated: 'Last Updated',
    createDefinition: 'New',
    deleteDefinitions: 'Delete',
    invalidDefinition : 'Invalid Definition',
    invalidDefinitionMsg : '<b>{0}</b> is not a valid definition. If this is a SDK Gateway Filter, make sure you define the Gateway correctly in blueprint properties.',

    uploadDefinition: 'Upload',
    reimportDefinitions: 'Import',
    exportDefinitions: 'Export',

    importTitle: 'Import',
    exportTitle: 'Export',

    ChooseOperation: 'Choose a operation.',

    Import: 'Import',
    Manifest: 'Manifest',
    validation: 'Validating...',

    uforwardWikiDocument: 'Forward To',

    sysCreatedBy: 'Created By',
    sysCreatedOn: 'Created On',
    sysUpdatedBy: 'Updated By',
    sysUpdatedOn: 'Updated On',

    componentType: 'Type',
    scanOn: 'Include',
    properties: 'Include Properties',
    actiontask: 'Include ActionTasks',
    cns: 'Include CNS',
    insertOnly: 'Insert Only',
    schema: 'Schema',
    data: 'Data',

    DeleteTitle: 'Delete Definitions',
    DeleteMsg: 'Are you sure you want to delete the selected record?',
    DeletesMsg: 'Are you sure you want to delete the selected records?({num} items selected)',
    DeleteSuc: 'The selected records have been deleted.',
    DeleteErr: 'Cannot delete the selected records.',
    yes: 'Yes',
    no: 'No',

    //manifest
    expandAll: 'Expand All',
    collapseAll: 'Collapse All',
    ManifestErr: 'Cannot get manifest from the server.',
    uoperation: 'Operation',
    ustatus: 'Status',
    ufullName: 'Name',
    udisplayType: 'Type',

    //definition
    SaveSuc: 'The record has been saved.',
    saveError: 'Cannot save the definition.[{0}]',
    LoadDefinitionErr: 'Cannot get defintion from server.',
    Components: 'Components',
    addComponents: 'Add',
    remove: 'Remove',
    save: 'Save',
    close: 'Close',

    save: 'Save',
    cancel: 'Cancel',

    uploadModuleWindowTitle: 'Upload Module',
    upload: 'Upload',
    impexDefinitionTitle: 'Import / Export Definition',
    back: 'Back',
    forwardTooltip: 'Redirect UI to specified wiki upon import completion.',
    uscriptName: 'Script Name',

    actionTaskTab: 'ActionTask',
    actionTaskModuleTab: 'ActionTask Module',
    wikiTab: 'Wiki',
    wikiNamespaceTab: 'Wiki Namespace',
    cnsTab: 'CNS',
    propertiesTab: 'Property',
    tagTab: 'Tag',
    wikiLookupTab: 'Wiki Lookup',
    schedulerTab: 'Scheduler',
    menuSetTab: 'Menu Set',
    menuDefinitionTab: 'Menu Definition',
    menuItemTab: 'Menu Item',
    processTab: 'Process',
    teamTab: 'Team',
    userTab: 'User',
    groupTab: 'Group',
    roleTab: 'Role',
    customTableTab: 'Custom Table',
    formTab: 'Form',
    customDBTab: 'Custom DB',
    businessRuleTab: 'Business Rule',
    wikiTemplateTab: 'Wiki Template',
    TCPFilterTab: 'TCP Filter',

    systemScriptTab: 'System Script',
    systemPropertyTab: 'System Property',
    catalogTab: 'Catalog',
    worksheetTab: 'Worksheet',
    forumTab: 'Forum',
    forumPosts: 'Posts',
    rssTab: 'RSS',

    uname: 'Name',
    unamespace: 'Namespace',
    usummary: 'Summary',
    ufullname: 'Fullname',
    udescription: 'Description',
    umodule: 'Module',
    uregex: 'Regex',
    uwiki: 'Wiki',
    uorder: 'Order',
    urunbook: 'Runbook',
    uexpression: 'Expression',
    uuserName: 'Username',
    ufirstName: 'First Name',
    ulastName: 'Last Name',
    udisplayName: 'Display Name',
    uformName: 'Form Name',
    utableName: 'Table Name',
    uquery: 'Query',
    uvalue: 'Value',
    forum: 'Forum',
    rss: 'RSS',
    title: 'Name',
    systemProperty: 'System Property',


    type: 'Type',
    filter: 'Query..',
    searchComponentTitle: 'Add Components',
    choose: 'Add',
    options: 'Options',

    removeactionTask: 'Remove ActionTask',

    //component search
    definition: 'Custom DB Defintion',
    ListCompErr: 'Cannot get components from server',
    none: 'Select component ...',
    actionTask: 'ActionTask',
    actionTaskModule: 'ActionTask Namespace',
    wiki: 'Wiki',
    runbook: 'Runbook',
    wikiNamespace: 'Wiki Namespace',
    cns: 'CNS',
    properties: 'Property',
    tag: 'Tag',
    wikiLookup: 'Wiki Lookup',
    scheduler: 'Scheduler',
    metricThreshold: 'Metric Threshold',
    mtNamespace: 'Metric Threshold Namespace',
    menuSet: 'Menu Set',
    menuDefinition: 'Menu Definition',
    menuItem: 'Menu Item',
    process: 'Process',
    team: 'Team',
    user: 'User',
    group: 'Group',
    role: 'Role',
    customTable: 'Custom Table',
    customForm: 'Custom Form',
    form: 'Form',
    customDB: 'Custom DB',
    businessRule: 'Business Rule',
    systemScript: 'System Script',
    selectionTab: 'Selections',
    optionTab: 'Options',
    catalog: 'Catalog',
    worksheet: 'Worksheet',
    wikiTemplate: 'Wiki Template',
    u_display_name: 'Name',
    u_description: 'Description',
    umoduleName: 'Module Name',
    menuDefTitle: 'Title',
    menuItemTitle: 'Title',
    //options
    atProperties: 'Include Properties',
    atPost: 'Include Posts',
    excludeRefAssessor: 'Exclude Referenced Assessors',
    excludeRefParser: 'Exclude Referenced Parsers',
    excludeRefPreprocessor: 'Exclude Referenced Preprocessors',

    atNsProperties: 'Include Properties',
    atNsPosts: 'Include Posts',
    excludeNsRefAssessor: 'Exclude Referenced Assessors',
    excludeNsRefParser: 'Exclude Referenced Parsers',
    excludeNsRefPreprocessor: 'Exclude Referenced Preprocessors',

    wikiOverride: 'Overwrite on Import',
    wikiRefATs: 'Include ActionTasks',
    wikiSubRB: 'Include Sub Runbooks and Sub Decision Tree',
    wikiSubDT: 'Include Sub Decision Tree',
    wikiForms: 'Include Custom Forms',
    wikiTags: 'Include Tags',
    wikiPost: 'Include Posts',
    wikiDeleteIfSameName: 'Delete if Same Document Name',
    wikiDeleteIfSameSysId: 'Delete if Same Document ID',
    wikiCatalogs: 'Include Catalogs',

    wikiNsOverride: 'Overwrite on Import',
    wikiNsRefATs: 'Include ActionTasks',
    wikiNsSubRB: 'Include Sub Runbooks and Sub Decision Tree',
    wikiNsSubDT: 'Include Sub Decision Tree',
    wikiNsForms: 'Include Custom Forms',
    wikiNsTags: 'Include Tags',
    wikiNsPost: 'Include Posts',
    wikiNsTables: 'Include Custom Tables',
    startDate: 'From',
    endDate: 'To',

    includeAllVersions : 'Include All Versions',
    uruleName: 'Rule Name',
    uruleModule: 'Rule Module',

    mSetMenuSection: 'Include Menu Section',
    mSetMenuItem: 'Include Menu Item',

    processUsers: 'Include User Members',
    processTeams: 'Include Team',
    processForums: 'Include Forum',
    processRSS: 'Include RSS',
    processWiki: 'Include Wiki / Runbook',
    processATs: 'Include ActionTask',
    processPosts: 'Include Posts',

    teamUsers: 'Include User Members',
    teamTeams: 'Include Sub Teams',
    teamPosts: 'Include Posts',

    forumUsers: 'Include User Members',
    forumPost: 'Include Posts',

    rssUsers: 'Include User Members',

    userInsert: 'Insert Only Definition',
    userRoleRel: 'Include Role Relations',
    userGroupRel: 'Include Group Relations',

    groupUsers: 'Include Users Relations',
    groupRoleRel: 'Include Role Relation',

    roleUsers: 'Include User Relations',
    roleGroups: 'Include Group Relations',

    tableForms: 'Include Custom Forms',
    tableData: 'Include Data',

    formRBs: 'Include Runbooks',
    formSS: 'Include System Scripts',
    formATs: 'Include ActionTasks',
    formTables: 'Include Custom Tables',

    catalogWikis: 'Include Wikis',
    catalogTags: 'Include Tags',
    catalogRefCatalogs: 'Export Reference Catalogs',

    wsPosts: 'Include Posts',

    templateForm: 'Include Form ',
    templateWiki: 'Include Template Wiki',

    //uploader
    file: 'File',
    url: 'URL',
    browse: 'Browse',
    fileIntro: 'Click "Browse" to choose a file for uploading or drag a file to the area below to upload it.',
    noDragFileIntro: 'Click "Browse" to choose a file for uploading.',
    urlIntro: 'Input the url of the file.',
    importModule: 'Import',
    exportModule: 'Export',
    manifest: 'Manifest',
    urlLink: 'URL',
    submit: 'Submit',
    dragHere: 'DROP YOUR FILE HERE',
    invalidURL: 'The the url is not valid',

    uploadInProgress: 'Uploading..',
    uploadComplete: 'File{zipName} is uploaded.',
    runbookTab: 'Runbook',
    componentTab: 'Component',
    ucomment: 'Comment',
    uoperation: 'Operation',
    invalidName: 'Name is required and can should only contain alphanumerics,space,\'_\', continuous space is not allowed',
    invalidWikiChars: 'Special character\'s are not allowed in wiki document name except \'-\', \'_\' and \'.\'',
    invalidWikiFormat: 'Invalid wiki document name format. Use: Namespace.Document Name (e.g., Network.Health Check)',
    moduleURL: 'URL',
    fileUploadBusy: 'Uploading module...',
    urlUploadBusy: 'Downloading module...',
    manifestBusy: 'Analyzing file "{zipName}"...',
    waitingResponse: 'Sending request...',
    importOther: 'Wait for other IMPEX operation on module:{name} to finish...',
    exportOther: 'Wait for other IMPEX operation on module:{name} to finish...',
    validateOther: 'Gathering info for module:{name}...',
    validateMine: 'Gathering info for module:{name}...',
    importMine: 'Importing module:{name}...',
    exportMine: 'Exporting module:{name}...',
    postimportOther: 'Performing post IMPEX operation of another module:{name}...',
    postimportMine: 'Performing post import operation of module:{name}...',
    postexportOther: 'Performing post IMPEX operation of another module:{name}...',
    postexportMine: 'Performing post export operation of module:{name}...',

    urlUploadErr: 'Upload Error',
    UploadError: 'Upload Error',
    polling: 'The operation cannot be completed.[{msg}]',
    beforepolling: 'The operation cannot be completed. [{msg}]',
    IMPORTError: 'Import Error',
    importCompleted: 'Import Completed',
    exportCompleted: 'Export Completed',
    startWiki: 'Start Wiki',
    lastImportResult: 'Getting last import result...',
    importResult: 'Getting last import result...',
    exportResult: 'Getting last export result...',
    getLastImpexResult: 'Result',
    download: 'Download',
    importSuc: 'Import Success',
    importError: 'Import Error',
    exportSuc: 'Export Success',
    exportError: 'Export Error',
    ok: 'OK',

    verify: 'Verify',
    verifyInProgress: 'Verifying...',

    EXPORTPolling: 'Exporting...',
    EXPORTCompleted: 'Export completed.',
    EXPORTError: 'Cannot complete the export.',
    ManifestPolling: 'Preparing the manifest.',
    PrepareManifestErr: 'Cannot get the manifest.',

    component: 'Component',
    componentOptions: 'Set Component Options',
    update: 'Set Options',
    typeText: 'Type',

    refTableDetected: 'Reference Table Detected',
    exportWithRefTable: 'Please make sure all the reference tables are exported.<br><center>Proceed?</center>',

    customTableDetected: 'CustomTable Detected',
    exportWithCustomTable: 'This module contains Custom Table and hibernate will re-initialize after import. Any current execution will be disturbed. Proceed?',
    removeComponents: 'Remove',
    automation: 'Automation',
    filters: 'Gateway Filters',
    menus: 'Menus',
    socialAdmin: 'Social Admin',
    system: 'System',
    uncategorized: 'Uncategorized',
    AmqpFilter: 'AMQP Filter',
    DatabaseFilter: 'Database Filter',
    NetcoolFilter: 'Netcool Filter',
    XMPPFilter: 'XMPP Filter',
    ServiceNowFilter: 'Service Now Filter',
    SNMPFilter: 'SNMP Filter',
    HPOMFilter: 'HPOM Filter',
    SalesforceFilter: 'Salesforce Filter',
    EWSFilter: 'EWS Filter',
    ewsAddress: 'Include EWS Addresse(s)',
    ExchangeserverFilter: 'Exchange Server Filter',
    EmailFilter: 'Email Filter',
    emailAddress: 'Include Email Addresse(s)',
    TSRMFilter: 'TSRM Filter',
    RemedyxFilter: 'Remedyx Filter',
    HPSMFilter: 'HPSM Filter',
    HTTPFilter: 'HTTP Filter',
    TCPFilter: 'TCP Filter',
    CASpectrumFilter: 'CA Spectrum Filter',
	TIBCOBespokeFilter: 'TIBCO Bespoke Filter',
    exportDBPool: 'Export DB Connection Pool',
    exportRemedyxForm: 'Export Remedyx Form',
    SSHPool: 'SSH Connections',
    usubnetMask: 'Subnet Mask',
    selectCategory: 'Select Category...',
    automationBuilder: 'Automation Builder',
    resolutionRouting: 'Resolution Routing',
    rrModule: 'Mapping Module',
    ridMapping: 'Mapping',
    rrSchema: 'Schema',
    rid: 'RID',
    module: 'Module',
    "schema.name": 'Schema',
    includeSchema: 'Include Schema',
    includeMapping: 'Include Mapping',
    includeUIDisplay: 'Include UI Display',
    includeUIAutomation: 'Inclide UI Automation',
    includeGWAutomation: 'Include GW Automation',
    includeSirPlaybook: 'Include SIR Playbook',
    pollingErr: 'Error getting status from the server.',
    securityTemplate: 'Security Template',

    RedirectToPackageManagerTitle: 'Redirect to Package Manager',
    RedirectToPackageManagerMsg: 'This package is created with the new package manager. Click OK to redirect to package manager page.'
}

