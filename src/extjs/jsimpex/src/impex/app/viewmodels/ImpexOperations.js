glu.defModel('RS.impex.ImpexOperations', {
    mixins: ['Impexable'],
    title: '',
    moduleName: '',
    wait: false,
    wikiName: null,
    init: function() {
        clientVM.getCSRFToken_ForURI('/resolve/service/wiki/impex/download', function(token_pair) {
            this.csrftoken = token_pair[0] + '=' + token_pair[1];
        }.bind(this))

        this.set('busyText', this.localize('ChooseOperation'));
    },

    busyText: '',
    progress: '',
    progressData: null,
    csrftoken: '',
    progressDataChanged$: function() {
        if (!this.progressData)
            return;
        this.set('progress', this.progressData.data.percent);
        var isMine = this.operation == this.progressData.data.operation && this.progressData.data.module == this.moduleName;
        //this.set('busyText', this.progressData.message);
        if (this.progressData.data.percent == 0)
            this.set('busyText', this.localize(Ext.String.format('validate{0}', isMine ? 'Mine' : 'Other'), {
                name: this.progressData.data.module
            }));
        if (this.progressData.data.percent > 0)
            this.set('busyText', this.localize(Ext.String.format('{0}{1}', this.progressData.data.operation, isMine ? 'Mine' : 'Other'), {
                name: this.progressData.data.module
            }));
        if (this.progressData.data.percent == 100)
            this.set('busyText', this.localize(Ext.String.format('post{0}{1}', this.progressData.data.operation, isMine ? 'Mine' : 'Other'), {
                name: this.progressData.data.module
            }));
    },
    checkRefTable: function(nextAction) {
        this.set('wait', true);
        this.ajax({
            url: '/resolve/service/impex/checkRefCustomTable',
            params: {
                moduleName: this.moduleName
            },
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    //clientVM.displayError(this.localize('checkRefTableError', respData.message));
					clientVM.displayError(respData.message);
                    return;
                }

                if (respData.data) {
                    this.message({
                        title: this.localize('refTableDetected'),
                        msg: this.localize('exportWithRefTable'),
                        buttons: Ext.MessageBox.YESNO,
                        buttonText: {
                            yes: this.localize('exportModule'),
                            no: this.localize('cancel')
                        },
                        fn: function(btn) {
                            if (btn != 'yes')
                                return;
                            nextAction();
                        }
                    })
                } else
                    nextAction();
            },
            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('wait', false);
			}
        })
    },
    checkCustomTable: function(nextAction) {
        this.set('wait', true);
        this.ajax({
            url: '/resolve/service/impex/checkForCustomTables',
            params: {
                moduleName: this.moduleName
            },
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    //clientVM.displayError(this.localize('checkCustomTableError', respData.message));
					clientVM.displayError(respData.message);
                    return;
                }

                if (respData.data) {
                    this.message({
                        title: this.localize('customTableDetected'),
                        msg: this.localize('exportWithCustomTable'),
                        buttons: Ext.MessageBox.YESNO,
                        buttonText: {
                            yes: this.localize('importModule'),
                            no: this.localize('cancel')
                        },
                        fn: function(btn) {
                            if (btn != 'yes')
                                return
                            nextAction();
                        }
                    })
                } else
                    nextAction();
            },
            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('wait', false);
			}
        })
    },
    doImpex: function(ids, operation) {
        var me = this;
        if (operation == 'export')
            this.checkRefTable(function() {
                me.impexAction(ids, operation);
            });
        else
            this.checkCustomTable(function() {
                me.impexAction(ids, operation);
            });
    },
    impexAction: function(ids, operation) {
        var me = this;
        this.set('busyText', this.localize('waitingResponse'));
        this.startImpex(this.moduleName, ids, operation, function(when, data) {
            if (when == 'polling' || when == 'beforepolling') {
                var msg = me.localize(when, {
                    msg: data.message
                });
			} else {
                var msg = me.localize(when);
			}
            clientVM.displayError(msg);
            me.set('busyText', msg);
        }, function(data) {
            me.set('progressData', data);
        }, function(data) {
            me.parentVM.impexGridStore.load();
            me.set('wait', false);
            var status = '.'
            if(data.status) {
                status = ' With ' + data.status + '.'
                me.getLastImpexResult();
            }
            me.set('busyText', me.localize(me.operation + 'Completed') + status);
            clientVM.displaySuccess(me.localize(me.operation + 'Completed') + status);
            var wikiName = data.wikiName || '';
            if (!/^\s*$/.test(wikiName) && me.operation != 'export')
                me.set('wikiName', wikiName);
            else
                me.set('wikiName', 'none');
        }, function(when, data) {
            clientVM.displaySuccess(data.message)
            me.set('busyText', data.message);
        });
    },
    importModule: function() {
        this.doImpex(null, this.operation);
    },
    importModuleIsVisible$: function() {
        return this.wikiName == null && !this.wait && this.operation == 'import' && !this.impexing;
    },
    importModuleIsEnabled$: function() {
        return !this.wait;
    },
    download: function() {
        downloadFile(Ext.String.format('/resolve/service/wiki/impex/download?{0}&filename={1}.zip&moduleId=', this.csrftoken, this.moduleName));
    },
    downloadIsVisible$: function() {
        return this.getLastImpexResultIsVisible && this.operation == 'export';
    },
    exportModule: function() {
        this.doImpex(null, this.operation);
    },
    exportModuleIsVisible$: function() {
        return this.wikiName == null && !this.wait && this.operation == 'export';
    },
    exportModuleIsEnabled$: function() {
        return !this.wait && !this.impexing;
    },
    getLastImpexResult: function() {
        this.set('wait', true);
        this.set('busyText', this.localize(this.operation + 'Result'));
        this.ajax({
            url: '/resolve/service/impex/getImpexResult',
            params: {
                moduleName: this.moduleName
            },
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    clientVM.displayError(this.localize('GetImpexResultErr'), respData.message);
                    return;
                }
                var title = null;
                if (respData.message == 'ERROR')
                    title = this.localize(this.operation + 'Error');
                else
                    title = this.localize(this.operation + 'Suc');
                this.open({
                    mtype: 'RS.impex.ImpexResult',
                    title: title,
                    result: respData.data
                });
            },
            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('wait', false);
			}
        })
    },

    getLastImpexResultIsVisible$: function() {
        return this.wikiName != null;
    },

    startWiki: function() {
        clientVM.handleNavigation({
            modelName: 'RS.wiki.Main',
            params: {
                name: this.wikiName
            },
            target: '_blank'
        });
    },

    startWikiIsVisible$: function() {
        return this.wikiName && this.wikiName != 'none';
    },

    manifest: function() {
        this.open({
            mtype: 'RS.impex.Manifest',
            moduleName: this.moduleName,
            operation: this.operation
        });
    },

    manifestIsEnabled$: function() {
        return !this.wait && !this.impexing;
    },

    manifestIsVisible$: function() {
        return this.wikiName === null && !this.impexing;
    },

    close: function() {
        this.set('impexing', false);
        this.parentVM.impexGridStore.load();
        this.doClose();
    }
});