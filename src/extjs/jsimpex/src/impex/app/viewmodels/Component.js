glu.defModel('RS.impex.Component', {
	mixins: ['ComponentSearch'],
	name: '',
	namespace: '',
	component: '',
	type: '',
	typeText: '',
	dumper: null,
	options: null,
	init: function() {
		this.set('typeText', this.localize(this.type));
		this.set('component', this.namespace + (this.name ? '.' + this.name : ''));
		for (opt in this.options) {
			if (this[opt] != null || opt == 'startDate' || opt == 'endDate')
				this.set(opt, (opt == 'startDate' || opt == 'endDate') && this.options[opt] ? new Date(this.options[opt]) : this.options[opt]);
		}
	},
	setOptions: function() {
		this.dumper();
		this.close();
	},
	close: function() {
		this.doClose();
	}
});