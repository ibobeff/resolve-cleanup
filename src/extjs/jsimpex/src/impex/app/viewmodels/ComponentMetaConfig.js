glu.ns('RS.impex');
RS.impex.metaConfigMap = {
	'empty': {
		fields: []
	},
	'actionTask': {
		fields: ['uname', 'unamespace', 'usummary'],
		glide: {
			unique: ['unamespace', 'uname'],
			name: 'uname',
			namespace: 'unamespace'
		},
		options: {
			atProperties: true,
			excludeRefAssessor: false,
			excludeRefParser: false,
			excludeRefPreprocessor: false
		},
		showSysColumn : true
	},
	'actionTaskModule': {
		fields: ['unamespace'],
		glide: {
			unique: ['unamespace'],
			namespace: 'unamespace'
		},
		options: {
			atNsProperties: true,
			excludeNsRefAssessor: false,
			excludeNsRefParser: false,
			excludeNsRefPreprocessor: false
		}
	},
	'wiki': {
		fields: ['ufullname', 'usummary'],
		glide: {
			unique: ['ufullname'],
			namespace: 'ufullname'
		},
		options: {
			wikiSubRB: true,
			wikiTags: true,
			wikiForms: false,
			wikiRefATs: true,
			wikiOverride: true,
			wikiCatalogs: false
		},
		showSysColumn : true
	},
	'securityTemplate': {
		fields: ['ufullname', 'usummary'],
		glide: {
			unique: ['ufullname'],
			namespace: 'ufullname'
		},
		options: {
			wikiSubRB: true,
			wikiTags: true,
			wikiForms: true,
			wikiRefATs: true,
			wikiOverride: true,
			wikiCatalogs: false
		},
		showSysColumn : true
	},
	'runbook': {
		fields: ['ufullname', 'usummary'],
		glide: {
			unique: ['ufullname'],
			namespace: 'ufullname'
		},
		options: {
			wikiSubRB: true,
			wikiTags: true,
			wikiForms: false,
			wikiRefATs: true,
			wikiOverride: true,
			wikiCatalogs: false
		},
		showSysColumn : true
	},
	'wikiNamespace': {
		fields: ['unamespace'],
		glide: {
			unique: ['unamespace'],
			namespace: 'unamespace'
		},
		options: {
			startDate: null,
			endDate: null,
			wikiNsRefATs: true,
			wikiNsSubRB: true,
			wikiNsTags: true,
			wikiNsForms: false,
			wikiNsOverride: true,
			wikiDeleteIfSameSysId: false,
			wikiDeleteIfSameName: false
		}
	},	
	'tag': {
		fields: ['name'],
		glide: {
			unique: ['name'],
			namespace: 'name'
		}
	},
	'wikiLookup': {
		fields: ['uregex', 'uwiki', 'uorder'],
		glide: {
			unique: ['uregex', 'uwiki', 'uorder'],
			name: 'uregex',
			namespace: 'umodule'
		}
	},
	'scheduler': {
		fields: ['uname', 'urunbook', 'umodule', 'uexpression'],
		glide: {
			unique: ['umodule', 'uname'],
			name: 'uname',
			namespace: 'umodule'
		}
	},
	'metricThreshold' : {
		fields: ['uruleName', 'uruleModule'],
		glide: {
			unique: ['uruleName', 'uruleModule'],
			name: 'uruleName',
			namespace: 'uruleModule'
		},
		options: {
			includeAllVersions: true
		}
	},
	'mtNamespace': {
		fields: ['uruleModule'],
		glide: {
			unique: ['uruleModule'],
			namespace: 'uruleModule'
		}
	},
	'menuSet': {
		fields: ['name'],
		glide: {
			unique: ['name'],
			namespace: 'name'
		},
		options: {
			mSetMenuSection: false,
			mSetMenuItem: false,
			mSetMenuItemIsVisible$: function() {
				return this.mSetMenuSection;
			}
		}
	},
	'menuDefinition': {
		fields: [{
			name: 'title',
			header: 'menuDefTitle'
		}],
		glide: {
			unique: ['title'],
			namespace: 'title'
		},
		options: {
			mSetMenuItem: true
		}
	},
	'menuItem': {
		fields: [{
			header: 'title',
			name: 'title'
		}, {
			header: 'menuItemTitle',
			name: 'name'
		}],
		glide: {
			unique: ['title', 'name'],
			namespace: 'title',
			name: 'name'
		}
	},
	'process': {
		fields: ['u_display_name', 'u_description'],
		glide: {
			unique: ['u_display_name'],
			namespace: 'u_display_name'
		},
		options: {
			processWiki: true,
			processUsers: true,
			processTeams: true,
			processForums: true,
			processRSS: true,
			processATs: true
		}
	},
	'team': {
		fields: ['u_display_name'],
		glide: {
			unique: ['u_display_name'],
			namespace: 'u_display_name'
		},
		options: {
			teamUsers: true,
			teamTeams: false
		}
	},
	'user': {
		fields: ['uuserName', 'ufirstName', 'ulastName'],
		glide: {
			unique: ['uuserName'],
			namespace: 'uuserName'
		},
		options: {
			userRoleRel: false,
			userGroupRel: false,
			userInsert: false
		}
	},
	'group': {
		fields: ['uname', 'udescription'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		},
		options: {
			groupUsers: false,
			groupRoleRel: false
		}
	},
	'role': {
		fields: ['uname', 'udescription'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		},
		options: {
			roleUsers: false,
			roleGroups: false
		}
	},
	'customTable': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			// name: 'uname',
			namespace: 'uname'
		},
		options: {
			tableForms: true, // Custom Forms
			tableData: false
		}
	},
	'form': {
		fields: ['uformName', 'udisplayName'],
		glide: {
			unique: ['uformName'],
			namespace: 'uformName'
		},
		options: {
			formTables: true, // Form Custom tables
			formRBs: true,
			formSS: true, // Form System Scripts
			formATs: true // Form ActionTasks
		}
	},
	'customDB': {
		fields: ['utableName', 'uquery'],
		glide: {
			unique: ['utableName', 'uquery'],
			name: 'uquery',
			namespace: 'utableName'
		}
	},
	'businessRule': {
		fields: ['uname', 'udescription'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'systemScript': {
		fields: ['uname', 'udescription'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'systemProperty': {
		fields: ['uname', 'udescription'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'forum': {
		fields: ['u_display_name', 'u_description'],
		glide: {
			unique: ['u_display_name'],
			namespace: 'u_display_name'
		},
		options: {
			forumUsers: true
		}
	},
	'rss': {
		fields: ['u_display_name', 'u_description'],
		glide: {
			unique: ['u_display_name'],
			namespace: 'u_display_name'
		}
	},
	'catalog': {
		fields: ['name'],
		glide: {
			unique: ['name'],
			namespace: 'name'
		},
		options: {
			catalogWikis: true
		}
	},
	'properties': {
		fields: ['uname', 'umodule', 'uvalue'],
		glide: {
			unique: ['umodule', 'uname'],
			name: 'uname',
			namespace: 'umodule'
		}
	},
	'wikiTemplate': {
		fields: ['uname'],
		glide: {
			unique: ['unamespace', 'uname'],
			name: 'uname',
			namespace: 'unamespace'
		},
		options: {
			templateForm: true,
			templateWiki: true
		}
	},
	'TCPFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'AmqpFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'DatabaseFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		},
		options: {
			exportDBPool: true
		}
	},
	'NetcoolFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'XMPPFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'ServiceNowFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'SNMPFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'HPOMFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'TIBCOBespokeFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'SalesforceFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'EWSFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		},
		options: {
			ewsAddress: true
		}
	},
	'ExchangeserverFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'EmailFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		},
		options: {
			emailAddress: true
		}
	},
	'TSRMFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'RemedyxFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		},
		options: {
			exportRemedyxForm: true
		}
	},
	'HPSMFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'SSHPool': {
		fields: ['usubnetMask'],
		glide: {
			unique: ['usubnetMask'],
			namespace: 'usubnetMask'
		}
	},
	'HTTPFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'CASpectrumFilter': {
		fields: ['uname'],
		glide: {
			unique: ['uname'],
			namespace: 'uname'
		}
	},
	'automationBuilder': {
		fields: ['ufullname', 'usummary'],
		glide: {
			unique: ['ufullname'],
			namespace: 'ufullname'
		},
		options: {
			wikiSubRB: true,
			wikiTags: true,
			wikiForms: true,
			wikiRefATs: true,
			wikiOverride: true,
			wikiCatalogs: false
		},
		showSysColumn : true
	},
	'ridMapping': {
		fields: ['rid', 'module',  'schema.name'],
		glide: {
			unique: ['rid', 'schema.name'],
			name: 'rid',
			namespace: 'schema.name'
		},
		options: {
			includeUIDisplay: true,
			includeUIAutomation: true,
    		includeGWAutomation: true,
			includeSirPlaybook: true
		}
	},
	'rrModule': {
		fields: ['module'],
		glide: {
			unique: ['module'],
			namespace: 'module'
		},
		options: {
			includeUIDisplay: true,
			includeUIAutomation: true,
    		includeGWAutomation: true,
			includeSirPlaybook: true
		}
	},
	'rrSchema': {
		fields: ['name'],
		glide: {
			unique: ['name'],
			name: 'name'
		},
		options: {
			includeMapping: false
		}
	}
};
