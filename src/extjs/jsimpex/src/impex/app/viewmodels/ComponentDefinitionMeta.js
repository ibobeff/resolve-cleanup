function defineImpexDefinitionMetaVMIfNotExist() {
	if (RS.impex.definitionMetaDefined)
		return;
	RS.impex.definitionMetaDefined = true;
	Ext.Object.each(RS.impex.metaConfigMap, function(metaType, meta) {
		var columns = [];
		Ext.each(meta.fields, function(field) {
			columns.push({
				header: '~~' + (field.header || field) + '~~',
				dataIndex: field.name || field,
				renderer: RS.common.grid.plainRenderer()
			});
		});
		if (columns.length > 0) {
			if (columns.length > 1) {
				columns[0].flex = 1;
				columns[columns.length - 1].flex = 2;
			} else
				columns[0].flex = 1;
		}
		Ext.each(RS.common.grid.getSysColumns(), function(col) {
			if (col.dataIndex == 'sysUpdatedBy' || col.dataIndex == 'sysUpdatedOn')
				columns.push(Ext.applyIf({
					hidden: false
				}, col))
		});
		var optionsList = [];
		Ext.Object.each(meta.options, function(k, v) {
			if (k.indexOf('$') == -1)
				optionsList.push(k);
		});
		if ((!meta.glide || !meta.glide.unique) && metaType != 'empty')
			throw 'Glide unique key of type:' + metaType + ' not defined!';
		var gridVMConfig = {
			statId: 'impexMeta_' + Ext.id(),
			type: metaType,
			componentStore: {
				mtype: 'store',
				fields: meta.fields.concat(['tmpBrowserId', 'sys_id']).concat(RS.common.grid.getSysFields()), //I add a temporary UI id so it decouples the ui id and impex definition sys_id
				proxy: {
					type: 'memory',
					reader: {
						type: 'json',
						idProperty: 'tmpBrowserId'
					}
				}
			},
			attached: false,
			glide: meta.glide,
			init: function() {
				this.componentStore.load();
				this.componentStore.on('datachanged', function() {
					this.parentVM.fireEvent('cmpStoreChanged', this);
				}, this);
			},
			componentsSelections: [],
			componentColumns: columns,
			componentsIsVisible$: function() {
				return !this.showOption;
			},
			editOption: function(browserId) {
				var me = this;
				var r = this.componentStore.getAt(this.componentStore.find('tmpBrowserId', browserId));
				this.open({
					mtype: 'RS.impex.' + metaType + 'DefinitionMetaOption',
					impexDef: r.raw,
					dumper: function() {
						r.raw.options = this.getOptions();
					}
				});
			},
			addOrUpdateCmp: function(rawCmp, optionsRef) {
				var cmpInStore = this.returnCmpInStoreIfContains(rawCmp);
				if (!cmpInStore) {
					//we want each cmp has its own option, so this we need to clone this options obj
					if (optionsRef)
						rawCmp.options = Ext.apply({}, optionsRef);
					rawCmp.tmpBrowserId = Ext.data.IdGenerator.get('uuid').generate();
					this.componentStore.add(rawCmp);
					return;
				}
				cmpInStore.raw.options = optionsRef;
			},
			addOrUpdateCmps: function(rawCmps, optionsRef) {
				Ext.each(rawCmps, function(rawCmp) {
					this.addOrUpdateCmp(rawCmp, optionsRef);
				}, this)
			},
			removeSelectedCmps: function() {
				this.componentStore.remove(this.componentsSelections);
			},
			returnCmpInStoreIfContains: function(cmp) {
				var contains = null;
				this.componentStore.each(function(cmpInStore) {
					if (this.isEqual(cmpInStore, cmp)) {
						contains = cmpInStore;
						return;
					}
				}, this)
				return contains;
			},
			isEqual: function(cmp1, cmp2) {
				var equal = true;
				if (cmp1.store)
					cmp1 = cmp1.raw;
				if (cmp2.store)
					cmp2 = cmp2.raw;
				Ext.each(this.glide.unique, function(partOfUnique) {
					equal = equal && (cmp1[partOfUnique] == cmp2[partOfUnique]);
				}, this);
				return equal;
			},
			when_componentsSelections_changed: {
				on: ['componentsSelectionsChanged'],
				action: function() {
					if (this.parentVM.activeItem != this)
						return;
					this.parentVM.fireEvent('activeCmpSelChange', this);
				}
			},
			getCmpImpexDefinitions: function() {
				var defs = [];
				this.componentStore.each(function(cmp) {
					var def = {};
					def['namespace'] = cmp.get(this.glide.namespace);
					if (this.glide.name)
						def['name'] = cmp.get(this.glide.name)
					var uiInfo = {};
					Ext.each(RS.impex.metaConfigMap[this.type].fields, function(f) {
						uiInfo[f.name || f] = cmp.get(f.name || f);
					}, this);
					def['description'] = Ext.encode(uiInfo) + ' ';
					def['type'] = this.type;
					var options = cmp.raw.options;
					//Convert start/end Date to timestap before save.
					if(options.startDate)
						options.startDate = new Date(options.startDate).getTime();
					if(options.endDate){
						//Add extra 24hr to cover all docs updated on that date.
						options.endDate = new Date(options.endDate).getTime() + 86399999;
					}
					def['options'] = cmp.raw.options;
					def['sys_id'] = cmp.get('sys_id');
					defs.push(def);
				}, this)
				return defs;
			}
		};
		Ext.apply(gridVMConfig, meta.options);
		glu.defModel('RS.impex.' + metaType + 'DefinitionMeta', gridVMConfig);
		glu.defModel('RS.impex.' + metaType + 'DefinitionMetaOption', Ext.apply({
			impexDef: null,
			typeText$: function() {
				return this.localize(this.parentVM.type);
			},
			component$: function() {
				var cmp = this.impexDef[this.parentVM.glide.namespace];
				if (this.parentVM.glide.name)
					cmp += '.' + this.impexDef[this.parentVM.glide.name];
				return cmp;
			},
			init: function() {
				this.setOptions(this.impexDef.options);
			},
			update: function() {
				this.dumper();
				this.cancel();
			},
			setOptions: function(options) {
				Ext.each(optionsList, function(o) {
					if (o == 'startDate' || o == 'endDate'){
						var date = null; //Avoid new Date(null) return unknown date.
						if(options[o] != null)
							date = new Date(options[o]);
						this.set(o, date);
					}else
						this.set(o, options[o]);
				}, this);
			},
			getOptions: function() {
				var options = {};
				Ext.each(optionsList, function(o) {
					options[o] = this[o]
				}, this);
				return options;
			},
			cancel: function() {
				this.doClose();
			},
			startDateChanged: function(newDate) {
				this.set('startDate', newDate);
			},
			endDateChanged: function(newDate) {
				this.set('endDate', newDate);
			}
		}, meta.options));
	});
}