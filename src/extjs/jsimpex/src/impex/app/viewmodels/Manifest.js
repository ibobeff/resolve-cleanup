glu.defModel('RS.impex.Manifest', {
	componentStore: {
		mtype: 'store',
		fields: ['sys_id', 'uname', 'ufullName', 'uoperation', 'ustatus', 'udisplayType', 'ucompSysId'],
		groupField: 'udisplayType',
		sorters: [{
			property: 'ufullName',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/impex/getManifest',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	moduleName: '',
	operation: '',
	immediateParentVM: null,
	wait: false,
	busyText: '',
	progress: 0,
	ready: false,
	componentColumns: [{
		header: '~~ufullName~~',
		dataIndex: 'ufullName',
		filterable: true,
		sortable: false,
		flex: 1
	}, {
		header: '~~uoperation~~',
		dataIndex: 'uoperation',
		filterable: true,
		sortable: false
	}, {
		header: '~~ustatus~~',
		dataIndex: 'ustatus',
		filterable: false,
		sortable: false
	}, {
		header: '~~udisplayType~~',
		dataIndex: 'udisplayType',
		filterable: true,
		hideable: false,
		hidden: true,
		sortable: false
	}],

	componentsSelections: [],
	when_componentsSelections_changed: {
		on: ['componentsSelectionsChanged'],
		action: function() {
			this.componentStore.each(function(r) {
				for (var idx=0; idx < this.componentsSelections.length; idx++) {
					if (this.componentsSelections[idx].get('ucompSysId') == r.get('ucompSysId')) {
						delete this.excludeList[r.get('ucompSysId')];
						return;
					}
					this.excludeList[r.get('ucompSysId')] = true;
				}
			}, this);
		}
	},
	excludeList: {},
	init: function() {
		this.componentStore.on('beforeload', function(store, op) {
			op.params = op.params || {};
			Ext.applyIf(op.params, {
				moduleName: this.moduleName,
				operation: this.operation
			})
			this.set('wait', true);
		}, this);
		this.componentStore.on('load', function(store) {
			this.set('wait', false);
			var filteredSelections = [];
			this.componentStore.each(function(r) {
				if (this.excludeList[r.get('ucompSysId')])
					return;
				filteredSelections.push(r);
			}, this);
			this.set('componentsSelections', filteredSelections);
		}, this);
		this.prepare();
		this.set('immediateParentVM', this.parentVM);
		this.parentVM = this.parentVM.parentVM || this.parentVM;
	},

	importModule: function() {
		var excludedIds = [];
		for (ucompSysId in this.excludeList)
			if (this.excludeList[ucompSysId])
				excludedIds.push(ucompSysId);
		if (excludedIds.length == 0)
			excludedIds = null;
		this.immediateParentVM.doImpex(excludedIds, 'import');
		this.close();
	},

	importModuleIsVisible$: function() {
		return this.operation == 'import' && this.immediateParentVM && this.immediateParentVM.wikiName == null;
	},

	exportModule: function() {
		var excludedIds = [];
		for (ucompSysId in this.excludeList)
			if (this.excludeList[ucompSysId])
				excludedIds.push(ucompSysId);
		if (excludedIds.length == 0)
			excludedIds = null;
		this.immediateParentVM.doImpex(excludedIds, 'export');
		this.close();
	},

	exportModuleIsVisible$: function() {
		return this.operation == 'export' && this.immediateParentVM;
	},

	prepare: function() {
		this.set('wait', true);
		this.set('busyText', this.localize('ManifestPolling'));
		this.ajax({
			url: '/resolve/service/impex/prepareManifest',
			timeout: 300000,
			params: {
				operation: this.operation,
				moduleName: this.moduleName
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					this.set('wait', false);
					this.set('busyText', this.localize('PrepareManifestErr', {
						msg: respData.message
					}));
					clientVM.displayError(this.localize('PrepareManifestErr', {
						msg: respData.message
					}));
					return;
				}
				if (!respData.data) {
					this.manifestPolling();
					return;
				}
				this.set('wait', false);
				this.set('ready', true);
				this.componentStore.loadPage(1);

			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
				this.set('wait', false);
			}
		})
	},

	manifestPolling: function() {
		Ext.TaskManager.start({
			run: function() {
				this.ajax({
					url: '/resolve/service/impex/manifestStatus',
					timeout: 300000,
					params: {
						operation: this.operation,
						moduleName: this.moduleName
					},
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						if (!respData.success) {
							this.set('wait', false);
							clientVM.displayError('ManifestPollingErr', {
								msg: respData.message
							});
							return;
						}
						if (respData.data.module == this.moduleName && !respData.data.finished && this.wait) {
							setTimeout(function() {
								this.manifestPolling();
							}.bind(this), 500);
							return;
						}
						this.set('wait', false);
						this.set('ready', true);
						this.componentStore.loadPage(1);
					}
				})
			},
			interval: 1500,
			scope: this,
			repeat: 1
		})
	},

	verify: function() {
		this.set('wait', true);
		this.set('busyText', this.localize('verifyInProgress'));
		this.ajax({
			url: '/resolve/service/impex/validateModule',
			timeout: 300000,
			params: {
				moduleName: this.moduleName
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('VerificatioErr') + '[' + respData.message + ']');
					return;
				}
				this.componentStore.load();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},

	verifyIsEnabled$: function() {
		return !this.wait;
	},

	importModuleIsEnabled$: function() {
		return !this.wait;
	},

	verifyIsVisible$: function() {
		return this.operation == 'import'
	},

	close: function() {
		this.set('wait', false);
		this.doClose();
	}
});