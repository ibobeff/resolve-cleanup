glu.defModel('RS.impex.ComponentSearchV2', {
	mixins: ['ImpexMetaContainer'],
	optionIsVisible: false,
	dumper: null,
	init: function() {
		this.figureOutTypeAndCat();
		//Set default category when this is loaded for the 1st time.
		var defaultCat = this.typeCatStore.getAt(0);
		this.set('typeCategory', defaultCat ? defaultCat.get('value') : '');
	},
	choose: function() {
		if (this.dumper)
			this.dumper(this.activeItem.getSelectedRawCmps(), this.activeItem.getOptions());
	},
	chooseIsEnabled: false,
	initialSearch: true
});