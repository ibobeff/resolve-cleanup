glu.defModel('RS.impex.Definition', (function() {
	var tabs = {
		types: ['actionTask', 'actionTaskModule', 'wiki', 'wikiNamespace', 'tag', 'wikiLookup', 'scheduler',
			'menuSet', 'menuDefinition', 'menuItem', 'process', 'team', 'forum', 'rss', 'user', 'group', 'role', 'customTable', 'form',
			'customDB', 'businessRule', 'systemScript', 'systemProperty', 'catalog', 'properties', 'wikiTemplate'
		].sort(function(a, b) {
			return a.toLowerCase() > b.toLowerCase() ? 1 : -1
		}),

		actionTaskStoreConf: ['uname', 'unamespace', 'usummary'],
		actionTaskModuleStoreConf: [{
			header: 'umodule',
			name: 'unamespace'
		}],
		wikiStoreConf: ['ufullname', 'usummary'],
		wikiNamespaceStoreConf: ['unamespace'],		
		tagStoreConf: ['name'],
		wikiLookupStoreConf: ['uregex', 'uwiki', 'uorder'],
		schedulerStoreConf: ['uname', 'urunbook', 'umodule', 'uexpression'],
		menuSetStoreConf: ['name'],
		menuDefinitionStoreConf: [{
			header: 'menuDefTitle',
			name: 'title'
		}, 'name'],
		menuItemStoreConf: ['title', {
			header: 'menuItemTitle',
			name: 'name'
		}],
		processStoreConf: ['u_display_name', 'u_description'],
		teamStoreConf: ['u_display_name'],
		userStoreConf: ['uuserName', 'ufirstName', 'ulastName'],
		groupStoreConf: ['uname', 'udescription'],
		roleStoreConf: ['uname', 'udescription'],
		customTableStoreConf: ['uname'],
		formStoreConf: ['uformName', 'udisplayName'],
		customDBStoreConf: ['utableName', 'uquery'],
		businessRuleStoreConf: ['uname', 'udescription'],
		systemScriptStoreConf: ['uname', 'udescription'],
		systemPropertyStoreConf: ['uname', 'udescription'],
		forumStoreConf: ['u_display_name', 'u_description'],
		rssStoreConf: ['u_display_name', 'u_description'],
		catalogStoreConf: ['name'],
		worksheetStoreConf: ['name'],
		propertiesStoreConf: ['uname', 'umodule', 'uvalue'],
		wikiTemplateStoreConf: ['uname', 'unamespace'],
		changes: [],

		activeItem: ''
	};

	function process(type) {
		tabs[type + 'Columns'] = [];
		tabs[type + 'TabIsPressed$'] = function() {
			return this.activeItem == this.activeItemName(type);
		};
		tabs[type + 'Tab'] = function() {
			this.set('activeItem', this.activeItemName(type))
		};
		var countName = type + 'Count';
		tabs[countName] = 0;
		tabs[type + 'TabIsVisible$'] = new Function("return this." + countName + ">0");
		tabs[type + 'IsVisible$'] = new Function("return this." + countName + ">0&&this.activeItem==this.activeItemName('" + type + "')");
		tabs[type + 'Selections'] = [];
		var config = tabs[type + 'StoreConf'];
		Ext.each(config, function(f) {
			tabs[type + 'Columns'].push({
				header: '~~' + (f.header || f) + '~~',
				dataIndex: f.name || f,
				filterable: true,
				flex: f.flex || 1,
				hidden: (f.name || f) == 'name' && type == 'menuDefinition',
				renderer: RS.common.grid.plainRenderer()
			});
		});
		tabs[type + 'StoreConf'].push('id');
		tabs[type + 'StoreConf'].push('sysUpdatedBy');
		tabs[type + 'StoreConf'].push({
			name: 'sysUpdatedOn',
			type: 'date',
			dateFormat: 'time',
			format: 'time'
		});
		Ext.each(RS.common.grid.getSysColumns(), function(col) {
			if (col.dataIndex == 'sysUpdatedBy' || col.dataIndex == 'sysUpdatedOn')
				tabs[type + 'Columns'].push(Ext.applyIf({
					hidden: false
				}, col))
		});
	}

	var removeEnabled = [];
	tabs.typeMap = {};
	Ext.each(tabs.types, function(t) {
		process(t);
		tabs.typeMap[t.toUpperCase()] = t;
		removeEnabled.push('this.' + t + 'Selections.length>0');
	})

	tabs.removeIsEnabled$ = new Function('return ' + removeEnabled.join('||'));

	var base = {
		wait: false,
		uname: '',
		uforwardWikiDocument: '',
		uscriptName: '',
		udescription: '',
		id: '',
		sysCreatedBy: '',
		sysCreatedOn: '',
		sysUpdatedBy: '',
		sysUpdatedOn: '',
		sysOrganizationName: '',

		rendered: false,
		screenRendered: function() {
			this.set('rendered', true)
		},

		hideAndShow: function(type, visibility) {
			if (this.btnTable)
				this.btnTable[type + 'Tab'][visibility ? 'show' : 'hide']();
			return visibility;
		},

		defaultData: {
			uname: '',
			uforwardWikiDocument: '',
			udescription: '',
			id: '',
			sysCreatedBy: '',
			sysCreatedOn: '',
			sysUpdatedBy: '',
			sysUpdatedOn: '',
			sysOrganizationName: ''
		},

		unameIsValid$: function() {
			return this.uname && (/^([a-zA-Z0-9_\-]+[a-zA-Z0-9_\-\s\.])*[a-zA-Z0-9_\-\s]*[a-zA-Z0-9_\-]$/.test(this.uname) || /^[a-zA-Z0-9_\-]+$/.test(this.uname)) && !/.*\s{2,}.*/.test(this.uname) ? true : this.localize('invalidName');
		},

		uforwardWikiDocumentIsValid$: function() {
			if (!this.uforwardWikiDocument)
				return true;
			if (/[^a-zA-Z0-9_ \-.]+/.test(this.uforwardWikiDocument))
				return this.localize('invalidWikiChars');
			if (!/^[\w]+[.]{1}[\w]+$/.test(this.uforwardWikiDocument))
				return this.localize('invalidWikiFormat');
			return true;
		},
		userDateFormat$: function() {
			return clientVM.getUserDefaultDateFormat()
		},

		activate: function(screen, params) {
			window.document.title = this.localize('windowTitle')
			this.resetForm()
			this.set('id', params ? params.id : '');
			this.set('wait', false);
			if (!!this.id)
				this.loadDefinition()
			var flag = false;
			for (btnName in this.btnTable) {
				flag = this[btnName + 'IsVisibleOverflowWorkAround'];
				this.btnTable[btnName][flag ? 'show' : 'hide']();
			}
		},

		fields: ['uname', 'uforwardWikiDocument', 'uscriptName', 'udescription'].concat(RS.common.grid.getSysFields()),

		init: function() {
			if (this.mock)
				this.backend = RS.impex.createMockBackend(true)
			this.gridFunctionalityBuilder();
		},

		loadDefinition: function() {
			this.set('wait', true);
			this.ajax({
				url: '/resolve/service/impex/get',
				params: {
					moduleId: this.id
				},
				scope: this,
				success: function(response) {
					var respData = RS.common.parsePayload(response);
					if (!respData.success) {
						clientVM.displayError(this.localize('LoadDefinitionErr'), respData.message);
						return;
					}
					var data = respData.data;
					this.populateEverything(data);
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				},
				callback: function() {
					this.set('wait', false);
				}
			});
		},

		populateEverything: function(data) {
			this.loadData(data);
			this.dispatchRecords(data.impexDefinition);
		},

		dispatchRecords: function(records) {
			Ext.each(this.types, function(t) {
				var store = this[t + 'Store'];
				store.removeAll();
			}, this);
			var addedTypes = [];
			Ext.each(records, function(r) {
				var type = this.typeMap[r.type.toUpperCase()];
				var currentStore = this[type + 'Store'];
				if (!r.description)
					return;
				var description = r.description.substring(0, r.description.length - 1);

				description = Ext.JSON.decode(description);
				description.sys_id = r.sys_id; //for sysCreatedOn used in the backend ...don't remove
				var id = '';
				Ext.each(this.glidesMap[type].unique, function(f) {
					id += description[f];
				});
				description.id = id;
				description.sysUpdatedOn = r.sysUpdatedOn;
				description.sysUpdatedBy = r.sysUpdatedBy;
				currentStore.add(description);
				(currentStore.getAt(currentStore.find('id', description.id))).options = r.options;
				addedTypes.push(type);
			}, this);
			addedTypes.sort(function(a, b) {
				return a.toLowerCase() > b.toLowerCase() ? 1 : -1
			});
			if (addedTypes.length > 0)
				this.set('activeItem', this.activeItemName(addedTypes[0]));
			this.updateCountAndSortData();
		},

		addComponents: function() {
			this.open({
				mtype: 'RS.impex.ComponentSearch',
				dumper: (function(self) {
					return function(records, type) {
						var options = {};
						for (opt in this[this.type + 'Opt']) {
							var optName = opt;
							options[optName] = this[opt];
						}
						var sharedWith = this.shared[this.type];
						if (sharedWith) {
							for (type in sharedWith) {
								var opt = sharedWith[type];
								options[opt] = this[opt];
							}
						}
						self.addCompsToList(this.type, records, options);
					}
				})(this)
			});
		},

		remove: function() {
			Ext.each(this.types, function(type) {
				this[type + 'Store'].remove(this[type + 'Selections']);
			}, this)
			this.updateCountAndSortData();
		},

		glidesMap: {
			actionTask: {
				unique: ['unamespace', 'uname'],
				name: 'uname',
				namespace: 'unamespace'
			},
			actionTaskModule: {
				unique: ['unamespace'],
				namespace: 'unamespace'
			},
			wiki: {
				unique: ['ufullname'],
				namespace: 'ufullname'
			},
			wikiNamespace: {
				unique: ['unamespace'],
				namespace: 'unamespace'
			},			
			tag: {
				unique: ['name'],
				namespace: 'name'
			},
			wikiLookup: {
				unique: ['uregex', 'uwiki', 'uorder'],
				name: 'uregex',
				namespace: 'umodule'
			},
			scheduler: {
				unique: ['umodule', 'uname'],
				name: 'uname',
				namespace: 'umodule'
			},
			menuSet: {
				unique: ['name'],
				namespace: 'name'
			},
			menuDefinition: {
				unique: ['name'],
				namespace: 'name'
			},
			menuItem: {
				unique: ['title', 'name'],
				namespace: 'title',
				name: 'name'
			},
			process: {
				unique: ['u_display_name'],
				namespace: 'u_display_name'
			},
			team: {
				unique: ['u_display_name'],
				namespace: 'u_display_name'
			},
			user: {
				unique: ['uuserName'],
				namespace: 'uuserName'
			},
			group: {
				unique: ['uname'],
				namespace: 'uname'
			},
			role: {
				unique: ['uname'],
				namespace: 'uname'
			},
			customTable: {
				unique: ['uname'],
				// name: 'uname',
				namespace: 'uname'
			},
			form: {
				unique: ['uformName'],
				namespace: 'uformName'
			},
			customDB: {
				unique: ['utableName', 'uquery'],
				name: 'uquery',
				namespace: 'utableName'
			},
			businessRule: {
				unique: ['uname'],
				namespace: 'uname'
			},
			systemScript: {
				unique: ['uname'],
				namespace: 'uname'
			},
			systemProperty: {
				unique: ['uname'],
				namespace: 'uname'
			},
			forum: {
				unique: ['u_display_name'],
				namespace: 'u_display_name'
			},
			rss: {
				unique: ['u_display_name'],
				namespace: 'u_display_name'
			},
			catalog: {
				unique: ['name'],
				namespace: 'name'
			},
			// worksheet: {
			// 	unique: ['name'],
			// 	namespace: 'name'
			// },
			properties: {
				unique: ['umodule', 'uname'],
				name: 'uname',
				namespace: 'umodule'
			},
			wikiTemplate: {
				unique: ['unamespace', 'uname'],
				name: 'uname',
				namespace: 'unamespace'
			}
		},
		updateCountAndSortData: function() {
			Ext.each(this.types, function(type) {
				var store = this[type + 'Store'];
				var firstField = store.model.getFields()[0];
				store.sort(firstField.name, 'ASC');
				this.set(type + 'Count', store.getCount());
			}, this);
		},
		addCompsToList: function(type, records, options) {
			if (!this.glidesMap[type].unique) {
				alert('unique key for:' + type + 'not assigned');
				return;
			}
			var store = this[type + 'Store'];
			this.set('activeItem', this.activeItemName(type));
			Ext.each(records, function(r) {
				var targetToUpdate = null;
				store.each(function(rInStore) {
					var duplicate = true;
					Ext.each(this.glidesMap[type].unique, function(partOfUnique) {
						duplicate = duplicate && r.get(partOfUnique) == rInStore.get(partOfUnique);
					}, this);
					if (duplicate)
						targetToUpdate = rInStore;
				}, this);
				if (!targetToUpdate) {
					r.options = options;
					store.add(r);
				} else {
					targetToUpdate.options = options;
				}
			}, this)
			this.updateCountAndSortData();
		},

		addComponentsIsEnabled$: function() {
			return !this.wait;
		},

		save: function(exitAfterSave) {
			if (this.task) this.task.cancel()
			this.task = new Ext.util.DelayedTask(this.saveDefinition, this, [exitAfterSave])
			this.task.delay(500)
		},
		saveIsEnabled$: function() {
			return !this.wait && this.isValid;
		},
		saveAndExit: function() {
			if (!this.saveIsEnabled)
				return;
			if (this.task) this.task.cancel()
			this.save(true)
		},

		toData: function() {
			var data = this.asObject();
			var glides = [];
			Ext.each(this.types, function(t) {
				var store = this[t + 'Store'];
				var name = this.glidesMap[t].name;
				var namespace = this.glidesMap[t].namespace;

				store.each(function(r) {
					var d = {};
					Ext.each(store.model.getFields(), function(f) {
						d[f.name] = r.get(f.name);
					});
					var def = {
						sys_id: r.get('sys_id'),
						type: t,
						name: r.get(name),
						namespace: r.get(namespace),
						description: Ext.JSON.encode(d) + ' ',
						options: {}
					};
					for (attr in r.options) {
						def.options[attr] = r.options[attr];
					}
					glides.push(def);
				});
			}, this);
			data.impexDefinition = glides;
			return data;
		},
		saveDefinition: function(exitAfterSave) {
			this.set('wait', true);
			var data = this.toData();
			data.uname = Ext.String.trim(data.uname);
			this.ajax({
				url: '/resolve/service/impex/save',
				jsonData: data,
				scope: this,
				success: function(resp) {
					var respData = RS.common.parsePayload(resp);
					if (!respData.success) {
						clientVM.displayError(this.localize('SaveErr') + '[' + respData.message + ']');
						return;
					} else
						clientVM.displaySuccess(this.localize('SaveSuc'));
					var data = respData.data;
					this.populateEverything(data);
					this.set('id', data.id);
					if (exitAfterSave === true)
						this.back();
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				},
				callback: function() {
					this.set('wait', false);
				}
			})
		},
		saveDefinitionIsEnabled$: function() {
			return !this.wait;
		},
		back: function() {
			clientVM.handleNavigation({
				modelName: 'RS.impex.Main'
			});
		},

		refresh: function() {
			if (!this.id)
				return;
			this.loadDefinition();
		},

		resetForm: function() {
			this.loadData(this.defaultData);
			this.set('activeItem', '');
			Ext.each(this.types, function(t) {
				var store = this[t + 'Store'];
				store.loadData([], false);
				this.set(t + 'Count', 0);
			}, this)
		},

		gridFunctionalityBuilder: function() {
			Ext.each(this.types, function(type) {
				this.buildGridFunctionality(type);
			}, this);
		},

		buildGridFunctionality: function(type) {
			var config = this[type + 'StoreConf'];
			this[type + 'Store'] = Ext.create('Ext.data.Store', {
				fields: config.concat(['sys_id', 'id']),
				proxy: {
					type: 'memory'
				}
			});
		},

		activeItemName: function(type) {
			return type + 'Card';
		},

		editComponentOpt: function(id) {
			var type = this.activeItem.substring(0, this.activeItem.length - 4)
			var store = this[type + 'Store'];
			var idx = store.find('id', id);
			var r = store.getAt(idx);
			this.open(Ext.applyIf({
				mtype: 'RS.impex.Component',
				type: type,
				name: this.glidesMap[type].name ? r.get(this.glidesMap[type].name) : '',
				namespace: r.get(this.glidesMap[type].namespace),
				options: r.options,
				dumper: function() {
					for (key in r.options) {
						r.options[key] = this[key];
					}
				}
			}, r.raw))
		}
	};

	Ext.apply(base, tabs);
	return base;
})());