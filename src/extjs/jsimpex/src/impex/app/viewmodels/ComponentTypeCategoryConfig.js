glu.ns('RS.impex');
RS.impex.componentTypeCategoryConfig = {
	automation: ['actionTask', 'actionTaskModule', 'runbook', 'automationBuilder', 'properties'],
	filters: ['TCPFilter', 'AmqpFilter', 'DatabaseFilter', 'NetcoolFilter', 'XMPPFilter', 'ServiceNowFilter',
		'SNMPFilter', 'HPOMFilter', 'SalesforceFilter', 'EWSFilter', 'ExchangeserverFilter', 'EmailFilter', 'TSRMFilter', 'RemedyxFilter', 'HPSMFilter', 'SSHPool', 'HTTPFilter', 'CASpectrumFilter', 'TIBCOBespokeFilter'],
	actionTask: ['actionTask', 'actionTaskModule','properties'],
	wiki: ['wiki', 'wikiNamespace', 'wikiTemplate', 'catalog', 'wikiLookup', 'securityTemplate'],
	menus: ['menuSet', 'menuDefinition', 'menuItem'],
	socialAdmin: ['process', 'team', 'forum', 'rss'],
	system: ['systemProperty', 'systemScript', 'tag', 'businessRule', 'customDB', 'scheduler', 'metricThreshold', 'mtNamespace'],
	form: ['form', 'customTable', 'wikiTemplate'],
	user: ['user', 'role', 'group'],
	resolutionRouting: ['ridMapping', 'rrModule', 'rrSchema']
}