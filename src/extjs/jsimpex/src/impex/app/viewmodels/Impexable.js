glu.defModel('RS.impex.Impexable', {
	impexing: false,
	startImport: function(module, ids, errorHandler, progressHandler, successHandler, warningHandler) {
		this.startImpex(module, ids, 'import', errorHandler, progressHandler, successHandler, warningHandler);
	},
	startExport: function(module, ids, errorHandler, progressHandler, successHandler, warningHandler) {
		this.startImpex(module, ids, 'export', errorHandler, progressHandler, successHandler, warningHandler);
	},
	startImpex: function(module, ids, operation, errorHandler, progressHandler, successHandler, warningHandler) {
		if (!ids)
			ids = null;
		this.set('impexing', true);
		this.ajax({
			url: Ext.String.format('/resolve/service/impex/{0}', this.operation),
			params: {
				moduleName: module,
				excludeIds: ids
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					this.set('impexing', false);
					errorHandler('beforepolling', respData);
					return;
				}
				if (!respData.data) {
					warningHandler('beforepolling', respData);
					return;
				}
				this.impexPolling(module, operation, errorHandler, progressHandler, successHandler);
			},

			failure: function(resp) {
				this.set('impexing', false);
				errorHandler('serverError');
			}
		});
	},

	impexPolling: function(module, operation, errorHandler, progressHandler, successHandler) {
		Ext.TaskManager.start({
			run: function() {
				this.ajax({
					url: '/resolve/service/impex/impexStatus',
					timeout: 300000,
					params: {
						operation: operation
					},
					scope: this,
					success: function(resp) {
						var respData = RS.common.parsePayload(resp);
						if (!respData.success && respData.errorCode !==  "IMPEX-105") {
							this.set('impexing', false);
							errorHandler('polling', respData);
							return;
						}
						var data = respData.data;
						if (!(data.module == module && data.operation == operation && data.finished) && this.impexing) {
							setTimeout(function() {
                                this.impexPolling(module, operation, errorHandler, progressHandler, successHandler);
                            }.bind(this), 500);
							progressHandler(respData);
							return;
						}
						this.set('impexing', false);
						successHandler(data);
					},
					failure: function(resp) {
						this.set('impexing', false);
						errorHandler('serverError');
					}
				})
			},
			interval: 1500,
			scope: this,
			repeat: 1
		})
	}
})