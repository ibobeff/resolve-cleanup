glu.defModel('RS.impex.Main', {

    activate: function() {
        clientVM.setWindowTitle(this.localize('displayName'))
        this.set('impexGridStoreSelections', []);
        this.impexGridStore.load()
    },

    displayName: '',

    mock: false,

    wait: false,

    impexGridStore: null,

    columns: [],

    init: function() {
        if (this.mock)
            this.backend = RS.impex.createMockBackend(true)
        this.set('impexGridStore', Ext.create('Ext.data.Store', {
            fields: ['uname', 'ulocation', 'uzipFileName'].concat(RS.common.grid.getSysFields()),
            remoteSort: true,
            sorters: [{
                property: 'sysUpdatedOn',
                direction: 'DESC'
            }],
            proxy: {
                type: 'ajax',
                url: '/resolve/service/impex/list',
                reader: {
                    type: 'json',
                    root: 'records'
                },
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
            }
        }));
        this.impexGridStore.parentVM = this;
        this.set('displayName', this.localize('displayName'));
        var sysCols = RS.common.grid.getSysColumns();
        Ext.each(sysCols, function(col) {
            if (col.dataIndex == 'sysUpdatedOn' || col.dataIndex == 'sysUpdatedBy') {
                col.hidden = false;
                col.initialShow = true;
            }
        });
        this.set('columns', [{
            header: '~~uname~~',
            dataIndex: 'uname',
            filterable: 'true',
            width: 350
        }, {
            header: '~~uzipFileName~~',
            dataIndex: 'uzipFileName',
            filterable: 'true',
            flex: 1,
            renderer: RS.common.grid.downloadLinkRenderer('ulocation')
        }].concat(sysCols))
        this.impexGridStore.on('beforeload', function() {
            this.set('wait', true);
        }, this)
        this.impexGridStore.on('load', function(rec, op, suc) {
            this.set('wait', false);
            if (!suc)
                clientVM.displayError(this.localize('ListDefErrMsg'));
        }, this)

        this.loadSDKCustomFilters();
    },

    impexGridStoreSelections: [],


    allSelected: false,

    selectAllAcrossPages: function(selectAll) {
        this.set('allSelected', selectAll)
    },

    selectionChanged: function() {
        this.selectAllAcrossPages(false)
    },

    createDefinition: function() {
        clientVM.handleNavigation({
            modelName: 'RS.impex.DefinitionV2',
            params: {
                id: null
            }
        })
    },

    createDefinitionIsEnabled$: function() {
        return !this.wait;
    },

    deleteDefinitions: function() {
        this.message({
            title: this.localize('DeleteTitle'),
            msg: this.localize(this.impexGridStoreSelections.length > 1 ? 'DeletesMsg' : 'DeleteMsg', {
                num: this.impexGridStoreSelections.length
            }),
            buttons: Ext.MessageBox.YESNO,
            buttonText: {
                yes: this.localize('deleteDefinitions'),
                no: this.localize('no')
            },
            scope: this,
            fn: function(btn) {
                if (btn == 'no')
                    return;
                this.sendReq('/resolve/service/impex/delete', 'DeleteErr', 'DeleteSuc')
            }
        })
    },

    sendReq: function(url, err, suc, params) {
        var ids = [];
        Ext.each(this.impexGridStoreSelections, function(i) {
            ids.push(i.get('id'));
        }, this);
        this.set('wait', true);
        if (!params)
            this.ajax({
                url: url,
                jsonData: ids,
				scope: this,
                success: function(resp) {
                    var respData = RS.common.parsePayload(resp);
                    if (!respData.success)
                        clientVM.displayError(this.localize(err) + '[' + respData.message + ']');
                    else
                        clientVM.displaySuccess(this.localize(suc));
                    this.impexGridStore.load();
                },
                failure: function(resp) {
                    clientVM.displayFailure(resp);
                },
				callback: function() {
					this.set('wait', false);
				}
            });
        else
            this.ajax({
                url: url,
                params: params,
				scope: this,
                success: function(resp) {
                    var respData = RS.common.parsePayload(resp);
                    if (!respData.success)
                        clientVM.displayError(this.localize(err) + '[' + respData.message + ']');
                    else
                        clientVM.displaySuccess(this.localize(suc));
                    this.impexGridStore.load();
                },
                failure: function(resp) {
                    clientVM.displayFailure(resp);
                },
				callback: function() {
					this.set('wait', false);
				}
            });
    },

    deleteDefinitionsIsEnabled$: function() {
        return !this.wait && this.impexGridStoreSelections.length > 0;
    },

    uploadDefinition: function() {
        this.open({
            mtype: 'RS.impex.UploadModule'
        });
    },

    uploadDefinitionIsEnabled$: function() {
        return !this.wait;
    },

    reimportDefinitions: function() {
        var moduleName = this.impexGridStoreSelections[0].get('uname');
        this.open({
            mtype: 'RS.impex.ImpexOperations',
            title: this.localize('importTitle'),
            moduleName: moduleName,
            operation: 'import'
        });

    },

    reimportDefinitionsIsEnabled$: function() {
        return this.impexGridStoreSelections.length == 1 && !this.wait && this.impexGridStoreSelections[0].get('uzipFileName');
    },

    exportDefinitions: function() {
        var moduleName = this.impexGridStoreSelections[0].get('uname');
        this.open({
            mtype: 'RS.impex.ImpexOperations',
            title: this.localize('exportTitle'),
            moduleName: moduleName,
            operation: 'export'
        });
    },

    exportDefinitionsIsEnabled$: function() {
        return this.impexGridStoreSelections.length == 1 && !this.wait;
    },

    editDefinition: function(id) {
        var record = this.impexGridStore.findRecord('id', id);
        if (record && record.raw.isNew) {
            clientVM.message({
                title: this.localize('RedirectToPackageManagerTitle'),
                msg: this.localize('RedirectToPackageManagerMsg'),
                buttons: Ext.MessageBox.OK,
                buttonText: {
                    ok: this.localize('ok'),
                    cancel : this.localize('cancel')
                },
                scope: this,
                fn: function(btn) {
                    if (btn == "ok") {
                        clientVM.handleNavigation({
                            modelName: 'RS.client.URLScreen',
                            params: {
                                location: '/resolve/sir/index.html#/imex'
                            }                           
                        })
                    }
                }
            })
        }
        else {
            clientVM.handleNavigation({
                modelName: 'RS.impex.DefinitionV2',
                params: {
                    id: id
                }
            });
        }        
    },

    loadSDKCustomFilters : function(){
        this.set('wait', true);
        this.ajax({
            url: '/resolve/service/gateway/getCustomGatewayNames',
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (respData.success){
                    var customFilterMap = respData.data;
                    for(var key in customFilterMap){
                        var filterName = customFilterMap[key] + "Filter";

                        RS.impex.componentTypeCategoryConfig.filters.push(filterName);
                        RS.impex.metaConfigMap[filterName] = {
                            fields: ['uname'],
                            glide: {
                                unique: ['uname'],
                                namespace: 'uname'
                            }
                        }
                        RS.impex.locale[filterName] = key;

                        //Define view config for each new custom filter
                        var options = [];
                        defineMetaViewConfig(filterName, options);
                        defineSearchMetaViewConfig(filterName, options);
                    }
                }
            },
            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('wait', false);
			}
        });
    }
});

glu.defModel('RS.impex.ImpexResult', {
    result: '',
    title: '',
    ok: function() {
        this.doClose();
    }
});