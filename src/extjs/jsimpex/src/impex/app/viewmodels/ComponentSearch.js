glu.defModel('RS.impex.ComponentSearch', (function() {
      var tabs = {
            types: ['actionTask', 'actionTaskModule', 'wiki', 'wikiNamespace', 'tag', 'wikiLookup', 'scheduler',
                  'menuSet', 'menuDefinition', 'menuItem', 'process', 'team', 'forum', 'rss', 'user', 'group', 'role', 'customTable', 'form',
                  'customDB', 'businessRule', 'systemScript', 'systemProperty', 'catalog', 'properties', 'wikiTemplate'
            ].sort(function(a, b) {
                  return a.toLowerCase() > b.toLowerCase() ? 1 : -1
            }),
            actionTaskStoreConf: ['uname', 'unamespace', 'usummary'],
            actionTaskModuleStoreConf: [{
                  name: 'unamespace',
                  header: 'umodule'
            }],
            wikiStoreConf: ['ufullname', 'usummary'],
            wikiNamespaceStoreConf: ['unamespace'],          
            tagStoreConf: ['name'],
            wikiLookupStoreConf: ['uregex', 'uwiki', 'uorder'],
            schedulerStoreConf: ['uname', 'urunbook', 'umodule', 'uexpression'],
            menuSetStoreConf: ['name'],
            menuDefinitionStoreConf: [{
                  name: 'title',
                  header: 'menuDefTitle'
            }, 'name'],
            menuItemStoreConf: ['title', {
                  header: 'menuItemTitle',
                  name: 'name'
            }],
            processStoreConf: ['u_display_name', 'u_description'],
            teamStoreConf: ['u_display_name'],
            userStoreConf: ['uuserName', 'ufirstName', 'ulastName'],
            groupStoreConf: ['uname', 'udescription'],
            roleStoreConf: ['uname', 'udescription'],
            customTableStoreConf: ['uname'],
            formStoreConf: ['uformName', 'udisplayName'],
            customDBStoreConf: ['utableName', 'uquery'],
            businessRuleStoreConf: ['uname', 'udescription'],
            systemScriptStoreConf: ['uname', 'udescription'],
            systemPropertyStoreConf: ['uname', 'udescription'],
            forumStoreConf: ['u_display_name', 'u_description'],
            rssStoreConf: ['u_display_name', 'u_description'],
            catalogStoreConf: ['name'],
            worksheetStoreConf: ['name'],
            propertiesStoreConf: ['uname', 'umodule', 'uvalue'],
            wikiTemplateStoreConf: ['uname'],
            changes: [],

            shared: {
                  menuDefinition: {
                        menuSet: 'mSetMenuItem'
                  }
            },
            //ActionTask opts
            actionTaskOpt: {
                  atProperties: true,
                  excludeRefAssessor: false,
                  excludeRefParser: false,
                  excludeRefPreprocessor: false
            },

            // ActionTask Namespace options
            actionTaskModuleOpt: {
                  atNsProperties: true,
                  excludeNsRefAssessor: false,
                  excludeNsRefParser: false,
                  excludeNsRefPreprocessor: false
            },

            // Wiki/Runbook options
            wikiOpt: {
                  wikiSubRB: true,
                  wikiTags: true,
                  // wikiSubDT: true,
                  wikiForms: false,
                  wikiRefATs: true,
                  wikiOverride: true,
                  wikiCatalogs: false
            },

            // Wiki/Runbook Namespace options
            wikiNamespaceOpt: {
                  startDate: null,
                  endDate: null,
                  wikiNsRefATs: true,
                  wikiNsSubRB: true,
                  wikiNsTags: true,
                  // wikiNsSubDT: true,
                  wikiNsForms: false,
                  wikiNsOverride: true,
                  wikiDeleteIfSameSysId: false,
                  wikiDeleteIfSameName: false
            },

            userDateFormat$: function() {
                  return clientVM.getUserDefaultDateFormat()
            },

            startDateChanged: function(newDate) {
                  this.set('startDate', newDate);
            },

            endDateChanged: function(newDate) {
                  this.set('endDate', newDate);
            },

            // Menus
            menuSetOpt: {
                  mSetMenuSection: false,
                  mSetMenuItem: false
            },

            // Social Components - Process
            processOpt: {
                  processWiki: true,
                  processUsers: true,
                  processTeams: true,
                  processForums: true,
                  processRSS: true,
                  processATs: true
            },

            // Team
            teamOpt: {
                  teamUsers: true,
                  teamTeams: false
            },

            // Forum
            forumOpt: {
                  forumUsers: true
            },

            // User
            userOpt: {
                  userRoleRel: false,
                  userGroupRel: false,
                  userInsert: false
            },

            // Role
            roleOpt: {
                  roleUsers: false,
                  roleGroups: false
            },

            // Group
            groupOpt: {
                  groupUsers: false,
                  groupRoleRel: false
            },

            // Form
            formOpt: {
                  formTables: false, // Form Custom tables
                  formRBs: true,
                  formSS: true, // Form System Scripts
                  formATs: true // Form ActionTasks
            },

            // Table
            customTableOpt: {
                  tableForms: true, // Custom Forms
                  tableData: false
            },

            // Catalog
            catalogOpt: {
                  catalogWikis: false,
                  catalogRefCatalogs: true
                  // catalogTags: false
            },

            // Worksheet
            worksheetOpt: {
                  //wsPosts: false
            },

            //Wiki Template
            wikiTemplateOpt: {
                  templateForm: true,
                  templateWiki: true
            }
      };

      function process(type) {
            tabs[type + 'Columns'] = [];
            var config = tabs[type + 'StoreConf'];
            Ext.each(config, function(f, idx) {
                  tabs[type + 'Columns'].push({
                        header: '~~' + (f.header || f) + '~~',
                        dataIndex: f.name || f,
                        filterable: idx == 0 || (type != 'menuItem'),
                        sortable: idx == 0 || (type != 'menuItem'),
                        flex: f.flex || 1,
                        hidden: (f.name || f) == 'name' && type == 'menuDefinition',
                        renderer: RS.common.grid.plainRenderer()
                  });
                  tabs[type + 'Store'] = null;
                  tabs[type + 'Selections'] = [];
                  tabs.changes.push(type + 'SelectionsChanged');
            });
            var currentOptObj = tabs[type + 'Opt'];
            var refType = null;
            for (opt in currentOptObj) {
                  tabs[opt] = currentOptObj[opt];
                  tabs[opt + 'IsVisible$'] = function() {
                        return this.type == type;
                  }
            }
      }

      Ext.each(tabs.types, function(t) {
            process(t);
      });
      //these formulas is just too special to process in a generic way
      tabs.mSetMenuItemIsVisible$ = function() {
            return (this.type == 'menuSet' && this.mSetMenuSection) || this.type == 'menuDefinition';
      };

      tabs.mSetMenuSectionChanged$ = function() {
            if (!this.mSetMenuSection && this.type != 'menuItem')
                  this.set('mSetMenuItem', false);
      };

      tabs.wikiOverrideChanged$ = function() {
            if (this.wikiOverride == false) {
                  this.set('wikiDeleteIfSameName', false);
                  this.set('wikiDeleteIfSameSysId', false);
            }
      };

      tabs.wikiDeleteIfSameNameIsVisible$ = function() {
            return (this.type == 'wiki' || this.type == 'wikiNamespace') &&
                  (this.wikiOverride && this.wikiOverrideIsVisible || this.wikiNsOverride && this.wikiNsOverrideIsVisible);
      };

      tabs.wikiDeleteIfSameSysIdIsVisible$ = function() {
            return (this.type == 'wiki' || this.type == 'wikiNamespace') &&
                  (this.wikiOverride && this.wikiOverrideIsVisible || this.wikiNsOverride && this.wikiNsOverrideIsVisible);
      };

      tabs.wikiDeleteIfSameNameChanged$ = function() {
            if (this.wikiDeleteIfSameName)
                  this.set('wikiOverride', true)
      };

      var base = {
            asWindowTitle: '',
            stateId: 'impexcomponentsearch',
            displayName: '',
            wait: false,
            typeStore: {
                  mtype: 'store',
                  fields: ['name', 'value'],
                  proxy: {
                        type: 'memory'
                  }
            },

            type: '',
            columns: [],
            activeItem: '',
            utableName: '',
            utableNameIsValid$: function() {
                  return this.utableName ? true : this.localize('invalidTableName');
            },
            uquery: '',

            when_custom_db_def_changed: {
                  on: ['utableNameChanged', 'uqueryChanged'],
                  action: function() {
                        if (this.utableNameIsValid)
                              this.set('chooseIsEnabled', true);
                  }
            },
            activate: function() {},
            init: function() {
                  this.set('type', 'none');
                  this.createStores();
                  this.set('activeItem', 'selectionTab');
                  this.set('utableName', '');
                  this.set('uquery', '');
            },

            createStores: function() {
                  this.typeStore.add({
                        name: this.localize('none'),
                        value: 'none'
                  });
                  Ext.each(this.types, function(t) {
                        this.typeStore.add({
                              name: this.localize(t),
                              value: t
                        });
                        this.createStore(t);
                  }, this);
            },

            createStore: function(t) {
                  var store = Ext.create('Ext.data.Store', {
                        fields: this[t + 'StoreConf'].concat('id'),
                        remoteSort: true,
                        sorters: [{
                              property: this[t + 'StoreConf'][0].name || this[t + 'StoreConf'][0],
                              direction: 'ASC'
                        }],
                        proxy: {
                              type: 'ajax',
                              url: '/resolve/service/impex/addcomponent/list',
                              extraParams: {
                                    compType: t
                              },
                              reader: {
                                    type: 'json',
                                    root: 'records'
                              },
                              listeners: {
                                    exception: function(e, resp, op) {
										clientVM.displayExceptionError(e, resp, op);
									}
                              }
                        }
                  });
                  store.on('beforeload', function(store, op) {
                        this.set('wait', true);
                  }, this);

                  store.on('load', function(recores, op, suc) {
                        this.set('wait', false);
						/*
                        if (!suc) {
                              clientVM.displayError(this.localize('ListCompErr'));
                        }
						*/
                  }, this);
                  this.set(t + 'Store', store);
            },

            when_type_changed: {
                  on: ['typeChanged'],
                  action: function(now, before) {
                        if (this.type == 'none') return;
                        this[this.type + 'Store'].load();
                        if (before == 'none') {
                              var idx = this.typeStore.find('value', 'none');
                              this.typeStore.removeAt(idx);
                              return;
                        }
                        this.set(before + 'Selections', []);
                  }
            },
            dumper: null,
            chooseIsEnabled: false,
            choose: function() {
                  if (!this.dumper) {
                        this.close();
                        return;
                  }
                  if (this.type == 'customDB') {
                        var store = this[this.type + 'Store'];
                        store.loadData([], false);
                        store.add({
                              utableName: this.utableName,
                              uquery: this.uquery
                        });
                        this.set(this.type + 'Selections', [store.getAt(0)]);
                  }
                  this.dumper(this[this.type + 'Selections'], this.type);
                  this.close();
            },

            close: function() {
                  this.doClose();
            },

            allSelected: false,

            selectAllAcrossPages: function(selectAll) {
                  this.set('allSelected', selectAll)
            },

            selectionChanged: function() {
                  this.selectAllAcrossPages(false)
            },

            when_selection_changed: {
                  on: tabs.changes,
                  action: function() {
                        if (this.type == 'customDB') {
                              if (!/^\s*$/.test(this.utableName))
                                    this.set('chooseIsEnabled', true);
                              else
                                    this.set('chooseIsEnabled', false);
                        } else
                              this.set('chooseIsEnabled', this[this.type + 'Selections'].length > 0);
                  }
            },

            selectionTab: function() {
                  this.set('activeItem', 'selectionTab');
            },

            selectionTabIsPressed$: function() {
                  return this.activeItem == 'selectionTab';
            },

            optionTab: function() {
                  this.set('activeItem', 'optionTab');
            },

            optionTabIsPressed$: function() {
                  return this.activeItem == 'optionTab';
            },

            optionIsVisible$: function() {
                  return this.activeItem == 'optionTab';
            }
      }

      Ext.apply(base, tabs);
      return base;
})());