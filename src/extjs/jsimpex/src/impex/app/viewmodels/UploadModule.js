glu.defModel('RS.impex.UploadModule', {
	mixins: ['FileUploadManager', 'Impexable', 'ImpexOperations'],
	api: {
		upload: '/resolve/service/impex/uploadFile',
		list: '/resolve/service/impex/list'
	},

	file: true,
	url: false,
	wait: false,

	allowedExtensions: ['zip'],
	fineUploaderTemplateHidden: false,
	checkForDuplicates: false,
	allowMultipleFiles: false,

	uploaderAdditionalClass: 'impex',

	init: function() {
        this.set('wait', false);
        this.operation = 'import';
		this.set('dragHereText', this.localize('dragHere'));
		this.set('busyText', this.localize(Ext.isIE9m ? 'noDragFileIntro' : 'fileIntro'));
	},

	fileOnUploadCallback: function(manager, filename) {
		this.set('wait', true);
	},

	fileOnCompleteCallback: function(manager, filename, response) {
		this.set('moduleName', response.data.NAME);
		this.set('busyText', this.localize('uploadComplete', {
			zipName: ' ' + filename
		}));

        this.set('wait', false);
        this.parentVM.impexGridStore.load();

		this.view.fireEvent('uploadComplete');
	},

	getLastImpexResultIsVisible$: function() {
        return this.wikiName != null;
    },

    getLastImpexResultIsEnabled$: function() {
        return !this.wait;
    },

	importModuleIsVisible$: function() {
		return this.moduleName && this.wikiName == null && !this.wait && this.operation == 'import' && !this.impexing;
    },

    browse: function() {},
    browseIsVisible$: function() {
        return this.file && !this.moduleName;
    },
    manifest: function() {
        this.open({
            mtype: 'RS.impex.Manifest',
            moduleName: this.moduleName,
            operation: 'import'
        });
    },
    manifestIsVisible$: function() {
        return !!this.moduleName && this.wikiName === null && !this.uploading && !this.importing;
    },

	view: null,
	viewRenderCompleted: function(view) {
		this.set('view', view);
	}
});

glu.defModel('RS.impex.FileUploadManagerFactory', function(cfg) {
	Ext.applyIf(cfg, RS.common.viewmodels.FileUploadManager);
	return cfg;
});

/*
glu.defModel('RS.impex.UploadModule', {
    mixins: ['Impexable', 'ImpexOperations'],
    file: true,
    url: false,
    wait: false,
    filePath: '',
    moduleURL: null,
    runtime: '',
    moduleURLIsValid$: function() {
        return this.moduleURL && Ext.form.field.VTypes.url(this.moduleURL) ? true : this.localize('invalidURL');
    },

    init: function() {
        this.set('wait', false);
        this.operation = 'import';
    },
    submit: function() {
        this.set('busyText', this.localize('urlUploadBusy'));
        this.set('wait', true);
        this.ajax({
            url: '/resolve/service/impex/urlUpload',
            params: {
                moduleURL: this.moduleURL
            },
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    clientVM.displayError(this.localize('urlUploadErr'), respData.message);
                    return;
                }
                this.set('moduleName', respData.data);
                this.set('uploading', true);
                this.uploadPolling();
            },
            failure: function(resp) {
                this.set('wait', false);
                clientVM.displayFailure(resp);
            }
        })
    },
    submitIsVisible$: function() {
        return this.url && !this.moduleName;
    },
    submitIsEnabled$: function() {
        return this.moduleURLIsValid === true;
    },
    uploading: false,
    uploadPolling: function() {
        Ext.TaskManager.start({
            run: function() {
                this.uploadPollingAction();
            },
            interval: 1000,
            scope: this,
            repeat: 1
        })
    },

    uploadPollingAction: function() {
        this.ajax({
            url: '/resolve/service/impex/urlUploadStatus',
			scope: this,
            success: function(resp) {
                var respData = RS.common.parsePayload(resp);
                if (!respData.success) {
                    this.set('uploading', false);
                    clientVM.displayError(this.localize('pollingErr'), respData.message);
                    return;
                }

                if (!respData.message && this.wait) {
                    this.uploadPolling();
                    return;
                }
                this.set('uploading', false);
                if (respData.data == '-1') {
                    clientVM.displayError(this.localize('pollingErr'));
                    this.set('busyText', this.localize('pollingErr'));
                    this.message({
                        title: this.localize('urlUploadErr'),
                        msg: respData.message,
                        buttons: Ext.MessageBox.OK,
                        buttonText: {
                            ok: this.localize('ok')
                        },
                        scope: this,
                        fn: function() {
                            this.close();
                        }
                    });
                    return;
                }
                this.set('moduleName', respData.message);
                this.set('busyText', this.localize('uploadComplete', {
                    zipName: ' ' + this.moduleName
                }));
            },
            failure: function(resp) {
                clientVM.displayFailure(resp);
            },
			callback: function() {
				this.set('wait', false);
			}
        });
    },

    getLastImpexResultIsVisible$: function() {
        return this.wikiName != null;
    },

    getLastImpexResultIsEnabled$: function() {
        return !this.wait;
    },

    browse: function() {},
    browseIsVisible$: function() {
        return this.file && !this.moduleName;
    },
    manifest: function() {
        this.open({
            mtype: 'RS.impex.Manifest',
            moduleName: this.moduleName,
            operation: 'import'
        });
    },

    manifestIsVisible$: function() {
        return !!this.moduleName && this.wikiName === null && !this.uploading && !this.importing;
    },
    close: function() {
        this.set('wait', false);
        this.doClose();
    },
    fileChanged$: function() {
        if (this.file)
            this.set('busyText', this.localize(Ext.isIE9m ? 'noDragFileIntro' : 'fileIntro'));
        else
            this.set('busyText', this.localize('urlIntro'));
    },

    fileIsEnabled$: function() {
        return !this.wait;
    },
    fileIsVisible$: function() {
        return !this.moduleName;
    },
    urlIsEnabled$: function() {
        return !this.wait;
    },
    urlIsVisible$: function() {
        return !this.moduleName;
    },
    browseIsEnabled$: function() {
        return !this.wait;
    },
    moduleURLIsVisible$: function() {
        return this.url && !this.importing && this.wikiName == null;
    },
    dropAreaIsVisible$: function() {
        return !this.wait && this.file && this.wikiName == null && !this.moduleName && !Ext.isIE9m;
    },
    uploadError: function(uploader, data) {
        clientVM.displayError(this.localize('UploadError') + '[' + data.file.msg + ']');
    },
    filesAdded: function(uploader, data) {
        this.set('busyText', this.localize('fileUploadBusy'));
        this.set('wait', true);
        this.set('uploading', true);
        var name = data[0].name;
        this.set('moduleName', name.substring(0, name.length - 4));
    },
    uploadComplete: function(uploader, data) {
        this.set('busyText', this.localize('uploadComplete', {
            zipName: this.file ? ' "' + this.moduleName + '.zip"' : ''
        }));
        this.set('wait', false);
        this.set('uploading', false);
        this.parentVM.impexGridStore.load();
    },
    runtimeDecided: function(runtime) {
        if (!runtime) {
            alert('no uploader runtime!!')
            return;
        }
        this.set('runtime', runtime.type);
    }
})
*/
