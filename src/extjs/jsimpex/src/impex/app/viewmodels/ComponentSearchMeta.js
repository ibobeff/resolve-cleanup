function defineImpexSearchMetaVMIfNotExist() {
	if (RS.impex.searchMetaDefined)
		return;
	RS.impex.searchMetaDefined = true;
	Ext.Object.each(RS.impex.metaConfigMap, function(metaType, meta) {
		if (metaType == 'customDB')
			return;
		var columns = [];
		Ext.each(meta.fields, function(field, idx) {
			var flexVal = (field == 'usummary' || field.name == 'usummary') ? 2 : 1;
			columns.push({
				header: '~~' + (field.header || field.name || field) + '~~',
				dataIndex: field.name || field,
				filterable: idx == 0 && field.filterable !== false,
				renderer: RS.common.grid.plainRenderer(),
				flex : flexVal
			});
		});
		if(meta.showSysColumn){
			Ext.each(RS.common.grid.getSysColumns(), function(col) {
				if (col.dataIndex == 'sysUpdatedOn'){
					columns.push(Ext.applyIf({
						hidden: false
					}, col))
					meta.fields.push({
						dateFormat : "time",
						format: "time",
						name : "sysUpdatedOn",
						type : "date"
					});
				}
			});
		}	
		var optionsList = [];
		Ext.Object.each(meta.options, function(k, v) {
			if (k.indexOf('$') == -1)
				optionsList.push(k);
		});
		var gridVMConfig = {
			statId: 'impexMeta_' + Ext.id(),
			type: metaType,
			componentStore: {
				mtype: 'store',
				fields: meta.fields,
				remoteSort: true,
				proxy: {
					type: 'ajax',
					url: '/resolve/service/impex/addcomponent/list',
					reader: {
						type: 'json',
						root: 'records'
					},
					listeners: {
						exception: function(e, resp, op) {
							clientVM.displayExceptionError(e, resp, op);
						}
					},
					extraParams: {
						compType: metaType
					}
				}
			},
			resetTime: function() {
				if (this['startDate'])
					this.set('startDate', null);
				if (this['endDate'])
					this.set('endDate', null);
			},
			init: function() {
				this.componentStore.load();
			},
			componentsSelections: [],
			componentColumns: columns,
			componentsIsVisible$: function() {
				return !this.showOption;
			},
			showOption: false,
			selectionTab: function() {
				this.set('showOption', false);
			},
			selectionTabIsPressed$: function() {
				return !this.showOption;
			},
			optionTab: function() {
				this.set('showOption', true);
			},
			optionTabIsPressed$: function() {
				return this.showOption;
			},
			componentsSelectionsChanged$: function() {
				if (this.parentVM.activeItem != this)
					return;
				if (this.componentsSelections && this.componentsSelections.length > 0)
					this.parentVM.set('chooseIsEnabled', true);
				else
					this.parentVM.set('chooseIsEnabled', false);
			},
			getOptions: function() {
				var options = {};
				Ext.each(optionsList, function(o) {
					options[o] = this[o];
				}, this);
				return options;
			},
			getSelectedRawCmps: function() {
				return this.getRawCmps(this.componentsSelections);
			},
			getRawCmps: function(storeRecords) {
				var rawCmps = [];
				Ext.each(storeRecords, function(r) {
					var obj = {};
					Ext.each(RS.impex.metaConfigMap[this.type].fields, function(f) {
						obj[f.name || f] = r.get(f.name || f);
					}, this);
					obj['type'] = this.type;
					rawCmps.push(obj);
				}, this);
				return rawCmps;
			},
			startDateChanged: function(newDate) {
				this.set('startDate', newDate);
			},

			endDateChanged: function(newDate) {
				this.set('endDate', newDate);
			}
		};
		Ext.apply(gridVMConfig, meta.options);
		glu.defModel('RS.impex.' + metaType + 'SearchMeta', gridVMConfig);
	});
	glu.defModel('RS.impex.customDBSearchMeta', {
		utableName: '',
		utableNameIsValid$: function() {
			return this.utableName ? true : this.localize('invalidTableName');
		},
		uquery: '',
		when_custom_db_def_changed: {
			on: ['utableNameChanged', 'uqueryChanged'],
			action: function() {
				if (this.utableNameIsValid)
					this.parentVM.set('chooseIsEnabled', true);
			}
		},
		getOptions: function() {
			return {};
		},
		getSelectedRawCmps: function() {
			return [{
				utableName: this.utableName,
				uquery: this.uquery,
				type: 'customDB'
			}]
		}
	});
}