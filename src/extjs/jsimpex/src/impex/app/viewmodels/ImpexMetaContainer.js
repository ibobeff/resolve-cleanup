glu.defModel('RS.impex.ImpexMetaContainer', {
	metaVMs: {
		mtype: 'activatorlist',
		focusProperty: 'activeItem',
		autoParent: true
	},
	cachedMetaVM: {},
	type: 'empty',
	typeStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	selectedType: null,
	selectedTypeChanged$: function() {
		if (!this.selectedType)
			return;
		this.set('type', this.selectedType.get('value'));
	},
	typeCatStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	selectType: function(type) {
		var t = null;
		this.typeStore.each(function(r) {
			if (r.get('value') == type)
				t = r;
		}, this);
		if (!t)
			return;
		this.set('selectedType', t);
	},
	typeCategory: '',
	updateTypeStore: function() {
		this.typeStore.removeAll();
		if (this.typeCategory == 'uncategorized') {
			Ext.each(this.uncategorizedTypes, function(type) {
				if (type == 'empty' || !this.filterType(type))
					return;
				this.typeStore.add({
					name: this.localize(type),
					value: type
				});
			}, this);
			return;
		}
		if (!RS.impex.componentTypeCategoryConfig[this.typeCategory])
			return;
		Ext.each(RS.impex.componentTypeCategoryConfig[this.typeCategory], function(type) {
			if (type == 'empty' || !this.filterType(type))
				return;
			this.typeStore.add({
				name: this.localize(type),
				value: type
			});
		}, this);
		this.typeStore.sort({
			property: 'name',
			direction: 'ASC'
		});
		this.selectType(this.typeStore.getAt(0).get('value'));
	},
	when_category_changed: {
		on: ['typeCategoryChanged'],
		action: function() {
			this.updateTypeStore();
		}
	},
	filterType: function() {
		return true;
	},
	activeItem: null,
	updateActiveComponentType: function() {
		if (!this.initialSearch)
			this.cachedMetaVM[this.type].set('type', this.type);
		else
			this.set('initialSearch', false);
	},
	typeChanged$: function() {
		defineImpexDefinitionMetaVMIfNotExist();
		defineImpexSearchMetaVMIfNotExist();
		if (!this.type || !RS.impex.metaConfigMap[this.type])
			return;
		if (!this.cachedMetaVM[this.type]) {
			var vm = this.model('RS.impex.' + this.type + (this.viewmodelName.indexOf('Definition') != -1 ? 'Definition' : 'Search') + 'Meta');
			this.metaVMs.add(vm);
			this.cachedMetaVM[this.type] = vm;
		}
		this.set('activeItem', this.cachedMetaVM[this.type]);
	},
	uncategorizedTypes: [],
	filterCategory: function() {
		return true;
	},
	//we can do this kind of naive filtering coz the category is small,if in the future there will be a lot of category then we need to improve the algorithm
	figureOutTypeAndCat: function() {
		var categorizedTypes = {};
		this.set('typeCategory', '');
		this.set('type', '');
		this.typeCatStore.removeAll();
		Ext.Object.each(RS.impex.componentTypeCategoryConfig, function(k, v) {
			if (this.filterCategory(k)) {
				this.typeCatStore.add({
					name: this.localize(k),
					value: k
				});
				Ext.each(v, function(type) {
					categorizedTypes[type] = true;
				})
			}
		}, this);
		var uncategorizedTypes = [];
		Ext.Object.each(RS.impex.metaConfigMap, function(k, v) {
			if (!categorizedTypes[k])
				uncategorizedTypes.push(k)
		}, this);
		if (uncategorizedTypes && this.filterCategory('uncategorized')) {
			// this.typeCatStore.add({
			// 	name: this.localize('uncategorized'),
			// 	value: 'uncategorized'
			// });
			this.set('uncategorizedTypes', uncategorizedTypes);
		}
		this.typeCatStore.sort({
			property: 'name',
			direction: 'ASC'
		});
	}
})