glu.defModel('RS.impex.DefinitionV2', {
	mixins: ['ImpexMetaContainer'],
	wait: false,
	id: '',
	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrg: '',

	uname: '',
	unameIsValid$: function() {
		return this.uname && (/^([a-zA-Z0-9_\-]+[a-zA-Z0-9_\-\s\.])*[a-zA-Z0-9_\-\s]*[a-zA-Z0-9_\-]$/.test(this.uname) || /^[a-zA-Z0-9_\-]+$/.test(this.uname)) && !/.*\s{2,}.*/.test(this.uname) ? true : this.localize('invalidName');
	},
	uforwardWikiDocument: '',
	uforwardWikiDocumentIsValid$: function() {
		if (!this.uforwardWikiDocument)
			return true;
		if (/[^a-zA-Z0-9_ \-.]+/.test(this.uforwardWikiDocument))
			return this.localize('invalidWikiChars');
		if (!/^[\w]+[.]{1}[\w]+$/.test(this.uforwardWikiDocument))
			return this.localize('invalidWikiFormat');
		return true;
	},
	uscriptName: '',
	udescription: '',

	defaultData: {
		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrg: '',
		uname: '',
		uforwardWikiDocument: ''
	},

	metaVMs: {
		mtype: 'activatorlist',
		focusProperty: 'activeItem',
		autoParent: true
	},
	fields: ['uname', 'uforwardWikiDocument', 'uscriptName', 'udescription'].concat(RS.common.grid.getSysFields()),
	activeItem: '',
	componentsGridIsVisible: false,
	reverseCatTypeMap: {},
	figureOutReverseCatTypeMap: function() {
		Ext.Object.each(RS.impex.componentTypeCategoryConfig, function(k, v) {
			Ext.each(v, function(type) {
				if (!this.reverseCatTypeMap[type])
					this.reverseCatTypeMap[type] = [];
				this.reverseCatTypeMap[type].push(k);
			}, this)
		}, this);
		Ext.Object.each(RS.impex.metaConfigMap, function(k, v) {
			if (!this.reverseCatTypeMap[k])
				this.reverseCatTypeMap[k] = ['uncategorized'];
		}, this);
	},
	filterCategory: function(cat) {
		var contains = false;
		this.metaVMs.forEach(function(metaVM) {
			if (metaVM.type == 'empty')
				return;
			if (this.reverseCatTypeMap[metaVM.type].indexOf(cat) != -1)
				contains = true;
		}, this);
		return contains;
	},
	filterType: function(type) {
		var contains = false;
		this.metaVMs.forEach(function(metaVM) {
			if (metaVM.type == type)
				contains = true;
		});
		return contains;
	},
	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'));
		this.resetForm();
		this.set('id', params && params.id || '');
		if (params && params.id)
			Ext.Function.defer(this.loadDefinition, 500, this);
	},
	batch: false,
	init: function() {
		this.on('activeCmpSelChange', function(vm) {
			if (vm != this.activeItem)
				return false;
			this.set('removeComponentsIsEnbled', vm.componentsSelections.length > 0);
		});
		this.on('cmpStoreChanged', function(vm) {
			if (this.batch)
				return;
			if (vm.componentStore.getCount() == 0) {
				this.metaVMs.remove(vm);
				vm.set('attached', false);
			} else {
				if (!vm.attached) {
					vm.set('attached', true);
					this.metaVMs.add(vm)
				}
			}
			this.set('componentsGridIsVisible', this.metaVMs.length > 1);
			this.figureOutTypeAndCat();
			if (vm.attached) {
				this.set('typeCategory', this.reverseCatTypeMap[vm.type][0]);
				this.selectType(vm.type)
			} else {
				if (this.metaVMs.length > 1) {
					this.set('typeCategory', this.reverseCatTypeMap[this.metaVMs.getAt(1).type][0]);
					this.selectType(this.metaVMs.getAt(1).type)
				}
			}
		}, this);
		this.figureOutReverseCatTypeMap();
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.impex.Main'
		});
	},
	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveDefinition, this, [exitAfterSave])
		this.task.delay(500)
	},
	saveIsEnabled$: function() {
		return !this.wait && this.isValid;
	},
	saveAndExit: function() {
		if (!this.saveIsEnabled)
			return;
		if (this.task) this.task.cancel()
		this.save(true)
	},
	saveDefinition: function(exitAfterSave) {
		var defs = [];
		this.metaVMs.forEach(function(vm) {
			defs = defs.concat(vm.getCmpImpexDefinitions());
		});
		var data = this.asObject();
		data.impexDefinition = defs;
		this.set('wait', true);
		data.uname = Ext.String.trim(data.uname);
		this.ajax({
			url: '/resolve/service/impex/save',
			jsonData: data,
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success) {
					clientVM.displayError(this.localize('saveError', respData.message || ''));
					return;
				}
				clientVM.displaySuccess(this.localize('SaveSuc'));
				var data = respData.data;
				this.resetForm();
				this.populateEverything(data);
				this.set('id', data.id);
				if (exitAfterSave === true)
					this.back();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
		return data;
	},
	populateEverything: function(data) {
		this.loadData(data);
		this.dispatchRecords(data.impexDefinition);
	},

	getRawCmpsFromServerResp: function(record) {
		var rawCmps = [];
		var obj = {};
		var desp = Ext.decode(record.description);
		Ext.each(RS.impex.metaConfigMap[record.type].fields, function(f) {
			obj[f.name || f] = desp[f.name || f];
		}, this);
		obj['options'] = record.options;
		obj['sys_id'] = record.sys_id;
		obj['sysUpdatedOn'] = record.sysUpdatedOn;
		obj['sysUpdatedBy'] = record.sysUpdatedBy;
		return obj;
	},
	processAdd: function(r, massager, options) {
		var vm = null;
		var m = null;
		var o = null;
		if (Ext.isFunction(massager)) {
			m = massager;
			o = options;
		} else
			o = massager;
		if (!this.cachedMetaVM[r.type]) {
			var modelName = r.type + (this.viewmodelName.indexOf('Definition') != -1 ? 'Definition' : 'Search') + 'Meta';
			if(!glu.namespace('RS.impex.viewmodels')[modelName]){
			clientVM.displayError(this.localize('invalidDefinitionMsg', r.type));	
				return false;
			}			
			else{
				vm = this.model('RS.impex.' + modelName);
				this.cachedMetaVM[r.type] = vm;
			}			
		}
		vm = this.cachedMetaVM[r.type];
		if (!vm.attached) {
			vm.set('attached', true);
			this.metaVMs.add(vm);
		}
		vm.addOrUpdateCmp(m ? m(r) : r, o);
		return true;
	},
	dispatchRecords: function(records) {
		var invalidRecord = [];
		if (records.length == 0)
			return;
		this.set('batch', true);
		for (var i = 0; i < records.length - 1; i++){
			var added = this.processAdd.apply(this, [records[i], this.getRawCmpsFromServerResp]);
			if(!added){
				records.splice(i, 1);
				i--;
			}
		}	

		this.set('batch', false);
		this.processAdd.apply(this, [records[records.length - 1], this.getRawCmpsFromServerResp]);
		if (this.metaVMs.length > 1) {
			this.set('typeCategory', this.reverseCatTypeMap[records[0].type][0]);
			this.selectType(records[0].type);
		}
	},
	loadDefinition: function() {
		if(!this.id)
			return;
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/impex/get',
			method: 'GET',
			params: {
				moduleId: this.id
			},
			scope: this,
			success: function(response) {
				var respData = RS.common.parsePayload(response);
				if (!respData.success) {
					clientVM.displayError(this.localize('LoadDefinitionErr'), respData.message);
					return;
				}
				var data = respData.data;
				this.populateEverything(data);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},
	resetForm: function() {
		var id = this.id;
		this.loadData(this.defaultData);
		while (this.metaVMs.length > 1)
			this.metaVMs.forEach(function(vm) {
				if (vm.type == 'empty')
					return;
				vm.componentStore.removeAll();
			});
		this.set('id', id);
	},
	refresh: function() {
		this.resetForm();
		this.loadDefinition();
	},	
	addComponents: function() {
		var me = this;
		this.open({
			mtype: 'RS.impex.ComponentSearchV2',
			dumper: function(rawCmps, options) {
				Ext.each(rawCmps, function(rawCmp) {
					me.processAdd(rawCmp, options);
				});
			}
		});
	},
	removeComponentsIsEnbled: false,
	removeComponents: function() {
		this.activeItem.removeSelectedCmps();
	},
	typeSelectionChanged: function(selections) {
		this.set('selectedType', selections.length > 0 ? selections[0] : null);
	}
})
