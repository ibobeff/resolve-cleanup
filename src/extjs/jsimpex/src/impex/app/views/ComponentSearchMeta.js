Ext.Object.each(RS.impex.metaConfigMap, function(metaType, meta) {
	if (metaType == 'customDB')
		return;
	var t = metaType;
	var options = [];
	Ext.Object.each(meta.options, function(k, v) {
		if (k.indexOf('$') != -1)
			return;
		if (k == 'startDate')
			options.push({
				xtype: 'datefield',
				hideLabel: false,
				width: 350,
				name: k,
				format: 'm/d/Y',
				maxValue: '@{endDate}',
				labelWidth: 50,
				listeners: {
					change: function(datefield, newDate, oldDate) {
						datefield.fireEvent('startDateChanged', datefield, newDate);
					},
					startDateChanged: '@{startDateChanged}'
				}
			});
		else if (k == 'endDate')
			options.push({
				xtype: 'datefield',
				hideLabel: false,
				width: 350,
				name: 'endDate',
				minValue : '@{startDate}',
				format: 'm/d/Y',
				labelWidth: 50,
				listeners: {
					change: function(datefield, newDate, oldDate) {
						datefield.fireEvent('endDateChanged', datefield, newDate);
					},
					endDateChanged: '@{endDateChanged}'
				}
			});
		else
			options.push({
				name: k,
				boxLabel: '~~' + k + '~~'
			});
	});
	defineSearchMetaViewConfig(metaType, options);
});
function defineSearchMetaViewConfig(metaType, options){
	var grid = {
		layout: 'fit',
		buttonAlign: 'left',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'toolbar',
				layout: 'hbox',
				items: [{
					xtype: 'tbtext',
					cls: 'rs-display-name',
					text: '~~options~~',
					hidden: '@{!showOption}'
				}, {
					minWidth: 300,
					xtype: 'combobox',
					itemId: metaType + 'filter',
					emptyText: '~~filter~~',
					inputCls: 'post-message',
					filterStore: '@{componentStore}',
					hidden: '@{showOption}',
					displayField: 'name',
					valueField: 'value',
					flex: 1,
					margins: {
						left: 5
					},
					plugins: [{
						ptype: 'searchfieldfilter'
					}],
					getFilterBar: function() {
						return this.ownerCt.ownerCt.ownerCt.down('#' + metaType + 'filterBar');
					},
					columns: '@{componentColumns}',
					plugins: [{
						ptype: 'searchfieldfilter'
					}],
					listeners: {
						beforerender: function(combo) {
							Ext.Array.forEach(combo.columns, function(column) {
								column.text = column.header;
							});
						}
					}
				}, '->', 'selectionTab', 'optionTab']
			}]
		}, {
			xtype: 'filterbar',
			itemId: metaType + 'filterBar',
			hidden: '@{showOption}',
			allowPersistFilter: false,
			listeners: {
				add: {
					fn: function() {
						this.ownerCt.down('#' + metaType + 'filter').getPlugin().filterChanged()
					},
					buffer: 10
				},
				remove: {
					fn: function() {
						this.ownerCt.down('#' + metaType + 'filter').getPlugin().filterChanged()
					},
					buffer: 10
				},
				clearFilter: function() {
					this.ownerCt.down('#' + metaType + 'filter').clearValue()
				}
			}
		}],
		items: [{
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar rs-dockedtoolbar',
				name: 'actionBar',
				items: []
			}],
			name: 'components',
			xtype: 'grid',
			cls : 'rs-grid-dark',
			layout: 'fit',
			store: '@{componentStore}',
			columns: '@{componentColumns}',
			plugins: [{
				ptype: 'pager',
				showSysInfo: false
			}, {
				ptype: 'columnautowidth'
			}]
		}, {
			hidden: '@{!showOption}',
			defaultType: 'checkboxfield',
			defaults: {
				hideLabel: true
			},
			items: options
		}],
		listeners: {
			activate: '@{resetTime}'
		}
	};
	glu.defView('RS.impex.' + metaType + 'SearchMeta', grid);
}
glu.defView('RS.impex.customDBSearchMeta', {
	title: '',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'textfield',
		name: 'utableName'
	}, {
		xtype: 'textarea',
		flex: 1,
		name: 'uquery'
	}]
});