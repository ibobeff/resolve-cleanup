Ext.Object.each(RS.impex.metaConfigMap, function(metaType, meta) {
	var t = metaType;
	var options = [];
	Ext.Object.each(meta.options, function(k, v) {
		if (k.indexOf('$') != -1)
			return;
		if (k == 'startDate') {
			options.push({
				xtype: 'datefield',
				hideLabel: false,
				width: 350,
				name: 'startDate',
				format: 'm/d/Y',
				maxValue: '@{endDate}',
				value: '@{startDate}',
				labelWidth: 50,
				listeners: {
					change: function(datefield, newDate, oldDate) {
						datefield.fireEvent('startDateChanged', datefield, newDate);
					},
					startDateChanged: '@{startDateChanged}'
				}
			})
			return
		}
		if (k == 'endDate') {
			options.push({
				xtype: 'datefield',
				hideLabel: false,
				width: 350,
				name: 'endDate',
				minValue : '@{startDate}',
				format: 'm/d/Y',
				labelWidth: 50,
				listeners: {
					change: function(datefield, newDate, oldDate) {
						datefield.fireEvent('endDateChanged', datefield, newDate);
					},
					endDateChanged: '@{endDateChanged}'
				}
			})
			return
		}
		options.push({
			name: k,
			boxLabel: '~~' + k + '~~'
		});
	});
	if (options.length > 0)
		options[0].padding = '10 0 0 0';
	defineMetaViewConfig(metaType, options);	
});

function defineMetaViewConfig(metaType, options){
	var grid = {
		layout: 'fit',		
		items: [{
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar rs-dockedtoolbar'
			}],
			name: 'components',
			xtype: 'grid',
			cls : 'rs-grid-dark',
			layout: 'fit',
			store: '@{componentStore}',
			columns: '@{componentColumns}',
			selModel: {
				selType: 'resolvecheckboxmodel',
				columnTooltip: RS.common.locale.editColumnTooltip,
				glyph : 0xF044,
				columnTarget: '_self',
				columnEventName: 'editAction',
				columnIdField: 'tmpBrowserId'
			},
			viewConfig: {
				enableTextSelection: true
			},
			listeners: {
				editAction: '@{editOption}'
			}
		}]
	};

	var optionPanel = {
		layout: 'vbox',
		defaultType: 'checkboxfield',
		defaults: {
			hideLabel: true
		},
		items: options
	};
	glu.defView('RS.impex.' + metaType + 'DefinitionMeta', grid);
	glu.defView('RS.impex.' + metaType + 'DefinitionMetaOption', {		
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			title: '~~component~~',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'displayfield',
				name: 'component'
			}, {
				xtype: 'displayfield',
				name: 'typeText'
			}]
		}, {
			title: '~~options~~',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaultType: 'checkboxfield',
			defaults: {
				hideLabel: true
			},
			items: options
		}],
		asWindow: {
			title: '~~componentOptions~~',
			cls : 'rs-modal-popup',
			width: 400,
			padding : 15,
			modal: true
		},	
		buttons: [{
			name : 'update', 
			cls : 'rs-med-btn rs-btn-dark',
		},{
			name : 'cancel',
			cls : 'rs-med-btn rs-btn-light'
		}]
	});
}