glu.defView('RS.impex.Component', function(args) {
	var vm = args.vm;
	var options = [];
	Ext.each(vm.types, function(type) {
		if (type == 'menuDefinition')
			return;
		for (opt in vm[type + 'Opt']) {
			if (opt == 'startDate' || opt == 'endDate')
				continue;

			options.push({
				name: opt,
				boxLabel: '~~' + opt + '~~'
			});
		}
	});
	var config = {
		title: '~~component~~',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		bodyPadding: '10px',
		defaultType: 'displayfield',
		defaults: {
			labelWidth: 100
		},

		items: ['component', 'typeText', {
			xtype: 'panel',
			layout: 'vbox',
			defaultType: 'checkbox',
			title: 'Options',
			defaults: {
				hideLabel: 150
			},
			items: options.concat([{
				xtype: 'datefield',
				hideLabel: false,
				width: 350,
				name: 'startDate',
				format: 'm/d/Y',
				maxValue: '@{endDate}',
				labelWidth: 50,
				listeners: {
					change: function(datefield, newDate, oldDate) {
						datefield.fireEvent('startDateChanged', datefield, newDate);
					},
					startDateChanged: '@{startDateChanged}'
				}
			}, {
				xtype: 'datefield',
				hideLabel: false,
				width: 350,
				name: 'endDate',
				minValue: '@{startDate}',
				format: 'm/d/Y',
				labelWidth: 50,
				listeners: {
					change: function(datefield, newDate, oldDate) {
						datefield.fireEvent('endDateChanged', datefield, newDate);
					},
					endDateChanged: '@{endDateChanged}'
				}
			}])
		}],
		asWindow: {
			title: '~~componentOptions~~',
			modal: true,
			cls : 'rs-modal-popup',
			padding : 15,
			width: 400
		},	
		buttons: [{
			cls : 'rs-med-btn rs-btn-dark',
			name : 'setOptions',
		}, {
			cls : 'rs-med-btn rs-btn-light',
			name :  'close'
		}]
	}

	return config;
});