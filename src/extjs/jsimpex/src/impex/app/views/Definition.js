glu.defView('RS.impex.Definition', function(arg) {
	var vm = arg.vm;
	var tabs = [];
	var grids = [];
	Ext.each(vm.types, function(attr) {
		tabs.push({
			xtype: 'button',
			name: attr + 'Tab',
			listeners: {
				show: function() {
					if (!vm[attr + 'TabIsVisible'])
						this.hide();
				}
			}
		});

		grids.push({
			xtype: 'grid',
			cls : 'rs-grid-dark',
			minHeight: 300,
			title: '~~' + attr + '~~',
			id: attr + 'Card',
			name: attr,
			columns: '@{' + attr + 'Columns}',
			store: '@{' + attr + 'Store}',
			selModel: {
				selType: 'resolvecheckboxmodel',
				columnTooltip: RS.common.locale.editColumnTooltip,
				columnTarget: '_self',
				columnEventName: 'editAction',
				columnIdField: 'id'
			},
			listeners: {
				editAction: '@{editComponentOpt}'
			}
		});
	});
	return {
		xtype: 'panel',
		autoScroll: true,
		bodyPadding: '10px',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			ui: 'display-toolbar',
			defaultButtonUI: 'display-toolbar-button',
			items: [{
				xtype: 'tbtext',
				cls: 'rs-display-name',
				text: '~~impexDefinitionTitle~~'
			}]
		}, {
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar actionBar-form',
			items: [{
				name: 'back',
				iconCls: 'icon-large icon-eject rs-icon',
				text: ''
			}, {
				name: 'save',
				listeners: {
					doubleClickHandler: '@{saveAndExit}',
					render: function(button) {
						button.getEl().on('dblclick', function() {
							button.fireEvent('doubleClickHandler')
						});
						clientVM.updateSaveButtons(button);
					}
				}
			}, '->', {
				xtype: 'sysinfobutton',
				sysId: '@{id}',
				sysCreated: '@{sysCreatedOn}',
				sysCreatedBy: '@{sysCreatedBy}',
				sysUpdated: '@{sysUpdatedOn}',
				sysUpdatedBy: '@{sysUpdatedBy}',
				sysOrg: '@{sysOrganizationName}',
				dateFormat: '@{userDateFormat}'
			}, {
				iconCls: 'x-tbar-loading',
				handler: '@{refresh}',
				listeners: {
					render: function(button) {
						clientVM.updateRefreshButtons(button);
					}
				}
			}]
		}],
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'panel',
			layout: 'hbox',
			items: [{
				xtype: 'panel',
				flex: 1,
				defaultType: 'textfield',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: ['uname', {
					name: 'uforwardWikiDocument',
					tooltip: '~~forwardTooltip~~',
					listeners: {
						render: function(p) {
							p.getEl().down('input').set({
								'data-qtip': p.tooltip
							});
						}
					}
				}, 'uscriptName', {
					xtype: 'textarea',
					name: 'udescription'
				}]
			}]
		}, {
			xtype: 'panel',
			title: '~~Components~~',
			flex: 1,
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar',
				name: 'actionBar',
				items: ['addComponents', 'remove', '-'].concat(tabs),
				listeners: {
					overflowchange: function(lastHiddenCount, hiddenCount, hiddenItems) {
						Ext.each(this.items.items, function(tab) {
							if (/.*Tab$/.test(tab.name))
								tab.initialConfig.hidden = false;
						})
					}
				}
			}],
			layout: 'fit',
			activeItem: '@{activeItem}',
			items: grids
		}]
	}
})
