glu.defView('RS.impex.DefinitionV2', {
	padding: 15,
	xtype: 'panel',
	autoScroll: true,	
	dockedItems: [{
		xtype: 'toolbar',	
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls : 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '~~impexDefinitionTitle~~'
		}]
	}, {
		xtype: 'toolbar',
		cls: 'actionBar rs-dockedtoolbar',	
		defaults : {
			cls :'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'	
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'panel',
		style : 'border-top:1px solid silver',
		padding : '5 0',	
		layout: 'hbox',		
		items: [{
			xtype: 'panel',
			flex: 1,
			defaultType: 'textfield',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			defaults : {
				margin : '4 0',
			},
			items: ['uname', {
				name: 'uforwardWikiDocument',
				tooltip: '~~forwardTooltip~~',
				listeners: {
					render: function(p) {
						p.getEl().down('input').set({
							'data-qtip': p.tooltip
						});
					}
				}
			}, 'uscriptName', {
				xtype: 'textarea',
				name: 'udescription'
			}]
		}]
	}, {
		flex: 1,
		xtype: 'panel',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		minHeight: 100,
		title: '~~Components~~',
		dockedItems: [{
			xtype: 'toolbar',
			margin : '8 0 0 0',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light',
			},
			items: ['addComponents', {
				name: 'removeComponents',
				disabled: '@{!removeComponentsIsEnbled}'
			}]
		}],
		items: [{
			hidden: '@{!componentsGridIsVisible}',
			width: 200,
			minWidth: 200,
			margin : '8 0 0 0',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'combo',
				hideLabel: true,
				name: 'typeCategory',
				queryMode: 'local',
				editable: false,
				displayField: 'name',
				emptyString: '~~selectCategory~~',
				valueField: 'value',
				store: '@{typeCatStore}'
			}, {
				xtype: 'grid',
				cls : 'rs-grid-dark',
				store: '@{typeStore}',
				hideHeaders: true,
				selectedType: "@{selectedType}",
				setSelectedType: function(type) {
					var sm = this.getSelectionModel();
					sm.select(type || []);
				},
				flex: 1,
				columns: [{
					header: '~~name~~',
					dataIndex: 'name',
					flex: 1
				}],
				listeners: {
					selectionschanged: '@{typeSelectionChanged}'
				}
			}]
		}, {
			hidden: '@{!componentsGridIsVisible}',
			xtype: 'splitter'
		}, {
			hidden: '@{!componentsGridIsVisible}',
			flex: 1,
			xtype: 'panel',
			layout: 'card',
			activeItem: '@{activeItem}',
			items: '@{metaVMs}'
		}]
	}]
});
