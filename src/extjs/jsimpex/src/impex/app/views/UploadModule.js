glu.defView('RS.impex.UploadModule', {
	asWindow: {
		title: '~~uploadModuleWindowTitle~~',
		cls : 'rs-modal-popup',
		height: 500,
		width: 800,
		modal: true,
		padding : 15,
		closable: true
	},

	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	
	items: [{
		html: '@{busyText}',
		margin: '0 0 10 0'
	}, {
        xtype: 'fieldcontainer',
        hidden: '@{!impexing}',
        items: [{
            itemId: 'pg',
            xtype: 'progressbar'
        }, {
            xtype: 'displayfield',
            hidden: true,
            value: '@{progress}',
            listeners: {
                change: function(field, newData) {
                    var progressBar = this.ownerCt.down('#pg');
                    progressBar.updateProgress(newData / 100, newData + '%');
                }
            }
        }]
    }, {
		itemId: 'fineUploaderTemplate',
		html: '@{fineUploaderTemplate}',
		height: '@{fineUploaderTemplateHeight}',
		hidden: '@{fineUploaderTemplateHidden}',
	}, {
		id: 'attachments_grid',
		hidden: true
	}],

	listeners: {
		afterrender: function() {
			this.fireEvent('afterRendered', this, this);
		},
		afterRendered: '@{viewRenderCompleted}',
		uploadComplete: function() {
			var fineuploader = this.down('#fineUploaderTemplate');
			fineuploader.hide();
		}
	},

	buttons: [{
        itemId: 'browse',
        cls : 'rs-med-btn rs-btn-dark',
        text: '~~browse~~',
        hidden: '@{!browseIsVisible}',
		id: 'upload_button',
		preventDefault: false,
    },{
        name : 'importModule',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'manifest',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'startWiki', 
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'getLastImpexResult',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name :'close',
        cls : 'rs-med-btn rs-btn-light'
	}]
})

/*
glu.defView('RS.impex.UploadModule', {
    xtype: 'panel',
    getConfig: function() {
        return {
            autoStart: true,
            url: '/resolve/service/impex/uploadFile',
            filters: [{
                title: "Zip files",
                extensions: "zip"
            }],
            browse_button: this.getBrowseButton(),
            drop_element: this.getDropArea(),
            container: this.getContainer(),
            chunk_size: null,
            runtimes: "html5,flash,silverlight,html4",
            flash_swf_url: '/resolve/js/plupload/Moxie.swf',
            unique_names: true,
            multipart: true,
            multipart_params: {},
            multi_selection: false,
            required_features: null,
            ownerCt: this
        }
    },
    getBrowseButton: function() {
        return this.down('#browse').btnEl.dom.id;
    },
    getDropArea: function() {
        return this.down('#dropzone').getEl().dom.id;
    },
    getContainer: function() {
        var btn = this.down('#browse');
        return btn.ownerCt.id;
    },
    uploaderListeners: {
        beforestart: function(uploader, data) {
            //console.log('beforestart')
        },
        // uploadready: function(uploader, data) {
        //     var me = uploader.uploaderConfig.ownerCt;
        //     var inputs = Ext.query('#' + me.getEl().id + ' input[type="file"]');
        //     var file = inputs[0];
        //     var runtime = mOxie.Runtime.getRuntime(file.id);
        //     me.fireEvent('runtimedecided', null, runtime);
        // },
        uploadstarted: function(uploader, data) {},
        uploadcomplete: function(uploader, data) {
            this.owner.fireEvent('uploadComplete', this.owner, uploader, data);
        },
        // uploaderror: function(uploader, data) {
        //     this.owner.fireEvent('uploadError', this.owner, uploader, data);
        // },
        filesadded: function(uploader, data) {
            this.owner.fireEvent('filesAdded', this.owner, uploader, data);
        },
        beforeupload: function(uploader, data) {
            //console.log('beforeupload')
        },
        // fileuploaded:function(uploader,data,response){this.owner.fireEvent('fileUploaded',this.owner,uploader,data,response);},
        uploadprogress: function(uploader, file, name, size, percent) {
            this.owner.percent = file.percent;
        },
        storeempty: function(uploader, data) {
            //console.log('storeempty')
        }
    },

    listeners: {
        uploadError: '@{uploadError}',
        filesAdded: '@{filesAdded}',
        uploadComplete: '@{uploadComplete}',
        runtimedecided: '@{runtimeDecided}',
        afterrender: function() {
            this.myUploader = Ext.create('RS.common.SinglePlupUpload', this);
            this.uploader = this.getConfig();
            this.myUploader.initialize(this);
        }
    },
  
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults : {
        margin : '4 0'
    },
    dockedItems: [{
        xtype: 'toolbar',
        cls : 'rs-dockedtoolbar',      
        items: [/ *{ 
            xtype: 'radiofield',
            name: 'type',
            hideLabel: true,
            boxLabel: '~~file~~',
            disabled: '@{!fileIsEnabled}',
            hidden: '@{!fileIsVisible}',
            value: '@{file}'
        }, {
            xtype: 'radiofield',
            name: 'type',
            hideLabel: true,
            boxLabel: '~~url~~',
            disabled: '@{!urlIsEnabled}',
            hidden: '@{!urlIsVisible}',
            value: '@{url}'
        }* /]
    }],
    items: [{
        html: '@{busyText}'
    }, {
        xtype: 'form',
        itemId: 'dropzone',
        style: 'border: 2px dashed #ccc;margin: 10px 0px 0px 0px',
        hidden: '@{!dropAreaIsVisible}',
        flex: 1,
        layout: {
            type: 'vbox',
            pack: 'center',
            align: 'center'
        },
        items: [{
            xtype: 'label',
            text: '~~dragHere~~',
            style: {
                'font-size': '30px',
                'color': '#CCCCCC'
            }
        }]
    }, {
        xtype: 'fieldcontainer',
        margin: '20px 0px 0px 0px',
        hidden: '@{!moduleURLIsVisible}',
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        items: [{
            xtype: 'textfield',
            labelWidth: 100,
            name: 'moduleURL',
            flex: 1
        }]
    }, {
        xtype: 'fieldcontainer',
        hidden: '@{!impexing}',
        items: [{
            itemId: 'pg',
            xtype: 'progressbar'
        }, {
            xtype: 'displayfield',
            hidden: true,
            value: '@{progress}',
            listeners: {
                change: function(field, newData) {
                    var progressBar = this.ownerCt.down('#pg');
                    progressBar.updateProgress(newData / 100, newData + '%');
                }
            }
        }]
    }],

    asWindow: {
        width: 600,
        height: 350,
        padding : 15,
        title: '~~uploadModuleWindowTitle~~',
        cls : 'rs-modal-popup',
        modal: true,
        closable: false
    },  
    buttons: [{
        itemId: 'browse',
        cls : 'rs-med-btn rs-btn-dark',
        text: '~~browse~~',
        hidden: '@{!browseIsVisible}',
        listeners: {
            hide: function() {
                this.up().el.query('.moxie-shim')[0].style.width = '0px';
                // this.up().el.query('.moxie-shim')[0].style.display = 'none'
            },
            show: function() {
                this.up().el.query('.moxie-shim')[0].style.width = '100%'
                    // this.up().el.query('.moxie-shim')[0].style.display = 'inline'
            }
        }
    }, {
        name : 'submit',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'importModule',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'manifest',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'startWiki', 
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'getLastImpexResult',
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name :'close',
        cls : 'rs-med-btn rs-btn-light'
   }]
})
*/