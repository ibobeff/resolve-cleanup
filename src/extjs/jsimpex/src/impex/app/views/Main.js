glu.defView('RS.impex.Main', {
    padding : 15,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [{
        flex: 1,
        xtype: 'grid',
        cls : 'rs-grid-dark',
        name: 'impexGridStore',
        columns: '@{columns}',
        displayName: '@{displayName}',
        plugins: [{
            ptype: 'searchfilter',
            allowPersistFilter: false,
            hideMenu: true
        }, {
            ptype: 'pager'
        }],
        selModel: {
            selType: 'resolvecheckboxmodel',         
            columnTarget: '_self',
            columnEventName: 'editAction',
            columnIdField: 'id'
        },
        dockedItems: [{
            xtype: 'toolbar',
            dock: 'top',
            cls: 'actionBar rs-dockedtoolbar',
            name: 'actionBar',
            defaults : {
                cls : 'rs-small-btn rs-btn-light',
            },
            items: ['createDefinition', 'deleteDefinitions', '-', 'uploadDefinition', 'reimportDefinitions', 'exportDefinitions']
        }],
        viewConfig: {
            enableTextSelection: true
        },
        listeners: {
            editAction: '@{editDefinition}',
            selectAllAcrossPages: '@{selectAllAcrossPages}',
            selectionChange: '@{selectionChanged}'
        }
    }]
});

glu.defView('RS.impex.ImpexResult', {
    xtype: 'panel',
    align: 'fit',
    items: [{
        html: '@{..result}',
        listeners: {
            afterrender: function(panel, eOpt) {
                clientVM.makeCtrlASelectAllOnView(panel);
            }
        }
    }],
    // items:[{
    //     flex:1,
    //     xtype:'displayfield',
    //     name:'result',
    //     hideLabel:true
    // }],
    autoScroll: true,   
    buttons: [{
        name : 'ok',
        cls : 'rs-med-btn rs-btn-dark'
    }],
    asWindow: {
        modal: true,
        height: 600,
        padding : 15,
        cls : 'rs-modal-popup',
        width: 600,
        title: '@{title}'
    }
});