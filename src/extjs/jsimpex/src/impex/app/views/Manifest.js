glu.defView('RS.impex.Manifest', function(args) {
	var vm = args.vm;
	return {
		xtype: 'panel',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			hidden: '@{ready}',
			xtype: 'panel',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'label',
						text: '@{busyText}'
					}, {
						html: '<i class="icon-spinner icon-spin icon-large"></i>'
					}]
				}
				// {
				// 	id: 'pg1',
				// 	hidden: '@{!importing}',
				// 	xtype: 'progressbar',
				// }, {
				// 	xtype: 'displayfield',
				// 	hidden: true,
				// 	value: '@{progress}',
				// 	listeners: {
				// 		change: function(field, newData) {
				// 			var progressBar = this.ownerCt.down('#pg1');
				// 			progressBar.updateProgress(newData / 100, newData + '%');
				// 		}
				// 	}
				// }
			]
		}, {
			flex: 1,
			cls: 'manifest-component',
			xtype: 'grid',
			cls : 'rs-grid-dark',
			displayName: '~~Components~~',
			dockedItems: [{
				xtype: 'toolbar',
				dock: 'top',
				cls: 'actionBar rs-dockedtoolbar',
				name: 'actionBar',
				defaults : {
					cls : 'rs-small-btn rs-btn-light'
				},
				items: [{
					xtpye: 'button',
					text: '~~expandAll~~',
					handler: function() {
						var feature = this.ownerCt.ownerCt.view.getFeature('group');
						feature.expandAll();
					}
				}, {
					xtype: 'button',
					text: '~~collapseAll~~',
					handler: function() {
						var feature = this.ownerCt.ownerCt.view.getFeature('group');
						feature.collapseAll();
					}
				}]
			}],
			plugins: [{
				ptype: 'searchfilter',
				allowPersistFilter: false,
				hideMenu: true
			}, {
				ptype: 'pager'
			}],
			hidden: '@{!ready}',
			name: 'components',
			store: '@{componentStore}',
			columns: [{
				header: '~~ufullName~~',
				dataIndex: 'ufullName',
				filterable: true,
				sortable: false,
				flex: 1
			}, {
				header: '~~uoperation~~',
				dataIndex: 'uoperation',
				filterable: true,
				sortable: false
			}, {
				header: '~~udisplayType~~',
				dataIndex: 'udisplayType',
				filterable: true,
				hideable: false,
				hidden: true,
				sortable: false
			}].concat(vm.operation == 'EXPORT' ? [] : [{
				header: '~~ustatus~~',
				dataIndex: 'ustatus',
				filterable: false,
				sortable: false
			}]),

			selModel: {
				selType: 'checkboxmodel',
				onRowMouseDown: true,  // Dirty trick to fix the M -> V binding issue
				checkOnly: true,
				mode: 'MULTI',
				listeners: {
					select: function(item, record, index, eOpts) {
						var view = item.view,
							feature = view.getFeature('group'),
							el = view.getEl();

						if (el) {
							var groupField = feature.getGroupField(),
								group = record.get(groupField),
								groupId = view.id + '-' + 'hd' + '-' + group,
								selModel = view.getSelectionModel(),
								grpCheckboxList = el.query('.grpCheckbox'),
								isChecked = true;

							for (var i = 0; i < view.store.data.items.length; i++) {
								var rec = view.store.data.items[i];
								if (rec.get(groupField) == group) {
									if (!selModel.isSelected(rec)) {
										isChecked = false;
										break;
									}
								}
							}

							for (var i = 0; i < grpCheckboxList.length; i++) {
								var grpCheckbox = grpCheckboxList[i],
									currentGroupId = grpCheckbox.parentElement.parentElement.id;

								if (currentGroupId == groupId) {
									var groupHeader = document.getElementById(currentGroupId),
										groupHeaderCheckbox = groupHeader.getElementsByClassName('grpCheckbox');

									if (isChecked) {
										groupHeaderCheckbox[0].setAttribute('class', 'grpCheckbox checked');
									} else {
										groupHeaderCheckbox[0].setAttribute('class', 'grpCheckbox');
									}
									break;
								}
							}
						}
					},
					deselect: function(item, record, index, eOpts) {
						var view = item.view,
							feature = view.getFeature('group'),
							el = view.getEl();

						if (el) {
							var groupField = feature.getGroupField(),
								groupId = view.id + '-' + 'hd' + '-' + record.get(groupField),
								grpCheckboxList = el.query('.grpCheckbox');

							for (var i = 0; i < grpCheckboxList.length; i++) {
								var grpCheckbox = grpCheckboxList[i],
									currentGroupId = grpCheckbox.parentElement.parentElement.id;

								if (currentGroupId == groupId) {
									var groupHeader = document.getElementById(currentGroupId),
										groupHeaderCheckbox = groupHeader.getElementsByClassName('grpCheckbox');

									groupHeaderCheckbox[0].setAttribute('class', 'grpCheckbox');
									break;
								}
							}
						}
					}
				}
			},
			features: [{
				id: 'group',
				ftype: 'grouping',
				collapsible: false,
				groupHeaderTpl: '<span class="grpCheckbox"></span>Type: {name}'
			}],
			listeners: {
				sortchange: function(ct) {
					var view = ct.view,
						feature = view.getFeature('group'),
						el = view.getEl();

					if (el) {
						var groupField = feature.getGroupField(),
							grpCheckboxList = el.query('.grpCheckbox'),
							selModel = view.getSelectionModel(),
							selectedRec = selModel.getSelection(),
							groupIdList = [];

						for (var groupname in feature.groupCache) {
							var group = feature.groupCache[groupname];
							var isChecked = true;

							for (var i = 0; i < group.children.length; i++) {
								var rec = group.children[i];
								if (!selModel.isSelected(rec)) {
									isChecked = false;
									break;
								}
							}

							if (isChecked) {
								var groupId = view.id + '-' + 'hd' + '-' + groupname;
								groupIdList.push(groupId);
							}
						}

						for (var i = 0; i < groupIdList.length; i++) {
							var groupId = groupIdList[i];
							for (var j = 0; j < grpCheckboxList.length; j++) {
								var grpCheckbox = grpCheckboxList[j],
									currentGroupId = grpCheckbox.parentElement.parentElement.id;

								if (currentGroupId == groupId) {
									var groupHeader = document.getElementById(currentGroupId),
										groupHeaderCheckbox = groupHeader.getElementsByClassName('grpCheckbox');

									groupHeaderCheckbox[0].setAttribute('class', 'grpCheckbox checked');
									break;
								}
							}
						}
					}
				},
				groupclick: function(view, node, group, e, eOpts) {
					var t = e.getTarget('.grpCheckbox'),
						feature = view.getFeature('group');

					if (t) {
						var groupField = feature.getGroupField(),
							isChecked = t.classList.contains('checked') ? true : false,
							selModel = view.getSelectionModel();

						if (isChecked) {
							t.setAttribute('class', 'grpCheckbox');
						} else {
							t.setAttribute('class', 'grpCheckbox checked');
						}

						for (var i = 0; i < view.store.data.items.length; i++) {
							var rec = view.store.data.items[i];
							if(rec.get(groupField) == group){
								if(isChecked) {
									selModel.deselect(i, true, true);
								} else {
									selModel.select(i, true, true);
								}
							}
						}
					}
					else {
						var groupHeader = document.getElementById(node.id),
							groupHeaderCheckbox = groupHeader.getElementsByClassName('grpCheckbox'),
							isChecked = groupHeaderCheckbox[0].classList.contains('checked') ? true : false;

						if (feature.isExpanded(group)) {
							feature.collapse(group);
						} else {
							feature.expand(group);
						}

						if (isChecked) {
							groupHeader = document.getElementById(node.id);
							groupHeaderCheckbox = groupHeader.getElementsByClassName('grpCheckbox');
							groupHeaderCheckbox[0].setAttribute('class', 'grpCheckbox checked');
						}
					}
				}
			}
		}],

		buttons: [{
			name :'importModule',
			cls : 'rs-med-btn rs-btn-dark',
		},{
			name : 'exportModule',
			cls : 'rs-med-btn rs-btn-dark'
		},{
			name : 'verify',
			cls : 'rs-med-btn rs-btn-dark'
		},{
			name :'close',
			cls : 'rs-med-btn rs-btn-light',
		}],
		asWindow: {
			title: '~~manifest~~',
			width: 800,
			height: 500,
			padding : 15,
			cls : 'rs-modal-popup',
			modal: true
		}
	}
});