glu.defView('RS.impex.ImpexOperations', {
    xtype: 'panel',  
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [{
        // /xtype: 'text',
        html: '@{busyText}'
    },
    //  {
    //     xtype: 'container',
    //     layout: 'hbox',
    //     items: [{
    //         // /xtype: 'text',
    //         html: '@{busyText}'
    //     }, {
    //         hidden: '@{!wait}',
    //         html: '<i class="icon-spinner icon-spin icon-large"></i>'
    //     }]
    // },
     {
        id: 'pg',
        xtype: 'progressbar',
        hidden: '@{!impexing}'
    }, {
        xtype: 'displayfield',
        hidden: true,
        value: '@{progress}',
        listeners: {
            change: function(field, newData) {
                var progressBar = this.ownerCt.down('#pg');
                progressBar.updateProgress(newData / 100, newData + '%');
            }
        }
    }], 
    buttons: [{
        name :'importModule', 
        cls : 'rs-med-btn rs-btn-dark',
    },{
        name : 'exportModule', 
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name : 'manifest',
         cls : 'rs-med-btn rs-btn-dark',
    },{
        name : 'startWiki',
         cls : 'rs-med-btn rs-btn-dark',
    },{ 
        name : 'getLastImpexResult',
        cls : 'rs-med-btn rs-btn-dark',
    },{
        name : 'download',
        cls : 'rs-med-btn rs-btn-dark',
    },{
        name : 'close',
        cls : 'rs-med-btn rs-btn-light',
    }],
    asWindow: {
        title: '@{title}',
        width: 460,
        height: 180,
        cls : 'rs-modal-popup',
        padding : 15,
        modal: true,
        closable: false
    }
});