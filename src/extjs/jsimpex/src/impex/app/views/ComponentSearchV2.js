glu.defView('RS.impex.ComponentSearchV2', {
	bodyPadding: '0 0 8 0',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	items: [{
		width: 200,
		minWidth: 200,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			padding: '3 0 0 0',
			xtype: 'combo',
			hideLabel: true,
			name: 'typeCategory',
			queryMode: 'local',
			displayField: 'name',
			emptyText: '~~selectCategory~~',
			editable: false,
			valueField: 'value',
			store: '@{typeCatStore}'
		}, {
			xtype: 'grid',
			cls : 'rs-grid-dark',
			flex: 1,
			store: '@{typeStore}',
			selected: '@{selectedType}',
			hideHeaders: true,
			columns: [{
				header: '~~name~~',
				dataIndex: 'name',
				flex: 1
			}]
		}]
	}, {
		xtype: 'splitter'
	}, {
		flex: 1,
		xtype: 'panel',
		layout: 'card',
		activeItem: '@{activeItem}',
		hidden: '@{optionIsVisible}',
		items: '@{metaVMs}'
	}],	
	buttons: [{
		name: 'choose',
		cls  :'rs-med-btn rs-btn-dark',
		disabled: '@{!chooseIsEnabled}'
	}, {
		name : 'close',
		cls : 'rs-btn-light rs-med-btn'
	}],
	asWindow: {
		title: '~~searchComponentTitle~~',
		modal: true,
		padding : 15,
		cls : 'rs-modal-popup',
		height: 700,
		width: 900,
		listeners: {
			show: function() {
				this.setHeight(700 > Ext.fly(document.body).getHeight() ? Ext.fly(document.body).getHeight() : 700);
				this.setWidth(900 > Ext.fly(document.body).getWidth() ? Ext.fly(document.body).getWidth() : 900);
			}
		}
	}
});