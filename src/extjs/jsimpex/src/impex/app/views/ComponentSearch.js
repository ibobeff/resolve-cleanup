glu.defView('RS.impex.ComponentSearch', function(args) {
	var vm = args.vm;
	var grid = null;
	var panels = [];
	Ext.each(vm.types, function(t) {
		if (t != 'customDB')
			grid = {
				layout: 'fit',
				id: t,
				buttonAlign: 'left',
				dockedItems: [{
					xtype: 'toolbar',
					dock: 'top',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: [{
						xtype: 'toolbar',
						items: [{
							xtype: 'combobox',
							editable: false,
							store: '@{typeStore}',
							displayField: 'name',
							valueField: 'value',
							queryMode: 'local',
							name: 'type',
							width: 230,
							labelWidth: 50
						}, {
							minWidth: 300,
							xtype: 'combobox',
							itemId: t + 'filter',
							emptyText: '~~filter~~',
							inputCls: 'post-message',
							filterStore: '@{' + t + 'Store}',
							displayField: 'name',
							valueField: 'value',
							autoSelect: false,
							margins: {
								left: 5
							},
							getFilterBar: function() {
								return this.ownerCt.ownerCt.ownerCt.down('#' + t + 'filterBar');
							},
							columns: '@{' + t + 'Columns}',
							plugins: [{
								ptype: 'searchfieldfilter'
							}],
							listeners: {
								beforerender: function(combo) {
									Ext.Array.forEach(combo.columns, function(column) {
										column.text = column.header;
									})
								}
							}
						}, '->', 'selectionTab', 'optionTab']
					}]
				}, {
					xtype: 'filterbar',
					itemId: t + 'filterBar',
					allowPersistFilter: false,
					listeners: {
						add: {
							fn: function() {
								this.ownerCt.down('#' + t + 'filter').getPlugin().filterChanged()
							},
							buffer: 10
						},
						remove: {
							fn: function() {
								this.ownerCt.down('#' + t + 'filter').getPlugin().filterChanged()
							},
							buffer: 10
						},
						clearFilter: function() {
							this.ownerCt.down('#' + t + 'filter').clearValue()
						}
					}
				}],
				items: [{
					dockedItems: [{
						xtype: 'toolbar',
						dock: 'top',
						cls: 'actionBar',
						name: 'actionBar',
						items: []
					}],
					name: t,
					xtype: 'grid',
					cls : 'rs-grid-dark',
					layout: 'fit',
					store: '@{' + t + 'Store}',
					columns: '@{' + t + 'Columns}',
					selModel: {
						selType: 'checkboxmodel',
						mode: 'MULTI'
					},
					plugins: [{
						ptype: 'pager',
						showSysInfo: false
					}, {
						ptype: 'columnautowidth'
					}]
				}]
			};
		else
			grid = {
				id: t,
				xtype: 'panel',
				dockedItems: [{
					xtype: 'toolbar',
					dock: 'top',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					items: [{
						xtype: 'toolbar',
						items: [{
							xtype: 'combobox',
							editable: false,
							store: '@{typeStore}',
							displayField: 'name',
							valueField: 'value',
							queryMode: 'local',
							name: 'type',
							width: 230,
							labelWidth: 50
						}]
					}]
				}],
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [{
					flex: 1,
					xtype: 'panel',
					title: '~~definition~~',
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					bodyPadding: '10px',
					items: [{
						xtype: 'textfield',
						name: 'utableName'
					}, {
						flex: 1,
						xtype: 'textarea',
						name: 'uquery'
					}]
				}]
			};
		panels.push(grid);
	});
	var none = {
		layout: 'fit',
		id: 'none',
		buttonAlign: 'left',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'toolbar',
				items: [{
					xtype: 'combobox',
					editable: false,
					store: '@{typeStore}',
					displayField: 'name',
					valueField: 'value',
					queryMode: 'local',
					name: 'type',
					width: 230,
					labelWidth: 50
				}, {
					minWidth: 300,
					xtype: 'combobox',
					disabled: true,
					itemId: 'nonefilter',
					emptyText: '~~filter~~',
					inputCls: 'post-message',
					filterStore: Ext.create('Ext.data.Store', {
						fields: [],
						proxy: {
							type: 'memory'
						}
					}),
					margins: {
						left: 5
					},
					getFilterBar: function() {
						return null;
					},
					columns: [],
					plugins: [{
						ptype: 'searchfieldfilter'
					}]
				}, '->', 'selectionTab', 'optionTab']
			}]
		}]
	};
	panels.push(none);
	var options = [];
	Ext.each(vm.types, function(type) {
		if (type == 'menuDefinition')
			return;
		for (opt in vm[type + 'Opt']) {
			if (opt == 'startDate' || opt == 'endDate')
				continue;

			options.push({
				name: opt,
				boxLabel: '~~' + opt + '~~'
			});
		}
	});

	var base = {
		xtype: 'form',
		autoScroll: true,
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		bodyPadding: '10px',
		items: [{
			flex: 1,
			xtype: 'panel',
			layout: {
				type: 'card'
			},
			activeItem: '@{type}',
			hidden: '@{optionIsVisible}',
			items: panels
		}, {
			xtype: 'panel',
			hidden: '@{!optionIsVisible}',
			layout: 'vbox',
			defaultType: 'checkbox',
			defaults: {
				hideLabel: true,
				padding: '0px 15px 0px 0px'
			},
			dockedItems: [{
				xtype: 'toolbar',
				cls: 'actionBar actionBar-form',
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				items: [{
					flex: 1,
					xtype: 'toolbar',
					items: [{
						xtype: 'tbtext',
						cls: 'rs-display-name',
						text: '~~options~~'
					}, '->', 'selectionTab', 'optionTab']
				}],
				margin: '0px 0px 30px 0px'
			}],
			items: options.concat([{
				xtype: 'datefield',
				hideLabel: false,
				width: 350,
				name: 'startDate',
				format: 'm/d/Y',
				maxValue: '@{endDate}',
				labelWidth: 50,
				listeners: {
					change: function(datefield, newDate, oldDate) {
						datefield.fireEvent('startDateChanged', datefield, newDate);
					},
					startDateChanged: '@{startDateChanged}'
				}
			}, {
				xtype: 'datefield',
				hideLabel: false,
				width: 350,
				name: 'endDate',
				minValue : '@{startDate}',
				format: 'm/d/Y',
				labelWidth: 50,
				listeners: {
					change: function(datefield, newDate, oldDate) {
						datefield.fireEvent('endDateChanged', datefield, newDate);
					},
					endDateChanged: '@{endDateChanged}'
				}
			}])
		}],
		buttons: [{
			name: 'choose',
			cls : 'rs-med-btn rs-btn-dark',
			enabled: '@{chooseIsEnabled}'
		},{
			cls : 'rs-med-btn rs-btn-dark',
			name : 'close'
		}],
		asWindow: {
			title: '~~searchComponentTitle~~',
			padding : 15,
			cls : 'rs-modal-popup',
			modal: true,
			height: 480,
			width: 760
		}
	};
	return base;
});