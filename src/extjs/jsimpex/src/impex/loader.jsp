<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.resolve.util.JspUtils" %>
<%@page import="org.owasp.esapi.ESAPI"%>
<%
    String ver = ESAPI.encoder().encodeForURL(JspUtils.getResolveVersion());

    boolean debug = false;
    String d = request.getParameter("debug");
    if( d != null && d.indexOf("t") > -1){
        debug = true;
        System.out.println("Debug true");
    }
    
    boolean debugImpex = false;
    String dT = request.getParameter("debugImpex");
    if( dT != null && dT.indexOf("t") > -1){
        debugImpex = true;
    }

    boolean mock = false;
    String m = request.getParameter("mock");
    if( m != null && m.indexOf("t") > -1){
        mock = true;
    }
%>

<%
    if( debug || debugImpex){
%>
<link rel="stylesheet" type="text/css" href="/resolve/impex/css/impex-all.css?_v=<%=ver%>"/>
<script type="text/javascript" src="/resolve/impex/js/impex-classes.js?_v=<%=ver%>"></script>
<%
    }else{
%>
<link rel="stylesheet" type="text/css" href="/resolve/impex/css/impex-all.css?_v=<%=ver%>"/>
<script type="text/javascript" src="/resolve/impex/js/impex-classes.js?_v=<%=ver%>"></script>
<%
    }
%>

