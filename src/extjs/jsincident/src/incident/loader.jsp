<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<%@page import="com.resolve.util.JspUtils" %>
<%@page import="org.owasp.esapi.ESAPI"%>
<%
    String ver = ESAPI.encoder().encodeForURL(JspUtils.getResolveVersion());

    boolean debug = false;
    String d = request.getParameter("debug");
    if( d != null && d.indexOf("t") > -1){
        debug = true;
    }

    boolean debugSecurity = false;
    String dS = request.getParameter("debugSecurity");
    if( dS != null && dS.indexOf("t") > -1){
        debugSecurity = true;
    }
%>
<script type="text/javascript" src="/resolve/js/amcharts/amcharts/amcharts.js?_v=<%=ver%>"></script>
<script type="text/javascript" src="/resolve/js/amcharts/amcharts/serial.js?_v=<%=ver%>"></script>
<script type="text/javascript" src="/resolve/js/amcharts/amcharts/pie.js?_v=<%=ver%>"></script>
<script type="text/javascript" src="/resolve/js/timezonedetect.js?_v=<%=ver%>"></script>
<%
    if( debug || debugSecurity ){
%>
<link rel="stylesheet" type="text/css" href="/resolve/incident/css/incident-all.css?_v=<%=ver%>"/>
<script type="text/javascript" src="/resolve/incident/js/incident-classes.js?_v=<%=ver%>"></script>
<%
    }else{
%>
<link rel="stylesheet" type="text/css" href="/resolve/incident/css/incident-all.css?_v=<%=ver%>"/>
<script type="text/javascript" src="/resolve/incident/js/incident-classes.js?_v=<%=ver%>"></script>
<%
    }
%>
