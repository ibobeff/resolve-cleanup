glu.defView('RS.incident.Dashboard.KPIViewSectionEntry', {
	minHeight: 290,
	margin : '2 2 2 2',
	overflowX: 'auto',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	items:[{
		xtype: 'chart',
		chartId: '@{chartId}',
		flex: 1,
		chartSettingsHandler: '@{chartSettingsHandler}',
		chartConfigs: '@{chartConfigs}',
		refreshMonitor: '@{refreshMonitor}'
	}],
	listeners: {
		afterrender: function () {
			var chart = this.down('chart');
			chart.setChartConfigs(chart.chartConfigs);
		}
	},
	chartConfigs: '@{chartConfigs}',
	setChartConfigs: function(config) {
		var chart = this.down('chart');
		chart.setChartConfigs(config);
	}
});
