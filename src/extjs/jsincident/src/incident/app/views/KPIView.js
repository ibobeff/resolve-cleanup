glu.defView('RS.incident.Dashboard.KPIView', {
	overflowY: 'auto',
	defaults: {
		flex: 1
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: '@{viewSections}',
	listeners: {
		listeners: {
			beforedestroy : '@{beforeDestroyComponent}'
		}
	}
});
