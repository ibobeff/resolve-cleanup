glu.defView('RS.incident.Dashboard.KPILayoutSection', {
	minHeight: 370,
	minWidth: 544,
	margin: '2 15 15 15',
	bodyPadding: '0 8 8 8',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	style: {
		borderWidth: '1px',
		borderColor: 'silver',
		borderStyle: 'solid'
	},
	dockedItems: [{
		xtype: 'toolbar',
		cls: 'actionBar rs-dockedtoolbar',
		margin: '5 5 0 5',
		items: ['->', {
			name: 'addKPIEntry',
			iconCls: 'rs-icon-button icon-plus-sign-alt',
			cls : 'rs-small-btn rs-btn-light'
		}, {
			iconCls: 'rs-social-button icon-remove-sign',
			name: 'remove',
			text: '',
			tooltip: '~~deleteSection~~'
		}]
	}],
	defaults: {
		flex: 1
	},
	items: '@{KPILayoutSectionEntries}'
});
