glu.defView('RS.incident.ReportTemplates', {
	padding : 15,
	displayName: '~~reportTemplates~~',
	name : 'reportTemplate',
	dockedItems : [{
		xtype : 'toolbar',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'	
		},
		items : ['new','copy','purge','|','goToDashboard']
	}],
	xtype : 'grid',
	cls : 'rs-grid-dark',
	store : '@{reportTemplateStore}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true,
		useWindowParams: false
	}, {
		ptype: 'pager'
	}], 
	columns: [{
		header: '~~name~~',
		dataIndex: 'uname',
		filterable: true,
		flex: 1,
	}, {
		header: '~~namespace~~',
		dataIndex: 'unamespace',
		flex: 1,
		filterable: true
	}, {
		header: '~~revision~~',
		dataIndex: 'uversion',
		flex: 0.5,
		filterable: true
	}].concat((function(columns) {
		Ext.each(columns, function(col) {
			if (col.dataIndex == 'sysCreatedOn') {
				col.hidden = false;
				col.width = 200,
				col.initialShow = true;
			}
			if (col.dataIndex == 'sysCreatedBy') {
				col.hidden = false;
				col.initialShow = true;
			}
			if (col.dataIndex == 'sysUpdatedOn') {
				col.hidden = false;
				col.initialShow = true;
			}
			if (col.dataIndex == 'sysUpdatedBy') {
				col.hidden = false;
				col.initialShow = true;
			}
			if (col.dataIndex == 'id') {
				col.flex = 1.5;
			}
		});
		return columns;
	})(RS.common.grid.getSysColumns())),
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},
	listeners: {
		editAction: '@{editTemplate}'
	}
})