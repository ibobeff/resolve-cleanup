glu.defView('RS.incident.Activities', {
	xtype: 'container',
	layout: 'anchor',
	items: [{
		itemId: 'playbook-activitiesgrid',
		hidden: '@{..isActivitiesView}',
		anchor: '100% 100%',
		xtype: 'grid',
		cls: 'rs-grid-dark sir-activities-grid',
		border: false,
		bodyBorder: false,
		modifiable: '@{..pSirInvestigationViewerActivitiesModify}',
		store: '@{activitiesStore}',

		//Icon Paths
		modIndicator: function() {
			return this.modifiable? '<img class="rs-playbook-indicator-img" src="/resolve/images/droparrow.png" style="float:right;width:12px;height:12px;position:relative;left:4px;">': '';
		},

		modIndicatorText:  function() {
			return this.modifiable? '<img class="rs-playbook-indicator-img" src="/resolve/images/edittsk_tsk_gray.gif" style="float:left;margin-left:5px;height:12px;">': '';
		},

		modIndicatorCalendar: function() {
			return this.modifiable? '<img class="rs-playbook-indicator-img" src="/resolve/images/dropcalendar.png" style="float:right;width:17px;height:15px;position:relative;left:7px;top:-1px">': '';
		},

		placeholderUserActIcon: '<span style=display:inline-block;height:10px;width:15px;float:left;position:relative;left:-5px;></span>',

		userActIcon: '<img src="/resolve/images/user_add.png" style="height: 15px;float:left;position:relative;left:-5px;">',

		//View Config
		viewConfig: {
			markDirty: false,
			plugins: {
				ptype: 'gridviewdragdrop',
				dropZone: {
					onNodeOver: function( nodeData, source, e, data ) {
						var rec = this.view.getRecord(nodeData);
						this.positionIndicator(nodeData, data, e);
						return (data.records[0] != rec && data.records[0].get('phase') == rec.get('phase'))? this.dropAllowed: this.dropNotAllowed;
					}
				}
			},
			stripeRows: false,
			getRowClass: function (record, rowIndex, rowParams, store) {
				if(!record.get('activityName') && !record.get('wikiName') && !record.get('wikiName') && !record.get('description')) {
					return 'rs-playbook-hide-row-expander';
				}
			}
		},

		//Grid Features
		features: [
			Ext.create('Ext.grid.feature.Grouping',{
				groupHeaderTpl: '{name}',
				onGroupClick: function(view, rowElement, groupName, e)  {
					// This is the workaround to fix the collapse issue on phase with html RBA-15274
					// Basically, the browser crashes on collapsing the phase with html. Subsequent clicks on the
					// 'Add' (activity) button causes black screen
					Ext.grid.feature.Grouping.prototype.onGroupClick.apply(this, [view, rowElement, Ext.htmlEncode(groupName), e]);
				}
			})
		],

		//features: [{
		//	ftype: 'grouping',
		//	groupHeaderTpl: '{name}',
		//}],

		//Grid Plugins
		plugins: [/*{
			ptype: 'rowexpander',
			columnWidth: 32,
			rowBodyTpl : ['<div style=padding-top:0px;word-wrap:break-word;><span style=font-weight:bold;>Description: </span>{description}</div>']
		}, */{
			ptype: 'cellediting',
			clicksToEdit: 1,
			listeners: {
				beforeedit: function(editor, context, eOpts) {
					if (context.record.get('isPreview')) {
						// preview activities are not modifiable
						return false;
					} else if (!context.grid.modifiable) {
						return false;
					} else if (context.colIdx == 0 &&
						(context.record.get('templateActivity') ||  // Template activities are not modifiable
								window.event && window.event.srcElement && window.event.srcElement.tagName.toLowerCase() == 'a')) {
						return false; // Don't want clicking on the link to enter edit mode
					}
					context.grid.editingContext = context; // Saving editing context to be used later (in controlling the template data from
					return (Ext.firefoxVersion !== 0)? (!context.record.get('templateActivity') ||  context.field != 'activityName'): true;
				}
			}
		}],

		//Docked Toolbar
		dockedItems: [{
			xtype: 'toolbar',
			cls: 'rs-dockedtoolbar',
			dock: 'top',
			disabled: '@{activityToolBarDisabled}',
			items: [{
				xtype: 'button',
				disabled: '@{!..pSirInvestigationViewerActivitiesCreate}',
				cls: 'rs-small-btn rs-btn-light',
				name: 'addActivity',
				maxWidth: 100
			}, {
				xtype: 'tbseparator'
			},
			{
				xtype: 'checkboxgroup',
				height: 24,
				width: 400,
				fieldLabel: 'Show',
				labelWidth: 50,
				columns: 4,
				vertical: false,
				defaultType : 'checkbox',
				items: [
					{checked: true, boxLabel: 'Open', inputValue: 'Open', width: 50},
					{checked: true, boxLabel: 'In Progress', inputValue: 'In Progress', width: 110},
					{checked: true, boxLabel: 'Complete', inputValue: 'Complete', width: 95},
					{checked: true, boxLabel: 'N/A', inputValue: 'N/A', width: 50}
				],
				listeners: {
					change: '@{handleFilterChange}'
				}
			}, '->', {
				xtype: 'tbtext',
				text: 'Progress',
				style: {
					fontWeight: 'bold'
				}
			}, {
				xtype: 'progressbar',
				itemId: 'playbook-progressBar',
				height: 12,
				width: 250,
				value : '@{playbookProgress}',
				listeners: {
					afterrender: function(progressBar) {
						this._vm.on('updateProgressBar', function(newProgress) {
							progressBar.updateProgress(newProgress);
						})
					}
				}
			}]
		}],

		columns: [{
			dataIndex: 'activityName',
			text: '~~activity~~',
			flex: 2,
			sortable: false,
			groupable: false,
			draggable: false,
			hideable: false,
			editor: {
				xtype: 'textfield',
				allowBlank: false,
				listeners: {
					focus: function(editor) {
						editor.focus();
					}
				}
			},
			renderer: function(value, meta, record) {
				if (record.getData().id === null) {
					return value
				} else {
					var vm = this._vm;
					var id = Ext.id();
					/*Ext.defer(function () {
						//Make sure this el still exist. TEMP fix.
						var el = document.getElementById(id);
						if(!el)
							return;
						Ext.widget('button', {
							renderTo: id,
							tooltip: vm.localize('moreTip'),
							iconCls : 'icon-ellipsis-horizontal',
							cls : ' sir-action-icon',
							menu: {
								xtype: 'menu',
								items: [{
									text: vm.localize('addNotes'),
									handler: function(){
										vm.addNotes();
									}
								}, {
									text: vm.localize('addArtifacts'),
									handler: function(){
										vm.addArtifacts();
									}
								}, {
									text: vm.localize('addAttachments'),
									handler: function() {
										vm.addAttachments();
									}
								}, {
									xtype: 'menuseparator'
								}, {
									text: vm.localize('viewAutomationResults'),
									handler: function() {
										vm.viewAutomationResults();
									}
								}],
								listeners: {
									mouseleave: function() {
										this.hide();
									}
								}
							},
						});
					}, 500);*/
					var actionBtn = Ext.String.format('<span style="float:right;position:relative;left:4px;" id="{0}"></span>', id)
					var isTemplateActivity = record.get('templateActivity');
					// Double-encoding is needed for qtip
					// User-added activities are already encoded by the backend so they are only needed to encode once more.
					var qTip = isTemplateActivity? Ext.htmlEncode(Ext.htmlEncode(record.get('description'))) : Ext.htmlEncode(record.get('description'));
					meta.tdAttr = 'data-qtip="' + qTip + '"';
					if (isTemplateActivity) {
						var href = '';
						if (record.get('isPreview')) {
							href = 'javascript:void(0)';
						} else {
							href =  Ext.String.format('#RS.incident.Playbook/sir={0}&activityId={1}', this._vm.parentVM.ticketInfo.sir, record.get('id'));
						}
						return Ext.String.format(
							'<div class="sir-activity-value">{0}<a style="float:left" class="sc-link rs-activityname-link" href="'+href+'">{1}</a>{2}</div>',
							this.placeholderUserActIcon,
							value,
							actionBtn
						)
					} else {
						meta.tdCls += 'rs-playbook-modindicator';
						return Ext.String.format(
							'<div class="sir-activity-value">{0}<a style="float:left" class="sc-link rs-activityname-link" href="#RS.incident.Playbook/sir={1}&activityId={2}">{3}</a>{4}{5}</div>',
							this.userActIcon,
							this._vm.parentVM.ticketInfo.sir,
							record.get('id'),
							record.activityNameDirty? Ext.htmlEncode(value): value, // Encode if not done so by backend
							this.modIndicatorText.call(this),
							actionBtn
						)
					}
				}
			}
		}, {
			dataIndex: 'days',
			text: '~~sla~~',
			flex: 1,
			sortable: false,
			groupable: false,
			draggable: false,
			hideable: false,
			renderer: function(val, meta, record) {
				var days = parseInt(record.getData().days) || 0;
				var hours = parseInt(record.getData().hours) || 0;
				var minutes = parseInt(record.getData().minutes) || 0;
				if ((days + hours + minutes) === 0) {
					return 'None'
				} else {
					var returnStrArr = [];
					if (days !== 0) {
						returnStrArr.push((days === 1) ? days + ' Day' : days + ' Days');
					}
					if (hours !== 0) {
						returnStrArr.push((hours === 1) ? hours + ' Hour' : hours + ' Hours');
					}
					if (minutes !== 0) {
						returnStrArr.push((minutes === 1) ? minutes + ' Minute' : minutes + ' Minutes');
					}
					return returnStrArr.join(' ')
				}
			}
		}, {
			dataIndex: 'dueDate',
			text: '~~dueBy~~',
			flex: 1,
			sortable: false,
			groupable: false,
			draggable: false,
			hideable: false,
			editor: {
				xtype: 'datetimefield',
				allowBlank: true,
				//minValue : new Date(),
				editable: false,
				format: Ext.state.Manager.get('userDefaultDateFormat', 'Y-m-d H:i:s'),
				submitFormat: 'Y-m-d',
				listeners: {
					focus: function(editor) {
						editor.expand();
					},
					select: function(editor) {
						editor.findParentByType('editor').completeEdit();
					},
					deactivate: function(editor) {
						editor.findParentByType('editor').completeEdit();
					}
				}
			},
			renderer: function(field, meta, record) {
				meta.tdCls += 'rs-playbook-modindicator';
				return Ext.String.format('<span style="float:left">{0}</span>{1}', field, record.get('isPreview') ? '' : this.modIndicatorCalendar.call(this));
			}
		}, {
			dataIndex: 'assignee',
			text: '~~assignee~~',
			flex: 1,
			sortable: false,
			groupable: false,
			draggable: false,
			hideable: false,
			editor: {
				xtype: 'combobox',
				allowBlank: true,
				displayField: 'userId',
				valueField: 'userId',
				store: '@{membersStoreGrid}',
				queryMode : 'local',
				editable: false,
				listeners: {
					focus: function(editor) {
						editor.expand();
					},
					select: function(editor) {
						editor.findParentByType('editor').completeEdit();
					}
				}
			},
			renderer: function(field, meta, record) {
				meta.tdCls += 'rs-playbook-modindicator';
				return Ext.String.format('<span style="float:left">{0}</span>{1}', field, record.get('isPreview') ? '' : this.modIndicator.call(this));
			}
		}, {
			dataIndex: 'status',
			text: '~~status~~',
			flex: 1,
			sortable: false,
			groupable: false,
			draggable: false,
			hideable: false,
			editor: {
				xtype: 'combobox',
				allowBlank: true,
				displayField: 'type',
				valueField: 'type',
				store: '@{statusStore}',
				queryMode : 'local',
				editable: false,
				listeners: {
					focus: function(editor) {
						editor.expand();
					},
					select: function(editor) {
						editor.findParentByType('editor').completeEdit();
					}
				}
			},
			renderer: function(field, meta, record) {
				meta.tdCls += 'rs-playbook-modindicator';
				return Ext.String.format('<span style="float:left">{0}</span>{1}', field, record.get('isPreview') ? '' : this.modIndicator.call(this));
			}
		}],
		listeners: {
			activate: '@{tabInit}',
			edit: '@{handleActivitiesEdit}',
			//Drag & Drop functionality
			afterrender: function() {
				this.view.on('beforedrop', function(node, data, dropRec, dropPosition, dropHandlers) {
					var store = dropRec.store,
						insertB4 = dropPosition == 'before',
						droppable = !data.records[0].get('templateActivity') && (data.records[0].get('phase') == dropRec.get('phase')); // Template's activity can't be modified
					if (droppable) {
						var recB4DropRec = store.getAt(store.indexOf(dropRec)-1);
						if (insertB4 && (!recB4DropRec || recB4DropRec.get('phase') !== data.records[0].get('phase'))) {
							// We have to manually do two insertions after to get around the issue of grouping if inserting before the leading node.
							store.remove(data.records, true);
							store.insert(store.indexOf(dropRec) + 1, data.records);
							store.remove(dropRec, true);
							store.insert(store.indexOf(data.records[0]) + 1, dropRec);
							this.getSelectionModel().select(data.records);
							dropHandlers.cancelDrop();
							store.fireEvent('recordmoved', data.records[0], data.records[0], dropPosition);
						}
					}
					return !!droppable;
				});
				this.view.on('drop', function(node, data, overModel, dropPosition, eOpts) {
					overModel.store.fireEvent('recordmoved', data.records[0], data.records[0], dropPosition);
				});
			},
			focuschange: '@{focuschange}',
			itemmouseenter: '@{itemmouseenter}'
		}
	}, {
		xtype: '@{activitiesView}',
		anchor: '100% 93%',
		itemId: 'playbook-activitiesview',
		style: {
			fontWeight: 'bold',
		},
		hidden: '@{!..isActivitiesView}'
	}]
});


glu.defView('RS.incident.ActivityModal', {
	itemId: 'playbook-addactivity',
	padding : 15,
	title: '~~addActivityModal~~',
	cls : 'rs-modal-popup',
	height: 500,
	width: 800,
	modal: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults : {
		margin : '4 0'
	},
	items: [{
		xtype: 'combo',
		fieldLabel: '~~phase~~',
		name: 'phase',
		displayField: 'type',
		valueField: 'type',
		store: '@{phasesStore}',
		queryMode: 'local',
		allowBlank: false
	}, {
		xtype: 'textfield',
		fieldLabel: '~~name~~',
		name: 'name',
		maxLength: 200,
		enforceMaxLength: true,
		allowBlank: false
	}, {
		xtype: 'container',
		layout: {
			type: 'hbox',
		},
		items: [{
			xtype: 'spinnerfield',
			fieldLabel: '~~sla~~',
			value: '@{slaDays}',
			allowBlank: false,
			width: 175,
			step: 1,
			listeners: {
				spinUp: function(spinner) {
					var value = parseInt(spinner.getValue() || 0);
					spinner.setValue((value + 1).toString());
				},
				spinDown: function(spinner) {
					var value = parseInt(spinner.getValue() || 0);
					if (value > 0) {
						spinner.setValue((value - 1).toString());
					}
				},
				change: function(spinner, newVal) {
					var value = parseInt(newVal) || 0;
					if (value < 0) {
						spinner.setValue('0');
					} else {
						spinner.setValue(value);
					}
				}
			}
		}, {
			xtype: 'label',
			height: 26,
			width: 43,
			padding: '5 0 0 10',
			text: '@{calcDaysText}'
		}, {
			xtype: 'spinnerfield',
			margin: '0 0 0 20',
			value: '@{slaHours}',
			allowBlank: false,
			hideLabel: true,
			width: 60,
			step: 1,
			listeners: {
				spinUp: function(spinner) {
					var value = parseInt(spinner.getValue() || 0);
					if (value < 23) {
						spinner.setValue((value + 1).toString());
					}
				},
				spinDown: function(spinner) {
					var value = parseInt(spinner.getValue() || 0);
					if (value > 0) {
						spinner.setValue((value - 1).toString());
					}
				},
				change: function(spinner, newVal) {
					var value = parseInt(newVal) || 0;
					if (value < 0) {
						spinner.setValue('0');
					} else if (value > 23) {
						spinner.setValue('23');
					} else {
						spinner.setValue(value);
					}
				}
			}
		}, {
			xtype: 'label',
			height: 26,
			width: 49,
			padding: '5 0 0 10',
			text: '@{calcHoursText}'
		}, {
			xtype: 'spinnerfield',
			margin: '0 0 0 20',
			value: '@{slaMinutes}',
			regex: /[0-9]+/,
			allowBlank: false,
			hideLabel: true,
			width: 60,
			step: 1,
			listeners: {
				spinUp: function(spinner) {
					var value = parseInt(spinner.getValue() || 0);
					if (value < 59) {
						spinner.setValue((value + 1).toString());
					}
				},
				spinDown: function(spinner) {
					var value = parseInt(spinner.getValue() || 0);
					if (value > 0) {
						spinner.setValue((value - 1).toString());
					}
				},
				change: function(spinner, newVal) {
					var value = parseInt(newVal) || 0;
					if (value < 0) {
						spinner.setValue('0');
					} else if (value > 59) {
						spinner.setValue('59');
					} else {
						spinner.setValue(value);
					}
				}
			}
		}, {
			xtype: 'label',
			height: 26,
			width: 61,
			padding: '5 0 0 10',
			text: '@{calcMinutesText}'
		}]
	}, {
		xtype: 'combo',
		editable: false,
		fieldLabel: 'Status',
		name: 'status',
		displayField: 'type',
		valueField: 'type',
		store: '@{..statusStore}',
		queryMode: 'local',
		allowBlank: false
	}, {
		xtype: 'combo',
		editable: false,
		fieldLabel: 'Assignee',
		name: 'assignee',
		displayField: 'userId', //display name/userId
		valueField: 'userId',
		store: '@{..membersStore}',
		queryMode: 'local',
		allowBlank: false
	}, {
		xtype: 'documentfield',
		itemId: 'wikiNameField',
		name: 'wikiName',
		store: '@{runbookStore}',
		allowBlank: false,
		validateBlank: true,
		displayField: 'ufullname',
		valueField: 'ufullname',
		maxLength: 2000,
		enforceMaxLength: true,
		editable: true,
		inline: false,  // NOT and inline editing
		width: '100%',
		listeners: {
			searchdocument: '@{searchRunbook}',
			select : '@{handleRunbookSelect}',
			validatewikiname: '@{validateWikiName}',
			blur: function(v) {
				v.fireEvent('validatewikiname', this, v)
			}
		}
	}, {
		xtype: 'textarea',
		fieldLabel: 'Description',
		name: 'description',
		flex : 1,
		margin : '4 0 10 0'
	}],
	buttons: [{
		name: 'saveActivity',
		cls : 'rs-med-btn rs-btn-dark',
		disabled: '@{isSaving}'
	}, {
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})