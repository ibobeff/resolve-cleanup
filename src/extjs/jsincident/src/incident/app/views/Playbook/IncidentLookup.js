glu.defView('RS.incident.IncidentLookup',{
	xtype : 'container',
	padding : 15,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	items : [{
		xtype : 'displayfield',
		htmlEncode: false,	
		value : '@{lookupInfo}',	
	},{
		xtype : 'grid',
		flex : 1,
		dockedItems: [{
			xtype: 'toolbar',		
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',			
			items: []
		}],
		cls : 'rs-grid-dark',	
		plugins: [{
			ptype: 'pager'
		}],
		columns : [{
			dataIndex: 'sir',
			text: '~~id~~',
			sortable: true,
			filterable: true,
			flex:.9,
			renderer: function(value, meta, record) {
				if (value) {
					return Ext.String.format('<a target="_blank" class="sc-link" href="/resolve/jsp/rsclient.jsp{0}#RS.incident.Playbook/sir={1}">{2}</a>', 
						record.store.rootVM.rootcsrftoken, record.get('sir'), record.get('sir'));
				}
				return ''
			}
		},{
			dataIndex: 'playbook',
			text: '~~playbook~~',
			sortable: true,
			filterable: true,
			flex: 1.1			
		},{
			dataIndex: 'title',
			text: '~~title~~',
			sortable: true,
			filterable: true,
			flex: 1.2
		},{
			dataIndex: 'sourceSystem',
			text: '~~origin~~',
			sortable: true,
			filterable: true,
			hidden: true,
			flex: .8
		},{
			dataIndex: 'id',
			text: RS.common.locale.sysId,
			sortable: true,
			hidden: true,
			sortable: false,
			filterable: false,
			flex: .8
		},{
			dataIndex: 'investigationType',
			text: '~~type~~',
			sortable: true,
			filterable: true,
			flex: .8,
		},{
			dataIndex: 'severity',
			text: '~~severity~~',
			sortable: true,
			filterable: true,
			flex: .5
		},{
			dataIndex: 'status',
			text: '~~status~~',
			sortable: true,
			filterable: false,
			flex: .6,
		},{
			dataIndex: 'dueBy',
			text: '~~dueBy~~',
			sortable: true,
			filterable: true,
			flex: .6,
			renderer: //Ext.util.Format.dateRenderer('Y-m-d H:i:s')
			function(val) {
				return (val? Ext.util.Format.dateRenderer('Y-m-d')(new Date(val)): val);
			}
		},{
			dataIndex: 'owner', //display owner/ownerName
			text: '~~owner~~',
			sortable: true,
			filterable: true,
			flex: 1
		},{
			dataIndex: 'sysCreatedOn',
			text: '~~createdOn~~',
			renderer: function(value) {
				var formated = Ext.util.Format.date(new Date(value), clientVM.getUserDefaultDateFormat());
				return formated;
			},
			sortable: true,
			hidden: false,
			filterable: true,
			flex: 1
		},{
			dataIndex: 'closedOn',
			text: '~~closedOn~~',
			sortable: true,
			filterable: true,
			flex: 1,
			renderer: function(value) {
				return value ? Ext.util.Format.date(new Date(value), clientVM.getUserDefaultDateFormat()) : '';
			}
		},{
			dataIndex: 'closedBy',
			text: '~~closedBy~~',
			sortable: true,
			filterable: true,
			flex: 1
		}],
		store : '@{incidentStore}',
	}]
})
glu.defView('RS.incident.IncidentLookupPopup',{
	title : '@{title}',
	cls : 'rs-modal-popup',		
	width : 700,
	plugins : [{
		ptype: "headericons",
	  	index : 1,
        headerButtons : [
            {
                xtype: 'button',
                iconCls: 'icon-external-link',
                cls : 'sir-external-link-header-icon', 
                handler: function(){
                	var vm = this.up().up()._vm;
                	vm.expandResultInTab();
                },
                listeners : {
                	render : function(){
                		this.setTooltip(glu.localize('expandResultInTab', {ns : 'RS.incident'}));
                	}
                }               	              
            }
        ]
    }],
	padding : 15,
	items : [{
		xtype : 'component',
		html : '@{noRecordFoundText}',
		hidden : '@{!noRecordFound}'
	},{
		xtype : 'displayfield',
		value : '@{lookupInfo}',
		hidden : '@{noRecordFound}',
		htmlEncode: false
	},{
		xtype : 'grid',
		dockedItems: [{
			xtype: 'toolbar',		
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',			
			items: []
		}],
		cls : 'rs-grid-dark',
		hidden : '@{noRecordFound}',
		plugins: [{
			ptype: 'pager',
			showSysInfo : false
		}],
		columns : [{
			dataIndex : 'sir',
			text: '~~id~~',
			width : 150,
			renderer: function(value, meta, record) {
				if (value) {
					return Ext.String.format('<a target="_blank" class="sc-link" href="/resolve/jsp/rsclient.jsp?{0}#RS.incident.Playbook/sir={1}">{2}</a>', 
						record.store.rootVM.rootcsrftoken, record.get('sir'), record.get('sir'));
				}
				return ''
			}
		},{
			dataIndex : 'title',
			text: '~~title~~',
			flex : 1
		},{
			dataIndex : 'investigationType',
			text: '~~type~~',
			width : 150
		},{
			dataIndex : 'severity',
			text: '~~severity~~',
			align : 'center',
			width : 80
		},{
			dataIndex : 'status',
			text: '~~status~~',
			align : 'center',
			width : 80
		}],
		store : '@{incidentStore}',
	}]	
})