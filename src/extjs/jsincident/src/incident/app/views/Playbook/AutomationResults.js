glu.defView('RS.incident.AutomationResults',{
	layout : 'fit',
	dockedItems : [{
		xtype : 'toolbar',
		cls : 'rs-dockedtoolbar',
		items : [{
			xtype : 'combo',
			name : 'activityFilter',
			editable : false,
			labelWidth : 150,
			width : 800,		
			labelStyle : 'font-weight:bold',
			store : '@{..activityFilterStore}',
			queryMode : 'local',
			displayField : 'activityName',
			valueField : 'activityName',
			hidden: '@{isModal}',
		},'->',{		
			iconCls: 'rs-social-button icon-repeat',
			name : 'refresh',
			text : '',
			tooltip: 'Refresh',
		}]
	},{
		dock : 'bottom',
		xtype : 'toolbar',
		hidden: '@{!isModal}',
		items : [{
	        text : '~~close~~',
	        cls : 'rs-med-btn rs-btn-light',		
	        handler : '@{close}',
	    }]
	}],
	items : '@{resultList}',
	listeners : {
		activate: '@{tabInit}'
	},
	padding: '@{padding}',
	asWindow: {
		minHeight: 400,
		minWidth: 700,
		title: '@{modalTitle}',
		modal: '@{modal}',
		cls :'rs-modal-popup',
		listeners: {
			beforeshow: function() {
				this.setWidth(Math.round(Ext.getBody().getWidth() * 0.6));
				this.setHeight(Math.round(Ext.getBody().getHeight() * 0.6));
			}
		}
	}	
})