glu.defView('RS.incident.UploadAttachment', {
	parentLayout: 'FileUploadManager',
	propertyPanel: {
		listeners: {
			beforerender : function(){
				var me = this;
				var toolbar = this.down('button[name="uploadFile"]').ownerCt;
				var deleteBtn = this.down('button[name="delete"]');
				var reloadBtn = this.down('button[name="reload"]');
				deleteBtn.hide();
				reloadBtn.hide();
				
				var attachments_grid = this.down('#attachments_grid');
				attachments_grid.hide();

				var items = this.down('#fineUploaderTemplate').ownerCt;
				var fineuploader = this.down('#fineUploaderTemplate');

				items.add([{
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					margin: '4 0',
					items: [{
						xtype: 'textarea',
						name: 'description',
						fieldLabel: RS.incident.locale.description,
						rows: 5,
						//grow: true,
						//growMax: 60,
						emptyText: RS.incident.locale.emptyAttachmentDescription,
						listeners: {
							blur: function() {
								me.fireEvent('descriptionchanged', this, this.value);
							}
						}
					}, {
						xtype: 'checkbox',
						name: 'malicious',
						hideLabel: true,
						boxLabel: RS.incident.locale.malicious,
						margin: '0 0 0 104',
						handler: function() {
							me.fireEvent('maliciouschanged', this, this.checked);
						}
					}]
				}]);

				var buttons = this.down('#cancelBnt').ownerCt;
				buttons.insert(0, {
					name: 'submit',
					text: RS.incident.locale.submit,
					cls: 'rs-med-btn rs-btn-dark',
					handler: function() {
						me.fireEvent('submit', this)
					}
				});

				this.on({
					render: function() {
						this.down('button[name="submit"]').disable();
					},
					enableSubmitBtn: function() {
						this.down('button[name="submit"]').enable();
					},
					disableSubmitBtn: function() {
						this.down('button[name="submit"]').disable();
					},
					afterrender: function() {
						 this.fireEvent('afterRendered', this, this);
					}
				});
			},
			submit: '@{submit}',
			afterRendered: '@{viewRenderCompleted}',
			maliciouschanged: '@{maliciousChanged}',
			descriptionchanged: '@{descriptionChanged}',
		}
	}
});

glu.defView('RS.incident.dummy');
RS.incident.views.FileUploadManagerFactory = function(subView) {
	var config = RS.common.views.FileUploadManagerFactory();
	if (subView && subView.propertyPanel) {
		Ext.applyIf(config, subView.propertyPanel);
	}
	return config;
};

/*
glu.defView('RS.incident.UploadAttachment', {
    xtype: 'panel',
	incidentId: '@{incidentId}',
    getConfig: function() {
        return {
            autoStart: false,
            url: '/resolve/service/playbook/attachment/upload',
            browse_button: this.getBrowseButton(),
            drop_element: this.getDropArea(),
            container: this.getContainer(),
            chunk_size: null,
			max_file_size: '2020mb',
            runtimes: "html5,flash,silverlight,html4",
            flash_swf_url: '/resolve/js/plupload/Moxie.swf',
            unique_names: true,
            multipart: true,
            multipart_params: {},
            multi_selection: true,
            required_features: null,
            ownerCt: this
        }
    },
    getBrowseButton: function() {
        //return this.up().down('#browse').btnEl.dom.id;
		return this.down('#browse').getEl().dom.id;
    },
    getDropArea: function() {
        return this.down('#dropzone').getEl().dom.id;
    },
    getContainer: function() {
        var btn = this.down('#browse');
        return btn.ownerCt.id;
    },
    uploaderListeners: {
        beforestart: function(uploader, data) {
			//console.log('beforestart');
			this.owner.fireEvent('beforeStart', this.owner, uploader, data);
        },
        // uploadready: function(uploader, data) {
        //     var me = uploader.uploaderConfig.ownerCt;
        //     var inputs = Ext.query('#' + me.getEl().id + ' input[type="file"]');
        //     var file = inputs[0];
        //     var runtime = mOxie.Runtime.getRuntime(file.id);
        //     me.fireEvent('runtimedecided', null, runtime);
        // },
        uploadstarted: function(uploader, data) {},
        uploadcomplete: function(uploader, data) {
			//console.log('uploadcomplete')
            this.owner.fireEvent('uploadComplete', this.owner, uploader, data);
        },
        uploaderror: function(uploader, data) {
			//console.log('uploaderror')
            this.owner.fireEvent('uploadError', this.owner, uploader, data);
        },
        filesadded: function(uploader, data) {
			//console.log('filesadded');
            this.owner.fireEvent('filesAdded', this.owner, uploader, data);
        },
        beforeupload: function(uploader, data) {
            //console.log('beforeupload')
        },
        fileuploaded: function(uploader, data, response){
			this.owner.fireEvent('fileUploaded', this.owner, uploader, data, response);
		},
        uploadprogress: function(uploader, file, name, size, percent) {
            //this.owner.percent = file.percent;
			this.owner.fireEvent('uploadProgress', this.owner, name, size, percent);
        },
		uploadstopped: function(uploader, data) {
            //console.log('uploadstopped')
        },
        storeempty: function(uploader, data) {
            //console.log('storeempty')
        }
    },

    listeners: {
        uploadError: '@{uploadError}',
        filesAdded: '@{filesAdded}',
        uploadComplete: '@{uploadComplete}',
        runtimedecided: '@{runtimeDecided}',
        afterrender: function() {
			this.fireEvent('afterRendered', this, this);
            this.myUploader = Ext.create('RS.common.SinglePlupUpload', this);
            this.uploader = this.getConfig();
            this.myUploader.initialize(this);
        },
		afterRendered: '@{viewRenderCompleted}',
		beforeStart: '@{beforeStart}',
		startUpload: function(malicious, description) {
			Ext.apply(this.uploader.multipart_params, {
				docFullName: this.incidentId,
				malicious: malicious,
				description: description
            })
			this.myUploader.start();
		},
		uploadProgress: '@{uploadProgress}',
		cancelUpload: function() {
			this.myUploader.stop();
		},
		clearQueue: function() {
			this.myUploader.clearQueue();
		}
    },
  
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
	overflowY: 'auto',
    defaults : {
        margin : '4 0'
    },
    items: [{
		html: '@{busyText}',
		hidden: '@{!uploading}'
	}, {
		hidden: '@{uploading}',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			layout: {
				type: 'hbox',
				//align: 'stretch'
			},
			items: [{
				xtype: 'textarea',
				name: 'filenames',
				emptyText: '~~emptyFilename~~',
				flex: 4,
				readOnly: true,
				grow: true,
				growMin: 24,
				growPad: -8,
				growAppend: '',
				growMax: 90,
			}, {
				xtype: 'button',
				name: 'browse',
				itemId: 'browse',
				flex: 1,
				margin: '0 4',
				text: '~~browse~~',
				listeners: {
					hide: function() {
						this.up().el.query('.moxie-shim')[0].style.width = '0px';
					},
					show: function() {
						this.up().el.query('.moxie-shim')[0].style.width = '100%'
					}
				}
			}]
		}, {
			xtype: 'textarea',
			name: 'description',
			rows: 2,
			grow: true,
			growMax: 90,
			emptyText: '~~emptyAttachmentDescription~~'
		}, {
			xtype: 'checkbox',
			hideLabel: true,
			boxLabel: '~~malicious~~',
			value: '@{malicious}',
			margin: '0 0 0 104'
		}, {
			xtype: 'form',
			itemId: 'dropzone',
			style: 'border: 2px dashed #ccc;margin: 10px 0px 0px 0px',
			flex: 1,
			height: 155,
			margin : '4 0',
			layout: {
				type: 'vbox',
				pack: 'center',
				align: 'center'
			},
			items: [{
				xtype: 'label',
				text: '~~dragHere~~',
				style: {
					'font-size': '30px',
					'color': '#CCCCCC'
				}
			}]
		}]
	}],

    asWindow: {
        width: 600,
        height: 400,
        padding : 15,
        title: '~~addAttachment~~',
        cls : 'rs-modal-popup',
        modal: true,
        closable: false
    },  
    buttons: [{
        name : 'submit',
        cls : 'rs-med-btn rs-btn-dark',
		hidden: '@{uploading}',
    }, {
        name : 'cancel',
        cls : 'rs-med-btn rs-btn-light',
		hidden: '@{!uploading}'
    }, {
        name :'close',
        cls : 'rs-med-btn rs-btn-light',
		disabled: '@{uploading}'
	}]
})
*/
