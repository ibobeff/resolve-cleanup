// This module renders Playbook builder's content and can be extended to render wiki page builder content
Ext.define('RS.incident.PageViewer', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.pageviewer',

	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	overflowY: 'auto',
	csrftoken: '',

	initComponent: function() {
		this.callParent(arguments);
		clientVM.getCSRFToken_ForURI('/resolve/service/wiki/download', function(token_pair) {
			this.csrftoken = token_pair[0] + '=' + token_pair[1];
		}.bind(this));
	},

	renderContent: function(v, content, ufullName) {
		var re = /(\{section[^\}]*\})([\s\S]*?)\{section\}/gmi, match, secProp, viewConfigs = [], secHeight, config;
		var fullName = ufullName.split('.');
		while (match = re.exec(content)) {
			secProp = this.getProperties(match[1]);
			config = {
				xtype: 'panel',
				cls: 'playbook-desc-view',
				width: '100%',
				layout:{
					type: 'hbox',
					align: 'stretch'
				},
				items: []
			};
			if (secProp.height) {
				config.height = (secProp.height.search('%') == -1)? parseInt(secProp.height): secProp.height;
			} else {
				config.flex = 1;
			}
			// START TEST
			if ((match[1].search('procedure') != -1) || (match[1].search('result') != -1)) {
				// Skip procedure and result sections
				continue;
			}
			// END TEST
			this.renderSection(config, match[2], fullName);
			viewConfigs.push(config);
		}
		if (viewConfigs) {
			v.removeAll();
			v.add(viewConfigs);
		}
	},

	renderSection: function(secConfigs, sec, fullName) {
		var re = /(<td.*class="wiki-builder">)([\s\S]*?)<\/td class="wiki-builder">/gmi, match, prop, content, table;
		var tableTagRegex = new RegExp(RS.wiki.pagebuilder.Constant.TABLETAG, 'gmi');
		if (table = tableTagRegex.exec(sec)) {
			while(match = re.exec(sec)) {
				this.renderComponent(secConfigs, match[2], match[1], fullName);
			}
		} else {
			this.renderComponent(secConfigs, sec, null, fullName);
		}
	},

	renderComponent: function(secConfigs, comContent, tdTag, fullName) {
		var content, tdTagConfigs;
		if (tdTag) {
			tdTagConfigs = this.getProperties(tdTag);
		}
		if(content = /<novelocity.*>([\s\S]*)<\/novelocity>/gmi.exec(comContent)) {
			this.renderTxtComponent(secConfigs, tdTagConfigs, content[1], fullName);
		} else if (content = /(\{image[^\}]*\})/gmi.exec(comContent)) {
			this.renderImgComponent(secConfigs, tdTagConfigs, content[1], fullName);
		}
	},

	getComTemplate: function(type, content, fullName) {
		var item, t, url;
		if (type == 'image') {
			item = {
				xtype: 'image',
				src: Ext.String.format('/resolve/service/wiki/download?wiki={0}.{1}&attachFileName={2}&{3}', fullName[0], fullName[1], content, this.csrftoken),
				style: 'border:0px solid black;'
			}
		} else if (type == 'texteditor') {
			item = {
				xtype: 'panel',
				html: content,
				flex: 1
			}
		}
		t = {
				xtype: 'panel',
				layout: 'auto',
				items: [item]
		};
		return t;
	},

	renderTxtComponent: function(secConfigs, colConfigs, content, fullName) {
		var txtViewConfig =  this.getComTemplate('texteditor', content, fullName);
		if (colConfigs && colConfigs.style) {
			var style = this.extractColumnStyleInfo(colConfigs.style);
			txtViewConfig.flex = (parseInt(style.width)/100)*100;
			txtViewConfig.layout = {
				type: 'hbox',
				align: style['vertical-align']
			}
		} else {
			txtViewConfig.flex = 1;
		}
		secConfigs.items.push(txtViewConfig);
	},

	renderImgComponent: function(secConfigs, colConfigs, content, fullName) {
		var img = content.split('|')[0].split(':')[1].split('}')[0],
			imgProp = this.getProperties(content),
			imgViewConfig =  this.getComTemplate('image', img, fullName),
			item = imgViewConfig.items[0];
		if (colConfigs && colConfigs.style) {
			var style = this.extractColumnStyleInfo(colConfigs.style);
			imgViewConfig.flex = (parseInt(style.width)/100)*100;
			imgViewConfig.layout = {
				type: 'hbox',
				align: style['vertical-align']
			}
		} else {
			imgViewConfig.flex = 1;
		}

		if(!imgProp || !imgProp.width || !imgProp.height
			|| typeof(imgProp.width) == 'string' || typeof(imgProp.height) == 'string') {
			item.listeners = {
				afterrender: function(v) {
					var img = new Image();
					img.src = item.src;
					img.onload = function() {
						var setHeightOrWidth = function(v, unit, actualHeightOrWidth, type) {
							if (unit.search('%') != -1) {
								// Set as percentage
								v['set'+type].bind(v)(actualHeightOrWidth*parseInt((unit))/100);
							} else {
								// Set actual size
								v['set'+type].bind(v)(parseInt(unit));
							}
						}
						if (!imgProp || (imgProp && !imgProp.height && !imgProp.width)) {
							// Neither height and width is configured; set natural size
							v.setHeight(img.naturalHeight);
							v.setWidth(img.naturalWidth);
						} else if (imgProp && imgProp.height && imgProp.width) {
							// Both height and width are configured
							setHeightOrWidth(v, imgProp.height, img.naturalHeight, 'Height');
							setHeightOrWidth(v, imgProp.width, img.naturalWidth, 'Width');
						} else if (imgProp && imgProp.height && !imgProp.width) {
							// Only height is configured
							setHeightOrWidth(v, imgProp.height, img.naturalHeight, 'Height');
							v.setWidth(img.naturalWidth);
						} else if (imgProp && !imgProp.height && imgProp.width) {
							// Only width is configured
							v.setHeight(img.naturalHeight);
							setHeightOrWidth(v, imgProp.width, img.naturalWidth, 'Width');
						}
					}
				}
			}
		}
		secConfigs.items.push(imgViewConfig);
	},

	extractColumnStyleInfo: function(style) {
		var match = /width:([^;]+);vertical-align:([^;]+);/gi.exec(style)
		return {
			'width': match[1],
			'vertical-align': match[2]
		};
	},

	getProperties: function(el) {
		var prop = {};
		var match,
			re = new RegExp('([a-z]+)+=[\'"]*([^\|\}\'"]*)[\'"]*', 'gi');
		while (match = re.exec(el)) {
			prop[match[1]] = match[2];
		}
		return prop;
	}
});
