glu.defView('RS.incident.SIEMData', {
	xtype: 'container',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	itemId: 'playbook-siemdata',
	style: {
		border: '1px silver solid'
	},
	defaults: {
		margin: 4
	},
	items: [{
		xtype: 'label',
		style: {
			fontWeight: 'bold',
		},
		text: '~~rawData~~'
	}, {
		xtype: 'text',
		text: '@{data}'
	}],
	listeners: {
		activate: '@{tabInit}'
	}
});