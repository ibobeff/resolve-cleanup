//Parent: RS.incident.Playbook
glu.defView('RS.incident.AuditLog', {
	xtype: 'grid',
	cls: 'rs-grid-dark',
	itemId: 'playbook-auditlog',
	plugins: [{
		ptype: 'searchpager',
		allowPersistFilter: false,
		hideMenu: true,
		toolbarCls : 'rs-dockedtoolbar',
		buttons: [{
			xtype: 'button',
			text: 'Export',
			tooltip: 'Export to PDF, JSON or CSV',
			cls: 'rs-btn-light rs-small-btn',
			handler: function() {
				this.up('grid').fireEvent('exportauditlog');
			}
		}]
	}],
	store: '@{auditlogStore}',
	columns: [{
		dataIndex: 'sysCreatedBy',
		text: '~~user~~',
		sortable: true,
		filterable: true,
		flex: 1,
		initialShow: true
	}, {
		dataIndex: 'ipAddress',
		text: '~~IPAddress~~',
		sortable: true,
		filterable: true,
		flex: 1
	}, {
		dataIndex: 'description',
		text: '~~description~~',
		sortable: true,
		filterable: true,
		flex: 5
	}, {
		dataIndex: 'sysCreatedOn',
		text: '~~loggedOn~~',
		flex: 1,
		sortable: true,
		filterable: false,
		hidden: false,
		initialShow: true,
		renderer: function(field) {
			return Ext.util.Format.date(new Date(field), clientVM.getUserDefaultDateFormat());
		}
	}],
	listeners: {
		activate: '@{tabInit}',
		exportauditlog: '@{exportAuditlog}'
	}
});