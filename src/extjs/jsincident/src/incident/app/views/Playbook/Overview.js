glu.defView('RS.incident.Overview', {
	itemId: 'playbook-overview',
	cls: 'playbook-overview',
	xtype: 'container',
	defaults: {
		margin: '0 10 0 0',
		padding: 10
	},
	items: [{
		//Summary
		itemId: 'playbook-summary',
		defaultType: 'displayfield',
		defaults: {
			labelWidth: 140,
			cls: 'rs-displayfield-value',
			margin: '0 0 5 0',
			fieldStyle: {
				fontWeight: 'bold'
			}
		},
		items: [{
			xtype: 'label',
			text: '~~summary~~',
			style: {
				fontWeight: 'bold'
			},
		}, {
			fieldLabel: '~~id~~',
			value: '@{..returnId}'
		}, {
			fieldLabel: '~~severity~~',
			value: '@{..returnSeverity}'
		}, {
			fieldLabel: '~~createdOn~~',
			value: '@{..returnDateCreated}'
		}, {
			fieldLabel: '~~type~~',
			value: '@{..returnType}',
			renderer: function(value) {
				if (value) {
					return value
				} else {
					return 'None'
				}
			}
		}, {
			xtype: 'combobox',
			fieldLabel: '~~dataCompromised~~',
			displayField: 'display',
			valueField: 'value',
			value: '@{..returnDataCompromised}',
			store: '@{dataCompromisedStore}',
			queryMode : 'local',
			editable: false,
			listeners: {
				change: '@{updateDataCompromised}'
			},
			readOnly: '@{readOnlyDataCompromised}'
		}, {
			xtype: 'combo',
			fieldLabel: '~~status~~',
			store: '@{statusStore}',
			displayField: 'display',
			valueField: 'value',
			queryMode : 'local',
			editable: false,
			value: '@{..returnStatus}',
			listeners: {
				change: '@{updateStatus}'
			},
			readOnly: '@{readOnlyStatus}'
		}]
	}, {
		//Team
		itemId: 'playbook-team',
		layout: {
			type: 'vbox',
		},
		items: [{
			xtype: 'label',
			text: '~~team~~',
			style: {
				fontWeight: 'bold'
			}
		}, {
			xtype: 'displayfield',
			fieldLabel: '~~owner~~',
			value: '@{owner}',
			labelWidth: 140,
			cls: 'rs-displayfield-value',
			fieldStyle: {
				fontWeight: 'bold'
			},
			margin: '0 0 5 0',
			renderer: function(value) {
				if (value) {
					return value
				} else {
					return 'None'
				}
			}
		}, {
			xtype: 'displayfield',
			fieldLabel: '~~createdBy~~',
			value: '@{..returnCreatedBy}',
			labelWidth: 140,
			cls: 'rs-displayfield-value',
			fieldStyle: {
				fontWeight: 'bold'
			},
			margin: '0 0 5 0'
		}, {
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [{
				width: 135,
				xtype: 'displayfield',
				cls: 'rs-displayfield-value',
				fieldStyle: {
					fontWeight: 'bold'
				},
				fieldLabel: '~~members~~'
			}, {
				xtype: 'component',
				layout: 'vbox',
				flex: 1,
				width: '100%',
				xtype: 'fieldset',
				defaults: {
					margin: '5 0 0 0'
				},
				maxHeight: 150,
				autoScroll: true,
				items: '@{membersList}',
				border: false,
			}]
		}, {
			xtype: 'button',
			cls: 'rs-small-btn rs-btn-light',
			height: 24,
			margin: '0 0 0 140',
			text: '~~addRemoveMember~~',
			handler: '@{addMember}',
			disabled: '@{readOnlyTeamMebers}'
		}]
	}, {
		itemId: 'playbook-relatedinvestigations',
		layout: {
			type: 'vbox',
		},
		defaultType: 'displayfield',
		defaults: {
			labelWidth: 140,
			cls: 'rs-displayfield-value',
			margin: '0 0 5 0',
			fieldStyle: {
				fontWeight: 'bold'
			},
		},
		items: [{
			xtype: 'label',
			text: '~~relatedInvestigations~~',
			style: {
				fontWeight: 'bold'
			}
		}, {
			fieldLabel: '~~referenceId~~',
			value: '@{referenceId}',
			hidden: '@{referenceIdIsBlank}'
		}, {
			fieldLabel: '~~alertId~~',
			value: '@{alertId}',
			hidden: '@{alertIdIsBlank}'
		}, {
			fieldLabel: '~~correlationId~~',
			value: '@{correlationId}',
			hidden: '@{correlationIdIsBlank}'
		}, {
			xtype: 'label',
			text: '~~none~~',
			hidden: '@{noRelatedIsVisible}'
		}]
	}]
});


glu.defView('RS.incident.MemberModal', {
	itemId: 'playbook-addmember',
	width: 700,
	padding: 15,
	modal : true,
	cls : 'rs-modal-popup',
	title: '~~manageMembersModal~~',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'combobox',
		fieldLabel: '~~owner~~',
		store: '@{..securityGroupStore}',
		queryMode: 'local',
		displayField: 'userId',
		valueField: 'userId',
		value: '@{ownerSelection}',
		queryMode : 'local',
		editable: false,
	}, {
		xtype: 'displayfield',
		fieldLabel: '~~members~~',
		margin: '5 0 0 0'
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		maxHeight: 500,
		margin : '0 0 10 0',
		viewConfig: {
			markDirty: false
		},
		store: '@{selectedMembersStore}',
		name : 'member',
		selType:'checkboxmodel',
    selModel : {
        mode : 'simple'
    },
		columns: [
			{text: '~~userId~~', dataIndex: 'userId', flex: 1,},
			{text: '~~displayName~~', dataIndex: 'displayName', flex: 1,}
		],
		listeners : {
			afterrender : function(grid) {
				grid._vm.on('selectMember', function(selectedMember) {
					for (var i = 0; i < selectedMember.length; i++){
					    grid.getSelectionModel().select(selectedMember[i], true);
					}
				})
				grid._vm.populateList();
			},
		},
	}],

	buttons: [{
		name: 'saveMembers',
		cls : 'rs-med-btn rs-btn-dark'
	}, {
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});

//Allow for member tooltips
glu.defView('RS.incident.MemberLabel', {
	xtype: 'label',
	//itemId: '@{elementId}',
	userId: '@{userId}',
	text: '@{userId}',
	memberName : '@{memberName}',
	style: {
		fontWeight: 'bold'
	},
	listeners : {
		afterrender : function(panel) {
			var tooltipUserId = (this.userId.indexOf('(Owner)')) ? this.userId.slice(0, -7) : this.userId;
			Ext.QuickTips.register({
				target: this.getEl(),
				text: 'Name: ' + this.memberName
			});
		}
	}
})