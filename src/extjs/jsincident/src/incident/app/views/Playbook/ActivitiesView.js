glu.defView('RS.incident.ActivitiesView', {
	xtype: 'container',
	itemId: 'playbook-activitiesview',
	items: [{
		xtype: 'toolbar',
		anchor: '100% 5%',
		padding: 8,
		style: {
			fontSize: '16px',
		},
		items: [{
			xtype: 'button',
			iconCls: 'rs-social-button icon-reply-all',
			tooltip: 'Return to Activities',
			handler: '@{returnToActivities}'
		}, {
			xtype: 'tbseparator'
		}, {
			xtype: 'text',
			text: '@{returnHeader}'
		},
		'->',
		/*{
			xtype: 'button',
			hidden: '@{dtDisplaymode}',
			cls: 'rs-execute-button',
			iconCls: 'rs-social-button icon-play',
			tooltip: 'Execute',
			handler: function(button) {
				button.fireEvent('executeActivity', button, button);
			},
			listeners: {
				executeActivity: '@{execute}'
			}
		},*/ {
			xtype: 'button',
			hidden: '@{dtDisplaymode}',
			name: 'notes',
			text: '',
			tooltip: 'Show Notes',
			iconCls: 'rs-social-button icon-copy'
		}]
	}, {
		xtype: '@{dtView}',
		hidden: '@{!..dtDisplaymode}',
		style: {
			borderTop: '1px #b5b8c8 solid'
		},
		listeners: {
			show: function(v) {
				var p = v.up('#playbook-activitiesview');
				v.setWidth(p.getWidth());
				v.setHeight(p.getHeight() *.95);  // 5% is for the toolbar
			}
		}
	}, {
		xtype: 'component',
		id: 'playbook-iframe',
		hidden: '@{dtDisplaymode}',
		height: '100%',
		style: {
			borderTop: '1px #b5b8c8 solid'
		}
	}]
});