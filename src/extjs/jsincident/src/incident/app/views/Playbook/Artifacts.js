//Parent: RS.incident.Artifacts
glu.defView('RS.incident.Artifacts', {
	xtype: 'grid',
	itemId: 'playbook-artifacts',
	cls: 'rs-grid-dark',
	padding: 0,
	store: '@{artifactsStore}',
	multiSelect: true,
	name: 'artifacts',
	viewConfig: {
		stripeRows: true,
		enableTextSelection: true
	},
	dockedItems: [{
		xtype: 'toolbar',
		cls: 'rs-dockedtoolbar',
		items: [{
			xtype: 'button',
			disabled: '@{!..pSirInvestigationViewerArtifactsCreate}',
			name: 'addArtifact',
			text: '~~add~~',
			cls: 'rs-small-btn rs-btn-light'		
		}, {
			xtype: 'button',	
			name: 'removeArtifact',
			text: '~~remove~~',
			cls: 'rs-small-btn rs-btn-light'		
		}],
	}],
	plugins: [{
		ptype: 'rowexpander',
		pluginId: 'playbook-rowexpander',
		columnWidth: 32,
		selectRowOnExpand: true,
		rowBodyTpl : [
			'<div><b>Related SIR:</b> {sirList}</div>',
			'<div style=margin-top:5px;><b>Added By: </b>{sysCreatedBy} on {sysCreatedOn}</div>' +
			'<div style=margin-top:5px;><b>Updated By: </b>{sysUpdatedBy} on {sysUpdatedOn}</div>' +
			'<div style=margin-top:15px;word-wrap:break-word;><b>Key: </b>{name}</div>' +
			'<div style=margin-top:5px;word-wrap:break-word;><b>Value: </b>{encodedValue}</div>' +
			'<div style=margin-top:5px;word-wrap:break-word;><b>Description: </b>{encodedDescription}</div>'
		]
	}],
	columns: [
	{
		dataIndex: 'sysCreatedBy',
		text: '~~addedBy~~',
		flex: 1
	}, {
		dataIndex: 'name',
		text: '~~ushortName~~',
		flex: 1,
	}, {
		dataIndex: 'value',
		text: '~~value~~',
		flex: 3,
		renderer : function(value, metaData, record){
			var vm = this._vm;
			var id = Ext.id();
			Ext.defer(function () {
				Ext.widget('button', {
					renderTo: id,
					tooltip: vm.localize('lookupTootltip'),
					iconCls : 'icon-search',
					cls : ' sir-action-icon',
					handler: function(){
						vm.lookupIncidentByArtifact(record);
					}
				});
			}, 50);
			return Ext.String.format('<div class="sir-artifact-value">' + Ext.String.htmlEncode(value) + '<span style=float:right; id="{0}"></span></div>', id);
		}
	},{
		xtype: 'actioncolumn',		
		hideable: false,
		sortable: false,
		align: 'center',			
		width: 45,			
		items: [{
			glyph: 0xF0AE,
			tooltip : 'Action',			
			handler: function(view, rowIndex, colIndex, item, e, record) {
				this.up('grid').fireEvent('handleActionClick', this, record);
			}
		}]
	},{
		dataIndex: 'sysCreatedOn',
		text: '~~addedOn~~',
		flex: 1,
	}, {
		dataIndex: 'sysUpdatedOn',
		text: '~~updatedOn~~',
		flex: 1,
	},{
		text: RS.common.locale.sysId,
		dataIndex: 'id',
		hidden: true,
		autoWidth: true,
		sortable: false,
		filterable: false,
		groupable: false,
		width: 300
	}],
	listeners: {	
		activate: '@{tabInit}',
		handleActionClick : '@{actionHandler}'
	}
});
//Artifacts Modal
//Parent: RS.incident.Artifacts
glu.defView('RS.incident.ArtifactModal', {
	itemId: 'playbook-artifactmodal',
	title: '~~addArtifact~~',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	padding: 15,
	height: 450,
	width: 700,
	modal: true,
	cls : 'rs-modal-popup',
	defaults : {
		margin : '4 0'
	},
	items: [{
		xtype: 'combobox',
		store : '@{artifactTypeStore}',
		displayField : 'uname',
		valueField : 'uname',
		queryMode : 'local',		
		name: 'type',
		editable : false,
	},{
		xtype: 'combobox',
		store : '@{aliasStore}',		
		displayField : 'ushortName',
		valueField : 'ushortName',
		queryMode : 'local',		
		name: 'ushortName'
	},{
		xtype: 'textareafield',
		fieldLabel: '~~value~~',		
		flex : 1,
		name : 'value',
		renderer: function(field) {
			return field.replace(/(\r\n|\n|\r)/gm, "<br />")
		}
	}, {
		xtype: 'textareafield',
		fieldLabel: '~~description~~',
		flex : 1,
		value: '@{description}',
		renderer: function(field) {
			return field.replace(/(\r\n|\n|\r)/gm, "<br />")
		},
		margin : '4 0 10 0'
	}],
	buttons: [{
		name: 'saveArtifact',
		cls : 'rs-med-btn rs-btn-dark',
		disabled: '@{isSaving}'
	},{
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});