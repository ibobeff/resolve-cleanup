//Parent: RS.incident.Playbook
glu.defView('RS.incident.Notes', {
	xtype: 'grid',
	itemId: 'playbook-notes',
	cls: 'rs-grid-dark',
	padding: 0,
	store: '@{notesStore}',
	dockedItems: [{
		xtype: 'toolbar',
		cls: 'rs-dockedtoolbar',
		items: [{
			xtype: 'button',
			disabled: '@{!..pSirInvestigationViewerNotesCreate}',
			name: 'addNote',
			text: '~~add~~',
			cls: 'rs-small-btn rs-btn-light',
		}/*, {
			xtype: 'button',
			disabled: '@{!..pSirInvestigationViewerNotesModify}',
			name: 'editNote',
			text: '@{editButtonText}',
			cls: 'rs-small-btn rs-btn-light',
		}, {
			xtype: 'button',
			name: 'deleteNote',
			text: 'Delete',
			cls: 'rs-small-btn rs-btn-light',
			maxWidth: 80,
		}*/]
	}],
	plugins: [/*{
		ptype: 'searchpager',
		allowPersistFilter: false,
		hideMenu: true,
		toolbarCls : 'rs-dockedtoolbar'
	}, */{
		ptype: 'rowexpander',
		columnWidth: 32,
		rowBodyTpl : [
			'<div style=margin-top:0px;><span style=font-weight:bold;>Added By: </span>{sysCreatedBy} on {sysCreatedOn}</div>' +
			//'<div style=margin-top:5px;><span style=font-weight:bold;>Updated By: </span>{sysUpdatedBy} on {sysUpdatedOn}</div>' +
			'<div style=margin-top:10px;word-wrap:break-word;><span style=font-weight:bold;>Note: </span>{fullNote}</div>'
		]
	}],
	columns: [{
		dataIndex: 'activityName',
		text: '~~activity~~',
		flex: 1,
		renderer: function(value) {
			return (value === null) ? 'N/A' : value
		}
	}, {
		dataIndex: 'postedBy',
		text: '~~addedBy~~',
		flex: 1
	}, {
		dataIndex: 'note',
		text: '~~notePreview~~',
		flex: 4
	}, {
		dataIndex: 'sysCreatedOn',
		text: '~~addedOn~~',
		flex: 1
	}/*, {
		dataIndex: 'sysUpdatedOn',
		text: '~~updatedOn~~',
		flex: 1
	}*/],
	listeners: {
		itemclick: '@{selectNote}',
		activate: '@{tabInit}'
	}
});

//Notes Modal
//Parent: RS.incident.Notes
glu.defView('RS.incident.NoteModal', {
	itemId: 'playbook-notemodal',
	padding : 10,
	cls : 'rs-modal-popup',
	title: '@{returnTitle}',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	width: 600,
	modal: true,
	items: [{
		xtype: 'textareafield',
		hideLabel: true,
		height: 350,
		fieldStyle: {
			border: 0
		},
		name: 'note',
		renderer: function(field) {
			return field.replace(/(\r\n|\n|\r)/gm, "<br />")
		}
	}],
	buttons: [{
		name: 'saveNote',
		text: '@{saveButtonText}',
		cls : 'rs-med-btn rs-btn-dark',
		disabled: '@{isSaving}'
	},{
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})