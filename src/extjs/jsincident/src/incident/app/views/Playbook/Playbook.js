glu.defView('RS.incident.dummy') //to make sure the ns is init before creating the factory
RS.incident.views.basePlaybookFactory = function(actualView) {
  var output = {
	itemId: 'playbook-viewport',
	overflowY: 'auto',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	bodyPadding : 15,
	ticketInfo: '@{ticketInfo}',
	cls: 'playbook-main',
	items: [{
		xtype: '@{overviewComponent}',
		width: 330,
		hidden: '@{..isReadOnly}',
	}, {
		xtype: 'container',
		layout: 'anchor',
		flex: 1,
		items: [{
			anchor: '100% 25%',
			padding: 10,
			style: {
				border: '1px silver solid',
			},
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'label',
				text: '~~description~~',
				style: {
					fontWeight: 'bold'
				}
			}, {
				//Description
				itemId: 'playbook-description',
				xtype: 'pageviewer',
				ucontent: '@{ticketInfo}',
				padding: '4px 0px 0px 0px',
				flex: 1,
				cls: 'rs-playbook-description',

				bodyStyle: {
					background: 'none'
				},
				listeners: {
					afterrender: function() {
						var me = this;
						this.body.on('dblclick', function() {
							me._vm.expandDescription();
						});
						this._vm.on('loadDescription', function(ucontent, fullName) {
							this.renderContent(this, ucontent, fullName);
						}, this);
						Ext.QuickTips.register({
							target: this.getEl(),
							text: me._vm.localize('doubleClickExpand')
						});
					}
				},
			}]
		}, {
			xtype: 'tabpanel',
			anchor: '100% 75%',
			itemId: 'playbook-tabpanel',
			cls: 'playbook-tabpanel rs-tabpanel-dark',
			margin: '10 0 0 0',
			header: false,
			activeTab: '@{activeTab}',
			defaults: {
				overflowY: 'auto',			
				disabled: '@{..isPreview}',
				hidden: '@{..isReadOnly}'
			},
			items: [{
				title: '~~activitiesTab~~',
				itemId: 'activitiesTab',
				xtype: '@{activitiesTab}',
				disabled: false,
				hidden: false
			}, {
				title: '~~notesTab~~',
				itemId: 'notesTab',
				xtype: '@{notesTab}'
			}, {
				title: '~~artifactsTab~~',
				itemId: 'artifactsTab',
				xtype: '@{artifactsTab}'
			}, {
				title: '~~attachmentsTab~~',
				itemId: 'attachmentsTab',
				xtype: '@{attachmentsTab}'
			}, {
				title: '~~automationresultsTab~~',
				itemId: 'automationresultsTab',
				xtype: '@{automationresultsTab}'
			}, {
				title: '~~auditlogTab~~',
				itemId: 'auditlogTab',
				xtype: '@{auditlogTab}'
			},  {
				title: '~~siemdataTab~~',
				itemId: 'siemdataTab',
				xtype: '@{siemdataTab}'
			}/*, {
				title: '~~relatedinvestigationsTab~~',
				itemId: 'relatedinvestigationsTab',
				xtype: '@{relatedinvestigationsTab}'
			}, {
				title: 'React',
				id: 'reactroot',
				listeners: {
					afterrender: function() {
						Ext.Loader.loadScript('/resolve/js/build.js');
					}
				}
			}*/]
		}]
	}],
	listeners: {
		afterrender: function(panel) {
			function beforedestroyHandler(){
				panel.fireEvent('beforedestroy');
			};
			document.addEventListener("beforedestroy", beforedestroyHandler);
			panel._vm.on("removeEventReference", function(){
				document.removeEventListener("beforedestroy", beforedestroyHandler);
			})
		},
		beforedestroy : '@{beforeDestroyComponent}'
	},
	activitiesModifiable: '@{pSirInvestigationViewerActivitiesModify}',
	setActivitiesModifiable: function(value) {
		var activities = this.down('#playbook-activitiesgrid');
		if (activities) {
			activities.modifiable = value;
		}
	}
  }

  Ext.apply(output, actualView.propertyPanel);
  return output;
}

glu.defView('RS.incident.Playbook', {
	parentLayout: 'basePlaybook',
	propertyPanel: {
		dockedItems: [{
			xtype : 'toolbar',
			margin : '15 15 0 0',
			items : [{
				xtype: 'tbtext',
				cls: 'rs-display-name',
				text: '@{returnTitle}',
			},'->', {
				cls : 'rs-small-btn rs-btn-light',
				name : 'exportPDF'
			}]
		}]
	}
})

//Parent: RS.incident.Playbook
glu.defView('RS.incident.DescriptionModal', {
	itemId: 'playbook-descriptionmodal',
	title: '~~playbookDescription~~',
	height: '75%',
	width: '75%',
	modal: true,
	cls : 'rs-modal-popup',
	bodyPadding: 10,
	overflowY: 'auto',
	items: [{
		//Description
		itemId: 'playbook-description',
		xtype: 'pageviewer',
		ucontent: '@{..ticketInfo}',
		flex: 1,
		cls: 'rs-playbook-description',
		bodyStyle: {
			background: 'none'
		},
		listeners: {
			afterrender: function() {
				this._vm.on('loadDescriptionModal', function(ucontent, fullName) {
					this.renderContent(this, ucontent, fullName);
				}, this);
			}
		}
	}]
});
