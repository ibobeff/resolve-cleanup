glu.defView('RS.incident.Attachments', {
	itemId: 'playbook-attachments',
	api: '@{api}',
	dockedItems: [{
		xtype: 'toolbar',
		name: 'actionBar',
		cls : 'rs-dockedtoolbar',
		items: [{
			xtype: 'button',
			disabled: '@{!..pSirInvestigationViewerAttachmentsCreate}',
			text: '~~upload~~',
			name: 'addAttachment',
			cls: 'rs-btn-light rs-small-btn',
			maxWidth: 80
		}, {
			xtype: 'button',
			name: 'deleteAttachment',
			cls: 'rs-btn-light rs-small-btn',
			maxWidth: 80,
			disabled: true,
		}, '->', {
			name: 'reload',
			tooltip: 'Reload',
			text: '',
			iconCls: 'x-tbar-loading',
		}]
	}],
	modifiable: '@{..pSirInvestigationViewerAttachmentsModify}',
	setModifiable: function(val) {
		this.down('#maliciousCheckBox').setDisabled(!val);
	},
	xtype: 'grid',
	cls : 'rs-grid-dark',
	name: 'attachments',
	layout : 'fit',
	plugins: [{
		ptype: 'cellediting',
		clicksToEdit: 1,
		listeners: {
			beforeedit: function(editor, context, eOpts) {
				if (!context.grid.modifiable) {
					return false;
				}
				return true;
			}
		}
	}],
	selModel: {
		selType: 'rowmodel'
	},
	store: '@{store}',
	columns: [{
		text: '~~name~~',
		flex: 3,
		sortable: false,
		dataIndex: 'name'
	}, {
		text: '~~description~~',
		flex: 3,
		sortable: false,
		dataIndex: 'description',
		renderer: RS.common.grid.plainRenderer(),
		editor: 'textfield'
	}, {
		text: '~~size~~',
		width: 90,
		sortable: true,
		renderer: Ext.util.Format.fileSize,
		dataIndex: 'size'
	}, {
		xtype: 'checkcolumn',
		itemId: 'maliciousCheckBox',
		text: '~~malicious~~',
		width: 90,
		sortable: true,
		dataIndex: 'malicious'
	}, {
		text: '~~uploadedOn~~',
		width: 200,
		sortable: true,
		dataIndex: 'sysCreatedOn',
		renderer: function(value) {
			var r = Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat());
			if (Ext.isString(value))
				return value;
			return r(value);
		}
	}, {
		text: '~~uploadedBy~~',
		sortable: true,
		flex: 1,
		dataIndex: 'sysCreatedBy',
	}, {
		xtype: 'actioncolumn',
		hideable: false,
		width: 38,
		items: [{
			icon: '/resolve/images/arrow_down.png',
			// Use a URL in the icon config
			tooltip: RS.common.locale.download,
			handler: function(gridView, rowIndex, colIndex) {
				var rec = gridView.getStore().getAt(rowIndex),
					api = gridView.ownerCt.api,
					sys_id = rec.get('id');

				if (sys_id) gridView.ownerCt.downloadURL(Ext.String.format(api.downloadViaDocName, sys_id, gridView.ownerCt._vm.csrftoken));
				else clientVM.displayError(RS.incident.locale.noDownloadBody)
			}
		}]
	}],
	downloadURL: function(url) {
		var iframe = document.getElementById("hiddenDownloader");
		if (iframe === null) {
			iframe = document.createElement('iframe');
			iframe.id = "hiddenDownloader";
			iframe.style.visibility = 'hidden';
			iframe.style.display = 'none';
			document.body.appendChild(iframe);
		}
		iframe.src = url;
	},
	listeners: {
		selectionchange: function(v, selected, eOpts) {
			this.fireEvent('selectionChanged', this, this, selected);
		},
		selectionChanged: '@{selectionChanged}',
		beforerender: function(v, eOpts) {
			this.fireEvent('initView', this, this);
		},
		initview: '@{initView}',
		activate: '@{tabInit}'
	}
});

