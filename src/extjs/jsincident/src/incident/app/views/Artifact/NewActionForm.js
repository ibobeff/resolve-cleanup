glu.defView('RS.incident.NewActionForm',{
	title : '@{title}',
	cls : 'rs-modal-popup',
	modal : true,
	width : 650,	
	padding : 15,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : '4 0',
		labelWidth : 130
	},
	items : [{
		xtype : 'textfield',
		name : 'uname',
		readOnly : '@{edit}'
	},{
		xtype : 'fieldcontainer',
		hideLabel : true,
		layout : 'hbox',
		margin : '0 0 0 135',
		defaultType : 'radio',	
		items : [{
			boxLabel : '~~automation~~',
			value : '@{!actionTaskIsSelected}',
			margin : '0 10 0 0'
		},{
			boxLabel : '~~actiontask~~',
			value : '@{actionTaskIsSelected}'			
		}]
	},{
		xtype: 'triggerfield',
		name: 'automationName',
		fieldLabel : '~~automation~~',
		hidden : '@{actionTaskIsSelected}',
		editable : false,
		triggerCls: 'x-form-search-trigger',
		onTriggerClick: function() {
			this.fireEvent('openAutomationSearch');
		},
		listeners: {
			openAutomationSearch: '@{openAutomationSearch}'
		}
	},{
		xtype: 'triggerfield',
		hidden : '@{!actionTaskIsSelected}',
		name: 'taskName',
		fieldLabel : '~~actiontask~~',
		editable : false,
		triggerCls: 'x-form-search-trigger',
		onTriggerClick: function() {
			this.fireEvent('openActiontaskSearch');
		},
		listeners: {
			openActiontaskSearch: '@{openActiontaskSearch}'
		}
	},{
		xtype : 'combo',
		displayField : 'name',
		valueField : 'name',
		name : 'param',		
		fieldLabel : '~~parameter~~',
		store : '@{parameterStore}',
		editable : false,
		margin : '4 0 20 0',
		queryMode : 'local'
	}],
	buttons : [{
		name : 'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})