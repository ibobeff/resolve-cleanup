glu.defView('RS.incident.NewAliasForm',{
	title : '~~newAliasForm~~',
	cls : 'rs-modal-popup',
	modal : true,
	width : 600,	
	padding : 15,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : '4 0'
	},
	items : [{
		xtype : 'displayfield',
		name : 'type',
		cls : 'rs-displayfield-value'		
	},{
		xtype : 'combo',
		name : 'uartifactTypeSource',
		store : '@{sourceStore}',
		valueField : 'value',
		displayField : 'display',
		editable : false
	},{
		xtype : 'combo',
		name : 'ushortName',
		displayField : 'ushortName',
		valueField : 'ushortName',
		queryMode : 'local',
		store : '@{aliasStore}'
	},{
		xtype : 'textfield',
		name : 'ufullName',
		readOnly : '@{!isCustomAlias}'
	},{
		xtype : 'textarea',
		name : 'udescription',
		readOnly : '@{!isCustomAlias}',
		height : 130,
		margin : '4 0 10'
	}],
	buttons : [{
		name : 'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})