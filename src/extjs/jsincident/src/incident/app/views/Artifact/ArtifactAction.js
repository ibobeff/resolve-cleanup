glu.defView('RS.incident.ArtifactAction',{
	title : 'Actions',
	id: 'artifact-action',
	padding : 15,
	width : 600,
	height : 400,
	modal : true,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	cls : 'rs-modal-popup',
	defaults : {
		margin : '4 0'
	},
	items : [{
		xtype : 'component',
		html : '@{valuePairInfo}'
	},{
		xtype : 'grid',
		flex : 1,
		margin : '4 0 10 0',
		cls : 'rs-grid-dark',
		autoScroll : true,		
		store : '@{actionStore}',
		viewConfig: {
	        getRowClass: function (record, index) {
	            if (record.get('isDisabled')) 
	            	return 'disabled-row';
	        }        
	    },
		columns : [{
			text : '~~name~~',
			flex : 3,
			dataIndex : 'uname'
		},{
			xtype: 'actioncolumn',
			text : 'Action',		
			hideable: false,
			sortable: false,
			align: 'center',			
			width: 60,	
			items: [{
				glyph : 0xF04B,	
				tooltip : 'Execute',		
				handler: function(view, rowIndex, colIndex, item, e, record) {
					this.up('grid').fireEvent('handleActionClick', this, record, view);
				},
				isDisabled : function(view, rowIndex, colIndex, item, record) {
					return record.get('isDisabled');
				}	
			}]
		}],
		listeners: {	
			handleActionClick : '@{actionHandler}'
		}
	}],
	buttons : [{
		name : 'close',
		cls : 'rs-med-btn rs-btn-dark'
	}]
})