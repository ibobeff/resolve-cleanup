glu.defView('RS.incident.ArtifactConfigEntry',{
	header:{
        titlePosition: 0,     
        items:[{        	
            xtype:'button',
            text: 'Delete',
            cls : 'rs-small-btn rs-btn-dark',
            handler: function(btn){            	
            	var vm = btn.up().up()._vm;
            	vm.removeType();
            },
            listeners : {
            	afterrender : function(btn){
            		var vm = btn.up().up()._vm;
            		if(!vm.isCustomType)
            			btn.hide();
            	}
            },
        }],
        listeners : {
        	click : function(){
        		var parentCnt = this.up();
        		parentCnt.collapsed == false ? parentCnt.collapse() : parentCnt.expand();
        	}
        }
    },
	title : '@{artifactType}',
	htmlEncodeTitle : true,
	collapsible : true,
	collapsed : true,
	animCollapse : false,
	bodyPadding : 15,
	margin : '0 0 5 0',
	cls : 'block-model',		
	layout : {
		type : 'hbox',
		align: 'stretch'
	},
	defaults : {
		height : 400,
		autoScroll : true
	},
	items : [{
		//Action
		xtype : 'grid',
		cls : 'rs-grid-dark',
		name : 'action',		
		flex : 3,
		margin : '0 20 0 0',
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-btn-light rs-small-btn'
			},
			items :[{
				xtype: 'tbtext',
				cls: 'artifact-tbtext',
				text: '~~action~~',				
			},'|','addAction','removeAction']
		}],
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.editColumnTooltip,
			glyph : 0xF044,	
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'uname'			
		},		
		store : '@{actionStore}',
		columns : [{
			text : '~~name~~',
			flex : 3,
			dataIndex : 'uname'
		},{
			text : '~~automation~~',
			flex : 2,
			dataIndex : 'executionItem'
		},{
			text : '~~executionType~~',
			flex : 1,
			dataIndex : 'executionType'
		},{
			text : '~~parameter~~',
			width : 200,
			dataIndex : 'param'
		}],
		listeners: {
			editAction: '@{editAction}',			
		}
	},{
		//Alias Keys		
		xtype : 'grid',
		cls : 'rs-grid-dark',
		store : '@{aliasStore}',
		name : 'aliasKeys',
		selModel : {
	        mode : 'single'
	    },
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items :[{
				xtype: 'tbtext',
				cls: 'artifact-tbtext',
				text: '~~aliasKeys~~',					
			},'|','addAlias','removeAlias']
		}],	
		flex : 2,
		columns : [{
			text : '~~ushortName~~',
			dataIndex : 'ushortName',
			flex : 2
		},{
			text : '~~uartifactTypeSource~~',
			dataIndex : 'uartifactTypeSource',
			width : 100
		},{
			text :'~~ufullName~~',
			dataIndex : 'ufullName',
			flex : 3
		}]
	}]	
})