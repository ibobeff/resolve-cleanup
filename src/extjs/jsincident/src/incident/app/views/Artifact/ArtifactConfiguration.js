glu.defView('RS.incident.ArtifactConfiguration', {
	bodyPadding : 15,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},	
	autoScroll : true,
	dockedItems : [{
		xtype: 'tbtext',
		cls: 'rs-display-name',
		text: '~~articfactConfigTitle~~',
		padding : '15 0 5 15'		
	},{
		xtype : 'toolbar',
		cls: 'actionBar actionBar-form',
		padding : '5 0 5 15',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items : ['newType']
	}],
	items : '@{configList}'
})
glu.defView('RS.incident.NewTypeForm',{
	title : '~~createNewType~~',
	padding : 15,
	width : 450,
	height : 200,
	modal : true,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	cls : 'rs-modal-popup',
	items : [{
		xtype : 'textfield',
		name : 'name'		
	}],
	buttons : [{
		name : 'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})