glu.defView('RS.incident.Dashboard.MetricsSetting', {
	xtype: 'form',
	itemId: 'metricssetting',
	target: '@{target}',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		margin: '4 0',
		labelWidth: 130,
		flex: 1
	},
	items: [{
		xtype: 'fieldcontainer',
		margin : 0,
		fieldLabel: '~~show~~',
		layout: 'hbox',
		defaultType: 'checkbox',
		defaults: {
			hideLabel : true,
			margin : '0 15 0 0'
		},
		items: [{
			name: 'dashboardShowOpen',
			boxLabel : '~~dashboardShowOpen~~',
			value: false
		}, {
			name: 'dashboardShowInProgress',
			boxLabel : '~~dashboardShowInProgress~~',
			value: false
		}, {
			name: 'dashboardShowClosed',
			boxLabel : '~~dashboardShowClosed~~',
			value: false
		}, {
			name: 'dashboardShowPastDue',
			boxLabel : '~~dashboardShowPastDue~~',
			value: false
		}, {
			name: 'dashboardShowSLAatRisk',
			boxLabel : '~~dashboardShowSLAatRisk~~',
			value: false
		}]
	}, {
		xtype: 'combo',
		editable: false,
		store: '@{slaPolicyStore}',
		name: 'sLAatRiskPolicy',
		value: '',
		displayField: 'label',
		valueField: 'nDays'
	}, {
		xtype: 'fieldcontainer',
		fieldLabel: 'Scope',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'numberfield',
			name: 'scope',
			allowBlank: false,
			labelSeparator: '',
			fieldLabel: '',
			anchor: '100%',
			flex: 1,
			margin: '0 5 0 0',
			minValue: 1,
			maxValue: '@{maxScope}'
		}, {
			xtype: 'combo',
			name: 'scopeUnit',
			labelSeparator: '',
			fieldLabel: '',
			width: 120,
			editable: false,
			store: '@{scopeUnitStore}',
			displayField: 'unit',
			valueField: 'unit',
			listeners: {
				select: '@{scopeUnitChange}'
			}
		}]
    }, {
		xtype: 'fieldcontainer',
		labelSeparator: '',
		fieldLabel: ' ',
		//margin: '2 0 10 10',
		items: [{
			xtype: 'label',
			style: 'font-style:italic;',
			text: '@{tips}'
		}]
	}],

	buttons: [{
		name: 'apply',
		formBind: true,
		cls : 'rs-med-btn rs-btn-dark'
	} ,{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}],

	listeners: {
		loadform: '@{loadForm}'
	},

	fValid: '@{fvalid}',
	setFValid: function(val) {
		this.down('numberfield').clearInvalid();
	},

	asWindow: {
		title: '~~metricsSetting~~',
		closeAction: 'hide',
		border: false,
		closable: true,
		cls : 'rs-modal-popup',
		padding :15,
		listeners: {
			beforerender: function() {
				var form = this.down('#metricssetting');
				form.fireEvent('loadform', this, form);
			},
			show: function(win) {
				win.alignTo(this.down('#metricssetting').target.id, 'tr-br');

			}
		}
	}
});
