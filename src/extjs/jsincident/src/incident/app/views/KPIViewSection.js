glu.defView('RS.incident.Dashboard.KPIViewSection', {
	minHeight: 300,
	padding : 8,
	overflowX: 'auto',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	defaults: {
		flex: 1
	},
	items: '@{viewSectionEntries}'
});
