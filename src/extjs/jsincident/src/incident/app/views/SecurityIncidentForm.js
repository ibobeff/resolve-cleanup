glu.defView('RS.incident.SecurityIncidentForm', {
	asWindow: {
		title: '~~newInvestigation~~',
		modal: true,
		padding: 15,
		cls : 'rs-modal-popup',
		width : 700
	},
	xtype: 'form',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		labelWidth: 190,
	},
	items: [{
		xtype: 'textfield',
		fieldLabel: '~~title~~',
		name: 'title',
		maxLength: 200,
		enforceMaxLength: true,
		allowBlank: false
	},{
		xtype: 'combo',
		fieldLabel: '~~type~~',
		name: 'investigationType',
		allowBlank: false,
		store: '@{..typeStore}',
		queryMode: 'local',
		displayField: 'type',
		valueField: 'type',
		editable: false
	},{
		xtype: 'documentfield',
		fieldLabel: '~~playbookTemplate~~',
		name: 'playbook',
		store: '@{..playbookTemplateStore}',
		allowBlank: false,
		inline: true,   // inline cell editing
		displayField: 'ufullname',
		valueField: 'ufullname',
		trigger2Cls: '',
		listeners: {
			select : '@{handlePlaybookSelect}',
			validatetemplatename: '@{validateTemplateName}',
			blur: function(v) {
				v.fireEvent('validatetemplatename', this, v)
			}
		}
	},{
		xtype: 'combo',
		fieldLabel: '~~severity~~',
		name: 'severity',
		store: '@{severityStore}',
		displayField: 'severity',
		valueField: 'severity',
		editable: false
	}, {
		xtype: 'datefield',
		fieldLabel: '~~dueDate~~',
		name: 'dueBy',
		allowBlank: true,
		minValue : new Date(),
		editable: false
	}, {
		xtype: 'combo',
		fieldLabel: '~~owner~~',
		name: 'owner',
		allowBlank: true,
		store: '@{..ownerStore}',
		displayField: 'userId', //display name/userId
		valueField: 'userId',
		queryMode: 'local',
		editable: false
	}, {
		xtype: 'combo',
		fieldLabel: '~~origin~~',
		name: 'sourceSystem',
		store: '@{originStore}',
		editable: true,
		displayField: 'origin',
		valueField: 'origin',
		listeners: {
			select: '@{originSelected}'
		}
	} ,{
		xtype: 'textfield',
		name: 'externalReferenceId',
		value: '',
		hidden: '@{resolveSelected}',
		allowBlank: '@{resolveSelected}',
		setAllowBlank: function(allow) {
			this.allowBlank = allow;
			this.setValue(allow? 'allow': ''); // Trick to by pass allowBlank for Resolve
		}
	},{
		xtype: 'textfield',
		name: 'status',
		value: 'Open',
		hidden: true
	}],

	listeners: {
		afterrender: function() {
			this.fireEvent('adjustwindowsize', this, this);
		},
		adjustwindowsize: '@{adjustWindowSize}'
	},

	buttons: [{
		name: 'add',
		formBind: true,
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name :'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}],	
});