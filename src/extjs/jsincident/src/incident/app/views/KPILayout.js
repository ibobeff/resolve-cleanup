glu.defView('RS.incident.Dashboard.KPILayout', {
	overflowX: 'auto',
	overflowY: 'auto',
	margin: 5,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: '@{layoutSections}'
});
