glu.defView('RS.incident.PlaybookTemplatePreview', {
	parentLayout: 'basePlaybook',
	propertyPanel: {
		asWindow: {
			padding : 15,
			title: '~~preview~~',
			cls : 'rs-modal-popup',
			modal : true,
			listeners: {
				beforeshow: function() {
					this.setWidth(Math.round(Ext.getBody().getWidth() * .90));
					this.setHeight(Math.round(Ext.getBody().getHeight() * .90));
				}
			},
			/*
			buttons: [{
				hidden: '@{!isPreview}',
				name: 'close',
				cls : 'rs-med-btn rs-btn-light',
			}] 
			*/
		}
	}
})

glu.defView('RS.incident.PlaybookTemplateSearchPreview', {
	parentLayout: 'basePlaybook',
	propertyPanel: {
		id: 'playbookTemplateSearchPreview'
	}
})

