Ext.define('RS.incident.PlaybookTemplate.Spinnerfield', {
	extend: 'Ext.form.field.Spinner',
	alias: 'widget.rsspinnerfield',
	maxVal: Number.MAX_VALUE,
	value: 0,
	step: 1,
	allowBlank: false,
	initComponent: function() {
		var me = this;
		this.listeners = {
			spinUp: function(spinner) {
				var value = parseInt(spinner.getValue() || 0);
				if (value < me.maxVal) {
					spinner.setValue((value + 1).toString());
				}
			},
			spinDown: function(spinner) {
				var value = parseInt(spinner.getValue() || 0);
				if (value > 0) {
					spinner.setValue((value - 1).toString());
				}
			},
			change: function(spinner, newVal) {
				var value = parseInt(newVal) || 0;
				if (value < 0) {
					spinner.setValue('0');
				} else if (value > me.maxVal) {
					spinner.setValue(me.maxVal);
				} else {
					spinner.setValue(value);
				}
				var label = this.up().down('label[name='+this.name+'label]');
				label.setText(this.name.slice(0, (value==1)? this.name.length-1: undefined)); // i.e. 1 hour/2 hours
				this.up().fireEvent('slachanged');
			}
		};
		this.callParent(arguments);
	}
});

Ext.define('RS.incident.PlaybookTemplate.Numberfield', {
	extend: 'Ext.form.FieldContainer',
	alias: 'widget.rsnumberfield',
	defaults: {
		flex: 1
	},
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	paddingBetweenUnit: 0,
	validateOnChange: false,
	initComponent: function() {
		var hideHour = false;
		var me = this;
		var hideMinute = false;
		if (this.precision == 'day') {
			hideHour = hideMinute = true;
		} else if (this.precision == 'hour') {
			hideMinute = true;
		}
		var padding = '5 ' + this.paddingBetweenUnit + ' 0 5';
		this.items = [{
			xtype: 'rsspinnerfield',
			name: 'days'
		}, {
			xtype: 'label',
			name: 'dayslabel',
			text: 'days',
			padding: padding
		}, {
			xtype: 'rsspinnerfield',
			name: 'hours',
			hidden: hideHour,
			maxVal: 23
		}, {
			xtype: 'label',
			name: 'hourslabel',
			text: 'hours',
			hidden: hideHour,
			padding: padding
		}, {
			xtype: 'rsspinnerfield',
			name: 'minutes',
			regex: /[0-9]+/,
			hidden: hideMinute,
			maxVal: 59
		}, {
			xtype: 'label',
			name: 'minuteslabel',
			text: 'minutes',
			hidden: hideMinute,
			padding: padding
		}];
		this.callParent(arguments);
	}
});

Ext.define('RS.incident.store.RequiredStates', {
	extend: 'Ext.data.Store',
	alias: 'store.requiredstates',
	fields: ['name', 'value']
});

glu.defView('RS.incident.PlaybookTemplate', {
	cls: 'playbooktemplate',
	hidden: '@{!pSirPlaybookTemplatesTemplateBuilderChange}',
	itemId: 'playbookTemplate',
	padding : '15 0',
	layout : 'fit',
	dockedItems : [{
		xtype: 'tbtext',
		cls: 'rs-display-name',
		text: '@{wikiTitle}',
		margin : '0 0 15 0',
		padding : '0 20',
	},{
		xtype : 'toolbar',
		padding : '0 15',
		cls : 'rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items : [{
			iconCls: 'rs-icon-button icon-file-text',
			text: '~~viewFileOptions~~',
			menu: [{
				name: 'save',
				width : 200
			}, 'saveAs', 'copy', {
				xtype: 'menuseparator',
			}, 'preview', {
				xtype: 'menuseparator',
			}, {
				name: 'source', hidden: true
			}, {
				name: 'exitEdit'
			}, {
				name: 'returnToTemplates'
			}]
		},
		'->', {
			xtype: 'tbtext',
			htmlEncode: false,
			text: '@{lockedDisplayText}',
			cls: 'rs-link',
			margin : '0 10 0 0',
			tooltip: '~~lockedByTooltip~~',
			listeners: {
				render: function(tbtext) {
					if (hasPermission(tbtext._vm.accessRights.uadminAccess)) {
						Ext.create('Ext.tip.ToolTip', {
							target: tbtext.getEl(),
							html: tbtext.tooltip
						})

						tbtext.getEl().on('click', function() {
							tbtext.fireEvent('overrideLock', tbtext)
						})
					}
				},
				overrideLock: '@{overrideLock}'
			}
		}, {
			iconCls: 'rs-icon-button icon-plus-sign-alt',
			name: 'addLayoutSection',
			text: '~~section~~',
			cls : 'rs-small-btn rs-btn-light'
		}, {
			xtype: 'tbseparator'
		}, {
			name: 'preview',
			iconCls: 'rs-social-button icon-eye-open',
			text: '',
			tooltip: '~~preview~~'
		}, {
			name: 'save',
			iconCls: 'rs-social-button icon-save',
			text: '',
			tooltip: '~~save~~'
		}, {
			name: 'exitEdit',
			iconCls: 'rs-social-button icon-reply-all',
			text: '',
			tooltip: '~~exitEdit~~'
		}, {
			iconCls: 'rs-social-button icon-repeat',
			name: 'reload',
			text: '',
			tooltip: '~~reload~~'
		}]
	}],
	items: [{
		overflowY: 'scroll',
		flex : 1,
		style : 'border-top:1px solid silver',
		bodyPadding : '8 0 8 15',
		items: [{
			xtype: '@{sectionsLayout}',
			itemId : 'section-layout',
			listeners : {
				afterrender : function(panel){
					//Fired from sectionLayout
					this._vm.on('new_section_added', function(){
						//Lazy way to do scroll.
						setTimeout(function(){
							var d = panel.ownerCt.body.dom;
							var distance = d.scrollHeight - d.offsetHeight;
							panel.ownerCt.body.scroll('b',distance,true);
						},0);
					})
				}
			}
		}, {
			xtype : 'tabpanel',
			cls : 'rs-tabpanel-dark',
			style: 'border: #b5b8c8 1px solid;',
			padding : 15,
			defaults : {
				minHeight : 350,
				flex : 1,
			},
			items : [{
				xtype: 'panel',
				title: '~~procedure~~',
				layout: {
					type : 'hbox',
					align : 'stretch'
				},
				items: [{
					xtype: 'treepanel',
					cls : 'rs-grid-dark',
					flex: 2,
					margin : '0 15 0 0',
					useArrows: true,
					rootVisible: false,
					itemId: 'activityGrid',
					store: '@{procedures}',
					dockedItems: [{
						xtype: 'toolbar',
						dock: 'top',
						padding: '0 0 6 10',
						items: [{
							xtype: 'form',
							itemId: 'sla',
							layout: 'fit',
							width: 400,
							items: [{
								xtype: 'rsnumberfield',
								fieldLabel: '~~sla~~',
								labelWidth: 60,
								precision: 'hour',
								validateOnChange: true,
								listeners: {
									slachanged: function() {
										this.fireEvent('templateslachanged', this, this.up().getForm().getFieldValues());
									},
									templateslachanged: '@{templateSlaChanged}'
								}
							}]
						}, {
							xtype: 'tbtext',
							hidden: '@{!templateSlaWarning}',
							htmlEncode: false,
							padding: '2 0 0 0',
							text: '@{templateSlaWarning}'
						}]
					}],
					plugins: [{
						ptype: 'cellediting',
						clicksToEdit: 2,
						listeners: {
							beforeedit: function(editor, context, eOpts) {
								if (context.record.get('leaf')) {
									context.grid.editingContext = context;
								} else {
									return false;
								}
							}
						}
					}],
					multiSelect: true,
					viewConfig: {
						markDirty: false,
						plugins: {
							ptype: 'treeviewdragdrop'
						},
						listeners: {
							beforedrop: function(node, data, overModel, dropPosition, dropHandlers, eOpts) {
								var r = data.records[0];
								if (dropPosition == 'append') {
									if (!r.isLeaf()) // Nested structure is not allowed
										return false;
								} else if (dropPosition == 'before' || dropPosition == 'after') {
									if (r.isLeaf() && !overModel.isLeaf()) // Moving out of parent is not allowed
										return false;
									if (!r.isLeaf() && overModel.isLeaf()) // Nested structure is not allowed
										return false;
								}
								return true;
							}
						}
					},
					columns: [{
						xtype: 'treecolumn',
						text: '~~phaseActivity~~',
						dataIndex: 'activityName',
						hideable: false,
						sortable: false,
						flex: 2,
						editor: {
							xtype: 'phaseeditor',
							validator: function (val) {
								var context = this.up('treepanel').editingContext;
								if (!context.record.isLeaf() && val.trim() == '') {
									return 'Phase can\'t be empty';
								}
								this.up('#playbookTemplate').fireEvent('validateeditprocedure', this, context, val);
								return context.dupPhaseFound? 'Duplicate phase name found': true;
							}
						}
					}, {
						header: '~~runbook~~',
						dataIndex: 'wikiName',
						hideable: false,
						sortable: false,
						flex: 4,
						renderer: function(value, meta, record) {
							if (value) {
								return Ext.String.format('<a target="_blank" class="sc-link" href="/resolve/jsp/rsclient.jsp?{0}#RS.wiki.Main/name={1}">{2}</a>',
								this._vm.csrftoken, record.get('wikiName'), value);
							}
							return '';
						},
						editor: {
							xtype: 'documentfield',
							name: 'documentName',
							value: '@{..documentName}',
							store: '@{..documentStore}',
							labelWidth: 0,
							labelSeparator: '&nbsp',
							allowBlank: false,
							inline: true,   // inline cell editing
							displayField: 'ufullname',
							valueField: 'ufullname',
							editable: false,
							listeners: {
								searchdocument: '@{..searchRunbook}'
							}
						}
					}, {
						header: '~~required~~',
						dataIndex: 'isRequired',
						flex: 1.2,
						filterable: true,
						renderer:  function(value, meta, record) { //,
							if (value === false || value === 'false' || !record.isLeaf()) {
							return '';
							}
							return '<span class="icon-large icon-ok-sign rs-boolean-renderer"></span>'
						},
						align: 'center',
						editor: {
							xtype: 'combobox',
							editable: false,
							queryMode: 'local',
							store: { type: 'requiredstates', data: [{name: 'Yes', value: true}, {name: 'No', value: false}] },
							displayField: 'name',
							valueField: 'value'
						},
					}, {
						header: '~~sla~~',
						dataIndex: 'days',
						renderer: function(value, meta, record) {
							var days = parseInt(record.getData().days);
							var hours = parseInt(record.getData().hours);
							var minutes = parseInt(record.getData().minutes);
							var daysText = '';
							var hoursText = '';
							var minutesText = '';
							if (days === 1) {
								daysText = 'Day';
							} else if (days > 1) {
								daysText = 'Days';
							} else {
								days = '';
							}
							if (hours === 1) {
								hoursText = 'Hour';
							} else if (hours > 1) {
								hoursText = 'Hours';
							} else {
								hours = '';
							}
							if (minutes === 1) {
								minutesText = 'Minute';
							} else if (minutes > 1) {
								minutesText = 'Minutes';
							} else {
								minutes = '';
							}
							if ((record.get('leaf')) && (days === "") && (hours === "") && (minutes === "")) {
								return 'N/A'
							} else if (!isNaN(days) || !isNaN(hours) ||!isNaN(minutes)) {
								return ([days, daysText, hours, hoursText, minutes, minutesText].filter(function(item) {return item !== ''}).join(' '));
							} else {
								return ''
							}
						},
						flex: 1.5,
						sortable: false,
						hideable: false
					}, {
						header: '~~description~~',
						dataIndex: 'description',
						hideable: false,
						sortable: false,
						flex: 4,
						renderer: function(value, metaData, record, rowIndex, colIndex, store) {
							var val = Ext.htmlEncode(value);
							var cont = (value && value.length > 64)? '... ': '';
							metaData.tdAttr = 'data-qtip="' + Ext.htmlEncode(val) + '"';
							return val.substr(0, 64) + cont;
						},
						editor: 'textarea'
					}, {
						xtype: 'actioncolumn',
						menuDisabled: true,
						sortable: false,
						width: 60,
						align: 'center',
						items: [ {
							glyph: 0xF146,
							iconCls: 'rs-sir-procedure-button-show',
							tooltip : 'Remove',
							handler: function(view, rowIndex, colIndex, item, e, record) {
								this.up('#playbookTemplate').fireEvent('removeactivity', this, record);
							}
						}]
					}]
				},{
					xtype: 'fieldset',
					flex: 1,
					maxHeight: 350,
					padding : '5 10 10',
					margin : 0,
					layout : 'fit',
					title: '~~createActivity~~',
					items: [{
						flex : 1,
						xtype: 'form',
						itemId: 'activityForm',
						layout: {
							type :'vbox',
							align : 'stretch'
						},
						defaults : {
							labelWidth : 120,
							margin : '4 0'
						},
						items: [{
							xtype: 'combo',
							itemId: 'availablePhasesField',
							editable: true,
							name: 'phase',
							queryMode: 'local',
							store: '@{knownPhases}',
							displayField: 'phase',
							valueField: 'phase',
							listeners: {
								expand: function(field, eOpts) {
									this.up('#playbookTemplate').fireEvent('populatephases', this, field, eOpts);
								}
							}
						},{
							xtype: 'textfield',
							name: 'activityName',
							maxLength: 200,
							enforceMaxLength: true
						}, {
							xtype: 'rsnumberfield',
							itemId: 'activitySla',
							fieldLabel: '~~sla~~',
							listeners: {
								slachanged: function() {
									this.fireEvent('activityslachanged', this, this.up().getForm().getFieldValues());
								},
								activityslachanged: '@{activitySlaChanged}'
							}
						}, {
							xtype: 'tbtext',
							hidden: '@{!activitySlaWarning}',
							htmlEncode: false,
							padding: '0 0 0 125',
							text: '@{activitySlaWarning}'
						}, {
							xtype: 'documentfield',
							itemId: 'wikiNameField',
							name: 'wikiName',
							store: '@{runbookStore}',
							allowBlank: false,
							displayField: 'ufullname',
							valueField: 'ufullname',
							editable: true,
							inline: false,  // NOT and inline editing
							listeners: {
								searchdocument: '@{searchRunbook}',
								select : '@{handleRunbookSelect}',
								validatewikiname: '@{validateWikiName}',
								blur: function(v) {
									v.fireEvent('validatewikiname', this, v)
								}
							}
						}, {
							xtype: 'fieldcontainer',
							fieldLabel: '~~required~~',
							defaultType: 'checkbox',
							items: [{
								name: 'isRequired',
								checked: true,
								value: true,
								hideLabel: true
							}]
						}, {
							xtype: 'textarea',
							name: 'description',
							value: '',
							maxLength: 2000,
							flex : 1,
							enforceMaxLength: true,
							fieldLabel: '~~description~~',
							allowBlank: true
						},{
							xtype : 'button',
							margin : '4 0 0 125',
							maxWidth : 120,
							name : 'addActivity',
							cls : 'rs-small-btn rs-btn-dark'
						}]
					}]
				}]
			},{
				title : '~~automationFilter~~',
				xtype : '@{automationFilter}'
			}]
		}]
	}],

	listeners: {
		removeactivity: '@{removeActivity}',
		populatephases: '@{populatePhases}',
		validateeditprocedure: '@{validateEditProcedure}',
		beforedestroy : '@{beforeDestroyComponent}'
	}
});

Ext.define('RS.incident.PlaybookTemplate.PhaseEditor', {
	extend: 'Ext.form.field.Text',
	xtype: 'phaseeditor'
});

glu.defView('RS.incident.PlaybookTemplate.AutomationFilter', {
	minHeight: 400,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : '4 0',
		labelWidth : 130
	},
	items : [{
		xtype: 'combobox',
		name: 'filterValue',
		store: '@{filterStore}',
		displayField: 'name',
		valueField: 'value',
		queryMode: 'local',
		multiSelect: true
	},{
		xtype: 'combobox',
		name: 'order',
		store: '@{orderStore}',
		displayField: 'name',
		valueField: 'value',
		queryMode: 'local',
		editable : false
	}, {
		xtype: 'numberfield',
		name: 'refreshInterval',
		minValue: 5,
	}, {
		xtype: 'numberfield',
		name: 'refreshCountMax',
		minValue: 1,
	}, {
		xtype: 'checkbox',
		name: 'encodeSummary',
		boxLabel: '~~encodeSummary~~',
		hideLabel : true,
		margin : '0 0 0 135'
	},{
		xtype: 'checkbox',
		name: 'selectAll',
		boxLabel: '~~showAllActionTasks~~',
		hideLabel : true,
		margin : '0 0 0 135'
	},{
		xtype: 'checkbox',
		disabled: '@{!selectAll}',
		margin: '0 0 0 155',
		name: 'includeStartEnd',
		hideLabel : true,
		boxLabel: '~~includeStartEndTasks~~'
	},{
		disabled: '@{selectAll}',
		xtype: 'treepanel',
		cls : 'actionTaskFilters rs-grid-dark',
		name: 'actionTasks',
		title: '~~actionTaskFilters~~',
		columns: [{
			xtype: 'treecolumn',
			dataIndex: 'description',
			text: '~~description~~',
			flex: 1,
			renderer: function(value, meta, record) {
				var markup = '';
				var task = record.get('task');
				var tags = record.get('tags');

				if (value && task) {
					markup = value;
					markup += '&nbsp;&nbsp;&nbsp;';
					markup += '<span class="rs-accent">';
					markup += 'Task: ' + record.get('task');
					var namespace = record.get('namespace');

					if (namespace) {
						markup += '&nbsp;&nbsp;&nbsp;';
						markup += 'Namespace: ';
						markup += namespace;
					}

					var wiki = record.get('wiki');

					if (wiki) {
						markup += 'Wiki: ';
						markup += '&nbsp;&nbsp;&nbsp;';
						markup += wiki;
					}

					markup += '</span>';
				} else if (tags) {
					tags = tags.split(',');

					if (tags.length > 0) {
						markup = "#";
						markup += tags.join(' #');
					}
				} else {
					markup = value || record.get('task');
				}

				return markup;
			}
		}],
		rootVisible: false,
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items :['addGroup', 'addActionTask','edit', 'remove',
			{
				xtype: 'checkbox',
				name: 'preserveTaskOrder',
				boxLabel: '~~preserveTaskOrder~~',
				hideLabel: true
			}]
		}],
		flex: 1,
		viewConfig: {
			plugins: {
				ptype: 'treeviewdragdrop'
			}
		},
		plugins: [{
			ptype: 'cellediting',
			pluginId: 'celledit',
			clicksToEdit: 1,
			listeners: {
				beforeEdit: function(editor, e) {
					if (!e.record.isLeaf() && (e.column.dataIndex != 'description' && e.column.dataIndex != 'autoCollapse')) {
						return false
					}
				},
				edit: function(editor, e) {
					e.record.commit()
				}
			}
		}],
		/*listeners: {
			selectionchange: '@{selectionchange}',
			itemdblclick: '@{itemdblclick}'
		},*/
	}]
})
