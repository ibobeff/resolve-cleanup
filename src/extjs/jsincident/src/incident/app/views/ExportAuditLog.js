glu.defView('RS.incident.ExportAuditLog', {
    asWindow: {
        title: '~~exportingAuditLogs~~',
        modal: true,
        padding: 15,
        cls : 'rs-modal-popup',
        minWidth : 600,
        minHeight: 300
    },
    xtype: 'form',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        labelWidth: 150
    },
    items: [{
        xtype: 'fieldcontainer',
        fieldLabel: '~~additionalSir~~',
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        items: [{
            xtype: 'documentfield',
            flex: 8,
            fieldLabel: '',
            name: 'sir',
            value: '@{sir}',
            store: '@{sirStore}',
            allowBlank: true,
            inline: true,   // inline cell editing
            displayField: 'sir',
            valueField: 'sir',
            trigger2Cls: '',
            listeners: {
                select : '@{handleSirSelect}',
                validatesirname: '@{validateSirName}',
                blur: function(v) {
                    v.fireEvent('validatesirname', this, v.store);
                }
            },
            margin: '0 5 0 0'
        }, {
            xtype: 'button',
            cls : 'rs-small-btn rs-btn-dark',
            flex: 2,
            name: 'add'
        }]
    },{
        xtype: 'textarea',
        name: 'sirList',
        readOnly: true
    }, {
        xtype: 'combo',
        name: 'fileType',
        store: '@{fileTypeStore}',
        editable: false,
        allowBlank: false,
        displayField: 'fileType',
        valueField: 'fileType'
    },  {
        xtype: 'textfield',
        name: 'outputFileName',
        allowBlank: false
    }],
    exporting: '@{exporting}',
    setExporting: function() {
        var btn = this.down('#exportBnt');
        btn.setText('Exporting...');
        btn.setDisabled(true);
    },
    buttons: [{
        name: 'exportAll',
        itemId: 'exportBnt',
        formBind: true,
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name :'cancel',
        cls : 'rs-med-btn rs-btn-light'
    }],
});