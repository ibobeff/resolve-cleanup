glu.defView('RS.incident.Dashboard', {
	hidden: '@{!pSirDashboardView}',
	itemId: 'sirDashboardPanel',
	layout: 'vbox',
	defaults: {
		width: '100%'
	},
	cls: 'rs-sir-dashboard',
	items: [{
		height: 80,
		xtype: 'panel',
		hidden: '@{!pSirDashboardCharts}',
		layout: {
			type: 'hbox',
			pack: 'center',
			align: 'stretch'
		},
		defaults: {
			height: '100%',
			style: 'border:1px solid #d2d2d2;border-right-style: none;',
		},
		items: [{
			xtype: 'container',
			width: 300,
			layout: {
				type: 'vbox',
				align: 'center',
				pack: 'center'
			},
			items: [{
				xtype: 'label',
				cls: 'rs-sir-dashboard-banner',
				text: '~~irDashboard~~',
			}, {
				xtype: 'button',
				margin: '10 0 0 0',
				name: 'openGraphicReports',
				cls: 'rs-small-btn rs-btn-light',
				tooltip: '~~openGraphicReports~~',
				text: 'Analytic Reports'
			}]
		}, {
			xtype: 'panel',
			flex: 8,
			tbar: ['->', {
				name: 'configureMetrics',
				text: '',
				glyph: 0xF013,
				tooltip: '~~configureMetrics~~'
			}],
			layout: {
				type: 'hbox',
				align: 'center',
				pack: 'center'
			},
			defaults: {
				flex: 1
			},
			items: [{
				xtype: 'dashboardmetric',
				hidden: '@{!dashboardShowOpen}',
				metricLabel: '~~open~~',
				metric: '@{openMetric}'
			}, {
				xtype: 'dashboardmetric',
				hidden: '@{!dashboardShowInProgress}',
				metricLabel: '~~inProgress~~',
				metric: '@{inProgressMetric}'
			}, {
				xtype: 'dashboardmetric',
				hidden: '@{!dashboardShowClosed}',
				metricLabel: '~~closed~~',
				metric: '@{closedMetric}'
			}, {
				xtype: 'dashboardalertmetric',
				hidden: '@{!dashboardShowPastDue}',
				metricLabel: '~~pastDue~~',
				metric: '@{pastDueMetric}'
			}, {
				xtype: 'dashboardalertmetric',
				hidden: '@{!dashboardShowSLAatRisk}',
				metricLabel: '~~slaAtRisk~~',
				metric: '@{aLAatRiskMetric}'
			}]
		}, {
			xtype: 'panel',
			width: 163,
			dockedItems: [{
				xtype: 'toolbar',
				margin: '0 0 12 0',
				cls: 'rs-dashboard-refresh-tbar',
				dock: 'top',
				items: [{
					xtype: 'combo',
					width: '100%',
					editable: false,
					fieldLabel: '~~refresh~~',
					labelStyle: 'font-weight: bold;',
					labelWidth: 60,
					displayField: 'rate',
					valueField: 'rate',
					value: '@{dashboardrefreshRate}',
					store: '@{refreshRateStore}',
					listeners: {
						change: '@{refreshRateChange}',
					}
				}],
			}],
			layout: {
				type: 'hbox',
				pack: 'center',
				align: 'center'
			},
			defaultType: 'button',
			defaults: {
				style: 'border-style: none; background-color: transparent;',
				text: ''
			},
			items: [{
				glyph: 0xF01e,
				name: 'manualRefresh',
				tooltip: '~~refresh~~',
			}, {
				glyph: 0xF04b,
				name: 'autoRefreshPlay',
				tooltip: '~~play~~',
				margin: '0 30 0 30'
			}, {
				glyph: 0xF04c,
				name: 'autoRefreshPause',
				tooltip: '~~pause~~'
			}]
		}]
	}, {
		xtype: 'grid',
		hidden: '@{!pSirDashboardInvestigationListView}',
		flex: 5,
		name: 'securityIncident',
		displayName: 'Investigations',
		cls: 'rs-grid-dark',
		itemId: 'sirDashboard',
		padding: '10 10 10 10',
		modIndicatorIcon: '<img src="/resolve/images/droparrow.png" style="float:right;width:12px;height:12px;position:relative;left:4px;">',
		modIndicatorCalendarIcon: '<img class="rs-playbook-indicator-img" src="/resolve/images/dropcalendar.png" style="float:right;width:17px;height:15px;position:relative;left:7px;top:-1px">',
		hasEditPermission: function(record, field) {
			if (this.itemId != 'sirDashboard') {
				return;
			}
			var hasAdminRole = (clientVM.user.roles.indexOf('admin') != -1);
			// Sponsorship are owner + team members
			var owner = (record.get('owner') || '').trim();
			var sponsorship =  (record.get('members') || '').split(',').map(function(m) {
				return m.trim();
			});
			sponsorship.push(owner);
			var isOwnerOrTeamMember = sponsorship.indexOf(clientVM.user.name) != -1;
			field = field || this.editingContext.field;
			return  hasAdminRole || isOwnerOrTeamMember || this.up('#sirDashboardPanel').pDashboardSirMap[field];
		},
		getIcon: function(record, icon, field) {
			var pEdit = this.hasEditPermission(record, field);
			if (pEdit) {
				return icon;
			} else {
				return '';
			}
		},
		viewConfig: {
			markDirty: false,
			getRowClass: function(rec) {
				return 'rs-sir-dashboard-record'
			}
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: [{
				name: 'newInvestigation',
				disabled: '@{!pSirInvestigationsCreateNew}',
				text: '~~newSecurityIncident~~',
			},'export',{
				xtype: 'tbseparator'
			}, {
				xtype: 'checkboxgroup',
				height: 24,
				width: 400,
				fieldLabel: 'Show',
				labelWidth: 50,
				columns: 4,
				vertical: false,
				defaultType : 'checkbox',
				items: [
					{checked: true, boxLabel: 'Open', inputValue: 'Open', width: 50},
					{checked: true, boxLabel: 'In Progress', inputValue: 'In Progress', width: 110},
					{checked: true, boxLabel: 'Complete', inputValue: 'Complete', width: 95},
					{checked: false, boxLabel: 'Closed', inputValue: 'Closed', width: 50}
				],
				listeners: {
					change: '@{handleFilterChange}'
				}
			}]
		}],
		buttonAlign: 'left',
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}, {
			ptype: 'cellediting',
			clicksToEdit: 1,
			listeners: {
				beforeedit: function( editor, context, eOpts ) {
					context.grid.editingContext = context;   // Saving editing context to be used later (in controlling the template data from being modified)

					// User who is neither the case owner nor team members is not allowed to edit investigation
					var pEdit = context.grid.hasEditPermission(context.record);
					if (!pEdit) {
						return false;
					}

					// Don't want clicking on the link to enter edit mode
					// Also do not allow columns to be edited if status is closed except to reopen an incident
					if ((context.colIdx == 0) || ((context.field !== 'status' && context.record.get('status').toLowerCase() === 'closed'))) {
						return false;
					}

					if (context.grid.activityEditor) {
						if (context.record.get('sirStarted')) {
							// Tickets are not allowed to be modified once investigation has started
							context.grid.activityEditor.setDisabled(true);
						}
						else {
							context.grid.activityEditor.setDisabled(false);
						}
					}
					return true;
				}
			}
		}],

		store: '@{store}',
		columns: [{
			dataIndex: 'sir',
			text: '~~id~~',
			sortable: true,
			filterable: true,
			flex:.9,
			renderer: function(value, meta, record) {
				if (value) {
					return Ext.String.format('<a target="_blank" class="sc-link" href="/resolve/jsp/rsclient.jsp?{0}#RS.incident.Playbook/sir={1}">{2}</a>',
						record.store.rootVM.rootcsrftoken, record.get('sir'), record.get('sir'));
				}
				return ''
			}
		}, {
			dataIndex: 'playbook',
			text: '~~playbook~~',
			sortable: true,
			filterable: true,
			flex: 1.1,
			editor: {
				xtype: 'documentfield',
				store: '@{playbookTemplateStore}',
				displayField: 'ufullname',
				valueField: 'ufullname',
				editable: false,
				trigger2Cls: '',
				inline: true,   // inline cell editing
				listeners: {
					beforerender: function( v, eOpts ) {
						var grid = this.up('#sirDashboard');
						grid.activityEditor = v;
						if (grid.editingContext) {
							// Can't modify playbook after the investigation has started
							v.setDisabled(!!grid.editingContext.record.get('sirStarted'));
						}
					},
					focus: function(editor) {
						editor.store.load({
							callback: function(records, operation, success) {
								editor.expand();
							}
						});
					},
					select: function(editor) {
						editor.findParentByType('editor').completeEdit();
					}
				}
			},
			renderer: function(val, meta, record, rowIndex, colIndex) {
				val = Ext.String.htmlEncode(val || '');
				var pencilIcon = '';
				var field = this.getHeaderContainer().getHeaderAtIndex(colIndex).dataIndex;

				if (!record.get('sirStarted')) {
					pencilIcon = this.getIcon.call(this, record, this.modIndicatorIcon, field);
				}
				meta.tdCls += ' rs-playbook-modindicator';
				return Ext.String.format('<span style="float:left">{0}</span>{1}', val, pencilIcon);
			}
		}, {
			dataIndex: 'title',
			text: '~~title~~',
			sortable: true,
			filterable: true,
			flex: 1.2,
			editor: {
				xtype: 'textfield',
				allowBlank: false,
				listeners: {
					focus: function(editor) {
						editor.focus();
					}
				}
			},
			renderer: 'htmlEncode'
		}, {
			dataIndex: 'sourceSystem',
			text: '~~origin~~',
			sortable: true,
			filterable: true,
			hidden: true,
			flex: .8
		}, {
			dataIndex: 'id',
			text: RS.common.locale.sysId,
			sortable: true,
			hidden: true,
			sortable: false,
			filterable: false,
			flex: .8
		}, {
			dataIndex: 'investigationType',
			text: '~~type~~',
			sortable: true,
			filterable: true,
			flex: .8,
			editor: {
				xtype: 'combo',
				store: '@{..typeStore}',
				queryMode: 'local',
				displayField: 'type',
				valueField: 'type',
				editable: false,
				listeners: {
					focus: function(editor) {
						this.fireEvent('refreshTypes', null, editor);
					},
					select: function(editor) {
						editor.findParentByType('editor').completeEdit();
					},
					refreshTypes: '@{refreshTypes}'
				}
			},
			renderer: function(val, meta, record, rowIndex, colIndex) {
				val = Ext.String.htmlEncode(val || '');
				var field = this.getHeaderContainer().getHeaderAtIndex(colIndex).dataIndex;
				if (meta.record.get('status').toLowerCase() !== 'closed') {
					meta.tdCls += ' rs-playbook-modindicator';
					return Ext.String.format('<span style="float:left">{0}</span>{1}', val, this.getIcon.call(this, meta.record, this.modIndicatorIcon, field));
				} else {
					return val;
				}
			}
		}, {
			dataIndex: 'severity',
			text: '~~severity~~',
			sortable: true,
			filterable: true,
			flex: .5,
			editor: {
				xtype: 'combo',
				store: ['Critical', 'High', 'Medium', 'Low'],
				editable: false,
				listeners: {
					focus: function(editor) {
						editor.expand();
					},
					select: function(editor) {
						editor.findParentByType('editor').completeEdit();
					}
				}
			},
			renderer: function(val, meta, record, rowIndex, colIndex) {
				val = Ext.String.htmlEncode(val || '');
				var field = this.getHeaderContainer().getHeaderAtIndex(colIndex).dataIndex;
				var mVal = (val.toLocaleLowerCase() == 'close')? 'Closed': val;
				meta.tdCls += ' rs-playbook-modindicator';
				return Ext.String.format('<span style="float:left">{0}</span>{1}', mVal, this.getIcon.call(this, record, this.modIndicatorIcon, field));

			}
		}, {
			dataIndex: 'status',
			text: '~~status~~',
			sortable: true,
			filterable: false,
			flex: .6,
			editor: {
				xtype: 'combo',
				store: ['Open', 'In Progress', 'Complete', 'Closed'],
				editable: false,
				listeners: {
					focus: function(editor) {
						editor.expand();
					},
					select: function(editor) {
						editor.findParentByType('editor').completeEdit();
					}
				}
			},
			renderer: function(val, meta, record, rowIndex, colIndex) {
				val = Ext.String.htmlEncode(val || '');
				var field = this.getHeaderContainer().getHeaderAtIndex(colIndex).dataIndex;
				var mVal = (val.toLocaleLowerCase() == 'close')? 'Closed': val;
				meta.tdCls += ' rs-playbook-modindicator';
				return Ext.String.format('<span style="float:left">{0}</span>{1}', mVal, this.getIcon.call(this, record, this.modIndicatorIcon, field));

			}
		}, {
			dataIndex: 'sla',
			flex: .8,
			text: '~~sla~~',
			renderer: function (slaInSeconds, metaData, record, rowIndex, colIndex, store, view) {
				var days = 0;
				var hours = 0;
				if (slaInSeconds) {
					// 86400 = n_seconds_per_day
					// 3600 = n_seconds_per_hour
					days = Math.floor(slaInSeconds/86400);        // IE does't support Math.trunc
					hours = Math.floor(slaInSeconds%86400)/3600;  // using floor assuming positive inputs
				}
				if (days || hours) {
					return Ext.String.format('{0} Days {1} Hours', days, hours);
				} else {
					return 'None';
				}
			}
		}, {
			dataIndex: 'dueBy',
			text: '~~dueDate~~',
			sortable: true,
			filterable: true,
			flex: .8,
			editor: {
				xtype: 'datetimefield',
				allowBlank: true,
				//minValue : new Date(),
				editable: false,
				submitFormat: 'Y-m-d',
				listeners: {
					focus: function(editor) {
						editor.expand();
					},
					select: function(editor) {
						editor.findParentByType('editor').completeEdit();
					},
					deactivate: function(editor) {
						editor.findParentByType('editor').completeEdit();
					}
				}
			},
			renderer: function(field, meta, record, rowIndex, colIndex) {
				field = Ext.String.htmlEncode(field || '');
				var fld = this.getHeaderContainer().getHeaderAtIndex(colIndex).dataIndex;
				meta.tdCls += 'rs-playbook-modindicator';
				return Ext.String.format('<span style="float:left">{0}</span>{1}', field, this.getIcon.call(this, meta.record, this.modIndicatorCalendarIcon, fld));
			}
		}, {
			dataIndex: 'owner', //display owner/ownerName
			text: '~~owner~~',
			sortable: true,
			filterable: true,
			flex: 1,
			editor: {
				xtype: 'combo',
				store: '@{ownerStore}',
				queryMode: 'local',
				displayField: 'userId', //display owner/ownerName
				valueField: 'userId',
				editable: false,
				//value: '',
				listeners: {
					focus: function(editor) {
						this.fireEvent('refreshMembers', null, editor);
					},
					select: function(editor) {
						editor.findParentByType('editor').completeEdit();
					},
					refreshMembers: '@{refreshMembers}'
				}
			},
			renderer: function(val, meta, record, rowIndex, colIndex) {
				val = Ext.String.htmlEncode(val || '');
				var field = this.getHeaderContainer().getHeaderAtIndex(colIndex).dataIndex;
				if (meta.record.get('status').toLowerCase() !== 'closed') {
					meta.tdCls += ' rs-playbook-modindicator';
					return Ext.String.format('<span style="float:left">{0}</span>{1}', val, this.getIcon.call(this, meta.record, this.modIndicatorIcon, field));
				} else {
					return val
				}
			}
		}, {
			xtype: 'datecolumn',
			dataIndex: 'sysCreatedOn',
			text: '~~createdOn~~',
			sortable: true,
			hidden: false,
			initialShow: true,
			filterable: true,
			flex: 1,
			renderer: function(val) {
				return (val === null) ? '' : Ext.util.Format.htmlEncode(Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat()))
			}
		}, {
			xtype: 'datecolumn',
			dataIndex: 'closedOn',
			text: '~~closedOn~~',
			sortable: true,
			filterable: true,
			flex: 1,
			renderer: function(val) {
				return (val === null) ? '' : Ext.util.Format.htmlEncode(Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat()))
			}
		}, {
			dataIndex: 'closedBy',
			text: '~~closedBy~~',
			sortable: true,
			filterable: true,
			flex: 1,
			renderer: 'htmlEncode'
		}, {
			header: '~~organization~~',
			dataIndex: 'sysOrg',
			initialShow: true,
			sortable: true,
			renderer: function(value, metaData, record) {
				if (value) {
					if(!record.store.id2NameMap) {
						record.store.id2NameMap = {};
						clientVM.user.orgs.forEach(function(e) {
							if(e.uname != 'None')
								record.store.id2NameMap[e.id] = e.uname;
						});
					}
					return RS.common.grid.plainRenderer() (record.store.id2NameMap[value]);
				}
				return '';
			}
		}],
		listeners: {
			edit: '@{handleEdit}'
		}
	}],

	listeners: {
		chartdatachange: '@{chartDataChange}',
		afterrender: '@{loadSettings}',
		beforedestroy : '@{beforeDestroyComponent}'
	},

	scope: '@{scope}',
	setScope: function(val) {
		this.scope = val;
	},
	scopeUnit: '@{scopeUnit}',
	setScopeUnit: function(val) {
		this.scopeUnit = val;
	},

	sirPermissions: '@{sirPermissions}',
	setSirPermissions: function(val) {
		this.sirPermissions = val;
		this.pDashboardSirMap = {
			playbook: this.sirPermissions.sirDashboardPlaybook ? this.sirPermissions.sirDashboardPlaybook.modify : false,
			title: this.sirPermissions.sirDashboardTitle ? this.sirPermissions.sirDashboardTitle.modify : false,
			investigationType: this.sirPermissions.sirDashboardType ? this.sirPermissions.sirDashboardType.modify : false,
			severity: this.sirPermissions.sirDashboardSeverity ? this.sirPermissions.sirDashboardSeverity.modify : false,
			status: this.sirPermissions.sirDashboardStatus ? this.sirPermissions.sirDashboardStatus.modify : false,
			dueBy: this.sirPermissions.sirDashboardDueDate ? this.sirPermissions.sirDashboardDueDate.modify : false,
			owner: this.sirPermissions.sirDashboardOwner ? this.sirPermissions.sirDashboardOwner.modify : false
		}
	}
});

glu.defView('RS.incident.ChangeForm', {
	asWindow: {
		title: '@{title}',
		modal: true,
		padding: 10
	},
	xtype: 'form',
	buttons: [{
		name: 'apply',
		formBind: true
	}, 'cancel'],
	buttonAlign: 'left',
	listeners: {
		afterrender: function(v) {
			this.fireEvent('initview', this, v);
		},
		initview: '@{initView}'
	}
});
glu.defView('RS.incident.Dashboard.PDFConverter',{
	modal: true,
	width : 450,
	height: 500,
	padding : 15,
	cls: 'rs-modal-popup',
	title : '~~exportPDF~~',
	layout : {
		type: 'vbox',
		align : 'stretch'
	},
	items : [{
		xtype : 'component',
		html : '~~info~~',
		margin : '0 0 5 0'
	},{
		xtype: 'grid',
		autoScroll : true,
		flex: 1,
		store : '@{store}',
		name: 'entry',
		cls: 'rs-grid-dark',
		columns: [{
			dataIndex: 'display',
			text: '~~name~~',
			flex: 1
		}],
		selType:'checkboxmodel',
	    selModel : {
	        mode : 'simple'
	    },
	    margin: '0 0 10 0',
	}],
	buttons : [{
		name: 'start',
		cls : 'rs-med-btn rs-btn-dark',
	},{
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light',
	}]
})