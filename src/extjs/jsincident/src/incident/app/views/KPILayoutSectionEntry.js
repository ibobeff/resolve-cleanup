glu.defView('RS.incident.Dashboard.KPILayoutSectionEntry', {
	margin : '5 8 10 8',
	layout: 'anchor',
	style: {
		borderWidth: '1px',
		borderColor: 'silver',
		borderStyle: 'dashed'
	},
	dockedItems: [{
		xtype: 'toolbar',
		cls: 'actionBar rs-dockedtoolbar',
		items: ['->', {
			iconCls: 'rs-social-button icon-remove-sign',
			name: 'remove',
			text: '',
			tooltip: '~~deleteSection~~'
		}]
	}],
	items:[{
		xtype: '@{chartSetting}',
		anchor: '100%',
		margin: 10
	}]
});
