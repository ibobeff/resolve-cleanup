glu.defView('RS.incident.PlaybookTemplateRead', {
	parentLayout: 'basePlaybook',
	propertyPanel: {
		padding : '15 0',
		dockedItems : [{
			margin : '0 0 15 0',
			padding : '0 20',
			layout: 'hbox',
			items: [{
				xtype: 'tbtext',
				cls: 'rs-display-name',
				text: '@{returnTitle}',
			}, {
				xtype: 'tbtext',
				text: '~~readOnlyTxt~~'
			}]
 		}, {
			xtype : 'toolbar',
			margin : '0 15 0 15',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
 			},
			items : [{
				iconCls: 'rs-icon-button icon-file-text',
				text: '~~viewFileOptions~~',
				menu: [{
					name: 'edit',
					disabled: '@{!editIsEnabled}',
					width : 200
				}, {
					xtype: 'menuseparator',
				}, {
					name: 'copy',
					disabled: '@{!copyIsEnabled}'
				}, {
					xtype: 'menuseparator',
				}, 'preview', {
					xtype: 'menuseparator',
				}, {
					name: 'returnToTemplates'
				}]
			},
			'->', {
				name: 'preview',
				iconCls: 'rs-social-button icon-eye-open',
				text: '',
				tooltip: '~~preview~~'
 			}, {
				name: 'edit',
				iconCls: 'rs-social-button icon-pencil',
				text: '',
				tooltip: '~~edit~~'
 			}, {
				iconCls: 'rs-social-button icon-repeat',
				name: 'reload',
				text: '',
				tooltip: '~~reload~~'
			}]
		}]
	}
})
