glu.defView('RS.incident.Dashboard.KPI', {
	cls: 'rs-sir-dashboard',
	dockedItems: [{
		xtype: 'toolbar',
		padding : '10 25 0 15',
		defaults: {
			margin: '0 5 0 5'
		},
		items: [{
			xtype: 'label',
			cls: 'rs-sir-dashboard-banner',
			text: '@{bannerText}'
		}, '->', {
			xtype: 'panel',
			hidden: '@{!viewMode}',
			layout: 'hbox',
			defaultType: 'button',
			defaults: {
				style: 'border-style: none; background-color: transparent;',
				text: ''
			},
			items: [{
				glyph: 0xF01e,
				padding: '5',
				name: 'manualRefresh',
				tooltip: '~~refresh~~'
			}, {
				xtype: 'combo',
				width: 80,
				editable: false,
				labelStyle: 'font-weight: bold;',
				displayField: 'rate',
				valueField: 'rate',
				value: '@{dashboardrefreshRate}',
				store: '@{refreshRateStore}',
				listeners: {
					change: '@{refreshRateChange}'
				}
			}]
		}, {
			name: 'addSection',
			hidden: '@{viewMode}',
			iconCls: 'rs-icon-button icon-plus-sign-alt',
			cls : 'rs-small-btn rs-btn-light'
		}, {
			name: 'save',
			hidden: '@{viewMode}',
			iconCls: 'rs-social-button icon-save',
			text: '',
			tooltip: '~~save~~'
		}, {
			iconCls: 'rs-social-button icon-reply-all',
			hidden: '@{viewMode}',
			name: 'exitEdit',
			text: '',
			tooltip: '~~exitEdit~~'
		}, {
			glyph: 0xF013,
			hidden: '@{!viewMode}',
			name: 'configureSettings',
			text: '',
			tooltip: '~~configureSettings~~'
		}, {
			name: 'refreshSettings',
			hidden: true, // '@{viewMode}', to-be-supported
			iconCls: 'rs-social-button icon-repeat',
			text: '',
			tooltip: '~~refresh~~'
		}]
	}],
	layout: {
		type: 'card',
		deferredRender: true
	},
	activeItem: '@{activeItem}',
	defaults: {
		hideMode: 'offsets'
	},
	items: [{
		xtype: '@{kpiView}'
	}, {
		xtype: '@{kpiLayout}'
	}]
});
