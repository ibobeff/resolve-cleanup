glu.defView('RS.incident.ExportDashboard', {   
    title: '~~export~~',
    modal: true,
    padding: 15,
    cls : 'rs-modal-popup',
    width : 600, 
    minHeight : 500,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        labelWidth: 150
    },
    items: [{
        xtype: 'textfield',
        name: 'outputFileName',
        allowBlank: false
    },{
        xtype: 'combo',
        name: 'fileType',
        store: '@{fileTypeStore}',
        editable: false,
        allowBlank: false,
        displayField: 'fileType',
        valueField: 'fileType'
    },{
        xtype : 'component',
        hidden : '@{fileTypeIsNotPDF}',  
        html : '~~info~~',
        margin : '0 0 5 0'
    },{
        xtype: 'grid',
        hidden : '@{fileTypeIsNotPDF}',  
        autoScroll : true,
        flex: 1,
        store : '@{columnSelectionStore}',
        name: 'entry',
        cls: 'rs-grid-dark',
        columns: [{
            dataIndex: 'display',
            text: '~~name~~',
            flex: 1
        }],
        selType:'checkboxmodel',
        selModel : {
            mode : 'simple'
        },
        margin: '0 0 10 0',
    }],   
    buttons: [{
        name: 'exportAll',      
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name :'cancel',
        cls : 'rs-med-btn rs-btn-light'
    }],
});