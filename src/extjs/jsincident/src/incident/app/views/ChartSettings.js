glu.defView('RS.incident.Dashboard.ChartSettings', {
	xtype: 'form',
	trackResetOnLoad: true,
	itemId: 'chartsettings',
	target: '@{target}',
	chartId: '@{chartId}',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		labelWidth: 175
	},

	items: [{
		xtype: 'combo',
		name: 'chartType',
		store: '@{chartTypeFilterStore}',
		queryMode: 'local',
		displayField: 'chartType',
		valueField: 'chartType',
		editable: false
	}, {
		xtype: 'fieldcontainer',
		hidden: '@{hideScopeConfig}',
		fieldLabel: '~~scope~~',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'numberfield',
			itemId: 'scopeValue',
			name: 'scope',
			allowBlank: false,
			labelSeparator: '',
			fieldLabel: '',
			anchor: '100%',
			flex: 2,
			margin: '0 5 0 0',
			minValue: 1,
			maxValue: '@{maxScope}'
		}, {
			xtype: 'combo',
			name: 'scopeUnit',
			labelSeparator: '',
			fieldLabel: '',
			width: 90,
			editable: false,
			store: '@{scopeUnitFilterStore}',
			queryMode: 'local',
			displayField: 'scopeUnit',
			valueField: 'scopeUnit',
			listeners: {
				select: '@{scopeUnitChange}'
			}
		}]
	}, {
		xtype: 'fieldcontainer',
		hidden: '@{hideScopeConfig}',
		labelSeparator: '',
		fieldLabel: ' ',
		items: [{
			xtype: 'label',
			style: 'font-style:italic;',
			text: '@{tips}'
		}]
	}, {
		xtype: 'numberfield',
		hidden: '@{hideColumnConfig}',
		name: 'numberOfCols',
		allowBlank: false,
		anchor: '100%',
		minValue: '@{minNColumn}',
		maxValue: '@{maxNColumn}'
	}, {
		xtype: 'agebucketbuilder',
		graphType: '@{graphType}',
		unitStore: '@{timeframeUnitFilterStore}',
		hidden: '@{hideAgeBucketConfig}',
		listeners: {
			validitychange: '@{agingBucketValidityChange}'
		}
	},{
		xtype:'fieldset',
		itemId: 'filtersSet',
		title: '~~filters~~',
		defaultType: 'chartfiltercombo',
		defaults: {
			labelWidth: 175
		},
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: []
	}, {
		xtype: 'fieldcontainer',
		fieldLabel: '~~graphType~~',
		defaultType: 'radiofield',
		layout: 'hbox',
		items: [{
			boxLabel: '~~barGraph~~',
			checked : true,
			name: 'graphType',
			margin: '0 20 0 0',
			inputValue: 'Bar Graph'

		}, {
			boxLabel: '~~pieChart~~',
			checked : false,
			name: 'graphType',
			inputValue: 'Pie Chart'
		}]

	}],

	dockedItems: [{
		xtype: 'toolbar',
		dock: 'bottom',
		hidden: '@{layoutContext}',
		defaults: {
			cls: 'rs-med-btn rs-btn-light'
		},
		items: ['->', {
			name : 'apply',
			cls : 'rs-med-btn rs-btn-dark'
		}, 'cancel']
	}],

	listeners: {
		beforerender: function() {
			var form = this.down('#chartsettings');
			this.fireEvent('loadform', this, this);
		},
		calculateposition: '@{calculatePosition}',
		loadform: '@{loadForm}',
		dirtychange: '@{dirtyChange}'
	},

	fValid: '@{fvalid}',
	setFValid: function(val) {
		this.down('#scopeValue').clearInvalid();
	},

	formLoaded: '@{formLoaded}',
	setFormLoaded: function(val) {
		this.getForm().reset();
	},
	alignPosition: '@{alignPosition}',
	setAlignPosition: function(pos) {
		this.alignPosition = pos;
	},
	asWindow: {
		title: '~~chartSettings~~',
		width: 590,
		cls : 'rs-modal-popup',
		padding : 15,
		closeAction: 'hide',
		modal: true,
		listeners: {
			boxready: function(win) {
				var me = this.down('#chartsettings');
				me.fireEvent('calculateposition');
				win.alignTo(me.target.getEl().dom.id, me.alignPosition);
				}
			}
	}
});
