glu.defView('RS.incident.PlaybookTemplates', {
	layout: 'fit',
	padding : 15,
	items: [{
		xtype: 'grid',
		hideSelModel: '@{!pSirPlaybookTemplatesTemplateBuilderView}',
		hidden: '@{!pSirPlaybookTemplatesView}',
		cls : 'rs-grid-dark',
		displayName: '~~playbookTemplates~~',
		dockedItems: [{
			xtype: 'toolbar',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: [{
				name: 'createTemplate',
				disabled: '@{!pSirPlaybookTemplatesCreate}'
			}, {
				name: 'inactivate'
			}, {
				name: 'activatePB',
				hidden: '@{!inactiveColumnShow}'
			}, {
				name: 'deleteTemplate',
				hidden: true
			}]
		}],
		name: 'template',
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true,
			useWindowParams: false
		}, {
			ptype: 'pager'
		}, {
			ptype: 'columnautowidth'
		}],

		stateId: 'rsplaybookTemplates',
		store: '@{store}',
		columns: '@{columns}',
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id'
		},
		listeners: {
			editAction: '@{editTemplate}'
		}
	}]
});