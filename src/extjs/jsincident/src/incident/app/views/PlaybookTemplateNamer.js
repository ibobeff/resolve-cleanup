glu.defView('RS.incident.PlaybookTemplateNamer', {
	title: '~~newTemplate~~',
	cls : 'rs-modal-popup',
	modal: true,
	height : 250,   
    width: 600, 
	xtype: 'form',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	padding : 15,
	defaultType: 'textfield',
	items: [{
		xtype: 'documentfield',
		trigger2Cls: '',
		fieldLabel: '~~namespace~~',
		value: '@{namespace}',
		store: '@{namespaceStore}',
		queryMode: 'remote',
		displayField: 'unamespace',
		valueField: 'unamespace',
		allowBlank: false,
		regex: RS.common.validation.TaskNamespace,
		regexText: '~~invalidNamespace~~'
	}, {
		xtype: 'textfield',
		fieldLabel: '~~name~~',
		value: '@{name}',
		regex: RS.common.validation.TaskName,
		regexText: '~~invalidName~~',
		allowBlank: false
	}],
	
	buttons: [{
		name: 'create',
		cls : 'rs-med-btn rs-btn-dark',
		formBind: true
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]	
});
