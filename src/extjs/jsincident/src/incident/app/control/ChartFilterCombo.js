Ext.define('RS.incident.Dashboard.ChartFilterCombo', {
	extend: 'Ext.form.field.ComboBox',

	alias: 'widget.chartfiltercombo',
	editable: false,
	queryMode: 'local',

	setValue: function(value) {
		var me = this;
		// If 'All' is selected, deselect all but 'All'
		// If an item other than 'All' is selected, deselect 'All' if it is select
		// In other words, 'All' and other options are mutually exclusive.
		var locale = RS.incident.Dashboard.locale.ChartSettings;
		if (Array.isArray(value) && value.length > 1) {
			if(me.allSelected) { // || me.getValue()[0] == locale.all) {
				arguments[0] = value.filter(function(r) {
					return r.get(me.valueField) == locale.all;
				})
			} else {
				arguments[0] = value.filter(function(r) {
					return r.get(me.valueField) != locale.all;
				})
			}
		}
		arguments[1] = true;  // Make sure  the row will be selected/deselected in the list
		this.callParent(arguments);
	},

	listeners: {
		beforeselect: function( combo, record, index, eOpts ) {
			var locale = RS.incident.Dashboard.locale.ChartSettings;
			combo.allSelected = record.get(combo.valueField) == locale.all? true: false;
		}
	},

	initComponent: function() {
		var me = this;
		var locale = RS.incident.Dashboard.locale.ChartSettings;
		Ext.apply(this, {
			value: this.value || locale.all,
			fieldLabel: locale['investigation'+Ext.String.capitalize(me.filterOn)],
			name: me.filterOn+'Filter',
			displayField: me.filterOn,
			valueField: me.filterOn,
			multiSelect: true,
			allowBlank: false
		});
		me.callParent(arguments);
	}
});