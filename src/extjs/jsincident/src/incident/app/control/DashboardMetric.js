Ext.define('RS.incident.Dashboard.Metric', {
    extend: 'Ext.container.Container',
    alias: 'widget.dashboardmetric',
    defaultType: 'label',
    layout: {
        type: 'vbox',
        align: 'center',
        pack: 'top'
    },
    defaults: {
        width: '100%',
        flex: 1
    },
    items: [{
        itemId:  'metric',
        style: 'text-align: center;font-size: 23px;font-weight: bold;'
    }, {
        itemId: 'metricLabel',
        style: 'text-align: center;font-size: 13px;font-weight: bold;color: #686eb3;'
    }],
    initComponent: function() {
        this.items[0].text = this.metric;
        this.items[1].text = this.metricLabel;
        this.callParent(arguments);
    },

    setMetric: function(val) {
        this.down('#metric').setText(val);
    }
});


Ext.define('RS.incident.Dashboard.AlertMetric', {
    extend: 'RS.incident.Dashboard.Metric',
    alias: 'widget.dashboardalertmetric',
    initComponent: function() {
        this.items[0].style = 'text-align: center;font-size: 22px;font-weight: bold;color:#f06f6f';
        this.callParent(arguments);
    }
});