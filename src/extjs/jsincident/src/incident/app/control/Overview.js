Ext.define('RS.investigation.Overview', {
	extend: 'Ext.form.Panel',
	itemId: 'overview',
	xtype: 'overview',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		margin: '0 0 40 0'
	},
	items: [{
		xtype: 'fieldset',
		title: 'Summary',
		border: false,
		defaultType: 'displayfield',
		defaults: {			
			labelWidth: 130,
			margin : '5 5 0'
		},
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			fieldLabel: 'ID',
			name: 'id'
		}, {
			fieldLabel: 'Serverity',
			name: 'severity'
		}, {

			fieldLabel: 'Created',
			name: 'created'
		}, {

			fieldLabel: 'Data Compromised',
			name: 'comprosised'
		}, {

			fieldLabel: 'Investigation Type',
			name: 'type'
		}]
	}, {
		xtype: 'fieldset',
		title: 'Team',
		border: false,
		defaults: {
			xtype: 'displayfield',
			labelWidth: 130,
			margin : '5 5 0'
		},
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			fieldLabel: 'Created By',
			name: 'createdBy'
		}, {
			fieldLabel: 'Owner',
			name: 'owner'
		}, {

			fieldLabel: 'Members',
			itemId: 'members',
			name: 'members'
		}, {
			xtype: 'button',
			cls: 'sc-add-member-btn rs-small-btn rs-btn-dark',	
			text : 'Add Member',
			itemId: 'addMemberBtn',
			handler: function(btn) {
				var getCandidates = function(members, curMem) {
					var candidates = [];
					members.forEach(function(e) {
						if (curMem.indexOf(e) == -1) {
							candidates.push(e);
						}
					});
					return candidates;
				};
				var curMem = btn.up().down('#members').getValue().split('<br />') || [],
					members = btn.up('viewport').data.prospectiveMembers
				Ext.create('Ext.window.Window', {
					title: 'Add Member',
					height: 140,
					width: 300,
					modal: true,
					items: [{
						xtype: 'form',
						cls: 'sc-form',
						padding: '10 10 10 10',
						layout: {
							type: 'vbox',
							align: 'stretch'
						},
						itemId: 'addMember',
						items: {
							xtype: 'combo',
							itemId: 'memberInput',
							editable: false,
							fieldLabel: 'Member',
							labelWidth: 70,
							allowBlank: false,
							store: getCandidates(members, curMem)
						},
						buttons: [{
								text: 'Add',							
								viewport: btn.up('viewport'),
								formBind: true,
								handler: function(b) {
									var newMember = b.up('form').down('#memberInput').getValue();
									curMem.push(newMember);
									Ext.Ajax.request({
										scope: this,
										url: '/resolve/service/demo/saveJson',
										params: {
											companyName: 'Resolve',
											iRid: clientVM.activeScreen.parentVM.name,
											jsonContent: Ext.encode(curMem),
											type: 'members'
										},
										success: function(response, opts) {
											this.viewport.data.members = curMem;
											this.viewport.down('#members').setValue(curMem.join('<br />'));
											this.viewport.fireEvent('memberadded', curMem, newMember);
											this.up('window').close();
										},
										failure: function(response, opts) {
											clientVM.displayFailure(response);
										}
									});
								}
							}, {
							  	text: 'Cancel',
							  	handler: function(b) {
							  		b.up('window').close();
							  	}
							}
						]
				    }]
				}).show();
			}
		}]
	}, {
		xtype: 'fieldset',
		title: 'Associated Investigations',
		border: false,
		defaults: {
			xtype: 'displayfield',
			labelWidth: 130,
			fieldStyle: 'font-weight:normal;',
			margin : '5 5 0'
		},
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			name: 'associatedInvestigations'
		}]
	}, {
		xtype: 'fieldset',
		title: 'Associated SIEM Records',
		border: false,
		defaults: {
			xtype: 'displayfield',
			labelWidth: 130,
			fieldStyle: 'font-weight:normal;',
			margin : '5 5 0'
		},
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			name: 'associatedSIEMRecords'
		}]
	}, {
		xtype: 'fieldset',
		title: 'Associated ITSM  Tickets',
		border: false,
		defaults: {
			xtype: 'displayfield',
			labelWidth: 130,
			fieldStyle: 'font-weight:normal;',
			margin : '5 5 0'
		},
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			name: 'associatedITSMTickets'
		}]
	}]
});
