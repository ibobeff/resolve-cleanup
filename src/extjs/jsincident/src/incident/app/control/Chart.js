Ext.define('RS.incident.Dashboard.Chart', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.chart',
	style: 'border-style: solid; border-width: 1px;border-color: #dadadf',

	layout: 'anchor',
	config :{
		chartId: '',
		chartConfigs: undefined,
		chartSettingsHandler: undefined
	},

	setChartConfigs: function(configs) {
		this.chartConfigs = configs;
		this.renderChart(configs);
	},

	setRefreshMonitor: function() {
		if (!this.up('panel').hidden) {
			this.refreshchart();
		}
	},

	initComponent: function() {
		var me = this;
		this.dockedItems =  [{
			xtype: 'toolbar',
			dock: 'top',
			height: 35,
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'label',
				itemId: 'chartTitle',
				style: 'text-align: right;font-weight: bold;margin-top:5px',
				flex: 1,
				text: this.chartTitle,
			}, {
				xtype: 'toolbar',
				flex:  0.6,
				layout: 'hbox',
				items: ['->', {
					glyph: 0xF013,
					itemId: 'gearIcon',
					hidden: true,
					tooltip: RS.incident.Dashboard.locale.ChartSettings.configureChartSettings,
					chartId: this.chartId,
					graphType: me.graphBy,
					handler: me.chartSettingsHandler
				}]
			}],
			listeners: {
				afterrender: function(tb) {
					var me = tb;
					me.getEl().on({
						'mouseenter': function (e, t, eOpts) {
								me.down('#gearIcon').show();
						},
						'mouseleave': function() {
							me.down('#gearIcon').hide();
						}
					});
				}
			}
		}];
		this.callParent(arguments);
		this.on('refreshchart', this.refreshchart, this);
	},

	setTitle: function(title) {
		this.down('#chartTitle').setText(title);
	},

	// Sample of configs object
	// configs = {
	//		ageBuckets: "0 Days-1 Days,3 Days,7 Days"
	//		chartType: "Investigations by Type" // "Investigations by age", "Investigations 'Investigations by Severity'"
	//		graphType: "Pie Chart",             // "Bar Graph"
	//		id: 1
	//		numberOfCols: 6
	//		ownerFilter: "All"
	//		scope: "23"
	//		scopeUnit: "Days"
	//		severityFilter: "All"
	//		statusFilter: "All"
	//		teamFilter: "All"
	//		typeFilter: "All"
	// }
	renderChart: function(configs) {
		this.removeAll();
		this.setTitle(configs.chartType);
		this.add({
			xtype: configs.graphType == RS.incident.Dashboard.locale.ChartSettings.pieChart? 'sirpiechart': 'sircolumnchart',
			graphBy: configs.chartType,
			filters: configs,
			itemId: 'graphView'
		});
	},
	refreshchart: function() {
		this.down('#graphView').refreshchart();
	}
});

Ext.define('RS.incident.Dashboard.SIRChart', {
	extend: 'Ext.Component',
	alias: 'widget.sirchart',
	anchor: '100% 100%',

	initComponent: function() {
		this.dataByTypeFuncMap = {};
		this.dataByTypeFuncMap[RS.incident.locale.investigationByType] = function () {
			var dashboard = this.up('#sirDashboardPanel');
			var payload = this.createFilters(this.up('chart').chartConfigs);
			this.getReport('/resolve/service/playbook/getsircountbytypereport', payload);
		};
		this.dataByTypeFuncMap[RS.incident.locale.investigationBySeverity] = function() {
			var dashboard = this.up('#sirDashboardPanel');
			var payload = this.createFilters(this.up('chart').chartConfigs);
			this.getReport('/resolve/service/playbook/getsircountbyseverityreport', payload);
		};
		this.dataByTypeFuncMap[RS.incident.locale.investigationByAge] = function() {
			var chartConfigs = this.up('chart').chartConfigs;
			var payload = this.createFilters(chartConfigs);
			payload.ageBuckets = chartConfigs.ageBuckets;
			this.getReport('/resolve/service/playbook/getsircountbyagereport', payload);
		};
		this.dataByTypeFuncMap[RS.incident.locale.investigationOverTimes] = function() {
			var chartConfigs = this.up('chart').chartConfigs;
			var payload = this.createFilters(chartConfigs);
			payload.ageBuckets = chartConfigs.ageBuckets;
			payload.numberOfCols = chartConfigs.numberOfCols;
			this.getReport('/resolve/service/playbook/getsircountovertimes', payload);
		};
		this.dataByTypeFuncMap[RS.incident.locale.investigationByTeamMember] = function() {
			var dashboard = this.up('#sirDashboardPanel');
			var payload = this.createFilters(this.up('chart').chartConfigs);
			this.getReport('/resolve/service/playbook/getsircountbyteammember', payload);
		};
		this.dataByTypeFuncMap[RS.incident.locale.investigationByStatus] = function() {
			var dashboard = this.up('#sirDashboardPanel');
			var payload = this.createFilters(this.up('chart').chartConfigs);
			this.getReport('/resolve/service/playbook/getsircountbystatusreport', payload)
		};
		this.dataByTypeFuncMap[RS.incident.locale.investigationBySLA] = function() {
			var dashboard = this.up('#sirDashboardPanel');
			var payload = this.createFilters(this.up('chart').chartConfigs);
			RS.incident.model.DashboardSettings.load(1, {  // Load settings from cache
				scope: this,
				callback: function (record, operation, success) {
					payload.nDaysBAtRisk = success ? record.get('sLAatRiskPolicy') : 5; // 5 is the default
					this.getReport('/resolve/service/playbook/getsircountbyslareport', payload);
				}
			});
		};
		this.dataByTypeFuncMap[RS.incident.locale.investigationByWorkload] = function() {
			var dashboard = this.up('#sirDashboardPanel');
			var payload = this.createFilters(this.up('chart').chartConfigs);
			this.getReport('/resolve/service/playbook/getsircountbyworkloadreport', payload);
		};

		var me = this;
		this.chartConfig = Ext.apply({}, {
			theme: 'light',
			valueField: 'value',
			colorField: 'color',
			categoryAxis: {
				labelRotation: 40
			},
			chartCursor: {
				cursorAlpha: 0.3,
				zoomable: false,
				categoryBalloonEnabled: true
			},
			graphs: [{
				valueField: 'value',
				colorField: 'color'
			}]
		 });

		this.callParent(arguments);
		this.on('boxReady', this.boxReady, this);
		this.resizeHandler = function() {
			me.up('panel').doLayout();
		};
		Ext.EventManager.onWindowResize(this.resizeHandler, this);

		this.on('destroy', function() {
			// Remove resize handler when chart is destroyed.
			Ext.EventManager.removeResizeListener(this.resizeHandler, this);
		});
	},


	boxReady: function(v, width, height, eOpts ) {
		Ext.apply(v.chartConfig, {
			width: width,
			height: height,
			categoryField: v.graphBy.split(' ')[1].toLowerCase()
		});
		v.createChart(this.getId(), v.chartConfig);
		v.syncChart();
	},


	syncChart: function() {
		this.dataByTypeFuncMap[this.graphBy].call(this);
	},

	byTypeColors: function(k){
		// By type must use customized colors
		return {
			'Malware': '#118fde',
			'Phishing': '#ef9911',
			'Network Protection': '#2b9221',
			'Access Protection': '#b52753',
			'Data Protection': '#ae73e0'
		}[k];
	},

	byServerityColors: function(k) {
		// By serverity must use customized colors
		return {
			Critical: '#c03131',
			High: "#f17954",
			Medium: '#e0dc3a',
			Low: '#85cc47'
		}[k];
	},

	getColorCode: function() {
		var colorSet = [
			'#85CC47', '#E0DC3A', '#F17954', '#C03131', '#F80606', '#88221B', '#880F07', '#722380', '#44164C', '#1D3ADC',
			"#FF6600", "#FCD202", "#B0DE09", "#0D8ECF", "#2A0CD0", "#CC0000", "#00CC00", "#821432", "#0C5B8E", "#FAFEAB", "#2EAB64", "#F8298F", "#AF661D",
			"#2E7CEF", "#7F9004", "#F947FB", "#FA847B", "#E2A3F7", "#9CC4FA", "#4E3049", "#DAF756", "#288D98", "#FB1558", "#2E9F0A", "#80FAC4", "#900E04",
			"#F9B873", "#69FB81", "#946AFE", "#EBFBE2", "#760F5B", "#E983B4", "#FE56D0", "#F8790D", "#2A3F92", "#2A650F", "#32555B", "#653F09", "#0B613A",
			"#EDDA60", "#2E9A7C", "#4A382D", "#B0EF96", "#8A94E8", "#DA78F9", "#BFECF9", "#4ECEF4", "#B50A7E", "#FE6183", "#BB2839", "#F77D50", "#552D6E",
			"#F09C3C", "#178FC2", "#29CAB6", "#525B00", "#F4A896", "#93C90E", "#FFE3BC", "#6D3527", "#A4FAEC", "#BD0BA3", "#897606", "#B8A50C", "#D8C6EF",
			"#BF276A", "#363A65", "#B088FB", "#0D82D9", "#FE4E4F", "#7C2F08", "#25E444", "#C91D1E", "#D5F30B", "#25B44C", "#EAE32F", "#C942D1", "#D4F3BD",
			"#1DDF9E", "#EF84DE", "#F967A6", "#D45311", "#20F7DC", "#BBEF6E", "#08BBCB", "#FCE089", "#725FD9", "#84F75F", "#3AC092", "#7B3443", "#AC4815",
			"#FECA64", "#760A41", "#D19C10", "#FFA67C", "#21463A", "#63A2EA", "#FD5C36", "#CB7603", "#248543", "#497F04", "#F3FA7F", "#075F77", "#91FEA9",
			"#5842A6", "#1C4305", "#7D5A02", "#FE88A5", "#4C494C", "#C859F4", "#642757", "#E9B2E4", "#F8C893", "#1D6E59", "#F39857", "#BCFE54", "#8B1E1C",
			"#ACED0C", "#1C64AB", "#A2258D", "#28BC1A", "#FB34D4", "#224480", "#F79FA6", "#E090FA", "#BFD514", "#464931", "#345274", "#85FBDC", "#91D0F1",
			"#F2645A", "#23FA71", "#FC0E23", "#A1AB00", "#9B2063", "#C2FFD6", "#5C4C05", "#DCB9FF", "#0DB4EB", "#EE0B3E", "#3D3D52", "#6C74FC", "#44D481",
			"#F029AF", "#9BEB7E", "#6AD511", "#F8307C", "#C4EA81", "#FFE1D1", "#667A0F", "#D1D2E8", "#D71946", "#3FA249", "#643286", "#ACB0F8", "#129B69",
			"#F9A0C1", "#3BB0AF", "#03741C", "#5E3E41", "#237F53", "#234226", "#453C7F", "#1FAED2", "#F5CE49", "#3B66E0", "#F1822C", "#268C16", "#E95DC1",
			"#8B4A1A", "#2C625A", "#A7700B", "#29739D", "#A947CC", "#0259AF", "#ED70B6", "#AA2655", "#664229", "#A55AEA", "#0EB335", "#FF5BB1", "#642345",
			"#DFEEF2", "#FC336D", "#B3093F", "#E8568A", "#DD329E", "#197E97", "#234052", "#950469", "#C4F9B6", "#61FF97", "#C3AAFE", "#F72BA1", "#6C2B6A",
			"#27F8BB", "#3C5D0A", "#2ACE12", "#6AEAA4", "#E986CE", "#70BA01", "#EC6CF5", "#076C2E", "#F1AD3F", "#8A2646", "#2B9CDE", "#BF73FC", "#FE8566",
			"#FBF795", "#418E79", "#397C82", "#3A4AAD", "#702621", "#48C264", "#275F42", "#938AFF", "#7F3C99", "#96083B", "#EC2FBE", "#4A3156", "#F85F23",
			"#39E5B1", "#3473CD", "#D8C3FC", "#D4EC71", "#9C78FF", "#B5220E", "#D20014", "#C382FE", "#F4D2D0", "#53A507", "#EEE5C4", "#4B3286", "#8CA801",
			"#F781FE", "#7C031A", "#D50230", "#A2E3F9", "#A1C415", "#B2A728", "#DE0E84", "#A0E223", "#B7CFFC", "#C20479", "#ACF7F5", "#3CEA78", "#FD8287",
			"#134A9A", "#FAE0AC", "#3E6FDB", "#CBFC5C", "#790A66", "#2D81E2", "#FE3242", "#7060E6", "#FEAE77", "#D1D722", "#3D92DC", "#2C4204", "#D6871C",
			"#FC5DDE", "#62D0FF", "#454621", "#E46904", "#4C2C63", "#FD8F8B", "#55F2D2", "#807517", "#ECFCF6", "#E8AAE7", "#1A5432", "#55ED85", "#FCD98E",
			"#5F4113", "#F5C5DD", "#F7BF6F", "#FC507B", "#3EF9B1", "#6E332F", "#3AEDFE", "#EDFBA0", "#2CA1C7", "#EB8635", "#43B916", "#9C0504", "#CB4309",
			"#CBFEDD", "#1C6932", "#2B6A99", "#75F676", "#ACC2FA", "#860664", "#9A2D9F", "#5082EF", "#2B8A03", "#B047D3", "#F83913", "#0086BB", "#2A4400",
			"#A5F7F1"
		];
		this.colorAssigned = (this.colorAssigned >= colorSet.length)? 0: this.colorAssigned || 0; // Rotate if > colorSet.length; initialized to 0 if not
		return colorSet[this.colorAssigned++];
	},

	graphByMap: function(type) {
		switch(type) {
			case RS.incident.locale.investigationByType:
				return 'byTypeColors';
			case RS.incident.locale.investigationBySeverity:
				return 'byServerityColors';
			default:
				return 'getColorCode';
		}
	},

	getColor: function(graphBy, value) {
		return this[this.graphByMap(graphBy)].call(this, value);

	},

	unpackData: function(recs, graphBy) {
		var me = this;
		var data = [];
		recs.forEach(function (rec) {
			Object.keys(rec).forEach(function(k) {
				var d = {
					value: rec[k],
					color: me.getColor(graphBy, k),
					showInLegend: rec[k]? true: false  // Only show in legend non-zero items
				};
				d[graphBy.split(' ')[1].toLowerCase()] = k;
				data.push(d);
			});
		});
		return data;
	},

	getReport: function(url, params) {
		params = params || {};
		params.orgId = clientVM.orgId;
		Ext.Ajax.request({
			scope: this,
			url: url,
			method: 'POST',
			params: params,
			success: function(resp) {
				var respData  = RS.common.parsePayload(resp);
				if (!respData .success) {
					clientVM.displayError(respData.message);
					return;
				}
				this.chart.dataProvider = this.unpackData(respData .records, this.graphBy);
				if (!this.chart.dataProvider || !this.chart.dataProvider.length) {
					this.chart.addLabel('50%', '50%', RS.incident.Dashboard.locale.ChartSettings.noDataMsg, 'middle', 12);
					if (this.chart.type == 'pie') {
						// Add bogus data
						var dp = {
							showInLegend: false
						};
						dp[this.chart.titleField] = "";
						dp[this.chart.valueField] = 0;
						this.chart.dataProvider.push(dp)
						this.chart.alpha = 0.3;
					}
				} else {
					this.chart.clearLabels();
				}
				this.chart.validateData();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	createFilters: function(chartConfigs) {
		return {
			scopeUnits: chartConfigs.scope,
			unitType: chartConfigs.scopeUnit,
			typeFilter: chartConfigs.typeFilter,
			severityFilter: chartConfigs.severityFilter,
			ownerFilter: chartConfigs.ownerFilter,
			teamFilter: chartConfigs.teamFilter,
			statusFilter: chartConfigs.statusFilter
		};
	},

	createChart: function(id) {
		var me = this;
		this.getCustomChartConfig(this.chartConfig);
		this.chart = AmCharts.makeChart(id, this.chartConfig);
		Ext.EventManager.onWindowResize(function() {
			me.chart.invalidateSize(); // Force the chart to resize to it's current container size.
		});
	},

	setChartInvestigateBy: function(val, silent) {
		var map = {
			'1': 'pieChartInvestigateBy',
			'2': 'column1ChartInvestigateBy',
			'3': 'column2ChartInvestigateBy'
		};
		this.down('#combobox').setValue(val);
		this.graphBy = val;
		if (!silent){
			// control -> viewmodel binding
			this.up('#sirDashboardPanel').fireEvent('chartdatachange', this, map[this.chartId], val);
		}
		if (val != RS.incident.locale.investigationByAge) {
			this.down('#chartTitle').setText('Investigations ' + val);
		} else {
			this.down('#chartTitle').setText(RS.incident.locale.investigationByAge);
		}
		this.refreshchart();
	},

	setHideChartDataPicker: function(val) {
		if (val) {
			this.down('#combobox').hide();
			this.down('#dropDownIcon').show();
		} else {
			var combo = this.down('#combobox');
			combo.show();
			this.down('#dropDownIcon').hide();
		}
	},

	refreshchart: function() {
		this.colorAssigned = 0; // Reset colorAssigned for 'Investigations by age' report.
		this.syncChart();
	}
});

Ext.define('RS.incident.Dashboard.PieChart', {
	extend: 'RS.incident.Dashboard.SIRChart',
	alias: 'widget.sirpiechart',

	getCustomChartConfig: function(chartConfig) {
		Ext.apply(chartConfig, {
			type: 'pie',
			titleField: this.graphBy.split(' ')[1].toLowerCase(),
			labelText: '[[percents]]%',
			labelRadius: 5,
			marginBottom: 0,
			marginTop: 0,
			innerRadius: '30%',
			visibleInLegendField: 'showInLegend',
			legend: {
				position: 'right',
				align: 'center',
				autoMargins: true,
				labelWidth: 160,
				markerSize: 10,
				verticalGap: 0,
				switchable: false,
				valueText: '',
				fontSize: 12
			},
			balloon:{
				fixedPosition:true
			}
		})
	},

	setChartInvestigateBy: function(investigateBy, silent) {
		if (this.chart) {
			this.chart.titleField = investigateBy.split(' ')[1].toLowerCase();
			this.chart.validateNow();
		}

		this.callParent(arguments);
	}
});

Ext.define('RS.incident.Dashboard.ColumnChart', {
	extend: 'RS.incident.Dashboard.SIRChart',
	alias: 'widget.sircolumnchart',

	getCustomChartConfig: function(chartConfig) {
		Ext.apply(this.chartConfig, {
			type: 'serial',
			valueAxes: [{
				integersOnly: true
			}]
		});
		Ext.apply(this.chartConfig.graphs[0], {
			type: 'column',
			columnWidth: 0.5,
			fillAlphas: 1,
			lineAlpha: 0
		});
	},

	setChartInvestigateBy: function(investigateBy, silent) {
		if (this.chart) {
			this.chart.categoryField = investigateBy.split(' ')[1].toLowerCase();
			this.chart.validateNow();
		}

		this.callParent(arguments);
	}
});

Ext.define('RS.incident.Dashboard.AreaChart', {
	extend: 'RS.incident.Dashboard.SIRChart',
	alias: 'widget.sirareachart',

	getCustomChartConfig: function(chartConfig) {
		Ext.apply(this.chartConfig, {
			type: 'serial'
		});
		Ext.apply(this.chartConfig.graphs[0], {
			type: 'line',
			fillAlphas: 0.6,
		});
		Ext.apply(this.chartConfig.categoryAxis, {
			startOnAxis: true
		});
	}
});
