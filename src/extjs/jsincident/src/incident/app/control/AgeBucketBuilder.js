Ext.define('RS.incident.Dashboard.AgingBucket.UnitStore', {
	extend: 'Ext.data.Store',
	fields: [
		{name: 'timeframeUnit', type: 'string'}
	]
});

Ext.define('RS.incident.Dashboard.AgeBucketBuilder', {
	extend: 'Ext.form.FieldSet',
	alias: 'widget.agebucketbuilder',
	itemId: 'ageBucketBuilder',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	items: [],
	frames: [],
	nFrame: 0,
	initComponent: function() {
		var me = this;
		Ext.apply(me, {
			title: RS.incident.Dashboard.locale.AgeBucketBuilder.ageBuckets
		});
		me.callParent(arguments);
		me.add({
			xtype: 'toolbar',
			margin: '5 0 5 0',
			layout: 'hbox',
			items: ['->', {
				text: RS.incident.Dashboard.locale.AgeBucketBuilder.newTimeFrame,
				style: 'border: 1px solid gray;border-radius: 0px;',
				itemId: 'newTimeFrameButton',
				handler: function() {
					var start = me.getNextStartTimeframe();
					var valUnit = start.split(' ');
					me.createPriorToEndTimeframe(start, [valUnit[0]-0+1 /*add 1 to string*/, valUnit[1]].join(' '), true);
				}
			}, {
				text: RS.incident.Dashboard.locale.AgeBucketBuilder.deleteLastTimeframe,
				style: 'border: 1px solid gray;border-radius: 0px;',
				disabled: true,
				itemId: 'deleteTimeframeButton',
				handler: me.deleteLastTimeframe.bind(me)
			}]
		});
	},
	loadData: function(data) {
		// '0 Days-1 Days,3 Days,7 Days,21 Days,365 Days'
		var buckets = data.split(','); // ['0 Days-1 Days','3 Days','7 Days','21 Days','365 Days']
		var bucket1 = buckets.shift().split('-');
		// First entry must always be a time frame with two values, start/end time
		this.createStartToEndTimeframe(bucket1[0], bucket1[1]);
		var priorEndTime = bucket1[1];
		buckets.forEach(function(bucket) {
			this.createPriorToEndTimeframe(priorEndTime, bucket);
			priorEndTime = bucket;
		}, this);
	},

	getData: function() {
		var data = [];
		this.items.items.forEach(function(frame) {
			if(typeof(frame.getData) == 'function') {
				data.push(frame.getData());
			}
		}, this);
		return data.join(',');
	},

	// This method is called after data is loaded
	createStartToEndTimeframe: function(start, end) {
		if (this.nFrame) {
			console.error('There must only be one "FirstTimeFrame"');
			return;
		}
		this.insert(this.nFrame, {
			xtype: 'timeframe',
			start: start,
			end: end,
			order: ++this.nFrame,
			store: this.unitStore
		});
		this.down('#deleteTimeframeButton').setDisabled(true); // First time frame is mandatory
	},

	// This method is called after data is loaded
	createPriorToEndTimeframe: function(start, end, noisy) {
		this.insert(this.nFrame, {
			xtype: 'nextframe',
			start: start,
			end: end,
			order: ++this.nFrame,
			store: this.unitStore
		}, noisy);
		this.down('#deleteTimeframeButton').setDisabled(false);
		this.down('#newTimeFrameButton').setDisabled(this.nFrame >= 10);
	},

	deleteLastTimeframe: function() {
		var selector = '#' + RS.incident.Dashboard.locale.AgeBucketBuilder.column + this.nFrame;
		var removedFrame = this.down(selector);
		if (removedFrame) {
			this.remove(removedFrame);
			if (--this.nFrame == 1) {
				this.down('#deleteTimeframeButton').setDisabled(true);
			}
			this.down('#newTimeFrameButton').setDisabled(false);
		}
	},

	insert: function(nFrame, frame, noisy) {
		var newElem = this.callParent(arguments);
		if (this.frames.length) {
			this.frames[this.frames.length-1].nextframe = newElem;
		}
		this.frames.push(newElem);
		if (noisy) {
			this.up('form').fireEvent('dirtychange', this, true);
		}
	},

	remove: function() {
		this.frames.pop();
		this.frames[this.frames.length-1].nextframe = null; // Assuming there is at least one element in the frame
		this.callParent(arguments);
		this.up('form').fireEvent('dirtychange', this, true);
	},

	getNextStartTimeframe: function() {
		var f = this.frames[this.frames.length-1];
		return f.down('#endvalue').getValue()+ ' ' + f.down('#endunit').getValue();
	}
});

Ext.define('RS.incident.Dashboard.BaseFrame', {
	extend: 'Ext.form.FieldContainer',
	alias: 'widget.baseframe',
	config: {
		order: 1
	},
	layout: {
		type: 'hbox',
		align: 'stretch',
		pack: 'center'
	},
	defaults: {
		labelSeparator: '',
		fieldLabel: ''
	},
	labelWidth: 163,
	items: [],

	initComponent: function() {
		var frameName = 'Column ' + this.order;
		var itemId  = frameName.replace(/\s/g, '');
		var sValUnit = this.start.split(' ');
		var valUnit = this.end.split(' ');
		var units = [];
		var unit;
		this.store.each(function(r) {
			unit = r.get('timeframeUnit');
			(unit == sValUnit[1])? units.push(r.getData()): units.length? units.push(r.getData()): null;
		}, this);
		var unitStore = new RS.incident.Dashboard.AgingBucket.UnitStore();
		unitStore.loadData(units);
		this.items.push({
			xtype: 'label',
			text: RS.incident.Dashboard.locale.TimeFrame.to,
			style: 'text-align:center;margin-top:4px;font-style:italic;',
			flex: 12
		}, {
			xtype: 'agingbucketvaluefield',
			itemId: 'endvalue',
			flex: 10,
			margin: '0 2 0 0',
			minValue: sValUnit[0]-0+1, // 'Integer' add 1 to string
			value: valUnit[0]
		}, {
			xtype: 'agingbucketunitfield',
			itemId: 'endunit',
			store: unitStore,
			displayField: 'timeframeUnit',
			valueField: 'timeframeUnit',
			value: valUnit[1],
			queryMode: 'local',
			editable: false,
			flex: 15
		});
		Ext.apply(this, {
			fieldLabel: frameName,
			itemId: itemId,
			items: this.items
		});
		this.callParent(arguments);
	},

	triggerChange: function(type, v, newValue, oldValue) {
		var endField = this.down('#end' + type);
		if (v['start'+type]) {
			endField.fireEvent(type+'change', newValue);
		}
		var currentValue = endField.getValue();
		var nextFrame = this.nextframe;
		while(nextFrame) {
			endField = nextFrame.down('#end'+type);
			endField.fireEvent(type+'change', currentValue);
			currentValue = endField.getValue();
			nextFrame = nextFrame.nextframe;
		}
	},

	listeners: {
		bvaluechanged: function(oArgs) {
			this.triggerChange('value', oArgs[0], oArgs[1], oArgs[2]);
		},
		bunitchanged: function(oArgs) {
			this.triggerChange('unit', oArgs[0], oArgs[1], oArgs[2]);
		}
	}
});

Ext.define('RS.incident.Dashboard.TimeFrame', {
	extend: 'RS.incident.Dashboard.BaseFrame',
	alias: 'widget.timeframe',
	isFirstFrame: true,
	initComponent: function() {
		// start: '1 days', end: '5 days'
		var valUnit = this.start.split(' ');
		this.items = [{
			xtype: 'agingbucketvaluefield',
			itemId: 'startvalue',
			startvalue: true,
			flex: 10,
			margin: '0 2 0 0',
			minValue: 1,
			maxValue: 99,
			value: valUnit[0]
		}, {
			xtype: 'agingbucketunitfield',
			itemId: 'startunit',
			startunit: true,
			store: this.store,
			displayField: 'timeframeUnit',
			valueField: 'timeframeUnit',
			value: valUnit[1],
			queryMode: 'local',
			editable: false,
			flex: 15
		}];
		this.callParent(arguments);
	},
	getData: function() {
		return (this.down('#startvalue').getValue() + ' ' + this.down('#startunit').getValue() + '-' +
		this.down('#endvalue').getValue() + ' ' + this.down('#endunit').getValue());
	}
});

Ext.define('RS.incident.Dashboard.NextFrame', {
	extend: 'RS.incident.Dashboard.BaseFrame',
	alias: 'widget.nextframe',
	isFirstFrame: false,
	initComponent: function() {
		this.items = [{
			xtype: 'label',
			text: RS.incident.Dashboard.locale.TimeFrame.priorTimeFrameEnd,
			style: 'text-align:center;margin-top:4px;margin-right:3px',
			flex: 25
		}];
		this.callParent(arguments);
	},
	getData: function() {
		return this.down('#endvalue').getValue() + ' ' + this.down('#endunit').getValue();
	}
});

Ext.define('RS.incident.Dashboard.AgingBucket.Valuefield', {
	extend: 'Ext.form.field.Number',
	alias: 'widget.agingbucketvaluefield',
	allowBlank: false,
	initComponent: function() {
		this.enableBubble('bvaluechanged');
		this.callParent(arguments);
		this.on('valuechange', this.reInit, this);
		this.on('change', function (v) {
			v.fireEvent('bvaluechanged', arguments);
		});
		this.on('validitychange', function(v, isValid, eOpts) {
			this.findParentByType('agebucketbuilder').fireEvent('validitychange', v, isValid, eOpts);
		})
	},
	reInit: function(newVal) {
		if (this.getValue() <= newVal) {
			// If current value is LTE new value, set newVal + 1 as current value
			this.setValue(newVal+1);
		}
		this.setMinValue(newVal+1);
	}
});

Ext.define('RS.incident.Dashboard.AgingBucket.Unitfield', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.agingbucketunitfield',

	initComponent: function() {
		this.enableBubble('bunitchanged');
		this.callParent(arguments);
		this.on('unitchange', this.reInit, this);
		this.on('change', function (v) {
			v.fireEvent('bunitchanged', arguments);
		});
	},

	reInit: function(newVal) {
		var currentVal = this.getValue();
		var unitMap = {
			Hours: [{timeframeUnit: 'Hours'}, {timeframeUnit: 'Days'}, {timeframeUnit: 'Weeks'}],
			Days: [{timeframeUnit: 'Days'}, {timeframeUnit: 'Weeks'}],
			Weeks: [{timeframeUnit: 'Weeks'}]
		};
		var weightMap = {
			Hours: 1,
			Days: 2,
			Weeks: 3
		}
		this.store.loadData(unitMap[newVal]);
		if (weightMap[currentVal] < weightMap[newVal] ) {
			this.setValue(unitMap[newVal][0].timeframeUnit);
		}
	}
});