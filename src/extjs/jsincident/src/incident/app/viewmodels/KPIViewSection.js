glu.defModel('RS.incident.Dashboard.KPIViewSection', {
	sectionId: null,
	viewSectionEntries: {
		mtype: 'list',
		autoParent: true
	},

	init: function() {
	},

	addGraph: function(record) {
		var id = record.get('id').toString();
		if (id.charAt(0) != this.sectionId) {
			console.error('Adding graph to the wrong section');
			return;
		}
		var graphModel = this.model({
			mtype : 'RS.incident.Dashboard.KPIViewSectionEntry',
			chartId: id,
			configs: record.getData()
		});
		this.viewSectionEntries.add(graphModel);
	},

	refresh: function() {
		for (var i=0; i<this.viewSectionEntries.length; i++) {
			this.viewSectionEntries.getAt(i).refresh();
		}
	}
});
