glu.defModel('RS.incident.Dashboard.KPILayoutSection', {
	sectionId: null,
	KPILayoutSectionEntries: {
		mtype: 'list',
		autoParent: true
	},

	init: function() {
	},

	addKPIEntryIsEnabled$: function() {
		return this.KPILayoutSectionEntries.length < 3;
	},

	getGraphIds: function() {
		var ids = [];
		for (var i=0; i<this.KPILayoutSectionEntries.length; i++) {
			ids.push(this.KPILayoutSectionEntries.getAt(i).chartId.charAt(1));
		}
		return ids;
	},

	createGraphFrom: function(record) {
		this.reallyAddKPISetting(record);
	},

	createNewEntry: function(id) {
		var dataSourceMap = {
			'11': this.localize('investigationByType'),
			'12': this.localize('investigationBySeverity'),
			'13': this.localize('investigationByAge'),
			'21': this.localize('investigationByTeamMember'),
			'22': this.localize('investigationByStatus'),
			'23': this.localize('investigationOverTimes'),
			'31': this.localize('investigationBySLA'),
			'32': this.localize('investigationByType'),
			'33': this.localize('investigationByAge')
		};
		var graphTypeMap = {
			'11': this.localize('pieChart'),
			'12': this.localize('barGraph'),
			'13': this.localize('pieChart'),
			'21': this.localize('barGraph'),
			'22': this.localize('pieChart'),
			'23': this.localize('barGraph'),
			'31': this.localize('pieChart'),
			'32': this.localize('barGraph'),
			'33': this.localize('pieChart')
		};
		var record = new RS.incident.model.ChartSettings({
			id: id,
			chartType: dataSourceMap[id],
			graphType: graphTypeMap[id]
		});
		this.createGraphFrom(record);
	},

	addKPIEntry: function() {
		var ids = ['1', '2', '3'];
		var graphIds = this.getGraphIds();
		var graphId = undefined;
		for (var i=0; i<ids.length; i++) {
			if (graphIds.indexOf(ids[i]) == -1) {
				graphId = this.sectionId + ids[i];
				break;
			}
		}
		if (graphId) {
			// Stay tune for more exciting stuffs. Let the big shot do the book keeping
			// and the Graph will be added for real.
			this.createNewEntry(graphId);
		}
		// What a fool, graph isn't really added yet.
	},

	reallyAddKPISetting: function(record) {
		var id = record.get('id').toString();
		if (id.charAt(0) != this.sectionId) {
			console.error('Adding graph to the wrong section');
			return;
		}
		var graphModel = this.model({
			mtype : 'RS.incident.Dashboard.KPILayoutSectionEntry',
			chartId: id,
			chartConfigs: record.getData()
		});
		graphModel.record = record;
		this.KPILayoutSectionEntries.add(graphModel);
	},

	removeGraph: function(graph) {
		this.KPILayoutSectionEntries.remove(graph);
	},

	removeIsEnabled$: function() {
		return this.parentVM.layoutSections.length > 1;
	},

	remove: function() {
		while (this.KPILayoutSectionEntries.length > 0) {
			this.KPILayoutSectionEntries.getAt(0).remove();
		}
		this.parentVM.removeSection(this);
	},

	save: function() {
		for (var i=0; i<this.KPILayoutSectionEntries.length; i++) {
			this.KPILayoutSectionEntries.getAt(i).save();
		}
	},

	refresh: function() {
		for (var i=0; i<this.KPILayoutSectionEntries.length; i++) {
			this.KPILayoutSectionEntries.getAt(i).refresh();
		}
	}
});
