glu.defModel('RS.incident.ExportDashboard', {
    fileType: 'CSV',
    outputFileName: 'ResolveInvestigationList-' + Ext.Date.format(new Date(), 'Ymd-His'),  
    fileTypeStore: {
        mtype: 'store',
        fields: ['fileType'],
        data: [{fileType: 'PDF'}, {fileType: 'JSON'}, {fileType: 'CSV'}]
    }, 
    userFilter : [],
    fields : [{
        name : 'sir',
        locale : 'sir',
        show : true,
        width : 85          
    },{
        name : 'playbook',
        locale : 'playbook',
        show : true,
        width : 'auto'          
    },{
        name : 'title',
        locale : 'title',
        show : true,
        width : '*'         
    },{
        name : 'sourceSystem',
        locale : 'sourceSystem',
        show : true,
        width : 70          
    },{
        name : 'id',
        locale : 'id',
        show : true,
        width : 220
    },{
        name : 'investigationType',
        locale : 'investigationType',
        show : true,
        width : 60
    },{
        name : 'severity',
        locale : 'severity',
        show : true,
        width : 60
    },{
        name : 'status',
        locale : 'status',
        show : true,
        width : 60
    },{
        name : 'sla',
        locale : 'sla',
        show : true,
        width : 70,
        convert: function(val){
            var days = 0;
            var hours = 0;
            if (val) {
                // 86400 = n_seconds_per_day
                // 3600 = n_seconds_per_hour
                days = Math.floor(val/86400);
                hours = Math.floor(val%86400)/3600;
            }
            if (days || hours) {
                return Ext.String.format('{0} Days {1} Hours', days, hours);
            } else {
                return 'None';
            }
        }
    },{
        name : 'dueBy',
        locale : 'dueBy',
        show : true,
        width : 70,
        convert : function(v){
            return v ? Ext.util.Format.date(new Date(v), clientVM.getUserDefaultDateFormat()) : v;
        }
    },{
        name : 'owner',
        locale : 'owner',
        show : true,
        width : 70
    },{
        name : 'sysCreatedOn',
        locale : 'sysCreatedOn',
        show : true,
        width : 70,
        convert: function(val) {
            return (val === null) ? '' : Ext.util.Format.htmlEncode(Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat()))
        }
    },{
        name : 'closedOn',
        locale : 'closedOn',
        show : true,
        width : 70
    },{
        name: 'closedBy',
        locale: '~~closedBy~~',
        show : true,
        width : 70
    }, {
        name: 'sysOrg',
        locale: 'sysOrg',       
        show : true,
        width : 70,
        convert: function(value) {
            if (value) {
                if(!this.orgStore) {
                    this.orgStore = {};
                    clientVM.user.orgs.forEach(function(e) {
                        if(e.uname != 'None')
                            this.orgStore[e.id] = e.uname;
                    }, this);
                }
                return this.orgStore[value];
            }
            return '';
        }
    }],

    init: function() {
        var me = this;
        this.set('columnSelectionStore', Ext.create('Ext.data.Store',{         
            fields : [{
                name: 'name',
                convert: function(v, r){
                    return r.raw.name;
                }
            },{
                name: 'display',
                convert: function(v, r){
                    return me.localize(r.raw.name);
                }
            }]      
        }))
        this.columnSelectionStore.loadData(this.fields);
    },
    getFileWithExtension: function() {
       return  this.outputFileName + '.' + this.fileType.toLowerCase();
    },
    cancel: function() {
        this.doClose();
    },  
    exportAll: function(sir) {
        this.ajax({
            url: '/resolve/service/playbook/securityIncident/list',
            method: 'GET',
            params : {
                page: 1,
                start: 0,
                limit: -1,
                sort: JSON.stringify([{direction:'DESC',property:'sysCreatedOn'}]),
                filter: JSON.stringify(this.userFilter)
            },
            success: function(resp){
                var response = RS.common.parsePayload(resp);
                if (response.success) {                   
                   switch (this.fileType) {
                        case 'PDF':
                           this.exportPDF(response.records);
                           break;
                        case 'JSON':
                           this.exportJSON(this.prepareRecords(response.records));
                           break;
                        case 'CSV':
                           this.exportCSV(this.prepareRecords(response.records));
                           break;                      
                   }
                   this.close();
               }
           }
       })       
    },

    //PDF 
    CONST : {
        PAGE_SIZE : 'A4',
        PAGE_DIMENSION: 'landscape',
        PAGE_MARGIN : [20,20,20,20],
        AVERAGE_CHAR_WIDTH : 6.5
    },    
    columnSelectionStore : null,
    entrySelections: [], 
    fileTypeIsNotPDF$ : function(){
        return this.fileType != 'PDF';
    },
    exportPDF: function(records){
        var selectedFields = [];
        Ext.Array.forEach(this.entrySelections, function(entry){
            selectedFields.push(entry.get('name'));
        })  
        var fields = this.fields;
        Ext.Array.forEach(fields, function(entry){
            if(selectedFields.indexOf(entry.name) != -1)
                entry.show = true;
            else
                entry.show = false;
        })  
        var fragment = this.getTableFragments('activities', '', fields, records , null);
        var PDFContent = {
            //pageSize: this.CONST.PAGE_SIZE,
            pageMargins: this.CONST.PAGE_MARGIN,
            pageOrientation: this.CONST.PAGE_DIMENSION,
            content :
            [{
                text : 'Investigations',
                style : ['incidentTitle']
            },fragment],
            styles : {
                partHeader : {
                    bold: true,
                    fontSize: 15,
                    margin : [0,10,0,0]
                },
                incidentTitle : {
                    bold: true,
                    fontSize: 20,
                    margin : [0,0,0,5]
                },
                tableHeader : {
                    bold : true,
                    fillColor : '#3d3d3d',
                    color : 'white'
                },
                tableGroup : {
                    color : 'white',
                    fillColor : '#777777',
                    alignment : 'center'
                }
            }
        }                   
        var currentDate = new Date();
        var formatedDate = (currentDate.getMonth() + 1) + '-' + (currentDate.getDate() < 10 ? '0' + currentDate.getDate() : currentDate.getDate() ) + '-' + currentDate.getFullYear();
        pdfMake.createPdf(PDFContent).download(this.outputFileName + '.pdf');       
    },      
    getTableFragments : function(dataPartName, partTitle, fields, records, groupConfig){
        if(!records || records.length == 0){
            return [{
                text : partTitle,
                style : ["partHeader"]
            },this.localize(dataPartName + 'StoreIsEmpty')]
        }

        //Table Style
        pdfMake.tableLayouts = {
            resolveTable: {
                hLineColor: function (i) {
                    return '#3d3d3d';
                },
                vLineColor: function (i) {
                    return '#3d3d3d';
                },
                fillColor: function (i, node) {
                    return i % 2 == 0 ? '#dadada' : 'white'
                },
                paddingTop : function(i, node){
                    return 5;
                },
                paddingBottom : function(i, node){
                    return 5;
                }
            }
        };

        var tableHeader = [];
        var rowWidth = [];
        var allRows = [];
        //Add Header
        for(var i = 0; i < fields.length; i++){
            var field = fields[i];
            if(field.show){
                tableHeader.push({
                    text : this.localize(field.locale),
                    style : 'tableHeader'
                })
                rowWidth.push(field.width ? field.width : 'auto');
            }
        }
        this.getActualColumnWidth(rowWidth);

        //Add Row
        var groupMaps = {
            _default : []
        };
        for(var i = 0; i < records.length; i++){
            var record = records[i];
            var groupName = '_default';
            if(groupConfig && groupConfig.groupField){
                groupName = record[groupConfig.groupField];
                if(!groupMaps.hasOwnProperty(groupName))
                    groupMaps[groupName] = [];
            }

            var row = [];
            for(var j = 0; j < fields.length; j++){
                var field = fields[j];

                if(field.show){
                    var fieldRawValue = field.mapping ? record[field.mapping] : record[field.name];
                    var value = Ext.isFunction(field.convert) ? field.convert.apply(this, [fieldRawValue, record]) : fieldRawValue;

                    //Mising required field will display empty row instead
                    if(!value && field.required){
                        row = [{
                            text : field.errorDisplay,
                            colSpan : tableHeader.length,
                            alignment : 'center'
                        }]
                        break;
                    } else
                        row.push(value ? this.wrapLongName(value, rowWidth[row.length]) : 'N/A');
                }
            }
            groupMaps[groupName].push(row);
        }
        for(var g in groupMaps){
            if(g != '_default'){
                allRows.push([{
                    text : g,
                    colSpan : tableHeader.length,
                    style : 'tableGroup'
                }]);
            }
            if(groupMaps[g].length > 0)
                allRows = allRows.concat(groupMaps[g]);
        }

        return [{
            text : partTitle,
            style : ["partHeader"]
        },{
            layout : 'resolveTable',
            margin : [0,5,0,0],
            table : {
                headerRows : 1,
                widths : rowWidth,
                body : [tableHeader].concat(allRows)
            }
        }]
    },
    getActualColumnWidth : function(widths){
        var pageSizeInfo = {
            A0: [2383.94, 3370.39],
            A1: [1683.78, 2383.94],
            A2: [1190.55, 1683.78],
            A3: [841.89, 1190.55],
            A4: [595.28, 841.89],
            A5: [419.53, 595.28]
        }
        var pageWidth = pageSizeInfo[this.CONST.PAGE_SIZE][this.CONST.PAGE_DIMENSION == 'landscape' ? 1 : 0] - this.CONST.PAGE_MARGIN[1] - this.CONST.PAGE_MARGIN[3] - 45;
        var autoSizeEntry = [];
        widths.forEach(function(w,i){
            if(!isNaN(w)){
                pageWidth -= w;
            }
            else
                autoSizeEntry.push(i);
        })

        autoSizeEntry.forEach(function(i){
            widths[i] = Math.max(0 , parseInt(pageWidth / autoSizeEntry.length));
        })
    },
    wrapLongName : function(name, thresholdWidth){
        name = name.toString();     
        var maxCharPerWord = parseInt(thresholdWidth / this.CONST.AVERAGE_CHAR_WIDTH);
        var regex = new RegExp('[^\\s]' +'{' + maxCharPerWord + '}','gi');      
        name = name.replace(regex, function(matched, index){
            return matched + ' ';
        })      
        return name;
    },

    //CSV and JSON
    prepareRecords: function(records) {
        var preparedRecords = [];
        for(var i = 0; i < records.length; i++){
            var record = records[i];
            var dataEntry = {};
            for(var j = 0; j < this.fields.length; j++){
                var fieldProperty = this.fields[j];
                var rawValue = record[fieldProperty.name];
                var value = Ext.isFunction(fieldProperty.convert) ? fieldProperty.convert.apply(this, [rawValue]) : rawValue;
                dataEntry[this.localize(fieldProperty.locale)] = value;
            }
            preparedRecords.push(dataEntry);
        }
        return preparedRecords;
    },
    exportJSON: function(toBeExported) {
        var blob = new Blob([Ext.JSON.encode(toBeExported)], {type : 'application/json'});
        downloadjs(blob, this.getFileWithExtension(), "text/plain");
    },
    exportCSV: function(toBeExported) {
        var csv = Papa.unparse(toBeExported);
        downloadjs(csv, this.getFileWithExtension(), "text/plain");
    }   
});