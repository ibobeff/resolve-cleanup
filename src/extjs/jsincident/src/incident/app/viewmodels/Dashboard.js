glu.defModel('RS.incident.Dashboard', {
	//Chain to Audit Log
	playbookComponent: {
		mtype: 'Playbook'
	},

	handleEdit: function(context) {
		//Save Activity Edit
		if (context.value !== context.originalValue) {
			if (context.record.get('status') === 'Closed' && context.field === 'status') {
				this.closeTicket(null, context.record);
			}

			var modifiedContext = context.record.getData();
			var DateTime = modifiedContext.dueBy = Ext.util.Format.date((modifiedContext.dueBy || '').split(' ').join('T'), 'time'); //convert human time to ms timestamp
			if (context.field === 'ownerName') {
				modifiedContext.owner = context.value;
			}

			//Orgs Info
			Ext.apply(modifiedContext, {
				'sysOrg' : clientVM.orgId
			})
			this.ajax({
				url: '/resolve/service/playbook/securityIncident/save',
				method: 'POST',
				jsonData: modifiedContext,
				success: function(resp) {
					var response = RS.common.parsePayload(resp)
					if (response.success) {
						if (!(context.field === 'playbook' && context.record.get('status') === 'In Progress')) {
							clientVM.displaySuccess(this.localize('updateActivitySuccess'));
						} else {
							clientVM.displayError(this.localize('attemptUpdatePlaybook'));
						}
						setTimeout(function(){
							this.store.reload();
						}.bind(this), 500);
					}
					else {
						clientVM.displayError(response.message);
					}
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			});
		}
	},

	filterType: null,
	handleFilterChange: null,

	store: {
		mtype : 'store',
		remoteSort: true,
		pageSize : 20,
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		fields: [
			'id',
			'sir',
			'title',
			'externalReferenceId',
			'investigationType',
			'type',
			'severity',
			'description',
			'sourceSystem',
			'playbook',
			'externalStatus',
			'status',
			'owner',
			'members',
			'playbookVersion',
			'sequence',
			'closedBy',
			'primary',
			'sirStarted',
			'problemId',
			'closedOn',
			'sysCreatedOn',
			{
				name: 'dueBy',
				type: 'date',
				convert: function(v, r) {return v ? Ext.util.Format.date(new Date(v), clientVM.getUserDefaultDateFormat()) : v},
				serialize: function(v) {
					//console.log('serializing');
					return v ? (new Date(v)).getTime() : v
				}
			},
			{name: 'sys_id', mapping: 'id'},
			'sla', 'sysOrg'
		],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/playbook/securityIncident/list',
			reader: {
				type: 'json',
				root: 'records'
			}
		},
		autoLoad :false
	},
	securityIncidentSelections: [],

	typeStore: {
		mtype: 'store',
		fields: ['type'],
		proxy: {
			type: 'memory'
		}
	},

	refreshTypes: function(editor) {
		Ext.Ajax.request({
			scope: this,
			url: '/resolve/service/sysproperties/getSystemPropertyByName',
			method: 'POST',
			params: {
				name: 'app.security.types'
			},
			success: function(resp) {
				var data = RS.common.parsePayload(resp);
				var typesStoreData = [];
				this.typesStoreData = [];
				if (!data.success) {
					clientVM.displayError(data.message);
					return;
				}
				var types = data.data.uvalue.split(',');
				types.forEach(function(r) {
					typesStoreData.push({type: r.trim()});
				});
				this.typeStore.loadData(typesStoreData);
				try {
					editor.expand();
				} catch(err) {}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	playbookTemplateStore: {
		mtype: 'store',
		fields: ['id', 'uname', 'unamespace', 'usummary', 'ufullname'],
		pageSize: 25,
		sorters: [{
			property: 'ufullname',
			direction: 'ASC'
		}],

		proxy: {
			type: 'ajax',
			url: '/resolve/service/wiki/listPlaybookTemplates',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	ownerStore: {
		mtype: 'store',
		fields: [
			'userId',
			{
				name: 'name',
				convert: function(value, record) {
					if (value.trim().length === 0) {
						return record.get('userId')
					} else {
						return value
					}
				}
			}
		],
		proxy: {
			type: 'memory'
		}
	},

	refreshMembers: function(editor) {
		this.ajax({
			url: '/resolve/service/playbook/securityIncident/getSecurityGroup',
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					var recs = RS.common.parsePayload(resp).records;
					this.ownerStore.loadData(recs);
					this.ownerStore.sort('userId', 'ASC');
					if (editor) {
						try {
							editor.expand();
						} catch(err) {}
					}
				} else {
					clientVM.displayError(this.localize('failedLoadingSecurityUserGroup'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	refreshRateStore: {
		mtype: 'store',
		fields: ['rate'],
		data: [
			{rate: '1 min'},
			{rate: '5 min'},
			{rate: '10 min'},
			{rate: '15 min'},
			{rate: 'Never'}
		],
		proxy: {
			type: 'memory'
		}
	},

	// Dashboard metrics
	openMetric: '',
	inProgressMetric: '',
	closedMetric: '',
	pastDueMetric: '',
	aLAatRiskMetric: '',
	sLAatRiskPolicy: '5 days',
	// Dashboard configuration settings
	dashboardSettingsStore: {
		mtype: 'store',
		model: 'RS.incident.model.DashboardSettings',
	},

	scope: 30,
	scopeUnit: 'days',

	dashboardrefreshRate: 'Never',
	autoRefreshPlayed: false,
	autoRefreshPaused: true,

	dashboardShowOpen: true,
	dashboardShowInProgress: true,
	dashboardShowClosed: true,
	dashboardShowPastDue: true,
	dashboardShowSLAatRisk: true,

	// Chart refresh monitor flag (to be toggled)
	refreshMonitor: true,

	// RBAC
	sirPermissions: undefined,
	pSirInvestigationsCreateNew: true,
	pSirDashboardCharts: true,
	pSirDashboardView: true,
	pSirDashboardInvestigationListView: true,

	dashboardrefreshRateChanges$: function() {
		var record = this.dashboardSettingsStore.getAt(0);
		record? record.set('dashboardrefreshRate', this.dashboardrefreshRate): 'ignoreme';
	},

	chartDataChange: function(type, val) {
		this.set(type, val);
		this.dashboardSettingsStore.getAt(0).set(type, val);
	},

	checkViewPermission: function() {
		return this.pSirDashboardInvestigationListView;
	},

	rootcsrftoken: '',

	initToken: function() {
		this.set('rootcsrftoken', clientVM.rsclientToken);
	},
	init: function() {
		this.initToken();
		var me = this;
		var filterType = ['Open', 'In Progress', 'Complete'];
		this.set('filterType', filterType);
		this.store.on('beforeload', function(store, op) {
			var userFilter = this.getUserFilter();
			op.params = {};
			op.params.filter = JSON.stringify(userFilter);
		}, this);
		this.store.on('beforeload', this.checkViewPermission);
		this.store.on('load', function(store, records, successful) {
			this.doRefresh();
		}, this);
		var handleFilterChange = function(newFilters) {
			var newFiltersArr = [];
			for (key in newFilters) {
				newFiltersArr.push(newFilters[key])
			}
			this.filterType = newFiltersArr;
			this.store.loadPage(1); //Reset grid to view first page after filter change
		};
		this.set('handleFilterChange', handleFilterChange);
		this.refreshTypes();
		this.applyRBAC();
		clientVM.on('orgchange', this.orgChange.bind(this));
	},
	getUserFilter: function(){
		var types = ['Open', 'In Progress', 'Complete', 'Closed'];
		var subset = types.filter(function(e) { return this.filterType.indexOf(e) === -1 }, this);

		//Filter format: {"field":"status","type":"auto","condition":"notEquals","value":"Closed"}
		var userFilter = subset.map(function(type) {
			return {"field": "status", "type": "auto", "condition": "notEquals", "value": type}
		});
		userFilter.push({
			field: 'sysOrg',
			type: 'auto',
			condition: 'equals',
			value: clientVM.orgId || 'nil'
		});
		return userFilter;
	},
	applyRBAC: function() {
		var permissions = clientVM.user.permissions || {};

		// pSirDashboardInvestigationListView
		permissions.sirDashboardInvestigationList = permissions.sirDashboardInvestigationList || {};
		this.set('pSirDashboardInvestigationListView', permissions.sirDashboardInvestigationList.view === true);

		permissions.sirDashboard = permissions.sirDashboard || {};
		this.set('pSirDashboardView', permissions.sirDashboard.view === true);

		permissions.sirDashboardCharts = permissions.sirDashboardCharts || {};
		this.set('pSirDashboardCharts', permissions.sirDashboardCharts.view === true);

		permissions.sirDashboardInvestigationList = permissions.sirDashboardInvestigationList || {};
		this.set('pSirInvestigationsCreateNew', permissions.sirDashboardInvestigationList.create === true);

	},

	configureMetrics: function() {
		if (!this.metricsSetting) {
			this.metricsSetting = this.open({
				mtype: 'RS.incident.Dashboard.MetricsSetting',
				target: arguments[0].target
			});
		}
	},

	activate: function(screen, params) {
		this.initToken();
		this.applyRBAC();
		if (!this.pSirDashboardView) {
			this.message({
				title: this.localize('insufficientPermissionMsgTitle'),
				msg: this.localize('insufficientPermissionMsg'),
				closable : false,
				buttonText: {
					ok: this.localize('close')
				},
				scope: this,
				fn: function(btn) {
					clientVM.handleNavigationBack();
				}
			});
		} else {
			this.loadChartSettings();
			this.store.reload();
		}
		this.refreshMembers();
	},

	newInvestigation: function(e) {
		this.open({
			mtype: 'RS.incident.SecurityIncidentForm',
			pos: e.getXY(),
		});
	},

	closeTicket: function(grid, record) {
		var me = this;
		if (clientVM.delayedTask) {
			clientVM.delayedTask.cancel();
		}
		this.ajax({
			url: '/resolve/service/playbook/securityIncident/close',
			params: {
				incidentId: record.get('id'),
				sir: ''
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					var data = respData.data;
					record.skipSave = true;
					record.set({
						status: data.status,
						closedOn: data.closedOn,
						closedBy: data.closedBy
					});
					clientVM.delayedTask = new Ext.util.DelayedTask(function () {
						clientVM.displaySuccess(me.localize('successfullyCloseTicket'));
					});
				} else {
					clientVM.delayedTask = new Ext.util.DelayedTask(function () {
						clientVM.displayError(me.localize('failedToCloseTicket'));
					});
				}
			},
			failure: function(resp) {
				clientVM.delayedTask = new Ext.util.DelayedTask(function () {
					clientVM.displayFailure(resp);
				});
			},
			callback: function() {
				clientVM.delayedTask.delay(500);
			}
		});
	},

	loadMetrics: function() {
		this.ajax({
			scope: this,
			url: '/resolve/service/playbook/getsircountreport',
			method: 'POST',
			params: {
				slaAtRiskUnits: this.sLAatRiskPolicy,
				scopeUnits: this.scope,
				unitType: this.scopeUnit,
				orgId: clientVM.orgId
			},
			success: function(resp) {
				var data = RS.common.parsePayload(resp);
				if (!data.success) {
					clientVM.displayError(data.message);
					return;
				}
				data = data.data;
				this.set('aLAatRiskMetric', data['SLA at Risk']? data['SLA at Risk'].toString(): '0');
				var completed = data['Complete']? data['Complete']: 0;  // Completed SIRs are counted toward Closed SIRs
				this.set('closedMetric', data['Closed']? (completed+data['Closed']).toString(): completed.toString());
				this.set('closedMetric', data['Closed']?data['Closed'].toString(): '0');
				this.set('inProgressMetric', data['In Progress']?data['In Progress'].toString(): '0');
				this.set('pastDueMetric', data['Past Due']? data['Past Due'].toString(): '0');
				this.set('openMetric', data['Open']? data['Open'].toString(): '0');
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	syncBindings: function(settings) {
		var me = this;
		Object.keys(settings).forEach(function(k) {
			(me[k] != undefined)? me.set(k, settings[k]): 'TryNotUsingIf';
		});
	},

	loadSettings: function() {
		var me = this;
		//loads any existing settings from localStorage
		this.dashboardSettingsStore.load();
		if (!this.dashboardSettingsStore.count()) {
			this.dashboardSettingsStore.loadData([new RS.incident.model.DashboardSettings()]);
			this.dashboardSettingsStore.sync();
		}

		this.syncBindings(this.dashboardSettingsStore.getAt(0).getData());

		// saves settings to localStorage
		this.dashboardSettingsStore.on('update', function(store, record) {
			me.syncBindings(record.getData());
			store.sync();
		});

		this.loadMetrics();

		// start auto-refresh if the dashboard was configured to do so
		if (this.dashboardrefreshRate !== 'Never' && this.autoRefreshPlayed) {
			this.startAutoRefresh(this.refreshRateMap[this.dashboardrefreshRate]);
		}

		this.initPermissions();
	},

	initPermissions: function() {
		var permissions = clientVM.user.permissions;
		permissions.sirDashboardPlaybook = permissions.sirDashboardPlaybook || {};
		permissions.sirDashboardTitle = permissions.sirDashboardTitle || {};
		permissions.sirDashboardType = permissions.sirDashboardType || {};
		permissions.sirDashboardSeverity = permissions.sirDashboardSeverity || {};
		permissions.sirDashboardStatus = permissions.sirDashboardStatus || {};
		permissions.sirDashboardDueDate = permissions.sirDashboardDueDate || {};
		permissions.sirPlaybookTemplates = permissions.sirPlaybookTemplates || {};
		// To modify a playbook template, user must have view permission on playbook template list
		permissions.sirDashboardPlaybook.modify = permissions.sirDashboardPlaybook.modify && permissions.sirPlaybookTemplates.view;
		this.set('sirPermissions', clientVM.user.permissions);
	},

	chart1Configs: {},
	chart2Configs: {},
	chart3Configs: {},

	loadChartSettings: function() {
		var me = this;
		var dataSourceMap = {
			'1': this.localize('investigationByType'),
			'2': this.localize('investigationBySeverity'),
			'3': this.localize('investigationByAge')
		};  // For default charts
		var graphTypeMap = {
			'1': this.localize('pieChart'),
			'2': this.localize('barGraph'),
			'3': this.localize('barGraph')
		};  // For default charts
		['1', '2', '3'].forEach(function(id) {
			RS.incident.model.ChartSettings.load(id, {
				scope: this,
				callback: function(record) {
					record = record? record: new RS.incident.model.ChartSettings({
						id: id,
						chartType: dataSourceMap[id],
						graphType: graphTypeMap[id]
					});  // Create default if not exist
					me.set('chart'+id+'Configs', record.getData());
				}
			});
		});
	},


	refreshRateMap: {
		'1 min': 60,
		'5 min': 300,
		'10 min': 600,
		'15 min': 900
	},

	manualRefresh: function() {
		this.doRefresh();
	},

	autoRefreshPlayIsEnabled$: function() {
		return this.autoRefreshPaused && this.dashboardrefreshRate !== 'Never';
	},
	autoRefreshPlay: function() {
		this.dashboardSettingsStore.getAt(0).set('autoRefreshPlayed', true);
		this.dashboardSettingsStore.getAt(0).set('autoRefreshPaused', false);
		this.startAutoRefresh(this.refreshRateMap[this.dashboardrefreshRate]);
	},

	refreshRateChange: function(newRate, oldRate) {
		if (newRate == 'Never') {
			this.stopAutoRefresh();
		} else if (oldRate == 'Never') {
			this.dashboardSettingsStore.getAt(0).set('autoRefreshPaused', true);
			this.dashboardSettingsStore.getAt(0).set('autoRefreshPlayed', false);
		}
		else if (this.autoRefreshPlayed) {
			this.startAutoRefresh(this.refreshRateMap[newRate]);
		}
	},

	autoRefreshPauseIsEnabled$: function() {
		return this.autoRefreshPlayed && this.dashboardrefreshRate !== 'Never';
	},
	autoRefreshPause: function() {
		this.dashboardSettingsStore.getAt(0).set('autoRefreshPaused', true);
		this.dashboardSettingsStore.getAt(0).set('autoRefreshPlayed', false);
		this.stopAutoRefresh();
	},

	doRefresh: function() {
		// Load metrics
		this.loadMetrics();
		// Load charts
		this.set('refreshMonitor', !this.refreshMonitor);
	},

	startAutoRefresh: function(interval) {
		if (this.autoRefreshTask) this.autoRefreshTask.destroy();

		var runner = new Ext.util.TaskRunner();
		this.autoRefreshTask = runner.newTask({
			run: this.doRefresh,
			scope: this,
			interval: (interval || 30)* 1000
		});
		this.autoRefreshTask.start();
	},
	stopAutoRefresh: function() {
		if (this.autoRefreshTask) {
			this.autoRefreshTask.destroy();
			delete this.autoRefreshTask;
		}
	},

	applyChartSettings: function(configs) {
		this.set('chart'+configs.id+'Configs', configs);
	},

	chartSettingsHandler: function(e) {
		var btn = Ext.getCmp(e.currentTarget.id);
		var chartConfigs = btn.up('chart').chartConfigs;
		var chartType = chartConfigs.chartType;
		var hideScopeConfig = true;
		var hideAgeBucketConfig = true;
		var hideColumnConfig = true;
		if (chartType == this.localize('investigationByType')) {
			hideScopeConfig =  false;
		} else if (chartType == this.localize('investigationBySeverity')) {
			hideScopeConfig = false;
		} else if (chartType == this.localize('investigationByAge')) {
			hideAgeBucketConfig =  false;
		} else if (chartType == this.localize('investigationByOwner')) {
		} else if (chartType == this.localize('investigationByStatus')) {
		} else if (chartType == this.localize('investigationByPhases')) {
			// Will support later
		} else if (chartType == this.localize('investigationOverTimes')) {
		} else if (chartType == this.localize('investigationBySLA')) {
			hideScopeConfig = false;
		} else if (chartType == this.localize('investigationByWorkload')) {
			hideScopeConfig = false;
		}
		this.open({
			mtype: 'RS.incident.Dashboard.ChartSettings',
			target: btn.up('panel'),
			chartConfigs: chartConfigs,
			hideScopeConfig: hideScopeConfig,
			hideAgeBucketConfig: hideAgeBucketConfig,
			hideColumnConfig: hideColumnConfig,
			dumper: this.applyChartSettings.bind(this)
		});
	},

	openGraphicReports: function() {
		clientVM.handleNavigation({
			modelName: 'RS.incident.Dashboard.KPI',
			target: '_blank'
		});
	},

	orgChange: function() {
		this.store.load();
		this.doRefresh();
	},

	beforeDestroyComponent : function() {
		clientVM.removeListener('orgchange', this.orgChange);
	},

	export: function(){
		this.open({
			mtype : 'RS.incident.ExportDashboard',
			userFilter : this.getUserFilter()
		})
	}
});
glu.defModel('RS.incident.Dashboard.PDFConverter',{
	//Export to PDF
	CONST : {
		PAGE_SIZE : 'A4',
		PAGE_DIMENSION: 'landscape',
		PAGE_MARGIN : [20,20,20,20],
		AVERAGE_CHAR_WIDTH : 6.5
	},
	fields : [{
		name : 'sir',
		locale : 'sir',
		show : true,
		width : 85
	},{
		name : 'playbook',
		locale : 'playbook',
		show : true,
		width : 'auto'
	},{
		name : 'title',
		locale : 'title',
		show : true,
		width : '*'
	},{
		name : 'sourceSystem',
		locale : 'sourceSystem',
		show : true,
		width : 70
	},{
		name : 'id',
		locale : 'id',
		show : true,
		width : 220
	},{
		name : 'investigationType',
		locale : 'investigationType',
		show : true,
		width : 60
	},{
		name : 'severity',
		locale : 'severity',
		show : true,
		width : 60
	},{
		name : 'status',
		locale : 'status',
		show : true,
		width : 60
	},{
		name : 'sla',
		locale : 'sla',
		show : true,
		width : 70,
		convert: function(val){
			var days = 0;
			var hours = 0;
			if (val) {
				// 86400 = n_seconds_per_day
				// 3600 = n_seconds_per_hour
				days = Math.floor(val/86400);        // assuming pos val
				hours = Math.floor(val%86400)/3600;  // assuming pos val
			}
			if (days || hours) {
				return Ext.String.format('{0} Days {1} Hours', days, hours);
			} else {
				return 'None';
			}
		}
	},{
		name : 'dueBy',
		locale : 'dueBy',
		show : true,
		width : 70,
		convert : function(v){
			return v ? Ext.util.Format.date(new Date(v), clientVM.getUserDefaultDateFormat()) : v;
		}
	},{
		name : 'owner',
		locale : 'owner',
		show : true,
		width : 70
	},{
		name : 'sysCreatedOn',
		locale : 'sysCreatedOn',
		show : true,
		width : 70,
		convert: function(val) {
			return (val === null) ? '' : Ext.util.Format.htmlEncode(Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat()))
		}
	},{
		name : 'closedOn',
		locale : 'closedOn',
		show : true,
		width : 70
	},{
		name: 'closedBy',
		locale: '~~closedBy~~',
		show : true,
		width : 70
	}, {
		name: 'sysOrg',
		locale: 'sysOrg',
		show : true,
		width : 70,
		convert: function(value) {
			if (value) {
				if(!this.orgStore) {
					this.orgStore = {};
					clientVM.user.orgs.forEach(function(e) {
						if(e.uname != 'None')
							this.orgStore[e.id] = e.uname;
					}, this);
				}
				return this.orgStore[value];
			}
			return '';
		}
	}],
	store : null,
	entrySelections: [],
	init: function(){
		var me = this;
		this.set('store', Ext.create('Ext.data.Store',{
			fields : [{
				name: 'name',
				convert: function(v, r){
					return r.raw.name;
				}
			},{
				name: 'display',
				convert: function(v, r){
					return me.localize(r.raw.name);
				}
			}]
		}))
		this.store.loadData(this.fields);
	},
	startIsEnabled$ : function(){
		return this.entrySelections.length != 0;
	},
	start: function(){
		this.ajax({
			url: '/resolve/service/playbook/securityIncident/list',
			method: 'GET',
			params : {
				page: 1,
				start: 0,
				limit: -1,
				sort: JSON.stringify([{direction:'DESC',property:'sysCreatedOn'}]),
				filter: JSON.stringify(this.userFilter)
			},
			success: function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success){
					var selectedFields = [];
					Ext.Array.forEach(this.entrySelections, function(entry){
						selectedFields.push(entry.get('name'));
					})
					var fields = this.fields;
					Ext.Array.forEach(fields, function(entry){
						if(selectedFields.indexOf(entry.name) != -1)
							entry.show = true;
						else
							entry.show = false;
					})
					var fragment = this.getTableFragments('activities', '', fields, response.records , null);
					var PDFContent = {
						//pageSize: this.CONST.PAGE_SIZE,
			   			pageMargins: this.CONST.PAGE_MARGIN,
			   			pageOrientation: this.CONST.PAGE_DIMENSION,
			   			content :
			   			[{
					        text : 'Investigations',
					        style : ['incidentTitle']
			    		},fragment],
			   			styles : {
					        partHeader : {
					            bold: true,
					            fontSize: 15,
					            margin : [0,10,0,0]
					        },
					        incidentTitle : {
					            bold: true,
					            fontSize: 20,
					            margin : [0,0,0,5]
					        },
					        tableHeader : {
					        	bold : true,
					        	fillColor : '#3d3d3d',
					        	color : 'white'
					        },
					        tableGroup : {
					        	color : 'white',
					        	fillColor : '#777777',
					        	alignment : 'center'
					        }
					    }
					}
					var currentDate = new Date();
					var formatedDate = (currentDate.getMonth() + 1) + '-' + (currentDate.getDate() < 10 ? '0' + currentDate.getDate() : currentDate.getDate() ) + '-' + currentDate.getFullYear();
					pdfMake.createPdf(PDFContent).download('Investigations' + ' (' + formatedDate + ').pdf');
					this.doClose();
				}
			}
		})
	},
	getTableFragments : function(dataPartName, partTitle, fields, records, groupConfig){

		if(!records || records.length == 0){
			return [{
	        	text : partTitle,
	        	style : ["partHeader"]
	      	},this.localize(dataPartName + 'StoreIsEmpty')]
		}

		//Table Style
		pdfMake.tableLayouts = {
	  		resolveTable: {
			    hLineColor: function (i) {
		      		return '#3d3d3d';
			    },
			    vLineColor: function (i) {
		      		return '#3d3d3d';
			    },
			    fillColor: function (i, node) {
					return i % 2 == 0 ? '#dadada' : 'white'
				},
				paddingTop : function(i, node){
					return 5;
				},
				paddingBottom : function(i, node){
					return 5;
				}
	  		}
		};

		var tableHeader = [];
		var rowWidth = [];
		var allRows = [];
		//Add Header
		for(var i = 0; i < fields.length; i++){
			var field = fields[i];
			if(field.show){
				tableHeader.push({
					text : this.localize(field.locale),
					style : 'tableHeader'
				})
				rowWidth.push(field.width ? field.width : 'auto');
			}
		}
		this.getActualColumnWidth(rowWidth);

		//Add Row
		var groupMaps = {
			_default : []
		};
		for(var i = 0; i < records.length; i++){
			var record = records[i];
			var groupName = '_default';
			if(groupConfig && groupConfig.groupField){
				groupName = record[groupConfig.groupField];
				if(!groupMaps.hasOwnProperty(groupName))
					groupMaps[groupName] = [];
			}

			var row = [];
			for(var j = 0; j < fields.length; j++){
				var field = fields[j];

				if(field.show){
					var fieldRawValue = field.mapping ? record[field.mapping] : record[field.name];
					var value = Ext.isFunction(field.convert) ? field.convert.apply(this, [fieldRawValue, record]) : fieldRawValue;

					//Mising required field will display empty row instead
					if(!value && field.required){
						row = [{
							text : field.errorDisplay,
							colSpan : tableHeader.length,
							alignment : 'center'
						}]
						break;
					} else
						row.push(value ? this.wrapLongName(value, rowWidth[row.length]) : 'N/A');
				}
			}
			groupMaps[groupName].push(row);
		}
		for(var g in groupMaps){
			if(g != '_default'){
				allRows.push([{
					text : g,
					colSpan : tableHeader.length,
					style : 'tableGroup'
				}]);
			}
			if(groupMaps[g].length > 0)
				allRows = allRows.concat(groupMaps[g]);
		}

		return [{
        	text : partTitle,
        	style : ["partHeader"]
      	},{
      		layout : 'resolveTable',
      		margin : [0,5,0,0],
      		table : {
      			headerRows : 1,
      			widths : rowWidth,
      			body : [tableHeader].concat(allRows)
      		}
      	}]
	},
	getActualColumnWidth : function(widths){
		var pageSizeInfo = {
			A0: [2383.94, 3370.39],
			A1: [1683.78, 2383.94],
			A2: [1190.55, 1683.78],
			A3: [841.89, 1190.55],
			A4: [595.28, 841.89],
			A5: [419.53, 595.28]
		}
		var pageWidth = pageSizeInfo[this.CONST.PAGE_SIZE][this.CONST.PAGE_DIMENSION == 'landscape' ? 1 : 0] - this.CONST.PAGE_MARGIN[1] - this.CONST.PAGE_MARGIN[3] - 45;
		var autoSizeEntry = [];
		widths.forEach(function(w,i){
			if(!isNaN(w)){
				pageWidth -= w;
			}
			else
				autoSizeEntry.push(i);
		})

		autoSizeEntry.forEach(function(i){
			widths[i] = Math.max(0 , parseInt(pageWidth / autoSizeEntry.length));
		})
	},
	wrapLongName : function(name, thresholdWidth){
		name = name.toString();
		var maxCharPerWord = parseInt(thresholdWidth / this.CONST.AVERAGE_CHAR_WIDTH);
		var regex = new RegExp('[^\\s]' +'{' + maxCharPerWord + '}','gi');
		name = name.replace(regex, function(matched, index){
			return matched + ' ';
		})
		return name;
	},
	cancel : function(){
		this.doClose();
	}
})
