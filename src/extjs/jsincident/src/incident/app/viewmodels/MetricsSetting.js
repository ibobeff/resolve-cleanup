glu.defModel('RS.incident.Dashboard.MetricsSetting', {
	target: null,
	store: null,
	slaPolicyStore: {
		mtype: 'store',
		proxy: {
			type: 'memory'
		},
		fields: ['label', 'nDays'],
		data: [{
			label: '1 day',
			nDays: 1
		}, {
			label: '2 days',
			nDays: 2
		}, {
			label: '3 days',
			nDays: 3
		}, {
			label: '4 days',
			nDays: 4
		}, {
			label: '5 days',
			nDays: 5
		}]
	},
	scopeUnitStore: {
		mtype: 'store',
		proxy: {
			type: 'memory'
		},
		fields: ['unit'],
		data: [{
			unit: 'Days'
		}, {
			unit: 'Hours'
		}, {
			unit: 'Minutes'
		}]
	},

	scope: 90,
	scopeUnit: 'Days',
	maxScope: 365,
	fvalid: true,
	tips: '',

	when_scope_unit_change_replace_the_tips: {
		on: ['scopeUnitChanged'],
		action: function() {
			var map = {
				days: this.localize('between1To365days'),
				hours: this.localize('between1To24Hours'),
				minutes: this.localize('between1To120Minutes')
			}
			this.set('tips', map[this.scopeUnit.toLowerCase()]);
		}
	},

	scopeUnitChange: function(rec) {
		var scopeUnit = rec[0].get('unit');
		if (scopeUnit == 'Days') {
			this.set('maxScope', 365);
			this.set('scope', (this.scope > 365)? 365: this.scope);
		} else if (scopeUnit == 'Hours') {
			this.set('maxScope', 24);
			this.set('scope', (this.scope > 24)? 24: this.scope);
		} else if (scopeUnit == 'Minutes') {
			this.set('maxScope', 120);
			this.set('scope', (this.scope > 120)? 120: this.scope);
		}
		this.set('fvalid', !this.fvalid); // Trick to clear invalid flag
	},

	init: function() {
		this.set('tips', this.localize('between1To365days'));
	},

	applyIsEnabled$: function() {
		return (this.scope <= this.maxScope)? true: false;
	},

	apply: function() {
		this.form.updateRecord(this.parentVM.dashboardSettingsStore.getAt(0));
		this.parentVM.doRefresh();
		this.cancel();
	},

	close: function() {
		this.parentVM.metricsSetting = null;
		this.doClose();
	},

	cancel: function() {
		this.close();
	},

	loadForm: function(form) {
		this.form = form;
		form.loadRecord(this.parentVM.dashboardSettingsStore.getAt(0));
	}
});