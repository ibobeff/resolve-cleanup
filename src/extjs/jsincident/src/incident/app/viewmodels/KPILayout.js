glu.defModel('RS.incident.Dashboard.KPILayout', {
	layoutSections: {
		mtype: 'list',
		autoParent: true
	},
	init: function() {
	},

	loadLayout: function() {
		this.layoutSections.removeAll();
		var me = this;
		['11', '12', '13', '21', '22', '23', '31', '32', '33'].forEach(function(id) {
			RS.incident.model.ChartSettings.load(id, {
				scope: this,
				callback: function(record) {
					if (record) {
						me.createKPISettingFrom(record);
					}
				}
			});
		});
		if (!this.layoutSections.length) {
			this.createDefaultLayout();
		}
	},

	createNewGraph: function(id) {
		var dataSourceMap = {
			'11': this.localize('investigationByType'),
			'12': this.localize('investigationBySeverity'),
			'13': this.localize('investigationByAge'),
			'21': this.localize('investigationByTeamMember'),
			'22': this.localize('investigationByStatus'),
			'23': this.localize('investigationOverTimes'),
			'31': this.localize('investigationBySLA'),
			'32': this.localize('investigationByType'),
			'33': this.localize('investigationByAge')
		};
		var graphTypeMap = {
			'11': this.localize('pieChart'),
			'12': this.localize('barGraph'),
			'13': this.localize('pieChart'),
			'21': this.localize('barGraph'),
			'22': this.localize('pieChart'),
			'23': this.localize('barGraph'),
			'31': this.localize('pieChart'),
			'32': this.localize('barGraph'),
			'33': this.localize('pieChart')
		};
		var record = new RS.incident.model.ChartSettings({
			id: id,
			chartType: dataSourceMap[id],
			graphType: graphTypeMap[id]
		});
		record.save();
		this.createKPISettingFrom(record);
	},

	createDefaultLayout: function() {
		var me = this;
		['11', '12', '13', '21', '22', '23'].forEach(function(id) {
			me.createNewGraph(id);
		});
	},

	getSectionById: function(sectionId) {
		for (var i=0; i<this.layoutSections.length; i++) {
			var section = this.layoutSections.getAt(i);
			if (section.sectionId == sectionId) {
				return section;
			}
		}
		return this.createSection(sectionId);
	},

	createKPISettingFrom: function(record) {
		var section = this.getSectionById(record.get('id').toString().charAt(0));
		section.reallyAddKPISetting(record);
	},

	createSection: function(sectionId) {
		var model = this.model({
			mtype : 'RS.incident.Dashboard.KPILayoutSection',
			sectionId: sectionId
		});
		this.layoutSections.add(model);
		return this.layoutSections.getAt(this.layoutSections.length-1);
	},

	getSectionIds: function() {
		var ids = [];
		for (var i=0; i<this.layoutSections.length; i++) {
			ids.push(this.layoutSections.getAt(i).sectionId);
		}
		return ids;
	},

	addSection: function() {
		var ids = ['1', '2', '3'];
		var sectionId = undefined;
		var sectionIds = this.getSectionIds();
		for (var i=0; i<ids.length; i++) {
			if (sectionIds.indexOf(ids[i]) == -1) {
				sectionId = ids[i];
				break;
			}
		}
		var section = this.createSection(sectionId);
		section.addKPIEntry();
	},

	removeSection: function(section) {
		this.layoutSections.remove(section);
	},

	removeAllRecords: function() {
		['11', '12', '13', '21', '22', '23', '31', '32', '33'].forEach(function(id) {
			RS.incident.model.ChartSettings.load(id, {
				callback: function(record) {
					if (record) {
						record.destroy();
					}
				}
			});
		});
	},

	save: function() {
		this.removeAllRecords();
		for (var i=0; i<this.layoutSections.length; i++) {
			this.layoutSections.getAt(i).save();
		}
	},

	refresh: function() {
		for (var i=0; i<this.layoutSections.length; i++) {
			this.layoutSections.getAt(i).refresh();
		}
	}
});
