glu.defModel('RS.incident.Dashboard.KPILayoutSectionEntry', {
	record: undefined,
	chartId: undefined,
	chartConfigs: undefined,
	chartSetting: {
		mtype: 'RS.incident.Dashboard.ChartSettings',
		autoParent: true,
		layoutContext: true
	},

	init: function() {
		this.chartSetting.chartId = this.chartId;
		this.chartSetting.chartConfigs = this.chartConfigs;
	},

	removeIsEnabled$: function() {
		return this.parentVM.KPILayoutSectionEntries.length > 1;
	},

	remove: function() {
		this.parentVM.removeGraph(this);
	},

	save: function() {
		this.chartSetting.save();
	},

	refresh: function(record) {
		this.chartSetting.refresh(this.record);
	}
});
