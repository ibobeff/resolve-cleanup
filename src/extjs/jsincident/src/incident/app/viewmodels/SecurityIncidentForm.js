glu.defModel('RS.incident.SecurityIncidentForm',  {
	//pos: [],
	resolveSelected: true,
	title: '',
	investigationType: '',
	playbook: '',
	severity: '',
	dueBy: '',
	owner: '',
	sourceSystem: 'Resolve',
	externalReferenceId: 'allow',
	playbookNameIsValidated: false,
	severityStore: {
		mtype: 'store',
		fields: ['severity'],
		data: [{severity: 'Critical'}, {severity: 'High'}, {severity: 'Medium'}, {severity: 'Low'}]
	},

	originStore: {
		mtype: 'store',
		fields: ['origin'],
		data: [{origin: 'Resolve'}, {origin: 'Splunk'}]
	},

	add: function() {
		this.fireEvent('sirAdded');
	},

	mSecondsInADay: 86399000, // 24*60*60*1000-1000  number of milli seconds in a day minus 1

	addNewSir: function(data) {
		data.dueBy = (new Date(data.dueBy)).getTime() + this.mSecondsInADay;

		//Orgs Info
		Ext.apply(data, {
			'sysOrg' : clientVM.orgId  || null
		})
		this.ajax({
			url: '/resolve/service/playbook/securityIncident/save',
			jsonData: data,
			method: 'POST',
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.parentVM.store.reload();
				} else {
					clientVM.displayError(response.message)
				}
			},
			failure: function (resp) {
				clientVM.displayFailure(resp);
			}
		});
		this.doClose();
	},

	cancel: function() {
		this.doClose();
	},

	adjustWindowSize: function(v) {
		var me = this,
			parentSize = Ext.getBody().getViewSize();
		/*v.setWidth(parentSize.width *.4);
		v.setHeight(parentSize.height *.27);*/
		this.on('sirAdded', function() {
			me.addNewSir(v.getValues());
		});
		//v.up().setPagePosition(this.pos);
	},

	playbookIsValid$: function() {
		return !!this.playbook && !!this.playbook.length && this.playbookNameIsValidated;
	},

	addIsEnabled$: function() {
		var isValid = function(val) {
			return (val && val.length > 0);
		}
		return (isValid(this.title) && isValid(this.investigationType) && isValid(this.severity)
					&& isValid(this.owner) && isValid(this.sourceSystem)&& isValid(this.externalReferenceId)
					&& isValid(this.playbook) && this.playbookNameIsValidated);
	},

	originSelected: function(record) {
		this.set('resolveSelected', record[0].get('origin') == 'Resolve')
	},

	handlePlaybookSelect: function() {
		this.set('playbookNameIsValidated', true);
	},

	validateTemplateName: function(v) {
		var r = v.store.findRecord('ufullname', v.getValue(), 0, false, false, true);
		this.set('playbookNameIsValidated', r? true: false);
	},

	init: function() {
		this.parentVM.refreshTypes();
		this.parentVM.playbookTemplateStore.on('load', function(store) {
			var r = store.findRecord('ufullname', this.playbook, 0, false, false, true);
			this.set('playbookNameIsValidated', r? true: false);
		}, this);
	}

});