glu.defModel('RS.incident.Dashboard.KPIViewSectionEntry', {
	chartId: undefined,
	init: function() {
		this.set('chartConfigs', this.configs);
	},

	// Chart refresh monitor flag (to be toggled)
	refreshMonitor: true,
	chartConfigs: {},

	applyChartSettings: function(configs) {
		this.set('chartConfigs', configs);
	},

	chartSettingsHandler: function(e) {
		var btn = Ext.getCmp(e.currentTarget.id);
		var chartConfigs = btn.up('chart').chartConfigs;
		var chartType = chartConfigs.chartType;
		var hideScopeConfig = true;
		var hideAgeBucketConfig = true;
		var hideColumnConfig = true;
		if (chartType == this.localize('investigationByType')) {
			hideScopeConfig =  false;
		} else if (chartType == this.localize('investigationBySeverity')) {
			hideScopeConfig = false;
		} else if (chartType == this.localize('investigationByAge')) {
			hideAgeBucketConfig =  false;
		} else if (chartType == this.localize('investigationBySLA')) {
			hideScopeConfig =  false;
		}  else if (chartType == this.localize('investigationByWorkload')) {
			hideScopeConfig =  false;
		} else if (chartType == this.localize('investigationByOwner')) {
			// Will support later
		} else if (chartType == this.localize('investigationByStatus')) {
		} else if (chartType == this.localize('investigationByPhases')) {
			// Will support later
		} else if (chartType == this.localize('investigationOverTimes')) {
		}
		this.open({
			mtype: 'RS.incident.Dashboard.ChartSettings',
			target: btn.up('panel'),
			chartConfigs: chartConfigs,
			hideScopeConfig: hideScopeConfig,
			hideAgeBucketConfig: hideAgeBucketConfig,
			hideColumnConfig: hideColumnConfig,
			dumper: this.applyChartSettings.bind(this)
		});
	},

	refresh: function() {
		this.set('refreshMonitor', !this.refreshMonitor)
	}
});
