glu.defModel('RS.incident.PlaybookTemplateNamer', {
	name: '',
	namespace: '',

	namespaceStore: {
		mtype: 'store',
		fields: ['unamespace'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/nsadmin/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	init: function() {
	},

	wait: false,
	create: function() {
		var newTemplateName = Ext.String.trim(this.namespace) + '.' + Ext.String.trim(this.name); 
		this.set('wait', true);
		this.ajax({
			url : '/resolve/service/playbook/template/new',
			params : {
				fullname: newTemplateName
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					clientVM.handleNavigation({
						modelName: 'RS.incident.PlaybookTemplate',
						params: {
							name: newTemplateName,
							new: true,
							type: 'nist'
						}
					});
					this.doClose();
				} else {
					clientVM.displayError(respData.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},

	namespaceIsValid$: function() {
		return RS.common.validation.TaskNamespace.test(this.namespace) ? true : this.localize('invalidNamespace');
	},

	nameIsValid$: function() {
		return RS.common.validation.TaskName.test(this.name) ? true : this.localize('invalidName');
	},

	createIsEnabled$: function() {
		return !this.wait && this.isValid;
	},

	cancel: function() {
		this.doClose();
	}
});
