glu.defModel('RS.incident.Dashboard.KPIView', {
	viewSections: {
		mtype: 'list',
		autoParent: true
	},

	init: function() {
		clientVM.on('orgchange', this.orgChange.bind(this));
	},

	activate: function() {
		this.refresh();
	},

	loadChartSettings: function() {
		var me = this;
		this.viewSections.removeAll();
		['11', '12', '13', '21', '22', '23', '31', '32', '33'].forEach(function(id) {
			RS.incident.model.ChartSettings.load(id, {
				scope: this,
				callback: function(record) {
					if (record) {
						me.createGraphFrom(record);
					}
				}
			});
		});
		if (!this.viewSections.length) {
			this.createDefaultGraphs();
		}
	},

	createNewGraph: function(id) {
		var dataSourceMap = {
			'11': this.localize('investigationByType'),
			'12': this.localize('investigationBySeverity'),
			'13': this.localize('investigationByAge'),
			'21': this.localize('investigationByTeamMember'),
			'22': this.localize('investigationByStatus'),
			'23': this.localize('investigationOverTimes'),
			'31': this.localize('investigationBySLA'),
			'32': this.localize('investigationByType'),
			'33': this.localize('investigationByAge')
		};
		var graphTypeMap = {
			'11': this.localize('pieChart'),
			'12': this.localize('barGraph'),
			'13': this.localize('pieChart'),
			'21': this.localize('barGraph'),
			'22': this.localize('pieChart'),
			'23': this.localize('barGraph'),
			'31': this.localize('pieChart'),
			'32': this.localize('barGraph'),
			'33': this.localize('pieChart')
		};
		var record = new RS.incident.model.ChartSettings({
			id: id,
			chartType: dataSourceMap[id],
			graphType: graphTypeMap[id]
		});
		record.save();
		this.createGraphFrom(record);
	},

	createDefaultGraphs: function() {
		var me = this;
		['11', '12', '13', '21', '22', '23'].forEach(function(id) {
			me.createNewGraph(id);
		});
	},

	getSectionById: function(sectionId) {
		for (var i=0; i<this.viewSections.length; i++) {
			var section = this.viewSections.getAt(i);
			if (section.sectionId == sectionId) {
				return section;
			}
		}
		return this.createSection(sectionId);
	},

	createGraphFrom: function(record) {
		var section = this.getSectionById(record.get('id').toString().charAt(0));
		section.addGraph(record);
	},

	createSection: function(sectionId) {
		var model = this.model({
			mtype : 'RS.incident.Dashboard.KPIViewSection',
			sectionId: sectionId
		});
		this.viewSections.add(model);
		return this.viewSections.getAt(this.viewSections.length-1);
	},

	refresh: function() {
		for (var i=0; i<this.viewSections.length; i++) {
			this.viewSections.getAt(i).refresh();
		}
	},

	orgChange: function() {
		this.refresh();
	},

	beforeDestroyComponent : function() {
		clientVM.removeListener('orgchange', this.orgChange);
	}
});
