glu.defModel('RS.incident.ExportAuditLog', {
    sir: ' ',
    sirList: '',
    sirIsValidated: true,
    initiallyRendered: true,
    exporting: false,
    sirIsDulicate: false,
    fileType: 'pdf',
    outputFileName: 'ResolveSIRAuditLogs-' + Ext.Date.format(new Date(), 'Ymd-His'),
    twoWaySirNameToIdMap: {},
    sirStore: {
        mtype: 'store',
        fields: ['id', 'sir'],
        pageSize: 25,
        sorters: [{
            property: 'sir',
            direction: 'DESC'
        }],

        proxy: {
            type: 'ajax',
            url: '/resolve/service/playbook/securityIncident/list',
            reader: {
                type: 'json',
                root: 'records'
            },
            listeners: {
                exception: function(e, resp, op) {
                    clientVM.displayExceptionError(e, resp, op);
                }
            }
        }
    },

    fileTypeStore: {
        mtype: 'store',
        fields: ['fileType'],
        data: [{fileType: 'pdf'}, {fileType: 'json'}, {fileType: 'csv'}]
    },

    auditlogStore: {
        mtype: 'store',
        model: 'RS.incident.model.AuditLog',
        pageSize : 1000000, // Don't support Audit log with > million records
        sorters: [{
            property: 'incidentId',
            direction: 'DESC'
        }]
    },
    applyOrgFilter: function(store, op) {
        op.params = op.params || {};
        var curFilter = op.params.filter || '[]';
        if (typeof(curFilter) == 'string') {
            curFilter = Ext.decode(curFilter);
        }
        op.params = Ext.apply(op.params, {
            filter: Ext.encode([{
                field: 'sysOrg',
                type: 'auto',
                condition: 'equals',
                value: clientVM.orgId || 'nil'
            }].concat(curFilter))
        });
    },
    applyFilters: function(store, op) {
        var sirs = this.sirList.split(', ') || [];
        var sirList = this.twoWaySirNameToIdMap[sirs[0]];
        for (var i=1; i<sirs.length; i++) {
            sirList = sirList + ',' + this.twoWaySirNameToIdMap[sirs[i]];
        }
        op.params = op.params || {};
        var curFilter = op.params.filter || '[]';
        if (typeof(curFilter) == 'string') {
            curFilter = Ext.decode(curFilter);
        }
        op.params = Ext.apply(op.params, {
            filter: Ext.encode([{
                field: 'incidentId',
                type:  'terms',
                condition: 'equals',
                value: sirList
            }].concat(curFilter))
        });
    },

    init: function() {
        this.sirStore.on('load', this.validateSirName, this);
        this.sirStore.on('beforeload', this.applyOrgFilter, this);
        this.auditlogStore.on('beforeload', this.applyFilters, this);
    },

    sirIsValid$: function() {
        if (this.isSirAlreadyAdded()) {
            return this.sir + this.localize('isAlreadyOnSIRList');
        }
        var isValid = !!this.sir && !!this.sir.length && this.sirIsValidated;
        return isValid? isValid: this.localize('inValidSirName');
    },

    handleSirSelect: function() {
        this.set('sirIsValidated', true);
    },

    validateSirName: function(store) {
        var r = store.findRecord('sir', this.sir, 0, false, false, true);
        this.set('sirIsValidated', r? true: false);
    },

    prepareRecordsForPDF: function(records) {
        var toBeExported = {};
        for (var i=0; i<records.length; i++) {
            var rec = records[i];
            var sirId = rec.get('incidentId');
            if (!toBeExported[sirId]) {
                toBeExported[sirId] = {
                    sir: this.twoWaySirNameToIdMap[sirId],
                    items: []
                };
            }
            toBeExported[sirId].items.push([
                rec.get('sysCreatedBy'),
                rec.get('ipAddress'),
                rec.get('description'),
                Ext.util.Format.date(new Date(rec.get('sysCreatedOn')), 'm/d/Y H:i:s')
            ]);

        }
        return toBeExported;
    },

    prepareRecords: function(records) {
        var toBeExported = [];
        for (var i=0; i<records.length; i++) {
           var rec = records[i];
           var sirId = rec.get('incidentId');
           toBeExported.push({
               'SIR Name': this.twoWaySirNameToIdMap[sirId],
               'SIR Id': sirId,
               'User': rec.get('sysCreatedBy'),
               'IP Address': rec.get('ipAddress'),
               'Description': rec.get('description'),
               'Logged On': Ext.util.Format.date(new Date(rec.get('sysCreatedOn')), 'm/d/Y H:i:s')
           });
        }
        return toBeExported;
    },

    exportAll: function(sir) {
        this.auditlogStore.load({
            scope: this,
            callback: function(records, operation, success) {
               if (success) {
                   this.set('exporting', true);
                   switch (this.fileType) {
                       case 'pdf':
                           this.exportPDF(this.prepareRecordsForPDF(records));
                           break;
                       case 'json':
                           this.exportJSON(this.prepareRecords(records));
                           break;
                       case 'csv':
                           this.exportCSV(this.prepareRecords(records));
                           break;
                       default:
                           this.exportPDF(this.prepareRecordsForPDF(records));
                           break;
                   }
                   this.close();
               }
        }});
    },

    cancel: function() {
        this.doClose();
    },

    isSirAlreadyAdded: function() {
        return Ext.Array.indexOf(this.sirList.split(', ') || [], this.sir) != -1;
    },

    addIsEnabled$: function() {
        // Only enabled if valid and non-duplicate sir
        this.sirIsDulicate = this.isSirAlreadyAdded();
        return ((this.sir||'').trim() && this.sirIsValidated && !this.sirIsDulicate);
    },

    add: function() {
        var newSirList = this.sirList + ', ' + this.sir;
        var sirId = this.sirStore.findRecord('sir', this.sir).get('id');
        this.set('sirList', newSirList);
        this.twoWaySirNameToIdMap[this.sir] = sirId
        this.twoWaySirNameToIdMap[sirId] = this.sir;
        this.set('sir', ' ');
    },

    getFileWithExtension: function() {
       return  this.outputFileName + '.' + this.fileType;
    },

    exportPDF: function(toBeExported) {
        var fileWithExtension = this.getFileWithExtension();
        var createdBy = clientVM.user.name;
        var createdOn = Ext.Date.format(new Date(), 'Y-m-d H:i:s');
        var content = [];
        for (var sirId in toBeExported) {
            content.push(
                ' ',
                {
                    text: [
                        {text: 'SIR Name: ', style: 'tableHeader'}, { text: this.twoWaySirNameToIdMap[sirId] + ', ', fontSize: 13},
                        {text: 'SIR ID: ', style: 'tableHeader'}, {text: sirId, fontSize: 13}
                    ]
                },

                {table: {
                    headerRows: 1,
                    body: [
                        [{text: 'User', style: 'columnHeader'}, {text: 'IP Address', style: 'columnHeader'}, {text: 'Description', style: 'columnHeader'}, {text: 'Logged On', style: 'columnHeader'}]
                    ].concat(toBeExported[sirId].items)
                }
            });
        }
        content.unshift(
            {text: 'Resolve System', style: 'rsheader'},
            {text: 'Security Incident Response Audit Log', style: 'header'},
            {text: [{text: 'Created By: ', style: 'subheaderlabel'},  {text: createdBy, fontSize: 14}]},
            {text: [{text: 'Created On: ', style: 'subheaderlabel'},  {text: createdOn, fontSize: 14}]}
        );
        var doc = {
            content: content,
            styles: {
                rsheader: {
                    fontSize: 20,
                    bold: true,
                    color: '#6c3',
                    alignment: 'center'
                },
                header: {
                    fontSize: 16,
                    bold: true,
                    alignment: 'center'
                },
                subheaderlabel: {
                    fontSize: 14,
                    bold: true
                },
                columnHeader: {
                    fillColor : '#3d3d3d',
                    color : 'white',
                    bold: true
                },
                tableHeader: {
                    bold: true,
                    fontSize: 13
                }
            },
            defaultStyle: {
                fontSize: 10,
                alignment: 'left'
            }
        };
        pdfMake.createPdf(doc).download(fileWithExtension);
    },

    exportJSON: function(toBeExported) {
        var blob = new Blob([Ext.JSON.encode(toBeExported)], {type : 'application/json'});
        downloadjs(blob, this.getFileWithExtension(), "text/plain"
        );
    },

    exportCSV: function(toBeExported) {
        var csv = Papa.unparse(toBeExported);
        downloadjs(csv, this.getFileWithExtension(), "text/plain");
    }
});