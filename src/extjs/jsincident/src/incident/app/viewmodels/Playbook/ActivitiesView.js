//Parent: RS.incident.Playbook
glu.defModel('RS.incident.ActivitiesView', {
	activityName: '',
	activityWiki: '',
	dueDate: '',
	phase: '',
	activityId: '',
	incidentId: '',
	csrftoken: '',
	dtView: {
		mtype: 'RS.decisiontree.Main'
	},
	dtDisplaymode: false,

	problemId: '',
	wikiSysId: '',
	mockList: [],
	wikiParams: [],
	paramsComponent: {
		worksheetOptionIsVisible: false,
		columnOptionIsVisible: false,
		mtype: 'RS.wiki.resolutionbuilder.Parameter'
	},

	returnHeader$: function() {
		var returnStr = '';
		if (this.phase) {
			returnStr += this.phase;
		}
		if (this.activityWiki) {
			if (returnStr) {
				returnStr += ': ';
			}
			returnStr += this.activityName;
		}
		if (this.dueDate) {
			if (returnStr) {
				returnStr += ' - ';
			}
			returnStr += 'Due Date: ' + Ext.util.Format.date(new Date(this.dueDate), 'Y-m-d');
		}
		return returnStr
	},

	generateiFrame: function() {
		this.set('showAllNotes', false);
		this.cleanIframes();
		document.getElementById('playbook-iframe').innerHTML = '';
		this.ajax({
			url: '/resolve/service/wiki/get/',
			method: 'POST',
			params: {
				id: '',
				name: this.activityWiki
			},
			scope: this,
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					this.set('wikiSysId', results.data.sys_id);
	
					//Load parameters into grid component
					var paramsArr = (results.data.uwikiParameters === '' || results.data.uwikiParameters === null) ? [] : JSON.parse(results.data.uwikiParameters);
					this.set('wikiParams', paramsArr);
					this.paramsComponent.firstColumnStore.removeAll();
					this.paramsComponent.firstColumnStore.add(paramsArr);
					if (results.data.udisplayMode == 'decisiontree') {
						this.set('dtDisplaymode', true);
						this.dtView.activate(null, {
							name: results.data.ufullname,
							problemId: this.problemId,
							sirContext : true
						});
					} else {
						var params = Ext.String.format('&sir={0}&problemId={1}&activityId={2}&activityName={3}&incidentId={4}',
							this.rootVM.sir, this.problemId, this.activityId, this.activityName, this.incidentId);
						var viewurl = window.location.origin + '/resolve/service/wiki/view?wiki=' + window.encodeURI(results.data.ufullname + params) + '&' + this.csrftoken;
						this.set('dtDisplaymode', false);
						document.getElementById('playbook-iframe').innerHTML = '<iframe id="playbook-wikiview" style="width:100%;height:100%;" marginheight="0" marginwidth="0" frameborder="0" problemId="'+this.problemId+'" activityId="'+this.activityId+'" src="' + viewurl + '"></iframe>';
	
					}
				} else {
					clientVM.displayError(this.localize('generateiFrameFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},
	cleanIframes: function() {
		//Clean up anything like leftover iframes and links
		var frame = document.getElementById('playbook-wikiview');
		if(frame && frame.contentWindow)
			frame.contentWindow.postMessage('beforedestroy', '/');		
	},

	executionWin: null,
	execute: function(button) {
		if (this.executionWin) {
			//If exec window open then close
			this.executionWin.doClose();
		} else {
			var me = this;
			this.executionWin = this.open({
				mtype: 'RS.actiontaskbuilder.Execute',
				target: button.getEl().id,
				fromWiki: true,
				newWorksheet: false,
				activeWorksheet: true,
				inputsLoader: function(paramStore, origParamStore, mockStore) {
					//Sourced from RS.wiki.Main
					me.paramsComponent.firstColumnStore.each(function(param) {
						param.data.value = param.get('sourceName');
						param.data.utype = 'PARAM';
						paramStore.add(param);
						origParamStore.add(param);
					})

					var mockNamesArray = [];
					if (me.mockList) {
						for (var i = 0; i < me.mockList.length; i++) {
							mockNamesArray.push({
								uname: me.mockList[i]
							})
						}
					}
					mockStore.loadData(mockNamesArray);
				},
				sirProblemId: this.problemId,
				activityId: this.activityId,
				executeDTO: {
					wiki: this.activityWiki
				}
			});
			this.executionWin.on('closed', function() {
				this.executionWin = null
			}, this);
		}
	},

	returnToActivities: function() {
		//remove activityId from url hash
		window.location.hash = window.location.hash.split('/')[0] + '/sir=' + this.rootVM.ticketInfo.sir;
		this.rootVM.set('isActivitiesView', false);
		this.set('showAllNotes', false);
	},

	showAllNotes: false,
	notes: function() {
		this.set('showAllNotes', !this.showAllNotes);
		if (this.showAllNotes) {
			clientVM.fireEvent('showAllNotes');
		} else {
			clientVM.fireEvent('hideAllNotes');
		}
	},

	notesIsPressed$: function() {
		return this.showAllNotes;
	},

	init: function() {
		this.initToken();
		clientVM.getResultMacroLimit();
	},

	activate: function() {
		this.initToken();
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/wiki/view', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this))
	}
});