/*Parent: RS.incident.Playbook*/
glu.defModel('RS.incident.Activities', {

	activitiesView: {
		mtype: 'RS.incident.ActivitiesView'
	},

	isPreview: false,
	isReadOnly: false,
	activityToolBarDisabled: false,
	previewContent: null,

	when_previewContent_change: {
		on: ['previewContentChanged'],
		action: function() {
			if (this.previewContent) {
				this.getPreviewActivities();
			}
		}
	},

	//Audit Log/Update Completion Status
	handleActivitiesEdit: function(record) {
		if ((record.field === 'dueDate') && (record.originalValue !== (Ext.util.Format.date(record.value, clientVM.getUserDefaultDateFormat())))) {
			clientVM.displaySuccess(this.localize('updateActivitySuccess'));
		} else if ((record.field !== 'dueDate') && (record.originalValue !== record.value)) {
			clientVM.displaySuccess(this.localize('updateActivitySuccess'));
		}
	},

	//Grid copy of members store, updated every time parent's memberStore changes via 'refreshMembers' event
	membersStoreGrid: {
		mtype: 'store',
		fields: [
			'name',
			'userId',
		],
		proxy: {
			type: 'memory'
		}
	},

	retrieveMembers: function() {
		this.membersStoreGrid.loadData(
			this.parentVM.membersStore.getRange().map(function(item) {
				return item.getData()
			})
		);
	},

	//Statuses
	statusStore: {
		mtype: 'store',
		fields: ['type'],
		data: [
			{type: 'Open'},
			{type: 'In Progress'},
			{type: 'Complete'},
			{type: 'N/A'}
		],
		proxy: {
			type: 'memory'
		}
	},

	activityTemplateValue : {
		'id' : '',
		'incidentId' : '',
		'altActivityId' : '',
		'activityName' : '',
		'phase' : '',
		'description' : '',
		'status' : '',
		'assignee' : '',
		'dueDate' : '',
		'days' : '',
		'hours' : '',
		'minutes' : '',
		'wikiName' : '',
		'worksheetId' : '',
		'templateActivity' : true,
		'isPreview' : true
	},

	//Load Activities + Store
	activitiesStore: {
		mtype: 'store',
		groupField: 'phase',
		sorters: [],
		sortOnLoad: false,
		fields: [
			'id',
			'incidentId',
			'altActivityId',
			'activityName',
			'phase',
			'description',
			'status',
			'assignee',
			{name: 'dueDate', type: 'date', convert: function(v, r) {return v ? Ext.util.Format.date(new Date(v), clientVM.getUserDefaultDateFormat()) : v}, serialize: function(v) {return v ? (new Date(v)).getTime() : v}},
			'days',
			'hours',
			'minutes',
			'wikiName',
			'worksheetId',
			{
				name: 'templateActivity',
				type: 'bool',
				defaultValue: true
			},
			{
				name: 'isPreview',
				type: 'bool',
				defaultValue: false
			}
		],
		proxy: {
			type: 'ajax',
			method: 'GET',
			url: '/resolve/service/playbook/activity/listByIncidentId',
			reader: {
				type: 'json',
				root: 'records'
			}
		},
		listeners: {
			recordmoved: function(record, dropRec, dropPosition) {
				var data = [];
				record.store.getRange().forEach(function(r) {
					data.push(r.getData());
				});
				Ext.Ajax.request({
					scope: this,
					url: '/resolve/service/playbook/activity/saveAll',
					method: 'POST',
					jsonData: data,
					params: {
						incidentId: record.get('incidentId')
					},
					success: function(resp, opts) {
						var respData = RS.common.parsePayload(resp);
						if (respData.success) {
							clientVM.displaySuccess(this.rootVM.localize('updateActivitySuccess'));
						} else {
							clientVM.displayError(this.rootVM.localize('updateActivityFail'));
						}
					},
					failure: function(resp, opts) {
						clientVM.displayFailure(resp);
					}
				});
			},
			update: function(store, record, operation, modifiedFieldNames, eOpts ) {
				var data = record.getData();
				if (modifiedFieldNames == 'activityName') {
					record.activityNameDirty = true;
				} else {
					// To prevent the backend from double encoding activityName
					data.activityName = Ext.htmlDecode(data.activityName);
				}
				data.dueDate = data.dueDate ? (new Date(data.dueDate)).getTime() : data.dueDate;
				var loadCallback = this.parentVM.calculateProgress.bind(this.parentVM);
				clientVM.ajax({
					type: 'ajax',
					method: 'POST',
					url: '/resolve/service/playbook/activity/update',
					jsonData: data,
					callback: function(opts, success, resp) {
						store.reload({
							callback: loadCallback
						});
					}
				});
			},
			beforeload: function(store, op) {
				op.params = Ext.apply(op.params || {}, {  // IE doesn't support Object.assign
					incidentId: this.parentVM.parentVM.ticketInfo.id
				});
			},
			load: function() {
				this.parentVM.calculateProgress();
			}
		}
	},

	loadActivities: function(onComplete) {
		this.activitiesStore.reload({
			scope: this,
			callback: function(recs, ops, success) {
				if (!success) {
					clientVM.displayError(this.localize('loadActivitiesFail'));
				}

				if (Ext.isFunction(onComplete)) {
					onComplete();
				}
			}
		});
	},

	getPreviewActivities: function() {
		var activities = {};
		this.activitiesStore.removeAll();
		if (this.previewContent) {
			activities = this.previewContent;
		} else if (this.isPreview && Ext.isFunction(this.rootVM.getContent)) {
			activities = this.rootVM.getContent();
		} else {
			return;
		}

		this.set('activityToolBarDisabled', true);

		var activityList = JSON.parse(activities);
		activityList.forEach(function(activity) {
			// SLA in minutes
			var sla = (parseInt(activity.days) * 24 * 60) + (parseInt(activity.hours) * 60) + (parseInt(activity.minutes));
			if (sla) {
				var currentTime = new Date().getTime();
				activity.dueDate = currentTime + (sla * 60 * 1000);	// convert to milliseconds
			}
			// mock status, assignee, etc
			activity.assignee = this.rootVM.sysCreatedBy;
			activity.status = 'Open';
			activity.templateActivity = true;
			activity.isPreview = true;

			var mockActivity = Ext.clone(this.activityTemplateValue);
			Ext.apply(mockActivity, activity)

			this.activitiesStore.insert(this.activitiesStore.getCount(), mockActivity);
		}.bind(this))
	},

	// short-cut icons
	addNotes: function() {
		this.rootVM.notesTab.openNoteModal(true, null, this.activityId, this.activityName);
	},

	addArtifacts: function() {

	},

	addAttachments: function() {
		this.open({
			mtype: 'RS.incident.UploadAttachment',
			incidentId: this.parentVM.ticketInfo.id,
			activityId: this.activityId
		});
	},

	viewAutomationResults: function() {
		this.open({
			mtype : 'RS.incident.AutomationResults',
			activityFilter: this.activityName,
			activityId: this.activityId,
			modal: true,
			modalTitle: this.localize('viewAutomationResultsTitle', this.activityName),
			padding: 15
		});
	},

	activityName: '',
	activityId: '',
	focuschange: function(old, selections) {
		this.set('activityName', selections.get('activityName'));
		this.set('activityId', selections.get('id'));
	},
	itemmouseenter: function(record, item) {
		this.set('activityName', record.get('activityName'));
		this.set('activityId', record.get('id'));
	},

	//Playbook Progress Bar
	playbookProgress: 0,
	calculateProgress: function() {
		var acc = 0;
		var len = this.activitiesStore.getRange().filter(function(activity) {
			return (activity.getData().id !== null) && (activity.getData().status !== 'N/A')
		}).length;
		this.activitiesStore.each(function(record) {
			if (record.data.status === 'Complete') {
				acc = acc + 1
			}
		});
		this.fireEvent('updateProgressBar', (acc / len))
	},

	//Filter Checkbox Handling
	filterType: [],
	applyFilter: function(value) {
		if (this.filterType.length > 0) {
			return (this.filterType.indexOf(value) !== -1) || (value === null);
		} else {
			return false
		}
	},

	handleFilterChange: function(newFilters) {
		var newFiltersArr = [];
		for (key in newFilters) {
			newFiltersArr.push(newFilters[key])
		}
		this.filterType = newFiltersArr;
		this.activitiesStore.filterBy((function(record) {
			return this.applyFilter(record.data.status)
		}).bind(this));
	},

	//Run on tab activation
	tabInit: function() {
		this.loadActivities();
	},

	init: function() {
		this.set('filterType', ['Open', 'In Progress', 'Complete', 'N/A']) //Set default filters
		this.on('refreshMembers', this.retrieveMembers);

		if (this.isPreview) {
			this.getPreviewActivities();
		}
	},

	//Add Activity Modal
	addActivity: function() {
		this.open({
			mtype: 'RS.incident.ActivityModal',
			ticketInfo: this.parentVM.ticketInfo,
		})
	},
});

//Add Activity
//Parent: RS.incident.Activity
glu.defModel('RS.incident.ActivityModal', {
	ticketInfo: {},
	phase: '',
	name: '',
	slaDays: '0',
	slaHours: '0',
	slaMinutes: '0',
	status: '',
	assignee: '',
	wikiName: '',
	wikiNameIsValidated: false,
	description: '',

	//Load Phases + Store
	phasesStore: {
		mtype: 'store',
		fields: ['type'],
	},

	loadPhases: function() {
		var phases = this.parentVM.activitiesStore.getGroups().map(function(group) {
			return {type: group.name}
		});
		this.phasesStore.loadData(phases);
	},

	//Load Runbook + Validation + Store
	runbookStore: {
		mtype: 'store',
		pageSize: 50,
		model: 'RS.incident.model.Runbook',
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wiki/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				},
				load: function(store) {
					var r = store.findRecord('ufullname', this.wikiName, 0, false, false, true);
					this.set('wikiNameIsValidated', r ? true : false);
				}
			}
		}
	},

	validateWikiName: function(v) {
		var r = v.store.findRecord('ufullname', v.getValue(), 0, false, false, true);
		this.set('wikiNameIsValidated', r ? true : false);
	},

	wikiNameIsValid$: function() {
		return !!this.wikiName && !!this.wikiName.length && this.wikiNameIsValidated;
	},

	handleRunbookSelect: function() {
		this.set('wikiNameIsValidated', true);
	},

	searchRunbook: function() {
		var me = this;
		this.open({
			mtype: 'RS.decisiontree.DocumentPicker',
			dumper: {
				dump: function(selections) {
					if (selections.length > 0) {
						me.set('wikiName', selections[0].get('ufullname'));
						me.set('wikiNameIsValidated', true);
					}
				}
			}
		})
	},

	//SLA Human readable date formatting
	calcDaysText$: function() {
		if (parseInt(this.slaDays) === 1) {
			return 'Day'
		} else {
			return 'Days'
		}
	},

	calcHoursText$: function() {
		if (parseInt(this.slaHours) === 1) {
			return 'Hour'
		} else {
			return 'Hours'
		}
	},

	calcMinutesText$: function() {
		if (parseInt(this.slaMinutes) === 1) {
			return 'Minute'
		} else {
			return 'Minutes'
		}
	},



	saveActivityIsEnabled$: function() {
		var notBlank = function(s) {
			return (s && s.trim().length != 0);
		};
		return (notBlank(this.phase) && notBlank(this.name) && notBlank(this.status)
			&& notBlank(this.assignee) && notBlank(this.wikiName) && this.wikiNameIsValidated);
	},

	//Save Activity
	isSaving: false,
	saveActivity: function() {
		this.set('isSaving', true);
		var params = {
			activityName: this.name,
			description: this.description,
			phase: this.phase,
			wikiName: this.wikiName,
			incidentId: this.ticketInfo.id,
			worksheetId: clientVM.problemId,
			days: this.slaDays,
			hours: this.slaHours,
			minutes: this.slaMinutes,
			status: this.status,
			assignee: this.assignee,
		};

		this.ajax({
			url: '/resolve/service/playbook/activity/add',
			method: 'POST',
			jsonData: params,
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					this.parentVM.activitiesStore.loadData(results.records);
					clientVM.displaySuccess(this.localize('addActivitySuccess'));
					this.doClose();
				} else {
					clientVM.displayError(this.localize('addActivityFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	cancel: function() {
		this.doClose();
	},

	init: function() {
		this.loadPhases();
		this.set('assignee', this.ticketInfo.owner);
		this.set('status', 'Open');
	}

});