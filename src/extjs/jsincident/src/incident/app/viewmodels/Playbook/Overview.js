//Parent: RS.incident.Playbook
glu.defModel('RS.incident.Overview', {
	referenceId: '',
	alertId: '',
	correlationId: '',
	readOnlyDataCompromised: true,
	readOnlyStatus: true,
	readOnlyTeamMebers: true,

	referenceIdIsBlank$: function() {
		return this.referenceId === ''
	},

	alertIdIsBlank$: function() {
		return this.alertId === ''
	},

	correlationIdIsBlank$: function() {
		return this.correlationId === ''
	},

	noRelatedIsVisible$: function() {
		return !((this.referenceId === '') && (this.alertId === '') && (this.correlationId === ''))
	},

	owner: '',

	dataCompromisedStore: {
		mtype: 'store',
		fields: ['display', 'value'],
		data: [
			{display: 'Unknown', value: 'Unknown'},
			{display: 'Yes', value: 'Yes'},
			{display: 'No', value: 'No'},
		],
		proxy: {
			type: 'memory'
		}
	},

	statusStore: {
		mtype: 'store',
		fields: ['display', 'value'],
		data: [
			{display: 'Open', value: 'Open'},
			{display: 'In Progress', value: 'In Progress'},
			{display: 'Complete', value: 'Complete'},
			{display: 'N/A', value: 'N/A'}
		],
		proxy: {
			type: 'memory'
		}
	},

	updateStatus: function(newValue, oldValue) {
		var modifiedTicketInfo = Object.assign({}, this.parentVM.ticketInfo);
		modifiedTicketInfo.status = newValue;

		//Orgs Info
		Ext.apply(modifiedTicketInfo, {
			'sysOrg' : clientVM.orgId
		})
		this.ajax({
			url: '/resolve/service/playbook/securityIncident/save',
			method: 'POST',
			jsonData: modifiedTicketInfo,
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					var status = results.data.status;
					this.parentVM.set('ticketInfo', results.data);
					clientVM.displaySuccess(this.localize('changeStatusSuccess'));
				} else {
					clientVM.displayError(this.localize('changeStatusFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	updateDataCompromised: function(newValue, oldValue) {
		var modifiedTicketInfo = Object.assign({}, this.parentVM.ticketInfo);
		modifiedTicketInfo.dataCompromised = newValue;

		//Orgs Info
		Ext.apply(modifiedTicketInfo, {
			'RESOLVE.ORG_ID' : clientVM.orgId,
			'RESOLVE.ORG_NAME' : clientVM.orgName
		})
		this.ajax({
			url: '/resolve/service/playbook/securityIncident/save',
			method: 'POST',
			jsonData: modifiedTicketInfo,
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					var dataCompromised = results.data.dataCompromised;
					this.parentVM.set('ticketInfo', results.data);
					clientVM.displaySuccess(this.localize('changeDataCompromisedSuccess'));
				} else {
					clientVM.displayError(this.localize('changeDataCompromisedFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	//Copy of members in Overview to trigger formatMembersList formula
	membersCopy: [],
	membersList: {
		mtype: 'list'
	},

	ownerObject: {},

	formatMembersList$: function() {
		this.membersList.removeAll();
		if (this.membersCopy.length !== 0) {
			var formattedArray = this.membersCopy.forEach(function(memberObj, index) {
				var memberName = (memberObj.get('name').trim().length === 0) ? memberObj.get('userId') : memberObj.get('name');
				var userId = memberObj.get('userId');
				if (userId !== this.owner) {
					this.membersList.insert(index + 1, {
						mtype: 'RS.incident.MemberLabel',
						userId: (userId.length > 40) ? userId.slice(0, 40) + '...' : userId, //userId
						memberName: memberName
					});
				}
			}, this);
		} else {
			this.membersList.add({
				mtype: 'RS.incident.MemberLabel',
				userId: 'None',
				memberName: 'None'
			})
		}
	},

	loadRelatedInvestigations: function() {
		this.ajax({
			url: '/resolve/service/worksheet/getWS',
			method: 'GET',
			params: {
				id: this.rootVM.ticketInfo.problemId,
			},
			scope: this,
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					if (results.data) {
						this.set('referenceId', results.data.reference);
						this.set('alertId', results.data.alertId);
						this.set('correlationId', results.data.correlationId);
					}
				} else {
					clientVM.displayError(this.localize('loadRelatedInvestigationsFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	applyRBAC: function()  {
		this.set('readOnlyDataCompromised', this.parentVM.isPreview || !this.rootVM.pSirInvestigationViewerDataCompromisedModify);
		this.set('readOnlyStatus', this.parentVM.isPreview || !this.rootVM.pSirInvestigationViewerStatusModify);
		this.set('readOnlyTeamMebers', this.parentVM.isPreview || !this.rootVM.pSirInvestigationViewerTeamModify);
	},

	init: function() {
		this.rootVM.on('applyrbac', this.applyRBAC.bind(this));
	},

	addMember: function() {
		this.open({
			mtype: 'RS.incident.MemberModal',
			loadMembers: this.parentVM.loadMembers.bind(this.parentVM, this.parentVM.ticketInfo.id),
		});
	}
});

//Parent: RS.incident.Overview
glu.defModel('RS.incident.MemberModal', {
	canAddMembers: true,
	memberSelections : [],
	ownerSelection: '',

	selectedMembersStore: {
		mtype: 'store',
		fields: ['userId', 'displayName'],
	},

	populateList: function() {
		var owner = this.rootVM.ticketInfo.owner || '';
		var secArr = this.rootVM.securityGroupStore.getRange().map(function(item) {
			var member = item.getData();
			if (member.userId !== owner) {
				return member
			}
		}, this).filter(function(notUndefined) {return notUndefined}); //filter out the owner object returning undef from map
		var selectedIndexes = [];
		var membersArr = (this.rootVM.ticketInfo.members || '').split(', '); //remove owner from array list and convert to arr from str
		this.selectedMembersStore.loadData(secArr.map(function(member, index) {
			if (member.userId !== owner) {
				if (membersArr.indexOf(member.userId) !== -1) {
					selectedIndexes.push(index);
				}
			 	return {
					userId: member.userId,
					displayName: member.name
				}
			}
		}, this).filter(function(notUndefined) {return notUndefined})); //filter out the owner object returning undef from map
		this.selectedMembersStore.sort('userId', 'ASC');
		this.fireEvent('selectMember', selectedIndexes);
	},

	saveMembers: function() {
		var modifiedTicketInfo = Object.assign({}, this.rootVM.ticketInfo);
		modifiedTicketInfo.members = this.memberSelections.map(function(member) {
			return member.get('userId');
		});
		//modifiedTicketInfo.members.unshift(this.rootVM.ticketInfo.owner); //Append admin to beginning of list
		modifiedTicketInfo.members = modifiedTicketInfo.members.join(', ');  //create arr str
		modifiedTicketInfo.owner = this.ownerSelection;

		//Orgs Info
		Ext.apply(modifiedTicketInfo, {
			'RESOLVE.ORG_ID' : clientVM.orgId,
			'RESOLVE.ORG_NAME' : clientVM.orgName
		})
		this.ajax({
			url: '/resolve/service/playbook/securityIncident/save',
			method: 'POST',
			jsonData: modifiedTicketInfo,
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					this.loadMembers();
					this.parentVM.set('owner', this.ownerSelection);
					this.rootVM.set('ticketInfo', modifiedTicketInfo);
					clientVM.displaySuccess(this.localize('addMemberSuccess'));
					this.doClose();
				} else {
					clientVM.displayError(this.localize('addMemberFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	cancel: function() {
		this.doClose();
	},

	init: function() {
		this.set('ownerSelection', this.rootVM.ticketInfo.owner);
	}
});

//Allow for member tooltips
glu.defModel('RS.incident.MemberLabel', {
	userId: '',
	memberName: ''
});
