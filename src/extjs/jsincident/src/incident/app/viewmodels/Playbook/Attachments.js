glu.defModel('RS.incident.Attachments', {
	csrftoken: '',
	api: {
		upload: '/resolve/service/playbook/attachment/upload',
		downloadViaDocName: '/resolve/service/playbook/attachment/download?attachId={0}&{1}',
		list: '/resolve/service/playbook/attachment/listBySir?incidentId=', //Shim wiki upload manager but also send incidentId because attachment/listBySir needs it
		update: '/resolve/service/playbook/attachment/update'
	},
	ticketInfo: {},
	incidentId: 'must_be_initialized_with_something', //will change after assigned playbookid

	wait: false,
	when_wait_changed: {
		on: ['waitChanged'],
		action: function() {
			var deleteBtn = this.view.down('button[name="deleteAttachment"]');
			if (this.wait) {
				deleteBtn.setDisabled(true);
			} else if (this.attachmentsSelections.length) {
				deleteBtn.setDisabled(false);
			}
		}
	},

	store: {
		mtype: 'store',
		fields: ['id','name','description','malicious','size'].concat(RS.common.grid.getSysFields()),
	},

	keyword: '',
	allowDownload: true,

	tabInit: function() {
		this.reload();
	},

	view: null,
	initView: function(v) {
		this.set('view', v);
	},

	init: function() {
		this.initToken();
		this.store.on('update', function(store, record, operation, modifiedFieldNames) {
			if (modifiedFieldNames == 'description' || modifiedFieldNames == 'malicious') {
				this.updateAttachment(record.getData());
			}
		}.bind(this));
	},

	activate: function() {
		this.initToken();
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/playbook/attachment/download', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this))
	},

	close: function() {
		this.doClose();
	},

	reload: function() {
		this.store.removeAll();
		var incidentId = this.ticketInfo.id || this.incidentId;
		this.ajax({
			url: this.api.list + incidentId,
			params: {
				docSysId: '',
				docFullName: incidentId
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					Ext.each(respData.records, function(record) {
						this.store.add({
							id: record.id,
							name: record.name,
							description: record.description,
							size: record.size,
							malicious: record.malicious,
							sysCreatedOn: record.sysCreatedOn,
							sysCreatedBy: record.sysCreatedBy
						});
					}, this);
				} else {
					clientVM.displayError(respData.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	addAttachment: function() {
		this.open({
			mtype: 'RS.incident.UploadAttachment',
			url: this.api.upload,
			incidentId: this.incidentId
		});
	},

	attachmentsSelections: [],
	selectionChanged: function(v, selections) {
		this.set('attachmentsSelections', selections);
		var deleteBtn = v.up().down('button[name="deleteAttachment"]');
		if (selections.length) {
			deleteBtn.setDisabled(!this.rootVM.pSirInvestigationViewerAttachmentsDelete);
		} else {
			deleteBtn.setDisabled(true);
		}
	},

	deleteAttachment: function() {
		this.message({
			title: this.localize('deleteAttachment'),
			msg: this.localize('deleteAttachmentWarningMsg'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAttachment'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.reallydeleteAttachment()
			}
		});
	},
	reallydeleteAttachment: function() {
		var attachments = [];
		this.attachmentsSelections.forEach(function(r) {
			var d = r.getData();
			attachments.push({
				id: d.id,
				name: d.name,
				value: d.value
			});
		}, this);
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/playbook/attachment/delete',
			method: 'POST',
			scope: this,
			jsonData: {
				attachments: attachments
			},
			params: {
				incidentId: this.incidentId
			},
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					this.reload();
					clientVM.displaySuccess(this.localize('attachmentsDeleteSuccess'));
					//this.close();
				} else {
					clientVM.displayError(this.localize('attachmentsDeleteFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},
	updateAttachment: function(data) {
		var attachments = [];
		attachments.push({
			id: data.id,
			name: data.name,
			malicious: data.malicious,
			description: data.description
		});
		this.ajax({
			url: this.api.update,
			method: 'POST',
			scope: this,
			jsonData: {
				attachments: attachments
			},
			params: {
				incidentId: this.incidentId
			},
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					this.reload();
					clientVM.displaySuccess(this.localize('attachmentsUpdateSuccess'));
				} else {
					clientVM.displayError(this.localize('attachmentsUpdateFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	}
});

/*
//Parent: RS.incident.Playbook
glu.defModel('RS.incident.Attachments', {
	ticketInfo: {},
	incidentId: 'must_be_initialized_with_something', //will change after assigned playbookid

	api: {
		upload: '/resolve/service/playbook/attachment/upload',
		downloadViaDocName: '/resolve/service/playbook/attachment/download?attachId={1}',
		list: '/resolve/service/playbook/attachment/listBySir?incidentId=' + 'will_change' //Shim wiki upload manager but also send incidentId because attachment/listBySir needs it
	},

	wait: false,
	when_wait_changed: {
		on: ['waitChanged'],
		action: function() {
			var deleteBtn = this.view.down('button[name="deleteAttachment"]');
			if (this.wait) {
				deleteBtn.setDisabled(true);
			} else {
				deleteBtn.setDisabled(false);
			}
		}
	},

	init: function() {
		this.set('wait', false);
	},

	activityFilter: 'All',

	view: null,
	initView: function(v) {
		this.set('view', v);
		var me = this;
		var toolbar = v.down('button[name="uploadFile"]').ownerCt;
		toolbar.addCls('rs-dockedtoolbar');
		Ext.Array.each(toolbar.items.items, function (btn) {
			btn.addCls('rs-btn-light rs-small-btn');
		});

		// insert
		toolbar.insert(0, [{
			xtype: 'button',
			disabled: !this.rootVM.pSirInvestigationViewerAttachmentsCreate,
			text: this.localize('upload'),
			name: 'addAttachment',
			cls: 'rs-btn-light rs-small-btn',
			maxWidth: 80,
			handler: function() {
				me.addAttachment();
			}
		}, {
			xtype: 'button',
			text: this.localize('deleteAttachment'),
			name: 'deleteAttachment',
			cls: 'rs-btn-light rs-small-btn',
			maxWidth: 80,
			disabled: true,
			handler: function() {
				me.deleteAttachment();
			}
		}]);

		var incidentId = this.ticketInfo.id || this.incidentId;
		this.set('api', {
			upload: '/resolve/service/playbook/attachment/upload',
			downloadViaDocName: '/resolve/service/playbook/attachment/download?attachId={1}',
			list: '/resolve/service/playbook/attachment/listBySir?incidentId=' + incidentId  //Shim wiki upload manager but also send incidentId because attachment/listBySir needs it
		});
		this.on('reloadAttachments', function () {
			v.loadFileUploads(true) //Force reload
		});
		var store = v.store;
		store.on('filesuploaded', function(files) {
			if (files && files.length) {
				var filenames = [];
				files.forEach(function (file) {
					filenames.push(file.name);
				});
				clientVM.displaySuccess(me.localize('attachmentsUploadSuccess'));
			}
		});
		v.on('filesadded', function(uploader, files) {
			me.fireEvent('reloadAttachments');
			clientVM.displayError(me.localize('attachmentsUploadNotAllowed'));
		});
		store.on('update', function(store, record, operation, modifiedFieldNames) {
			if (modifiedFieldNames == 'description' || modifiedFieldNames == 'malicious') {
				me.updateAttachment(record.getData());
			}
		});
	},
	tabInit: function() {
		this.fireEvent('reloadAttachments');
	},

	addAttachment: function() {
		this.open({
			mtype: 'RS.incident.UploadAttachment',
			url: this.api.upload,
			incidentId: this.incidentId
		});
	},

	deleteAttachment: function() {
		this.message({
			title: this.localize('deleteAttachment'),
			msg: this.localize('deleteAttachmentWarningMsg'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAttachment'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.reallydeleteAttachment()
			}
		});
	},
	reallydeleteAttachment: function() {
		var attachments = [];
		this.attachmentsSelections.forEach(function(r) {
			var d = r.getData();
			attachments.push({
				id: d.id,
				name: d.name,
				value: d.value
			});
		}, this);
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/playbook/attachment/delete',
			method: 'POST',
			scope: this,
			jsonData: {
				attachments: attachments
			},
			params: {
				incidentId: this.incidentId
			},
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					this.fireEvent('reloadAttachments');
					clientVM.displaySuccess(this.localize('attachmentsDeleteSuccess'));
					//this.close();
				} else {
					clientVM.displayError(this.localize('attachmentsDeleteFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	attachmentsSelections: [],
	selectionChanged: function(v, selections) {
		this.set('attachmentsSelections', selections);
		var deleteBtn = v.down('button[name="deleteAttachment"]');
		if (selections.length) {
			deleteBtn.setDisabled(!this.rootVM.pSirInvestigationViewerAttachmentsDelete);
		} else {
			deleteBtn.setDisabled(true);
		}
	},

	updateAttachment: function(data) {
		var attachments = [];
		attachments.push({
			id: data.id,
			name: data.name,
			malicious: data.malicious,
			description: data.description
		});
		this.ajax({
			url: '/resolve/service/playbook/attachment/update',
			method: 'POST',
			scope: this,
			jsonData: {
				attachments: attachments
			},
			params: {
				incidentId: this.incidentId
			},
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					this.fireEvent('reloadAttachments');
					clientVM.displaySuccess(this.localize('attachmentsUpdateSuccess'));
				} else {
					clientVM.displayError(this.localize('attachmentsUpdateFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	}
});
*/
