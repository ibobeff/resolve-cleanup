//Parent: RS.incident.Playbook
glu.defModel('RS.incident.AuditLog', {
	auditlogStore: {
		mtype: 'store',
		model: 'RS.incident.model.AuditLog',
		pageSize : 25,
		//remoteSort: true,
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		listeners: {
			beforeload: function(store, op) {
				op.params = Ext.apply(op.params || {}, {
					filter: '[{' +
						'"field":"incidentId",' +
						'"type":"auto",' +
						'"condition":"equals",' +
						'"value":"' + this.parentVM.parentVM.ticketInfo.id + '"}]'
				});
			}
		}
	},

	loadAuditLog: function() {
		this.auditlogStore.reload({
			scope: this,
			callback: function(recs, ops, success) {
				if (!success) {
					clientVM.displayError(this.parentVM.localize('auditlogLoadFail'));
				}
			}
		});
	},

	tabInit: function() {
		this.loadAuditLog();
	},

	init: function() {
	},

	exportAuditlog: function() {
		var twoWaySirNameToIdMap = {};
		var sirId = this.rootVM.ticketInfo.id;
		var sirName = this.rootVM.ticketInfo.sir;
		twoWaySirNameToIdMap[sirName] = sirId;
		twoWaySirNameToIdMap[sirId] = sirName;

		this.open({
			mtype: 'RS.incident.ExportAuditLog',
			sirList: this.rootVM.ticketInfo.sir,
			twoWaySirNameToIdMap: twoWaySirNameToIdMap
		});
	}
});