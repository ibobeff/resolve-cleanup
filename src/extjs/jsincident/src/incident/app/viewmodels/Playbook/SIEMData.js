glu.defModel('RS.incident.SIEMData', {
	data: {},

	loadSIEMData: function() {
		var rawData = this.rootVM.ticketInfo.requestData;
		if (rawData === null) {
			this.set('data', this.localize('noRequestData'));
		} else {
			this.set('data', JSON.stringify(JSON.parse(rawData), null, 2));
		}
	},

	tabInit: function() {
		this.loadSIEMData();
	}
});