glu.defModel('RS.incident.PDFConverter',{
	mixinAPI : {
		getAttachments : '/resolve/service/playbook/attachment/listBySir',
		getWS : '/resolve/service/worksheet/getWS',
		getNote : '/resolve/service/playbook/note/list',
		getArtifact : '/resolve/service/playbook/artifact/listBySir',
		getImageInfo : '/resolve/service/wiki/getSIRAttachments'
	},
	dataParts : {
		summary : null,
		description : null,
		notes : null,
		artifacts : null,
		activities : null,
		attachments : null,
		automationResults : null
	},
	convertingProgessTask : null,
	imageIdInfo : {},
	csrftoken: '',
	preparePDF : function(){
		//Get all image id that used in description
		this.ajax({
			url: this.mixinAPI['getImageInfo'],
			method: 'GET',
			params: {
				docSysId: '',
				docFullName: this.ticketInfo.playbook,
				incidentId: null,
				sir: this.ticketInfo.sir
			},
			scope: this,
			success: function(resp) {
				var response = RS.common.parsePayload(resp);
				if(response.success && response.records){
					for(var i = 0; i < response.records.length; i++){
						var r =response.records[i];
						this.imageIdInfo[r.fileName] = r.id;
					}
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
		
		var fullName = this.ticketInfo.playbook.split('.');
		var uri = Ext.String.format('/resolve/service/wiki/download/{0}/{1}', fullName[0], fullName[1]);
		clientVM.getCSRFToken_ForURI(uri, function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this))
	},
	clearPDFPart : function(){
		Ext.apply(this.dataParts, {
			summary : null,
			description : null,
			notes : null,
			artifacts : null,
			activities : null,
			attachments : null,
			automationResults : null
		})
	},
	exportPDF : function(){
		this.clearPDFPart();
		var msgBox = this.message({
			title : this.localize('convertPDFTitle'),
			msg : this.localize('convertPDFMsg'),
			closable : false,
			buttonText : {
				ok : this.localize('abort')
			},
			scope : this,
			fn : function(btn){
				this.abortConverting();
			}
		})
		//Summary
		this.getSummaryFragments(this.ticketInfo);

		//Description
		this.getDescriptionFragments(this.ticketInfo.description);

		//Activities
		this.getActivitiesFragments();

		//Notes
		this.getNoteFragments();

		//Artifacts
		this.getArtifactFragments();

		//Attachments
		this.getAttachmentFragments();

		//Automation Result
		this.getAutomationResultFragments();

		var taskStartTime = new Date().getTime();
		this.convertingProgessTask = setInterval(function(){
			var completed = true;
			for(var k in this.dataParts){
				if(this.dataParts[k] == null){
				completed = false
					return;
				}
			}
			if(completed){
				var pdfFile = this.getPDFContent();
				//console.log(JSON.stringify(pdfFile));
				var currentDate = new Date();
				var formatedDate = (currentDate.getMonth() + 1) + '-' + (currentDate.getDate() < 10 ? '0' + currentDate.getDate() : currentDate.getDate() ) + '-' + currentDate.getFullYear();
				pdfMake.createPdf(pdfFile).download(this.ticketInfo.title + ' (' + formatedDate + ').pdf');
				this.abortConverting();
				msgBox.doClose();
			}
			if(taskStartTime == taskStartTime + 1000 * 60)
				this.abortConverting();
		}.bind(this), 1000)
	},
	abortConverting : function(){
		this.clearPDFPart();
		if(this.convertingProgessTask)
			clearInterval(this.convertingProgessTask);
	},
	CONST : {
		PAGE_SIZE : 'A4',
		PAGE_DIMENSION : 'portrait',
		PAGE_MARGIN : [20,20,20,20],
		AVERAGE_CHAR_WIDTH : 6.5
	},
	getPDFContent : function(){
		return {
			pageSize: this.CONST.PAGE_SIZE,
   			pageMargins: this.CONST.PAGE_MARGIN,
   			content :
   			[{
		        text : this.ticketInfo.title,
		        style : ['incidentTitle']
    		},
    			this.dataParts.summary,
		    	this.dataParts.description, '\n',
		    	this.dataParts.activities, '\n',
		    	this.dataParts.notes, '\n',
		    	this.dataParts.artifacts, '\n',
		    	this.dataParts.attachments, '\n',
		    	this.dataParts.automationResults
	    	],
   			styles : {
		        partHeader : {
		            bold: true,
		            fontSize: 15,
		            margin : [0,10,0,0]
		        },
		        incidentTitle : {
		            bold: true,
		            fontSize: 20,
		            margin : [0,0,0,5]
		        },
		        tableHeader : {
		        	bold : true,
		        	fillColor : '#3d3d3d',
		        	color : 'white'
		        },
		        tableGroup : {
		        	color : 'white',
		        	fillColor : '#777777',
		        	alignment : 'center'
		        }
		    }
		}
	},

	getSummaryFragments : function(ticketInfo){
		this.ajax({
			url: this.mixinAPI['getWS'],
			method: 'GET',
			params: {
				id: ticketInfo.problemId,
			},
			scope: this,
			success: function(resp) {
				var response = RS.common.parsePayload(resp);
				if (response.success) {
					var members = ticketInfo.owner + ' (owner)' + (ticketInfo.members ? ', ' + ticketInfo.members : "");
					var ids = ['correlationId','alertId','reference'];
					var relatedInvestigationRecords = [];
					for(var i = 0; i < ids.length; i++){
						if(response.data[ids[i]])
							relatedInvestigationRecords.push(this.localize(ids[i]) + ': ' + response.data[ids[i]]);
					}
					if(relatedInvestigationRecords.length == 0)
						relatedInvestigationRecords.push('None');

					this.dataParts.summary = [
						{
							text : 'Summary',
							style : ['partHeader']
						},
						'ID: ' + ticketInfo.sir,
						'Severity: ' + ticketInfo.severity,
						'Type: ' + ticketInfo.investigationType,
						'Source: ' + ticketInfo.sourceSystem,
						'Data Compromised: ' + ticketInfo.dataCompromised,
						'Status: ' + ticketInfo.status,
						{
							text : 'Team',
							style : ['partHeader']
						},
						members,
						{
							text : this.localize('relatedInvestigations'),
							style : ['partHeader']
						},
						[relatedInvestigationRecords]
					]
				} else {
					clientVM.displayError(this.localize('loadRelatedInvestigationsFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	getActivitiesFragments : function(){
		var partTitle = this.localize('activitiesTab');
		var fields = [{
			name : 'activityName',
			locale : 'activity',
			show : true,
			width : '*',
			required : true,
			errorDisplay : this.localize("noActivityDefined")
		},{
			name : 'days',
			locale : 'sla',
			show : true,
			width : 70,
			convert : function(val, record){
				var d = record.days;
				var h = record.hours;
				var m = record.minutes;
				return (d ? (d + ' Days') : '' ) + (h ? (h + ' Hours') : '' ) + (m ? (m + ' Minutes') : '' )
			}
		},{
			name : 'dueDate',
			locale : 'dueBy',
			show : true,
			width : 70,
			convert: function(val) {
				return val ? Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat()) : '';
			}
		},{
			name : 'assignee',
			locale : 'assignee',
			show : true,
			width : 70
		},{
			name : 'status',
			locale : 'status',
			show : true,
			width : 70
		}];
		var groupConfig = {
			groupField : 'phase'
		}
		this.dataParts.activities = this.getTableFragments('activities', partTitle, fields, this.getActivityList() , groupConfig);
	},
	getNoteFragments : function(){
		var filter = [{field:"incidentId",type:"string",condition:"equals",value: this.rootVM.ticketInfo.id}];
		this.ajax({
			url: this.mixinAPI['getNote'],
			method : 'GET',
			params : {				
				filter: JSON.stringify(filter)
			},
			success: function(resp) {
				var response = RS.common.parsePayload(resp);
				if (response.success) {
					var partTitle = this.localize('notesTab');
					var fields = [{
						name : 'activityName',
						locale : 'activity',
						show : true,
						width : 70
					},{
						name : 'postedBy',
						locale : 'addedBy',
						show : true,
						width : 70
					},{
						name : 'note',
						locale : 'notePreview',
						show : true,
						width : '*'
					},{
						name: 'sysCreatedOn',
						type: 'int',
						locale : 'addedOn',
						show : true,
						width : 70,
						convert: function(val) {
							return Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat())
						}
					},{
						name: 'sysUpdatedOn',
						type: 'int',
						locale : 'updatedOn',
						show : true,
						width : 70,
						convert: function(val) {
							return Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat())
						}
					}];
					this.dataParts.notes = this.getTableFragments('notes', partTitle, fields, response.records);
				} else {
					clientVM.displayError(this.localize('loadRelatedInvestigationsFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	getArtifactFragments : function(){
		this.ajax({
			url: this.mixinAPI['getArtifact'],
			method : 'GET',
			params : {
				sir: this.ticketInfo.sir
			},
			success: function(resp) {
				var response = RS.common.parsePayload(resp);
				if (response.success) {
					var partTitle = this.localize('artifactsTab');
					var fields = [{
						name: 'activityName',
						locale : 'activity',
						show : true,
						width : 70
					},{
						name : 'sysCreatedBy',
						locale : 'addedBy',
						show : true,
						width : 70
					},{
						name : 'name',
						locale : 'name',
						show : true,
						width : 70
					},{
						name : 'value',
						locale : 'value',
						show : true,
						width : '*'
					},{
						name: 'sysCreatedOn',
						type: 'int',
						locale : 'addedOn',
						show : true,
						width : 70,
						convert: function(val) {
							return Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat())
						}
					},{
						name: 'sysUpdatedOn',
						type: 'int',
						locale : 'updatedOn',
						show : true,
						width : 70,
						convert: function(val) {
							return Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat())
						}
					}];
					this.dataParts.artifacts = this.getTableFragments('artifacts', partTitle, fields, response.records);
				} else {
					clientVM.displayError(this.localize('loadRelatedInvestigationsFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	getAttachmentFragments : function(){
		this.ajax({
			url: this.mixinAPI['getAttachments'] + '?incidentId=' + this.ticketInfo.id,
			params: {
				docSysId: '',
				docFullName: this.ticketInfo.id
			},
			method : 'POST',
			scope: this,
			success: function(resp){
				var response = RS.common.parsePayload(resp);
				if (response.success) {
					var partTitle = this.localize('attachmentsTab');
					var fields = [{
						name : 'name',
						locale : 'name',
						show : true,
						width : '*'
					},{
						name : 'size',
						locale : 'size',
						show : true,
						width : 70
					},{
						name : 'malicious',
						locale : 'malicious',
						show : true,
						width : 70,
						convert : function(val){
							return val ? 'Yes' : 'No';
						}
					},{
						name : 'sysCreatedOn',
						locale : 'uploadedOn',
						show : true,
						width : 70,
						convert: function(val) {
							return Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat())
						}
					},{
						name: 'sysCreatedBy',
						locale : 'uploadedBy',
						show : true,
						width : 70
					}];
					this.dataParts.attachments = this.getTableFragments('attachments', partTitle, fields, response.records);
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	getAutomationResultFragments : function(){
		var partTitle = this.localize('automationresultsTab');
		var fields =  [{
			name : 'activityName',
			locale : 'activity',
			show : true,
			width : 70
		},{
			name : 'taskSummary',
			locale : 'taskSummary',
			show : true,
			width : 120
		},{
			name : 'summary',
			locale : 'summary',
			show : true,
			width : '*'
		},{
			name : 'sysCreatedOn',
			locale : 'createdOn',
			show : true,
			width : 70,
			convert: function(val) {
				return Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat())
			}
		},{
			name: 'severity',
			locale : 'result',
			show : true,
			width : 70
		}];
		//For now just assume there is only 1 layer of grouping. REVISIT LATER.
		var rootNode = this.automationresultsTab.getCurrentData();
		var childNodes = rootNode.childNodes;
		var records = [];
		for(var i = 0; i < childNodes.length; i++){
			var childNode = childNodes[i];
			records.push(childNode.data);
		}
		this.dataParts.automationResults = this.getTableFragments('automationResults', partTitle, fields, records);
	},

	//DESCRIPTION
	getDescriptionFragments : function(content){
		var re = /(\{section[^\}]*\})([\s\S]*?)\{section\}/gmi;
		var match;
		var result = [{
	        "text": "Description",
	        "style": ["partHeader"]
	    }];
		this.currentImgId = 0;
		this.imgDataList = {};
		while (match = re.exec(content)) {
			if (match[1].search('procedure') != -1) {
				// Skip procedure section
				continue;
			}
			result = result.concat(this.extractSection(match[2]));
		}
		if(this.currentImgId == 0)
			this.dataParts.description = result;
		else {
			var startTime = new Date().getTime();
			var progessCheckingTask = setInterval(function(){
				//All data for image came back
				if(this.currentImgId == Object.keys(this.imgDataList).length){
					for(var i = 0; i < result.length; i++){
						var replacementStr = result[i];
						if(this.imgDataList.hasOwnProperty(replacementStr))
							result[i] = {
								image : this.imgDataList[replacementStr],
								width : 500,
								alignment : 'center'
							};
					}
					clearInterval(progessCheckingTask);
					this.dataParts.description = result;
				}
				if(new Date().getTime() >= startTime + 1000 * 60)
					clearInterval(progessCheckingTask);
			}.bind(this),1000)
		}
	},
	extractSection: function(sec) {
		var re = /(<td.*class="wiki-builder">)([\s\S]*?)<\/td class="wiki-builder">/gmi, match, prop, content, table;
		var result = [];
		if (table = /<table[^>]+>/gmi.exec(sec)) {
			while(match = re.exec(sec)) {
				result = result.concat(this.extractComponent(match[2]));
			}
		} else {
			result = result.concat(this.extractComponent(sec));
		}
		return result;
	},
	extractComponent: function(comContent) {
		var content;
		var result = [];
		if(content = /<novelocity.*>([\s\S]*)<\/novelocity>/gmi.exec(comContent)) {
			var paragraphs = this.splitTextParagraph(content[1].replace(/&nbsp;/g, ' '));
			for(var i = 0; i < paragraphs.length; i++)
				result.push(this.extractText(paragraphs[i]));

		} else if (content = /(\{image[^\}]*\})/gmi.exec(comContent)) {
			result.push(this.extractImage(content[1]));
		}
		return result;
	},
	splitTextParagraph : function(content){
		//Strip all div tag.
		var regex = /([^<>]*)<div[^>]*>(.*?)<\/div>/g;
		var result = [];
		while ((m = regex.exec(content)) !== null) {
		    if (m.index === regex.lastIndex)
		        regex.lastIndex++;

		    m.forEach(function(match, groupIndex){
		       if(groupIndex != 0 && match){
		       		result.push(match);
		       }
		    });
		}
		return result;
	},
	currentImgId : 0,
	imgDataList : {},
	extractImage : function(content){
		var src = content.split('|')[0].split(':')[1].split('}')[0];
		var srcId = this.imageIdInfo[src];
		var fullName = this.ticketInfo.playbook.split('.');
		var imgSrc = Ext.String.format('/resolve/service/wiki/download/{0}/{1}?attach={2}&{3}', fullName[0], fullName[1], srcId, this.csrftoken);
		var imgId = ++this.currentImgId;
		RS.common.getDataUri(imgSrc, function(data){
			this.imgDataList['{' + imgId + '}'] = data;
		}, this)
		return '{' + imgId + '}';
	},
	extractText : function(content){
		content = content.replace(/\n/g, '').replace(/<br>/g, '\n');
		if(/^[a-zA-Z0-9\s]*$/.test(content))
			return content;
		var regex = />([^<>]*)</g;
		var m;
		var result = '';
		while ((m = regex.exec(content)) !== null) {
			if (m.index === regex.lastIndex)
				regex.lastIndex++;
			m.forEach(function(match, groupIndex){
				if(groupIndex == 1)
					result += match;
			});
		}
		return result;
	},
	getTableFragments : function(dataPartName, partTitle, fields, records, groupConfig){

		if(!records || records.length == 0){
			return [{
	        	text : partTitle,
	        	style : ["partHeader"]
	      	},this.localize(dataPartName + 'StoreIsEmpty')]
		}

		//Table Style
		pdfMake.tableLayouts = {
	  		resolveTable: {
			    hLineColor: function (i) {
		      		return '#3d3d3d';
			    },
			    vLineColor: function (i) {
		      		return '#3d3d3d';
			    },
			    fillColor: function (i, node) {
					return i % 2 == 0 ? '#dadada' : 'white'
				},
				paddingTop : function(i, node){
					return 5;
				},
				paddingBottom : function(i, node){
					return 5;
				}
	  		}
		};

		var tableHeader = [];
		var rowWidth = [];
		var allRows = [];
		//Add Header
		for(var i = 0; i < fields.length; i++){
			var field = fields[i];
			if(field.show){
				tableHeader.push({
					text : this.localize(field.locale),
					style : 'tableHeader'
				})
				rowWidth.push(field.width ? field.width : 'auto');
			}
		}
		this.getActualColumnWidth(rowWidth);

		//Add Row
		var groupMaps = {
			_default : []
		};
		for(var i = 0; i < records.length; i++){
			var record = records[i];
			var groupName = '_default';
			if(groupConfig && groupConfig.groupField){
				groupName = record[groupConfig.groupField];
				if(!groupMaps.hasOwnProperty(groupName))
					groupMaps[groupName] = [];
			}

			var row = [];
			for(var j = 0; j < fields.length; j++){
				var field = fields[j];

				if(field.show){
					var fieldRawValue = field.mapping ? record[field.mapping] : record[field.name];
					var value = Ext.isFunction(field.convert) ? field.convert(fieldRawValue, record) : fieldRawValue;

					//Mising required field will display empty row instead
					if(!value && field.required){
						row = [{
							text : field.errorDisplay,
							colSpan : tableHeader.length,
							alignment : 'center'
						}]
						break;
					} else
						row.push(value ? this.wrapLongName(value, rowWidth[row.length]) : 'N/A');
				}
			}
			groupMaps[groupName].push(row);
		}
		for(var g in groupMaps){
			if(g != '_default'){
				allRows.push([{
					text : g,
					colSpan : tableHeader.length,
					style : 'tableGroup'
				}]);
			}
			if(groupMaps[g].length > 0)
				allRows = allRows.concat(groupMaps[g]);
		}

		return [{
        	text : partTitle,
        	style : ["partHeader"]
      	},{
      		layout : 'resolveTable',
      		margin : [0,5,0,0],
      		table : {
      			headerRows : 1,
      			widths : rowWidth,
      			body : [tableHeader].concat(allRows)
      		}
      	}]
	},
	getActualColumnWidth : function(widths){
		var pageSizeInfo = {
			A0: [2383.94, 3370.39],
			A1: [1683.78, 2383.94],
			A2: [1190.55, 1683.78],
			A3: [841.89, 1190.55],
			A4: [595.28, 841.89],
			A5: [419.53, 595.28]
		}
		var pageWidth = pageSizeInfo[this.CONST.PAGE_SIZE][this.CONST.PAGE_DIMENSION == 'landscape' ? 1 : 0] - this.CONST.PAGE_MARGIN[1] - this.CONST.PAGE_MARGIN[3] - 45;
		var autoSizeEntry = [];
		widths.forEach(function(w,i){
			if(!isNaN(w)){
				pageWidth -= w;
			}
			else
				autoSizeEntry.push(i);
		})
		autoSizeEntry.forEach(function(i){
			widths[i] = Math.max(0 , parseInt(pageWidth / autoSizeEntry.length));
		})
	},
	wrapLongName : function(name, thresholdWidth){
		name = name.toString();		
		var maxCharPerWord = parseInt(thresholdWidth / this.CONST.AVERAGE_CHAR_WIDTH);
		var regex = new RegExp('[^\\s]' +'{' + maxCharPerWord + '}','gi');		
		name = name.replace(regex, function(matched, index){
			return matched + ' ';
		})		
		return name;
	}
})