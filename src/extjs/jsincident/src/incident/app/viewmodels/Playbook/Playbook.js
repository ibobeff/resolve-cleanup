glu.defModel('RS.incident.Playbook', {
	mixins : ['PDFConverter'],
	sirContext: true,

	overviewComponent: {
		mtype: 'RS.incident.Overview'
	},

	isPreview: false,
	isReadOnly: false,
	sysCreatedOn: '',
	sysCreatedBy: '',

	activeTab: 0,
	activeTabMap : {
		ACTIVITIES : 0,
		NOTES : 1,
		ARTIFACTS: 2,
		ATTACHMENTS : 3,
		AUTOMATIONRESULTS : 4,
		AUDITLOG : 5
	},
	activityList : [],
	activitiesTab: {
		mtype: 'RS.incident.Activities'
	},

	notesTab: {
		mtype: 'RS.incident.Notes'
	},

	artifactsTab: {
		mtype: 'RS.incident.Artifacts'
	},

	attachmentsTab: {
		mtype: 'RS.incident.Attachments'
	},

	automationresultsTab: {
		mtype: 'RS.incident.AutomationResults'
	},

	auditlogTab: {
		mtype: 'RS.incident.AuditLog'
	},

	siemdataTab: {
		mtype: 'RS.incident.SIEMData'
	},

	/*relatedinvestigationsTab: {
		mtype: 'RS.incident.RelatedInvestigations'
	},*/

	isActivitiesView: false,
	activityId: '',

	activityFilterStore : {
		mtype : 'store',
		fields : ['activityName']
	},

	isTabPanelVisible$: function() {
		return this.isActivitiesView
	},

	isActivitiesViewVisible$: function() {
		return !this.isActivitiesView
	},

	initialLoad: true,
	//Provides extra control flow for tabs that can't use simple proxy loading
	loadTabs: function(ticketInfo) {
		//Replace with whatever tab needs to be loaded first, will be called after ticketInfo retrieved.
		this.activitiesTab.tabInit();

		//Load related investigations in overview
		this.overviewComponent.loadRelatedInvestigations();

		//Attachments needs info set explicitly
		this.attachmentsTab.set('ticketInfo', ticketInfo);
		this.attachmentsTab.set('incidentId', ticketInfo.id);

		//Set description. Prevent description from being loaded multiple times.
		if (this.initialLoad) {
			this.fireEvent('loadDescription', ticketInfo.description, ticketInfo.playbook);
			this.set('initialLoad', false);
		}
	},

	//Load TicketInfo + Ticket Info Object
	ticketInfo: {
		closedBy: '',
		closedOn: '',
		dataCompromised: '',
		description: '',
		externalStatus: '',
		externalReferenceId: '',
		id: '',
		investigationType: '',
		owner: '',
		playbook: '',
		playbookVersion: '',
		primary: '',
		problemId: '',
		sequence: '',
		severity: '',
		sir: '',
		sirStarted: '',
		sourceSystem: '',
		status: '',
		sysCreatedBy: '',
		sysCreatedOn: '',
		sysIsDeleted: '',
		title: ''
	},

	resultsInfo: {},

	loadTicketInfo: function(sir) {
		this.ajax({
			url: '/resolve/service/playbook/securityIncident/get',
			method: 'GET',
			params: {
				sir: sir,
			},
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if ((results.data === null) && (results.total === 0)) {
					this.message({
						title: this.localize('invalidIncidentTitle'),
						msg: this.localize('invalidIncident'),
						closable : false,
						buttonText: {
							ok: this.localize('close')
						},
						scope: this,
						fn: function(btn) {
							clientVM.handleNavigation({
								modelName: 'RS.incident.Dashboard'
							});
						}
					});
				}
				if (results.success) {
					this.set('activityList',results.data.INCIDENT_ACTIVITIES );
					var infoObj = results.data.INCIDENT_INFO
					this.set('ticketInfo', infoObj);
					this.loadMembers(results.data.INCIDENT_INFO.id);
					this.loadTabs(infoObj);

					//Load activity view if wiki param exists
					if (this.isActivitiesView) {
						var selectedActivity = results.data.INCIDENT_ACTIVITIES.filter(function(activity) {return activity.id === this.activityId}, this)[0];
						this.activitiesTab.activitiesView.set('activityWiki', selectedActivity.wikiName);
						this.activitiesTab.activitiesView.set('activityName', selectedActivity.activityName);
						this.activitiesTab.activitiesView.set('dueDate', selectedActivity.dueDate);
						this.activitiesTab.activitiesView.set('phase', selectedActivity.phase);
						this.activitiesTab.activitiesView.set('problemId', results.data.INCIDENT_INFO.problemId);
						this.activitiesTab.activitiesView.set('activityId', selectedActivity.id);
						this.activitiesTab.activitiesView.set('incidentId', this.ticketInfo.id);
						this.activitiesTab.activitiesView.generateiFrame();
					}

					this.set('resultsInfo', {
						sirProblemId : results.data.INCIDENT_INFO.problemId,
						automationFilter : results.data.AUTOMATION_FILTER,
						activities : results.data.INCIDENT_ACTIVITIES.filter(function(activity) {return activity.id})
					})

					//Load Activities filters
					var info = this.resultsInfo;
					info['activities'].splice(0, 0, {activityName : 'All'});
					this.activityFilterStore.loadData(info['activities']);

					//Prepare PDF converter
					this.preparePDF();

					this.automationresultsTab.updateInfo(this.resultsInfo);
					this.applyRBAC();
				} else {
					clientVM.displayError(results.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	//Load Security Group + Store
	securityGroupStore: {
		mtype: 'store',
		fields: ['userId', 'name']
	},
	loadActivities: function(onComplete) {
		this.activitiesTab.loadActivities(onComplete);
	},
	getActivityList : function(){
		var activities = this.activitiesTab.activitiesStore.getRange();
		return activities && activities.length > 0 ? activities.map(function(r){ return r.data }) : this.activityList;
	},
	loadSecurityGroup: function(callback) {
		this.ajax({
			url: '/resolve/service/playbook/securityIncident/getSecurityGroup',
			method: 'GET',
			scope: this,
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					this.securityGroupStore.loadData(results.records);
					this.securityGroupStore.sort('userId', 'ASC');
					if(typeof(callback) == 'function') {
						callback.call(this);
					}
				} else {
					clientVM.displayError(this.localize('securitygroupLoadFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	//Load Members + Store
	membersStore: {
		mtype: 'store',
		fields: ['userId', 'name']
	},

	loadMembers: function(id) {
		this.loadSecurityGroup(function() {
			this.ajax({
				url: '/resolve/service/playbook/securityIncident/get',
				method: 'GET',
				params: {
					incidentId: id,
				},
				success: function(response) {
					var results = RS.common.parsePayload(response);
					if (results.success) {
						var membersArr = results.data.INCIDENT_INFO.members || '';
						this.overviewComponent.set('owner', results.data.INCIDENT_INFO.owner) //Set owner manually so binding in overview can be dynamically updated.
						if (membersArr !== null && membersArr !== '') {
							var membersObjArr = membersArr.split(', ').map(function(userId) {
								var name = '';
								var r = this.securityGroupStore.findRecord('userId', userId);
								if (r) {
									name = r.getData().name;
								}
								return {
									userId: userId,
									name: name
								}
							}, this);
							this.membersStore.loadData(membersObjArr);
							//Set copy of members in overview to trigger formatting formula
							var membersCopy = this.membersStore.getRange();
							this.overviewComponent.set('membersCopy', membersCopy);
							this.activitiesTab.fireEvent('refreshMembers');
						}
					} else {
						clientVM.displayError(this.localize('membersLoadFail'));
					}
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			});
		});
	},

	fullName: '',
	previewUContent: null,

	getPreviewContent: function(onSuccess) {
		this.ajax({
			url: '/resolve/service/wiki/get/',
			method: 'POST',
			params: {
				id: '',
				name: this.fullName
			},
			scope: this,
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					this.uname = results.data.uname;
					this.unamespace = results.data.unamespace;
					this.id = results.data.id;
					this.set('previewUContent', results.data.ucontent);
					if (!this.sysCreatedOn) {
						this.set('sysCreatedOn', results.data.sysUpdatedOn);
						this.set('sysCreatedBy', results.data.sysCreatedBy);
					}

					var ucontent = results.data.ucontent;
					var re = /(\{section[^\}]*\})([\s\S]*?)\{section\}/gmi;
					var match;
					while (match = re.exec(ucontent)) {
						if (match[1].search('procedure') != -1) {
							this.activitiesTab.set('previewContent', match[2]);
							break;
						}
					}

					if (typeof onSuccess === 'function') {
						onSuccess.call(this);
					}
				} else {
					clientVM.displayError(results.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	activate: function(screen, params) {
		if (params.sir) {
			this.loadTicketInfo(params.sir);
			if (params.activityId !== undefined) {
				this.set('activityId', params.activityId);
				this.set('isActivitiesView', true);
			} else {
				this.set('isActivitiesView', false);
			}
		}
	},

	pSirInvestigationViewerView: true,

	pSirInvestigationViewerDataCompromisedModify: true,
	pSirInvestigationViewerStatusModify: true,
	pSirInvestigationViewerTeamModify: true,

	pSirInvestigationViewerActivitiesCreate: true,
	pSirInvestigationViewerActivitiesModify: true,
	pSirInvestigationViewerActivitiesExecute: true,

	pSirInvestigationViewerArtifactsCreate: true,
	pSirInvestigationViewerArtifactsModify: true,
	pSirInvestigationViewerArtifactsDelete: true,

	pSirInvestigationViewerAttachmentsCreate: true,
	pSirInvestigationViewerAttachmentsModify: true,
	pSirInvestigationViewerAttachmentsDelete: true,

	pSirInvestigationViewerNotesCreate: true,
	pSirInvestigationViewerNotesModify: true,
	pSirInvestigationViewerNotesDelete: true,

	hasSolePermissionToModify: function() {
		// Any one of the following conditions will allow the user to modify the SIR
		// 1. User having admin role
		// 2. User is the owner
		// 3. User is a team member of the SIR
		// 3 have RBAC to modify
		var hasAdminRole = (clientVM.user.roles.indexOf('admin') != -1);
		var isOwner = this.ticketInfo.owner == clientVM.user.name;
		var isTeamMember = (this.ticketInfo.members || []).indexOf(clientVM.user.name) != -1;
		var canModify = hasAdminRole || isOwner || isTeamMember;
		return canModify;
	},

	applyRBAC: function()   {
		var permissions = clientVM.user.permissions || {};

		var canModify = this.hasSolePermissionToModify();

		permissions.sirInvestigationViewer = permissions.sirInvestigationViewer || {};
		this.set('pSirInvestigationViewerView', permissions.sirInvestigationViewer.view === true);

		permissions.sirInvestigationViewerDataCompromised = permissions.sirInvestigationViewerDataCompromised || {};
		this.set('pSirInvestigationViewerDataCompromisedModify', canModify || permissions.sirInvestigationViewerDataCompromised.modify === true);

		permissions.sirInvestigationViewerStatus = permissions.sirInvestigationViewerStatus || {};
		this.set('pSirInvestigationViewerStatusModify', canModify || permissions.sirInvestigationViewerStatus.modify === true);

		permissions.sirInvestigationViewerTeam = permissions.sirInvestigationViewerTeam || {};
		this.set('pSirInvestigationViewerTeamModify', canModify || permissions.sirInvestigationViewerTeam.modify === true);

		permissions.sirInvestigationViewerActivities = permissions.sirInvestigationViewerActivities || {};
		this.set('pSirInvestigationViewerActivitiesCreate', permissions.sirInvestigationViewerActivities.create === true);
		this.set('pSirInvestigationViewerActivitiesModify', canModify || permissions.sirInvestigationViewerActivities.modify === true);
		this.set('pSirInvestigationViewerActivitiesExecute', permissions.sirInvestigationViewerActivities.execute === true);

		permissions.sirInvestigationViewerArtifacts = permissions.sirInvestigationViewerArtifacts || {};
		this.set('pSirInvestigationViewerArtifactsCreate', permissions.sirInvestigationViewerArtifacts.create === true);
		this.set('pSirInvestigationViewerArtifactsModify', canModify || permissions.sirInvestigationViewerArtifacts.modify === true);
		this.set('pSirInvestigationViewerArtifactsDelete', permissions.sirInvestigationViewerArtifacts.delete === true);

		permissions.sirInvestigationViewerAttachments = permissions.sirInvestigationViewerAttachments || {};
		this.set('pSirInvestigationViewerAttachmentsCreate', permissions.sirInvestigationViewerAttachments.create === true);
		this.set('pSirInvestigationViewerAttachmentsModify', canModify || permissions.sirInvestigationViewerAttachments.modify === true);
		this.set('pSirInvestigationViewerAttachmentsDelete', permissions.sirInvestigationViewerAttachments.delete === true);

		permissions.sirInvestigationViewerNotes = permissions.sirInvestigationViewerNotes || {};
		this.set('pSirInvestigationViewerNotesCreate', permissions.sirInvestigationViewerNotes.create === true);
		this.set('pSirInvestigationViewerNotesModify', canModify || permissions.sirInvestigationViewerNotes.modify === true);
		this.set('pSirInvestigationViewerNotesDelete', permissions.sirInvestigationViewerNotes.delete === true);
		this.fireEvent('applyrbac');
	},

	init: function() {
		if (!this.pSirInvestigationViewerView) {
			return;
		}
	},

	initialLoad: true,
	expandDescription: function() {
		var modal = this.open({
			mtype: 'RS.incident.DescriptionModal',
			ticketInfo: this.ticketInfo
		});
		modal.fireEvent('loadDescriptionModal', this.ticketInfo.description, this.ticketInfo.playbook)
	},

	returnTitle$: function() {
		return this.ticketInfo.title;
	},

	returnId$: function() {
		return this.ticketInfo.sir;
	},

	returnDateCreated$: function() {
		if (this.ticketInfo && this.ticketInfo.sysCreatedOn) {
			return Ext.util.Format.date(new Date(this.ticketInfo.sysCreatedOn), clientVM.getUserDefaultDateFormat());
		} else {
			return '';
		}
 	},

	returnSeverity$: function() {
		return this.ticketInfo.severity;
	},
	returnType$: function() {
		return this.ticketInfo.investigationType;
	},

	returnStatus$: function() {
		return this.ticketInfo.status;
	},

	returnSource$: function() {
		return this.ticketInfo.sourceSystem
	},
	returnSourceId$: function() {
		return this.ticketInfo.externalReferenceId
	},

	returnDataCompromised$: function() {
		if (this.ticketInfo.dataCompromised === true) {
			return 'Yes'
		} else if (this.ticketInfo.dataCompromised === false) {
			return 'No'
		} else {
			return this.ticketInfo.dataCompromised
		}
	},

	returnCreatedBy$: function() {
		return this.ticketInfo.sysCreatedBy
	},

	beforeDestroyComponent: function() {
		//Remove this handler from document's event.
		this.fireEvent('removeEventReference');
	}
});

//Parent: RS.incident.Playbook
glu.defModel('RS.incident.DescriptionModal', {
	close: function() {
		this.doClose();
	},

	init: function() {
	}
});
