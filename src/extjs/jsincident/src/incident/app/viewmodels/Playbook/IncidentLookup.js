glu.defModel('RS.incident.IncidentLookup',{
	mixins : ['IncidentLookupUtil']
})
glu.defModel('RS.incident.IncidentLookupPopup',{
	mixins : ['IncidentLookupUtil'],
	noRecordFound : false,
	init : function(){		
		this.incidentStore.on('datachanged', function(){			
			if(this.incidentStore.getCount() == 0)
				this.set('noRecordFound', this.incidentStore.getCount() == 0);
		}, this);
		this.incidentStore.pageSize = 5;
		this.incidentStore.load();
	},	
	noRecordFoundText$ : function(){
		return this.localize('noRecordFound', [Ext.String.htmlEncode(this.artifactName), Ext.String.htmlEncode(this.artifactValue)]);
	},
	expandResultInTab : function(){
		clientVM.handleNavigation({
			modelName: 'RS.incident.IncidentLookup',
			params: {
				artifactName: this.artifactName,
				artifactValue: this.artifactValue,				
			},
			target: '_blank'
		})
	}
})
glu.defModel('RS.incident.IncidentLookupUtil',{
	title : 'Incident Lookup',
	artifactName : '',
	artifactValue : '',
	lookupInfo : '',
	incidentStore: {
		mtype : 'store',
		remoteSort: true,
		pageSize : 50,
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		fields: [
			'id',
			'sir',
			'title',
			'externalReferenceId',
			'investigationType',
			'type',
			'severity',
			'description',
			'sourceSystem',
			'playbook',
			'externalStatus',
			'status',
			'owner',		
			'playbookVersion',
			'sequence',
			'closedOn',
			'closedBy',
			'primary',
			'sysCreatedOn',
			'sirStarted',
			'problemId',
			{name: 'sys_id', mapping: 'id'}
		],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/playbook/artifact/getIncidentsByArtifact',
			reader: {
				type: 'json',
				root: 'records'
			}
		}	
	},
	initMixin : function(){
		this.incidentStore.on('beforeload', function(store, operation){
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				filter: Ext.encode([{
					"field":"name",
					"type":"string",
					"condition":"equals",
					"caseSensitive" : true,
					"value": this.artifactName
				},{
					"field":"value",
					"type":"string",
					"condition":"equals",
					"caseSensitive" : true,
					"value": this.artifactValue
				}])
			});
		},this)
		this.incidentStore.on('datachanged', function(){
			this.set('lookupInfo', this.localize('lookupInfo', [this.incidentStore.getTotalCount(), Ext.String.htmlEncode(this.artifactName), Ext.String.htmlEncode(this.artifactValue)]));
		}, this);
	},
	activate : function(screen, params){
		this.set('artifactName', params['artifactName'] || '');
		this.set('artifactValue', params['artifactValue'] || '');
		this.incidentStore.load();
	}
})