//Parent: RS.incident.Playbook
glu.defModel('RS.incident.Notes', {
	//Load Notes + Store
	notesStore: {
		mtype: 'store',
		//remoteSort: true,
		//pageSize: 25,
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		fields: [
			'postedBy',
			'note',
			{
				name: 'fullNote',
				mapping: 'note',
				convert: function(val) {
					return Ext.String.htmlEncode(val).replace(/(\r\n|\n|\r)/gm, "<br />");
				}
			},
			'activityName',
			'activityId',
			'phase',
			'worksheetId',
			'incidentId',
			'id',
			{name: 'sysCreatedOn', type: 'int', convert: function(field) {return Ext.util.Format.date(new Date(field), clientVM.getUserDefaultDateFormat())}},
			//{name: 'sysUpdatedOn', type: 'int', convert: function(field) {return Ext.util.Format.date(new Date(field), clientVM.getUserDefaultDateFormat())}},
			'sysCreatedBy',
			//'sysUpdatedBy',
		],
		proxy: {
			type: 'ajax',
			method: 'GET',
			//url: '/resolve/service/playbook/note/listBySir',
			url: '/resolve/service/playbook/note/list',
			reader: {
				type: 'json',
				root: 'records'
			}
		},
		listeners: {
			beforeload: function(store, op) {
				op.params = Ext.apply(op.params || {}, {
					//incidentId: this.parentVM.parentVM.ticketInfo.id //change to sir
					filter: '[{"field":"incidentId","type":"string","condition":"equals","value":"' + this.rootVM.ticketInfo.id + '"}]'
				});
			}
		}
	},

	loadNotes: function() {
		this.notesStore.reload({
			scope: this,
			callback: function(recs, ops, success) {
				if (!success) {
					clientVM.displayError(this.localize('notesLoadFail'));
				}
			}
		});
	},

	//Get note info when selected on grid
	selectedNote: '',
	selectedNoteActivityId: null,
	selectNote: function(record) {
		this.set('selectedNote', record.getId());
		this.set('selectedNoteActivityId', record.get('activityId'));
	},

	addNote: function() {
		this.openNoteModal(true);
	},

	editNote: function() {
		if (this.selectedNoteActivityId === null) {
			this.openNoteModal(false, this.selectedNote);
		} else {
			this.rootVM.set('activeTab', this.rootVM.activeTabMap.ACTIVITIES);
			clientVM.handleNavigation({
				modelName: 'RS.incident.Playbook',
				params: {
					sir: this.rootVM.ticketInfo.sir,
					activityId: this.selectedNoteActivityId
				}
			})
		}
	},

	editNoteIsEnabled$: function() {
		return (this.selectedNote !== '')
	},

	editButtonText: '',
	editButtonSize: 0,

	editOrGoTo$: function() {
		if (this.selectedNoteActivityId === null) {
			this.set('editButtonText', this.localize('edit'));
		} else {
			this.set('editButtonText', this.localize('goToActivity'));
		}
	},

	tabInit: function() {
		this.loadNotes();
	},

	init: function() {
	},

	openNoteModal: function(isNew, noteId, activityId, activityName) {
		if (isNew) {
			this.open({
				mtype: 'RS.incident.NoteModal',
				incidentId: this.parentVM.ticketInfo.id,
				loadNotes: this.loadNotes.bind(this),
				isNew: isNew,
				activityId: activityId,
				activityName: activityName
			});
		} else {
			var noteObj = this.notesStore.getById(noteId).getData();
			this.open({
				mtype: 'RS.incident.NoteModal',
				incidentId: this.parentVM.ticketInfo.id,
				loadNotes: this.loadNotes.bind(this),
				isNew: isNew,
				note:  Ext.htmlDecode(noteObj.note),
				id: noteObj.id
			});
		}
	}
});

//Parent: RS.incident.Notes
glu.defModel('RS.incident.NoteModal', {
	isNew: true,
	note: '',
	id: '',
	incidentId: '',
	isSaving: false,

	saveNote: function() {
		this.set('isSaving', true);
		var params = {};
		if (this.isNew) {
			params.note = this.note;
			params.category = ''; //change to phase
			params.incidentId = this.incidentId;
			params.activityId = this.activityId;
			params.activityName = this.activityName;
		} else {
			params.id = this.id;
			params.note = this.note;
			params.incidentId = this.incidentId;
		}
		this.ajax({
			url: '/resolve/service/playbook/note/save',
			method: 'POST',
			jsonData: params,
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					window.setTimeout(this.loadNotes, 1000);
					//Audit Log
					if (!this.isNew) {
						clientVM.displaySuccess(this.localize('noteEditSuccess'));
					} else {
						clientVM.displaySuccess(this.localize('noteAddSuccess'));
					}
					this.close();
				} else {
					var message = (this.isNew) ? this.localize('noteAddFail') : this.localize('noteEditFail');
					clientVM.displayError(message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	saveNoteIsEnabled$: function() {
		return this.note.length !== 0
	},

	returnTitle$: function() {

		if (this.isNew) {
			return (this.activityId) ? this.localize('addNoteTitle', [this.activityName]) : this.localize('addNote');
		} else {
			return this.localize('editNote');
		}
	},

	saveButtonText: '',

	editOrAdd$: function() {
		if (this.isNew) {
			this.set('saveButtonText', this.localize('add'));
		} else {
			this.set('saveButtonText', this.localize('edit'));
		}
	},

	cancel: function() {
		this.doClose();
	},

	init: function() {
	}
});