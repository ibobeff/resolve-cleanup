glu.defModel('RS.incident.AutomationResults',{
	activityFilter : 'All',
	activityId: null,
	resultList : {
		mtype : 'list'
	},
	fields : ['title', 'order','refreshInterval','refreshCountMax','filter','showWiki','descriptionWidth',{   
    	name : 'autoHide',
    	type : 'boolean'
    },{   
    	name : 'autoCollapse',
    	type : 'boolean'
    },{   
    	name : 'encodeSummary',
    	type : 'boolean'
    },{   
    	name : 'progress',
    	type : 'boolean'
    },{   
    	name : 'selectAll',
    	type : 'boolean'
    },{   
    	name : 'includeStartEnd',
    	type : 'boolean'
    },{   
    	name : 'preserveTaskOrder',
    	type : 'boolean'
    },{
    	name : 'actionTasks',
    	type : 'raw'
    }],
    when_activity_filter_changed : {
    	on : ['activityFilterChanged'],
    	action : function(newVal){
			this.refreshAutomationResults();
    	}
    },
	init: function() {
		if (this.modal) {
			this.updateInfo(this.rootVM.resultsInfo);
		}
	},
	updateInfo : function(info){
		var resultMacro = {
			mtype : "RS.wiki.macros.Results2",
			sirProblemId: info['sirProblemId'],
			activityId: null,
			isModal: this.isModal,
			showHeader : false
		}

		//Convert string to correct type
		Ext.apply(resultMacro , this.asObject(this.loadData(info['automationFilter'])));
		this.resultList.removeAll();
		this.resultList.add(this.model(resultMacro));

		this.updateActivityMap();
		this.refreshAutomationResults();
	},
	getResultMacro : function(){
		return this.resultList.getAt(0);
	},
	getCurrentData : function(){
		return this.getResultMacro().getCurrentData();
	},
	refresh : function(){
		var macro = this.getResultMacro();
		if(macro) {
			macro.refresh(true);
		}
	},
	activityIdMap: {},
	tabInit: function() {
		this.parentVM.loadActivities(function() {
			this.updateActivityMap();
			this.refreshAutomationResults();
		}.bind(this));
	},
	updateActivityMap : function(){
		this.activityIdMap = {};
		var activities = this.parentVM.getActivityList();
		activities.filter(function(activity) {			
			this.activityIdMap[activity.id] = activity.activityName;
		}.bind(this));
	},
	refreshAutomationResults: function() {
		var macro = this.getResultMacro();
		if(macro){
			var activityId = null;
			var wikiName = this.rootVM.activitiesTab.activitiesStore.getRange().filter(function(activity) {
				return activity.get('activityName') === this.activityFilter
			}.bind(this)).map(function(activity) {
				activityId = activity.get('id')
				return activity.get('wikiName')
			})[0];
			macro.set('showWiki', this.activityFilter == 'All' ? '' : wikiName);

			if (this.activityId) {
				macro.set('activityId', this.activityId);
			} else if (activityId) {
				macro.set('activityId', activityId);
			} else {
				macro.set('activityId', null);
			}
			macro.refresh(true);
		}
	},

	modal: false,
	padding: 0,
	close: function() {
		this.doClose()
	},
	isModal$: function(){
		return this.modal;
	},
})