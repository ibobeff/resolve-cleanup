glu.defModel('RS.incident.UploadAttachment', {
	mixins: ['FileUploadManager'],
	api: {
		upload: '/resolve/service/playbook/attachment/upload',
		list: '/resolve/service/playbook/attachment/listBySir?incidentId='+'will_change' //Shim wiki upload manager but also send incidentId because attachment/listBySir needs it
	},
	autoUpload: false,
	deleteFileAfterUpload: true,

	fineUploaderTemplateHidden: false,
	fineUploaderTemplateHeight: 245,

	uploaderAdditionalClass: 'incident',

	fileOnSubmittedCallback: function(manager) {
		if (manager.getUploads({status: [qq.status.SUBMITTED, qq.status.QUEUED]}).length) {
			this.view.fireEvent('enableSubmitBtn');
		}
	},
	fileOnCancelCallback: function(manager) {
		setTimeout(function(){
			if (manager.getUploads({status: [qq.status.SUBMITTED, qq.status.QUEUED]}).length == 0) {
				this.view.fireEvent('disableSubmitBtn');
			}
		}.bind(this), 100)
	},

	filesOnAllCompleteCallback: function(manager) {
		this.parentVM.fireEvent('reloadAttachments');

		if (manager.getUploads({status: [qq.status.UPLOAD_FAILED, qq.status.REJECTED, qq.status.PAUSED] }).length == 0) {
			this.close();
		} else {
			setTimeout(function(){
				if (manager.getUploads({status: [qq.status.SUBMITTED, qq.status.QUEUED]}).length == 0) {
					this.view.fireEvent('disableSubmitBtn');
				}
			}.bind(this), 100);
		}
	},

	//fileOnErrorCallback: function(manager, filename, errorReason, xhr) {
	//	clientVM.displayError(filename+' : '+errorReason);
	//},

	malicious: false,
	maliciousChanged: function(malicious) {
		this.set('malicious', malicious);
	},

	description: '',
	descriptionChanged: function(description) {
		this.set('description', description || '');
	},

	init: function() {
		this.set('title', this.localize('addAttachment'));
		this.set('dragHereText', this.localize('dragHere'));
	},

	submit: function() {
		this.uploader.setParams({
			docFullName: this.incidentId,
			malicious: this.malicious,
			description: this.description
		});
		this.uploader.uploadStoredFiles();
	},

	view: null,
	viewRenderCompleted: function(view) {
		this.set('view', view);
	},

	close: function() {
		this.parentVM.reload();
		this.doClose();
	}
});

glu.defModel('RS.incident.FileUploadManagerFactory', function(cfg) {
	Ext.applyIf(cfg, RS.common.viewmodels.FileUploadManager);
	return cfg;
});

/*
glu.defModel('RS.incident.UploadAttachment', {
    runtime: '',
	incidentId: '',
	api: {
		upload: '/resolve/service/playbook/attachment/upload',
	},

    init: function() {
        this.clearForm();
    },

    browse: function() {
		//this.view.fireEvent('clearQueue');
	},
    
    close: function() {
        this.set('wait', false);
        this.doClose();
    },

	wait: false,
	uploading: false,
	filenames: '',
	description: '',
	malicious: false,
	percent: 0,
	filesize: 0,
	currentFile: '',
	isCancel: false,
	uploadlog: '',

	clearForm: function() {
		this.set('wait', false);
		this.set('uploading', false);
		this.set('filenames', '');
		this.set('description', '');
		this.set('malicious', false);
		this.set('percent', 0);
		this.set('filesize', 0);
		this.set('currentFile', '');
		this.set('isCancel', false);
		this.set('uploadlog', '');
	},

	busyText$: function() {
		return this.uploadlog + this.currentFile + ' ('+Ext.util.Format.fileSize(this.filesize)+') : ' + this.percent;
	},

    browseIsEnabled$: function() {
       return !this.wait;
    },

    uploadError: function(uploader, data) {
        clientVM.displayError(this.localize('UploadError') + '[' + data.file.msg + ']');
    },

	beforeStart: function(uploader, data) {
        this.set('wait', true);
        this.set('uploading', true);
	},

    filesAdded: function(uploader, data) {
        var filenames = '';
		data.forEach(function(file) {
			if (filenames) {
				filenames += ', '+file.name;
			} else {
				filenames = file.name;
			}
		});

		uploader.uploader.files.forEach(function(file) {
			if (filenames) {
				filenames += ', '+file.name;
			} else {
				filenames = file.name;
			}
		});

		this.set('filenames', filenames);
    },

	uploadProgress: function(filename, size, percent) {
		if (this.currentFile && this.currentFile != filename) {
			this.uploadlog += this.currentFile + ': Done<br>';
		}
		this.set('percent', percent + '%');
		this.currentFile = filename;
		this.filesize = size;
	},

    uploadComplete: function(uploader, data) {
		if (this.isCancel) {
			clientVM.displaySuccess(this.localize('attachmentsUploadCancelled'));
		} else {
			clientVM.displaySuccess(this.localize('attachmentsUploadSuccess'));
		}

		//uploader.clearQueue();
		//this.clearForm();
		this.close();

		this.parentVM.fireEvent('reloadAttachments');
    },
    runtimeDecided: function(runtime) {
        if (!runtime) {
            clientVM.displayError('no uploader runtime!!')
            return;
        }
        this.set('runtime', runtime.type);
    },
	submitIsEnabled$: function() {
        return this.filenames;
    },
	submit: function() {
		this.view.fireEvent('startUpload', this.malicious, this.description);
    },

	cancel: function() {
		this.set('isCancel', true);
		this.view.fireEvent('cancelUpload');
    },

	view: null,
	viewRenderCompleted: function(view) {
		this.set('view', view);
	}
})
*/
