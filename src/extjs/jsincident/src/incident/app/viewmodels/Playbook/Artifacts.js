//Parent: RS.incident.Playbook
glu.defModel('RS.incident.Artifacts', {
	API : {
		removeArtifact : '/resolve/service/playbook/artifact/delete'
	},
	artifactsSelections: [],	
	csrftoken: '',
	init: function() {
		this.initToken();
	},
	activate: function() {
		this.initToken();
	},
	initToken: function() {
		this.set('csrftoken', clientVM.rsclientToken);
	},
	artifactsStore: {
		mtype: 'store',		
		sorters: [{
			property: 'name',
			direction: 'ASC'
		}],
		fields: [
			'id',
			'name',
			{
				name : 'encodedValue',
				convert: function(v, record){
					return Ext.String.htmlEncode(record.get('value'));
				}
			},{
				name : 'encodedDescription',
				convert: function(v, record){
					return Ext.String.htmlEncode(record.get('description'));
				}
			},
			'value',
			'description',
			'worksheetId',
			'incidentId',
			{name: 'sysCreatedOn', type: 'int', convert: function(field) {return Ext.util.Format.date(new Date(field), clientVM.getUserDefaultDateFormat())}},
			{name: 'sysUpdatedOn', type: 'int', convert: function(field) {return Ext.util.Format.date(new Date(field), clientVM.getUserDefaultDateFormat())}},
			'sysCreatedBy',
			'sysUpdatedBy',{
				name : 'sirList',
				convert: function(val, record){
					var result = '';
					for(var i = 0; i < val.length; i++){
						result += '<a target=_blank href="/resolve/jsp/rsclient.jsp?=' + this.csrftoken + '#RS.incident.Playbook/sir=' + val[i] + '">' + val[i] + '</a>&nbsp;';
					}
					return result ? result : 'None';
				}
			}
		],
		proxy: {
			type: 'ajax',
			method: 'GET',
			url: '/resolve/service/playbook/artifact/listBySir',
			reader: {
				type: 'json',
				root: 'records'
			}
		},
		listeners: {
			beforeload: function(store, op) {
				op.params = Ext.apply(op.params || {}, {
					sir: this.rootVM.ticketInfo.sir
				});
			}
		}
	},	
	tabInit: function() {
		this.loadArtifacts();
	},
	loadArtifacts: function() {
		this.artifactsStore.load();
	},
	addArtifact: function() {
		this.open({
			mtype: 'RS.incident.ArtifactModal',
			sir: this.parentVM.ticketInfo.sir,
			dumper : {
				dump : function(artifact){
					this.artifactsStore.add(artifact);
				},
				scope : this
			}		
		});
	},
	removeArtifact: function() {
		this.message({
			title: this.localize('removeArtifactTitle'),
			msg: this.localize('removeArtifactBody'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirm'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.reallyRemoveArtifact()
			}
		});
	},
	reallyRemoveArtifact: function() {
		var artifacts = [];
		this.artifactsSelections.forEach(function(r) {
			var d = r.getData();
			artifacts.push({
				id: d.id,
				name: d.name,
				value: d.value
			});
		}, this);
		this.ajax({
			url: this.API['removeArtifact'],
			method: 'POST',		
			jsonData: {
				artifacts: artifacts
			},
			params: {incidentId: this.parentVM.ticketInfo.id},
			success: function(resp) {
				var response = RS.common.parsePayload(resp);
				if (response.success) {
					clientVM.displaySuccess(this.localize('artifactDeleteSuccess'));
					this.artifactsStore.remove(this.artifactsSelections);
					this.close();
				} else {
					clientVM.displayError(this.localize('artifactDeleteFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},
	removeArtifactIsEnabled$: function() {
		return (this.artifactsSelections.length > 0);
	},
	lookupIncidentByArtifact : function(record){
		this.open({
			mtype : 'IncidentLookupPopup',
			artifactName : record.get('name'),
			artifactValue : record.get('value')
		})
	},
	actionHandler : function(record){
		this.open({
			mtype : 'ArtifactAction',
			key : record.get('name'),
			value : record.get('value'),
			sirProblemId : this.rootVM.ticketInfo.problemId
		})
	}
});

//Artifact Modal
glu.defModel('RS.incident.ArtifactModal', {
	API : {
		saveArtifact : '/resolve/service/playbook/artifact/save',
		getAlias : '/resolve/service/at',
		getCEF : '/resolve/service/cef'
	},
	fields : ['ushortName','value','description'],
	type: '',	
	isSaving: false,
	artifactTypeStore : {
		mtype : 'store',
		fields : ['uname']
	},	
	aliasStore : {
		mtype : 'store',
		fields : ['ushortName']
	},
	availableAlias : [],
	when_type_changed : {
		on : ['typeChanged'],
		action : function(v){
			var selectedAlias = null;
			this.availableAlias.forEach(function(entry){
				if(entry.uname == v){
					selectedAlias = entry;
					return false;
				}
			})
			this.aliasStore.loadData(selectedAlias ? selectedAlias.dictionaryItems : []);
			var firstAlias = this.aliasStore.getAt(0);
			this.set('ushortName', firstAlias ? firstAlias.get('ushortName') : '');
		}
	},
	ushortNameIsValid$ : function(){
		return this.ushortName && this.isKeyValid(this.ushortName) ? true : this.localize('keyReqs');
	},
	valueIsValid$ : function(){
		return this.value ? true : this.localize('valueReqs');
	},
	isKeyValid : function(key){
		return this.aliasStore.find('ushortName', key, 0, false, false, true) != -1;
	},
	init: function() {
		this.ajax({
			url : this.API['getAlias'],
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success){
					var assignedTypeAlias = [];
					for(var i = 0; i < response.data.length;i++){
						var entry = response.data[i];
						this.availableAlias.push(entry);
						assignedTypeAlias = assignedTypeAlias.concat(entry.dictionaryItems);						
					}
					this.ajax({
						url : this.API['getCEF'],
						success : function(resp){
							var response = RS.common.parsePayload(resp);
							if(response.success){
								var unknownType = {
									uname : 'UNKNOWN',
									dictionaryItems : []
								}
								this.availableAlias.push(unknownType);
								var allCEFs = response.data || [];
								//Eliminate all alias that already assigned to a group. Might be slow if cef expands in future.
								allCEFs.forEach(function(cef){
									var found = false;
									assignedTypeAlias.forEach(function(assignedCEF){
										if(assignedCEF.ushortName == cef.ushortName){
											found = true;
											return false
										}									
									})
									if(!found)
										unknownType['dictionaryItems'].push(cef);								
								})
								this.artifactTypeStore.loadData(this.availableAlias);
								var firstType = this.artifactTypeStore.getAt(0);
								this.set('type', firstType ? firstType.get('uname') : '');
							}
							else
								clientVM.displayError(response.message);
						},
						failure: function(resp) {
							clientVM.displayFailure(resp);
						}
					})
				}
				else
					clientVM.displayError(response.message);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})	
	},
	saveArtifact: function() {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		this.set('isSaving', true);
		this.ajax({
			url: this.API['saveArtifact'],
			method: 'POST',
			jsonData: {
				sir: this.sir,
				name: this.ushortName,
				value: this.value,
				description: this.description,			
			},
			success: function(resp) {
				var response = RS.common.parsePayload(resp);
				if (response.success) {
					clientVM.displaySuccess(this.localize('artifactAddSuccess'));
					if(this.dumper && Ext.isFunction(this.dumper.dump)){
						this.dumper.dump.apply(this.dumper.scope, [response.records[0]]);
					}
					this.close();
				}
				else
					clientVM.displayError(this.localize('artifactAddFail'));
				
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	saveArtifactIsEnabled$: function() {
		return ((this.type.length !== 0) && (this.value.length !== 0))
	},	
	cancel: function() {
		this.doClose();
	}	
});