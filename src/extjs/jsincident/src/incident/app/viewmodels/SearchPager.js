Ext.define('RS.common.plugin.SearchPager', {
	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.searchpager',
	mixins: {
		bindable: 'Ext.util.Bindable'
	},

	//Pagination Config
	pageable: true,
	allowAutoRefresh: true,
	showSysInfo: true,
	displayJumpMenu: true,
	autoRefreshInterval: 30,
	autoRefreshEnabled: false,
	showSocial: false,
	socialStreamType: '',

	//Button configuration
	buttons: [],
	init: function(grid) {
		var me = this,
			myGrid = grid;

		me.grid = myGrid;

		if(this.clobCheckURL != ""){
			Ext.Ajax.request({
				url : this.clobCheckURL,
				success : function(resp){
					var response = RS.common.parsePayload(resp);
					me.clobTypeFields = me.convertColumnToField(response.data);
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
		if (!me.grid.isXType('grid')) {
			me.originalGrid = me.grid
			me.grid = me.grid.down(this.gridName ? '#' + this.gridName : 'grid')
		}
		me.gridColumns = [];

		me.conditions = {
			on: RS.common.locale.on,
			after: RS.common.locale.after,
			before: RS.common.locale.before,
			equals: RS.common.locale.equals,
			notEquals: RS.common.locale.notEquals,
			contains: RS.common.locale.contains,
			notContains: RS.common.locale.notContains,
			startsWith: RS.common.locale.startsWith,
			notStartsWith: RS.common.locale.notStartsWith,
			endsWith: RS.common.locale.endsWith,
			notEndsWith: RS.common.locale.notEndsWith,
			greaterThan: RS.common.locale.greaterThan,
			greaterThanOrEqualTo: RS.common.locale.greaterThanOrEqualTo,
			lessThan: RS.common.locale.lessThan,
			lessThanOrEqualTo: RS.common.locale.lessThanOrEqualTo,
			shortEquals: RS.common.locale.shortEquals,
			shortNotEquals: RS.common.locale.shortNotEquals,
			shortGreaterThan: RS.common.locale.shortGreaterThan,
			shortGreaterThanOrEqualTo: RS.common.locale.shortGreaterThanOrEqualTo,
			shortLessThan: RS.common.locale.shortLessThan,
			shortLessThanOrEqualTo: RS.common.locale.shortLessThanOrEqualTo
		};

		me.shortConditions = {};
		me.shortConditions[RS.common.locale.shortEquals] = 'equals';
		me.shortConditions[RS.common.locale.shortNotEquals] = 'notEquals';
		me.shortConditions[RS.common.locale.shortGreaterThan] = 'greaterThan';
		me.shortConditions[RS.common.locale.shortGreaterThanOrEqualTo] = 'greaterThanOrEqualTo';
		me.shortConditions[RS.common.locale.shortLessThan] = 'lessThan';
		me.shortConditions[RS.common.locale.shortLessThanOrEqualTo] = 'lessThanOrEqualTo';

		me.dataValues = {
			today: RS.common.locale.today,
			yesterday: RS.common.locale.yesterday,
			lastWeek: RS.common.locale.lastWeek,
			lastMonth: RS.common.locale.lastMonth,
			lastYear: RS.common.locale.lastYear
		};

		me.grid.store.on('beforeLoad', function(store, operation, eOpts) {
			var whereClause = this.getWhereClause();
			var	whereFilter = this.getWhereFilter();
			operation.params = operation.params || {};

			//merge any existing filters
			if (operation.params.filter) {
				var existingFilter = Ext.decode(operation.params.filter)
				whereFilter = whereFilter.concat(existingFilter)
			}

			Ext.apply(operation.params, {
				filter: Ext.encode(whereFilter)
			});
			store.lastWhereClause = whereClause;
		}, me);


		if (me.grid.store.parentVM && !me.grid.store.parentVM.activate)
			me.grid.store.parentVM.activate = Ext.emptyFn

		if (me.grid.store.parentVM)
			me.grid.store.parentVM.activate = Ext.Function.createInterceptor(me.grid.store.parentVM.activate, function() {
				me.parseWindowParameters();
			})

		me.grid.on('celldblclick', function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
			var column = grid.getHeaderCt().getHeaderAtIndex(cellIndex);
			var	dataIndex = column.dataIndex;
			var	cellValue = record.get(dataIndex);
			var operator = RS.common.locale.equals;
			Ext.each(me.clobTypeFields, function(clobField){
				if(column.dataIndex == clobField){
					operator = RS.common.locale.contains;
					return;
				}
			})

			if (!column.filterable) return;
			if (column.computeFilter) {
				me.addFilter(column.computeFilter(cellValue));
				return;
			}
			if (Ext.isDate(cellValue)) {
				//cellValue = Ext.Date.format(cellValue, me.dateFormat);
				cellValue = Ext.Date.format(cellValue, me.submitFormat);
				operator = RS.common.locale.on;
			}

			if (Ext.isBoolean(cellValue)) cellValue = cellValue.toString();

			if (Ext.isNumber(cellValue)) cellValue = cellValue.toString();

			if (cellValue.indexOf('|&|') > -1) {
				cellValue = cellValue.replace(/\|&\|/g, '|');
				operator = RS.common.locale.contains;
			}
			if (dataIndex) me.addFilter(column.text + ' ' + operator + ' ' + cellValue); //Check data index for action columns or columns that aren't a part of the data
		}, me);

		me.grid.on('reconfigure', function() {
			Ext.defer(function() {
				me.parseWindowParameters()
				me.grid.store.load()
			}, 1, me)
		})

		var fields = me.grid.store.proxy.reader.model.getFields();
		Ext.each(me.grid.columns, function(column) {
			var col = column;
			Ext.each(fields, function(field) {
				if (field.name == col.dataIndex) {
					col.dataType = field.type.type;
					return false;
				}
			});
			me.gridColumns.push(col);
		});

		me.searchStore = Ext.create('Ext.data.Store', {
			fields: ['name', 'value', 'type', 'displayName'],
			proxy: {
				type: 'memory',
				reader: {
					type: 'json',
					root: 'records'
				}
			},
			listeners: {
				load: function(store, records, successful, eOpts) {
					var combo = me.toolbar.down('#searchText'),
						value = combo.nextVal || combo.getValue();
					combo.nextVal = null;
					me.parseSuggestionsForStore(store, value);
				}
			}
		});

		me.filterStore = Ext.create('Ext.data.Store', {
			model: 'RS.common.model.Filter',
			proxy: {
				type: 'ajax',
				url: '/resolve/service/filter/list',
				reader: {
					type: 'json',
					root: 'records'
				}
			}
		});

		me.filterStore.on('load', function() {
			me.filterStore.add({
				name: RS.common.locale.manageFilters,
				value: 'managefilters',
				sysId: 'managefilters'
			})
		})

		me.filterStore.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}

			Ext.apply(operation.params, {
				table: me.grid.tableName,
				view: me.grid.viewName
			})
		})

		if (me.allowPersistFilter) me.filterStore.load();

		me.viewStore = Ext.create('Ext.data.Store', {
			model: 'RS.common.model.View',
			proxy: {
				type: 'ajax',
				url: '/resolve/service/view/list',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			},
			listeners: {
				scope: me,
				load: function(store, records, successful, eOpts) {
					this.configureViewMenuItems()
				}
			}
		});

		me.viewStore.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}

			Ext.apply(operation.params, {
				table: me.grid.tableName
			})
		})

		me.grid.on('serverDataChanged', function() {
			this.configureViewMenuItems()
		}, me)

		if (me.grid.tableName && !me.hideMenu) me.viewStore.load();

		var displayTableMenuItem = {
			cls: 'rs-display-name',
			text: me.displayName || me.grid.displayName,
			itemId: 'tableMenu'
		};

		if (me.hideMenu) {
			displayTableMenuItem.xtype = 'tbtext';
		} else {
			displayTableMenuItem.listeners = {
				render: function(button) {
					Ext.fly(button.getEl().query('span.x-btn-inner')[0]).setStyle({
						'font-size': '16px',
						'font-weight': 'bold'
					})
				}
			}
			displayTableMenuItem.menu = [{
					text: RS.common.locale.views,
					itemId: 'viewMenuItem',
					menu: {
						items: []
					}
				},
				{
					text: RS.common.locale.settings,
					handler: function(button) {
						clientVM.handleNavigation({
							modelName: 'RS.customtable.TableDefinition',
							params: {
								id: me.grid.tableSysId
							}
						})
					}
				}, {
					text: RS.common.locale.saveViewAs,
					scope: me,
					handler: me.saveView
				}
			]
		}

		//PAGINATION
		if (me.grid.hidePager === true) {
			return
		}

		this.autoRefreshInterval = Ext.state.Manager.get('autoRefreshInterval', 30)
		this.autoRefreshEnabled = Ext.state.Manager.get('autoRefreshEnabled', false)

		me.displayMsg = RS.common.locale.pageDisplayText
		me.firstText = RS.common.locale.firstText
		me.prevText = RS.common.locale.prevText
		me.nextText = RS.common.locale.nextText
		me.lastText = RS.common.locale.lastText
		me.refreshText = RS.common.locale.refreshText
		me.configureText = RS.common.locale.configureText
		me.emptyMsg = RS.common.locale.emptyMsg
		me.systemInformation = RS.common.locale.sysInfoButtonTooltip
		me.systemInformationTooltip = RS.common.locale.sysInfoButtonTitle
		me.goToText = RS.common.locale.goToText

		//Auto refresh
		me.autoRefreshText = RS.common.locale.autoRefreshText;

		myGrid.dockedItems.each(function(dockedItem) {
			if (dockedItem.name == 'actionBar') {
				me.toolbar = dockedItem
				return false
			}
		})

		if (!myGrid.isXType('grid')) myGrid = myGrid.down('grid')

		if (me.pageable) myGrid.store.pageSize = myGrid.store.pageSize || 50
		else myGrid.store.pageSize = -1

		//Add Layout for plugins
		var toolbarItems = this.buttons.concat([{
			xtype : 'tbtext',
			text: 'Filter By:',
			itemId: 'tableMenu',
			style : {
				fontWeight : 'bold'
			}
		},
		/*displayTableMenuItem, ' ', ' ', ' ', ' ', ' ', ' ', ' ', {
			xtype: 'combobox',
			itemId: 'filter',
			hidden: !me.allowPersistFilter,
			width: 200,
			emptyText: RS.common.locale.SelectFilter,
			store: me.filterStore,
			queryMode: 'local',
			displayField: 'name',
			valueField: 'sysId',
			listeners: {
				scope: me,
				change: function(combo, newValue, oldValue, eOpts) {
					if (newValue) {
						if (newValue == 'managefilters') {
							combo.setValue(oldValue)
							var w = Ext.create('Ext.window.Window', {
								width: 400,
								height: 300,
								layout: 'fit',
								title: RS.common.locale.manageFilters,
								items: [{
									xtype: 'grid',
									tbar: [{
										text: RS.common.locale.deleteText,
										itemId: 'deleteFilter',
										disabled: true,
										scope: this,
										handler: function(btn) {
											var selections = btn.up('window').down('grid').getSelectionModel().getSelection(),
												ids = [];
											Ext.Array.forEach(selections, function(selection) {
												ids.push(selection.get('sysId'))
											})
											Ext.Ajax.request({
												url: '/resolve/service/filter/delete',
												params: {
													sysIds: ids.join(',')
												},
												scope: this,
												success: function(r) {
													var response = RS.common.parsePayload(r)
													if (response.success) {
														clientVM.displaySuccess(ids.length === 1 ? RS.common.locale.filterDeleted : RS.common.locale.filtersDeleted)
														this.filterStore.load()
														btn.up('window').close()
													} else clientVM.displayError(response.message)
												},
												failure: function(r) {
													clientVM.displayFailure(r)
												}
											})
										}
									}],
									selMode: 'MULTI',
									columns: [{
										text: RS.common.locale.name,
										dataIndex: 'name',
										flex: 1
									}],
									store: Ext.create('Ext.data.Store', {
										autoLoad: true,
										model: 'RS.common.model.Filter',
										proxy: {
											type: 'ajax',
											url: '/resolve/service/filter/list',
											extraParams: {
												table: me.grid.tableName,
												view: me.grid.viewName
											},
											reader: {
												type: 'json',
												root: 'records'
											},
											listeners: {
												exception: function(e, resp, op) {
													clientVM.displayExceptionError(e, resp, op);
												}
											}
										}
									}),
									listeners: {
										selectionchange: function(grid, selections) {
											w.down('#deleteFilter')[selections.length > 0 ? 'enable' : 'disable']()
										}
									}
								}],
								buttonAlign: 'left',
								buttons: [{
									xtype: 'button',
									text: RS.common.locale.close,
									handler: function(btn) {
										btn.up('window').close()
									}
								}]
							}).show()
						} else
							this.setFilter(combo.store.getById(newValue).data.value);
					}
				}
			}
		}, */
		{
			xtype: 'combobox',
			itemId: 'searchText',
			width: 520,
			emptyText: RS.common.locale.Filter + RS.common.locale.egText,
			trigger2Cls: 'x-form-search-trigger',
			margin: '0 0 0 20',
			enableKeyEvents: true,
			// autoSelect: false,
			queryMode: 'remote',
			typeAhead: false,
			minChars: 1,
			store: me.searchStore,
			displayField: 'name',
			valueField: 'value',
			queryDelay: 10,
			listConfig: {
				emptyText: RS.common.locale.noSuggestions
			},
			onTriggerClick: function() {
				if (!this.advanced) {
					var pos = this.getPosition(),
						items = [];
					Ext.each(me.grid.columns, function(column) {
						var isClobField = false;
						Ext.each(me.clobTypeFields, function(clobField){
							if(column.dataIndex == clobField){
								isClobField = true;
								return;
							}
						})
						var columnXtype = '',
							comparisons = me.getShortOptionsForDataType(column.dataType, isClobField);

						switch (column.dataType) {
							case 'date':
								switch (column.uiType) {
									case 'Date':
										columnXtype = 'xdatefield';
										break;
									case 'Timestamp':
										columnXtype = 'xtimefield';
										break;
									default:
										columnXtype = 'xdatetimefield';
										break;
								}
								break;
							case 'float':
							case 'int':
								columnXtype = 'numberfield';
								break;
							case 'bool':
								columnXtype = 'booleancombobox';
								break;
							default:
								columnXtype = 'textfield';
								break;
						}

						if (column.filterable) {
							items.push({
								fieldLabel: column.filterLabel || column.tooltip || column.text,
								labelAlign: 'top',
								name: column.dataIndex + '_type',
								xtype: 'fieldcontainer',
								layout: 'hbox',
								items: [{
									xtype: 'combobox',
									name: column.dataIndex + '_comparison',
									width: 130,
									displayField: 'name',
									valueField: 'value',
									padding: '0 5 0 0',
									hidden: !me.showComparisons,
									store: Ext.create('Ext.data.Store', {
										fields: ['name', 'value'],
										data: comparisons
									}),
									value: column.dataType == 'date' ? 'on' : comparisons[0].name
								}, {
									name: column.dataIndex,
									xtype: columnXtype,
									validator: column.validator,
									format: columnXtype == 'xtimefield' ? me.timeFormat : me.dateFormat,
									submitFormat: 'Y-m-d H:i:sO',
									flex: 1,
									listeners: {
										validitychange: function(btn, isValid) {
											this.up().up().up().down('button[name="searchBtn"]')[isValid ? 'enable' : 'disable']();
										}
									}
								}]

							});
						}
					}, this);

					this.advanced = Ext.create('Ext.Window', {
						closeAction: 'hide',
						modal: true,
						cls : 'rs-modal-popup',
						title: 'Advanced Search',					
						width: 800,
						height : 600,
						layout: 'fit',
						padding : 15,					
						items: {
							xtype: 'form',
							autoScroll: true,												
							items: items,							
							buttons: [{
								text: RS.common.locale.search,
								name: 'searchBtn',
								cls : 'rs-med-btn rs-btn-dark',
								handler: function(button) {
									var form = button.up('form').getForm(),
										values = form.getValues();
									Ext.each(me.grid.columns, function(column) {
										var value = values[column.dataIndex],
											comp = values[column.dataIndex + '_comparison'];
										if (value) me.addFilter(Ext.String.format('{0} {1} {2}', column.text, comp, value));
									});
									form.reset();
									button.up('window').close();
								}
							}, {
								text: RS.common.locale.cancel,
								cls : 'rs-med-btn rs-btn-light',
								handler: function(button) {
									button.up('form').getForm().reset()
									button.up('window').close()
								}
							}]
						}
					});
				}			
				this.advanced.show();
			},
			onTrigger2Click: function() {
				if (!this.getValue()) return;
				if (me.addFilter(this.getValue())) this.reset();
			},
			listeners: {
				specialkey: function(field, e) {
					if (e.getKey() == e.ENTER) {
						Ext.defer(function() {
							field.onTrigger2Click();
						}, 10);
					}

					if (e.getKey() == e.TAB) {
						e.preventDefault();
					}
				},
				beforeSelect: function(combo, record, index, eOpts) {
					record.data.name = record.data.displayName;
					record.data.value = record.data.displayName;
					combo.nextVal = record.data.name;
					if (record.data.name.lastIndexOf(' ') == record.data.name.length - 1) {
						combo.store.load({
							params: {
								query: record.data.displayName
							}
						});
						Ext.defer(function() {
							combo.expand();
						}, 10);
					}
				},
				render: function(combo) {
					combo.setVisible(me.displayFilter)
				}
			}
		}]);

		if (me.pageable){
			toolbarItems = toolbarItems.concat([,'->',{
			xtype: 'tbtext',
			itemId: 'displayItem',
			listeners: {
				render: function(tbtext) {
					if (me.displayJumpMenu) {
						tbtext.getEl().on('click', function() {
							if (!tbtext.menu) {
								tbtext.menu = Ext.create('Ext.menu.Menu', {
									items: [{
										xtype: 'numberfield',
										itemId: 'pageNumberField',
										fieldLabel: me.goToText,
										labelWidth: 45,
										width: 120,
										minValue: 1,
										listeners: {
											change: function(field, newValue) {
												if (Ext.isNumber(newValue) && newValue > 0 && me.store.currentPage != newValue && newValue <= me.getPageData().pageCount)
													me.store.loadPage(newValue)
											}
										}
									}]
								})
							}
							tbtext.menu.down('#pageNumberField').setValue(me.store.currentPage)
							tbtext.menu.down('#pageNumberField').setMaxValue(me.getPageData().pageCount)
							tbtext.menu.showBy(tbtext)
						})
					}
				}
			}
				}, {
					itemId: 'first',
					tooltip: me.firstText,
					overflowText: me.firstText,
					iconCls: Ext.baseCSSPrefix + 'tbar-page-first',
					disabled: true,
					handler: me.moveFirst,
					scope: me
				}, {
					itemId: 'prev',
					tooltip: me.prevText,
					overflowText: me.prevText,
					iconCls: Ext.baseCSSPrefix + 'tbar-page-prev',
					disabled: true,
					handler: me.movePrevious,
					scope: me
				}, {
					itemId: 'next',
					tooltip: me.nextText,
					overflowText: me.nextText,
					iconCls: Ext.baseCSSPrefix + 'tbar-page-next',
					disabled: true,
					handler: me.moveNext,
					scope: me
				}, {
					itemId: 'last',
					tooltip: me.lastText,
					overflowText: me.lastText,
					iconCls: Ext.baseCSSPrefix + 'tbar-page-last',
					disabled: true,
					handler: me.moveLast,
					scope: me
				}, ' ', ' '
			])
		}

		if (me.showSysInfo) {
			var showSysInfoButton = {
				xtype: 'button',
				ui: 'system-info-button-small',
				iconCls: 'rs-icon-button icon-info-sign' + (Ext.isGecko ? ' rs-icon-firefox' : ''),
				tooltip: me.systemInformation,
				overflowText: me.systemInformationTooltip,
				enableToggle: true,
				pressed: Ext.state.Manager.get('userShowSysInfoColumns', false),
				handler: me.toggleSysColumns,
				scope: myGrid
			}
			toolbarItems = toolbarItems.concat(showSysInfoButton);
			me.toggleSysColumns.apply(myGrid, [showSysInfoButton])
		}

		toolbarItems = toolbarItems.concat([{
			xtype: me.allowAutoRefresh ? 'splitbutton' : 'button',
			itemId: 'refresh',
			tooltip: me.refreshText,
			overflowText: me.refreshText,
			iconCls: Ext.baseCSSPrefix + 'tbar-loading',
			handler: me.doRefresh,
			scope: me,
			menu: me.allowAutoRefresh ? {
				width: 175,
				items: [{
					text: me.autoRefreshText,
					checked: this.autoRefreshEnabled,
					checkHandler: function(item, checked) {
						var interval = item.ownerCt.down('numberfield').getValue();
						if (checked) {
							me.startAutoRefresh(interval || this.autoRefreshInterval)
							Ext.state.Manager.set('autoRefreshEnabled', true)
						} else {
							me.stopAutoRefresh()
							Ext.state.Manager.set('autoRefreshEnabled', false)
						}
					}
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'numberfield',
						padding: '5 5 7 60',
						hideLabel: true,
						value: this.autoRefreshInterval,
						width: 60,
						minValue: 10,
						listeners: {
							change: function(field, value) {
								me.changeInterval(value);
							},
							blur: function(field, value) {
								if (this.getValue() < 10)
									this.setValue(10);
							}
						}
					}]
				}]
			} : null,
			listeners: {
				render: function(button) {
					if (!me.notActivateCtrl_R) {
						clientVM.updateRefreshButtons(button);
					}
				}
			}
		}]);

		if (this.autoRefreshEnabled)
			me.startAutoRefresh(this.autoRefreshInterval)

		me.toolbar = Ext.create('Ext.toolbar.Toolbar', {
			enableOverflow: false,
			dock: 'top',
			cls: me.toolbarCls ? me.toolbarCls : '',
			items: toolbarItems
		});
		grid.insertDocked(0, me.toolbar);

		me.filterBar = Ext.create('RS.common.FilterBar', {
			allowPersistFilter: me.allowPersistFilter,
			cls : me.toolbarCls ? me.toolbarCls : '',
			listeners: {
				scope: me,
				filterChanged : {
					fn : me.filterChanged,
					buffer : 10
				},
				saveFilter: me.saveFilter,
				clearFilter: me.clearFilter
			}
		});
		grid.insertDocked(1, me.filterBar);
		me.toolbar.addEvents(
			/**
			 * @event change
			 * Fires after the active page has been changed.
			 * @param {Ext.toolbar.Paging} this
			 * @param {Object} pageData An object that has these properties:
			 *
			 * - `total` : Number
			 *
			 *   The total number of records in the dataset as returned by the server
			 *
			 * - `currentPage` : Number
			 *
			 *   The current page number
			 *
			 * - `pageCount` : Number
			 *
			 *   The total number of pages (calculated from the total number of records in the dataset as returned by the
			 *   server and the current {@link Ext.data.Store#pageSize pageSize})
			 *
			 * - `toRecord` : Number
			 *
			 *   The starting record index for the current page
			 *
			 * - `fromRecord` : Number
			 *
			 *   The ending record index for the current page
			 */
			'change',

			/**
			 * @event beforechange
			 * Fires just before the active page is changed. Return false to prevent the active page from being changed.
			 * @param {Ext.toolbar.Paging} this
			 * @param {Number} page The page number that will be loaded on change
			 */
			'beforechange');

		me.toolbar.on('beforerender', me.onLoad, me, {
			single: true
		});
		me.grid.store.fireEvent('commonSearchFilterInited', me, me);
		me.bindStore(myGrid.store || 'ext-empty-store', true);
	},

	// @private
	updateInfo: function() {
		var me = this,
			displayItem = me.toolbar.child('#displayItem'),
			store = me.store,
			pageData = me.getPageData(),
			count, msg;

		if (displayItem) {
			count = store.getCount();
			if (count === 0) {
				msg = me.emptyMsg;
			} else {
				msg = Ext.String.format(
					me.displayMsg, pageData.fromRecord, pageData.toRecord, pageData.total);
			}
			displayItem.setText(msg);
		}
	},

	// @private
	beforeLoad: function() {
		if (this.rendered && this.refresh) {
			this.refresh.disable();
		}
	},

	// @private
	onLoadError: function() {
		if (!this.rendered) {
			return;
		}
		this.child('#refresh').enable();
	},

	// @private
	onLoad: function() {
		var me = this,
			pageData, currPage, pageCount, afterText, count, isEmpty;

		count = me.store.getCount();
		isEmpty = count === 0;
		if (!isEmpty) {
			pageData = me.getPageData();
			currPage = pageData.currentPage;
			pageCount = pageData.pageCount;
		} else {
			currPage = 0;
			pageCount = 0;
		}

		if (me.pageable) {
			Ext.suspendLayouts();
			me.toolbar.child('#first').setDisabled(currPage === 1 || isEmpty);
			me.toolbar.child('#prev').setDisabled(currPage === 1 || isEmpty);
			me.toolbar.child('#next').setDisabled(currPage === pageCount || isEmpty);
			me.toolbar.child('#last').setDisabled(currPage === pageCount || isEmpty);
			me.toolbar.child('#refresh').enable();
			me.updateInfo();
			Ext.resumeLayouts(true);
		} else {
			me.toolbar.child('#refresh').enable();
			me.updateInfo();
		}

		if (me.toolbar.rendered) {
			me.toolbar.fireEvent('change', me, pageData);
		}
	},

	// @private
	getPageData: function() {
		var store = this.store,
			totalCount = store.getTotalCount();

		return {
			total: totalCount,
			currentPage: store.currentPage,
			pageCount: Math.ceil(totalCount / store.pageSize),
			fromRecord: ((store.currentPage - 1) * store.pageSize) + 1,
			toRecord: Math.min(store.currentPage * store.pageSize, totalCount)

		};
	},

	/**
	 * Move to the first page, has the same effect as clicking the 'first' button.
	 */
	moveFirst: function() {
		var me = this;
		if (me.toolbar.fireEvent('beforechange', me, 1) !== false) {
			me.store.loadPage(1);
		}
	},

	/**
	 * Move to the previous page, has the same effect as clicking the 'previous' button.
	 */
	movePrevious: function() {
		var me = this,
			prev = me.store.currentPage - 1;

		if (prev > 0) {
			if (me.toolbar.fireEvent('beforechange', me, prev) !== false) {
				me.store.previousPage();
			}
		}
	},

	/**
	 * Move to the next page, has the same effect as clicking the 'next' button.
	 */
	moveNext: function() {
		var me = this,
			total = me.getPageData().pageCount,
			next = me.store.currentPage + 1;

		if (next <= total) {
			if (me.toolbar.fireEvent('beforechange', me, next) !== false) {
				me.store.nextPage();
			}
		}
	},

	/**
	 * Move to the last page, has the same effect as clicking the 'last' button.
	 */
	moveLast: function() {
		var me = this,
			last = me.getPageData().pageCount;

		if (me.toolbar.fireEvent('beforechange', me, last) !== false) {
			me.store.loadPage(last);
		}
	},

	/**
	 * Refresh the current page, has the same effect as clicking the 'refresh' button.
	 */
	doRefresh: function() {
		var me = this,
			current = me.store.currentPage;

		if (me.toolbar.fireEvent('beforechange', me, current) !== false) {
			me.store.loadPage(current);
		}
	},

	getStoreListeners: function() {
		return {
			beforeload: this.beforeLoad,
			load: this.onLoad,
			exception: this.onLoadError
		};
	},

	/**
	 * Unbinds the paging toolbar from the specified {@link Ext.data.Store} **(deprecated)**
	 * @param {Ext.data.Store} store The data store to unbind
	 */
	unbind: function(store) {
		this.bindStore(null);
	},

	/**
	 * Binds the paging toolbar to the specified {@link Ext.data.Store} **(deprecated)**
	 * @param {Ext.data.Store} store The data store to bind
	 */
	bind: function(store) {
		this.bindStore(store);
	},

	// @private
	destroy: function() {
		this.stopAutoRefresh()
		this.unbind()
		this.callParent()
	},

	changeInterval: function(interval) {
		if (this.autoRefreshTask) {
			this.stopAutoRefresh()
			this.startAutoRefresh(interval)
		}
		Ext.state.Manager.set('autoRefreshInterval', interval < 10 ? 10 : interval);
	},

	startAutoRefresh: function(interval) {
		if (this.autoRefreshTask) this.autoRefreshTask.destroy();

		var runner = new Ext.util.TaskRunner();
		this.autoRefreshTask = runner.newTask({
			run: this.doRefresh,
			scope: this,
			interval: interval * 1000
		});
		this.autoRefreshTask.start();
	},
	stopAutoRefresh: function() {
		if (this.autoRefreshTask) {
			this.autoRefreshTask.destroy();
			delete this.autoRefreshTask;
		}
	},

	toggleSysColumns: function(button) {
		Ext.state.Manager.set('userShowSysInfoColumns', button.pressed)
		Ext.Array.forEach(this.columns, function(column) {
			if ((column.dataIndex && column.dataIndex.indexOf('sys') == 0 && !column.initialShow) || column.dataIndex == 'id') {
				if (column.rendered)
					column[button.pressed ? 'show' : 'hide']()
				else column.hidden = !button.pressed
			} else if (column.dataIndex && Ext.Array.indexOf(['processNumber', 'duration', 'address', 'targetGUID', 'esbaddr'], column.dataIndex) > -1) {
				//This is for worksheet columns.  Total hack, but Duke wants these columns hidden/shown based on the sysinfo button too
				if (column.rendered)
					column[button.pressed ? 'show' : 'hide']()
				else column.hidden = !button.pressed
			}
		})
	},

	setSocialId: function(value) {
		this.toolbar.down('#followButton').streamId = value
	},
	setSocialStreamType: function(value) {
		this.toolbar.down('#followButton').streamType = value
	},

	//Search Configuration
	dateFormat: 'Y-m-d',
	timeFormat: 'H:i a',
	submitFormat: 'Y-m-d H:i:s',
	allowPersistFilter: true,
	showComparisons: true,
	useWindowParams: true,
	gridName: '',
	displayFilter: true,
	clobCheckURL : '',
	clobTypeFields : [],
	convertColumnToField : function(dataArr){
        if(dataArr.length == 0)
            return dataArr;
        var resArr = [];
        for(var i = 0; i < dataArr.length; i++){
            var column = dataArr[i];
            var field = column.toLowerCase().replace(/_/g, "");
            resArr.push(field);
        }
        return resArr;
    },
	configureViewMenuItems: function() {
		//Load the views into the view menu
		var viewMenuItem = this.toolbar.down('#viewMenuItem');
		if (viewMenuItem) {
			var viewMenu = viewMenuItem.menu;

			viewMenu.removeAll();
			this.viewStore.each(function(record) {
				viewMenu.add({
					text: record.data.name,
					group: 'view',
					checked: this.grid.serverData.id == record.data.id,
					tableName: this.grid.tableName,
					grid: this.grid,
					record: record.raw,
					checkHandler: this.selectView
				});
			}, this);
		}
	},
	saveView: function() {
		//Save the current view as a new view, so prompt the user for a new name (pre-populated with the current view if one is selected)
		var viewsMenuItem = this.toolbar.down('#viewMenuItem'),
			value = '';
		//figure out the selected view based on the menu
		viewsMenuItem.menu.items.each(function(view) {
			if (view.checked) {
				value = view.text;
				return false;
			}
		});

		Ext.MessageBox.show({
			title: RS.common.locale.saveViewTitle,
			msg: RS.common.locale.saveViewMessage,
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: RS.common.locale.saveView,
				no: RS.common.locale.cancel
			},
			scope: this,
			fn: this.handleSaveView,
			prompt: true,
			value: value || ''
		});
	},
	handleSaveView: function(button, input) {
		if (button == 'yes') {
			//Check if the name matches an existing view.  If it does, warm them that it will replace the existing view and are they sure they want to do that
			var viewsMenuItem = this.toolbar.down('#viewMenuItem'),
				match = false,
				isSystem = false;
			//figure out the selected view based on the menu
			viewsMenuItem.menu.items.each(function(view) {
				if (view.text == input) {
					match = true;
					isSystem = view.type == 'Shared';
					return false;
				}
			});

			if (isSystem) {
				Ext.MessageBox.show({
					title: RS.common.locale.saveSystemViewTitle,
					msg: RS.common.locale.saveSystemViewBody,
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						yes: RS.common.locale.saveView,
						no: RS.common.locale.cancel
					},
					scope: this,
					fn: this.handleSaveView,
					prompt: true
				});
				return;
			}

			if (match) {
				var inputConfig = {
					input: input,
					searchControl: this
				};
				//Double check about the duplicate name
				Ext.MessageBox.show({
					title: RS.common.locale.saveViewTitleConfirm,
					msg: Ext.String.format(RS.common.locale.saveViewMessageConfirm, input),
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						yes: RS.common.locale.saveViewConfirm,
						no: RS.common.locale.cancel
					},
					scope: inputConfig,
					fn: this.reallySaveView
				});
			} else this.persistView(input);
		}
	},
	reallySaveView: function(button) {
		if (button == 'yes') this.searchControl.persistView(this.input);
	},
	persistView: function(viewName) {
		var viewsMenuItem = this.toolbar.down('#viewMenuItem'),
			match = null,
			params = {},
			fields = [];

		this.grid.headerCt.items.each(function(column) {
			if (column.fieldConfig) {
				column.fieldConfig.hidden = column.hidden;
				fields.push(column.fieldConfig);
			}
		});

		//figure out the selected view based on the menu
		viewsMenuItem.menu.items.each(function(view) {
			if (view.checked) {
				match = view;
				return false;
			}
		});

		if (match) {
			var matchRecord = match.record;

			matchRecord.fields = fields;
			if (match.text != viewName || match.type == 'Shared') {

				var viewsMenuItem = this.toolbar.down('#viewMenuItem'),
					matchId = '';
				//figure out the selected view based on the menu
				viewsMenuItem.menu.items.each(function(view) {
					if (view.text == viewName) {
						matchId = view.id
						return false
					}
				});

				matchRecord.id = matchId || '';
				matchRecord.type = 'Self';
				matchRecord.user = '';
				matchRecord.name = viewName;

				//Need to make sure to get these from the parent when creating a new view
				delete matchRecord.metaFormLink;
				delete matchRecord.metaNewLink;
			}

			Ext.Ajax.request({
				url: '/resolve/service/view/save',
				jsonData: matchRecord,
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) {
						this.grid.serverData.id = response.data.id;
						this.viewStore.load();
					} else {
						clientVM.displayError(response.message);
					}
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			});
		}
	},

	clearFilter: function() {
		if(this.toolbar.down('#filter'))
			this.toolbar.down('#filter').clearValue();
	},
	saveFilter: function() {
		var value = this.toolbar.down('#filter').getDisplayValue();
		//Prompt the user for a name to save the filter as (must be unique in the list of filter names that they have though)
		Ext.MessageBox.show({
			title: RS.common.locale.saveFilterTitle,
			msg: RS.common.locale.saveFilterMessage,
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: RS.common.locale.saveFilter,
				no: RS.common.locale.cancel
			},
			scope: this,
			fn: this.handleSaveFilter,
			prompt: true,
			value: value || ''
		});
	},
	handleSaveFilter: function(button, input) {
		if (button == 'yes') {
			//Check if the name matches an existing filter.  If it does, warn them that it will replace the existing filter and are they sure they want to do that
			var rec = this.filterStore.findRecord('name', input, 0, false, false, true),
				inputConfig = {
					input: input,
					searchControl: this
				};
			if (rec) {
				//Double check about the duplicate name
				Ext.MessageBox.show({
					title: RS.common.locale.saveFilterTitleConfirm,
					msg: Ext.String.format(RS.common.locale.saveFilterMessageConfirm, input),
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						yes: RS.common.locale.saveFilterConfirm,
						no: RS.common.locale.cancel
					},
					scope: inputConfig,
					fn: this.reallySaveFilter
				});
			} else {
				this.persistFilter(input);
			}
		}
	},
	reallySaveFilter: function(button) {
		if (button == 'yes') this.searchControl.persistFilter(this.input);
	},
	persistFilter: function(filterName) {
		var filterString = this.getFiltersString(),
			rec = this.filterStore.findRecord('name', filterName, 0, false, false, true),
			params = {
				value: filterString
			};

		if (rec) {
			Ext.applyIf(params, rec.raw);
		}

		Ext.applyIf(params, {
			name: filterName,
			metaTableName: this.grid.tableName,
			metaTableSysId: this.grid.tableSysId,
			isSelf: true
		});
		Ext.Ajax.request({
			url: '/resolve/service/filter/save',
			params: params,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					this.filterStore.load({
						scope: this,
						callback: function(records) {
							Ext.each(records, function(record) {
								if (record.data.name == params.name) this.toolbar.down('#filter').setValue(record.internalId);
							}, this);
							clientVM.displaySuccess(RS.common.locale.filterSaved,4000,RS.common.locale.success);
						}
					});
				} else{
					clientVM.displayError(response.message, 0,RS.common.locale.error);
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		});
	},
	addFilter: function(value, suppressChangeEvt) {
		var internalValue = this.getInternalValue(value),
			split = internalValue.split(' ');

		//if the user has typed: field condition value, then add it
		if (split.length > 2 && split[2]) {
			this.filterBar.addFilter(value, internalValue);
			if(!suppressChangeEvt){
				this.filterBar.fireEvent('filterChanged');
			}
			return true;
		}

		return false;
	},
	parseSuggestionsForStore: function(store, value) {
		//Split the value on spaces for the search query
		var searches = value.split(' '),
			columnText = '',
			column = null,
			i;
		var me = this;
		//Parse column text
		for (i = 0; i < searches.length && this.couldBeColumn(Ext.String.trim(columnText + ' ' + searches[i])); i++) {
			columnText += ' ' + searches[i];
			columnText = Ext.String.trim(columnText);
		}

		Ext.each(this.grid.columns, function(gridColumn) {
			if (gridColumn.text.toLowerCase() === columnText.toLowerCase()) {
				column = gridColumn;
				return false;
			}
		});

		store.removeAll();

		//Add in the first non-date column as a search suggestion if the current selection isn't good
		if (!column || !column.filterable) {
			var col = null;
			Ext.each(this.grid.columns, function(c) {
				if (c.dataType != 'date' && c.uiType != 'DateTime' && c.filterable) {
					col = c
					return false
				}
			})
			if (col) {
				store.add({
					name: Ext.String.format('{0} {1} <b>{2}</b>', col.text, RS.common.locale.contains, value),
					value: RS.common.locale.contains,
					displayName: Ext.String.format('{0} {1} {2}', col.text, RS.common.locale.contains, value)
				})
			}
		}

		//If we have a column, then we're searching on a column and need the operators
		if (column && column.filterable) {
			var conditionText = '';
			var	operator = null;
			var isClobField = false;
			Ext.each(me.clobTypeFields, function(clobField){
				if(column.dataIndex == clobField){
					isClobField = true;
					return;
				}
			})
			for (; i < searches.length && this.couldBeCondition(Ext.String.trim(conditionText + ' ' + searches[i])); i++) {
				conditionText += ' ' + searches[i];
				conditionText = Ext.String.trim(conditionText);
				if(conditionText == RS.common.locale.equals && isClobField){
					conditionText = RS.common.locale.contains;
				}
			}

			//Get operator equivalent
			Ext.Object.each(this.conditions, function(con) {
				if (this.conditions[con] == conditionText) {
					if (con.indexOf('short') == 0) {
						operator = this.shortConditions[conditionText]
					} else
						operator = this.conditions[con];
					return false;
				}
			}, this);

			if (operator) {
				//Then we have an operator and need to display the data suggestions
				var value = searches.splice(i).join(' '),
					options = this.getValueOptionsForDataType(column, columnText, conditionText, value),
					dataValues = [];

				//Add in the user's typed in search criteria as the first option
				store.add({
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, conditionText, value),
					value: conditionText,
					displayName: Ext.String.format('{0} {1} {2}', columnText, conditionText, value)
				});

				store.add(options);

				//Get the data options for the column
				this.grid.store.each(function(record) {
					var data = record.get(column.dataIndex);
					if (data) {
						if (Ext.isDate(data)) data = Ext.Date.format(data, this.dateFormat);
						if (Ext.isBoolean(data)) data = data.toString();
						if (Ext.isNumber(data)) data = data.toString();
						if (dataValues.indexOf(data.toLowerCase()) === -1 && data.toLowerCase().indexOf(value.toLowerCase()) > -1) {
							dataValues.push(data.toLowerCase());
							store.add({
								name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, conditionText, data),
								value: conditionText,
								displayName: Ext.String.format('{0} {1} {2}', columnText, conditionText, data)
							});
						}
					}
					if (store.getCount() >= 6) return false;
				}, this);

				//If we have the operator, but don't have any data suggestions for what they are typing, then we need to just return the value that they are typing to help them out
				if (store.getCount() === 0) {
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, conditionText, value),
						value: conditionText,
						displayName: Ext.String.format('{0} {1} {2}', columnText, conditionText, value)
					})
				}
			} else {
				//Then we have a part of an operator and need to display the operators that match the search
				var options = this.getOptionsForDataType(column,isClobField);

				//First add in the contains of the user provided value as an option for the user if it doesn't match a condition option
				var val = searches.slice(1).join(' ');
				if (val) {
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, RS.common.locale.contains, val),
						value: RS.common.locale.contains,
						displayName: Ext.String.format('{0} {1} {2}', columnText, RS.common.locale.contains, val)
					})
				}

				Ext.each(options, function(option) {
					if (option.value.indexOf(conditionText) > -1) store.add(option);
				});
			}
		}

		if (columnText && store.getCount() < 7) {
			//We don't have a column, but have a part of a column so show those that match the current search
			Ext.each(this.grid.columns, function(column) {
				if (column.filterable && column.text.toLowerCase().indexOf(columnText.toLowerCase()) === 0) {
					store.add({
						name: Ext.String.format('<b>{0}</b>{1}', column.text.substring(0, columnText.length), column.text.substring(columnText.length)),
						value: column.text,
						type: column.dataType,
						displayName: column.text + ' '
					});
					if (store.getCount() >= 6) return false;
				}
			});
		}

		if (store.getCount() < 7) {
			//No column matches, so we need to search the data
			var query = value.toLowerCase(),
				columnData = [],
				recordData = [],
				records = [];
			this.grid.store.each(function(record) {
				Ext.Object.each(record.data, function(data) {
					var tempValue = record.data[data];
					if (Ext.isBoolean(tempValue)) tempValue = tempValue.toString()
					if (Ext.isDate(tempValue)) tempValue = Ext.Date.format(tempValue, this.dateFormat)
					if (Ext.isNumber(tempValue)) tempValue = tempValue.toString()
					if (tempValue && Ext.isString(tempValue) && tempValue.toLowerCase().indexOf(query.toLowerCase()) > -1 && recordData.indexOf(tempValue.toLowerCase()) == -1) {
						if (columnData.indexOf(data) == -1) columnData.push(data);
						recordData.push(tempValue.toLowerCase());
						records.push({
							column: data,
							data: tempValue
						});
					}
					if (columnData.length >= 6 || recordData.length >= 6) return false;
				}, this);
			}, this);

			//Now we have a list of columns.  Display the columns with the appropriate comparisons for the data
			Ext.each(columnData, function(column) {
				if (store.getCount() >= 6) return false;
				Ext.each(this.grid.columns, function(gridColumn) {
					if (store.getCount() >= 6) return false;
					if (gridColumn.dataIndex == column && column.filterable) {
						var conditionText = RS.common.locale.contains;
						if (gridColumn.dataType == 'date') conditionText = RS.common.locale.on;
						store.add({
							name: Ext.String.format('{0} {1} <b>{2}</b>', gridColumn.text, conditionText, query),
							value: conditionText,
							displayName: Ext.String.format('{0} {1} {2}', gridColumn.text, conditionText, query)
						});
					}
				});
			}, this);

			//Now we have a list of the records and their column dataIndexes so we need to turn those into proper queries based on their types
			Ext.each(records, function(record) {
				if (store.getCount() >= 6) return false;
				Ext.each(this.grid.columns, function(column) {
					if (column.dataIndex == record.column && column.filterable) {
						record.columnText = column.text;
						record.columnDataType = column.dataType;
						if (column.dataType == 'date') record.conditionText = RS.common.locale.on;
						else {
							var isClobField = false;
							Ext.each(me.clobTypeFields, function(clobField){
								if(column.dataIndex == clobField){
									isClobField = true;
									return;
								}
							})
							record.conditionText = isClobField ? RS.common.locale.contains : RS.common.locale.equals;
						}
						return false;
					}
				});
				if (record.columnText) {
					//Now that we have a record with all the information we could need, we need to add it to the store
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', record.columnText, record.conditionText, record.data),
						value: record.data,
						displayName: Ext.String.format('{0} {1} {2}', record.columnText, record.conditionText, record.data)
					});
				}
			}, this);
		}

		if (store.getCount() === 0) {
			//No records match the search criteria as of yet (could be another page or something), so load up the columns with their types based on the type of query
			var queryType = 'string',
				conditionText = RS.common.locale.contains;
			if (Ext.isNumber(Number(value))) {
				queryType = 'float';
				conditionText = RS.common.locale.equals
			}
			if (Ext.isDate(Ext.Date.parse(value, this.dateFormat))) {
				queryType = 'date';
				conditionText = RS.common.locale.on;
			}

			Ext.each(this.grid.columns, function(column) {
				if (column.filterable && column.dataType == queryType) {
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', column.text, conditionText, value),
						value: value,
						displayName: Ext.String.format('{0} {1} {2}', column.text, conditionText, value)
					});
				}
				if (store.getCount() >= 6) return false;
			});
		}
	},
	getInternalValue: function(value) {
		var returnText = '',
			split = value.split(' ');

		var columnText = '',
			i;
		//Get column dataIndex
		for (i = 0; i < split.length && this.couldBeColumn(Ext.String.trim(columnText + ' ' + split[i])); i++) {
			columnText += ' ' + split[i];
			columnText = Ext.String.trim(columnText);
		}
		Ext.each(this.grid.columns, function(column) {
			if (column.text.toLowerCase() == columnText.toLowerCase()) returnText += column.dataIndex + ' ';
		});

		var conditionText = '';
		for (; i < split.length && this.couldBeCondition(Ext.String.trim(conditionText + ' ' + split[i])); i++) {
			conditionText += ' ' + split[i];
			conditionText = Ext.String.trim(conditionText);
		}

		//Get operator equivalent
		Ext.Object.each(this.conditions, function(con) {
			var c = con;
			if (this.conditions[con] == conditionText) {
				if (con.indexOf('short') == 0) {
					c = this.shortConditions[conditionText]
				}
				returnText += c;
				return false;
			}
		}, this);
		returnText += ' ';

		var val = split.slice(i).join(' '),
			matchDataValue = false;
		//Get value equivalent
		Ext.Object.each(this.dataValues, function(value) {
			if (this.dataValues[value] == val) {
				matchDataValue = true;
				returnText += value;
				return false;
			}
		}, this);
		if (!matchDataValue) returnText += val;

		return returnText;
	},
	getExternalValue: function(filter) {
		var split = filter.split(' '),
			columnText = split[0],
			condition = split[1];
		split.splice(0, 2);
		var value = split.join(' ');
		//Convert column
		Ext.each(this.grid.columns, function(column) {
			if (column.dataIndex == columnText) {
				columnText = column.text;
				return false;
			}
		});
		//Convert condition
		condition = RS.common.locale[condition];

		return Ext.String.format('{0} {1} {2}', columnText, condition, value);
	},
	couldBeColumn: function(columnText) {
		var exists = false;
		Ext.each(this.grid.columns, function(column) {
			if (column.filterable && column.text.toLowerCase().indexOf(columnText.toLowerCase()) == 0) {
				exists = true;
				return false;
			}
		});
		return exists;
	},
	couldBeCondition: function(condition) {
		condition = condition.toLowerCase();
		var returnVal = false;
		Ext.Object.each(this.conditions, function(con) {
			if (this.conditions[con].indexOf(condition) == 0) {
				returnVal = true;
				return false;
			}
		}, this);
		return returnVal;
	},
	filterChanged: function(a,b,c,d) {
		//Reset to page 1 whenever the filter changes because the current page might not be around anymore and the server is naive to know
		this.grid.store.loadPage(1);
	},
	getWhereFilter: function() {
		var filters = this.filterBar.getFilters(),
			whereFilter = [];

		Ext.each(filters, function(filter, index) {
			var split = filter.split(/\s+/),
				column = split[0],
				condition = split[1],
				value = split.slice(2).join(' '),
				col = {},
				field, val;

			//get the column based on the column dataIndex
			Ext.each(this.grid.columns, function(gridColumn) {
				if (gridColumn.dataIndex == column) {
					col = gridColumn;
					return false;
				}
			});

			if (col) {
				if (col.dataType == 'date') {
					val = Ext.Date.parse(value, this.dateFormat);
					if (!Ext.isDate(val)) val = Ext.Date.parse(value, 'c')
					if (Ext.isDate(val)) {
						value = Ext.Date.format(val, 'c');
					}
				}

				if (col.dataType == 'bool') {
					value = value === 'true' || value === true || value === 1 || value === '1'
				}

				field = col.dataIndex;

				whereFilter.push({
					field: field,
					type: col.dataType,
					condition: condition,
					value: value
				});
			}
		}, this);

		return whereFilter;
	},
	getWhereClause: function() {
		var filters = this.filterBar.getFilters(),
			whereClause = '';
		//Parse through filters to get the where clause
		Ext.each(filters, function(filter, index) {
			var split = filter.split(' '),
				column = split[0],
				condition = split[1],
				value = split.splice(2).join(' '),
				where = '(',
				addValue = true;

			where += column + ' ';
			var values = value.split('|');
			//get the column based on the column dataIndex
			var col = {};
			Ext.each(this.grid.columns, function(gridColumn) {
				if (gridColumn.dataIndex == column) {
					col = gridColumn;
					return false;
				}
			});
			Ext.each(values, function(value, index) {
				if (value) {
					if (index > 0) where += 'OR ' + column + ' ';
					if (col.dataType == 'bool') {
						value = value === 'true' ? '1' : '0';
					}
					//Get condition equivalent
					switch (condition.toLowerCase()) {
						case 'on':
							where += "BETWEEN (date '{0}') AND (date '{1}')";
							break;
						case 'after':
							where += "> (date '{0}')";
							break;
						case 'before':
							where += "< (date '{0}')";
							break;
						case 'equals':
							var eqCond = Ext.isNumber(Number(value)) ? '=' : "= '{0}'";
							if (value === 'NULL') {
								eqCond = 'IS NULL';
								addValue = false;
							}
							if (value === 'NIL') {
								eqCond = Ext.String.format("IS NULL OR {0} = ''", column);
								addValue = false;
							}
							where += eqCond;
							break;
						case 'notequals':
							var eqCond = Ext.isNumber(Number(value)) ? '<>' : "<> '{0}'";
							if (value === 'NULL') {
								eqCond = 'IS NOT NULL';
								addValue = false;
							}
							if (value === 'NIL') {
								eqCond = Ext.String.format("IS NOT NULL OR {0} <> ''", column);
								addValue = false;
							}
							where += eqCond;
							break;
						case 'contains':
							where += "LIKE '%{0}%'";
							break;
						case 'notcontains':
							where += "NOT LIKE '%{0}%'";
							break;
						case 'startswith':
							where += "LIKE '{0}%'";
							break;
						case 'notstartswith':
							where += "NOT LIKE '{0}%'";
							break;
						case 'endswith':
							where += "LIKE '%{0}'";
							break;
						case 'notendswith':
							where += "NOT LIKE '%{0}'";
							break;
						case 'greaterthan':
							where += '> {0}';
							break;
						case 'greaterthanorequalto':
							where += '>= {0}';
							break;
						case 'lessthan':
							where += '< {0}';
							break;
						case 'lessthanorequalto':
							where += '<= {0}';
							break;
						default:
							where += '=';
					}

					where += ' ';

					switch (value) {
						case 'today':
							where = Ext.String.format(where, Ext.Date.format(new Date(), this.dateFormat), Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, 1), this.dateFormat));
							break;
						case 'yesterday':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						case 'lastweek':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.WEEK, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						case 'lastmonth':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.MONTH, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						case 'lastyear':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						default:
							if (addValue) {
								var val = value,
									secondVal = '';
								if (col.dataType == 'date') {
									val = Ext.Date.parse(value, this.dateFormat)
									if (!Ext.isDate(val)) val = Ext.Date.parse(value, 'c');
									if (Ext.isDate(val)) {
										value = Ext.Date.format(val, this.submitFormat);
										secondVal = Ext.Date.format(Ext.Date.add(val, Ext.Date.DAY, 1), this.submitFormat);
									}
								}

								if (where.indexOf('{0}') > -1) where = Ext.String.format(where, value.replace(/'/g, "''"), secondVal.replace(/'/g, "''"));
								else where += value.replace(/'/g, "''");
							}
					}
				}
			}, this);

			whereClause += (index > 0 ? ' AND ' : ' ') + where + ')';
		}, this);
		return whereClause;
	},
	getOptionsForDataType: function(column, isClobField) {
		var me = this,
			options = [];
		switch (column.dataType) {
			case 'date':
				options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.on),
					value: RS.common.locale.on,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.on)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.before),
					value: RS.common.locale.before,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.before)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.after),
					value: RS.common.locale.after,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.after)
				}];
				break;
			case 'float':
			case 'int':
				options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
					value: RS.common.locale.equals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEquals),
					value: RS.common.locale.notEquals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEquals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.greaterThan),
					value: RS.common.locale.greaterThan,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.greaterThan)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.greaterThanOrEqualTo),
					value: RS.common.locale.greaterThanOrEqualTo,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.greaterThanOrEqualTo)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.lessThan),
					value: RS.common.locale.lessThan,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.lessThan)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.lessThanOrEqualTo),
					value: RS.common.locale.lessThanOrEqualTo,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.lessThanOrEqualTo)
				}];
				break;
			case 'bool':
				options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
					value: RS.common.locale.equals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEquals),
					value: RS.common.locale.notEquals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEquals)
				}];
				break;
			default:
				options = [];
				if(!isClobField){
					options = [{
						name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
						value: RS.common.locale.equals,
						displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
					}, {
						name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEquals),
						value: RS.common.locale.notEquals,
						displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEquals)
					}]
				}
				options.push({
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.contains),
					value: RS.common.locale.contains,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.contains)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notContains),
					value: RS.common.locale.notContains,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notContains)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.startsWith),
					value: RS.common.locale.startsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.startsWith)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notStartsWith),
					value: RS.common.locale.notStartsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notStartsWith)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.endsWith),
					value: RS.common.locale.endsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.endsWith)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEndsWith),
					value: RS.common.locale.notEndsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEndsWith)
				});
				if (!me.showComparisons && !isClobField) options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
					value: RS.common.locale.equals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
				}]
				break;
		}
		return options;
	},
	getShortOptionsForDataType: function(type, isClobField) {
		var options = [];
		switch (type) {
			case 'date':
				options = [{
					name: Ext.String.format('{0}', RS.common.locale.on),
					value: Ext.String.format('{0}', RS.common.locale.on)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.before),
					value: Ext.String.format('{0}', RS.common.locale.before)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.after),
					value: Ext.String.format('{0}', RS.common.locale.after)
				}];
				break;
			case 'float':
			case 'int':
				options = [{
					name: Ext.String.format('{0}', RS.common.locale.shortEquals),
					value: Ext.String.format('{0}', RS.common.locale.shortEquals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortNotEquals),
					value: Ext.String.format('{0}', RS.common.locale.shortNotEquals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortGreaterThan),
					value: Ext.String.format('{0}', RS.common.locale.shortGreaterThan)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortGreaterThanOrEqualTo),
					value: Ext.String.format('{0}', RS.common.locale.shortGreaterThanOrEqualTo)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortLessThan),
					value: Ext.String.format('{0}', RS.common.locale.shortLessThan)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortLessThanOrEqualTo),
					value: Ext.String.format('{0}', RS.common.locale.shortLessThanOrEqualTo)
				}];
				break;
			case 'bool':
				options = [{
					name: Ext.String.format('{0}', RS.common.locale.equals),
					value: Ext.String.format('{0}', RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notEquals),
					value: Ext.String.format('{0}', RS.common.locale.notEquals)
				}];
				break;
			default:
				options = [];
				if(!isClobField){
					options.push({
						name: Ext.String.format('{0}', RS.common.locale.equals),
						value: Ext.String.format('{0}', RS.common.locale.equals)
					},{
						name: Ext.String.format('{0}', RS.common.locale.notEquals),
						value: Ext.String.format('{0}', RS.common.locale.notEquals)
					});
				}
				options.push({
					name: Ext.String.format('{0}', RS.common.locale.contains),
					value: Ext.String.format('{0}', RS.common.locale.contains)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notContains),
					value: Ext.String.format('{0}', RS.common.locale.notContains)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.startsWith),
					value: Ext.String.format('{0}', RS.common.locale.startsWith)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notStartsWith),
					value: Ext.String.format('{0}', RS.common.locale.notStartsWith)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.endsWith),
					value: Ext.String.format('{0}', RS.common.locale.endsWith)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notEndsWith),
					value: Ext.String.format('{0}', RS.common.locale.notEndsWith)
				});
		}
		return options;
	},
	getValueOptionsForDataType: function(column, columnText, operatorText, dataValue) {
		var options = [],
			returnOptions = [];
		switch (column.dataType) {
			case 'date':
				options = [{
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.today),
					value: RS.common.locale.today,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.today)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.yesterday),
					value: RS.common.locale.yesterday,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.yesterday)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.lastWeek),
					value: RS.common.locale.lastWeek,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.lastWeek)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.lastMonth),
					value: RS.common.locale.lastMonth,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.lastMonth)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.lastYear),
					value: RS.common.locale.lastYear,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.lastYear)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, Ext.Date.format(new Date(), this.dateFormat)),
					value: Ext.Date.format(new Date(), this.dateFormat),
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, Ext.Date.format(new Date(), this.dateFormat))
				}];
				break;
		}
		Ext.each(options, function(option) {
			if (option.value.indexOf(dataValue) > -1) returnOptions.push(option);
		});
		return returnOptions;
	},
	getFiltersString: function() {
		var filters = this.filterBar.getFilters(),
			filterString = '';
		Ext.each(filters, function(filter, index) {
			if (index > 0) filterString += '|&&|';
			filterString += filter.replace(/ /g, '^')
		});
		return filterString;
	},
	setFilter: function(filter) {
		if (!filter) return;
		var filterStrings = filter.split('|&&|'),
			filters = [];
		Ext.each(filterStrings, function(filterString) {
			var parsed = filterString.replace(/\^/g, ' ');
			filters.push({
				value: this.getExternalValue(parsed),
				internalValue: parsed
			});
		}, this);
		this.filterBar.setFilters(filters);
	},

	selectView: function(item, checked) {
		if (checked) {
			Ext.Ajax.request({
				url: '/resolve/service/view/read',
				params: {
					table: item.tableName,
					view: item.text
				},
				scope: item,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) {
						if (response.data && response.data.fields) {
							var modelFields = [],
								columns = [];
							configureColumns(response.data.fields, modelFields, columns, this.grid.serverData.autoSizeColumns);
	
							Ext.define('RS.customtable.Table', {
								extend: 'Ext.data.Model',
								idProperty: 'sys_id',
								fields: modelFields
							});
							//reconfigure the grid to use the new columns
							this.grid.reconfigure(this.grid.store, columns); //reconfigure will fire the load automatically
							this.grid.fireEvent('viewSelected', this.grid, response.data);
						}
					} else {
						clientVM.displayError(response.message);
					}
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			});
		}
	},
	parseWindowParameters: function() {
		if (!this.useWindowParams) return false

		//Parse through url parameters matching the dataIndex of the columns
		var me = this,
			windowLocation = window.location.href,
			winParams = {},
			addedFilter = false,
			windowParams = windowLocation.split('?')[1],
			hashParams = windowLocation.split('#')[1],
			windowParamsSplit = [],
			suppressChangeEvt = true;

		if (windowParams) {
			windowParamsSplit = windowParams.split('#'); //remove hastag navigation
			windowParams = windowParamsSplit[0]
			winParams = Ext.Object.fromQueryString(windowParams)
		}

		if (hashParams && clientVM && clientVM.routes) {
			Ext.Object.each(clientVM.routes, function(route) {
				var params = RS.util.routeMatcher(route).parse(hashParams)
				if (params && params.params) {
					var p = Ext.Object.fromQueryString(params.params)
					Ext.apply(winParams, p)
					return false
				}
			})
		}

		var clearBeforeAdd = true;
		//handle 'All XXX' menu item
		var noFilter = winParams['noFilter'];		
		if (noFilter == true || noFilter == "true") {
			me.clearFilter();
			me.filterBar.clearFilter(me,false,suppressChangeEvt);
		}
		else{
			Ext.Object.each(winParams, function(param) {
				Ext.each(me.grid.columns, function(column) {
					if (column.dataIndex == param && column.filterable) {
						var tmp = winParams[param];

						if (Ext.isString(tmp)) tmp = [tmp];

						Ext.each(tmp, function(tmpString) {
							var split = tmpString.split('~'),
								value = split[split.length - 1],
								comp = RS.common.locale.equals;
							if (split.length > 1) {
								switch (split[0]) {
									case 'on':
										comp = RS.common.locale.on;
										break;
									case 'after':
										comp = RS.common.locale.after;
										break;
									case 'before':
										comp = RS.common.locale.before;
										break;
									case 'equals':
									case 'eq':
										comp = RS.common.locale.equals;
										break;
									case 'notEquals':
									case 'neq':
										comp = RS.common.locale.notEquals;
										break;
									case 'contains':
										comp = RS.common.locale.contains;
										break;
									case 'notContains':
										comp = RS.common.locale.notContains;
										break;
									case 'startsWith':
										comp = RS.common.locale.startsWith;
										break;
									case 'notStartsWith':
										comp = RS.common.locale.notStartsWith;
										break;
									case 'endsWith':
										comp = RS.common.locale.endsWith;
										break;
									case 'notEndsWith':
										comp = RS.common.locale.notEndsWith;
										break;
									case 'greaterThan':
									case 'gt':
										comp = RS.common.locale.greaterThan;
										break;
									case 'greaterThanOrEqualTo':
									case 'gteq':
										comp = RS.common.locale.greaterThanOrEqualTo;
										break;
									case 'lessThan':
									case 'lt':
										comp = RS.common.locale.lessThan;
										break;
									case 'lessThanOrEqualTo':
									case 'lteq':
										comp = RS.common.locale.lessThanOrEqualTo;
										break;
									default:
										comp = RS.common.locale.equals;
										break;
								}
							}

							if (clearBeforeAdd) {
								me.filterBar.clearFilter(me,false,suppressChangeEvt);
								clearBeforeAdd = false
							}

							me.addFilter(column.text + ' ' + comp + ' ' + value.replace(/\${USERID}/g, user.name || me.grid.serverData.currentUsername), suppressChangeEvt);
							addedFilter = true;
						});
						return false;
					}
				});

				if (param == 'filter') {
					var filterParam = winParams[param];
					//Set the filter by name
					me.filterStore.on('load', function() {
						var rec = this.filterStore.findRecord('name', winParams[param], 0, false, false, true);
						if (rec) {
							this.toolbar.down('#filter').setValue(rec.data.sysId);
						}
					}, me, {
						single: true
					});
				}
			});
		}
		return addedFilter
	},

	setDisplayName: function(name) {
		this.toolbar.down('#tableMenu').setText(name)
		if (this.allowPersistFilter) this.filterStore.load()
		if (this.grid.tableName && !this.hideMenu) this.viewStore.load()
	},

	setDisplayFilter: function(value) {
		this.displayFilter = value
		this.toolbar.down('#searchText').setVisible(value)
		this.filterBar.setVisible(value)
	}
});