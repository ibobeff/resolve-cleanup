glu.defModel('RS.incident.PlaybookTemplatePreview', {
	mixins: ['Playbook'],
	isPreview: true,
	init: function() {
		// previewing from Template Builder
		this.activitiesTab.set('isPreview', true);
		this.set('sysCreatedOn', this.parentVM.sysCreatedOn);
		this.set('sysCreatedBy', this.parentVM.sysCreatedBy);

		if (!this.previewUContent) {
			this.set('previewUContent', this.parentVM.getPageContent());
			this.set('fullName', this.parentVM.ufullname);
		}

		if (this.previewContent) {
			this.activitiesTab.set('previewContent', this.previewContent);
		}

		setTimeout(function(){
			this.fireEvent('loadDescription', this.previewUContent, this.fullName);
		}.bind(this), 500);
	},
	activate: function(screen, params) {
		if (params.name) {
			this.set('fullName', params.name);
			this.getPreviewContent(function(){
				this.fireEvent('loadDescription', this.previewUContent, this.fullName);
			});
		}
	},

	expandDescription: function() {
		var modal = this.open({
			mtype: 'RS.incident.DescriptionModal',
			ticketInfo: this.ticketInfo
		});
		modal.fireEvent('loadDescriptionModal', this.previewUContent, this.fullName)
	},
	returnTitle$: function() {
		return this.fullName;
	},
	returnId$: function() {
		return 'SIR-000000000';
	},
	returnDateCreated$: function() {
		if (this.sysCreatedOn) {
			return Ext.util.Format.date(new Date(this.sysCreatedOn), clientVM.getUserDefaultDateFormat());
		} else {
			return '';
		}
 	},
	returnSeverity$: function() {
		return 'Test';
	},
	returnType$: function() {
		return 'Test';
	},
	returnStatus$: function() {
		return 'Open';
	},
	returnDataCompromised$: function() {
		return 'Unknown';
	},
	returnCreatedBy$: function() {
		return this.sysCreatedBy;
	},

	close: function() {
		this.doClose()
	}
})

glu.defModel('RS.incident.PlaybookTemplateSearchPreview', {
	mixins: ['Playbook'],
	isReadOnly: true,
	activate: function(screen, params) {
		if (params.name) {
			this.set('fullName', params.name);
			this.activitiesTab.set('isReadOnly', true);
			this.getPreviewContent(function(){
				this.fireEvent('loadDescription', this.previewUContent, this.fullName);
			});
		}
	}
});
