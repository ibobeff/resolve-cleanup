glu.defModel('RS.incident.PlaybookTemplates', {
	store: {
		mtype: 'store',
		pageSize : 25,
		remoteSort: true,
		fields: ['id', 'uname', 'unamespace', 'usummary', 'ufullname', 'uisActive',
			{name: 'uversion', type: 'int'}].concat(RS.common.grid.getSysFields()),
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wikiadmin/listPlaybookTemplates',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	columns : [],
	templateSelections: [],
	pSirPlaybookTemplatesCreate: false,
	pSirPlaybookTemplatesView: false,
	pSirPlaybookTemplatesTemplateBuilderView: false,
	pSirPlaybookTemplatesTemplateBuilderModify: false,
	inactiveColumnShow: false,

	applyRBAC: function() {
		var permissions = clientVM.user.permissions || {};

		permissions.sirPlaybookTemplates = permissions.sirPlaybookTemplates || {};
		this.set('pSirPlaybookTemplatesView', permissions['sir'] && (permissions['sir.playbookTemplates.templateBuilder.create'] || permissions['sir.playbookTemplates.templateBuilder.change']));

		permissions.sirPlaybookTemplatesTemplateBuilder = permissions.sirPlaybookTemplatesTemplateBuilder || {};
		this.set('pSirPlaybookTemplatesTemplateBuilderView', permissions['sir'] && (permissions['sir.playbookTemplates.templateBuilder.create'] || permissions['sir.playbookTemplates.templateBuilder.change']));
		this.set('pSirPlaybookTemplatesTemplateBuilderModify', permissions['sir.playbookTemplates.templateBuilder.change']);
		this.set('pSirPlaybookTemplatesCreate', permissions['sir.playbookTemplates.templateBuilder.create']);
	},

	checkViewPermission: function() {
		return this.pSirPlaybookTemplatesView;
	},

	init: function() {
		var me = this;
		this.applyRBAC();
		this.store.on('beforeload', function(store, op) {
			var filters = {};
			if (!me.inactiveColumnShow) {
				filters.field="uisActive";
				filters.type="bool";
				filters.condition="equals";
				filters.value="true"
			}
			op.params = {};
			op.params.filter = JSON.stringify(filters);
			return me.checkViewPermission();
		});
		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'uname',
			filterable: true,
			flex: 1,
		}, {
			header: '~~namespace~~',
			dataIndex: 'unamespace',
			flex: 1,
			filterable: true
		}, {
			header: '~~revision~~',
			dataIndex: 'uversion',
			flex: 0.5,
			filterable: true
		}, {
			header: '~~inactive~~',
			dataIndex: 'uisActive',
			flex: 0.2,
			filterable: true,
			hidden: true,
			renderer:  function(value) { //,
				if (value) {
					return '';
				}
				return '<span class="icon-large icon-ok-sign rs-boolean-renderer"></span>'
			},
			align: 'center',
			listeners: {
				show: function() {
					me.set('inactiveColumnShow', true);
					me.store.load();
				},
				hide: function() {
					me.set('inactiveColumnShow', false);
					me.store.load();
				}
			}
		}].concat((function(columns) {
			Ext.each(columns, function(col) {
				if (col.dataIndex == 'sysCreatedOn') {
					col.hidden = false;
					col.width = 200,
					col.initialShow = true;
				}
				if (col.dataIndex == 'sysCreatedBy') {
					col.hidden = false;
					col.initialShow = true;
				}
				if (col.dataIndex == 'sysUpdatedOn') {
					col.hidden = false;
					col.initialShow = true;
				}
				if (col.dataIndex == 'sysUpdatedBy') {
					col.hidden = false;
					col.initialShow = true;
				}
				if (col.dataIndex == 'id') {
					col.flex = 1.5;
				}
			});
			return columns;
		})(RS.common.grid.getSysColumns())));
		//this.store.on('load', function() {
		//	var respData = this.store.proxy.reader.rawData;
		//	if (respData && !respData.success)
		//		clientVM.displayError(this.localize('listTemplatesError', respData.message));
		//}, this);
	},

	activate: function(screen, params) {
		this.applyRBAC();
		if (!this.pSirPlaybookTemplatesView) {
			this.message({
				title: this.localize('insufficientPermissionMsgTitle'),
				msg: this.localize('insufficientPermissionMsg'),
				closable : false,
				buttonText: {
					ok: this.localize('close')
				},
				scope: this,
				fn: function(btn) {
					clientVM.handleNavigationBack();
				}
			});
		} else {
			this.store.reload();
		}
	},

	editTemplate: function(id) {
		var r = this.store.findRecord('id', id);
		if (this.pSirPlaybookTemplatesTemplateBuilderView) {
			clientVM.handleNavigation({
				modelName: 'RS.incident.PlaybookTemplateRead',
				params: {
					name: r.get('ufullname'),
					uname: r.get('uname'),
					unamespace: r.get('unamespace'),
					id: r.get('id')
				}
			});
		}
	},
	createTemplate: function() {
		var me = this;
		this.open({
			mtype: 'RS.incident.PlaybookTemplateNamer'
		});
	},
	activatePBIsEnabled$: function() {
		// Make the Activate button enabled if there is an inactivated template selected
		var enabled = false;
		for (var i=0; !enabled && i<this.templateSelections.length; i++) {
			enabled = !this.templateSelections[i].get('uisActive');
		}
		return !!enabled;  // Making sure only true or false value is returned
	},
	activatePB: function() {
		this.updatePBActiveStatus('Activate');
	},

	inactivateIsEnabled$: function() {
		// Make the Inactivate button enabled if there is an activated template selected
		var enabled = false;
		for (var i=0; !enabled && i<this.templateSelections.length; i++) {
			enabled = this.templateSelections[i].get('uisActive');
		}
		return !!enabled;  // Making sure only true or false value is returned
	},

	inactivate: function() {
		this.updatePBActiveStatus('Inactivate');
	},

	updatePBActiveStatus: function(action) {
		var ids = [];
		for (var i=0; i<this.templateSelections.length; i++) {
			ids.push(this.templateSelections[i].get('id'));
		}
		this.ajax({
			url: '/resolve/service/playbook/updatePBActiveStatus',
			method: 'POST',
			scope: this,
			params: {
				status: action == 'Activate'? true: false
			},
			jsonData: {
				playbookSysids: ids
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					clientVM.displaySuccess(this.localize('successfully'+action+'PBs'));
					this.store.reload();
				} else {
					clientVM.displayError(respData.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	deleteTemplate: function() {
	}
});