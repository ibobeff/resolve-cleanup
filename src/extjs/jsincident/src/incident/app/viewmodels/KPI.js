glu.defModel('RS.incident.Dashboard.KPI', {
	viewMode: false,
	activeItem: 0,
	bannerText: '',
	dashboardrefreshRate: 'Never',
	refreshRateStore: {
		mtype: 'store',
		fields: ['rate'],
		data: [
			{rate: '1 min'},
			{rate: '5 min'},
			{rate: '10 min'},
			{rate: '15 min'},
			{rate: 'Never'}
		],
		proxy: {
			type: 'memory'
		}
	},
	kpiView: {
		mtype: 'RS.incident.Dashboard.KPIView'
	},
	kpiLayout: {
		mtype: 'RS.incident.Dashboard.KPILayout'
	},
	init: function() {
		this.set('activeItem', 0);
		this.set('viewMode', true);
		this.set('bannerText', this.localize('irDashboard'));
	},

	exitEdit: function() {
		this.set('activeItem', 0);
		this.set('viewMode', true);
		this.set('bannerText', this.localize('irDashboard'));
		this.kpiView.loadChartSettings();
	},

	configureSettings: function() {
		this.set('activeItem', 1);
		this.set('viewMode', false);
		this.set('bannerText', this.localize('irDashboardBuilder'));
		this.kpiLayout.loadLayout();
	},

	activate: function() {
		this.kpiView.loadChartSettings();
	},

	addSection: function() {
		this.kpiLayout.addSection();
	},

	addSectionIsEnabled$: function() {
		return this.kpiLayout.layoutSections.length < 3;
	},

	save: function() {
		this.kpiLayout.save();
	},

	refreshRateMap: {
		'1 min': 60,
		'5 min': 300,
		'10 min': 600,
		'15 min': 900
	},

	refreshRateChange: function(newRate, oldRate) {
		this.stopAutoRefresh();
		if (newRate != this.localize('never')) {
			this.startAutoRefresh(this.refreshRateMap[newRate]);
		}
	},

	manualRefresh: function() {
		this.kpiView.refresh();
	},

	doRefresh: function() {
		this.kpiView.refresh();
	},

	startAutoRefresh: function(interval) {
		if (this.autoRefreshTask) {
			this.autoRefreshTask.destroy();
		}

		var runner = new Ext.util.TaskRunner();
		this.autoRefreshTask = runner.newTask({
			run: this.doRefresh,
			scope: this,
			interval: (interval || 30)* 1000
		});
		this.autoRefreshTask.start();
	},

	stopAutoRefresh: function() {
		if (this.autoRefreshTask) {
			this.autoRefreshTask.destroy();
			delete this.autoRefreshTask;
		}
	},

	refreshSettings: function() {
		this.kpiLayout.refresh();
	}
});
