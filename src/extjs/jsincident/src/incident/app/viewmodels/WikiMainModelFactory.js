glu.defModel('RS.incident.WikiMainModelFactory', function(cfg) {
	var main = RS.wiki.viewmodels.Main,
		defaultData = Ext.clone(main.defaultData),
		fields = Ext.clone(main.fields);

	Ext.applyIf(cfg, RS.wiki.viewmodels.Main);

	return Ext.apply(cfg, {
		fields: fields.concat([
			{name: 'udisplayMode', type: 'string'},
			{name: 'uisTemplate', type: 'bool'},
			{name: 'uisDeletable', type: 'bool'}
		]),
		defaultData: Ext.apply(defaultData, {
			udisplayMode: 'playbook',
			uisTemplate: true,
			uisDeletable: false
		}),
		wikiMainInit: main.init,
		wikiMainActivate: main.activate,
		wikiMaingetPageContent: main.getPageContent,
		wikiMainReloadContent: main.reloadContent
	});
});
