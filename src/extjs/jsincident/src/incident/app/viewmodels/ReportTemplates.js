glu.defModel('RS.incident.ReportTemplates', {
	allSelected: false,	
	reportTemplateSelections: [],
	wait: false,
	reportTemplateStore: {
		mtype: 'store',
		pageSize : 25,
		remoteSort: true,
		fields: ['id', 'uname', 'unamespace', 'usummary', 'ufullname', {name: 'uversion', type: 'int'}].concat(RS.common.grid.getSysFields()),
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wikiadmin/listRunbooks',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		},
		autoLoad : true
	},

	init : function(){
		this.reportTemplateStore.on('beforeload', function(store, op) {
			this.set('wait', true);
		}, this);
		this.reportTemplateStore.on('load', function(store, records, success) {
			this.set('wait', false);
			//if (!success)
			//	clientVM.displayError(this.localize('listTemplatesError', this.store.getProxy().getReader().rawData.message));
		}, this);
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},
	showMessage: function(title, OpsMsg, OpMsg, confirm, url, errMsg, sucMsg, params) {
		var strFullName = [];
		Ext.each(this.reportTemplateSelections, function(r, idx) {
			strFullName.push(r.get('ufullname'));
		});
		var msg = this.localize(this.reportTemplateSelections.length > 1 || this.allSelected ? OpsMsg : OpMsg, {
			num: this.allSelected ? this.reportTemplateStore.getTotalCount() : this.reportTemplateSelections.length,
			names: strFullName.join(',')
		});

		// msg += '<br/><input type="checkbox" id="index_attachment" /> Index Attachments' +
		// '<input type="checkbox" id="index_actiontask" /> Index ActionTasks<br/><br/>';
		this.message({
			title: this.localize(title),
			msg: msg,
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize(confirm),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				params = params || {};
				params.indexAttachment = true;
				params.indexActionTask = true;
				this.sendReq(url, errMsg, sucMsg, params, this);
			}
		});
	},
	sendReq: function(url, errMsg, sucMsg, params, lockScreen) {
		lockScreen.set('wait', true);
		var ids = [];
		Ext.each(this.reportTemplateSelections, function(r, idx) {
			ids.push(r.get('id'));
		});
		var reqParams = {};
		reqParams.ids = ids;
		reqParams.all = this.allSelected;
		if (params)
			Ext.apply(reqParams, params);
		var win = this.open({
			mtype: 'RS.wiki.Polling'
		});
		this.ajax({
			url: url,
			params: reqParams,
			scope: this,
			success: function(resp) {
				win.doClose();
				this.handleSucResp(resp, errMsg, sucMsg);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				lockScreen.set('wait', false);
			}
		});
	},
	handleSucResp: function(resp, errMsg, sucMsg) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize(errMsg) + '[' + respData.message + ']');
		} else
			clientVM.displaySuccess(this.localize(sucMsg));
		this.set('reportTemplateSelections', []);
		this.reportTemplateStore.load();
	},
	new : function(){

	},
	copy : function(){
		var me = this;
		this.open({
			mtype: 'RS.wiki.pagebuilder.MoveOrRename',
			action: 'copy',
			namespace: this.reportTemplateSelections[0].get('unamespace'),
			filename: this.reportTemplateSelections[0].get('uname'),
			filenameIsVisible$: function() {
				return me.reportTemplateSelections.length == 1;
			},
			moveRenameCopyTitle: 'CopyTitle',
			viewmodelName: this.viewmodelName,
			dumper: (function(self) {
				return function(params) {
					self.doCopy(params)
				}
			})(this)
		});		
	},
	doCopy : function(params){
		this.showMessage(
			'copyTemplate',
			'copysMsg',
			'copyMsg',
			'copyTemplate',
			'/resolve/service/wikiadmin/moveRenameCopy',
			'copyErrMsg',
			'copySucMsg',
			params
		);
	},
	copyIsEnabled$: function() {
		return !this.wait && this.reportTemplateSelections.length > 0;
	},
	purge : function(){
		this.showMessage(
			'purgeTemplate',
			'purgesMsg',
			'purgeMsg',
			'purgeTemplate',
			'/resolve/service/wikiadmin/purge',
			'purgeErrMsg',
			'purgeSucMsg'
		);
	},
	purgeIsEnabled$: function() {
		return !this.wait && this.reportTemplateSelections.length > 0;
	},
	goToDashboard : function(){
		clientVM.handleNavigation({
			modelName: 'RS.incident.Dashboard'			
		});
	},
	editTemplate: function(id) {
		var r = this.reportTemplateStore.findRecord('id', id);
		clientVM.handleNavigation({
			modelName: 'RS.incident.PlaybookTemplate',
			params: {
				name: r.get('ufullname')
			}
		});
	},
})