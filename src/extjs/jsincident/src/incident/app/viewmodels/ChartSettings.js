glu.defModel('RS.incident.Dashboard.ChartSettings', function() {
	var filterStores = {};
	var arr = ['timeframeUnit', 'chartType', 'scopeUnit', 'type', 'severity', 'owner', 'team', 'status'];

	// Walk through the array and define combox's store
	// 'chartTypeFilterStore', 'scopeUnitFilterStore', 'typeFilterStore', 'severityFilterStore',
	// 'ownerFilterStore', 'teamFilterStore', 'statusFilterStore'
	arr.forEach(function(n) {
		filterStores[n+'FilterStore'] = {
			mtype: 'store',
			proxy: {
				type: 'memory'
			},
			fields: [n]
		}
	});
	return Ext.apply({
		chartId: '',
		target: null,
		chartType: '',
		scopeValue: 0,
		scope: 90,
		scopeUnit: 'Days',
		maxScope: 365,
		maxScopeInDays: 365,
		maxScopeInHours: 23,
		maxScopeInMinutes: 59,
		fvalid: true,
		numberOfCols: 6,
		minNColumn: 1,
		maxNColumn: 10,
		rb: '',
		graphType: '',
		typeFilter: 'All',
		severityFilter: 'All',
		ownerFilter: 'All',
		teamFilter: 'All',
		statusFilter: 'All',
		hideAgeBucketConfig: true,
		hideScopeConfig: true,
		hideColumnConfig: true,
		tips: '',
		chartConfigs: undefined,
		formLoaded: false,
		dirty: false,
		layoutContext: false,
		filterItems: {
			mtype: 'list'
		},

		// Chart settings configuration
		store: {
			mtype: 'store',
			model: 'RS.incident.model.ChartSettings'
		},

		when_scope_unit_changed_replace_the_tips: {
			on: ['scopeUnitChanged'],
			action: function() {
				this.setScopeUnitTips(this.scopeUnit);
			}
		},

		when_report_type_changed: {
			on : ['chartTypeChanged'],
			action : function() {
				this.reportTypeChanged();
			}
		},

		when_scope_changed: {
			on : ['scopeChanged'],
			action : function() {
				if (this.scope < this.numberOfCols) {
					this.set('numberOfCols', this.scope);
				}
			}
		},

		setScopeUnitTips: function(tips) {
			var map = {
				days: this.localize('between1To365days'),
				hours: this.localize('between1To23Hours'),
				minutes: this.localize('between1To59Minutes')
			}
			this.set('tips', map[tips.toLowerCase()]);
		},

		scopeUnitChange: function(rec) {
			var scopeUnit = rec[0].get('scopeUnit');
			if (scopeUnit == 'Days') {
				this.set('maxScope', 365);
				this.set('scope', (this.scope > this.maxScopeInDays)? this.maxScopeInDays: this.scope);
			} else if (scopeUnit == 'Hours') {
				this.set('maxScope', this.maxScopeInHours);
				this.set('scope', (this.scope > this.maxScopeInHours)? this.maxScopeInHours: this.scope);
			} else if (scopeUnit == 'Minutes') {
				this.set('maxScope', this.maxScopeInMinutes);
				this.set('scope', (this.scope > this.maxScopeInMinutes)? this.maxScopeInMinutes: this.scope);
			}
			this.set('fvalid', !this.fvalid); // Trick to clear invalid flag
		},

		addFilters: function(drivenBy) {
			var map = {};
			map[this.localize('investigationBySeverity')] = 0;
			map[this.localize('investigationByType')] = 1;
			map[this.localize('investigationByTeamMember')] = 3;
			map[this.localize('investigationByStatus')] = 4;

			var rec = this.filters.up('form').updateRecord().getRecord();
			var items = [{
				xtye: 'chartfiltercombo',
				filterOn: 'severity',
				name: 'severityFilter',
				store: this.severityFilterStore,
				value: rec.get('severityFilter')
			}, {
				xtye: 'chartfiltercombo',
				filterOn: 'type',
				name: 'typeFilter',
				store: this.typeFilterStore,
				value: rec.get('typeFilter')
			}, {
				xtye: 'chartfiltercombo',
				filterOn: 'owner',
				name: 'ownerFilter',
				store: this.ownerFilterStore,
				value: rec.get('ownerFilter')
			},{
				xtye: 'chartfiltercombo',
				filterOn: 'team',
				name: 'teamFilter',
				store: this.teamFilterStore,
				value: rec.get('teamFilter')
			},{
				xtye: 'chartfiltercombo',
				filterOn: 'status',
				name: 'statusFilter',
				store: this.statusFilterStore,
				value: rec.get('statusFilter')
			}];
			this.filters.removeAll(true);
			// This is to have what's selected being shown first
			this.filters.add(map[drivenBy]? items.splice(map[drivenBy], 1).concat(items): items);
		},

		loadForm: function(form) {
			this.filters = form.down('#filtersSet');
			this.ajax({
				url: '/resolve/service/playbook/securityIncident/getSecurityGroup',
				scope: this,
				success: function(resp) {
					var respData = RS.common.parsePayload(resp)
					if (respData.success) {
						var grpRecs = respData.records;
						this.ajax({
							url: '/resolve/service/playbook/getsirtypesinscope',
							scope: this,
							params: {
								scopeUnits: parseInt(this.chartConfigs.scope),
								unitType: this.chartConfigs.scopeUnit
							},
							success: function(resp) {
								var response = RS.common.parsePayload(resp)
								if (response.success) {
									// Dynamically load data for 'owner/team/type' filter
									this.ownerFilterStore.loadData(grpRecs.map(function(r) {return {owner: r.userId};}), true); // true for appending
									this.teamFilterStore.loadData(grpRecs.map(function(r) {return {team: r.userId};}), true);   // true for appending
									this.typeFilterStore.loadData(response.records, true);  // true for appending

									var record = new RS.incident.model.ChartSettings(this.chartConfigs);
									this.setScopeUnitTips(record.get('scopeUnit'));
									this.form = form;
									this.loadRecord(record);
								} else {
									clientVM.displayError(this.localize('failedLoadingSecurityTypes'));
								}
							},
							failure: function(r) {
								clientVM.displayFailure(r);
							}
						});
					} else {
						clientVM.displayError(this.localize('failedLoadingSecurityUserGroupAndTypes'));
					}
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			});
		},

		loadFiltersStore: function() {
			this.chartTypeFilterStore.loadData([
				{chartType: this.localize('investigationByType')},
				{chartType: this.localize('investigationBySeverity')},
				{chartType: this.localize('investigationByAge')},
				{chartType: this.localize('investigationByTeamMember')},
				// These types will be supported shortly
				//,{chartType: this.localize('investigationByOwner')},
				{chartType: this.localize('investigationByStatus')},
				//{chartType: this.localize('investigationByPhases')},
				{chartType: this.localize('investigationOverTimes')},
				{chartType: this.localize('investigationBySLA')},
				{chartType: this.localize('investigationByWorkload')}
			]);
			this.scopeUnitFilterStore.loadData([
				{scopeUnit: this.localize('days')},
				{scopeUnit: this.localize('hours')},
				{scopeUnit: this.localize('minutes')}
			]);
			this.typeFilterStore.loadData([
				{type: this.localize('all')}
			]);
			this.severityFilterStore.loadData([
				{severity: this.localize('all')},
				{severity: this.localize('critical')},
				{severity: this.localize('high')},
				{severity: this.localize('medium')},
				{severity: this.localize('low')}
			]);
			this.ownerFilterStore.loadData([
				{owner: this.localize('all')}
			]);
			this.teamFilterStore.loadData([
				{team: this.localize('all')}
			]);
			this.statusFilterStore.loadData([
				{status: this.localize('all')},
				{status: this.localize('open')},
				{status: this.localize('inProgress')},
				{status: this.localize('closed')}
			]);
			this.timeframeUnitFilterStore.loadData([
				{timeframeUnit: this.localize('hours')},
				{timeframeUnit: this.localize('days')},
				{timeframeUnit: this.localize('weeks')}
			]);
		},

		reportTypeChanged: function() {
			this.addFilters(this.chartType);
			this.set('hideScopeConfig', true);
			this.set('hideAgeBucketConfig', true);
			this.set('hideColumnConfig', true);
			if (this.chartType == this.localize('investigationByType')) {
				this.set('hideScopeConfig', false);
			} else if (this.chartType == this.localize('investigationBySeverity')) {
				this.set('hideScopeConfig', false);
			} else if (this.chartType == this.localize('investigationByAge')) {
				this.set('hideAgeBucketConfig', false);
			} else if (this.chartType == this.localize('investigationOverTimes')) {
				this.set('hideScopeConfig', false);
				this.set('hideColumnConfig', false);
			} else if (this.chartType == this.localize('investigationByTeamMember')) {
				this.set('hideScopeConfig', false);
			} else if (this.chartType == this.localize('investigationBySLA')) {
				this.set('hideScopeConfig', false);
			} else if (this.chartType == this.localize('investigationByWorkload')) {
				this.set('hideScopeConfig', false);
			} else if (this.chartType == this.localize('investigationByOwner')) {
				// Will support later
			} else if (this.chartType == this.localize('investigationByStatus')) {
				this.set('hideScopeConfig', false);
			} else if (this.chartType == this.localize('investigationByPhases')) {
				// Will support later
			}
		},

		init: function() {
			this.loadFiltersStore();
			this.filtersSet = [{
				xtye: 'chartfiltercombo',
				filterOn: 'severity',
				name: 'severityFilter',
				store: this.severityFilterStore
			}, {
				xtye: 'chartfiltercombo',
				filterOn: 'type',
				name: 'typeFilter',
				store: this.typeFilterStore
			}, {
				xtye: 'chartfiltercombo',
				filterOn: 'owner',
				name: 'ownerFilter',
				store: this.ownerFilterStore
			},{
				xtye: 'chartfiltercombo',
				filterOn: 'team',
				name: 'teamFilter',
				store: this.teamFilterStore
			},{
				xtye: 'chartfiltercombo',
				filterOn: 'status',
				name: 'statusFilter',
				store: this.statusFilterStore
			}];
		},

		agingBucketIsValid: true,
		agingBucketValidityChange: function(isValid) {
			this.set('agingBucketIsValid', isValid);
		},

		numberOfColsIsValid$: function() {
			return (this.numberOfCols && this.numberOfCols <= this.maxNColumn && this.minNColumn <= this.numberOfCols)
				? true
				: this.localize('inValidNumberOfCols');
		},

		applyIsEnabled$: function() {
			var isValid = function(val) {
				return ((Array.isArray(val) && val.length > 0) || (typeof(val) == 'string') && val);
			};

			return (this.dirty && this.scope
					&& this.scope <= this.maxScope
					&& isValid(this.typeFilter)
					&& isValid(this.severityFilter)
					&& isValid(this.ownerFilter)
					&& isValid(this.teamFilter)
					&& isValid(this.statusFilter)
					&& this.agingBucketIsValid
					&& this.numberOfColsIsValid === true);
		},
		dirtyChange: function(dirty) {
			this.set('dirty', dirty);
		},
		getConfigs: function() {
			if (!this.form) {
				return new RS.incident.model.ChartSettings();
			}
			var rec = this.form.updateRecord().getRecord();
			rec.set('ageBuckets', this.form.down('agebucketbuilder').getData());
			return rec;
		},

		save: function() {
			var rec = this.getConfigs();
			rec.save();
			return rec;
		},

		apply: function() {
			var rec = this.save();
			this.dumper(rec.getData());
			this.close();
		},
		cancel: function() {
			this.close();
		},

		loadRecord: function(record) {
			if (this.form) {
				this.form.loadRecord(record);
				this.form.down('agebucketbuilder').loadData(record.getData()['ageBuckets']);
				this.set('formLoaded', !this.formLoaded);
			}
		},

		refresh: function(record) {
			this.loadRecord(record);
		},

		alignPosition: '',
		calculatePosition: function() {
			var hIndex = this.parentVM.parentVM.viewSectionEntries.indexOf(this.parentVM);
			var vIndex = this.parentVM.parentVM.parentVM.viewSections.indexOf(this.parentVM.parentVM);
			var onLeftSide = (hIndex == 0);
			var onRightSide = (hIndex == this.parentVM.parentVM.viewSectionEntries.length - 1);
			var atBottom = (vIndex == this.parentVM.parentVM.parentVM.viewSections.length - 1);
			var onTop = (vIndex == 0);
			if (onLeftSide && atBottom && !onTop && !onRightSide) {
				this.set('alignPosition', 'bl-tl'); // bottom-left of this with bottom-left of other
			} else if (onLeftSide && !onRightSide) {
				this.set('alignPosition', 'tl-tl'); // top-left of this with top-left of other
			} else if (atBottom && !onTop) {
				this.set('alignPosition', 'br-tr'); // top-left of this with top-left of other
			} else {
				this.set('alignPosition', 'tr-tr'); // top-right of this with top-right of other
			}
		}

	}, filterStores);
}());
