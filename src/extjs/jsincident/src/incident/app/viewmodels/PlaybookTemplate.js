glu.defModel('RS.incident.PlaybookTemplate', function() {
	var documentStore = {
		mtype: 'store',
		pageSize: 50,
		model: 'RS.incident.model.Runbook',
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wiki/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	};

	return {
		mixins: ['WikiMainModel'], // A playbook template is a specialized wiki doc
		mtype: 'datamodel',        // A wiki doc is a datamodel and must be specified if used as mixins
		templateType: 'nist',
		preDefinedPhases: {
			nist: [
				{phase: 'Preparation'},
				{phase: 'Detection and Analysis'},
				{phase: 'Containment, Eradication, and Recovery'},
				{phase: 'Post-Incident Activity'}
			]
		},
		knownPhases: {
			mtype: 'store',
			model: 'RS.incident.model.Phase',
			proxy: {
				type: 'memory'
			}
		},
		runbookStore: documentStore,
		documentStore: documentStore,
		procedurePhases: {children: []},
		procedures: {
			mtype: 'treestore',
			model: 'RS.incident.model.InvestigativeProcedure',
			proxy: {
				type: 'memory'
			},
			root: {
				text: '.',
				children: []
			},
			folderSort: true
		},
		hideImgComZoom: true,
		phase: '',
		activityName: '',
		wikiName: '',
		documentName: '',
		wikiNameIsValidated: false,
		dirtyFlag: true,
		wait: false,
		sir : true,
		pSirPlaybookTemplatesTemplateBuilderChange: true,
		csrftoken: '',

		applyRBAC: function() {
			var permissions = clientVM.user.permissions || {};

			this.set('pSirPlaybookTemplatesTemplateBuilderChange', permissions['sir.playbookTemplates.templateBuilder.change'] === true);
			this.set('saveAsIsEnabled', permissions['sir.playbookTemplates.templateBuilder.create']);
		},

		handleInsufficientPermission: function() {
			this.message({
				title: this.localize('insufficientPermissionMsgTitle'),
				msg: this.localize('insufficientPermissionMsg'),
				closable : false,
				buttonText: {
					ok: this.localize('close')
				},
				scope: this,
				fn: function(btn) {
					clientVM.handleNavigationBack();
				}
			});
		},

		init: function() {
			this.initToken();
			this.applyRBAC();
			if (!this.pSirPlaybookTemplatesTemplateBuilderChange) {
				this.handleInsufficientPermission();
				return;
			}
			this.knownPhases.loadData(this.preDefinedPhases[this.templateType]);
			this.runbookStore.on('load', function(store) {
				if (this.ignoreLoadEvent) {
					this.ignoreLoadEvent = false;
				} else {
					var r = store.findRecord('ufullname', this.wikiName, 0, false, false, true);
					this.set('wikiNameIsValidated', r? true: false);
				}
			}, this);
			this.wikiMainInit.apply(this, arguments);
			var me = this,
				wikiMainSectionsLayoutPopulateSection = this.sectionsLayout.populateSection;

			Ext.apply(this.sectionsLayout, {
				populateSection: function(section) {
					var match = section.match(/type=[A-Za-z]+/i);
					var sectionName = match[0].split('=')[1];
					if (match && sectionName == 'procedure') {
						var components = me.parseSection(section);
						me.insertToProcedure(Ext.decode(components));
					}
					else if (match && sectionName == 'result') {
						var components = me.parseSection(section);
						me.parseAutomationFilterSection(components);
					} else if (match && sectionName == 'sla') {
						var sla = me.parseSection(section);
						me.parseSlaSection(sla);
					}else {
						wikiMainSectionsLayoutPopulateSection.apply(this, arguments);
					}
				}
			});
			this.procedures.on('datachanged', function(store) {
				this.handleDirtyFlag();
			}, this);
			this.procedures.on('update', function(store, record) {
				this.handleDirtyFlag();
			}, this);
			this.procedures.on('move', function(record) {
				this.handleDirtyFlag();
			}, this);
			this.procedures.on('remove', function(n, record) {
				this.handleDirtyFlag();
				this.activityRemoved({
					days:  record.get('days'),
					hours: record.get('hours'),
					minutes: record.get('minutes')
				});
			}, this);
			this.on('pageLoaded', function() {
				this.initOriginalContent();
			}, this);
		},

		validateEditProcedure: function(context, value) {
			if (!context.record.isLeaf()) {
				context.dupPhaseFound  = false;
				context.grid.store.getRootNode().eachChild(function(n) {
					if (n != context.record && n.get('activityName') == value) {
						context.dupPhaseFound = true;
					}
				}, this);
			}
		},

		automationFilterChangeHandler: function(dirty) {
			this.set('dirtyFlag', dirty)
		},

		asynCall: function(func) {
			var me = this;
			setTimeout(func.bind(me), 100);
		},

		initOriginalContent: function() {
			var me = this;
			// getting content asynchronously makes the UI more responsive
			this.asynCall(function() {
				me.set('dirtyFlag', me.id.trim()? false: true);
				me.originalContent = me.getPageContent();
			});
		},

		handleDirtyFlag: function() {
			var me = this;
			// getting content asynchronously makes the UI more responsive
			this.asynCall(function() {
				var newContent = me.getPageContent();
				if (newContent == me.originalContent) {
					me.set('dirtyFlag', false);
				} else {
					me.set('dirtyFlag', true);
				}
			});
		},

		getAddedPhases: function() {
			var phases = [];
			this.procedures.getRootNode().childNodes.forEach(function(n) {
				var phase = n.get('activityName');
				var phaseIsPredefined = false;
				this.preDefinedPhases[this.templateType].forEach(function(preDefined) {
					if (preDefined.phase == phase) {
						phaseIsPredefined = true;
					}
				}, this);
				phaseIsPredefined? '': phases.push({phase: phase});
			}, this);
			return phases;
		},

		populatePhases: function(v) {
			this.knownPhases.removeAll(true); // true not prevent collapsing the combo
			this.knownPhases.loadData(this.preDefinedPhases[this.templateType]);
			this.knownPhases.add(this.getAddedPhases());
		},

		parseSection: function(section) {
			var components = [],
				c = /\{section[^\}]*\}\n([\s\S]*?)\n\{section\}/gi.exec(section);
			if (Ext.isArray(c)) {
				c.splice(0, 1);
				components = c.join('');
			}
			return components;
		},

		parseAutomationFilterSection : function(section){
			this.automationFilter.parseAutomationFilterSection(section);
		},

		//Lock this document once data is ready
		when_id_is_loaded : {
			on : ['idChanged'],
			action : function(){
				if(this.id && !this.softLock)
					this.setSoftLocked(true);
			}
		},
		activate: function(screen, params) {
			var me = this;
			this.initToken();
			this.applyRBAC();
			if (!this.pSirPlaybookTemplatesTemplateBuilderChange) {
				this.handleInsufficientPermission();
				return;
			}
			this.clearProcedure();
			this.wikiMainActivate.apply(this, arguments);
			if (params.type) {
				this.set('templateType', params.type);
				this.set('procedurePhases', {children: this.preDefinedPhases[params.type].map(function(phase) {
					return {
						activityName: phase.phase,
						expanded: true,
						leaf: false
					}
				})});
			};
			if (params.new) {
				this.procedures.setRootNode(this.procedurePhases);
				this.sectionsLayout.defaultComType = 'wysiwyg';
				this.sectionsLayout.on('newComponentAdded', function() {
					delete me.sectionsLayout.defaultComType;
				});
			}
		},

		initToken: function() {
			this.set('csrftoken', clientVM.rsclientToken);
		},

		createRecord: function(record) {
			var fields = record.getUfields(),
				obj = {};
			for (var i=0; i<fields.length; i++) {
				obj[fields[i]] = record.get(fields[i]);
			}
			return obj;
		},

		addNode: function(node) {
			var rootNode = this.procedures.getRootNode(),
				insertInto = rootNode.findChild('activityName', node.phase);

			if (!insertInto) {
				insertInto = rootNode.createNode({
					activityName: node.phase,
					expanded: true,
					leaf: false
				});
				rootNode.insertChild(rootNode.childNodes.length, insertInto);
			}
			if (node.activityName && node.wikiName) {
				var rec = this.createRecord(new this.procedures.model(node));
				rec.leaf = true;
				insertInto.insertChild(insertInto.childNodes.length, insertInto.createNode(rec));
			}
			this.activityAdded({
				days: node.days,
				hours: node.hours,
				minutes: node.minutes
			});
		},

		addActivity: function(fieldValues) {
			var form = this.getActivityForm().getForm();
			this.addNode(form.getFieldValues());
			form.reset();
		},

		removeActivity: function( record) {
			if(record.isLeaf()) {
				record.parentNode.removeChild(record);
			} else if (record.hasChildNodes()){
				this.message({
					title: this.localize('deletePhaseTitle'),
					msg: this.localize('deletePhaseBody'),
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						yes: this.localize('yes'),
						no: this.localize('cancel')
					},
					scope: this,
					fn: function(btn) {
						if (btn == "yes") {
							this.procedures.getRootNode().removeChild(record);
						}
					}
				})
			} else {
				this.procedures.getRootNode().removeChild(record);
			}
		},

		searchRunbook: function(v, inline) {
			var me = this;
			this.open({
				mtype: 'RS.decisiontree.DocumentPicker',
				dumper: {
					dump: function(selections) {
						if (selections.length > 0) {
							var wikiName = selections[0].get('ufullname');
							if (inline) {
								// Set runbook name to the cell
								v.up('#activityGrid').editingContext.record.set('wikiName', wikiName);
							} else {
								// Set runbook name to the form field
								me.set('wikiName',wikiName);
								me.ignoreLoadEvent = true;
								me.runbookStore.load();
								me.set('wikiNameIsValidated', true);
							}
						}
					}
				}
			})
		},

		handleRunbookSelect: function() {
			this.set('wikiNameIsValidated', true);
		},

		validateWikiName: function(v) {
			var r = v.store.findRecord('ufullname', v.getValue(), 0, false, false, true);
			this.set('wikiNameIsValidated', r? true: false);
		},

		getContent: function() {
			return this.serializeProcedure();
		},

		clearProcedure: function() {
			if(this.procedures.getRootNode().childNodes.length) {
				this.procedures.getRootNode().removeAll();
			}
		},

		insertToProcedure: function(data) {
			this.clearProcedure();
			for(var i=0; i<data.length; i++) {
				this.addNode(this.createRecord(new this.procedures.model(data[i])));
			}
		},

		serializeProcedure: function() {
			var me = this,
				sData = [];
			// Flatten out tree store as a normal store
			this.procedures.getRootNode().eachChild( function(n){
				var phase = n.get('activityName');
				n.eachChild(function(r) {
					var record = me.createRecord(r);
					record.phase = phase;
					sData.push(record);
				});
				if (!n.childNodes.length) {
					sData.push({
						phase: phase
					});
				}
			});
			return Ext.encode(sData);
		},

		getProcedureSectionContent: function() {
			return '{section:type=procedure|title=Untitled Section' + '}\n' + this.getContent() + '\n{section}'
		},

		maxSla: 0,
		templateSla: 0,

		activitySlaWarning:  false,
		templateSlaWarning: false,

		activityAdded: function(sla) {
			this.maxSla = Math.max(this.maxSla, this.slaToHours(sla) + Math.ceil(parseInt(sla.minutes ||0)/60));
		},

		updateMaxSlas: function(node) {
			var me = this;
			if (node.isLeaf()) {
				this.maxSla = Math.max(this.maxSla, this.slaToMinutes({
					days: node.get('days'),
					hours: node.get('hours')}));
			}
			(node.childNodes || []).forEach(function(childNode) {
				me.updateMaxSlas(childNode);
			});
		},

		activityRemoved: function(sla) {
			if (this.slaToMinutes(sla) >= this.maxSla) {
				this.maxSla = 0;
				this.updateMaxSlas(this.procedures.getRootNode());
			}
		},

		slaToHours: function(sla) {
			return parseInt(sla.days||0)*24*60 + parseInt(sla.hours||0)*60;
		},

		slaToMinutes: function(sla) {
			return this.slaToHours(sla) + parseInt(sla.minutes||0);
		},

		convertMinutesToDaysHours: function(nMinutes) {
			return {
				days: Math.floor(nMinutes/1440),
				hours: Math.ceil((nMinutes%1440)/60)
			};
		},

		templateSlaChanged: function(sla) {
			var slaInMinutes = this.slaToMinutes(sla);
			// No validation is required if sla value is undefined (0)
			if (slaInMinutes && slaInMinutes < this.maxSla) {
				var slaInDaysHours = this.convertMinutesToDaysHours(this.maxSla);
				var msg = Ext.String.format('<font color="red"><i>SLA can not be less than {0} days and {1} hours </i></font>',
					slaInDaysHours.days, slaInDaysHours.hours);
				this.set('templateSlaWarning', msg);
			} else {
				this.templateSla = slaInMinutes;
				this.set('templateSlaWarning', false);
				this.updateActivitySlaWarningMsg();
			}
			this.handleDirtyFlag();
		},

		updateActivitySlaWarningMsg: function() {
			var slaForm = Ext.ComponentQuery.query('rsnumberfield[itemId=activitySla]')[0];
			var sla = slaForm.up().getForm().getFieldValues();
			this.activitySlaChanged(sla);
		},

		activitySlaChanged: function(sla) {
			// No validation is required if sla value is undefined (0)
			if (this.templateSla && this.slaToMinutes(sla) > this.templateSla) {
				var slaInDaysHours = this.convertMinutesToDaysHours(this.templateSla);
				var msg = Ext.String.format('<font color="red"><i>SLA must not exceed {0} days and {1} hours </i></font>',
					slaInDaysHours.days, slaInDaysHours.hours);
				this.set('activitySlaWarning', msg);
			} else {
				this.set('activitySlaWarning', false);
			}
		},

		parseSlaSection: function(sla) {
			var slaForm = Ext.ComponentQuery.query('form[itemId=sla]')[0];
			var slaRaw = Ext.decode(sla);
			this.templateSla = this.slaToMinutes(slaRaw);
			slaForm.getForm().setValues(slaRaw);
		},

		getSLAContent: function() {
			var slaForm = Ext.ComponentQuery.query('form[itemId=sla]')[0];
			var sla = slaForm.getForm().getFieldValues();
			return '{section:type=sla|title=Untitled Section' + '}\n' + Ext.encode(sla) + '\n{section}'
		},

		getPageContent: function() {
			var automationFilter = this.automationFilter.generateContent();
			return this.wikiMaingetPageContent.apply(this, arguments) + '\n' + this.getSLAContent()
				+ '\n' + this.getProcedureSectionContent() + '\n' + automationFilter;
		},

		getView: function(viewType, viewItemId) {
			if (!this[viewItemId]) {
				var sel = Ext.String.format('{0}[itemId={1}]', viewType, viewItemId);
				this[viewItemId] = Ext.ComponentQuery.query(sel)[0];
			}
			return this[viewItemId];
		},

		getActivityForm: function() {
			return this.getView('form', 'activityForm');
		},

		getGrid: function() {
			return this.getView('treepanel', 'activityGrid');
		},

		// Overide mixin
		checkForDirtyFlag: function() {
			this.handleDirtyFlag();
		},

		reloadContent: function(btn) {
			this.wikiMainReloadContent.apply(this, arguments);
			if(btn == 'yes') {
				this.sectionsLayout.parseSection(this.ucontent);
			}
		},

		preview: function() {
			this.open({
				mtype: 'RS.incident.PlaybookTemplatePreview'
			});
		},

		exitEdit: function() {
			clientVM.handleNavigation({
				modelName: 'RS.incident.PlaybookTemplateRead',
				params: {
					name: this.fullName,
					uname: this.uname,
					unamespace: this.unamespace,
					id: this.id
				}
			});
		},

		returnToTemplates: function() {
			clientVM.handleNavigation({
				modelName: 'RS.incident.PlaybookTemplates'
			});
		},

		// Formulas to control the buttons visibility & disability. Currently we want all of them visible and enable
		phaseIsValid$ : function(){
			return this.phase && RS.common.validation.PlainText.test(this.phase) ? true : this.localize('invalidPhase');
		},
		activityNameIsValid$ : function(){
			return this.activityName && RS.common.validation.PlainText.test(this.activityName) ? true : this.localize('invalidActivityName');
		},
		wikiNameIsValid$: function() {
			return !!this.wikiName && !!this.wikiName.length && this.wikiNameIsValidated;
		},

		addActivityIsEnabled$: function() {
			return this.isValid && !this.activitySlaWarning;
		},
		saveIsEnabled$: function() {
			return !this.templateSlaWarning && this.dirtyFlag;
		},
		saveAsIsEnabled: true,
		saveIsVisible$: function() {
			return true;
		},

		saveAndCommitIsVisible$: function() {
			return true;
		},

		addLayoutSectionIsVisible$: function() {
			return true;
		},

		sourceIsVisible$: function() {
			return true;
		},

		reloadIsVisible$: function() {
			return true;
		},

		lockedDisplayText$: function() {
			return this.softLock && this.lockedByUsername != user.name ? this.localize('lockedBy', [this.lockedByUsername]) : '';
		},

		automationFilter : {
			mtype : 'RS.incident.PlaybookTemplate.AutomationFilter'
		},

		beforeDestroyComponent: function() {
			//Remove this handler from document's event.
			this.fireEvent('removeEventReference');
		}
	}
}());

glu.defModel('RS.incident.PlaybookTemplate.AutomationFilter',{
	fields : [
		//Fields without UI
		'descriptionWidth', 'autoCollapse', 'collapsed', 'autoHide', 'progress',
		//Fields with UI
		'refreshInterval','refreshCountMax', 'filterValue','order',{
			name : 'encodeSummary',
			type : 'boolean'
		},{
			name : 'selectAll',
			type : 'boolean'
		},{
			name : 'includeStartEnd',
			type : 'boolean'
		},{
			name : 'preserveTaskOrder',
			type : 'boolean'
		}],
	defaultData : {
		refreshInterval: 5,
		refreshCountMax: 60,
		descriptionWidth: 0,
		autoCollapse: false,
		collapsed : false,
		autoHide : false,
		progress : false,
		order: 'DESC',
		selectAll : true,
		includeStartEnd: false,
		encodeSummary: false,
		preserveTaskOrder: true,
	},
	filterStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	orderStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	when_filters_changed: {
		on: ['filterValueChanged',
			'orderChanged',
			'encodeSummaryChanged',
			'selectAllChanged',
			'includeStartEndChanged',
			'preserveTaskOrderChanged',
			'refreshIntervalChanged',
			'refreshCountMaxChanged'],
		action : function(){
			this.rootVM.handleDirtyFlag();
		}
	},
	init : function(){
		this.filterStore.loadData([{
			name: this.localize('GOOD'),
			value: 'GOOD'
		}, {
			name: this.localize('WARNING'),
			value: 'WARNING'
		}, {
			name: this.localize('SEVERE'),
			value: 'SEVERE'
		}, {
			name: this.localize('CRITICAL'),
			value: 'CRITICAL'
		}])

		this.orderStore.loadData([{
			name: this.localize('ASC'),
			value: 'ASC'
		}, {
			name: this.localize('DESC'),
			value: 'DESC'
		}])
		this.loadData(this.defaultData);
		this.actionTasks.on('datachanged', this.rootVM.handleDirtyFlag, this.rootVM);
		this.actionTasks.on('update', this.rootVM.handleDirtyFlag, this.rootVM)
	},
	serializeAT: function(node) {
		var ats = [],
			data = Ext.clone(node.data),
			ser = [],
			split = data.actionTask.split('#');

		data.task = split[0]
		data.namespace = split[1]

		Ext.Object.each(data, function(key, value) {
			if (Ext.Array.indexOf(['description', 'descriptionWikiLink', 'namespace', 'nodeId', 'resultWikiLink', 'task', 'wiki', 'autoCollapse', 'tags'], key) > -1 && value)
				ser.push(Ext.String.format('{0}={1}', key, value))
		})

		if (!node.isLeaf()) { //node.childNodes && node.childNodes.length > 0) {
			ats.push('{group:title=' + node.get('description') + (!node.get('autoCollapse') ? '|autoCollapse=' + node.get('autoCollapse') : '') + '}')
			node.eachChild(function(child) {
				ats = ats.concat(this.serializeAT(child))
			}, this)
			ats.push('{group}')
		} else {
			ats.push('{' + ser.join('|') + '}')
		}

		return ats
	},
	generateContent: function() {
		var actionTasks = [],
			tags = [];
		this.actionTasks.getRootNode().eachChild(function(child) {
			actionTasks = actionTasks.concat(this.serializeAT(child))
		}, this)
		return '{section:type=result|title=Untitled Section' + '}\n' + '{result2:title=' + this.title + '|encodeSummary=' + this.encodeSummary + '|refreshInterval=' + this.refreshInterval + '|refreshCountMax=' + this.refreshCountMax + '|descriptionWidth=' + this.descriptionWidth + '|order=' + this.order + '|autoCollapse=' + this.autoCollapse + '|collapsed=' + this.collapsed + '|autoHide=' + this.autoHide + '|filter=' + (this.filterValue || '') + '|progress=' + this.progress + '|selectAll=' + this.selectAll + '|includeStartEnd=' + this.includeStartEnd + '|preserveTaskOrder=' + this.preserveTaskOrder + '}\n' + actionTasks.join('\n') + '\n{result2}' + '\n{section}';
	},
	parseAutomationFilterSection : function(section){
		//Parse Properties first
		this.parseProperties(section);

		//COMPATIBILITY : parse action tasks from content. Make sure task summary (which will be used as description) is in 1 line.
		var content = section.replace(/([\s\S]*)({description=[^{]*})([\s\S]*)/g, function(v,g1,g2,g3){
			return g1 + g2.replace(/\n/g, ' ') + g3;
		});
		var	contents = (content|| '').split('\n');

		//Remove first and last containing {result2} tags
		contents.splice(contents.length - 1, 1)
		contents.splice(0, 1);

		//Parse this actiontask list.
		var processedActionTasks = processActionTasks(contents);
		this.actionTasks.getRootNode().removeAll();
		Ext.Array.forEach(processedActionTasks, function(at) {
			this.actionTasks.getRootNode().appendChild(at)
		}, this)
	},
	parseProperties: function(content) {
		var configurationLines = (content || '').split('\n'),
			properties = {};

		//Parse porperties from content
		var props = this.extractComponentProperties(configurationLines);
		for (var j = 0; j < props.length; j++) {
			var split = props[j].split('=');

			if (split.length > 1) {
				properties[split[0]] = split.slice(1).join('=');
			} else  {
				properties['text'] = split[0];
			}
		}

		//Conversion
		var filterParams = {};
		filterParams.refreshInterval = isNaN(+properties.refreshInterval) ? 5 : +properties.refreshInterval;
		filterParams.refreshCountMax = isNaN(+properties.refreshCountMax) ? 60 : +properties.refreshCountMax;
		filterParams.descriptionWidth = isNaN(+properties.descriptionWidth) ? 0 : +properties.descriptionWidth;
		filterParams.filterValue = (properties.filter || '').split(',');

		if (properties.encodeSummary) filterParams.encodeSummary = properties.encodeSummary === 'true';
		if (properties.order) filterParams.order = properties.order.toLowerCase() == 'asc' ? 'ASC' : 'DESC';
		if (properties.autoCollapse) filterParams.autoCollapse = properties.autoCollapse === 'true';
		if (properties.collapsed) filterParams.collapsed = properties.collapsed === 'true';
		if (properties.autoHide) filterParams.autoHide = properties.autoHide === 'true';
		if (properties.progress) filterParams.progress = properties.progress === 'true';
		if (properties.selectAll) filterParams.selectAll = properties.selectAll === 'true';
		if (properties.includeStartEnd) filterParams.includeStartEnd = properties.includeStartEnd === 'true';
		if (properties.preserveTaskOrder) filterParams.preserveTaskOrder = properties.preserveTaskOrder === 'true';

		this.loadData(filterParams);
	},
	extractComponentProperties: function (configurationLines) {
		var props = [];
		if (configurationLines.length > 0) {
			var c = configurationLines.splice(0, 1)[0];
			var colonIndex = c.indexOf(':');
			if(colonIndex > -1) {
				var parenIndex = c.indexOf('}');
				var endIndex = c.length - 1;
				if(parenIndex > -1) {
					endIndex = parenIndex;
				}
				props = c.substring(colonIndex + 1, endIndex).split('|');
			}
		}
		return props;
	},

	//Action Task Filter
	actionTasksSelections: [],
	actionTasks: {
		mtype: 'treestore',
		fields: ['id', 'description', 'descriptionWikiLink', 'namespace', 'nodeId', 'resultWikiLink', 'task', 'wiki', 'actionTask', 'tags', {
			name: 'autoCollapse',
			type: 'boolean'
		}],
		proxy: {
			type: 'memory'
		}
	},
	selectionchange: function(records) {
		this.set('actionTasksSelections', records);
	},
	addGroup: function() {
		if (this.actionTasksSelections.length > 0 && !this.actionTasksSelections[0].isLeaf()) {
			this.actionTasksSelections[0].appendChild({
				description: this.localize('newGroup'),
				expanded: true
			})
		} else {
			this.actionTasks.getRootNode().appendChild({
				description: this.localize('newGroup'),
				expanded: true
			})
		}
	},
	addActionTask: function() {
		var currentDocName = this.rootVM.fullName;
		this.open({
			mtype: 'RS.actiontask.ActionTaskPicker',
			multiSelection : true,
			currentDocName : currentDocName,
			dumper: {
				scope: this,
				dump: this.reallyAddActionTask
			}
		})
	},
	reallyAddActionTask: function(selections) {
		//Parse summary txt to make it 1 line description.
		Ext.Array.each(selections,function(selection){
			var rawTaskSummary = selection.get('usummary');
			var parsedSummary = rawTaskSummary ? rawTaskSummary.replace(/\n/g, '. ') : '';
			if (this.actionTasksSelections.length > 0 && !this.actionTasksSelections[0].isLeaf()) {
				this.actionTasksSelections[0].appendChild({
					leaf: true,
					actionTask: selection.get('uname') + '#' + selection.get('unamespace'),
					description: parsedSummary,
					task: selection.get('uname'),
					namespace: selection.get('unamespace')
				})
			} else {
				this.actionTasks.getRootNode().appendChild({
					leaf: true,
					actionTask: selection.get('uname') + '#' + selection.get('unamespace'),
					description: parsedSummary,
					task: selection.get('uname'),
					namespace: selection.get('unamespace')
				})
			}
		},this)
	},
	edit: function() {
		this.open(Ext.applyIf({
			mtype: 'RS.wiki.pagebuilder.TaskEdit'
		}, this.actionTasksSelections[0].data))
	},
	remove: function() {
		Ext.each(this.actionTasksSelections, function(t) {
			t.remove();
		}, this);
	},
});
