glu.defModel('RS.incident.ArtifactConfigEntry',{
	API : {
		removeType : '/resolve/service/at/',
		removeAliasFromType : '/resolve/service/at/',
		removeActionFromType : '/resolve/service/ac/'
	},
	isCustomType : false,
	artifactType : '',
	aliasKeys : [],
	actions : [],	
	aliasStore : {
		mtype : 'store',
		fields : ['ushortName', 'ufullName','uartifactTypeSource'],
		sorters : [{
			property : 'ushortName',
			transform : function(val){
				return val.toLowerCase();
			},
			direction : 'ASC'
		}]
	},
	aliasKeysSelections : [],
	init : function(){
		this.aliasStore.loadData(this.aliasKeys);
		this.actionStore.loadData(this.actions);
	},
	removeType : function(){
		//Might need to manual delete actions as well
		this.message({
			title : this.localize('comfirmRemoveTypeTitle'),
			msg : this.localize('comfirmRemoveTypeBody',[this.artifactType]),		
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirm'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.ajax({
					url : this.API['removeType'] + this.artifactType,
					method : 'DELETE',
					success : function(resp){
						var response = RS.common.parsePayload(resp);
						if(response.success){
							this.parentVM.removeType(this);
						}
						else
							clientVM.displayError(response.message);
					},
					failure : function(resp){
						clientVM.displayFailure(resp);
					}
				})
			}
		})		
	},
	addAlias : function(){
		this.open({
			mtype : 'NewAliasForm',
			type : this.artifactType			
		})
	},
	addAliasToStore : function(data){
		this.aliasStore.add(data);
	},
	removeAlias : function(){
		var entry = this.aliasKeysSelections[0];
		this.message({
			title : this.localize('comfirmRemoveAliasTitle'),
			msg : this.localize('comfirmRemoveAliasBody',[entry.get('ushortName')]),		
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirm'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.ajax({
					url : this.API['removeAliasFromType'] + this.artifactType + '/remove',
					method : 'POST',
					jsonData : {
						ushortName : entry.get('ushortName'),
						uartifactTypeSource : entry.get('uartifactTypeSource')
					},
					success : function(resp){
						var response = RS.common.parsePayload(resp);
						if(response.success){
							this.aliasStore.remove(entry);
						}
						else
							clientVM.displayError(response.message);
					},
					failure : function(resp){
						clientVM.displayFailure(resp);
					}
				})
			}
		})
	},
	removeAliasIsEnabled$ : function(){
		return this.aliasKeysSelections.length > 0
	},
	actionStore : {
		mtype : 'store',	
		fields : ['task','automation','uname','param',{
			name : 'executionType',
			convert : function(val, record){
				var data = record.raw;
				if(data.task)
					return 'ACTIONTASK'
				else
					return 'AUTOMATION'
			}
		},{
			name : 'executionItem',
			convert : function(val, record){
				var data = record.raw;
				if(data.task)
					return data.task.ufullName;
				else
					return data.automation.ufullname;
			}
		}]
	},
	actionSelections : [],
	addAction : function(){
		var nameCollection = [];
		for(var i = 0; i < this.rootVM.actionData.length; i++){
			nameCollection.push(this.rootVM.actionData[i].uname);
		}
		this.open({
			mtype : 'NewActionForm',
			nameCollection : nameCollection,
			artifactType : this.artifactType,
			dumper : {
				dump : function(data){
					this.actionStore.add(data);
				},
				scope : this
			}
		})
	},
	removeAction : function(){
		var entry = this.actionSelections[0];
		this.message({
			title : this.localize('comfirmRemoveActionTitle'),
			msg : this.localize('comfirmRemoveActionBody',[entry.get('uname')]),		
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirm'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.ajax({
					url : this.API['removeActionFromType'] + entry.get('uname'),
					method : 'DELETE',					
					success : function(resp){
						var response = RS.common.parsePayload(resp);
						if(response.success){
							this.actionStore.remove(entry);
						}
						else
							clientVM.displayError(response.message);
					},
					failure : function(resp){
						clientVM.displayFailure(resp);
					}
				})
			}
		})	
	},
	removeActionIsEnabled$ : function(){
		return this.actionSelections.length > 0;
	},
	editAction : function(name){
		var record = this.actionStore.findRecord('uname', name);
		this.open({
			mtype : 'NewActionForm',
			artifactType : this.artifactType,
			edit : true,
			data : record.data,
			dumper : {
				dump : function(data){
					var r = this.actionStore.findRecord('uname', data.uname);
					this.actionStore.remove(r);
					this.actionStore.add(data);
				},
				scope : this
			}
		})
	}
})