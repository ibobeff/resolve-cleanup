glu.defModel('RS.incident.NewAliasForm',{
	type : '',	
	isCustomAlias : true,
	waiting : false,	
	fields : ['ushortName','ufullName','udescription','uartifactTypeSource'],
	sourceStore : null,
	aliasStore : {
		mtype : 'store',
		fields : ['ushortName','ufullName','udescription']
	},
	availableAlias : {
		CEF : [],
		CUSTOM : []
	},
	API : {
		saveCustomAlias : '/resolve/service/cd',
		getCEF : '/resolve/service/cef',
		getCUSTOM : '/resolve/service/cd',
		assignAliasToType : '/resolve/service/at/'
	},	
	ushortNameIsValid$ : function(){
		return this.ushortName && (this.uartifactTypeSource == 'CUSTOM' || this.isNonCustomKeyValid(this.ushortName))? true : this.localize('keyReqs');
	},
	ufullNameIsValid$ : function(){
		return this.ufullName ? true : this.localize('nameReqs');
	},
	uartifactTypeSourceIsValid$ : function(){
		return this.uartifactTypeSource ? true : this.localize('sourceReqs');
	},
	isNonCustomKeyValid : function(key){
		return this.aliasStore.find('ushortName', key, 0, false, false, true) != -1;
	},
	when_source_changed : {
		on : ['uartifactTypeSourceChanged'],
		action : function(v){		
			this.aliasStore.loadData(this.availableAlias[v]);
			this.set('isCustomAlias', v == 'CUSTOM');
			if(v != 'CUSTOM'){
				var firstAlias = this.aliasStore.getAt(0);
				this.set('ushortName', firstAlias ? firstAlias.get('ushortName') : '');				
			}
			else
				this.clearForm();
		}
	},
	when_key_changed : {
		on : ['ushortNameChanged'],
		action : function(v){
			if(this.ushortNameIsValid == true){
				var alias = this.aliasStore.findRecord('ushortName', v);
				if(alias){
					this.set('ufullName', alias.get('ufullName'));
					this.set('udescription', alias.get('udescription'));
				}				
			}
		}
	},	
	init : function(){
		var me = this;
		for(var source in this.availableAlias){
			(function(s){
				this.ajax({
					url : this.API['get' + s],
					method : 'GET',
					success : function(resp){
						var response = RS.common.parsePayload(resp);
						if(response.success){
							this.availableAlias[s] = response.data;
						}
						else
							clientVM.displayError(response.message);					
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}.bind(this))(source)		
		}
		var sourceStore = Ext.create('Ext.data.Store',{		
			fields : [{
				name : 'value',
				convert : function(v, record){
					return record.raw;
				}
			},{
				name : 'display',
				convert : function(v, record){
					return me.localize(record.raw);
				}
			}],
			data : ['CEF', 'CUSTOM']
		})
		this.set('sourceStore', sourceStore);		
	},	
	clearForm : function(){
		this.set('ushortName','');
		this.set('udescription', '');
		this.set('ufullName','');
		this.set('isPristine', true);
	},
	save : function(){
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		this.set('waiting', true);
		if(this.uartifactTypeSource == 'CUSTOM'){
			this.ajax({
				url : this.API['saveCustomAlias'],
				jsonData : {				
					ufullName : this.ufullName,
					ushortName : this.ushortName,
					udescription : ''
				},
				success : function(resp){
					var response = RS.common.parsePayload(resp);
					if(response.success){
						clientVM.displaySuccess(this.localize('keySaveSuccess'));
						this.assignAliasToType();
					}
					else
						clientVM.displayError(response.message);					
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				},
				callback : function(){
					this.set('waiting', false);
				}
			})		
		}
		else 
			this.assignAliasToType();	
	},
	saveIsEnabled$ : function(){
		return !this.waiting;
	},
	assignAliasToType : function(){
		this.ajax({
			url : this.API['assignAliasToType'] + this.type + '/add',
			jsonData : {
				ushortName : this.ushortName,
				uartifactTypeSource : this.uartifactTypeSource
			},
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success){
					this.parentVM.addAliasToStore(this.asObject());
					this.doClose();
				}
				else
					clientVM.displayError(response.message);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback : function(){
				this.set('waiting', false);
			}
		})
	},
	cancel : function(){
		this.doClose();
	}
})