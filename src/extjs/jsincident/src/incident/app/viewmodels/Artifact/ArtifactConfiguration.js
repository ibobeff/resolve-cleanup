glu.defModel('RS.incident.ArtifactConfiguration', {
	API : {	
		getAlias : '/resolve/service/at',
		getAction : '/resolve/service/ac'
	},
	configList : {
		mtype : 'list'
	},
	actionLoaded : false,
	actionData : [],
	keyLoaded :false,
	keyData : [],
	artifactTypeName: [],
	activate : function(){
		this.loadKey();	
		this.loadAction();	
	},
	when_data_ready : {
		on : ['actionLoadedChanged', 'keyLoadedChanged'],
		action : function(){
			if(this.actionLoaded && this.keyLoaded)
				this.buildConfigEntry();
		}
	},
	loadKey : function(){
		this.ajax({
			url : this.API['getAlias'],
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success){
					this.keyData = response.data;
					this.set('keyLoaded', true);
				}
				else
					clientVM.displayError(response.message);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})	
	},
	loadAction : function(){
		this.ajax({
			url : this.API['getAction'],
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success){
					this.actionData = response.data;
					this.set('actionLoaded', true);
				}
				else
					clientVM.displayError(response.message);
			},
			failure : function(resp){
				clientVM.displayFailure(resp);
			}
		})
	},
	buildConfigEntry : function(){
		this.configList.removeAll();
		var actions = {};
		for(var i = 0; i < this.actionData.length; i++){
			var entry = this.actionData[i];
			var artifactType = entry.artifactType.uname;
			if(!actions.hasOwnProperty(artifactType))
				actions[entry.artifactType.uname] = [];
			actions[entry.artifactType.uname].push(RS.common.mergeObject({
				automation : null,
				task : null,
				param : null,
				uname : null
			}, entry));
		}
		for(var i = 0; i < this.keyData.length;i++){
			var entry = this.keyData[i];
			this.configList.add(this.model({
				mtype : 'ArtifactConfigEntry',
				artifactType : entry.uname,
				isCustomType : !entry.ustandard,
				aliasKeys : entry.dictionaryItems || [],
				actions : actions[entry.uname] || []
			}))
			this.artifactTypeName.push(entry.uname.toUpperCase());
		}
	},
	newType : function(){
		this.open({
			mtype : 'NewTypeForm',	
			artifactTypeName: this.artifactTypeName,
			dumper : {
				dump : function(data){
					this.configList.add(this.model({
						mtype : 'ArtifactConfigEntry',
						artifactType : data.name,
						isCustomType : true,
						aliasKeys : [],
						actions : []					
					}));					
				},
				scope : this
			}
		})
	},
	removeType : function(entry){
		this.configList.remove(entry);
	}
})
glu.defModel('RS.incident.NewTypeForm',{
	API : {
		saveType : '/resolve/service/at'
	},
	artifactTypeName :[],
	fields : ['name'],
	nameIsValid$ : function(){
		if(this.name && this.artifactTypeName.indexOf(this.name.toUpperCase()) != -1)
			return this.localize('duplicatedType');
		return RS.common.validation.PlainText.test(this.name) ? true : this.localize('invalidName');
	},
	save : function(){
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		this.ajax({
			url : this.API['saveType'],
			jsonData : {
				uname : this.name,
				ustandard : false,
				dictionaryItems : []
			},
			method : 'POST',
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success){
					if(this.dumper){
						this.dumper.dump.apply(this.dumper.scope, [this.asObject()]);
					}
					this.doClose();
				}
				else
					clientVM.displayError(response.message);					
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	cancel : function(){
		this.doClose();
	}
})