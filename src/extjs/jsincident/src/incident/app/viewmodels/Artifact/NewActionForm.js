glu.defModel('RS.incident.NewActionForm',{
	API : {
		saveAction : '/resolve/service/ac',
		getTask : '/resolve/service/actiontask/get',
		getAutomation : '/resolve/service/wiki/get',
	},
	fields : ['id','param','uname',{
		name : 'automation',
		type : 'auto'
	},{
		name :'task',
		type : 'auto'
	},{
		name : 'automationName',
		convert : function(val, record){
			var automation = record.raw.automation;
			if(automation)
				return automation.ufullname
			else 
				return "";
		}
	},{
		name : 'taskName',
		convert : function(val, record){
			var task = record.raw.task;
			if(task)
				return task.ufullName
			else 
				return "";
		}
	},{
		name : 'actionTaskIsSelected',		
		type : 'bool',
		convert : function(val, record){
			return record.raw.executionType == 'ACTIONTASK' ? true : false;
		}
	}],
	actionTaskIsSelected : false,
	artifactType : '',
	saving : false,	
	loading : false,
	edit : false,
	nameCollection : [],
	parameterStore : {
		mtype : 'store',
		fields : ['name']
	},
	when_execution_type_switched : {
		on : ['actionTaskIsSelectedChanged'],
		action : function(val){
			if(!this.loading)
				this.set('param', '');
		}
	},
	init : function(){	
		if(this.edit && this.data){
			this.loading = true;
			this.loadData(this.data);
			this.populateParameterStore(this.actionTaskIsSelected , this.param);
			this.set('isPristine', true);
			this.loading = false;
		}
		else
			this.nameCollection = this.nameCollection.map(function(x){return x.toUpperCase();})
	},
	populateParameterStore : function(isActionTask, param){
		var parameters = [];
		this.ajax({
			url: isActionTask ? this.API['getTask'] : this.API['getAutomation'],
			params : {
				id : '',
				name : isActionTask ? this.taskName : this.automationName
			},
			scope: this,
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(!response.success){
					clientVM.displayError(response.message);				
				}
				var data = response.data;
				if(isActionTask){
					var paramList = data['resolveActionInvoc']['resolveActionParameters'];
					for(var i = 0; i < paramList.length; i++){
						if(paramList[i].utype == 'INPUT')
							parameters.push({name : paramList[i].uname});
					}
				}
				else {
					var parameterData = data['uwikiParameters'];
					parameters = parameterData ? Ext.decode(parameterData) : [];					
				}
				this.parameterStore.loadData(parameters);
				this.set('param', null);
				this.set('param', param);
			},
			failure : function(r){
				clientVM.displayFailure(r);
				this.abortExecution(true);
			}
		})
	},
	title$ : function(){
		return (this.edit ? 'Edit' : 'New') + ' Action';
	},	
	automationNameIsValid$ : function(){
		return this.actionTaskIsSelected || this.automationName != '' ? true : this.localize('automationReqs');
	},
	taskNameIsValid$ : function(){
		return !this.actionTaskIsSelected || this.taskName != '' ? true : this.localize('actiontaskReqs');
	},
	unameIsValid$ : function(){
		if(this.nameCollection.indexOf(this.uname.toUpperCase()) != -1)
			return this.localize('duplicatedName');
		else
			return RS.common.validation.PlainText.test(this.uname) ? true : this.localize('invalidName');
	},
	paramIsValid$ : function(){
		return this.parameter != '' ? true : this.localize('parameterReqs');
	},
	saveIsEnabled$ : function(){
		return !this.saving && this.isValid;
	},
	selectedAutomation : null,
	openAutomationSearch: function() {
		this.open({
			mtype : 'RS.common.ContentPicker',
			contentType : 'automation',
			dumper : {
				dump : function(selection) {
					this.parameterStore.removeAll();
					this.set('automation', selection.data);
					this.set('automationName', selection.get('ufullname'));
					var params = JSON.parse(selection.get('uwikiParameters') || '[]');
					this.set('automationNameIsValid', params.length == 0 ? this.localize('invalidAutomation') : true);
					this.parameterStore.loadData(params);
					this.set('param', params.length > 0 ? this.parameterStore.getAt(0).get('name') : '');
				},
				scope : this
			}
		})
	},
	selectedTask : null,
	openActiontaskSearch: function() {
		this.open({
			mtype : 'RS.actiontask.ActionTaskPicker',			
			dumper : {
				dump : function(selectedRecords){
					this.set('task', selectedRecords[0].data);					
					this.set('taskName', this.task['ufullName']);
					var params = this.task.resolveActionInvoc.resolveActionParameters || [];
					var inputs = [];
					for(var i = 0; i < params.length; i++){
						var p = params[i];
						if(p.utype == 'INPUT'){
							inputs.push({name : p.uname});
						}
					}
					this.parameterStore.loadData(inputs);
					this.set('taskNameIsValid', inputs.length == 0 ? this.localize('invalidActiontask') : true);
					this.set('param', inputs.length > 0 ? this.parameterStore.getAt(0).get('name') : '');
				},
				scope : this
			}
		})
	},
	save : function(){
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		this.set('saving', true);
		var payload = {
			uname : this.uname,
			artifactType : {
				uname : this.artifactType
			},				
			param : this.param
		}
		if(this.actionTaskIsSelected){
			var task = RS.common.mergeObject({	
				id : '',	
				uname : '',
				ufullName : '',
				unamespace : ''
			}, this.task)
			payload.task = task;
			payload.automation = null;
		}
		else {
			var automation = RS.common.mergeObject({
				id : '',			
				ufullname : '',
				uname : ''		
			}, this.automation)
			payload.automation = automation;
			payload.task = null;
		}
		
		this.ajax({
			url : this.API['saveAction'],
			method : 'POST',
			jsonData : payload,
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success){
					clientVM.displaySuccess(this.localize('actionSaveSuccess', [this.edit ? 'updated' : 'created']));
					if(this.dumper)			
						this.dumper.dump.apply(this.dumper.scope, [response.data]);
					this.doClose();
				}
				else 
					clientVM.displayError(response.message);
			},
			failure : function(resp){
				clientVM.displayFailure(resp);
			},
			callback : function(){
				this.set('saving', false);
			}
		})		
	},
	cancel : function(){
		this.doClose();
	}
})