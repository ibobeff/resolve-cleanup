glu.defModel('RS.incident.ArtifactAction',{
	key: '',
	value: '',
	sirProblemId : '',
	API : {
		actionForKey : '/resolve/service/ac/item/',
		doExecution : '/resolve/service/execute/submit'
	},
	valuePairInfo$ : function(){
		return '<b>Key:</b>&nbsp;' + Ext.String.htmlEncode(this.key) + '&nbsp;&nbsp;<b>Value:</b>&nbsp;' + Ext.String.htmlEncode(this.value);
	},
	actionStore : {
		mtype : 'store',	
		fields : [{
			name : 'task',
			type : 'auto'
		},{
			name :'automation',
			type : 'auto'
		},'uname','param',{
			name : 'executionType',
			convert : function(val, record){
				var data = record.raw;
				if(data.task)
					return 'ACTIONTASK'
				else
					return 'AUTOMATION'
			}
		},{
			name : 'executionItem',
			convert : function(val, record){
				var data = record.raw;
				if(data.task)
					return data.task.ufullName;
				else
					return data.automation.ufullname;
			}
		},{
			name : 'isDisabled',
			type : 'bool',
			convert : function(){
				return false;
			}
		}]
	},
	init: function(){
		this.ajax({
			url : this.API['actionForKey'] + this.key,
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success){
					this.actionStore.loadData(response.data);
				}
				else
					clientVM.displayError(response.message);
			},
			failure: function(resp){
				clientVM.displayFailure(resp);
			}
		})
	},
	actionHandler : function(record, grid){
		record.data.isDisabled = true;
		grid.refresh();
		var executionType = record.get('executionType');	
		var params = {};
		params[record.get('param')] = this.value;
		var payload = {
			params : params,
			problemId : this.sirProblemId
		}
		if(executionType == 'ACTIONTASK'){	
			
			payload = Ext.apply(payload,{
				action: 'TASK',
				actiontask : record.get('task').ufullName,				
			})
		}
		else
		{			
			payload = Ext.apply(payload,{
				wiki : record.get('automation').ufullname,			
			})
		}
		this.ajax({
			url : this.API['doExecution'],
			jsonData : payload,
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success){
				clientVM.displaySuccess(this.localize('executionSubmitSucc'));
				}
				else
					clientVM.displayError(response.message);
			},
			failure: function(resp){
				clientVM.displayFailure(resp);
			},
			callback : function(){
				setTimeout(function(){
					record.data.isDisabled = false;
					grid.refresh();
				}.bind(this), 5000)				
			}
		})		
	},
	close : function(){
		this.doClose();
	}
})