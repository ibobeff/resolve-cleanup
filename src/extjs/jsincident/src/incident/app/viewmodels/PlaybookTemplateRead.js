glu.defModel('RS.incident.PlaybookTemplateRead', {
	mixins: ['Playbook'],
	isReadOnly: true,
	activate: function(screen, params) {
		this.applyRBAC();
		if (params.name) {
			this.set('fullName', params.name);
			this.activitiesTab.set('isReadOnly', true);
			this.getPreviewContent(function(){
				this.fireEvent('loadDescription', this.previewUContent, this.fullName);
			});
		}
		if (!this.pSirPlaybookTemplatesTemplateBuilderView) {
			this.message({
				title: this.localize('insufficientPermissionMsgTitle'),
				msg: this.localize('insufficientPermissionMsg'),
				closable : false,
				buttonText: {
					ok: this.localize('close')
				},
				scope: this,
				fn: function(btn) {
					clientVM.handleNavigationBackOrExit();
				}
			});
		}
	},

	pSirPlaybookTemplatesTemplateBuilderModify: false,
	pSirPlaybookTemplatesCreate: false,
	pSirPlaybookTemplatesTemplateBuilderView: false,
	applyRBAC: function() {
		var permissions = clientVM.user.permissions || {};
		var pCreate = permissions['sir.playbookTemplates.templateBuilder.create'];
		var pModify = permissions['sir.playbookTemplates.templateBuilder.change'];

		this.set('pSirPlaybookTemplatesTemplateBuilderModify', pModify);
		this.set('pSirPlaybookTemplatesCreate', pCreate);

		this.set('editIsEnabled', pModify);
		this.set('copyIsEnabled', pCreate);

		this.set('pSirPlaybookTemplatesTemplateBuilderView', true);
	},
	init: function() {
		this.applyRBAC();
	},

	expandDescription: function() {
		var modal = this.open({
			mtype: 'RS.incident.DescriptionModal',
			ticketInfo: this.ticketInfo
		});
		modal.fireEvent('loadDescriptionModal', this.previewUContent, this.fullName)
	},
	returnTitle$: function() {
		return this.fullName;
	},

	edit: function() {
		clientVM.handleNavigation({
			modelName: 'RS.incident.PlaybookTemplate',
			params: {
				name: this.fullName
			}
		});
	},
	editIsEnabled: false,
	copyIsEnabled: false,

	copy: function() {
		var me = this;

		this.open({
			mtype: 'RS.wiki.CopyWiki',
			name: this.uname,
			namespace: this.unamespace,
			id: this.id
		})
	},

	returnToTemplates: function() {
		clientVM.handleNavigation({
			modelName: 'RS.incident.PlaybookTemplates'
		});
	},
	preview: function() {
		this.open({
			mtype: 'RS.incident.PlaybookTemplatePreview',
			previewUContent: this.previewUContent,
			previewContent: this.activitiesTab.previewContent,
			fullName: this.fullName
		});
	},
	reload: function() {
		this.getPreviewContent(function(){
			this.fireEvent('loadDescription', this.previewUContent, this.fullName);
		});
	},
});
