Ext.define('RS.incident.model.Phase', {
	extend: 'Ext.data.Model',
	fields: ['phase'],
	idProperty: 'phase'
});
