Ext.define('RS.incident.model.Activity', {
    extend: 'Ext.data.Model',
    idProperty: 'altActivityId',
    fields: [
        'phase',
        'phaseOrder',
        'activityName',
        'activityOrder',
        'wikiName',
        'status',
        'assignee',
        'description',
        'id',
        'template',
        'incidentId',
        'templateActivity',
        {
            name: 'dueDate',
            type: 'date',
            convert: function(v, r) {
                return v? Ext.util.Format.date(new Date(v), clientVM.getUserDefaultDateFormat()): v;
            },
            serialize: function(v) {
                return v? (new Date(v)).getTime(): v;
            }
        }
    ],
    proxy: {
        type: 'ajax',
        url: '/resolve/service/playbook/activity/update',
        reader: 'json',
        writer: 'json',
        listeners: {
            exception: function(e, resp, op) {
				clientVM.displayExceptionError(e, resp, op);
			}
        }
    }
});
