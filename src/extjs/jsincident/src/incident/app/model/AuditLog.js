Ext.define('RS.incident.model.AuditLog', {
    extend: 'Ext.data.Model',
    fields: [
        'sysCreatedBy',
        'ipAddress',
        'sysCreatedOn',
        'description',
        'incidentId',
        'worksheetId'
    ],
    proxy: {
        type: 'ajax',
        method: 'GET',
        url: '/resolve/service/playbook/auditlog/list',
        reader: {
            type: 'json',
            root: 'records'
        },
        listeners: {
            exception: function(e, resp, op) {
                clientVM.displayExceptionError(e, resp, op);
            }
        }
    }
});