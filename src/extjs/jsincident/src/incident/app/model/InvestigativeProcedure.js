Ext.define('RS.incident.model.InvestigativeProcedure', function () {
	ufields = ['phase', 'activityName', 'wikiName', 'isRequired', 'description', 'days', 'hours', 'minutes', 'altActivityId', 'templateActivity'];
	return {
		extend: 'Ext.data.TreeModel',
		config: {
			ufields: this.ufields
		},
		fields: this.ufields.slice()
	}
}());
