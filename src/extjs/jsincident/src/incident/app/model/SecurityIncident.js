Ext.define('RS.incident.model.SecurityIncident', {
	extend: 'Ext.data.Model',
	fields: [
		'id',
		'sir',
		'title',
		'externalReferenceId',
		'investigationType',
		'type',
		'severity',
		'description',
		'sourceSystem',
		'playbook',
		'externalStatus',
		'status',
		'owner',
		'playbookVersion',
		'sequence',
		'closedOn',
		'closedBy',
		'primary',
		'sysCreatedOn',
		'sirStarted',
		'problemId',
		'dueBy'
	],
	idProperty: 'id',
	proxy: {
		type: 'ajax',
		url: '/resolve/service/playbook/securityIncident/save',
		reader: 'json',
		writer: 'json',
		listeners: {
			exception: function(e, resp, op) {
				if (clientVM.delayedTask) {
					clientVM.delayedTask.cancel();
				}
				clientVM.delayedTask = new Ext.util.DelayedTask(function () {
					clientVM.displayExceptionError(e, resp, op);
				});
				clientVM.delayedTask.delay(500);
			}
		}
	}
});