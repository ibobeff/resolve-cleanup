Ext.define('RS.incident.model.ChartSettings', {
	extend: 'Ext.data.Model',
	fields: [
		{name: 'id', type: 'int'},
		{name: 'chartType', type: 'string', defaultValue: 'Investigations by Type'},
		{name: 'scope', type: 'string', defaultValue: '90'},
		{name: 'scopeUnit', type: 'string', defaultValue: 'Days'},
		{name: 'numberOfCols', type: 'int', defaultValue: 6}, // Supported shortly
		{name: 'ageBuckets', type: 'string', defaultValue: '0 Days-1 Days,3 Days,7 Days,21 Days,365 Days'},
		{name: 'typeFilter', type: 'string', defaultValue: 'All'},
		{name: 'severityFilter', type: 'string', defaultValue: 'All'},
		{name: 'ownerFilter', type: 'string', defaultValue: 'All'},
		{name: 'teamFilter', type: 'string', defaultValue: 'All'},
		{name: 'statusFilter', type: 'string', defaultValue: 'All'},
		{name: 'graphType', type: 'string', defaultValue: 'Pie Chart'}
	],
	proxy: {
		type: 'localstorage'
	}
});