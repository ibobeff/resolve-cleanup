Ext.define('RS.incident.model.DashboardSettings', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'dashboardShowOpen', type: 'bool', defaultValue: true},
        {name: 'dashboardShowInProgress', type: 'bool', defaultValue: true},
        {name: 'dashboardShowClosed', type: 'bool', defaultValue: true},
        {name: 'dashboardShowPastDue', type: 'bool', defaultValue: true},
        {name: 'dashboardShowSLAatRisk', type: 'bool', defaultValue: true},
        {name: 'sLAatRiskPolicy', type: 'int', defaultValue: 5},
        {name: 'scope', type: 'int', defaultValue: 90},
        {name: 'scopeUnit', type: 'string', defaultValue: 'days'},
        {name: 'dashboardrefreshRate', type: 'string', defaultValue: 'Never'},
        {name: 'autoRefreshPlayed', type: 'bool', defaultValue: true},
        {name: 'autoRefreshPaused', type: 'bool', defaultValue: false},
        {name: 'pieChartInvestigateBy', type: 'string', defaultValue: 'by Type'},
        {name: 'column1ChartInvestigateBy', type: 'string', defaultValue: 'by Severity'},
        {name: 'column2ChartInvestigateBy', type: 'string', defaultValue: 'by Age'}
    ],

    proxy: {
        type: 'localstorage',
        id: 'dashboard-settings'
    }
});
