Ext.define('RS.incident.store.Runbook', {
	extend: 'Ext.data.Store',
	model: 'RS.incident.model.Runbook',
	pageSize: 25,
	remoteFilter: true,
	proxy: {
		type: 'ajax',
		url: '/resolve/service/wiki/list',
		reader: {
			type: 'json',
			root: 'records'
		}
	}
});
