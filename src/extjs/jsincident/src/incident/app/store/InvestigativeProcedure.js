Ext.define('RS.incident.store.InvestigativeProcedure', {
    extend: 'Ext.data.Store',
    groupField: 'phase',
    model: 'RS.incident.model.InvestigativeProcedure'
});
