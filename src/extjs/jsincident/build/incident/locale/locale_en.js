glu.namespace('RS.incident').locale = (function(
	investigationByType,
	investigationBySeverity,
	investigationByAge,
	investigationByTeamMember,
	investigationByOwner,
	investigationByStatus,
	investigationByPhases,
	investigationOverTimes,
	investigationBySLA,
	investigationByWorkload
) {
	return {
		additionalSir: 'Additional SIR',
		fileType: 'File Type',
		outputFileName: 'Output File Name',
		exportingAuditLogs: 'Exporting SIR Audit Logs',
		exportAll: 'Export',
		sirList: 'SIR list',
		add: 'Add',
		isAlreadyOnSIRList: ' is already on the SIR list',
		inValidSirName: 'The SIR name in this field is invalid',
		edit: 'Edit',
		createPhase: 'Create Phase',
		activityPhase: 'Phase',
		invalidPhase: 'Phase is required and may only contain alphanumeric characters, spaces, commas, dashes, or underscores.',
		invalidActivityName: 'Name is required and may only contain alphanumeric characters, spaces, commas, dashes, or underscores.',
		createActivity: 'Create Activity',
		runbook: 'Wiki Name',
		wikiName: 'Wiki Name',
		required: 'Required',
		activityName: 'Name',
		sla: 'SLA',
		id: 'ID',
		phaseActivity: 'Phase/Activity',
		description: 'Description',
		addPhase: 'Add Phase',
		addActivity: 'Add',
		addActivityModal: 'Add Activity',
		removeActivity: 'Remove',
		deletePhaseTitle: 'Delete Phase',
		deletePhaseBody: 'Deleting selected phase will delete all associated activities?',
		yes: 'Yes',
		cancel: 'Cancel',
		confirm: 'Confirm',
		save: 'Save',
		saveAs: 'Save As',
		reload: 'Reload',
		preview: 'Preview',
		returnToTemplates: 'Exit to Templates',
		exitEdit: 'Exit to View',
		readOnlyTxt: '(read-only)',
		saveAndCommit: 'Commit',
		copy: 'Copy',
		source: 'Source',
		addLayoutSection: 'Section',
		viewFileOptions: 'File',
		revision: 'Revision',
		serverError: 'Server Error: {0}',
		autoCreatedTitle: 'New',
		autoCreatedMessage: 'This playbook template does not currently exist, click save to create the template.',
		WikiSaved: 'Playbook template saved successfully.',
		reloadWithoutSaveBody: 'Reload will lose all changes you have made to the document.<br >Proceed to reload?',
		reloadWithoutSaveTitle: 'Reload without saving?',
		allMembersAdded: 'All prospective members have been added.',
		openGraphicReports: 'Open Graphic Reports in new browser tab',
		Activities: {
			moreTip: 'More',
			addNotes: 'Add Note',
			addArtifacts: 'Add Artifact',
			addAttachments: 'Upload Attachment',
			viewAutomationResults: 'View Automation Results',
		},

		playbookTemplates: 'Playbook Templates',
		createTemplate: 'New',
		editTemplate: 'Edit',
		deleteTemplate: 'Delete',
		templates: 'Templates',
		name: 'Name',
		namespace: 'Namespace',
		activatePB: 'Activate',
		inactivate: 'Inactivate',
		active: 'Active',
		inactive: 'Inactive',
		successfullyActivatePBs: 'Successfully activate playbook templates',
		successfullyInactivatePBs: 'Successfully inactivate playbook templates',
		listTemplatesError: 'Cannot get templates from the server:{0}',
		deleteTemplateMsg: 'Delete Selected Template?',
		deleteTemplatesMsg: 'Delete Selected Template?',
		deleteTemplateError: 'Cannot delete the selected schemas:{0}',
		deleteTemplateSuccess: 'The selected schemas have been deleted.',
		insufficientPermissionMsgTitle: 'Insufficient Permissions',
		insufficientPermissionMsg: 'You don\'t have permissions to view this page. Click close to go back to the previous page.',
		ReportTemplates: {
			reportTemplates : 'Report Templates',
			new : 'New',
			copy : 'Copy',
			purge : 'Purge',
			goToDashboard : 'Investigation Dashboard',
			listTemplatesError: 'Cannot get the list of Templates from the server.[{0}]',

			purgeTemplate : 'Purge',
			purgeMsg: 'Are you sure you want to purge the selected templates? Selected templates:{names}',
			purgesMsg: 'Are you sure you want to purge the selected templates? Selected templates({num} selected):{names}',
			purgeSucMsg: 'The selected records have been purged.',
			purgeErrMsg: 'Cannot purge the selected templates.',

			copyTemplate : 'Copy',
			copyMsg: 'Are you sure you want to copy the selected templates? Selected templates:{names}',
			copysMsg: 'Are you sure you want to copy the selected templates? Selected templates({num} selected):{names}',
			copySucMsg: 'The selected records have been copied.',
			copyErrMsg: 'Cannot copy the selected templates.',
		},
		// PlaybookTemplateNamer
		invalidNamespace: 'Namespace is required and may only contain alphanumeric characters, spaces, dashes, or underscores.',
		newTemplate: 'New Playbook Template',
		name: 'Name',
		namespace: 'Namespace',
		create: 'Create',
		saveGeneralSuccess: 'Successfully saved the general information of the playbook template.',
		saveSuccess: 'Successfully saved playbook template.',

		//Playbook Template
		lockedBy: '<b>{0}</b> is editing',
		lockedByTooltip: 'Click to override lock',
		overrideSoftLock: 'Override the soft lock',
		overrideSoftLockAction: 'Override',
		confirmOverride: 'Are you sure you want to lock the wiki?',

		// Misc
		uploadImageOnNotExistDoc: 'Please save the document before uploading images.',
		docNotExist: 'Document not saved',

		// Dashboard
		id: 'ID',
		playbook: 'Playbook',
		title: 'Title',
		origin: 'Origin',
		type: 'Type',
		primary: 'Primary',
		severity: 'Severity',
		status: 'Status',
		owner: 'Owner',
		createdOn: 'Created On',
		closedOn: 'Closed On',
		closedBy: 'Closed By',
		organization: 'Organization',
		add: 'Add',
		newInvestigation: 'Create New Investigation',
		externalReferenceId: 'External Reference ID',
		close: 'Close',
		cancel: 'Cancel',
		prospectiveMembers: 'Prospective Members',
		newSecurityIncident: 'Create',
		playbookTemplate: 'Playbook Template',
		associatedIRRecords: 'Associated IR Records',
		origin: 'Origin',
		owner: 'Owner',
		addedBy: 'Added By',
		addedOn: 'Added On',
		updatedBy: 'Updated By',
		updatedOn: 'Updated On',
		createdBy: 'Created By',
		createdOn: 'Created On',
		value: 'Value',
		dateOpened: 'Date Opened',
		changeType: 'Change Type',
		changeStatus: 'Change Status',
		changeOwner: 'Change Owner',
		apply: 'Apply',
		selectType: 'Select Type',
		selectStatus: 'Select Status',
		selectOwner: 'Select Owner',
		successfullyUpdatedIncident: 'Incident updated',
		failedToUpdatedIncident: 'Failed to update incident',
		attemptUpdatePlaybook: 'Playbook of an in progress incident can not be changed',
		successfullyCloseTicket: 'Successfully closed the ticket(s)',
		successfullySaveTicket: 'Successfully updating the ticket(s)',
		failedToCloseTicket: 'Failed to close one or more tickets',
		successfullyUpdatingPlaybookActivity: 'Successfully updating playbook activity',
		successfullyUpdatingPlaybookActivities: 'Successfully updating playbook activities',
		failedLoadingSecurityUserGroup: 'Failed loading the system \'s security user group',
		referenceId: 'Reference ID',
		reference: 'Reference ID',
		alertId: 'Alert ID',
		correlationId: 'Correlation ID',
		relatedInvestigations: 'Associated SIEM Records',
		loadRelatedInvestigationsFail: 'Failed to load related investigations',
		none: 'None',
		configureSecurityGroup : 'Configure Security Group',
		configureChartType: 'Configure Chart Type',


		// Dashboard Metrics
		irDashboard: 'Incident Response Dashboard',
		open: 'Open',
		inProgress: 'In Progress',
		closed: 'Closed',
		pastDue: 'Past Due',
		slaAtRisk: 'SLA at Risk',
		refresh: 'Refresh',
		play: 'Play',
		pause: 'Pause',
		configureMetrics: 'Configure Metrics',
		slaPolicy: 'SLA Policy',
		show: 'Show',

		// Dashboard charts
		investigationByType: investigationByType,
		investigationBySeverity: investigationBySeverity,
		investigationByAge: investigationByAge,
		investigationBySLA: investigationBySLA,
		investigationByWorkload: investigationByWorkload,
		configureChartSettings: 'Chart Settings',

		SecurityGroupConfiguration : {
			defaultGroup : 'Default Security Group',
			defaultOwner : 'Default Investigation Owner',
			incidentType : 'Security Incident Type',
			securityGroupConfiguration : 'Security Group Configuration'
		},

		// Playbook Summary
		activity: 'Activity',
		summary: 'Summary',
		dueDate: 'Due Date',
		dueBy: 'Due By',
		assignee: 'Assignee',
		status: 'Status',
		phase: 'Phase',
		team: 'Team',
		user: 'User',
		add: 'Add',
		edit: 'Edit',
		goToActivity: 'Go To Activity',
		notePreview: 'Note Preview',
		source: 'Source',
		severity: 'Severity',

		dataCompromised: 'Data Compromised',
		IPAddress: 'IP Address',
		description: 'Description',
		loggedOn: 'Logged On',
		members: 'Members',
		playbookDescription: 'Playbook Description',
		activitiesTab: 'Activities',
		notesTab: 'Notes',
		artifactsTab: 'Artifacts',
		attachmentsTab: 'Attachments',
		automationresultsTab: 'Automation Results',
		auditlogTab: 'Audit Log',

		auditlogLogFail: 'Failed to log event',
		ticketInfoLoadFail: 'Failed to load incident information',
		membersLoadFail: 'Failed to load incident members',
		securitygroupLoadFail: 'Failed to load security group members',
		toggleDataCompromised: 'Toggle',
		saveMembers: 'Save',
		saveActivity: 'Add',
		saveNote: 'Add',
		saveArtifact: 'Add',
		cancel: 'Cancel',
		invalidIncidentTitle: 'Incident does not exist',
		invalidIncident: 'The referenced incident ID is not valid or does not exist. Click close to return to the Incident Dashboard.',
		changeDataCompromisedSuccess: 'Data compromised status updated',
		changeDataCompromisedFail: 'Failed to change data compromised status',
		changeStatusSuccess: 'Status updated',
		changeStatusFail: 'Failed to change status',
		addMemberSuccess: 'Incident members saved',
		addMemberFail: 'Failed to save members',
		addRemoveMember: 'Manage Team',
		userId: 'User ID',
		displayName: 'Display Name',
		manageMembersModal: 'Manage Members',
		securityGroup: 'Security Group',
		loadActivitiesFail: 'Failed to load activities',
		addActivitySuccess: 'Activity added',
		addActivityFail: 'Failed to add activity',
		updateActivityFail: 'Failed to update activity',
		updateActivitySuccess: 'Activity updated',
		loadRunbooksFail: 'Failed to load runbooks',
		generateiFrameFail: 'Could not load Wiki contents',
		artifactsLoadFail: 'Failed to load artifacts',
		artifactAddSuccess: 'Artifact added',
		artifactAddFail: 'Failed to add artifact',
		artifactDeleteSuccess: 'Artifact(s) removed successfully',
		artifactDeleteFail: 'Failed to remove selected artifact(s)',
		addArtifact: 'Add artifact',
		addArtifactTitle: 'Add artifact to {0}',
		editArtifact: 'Edit artifact',
		notesLoadFail: 'Failed to load notes',
		noteAddSuccess: 'Note added',
		noteAddFail: 'Failed to add note',
		noteEditSuccess: 'Note updated',
		noteEditFail: 'Failed to update note',
		addNote: 'Add Note',
		addNoteTitle: 'Add note to {0}',
		editNote: 'Edit Note',

		addAttachment: 'Upload Attachments',
		attachmentsUploadSuccess: 'Attachments uploaded',
		attachmentsDeleteSuccess: 'Attachments removed successfully',
		attachmentsDeleteFail: 'Failed to remove selected attachment(s)',
		deleteAttachment: 'Remove',
		deleteAttachmentWarningMsg: 'Are you sure you want to remove the selected attachment(s)?',
		browse: 'Browse',
		malicious: 'Malicious',
		dragHere: 'DROP FILES HERE',
		filenames: 'Files',
		submit: 'Submit',
		upload: 'Upload',
		emptyAttachmentDescription: 'Enter a description',
		emptyFilename: 'Browse for files',
		UploadError: 'Upload Error',
		attachmentsUpdateSuccess: 'Attachment updated',
		attachmentsUpdateFail: 'Attachment update failed',
		attachmentsUploadNotAllowed: 'Upload not allowed',
		attachmentsUploadCancelled: 'Upload cancelled',

		noDownloadTitle: 'Upload not done',
		noDownloadBody: 'Please wait for the upload to complete before downloading this file',

		auditlogLoadFail: 'Failed to load audit log',
		doubleClickExpand: 'Double click to expand',

		//Audit Log Strings
		incidentOpenedLog: 'User {0} opened incident {1}',
		incidentStatusChangedLog: 'User {0} changed the incident status to {1}',
		incidentOwnerChangedLog: 'User {0} changed the incident owner to {1}',
		incidentTypeChangedLog: 'User {0} changed the incident type to {1}',

		addActivityLog: 'User {0} added user activity {1}',
		editActivityLog: 'User {0} changed the {1} of activity {2} to {3}',

		addNoteLog: 'User {0} added a note: {1}',
		editNoteLog: 'User {0} edited a note to: {1}',

		addArtifactLog: 'User {0} added artifact {1} with value: {2}',
		editArtifactLog: 'User {0} edited the value of artifact {1} to: {2}',

		attachmentUploadLog: 'User {0} added {1} {2}: {3}',

		addMemberLog: 'User {0} added member {1}',
		dataCompromisedChangedLog: 'User {0} changed the Data Compromised status to {1}',

		addAttachmentsTitle: 'Upload Attachment to {0}',
		viewAutomationResultsTitle: 'Automation Results for {0}',

		Attachments : {
			activityFilter : 'Filter on Activity:'
		},
		AutomationResults : {
			activityFilter : 'Filter on Activity'
		},
		Artifacts : {
			lookupTootltip : 'Look up incident on this name and value'
		},

		SIEMData: {
			siemdataTab: 'SIEM Data',
			rawData: 'Raw SIEM Data:',
			noRequestData: 'No data present.'
		},

		//Incident Lookup
		noRecordFound : 'No record found for artifact <b>{0}</b> with value <b>{1}</b>',
		lookupInfo : 'Found {0} incident(s) that use artifact <b>{1}</b> with value <b>{2}</b>.',
		expandResultInTab : 'Show full results in new tab.',
		investigationByType: investigationByType,
		investigationBySeverity: investigationBySeverity,
		investigationByAge: investigationByAge,
		investigationByTeamMember: investigationByTeamMember,
		investigationByOwner: investigationByOwner,
		investigationByStatus: investigationByStatus,
		investigationByPhases: investigationByPhases,
		investigationOverTimes: investigationOverTimes,
		investigationBySLA: investigationBySLA,
		investigationByWorkload: investigationByWorkload,
		barGraph: 'Bar Graph',
		pieChart: 'Pie Chart',

		//Print Preview
		notesStoreIsEmpty : 'There is no notes added.',
		artifactsStoreIsEmpty : 'There is no artifacts added.',
		attachmentsStoreIsEmpty : 'There is no attachments uploaded.',
		noActivityDefined : '( There is no activities for this phase. )',
		automationResultsStoreIsEmpty : 'There is no automation results',
		size : 'Size',
		taskSummary : 'Description',
		result : 'Result',
		uploadedOn : 'Uploaded On',
		uploadedBy : 'Uploaded By',
		exportPDF : 'Export to PDF',
		convertPDFMsg : 'Converting Incident Record to PDF format. Please wait.',
		convertPDFTitle : 'Export to PDF',
		abort : 'Abort',

		section: 'Section',
		procedure: 'Procedure',
		remove: 'Remove',

		download: 'Download',

		//Artifacts
		automation : 'Automation',
		actiontask : 'ActionTask',
		keyReqs : 'Key is required and needs to be valid.',
		invalidName: 'Name is required and must be in alphanumerics, but may contain single spaces, underscores \'_\' or dash \'-\' between alphanumeric characters.',
		duplicatedType : 'Type cannot be duplicated.',
		duplicatedName : 'Name cannot be duplicated.',
		valueReqs : 'Value is required.',
		uname : 'Name',
		ushortName : 'Key',
		ufullName : 'Full Name',
		uartifactTypeSource : 'Source',
		udescription : 'Description',
		type : 'Type',
		automation : 'Automation',
		executionType : 'Execution Type',
		parameter : 'Mapped Parameter',
		name : 'Name',
		group : 'Group',
		createNewType : 'Create New Type',
		nameReqs : 'Name is required.',
		sourceReqs : 'Source is required.',
		removeArtifactTitle: 'Confirm Remove Artifact',
		removeArtifactBody: 'Are you sure you want to remove selected artifacts? This action cannot be undone.',
		executionSubmitSucc : 'The execution is successfully submitted.',
		ArtifactConfiguration : {
			articfactConfigTitle : 'Artifact Configuration',
			newType : 'New Type'
		},
		ArtifactConfigEntry : {
			aliasKeys : 'Keys',
			addAlias: 'Add',
			removeAlias : 'Remove',
			action : 'Action Definition',
			addAction : 'Add',
			removeAction : 'Remove'	,
			comfirmRemoveTypeTitle : 'Confirm Remove Artifact Type',
			comfirmRemoveTypeBody : 'Are sure you want to remove artifact type <b>{0}</b>. This action cannot be undone.',
			comfirmRemoveAliasTitle : 'Confirm Remove Key',
			comfirmRemoveAliasBody : 'Are sure you want to remove key <b>{0}</b>. This action cannot be undone.',
			comfirmRemoveActionTitle : 'Confirm Remove Action',
			comfirmRemoveActionBody : 'Are sure you want to remove action <b>{0}</b>. This action cannot be undone.'
		},
		NewAliasForm : {
			newAliasForm : 'New Key',
			CEF : 'CEF Standard',
			CUSTOM : 'Custom',
			keySaveSuccess : 'Key is saved successfully.'
		},
		NewActionForm : {
			newActionForm : 'New Action',
			invalidAutomation : 'Automation needs to have at least 1 parameter.',
			invalidActiontask : 'ActionTask needs to have at least 1 input parameter.',
			automationReqs : 'Automation is required.',
			actiontaskReqs : 'ActionTask is required.',
			parameterReqs : 'Parameter is required.',
			actionSaveSuccess : 'Action is {0} successfully.'
		},
		export : 'Export',
		ExportDashboard : {
			info : 'Select columns to include in the PDF:',	
			cancel : 'Cancel',
			sir : 'ID',
			id : 'System ID',
			playbook : 'Playbook',
			title : 'Title',
			sourceSystem : 'Origin',
			investigationType : 'Type',
			severity : 'Severity',
			status : 'Status',
			sla : 'SLA',
			dueBy : 'Due Date',
			owner : 'Owner',
			sysCreatedOn : 'Created On',
			closedOn : 'Closed On',
			closedBy : 'Closed By',
			sysOrg : 'Organization'
		}
	};

})(
	'Investigations by Type',
	'Investigations by Severity',
	'Investigations by Age',
	 'Investigations by Team Member',
	'Investigations by Owner',
	'Investigations by Status',
	'Investigations by Phases',
	'New Investigations Over Times',
	'Investigations by SLA',
	'Investigations by Workload'
);

glu.namespace('RS.incident.Dashboard').locale = {
	MetricsSetting: {
		show: 'Show',
		metricsSetting: 'Metrics Setting',
		apply: 'Apply',
		cancel: 'Cancel',
		dashboardShowOpen: 'Open',
		dashboardShowInProgress: 'In Progress',
		dashboardShowClosed: 'Closed',
		dashboardShowPastDue: 'Past Due',
		dashboardShowSLAatRisk: 'SLA at Risk',
		sLAatRiskPolicy: 'SLA at Risk Policy',
		scope: 'Scope',
		between1To365days:'Enter a value from 1 to 365 (days)',
		between1To24Hours: 'Enter a value from 1 to 24 (hours)',
		between1To120Minutes: 'Enter a value from 1 to 120 (minutes)'
	},

	Panel: {
		configureLabel: 'Configure'
	},

	ChartSettings: {
		chartSettings: 'Chart Settings',
		chartType: 'Investigation Report',
		investigationByType: 		RS.incident.locale.investigationByType,
		investigationBySeverity: 	RS.incident.locale.investigationBySeverity,
		investigationByAge: 		RS.incident.locale.investigationByAge,
		investigationByTeamMember: 	RS.incident.locale.investigationByTeamMember,
		investigationByOwner: 		RS.incident.locale.investigationByOwner,
		investigationByStatus: 		RS.incident.locale.investigationByStatus,
		investigationByPhases: 		RS.incident.locale.investigationByPhases,
		investigationOverTimes: 	RS.incident.locale.investigationOverTimes,
		investigationBySLA: 		RS.incident.locale.investigationBySLA,
		investigationByWorkload: 	RS.incident.locale.investigationByWorkload,
		investigationType: 'Investigation Type',
		all: 'All',
		dynamicUpdateBasedOnScope: 'Dynamic Update Based on Scope',
		investigationSeverity: 'Investigation Severity',
		critical: 'Critical',
		high: 'High',
		medium: 'Medium',
		low: 'Low',
		investigationOwner: 'Investigation Owner',
		listResolveUsers: 'List Resolve Users',
		investigationTeam: 'Investigation Team',
		investigationStatus: 'Investigation Status',
		open: 'Open',
		inProgress: 'In Progress',
		closed: 'Closed',
		configureChartSettings: 'Chart Settings',
		scope: 'Scope',
		numberOfCols: 'Number of Columns',
		weeks: 'Weeks',
		days: 'Days',
		hours: 'Hours',
		minutes: 'Minutes',
		filters: 'Filters',
		apply: 'Apply',
		cancel: 'Cancel',
		hour: 'Hour(s)',
		day: 'Day(s)',
		week: 'Week(s)',
		graphType: 'Graph Type',
		barGraph: 'Bar Graph',
		pieChart: 'Pie Chart',
		between1To365days:'Enter a value from 1 to 365 (days)',
		between1To23Hours: 'Enter a value from 1 to 23 (hours)',
		between1To59Minutes: 'Enter a value from 1 to 59 (minutes)',
		failedLoadingSecurityUserGroupAndTypes: 'Failed loading the system\'s SIR user group and types.',
		failedLoadingSecurityTypes: 'Failed loading the system\'s SIR types.',
		inValidNumberOfCols: 'Number of Columns must be from 1 to 10.',
		noDataMsg: 'The chart contains no data.'
	},
	AgeBucketBuilder: {
		ageBuckets: 'Age Buckets',
		column: 'Column',
		newTimeFrame: 'Add New Timeframe',
		deleteLastTimeframe: 'Remove Last Timeframe'
	},
	TimeFrame: {
		to: 'To',
		priorTimeFrameEnd: 'Prior timeframe end'
	},

	// KPI
	KPI: {
		addSection: 'Section',
		configureSettings: 'Edit Settings',
		save: 'Save',
		irDashboard: 'Analytic Reports',
		irDashboardBuilder: 'Analytic Reports Builder',
		refresh: 'Refresh',
		play: 'Play',
		pause: 'Pause',
		exitEdit: 'Exit to View'
	},

	KPISectionEntry: {
		addGraph: 'Graph',
		deleteSection: 'Delete Section'
	},
	addKPIEntry: 'Graph',
	deleteSection: 'Delete',

	investigationByType: 		RS.incident.locale.investigationByType,
	investigationBySeverity: 	RS.incident.locale.investigationBySeverity,
	investigationByAge: 		RS.incident.locale.investigationByAge,
	investigationByTeamMember: 	RS.incident.locale.investigationByTeamMember,
	investigationByOwner: 		RS.incident.locale.investigationByOwner,
	investigationByStatus: 		RS.incident.locale.investigationByStatus,
	investigationByPhases: 		RS.incident.locale.investigationByPhases,
	investigationOverTimes: 	RS.incident.locale.investigationOverTimes,
	investigationBySLA: 		RS.incident.locale.investigationBySLA,
	investigationByWorkload: 	RS.incident.locale.investigationByWorkload,

	name : 'Name',
	
};

glu.namespace('RS.incident.PlaybookTemplate').locale = {
	description: 'Description',
	AutomationFilter : {
		automationFilter : 'Automation Result Filter',
		resultConfiguration: 'Result',
		description: 'Description',
		addActionTask: 'Add Task',
		addGroup: 'Add Group',
		remove: 'Remove',
		filter: 'Filter',
		filterValue: 'Severity Filter',
		title: 'Title',
		preserveTaskOrder: 'Preserve Task Order',
		order: 'Order',
		refreshInterval: 'Refresh Interval',
		refreshCountMax: 'Refresh Attempts',
		descriptionWidth: 'Description Width',
		autoHide: 'Auto Hide',
		actionTasks: 'Action Tasks',
		actionTaskFilters: 'Action Task Filters',
		encodeSummary: 'Encode Summary',
		GOOD: 'GOOD',
		WARNING: 'WARNING',
		SEVERE: 'SEVERE',
		CRITICAL: 'CRITICAL',

		ASC: 'Ascending',
		DESC: 'Descending',

		newTask: 'New Task',
		newGroup: 'New Group',

		tags: 'Tags',
		addTag: 'Add Tag',
		removeTag: 'Remove',
		edit: 'Edit',

		progress: 'Progress',

		showAllActionTasks: 'Show All Action Tasks in Current Worksheet',
		includeStartEndTasks: 'Include Hidden Action Tasks',

		showResultsFrom: 'Show Results From',
		showAnyWiki: 'Any Automation',
		showCurrentWikiOnly: 'Current Automation Only',
		showCustomWikiOnly: 'Only this Automation'
	}
}
