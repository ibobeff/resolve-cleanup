/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
Ext.define('RS.incident.model.Activity', {
    extend: 'Ext.data.Model',
    idProperty: 'altActivityId',
    fields: [
        'phase',
        'phaseOrder',
        'activityName',
        'activityOrder',
        'wikiName',
        'status',
        'assignee',
        'description',
        'id',
        'template',
        'incidentId',
        'templateActivity',
        {
            name: 'dueDate',
            type: 'date',
            convert: function(v, r) {
                return v? Ext.util.Format.date(new Date(v), clientVM.getUserDefaultDateFormat()): v;
            },
            serialize: function(v) {
                return v? (new Date(v)).getTime(): v;
            }
        }
    ],
    proxy: {
        type: 'ajax',
        url: '/resolve/service/playbook/activity/update',
        reader: 'json',
        writer: 'json',
        listeners: {
            exception: function(e, resp, op) {
				clientVM.displayExceptionError(e, resp, op);
			}
        }
    }
});

Ext.define('RS.incident.model.AuditLog', {
    extend: 'Ext.data.Model',
    fields: [
        'sysCreatedBy',
        'ipAddress',
        'sysCreatedOn',
        'description',
        'incidentId',
        'worksheetId'
    ],
    proxy: {
        type: 'ajax',
        method: 'GET',
        url: '/resolve/service/playbook/auditlog/list',
        reader: {
            type: 'json',
            root: 'records'
        },
        listeners: {
            exception: function(e, resp, op) {
                clientVM.displayExceptionError(e, resp, op);
            }
        }
    }
});
Ext.define('RS.incident.model.ChartSettings', {
	extend: 'Ext.data.Model',
	fields: [
		{name: 'id', type: 'int'},
		{name: 'chartType', type: 'string', defaultValue: 'Investigations by Type'},
		{name: 'scope', type: 'string', defaultValue: '90'},
		{name: 'scopeUnit', type: 'string', defaultValue: 'Days'},
		{name: 'numberOfCols', type: 'int', defaultValue: 6}, // Supported shortly
		{name: 'ageBuckets', type: 'string', defaultValue: '0 Days-1 Days,3 Days,7 Days,21 Days,365 Days'},
		{name: 'typeFilter', type: 'string', defaultValue: 'All'},
		{name: 'severityFilter', type: 'string', defaultValue: 'All'},
		{name: 'ownerFilter', type: 'string', defaultValue: 'All'},
		{name: 'teamFilter', type: 'string', defaultValue: 'All'},
		{name: 'statusFilter', type: 'string', defaultValue: 'All'},
		{name: 'graphType', type: 'string', defaultValue: 'Pie Chart'}
	],
	proxy: {
		type: 'localstorage'
	}
});
Ext.define('RS.incident.model.DashboardSettings', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'dashboardShowOpen', type: 'bool', defaultValue: true},
        {name: 'dashboardShowInProgress', type: 'bool', defaultValue: true},
        {name: 'dashboardShowClosed', type: 'bool', defaultValue: true},
        {name: 'dashboardShowPastDue', type: 'bool', defaultValue: true},
        {name: 'dashboardShowSLAatRisk', type: 'bool', defaultValue: true},
        {name: 'sLAatRiskPolicy', type: 'int', defaultValue: 5},
        {name: 'scope', type: 'int', defaultValue: 90},
        {name: 'scopeUnit', type: 'string', defaultValue: 'days'},
        {name: 'dashboardrefreshRate', type: 'string', defaultValue: 'Never'},
        {name: 'autoRefreshPlayed', type: 'bool', defaultValue: true},
        {name: 'autoRefreshPaused', type: 'bool', defaultValue: false},
        {name: 'pieChartInvestigateBy', type: 'string', defaultValue: 'by Type'},
        {name: 'column1ChartInvestigateBy', type: 'string', defaultValue: 'by Severity'},
        {name: 'column2ChartInvestigateBy', type: 'string', defaultValue: 'by Age'}
    ],

    proxy: {
        type: 'localstorage',
        id: 'dashboard-settings'
    }
});

Ext.define('RS.incident.model.InvestigativeProcedure', function () {
	ufields = ['phase', 'activityName', 'wikiName', 'isRequired', 'description', 'days', 'hours', 'minutes', 'altActivityId', 'templateActivity'];
	return {
		extend: 'Ext.data.TreeModel',
		config: {
			ufields: this.ufields
		},
		fields: this.ufields.slice()
	}
}());

Ext.define('RS.incident.model.Phase', {
	extend: 'Ext.data.Model',
	fields: ['phase'],
	idProperty: 'phase'
});

Ext.define('RS.incident.model.Runbook', {
	extend: 'Ext.data.Model',
	fields: ['ufullname']
});

Ext.define('RS.incident.model.SecurityIncident', {
	extend: 'Ext.data.Model',
	fields: [
		'id',
		'sir',
		'title',
		'externalReferenceId',
		'investigationType',
		'type',
		'severity',
		'description',
		'sourceSystem',
		'playbook',
		'externalStatus',
		'status',
		'owner',
		'playbookVersion',
		'sequence',
		'closedOn',
		'closedBy',
		'primary',
		'sysCreatedOn',
		'sirStarted',
		'problemId',
		'dueBy'
	],
	idProperty: 'id',
	proxy: {
		type: 'ajax',
		url: '/resolve/service/playbook/securityIncident/save',
		reader: 'json',
		writer: 'json',
		listeners: {
			exception: function(e, resp, op) {
				if (clientVM.delayedTask) {
					clientVM.delayedTask.cancel();
				}
				clientVM.delayedTask = new Ext.util.DelayedTask(function () {
					clientVM.displayExceptionError(e, resp, op);
				});
				clientVM.delayedTask.delay(500);
			}
		}
	}
});
Ext.define('RS.incident.store.InvestigativeProcedure', {
    extend: 'Ext.data.Store',
    groupField: 'phase',
    model: 'RS.incident.model.InvestigativeProcedure'
});

Ext.define('RS.incident.store.Phase', {
    extend: 'Ext.data.Store',
    model: 'RS.incident.model.Phase'
});

Ext.define('RS.incident.store.Runbook', {
	extend: 'Ext.data.Store',
	model: 'RS.incident.model.Runbook',
	pageSize: 25,
	remoteFilter: true,
	proxy: {
		type: 'ajax',
		url: '/resolve/service/wiki/list',
		reader: {
			type: 'json',
			root: 'records'
		}
	}
});

glu.defModel('RS.incident.ArtifactAction',{
	key: '',
	value: '',
	sirProblemId : '',
	API : {
		actionForKey : '/resolve/service/ac/item/',
		doExecution : '/resolve/service/execute/submit'
	},
	valuePairInfo$ : function(){
		return '<b>Key:</b>&nbsp;' + Ext.String.htmlEncode(this.key) + '&nbsp;&nbsp;<b>Value:</b>&nbsp;' + Ext.String.htmlEncode(this.value);
	},
	actionStore : {
		mtype : 'store',	
		fields : [{
			name : 'task',
			type : 'auto'
		},{
			name :'automation',
			type : 'auto'
		},'uname','param',{
			name : 'executionType',
			convert : function(val, record){
				var data = record.raw;
				if(data.task)
					return 'ACTIONTASK'
				else
					return 'AUTOMATION'
			}
		},{
			name : 'executionItem',
			convert : function(val, record){
				var data = record.raw;
				if(data.task)
					return data.task.ufullName;
				else
					return data.automation.ufullname;
			}
		},{
			name : 'isDisabled',
			type : 'bool',
			convert : function(){
				return false;
			}
		}]
	},
	init: function(){
		this.ajax({
			url : this.API['actionForKey'] + this.key,
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success){
					this.actionStore.loadData(response.data);
				}
				else
					clientVM.displayError(response.message);
			},
			failure: function(resp){
				clientVM.displayFailure(resp);
			}
		})
	},
	actionHandler : function(record, grid){
		record.data.isDisabled = true;
		grid.refresh();
		var executionType = record.get('executionType');	
		var params = {};
		params[record.get('param')] = this.value;
		var payload = {
			params : params,
			problemId : this.sirProblemId
		}
		if(executionType == 'ACTIONTASK'){	
			
			payload = Ext.apply(payload,{
				action: 'TASK',
				actiontask : record.get('task').ufullName,				
			})
		}
		else
		{			
			payload = Ext.apply(payload,{
				wiki : record.get('automation').ufullname,			
			})
		}
		this.ajax({
			url : this.API['doExecution'],
			jsonData : payload,
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success){
				clientVM.displaySuccess(this.localize('executionSubmitSucc'));
				}
				else
					clientVM.displayError(response.message);
			},
			failure: function(resp){
				clientVM.displayFailure(resp);
			},
			callback : function(){
				setTimeout(function(){
					record.data.isDisabled = false;
					grid.refresh();
				}.bind(this), 5000)				
			}
		})		
	},
	close : function(){
		this.doClose();
	}
})
glu.defModel('RS.incident.ArtifactConfigEntry',{
	API : {
		removeType : '/resolve/service/at/',
		removeAliasFromType : '/resolve/service/at/',
		removeActionFromType : '/resolve/service/ac/'
	},
	isCustomType : false,
	artifactType : '',
	aliasKeys : [],
	actions : [],	
	aliasStore : {
		mtype : 'store',
		fields : ['ushortName', 'ufullName','uartifactTypeSource'],
		sorters : [{
			property : 'ushortName',
			transform : function(val){
				return val.toLowerCase();
			},
			direction : 'ASC'
		}]
	},
	aliasKeysSelections : [],
	init : function(){
		this.aliasStore.loadData(this.aliasKeys);
		this.actionStore.loadData(this.actions);
	},
	removeType : function(){
		//Might need to manual delete actions as well
		this.message({
			title : this.localize('comfirmRemoveTypeTitle'),
			msg : this.localize('comfirmRemoveTypeBody',[this.artifactType]),		
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirm'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.ajax({
					url : this.API['removeType'] + this.artifactType,
					method : 'DELETE',
					success : function(resp){
						var response = RS.common.parsePayload(resp);
						if(response.success){
							this.parentVM.removeType(this);
						}
						else
							clientVM.displayError(response.message);
					},
					failure : function(resp){
						clientVM.displayFailure(resp);
					}
				})
			}
		})		
	},
	addAlias : function(){
		this.open({
			mtype : 'NewAliasForm',
			type : this.artifactType			
		})
	},
	addAliasToStore : function(data){
		this.aliasStore.add(data);
	},
	removeAlias : function(){
		var entry = this.aliasKeysSelections[0];
		this.message({
			title : this.localize('comfirmRemoveAliasTitle'),
			msg : this.localize('comfirmRemoveAliasBody',[entry.get('ushortName')]),		
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirm'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.ajax({
					url : this.API['removeAliasFromType'] + this.artifactType + '/remove',
					method : 'POST',
					jsonData : {
						ushortName : entry.get('ushortName'),
						uartifactTypeSource : entry.get('uartifactTypeSource')
					},
					success : function(resp){
						var response = RS.common.parsePayload(resp);
						if(response.success){
							this.aliasStore.remove(entry);
						}
						else
							clientVM.displayError(response.message);
					},
					failure : function(resp){
						clientVM.displayFailure(resp);
					}
				})
			}
		})
	},
	removeAliasIsEnabled$ : function(){
		return this.aliasKeysSelections.length > 0
	},
	actionStore : {
		mtype : 'store',	
		fields : ['task','automation','uname','param',{
			name : 'executionType',
			convert : function(val, record){
				var data = record.raw;
				if(data.task)
					return 'ACTIONTASK'
				else
					return 'AUTOMATION'
			}
		},{
			name : 'executionItem',
			convert : function(val, record){
				var data = record.raw;
				if(data.task)
					return data.task.ufullName;
				else
					return data.automation.ufullname;
			}
		}]
	},
	actionSelections : [],
	addAction : function(){
		var nameCollection = [];
		for(var i = 0; i < this.rootVM.actionData.length; i++){
			nameCollection.push(this.rootVM.actionData[i].uname);
		}
		this.open({
			mtype : 'NewActionForm',
			nameCollection : nameCollection,
			artifactType : this.artifactType,
			dumper : {
				dump : function(data){
					this.actionStore.add(data);
				},
				scope : this
			}
		})
	},
	removeAction : function(){
		var entry = this.actionSelections[0];
		this.message({
			title : this.localize('comfirmRemoveActionTitle'),
			msg : this.localize('comfirmRemoveActionBody',[entry.get('uname')]),		
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirm'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.ajax({
					url : this.API['removeActionFromType'] + entry.get('uname'),
					method : 'DELETE',					
					success : function(resp){
						var response = RS.common.parsePayload(resp);
						if(response.success){
							this.actionStore.remove(entry);
						}
						else
							clientVM.displayError(response.message);
					},
					failure : function(resp){
						clientVM.displayFailure(resp);
					}
				})
			}
		})	
	},
	removeActionIsEnabled$ : function(){
		return this.actionSelections.length > 0;
	},
	editAction : function(name){
		var record = this.actionStore.findRecord('uname', name);
		this.open({
			mtype : 'NewActionForm',
			artifactType : this.artifactType,
			edit : true,
			data : record.data,
			dumper : {
				dump : function(data){
					var r = this.actionStore.findRecord('uname', data.uname);
					this.actionStore.remove(r);
					this.actionStore.add(data);
				},
				scope : this
			}
		})
	}
})
glu.defModel('RS.incident.ArtifactConfiguration', {
	API : {	
		getAlias : '/resolve/service/at',
		getAction : '/resolve/service/ac'
	},
	configList : {
		mtype : 'list'
	},
	actionLoaded : false,
	actionData : [],
	keyLoaded :false,
	keyData : [],
	artifactTypeName: [],
	activate : function(){
		this.loadKey();	
		this.loadAction();	
	},
	when_data_ready : {
		on : ['actionLoadedChanged', 'keyLoadedChanged'],
		action : function(){
			if(this.actionLoaded && this.keyLoaded)
				this.buildConfigEntry();
		}
	},
	loadKey : function(){
		this.ajax({
			url : this.API['getAlias'],
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success){
					this.keyData = response.data;
					this.set('keyLoaded', true);
				}
				else
					clientVM.displayError(response.message);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})	
	},
	loadAction : function(){
		this.ajax({
			url : this.API['getAction'],
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success){
					this.actionData = response.data;
					this.set('actionLoaded', true);
				}
				else
					clientVM.displayError(response.message);
			},
			failure : function(resp){
				clientVM.displayFailure(resp);
			}
		})
	},
	buildConfigEntry : function(){
		this.configList.removeAll();
		var actions = {};
		for(var i = 0; i < this.actionData.length; i++){
			var entry = this.actionData[i];
			var artifactType = entry.artifactType.uname;
			if(!actions.hasOwnProperty(artifactType))
				actions[entry.artifactType.uname] = [];
			actions[entry.artifactType.uname].push(RS.common.mergeObject({
				automation : null,
				task : null,
				param : null,
				uname : null
			}, entry));
		}
		for(var i = 0; i < this.keyData.length;i++){
			var entry = this.keyData[i];
			this.configList.add(this.model({
				mtype : 'ArtifactConfigEntry',
				artifactType : entry.uname,
				isCustomType : !entry.ustandard,
				aliasKeys : entry.dictionaryItems || [],
				actions : actions[entry.uname] || []
			}))
			this.artifactTypeName.push(entry.uname.toUpperCase());
		}
	},
	newType : function(){
		this.open({
			mtype : 'NewTypeForm',	
			artifactTypeName: this.artifactTypeName,
			dumper : {
				dump : function(data){
					this.configList.add(this.model({
						mtype : 'ArtifactConfigEntry',
						artifactType : data.name,
						isCustomType : true,
						aliasKeys : [],
						actions : []					
					}));					
				},
				scope : this
			}
		})
	},
	removeType : function(entry){
		this.configList.remove(entry);
	}
})
glu.defModel('RS.incident.NewTypeForm',{
	API : {
		saveType : '/resolve/service/at'
	},
	artifactTypeName :[],
	fields : ['name'],
	nameIsValid$ : function(){
		if(this.name && this.artifactTypeName.indexOf(this.name.toUpperCase()) != -1)
			return this.localize('duplicatedType');
		return RS.common.validation.PlainText.test(this.name) ? true : this.localize('invalidName');
	},
	save : function(){
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		this.ajax({
			url : this.API['saveType'],
			jsonData : {
				uname : this.name,
				ustandard : false,
				dictionaryItems : []
			},
			method : 'POST',
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success){
					if(this.dumper){
						this.dumper.dump.apply(this.dumper.scope, [this.asObject()]);
					}
					this.doClose();
				}
				else
					clientVM.displayError(response.message);					
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	cancel : function(){
		this.doClose();
	}
})
glu.defModel('RS.incident.NewActionForm',{
	API : {
		saveAction : '/resolve/service/ac',
		getTask : '/resolve/service/actiontask/get',
		getAutomation : '/resolve/service/wiki/get',
	},
	fields : ['id','param','uname',{
		name : 'automation',
		type : 'auto'
	},{
		name :'task',
		type : 'auto'
	},{
		name : 'automationName',
		convert : function(val, record){
			var automation = record.raw.automation;
			if(automation)
				return automation.ufullname
			else 
				return "";
		}
	},{
		name : 'taskName',
		convert : function(val, record){
			var task = record.raw.task;
			if(task)
				return task.ufullName
			else 
				return "";
		}
	},{
		name : 'actionTaskIsSelected',		
		type : 'bool',
		convert : function(val, record){
			return record.raw.executionType == 'ACTIONTASK' ? true : false;
		}
	}],
	actionTaskIsSelected : false,
	artifactType : '',
	saving : false,	
	loading : false,
	edit : false,
	nameCollection : [],
	parameterStore : {
		mtype : 'store',
		fields : ['name']
	},
	when_execution_type_switched : {
		on : ['actionTaskIsSelectedChanged'],
		action : function(val){
			if(!this.loading)
				this.set('param', '');
		}
	},
	init : function(){	
		if(this.edit && this.data){
			this.loading = true;
			this.loadData(this.data);
			this.populateParameterStore(this.actionTaskIsSelected , this.param);
			this.set('isPristine', true);
			this.loading = false;
		}
		else
			this.nameCollection = this.nameCollection.map(function(x){return x.toUpperCase();})
	},
	populateParameterStore : function(isActionTask, param){
		var parameters = [];
		this.ajax({
			url: isActionTask ? this.API['getTask'] : this.API['getAutomation'],
			params : {
				id : '',
				name : isActionTask ? this.taskName : this.automationName
			},
			scope: this,
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(!response.success){
					clientVM.displayError(response.message);				
				}
				var data = response.data;
				if(isActionTask){
					var paramList = data['resolveActionInvoc']['resolveActionParameters'];
					for(var i = 0; i < paramList.length; i++){
						if(paramList[i].utype == 'INPUT')
							parameters.push({name : paramList[i].uname});
					}
				}
				else {
					var parameterData = data['uwikiParameters'];
					parameters = parameterData ? Ext.decode(parameterData) : [];					
				}
				this.parameterStore.loadData(parameters);
				this.set('param', null);
				this.set('param', param);
			},
			failure : function(r){
				clientVM.displayFailure(r);
				this.abortExecution(true);
			}
		})
	},
	title$ : function(){
		return (this.edit ? 'Edit' : 'New') + ' Action';
	},	
	automationNameIsValid$ : function(){
		return this.actionTaskIsSelected || this.automationName != '' ? true : this.localize('automationReqs');
	},
	taskNameIsValid$ : function(){
		return !this.actionTaskIsSelected || this.taskName != '' ? true : this.localize('actiontaskReqs');
	},
	unameIsValid$ : function(){
		if(this.nameCollection.indexOf(this.uname.toUpperCase()) != -1)
			return this.localize('duplicatedName');
		else
			return RS.common.validation.PlainText.test(this.uname) ? true : this.localize('invalidName');
	},
	paramIsValid$ : function(){
		return this.parameter != '' ? true : this.localize('parameterReqs');
	},
	saveIsEnabled$ : function(){
		return !this.saving && this.isValid;
	},
	selectedAutomation : null,
	openAutomationSearch: function() {
		this.open({
			mtype : 'RS.common.ContentPicker',
			contentType : 'automation',
			dumper : {
				dump : function(selection) {
					this.parameterStore.removeAll();
					this.set('automation', selection.data);
					this.set('automationName', selection.get('ufullname'));
					var params = JSON.parse(selection.get('uwikiParameters') || '[]');
					this.set('automationNameIsValid', params.length == 0 ? this.localize('invalidAutomation') : true);
					this.parameterStore.loadData(params);
					this.set('param', params.length > 0 ? this.parameterStore.getAt(0).get('name') : '');
				},
				scope : this
			}
		})
	},
	selectedTask : null,
	openActiontaskSearch: function() {
		this.open({
			mtype : 'RS.actiontask.ActionTaskPicker',			
			dumper : {
				dump : function(selectedRecords){
					this.set('task', selectedRecords[0].data);					
					this.set('taskName', this.task['ufullName']);
					var params = this.task.resolveActionInvoc.resolveActionParameters || [];
					var inputs = [];
					for(var i = 0; i < params.length; i++){
						var p = params[i];
						if(p.utype == 'INPUT'){
							inputs.push({name : p.uname});
						}
					}
					this.parameterStore.loadData(inputs);
					this.set('taskNameIsValid', inputs.length == 0 ? this.localize('invalidActiontask') : true);
					this.set('param', inputs.length > 0 ? this.parameterStore.getAt(0).get('name') : '');
				},
				scope : this
			}
		})
	},
	save : function(){
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		this.set('saving', true);
		var payload = {
			uname : this.uname,
			artifactType : {
				uname : this.artifactType
			},				
			param : this.param
		}
		if(this.actionTaskIsSelected){
			var task = RS.common.mergeObject({	
				id : '',	
				uname : '',
				ufullName : '',
				unamespace : ''
			}, this.task)
			payload.task = task;
			payload.automation = null;
		}
		else {
			var automation = RS.common.mergeObject({
				id : '',			
				ufullname : '',
				uname : ''		
			}, this.automation)
			payload.automation = automation;
			payload.task = null;
		}
		
		this.ajax({
			url : this.API['saveAction'],
			method : 'POST',
			jsonData : payload,
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success){
					clientVM.displaySuccess(this.localize('actionSaveSuccess', [this.edit ? 'updated' : 'created']));
					if(this.dumper)			
						this.dumper.dump.apply(this.dumper.scope, [response.data]);
					this.doClose();
				}
				else 
					clientVM.displayError(response.message);
			},
			failure : function(resp){
				clientVM.displayFailure(resp);
			},
			callback : function(){
				this.set('saving', false);
			}
		})		
	},
	cancel : function(){
		this.doClose();
	}
})
glu.defModel('RS.incident.NewAliasForm',{
	type : '',	
	isCustomAlias : true,
	waiting : false,	
	fields : ['ushortName','ufullName','udescription','uartifactTypeSource'],
	sourceStore : null,
	aliasStore : {
		mtype : 'store',
		fields : ['ushortName','ufullName','udescription']
	},
	availableAlias : {
		CEF : [],
		CUSTOM : []
	},
	API : {
		saveCustomAlias : '/resolve/service/cd',
		getCEF : '/resolve/service/cef',
		getCUSTOM : '/resolve/service/cd',
		assignAliasToType : '/resolve/service/at/'
	},	
	ushortNameIsValid$ : function(){
		return this.ushortName && (this.uartifactTypeSource == 'CUSTOM' || this.isNonCustomKeyValid(this.ushortName))? true : this.localize('keyReqs');
	},
	ufullNameIsValid$ : function(){
		return this.ufullName ? true : this.localize('nameReqs');
	},
	uartifactTypeSourceIsValid$ : function(){
		return this.uartifactTypeSource ? true : this.localize('sourceReqs');
	},
	isNonCustomKeyValid : function(key){
		return this.aliasStore.find('ushortName', key, 0, false, false, true) != -1;
	},
	when_source_changed : {
		on : ['uartifactTypeSourceChanged'],
		action : function(v){		
			this.aliasStore.loadData(this.availableAlias[v]);
			this.set('isCustomAlias', v == 'CUSTOM');
			if(v != 'CUSTOM'){
				var firstAlias = this.aliasStore.getAt(0);
				this.set('ushortName', firstAlias ? firstAlias.get('ushortName') : '');				
			}
			else
				this.clearForm();
		}
	},
	when_key_changed : {
		on : ['ushortNameChanged'],
		action : function(v){
			if(this.ushortNameIsValid == true){
				var alias = this.aliasStore.findRecord('ushortName', v);
				if(alias){
					this.set('ufullName', alias.get('ufullName'));
					this.set('udescription', alias.get('udescription'));
				}				
			}
		}
	},	
	init : function(){
		var me = this;
		for(var source in this.availableAlias){
			(function(s){
				this.ajax({
					url : this.API['get' + s],
					method : 'GET',
					success : function(resp){
						var response = RS.common.parsePayload(resp);
						if(response.success){
							this.availableAlias[s] = response.data;
						}
						else
							clientVM.displayError(response.message);					
					},
					failure: function(resp) {
						clientVM.displayFailure(resp);
					}
				})
			}.bind(this))(source)		
		}
		var sourceStore = Ext.create('Ext.data.Store',{		
			fields : [{
				name : 'value',
				convert : function(v, record){
					return record.raw;
				}
			},{
				name : 'display',
				convert : function(v, record){
					return me.localize(record.raw);
				}
			}],
			data : ['CEF', 'CUSTOM']
		})
		this.set('sourceStore', sourceStore);		
	},	
	clearForm : function(){
		this.set('ushortName','');
		this.set('udescription', '');
		this.set('ufullName','');
		this.set('isPristine', true);
	},
	save : function(){
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		this.set('waiting', true);
		if(this.uartifactTypeSource == 'CUSTOM'){
			this.ajax({
				url : this.API['saveCustomAlias'],
				jsonData : {				
					ufullName : this.ufullName,
					ushortName : this.ushortName,
					udescription : ''
				},
				success : function(resp){
					var response = RS.common.parsePayload(resp);
					if(response.success){
						clientVM.displaySuccess(this.localize('keySaveSuccess'));
						this.assignAliasToType();
					}
					else
						clientVM.displayError(response.message);					
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				},
				callback : function(){
					this.set('waiting', false);
				}
			})		
		}
		else 
			this.assignAliasToType();	
	},
	saveIsEnabled$ : function(){
		return !this.waiting;
	},
	assignAliasToType : function(){
		this.ajax({
			url : this.API['assignAliasToType'] + this.type + '/add',
			jsonData : {
				ushortName : this.ushortName,
				uartifactTypeSource : this.uartifactTypeSource
			},
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success){
					this.parentVM.addAliasToStore(this.asObject());
					this.doClose();
				}
				else
					clientVM.displayError(response.message);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback : function(){
				this.set('waiting', false);
			}
		})
	},
	cancel : function(){
		this.doClose();
	}
})
glu.defModel('RS.incident.Dashboard.ChartSettings', function() {
	var filterStores = {};
	var arr = ['timeframeUnit', 'chartType', 'scopeUnit', 'type', 'severity', 'owner', 'team', 'status'];

	// Walk through the array and define combox's store
	// 'chartTypeFilterStore', 'scopeUnitFilterStore', 'typeFilterStore', 'severityFilterStore',
	// 'ownerFilterStore', 'teamFilterStore', 'statusFilterStore'
	arr.forEach(function(n) {
		filterStores[n+'FilterStore'] = {
			mtype: 'store',
			proxy: {
				type: 'memory'
			},
			fields: [n]
		}
	});
	return Ext.apply({
		chartId: '',
		target: null,
		chartType: '',
		scopeValue: 0,
		scope: 90,
		scopeUnit: 'Days',
		maxScope: 365,
		maxScopeInDays: 365,
		maxScopeInHours: 23,
		maxScopeInMinutes: 59,
		fvalid: true,
		numberOfCols: 6,
		minNColumn: 1,
		maxNColumn: 10,
		rb: '',
		graphType: '',
		typeFilter: 'All',
		severityFilter: 'All',
		ownerFilter: 'All',
		teamFilter: 'All',
		statusFilter: 'All',
		hideAgeBucketConfig: true,
		hideScopeConfig: true,
		hideColumnConfig: true,
		tips: '',
		chartConfigs: undefined,
		formLoaded: false,
		dirty: false,
		layoutContext: false,
		filterItems: {
			mtype: 'list'
		},

		// Chart settings configuration
		store: {
			mtype: 'store',
			model: 'RS.incident.model.ChartSettings'
		},

		when_scope_unit_changed_replace_the_tips: {
			on: ['scopeUnitChanged'],
			action: function() {
				this.setScopeUnitTips(this.scopeUnit);
			}
		},

		when_report_type_changed: {
			on : ['chartTypeChanged'],
			action : function() {
				this.reportTypeChanged();
			}
		},

		when_scope_changed: {
			on : ['scopeChanged'],
			action : function() {
				if (this.scope < this.numberOfCols) {
					this.set('numberOfCols', this.scope);
				}
			}
		},

		setScopeUnitTips: function(tips) {
			var map = {
				days: this.localize('between1To365days'),
				hours: this.localize('between1To23Hours'),
				minutes: this.localize('between1To59Minutes')
			}
			this.set('tips', map[tips.toLowerCase()]);
		},

		scopeUnitChange: function(rec) {
			var scopeUnit = rec[0].get('scopeUnit');
			if (scopeUnit == 'Days') {
				this.set('maxScope', 365);
				this.set('scope', (this.scope > this.maxScopeInDays)? this.maxScopeInDays: this.scope);
			} else if (scopeUnit == 'Hours') {
				this.set('maxScope', this.maxScopeInHours);
				this.set('scope', (this.scope > this.maxScopeInHours)? this.maxScopeInHours: this.scope);
			} else if (scopeUnit == 'Minutes') {
				this.set('maxScope', this.maxScopeInMinutes);
				this.set('scope', (this.scope > this.maxScopeInMinutes)? this.maxScopeInMinutes: this.scope);
			}
			this.set('fvalid', !this.fvalid); // Trick to clear invalid flag
		},

		addFilters: function(drivenBy) {
			var map = {};
			map[this.localize('investigationBySeverity')] = 0;
			map[this.localize('investigationByType')] = 1;
			map[this.localize('investigationByTeamMember')] = 3;
			map[this.localize('investigationByStatus')] = 4;

			var rec = this.filters.up('form').updateRecord().getRecord();
			var items = [{
				xtye: 'chartfiltercombo',
				filterOn: 'severity',
				name: 'severityFilter',
				store: this.severityFilterStore,
				value: rec.get('severityFilter')
			}, {
				xtye: 'chartfiltercombo',
				filterOn: 'type',
				name: 'typeFilter',
				store: this.typeFilterStore,
				value: rec.get('typeFilter')
			}, {
				xtye: 'chartfiltercombo',
				filterOn: 'owner',
				name: 'ownerFilter',
				store: this.ownerFilterStore,
				value: rec.get('ownerFilter')
			},{
				xtye: 'chartfiltercombo',
				filterOn: 'team',
				name: 'teamFilter',
				store: this.teamFilterStore,
				value: rec.get('teamFilter')
			},{
				xtye: 'chartfiltercombo',
				filterOn: 'status',
				name: 'statusFilter',
				store: this.statusFilterStore,
				value: rec.get('statusFilter')
			}];
			this.filters.removeAll(true);
			// This is to have what's selected being shown first
			this.filters.add(map[drivenBy]? items.splice(map[drivenBy], 1).concat(items): items);
		},

		loadForm: function(form) {
			this.filters = form.down('#filtersSet');
			this.ajax({
				url: '/resolve/service/playbook/securityIncident/getSecurityGroup',
				scope: this,
				success: function(resp) {
					var respData = RS.common.parsePayload(resp)
					if (respData.success) {
						var grpRecs = respData.records;
						this.ajax({
							url: '/resolve/service/playbook/getsirtypesinscope',
							scope: this,
							params: {
								scopeUnits: parseInt(this.chartConfigs.scope),
								unitType: this.chartConfigs.scopeUnit
							},
							success: function(resp) {
								var response = RS.common.parsePayload(resp)
								if (response.success) {
									// Dynamically load data for 'owner/team/type' filter
									this.ownerFilterStore.loadData(grpRecs.map(function(r) {return {owner: r.userId};}), true); // true for appending
									this.teamFilterStore.loadData(grpRecs.map(function(r) {return {team: r.userId};}), true);   // true for appending
									this.typeFilterStore.loadData(response.records, true);  // true for appending

									var record = new RS.incident.model.ChartSettings(this.chartConfigs);
									this.setScopeUnitTips(record.get('scopeUnit'));
									this.form = form;
									this.loadRecord(record);
								} else {
									clientVM.displayError(this.localize('failedLoadingSecurityTypes'));
								}
							},
							failure: function(r) {
								clientVM.displayFailure(r);
							}
						});
					} else {
						clientVM.displayError(this.localize('failedLoadingSecurityUserGroupAndTypes'));
					}
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			});
		},

		loadFiltersStore: function() {
			this.chartTypeFilterStore.loadData([
				{chartType: this.localize('investigationByType')},
				{chartType: this.localize('investigationBySeverity')},
				{chartType: this.localize('investigationByAge')},
				{chartType: this.localize('investigationByTeamMember')},
				// These types will be supported shortly
				//,{chartType: this.localize('investigationByOwner')},
				{chartType: this.localize('investigationByStatus')},
				//{chartType: this.localize('investigationByPhases')},
				{chartType: this.localize('investigationOverTimes')},
				{chartType: this.localize('investigationBySLA')},
				{chartType: this.localize('investigationByWorkload')}
			]);
			this.scopeUnitFilterStore.loadData([
				{scopeUnit: this.localize('days')},
				{scopeUnit: this.localize('hours')},
				{scopeUnit: this.localize('minutes')}
			]);
			this.typeFilterStore.loadData([
				{type: this.localize('all')}
			]);
			this.severityFilterStore.loadData([
				{severity: this.localize('all')},
				{severity: this.localize('critical')},
				{severity: this.localize('high')},
				{severity: this.localize('medium')},
				{severity: this.localize('low')}
			]);
			this.ownerFilterStore.loadData([
				{owner: this.localize('all')}
			]);
			this.teamFilterStore.loadData([
				{team: this.localize('all')}
			]);
			this.statusFilterStore.loadData([
				{status: this.localize('all')},
				{status: this.localize('open')},
				{status: this.localize('inProgress')},
				{status: this.localize('closed')}
			]);
			this.timeframeUnitFilterStore.loadData([
				{timeframeUnit: this.localize('hours')},
				{timeframeUnit: this.localize('days')},
				{timeframeUnit: this.localize('weeks')}
			]);
		},

		reportTypeChanged: function() {
			this.addFilters(this.chartType);
			this.set('hideScopeConfig', true);
			this.set('hideAgeBucketConfig', true);
			this.set('hideColumnConfig', true);
			if (this.chartType == this.localize('investigationByType')) {
				this.set('hideScopeConfig', false);
			} else if (this.chartType == this.localize('investigationBySeverity')) {
				this.set('hideScopeConfig', false);
			} else if (this.chartType == this.localize('investigationByAge')) {
				this.set('hideAgeBucketConfig', false);
			} else if (this.chartType == this.localize('investigationOverTimes')) {
				this.set('hideScopeConfig', false);
				this.set('hideColumnConfig', false);
			} else if (this.chartType == this.localize('investigationByTeamMember')) {
				this.set('hideScopeConfig', false);
			} else if (this.chartType == this.localize('investigationBySLA')) {
				this.set('hideScopeConfig', false);
			} else if (this.chartType == this.localize('investigationByWorkload')) {
				this.set('hideScopeConfig', false);
			} else if (this.chartType == this.localize('investigationByOwner')) {
				// Will support later
			} else if (this.chartType == this.localize('investigationByStatus')) {
				this.set('hideScopeConfig', false);
			} else if (this.chartType == this.localize('investigationByPhases')) {
				// Will support later
			}
		},

		init: function() {
			this.loadFiltersStore();
			this.filtersSet = [{
				xtye: 'chartfiltercombo',
				filterOn: 'severity',
				name: 'severityFilter',
				store: this.severityFilterStore
			}, {
				xtye: 'chartfiltercombo',
				filterOn: 'type',
				name: 'typeFilter',
				store: this.typeFilterStore
			}, {
				xtye: 'chartfiltercombo',
				filterOn: 'owner',
				name: 'ownerFilter',
				store: this.ownerFilterStore
			},{
				xtye: 'chartfiltercombo',
				filterOn: 'team',
				name: 'teamFilter',
				store: this.teamFilterStore
			},{
				xtye: 'chartfiltercombo',
				filterOn: 'status',
				name: 'statusFilter',
				store: this.statusFilterStore
			}];
		},

		agingBucketIsValid: true,
		agingBucketValidityChange: function(isValid) {
			this.set('agingBucketIsValid', isValid);
		},

		numberOfColsIsValid$: function() {
			return (this.numberOfCols && this.numberOfCols <= this.maxNColumn && this.minNColumn <= this.numberOfCols)
				? true
				: this.localize('inValidNumberOfCols');
		},

		applyIsEnabled$: function() {
			var isValid = function(val) {
				return ((Array.isArray(val) && val.length > 0) || (typeof(val) == 'string') && val);
			};

			return (this.dirty && this.scope
					&& this.scope <= this.maxScope
					&& isValid(this.typeFilter)
					&& isValid(this.severityFilter)
					&& isValid(this.ownerFilter)
					&& isValid(this.teamFilter)
					&& isValid(this.statusFilter)
					&& this.agingBucketIsValid
					&& this.numberOfColsIsValid === true);
		},
		dirtyChange: function(dirty) {
			this.set('dirty', dirty);
		},
		getConfigs: function() {
			if (!this.form) {
				return new RS.incident.model.ChartSettings();
			}
			var rec = this.form.updateRecord().getRecord();
			rec.set('ageBuckets', this.form.down('agebucketbuilder').getData());
			return rec;
		},

		save: function() {
			var rec = this.getConfigs();
			rec.save();
			return rec;
		},

		apply: function() {
			var rec = this.save();
			this.dumper(rec.getData());
			this.close();
		},
		cancel: function() {
			this.close();
		},

		loadRecord: function(record) {
			if (this.form) {
				this.form.loadRecord(record);
				this.form.down('agebucketbuilder').loadData(record.getData()['ageBuckets']);
				this.set('formLoaded', !this.formLoaded);
			}
		},

		refresh: function(record) {
			this.loadRecord(record);
		},

		alignPosition: '',
		calculatePosition: function() {
			var hIndex = this.parentVM.parentVM.viewSectionEntries.indexOf(this.parentVM);
			var vIndex = this.parentVM.parentVM.parentVM.viewSections.indexOf(this.parentVM.parentVM);
			var onLeftSide = (hIndex == 0);
			var onRightSide = (hIndex == this.parentVM.parentVM.viewSectionEntries.length - 1);
			var atBottom = (vIndex == this.parentVM.parentVM.parentVM.viewSections.length - 1);
			var onTop = (vIndex == 0);
			if (onLeftSide && atBottom && !onTop && !onRightSide) {
				this.set('alignPosition', 'bl-tl'); // bottom-left of this with bottom-left of other
			} else if (onLeftSide && !onRightSide) {
				this.set('alignPosition', 'tl-tl'); // top-left of this with top-left of other
			} else if (atBottom && !onTop) {
				this.set('alignPosition', 'br-tr'); // top-left of this with top-left of other
			} else {
				this.set('alignPosition', 'tr-tr'); // top-right of this with top-right of other
			}
		}

	}, filterStores);
}());

glu.defModel('RS.incident.Dashboard', {
	//Chain to Audit Log
	playbookComponent: {
		mtype: 'Playbook'
	},

	handleEdit: function(context) {
		//Save Activity Edit
		if (context.value !== context.originalValue) {
			if (context.record.get('status') === 'Closed' && context.field === 'status') {
				this.closeTicket(null, context.record);
			}

			var modifiedContext = context.record.getData();
			var DateTime = modifiedContext.dueBy = Ext.util.Format.date((modifiedContext.dueBy || '').split(' ').join('T'), 'time'); //convert human time to ms timestamp
			if (context.field === 'ownerName') {
				modifiedContext.owner = context.value;
			}

			//Orgs Info
			Ext.apply(modifiedContext, {
				'sysOrg' : clientVM.orgId
			})
			this.ajax({
				url: '/resolve/service/playbook/securityIncident/save',
				method: 'POST',
				jsonData: modifiedContext,
				success: function(resp) {
					var response = RS.common.parsePayload(resp)
					if (response.success) {
						if (!(context.field === 'playbook' && context.record.get('status') === 'In Progress')) {
							clientVM.displaySuccess(this.localize('updateActivitySuccess'));
						} else {
							clientVM.displayError(this.localize('attemptUpdatePlaybook'));
						}
						setTimeout(function(){
							this.store.reload();
						}.bind(this), 500);
					}
					else {
						clientVM.displayError(response.message);
					}
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			});
		}
	},

	filterType: null,
	handleFilterChange: null,

	store: {
		mtype : 'store',
		remoteSort: true,
		pageSize : 20,
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		fields: [
			'id',
			'sir',
			'title',
			'externalReferenceId',
			'investigationType',
			'type',
			'severity',
			'description',
			'sourceSystem',
			'playbook',
			'externalStatus',
			'status',
			'owner',
			'members',
			'playbookVersion',
			'sequence',
			'closedBy',
			'primary',
			'sirStarted',
			'problemId',
			'closedOn',
			'sysCreatedOn',
			{
				name: 'dueBy',
				type: 'date',
				convert: function(v, r) {return v ? Ext.util.Format.date(new Date(v), clientVM.getUserDefaultDateFormat()) : v},
				serialize: function(v) {
					//console.log('serializing');
					return v ? (new Date(v)).getTime() : v
				}
			},
			{name: 'sys_id', mapping: 'id'},
			'sla', 'sysOrg'
		],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/playbook/securityIncident/list',
			reader: {
				type: 'json',
				root: 'records'
			}
		},
		autoLoad :false
	},
	securityIncidentSelections: [],

	typeStore: {
		mtype: 'store',
		fields: ['type'],
		proxy: {
			type: 'memory'
		}
	},

	refreshTypes: function(editor) {
		Ext.Ajax.request({
			scope: this,
			url: '/resolve/service/sysproperties/getSystemPropertyByName',
			method: 'POST',
			params: {
				name: 'app.security.types'
			},
			success: function(resp) {
				var data = RS.common.parsePayload(resp);
				var typesStoreData = [];
				this.typesStoreData = [];
				if (!data.success) {
					clientVM.displayError(data.message);
					return;
				}
				var types = data.data.uvalue.split(',');
				types.forEach(function(r) {
					typesStoreData.push({type: r.trim()});
				});
				this.typeStore.loadData(typesStoreData);
				try {
					editor.expand();
				} catch(err) {}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	playbookTemplateStore: {
		mtype: 'store',
		fields: ['id', 'uname', 'unamespace', 'usummary', 'ufullname'],
		pageSize: 25,
		sorters: [{
			property: 'ufullname',
			direction: 'ASC'
		}],

		proxy: {
			type: 'ajax',
			url: '/resolve/service/wiki/listPlaybookTemplates',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	ownerStore: {
		mtype: 'store',
		fields: [
			'userId',
			{
				name: 'name',
				convert: function(value, record) {
					if (value.trim().length === 0) {
						return record.get('userId')
					} else {
						return value
					}
				}
			}
		],
		proxy: {
			type: 'memory'
		}
	},

	refreshMembers: function(editor) {
		this.ajax({
			url: '/resolve/service/playbook/securityIncident/getSecurityGroup',
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					var recs = RS.common.parsePayload(resp).records;
					this.ownerStore.loadData(recs);
					this.ownerStore.sort('userId', 'ASC');
					if (editor) {
						try {
							editor.expand();
						} catch(err) {}
					}
				} else {
					clientVM.displayError(this.localize('failedLoadingSecurityUserGroup'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	refreshRateStore: {
		mtype: 'store',
		fields: ['rate'],
		data: [
			{rate: '1 min'},
			{rate: '5 min'},
			{rate: '10 min'},
			{rate: '15 min'},
			{rate: 'Never'}
		],
		proxy: {
			type: 'memory'
		}
	},

	// Dashboard metrics
	openMetric: '',
	inProgressMetric: '',
	closedMetric: '',
	pastDueMetric: '',
	aLAatRiskMetric: '',
	sLAatRiskPolicy: '5 days',
	// Dashboard configuration settings
	dashboardSettingsStore: {
		mtype: 'store',
		model: 'RS.incident.model.DashboardSettings',
	},

	scope: 30,
	scopeUnit: 'days',

	dashboardrefreshRate: 'Never',
	autoRefreshPlayed: false,
	autoRefreshPaused: true,

	dashboardShowOpen: true,
	dashboardShowInProgress: true,
	dashboardShowClosed: true,
	dashboardShowPastDue: true,
	dashboardShowSLAatRisk: true,

	// Chart refresh monitor flag (to be toggled)
	refreshMonitor: true,

	// RBAC
	sirPermissions: undefined,
	pSirInvestigationsCreateNew: true,
	pSirDashboardCharts: true,
	pSirDashboardView: true,
	pSirDashboardInvestigationListView: true,

	dashboardrefreshRateChanges$: function() {
		var record = this.dashboardSettingsStore.getAt(0);
		record? record.set('dashboardrefreshRate', this.dashboardrefreshRate): 'ignoreme';
	},

	chartDataChange: function(type, val) {
		this.set(type, val);
		this.dashboardSettingsStore.getAt(0).set(type, val);
	},

	checkViewPermission: function() {
		return this.pSirDashboardInvestigationListView;
	},

	rootcsrftoken: '',

	initToken: function() {
		this.set('rootcsrftoken', clientVM.rsclientToken);
	},
	init: function() {
		this.initToken();
		var me = this;
		var filterType = ['Open', 'In Progress', 'Complete'];
		this.set('filterType', filterType);
		this.store.on('beforeload', function(store, op) {
			var userFilter = this.getUserFilter();
			op.params = {};
			op.params.filter = JSON.stringify(userFilter);
		}, this);
		this.store.on('beforeload', this.checkViewPermission);
		this.store.on('load', function(store, records, successful) {
			this.doRefresh();
		}, this);
		var handleFilterChange = function(newFilters) {
			var newFiltersArr = [];
			for (key in newFilters) {
				newFiltersArr.push(newFilters[key])
			}
			this.filterType = newFiltersArr;
			this.store.loadPage(1); //Reset grid to view first page after filter change
		};
		this.set('handleFilterChange', handleFilterChange);
		this.refreshTypes();
		this.applyRBAC();
		clientVM.on('orgchange', this.orgChange.bind(this));
	},
	getUserFilter: function(){
		var types = ['Open', 'In Progress', 'Complete', 'Closed'];
		var subset = types.filter(function(e) { return this.filterType.indexOf(e) === -1 }, this);

		//Filter format: {"field":"status","type":"auto","condition":"notEquals","value":"Closed"}
		var userFilter = subset.map(function(type) {
			return {"field": "status", "type": "auto", "condition": "notEquals", "value": type}
		});
		userFilter.push({
			field: 'sysOrg',
			type: 'auto',
			condition: 'equals',
			value: clientVM.orgId || 'nil'
		});
		return userFilter;
	},
	applyRBAC: function() {
		var permissions = clientVM.user.permissions || {};

		// pSirDashboardInvestigationListView
		permissions.sirDashboardInvestigationList = permissions.sirDashboardInvestigationList || {};
		this.set('pSirDashboardInvestigationListView', permissions.sirDashboardInvestigationList.view === true);

		permissions.sirDashboard = permissions.sirDashboard || {};
		this.set('pSirDashboardView', permissions.sirDashboard.view === true);

		permissions.sirDashboardCharts = permissions.sirDashboardCharts || {};
		this.set('pSirDashboardCharts', permissions.sirDashboardCharts.view === true);

		permissions.sirDashboardInvestigationList = permissions.sirDashboardInvestigationList || {};
		this.set('pSirInvestigationsCreateNew', permissions.sirDashboardInvestigationList.create === true);

	},

	configureMetrics: function() {
		if (!this.metricsSetting) {
			this.metricsSetting = this.open({
				mtype: 'RS.incident.Dashboard.MetricsSetting',
				target: arguments[0].target
			});
		}
	},

	activate: function(screen, params) {
		this.initToken();
		this.applyRBAC();
		if (!this.pSirDashboardView) {
			this.message({
				title: this.localize('insufficientPermissionMsgTitle'),
				msg: this.localize('insufficientPermissionMsg'),
				closable : false,
				buttonText: {
					ok: this.localize('close')
				},
				scope: this,
				fn: function(btn) {
					clientVM.handleNavigationBack();
				}
			});
		} else {
			this.loadChartSettings();
			this.store.reload();
		}
		this.refreshMembers();
	},

	newInvestigation: function(e) {
		this.open({
			mtype: 'RS.incident.SecurityIncidentForm',
			pos: e.getXY(),
		});
	},

	closeTicket: function(grid, record) {
		var me = this;
		if (clientVM.delayedTask) {
			clientVM.delayedTask.cancel();
		}
		this.ajax({
			url: '/resolve/service/playbook/securityIncident/close',
			params: {
				incidentId: record.get('id'),
				sir: ''
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					var data = respData.data;
					record.skipSave = true;
					record.set({
						status: data.status,
						closedOn: data.closedOn,
						closedBy: data.closedBy
					});
					clientVM.delayedTask = new Ext.util.DelayedTask(function () {
						clientVM.displaySuccess(me.localize('successfullyCloseTicket'));
					});
				} else {
					clientVM.delayedTask = new Ext.util.DelayedTask(function () {
						clientVM.displayError(me.localize('failedToCloseTicket'));
					});
				}
			},
			failure: function(resp) {
				clientVM.delayedTask = new Ext.util.DelayedTask(function () {
					clientVM.displayFailure(resp);
				});
			},
			callback: function() {
				clientVM.delayedTask.delay(500);
			}
		});
	},

	loadMetrics: function() {
		this.ajax({
			scope: this,
			url: '/resolve/service/playbook/getsircountreport',
			method: 'POST',
			params: {
				slaAtRiskUnits: this.sLAatRiskPolicy,
				scopeUnits: this.scope,
				unitType: this.scopeUnit,
				orgId: clientVM.orgId
			},
			success: function(resp) {
				var data = RS.common.parsePayload(resp);
				if (!data.success) {
					clientVM.displayError(data.message);
					return;
				}
				data = data.data;
				this.set('aLAatRiskMetric', data['SLA at Risk']? data['SLA at Risk'].toString(): '0');
				var completed = data['Complete']? data['Complete']: 0;  // Completed SIRs are counted toward Closed SIRs
				this.set('closedMetric', data['Closed']? (completed+data['Closed']).toString(): completed.toString());
				this.set('closedMetric', data['Closed']?data['Closed'].toString(): '0');
				this.set('inProgressMetric', data['In Progress']?data['In Progress'].toString(): '0');
				this.set('pastDueMetric', data['Past Due']? data['Past Due'].toString(): '0');
				this.set('openMetric', data['Open']? data['Open'].toString(): '0');
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	syncBindings: function(settings) {
		var me = this;
		Object.keys(settings).forEach(function(k) {
			(me[k] != undefined)? me.set(k, settings[k]): 'TryNotUsingIf';
		});
	},

	loadSettings: function() {
		var me = this;
		//loads any existing settings from localStorage
		this.dashboardSettingsStore.load();
		if (!this.dashboardSettingsStore.count()) {
			this.dashboardSettingsStore.loadData([new RS.incident.model.DashboardSettings()]);
			this.dashboardSettingsStore.sync();
		}

		this.syncBindings(this.dashboardSettingsStore.getAt(0).getData());

		// saves settings to localStorage
		this.dashboardSettingsStore.on('update', function(store, record) {
			me.syncBindings(record.getData());
			store.sync();
		});

		this.loadMetrics();

		// start auto-refresh if the dashboard was configured to do so
		if (this.dashboardrefreshRate !== 'Never' && this.autoRefreshPlayed) {
			this.startAutoRefresh(this.refreshRateMap[this.dashboardrefreshRate]);
		}

		this.initPermissions();
	},

	initPermissions: function() {
		var permissions = clientVM.user.permissions;
		permissions.sirDashboardPlaybook = permissions.sirDashboardPlaybook || {};
		permissions.sirDashboardTitle = permissions.sirDashboardTitle || {};
		permissions.sirDashboardType = permissions.sirDashboardType || {};
		permissions.sirDashboardSeverity = permissions.sirDashboardSeverity || {};
		permissions.sirDashboardStatus = permissions.sirDashboardStatus || {};
		permissions.sirDashboardDueDate = permissions.sirDashboardDueDate || {};
		permissions.sirPlaybookTemplates = permissions.sirPlaybookTemplates || {};
		// To modify a playbook template, user must have view permission on playbook template list
		permissions.sirDashboardPlaybook.modify = permissions.sirDashboardPlaybook.modify && permissions.sirPlaybookTemplates.view;
		this.set('sirPermissions', clientVM.user.permissions);
	},

	chart1Configs: {},
	chart2Configs: {},
	chart3Configs: {},

	loadChartSettings: function() {
		var me = this;
		var dataSourceMap = {
			'1': this.localize('investigationByType'),
			'2': this.localize('investigationBySeverity'),
			'3': this.localize('investigationByAge')
		};  // For default charts
		var graphTypeMap = {
			'1': this.localize('pieChart'),
			'2': this.localize('barGraph'),
			'3': this.localize('barGraph')
		};  // For default charts
		['1', '2', '3'].forEach(function(id) {
			RS.incident.model.ChartSettings.load(id, {
				scope: this,
				callback: function(record) {
					record = record? record: new RS.incident.model.ChartSettings({
						id: id,
						chartType: dataSourceMap[id],
						graphType: graphTypeMap[id]
					});  // Create default if not exist
					me.set('chart'+id+'Configs', record.getData());
				}
			});
		});
	},


	refreshRateMap: {
		'1 min': 60,
		'5 min': 300,
		'10 min': 600,
		'15 min': 900
	},

	manualRefresh: function() {
		this.doRefresh();
	},

	autoRefreshPlayIsEnabled$: function() {
		return this.autoRefreshPaused && this.dashboardrefreshRate !== 'Never';
	},
	autoRefreshPlay: function() {
		this.dashboardSettingsStore.getAt(0).set('autoRefreshPlayed', true);
		this.dashboardSettingsStore.getAt(0).set('autoRefreshPaused', false);
		this.startAutoRefresh(this.refreshRateMap[this.dashboardrefreshRate]);
	},

	refreshRateChange: function(newRate, oldRate) {
		if (newRate == 'Never') {
			this.stopAutoRefresh();
		} else if (oldRate == 'Never') {
			this.dashboardSettingsStore.getAt(0).set('autoRefreshPaused', true);
			this.dashboardSettingsStore.getAt(0).set('autoRefreshPlayed', false);
		}
		else if (this.autoRefreshPlayed) {
			this.startAutoRefresh(this.refreshRateMap[newRate]);
		}
	},

	autoRefreshPauseIsEnabled$: function() {
		return this.autoRefreshPlayed && this.dashboardrefreshRate !== 'Never';
	},
	autoRefreshPause: function() {
		this.dashboardSettingsStore.getAt(0).set('autoRefreshPaused', true);
		this.dashboardSettingsStore.getAt(0).set('autoRefreshPlayed', false);
		this.stopAutoRefresh();
	},

	doRefresh: function() {
		// Load metrics
		this.loadMetrics();
		// Load charts
		this.set('refreshMonitor', !this.refreshMonitor);
	},

	startAutoRefresh: function(interval) {
		if (this.autoRefreshTask) this.autoRefreshTask.destroy();

		var runner = new Ext.util.TaskRunner();
		this.autoRefreshTask = runner.newTask({
			run: this.doRefresh,
			scope: this,
			interval: (interval || 30)* 1000
		});
		this.autoRefreshTask.start();
	},
	stopAutoRefresh: function() {
		if (this.autoRefreshTask) {
			this.autoRefreshTask.destroy();
			delete this.autoRefreshTask;
		}
	},

	applyChartSettings: function(configs) {
		this.set('chart'+configs.id+'Configs', configs);
	},

	chartSettingsHandler: function(e) {
		var btn = Ext.getCmp(e.currentTarget.id);
		var chartConfigs = btn.up('chart').chartConfigs;
		var chartType = chartConfigs.chartType;
		var hideScopeConfig = true;
		var hideAgeBucketConfig = true;
		var hideColumnConfig = true;
		if (chartType == this.localize('investigationByType')) {
			hideScopeConfig =  false;
		} else if (chartType == this.localize('investigationBySeverity')) {
			hideScopeConfig = false;
		} else if (chartType == this.localize('investigationByAge')) {
			hideAgeBucketConfig =  false;
		} else if (chartType == this.localize('investigationByOwner')) {
		} else if (chartType == this.localize('investigationByStatus')) {
		} else if (chartType == this.localize('investigationByPhases')) {
			// Will support later
		} else if (chartType == this.localize('investigationOverTimes')) {
		} else if (chartType == this.localize('investigationBySLA')) {
			hideScopeConfig = false;
		} else if (chartType == this.localize('investigationByWorkload')) {
			hideScopeConfig = false;
		}
		this.open({
			mtype: 'RS.incident.Dashboard.ChartSettings',
			target: btn.up('panel'),
			chartConfigs: chartConfigs,
			hideScopeConfig: hideScopeConfig,
			hideAgeBucketConfig: hideAgeBucketConfig,
			hideColumnConfig: hideColumnConfig,
			dumper: this.applyChartSettings.bind(this)
		});
	},

	openGraphicReports: function() {
		clientVM.handleNavigation({
			modelName: 'RS.incident.Dashboard.KPI',
			target: '_blank'
		});
	},

	orgChange: function() {
		this.store.load();
		this.doRefresh();
	},

	beforeDestroyComponent : function() {
		clientVM.removeListener('orgchange', this.orgChange);
	},

	export: function(){
		this.open({
			mtype : 'RS.incident.ExportDashboard',
			userFilter : this.getUserFilter()
		})
	}
});
glu.defModel('RS.incident.Dashboard.PDFConverter',{
	//Export to PDF
	CONST : {
		PAGE_SIZE : 'A4',
		PAGE_DIMENSION: 'landscape',
		PAGE_MARGIN : [20,20,20,20],
		AVERAGE_CHAR_WIDTH : 6.5
	},
	fields : [{
		name : 'sir',
		locale : 'sir',
		show : true,
		width : 85
	},{
		name : 'playbook',
		locale : 'playbook',
		show : true,
		width : 'auto'
	},{
		name : 'title',
		locale : 'title',
		show : true,
		width : '*'
	},{
		name : 'sourceSystem',
		locale : 'sourceSystem',
		show : true,
		width : 70
	},{
		name : 'id',
		locale : 'id',
		show : true,
		width : 220
	},{
		name : 'investigationType',
		locale : 'investigationType',
		show : true,
		width : 60
	},{
		name : 'severity',
		locale : 'severity',
		show : true,
		width : 60
	},{
		name : 'status',
		locale : 'status',
		show : true,
		width : 60
	},{
		name : 'sla',
		locale : 'sla',
		show : true,
		width : 70,
		convert: function(val){
			var days = 0;
			var hours = 0;
			if (val) {
				// 86400 = n_seconds_per_day
				// 3600 = n_seconds_per_hour
				days = Math.floor(val/86400);        // assuming pos val
				hours = Math.floor(val%86400)/3600;  // assuming pos val
			}
			if (days || hours) {
				return Ext.String.format('{0} Days {1} Hours', days, hours);
			} else {
				return 'None';
			}
		}
	},{
		name : 'dueBy',
		locale : 'dueBy',
		show : true,
		width : 70,
		convert : function(v){
			return v ? Ext.util.Format.date(new Date(v), clientVM.getUserDefaultDateFormat()) : v;
		}
	},{
		name : 'owner',
		locale : 'owner',
		show : true,
		width : 70
	},{
		name : 'sysCreatedOn',
		locale : 'sysCreatedOn',
		show : true,
		width : 70,
		convert: function(val) {
			return (val === null) ? '' : Ext.util.Format.htmlEncode(Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat()))
		}
	},{
		name : 'closedOn',
		locale : 'closedOn',
		show : true,
		width : 70
	},{
		name: 'closedBy',
		locale: '~~closedBy~~',
		show : true,
		width : 70
	}, {
		name: 'sysOrg',
		locale: 'sysOrg',
		show : true,
		width : 70,
		convert: function(value) {
			if (value) {
				if(!this.orgStore) {
					this.orgStore = {};
					clientVM.user.orgs.forEach(function(e) {
						if(e.uname != 'None')
							this.orgStore[e.id] = e.uname;
					}, this);
				}
				return this.orgStore[value];
			}
			return '';
		}
	}],
	store : null,
	entrySelections: [],
	init: function(){
		var me = this;
		this.set('store', Ext.create('Ext.data.Store',{
			fields : [{
				name: 'name',
				convert: function(v, r){
					return r.raw.name;
				}
			},{
				name: 'display',
				convert: function(v, r){
					return me.localize(r.raw.name);
				}
			}]
		}))
		this.store.loadData(this.fields);
	},
	startIsEnabled$ : function(){
		return this.entrySelections.length != 0;
	},
	start: function(){
		this.ajax({
			url: '/resolve/service/playbook/securityIncident/list',
			method: 'GET',
			params : {
				page: 1,
				start: 0,
				limit: -1,
				sort: JSON.stringify([{direction:'DESC',property:'sysCreatedOn'}]),
				filter: JSON.stringify(this.userFilter)
			},
			success: function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success){
					var selectedFields = [];
					Ext.Array.forEach(this.entrySelections, function(entry){
						selectedFields.push(entry.get('name'));
					})
					var fields = this.fields;
					Ext.Array.forEach(fields, function(entry){
						if(selectedFields.indexOf(entry.name) != -1)
							entry.show = true;
						else
							entry.show = false;
					})
					var fragment = this.getTableFragments('activities', '', fields, response.records , null);
					var PDFContent = {
						//pageSize: this.CONST.PAGE_SIZE,
			   			pageMargins: this.CONST.PAGE_MARGIN,
			   			pageOrientation: this.CONST.PAGE_DIMENSION,
			   			content :
			   			[{
					        text : 'Investigations',
					        style : ['incidentTitle']
			    		},fragment],
			   			styles : {
					        partHeader : {
					            bold: true,
					            fontSize: 15,
					            margin : [0,10,0,0]
					        },
					        incidentTitle : {
					            bold: true,
					            fontSize: 20,
					            margin : [0,0,0,5]
					        },
					        tableHeader : {
					        	bold : true,
					        	fillColor : '#3d3d3d',
					        	color : 'white'
					        },
					        tableGroup : {
					        	color : 'white',
					        	fillColor : '#777777',
					        	alignment : 'center'
					        }
					    }
					}
					var currentDate = new Date();
					var formatedDate = (currentDate.getMonth() + 1) + '-' + (currentDate.getDate() < 10 ? '0' + currentDate.getDate() : currentDate.getDate() ) + '-' + currentDate.getFullYear();
					pdfMake.createPdf(PDFContent).download('Investigations' + ' (' + formatedDate + ').pdf');
					this.doClose();
				}
			}
		})
	},
	getTableFragments : function(dataPartName, partTitle, fields, records, groupConfig){

		if(!records || records.length == 0){
			return [{
	        	text : partTitle,
	        	style : ["partHeader"]
	      	},this.localize(dataPartName + 'StoreIsEmpty')]
		}

		//Table Style
		pdfMake.tableLayouts = {
	  		resolveTable: {
			    hLineColor: function (i) {
		      		return '#3d3d3d';
			    },
			    vLineColor: function (i) {
		      		return '#3d3d3d';
			    },
			    fillColor: function (i, node) {
					return i % 2 == 0 ? '#dadada' : 'white'
				},
				paddingTop : function(i, node){
					return 5;
				},
				paddingBottom : function(i, node){
					return 5;
				}
	  		}
		};

		var tableHeader = [];
		var rowWidth = [];
		var allRows = [];
		//Add Header
		for(var i = 0; i < fields.length; i++){
			var field = fields[i];
			if(field.show){
				tableHeader.push({
					text : this.localize(field.locale),
					style : 'tableHeader'
				})
				rowWidth.push(field.width ? field.width : 'auto');
			}
		}
		this.getActualColumnWidth(rowWidth);

		//Add Row
		var groupMaps = {
			_default : []
		};
		for(var i = 0; i < records.length; i++){
			var record = records[i];
			var groupName = '_default';
			if(groupConfig && groupConfig.groupField){
				groupName = record[groupConfig.groupField];
				if(!groupMaps.hasOwnProperty(groupName))
					groupMaps[groupName] = [];
			}

			var row = [];
			for(var j = 0; j < fields.length; j++){
				var field = fields[j];

				if(field.show){
					var fieldRawValue = field.mapping ? record[field.mapping] : record[field.name];
					var value = Ext.isFunction(field.convert) ? field.convert.apply(this, [fieldRawValue, record]) : fieldRawValue;

					//Mising required field will display empty row instead
					if(!value && field.required){
						row = [{
							text : field.errorDisplay,
							colSpan : tableHeader.length,
							alignment : 'center'
						}]
						break;
					} else
						row.push(value ? this.wrapLongName(value, rowWidth[row.length]) : 'N/A');
				}
			}
			groupMaps[groupName].push(row);
		}
		for(var g in groupMaps){
			if(g != '_default'){
				allRows.push([{
					text : g,
					colSpan : tableHeader.length,
					style : 'tableGroup'
				}]);
			}
			if(groupMaps[g].length > 0)
				allRows = allRows.concat(groupMaps[g]);
		}

		return [{
        	text : partTitle,
        	style : ["partHeader"]
      	},{
      		layout : 'resolveTable',
      		margin : [0,5,0,0],
      		table : {
      			headerRows : 1,
      			widths : rowWidth,
      			body : [tableHeader].concat(allRows)
      		}
      	}]
	},
	getActualColumnWidth : function(widths){
		var pageSizeInfo = {
			A0: [2383.94, 3370.39],
			A1: [1683.78, 2383.94],
			A2: [1190.55, 1683.78],
			A3: [841.89, 1190.55],
			A4: [595.28, 841.89],
			A5: [419.53, 595.28]
		}
		var pageWidth = pageSizeInfo[this.CONST.PAGE_SIZE][this.CONST.PAGE_DIMENSION == 'landscape' ? 1 : 0] - this.CONST.PAGE_MARGIN[1] - this.CONST.PAGE_MARGIN[3] - 45;
		var autoSizeEntry = [];
		widths.forEach(function(w,i){
			if(!isNaN(w)){
				pageWidth -= w;
			}
			else
				autoSizeEntry.push(i);
		})

		autoSizeEntry.forEach(function(i){
			widths[i] = Math.max(0 , parseInt(pageWidth / autoSizeEntry.length));
		})
	},
	wrapLongName : function(name, thresholdWidth){
		name = name.toString();
		var maxCharPerWord = parseInt(thresholdWidth / this.CONST.AVERAGE_CHAR_WIDTH);
		var regex = new RegExp('[^\\s]' +'{' + maxCharPerWord + '}','gi');
		name = name.replace(regex, function(matched, index){
			return matched + ' ';
		})
		return name;
	},
	cancel : function(){
		this.doClose();
	}
})

glu.defModel('RS.incident.ExportAuditLog', {
    sir: ' ',
    sirList: '',
    sirIsValidated: true,
    initiallyRendered: true,
    exporting: false,
    sirIsDulicate: false,
    fileType: 'pdf',
    outputFileName: 'ResolveSIRAuditLogs-' + Ext.Date.format(new Date(), 'Ymd-His'),
    twoWaySirNameToIdMap: {},
    sirStore: {
        mtype: 'store',
        fields: ['id', 'sir'],
        pageSize: 25,
        sorters: [{
            property: 'sir',
            direction: 'DESC'
        }],

        proxy: {
            type: 'ajax',
            url: '/resolve/service/playbook/securityIncident/list',
            reader: {
                type: 'json',
                root: 'records'
            },
            listeners: {
                exception: function(e, resp, op) {
                    clientVM.displayExceptionError(e, resp, op);
                }
            }
        }
    },

    fileTypeStore: {
        mtype: 'store',
        fields: ['fileType'],
        data: [{fileType: 'pdf'}, {fileType: 'json'}, {fileType: 'csv'}]
    },

    auditlogStore: {
        mtype: 'store',
        model: 'RS.incident.model.AuditLog',
        pageSize : 1000000, // Don't support Audit log with > million records
        sorters: [{
            property: 'incidentId',
            direction: 'DESC'
        }]
    },
    applyOrgFilter: function(store, op) {
        op.params = op.params || {};
        var curFilter = op.params.filter || '[]';
        if (typeof(curFilter) == 'string') {
            curFilter = Ext.decode(curFilter);
        }
        op.params = Ext.apply(op.params, {
            filter: Ext.encode([{
                field: 'sysOrg',
                type: 'auto',
                condition: 'equals',
                value: clientVM.orgId || 'nil'
            }].concat(curFilter))
        });
    },
    applyFilters: function(store, op) {
        var sirs = this.sirList.split(', ') || [];
        var sirList = this.twoWaySirNameToIdMap[sirs[0]];
        for (var i=1; i<sirs.length; i++) {
            sirList = sirList + ',' + this.twoWaySirNameToIdMap[sirs[i]];
        }
        op.params = op.params || {};
        var curFilter = op.params.filter || '[]';
        if (typeof(curFilter) == 'string') {
            curFilter = Ext.decode(curFilter);
        }
        op.params = Ext.apply(op.params, {
            filter: Ext.encode([{
                field: 'incidentId',
                type:  'terms',
                condition: 'equals',
                value: sirList
            }].concat(curFilter))
        });
    },

    init: function() {
        this.sirStore.on('load', this.validateSirName, this);
        this.sirStore.on('beforeload', this.applyOrgFilter, this);
        this.auditlogStore.on('beforeload', this.applyFilters, this);
    },

    sirIsValid$: function() {
        if (this.isSirAlreadyAdded()) {
            return this.sir + this.localize('isAlreadyOnSIRList');
        }
        var isValid = !!this.sir && !!this.sir.length && this.sirIsValidated;
        return isValid? isValid: this.localize('inValidSirName');
    },

    handleSirSelect: function() {
        this.set('sirIsValidated', true);
    },

    validateSirName: function(store) {
        var r = store.findRecord('sir', this.sir, 0, false, false, true);
        this.set('sirIsValidated', r? true: false);
    },

    prepareRecordsForPDF: function(records) {
        var toBeExported = {};
        for (var i=0; i<records.length; i++) {
            var rec = records[i];
            var sirId = rec.get('incidentId');
            if (!toBeExported[sirId]) {
                toBeExported[sirId] = {
                    sir: this.twoWaySirNameToIdMap[sirId],
                    items: []
                };
            }
            toBeExported[sirId].items.push([
                rec.get('sysCreatedBy'),
                rec.get('ipAddress'),
                rec.get('description'),
                Ext.util.Format.date(new Date(rec.get('sysCreatedOn')), 'm/d/Y H:i:s')
            ]);

        }
        return toBeExported;
    },

    prepareRecords: function(records) {
        var toBeExported = [];
        for (var i=0; i<records.length; i++) {
           var rec = records[i];
           var sirId = rec.get('incidentId');
           toBeExported.push({
               'SIR Name': this.twoWaySirNameToIdMap[sirId],
               'SIR Id': sirId,
               'User': rec.get('sysCreatedBy'),
               'IP Address': rec.get('ipAddress'),
               'Description': rec.get('description'),
               'Logged On': Ext.util.Format.date(new Date(rec.get('sysCreatedOn')), 'm/d/Y H:i:s')
           });
        }
        return toBeExported;
    },

    exportAll: function(sir) {
        this.auditlogStore.load({
            scope: this,
            callback: function(records, operation, success) {
               if (success) {
                   this.set('exporting', true);
                   switch (this.fileType) {
                       case 'pdf':
                           this.exportPDF(this.prepareRecordsForPDF(records));
                           break;
                       case 'json':
                           this.exportJSON(this.prepareRecords(records));
                           break;
                       case 'csv':
                           this.exportCSV(this.prepareRecords(records));
                           break;
                       default:
                           this.exportPDF(this.prepareRecordsForPDF(records));
                           break;
                   }
                   this.close();
               }
        }});
    },

    cancel: function() {
        this.doClose();
    },

    isSirAlreadyAdded: function() {
        return Ext.Array.indexOf(this.sirList.split(', ') || [], this.sir) != -1;
    },

    addIsEnabled$: function() {
        // Only enabled if valid and non-duplicate sir
        this.sirIsDulicate = this.isSirAlreadyAdded();
        return ((this.sir||'').trim() && this.sirIsValidated && !this.sirIsDulicate);
    },

    add: function() {
        var newSirList = this.sirList + ', ' + this.sir;
        var sirId = this.sirStore.findRecord('sir', this.sir).get('id');
        this.set('sirList', newSirList);
        this.twoWaySirNameToIdMap[this.sir] = sirId
        this.twoWaySirNameToIdMap[sirId] = this.sir;
        this.set('sir', ' ');
    },

    getFileWithExtension: function() {
       return  this.outputFileName + '.' + this.fileType;
    },

    exportPDF: function(toBeExported) {
        var fileWithExtension = this.getFileWithExtension();
        var createdBy = clientVM.user.name;
        var createdOn = Ext.Date.format(new Date(), 'Y-m-d H:i:s');
        var content = [];
        for (var sirId in toBeExported) {
            content.push(
                ' ',
                {
                    text: [
                        {text: 'SIR Name: ', style: 'tableHeader'}, { text: this.twoWaySirNameToIdMap[sirId] + ', ', fontSize: 13},
                        {text: 'SIR ID: ', style: 'tableHeader'}, {text: sirId, fontSize: 13}
                    ]
                },

                {table: {
                    headerRows: 1,
                    body: [
                        [{text: 'User', style: 'columnHeader'}, {text: 'IP Address', style: 'columnHeader'}, {text: 'Description', style: 'columnHeader'}, {text: 'Logged On', style: 'columnHeader'}]
                    ].concat(toBeExported[sirId].items)
                }
            });
        }
        content.unshift(
            {text: 'Resolve System', style: 'rsheader'},
            {text: 'Security Incident Response Audit Log', style: 'header'},
            {text: [{text: 'Created By: ', style: 'subheaderlabel'},  {text: createdBy, fontSize: 14}]},
            {text: [{text: 'Created On: ', style: 'subheaderlabel'},  {text: createdOn, fontSize: 14}]}
        );
        var doc = {
            content: content,
            styles: {
                rsheader: {
                    fontSize: 20,
                    bold: true,
                    color: '#6c3',
                    alignment: 'center'
                },
                header: {
                    fontSize: 16,
                    bold: true,
                    alignment: 'center'
                },
                subheaderlabel: {
                    fontSize: 14,
                    bold: true
                },
                columnHeader: {
                    fillColor : '#3d3d3d',
                    color : 'white',
                    bold: true
                },
                tableHeader: {
                    bold: true,
                    fontSize: 13
                }
            },
            defaultStyle: {
                fontSize: 10,
                alignment: 'left'
            }
        };
        pdfMake.createPdf(doc).download(fileWithExtension);
    },

    exportJSON: function(toBeExported) {
        var blob = new Blob([Ext.JSON.encode(toBeExported)], {type : 'application/json'});
        downloadjs(blob, this.getFileWithExtension(), "text/plain"
        );
    },

    exportCSV: function(toBeExported) {
        var csv = Papa.unparse(toBeExported);
        downloadjs(csv, this.getFileWithExtension(), "text/plain");
    }
});
glu.defModel('RS.incident.ExportDashboard', {
    fileType: 'CSV',
    outputFileName: 'ResolveInvestigationList-' + Ext.Date.format(new Date(), 'Ymd-His'),  
    fileTypeStore: {
        mtype: 'store',
        fields: ['fileType'],
        data: [{fileType: 'PDF'}, {fileType: 'JSON'}, {fileType: 'CSV'}]
    }, 
    userFilter : [],
    fields : [{
        name : 'sir',
        locale : 'sir',
        show : true,
        width : 85          
    },{
        name : 'playbook',
        locale : 'playbook',
        show : true,
        width : 'auto'          
    },{
        name : 'title',
        locale : 'title',
        show : true,
        width : '*'         
    },{
        name : 'sourceSystem',
        locale : 'sourceSystem',
        show : true,
        width : 70          
    },{
        name : 'id',
        locale : 'id',
        show : true,
        width : 220
    },{
        name : 'investigationType',
        locale : 'investigationType',
        show : true,
        width : 60
    },{
        name : 'severity',
        locale : 'severity',
        show : true,
        width : 60
    },{
        name : 'status',
        locale : 'status',
        show : true,
        width : 60
    },{
        name : 'sla',
        locale : 'sla',
        show : true,
        width : 70,
        convert: function(val){
            var days = 0;
            var hours = 0;
            if (val) {
                // 86400 = n_seconds_per_day
                // 3600 = n_seconds_per_hour
                days = Math.floor(val/86400);
                hours = Math.floor(val%86400)/3600;
            }
            if (days || hours) {
                return Ext.String.format('{0} Days {1} Hours', days, hours);
            } else {
                return 'None';
            }
        }
    },{
        name : 'dueBy',
        locale : 'dueBy',
        show : true,
        width : 70,
        convert : function(v){
            return v ? Ext.util.Format.date(new Date(v), clientVM.getUserDefaultDateFormat()) : v;
        }
    },{
        name : 'owner',
        locale : 'owner',
        show : true,
        width : 70
    },{
        name : 'sysCreatedOn',
        locale : 'sysCreatedOn',
        show : true,
        width : 70,
        convert: function(val) {
            return (val === null) ? '' : Ext.util.Format.htmlEncode(Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat()))
        }
    },{
        name : 'closedOn',
        locale : 'closedOn',
        show : true,
        width : 70
    },{
        name: 'closedBy',
        locale: '~~closedBy~~',
        show : true,
        width : 70
    }, {
        name: 'sysOrg',
        locale: 'sysOrg',       
        show : true,
        width : 70,
        convert: function(value) {
            if (value) {
                if(!this.orgStore) {
                    this.orgStore = {};
                    clientVM.user.orgs.forEach(function(e) {
                        if(e.uname != 'None')
                            this.orgStore[e.id] = e.uname;
                    }, this);
                }
                return this.orgStore[value];
            }
            return '';
        }
    }],

    init: function() {
        var me = this;
        this.set('columnSelectionStore', Ext.create('Ext.data.Store',{         
            fields : [{
                name: 'name',
                convert: function(v, r){
                    return r.raw.name;
                }
            },{
                name: 'display',
                convert: function(v, r){
                    return me.localize(r.raw.name);
                }
            }]      
        }))
        this.columnSelectionStore.loadData(this.fields);
    },
    getFileWithExtension: function() {
       return  this.outputFileName + '.' + this.fileType.toLowerCase();
    },
    cancel: function() {
        this.doClose();
    },  
    exportAll: function(sir) {
        this.ajax({
            url: '/resolve/service/playbook/securityIncident/list',
            method: 'GET',
            params : {
                page: 1,
                start: 0,
                limit: -1,
                sort: JSON.stringify([{direction:'DESC',property:'sysCreatedOn'}]),
                filter: JSON.stringify(this.userFilter)
            },
            success: function(resp){
                var response = RS.common.parsePayload(resp);
                if (response.success) {                   
                   switch (this.fileType) {
                        case 'PDF':
                           this.exportPDF(response.records);
                           break;
                        case 'JSON':
                           this.exportJSON(this.prepareRecords(response.records));
                           break;
                        case 'CSV':
                           this.exportCSV(this.prepareRecords(response.records));
                           break;                      
                   }
                   this.close();
               }
           }
       })       
    },

    //PDF 
    CONST : {
        PAGE_SIZE : 'A4',
        PAGE_DIMENSION: 'landscape',
        PAGE_MARGIN : [20,20,20,20],
        AVERAGE_CHAR_WIDTH : 6.5
    },    
    columnSelectionStore : null,
    entrySelections: [], 
    fileTypeIsNotPDF$ : function(){
        return this.fileType != 'PDF';
    },
    exportPDF: function(records){
        var selectedFields = [];
        Ext.Array.forEach(this.entrySelections, function(entry){
            selectedFields.push(entry.get('name'));
        })  
        var fields = this.fields;
        Ext.Array.forEach(fields, function(entry){
            if(selectedFields.indexOf(entry.name) != -1)
                entry.show = true;
            else
                entry.show = false;
        })  
        var fragment = this.getTableFragments('activities', '', fields, records , null);
        var PDFContent = {
            //pageSize: this.CONST.PAGE_SIZE,
            pageMargins: this.CONST.PAGE_MARGIN,
            pageOrientation: this.CONST.PAGE_DIMENSION,
            content :
            [{
                text : 'Investigations',
                style : ['incidentTitle']
            },fragment],
            styles : {
                partHeader : {
                    bold: true,
                    fontSize: 15,
                    margin : [0,10,0,0]
                },
                incidentTitle : {
                    bold: true,
                    fontSize: 20,
                    margin : [0,0,0,5]
                },
                tableHeader : {
                    bold : true,
                    fillColor : '#3d3d3d',
                    color : 'white'
                },
                tableGroup : {
                    color : 'white',
                    fillColor : '#777777',
                    alignment : 'center'
                }
            }
        }                   
        var currentDate = new Date();
        var formatedDate = (currentDate.getMonth() + 1) + '-' + (currentDate.getDate() < 10 ? '0' + currentDate.getDate() : currentDate.getDate() ) + '-' + currentDate.getFullYear();
        pdfMake.createPdf(PDFContent).download(this.outputFileName + '.pdf');       
    },      
    getTableFragments : function(dataPartName, partTitle, fields, records, groupConfig){
        if(!records || records.length == 0){
            return [{
                text : partTitle,
                style : ["partHeader"]
            },this.localize(dataPartName + 'StoreIsEmpty')]
        }

        //Table Style
        pdfMake.tableLayouts = {
            resolveTable: {
                hLineColor: function (i) {
                    return '#3d3d3d';
                },
                vLineColor: function (i) {
                    return '#3d3d3d';
                },
                fillColor: function (i, node) {
                    return i % 2 == 0 ? '#dadada' : 'white'
                },
                paddingTop : function(i, node){
                    return 5;
                },
                paddingBottom : function(i, node){
                    return 5;
                }
            }
        };

        var tableHeader = [];
        var rowWidth = [];
        var allRows = [];
        //Add Header
        for(var i = 0; i < fields.length; i++){
            var field = fields[i];
            if(field.show){
                tableHeader.push({
                    text : this.localize(field.locale),
                    style : 'tableHeader'
                })
                rowWidth.push(field.width ? field.width : 'auto');
            }
        }
        this.getActualColumnWidth(rowWidth);

        //Add Row
        var groupMaps = {
            _default : []
        };
        for(var i = 0; i < records.length; i++){
            var record = records[i];
            var groupName = '_default';
            if(groupConfig && groupConfig.groupField){
                groupName = record[groupConfig.groupField];
                if(!groupMaps.hasOwnProperty(groupName))
                    groupMaps[groupName] = [];
            }

            var row = [];
            for(var j = 0; j < fields.length; j++){
                var field = fields[j];

                if(field.show){
                    var fieldRawValue = field.mapping ? record[field.mapping] : record[field.name];
                    var value = Ext.isFunction(field.convert) ? field.convert.apply(this, [fieldRawValue, record]) : fieldRawValue;

                    //Mising required field will display empty row instead
                    if(!value && field.required){
                        row = [{
                            text : field.errorDisplay,
                            colSpan : tableHeader.length,
                            alignment : 'center'
                        }]
                        break;
                    } else
                        row.push(value ? this.wrapLongName(value, rowWidth[row.length]) : 'N/A');
                }
            }
            groupMaps[groupName].push(row);
        }
        for(var g in groupMaps){
            if(g != '_default'){
                allRows.push([{
                    text : g,
                    colSpan : tableHeader.length,
                    style : 'tableGroup'
                }]);
            }
            if(groupMaps[g].length > 0)
                allRows = allRows.concat(groupMaps[g]);
        }

        return [{
            text : partTitle,
            style : ["partHeader"]
        },{
            layout : 'resolveTable',
            margin : [0,5,0,0],
            table : {
                headerRows : 1,
                widths : rowWidth,
                body : [tableHeader].concat(allRows)
            }
        }]
    },
    getActualColumnWidth : function(widths){
        var pageSizeInfo = {
            A0: [2383.94, 3370.39],
            A1: [1683.78, 2383.94],
            A2: [1190.55, 1683.78],
            A3: [841.89, 1190.55],
            A4: [595.28, 841.89],
            A5: [419.53, 595.28]
        }
        var pageWidth = pageSizeInfo[this.CONST.PAGE_SIZE][this.CONST.PAGE_DIMENSION == 'landscape' ? 1 : 0] - this.CONST.PAGE_MARGIN[1] - this.CONST.PAGE_MARGIN[3] - 45;
        var autoSizeEntry = [];
        widths.forEach(function(w,i){
            if(!isNaN(w)){
                pageWidth -= w;
            }
            else
                autoSizeEntry.push(i);
        })

        autoSizeEntry.forEach(function(i){
            widths[i] = Math.max(0 , parseInt(pageWidth / autoSizeEntry.length));
        })
    },
    wrapLongName : function(name, thresholdWidth){
        name = name.toString();     
        var maxCharPerWord = parseInt(thresholdWidth / this.CONST.AVERAGE_CHAR_WIDTH);
        var regex = new RegExp('[^\\s]' +'{' + maxCharPerWord + '}','gi');      
        name = name.replace(regex, function(matched, index){
            return matched + ' ';
        })      
        return name;
    },

    //CSV and JSON
    prepareRecords: function(records) {
        var preparedRecords = [];
        for(var i = 0; i < records.length; i++){
            var record = records[i];
            var dataEntry = {};
            for(var j = 0; j < this.fields.length; j++){
                var fieldProperty = this.fields[j];
                var rawValue = record[fieldProperty.name];
                var value = Ext.isFunction(fieldProperty.convert) ? fieldProperty.convert.apply(this, [rawValue]) : rawValue;
                dataEntry[this.localize(fieldProperty.locale)] = value;
            }
            preparedRecords.push(dataEntry);
        }
        return preparedRecords;
    },
    exportJSON: function(toBeExported) {
        var blob = new Blob([Ext.JSON.encode(toBeExported)], {type : 'application/json'});
        downloadjs(blob, this.getFileWithExtension(), "text/plain");
    },
    exportCSV: function(toBeExported) {
        var csv = Papa.unparse(toBeExported);
        downloadjs(csv, this.getFileWithExtension(), "text/plain");
    }   
});
glu.defModel('RS.incident.Dashboard.KPI', {
	viewMode: false,
	activeItem: 0,
	bannerText: '',
	dashboardrefreshRate: 'Never',
	refreshRateStore: {
		mtype: 'store',
		fields: ['rate'],
		data: [
			{rate: '1 min'},
			{rate: '5 min'},
			{rate: '10 min'},
			{rate: '15 min'},
			{rate: 'Never'}
		],
		proxy: {
			type: 'memory'
		}
	},
	kpiView: {
		mtype: 'RS.incident.Dashboard.KPIView'
	},
	kpiLayout: {
		mtype: 'RS.incident.Dashboard.KPILayout'
	},
	init: function() {
		this.set('activeItem', 0);
		this.set('viewMode', true);
		this.set('bannerText', this.localize('irDashboard'));
	},

	exitEdit: function() {
		this.set('activeItem', 0);
		this.set('viewMode', true);
		this.set('bannerText', this.localize('irDashboard'));
		this.kpiView.loadChartSettings();
	},

	configureSettings: function() {
		this.set('activeItem', 1);
		this.set('viewMode', false);
		this.set('bannerText', this.localize('irDashboardBuilder'));
		this.kpiLayout.loadLayout();
	},

	activate: function() {
		this.kpiView.loadChartSettings();
	},

	addSection: function() {
		this.kpiLayout.addSection();
	},

	addSectionIsEnabled$: function() {
		return this.kpiLayout.layoutSections.length < 3;
	},

	save: function() {
		this.kpiLayout.save();
	},

	refreshRateMap: {
		'1 min': 60,
		'5 min': 300,
		'10 min': 600,
		'15 min': 900
	},

	refreshRateChange: function(newRate, oldRate) {
		this.stopAutoRefresh();
		if (newRate != this.localize('never')) {
			this.startAutoRefresh(this.refreshRateMap[newRate]);
		}
	},

	manualRefresh: function() {
		this.kpiView.refresh();
	},

	doRefresh: function() {
		this.kpiView.refresh();
	},

	startAutoRefresh: function(interval) {
		if (this.autoRefreshTask) {
			this.autoRefreshTask.destroy();
		}

		var runner = new Ext.util.TaskRunner();
		this.autoRefreshTask = runner.newTask({
			run: this.doRefresh,
			scope: this,
			interval: (interval || 30)* 1000
		});
		this.autoRefreshTask.start();
	},

	stopAutoRefresh: function() {
		if (this.autoRefreshTask) {
			this.autoRefreshTask.destroy();
			delete this.autoRefreshTask;
		}
	},

	refreshSettings: function() {
		this.kpiLayout.refresh();
	}
});

glu.defModel('RS.incident.Dashboard.KPILayout', {
	layoutSections: {
		mtype: 'list',
		autoParent: true
	},
	init: function() {
	},

	loadLayout: function() {
		this.layoutSections.removeAll();
		var me = this;
		['11', '12', '13', '21', '22', '23', '31', '32', '33'].forEach(function(id) {
			RS.incident.model.ChartSettings.load(id, {
				scope: this,
				callback: function(record) {
					if (record) {
						me.createKPISettingFrom(record);
					}
				}
			});
		});
		if (!this.layoutSections.length) {
			this.createDefaultLayout();
		}
	},

	createNewGraph: function(id) {
		var dataSourceMap = {
			'11': this.localize('investigationByType'),
			'12': this.localize('investigationBySeverity'),
			'13': this.localize('investigationByAge'),
			'21': this.localize('investigationByTeamMember'),
			'22': this.localize('investigationByStatus'),
			'23': this.localize('investigationOverTimes'),
			'31': this.localize('investigationBySLA'),
			'32': this.localize('investigationByType'),
			'33': this.localize('investigationByAge')
		};
		var graphTypeMap = {
			'11': this.localize('pieChart'),
			'12': this.localize('barGraph'),
			'13': this.localize('pieChart'),
			'21': this.localize('barGraph'),
			'22': this.localize('pieChart'),
			'23': this.localize('barGraph'),
			'31': this.localize('pieChart'),
			'32': this.localize('barGraph'),
			'33': this.localize('pieChart')
		};
		var record = new RS.incident.model.ChartSettings({
			id: id,
			chartType: dataSourceMap[id],
			graphType: graphTypeMap[id]
		});
		record.save();
		this.createKPISettingFrom(record);
	},

	createDefaultLayout: function() {
		var me = this;
		['11', '12', '13', '21', '22', '23'].forEach(function(id) {
			me.createNewGraph(id);
		});
	},

	getSectionById: function(sectionId) {
		for (var i=0; i<this.layoutSections.length; i++) {
			var section = this.layoutSections.getAt(i);
			if (section.sectionId == sectionId) {
				return section;
			}
		}
		return this.createSection(sectionId);
	},

	createKPISettingFrom: function(record) {
		var section = this.getSectionById(record.get('id').toString().charAt(0));
		section.reallyAddKPISetting(record);
	},

	createSection: function(sectionId) {
		var model = this.model({
			mtype : 'RS.incident.Dashboard.KPILayoutSection',
			sectionId: sectionId
		});
		this.layoutSections.add(model);
		return this.layoutSections.getAt(this.layoutSections.length-1);
	},

	getSectionIds: function() {
		var ids = [];
		for (var i=0; i<this.layoutSections.length; i++) {
			ids.push(this.layoutSections.getAt(i).sectionId);
		}
		return ids;
	},

	addSection: function() {
		var ids = ['1', '2', '3'];
		var sectionId = undefined;
		var sectionIds = this.getSectionIds();
		for (var i=0; i<ids.length; i++) {
			if (sectionIds.indexOf(ids[i]) == -1) {
				sectionId = ids[i];
				break;
			}
		}
		var section = this.createSection(sectionId);
		section.addKPIEntry();
	},

	removeSection: function(section) {
		this.layoutSections.remove(section);
	},

	removeAllRecords: function() {
		['11', '12', '13', '21', '22', '23', '31', '32', '33'].forEach(function(id) {
			RS.incident.model.ChartSettings.load(id, {
				callback: function(record) {
					if (record) {
						record.destroy();
					}
				}
			});
		});
	},

	save: function() {
		this.removeAllRecords();
		for (var i=0; i<this.layoutSections.length; i++) {
			this.layoutSections.getAt(i).save();
		}
	},

	refresh: function() {
		for (var i=0; i<this.layoutSections.length; i++) {
			this.layoutSections.getAt(i).refresh();
		}
	}
});

glu.defModel('RS.incident.Dashboard.KPILayoutSection', {
	sectionId: null,
	KPILayoutSectionEntries: {
		mtype: 'list',
		autoParent: true
	},

	init: function() {
	},

	addKPIEntryIsEnabled$: function() {
		return this.KPILayoutSectionEntries.length < 3;
	},

	getGraphIds: function() {
		var ids = [];
		for (var i=0; i<this.KPILayoutSectionEntries.length; i++) {
			ids.push(this.KPILayoutSectionEntries.getAt(i).chartId.charAt(1));
		}
		return ids;
	},

	createGraphFrom: function(record) {
		this.reallyAddKPISetting(record);
	},

	createNewEntry: function(id) {
		var dataSourceMap = {
			'11': this.localize('investigationByType'),
			'12': this.localize('investigationBySeverity'),
			'13': this.localize('investigationByAge'),
			'21': this.localize('investigationByTeamMember'),
			'22': this.localize('investigationByStatus'),
			'23': this.localize('investigationOverTimes'),
			'31': this.localize('investigationBySLA'),
			'32': this.localize('investigationByType'),
			'33': this.localize('investigationByAge')
		};
		var graphTypeMap = {
			'11': this.localize('pieChart'),
			'12': this.localize('barGraph'),
			'13': this.localize('pieChart'),
			'21': this.localize('barGraph'),
			'22': this.localize('pieChart'),
			'23': this.localize('barGraph'),
			'31': this.localize('pieChart'),
			'32': this.localize('barGraph'),
			'33': this.localize('pieChart')
		};
		var record = new RS.incident.model.ChartSettings({
			id: id,
			chartType: dataSourceMap[id],
			graphType: graphTypeMap[id]
		});
		this.createGraphFrom(record);
	},

	addKPIEntry: function() {
		var ids = ['1', '2', '3'];
		var graphIds = this.getGraphIds();
		var graphId = undefined;
		for (var i=0; i<ids.length; i++) {
			if (graphIds.indexOf(ids[i]) == -1) {
				graphId = this.sectionId + ids[i];
				break;
			}
		}
		if (graphId) {
			// Stay tune for more exciting stuffs. Let the big shot do the book keeping
			// and the Graph will be added for real.
			this.createNewEntry(graphId);
		}
		// What a fool, graph isn't really added yet.
	},

	reallyAddKPISetting: function(record) {
		var id = record.get('id').toString();
		if (id.charAt(0) != this.sectionId) {
			console.error('Adding graph to the wrong section');
			return;
		}
		var graphModel = this.model({
			mtype : 'RS.incident.Dashboard.KPILayoutSectionEntry',
			chartId: id,
			chartConfigs: record.getData()
		});
		graphModel.record = record;
		this.KPILayoutSectionEntries.add(graphModel);
	},

	removeGraph: function(graph) {
		this.KPILayoutSectionEntries.remove(graph);
	},

	removeIsEnabled$: function() {
		return this.parentVM.layoutSections.length > 1;
	},

	remove: function() {
		while (this.KPILayoutSectionEntries.length > 0) {
			this.KPILayoutSectionEntries.getAt(0).remove();
		}
		this.parentVM.removeSection(this);
	},

	save: function() {
		for (var i=0; i<this.KPILayoutSectionEntries.length; i++) {
			this.KPILayoutSectionEntries.getAt(i).save();
		}
	},

	refresh: function() {
		for (var i=0; i<this.KPILayoutSectionEntries.length; i++) {
			this.KPILayoutSectionEntries.getAt(i).refresh();
		}
	}
});

glu.defModel('RS.incident.Dashboard.KPILayoutSectionEntry', {
	record: undefined,
	chartId: undefined,
	chartConfigs: undefined,
	chartSetting: {
		mtype: 'RS.incident.Dashboard.ChartSettings',
		autoParent: true,
		layoutContext: true
	},

	init: function() {
		this.chartSetting.chartId = this.chartId;
		this.chartSetting.chartConfigs = this.chartConfigs;
	},

	removeIsEnabled$: function() {
		return this.parentVM.KPILayoutSectionEntries.length > 1;
	},

	remove: function() {
		this.parentVM.removeGraph(this);
	},

	save: function() {
		this.chartSetting.save();
	},

	refresh: function(record) {
		this.chartSetting.refresh(this.record);
	}
});

glu.defModel('RS.incident.Dashboard.KPIView', {
	viewSections: {
		mtype: 'list',
		autoParent: true
	},

	init: function() {
		clientVM.on('orgchange', this.orgChange.bind(this));
	},

	activate: function() {
		this.refresh();
	},

	loadChartSettings: function() {
		var me = this;
		this.viewSections.removeAll();
		['11', '12', '13', '21', '22', '23', '31', '32', '33'].forEach(function(id) {
			RS.incident.model.ChartSettings.load(id, {
				scope: this,
				callback: function(record) {
					if (record) {
						me.createGraphFrom(record);
					}
				}
			});
		});
		if (!this.viewSections.length) {
			this.createDefaultGraphs();
		}
	},

	createNewGraph: function(id) {
		var dataSourceMap = {
			'11': this.localize('investigationByType'),
			'12': this.localize('investigationBySeverity'),
			'13': this.localize('investigationByAge'),
			'21': this.localize('investigationByTeamMember'),
			'22': this.localize('investigationByStatus'),
			'23': this.localize('investigationOverTimes'),
			'31': this.localize('investigationBySLA'),
			'32': this.localize('investigationByType'),
			'33': this.localize('investigationByAge')
		};
		var graphTypeMap = {
			'11': this.localize('pieChart'),
			'12': this.localize('barGraph'),
			'13': this.localize('pieChart'),
			'21': this.localize('barGraph'),
			'22': this.localize('pieChart'),
			'23': this.localize('barGraph'),
			'31': this.localize('pieChart'),
			'32': this.localize('barGraph'),
			'33': this.localize('pieChart')
		};
		var record = new RS.incident.model.ChartSettings({
			id: id,
			chartType: dataSourceMap[id],
			graphType: graphTypeMap[id]
		});
		record.save();
		this.createGraphFrom(record);
	},

	createDefaultGraphs: function() {
		var me = this;
		['11', '12', '13', '21', '22', '23'].forEach(function(id) {
			me.createNewGraph(id);
		});
	},

	getSectionById: function(sectionId) {
		for (var i=0; i<this.viewSections.length; i++) {
			var section = this.viewSections.getAt(i);
			if (section.sectionId == sectionId) {
				return section;
			}
		}
		return this.createSection(sectionId);
	},

	createGraphFrom: function(record) {
		var section = this.getSectionById(record.get('id').toString().charAt(0));
		section.addGraph(record);
	},

	createSection: function(sectionId) {
		var model = this.model({
			mtype : 'RS.incident.Dashboard.KPIViewSection',
			sectionId: sectionId
		});
		this.viewSections.add(model);
		return this.viewSections.getAt(this.viewSections.length-1);
	},

	refresh: function() {
		for (var i=0; i<this.viewSections.length; i++) {
			this.viewSections.getAt(i).refresh();
		}
	},

	orgChange: function() {
		this.refresh();
	},

	beforeDestroyComponent : function() {
		clientVM.removeListener('orgchange', this.orgChange);
	}
});

glu.defModel('RS.incident.Dashboard.KPIViewSection', {
	sectionId: null,
	viewSectionEntries: {
		mtype: 'list',
		autoParent: true
	},

	init: function() {
	},

	addGraph: function(record) {
		var id = record.get('id').toString();
		if (id.charAt(0) != this.sectionId) {
			console.error('Adding graph to the wrong section');
			return;
		}
		var graphModel = this.model({
			mtype : 'RS.incident.Dashboard.KPIViewSectionEntry',
			chartId: id,
			configs: record.getData()
		});
		this.viewSectionEntries.add(graphModel);
	},

	refresh: function() {
		for (var i=0; i<this.viewSectionEntries.length; i++) {
			this.viewSectionEntries.getAt(i).refresh();
		}
	}
});

glu.defModel('RS.incident.Dashboard.KPIViewSectionEntry', {
	chartId: undefined,
	init: function() {
		this.set('chartConfigs', this.configs);
	},

	// Chart refresh monitor flag (to be toggled)
	refreshMonitor: true,
	chartConfigs: {},

	applyChartSettings: function(configs) {
		this.set('chartConfigs', configs);
	},

	chartSettingsHandler: function(e) {
		var btn = Ext.getCmp(e.currentTarget.id);
		var chartConfigs = btn.up('chart').chartConfigs;
		var chartType = chartConfigs.chartType;
		var hideScopeConfig = true;
		var hideAgeBucketConfig = true;
		var hideColumnConfig = true;
		if (chartType == this.localize('investigationByType')) {
			hideScopeConfig =  false;
		} else if (chartType == this.localize('investigationBySeverity')) {
			hideScopeConfig = false;
		} else if (chartType == this.localize('investigationByAge')) {
			hideAgeBucketConfig =  false;
		} else if (chartType == this.localize('investigationBySLA')) {
			hideScopeConfig =  false;
		}  else if (chartType == this.localize('investigationByWorkload')) {
			hideScopeConfig =  false;
		} else if (chartType == this.localize('investigationByOwner')) {
			// Will support later
		} else if (chartType == this.localize('investigationByStatus')) {
		} else if (chartType == this.localize('investigationByPhases')) {
			// Will support later
		} else if (chartType == this.localize('investigationOverTimes')) {
		}
		this.open({
			mtype: 'RS.incident.Dashboard.ChartSettings',
			target: btn.up('panel'),
			chartConfigs: chartConfigs,
			hideScopeConfig: hideScopeConfig,
			hideAgeBucketConfig: hideAgeBucketConfig,
			hideColumnConfig: hideColumnConfig,
			dumper: this.applyChartSettings.bind(this)
		});
	},

	refresh: function() {
		this.set('refreshMonitor', !this.refreshMonitor)
	}
});

glu.defModel('RS.incident.Dashboard.MetricsSetting', {
	target: null,
	store: null,
	slaPolicyStore: {
		mtype: 'store',
		proxy: {
			type: 'memory'
		},
		fields: ['label', 'nDays'],
		data: [{
			label: '1 day',
			nDays: 1
		}, {
			label: '2 days',
			nDays: 2
		}, {
			label: '3 days',
			nDays: 3
		}, {
			label: '4 days',
			nDays: 4
		}, {
			label: '5 days',
			nDays: 5
		}]
	},
	scopeUnitStore: {
		mtype: 'store',
		proxy: {
			type: 'memory'
		},
		fields: ['unit'],
		data: [{
			unit: 'Days'
		}, {
			unit: 'Hours'
		}, {
			unit: 'Minutes'
		}]
	},

	scope: 90,
	scopeUnit: 'Days',
	maxScope: 365,
	fvalid: true,
	tips: '',

	when_scope_unit_change_replace_the_tips: {
		on: ['scopeUnitChanged'],
		action: function() {
			var map = {
				days: this.localize('between1To365days'),
				hours: this.localize('between1To24Hours'),
				minutes: this.localize('between1To120Minutes')
			}
			this.set('tips', map[this.scopeUnit.toLowerCase()]);
		}
	},

	scopeUnitChange: function(rec) {
		var scopeUnit = rec[0].get('unit');
		if (scopeUnit == 'Days') {
			this.set('maxScope', 365);
			this.set('scope', (this.scope > 365)? 365: this.scope);
		} else if (scopeUnit == 'Hours') {
			this.set('maxScope', 24);
			this.set('scope', (this.scope > 24)? 24: this.scope);
		} else if (scopeUnit == 'Minutes') {
			this.set('maxScope', 120);
			this.set('scope', (this.scope > 120)? 120: this.scope);
		}
		this.set('fvalid', !this.fvalid); // Trick to clear invalid flag
	},

	init: function() {
		this.set('tips', this.localize('between1To365days'));
	},

	applyIsEnabled$: function() {
		return (this.scope <= this.maxScope)? true: false;
	},

	apply: function() {
		this.form.updateRecord(this.parentVM.dashboardSettingsStore.getAt(0));
		this.parentVM.doRefresh();
		this.cancel();
	},

	close: function() {
		this.parentVM.metricsSetting = null;
		this.doClose();
	},

	cancel: function() {
		this.close();
	},

	loadForm: function(form) {
		this.form = form;
		form.loadRecord(this.parentVM.dashboardSettingsStore.getAt(0));
	}
});
/*Parent: RS.incident.Playbook*/
glu.defModel('RS.incident.Activities', {

	activitiesView: {
		mtype: 'RS.incident.ActivitiesView'
	},

	isPreview: false,
	isReadOnly: false,
	activityToolBarDisabled: false,
	previewContent: null,

	when_previewContent_change: {
		on: ['previewContentChanged'],
		action: function() {
			if (this.previewContent) {
				this.getPreviewActivities();
			}
		}
	},

	//Audit Log/Update Completion Status
	handleActivitiesEdit: function(record) {
		if ((record.field === 'dueDate') && (record.originalValue !== (Ext.util.Format.date(record.value, clientVM.getUserDefaultDateFormat())))) {
			clientVM.displaySuccess(this.localize('updateActivitySuccess'));
		} else if ((record.field !== 'dueDate') && (record.originalValue !== record.value)) {
			clientVM.displaySuccess(this.localize('updateActivitySuccess'));
		}
	},

	//Grid copy of members store, updated every time parent's memberStore changes via 'refreshMembers' event
	membersStoreGrid: {
		mtype: 'store',
		fields: [
			'name',
			'userId',
		],
		proxy: {
			type: 'memory'
		}
	},

	retrieveMembers: function() {
		this.membersStoreGrid.loadData(
			this.parentVM.membersStore.getRange().map(function(item) {
				return item.getData()
			})
		);
	},

	//Statuses
	statusStore: {
		mtype: 'store',
		fields: ['type'],
		data: [
			{type: 'Open'},
			{type: 'In Progress'},
			{type: 'Complete'},
			{type: 'N/A'}
		],
		proxy: {
			type: 'memory'
		}
	},

	activityTemplateValue : {
		'id' : '',
		'incidentId' : '',
		'altActivityId' : '',
		'activityName' : '',
		'phase' : '',
		'description' : '',
		'status' : '',
		'assignee' : '',
		'dueDate' : '',
		'days' : '',
		'hours' : '',
		'minutes' : '',
		'wikiName' : '',
		'worksheetId' : '',
		'templateActivity' : true,
		'isPreview' : true
	},

	//Load Activities + Store
	activitiesStore: {
		mtype: 'store',
		groupField: 'phase',
		sorters: [],
		sortOnLoad: false,
		fields: [
			'id',
			'incidentId',
			'altActivityId',
			'activityName',
			'phase',
			'description',
			'status',
			'assignee',
			{name: 'dueDate', type: 'date', convert: function(v, r) {return v ? Ext.util.Format.date(new Date(v), clientVM.getUserDefaultDateFormat()) : v}, serialize: function(v) {return v ? (new Date(v)).getTime() : v}},
			'days',
			'hours',
			'minutes',
			'wikiName',
			'worksheetId',
			{
				name: 'templateActivity',
				type: 'bool',
				defaultValue: true
			},
			{
				name: 'isPreview',
				type: 'bool',
				defaultValue: false
			}
		],
		proxy: {
			type: 'ajax',
			method: 'GET',
			url: '/resolve/service/playbook/activity/listByIncidentId',
			reader: {
				type: 'json',
				root: 'records'
			}
		},
		listeners: {
			recordmoved: function(record, dropRec, dropPosition) {
				var data = [];
				record.store.getRange().forEach(function(r) {
					data.push(r.getData());
				});
				Ext.Ajax.request({
					scope: this,
					url: '/resolve/service/playbook/activity/saveAll',
					method: 'POST',
					jsonData: data,
					params: {
						incidentId: record.get('incidentId')
					},
					success: function(resp, opts) {
						var respData = RS.common.parsePayload(resp);
						if (respData.success) {
							clientVM.displaySuccess(this.rootVM.localize('updateActivitySuccess'));
						} else {
							clientVM.displayError(this.rootVM.localize('updateActivityFail'));
						}
					},
					failure: function(resp, opts) {
						clientVM.displayFailure(resp);
					}
				});
			},
			update: function(store, record, operation, modifiedFieldNames, eOpts ) {
				var data = record.getData();
				if (modifiedFieldNames == 'activityName') {
					record.activityNameDirty = true;
				} else {
					// To prevent the backend from double encoding activityName
					data.activityName = Ext.htmlDecode(data.activityName);
				}
				data.dueDate = data.dueDate ? (new Date(data.dueDate)).getTime() : data.dueDate;
				var loadCallback = this.parentVM.calculateProgress.bind(this.parentVM);
				clientVM.ajax({
					type: 'ajax',
					method: 'POST',
					url: '/resolve/service/playbook/activity/update',
					jsonData: data,
					callback: function(opts, success, resp) {
						store.reload({
							callback: loadCallback
						});
					}
				});
			},
			beforeload: function(store, op) {
				op.params = Ext.apply(op.params || {}, {  // IE doesn't support Object.assign
					incidentId: this.parentVM.parentVM.ticketInfo.id
				});
			},
			load: function() {
				this.parentVM.calculateProgress();
			}
		}
	},

	loadActivities: function(onComplete) {
		this.activitiesStore.reload({
			scope: this,
			callback: function(recs, ops, success) {
				if (!success) {
					clientVM.displayError(this.localize('loadActivitiesFail'));
				}

				if (Ext.isFunction(onComplete)) {
					onComplete();
				}
			}
		});
	},

	getPreviewActivities: function() {
		var activities = {};
		this.activitiesStore.removeAll();
		if (this.previewContent) {
			activities = this.previewContent;
		} else if (this.isPreview && Ext.isFunction(this.rootVM.getContent)) {
			activities = this.rootVM.getContent();
		} else {
			return;
		}

		this.set('activityToolBarDisabled', true);

		var activityList = JSON.parse(activities);
		activityList.forEach(function(activity) {
			// SLA in minutes
			var sla = (parseInt(activity.days) * 24 * 60) + (parseInt(activity.hours) * 60) + (parseInt(activity.minutes));
			if (sla) {
				var currentTime = new Date().getTime();
				activity.dueDate = currentTime + (sla * 60 * 1000);	// convert to milliseconds
			}
			// mock status, assignee, etc
			activity.assignee = this.rootVM.sysCreatedBy;
			activity.status = 'Open';
			activity.templateActivity = true;
			activity.isPreview = true;

			var mockActivity = Ext.clone(this.activityTemplateValue);
			Ext.apply(mockActivity, activity)

			this.activitiesStore.insert(this.activitiesStore.getCount(), mockActivity);
		}.bind(this))
	},

	// short-cut icons
	addNotes: function() {
		this.rootVM.notesTab.openNoteModal(true, null, this.activityId, this.activityName);
	},

	addArtifacts: function() {

	},

	addAttachments: function() {
		this.open({
			mtype: 'RS.incident.UploadAttachment',
			incidentId: this.parentVM.ticketInfo.id,
			activityId: this.activityId
		});
	},

	viewAutomationResults: function() {
		this.open({
			mtype : 'RS.incident.AutomationResults',
			activityFilter: this.activityName,
			activityId: this.activityId,
			modal: true,
			modalTitle: this.localize('viewAutomationResultsTitle', this.activityName),
			padding: 15
		});
	},

	activityName: '',
	activityId: '',
	focuschange: function(old, selections) {
		this.set('activityName', selections.get('activityName'));
		this.set('activityId', selections.get('id'));
	},
	itemmouseenter: function(record, item) {
		this.set('activityName', record.get('activityName'));
		this.set('activityId', record.get('id'));
	},

	//Playbook Progress Bar
	playbookProgress: 0,
	calculateProgress: function() {
		var acc = 0;
		var len = this.activitiesStore.getRange().filter(function(activity) {
			return (activity.getData().id !== null) && (activity.getData().status !== 'N/A')
		}).length;
		this.activitiesStore.each(function(record) {
			if (record.data.status === 'Complete') {
				acc = acc + 1
			}
		});
		this.fireEvent('updateProgressBar', (acc / len))
	},

	//Filter Checkbox Handling
	filterType: [],
	applyFilter: function(value) {
		if (this.filterType.length > 0) {
			return (this.filterType.indexOf(value) !== -1) || (value === null);
		} else {
			return false
		}
	},

	handleFilterChange: function(newFilters) {
		var newFiltersArr = [];
		for (key in newFilters) {
			newFiltersArr.push(newFilters[key])
		}
		this.filterType = newFiltersArr;
		this.activitiesStore.filterBy((function(record) {
			return this.applyFilter(record.data.status)
		}).bind(this));
	},

	//Run on tab activation
	tabInit: function() {
		this.loadActivities();
	},

	init: function() {
		this.set('filterType', ['Open', 'In Progress', 'Complete', 'N/A']) //Set default filters
		this.on('refreshMembers', this.retrieveMembers);

		if (this.isPreview) {
			this.getPreviewActivities();
		}
	},

	//Add Activity Modal
	addActivity: function() {
		this.open({
			mtype: 'RS.incident.ActivityModal',
			ticketInfo: this.parentVM.ticketInfo,
		})
	},
});

//Add Activity
//Parent: RS.incident.Activity
glu.defModel('RS.incident.ActivityModal', {
	ticketInfo: {},
	phase: '',
	name: '',
	slaDays: '0',
	slaHours: '0',
	slaMinutes: '0',
	status: '',
	assignee: '',
	wikiName: '',
	wikiNameIsValidated: false,
	description: '',

	//Load Phases + Store
	phasesStore: {
		mtype: 'store',
		fields: ['type'],
	},

	loadPhases: function() {
		var phases = this.parentVM.activitiesStore.getGroups().map(function(group) {
			return {type: group.name}
		});
		this.phasesStore.loadData(phases);
	},

	//Load Runbook + Validation + Store
	runbookStore: {
		mtype: 'store',
		pageSize: 50,
		model: 'RS.incident.model.Runbook',
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wiki/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				},
				load: function(store) {
					var r = store.findRecord('ufullname', this.wikiName, 0, false, false, true);
					this.set('wikiNameIsValidated', r ? true : false);
				}
			}
		}
	},

	validateWikiName: function(v) {
		var r = v.store.findRecord('ufullname', v.getValue(), 0, false, false, true);
		this.set('wikiNameIsValidated', r ? true : false);
	},

	wikiNameIsValid$: function() {
		return !!this.wikiName && !!this.wikiName.length && this.wikiNameIsValidated;
	},

	handleRunbookSelect: function() {
		this.set('wikiNameIsValidated', true);
	},

	searchRunbook: function() {
		var me = this;
		this.open({
			mtype: 'RS.decisiontree.DocumentPicker',
			dumper: {
				dump: function(selections) {
					if (selections.length > 0) {
						me.set('wikiName', selections[0].get('ufullname'));
						me.set('wikiNameIsValidated', true);
					}
				}
			}
		})
	},

	//SLA Human readable date formatting
	calcDaysText$: function() {
		if (parseInt(this.slaDays) === 1) {
			return 'Day'
		} else {
			return 'Days'
		}
	},

	calcHoursText$: function() {
		if (parseInt(this.slaHours) === 1) {
			return 'Hour'
		} else {
			return 'Hours'
		}
	},

	calcMinutesText$: function() {
		if (parseInt(this.slaMinutes) === 1) {
			return 'Minute'
		} else {
			return 'Minutes'
		}
	},



	saveActivityIsEnabled$: function() {
		var notBlank = function(s) {
			return (s && s.trim().length != 0);
		};
		return (notBlank(this.phase) && notBlank(this.name) && notBlank(this.status)
			&& notBlank(this.assignee) && notBlank(this.wikiName) && this.wikiNameIsValidated);
	},

	//Save Activity
	isSaving: false,
	saveActivity: function() {
		this.set('isSaving', true);
		var params = {
			activityName: this.name,
			description: this.description,
			phase: this.phase,
			wikiName: this.wikiName,
			incidentId: this.ticketInfo.id,
			worksheetId: clientVM.problemId,
			days: this.slaDays,
			hours: this.slaHours,
			minutes: this.slaMinutes,
			status: this.status,
			assignee: this.assignee,
		};

		this.ajax({
			url: '/resolve/service/playbook/activity/add',
			method: 'POST',
			jsonData: params,
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					this.parentVM.activitiesStore.loadData(results.records);
					clientVM.displaySuccess(this.localize('addActivitySuccess'));
					this.doClose();
				} else {
					clientVM.displayError(this.localize('addActivityFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	cancel: function() {
		this.doClose();
	},

	init: function() {
		this.loadPhases();
		this.set('assignee', this.ticketInfo.owner);
		this.set('status', 'Open');
	}

});
//Parent: RS.incident.Playbook
glu.defModel('RS.incident.ActivitiesView', {
	activityName: '',
	activityWiki: '',
	dueDate: '',
	phase: '',
	activityId: '',
	incidentId: '',
	csrftoken: '',
	dtView: {
		mtype: 'RS.decisiontree.Main'
	},
	dtDisplaymode: false,

	problemId: '',
	wikiSysId: '',
	mockList: [],
	wikiParams: [],
	paramsComponent: {
		worksheetOptionIsVisible: false,
		columnOptionIsVisible: false,
		mtype: 'RS.wiki.resolutionbuilder.Parameter'
	},

	returnHeader$: function() {
		var returnStr = '';
		if (this.phase) {
			returnStr += this.phase;
		}
		if (this.activityWiki) {
			if (returnStr) {
				returnStr += ': ';
			}
			returnStr += this.activityName;
		}
		if (this.dueDate) {
			if (returnStr) {
				returnStr += ' - ';
			}
			returnStr += 'Due Date: ' + Ext.util.Format.date(new Date(this.dueDate), 'Y-m-d');
		}
		return returnStr
	},

	generateiFrame: function() {
		this.set('showAllNotes', false);
		this.cleanIframes();
		document.getElementById('playbook-iframe').innerHTML = '';
		this.ajax({
			url: '/resolve/service/wiki/get/',
			method: 'POST',
			params: {
				id: '',
				name: this.activityWiki
			},
			scope: this,
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					this.set('wikiSysId', results.data.sys_id);
	
					//Load parameters into grid component
					var paramsArr = (results.data.uwikiParameters === '' || results.data.uwikiParameters === null) ? [] : JSON.parse(results.data.uwikiParameters);
					this.set('wikiParams', paramsArr);
					this.paramsComponent.firstColumnStore.removeAll();
					this.paramsComponent.firstColumnStore.add(paramsArr);
					if (results.data.udisplayMode == 'decisiontree') {
						this.set('dtDisplaymode', true);
						this.dtView.activate(null, {
							name: results.data.ufullname,
							problemId: this.problemId,
							sirContext : true
						});
					} else {
						var params = Ext.String.format('&sir={0}&problemId={1}&activityId={2}&activityName={3}&incidentId={4}',
							this.rootVM.sir, this.problemId, this.activityId, this.activityName, this.incidentId);
						var viewurl = window.location.origin + '/resolve/service/wiki/view?wiki=' + window.encodeURI(results.data.ufullname + params) + '&' + this.csrftoken;
						this.set('dtDisplaymode', false);
						document.getElementById('playbook-iframe').innerHTML = '<iframe id="playbook-wikiview" style="width:100%;height:100%;" marginheight="0" marginwidth="0" frameborder="0" problemId="'+this.problemId+'" activityId="'+this.activityId+'" src="' + viewurl + '"></iframe>';
	
					}
				} else {
					clientVM.displayError(this.localize('generateiFrameFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},
	cleanIframes: function() {
		//Clean up anything like leftover iframes and links
		var frame = document.getElementById('playbook-wikiview');
		if(frame && frame.contentWindow)
			frame.contentWindow.postMessage('beforedestroy', '/');		
	},

	executionWin: null,
	execute: function(button) {
		if (this.executionWin) {
			//If exec window open then close
			this.executionWin.doClose();
		} else {
			var me = this;
			this.executionWin = this.open({
				mtype: 'RS.actiontaskbuilder.Execute',
				target: button.getEl().id,
				fromWiki: true,
				newWorksheet: false,
				activeWorksheet: true,
				inputsLoader: function(paramStore, origParamStore, mockStore) {
					//Sourced from RS.wiki.Main
					me.paramsComponent.firstColumnStore.each(function(param) {
						param.data.value = param.get('sourceName');
						param.data.utype = 'PARAM';
						paramStore.add(param);
						origParamStore.add(param);
					})

					var mockNamesArray = [];
					if (me.mockList) {
						for (var i = 0; i < me.mockList.length; i++) {
							mockNamesArray.push({
								uname: me.mockList[i]
							})
						}
					}
					mockStore.loadData(mockNamesArray);
				},
				sirProblemId: this.problemId,
				activityId: this.activityId,
				executeDTO: {
					wiki: this.activityWiki
				}
			});
			this.executionWin.on('closed', function() {
				this.executionWin = null
			}, this);
		}
	},

	returnToActivities: function() {
		//remove activityId from url hash
		window.location.hash = window.location.hash.split('/')[0] + '/sir=' + this.rootVM.ticketInfo.sir;
		this.rootVM.set('isActivitiesView', false);
		this.set('showAllNotes', false);
	},

	showAllNotes: false,
	notes: function() {
		this.set('showAllNotes', !this.showAllNotes);
		if (this.showAllNotes) {
			clientVM.fireEvent('showAllNotes');
		} else {
			clientVM.fireEvent('hideAllNotes');
		}
	},

	notesIsPressed$: function() {
		return this.showAllNotes;
	},

	init: function() {
		this.initToken();
		clientVM.getResultMacroLimit();
	},

	activate: function() {
		this.initToken();
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/wiki/view', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this))
	}
});
//Parent: RS.incident.Playbook
glu.defModel('RS.incident.Artifacts', {
	API : {
		removeArtifact : '/resolve/service/playbook/artifact/delete'
	},
	artifactsSelections: [],	
	csrftoken: '',
	init: function() {
		this.initToken();
	},
	activate: function() {
		this.initToken();
	},
	initToken: function() {
		this.set('csrftoken', clientVM.rsclientToken);
	},
	artifactsStore: {
		mtype: 'store',		
		sorters: [{
			property: 'name',
			direction: 'ASC'
		}],
		fields: [
			'id',
			'name',
			{
				name : 'encodedValue',
				convert: function(v, record){
					return Ext.String.htmlEncode(record.get('value'));
				}
			},{
				name : 'encodedDescription',
				convert: function(v, record){
					return Ext.String.htmlEncode(record.get('description'));
				}
			},
			'value',
			'description',
			'worksheetId',
			'incidentId',
			{name: 'sysCreatedOn', type: 'int', convert: function(field) {return Ext.util.Format.date(new Date(field), clientVM.getUserDefaultDateFormat())}},
			{name: 'sysUpdatedOn', type: 'int', convert: function(field) {return Ext.util.Format.date(new Date(field), clientVM.getUserDefaultDateFormat())}},
			'sysCreatedBy',
			'sysUpdatedBy',{
				name : 'sirList',
				convert: function(val, record){
					var result = '';
					for(var i = 0; i < val.length; i++){
						result += '<a target=_blank href="/resolve/jsp/rsclient.jsp?=' + this.csrftoken + '#RS.incident.Playbook/sir=' + val[i] + '">' + val[i] + '</a>&nbsp;';
					}
					return result ? result : 'None';
				}
			}
		],
		proxy: {
			type: 'ajax',
			method: 'GET',
			url: '/resolve/service/playbook/artifact/listBySir',
			reader: {
				type: 'json',
				root: 'records'
			}
		},
		listeners: {
			beforeload: function(store, op) {
				op.params = Ext.apply(op.params || {}, {
					sir: this.rootVM.ticketInfo.sir
				});
			}
		}
	},	
	tabInit: function() {
		this.loadArtifacts();
	},
	loadArtifacts: function() {
		this.artifactsStore.load();
	},
	addArtifact: function() {
		this.open({
			mtype: 'RS.incident.ArtifactModal',
			sir: this.parentVM.ticketInfo.sir,
			dumper : {
				dump : function(artifact){
					this.artifactsStore.add(artifact);
				},
				scope : this
			}		
		});
	},
	removeArtifact: function() {
		this.message({
			title: this.localize('removeArtifactTitle'),
			msg: this.localize('removeArtifactBody'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('confirm'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.reallyRemoveArtifact()
			}
		});
	},
	reallyRemoveArtifact: function() {
		var artifacts = [];
		this.artifactsSelections.forEach(function(r) {
			var d = r.getData();
			artifacts.push({
				id: d.id,
				name: d.name,
				value: d.value
			});
		}, this);
		this.ajax({
			url: this.API['removeArtifact'],
			method: 'POST',		
			jsonData: {
				artifacts: artifacts
			},
			params: {incidentId: this.parentVM.ticketInfo.id},
			success: function(resp) {
				var response = RS.common.parsePayload(resp);
				if (response.success) {
					clientVM.displaySuccess(this.localize('artifactDeleteSuccess'));
					this.artifactsStore.remove(this.artifactsSelections);
					this.close();
				} else {
					clientVM.displayError(this.localize('artifactDeleteFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},
	removeArtifactIsEnabled$: function() {
		return (this.artifactsSelections.length > 0);
	},
	lookupIncidentByArtifact : function(record){
		this.open({
			mtype : 'IncidentLookupPopup',
			artifactName : record.get('name'),
			artifactValue : record.get('value')
		})
	},
	actionHandler : function(record){
		this.open({
			mtype : 'ArtifactAction',
			key : record.get('name'),
			value : record.get('value'),
			sirProblemId : this.rootVM.ticketInfo.problemId
		})
	}
});

//Artifact Modal
glu.defModel('RS.incident.ArtifactModal', {
	API : {
		saveArtifact : '/resolve/service/playbook/artifact/save',
		getAlias : '/resolve/service/at',
		getCEF : '/resolve/service/cef'
	},
	fields : ['ushortName','value','description'],
	type: '',	
	isSaving: false,
	artifactTypeStore : {
		mtype : 'store',
		fields : ['uname']
	},	
	aliasStore : {
		mtype : 'store',
		fields : ['ushortName']
	},
	availableAlias : [],
	when_type_changed : {
		on : ['typeChanged'],
		action : function(v){
			var selectedAlias = null;
			this.availableAlias.forEach(function(entry){
				if(entry.uname == v){
					selectedAlias = entry;
					return false;
				}
			})
			this.aliasStore.loadData(selectedAlias ? selectedAlias.dictionaryItems : []);
			var firstAlias = this.aliasStore.getAt(0);
			this.set('ushortName', firstAlias ? firstAlias.get('ushortName') : '');
		}
	},
	ushortNameIsValid$ : function(){
		return this.ushortName && this.isKeyValid(this.ushortName) ? true : this.localize('keyReqs');
	},
	valueIsValid$ : function(){
		return this.value ? true : this.localize('valueReqs');
	},
	isKeyValid : function(key){
		return this.aliasStore.find('ushortName', key, 0, false, false, true) != -1;
	},
	init: function() {
		this.ajax({
			url : this.API['getAlias'],
			success : function(resp){
				var response = RS.common.parsePayload(resp);
				if(response.success){
					var assignedTypeAlias = [];
					for(var i = 0; i < response.data.length;i++){
						var entry = response.data[i];
						this.availableAlias.push(entry);
						assignedTypeAlias = assignedTypeAlias.concat(entry.dictionaryItems);						
					}
					this.ajax({
						url : this.API['getCEF'],
						success : function(resp){
							var response = RS.common.parsePayload(resp);
							if(response.success){
								var unknownType = {
									uname : 'UNKNOWN',
									dictionaryItems : []
								}
								this.availableAlias.push(unknownType);
								var allCEFs = response.data || [];
								//Eliminate all alias that already assigned to a group. Might be slow if cef expands in future.
								allCEFs.forEach(function(cef){
									var found = false;
									assignedTypeAlias.forEach(function(assignedCEF){
										if(assignedCEF.ushortName == cef.ushortName){
											found = true;
											return false
										}									
									})
									if(!found)
										unknownType['dictionaryItems'].push(cef);								
								})
								this.artifactTypeStore.loadData(this.availableAlias);
								var firstType = this.artifactTypeStore.getAt(0);
								this.set('type', firstType ? firstType.get('uname') : '');
							}
							else
								clientVM.displayError(response.message);
						},
						failure: function(resp) {
							clientVM.displayFailure(resp);
						}
					})
				}
				else
					clientVM.displayError(response.message);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})	
	},
	saveArtifact: function() {
		if(!this.isValid){
			this.set('isPristine', false);
			return;
		}
		this.set('isSaving', true);
		this.ajax({
			url: this.API['saveArtifact'],
			method: 'POST',
			jsonData: {
				sir: this.sir,
				name: this.ushortName,
				value: this.value,
				description: this.description,			
			},
			success: function(resp) {
				var response = RS.common.parsePayload(resp);
				if (response.success) {
					clientVM.displaySuccess(this.localize('artifactAddSuccess'));
					if(this.dumper && Ext.isFunction(this.dumper.dump)){
						this.dumper.dump.apply(this.dumper.scope, [response.records[0]]);
					}
					this.close();
				}
				else
					clientVM.displayError(this.localize('artifactAddFail'));
				
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	saveArtifactIsEnabled$: function() {
		return ((this.type.length !== 0) && (this.value.length !== 0))
	},	
	cancel: function() {
		this.doClose();
	}	
});
glu.defModel('RS.incident.Attachments', {
	csrftoken: '',
	api: {
		upload: '/resolve/service/playbook/attachment/upload',
		downloadViaDocName: '/resolve/service/playbook/attachment/download?attachId={0}&{1}',
		list: '/resolve/service/playbook/attachment/listBySir?incidentId=', //Shim wiki upload manager but also send incidentId because attachment/listBySir needs it
		update: '/resolve/service/playbook/attachment/update'
	},
	ticketInfo: {},
	incidentId: 'must_be_initialized_with_something', //will change after assigned playbookid

	wait: false,
	when_wait_changed: {
		on: ['waitChanged'],
		action: function() {
			var deleteBtn = this.view.down('button[name="deleteAttachment"]');
			if (this.wait) {
				deleteBtn.setDisabled(true);
			} else if (this.attachmentsSelections.length) {
				deleteBtn.setDisabled(false);
			}
		}
	},

	store: {
		mtype: 'store',
		fields: ['id','name','description','malicious','size'].concat(RS.common.grid.getSysFields()),
	},

	keyword: '',
	allowDownload: true,

	tabInit: function() {
		this.reload();
	},

	view: null,
	initView: function(v) {
		this.set('view', v);
	},

	init: function() {
		this.initToken();
		this.store.on('update', function(store, record, operation, modifiedFieldNames) {
			if (modifiedFieldNames == 'description' || modifiedFieldNames == 'malicious') {
				this.updateAttachment(record.getData());
			}
		}.bind(this));
	},

	activate: function() {
		this.initToken();
	},

	initToken: function() {
		clientVM.getCSRFToken_ForURI('/resolve/service/playbook/attachment/download', function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this))
	},

	close: function() {
		this.doClose();
	},

	reload: function() {
		this.store.removeAll();
		var incidentId = this.ticketInfo.id || this.incidentId;
		this.ajax({
			url: this.api.list + incidentId,
			params: {
				docSysId: '',
				docFullName: incidentId
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					Ext.each(respData.records, function(record) {
						this.store.add({
							id: record.id,
							name: record.name,
							description: record.description,
							size: record.size,
							malicious: record.malicious,
							sysCreatedOn: record.sysCreatedOn,
							sysCreatedBy: record.sysCreatedBy
						});
					}, this);
				} else {
					clientVM.displayError(respData.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	addAttachment: function() {
		this.open({
			mtype: 'RS.incident.UploadAttachment',
			url: this.api.upload,
			incidentId: this.incidentId
		});
	},

	attachmentsSelections: [],
	selectionChanged: function(v, selections) {
		this.set('attachmentsSelections', selections);
		var deleteBtn = v.up().down('button[name="deleteAttachment"]');
		if (selections.length) {
			deleteBtn.setDisabled(!this.rootVM.pSirInvestigationViewerAttachmentsDelete);
		} else {
			deleteBtn.setDisabled(true);
		}
	},

	deleteAttachment: function() {
		this.message({
			title: this.localize('deleteAttachment'),
			msg: this.localize('deleteAttachmentWarningMsg'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAttachment'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.reallydeleteAttachment()
			}
		});
	},
	reallydeleteAttachment: function() {
		var attachments = [];
		this.attachmentsSelections.forEach(function(r) {
			var d = r.getData();
			attachments.push({
				id: d.id,
				name: d.name,
				value: d.value
			});
		}, this);
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/playbook/attachment/delete',
			method: 'POST',
			scope: this,
			jsonData: {
				attachments: attachments
			},
			params: {
				incidentId: this.incidentId
			},
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					this.reload();
					clientVM.displaySuccess(this.localize('attachmentsDeleteSuccess'));
					//this.close();
				} else {
					clientVM.displayError(this.localize('attachmentsDeleteFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},
	updateAttachment: function(data) {
		var attachments = [];
		attachments.push({
			id: data.id,
			name: data.name,
			malicious: data.malicious,
			description: data.description
		});
		this.ajax({
			url: this.api.update,
			method: 'POST',
			scope: this,
			jsonData: {
				attachments: attachments
			},
			params: {
				incidentId: this.incidentId
			},
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					this.reload();
					clientVM.displaySuccess(this.localize('attachmentsUpdateSuccess'));
				} else {
					clientVM.displayError(this.localize('attachmentsUpdateFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	}
});

/*
//Parent: RS.incident.Playbook
glu.defModel('RS.incident.Attachments', {
	ticketInfo: {},
	incidentId: 'must_be_initialized_with_something', //will change after assigned playbookid

	api: {
		upload: '/resolve/service/playbook/attachment/upload',
		downloadViaDocName: '/resolve/service/playbook/attachment/download?attachId={1}',
		list: '/resolve/service/playbook/attachment/listBySir?incidentId=' + 'will_change' //Shim wiki upload manager but also send incidentId because attachment/listBySir needs it
	},

	wait: false,
	when_wait_changed: {
		on: ['waitChanged'],
		action: function() {
			var deleteBtn = this.view.down('button[name="deleteAttachment"]');
			if (this.wait) {
				deleteBtn.setDisabled(true);
			} else {
				deleteBtn.setDisabled(false);
			}
		}
	},

	init: function() {
		this.set('wait', false);
	},

	activityFilter: 'All',

	view: null,
	initView: function(v) {
		this.set('view', v);
		var me = this;
		var toolbar = v.down('button[name="uploadFile"]').ownerCt;
		toolbar.addCls('rs-dockedtoolbar');
		Ext.Array.each(toolbar.items.items, function (btn) {
			btn.addCls('rs-btn-light rs-small-btn');
		});

		// insert
		toolbar.insert(0, [{
			xtype: 'button',
			disabled: !this.rootVM.pSirInvestigationViewerAttachmentsCreate,
			text: this.localize('upload'),
			name: 'addAttachment',
			cls: 'rs-btn-light rs-small-btn',
			maxWidth: 80,
			handler: function() {
				me.addAttachment();
			}
		}, {
			xtype: 'button',
			text: this.localize('deleteAttachment'),
			name: 'deleteAttachment',
			cls: 'rs-btn-light rs-small-btn',
			maxWidth: 80,
			disabled: true,
			handler: function() {
				me.deleteAttachment();
			}
		}]);

		var incidentId = this.ticketInfo.id || this.incidentId;
		this.set('api', {
			upload: '/resolve/service/playbook/attachment/upload',
			downloadViaDocName: '/resolve/service/playbook/attachment/download?attachId={1}',
			list: '/resolve/service/playbook/attachment/listBySir?incidentId=' + incidentId  //Shim wiki upload manager but also send incidentId because attachment/listBySir needs it
		});
		this.on('reloadAttachments', function () {
			v.loadFileUploads(true) //Force reload
		});
		var store = v.store;
		store.on('filesuploaded', function(files) {
			if (files && files.length) {
				var filenames = [];
				files.forEach(function (file) {
					filenames.push(file.name);
				});
				clientVM.displaySuccess(me.localize('attachmentsUploadSuccess'));
			}
		});
		v.on('filesadded', function(uploader, files) {
			me.fireEvent('reloadAttachments');
			clientVM.displayError(me.localize('attachmentsUploadNotAllowed'));
		});
		store.on('update', function(store, record, operation, modifiedFieldNames) {
			if (modifiedFieldNames == 'description' || modifiedFieldNames == 'malicious') {
				me.updateAttachment(record.getData());
			}
		});
	},
	tabInit: function() {
		this.fireEvent('reloadAttachments');
	},

	addAttachment: function() {
		this.open({
			mtype: 'RS.incident.UploadAttachment',
			url: this.api.upload,
			incidentId: this.incidentId
		});
	},

	deleteAttachment: function() {
		this.message({
			title: this.localize('deleteAttachment'),
			msg: this.localize('deleteAttachmentWarningMsg'),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAttachment'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				this.reallydeleteAttachment()
			}
		});
	},
	reallydeleteAttachment: function() {
		var attachments = [];
		this.attachmentsSelections.forEach(function(r) {
			var d = r.getData();
			attachments.push({
				id: d.id,
				name: d.name,
				value: d.value
			});
		}, this);
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/playbook/attachment/delete',
			method: 'POST',
			scope: this,
			jsonData: {
				attachments: attachments
			},
			params: {
				incidentId: this.incidentId
			},
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					this.fireEvent('reloadAttachments');
					clientVM.displaySuccess(this.localize('attachmentsDeleteSuccess'));
					//this.close();
				} else {
					clientVM.displayError(this.localize('attachmentsDeleteFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		});
	},

	attachmentsSelections: [],
	selectionChanged: function(v, selections) {
		this.set('attachmentsSelections', selections);
		var deleteBtn = v.down('button[name="deleteAttachment"]');
		if (selections.length) {
			deleteBtn.setDisabled(!this.rootVM.pSirInvestigationViewerAttachmentsDelete);
		} else {
			deleteBtn.setDisabled(true);
		}
	},

	updateAttachment: function(data) {
		var attachments = [];
		attachments.push({
			id: data.id,
			name: data.name,
			malicious: data.malicious,
			description: data.description
		});
		this.ajax({
			url: '/resolve/service/playbook/attachment/update',
			method: 'POST',
			scope: this,
			jsonData: {
				attachments: attachments
			},
			params: {
				incidentId: this.incidentId
			},
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					this.fireEvent('reloadAttachments');
					clientVM.displaySuccess(this.localize('attachmentsUpdateSuccess'));
				} else {
					clientVM.displayError(this.localize('attachmentsUpdateFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	}
});
*/

//Parent: RS.incident.Playbook
glu.defModel('RS.incident.AuditLog', {
	auditlogStore: {
		mtype: 'store',
		model: 'RS.incident.model.AuditLog',
		pageSize : 25,
		//remoteSort: true,
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		listeners: {
			beforeload: function(store, op) {
				op.params = Ext.apply(op.params || {}, {
					filter: '[{' +
						'"field":"incidentId",' +
						'"type":"auto",' +
						'"condition":"equals",' +
						'"value":"' + this.parentVM.parentVM.ticketInfo.id + '"}]'
				});
			}
		}
	},

	loadAuditLog: function() {
		this.auditlogStore.reload({
			scope: this,
			callback: function(recs, ops, success) {
				if (!success) {
					clientVM.displayError(this.parentVM.localize('auditlogLoadFail'));
				}
			}
		});
	},

	tabInit: function() {
		this.loadAuditLog();
	},

	init: function() {
	},

	exportAuditlog: function() {
		var twoWaySirNameToIdMap = {};
		var sirId = this.rootVM.ticketInfo.id;
		var sirName = this.rootVM.ticketInfo.sir;
		twoWaySirNameToIdMap[sirName] = sirId;
		twoWaySirNameToIdMap[sirId] = sirName;

		this.open({
			mtype: 'RS.incident.ExportAuditLog',
			sirList: this.rootVM.ticketInfo.sir,
			twoWaySirNameToIdMap: twoWaySirNameToIdMap
		});
	}
});
glu.defModel('RS.incident.AutomationResults',{
	activityFilter : 'All',
	activityId: null,
	resultList : {
		mtype : 'list'
	},
	fields : ['title', 'order','refreshInterval','refreshCountMax','filter','showWiki','descriptionWidth',{   
    	name : 'autoHide',
    	type : 'boolean'
    },{   
    	name : 'autoCollapse',
    	type : 'boolean'
    },{   
    	name : 'encodeSummary',
    	type : 'boolean'
    },{   
    	name : 'progress',
    	type : 'boolean'
    },{   
    	name : 'selectAll',
    	type : 'boolean'
    },{   
    	name : 'includeStartEnd',
    	type : 'boolean'
    },{   
    	name : 'preserveTaskOrder',
    	type : 'boolean'
    },{
    	name : 'actionTasks',
    	type : 'raw'
    }],
    when_activity_filter_changed : {
    	on : ['activityFilterChanged'],
    	action : function(newVal){
			this.refreshAutomationResults();
    	}
    },
	init: function() {
		if (this.modal) {
			this.updateInfo(this.rootVM.resultsInfo);
		}
	},
	updateInfo : function(info){
		var resultMacro = {
			mtype : "RS.wiki.macros.Results2",
			sirProblemId: info['sirProblemId'],
			activityId: null,
			isModal: this.isModal,
			showHeader : false
		}

		//Convert string to correct type
		Ext.apply(resultMacro , this.asObject(this.loadData(info['automationFilter'])));
		this.resultList.removeAll();
		this.resultList.add(this.model(resultMacro));

		this.updateActivityMap();
		this.refreshAutomationResults();
	},
	getResultMacro : function(){
		return this.resultList.getAt(0);
	},
	getCurrentData : function(){
		return this.getResultMacro().getCurrentData();
	},
	refresh : function(){
		var macro = this.getResultMacro();
		if(macro) {
			macro.refresh(true);
		}
	},
	activityIdMap: {},
	tabInit: function() {
		this.parentVM.loadActivities(function() {
			this.updateActivityMap();
			this.refreshAutomationResults();
		}.bind(this));
	},
	updateActivityMap : function(){
		this.activityIdMap = {};
		var activities = this.parentVM.getActivityList();
		activities.filter(function(activity) {			
			this.activityIdMap[activity.id] = activity.activityName;
		}.bind(this));
	},
	refreshAutomationResults: function() {
		var macro = this.getResultMacro();
		if(macro){
			var activityId = null;
			var wikiName = this.rootVM.activitiesTab.activitiesStore.getRange().filter(function(activity) {
				return activity.get('activityName') === this.activityFilter
			}.bind(this)).map(function(activity) {
				activityId = activity.get('id')
				return activity.get('wikiName')
			})[0];
			macro.set('showWiki', this.activityFilter == 'All' ? '' : wikiName);

			if (this.activityId) {
				macro.set('activityId', this.activityId);
			} else if (activityId) {
				macro.set('activityId', activityId);
			} else {
				macro.set('activityId', null);
			}
			macro.refresh(true);
		}
	},

	modal: false,
	padding: 0,
	close: function() {
		this.doClose()
	},
	isModal$: function(){
		return this.modal;
	},
})
glu.defModel('RS.incident.IncidentLookup',{
	mixins : ['IncidentLookupUtil']
})
glu.defModel('RS.incident.IncidentLookupPopup',{
	mixins : ['IncidentLookupUtil'],
	noRecordFound : false,
	init : function(){		
		this.incidentStore.on('datachanged', function(){			
			if(this.incidentStore.getCount() == 0)
				this.set('noRecordFound', this.incidentStore.getCount() == 0);
		}, this);
		this.incidentStore.pageSize = 5;
		this.incidentStore.load();
	},	
	noRecordFoundText$ : function(){
		return this.localize('noRecordFound', [Ext.String.htmlEncode(this.artifactName), Ext.String.htmlEncode(this.artifactValue)]);
	},
	expandResultInTab : function(){
		clientVM.handleNavigation({
			modelName: 'RS.incident.IncidentLookup',
			params: {
				artifactName: this.artifactName,
				artifactValue: this.artifactValue,				
			},
			target: '_blank'
		})
	}
})
glu.defModel('RS.incident.IncidentLookupUtil',{
	title : 'Incident Lookup',
	artifactName : '',
	artifactValue : '',
	lookupInfo : '',
	incidentStore: {
		mtype : 'store',
		remoteSort: true,
		pageSize : 50,
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		fields: [
			'id',
			'sir',
			'title',
			'externalReferenceId',
			'investigationType',
			'type',
			'severity',
			'description',
			'sourceSystem',
			'playbook',
			'externalStatus',
			'status',
			'owner',		
			'playbookVersion',
			'sequence',
			'closedOn',
			'closedBy',
			'primary',
			'sysCreatedOn',
			'sirStarted',
			'problemId',
			{name: 'sys_id', mapping: 'id'}
		],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/playbook/artifact/getIncidentsByArtifact',
			reader: {
				type: 'json',
				root: 'records'
			}
		}	
	},
	initMixin : function(){
		this.incidentStore.on('beforeload', function(store, operation){
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				filter: Ext.encode([{
					"field":"name",
					"type":"string",
					"condition":"equals",
					"caseSensitive" : true,
					"value": this.artifactName
				},{
					"field":"value",
					"type":"string",
					"condition":"equals",
					"caseSensitive" : true,
					"value": this.artifactValue
				}])
			});
		},this)
		this.incidentStore.on('datachanged', function(){
			this.set('lookupInfo', this.localize('lookupInfo', [this.incidentStore.getTotalCount(), Ext.String.htmlEncode(this.artifactName), Ext.String.htmlEncode(this.artifactValue)]));
		}, this);
	},
	activate : function(screen, params){
		this.set('artifactName', params['artifactName'] || '');
		this.set('artifactValue', params['artifactValue'] || '');
		this.incidentStore.load();
	}
})
//Parent: RS.incident.Playbook
glu.defModel('RS.incident.Notes', {
	//Load Notes + Store
	notesStore: {
		mtype: 'store',
		//remoteSort: true,
		//pageSize: 25,
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		fields: [
			'postedBy',
			'note',
			{
				name: 'fullNote',
				mapping: 'note',
				convert: function(val) {
					return Ext.String.htmlEncode(val).replace(/(\r\n|\n|\r)/gm, "<br />");
				}
			},
			'activityName',
			'activityId',
			'phase',
			'worksheetId',
			'incidentId',
			'id',
			{name: 'sysCreatedOn', type: 'int', convert: function(field) {return Ext.util.Format.date(new Date(field), clientVM.getUserDefaultDateFormat())}},
			//{name: 'sysUpdatedOn', type: 'int', convert: function(field) {return Ext.util.Format.date(new Date(field), clientVM.getUserDefaultDateFormat())}},
			'sysCreatedBy',
			//'sysUpdatedBy',
		],
		proxy: {
			type: 'ajax',
			method: 'GET',
			//url: '/resolve/service/playbook/note/listBySir',
			url: '/resolve/service/playbook/note/list',
			reader: {
				type: 'json',
				root: 'records'
			}
		},
		listeners: {
			beforeload: function(store, op) {
				op.params = Ext.apply(op.params || {}, {
					//incidentId: this.parentVM.parentVM.ticketInfo.id //change to sir
					filter: '[{"field":"incidentId","type":"string","condition":"equals","value":"' + this.rootVM.ticketInfo.id + '"}]'
				});
			}
		}
	},

	loadNotes: function() {
		this.notesStore.reload({
			scope: this,
			callback: function(recs, ops, success) {
				if (!success) {
					clientVM.displayError(this.localize('notesLoadFail'));
				}
			}
		});
	},

	//Get note info when selected on grid
	selectedNote: '',
	selectedNoteActivityId: null,
	selectNote: function(record) {
		this.set('selectedNote', record.getId());
		this.set('selectedNoteActivityId', record.get('activityId'));
	},

	addNote: function() {
		this.openNoteModal(true);
	},

	editNote: function() {
		if (this.selectedNoteActivityId === null) {
			this.openNoteModal(false, this.selectedNote);
		} else {
			this.rootVM.set('activeTab', this.rootVM.activeTabMap.ACTIVITIES);
			clientVM.handleNavigation({
				modelName: 'RS.incident.Playbook',
				params: {
					sir: this.rootVM.ticketInfo.sir,
					activityId: this.selectedNoteActivityId
				}
			})
		}
	},

	editNoteIsEnabled$: function() {
		return (this.selectedNote !== '')
	},

	editButtonText: '',
	editButtonSize: 0,

	editOrGoTo$: function() {
		if (this.selectedNoteActivityId === null) {
			this.set('editButtonText', this.localize('edit'));
		} else {
			this.set('editButtonText', this.localize('goToActivity'));
		}
	},

	tabInit: function() {
		this.loadNotes();
	},

	init: function() {
	},

	openNoteModal: function(isNew, noteId, activityId, activityName) {
		if (isNew) {
			this.open({
				mtype: 'RS.incident.NoteModal',
				incidentId: this.parentVM.ticketInfo.id,
				loadNotes: this.loadNotes.bind(this),
				isNew: isNew,
				activityId: activityId,
				activityName: activityName
			});
		} else {
			var noteObj = this.notesStore.getById(noteId).getData();
			this.open({
				mtype: 'RS.incident.NoteModal',
				incidentId: this.parentVM.ticketInfo.id,
				loadNotes: this.loadNotes.bind(this),
				isNew: isNew,
				note:  Ext.htmlDecode(noteObj.note),
				id: noteObj.id
			});
		}
	}
});

//Parent: RS.incident.Notes
glu.defModel('RS.incident.NoteModal', {
	isNew: true,
	note: '',
	id: '',
	incidentId: '',
	isSaving: false,

	saveNote: function() {
		this.set('isSaving', true);
		var params = {};
		if (this.isNew) {
			params.note = this.note;
			params.category = ''; //change to phase
			params.incidentId = this.incidentId;
			params.activityId = this.activityId;
			params.activityName = this.activityName;
		} else {
			params.id = this.id;
			params.note = this.note;
			params.incidentId = this.incidentId;
		}
		this.ajax({
			url: '/resolve/service/playbook/note/save',
			method: 'POST',
			jsonData: params,
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					window.setTimeout(this.loadNotes, 1000);
					//Audit Log
					if (!this.isNew) {
						clientVM.displaySuccess(this.localize('noteEditSuccess'));
					} else {
						clientVM.displaySuccess(this.localize('noteAddSuccess'));
					}
					this.close();
				} else {
					var message = (this.isNew) ? this.localize('noteAddFail') : this.localize('noteEditFail');
					clientVM.displayError(message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	saveNoteIsEnabled$: function() {
		return this.note.length !== 0
	},

	returnTitle$: function() {

		if (this.isNew) {
			return (this.activityId) ? this.localize('addNoteTitle', [this.activityName]) : this.localize('addNote');
		} else {
			return this.localize('editNote');
		}
	},

	saveButtonText: '',

	editOrAdd$: function() {
		if (this.isNew) {
			this.set('saveButtonText', this.localize('add'));
		} else {
			this.set('saveButtonText', this.localize('edit'));
		}
	},

	cancel: function() {
		this.doClose();
	},

	init: function() {
	}
});
//Parent: RS.incident.Playbook
glu.defModel('RS.incident.Overview', {
	referenceId: '',
	alertId: '',
	correlationId: '',
	readOnlyDataCompromised: true,
	readOnlyStatus: true,
	readOnlyTeamMebers: true,

	referenceIdIsBlank$: function() {
		return this.referenceId === ''
	},

	alertIdIsBlank$: function() {
		return this.alertId === ''
	},

	correlationIdIsBlank$: function() {
		return this.correlationId === ''
	},

	noRelatedIsVisible$: function() {
		return !((this.referenceId === '') && (this.alertId === '') && (this.correlationId === ''))
	},

	owner: '',

	dataCompromisedStore: {
		mtype: 'store',
		fields: ['display', 'value'],
		data: [
			{display: 'Unknown', value: 'Unknown'},
			{display: 'Yes', value: 'Yes'},
			{display: 'No', value: 'No'},
		],
		proxy: {
			type: 'memory'
		}
	},

	statusStore: {
		mtype: 'store',
		fields: ['display', 'value'],
		data: [
			{display: 'Open', value: 'Open'},
			{display: 'In Progress', value: 'In Progress'},
			{display: 'Complete', value: 'Complete'},
			{display: 'N/A', value: 'N/A'}
		],
		proxy: {
			type: 'memory'
		}
	},

	updateStatus: function(newValue, oldValue) {
		var modifiedTicketInfo = Object.assign({}, this.parentVM.ticketInfo);
		modifiedTicketInfo.status = newValue;

		//Orgs Info
		Ext.apply(modifiedTicketInfo, {
			'sysOrg' : clientVM.orgId
		})
		this.ajax({
			url: '/resolve/service/playbook/securityIncident/save',
			method: 'POST',
			jsonData: modifiedTicketInfo,
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					var status = results.data.status;
					this.parentVM.set('ticketInfo', results.data);
					clientVM.displaySuccess(this.localize('changeStatusSuccess'));
				} else {
					clientVM.displayError(this.localize('changeStatusFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	updateDataCompromised: function(newValue, oldValue) {
		var modifiedTicketInfo = Object.assign({}, this.parentVM.ticketInfo);
		modifiedTicketInfo.dataCompromised = newValue;

		//Orgs Info
		Ext.apply(modifiedTicketInfo, {
			'RESOLVE.ORG_ID' : clientVM.orgId,
			'RESOLVE.ORG_NAME' : clientVM.orgName
		})
		this.ajax({
			url: '/resolve/service/playbook/securityIncident/save',
			method: 'POST',
			jsonData: modifiedTicketInfo,
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					var dataCompromised = results.data.dataCompromised;
					this.parentVM.set('ticketInfo', results.data);
					clientVM.displaySuccess(this.localize('changeDataCompromisedSuccess'));
				} else {
					clientVM.displayError(this.localize('changeDataCompromisedFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	//Copy of members in Overview to trigger formatMembersList formula
	membersCopy: [],
	membersList: {
		mtype: 'list'
	},

	ownerObject: {},

	formatMembersList$: function() {
		this.membersList.removeAll();
		if (this.membersCopy.length !== 0) {
			var formattedArray = this.membersCopy.forEach(function(memberObj, index) {
				var memberName = (memberObj.get('name').trim().length === 0) ? memberObj.get('userId') : memberObj.get('name');
				var userId = memberObj.get('userId');
				if (userId !== this.owner) {
					this.membersList.insert(index + 1, {
						mtype: 'RS.incident.MemberLabel',
						userId: (userId.length > 40) ? userId.slice(0, 40) + '...' : userId, //userId
						memberName: memberName
					});
				}
			}, this);
		} else {
			this.membersList.add({
				mtype: 'RS.incident.MemberLabel',
				userId: 'None',
				memberName: 'None'
			})
		}
	},

	loadRelatedInvestigations: function() {
		this.ajax({
			url: '/resolve/service/worksheet/getWS',
			method: 'GET',
			params: {
				id: this.rootVM.ticketInfo.problemId,
			},
			scope: this,
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					if (results.data) {
						this.set('referenceId', results.data.reference);
						this.set('alertId', results.data.alertId);
						this.set('correlationId', results.data.correlationId);
					}
				} else {
					clientVM.displayError(this.localize('loadRelatedInvestigationsFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	applyRBAC: function()  {
		this.set('readOnlyDataCompromised', this.parentVM.isPreview || !this.rootVM.pSirInvestigationViewerDataCompromisedModify);
		this.set('readOnlyStatus', this.parentVM.isPreview || !this.rootVM.pSirInvestigationViewerStatusModify);
		this.set('readOnlyTeamMebers', this.parentVM.isPreview || !this.rootVM.pSirInvestigationViewerTeamModify);
	},

	init: function() {
		this.rootVM.on('applyrbac', this.applyRBAC.bind(this));
	},

	addMember: function() {
		this.open({
			mtype: 'RS.incident.MemberModal',
			loadMembers: this.parentVM.loadMembers.bind(this.parentVM, this.parentVM.ticketInfo.id),
		});
	}
});

//Parent: RS.incident.Overview
glu.defModel('RS.incident.MemberModal', {
	canAddMembers: true,
	memberSelections : [],
	ownerSelection: '',

	selectedMembersStore: {
		mtype: 'store',
		fields: ['userId', 'displayName'],
	},

	populateList: function() {
		var owner = this.rootVM.ticketInfo.owner || '';
		var secArr = this.rootVM.securityGroupStore.getRange().map(function(item) {
			var member = item.getData();
			if (member.userId !== owner) {
				return member
			}
		}, this).filter(function(notUndefined) {return notUndefined}); //filter out the owner object returning undef from map
		var selectedIndexes = [];
		var membersArr = (this.rootVM.ticketInfo.members || '').split(', '); //remove owner from array list and convert to arr from str
		this.selectedMembersStore.loadData(secArr.map(function(member, index) {
			if (member.userId !== owner) {
				if (membersArr.indexOf(member.userId) !== -1) {
					selectedIndexes.push(index);
				}
			 	return {
					userId: member.userId,
					displayName: member.name
				}
			}
		}, this).filter(function(notUndefined) {return notUndefined})); //filter out the owner object returning undef from map
		this.selectedMembersStore.sort('userId', 'ASC');
		this.fireEvent('selectMember', selectedIndexes);
	},

	saveMembers: function() {
		var modifiedTicketInfo = Object.assign({}, this.rootVM.ticketInfo);
		modifiedTicketInfo.members = this.memberSelections.map(function(member) {
			return member.get('userId');
		});
		//modifiedTicketInfo.members.unshift(this.rootVM.ticketInfo.owner); //Append admin to beginning of list
		modifiedTicketInfo.members = modifiedTicketInfo.members.join(', ');  //create arr str
		modifiedTicketInfo.owner = this.ownerSelection;

		//Orgs Info
		Ext.apply(modifiedTicketInfo, {
			'RESOLVE.ORG_ID' : clientVM.orgId,
			'RESOLVE.ORG_NAME' : clientVM.orgName
		})
		this.ajax({
			url: '/resolve/service/playbook/securityIncident/save',
			method: 'POST',
			jsonData: modifiedTicketInfo,
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					this.loadMembers();
					this.parentVM.set('owner', this.ownerSelection);
					this.rootVM.set('ticketInfo', modifiedTicketInfo);
					clientVM.displaySuccess(this.localize('addMemberSuccess'));
					this.doClose();
				} else {
					clientVM.displayError(this.localize('addMemberFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},

	cancel: function() {
		this.doClose();
	},

	init: function() {
		this.set('ownerSelection', this.rootVM.ticketInfo.owner);
	}
});

//Allow for member tooltips
glu.defModel('RS.incident.MemberLabel', {
	userId: '',
	memberName: ''
});

glu.defModel('RS.incident.PDFConverter',{
	mixinAPI : {
		getAttachments : '/resolve/service/playbook/attachment/listBySir',
		getWS : '/resolve/service/worksheet/getWS',
		getNote : '/resolve/service/playbook/note/list',
		getArtifact : '/resolve/service/playbook/artifact/listBySir',
		getImageInfo : '/resolve/service/wiki/getSIRAttachments'
	},
	dataParts : {
		summary : null,
		description : null,
		notes : null,
		artifacts : null,
		activities : null,
		attachments : null,
		automationResults : null
	},
	convertingProgessTask : null,
	imageIdInfo : {},
	csrftoken: '',
	preparePDF : function(){
		//Get all image id that used in description
		this.ajax({
			url: this.mixinAPI['getImageInfo'],
			method: 'GET',
			params: {
				docSysId: '',
				docFullName: this.ticketInfo.playbook,
				incidentId: null,
				sir: this.ticketInfo.sir
			},
			scope: this,
			success: function(resp) {
				var response = RS.common.parsePayload(resp);
				if(response.success && response.records){
					for(var i = 0; i < response.records.length; i++){
						var r =response.records[i];
						this.imageIdInfo[r.fileName] = r.id;
					}
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
		
		var fullName = this.ticketInfo.playbook.split('.');
		var uri = Ext.String.format('/resolve/service/wiki/download/{0}/{1}', fullName[0], fullName[1]);
		clientVM.getCSRFToken_ForURI(uri, function(token_pair) {
			this.set('csrftoken', token_pair[0] + '=' + token_pair[1]);
		}.bind(this))
	},
	clearPDFPart : function(){
		Ext.apply(this.dataParts, {
			summary : null,
			description : null,
			notes : null,
			artifacts : null,
			activities : null,
			attachments : null,
			automationResults : null
		})
	},
	exportPDF : function(){
		this.clearPDFPart();
		var msgBox = this.message({
			title : this.localize('convertPDFTitle'),
			msg : this.localize('convertPDFMsg'),
			closable : false,
			buttonText : {
				ok : this.localize('abort')
			},
			scope : this,
			fn : function(btn){
				this.abortConverting();
			}
		})
		//Summary
		this.getSummaryFragments(this.ticketInfo);

		//Description
		this.getDescriptionFragments(this.ticketInfo.description);

		//Activities
		this.getActivitiesFragments();

		//Notes
		this.getNoteFragments();

		//Artifacts
		this.getArtifactFragments();

		//Attachments
		this.getAttachmentFragments();

		//Automation Result
		this.getAutomationResultFragments();

		var taskStartTime = new Date().getTime();
		this.convertingProgessTask = setInterval(function(){
			var completed = true;
			for(var k in this.dataParts){
				if(this.dataParts[k] == null){
				completed = false
					return;
				}
			}
			if(completed){
				var pdfFile = this.getPDFContent();
				//console.log(JSON.stringify(pdfFile));
				var currentDate = new Date();
				var formatedDate = (currentDate.getMonth() + 1) + '-' + (currentDate.getDate() < 10 ? '0' + currentDate.getDate() : currentDate.getDate() ) + '-' + currentDate.getFullYear();
				pdfMake.createPdf(pdfFile).download(this.ticketInfo.title + ' (' + formatedDate + ').pdf');
				this.abortConverting();
				msgBox.doClose();
			}
			if(taskStartTime == taskStartTime + 1000 * 60)
				this.abortConverting();
		}.bind(this), 1000)
	},
	abortConverting : function(){
		this.clearPDFPart();
		if(this.convertingProgessTask)
			clearInterval(this.convertingProgessTask);
	},
	CONST : {
		PAGE_SIZE : 'A4',
		PAGE_DIMENSION : 'portrait',
		PAGE_MARGIN : [20,20,20,20],
		AVERAGE_CHAR_WIDTH : 6.5
	},
	getPDFContent : function(){
		return {
			pageSize: this.CONST.PAGE_SIZE,
   			pageMargins: this.CONST.PAGE_MARGIN,
   			content :
   			[{
		        text : this.ticketInfo.title,
		        style : ['incidentTitle']
    		},
    			this.dataParts.summary,
		    	this.dataParts.description, '\n',
		    	this.dataParts.activities, '\n',
		    	this.dataParts.notes, '\n',
		    	this.dataParts.artifacts, '\n',
		    	this.dataParts.attachments, '\n',
		    	this.dataParts.automationResults
	    	],
   			styles : {
		        partHeader : {
		            bold: true,
		            fontSize: 15,
		            margin : [0,10,0,0]
		        },
		        incidentTitle : {
		            bold: true,
		            fontSize: 20,
		            margin : [0,0,0,5]
		        },
		        tableHeader : {
		        	bold : true,
		        	fillColor : '#3d3d3d',
		        	color : 'white'
		        },
		        tableGroup : {
		        	color : 'white',
		        	fillColor : '#777777',
		        	alignment : 'center'
		        }
		    }
		}
	},

	getSummaryFragments : function(ticketInfo){
		this.ajax({
			url: this.mixinAPI['getWS'],
			method: 'GET',
			params: {
				id: ticketInfo.problemId,
			},
			scope: this,
			success: function(resp) {
				var response = RS.common.parsePayload(resp);
				if (response.success) {
					var members = ticketInfo.owner + ' (owner)' + (ticketInfo.members ? ', ' + ticketInfo.members : "");
					var ids = ['correlationId','alertId','reference'];
					var relatedInvestigationRecords = [];
					for(var i = 0; i < ids.length; i++){
						if(response.data[ids[i]])
							relatedInvestigationRecords.push(this.localize(ids[i]) + ': ' + response.data[ids[i]]);
					}
					if(relatedInvestigationRecords.length == 0)
						relatedInvestigationRecords.push('None');

					this.dataParts.summary = [
						{
							text : 'Summary',
							style : ['partHeader']
						},
						'ID: ' + ticketInfo.sir,
						'Severity: ' + ticketInfo.severity,
						'Type: ' + ticketInfo.investigationType,
						'Source: ' + ticketInfo.sourceSystem,
						'Data Compromised: ' + ticketInfo.dataCompromised,
						'Status: ' + ticketInfo.status,
						{
							text : 'Team',
							style : ['partHeader']
						},
						members,
						{
							text : this.localize('relatedInvestigations'),
							style : ['partHeader']
						},
						[relatedInvestigationRecords]
					]
				} else {
					clientVM.displayError(this.localize('loadRelatedInvestigationsFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	getActivitiesFragments : function(){
		var partTitle = this.localize('activitiesTab');
		var fields = [{
			name : 'activityName',
			locale : 'activity',
			show : true,
			width : '*',
			required : true,
			errorDisplay : this.localize("noActivityDefined")
		},{
			name : 'days',
			locale : 'sla',
			show : true,
			width : 70,
			convert : function(val, record){
				var d = record.days;
				var h = record.hours;
				var m = record.minutes;
				return (d ? (d + ' Days') : '' ) + (h ? (h + ' Hours') : '' ) + (m ? (m + ' Minutes') : '' )
			}
		},{
			name : 'dueDate',
			locale : 'dueBy',
			show : true,
			width : 70,
			convert: function(val) {
				return val ? Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat()) : '';
			}
		},{
			name : 'assignee',
			locale : 'assignee',
			show : true,
			width : 70
		},{
			name : 'status',
			locale : 'status',
			show : true,
			width : 70
		}];
		var groupConfig = {
			groupField : 'phase'
		}
		this.dataParts.activities = this.getTableFragments('activities', partTitle, fields, this.getActivityList() , groupConfig);
	},
	getNoteFragments : function(){
		var filter = [{field:"incidentId",type:"string",condition:"equals",value: this.rootVM.ticketInfo.id}];
		this.ajax({
			url: this.mixinAPI['getNote'],
			method : 'GET',
			params : {				
				filter: JSON.stringify(filter)
			},
			success: function(resp) {
				var response = RS.common.parsePayload(resp);
				if (response.success) {
					var partTitle = this.localize('notesTab');
					var fields = [{
						name : 'activityName',
						locale : 'activity',
						show : true,
						width : 70
					},{
						name : 'postedBy',
						locale : 'addedBy',
						show : true,
						width : 70
					},{
						name : 'note',
						locale : 'notePreview',
						show : true,
						width : '*'
					},{
						name: 'sysCreatedOn',
						type: 'int',
						locale : 'addedOn',
						show : true,
						width : 70,
						convert: function(val) {
							return Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat())
						}
					},{
						name: 'sysUpdatedOn',
						type: 'int',
						locale : 'updatedOn',
						show : true,
						width : 70,
						convert: function(val) {
							return Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat())
						}
					}];
					this.dataParts.notes = this.getTableFragments('notes', partTitle, fields, response.records);
				} else {
					clientVM.displayError(this.localize('loadRelatedInvestigationsFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	getArtifactFragments : function(){
		this.ajax({
			url: this.mixinAPI['getArtifact'],
			method : 'GET',
			params : {
				sir: this.ticketInfo.sir
			},
			success: function(resp) {
				var response = RS.common.parsePayload(resp);
				if (response.success) {
					var partTitle = this.localize('artifactsTab');
					var fields = [{
						name: 'activityName',
						locale : 'activity',
						show : true,
						width : 70
					},{
						name : 'sysCreatedBy',
						locale : 'addedBy',
						show : true,
						width : 70
					},{
						name : 'name',
						locale : 'name',
						show : true,
						width : 70
					},{
						name : 'value',
						locale : 'value',
						show : true,
						width : '*'
					},{
						name: 'sysCreatedOn',
						type: 'int',
						locale : 'addedOn',
						show : true,
						width : 70,
						convert: function(val) {
							return Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat())
						}
					},{
						name: 'sysUpdatedOn',
						type: 'int',
						locale : 'updatedOn',
						show : true,
						width : 70,
						convert: function(val) {
							return Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat())
						}
					}];
					this.dataParts.artifacts = this.getTableFragments('artifacts', partTitle, fields, response.records);
				} else {
					clientVM.displayError(this.localize('loadRelatedInvestigationsFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	getAttachmentFragments : function(){
		this.ajax({
			url: this.mixinAPI['getAttachments'] + '?incidentId=' + this.ticketInfo.id,
			params: {
				docSysId: '',
				docFullName: this.ticketInfo.id
			},
			method : 'POST',
			scope: this,
			success: function(resp){
				var response = RS.common.parsePayload(resp);
				if (response.success) {
					var partTitle = this.localize('attachmentsTab');
					var fields = [{
						name : 'name',
						locale : 'name',
						show : true,
						width : '*'
					},{
						name : 'size',
						locale : 'size',
						show : true,
						width : 70
					},{
						name : 'malicious',
						locale : 'malicious',
						show : true,
						width : 70,
						convert : function(val){
							return val ? 'Yes' : 'No';
						}
					},{
						name : 'sysCreatedOn',
						locale : 'uploadedOn',
						show : true,
						width : 70,
						convert: function(val) {
							return Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat())
						}
					},{
						name: 'sysCreatedBy',
						locale : 'uploadedBy',
						show : true,
						width : 70
					}];
					this.dataParts.attachments = this.getTableFragments('attachments', partTitle, fields, response.records);
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		})
	},
	getAutomationResultFragments : function(){
		var partTitle = this.localize('automationresultsTab');
		var fields =  [{
			name : 'activityName',
			locale : 'activity',
			show : true,
			width : 70
		},{
			name : 'taskSummary',
			locale : 'taskSummary',
			show : true,
			width : 120
		},{
			name : 'summary',
			locale : 'summary',
			show : true,
			width : '*'
		},{
			name : 'sysCreatedOn',
			locale : 'createdOn',
			show : true,
			width : 70,
			convert: function(val) {
				return Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat())
			}
		},{
			name: 'severity',
			locale : 'result',
			show : true,
			width : 70
		}];
		//For now just assume there is only 1 layer of grouping. REVISIT LATER.
		var rootNode = this.automationresultsTab.getCurrentData();
		var childNodes = rootNode.childNodes;
		var records = [];
		for(var i = 0; i < childNodes.length; i++){
			var childNode = childNodes[i];
			records.push(childNode.data);
		}
		this.dataParts.automationResults = this.getTableFragments('automationResults', partTitle, fields, records);
	},

	//DESCRIPTION
	getDescriptionFragments : function(content){
		var re = /(\{section[^\}]*\})([\s\S]*?)\{section\}/gmi;
		var match;
		var result = [{
	        "text": "Description",
	        "style": ["partHeader"]
	    }];
		this.currentImgId = 0;
		this.imgDataList = {};
		while (match = re.exec(content)) {
			if (match[1].search('procedure') != -1) {
				// Skip procedure section
				continue;
			}
			result = result.concat(this.extractSection(match[2]));
		}
		if(this.currentImgId == 0)
			this.dataParts.description = result;
		else {
			var startTime = new Date().getTime();
			var progessCheckingTask = setInterval(function(){
				//All data for image came back
				if(this.currentImgId == Object.keys(this.imgDataList).length){
					for(var i = 0; i < result.length; i++){
						var replacementStr = result[i];
						if(this.imgDataList.hasOwnProperty(replacementStr))
							result[i] = {
								image : this.imgDataList[replacementStr],
								width : 500,
								alignment : 'center'
							};
					}
					clearInterval(progessCheckingTask);
					this.dataParts.description = result;
				}
				if(new Date().getTime() >= startTime + 1000 * 60)
					clearInterval(progessCheckingTask);
			}.bind(this),1000)
		}
	},
	extractSection: function(sec) {
		var re = /(<td.*class="wiki-builder">)([\s\S]*?)<\/td class="wiki-builder">/gmi, match, prop, content, table;
		var result = [];
		if (table = /<table[^>]+>/gmi.exec(sec)) {
			while(match = re.exec(sec)) {
				result = result.concat(this.extractComponent(match[2]));
			}
		} else {
			result = result.concat(this.extractComponent(sec));
		}
		return result;
	},
	extractComponent: function(comContent) {
		var content;
		var result = [];
		if(content = /<novelocity.*>([\s\S]*)<\/novelocity>/gmi.exec(comContent)) {
			var paragraphs = this.splitTextParagraph(content[1].replace(/&nbsp;/g, ' '));
			for(var i = 0; i < paragraphs.length; i++)
				result.push(this.extractText(paragraphs[i]));

		} else if (content = /(\{image[^\}]*\})/gmi.exec(comContent)) {
			result.push(this.extractImage(content[1]));
		}
		return result;
	},
	splitTextParagraph : function(content){
		//Strip all div tag.
		var regex = /([^<>]*)<div[^>]*>(.*?)<\/div>/g;
		var result = [];
		while ((m = regex.exec(content)) !== null) {
		    if (m.index === regex.lastIndex)
		        regex.lastIndex++;

		    m.forEach(function(match, groupIndex){
		       if(groupIndex != 0 && match){
		       		result.push(match);
		       }
		    });
		}
		return result;
	},
	currentImgId : 0,
	imgDataList : {},
	extractImage : function(content){
		var src = content.split('|')[0].split(':')[1].split('}')[0];
		var srcId = this.imageIdInfo[src];
		var fullName = this.ticketInfo.playbook.split('.');
		var imgSrc = Ext.String.format('/resolve/service/wiki/download/{0}/{1}?attach={2}&{3}', fullName[0], fullName[1], srcId, this.csrftoken);
		var imgId = ++this.currentImgId;
		RS.common.getDataUri(imgSrc, function(data){
			this.imgDataList['{' + imgId + '}'] = data;
		}, this)
		return '{' + imgId + '}';
	},
	extractText : function(content){
		content = content.replace(/\n/g, '').replace(/<br>/g, '\n');
		if(/^[a-zA-Z0-9\s]*$/.test(content))
			return content;
		var regex = />([^<>]*)</g;
		var m;
		var result = '';
		while ((m = regex.exec(content)) !== null) {
			if (m.index === regex.lastIndex)
				regex.lastIndex++;
			m.forEach(function(match, groupIndex){
				if(groupIndex == 1)
					result += match;
			});
		}
		return result;
	},
	getTableFragments : function(dataPartName, partTitle, fields, records, groupConfig){

		if(!records || records.length == 0){
			return [{
	        	text : partTitle,
	        	style : ["partHeader"]
	      	},this.localize(dataPartName + 'StoreIsEmpty')]
		}

		//Table Style
		pdfMake.tableLayouts = {
	  		resolveTable: {
			    hLineColor: function (i) {
		      		return '#3d3d3d';
			    },
			    vLineColor: function (i) {
		      		return '#3d3d3d';
			    },
			    fillColor: function (i, node) {
					return i % 2 == 0 ? '#dadada' : 'white'
				},
				paddingTop : function(i, node){
					return 5;
				},
				paddingBottom : function(i, node){
					return 5;
				}
	  		}
		};

		var tableHeader = [];
		var rowWidth = [];
		var allRows = [];
		//Add Header
		for(var i = 0; i < fields.length; i++){
			var field = fields[i];
			if(field.show){
				tableHeader.push({
					text : this.localize(field.locale),
					style : 'tableHeader'
				})
				rowWidth.push(field.width ? field.width : 'auto');
			}
		}
		this.getActualColumnWidth(rowWidth);

		//Add Row
		var groupMaps = {
			_default : []
		};
		for(var i = 0; i < records.length; i++){
			var record = records[i];
			var groupName = '_default';
			if(groupConfig && groupConfig.groupField){
				groupName = record[groupConfig.groupField];
				if(!groupMaps.hasOwnProperty(groupName))
					groupMaps[groupName] = [];
			}

			var row = [];
			for(var j = 0; j < fields.length; j++){
				var field = fields[j];

				if(field.show){
					var fieldRawValue = field.mapping ? record[field.mapping] : record[field.name];
					var value = Ext.isFunction(field.convert) ? field.convert(fieldRawValue, record) : fieldRawValue;

					//Mising required field will display empty row instead
					if(!value && field.required){
						row = [{
							text : field.errorDisplay,
							colSpan : tableHeader.length,
							alignment : 'center'
						}]
						break;
					} else
						row.push(value ? this.wrapLongName(value, rowWidth[row.length]) : 'N/A');
				}
			}
			groupMaps[groupName].push(row);
		}
		for(var g in groupMaps){
			if(g != '_default'){
				allRows.push([{
					text : g,
					colSpan : tableHeader.length,
					style : 'tableGroup'
				}]);
			}
			if(groupMaps[g].length > 0)
				allRows = allRows.concat(groupMaps[g]);
		}

		return [{
        	text : partTitle,
        	style : ["partHeader"]
      	},{
      		layout : 'resolveTable',
      		margin : [0,5,0,0],
      		table : {
      			headerRows : 1,
      			widths : rowWidth,
      			body : [tableHeader].concat(allRows)
      		}
      	}]
	},
	getActualColumnWidth : function(widths){
		var pageSizeInfo = {
			A0: [2383.94, 3370.39],
			A1: [1683.78, 2383.94],
			A2: [1190.55, 1683.78],
			A3: [841.89, 1190.55],
			A4: [595.28, 841.89],
			A5: [419.53, 595.28]
		}
		var pageWidth = pageSizeInfo[this.CONST.PAGE_SIZE][this.CONST.PAGE_DIMENSION == 'landscape' ? 1 : 0] - this.CONST.PAGE_MARGIN[1] - this.CONST.PAGE_MARGIN[3] - 45;
		var autoSizeEntry = [];
		widths.forEach(function(w,i){
			if(!isNaN(w)){
				pageWidth -= w;
			}
			else
				autoSizeEntry.push(i);
		})
		autoSizeEntry.forEach(function(i){
			widths[i] = Math.max(0 , parseInt(pageWidth / autoSizeEntry.length));
		})
	},
	wrapLongName : function(name, thresholdWidth){
		name = name.toString();		
		var maxCharPerWord = parseInt(thresholdWidth / this.CONST.AVERAGE_CHAR_WIDTH);
		var regex = new RegExp('[^\\s]' +'{' + maxCharPerWord + '}','gi');		
		name = name.replace(regex, function(matched, index){
			return matched + ' ';
		})		
		return name;
	}
})
glu.defModel('RS.incident.Playbook', {
	mixins : ['PDFConverter'],
	sirContext: true,

	overviewComponent: {
		mtype: 'RS.incident.Overview'
	},

	isPreview: false,
	isReadOnly: false,
	sysCreatedOn: '',
	sysCreatedBy: '',

	activeTab: 0,
	activeTabMap : {
		ACTIVITIES : 0,
		NOTES : 1,
		ARTIFACTS: 2,
		ATTACHMENTS : 3,
		AUTOMATIONRESULTS : 4,
		AUDITLOG : 5
	},
	activityList : [],
	activitiesTab: {
		mtype: 'RS.incident.Activities'
	},

	notesTab: {
		mtype: 'RS.incident.Notes'
	},

	artifactsTab: {
		mtype: 'RS.incident.Artifacts'
	},

	attachmentsTab: {
		mtype: 'RS.incident.Attachments'
	},

	automationresultsTab: {
		mtype: 'RS.incident.AutomationResults'
	},

	auditlogTab: {
		mtype: 'RS.incident.AuditLog'
	},

	siemdataTab: {
		mtype: 'RS.incident.SIEMData'
	},

	/*relatedinvestigationsTab: {
		mtype: 'RS.incident.RelatedInvestigations'
	},*/

	isActivitiesView: false,
	activityId: '',

	activityFilterStore : {
		mtype : 'store',
		fields : ['activityName']
	},

	isTabPanelVisible$: function() {
		return this.isActivitiesView
	},

	isActivitiesViewVisible$: function() {
		return !this.isActivitiesView
	},

	initialLoad: true,
	//Provides extra control flow for tabs that can't use simple proxy loading
	loadTabs: function(ticketInfo) {
		//Replace with whatever tab needs to be loaded first, will be called after ticketInfo retrieved.
		this.activitiesTab.tabInit();

		//Load related investigations in overview
		this.overviewComponent.loadRelatedInvestigations();

		//Attachments needs info set explicitly
		this.attachmentsTab.set('ticketInfo', ticketInfo);
		this.attachmentsTab.set('incidentId', ticketInfo.id);

		//Set description. Prevent description from being loaded multiple times.
		if (this.initialLoad) {
			this.fireEvent('loadDescription', ticketInfo.description, ticketInfo.playbook);
			this.set('initialLoad', false);
		}
	},

	//Load TicketInfo + Ticket Info Object
	ticketInfo: {
		closedBy: '',
		closedOn: '',
		dataCompromised: '',
		description: '',
		externalStatus: '',
		externalReferenceId: '',
		id: '',
		investigationType: '',
		owner: '',
		playbook: '',
		playbookVersion: '',
		primary: '',
		problemId: '',
		sequence: '',
		severity: '',
		sir: '',
		sirStarted: '',
		sourceSystem: '',
		status: '',
		sysCreatedBy: '',
		sysCreatedOn: '',
		sysIsDeleted: '',
		title: ''
	},

	resultsInfo: {},

	loadTicketInfo: function(sir) {
		this.ajax({
			url: '/resolve/service/playbook/securityIncident/get',
			method: 'GET',
			params: {
				sir: sir,
			},
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if ((results.data === null) && (results.total === 0)) {
					this.message({
						title: this.localize('invalidIncidentTitle'),
						msg: this.localize('invalidIncident'),
						closable : false,
						buttonText: {
							ok: this.localize('close')
						},
						scope: this,
						fn: function(btn) {
							clientVM.handleNavigation({
								modelName: 'RS.incident.Dashboard'
							});
						}
					});
				}
				if (results.success) {
					this.set('activityList',results.data.INCIDENT_ACTIVITIES );
					var infoObj = results.data.INCIDENT_INFO
					this.set('ticketInfo', infoObj);
					this.loadMembers(results.data.INCIDENT_INFO.id);
					this.loadTabs(infoObj);

					//Load activity view if wiki param exists
					if (this.isActivitiesView) {
						var selectedActivity = results.data.INCIDENT_ACTIVITIES.filter(function(activity) {return activity.id === this.activityId}, this)[0];
						this.activitiesTab.activitiesView.set('activityWiki', selectedActivity.wikiName);
						this.activitiesTab.activitiesView.set('activityName', selectedActivity.activityName);
						this.activitiesTab.activitiesView.set('dueDate', selectedActivity.dueDate);
						this.activitiesTab.activitiesView.set('phase', selectedActivity.phase);
						this.activitiesTab.activitiesView.set('problemId', results.data.INCIDENT_INFO.problemId);
						this.activitiesTab.activitiesView.set('activityId', selectedActivity.id);
						this.activitiesTab.activitiesView.set('incidentId', this.ticketInfo.id);
						this.activitiesTab.activitiesView.generateiFrame();
					}

					this.set('resultsInfo', {
						sirProblemId : results.data.INCIDENT_INFO.problemId,
						automationFilter : results.data.AUTOMATION_FILTER,
						activities : results.data.INCIDENT_ACTIVITIES.filter(function(activity) {return activity.id})
					})

					//Load Activities filters
					var info = this.resultsInfo;
					info['activities'].splice(0, 0, {activityName : 'All'});
					this.activityFilterStore.loadData(info['activities']);

					//Prepare PDF converter
					this.preparePDF();

					this.automationresultsTab.updateInfo(this.resultsInfo);
					this.applyRBAC();
				} else {
					clientVM.displayError(results.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	//Load Security Group + Store
	securityGroupStore: {
		mtype: 'store',
		fields: ['userId', 'name']
	},
	loadActivities: function(onComplete) {
		this.activitiesTab.loadActivities(onComplete);
	},
	getActivityList : function(){
		var activities = this.activitiesTab.activitiesStore.getRange();
		return activities && activities.length > 0 ? activities.map(function(r){ return r.data }) : this.activityList;
	},
	loadSecurityGroup: function(callback) {
		this.ajax({
			url: '/resolve/service/playbook/securityIncident/getSecurityGroup',
			method: 'GET',
			scope: this,
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					this.securityGroupStore.loadData(results.records);
					this.securityGroupStore.sort('userId', 'ASC');
					if(typeof(callback) == 'function') {
						callback.call(this);
					}
				} else {
					clientVM.displayError(this.localize('securitygroupLoadFail'));
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	//Load Members + Store
	membersStore: {
		mtype: 'store',
		fields: ['userId', 'name']
	},

	loadMembers: function(id) {
		this.loadSecurityGroup(function() {
			this.ajax({
				url: '/resolve/service/playbook/securityIncident/get',
				method: 'GET',
				params: {
					incidentId: id,
				},
				success: function(response) {
					var results = RS.common.parsePayload(response);
					if (results.success) {
						var membersArr = results.data.INCIDENT_INFO.members || '';
						this.overviewComponent.set('owner', results.data.INCIDENT_INFO.owner) //Set owner manually so binding in overview can be dynamically updated.
						if (membersArr !== null && membersArr !== '') {
							var membersObjArr = membersArr.split(', ').map(function(userId) {
								var name = '';
								var r = this.securityGroupStore.findRecord('userId', userId);
								if (r) {
									name = r.getData().name;
								}
								return {
									userId: userId,
									name: name
								}
							}, this);
							this.membersStore.loadData(membersObjArr);
							//Set copy of members in overview to trigger formatting formula
							var membersCopy = this.membersStore.getRange();
							this.overviewComponent.set('membersCopy', membersCopy);
							this.activitiesTab.fireEvent('refreshMembers');
						}
					} else {
						clientVM.displayError(this.localize('membersLoadFail'));
					}
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			});
		});
	},

	fullName: '',
	previewUContent: null,

	getPreviewContent: function(onSuccess) {
		this.ajax({
			url: '/resolve/service/wiki/get/',
			method: 'POST',
			params: {
				id: '',
				name: this.fullName
			},
			scope: this,
			success: function(response) {
				var results = RS.common.parsePayload(response);
				if (results.success) {
					this.uname = results.data.uname;
					this.unamespace = results.data.unamespace;
					this.id = results.data.id;
					this.set('previewUContent', results.data.ucontent);
					if (!this.sysCreatedOn) {
						this.set('sysCreatedOn', results.data.sysUpdatedOn);
						this.set('sysCreatedBy', results.data.sysCreatedBy);
					}

					var ucontent = results.data.ucontent;
					var re = /(\{section[^\}]*\})([\s\S]*?)\{section\}/gmi;
					var match;
					while (match = re.exec(ucontent)) {
						if (match[1].search('procedure') != -1) {
							this.activitiesTab.set('previewContent', match[2]);
							break;
						}
					}

					if (typeof onSuccess === 'function') {
						onSuccess.call(this);
					}
				} else {
					clientVM.displayError(results.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	activate: function(screen, params) {
		if (params.sir) {
			this.loadTicketInfo(params.sir);
			if (params.activityId !== undefined) {
				this.set('activityId', params.activityId);
				this.set('isActivitiesView', true);
			} else {
				this.set('isActivitiesView', false);
			}
		}
	},

	pSirInvestigationViewerView: true,

	pSirInvestigationViewerDataCompromisedModify: true,
	pSirInvestigationViewerStatusModify: true,
	pSirInvestigationViewerTeamModify: true,

	pSirInvestigationViewerActivitiesCreate: true,
	pSirInvestigationViewerActivitiesModify: true,
	pSirInvestigationViewerActivitiesExecute: true,

	pSirInvestigationViewerArtifactsCreate: true,
	pSirInvestigationViewerArtifactsModify: true,
	pSirInvestigationViewerArtifactsDelete: true,

	pSirInvestigationViewerAttachmentsCreate: true,
	pSirInvestigationViewerAttachmentsModify: true,
	pSirInvestigationViewerAttachmentsDelete: true,

	pSirInvestigationViewerNotesCreate: true,
	pSirInvestigationViewerNotesModify: true,
	pSirInvestigationViewerNotesDelete: true,

	hasSolePermissionToModify: function() {
		// Any one of the following conditions will allow the user to modify the SIR
		// 1. User having admin role
		// 2. User is the owner
		// 3. User is a team member of the SIR
		// 3 have RBAC to modify
		var hasAdminRole = (clientVM.user.roles.indexOf('admin') != -1);
		var isOwner = this.ticketInfo.owner == clientVM.user.name;
		var isTeamMember = (this.ticketInfo.members || []).indexOf(clientVM.user.name) != -1;
		var canModify = hasAdminRole || isOwner || isTeamMember;
		return canModify;
	},

	applyRBAC: function()   {
		var permissions = clientVM.user.permissions || {};

		var canModify = this.hasSolePermissionToModify();

		permissions.sirInvestigationViewer = permissions.sirInvestigationViewer || {};
		this.set('pSirInvestigationViewerView', permissions.sirInvestigationViewer.view === true);

		permissions.sirInvestigationViewerDataCompromised = permissions.sirInvestigationViewerDataCompromised || {};
		this.set('pSirInvestigationViewerDataCompromisedModify', canModify || permissions.sirInvestigationViewerDataCompromised.modify === true);

		permissions.sirInvestigationViewerStatus = permissions.sirInvestigationViewerStatus || {};
		this.set('pSirInvestigationViewerStatusModify', canModify || permissions.sirInvestigationViewerStatus.modify === true);

		permissions.sirInvestigationViewerTeam = permissions.sirInvestigationViewerTeam || {};
		this.set('pSirInvestigationViewerTeamModify', canModify || permissions.sirInvestigationViewerTeam.modify === true);

		permissions.sirInvestigationViewerActivities = permissions.sirInvestigationViewerActivities || {};
		this.set('pSirInvestigationViewerActivitiesCreate', permissions.sirInvestigationViewerActivities.create === true);
		this.set('pSirInvestigationViewerActivitiesModify', canModify || permissions.sirInvestigationViewerActivities.modify === true);
		this.set('pSirInvestigationViewerActivitiesExecute', permissions.sirInvestigationViewerActivities.execute === true);

		permissions.sirInvestigationViewerArtifacts = permissions.sirInvestigationViewerArtifacts || {};
		this.set('pSirInvestigationViewerArtifactsCreate', permissions.sirInvestigationViewerArtifacts.create === true);
		this.set('pSirInvestigationViewerArtifactsModify', canModify || permissions.sirInvestigationViewerArtifacts.modify === true);
		this.set('pSirInvestigationViewerArtifactsDelete', permissions.sirInvestigationViewerArtifacts.delete === true);

		permissions.sirInvestigationViewerAttachments = permissions.sirInvestigationViewerAttachments || {};
		this.set('pSirInvestigationViewerAttachmentsCreate', permissions.sirInvestigationViewerAttachments.create === true);
		this.set('pSirInvestigationViewerAttachmentsModify', canModify || permissions.sirInvestigationViewerAttachments.modify === true);
		this.set('pSirInvestigationViewerAttachmentsDelete', permissions.sirInvestigationViewerAttachments.delete === true);

		permissions.sirInvestigationViewerNotes = permissions.sirInvestigationViewerNotes || {};
		this.set('pSirInvestigationViewerNotesCreate', permissions.sirInvestigationViewerNotes.create === true);
		this.set('pSirInvestigationViewerNotesModify', canModify || permissions.sirInvestigationViewerNotes.modify === true);
		this.set('pSirInvestigationViewerNotesDelete', permissions.sirInvestigationViewerNotes.delete === true);
		this.fireEvent('applyrbac');
	},

	init: function() {
		if (!this.pSirInvestigationViewerView) {
			return;
		}
	},

	initialLoad: true,
	expandDescription: function() {
		var modal = this.open({
			mtype: 'RS.incident.DescriptionModal',
			ticketInfo: this.ticketInfo
		});
		modal.fireEvent('loadDescriptionModal', this.ticketInfo.description, this.ticketInfo.playbook)
	},

	returnTitle$: function() {
		return this.ticketInfo.title;
	},

	returnId$: function() {
		return this.ticketInfo.sir;
	},

	returnDateCreated$: function() {
		if (this.ticketInfo && this.ticketInfo.sysCreatedOn) {
			return Ext.util.Format.date(new Date(this.ticketInfo.sysCreatedOn), clientVM.getUserDefaultDateFormat());
		} else {
			return '';
		}
 	},

	returnSeverity$: function() {
		return this.ticketInfo.severity;
	},
	returnType$: function() {
		return this.ticketInfo.investigationType;
	},

	returnStatus$: function() {
		return this.ticketInfo.status;
	},

	returnSource$: function() {
		return this.ticketInfo.sourceSystem
	},
	returnSourceId$: function() {
		return this.ticketInfo.externalReferenceId
	},

	returnDataCompromised$: function() {
		if (this.ticketInfo.dataCompromised === true) {
			return 'Yes'
		} else if (this.ticketInfo.dataCompromised === false) {
			return 'No'
		} else {
			return this.ticketInfo.dataCompromised
		}
	},

	returnCreatedBy$: function() {
		return this.ticketInfo.sysCreatedBy
	},

	beforeDestroyComponent: function() {
		//Remove this handler from document's event.
		this.fireEvent('removeEventReference');
	}
});

//Parent: RS.incident.Playbook
glu.defModel('RS.incident.DescriptionModal', {
	close: function() {
		this.doClose();
	},

	init: function() {
	}
});

glu.defModel('RS.incident.SIEMData', {
	data: {},

	loadSIEMData: function() {
		var rawData = this.rootVM.ticketInfo.requestData;
		if (rawData === null) {
			this.set('data', this.localize('noRequestData'));
		} else {
			this.set('data', JSON.stringify(JSON.parse(rawData), null, 2));
		}
	},

	tabInit: function() {
		this.loadSIEMData();
	}
});
glu.defModel('RS.incident.UploadAttachment', {
	mixins: ['FileUploadManager'],
	api: {
		upload: '/resolve/service/playbook/attachment/upload',
		list: '/resolve/service/playbook/attachment/listBySir?incidentId='+'will_change' //Shim wiki upload manager but also send incidentId because attachment/listBySir needs it
	},
	autoUpload: false,
	deleteFileAfterUpload: true,

	fineUploaderTemplateHidden: false,
	fineUploaderTemplateHeight: 245,

	uploaderAdditionalClass: 'incident',

	fileOnSubmittedCallback: function(manager) {
		if (manager.getUploads({status: [qq.status.SUBMITTED, qq.status.QUEUED]}).length) {
			this.view.fireEvent('enableSubmitBtn');
		}
	},
	fileOnCancelCallback: function(manager) {
		setTimeout(function(){
			if (manager.getUploads({status: [qq.status.SUBMITTED, qq.status.QUEUED]}).length == 0) {
				this.view.fireEvent('disableSubmitBtn');
			}
		}.bind(this), 100)
	},

	filesOnAllCompleteCallback: function(manager) {
		this.parentVM.fireEvent('reloadAttachments');

		if (manager.getUploads({status: [qq.status.UPLOAD_FAILED, qq.status.REJECTED, qq.status.PAUSED] }).length == 0) {
			this.close();
		} else {
			setTimeout(function(){
				if (manager.getUploads({status: [qq.status.SUBMITTED, qq.status.QUEUED]}).length == 0) {
					this.view.fireEvent('disableSubmitBtn');
				}
			}.bind(this), 100);
		}
	},

	//fileOnErrorCallback: function(manager, filename, errorReason, xhr) {
	//	clientVM.displayError(filename+' : '+errorReason);
	//},

	malicious: false,
	maliciousChanged: function(malicious) {
		this.set('malicious', malicious);
	},

	description: '',
	descriptionChanged: function(description) {
		this.set('description', description || '');
	},

	init: function() {
		this.set('title', this.localize('addAttachment'));
		this.set('dragHereText', this.localize('dragHere'));
	},

	submit: function() {
		this.uploader.setParams({
			docFullName: this.incidentId,
			malicious: this.malicious,
			description: this.description
		});
		this.uploader.uploadStoredFiles();
	},

	view: null,
	viewRenderCompleted: function(view) {
		this.set('view', view);
	},

	close: function() {
		this.parentVM.reload();
		this.doClose();
	}
});

glu.defModel('RS.incident.FileUploadManagerFactory', function(cfg) {
	Ext.applyIf(cfg, RS.common.viewmodels.FileUploadManager);
	return cfg;
});

/*
glu.defModel('RS.incident.UploadAttachment', {
    runtime: '',
	incidentId: '',
	api: {
		upload: '/resolve/service/playbook/attachment/upload',
	},

    init: function() {
        this.clearForm();
    },

    browse: function() {
		//this.view.fireEvent('clearQueue');
	},
    
    close: function() {
        this.set('wait', false);
        this.doClose();
    },

	wait: false,
	uploading: false,
	filenames: '',
	description: '',
	malicious: false,
	percent: 0,
	filesize: 0,
	currentFile: '',
	isCancel: false,
	uploadlog: '',

	clearForm: function() {
		this.set('wait', false);
		this.set('uploading', false);
		this.set('filenames', '');
		this.set('description', '');
		this.set('malicious', false);
		this.set('percent', 0);
		this.set('filesize', 0);
		this.set('currentFile', '');
		this.set('isCancel', false);
		this.set('uploadlog', '');
	},

	busyText$: function() {
		return this.uploadlog + this.currentFile + ' ('+Ext.util.Format.fileSize(this.filesize)+') : ' + this.percent;
	},

    browseIsEnabled$: function() {
       return !this.wait;
    },

    uploadError: function(uploader, data) {
        clientVM.displayError(this.localize('UploadError') + '[' + data.file.msg + ']');
    },

	beforeStart: function(uploader, data) {
        this.set('wait', true);
        this.set('uploading', true);
	},

    filesAdded: function(uploader, data) {
        var filenames = '';
		data.forEach(function(file) {
			if (filenames) {
				filenames += ', '+file.name;
			} else {
				filenames = file.name;
			}
		});

		uploader.uploader.files.forEach(function(file) {
			if (filenames) {
				filenames += ', '+file.name;
			} else {
				filenames = file.name;
			}
		});

		this.set('filenames', filenames);
    },

	uploadProgress: function(filename, size, percent) {
		if (this.currentFile && this.currentFile != filename) {
			this.uploadlog += this.currentFile + ': Done<br>';
		}
		this.set('percent', percent + '%');
		this.currentFile = filename;
		this.filesize = size;
	},

    uploadComplete: function(uploader, data) {
		if (this.isCancel) {
			clientVM.displaySuccess(this.localize('attachmentsUploadCancelled'));
		} else {
			clientVM.displaySuccess(this.localize('attachmentsUploadSuccess'));
		}

		//uploader.clearQueue();
		//this.clearForm();
		this.close();

		this.parentVM.fireEvent('reloadAttachments');
    },
    runtimeDecided: function(runtime) {
        if (!runtime) {
            clientVM.displayError('no uploader runtime!!')
            return;
        }
        this.set('runtime', runtime.type);
    },
	submitIsEnabled$: function() {
        return this.filenames;
    },
	submit: function() {
		this.view.fireEvent('startUpload', this.malicious, this.description);
    },

	cancel: function() {
		this.set('isCancel', true);
		this.view.fireEvent('cancelUpload');
    },

	view: null,
	viewRenderCompleted: function(view) {
		this.set('view', view);
	}
})
*/

glu.defModel('RS.incident.PlaybookTemplate', function() {
	var documentStore = {
		mtype: 'store',
		pageSize: 50,
		model: 'RS.incident.model.Runbook',
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wiki/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	};

	return {
		mixins: ['WikiMainModel'], // A playbook template is a specialized wiki doc
		mtype: 'datamodel',        // A wiki doc is a datamodel and must be specified if used as mixins
		templateType: 'nist',
		preDefinedPhases: {
			nist: [
				{phase: 'Preparation'},
				{phase: 'Detection and Analysis'},
				{phase: 'Containment, Eradication, and Recovery'},
				{phase: 'Post-Incident Activity'}
			]
		},
		knownPhases: {
			mtype: 'store',
			model: 'RS.incident.model.Phase',
			proxy: {
				type: 'memory'
			}
		},
		runbookStore: documentStore,
		documentStore: documentStore,
		procedurePhases: {children: []},
		procedures: {
			mtype: 'treestore',
			model: 'RS.incident.model.InvestigativeProcedure',
			proxy: {
				type: 'memory'
			},
			root: {
				text: '.',
				children: []
			},
			folderSort: true
		},
		hideImgComZoom: true,
		phase: '',
		activityName: '',
		wikiName: '',
		documentName: '',
		wikiNameIsValidated: false,
		dirtyFlag: true,
		wait: false,
		sir : true,
		pSirPlaybookTemplatesTemplateBuilderChange: true,
		csrftoken: '',

		applyRBAC: function() {
			var permissions = clientVM.user.permissions || {};

			this.set('pSirPlaybookTemplatesTemplateBuilderChange', permissions['sir.playbookTemplates.templateBuilder.change'] === true);
			this.set('saveAsIsEnabled', permissions['sir.playbookTemplates.templateBuilder.create']);
		},

		handleInsufficientPermission: function() {
			this.message({
				title: this.localize('insufficientPermissionMsgTitle'),
				msg: this.localize('insufficientPermissionMsg'),
				closable : false,
				buttonText: {
					ok: this.localize('close')
				},
				scope: this,
				fn: function(btn) {
					clientVM.handleNavigationBack();
				}
			});
		},

		init: function() {
			this.initToken();
			this.applyRBAC();
			if (!this.pSirPlaybookTemplatesTemplateBuilderChange) {
				this.handleInsufficientPermission();
				return;
			}
			this.knownPhases.loadData(this.preDefinedPhases[this.templateType]);
			this.runbookStore.on('load', function(store) {
				if (this.ignoreLoadEvent) {
					this.ignoreLoadEvent = false;
				} else {
					var r = store.findRecord('ufullname', this.wikiName, 0, false, false, true);
					this.set('wikiNameIsValidated', r? true: false);
				}
			}, this);
			this.wikiMainInit.apply(this, arguments);
			var me = this,
				wikiMainSectionsLayoutPopulateSection = this.sectionsLayout.populateSection;

			Ext.apply(this.sectionsLayout, {
				populateSection: function(section) {
					var match = section.match(/type=[A-Za-z]+/i);
					var sectionName = match[0].split('=')[1];
					if (match && sectionName == 'procedure') {
						var components = me.parseSection(section);
						me.insertToProcedure(Ext.decode(components));
					}
					else if (match && sectionName == 'result') {
						var components = me.parseSection(section);
						me.parseAutomationFilterSection(components);
					} else if (match && sectionName == 'sla') {
						var sla = me.parseSection(section);
						me.parseSlaSection(sla);
					}else {
						wikiMainSectionsLayoutPopulateSection.apply(this, arguments);
					}
				}
			});
			this.procedures.on('datachanged', function(store) {
				this.handleDirtyFlag();
			}, this);
			this.procedures.on('update', function(store, record) {
				this.handleDirtyFlag();
			}, this);
			this.procedures.on('move', function(record) {
				this.handleDirtyFlag();
			}, this);
			this.procedures.on('remove', function(n, record) {
				this.handleDirtyFlag();
				this.activityRemoved({
					days:  record.get('days'),
					hours: record.get('hours'),
					minutes: record.get('minutes')
				});
			}, this);
			this.on('pageLoaded', function() {
				this.initOriginalContent();
			}, this);
		},

		validateEditProcedure: function(context, value) {
			if (!context.record.isLeaf()) {
				context.dupPhaseFound  = false;
				context.grid.store.getRootNode().eachChild(function(n) {
					if (n != context.record && n.get('activityName') == value) {
						context.dupPhaseFound = true;
					}
				}, this);
			}
		},

		automationFilterChangeHandler: function(dirty) {
			this.set('dirtyFlag', dirty)
		},

		asynCall: function(func) {
			var me = this;
			setTimeout(func.bind(me), 100);
		},

		initOriginalContent: function() {
			var me = this;
			// getting content asynchronously makes the UI more responsive
			this.asynCall(function() {
				me.set('dirtyFlag', me.id.trim()? false: true);
				me.originalContent = me.getPageContent();
			});
		},

		handleDirtyFlag: function() {
			var me = this;
			// getting content asynchronously makes the UI more responsive
			this.asynCall(function() {
				var newContent = me.getPageContent();
				if (newContent == me.originalContent) {
					me.set('dirtyFlag', false);
				} else {
					me.set('dirtyFlag', true);
				}
			});
		},

		getAddedPhases: function() {
			var phases = [];
			this.procedures.getRootNode().childNodes.forEach(function(n) {
				var phase = n.get('activityName');
				var phaseIsPredefined = false;
				this.preDefinedPhases[this.templateType].forEach(function(preDefined) {
					if (preDefined.phase == phase) {
						phaseIsPredefined = true;
					}
				}, this);
				phaseIsPredefined? '': phases.push({phase: phase});
			}, this);
			return phases;
		},

		populatePhases: function(v) {
			this.knownPhases.removeAll(true); // true not prevent collapsing the combo
			this.knownPhases.loadData(this.preDefinedPhases[this.templateType]);
			this.knownPhases.add(this.getAddedPhases());
		},

		parseSection: function(section) {
			var components = [],
				c = /\{section[^\}]*\}\n([\s\S]*?)\n\{section\}/gi.exec(section);
			if (Ext.isArray(c)) {
				c.splice(0, 1);
				components = c.join('');
			}
			return components;
		},

		parseAutomationFilterSection : function(section){
			this.automationFilter.parseAutomationFilterSection(section);
		},

		//Lock this document once data is ready
		when_id_is_loaded : {
			on : ['idChanged'],
			action : function(){
				if(this.id && !this.softLock)
					this.setSoftLocked(true);
			}
		},
		activate: function(screen, params) {
			var me = this;
			this.initToken();
			this.applyRBAC();
			if (!this.pSirPlaybookTemplatesTemplateBuilderChange) {
				this.handleInsufficientPermission();
				return;
			}
			this.clearProcedure();
			this.wikiMainActivate.apply(this, arguments);
			if (params.type) {
				this.set('templateType', params.type);
				this.set('procedurePhases', {children: this.preDefinedPhases[params.type].map(function(phase) {
					return {
						activityName: phase.phase,
						expanded: true,
						leaf: false
					}
				})});
			};
			if (params.new) {
				this.procedures.setRootNode(this.procedurePhases);
				this.sectionsLayout.defaultComType = 'wysiwyg';
				this.sectionsLayout.on('newComponentAdded', function() {
					delete me.sectionsLayout.defaultComType;
				});
			}
		},

		initToken: function() {
			this.set('csrftoken', clientVM.rsclientToken);
		},

		createRecord: function(record) {
			var fields = record.getUfields(),
				obj = {};
			for (var i=0; i<fields.length; i++) {
				obj[fields[i]] = record.get(fields[i]);
			}
			return obj;
		},

		addNode: function(node) {
			var rootNode = this.procedures.getRootNode(),
				insertInto = rootNode.findChild('activityName', node.phase);

			if (!insertInto) {
				insertInto = rootNode.createNode({
					activityName: node.phase,
					expanded: true,
					leaf: false
				});
				rootNode.insertChild(rootNode.childNodes.length, insertInto);
			}
			if (node.activityName && node.wikiName) {
				var rec = this.createRecord(new this.procedures.model(node));
				rec.leaf = true;
				insertInto.insertChild(insertInto.childNodes.length, insertInto.createNode(rec));
			}
			this.activityAdded({
				days: node.days,
				hours: node.hours,
				minutes: node.minutes
			});
		},

		addActivity: function(fieldValues) {
			var form = this.getActivityForm().getForm();
			this.addNode(form.getFieldValues());
			form.reset();
		},

		removeActivity: function( record) {
			if(record.isLeaf()) {
				record.parentNode.removeChild(record);
			} else if (record.hasChildNodes()){
				this.message({
					title: this.localize('deletePhaseTitle'),
					msg: this.localize('deletePhaseBody'),
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						yes: this.localize('yes'),
						no: this.localize('cancel')
					},
					scope: this,
					fn: function(btn) {
						if (btn == "yes") {
							this.procedures.getRootNode().removeChild(record);
						}
					}
				})
			} else {
				this.procedures.getRootNode().removeChild(record);
			}
		},

		searchRunbook: function(v, inline) {
			var me = this;
			this.open({
				mtype: 'RS.decisiontree.DocumentPicker',
				dumper: {
					dump: function(selections) {
						if (selections.length > 0) {
							var wikiName = selections[0].get('ufullname');
							if (inline) {
								// Set runbook name to the cell
								v.up('#activityGrid').editingContext.record.set('wikiName', wikiName);
							} else {
								// Set runbook name to the form field
								me.set('wikiName',wikiName);
								me.ignoreLoadEvent = true;
								me.runbookStore.load();
								me.set('wikiNameIsValidated', true);
							}
						}
					}
				}
			})
		},

		handleRunbookSelect: function() {
			this.set('wikiNameIsValidated', true);
		},

		validateWikiName: function(v) {
			var r = v.store.findRecord('ufullname', v.getValue(), 0, false, false, true);
			this.set('wikiNameIsValidated', r? true: false);
		},

		getContent: function() {
			return this.serializeProcedure();
		},

		clearProcedure: function() {
			if(this.procedures.getRootNode().childNodes.length) {
				this.procedures.getRootNode().removeAll();
			}
		},

		insertToProcedure: function(data) {
			this.clearProcedure();
			for(var i=0; i<data.length; i++) {
				this.addNode(this.createRecord(new this.procedures.model(data[i])));
			}
		},

		serializeProcedure: function() {
			var me = this,
				sData = [];
			// Flatten out tree store as a normal store
			this.procedures.getRootNode().eachChild( function(n){
				var phase = n.get('activityName');
				n.eachChild(function(r) {
					var record = me.createRecord(r);
					record.phase = phase;
					sData.push(record);
				});
				if (!n.childNodes.length) {
					sData.push({
						phase: phase
					});
				}
			});
			return Ext.encode(sData);
		},

		getProcedureSectionContent: function() {
			return '{section:type=procedure|title=Untitled Section' + '}\n' + this.getContent() + '\n{section}'
		},

		maxSla: 0,
		templateSla: 0,

		activitySlaWarning:  false,
		templateSlaWarning: false,

		activityAdded: function(sla) {
			this.maxSla = Math.max(this.maxSla, this.slaToHours(sla) + Math.ceil(parseInt(sla.minutes ||0)/60));
		},

		updateMaxSlas: function(node) {
			var me = this;
			if (node.isLeaf()) {
				this.maxSla = Math.max(this.maxSla, this.slaToMinutes({
					days: node.get('days'),
					hours: node.get('hours')}));
			}
			(node.childNodes || []).forEach(function(childNode) {
				me.updateMaxSlas(childNode);
			});
		},

		activityRemoved: function(sla) {
			if (this.slaToMinutes(sla) >= this.maxSla) {
				this.maxSla = 0;
				this.updateMaxSlas(this.procedures.getRootNode());
			}
		},

		slaToHours: function(sla) {
			return parseInt(sla.days||0)*24*60 + parseInt(sla.hours||0)*60;
		},

		slaToMinutes: function(sla) {
			return this.slaToHours(sla) + parseInt(sla.minutes||0);
		},

		convertMinutesToDaysHours: function(nMinutes) {
			return {
				days: Math.floor(nMinutes/1440),
				hours: Math.ceil((nMinutes%1440)/60)
			};
		},

		templateSlaChanged: function(sla) {
			var slaInMinutes = this.slaToMinutes(sla);
			// No validation is required if sla value is undefined (0)
			if (slaInMinutes && slaInMinutes < this.maxSla) {
				var slaInDaysHours = this.convertMinutesToDaysHours(this.maxSla);
				var msg = Ext.String.format('<font color="red"><i>SLA can not be less than {0} days and {1} hours </i></font>',
					slaInDaysHours.days, slaInDaysHours.hours);
				this.set('templateSlaWarning', msg);
			} else {
				this.templateSla = slaInMinutes;
				this.set('templateSlaWarning', false);
				this.updateActivitySlaWarningMsg();
			}
			this.handleDirtyFlag();
		},

		updateActivitySlaWarningMsg: function() {
			var slaForm = Ext.ComponentQuery.query('rsnumberfield[itemId=activitySla]')[0];
			var sla = slaForm.up().getForm().getFieldValues();
			this.activitySlaChanged(sla);
		},

		activitySlaChanged: function(sla) {
			// No validation is required if sla value is undefined (0)
			if (this.templateSla && this.slaToMinutes(sla) > this.templateSla) {
				var slaInDaysHours = this.convertMinutesToDaysHours(this.templateSla);
				var msg = Ext.String.format('<font color="red"><i>SLA must not exceed {0} days and {1} hours </i></font>',
					slaInDaysHours.days, slaInDaysHours.hours);
				this.set('activitySlaWarning', msg);
			} else {
				this.set('activitySlaWarning', false);
			}
		},

		parseSlaSection: function(sla) {
			var slaForm = Ext.ComponentQuery.query('form[itemId=sla]')[0];
			var slaRaw = Ext.decode(sla);
			this.templateSla = this.slaToMinutes(slaRaw);
			slaForm.getForm().setValues(slaRaw);
		},

		getSLAContent: function() {
			var slaForm = Ext.ComponentQuery.query('form[itemId=sla]')[0];
			var sla = slaForm.getForm().getFieldValues();
			return '{section:type=sla|title=Untitled Section' + '}\n' + Ext.encode(sla) + '\n{section}'
		},

		getPageContent: function() {
			var automationFilter = this.automationFilter.generateContent();
			return this.wikiMaingetPageContent.apply(this, arguments) + '\n' + this.getSLAContent()
				+ '\n' + this.getProcedureSectionContent() + '\n' + automationFilter;
		},

		getView: function(viewType, viewItemId) {
			if (!this[viewItemId]) {
				var sel = Ext.String.format('{0}[itemId={1}]', viewType, viewItemId);
				this[viewItemId] = Ext.ComponentQuery.query(sel)[0];
			}
			return this[viewItemId];
		},

		getActivityForm: function() {
			return this.getView('form', 'activityForm');
		},

		getGrid: function() {
			return this.getView('treepanel', 'activityGrid');
		},

		// Overide mixin
		checkForDirtyFlag: function() {
			this.handleDirtyFlag();
		},

		reloadContent: function(btn) {
			this.wikiMainReloadContent.apply(this, arguments);
			if(btn == 'yes') {
				this.sectionsLayout.parseSection(this.ucontent);
			}
		},

		preview: function() {
			this.open({
				mtype: 'RS.incident.PlaybookTemplatePreview'
			});
		},

		exitEdit: function() {
			clientVM.handleNavigation({
				modelName: 'RS.incident.PlaybookTemplateRead',
				params: {
					name: this.fullName,
					uname: this.uname,
					unamespace: this.unamespace,
					id: this.id
				}
			});
		},

		returnToTemplates: function() {
			clientVM.handleNavigation({
				modelName: 'RS.incident.PlaybookTemplates'
			});
		},

		// Formulas to control the buttons visibility & disability. Currently we want all of them visible and enable
		phaseIsValid$ : function(){
			return this.phase && RS.common.validation.PlainText.test(this.phase) ? true : this.localize('invalidPhase');
		},
		activityNameIsValid$ : function(){
			return this.activityName && RS.common.validation.PlainText.test(this.activityName) ? true : this.localize('invalidActivityName');
		},
		wikiNameIsValid$: function() {
			return !!this.wikiName && !!this.wikiName.length && this.wikiNameIsValidated;
		},

		addActivityIsEnabled$: function() {
			return this.isValid && !this.activitySlaWarning;
		},
		saveIsEnabled$: function() {
			return !this.templateSlaWarning && this.dirtyFlag;
		},
		saveAsIsEnabled: true,
		saveIsVisible$: function() {
			return true;
		},

		saveAndCommitIsVisible$: function() {
			return true;
		},

		addLayoutSectionIsVisible$: function() {
			return true;
		},

		sourceIsVisible$: function() {
			return true;
		},

		reloadIsVisible$: function() {
			return true;
		},

		lockedDisplayText$: function() {
			return this.softLock && this.lockedByUsername != user.name ? this.localize('lockedBy', [this.lockedByUsername]) : '';
		},

		automationFilter : {
			mtype : 'RS.incident.PlaybookTemplate.AutomationFilter'
		},

		beforeDestroyComponent: function() {
			//Remove this handler from document's event.
			this.fireEvent('removeEventReference');
		}
	}
}());

glu.defModel('RS.incident.PlaybookTemplate.AutomationFilter',{
	fields : [
		//Fields without UI
		'descriptionWidth', 'autoCollapse', 'collapsed', 'autoHide', 'progress',
		//Fields with UI
		'refreshInterval','refreshCountMax', 'filterValue','order',{
			name : 'encodeSummary',
			type : 'boolean'
		},{
			name : 'selectAll',
			type : 'boolean'
		},{
			name : 'includeStartEnd',
			type : 'boolean'
		},{
			name : 'preserveTaskOrder',
			type : 'boolean'
		}],
	defaultData : {
		refreshInterval: 5,
		refreshCountMax: 60,
		descriptionWidth: 0,
		autoCollapse: false,
		collapsed : false,
		autoHide : false,
		progress : false,
		order: 'DESC',
		selectAll : true,
		includeStartEnd: false,
		encodeSummary: false,
		preserveTaskOrder: true,
	},
	filterStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	orderStore: {
		mtype: 'store',
		fields: ['name', 'value']
	},
	when_filters_changed: {
		on: ['filterValueChanged',
			'orderChanged',
			'encodeSummaryChanged',
			'selectAllChanged',
			'includeStartEndChanged',
			'preserveTaskOrderChanged',
			'refreshIntervalChanged',
			'refreshCountMaxChanged'],
		action : function(){
			this.rootVM.handleDirtyFlag();
		}
	},
	init : function(){
		this.filterStore.loadData([{
			name: this.localize('GOOD'),
			value: 'GOOD'
		}, {
			name: this.localize('WARNING'),
			value: 'WARNING'
		}, {
			name: this.localize('SEVERE'),
			value: 'SEVERE'
		}, {
			name: this.localize('CRITICAL'),
			value: 'CRITICAL'
		}])

		this.orderStore.loadData([{
			name: this.localize('ASC'),
			value: 'ASC'
		}, {
			name: this.localize('DESC'),
			value: 'DESC'
		}])
		this.loadData(this.defaultData);
		this.actionTasks.on('datachanged', this.rootVM.handleDirtyFlag, this.rootVM);
		this.actionTasks.on('update', this.rootVM.handleDirtyFlag, this.rootVM)
	},
	serializeAT: function(node) {
		var ats = [],
			data = Ext.clone(node.data),
			ser = [],
			split = data.actionTask.split('#');

		data.task = split[0]
		data.namespace = split[1]

		Ext.Object.each(data, function(key, value) {
			if (Ext.Array.indexOf(['description', 'descriptionWikiLink', 'namespace', 'nodeId', 'resultWikiLink', 'task', 'wiki', 'autoCollapse', 'tags'], key) > -1 && value)
				ser.push(Ext.String.format('{0}={1}', key, value))
		})

		if (!node.isLeaf()) { //node.childNodes && node.childNodes.length > 0) {
			ats.push('{group:title=' + node.get('description') + (!node.get('autoCollapse') ? '|autoCollapse=' + node.get('autoCollapse') : '') + '}')
			node.eachChild(function(child) {
				ats = ats.concat(this.serializeAT(child))
			}, this)
			ats.push('{group}')
		} else {
			ats.push('{' + ser.join('|') + '}')
		}

		return ats
	},
	generateContent: function() {
		var actionTasks = [],
			tags = [];
		this.actionTasks.getRootNode().eachChild(function(child) {
			actionTasks = actionTasks.concat(this.serializeAT(child))
		}, this)
		return '{section:type=result|title=Untitled Section' + '}\n' + '{result2:title=' + this.title + '|encodeSummary=' + this.encodeSummary + '|refreshInterval=' + this.refreshInterval + '|refreshCountMax=' + this.refreshCountMax + '|descriptionWidth=' + this.descriptionWidth + '|order=' + this.order + '|autoCollapse=' + this.autoCollapse + '|collapsed=' + this.collapsed + '|autoHide=' + this.autoHide + '|filter=' + (this.filterValue || '') + '|progress=' + this.progress + '|selectAll=' + this.selectAll + '|includeStartEnd=' + this.includeStartEnd + '|preserveTaskOrder=' + this.preserveTaskOrder + '}\n' + actionTasks.join('\n') + '\n{result2}' + '\n{section}';
	},
	parseAutomationFilterSection : function(section){
		//Parse Properties first
		this.parseProperties(section);

		//COMPATIBILITY : parse action tasks from content. Make sure task summary (which will be used as description) is in 1 line.
		var content = section.replace(/([\s\S]*)({description=[^{]*})([\s\S]*)/g, function(v,g1,g2,g3){
			return g1 + g2.replace(/\n/g, ' ') + g3;
		});
		var	contents = (content|| '').split('\n');

		//Remove first and last containing {result2} tags
		contents.splice(contents.length - 1, 1)
		contents.splice(0, 1);

		//Parse this actiontask list.
		var processedActionTasks = processActionTasks(contents);
		this.actionTasks.getRootNode().removeAll();
		Ext.Array.forEach(processedActionTasks, function(at) {
			this.actionTasks.getRootNode().appendChild(at)
		}, this)
	},
	parseProperties: function(content) {
		var configurationLines = (content || '').split('\n'),
			properties = {};

		//Parse porperties from content
		var props = this.extractComponentProperties(configurationLines);
		for (var j = 0; j < props.length; j++) {
			var split = props[j].split('=');

			if (split.length > 1) {
				properties[split[0]] = split.slice(1).join('=');
			} else  {
				properties['text'] = split[0];
			}
		}

		//Conversion
		var filterParams = {};
		filterParams.refreshInterval = isNaN(+properties.refreshInterval) ? 5 : +properties.refreshInterval;
		filterParams.refreshCountMax = isNaN(+properties.refreshCountMax) ? 60 : +properties.refreshCountMax;
		filterParams.descriptionWidth = isNaN(+properties.descriptionWidth) ? 0 : +properties.descriptionWidth;
		filterParams.filterValue = (properties.filter || '').split(',');

		if (properties.encodeSummary) filterParams.encodeSummary = properties.encodeSummary === 'true';
		if (properties.order) filterParams.order = properties.order.toLowerCase() == 'asc' ? 'ASC' : 'DESC';
		if (properties.autoCollapse) filterParams.autoCollapse = properties.autoCollapse === 'true';
		if (properties.collapsed) filterParams.collapsed = properties.collapsed === 'true';
		if (properties.autoHide) filterParams.autoHide = properties.autoHide === 'true';
		if (properties.progress) filterParams.progress = properties.progress === 'true';
		if (properties.selectAll) filterParams.selectAll = properties.selectAll === 'true';
		if (properties.includeStartEnd) filterParams.includeStartEnd = properties.includeStartEnd === 'true';
		if (properties.preserveTaskOrder) filterParams.preserveTaskOrder = properties.preserveTaskOrder === 'true';

		this.loadData(filterParams);
	},
	extractComponentProperties: function (configurationLines) {
		var props = [];
		if (configurationLines.length > 0) {
			var c = configurationLines.splice(0, 1)[0];
			var colonIndex = c.indexOf(':');
			if(colonIndex > -1) {
				var parenIndex = c.indexOf('}');
				var endIndex = c.length - 1;
				if(parenIndex > -1) {
					endIndex = parenIndex;
				}
				props = c.substring(colonIndex + 1, endIndex).split('|');
			}
		}
		return props;
	},

	//Action Task Filter
	actionTasksSelections: [],
	actionTasks: {
		mtype: 'treestore',
		fields: ['id', 'description', 'descriptionWikiLink', 'namespace', 'nodeId', 'resultWikiLink', 'task', 'wiki', 'actionTask', 'tags', {
			name: 'autoCollapse',
			type: 'boolean'
		}],
		proxy: {
			type: 'memory'
		}
	},
	selectionchange: function(records) {
		this.set('actionTasksSelections', records);
	},
	addGroup: function() {
		if (this.actionTasksSelections.length > 0 && !this.actionTasksSelections[0].isLeaf()) {
			this.actionTasksSelections[0].appendChild({
				description: this.localize('newGroup'),
				expanded: true
			})
		} else {
			this.actionTasks.getRootNode().appendChild({
				description: this.localize('newGroup'),
				expanded: true
			})
		}
	},
	addActionTask: function() {
		var currentDocName = this.rootVM.fullName;
		this.open({
			mtype: 'RS.actiontask.ActionTaskPicker',
			multiSelection : true,
			currentDocName : currentDocName,
			dumper: {
				scope: this,
				dump: this.reallyAddActionTask
			}
		})
	},
	reallyAddActionTask: function(selections) {
		//Parse summary txt to make it 1 line description.
		Ext.Array.each(selections,function(selection){
			var rawTaskSummary = selection.get('usummary');
			var parsedSummary = rawTaskSummary ? rawTaskSummary.replace(/\n/g, '. ') : '';
			if (this.actionTasksSelections.length > 0 && !this.actionTasksSelections[0].isLeaf()) {
				this.actionTasksSelections[0].appendChild({
					leaf: true,
					actionTask: selection.get('uname') + '#' + selection.get('unamespace'),
					description: parsedSummary,
					task: selection.get('uname'),
					namespace: selection.get('unamespace')
				})
			} else {
				this.actionTasks.getRootNode().appendChild({
					leaf: true,
					actionTask: selection.get('uname') + '#' + selection.get('unamespace'),
					description: parsedSummary,
					task: selection.get('uname'),
					namespace: selection.get('unamespace')
				})
			}
		},this)
	},
	edit: function() {
		this.open(Ext.applyIf({
			mtype: 'RS.wiki.pagebuilder.TaskEdit'
		}, this.actionTasksSelections[0].data))
	},
	remove: function() {
		Ext.each(this.actionTasksSelections, function(t) {
			t.remove();
		}, this);
	},
});

glu.defModel('RS.incident.PlaybookTemplateNamer', {
	name: '',
	namespace: '',

	namespaceStore: {
		mtype: 'store',
		fields: ['unamespace'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/nsadmin/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	init: function() {
	},

	wait: false,
	create: function() {
		var newTemplateName = Ext.String.trim(this.namespace) + '.' + Ext.String.trim(this.name); 
		this.set('wait', true);
		this.ajax({
			url : '/resolve/service/playbook/template/new',
			params : {
				fullname: newTemplateName
			},
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					clientVM.handleNavigation({
						modelName: 'RS.incident.PlaybookTemplate',
						params: {
							name: newTemplateName,
							new: true,
							type: 'nist'
						}
					});
					this.doClose();
				} else {
					clientVM.displayError(respData.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},

	namespaceIsValid$: function() {
		return RS.common.validation.TaskNamespace.test(this.namespace) ? true : this.localize('invalidNamespace');
	},

	nameIsValid$: function() {
		return RS.common.validation.TaskName.test(this.name) ? true : this.localize('invalidName');
	},

	createIsEnabled$: function() {
		return !this.wait && this.isValid;
	},

	cancel: function() {
		this.doClose();
	}
});

glu.defModel('RS.incident.PlaybookTemplatePreview', {
	mixins: ['Playbook'],
	isPreview: true,
	init: function() {
		// previewing from Template Builder
		this.activitiesTab.set('isPreview', true);
		this.set('sysCreatedOn', this.parentVM.sysCreatedOn);
		this.set('sysCreatedBy', this.parentVM.sysCreatedBy);

		if (!this.previewUContent) {
			this.set('previewUContent', this.parentVM.getPageContent());
			this.set('fullName', this.parentVM.ufullname);
		}

		if (this.previewContent) {
			this.activitiesTab.set('previewContent', this.previewContent);
		}

		setTimeout(function(){
			this.fireEvent('loadDescription', this.previewUContent, this.fullName);
		}.bind(this), 500);
	},
	activate: function(screen, params) {
		if (params.name) {
			this.set('fullName', params.name);
			this.getPreviewContent(function(){
				this.fireEvent('loadDescription', this.previewUContent, this.fullName);
			});
		}
	},

	expandDescription: function() {
		var modal = this.open({
			mtype: 'RS.incident.DescriptionModal',
			ticketInfo: this.ticketInfo
		});
		modal.fireEvent('loadDescriptionModal', this.previewUContent, this.fullName)
	},
	returnTitle$: function() {
		return this.fullName;
	},
	returnId$: function() {
		return 'SIR-000000000';
	},
	returnDateCreated$: function() {
		if (this.sysCreatedOn) {
			return Ext.util.Format.date(new Date(this.sysCreatedOn), clientVM.getUserDefaultDateFormat());
		} else {
			return '';
		}
 	},
	returnSeverity$: function() {
		return 'Test';
	},
	returnType$: function() {
		return 'Test';
	},
	returnStatus$: function() {
		return 'Open';
	},
	returnDataCompromised$: function() {
		return 'Unknown';
	},
	returnCreatedBy$: function() {
		return this.sysCreatedBy;
	},

	close: function() {
		this.doClose()
	}
})

glu.defModel('RS.incident.PlaybookTemplateSearchPreview', {
	mixins: ['Playbook'],
	isReadOnly: true,
	activate: function(screen, params) {
		if (params.name) {
			this.set('fullName', params.name);
			this.activitiesTab.set('isReadOnly', true);
			this.getPreviewContent(function(){
				this.fireEvent('loadDescription', this.previewUContent, this.fullName);
			});
		}
	}
});

glu.defModel('RS.incident.PlaybookTemplateRead', {
	mixins: ['Playbook'],
	isReadOnly: true,
	activate: function(screen, params) {
		this.applyRBAC();
		if (params.name) {
			this.set('fullName', params.name);
			this.activitiesTab.set('isReadOnly', true);
			this.getPreviewContent(function(){
				this.fireEvent('loadDescription', this.previewUContent, this.fullName);
			});
		}
		if (!this.pSirPlaybookTemplatesTemplateBuilderView) {
			this.message({
				title: this.localize('insufficientPermissionMsgTitle'),
				msg: this.localize('insufficientPermissionMsg'),
				closable : false,
				buttonText: {
					ok: this.localize('close')
				},
				scope: this,
				fn: function(btn) {
					clientVM.handleNavigationBackOrExit();
				}
			});
		}
	},

	pSirPlaybookTemplatesTemplateBuilderModify: false,
	pSirPlaybookTemplatesCreate: false,
	pSirPlaybookTemplatesTemplateBuilderView: false,
	applyRBAC: function() {
		var permissions = clientVM.user.permissions || {};
		var pCreate = permissions['sir.playbookTemplates.templateBuilder.create'];
		var pModify = permissions['sir.playbookTemplates.templateBuilder.change'];

		this.set('pSirPlaybookTemplatesTemplateBuilderModify', pModify);
		this.set('pSirPlaybookTemplatesCreate', pCreate);

		this.set('editIsEnabled', pModify);
		this.set('copyIsEnabled', pCreate);

		this.set('pSirPlaybookTemplatesTemplateBuilderView', true);
	},
	init: function() {
		this.applyRBAC();
	},

	expandDescription: function() {
		var modal = this.open({
			mtype: 'RS.incident.DescriptionModal',
			ticketInfo: this.ticketInfo
		});
		modal.fireEvent('loadDescriptionModal', this.previewUContent, this.fullName)
	},
	returnTitle$: function() {
		return this.fullName;
	},

	edit: function() {
		clientVM.handleNavigation({
			modelName: 'RS.incident.PlaybookTemplate',
			params: {
				name: this.fullName
			}
		});
	},
	editIsEnabled: false,
	copyIsEnabled: false,

	copy: function() {
		var me = this;

		this.open({
			mtype: 'RS.wiki.CopyWiki',
			name: this.uname,
			namespace: this.unamespace,
			id: this.id
		})
	},

	returnToTemplates: function() {
		clientVM.handleNavigation({
			modelName: 'RS.incident.PlaybookTemplates'
		});
	},
	preview: function() {
		this.open({
			mtype: 'RS.incident.PlaybookTemplatePreview',
			previewUContent: this.previewUContent,
			previewContent: this.activitiesTab.previewContent,
			fullName: this.fullName
		});
	},
	reload: function() {
		this.getPreviewContent(function(){
			this.fireEvent('loadDescription', this.previewUContent, this.fullName);
		});
	},
});

glu.defModel('RS.incident.PlaybookTemplates', {
	store: {
		mtype: 'store',
		pageSize : 25,
		remoteSort: true,
		fields: ['id', 'uname', 'unamespace', 'usummary', 'ufullname', 'uisActive',
			{name: 'uversion', type: 'int'}].concat(RS.common.grid.getSysFields()),
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wikiadmin/listPlaybookTemplates',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	columns : [],
	templateSelections: [],
	pSirPlaybookTemplatesCreate: false,
	pSirPlaybookTemplatesView: false,
	pSirPlaybookTemplatesTemplateBuilderView: false,
	pSirPlaybookTemplatesTemplateBuilderModify: false,
	inactiveColumnShow: false,

	applyRBAC: function() {
		var permissions = clientVM.user.permissions || {};

		permissions.sirPlaybookTemplates = permissions.sirPlaybookTemplates || {};
		this.set('pSirPlaybookTemplatesView', permissions['sir'] && (permissions['sir.playbookTemplates.templateBuilder.create'] || permissions['sir.playbookTemplates.templateBuilder.change']));

		permissions.sirPlaybookTemplatesTemplateBuilder = permissions.sirPlaybookTemplatesTemplateBuilder || {};
		this.set('pSirPlaybookTemplatesTemplateBuilderView', permissions['sir'] && (permissions['sir.playbookTemplates.templateBuilder.create'] || permissions['sir.playbookTemplates.templateBuilder.change']));
		this.set('pSirPlaybookTemplatesTemplateBuilderModify', permissions['sir.playbookTemplates.templateBuilder.change']);
		this.set('pSirPlaybookTemplatesCreate', permissions['sir.playbookTemplates.templateBuilder.create']);
	},

	checkViewPermission: function() {
		return this.pSirPlaybookTemplatesView;
	},

	init: function() {
		var me = this;
		this.applyRBAC();
		this.store.on('beforeload', function(store, op) {
			var filters = {};
			if (!me.inactiveColumnShow) {
				filters.field="uisActive";
				filters.type="bool";
				filters.condition="equals";
				filters.value="true"
			}
			op.params = {};
			op.params.filter = JSON.stringify(filters);
			return me.checkViewPermission();
		});
		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'uname',
			filterable: true,
			flex: 1,
		}, {
			header: '~~namespace~~',
			dataIndex: 'unamespace',
			flex: 1,
			filterable: true
		}, {
			header: '~~revision~~',
			dataIndex: 'uversion',
			flex: 0.5,
			filterable: true
		}, {
			header: '~~inactive~~',
			dataIndex: 'uisActive',
			flex: 0.2,
			filterable: true,
			hidden: true,
			renderer:  function(value) { //,
				if (value) {
					return '';
				}
				return '<span class="icon-large icon-ok-sign rs-boolean-renderer"></span>'
			},
			align: 'center',
			listeners: {
				show: function() {
					me.set('inactiveColumnShow', true);
					me.store.load();
				},
				hide: function() {
					me.set('inactiveColumnShow', false);
					me.store.load();
				}
			}
		}].concat((function(columns) {
			Ext.each(columns, function(col) {
				if (col.dataIndex == 'sysCreatedOn') {
					col.hidden = false;
					col.width = 200,
					col.initialShow = true;
				}
				if (col.dataIndex == 'sysCreatedBy') {
					col.hidden = false;
					col.initialShow = true;
				}
				if (col.dataIndex == 'sysUpdatedOn') {
					col.hidden = false;
					col.initialShow = true;
				}
				if (col.dataIndex == 'sysUpdatedBy') {
					col.hidden = false;
					col.initialShow = true;
				}
				if (col.dataIndex == 'id') {
					col.flex = 1.5;
				}
			});
			return columns;
		})(RS.common.grid.getSysColumns())));
		//this.store.on('load', function() {
		//	var respData = this.store.proxy.reader.rawData;
		//	if (respData && !respData.success)
		//		clientVM.displayError(this.localize('listTemplatesError', respData.message));
		//}, this);
	},

	activate: function(screen, params) {
		this.applyRBAC();
		if (!this.pSirPlaybookTemplatesView) {
			this.message({
				title: this.localize('insufficientPermissionMsgTitle'),
				msg: this.localize('insufficientPermissionMsg'),
				closable : false,
				buttonText: {
					ok: this.localize('close')
				},
				scope: this,
				fn: function(btn) {
					clientVM.handleNavigationBack();
				}
			});
		} else {
			this.store.reload();
		}
	},

	editTemplate: function(id) {
		var r = this.store.findRecord('id', id);
		if (this.pSirPlaybookTemplatesTemplateBuilderView) {
			clientVM.handleNavigation({
				modelName: 'RS.incident.PlaybookTemplateRead',
				params: {
					name: r.get('ufullname'),
					uname: r.get('uname'),
					unamespace: r.get('unamespace'),
					id: r.get('id')
				}
			});
		}
	},
	createTemplate: function() {
		var me = this;
		this.open({
			mtype: 'RS.incident.PlaybookTemplateNamer'
		});
	},
	activatePBIsEnabled$: function() {
		// Make the Activate button enabled if there is an inactivated template selected
		var enabled = false;
		for (var i=0; !enabled && i<this.templateSelections.length; i++) {
			enabled = !this.templateSelections[i].get('uisActive');
		}
		return !!enabled;  // Making sure only true or false value is returned
	},
	activatePB: function() {
		this.updatePBActiveStatus('Activate');
	},

	inactivateIsEnabled$: function() {
		// Make the Inactivate button enabled if there is an activated template selected
		var enabled = false;
		for (var i=0; !enabled && i<this.templateSelections.length; i++) {
			enabled = this.templateSelections[i].get('uisActive');
		}
		return !!enabled;  // Making sure only true or false value is returned
	},

	inactivate: function() {
		this.updatePBActiveStatus('Inactivate');
	},

	updatePBActiveStatus: function(action) {
		var ids = [];
		for (var i=0; i<this.templateSelections.length; i++) {
			ids.push(this.templateSelections[i].get('id'));
		}
		this.ajax({
			url: '/resolve/service/playbook/updatePBActiveStatus',
			method: 'POST',
			scope: this,
			params: {
				status: action == 'Activate'? true: false
			},
			jsonData: {
				playbookSysids: ids
			},
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (respData.success) {
					clientVM.displaySuccess(this.localize('successfully'+action+'PBs'));
					this.store.reload();
				} else {
					clientVM.displayError(respData.message);
				}
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	deleteTemplate: function() {
	}
});
glu.defModel('RS.incident.ReportTemplates', {
	allSelected: false,	
	reportTemplateSelections: [],
	wait: false,
	reportTemplateStore: {
		mtype: 'store',
		pageSize : 25,
		remoteSort: true,
		fields: ['id', 'uname', 'unamespace', 'usummary', 'ufullname', {name: 'uversion', type: 'int'}].concat(RS.common.grid.getSysFields()),
		sorters: [{
			property: 'sysCreatedOn',
			direction: 'DESC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/wikiadmin/listRunbooks',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		},
		autoLoad : true
	},

	init : function(){
		this.reportTemplateStore.on('beforeload', function(store, op) {
			this.set('wait', true);
		}, this);
		this.reportTemplateStore.on('load', function(store, records, success) {
			this.set('wait', false);
			//if (!success)
			//	clientVM.displayError(this.localize('listTemplatesError', this.store.getProxy().getReader().rawData.message));
		}, this);
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},
	showMessage: function(title, OpsMsg, OpMsg, confirm, url, errMsg, sucMsg, params) {
		var strFullName = [];
		Ext.each(this.reportTemplateSelections, function(r, idx) {
			strFullName.push(r.get('ufullname'));
		});
		var msg = this.localize(this.reportTemplateSelections.length > 1 || this.allSelected ? OpsMsg : OpMsg, {
			num: this.allSelected ? this.reportTemplateStore.getTotalCount() : this.reportTemplateSelections.length,
			names: strFullName.join(',')
		});

		// msg += '<br/><input type="checkbox" id="index_attachment" /> Index Attachments' +
		// '<input type="checkbox" id="index_actiontask" /> Index ActionTasks<br/><br/>';
		this.message({
			title: this.localize(title),
			msg: msg,
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize(confirm),
				no: this.localize('cancel')
			},
			scope: this,
			fn: function(btn) {
				if (btn != 'yes')
					return;
				params = params || {};
				params.indexAttachment = true;
				params.indexActionTask = true;
				this.sendReq(url, errMsg, sucMsg, params, this);
			}
		});
	},
	sendReq: function(url, errMsg, sucMsg, params, lockScreen) {
		lockScreen.set('wait', true);
		var ids = [];
		Ext.each(this.reportTemplateSelections, function(r, idx) {
			ids.push(r.get('id'));
		});
		var reqParams = {};
		reqParams.ids = ids;
		reqParams.all = this.allSelected;
		if (params)
			Ext.apply(reqParams, params);
		var win = this.open({
			mtype: 'RS.wiki.Polling'
		});
		this.ajax({
			url: url,
			params: reqParams,
			scope: this,
			success: function(resp) {
				win.doClose();
				this.handleSucResp(resp, errMsg, sucMsg);
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				lockScreen.set('wait', false);
			}
		});
	},
	handleSucResp: function(resp, errMsg, sucMsg) {
		var respData = RS.common.parsePayload(resp);
		if (!respData.success) {
			clientVM.displayError(this.localize(errMsg) + '[' + respData.message + ']');
		} else
			clientVM.displaySuccess(this.localize(sucMsg));
		this.set('reportTemplateSelections', []);
		this.reportTemplateStore.load();
	},
	new : function(){

	},
	copy : function(){
		var me = this;
		this.open({
			mtype: 'RS.wiki.pagebuilder.MoveOrRename',
			action: 'copy',
			namespace: this.reportTemplateSelections[0].get('unamespace'),
			filename: this.reportTemplateSelections[0].get('uname'),
			filenameIsVisible$: function() {
				return me.reportTemplateSelections.length == 1;
			},
			moveRenameCopyTitle: 'CopyTitle',
			viewmodelName: this.viewmodelName,
			dumper: (function(self) {
				return function(params) {
					self.doCopy(params)
				}
			})(this)
		});		
	},
	doCopy : function(params){
		this.showMessage(
			'copyTemplate',
			'copysMsg',
			'copyMsg',
			'copyTemplate',
			'/resolve/service/wikiadmin/moveRenameCopy',
			'copyErrMsg',
			'copySucMsg',
			params
		);
	},
	copyIsEnabled$: function() {
		return !this.wait && this.reportTemplateSelections.length > 0;
	},
	purge : function(){
		this.showMessage(
			'purgeTemplate',
			'purgesMsg',
			'purgeMsg',
			'purgeTemplate',
			'/resolve/service/wikiadmin/purge',
			'purgeErrMsg',
			'purgeSucMsg'
		);
	},
	purgeIsEnabled$: function() {
		return !this.wait && this.reportTemplateSelections.length > 0;
	},
	goToDashboard : function(){
		clientVM.handleNavigation({
			modelName: 'RS.incident.Dashboard'			
		});
	},
	editTemplate: function(id) {
		var r = this.reportTemplateStore.findRecord('id', id);
		clientVM.handleNavigation({
			modelName: 'RS.incident.PlaybookTemplate',
			params: {
				name: r.get('ufullname')
			}
		});
	},
})
Ext.define('RS.common.plugin.SearchPager', {
	extend: 'Ext.AbstractPlugin',
	alias: 'plugin.searchpager',
	mixins: {
		bindable: 'Ext.util.Bindable'
	},

	//Pagination Config
	pageable: true,
	allowAutoRefresh: true,
	showSysInfo: true,
	displayJumpMenu: true,
	autoRefreshInterval: 30,
	autoRefreshEnabled: false,
	showSocial: false,
	socialStreamType: '',

	//Button configuration
	buttons: [],
	init: function(grid) {
		var me = this,
			myGrid = grid;

		me.grid = myGrid;

		if(this.clobCheckURL != ""){
			Ext.Ajax.request({
				url : this.clobCheckURL,
				success : function(resp){
					var response = RS.common.parsePayload(resp);
					me.clobTypeFields = me.convertColumnToField(response.data);
				},
				failure: function(resp) {
					clientVM.displayFailure(resp);
				}
			})
		}
		if (!me.grid.isXType('grid')) {
			me.originalGrid = me.grid
			me.grid = me.grid.down(this.gridName ? '#' + this.gridName : 'grid')
		}
		me.gridColumns = [];

		me.conditions = {
			on: RS.common.locale.on,
			after: RS.common.locale.after,
			before: RS.common.locale.before,
			equals: RS.common.locale.equals,
			notEquals: RS.common.locale.notEquals,
			contains: RS.common.locale.contains,
			notContains: RS.common.locale.notContains,
			startsWith: RS.common.locale.startsWith,
			notStartsWith: RS.common.locale.notStartsWith,
			endsWith: RS.common.locale.endsWith,
			notEndsWith: RS.common.locale.notEndsWith,
			greaterThan: RS.common.locale.greaterThan,
			greaterThanOrEqualTo: RS.common.locale.greaterThanOrEqualTo,
			lessThan: RS.common.locale.lessThan,
			lessThanOrEqualTo: RS.common.locale.lessThanOrEqualTo,
			shortEquals: RS.common.locale.shortEquals,
			shortNotEquals: RS.common.locale.shortNotEquals,
			shortGreaterThan: RS.common.locale.shortGreaterThan,
			shortGreaterThanOrEqualTo: RS.common.locale.shortGreaterThanOrEqualTo,
			shortLessThan: RS.common.locale.shortLessThan,
			shortLessThanOrEqualTo: RS.common.locale.shortLessThanOrEqualTo
		};

		me.shortConditions = {};
		me.shortConditions[RS.common.locale.shortEquals] = 'equals';
		me.shortConditions[RS.common.locale.shortNotEquals] = 'notEquals';
		me.shortConditions[RS.common.locale.shortGreaterThan] = 'greaterThan';
		me.shortConditions[RS.common.locale.shortGreaterThanOrEqualTo] = 'greaterThanOrEqualTo';
		me.shortConditions[RS.common.locale.shortLessThan] = 'lessThan';
		me.shortConditions[RS.common.locale.shortLessThanOrEqualTo] = 'lessThanOrEqualTo';

		me.dataValues = {
			today: RS.common.locale.today,
			yesterday: RS.common.locale.yesterday,
			lastWeek: RS.common.locale.lastWeek,
			lastMonth: RS.common.locale.lastMonth,
			lastYear: RS.common.locale.lastYear
		};

		me.grid.store.on('beforeLoad', function(store, operation, eOpts) {
			var whereClause = this.getWhereClause();
			var	whereFilter = this.getWhereFilter();
			operation.params = operation.params || {};

			//merge any existing filters
			if (operation.params.filter) {
				var existingFilter = Ext.decode(operation.params.filter)
				whereFilter = whereFilter.concat(existingFilter)
			}

			Ext.apply(operation.params, {
				filter: Ext.encode(whereFilter)
			});
			store.lastWhereClause = whereClause;
		}, me);


		if (me.grid.store.parentVM && !me.grid.store.parentVM.activate)
			me.grid.store.parentVM.activate = Ext.emptyFn

		if (me.grid.store.parentVM)
			me.grid.store.parentVM.activate = Ext.Function.createInterceptor(me.grid.store.parentVM.activate, function() {
				me.parseWindowParameters();
			})

		me.grid.on('celldblclick', function(grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
			var column = grid.getHeaderCt().getHeaderAtIndex(cellIndex);
			var	dataIndex = column.dataIndex;
			var	cellValue = record.get(dataIndex);
			var operator = RS.common.locale.equals;
			Ext.each(me.clobTypeFields, function(clobField){
				if(column.dataIndex == clobField){
					operator = RS.common.locale.contains;
					return;
				}
			})

			if (!column.filterable) return;
			if (column.computeFilter) {
				me.addFilter(column.computeFilter(cellValue));
				return;
			}
			if (Ext.isDate(cellValue)) {
				//cellValue = Ext.Date.format(cellValue, me.dateFormat);
				cellValue = Ext.Date.format(cellValue, me.submitFormat);
				operator = RS.common.locale.on;
			}

			if (Ext.isBoolean(cellValue)) cellValue = cellValue.toString();

			if (Ext.isNumber(cellValue)) cellValue = cellValue.toString();

			if (cellValue.indexOf('|&|') > -1) {
				cellValue = cellValue.replace(/\|&\|/g, '|');
				operator = RS.common.locale.contains;
			}
			if (dataIndex) me.addFilter(column.text + ' ' + operator + ' ' + cellValue); //Check data index for action columns or columns that aren't a part of the data
		}, me);

		me.grid.on('reconfigure', function() {
			Ext.defer(function() {
				me.parseWindowParameters()
				me.grid.store.load()
			}, 1, me)
		})

		var fields = me.grid.store.proxy.reader.model.getFields();
		Ext.each(me.grid.columns, function(column) {
			var col = column;
			Ext.each(fields, function(field) {
				if (field.name == col.dataIndex) {
					col.dataType = field.type.type;
					return false;
				}
			});
			me.gridColumns.push(col);
		});

		me.searchStore = Ext.create('Ext.data.Store', {
			fields: ['name', 'value', 'type', 'displayName'],
			proxy: {
				type: 'memory',
				reader: {
					type: 'json',
					root: 'records'
				}
			},
			listeners: {
				load: function(store, records, successful, eOpts) {
					var combo = me.toolbar.down('#searchText'),
						value = combo.nextVal || combo.getValue();
					combo.nextVal = null;
					me.parseSuggestionsForStore(store, value);
				}
			}
		});

		me.filterStore = Ext.create('Ext.data.Store', {
			model: 'RS.common.model.Filter',
			proxy: {
				type: 'ajax',
				url: '/resolve/service/filter/list',
				reader: {
					type: 'json',
					root: 'records'
				}
			}
		});

		me.filterStore.on('load', function() {
			me.filterStore.add({
				name: RS.common.locale.manageFilters,
				value: 'managefilters',
				sysId: 'managefilters'
			})
		})

		me.filterStore.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}

			Ext.apply(operation.params, {
				table: me.grid.tableName,
				view: me.grid.viewName
			})
		})

		if (me.allowPersistFilter) me.filterStore.load();

		me.viewStore = Ext.create('Ext.data.Store', {
			model: 'RS.common.model.View',
			proxy: {
				type: 'ajax',
				url: '/resolve/service/view/list',
				reader: {
					type: 'json',
					root: 'records'
				},
				listeners: {
					exception: function(e, resp, op) {
						clientVM.displayExceptionError(e, resp, op);
					}
				}
			},
			listeners: {
				scope: me,
				load: function(store, records, successful, eOpts) {
					this.configureViewMenuItems()
				}
			}
		});

		me.viewStore.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}

			Ext.apply(operation.params, {
				table: me.grid.tableName
			})
		})

		me.grid.on('serverDataChanged', function() {
			this.configureViewMenuItems()
		}, me)

		if (me.grid.tableName && !me.hideMenu) me.viewStore.load();

		var displayTableMenuItem = {
			cls: 'rs-display-name',
			text: me.displayName || me.grid.displayName,
			itemId: 'tableMenu'
		};

		if (me.hideMenu) {
			displayTableMenuItem.xtype = 'tbtext';
		} else {
			displayTableMenuItem.listeners = {
				render: function(button) {
					Ext.fly(button.getEl().query('span.x-btn-inner')[0]).setStyle({
						'font-size': '16px',
						'font-weight': 'bold'
					})
				}
			}
			displayTableMenuItem.menu = [{
					text: RS.common.locale.views,
					itemId: 'viewMenuItem',
					menu: {
						items: []
					}
				},
				{
					text: RS.common.locale.settings,
					handler: function(button) {
						clientVM.handleNavigation({
							modelName: 'RS.customtable.TableDefinition',
							params: {
								id: me.grid.tableSysId
							}
						})
					}
				}, {
					text: RS.common.locale.saveViewAs,
					scope: me,
					handler: me.saveView
				}
			]
		}

		//PAGINATION
		if (me.grid.hidePager === true) {
			return
		}

		this.autoRefreshInterval = Ext.state.Manager.get('autoRefreshInterval', 30)
		this.autoRefreshEnabled = Ext.state.Manager.get('autoRefreshEnabled', false)

		me.displayMsg = RS.common.locale.pageDisplayText
		me.firstText = RS.common.locale.firstText
		me.prevText = RS.common.locale.prevText
		me.nextText = RS.common.locale.nextText
		me.lastText = RS.common.locale.lastText
		me.refreshText = RS.common.locale.refreshText
		me.configureText = RS.common.locale.configureText
		me.emptyMsg = RS.common.locale.emptyMsg
		me.systemInformation = RS.common.locale.sysInfoButtonTooltip
		me.systemInformationTooltip = RS.common.locale.sysInfoButtonTitle
		me.goToText = RS.common.locale.goToText

		//Auto refresh
		me.autoRefreshText = RS.common.locale.autoRefreshText;

		myGrid.dockedItems.each(function(dockedItem) {
			if (dockedItem.name == 'actionBar') {
				me.toolbar = dockedItem
				return false
			}
		})

		if (!myGrid.isXType('grid')) myGrid = myGrid.down('grid')

		if (me.pageable) myGrid.store.pageSize = myGrid.store.pageSize || 50
		else myGrid.store.pageSize = -1

		//Add Layout for plugins
		var toolbarItems = this.buttons.concat([{
			xtype : 'tbtext',
			text: 'Filter By:',
			itemId: 'tableMenu',
			style : {
				fontWeight : 'bold'
			}
		},
		/*displayTableMenuItem, ' ', ' ', ' ', ' ', ' ', ' ', ' ', {
			xtype: 'combobox',
			itemId: 'filter',
			hidden: !me.allowPersistFilter,
			width: 200,
			emptyText: RS.common.locale.SelectFilter,
			store: me.filterStore,
			queryMode: 'local',
			displayField: 'name',
			valueField: 'sysId',
			listeners: {
				scope: me,
				change: function(combo, newValue, oldValue, eOpts) {
					if (newValue) {
						if (newValue == 'managefilters') {
							combo.setValue(oldValue)
							var w = Ext.create('Ext.window.Window', {
								width: 400,
								height: 300,
								layout: 'fit',
								title: RS.common.locale.manageFilters,
								items: [{
									xtype: 'grid',
									tbar: [{
										text: RS.common.locale.deleteText,
										itemId: 'deleteFilter',
										disabled: true,
										scope: this,
										handler: function(btn) {
											var selections = btn.up('window').down('grid').getSelectionModel().getSelection(),
												ids = [];
											Ext.Array.forEach(selections, function(selection) {
												ids.push(selection.get('sysId'))
											})
											Ext.Ajax.request({
												url: '/resolve/service/filter/delete',
												params: {
													sysIds: ids.join(',')
												},
												scope: this,
												success: function(r) {
													var response = RS.common.parsePayload(r)
													if (response.success) {
														clientVM.displaySuccess(ids.length === 1 ? RS.common.locale.filterDeleted : RS.common.locale.filtersDeleted)
														this.filterStore.load()
														btn.up('window').close()
													} else clientVM.displayError(response.message)
												},
												failure: function(r) {
													clientVM.displayFailure(r)
												}
											})
										}
									}],
									selMode: 'MULTI',
									columns: [{
										text: RS.common.locale.name,
										dataIndex: 'name',
										flex: 1
									}],
									store: Ext.create('Ext.data.Store', {
										autoLoad: true,
										model: 'RS.common.model.Filter',
										proxy: {
											type: 'ajax',
											url: '/resolve/service/filter/list',
											extraParams: {
												table: me.grid.tableName,
												view: me.grid.viewName
											},
											reader: {
												type: 'json',
												root: 'records'
											},
											listeners: {
												exception: function(e, resp, op) {
													clientVM.displayExceptionError(e, resp, op);
												}
											}
										}
									}),
									listeners: {
										selectionchange: function(grid, selections) {
											w.down('#deleteFilter')[selections.length > 0 ? 'enable' : 'disable']()
										}
									}
								}],
								buttonAlign: 'left',
								buttons: [{
									xtype: 'button',
									text: RS.common.locale.close,
									handler: function(btn) {
										btn.up('window').close()
									}
								}]
							}).show()
						} else
							this.setFilter(combo.store.getById(newValue).data.value);
					}
				}
			}
		}, */
		{
			xtype: 'combobox',
			itemId: 'searchText',
			width: 520,
			emptyText: RS.common.locale.Filter + RS.common.locale.egText,
			trigger2Cls: 'x-form-search-trigger',
			margin: '0 0 0 20',
			enableKeyEvents: true,
			// autoSelect: false,
			queryMode: 'remote',
			typeAhead: false,
			minChars: 1,
			store: me.searchStore,
			displayField: 'name',
			valueField: 'value',
			queryDelay: 10,
			listConfig: {
				emptyText: RS.common.locale.noSuggestions
			},
			onTriggerClick: function() {
				if (!this.advanced) {
					var pos = this.getPosition(),
						items = [];
					Ext.each(me.grid.columns, function(column) {
						var isClobField = false;
						Ext.each(me.clobTypeFields, function(clobField){
							if(column.dataIndex == clobField){
								isClobField = true;
								return;
							}
						})
						var columnXtype = '',
							comparisons = me.getShortOptionsForDataType(column.dataType, isClobField);

						switch (column.dataType) {
							case 'date':
								switch (column.uiType) {
									case 'Date':
										columnXtype = 'xdatefield';
										break;
									case 'Timestamp':
										columnXtype = 'xtimefield';
										break;
									default:
										columnXtype = 'xdatetimefield';
										break;
								}
								break;
							case 'float':
							case 'int':
								columnXtype = 'numberfield';
								break;
							case 'bool':
								columnXtype = 'booleancombobox';
								break;
							default:
								columnXtype = 'textfield';
								break;
						}

						if (column.filterable) {
							items.push({
								fieldLabel: column.filterLabel || column.tooltip || column.text,
								labelAlign: 'top',
								name: column.dataIndex + '_type',
								xtype: 'fieldcontainer',
								layout: 'hbox',
								items: [{
									xtype: 'combobox',
									name: column.dataIndex + '_comparison',
									width: 130,
									displayField: 'name',
									valueField: 'value',
									padding: '0 5 0 0',
									hidden: !me.showComparisons,
									store: Ext.create('Ext.data.Store', {
										fields: ['name', 'value'],
										data: comparisons
									}),
									value: column.dataType == 'date' ? 'on' : comparisons[0].name
								}, {
									name: column.dataIndex,
									xtype: columnXtype,
									validator: column.validator,
									format: columnXtype == 'xtimefield' ? me.timeFormat : me.dateFormat,
									submitFormat: 'Y-m-d H:i:sO',
									flex: 1,
									listeners: {
										validitychange: function(btn, isValid) {
											this.up().up().up().down('button[name="searchBtn"]')[isValid ? 'enable' : 'disable']();
										}
									}
								}]

							});
						}
					}, this);

					this.advanced = Ext.create('Ext.Window', {
						closeAction: 'hide',
						modal: true,
						cls : 'rs-modal-popup',
						title: 'Advanced Search',					
						width: 800,
						height : 600,
						layout: 'fit',
						padding : 15,					
						items: {
							xtype: 'form',
							autoScroll: true,												
							items: items,							
							buttons: [{
								text: RS.common.locale.search,
								name: 'searchBtn',
								cls : 'rs-med-btn rs-btn-dark',
								handler: function(button) {
									var form = button.up('form').getForm(),
										values = form.getValues();
									Ext.each(me.grid.columns, function(column) {
										var value = values[column.dataIndex],
											comp = values[column.dataIndex + '_comparison'];
										if (value) me.addFilter(Ext.String.format('{0} {1} {2}', column.text, comp, value));
									});
									form.reset();
									button.up('window').close();
								}
							}, {
								text: RS.common.locale.cancel,
								cls : 'rs-med-btn rs-btn-light',
								handler: function(button) {
									button.up('form').getForm().reset()
									button.up('window').close()
								}
							}]
						}
					});
				}			
				this.advanced.show();
			},
			onTrigger2Click: function() {
				if (!this.getValue()) return;
				if (me.addFilter(this.getValue())) this.reset();
			},
			listeners: {
				specialkey: function(field, e) {
					if (e.getKey() == e.ENTER) {
						Ext.defer(function() {
							field.onTrigger2Click();
						}, 10);
					}

					if (e.getKey() == e.TAB) {
						e.preventDefault();
					}
				},
				beforeSelect: function(combo, record, index, eOpts) {
					record.data.name = record.data.displayName;
					record.data.value = record.data.displayName;
					combo.nextVal = record.data.name;
					if (record.data.name.lastIndexOf(' ') == record.data.name.length - 1) {
						combo.store.load({
							params: {
								query: record.data.displayName
							}
						});
						Ext.defer(function() {
							combo.expand();
						}, 10);
					}
				},
				render: function(combo) {
					combo.setVisible(me.displayFilter)
				}
			}
		}]);

		if (me.pageable){
			toolbarItems = toolbarItems.concat([,'->',{
			xtype: 'tbtext',
			itemId: 'displayItem',
			listeners: {
				render: function(tbtext) {
					if (me.displayJumpMenu) {
						tbtext.getEl().on('click', function() {
							if (!tbtext.menu) {
								tbtext.menu = Ext.create('Ext.menu.Menu', {
									items: [{
										xtype: 'numberfield',
										itemId: 'pageNumberField',
										fieldLabel: me.goToText,
										labelWidth: 45,
										width: 120,
										minValue: 1,
										listeners: {
											change: function(field, newValue) {
												if (Ext.isNumber(newValue) && newValue > 0 && me.store.currentPage != newValue && newValue <= me.getPageData().pageCount)
													me.store.loadPage(newValue)
											}
										}
									}]
								})
							}
							tbtext.menu.down('#pageNumberField').setValue(me.store.currentPage)
							tbtext.menu.down('#pageNumberField').setMaxValue(me.getPageData().pageCount)
							tbtext.menu.showBy(tbtext)
						})
					}
				}
			}
				}, {
					itemId: 'first',
					tooltip: me.firstText,
					overflowText: me.firstText,
					iconCls: Ext.baseCSSPrefix + 'tbar-page-first',
					disabled: true,
					handler: me.moveFirst,
					scope: me
				}, {
					itemId: 'prev',
					tooltip: me.prevText,
					overflowText: me.prevText,
					iconCls: Ext.baseCSSPrefix + 'tbar-page-prev',
					disabled: true,
					handler: me.movePrevious,
					scope: me
				}, {
					itemId: 'next',
					tooltip: me.nextText,
					overflowText: me.nextText,
					iconCls: Ext.baseCSSPrefix + 'tbar-page-next',
					disabled: true,
					handler: me.moveNext,
					scope: me
				}, {
					itemId: 'last',
					tooltip: me.lastText,
					overflowText: me.lastText,
					iconCls: Ext.baseCSSPrefix + 'tbar-page-last',
					disabled: true,
					handler: me.moveLast,
					scope: me
				}, ' ', ' '
			])
		}

		if (me.showSysInfo) {
			var showSysInfoButton = {
				xtype: 'button',
				ui: 'system-info-button-small',
				iconCls: 'rs-icon-button icon-info-sign' + (Ext.isGecko ? ' rs-icon-firefox' : ''),
				tooltip: me.systemInformation,
				overflowText: me.systemInformationTooltip,
				enableToggle: true,
				pressed: Ext.state.Manager.get('userShowSysInfoColumns', false),
				handler: me.toggleSysColumns,
				scope: myGrid
			}
			toolbarItems = toolbarItems.concat(showSysInfoButton);
			me.toggleSysColumns.apply(myGrid, [showSysInfoButton])
		}

		toolbarItems = toolbarItems.concat([{
			xtype: me.allowAutoRefresh ? 'splitbutton' : 'button',
			itemId: 'refresh',
			tooltip: me.refreshText,
			overflowText: me.refreshText,
			iconCls: Ext.baseCSSPrefix + 'tbar-loading',
			handler: me.doRefresh,
			scope: me,
			menu: me.allowAutoRefresh ? {
				width: 175,
				items: [{
					text: me.autoRefreshText,
					checked: this.autoRefreshEnabled,
					checkHandler: function(item, checked) {
						var interval = item.ownerCt.down('numberfield').getValue();
						if (checked) {
							me.startAutoRefresh(interval || this.autoRefreshInterval)
							Ext.state.Manager.set('autoRefreshEnabled', true)
						} else {
							me.stopAutoRefresh()
							Ext.state.Manager.set('autoRefreshEnabled', false)
						}
					}
				}, {
					xtype: 'fieldcontainer',
					layout: 'hbox',
					items: [{
						xtype: 'numberfield',
						padding: '5 5 7 60',
						hideLabel: true,
						value: this.autoRefreshInterval,
						width: 60,
						minValue: 10,
						listeners: {
							change: function(field, value) {
								me.changeInterval(value);
							},
							blur: function(field, value) {
								if (this.getValue() < 10)
									this.setValue(10);
							}
						}
					}]
				}]
			} : null,
			listeners: {
				render: function(button) {
					if (!me.notActivateCtrl_R) {
						clientVM.updateRefreshButtons(button);
					}
				}
			}
		}]);

		if (this.autoRefreshEnabled)
			me.startAutoRefresh(this.autoRefreshInterval)

		me.toolbar = Ext.create('Ext.toolbar.Toolbar', {
			enableOverflow: false,
			dock: 'top',
			cls: me.toolbarCls ? me.toolbarCls : '',
			items: toolbarItems
		});
		grid.insertDocked(0, me.toolbar);

		me.filterBar = Ext.create('RS.common.FilterBar', {
			allowPersistFilter: me.allowPersistFilter,
			cls : me.toolbarCls ? me.toolbarCls : '',
			listeners: {
				scope: me,
				filterChanged : {
					fn : me.filterChanged,
					buffer : 10
				},
				saveFilter: me.saveFilter,
				clearFilter: me.clearFilter
			}
		});
		grid.insertDocked(1, me.filterBar);
		me.toolbar.addEvents(
			/**
			 * @event change
			 * Fires after the active page has been changed.
			 * @param {Ext.toolbar.Paging} this
			 * @param {Object} pageData An object that has these properties:
			 *
			 * - `total` : Number
			 *
			 *   The total number of records in the dataset as returned by the server
			 *
			 * - `currentPage` : Number
			 *
			 *   The current page number
			 *
			 * - `pageCount` : Number
			 *
			 *   The total number of pages (calculated from the total number of records in the dataset as returned by the
			 *   server and the current {@link Ext.data.Store#pageSize pageSize})
			 *
			 * - `toRecord` : Number
			 *
			 *   The starting record index for the current page
			 *
			 * - `fromRecord` : Number
			 *
			 *   The ending record index for the current page
			 */
			'change',

			/**
			 * @event beforechange
			 * Fires just before the active page is changed. Return false to prevent the active page from being changed.
			 * @param {Ext.toolbar.Paging} this
			 * @param {Number} page The page number that will be loaded on change
			 */
			'beforechange');

		me.toolbar.on('beforerender', me.onLoad, me, {
			single: true
		});
		me.grid.store.fireEvent('commonSearchFilterInited', me, me);
		me.bindStore(myGrid.store || 'ext-empty-store', true);
	},

	// @private
	updateInfo: function() {
		var me = this,
			displayItem = me.toolbar.child('#displayItem'),
			store = me.store,
			pageData = me.getPageData(),
			count, msg;

		if (displayItem) {
			count = store.getCount();
			if (count === 0) {
				msg = me.emptyMsg;
			} else {
				msg = Ext.String.format(
					me.displayMsg, pageData.fromRecord, pageData.toRecord, pageData.total);
			}
			displayItem.setText(msg);
		}
	},

	// @private
	beforeLoad: function() {
		if (this.rendered && this.refresh) {
			this.refresh.disable();
		}
	},

	// @private
	onLoadError: function() {
		if (!this.rendered) {
			return;
		}
		this.child('#refresh').enable();
	},

	// @private
	onLoad: function() {
		var me = this,
			pageData, currPage, pageCount, afterText, count, isEmpty;

		count = me.store.getCount();
		isEmpty = count === 0;
		if (!isEmpty) {
			pageData = me.getPageData();
			currPage = pageData.currentPage;
			pageCount = pageData.pageCount;
		} else {
			currPage = 0;
			pageCount = 0;
		}

		if (me.pageable) {
			Ext.suspendLayouts();
			me.toolbar.child('#first').setDisabled(currPage === 1 || isEmpty);
			me.toolbar.child('#prev').setDisabled(currPage === 1 || isEmpty);
			me.toolbar.child('#next').setDisabled(currPage === pageCount || isEmpty);
			me.toolbar.child('#last').setDisabled(currPage === pageCount || isEmpty);
			me.toolbar.child('#refresh').enable();
			me.updateInfo();
			Ext.resumeLayouts(true);
		} else {
			me.toolbar.child('#refresh').enable();
			me.updateInfo();
		}

		if (me.toolbar.rendered) {
			me.toolbar.fireEvent('change', me, pageData);
		}
	},

	// @private
	getPageData: function() {
		var store = this.store,
			totalCount = store.getTotalCount();

		return {
			total: totalCount,
			currentPage: store.currentPage,
			pageCount: Math.ceil(totalCount / store.pageSize),
			fromRecord: ((store.currentPage - 1) * store.pageSize) + 1,
			toRecord: Math.min(store.currentPage * store.pageSize, totalCount)

		};
	},

	/**
	 * Move to the first page, has the same effect as clicking the 'first' button.
	 */
	moveFirst: function() {
		var me = this;
		if (me.toolbar.fireEvent('beforechange', me, 1) !== false) {
			me.store.loadPage(1);
		}
	},

	/**
	 * Move to the previous page, has the same effect as clicking the 'previous' button.
	 */
	movePrevious: function() {
		var me = this,
			prev = me.store.currentPage - 1;

		if (prev > 0) {
			if (me.toolbar.fireEvent('beforechange', me, prev) !== false) {
				me.store.previousPage();
			}
		}
	},

	/**
	 * Move to the next page, has the same effect as clicking the 'next' button.
	 */
	moveNext: function() {
		var me = this,
			total = me.getPageData().pageCount,
			next = me.store.currentPage + 1;

		if (next <= total) {
			if (me.toolbar.fireEvent('beforechange', me, next) !== false) {
				me.store.nextPage();
			}
		}
	},

	/**
	 * Move to the last page, has the same effect as clicking the 'last' button.
	 */
	moveLast: function() {
		var me = this,
			last = me.getPageData().pageCount;

		if (me.toolbar.fireEvent('beforechange', me, last) !== false) {
			me.store.loadPage(last);
		}
	},

	/**
	 * Refresh the current page, has the same effect as clicking the 'refresh' button.
	 */
	doRefresh: function() {
		var me = this,
			current = me.store.currentPage;

		if (me.toolbar.fireEvent('beforechange', me, current) !== false) {
			me.store.loadPage(current);
		}
	},

	getStoreListeners: function() {
		return {
			beforeload: this.beforeLoad,
			load: this.onLoad,
			exception: this.onLoadError
		};
	},

	/**
	 * Unbinds the paging toolbar from the specified {@link Ext.data.Store} **(deprecated)**
	 * @param {Ext.data.Store} store The data store to unbind
	 */
	unbind: function(store) {
		this.bindStore(null);
	},

	/**
	 * Binds the paging toolbar to the specified {@link Ext.data.Store} **(deprecated)**
	 * @param {Ext.data.Store} store The data store to bind
	 */
	bind: function(store) {
		this.bindStore(store);
	},

	// @private
	destroy: function() {
		this.stopAutoRefresh()
		this.unbind()
		this.callParent()
	},

	changeInterval: function(interval) {
		if (this.autoRefreshTask) {
			this.stopAutoRefresh()
			this.startAutoRefresh(interval)
		}
		Ext.state.Manager.set('autoRefreshInterval', interval < 10 ? 10 : interval);
	},

	startAutoRefresh: function(interval) {
		if (this.autoRefreshTask) this.autoRefreshTask.destroy();

		var runner = new Ext.util.TaskRunner();
		this.autoRefreshTask = runner.newTask({
			run: this.doRefresh,
			scope: this,
			interval: interval * 1000
		});
		this.autoRefreshTask.start();
	},
	stopAutoRefresh: function() {
		if (this.autoRefreshTask) {
			this.autoRefreshTask.destroy();
			delete this.autoRefreshTask;
		}
	},

	toggleSysColumns: function(button) {
		Ext.state.Manager.set('userShowSysInfoColumns', button.pressed)
		Ext.Array.forEach(this.columns, function(column) {
			if ((column.dataIndex && column.dataIndex.indexOf('sys') == 0 && !column.initialShow) || column.dataIndex == 'id') {
				if (column.rendered)
					column[button.pressed ? 'show' : 'hide']()
				else column.hidden = !button.pressed
			} else if (column.dataIndex && Ext.Array.indexOf(['processNumber', 'duration', 'address', 'targetGUID', 'esbaddr'], column.dataIndex) > -1) {
				//This is for worksheet columns.  Total hack, but Duke wants these columns hidden/shown based on the sysinfo button too
				if (column.rendered)
					column[button.pressed ? 'show' : 'hide']()
				else column.hidden = !button.pressed
			}
		})
	},

	setSocialId: function(value) {
		this.toolbar.down('#followButton').streamId = value
	},
	setSocialStreamType: function(value) {
		this.toolbar.down('#followButton').streamType = value
	},

	//Search Configuration
	dateFormat: 'Y-m-d',
	timeFormat: 'H:i a',
	submitFormat: 'Y-m-d H:i:s',
	allowPersistFilter: true,
	showComparisons: true,
	useWindowParams: true,
	gridName: '',
	displayFilter: true,
	clobCheckURL : '',
	clobTypeFields : [],
	convertColumnToField : function(dataArr){
        if(dataArr.length == 0)
            return dataArr;
        var resArr = [];
        for(var i = 0; i < dataArr.length; i++){
            var column = dataArr[i];
            var field = column.toLowerCase().replace(/_/g, "");
            resArr.push(field);
        }
        return resArr;
    },
	configureViewMenuItems: function() {
		//Load the views into the view menu
		var viewMenuItem = this.toolbar.down('#viewMenuItem');
		if (viewMenuItem) {
			var viewMenu = viewMenuItem.menu;

			viewMenu.removeAll();
			this.viewStore.each(function(record) {
				viewMenu.add({
					text: record.data.name,
					group: 'view',
					checked: this.grid.serverData.id == record.data.id,
					tableName: this.grid.tableName,
					grid: this.grid,
					record: record.raw,
					checkHandler: this.selectView
				});
			}, this);
		}
	},
	saveView: function() {
		//Save the current view as a new view, so prompt the user for a new name (pre-populated with the current view if one is selected)
		var viewsMenuItem = this.toolbar.down('#viewMenuItem'),
			value = '';
		//figure out the selected view based on the menu
		viewsMenuItem.menu.items.each(function(view) {
			if (view.checked) {
				value = view.text;
				return false;
			}
		});

		Ext.MessageBox.show({
			title: RS.common.locale.saveViewTitle,
			msg: RS.common.locale.saveViewMessage,
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: RS.common.locale.saveView,
				no: RS.common.locale.cancel
			},
			scope: this,
			fn: this.handleSaveView,
			prompt: true,
			value: value || ''
		});
	},
	handleSaveView: function(button, input) {
		if (button == 'yes') {
			//Check if the name matches an existing view.  If it does, warm them that it will replace the existing view and are they sure they want to do that
			var viewsMenuItem = this.toolbar.down('#viewMenuItem'),
				match = false,
				isSystem = false;
			//figure out the selected view based on the menu
			viewsMenuItem.menu.items.each(function(view) {
				if (view.text == input) {
					match = true;
					isSystem = view.type == 'Shared';
					return false;
				}
			});

			if (isSystem) {
				Ext.MessageBox.show({
					title: RS.common.locale.saveSystemViewTitle,
					msg: RS.common.locale.saveSystemViewBody,
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						yes: RS.common.locale.saveView,
						no: RS.common.locale.cancel
					},
					scope: this,
					fn: this.handleSaveView,
					prompt: true
				});
				return;
			}

			if (match) {
				var inputConfig = {
					input: input,
					searchControl: this
				};
				//Double check about the duplicate name
				Ext.MessageBox.show({
					title: RS.common.locale.saveViewTitleConfirm,
					msg: Ext.String.format(RS.common.locale.saveViewMessageConfirm, input),
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						yes: RS.common.locale.saveViewConfirm,
						no: RS.common.locale.cancel
					},
					scope: inputConfig,
					fn: this.reallySaveView
				});
			} else this.persistView(input);
		}
	},
	reallySaveView: function(button) {
		if (button == 'yes') this.searchControl.persistView(this.input);
	},
	persistView: function(viewName) {
		var viewsMenuItem = this.toolbar.down('#viewMenuItem'),
			match = null,
			params = {},
			fields = [];

		this.grid.headerCt.items.each(function(column) {
			if (column.fieldConfig) {
				column.fieldConfig.hidden = column.hidden;
				fields.push(column.fieldConfig);
			}
		});

		//figure out the selected view based on the menu
		viewsMenuItem.menu.items.each(function(view) {
			if (view.checked) {
				match = view;
				return false;
			}
		});

		if (match) {
			var matchRecord = match.record;

			matchRecord.fields = fields;
			if (match.text != viewName || match.type == 'Shared') {

				var viewsMenuItem = this.toolbar.down('#viewMenuItem'),
					matchId = '';
				//figure out the selected view based on the menu
				viewsMenuItem.menu.items.each(function(view) {
					if (view.text == viewName) {
						matchId = view.id
						return false
					}
				});

				matchRecord.id = matchId || '';
				matchRecord.type = 'Self';
				matchRecord.user = '';
				matchRecord.name = viewName;

				//Need to make sure to get these from the parent when creating a new view
				delete matchRecord.metaFormLink;
				delete matchRecord.metaNewLink;
			}

			Ext.Ajax.request({
				url: '/resolve/service/view/save',
				jsonData: matchRecord,
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) {
						this.grid.serverData.id = response.data.id;
						this.viewStore.load();
					} else {
						clientVM.displayError(response.message);
					}
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			});
		}
	},

	clearFilter: function() {
		if(this.toolbar.down('#filter'))
			this.toolbar.down('#filter').clearValue();
	},
	saveFilter: function() {
		var value = this.toolbar.down('#filter').getDisplayValue();
		//Prompt the user for a name to save the filter as (must be unique in the list of filter names that they have though)
		Ext.MessageBox.show({
			title: RS.common.locale.saveFilterTitle,
			msg: RS.common.locale.saveFilterMessage,
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: RS.common.locale.saveFilter,
				no: RS.common.locale.cancel
			},
			scope: this,
			fn: this.handleSaveFilter,
			prompt: true,
			value: value || ''
		});
	},
	handleSaveFilter: function(button, input) {
		if (button == 'yes') {
			//Check if the name matches an existing filter.  If it does, warn them that it will replace the existing filter and are they sure they want to do that
			var rec = this.filterStore.findRecord('name', input, 0, false, false, true),
				inputConfig = {
					input: input,
					searchControl: this
				};
			if (rec) {
				//Double check about the duplicate name
				Ext.MessageBox.show({
					title: RS.common.locale.saveFilterTitleConfirm,
					msg: Ext.String.format(RS.common.locale.saveFilterMessageConfirm, input),
					buttons: Ext.MessageBox.YESNO,
					buttonText: {
						yes: RS.common.locale.saveFilterConfirm,
						no: RS.common.locale.cancel
					},
					scope: inputConfig,
					fn: this.reallySaveFilter
				});
			} else {
				this.persistFilter(input);
			}
		}
	},
	reallySaveFilter: function(button) {
		if (button == 'yes') this.searchControl.persistFilter(this.input);
	},
	persistFilter: function(filterName) {
		var filterString = this.getFiltersString(),
			rec = this.filterStore.findRecord('name', filterName, 0, false, false, true),
			params = {
				value: filterString
			};

		if (rec) {
			Ext.applyIf(params, rec.raw);
		}

		Ext.applyIf(params, {
			name: filterName,
			metaTableName: this.grid.tableName,
			metaTableSysId: this.grid.tableSysId,
			isSelf: true
		});
		Ext.Ajax.request({
			url: '/resolve/service/filter/save',
			params: params,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					this.filterStore.load({
						scope: this,
						callback: function(records) {
							Ext.each(records, function(record) {
								if (record.data.name == params.name) this.toolbar.down('#filter').setValue(record.internalId);
							}, this);
							clientVM.displaySuccess(RS.common.locale.filterSaved,4000,RS.common.locale.success);
						}
					});
				} else{
					clientVM.displayError(response.message, 0,RS.common.locale.error);
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		});
	},
	addFilter: function(value, suppressChangeEvt) {
		var internalValue = this.getInternalValue(value),
			split = internalValue.split(' ');

		//if the user has typed: field condition value, then add it
		if (split.length > 2 && split[2]) {
			this.filterBar.addFilter(value, internalValue);
			if(!suppressChangeEvt){
				this.filterBar.fireEvent('filterChanged');
			}
			return true;
		}

		return false;
	},
	parseSuggestionsForStore: function(store, value) {
		//Split the value on spaces for the search query
		var searches = value.split(' '),
			columnText = '',
			column = null,
			i;
		var me = this;
		//Parse column text
		for (i = 0; i < searches.length && this.couldBeColumn(Ext.String.trim(columnText + ' ' + searches[i])); i++) {
			columnText += ' ' + searches[i];
			columnText = Ext.String.trim(columnText);
		}

		Ext.each(this.grid.columns, function(gridColumn) {
			if (gridColumn.text.toLowerCase() === columnText.toLowerCase()) {
				column = gridColumn;
				return false;
			}
		});

		store.removeAll();

		//Add in the first non-date column as a search suggestion if the current selection isn't good
		if (!column || !column.filterable) {
			var col = null;
			Ext.each(this.grid.columns, function(c) {
				if (c.dataType != 'date' && c.uiType != 'DateTime' && c.filterable) {
					col = c
					return false
				}
			})
			if (col) {
				store.add({
					name: Ext.String.format('{0} {1} <b>{2}</b>', col.text, RS.common.locale.contains, value),
					value: RS.common.locale.contains,
					displayName: Ext.String.format('{0} {1} {2}', col.text, RS.common.locale.contains, value)
				})
			}
		}

		//If we have a column, then we're searching on a column and need the operators
		if (column && column.filterable) {
			var conditionText = '';
			var	operator = null;
			var isClobField = false;
			Ext.each(me.clobTypeFields, function(clobField){
				if(column.dataIndex == clobField){
					isClobField = true;
					return;
				}
			})
			for (; i < searches.length && this.couldBeCondition(Ext.String.trim(conditionText + ' ' + searches[i])); i++) {
				conditionText += ' ' + searches[i];
				conditionText = Ext.String.trim(conditionText);
				if(conditionText == RS.common.locale.equals && isClobField){
					conditionText = RS.common.locale.contains;
				}
			}

			//Get operator equivalent
			Ext.Object.each(this.conditions, function(con) {
				if (this.conditions[con] == conditionText) {
					if (con.indexOf('short') == 0) {
						operator = this.shortConditions[conditionText]
					} else
						operator = this.conditions[con];
					return false;
				}
			}, this);

			if (operator) {
				//Then we have an operator and need to display the data suggestions
				var value = searches.splice(i).join(' '),
					options = this.getValueOptionsForDataType(column, columnText, conditionText, value),
					dataValues = [];

				//Add in the user's typed in search criteria as the first option
				store.add({
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, conditionText, value),
					value: conditionText,
					displayName: Ext.String.format('{0} {1} {2}', columnText, conditionText, value)
				});

				store.add(options);

				//Get the data options for the column
				this.grid.store.each(function(record) {
					var data = record.get(column.dataIndex);
					if (data) {
						if (Ext.isDate(data)) data = Ext.Date.format(data, this.dateFormat);
						if (Ext.isBoolean(data)) data = data.toString();
						if (Ext.isNumber(data)) data = data.toString();
						if (dataValues.indexOf(data.toLowerCase()) === -1 && data.toLowerCase().indexOf(value.toLowerCase()) > -1) {
							dataValues.push(data.toLowerCase());
							store.add({
								name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, conditionText, data),
								value: conditionText,
								displayName: Ext.String.format('{0} {1} {2}', columnText, conditionText, data)
							});
						}
					}
					if (store.getCount() >= 6) return false;
				}, this);

				//If we have the operator, but don't have any data suggestions for what they are typing, then we need to just return the value that they are typing to help them out
				if (store.getCount() === 0) {
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, conditionText, value),
						value: conditionText,
						displayName: Ext.String.format('{0} {1} {2}', columnText, conditionText, value)
					})
				}
			} else {
				//Then we have a part of an operator and need to display the operators that match the search
				var options = this.getOptionsForDataType(column,isClobField);

				//First add in the contains of the user provided value as an option for the user if it doesn't match a condition option
				var val = searches.slice(1).join(' ');
				if (val) {
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, RS.common.locale.contains, val),
						value: RS.common.locale.contains,
						displayName: Ext.String.format('{0} {1} {2}', columnText, RS.common.locale.contains, val)
					})
				}

				Ext.each(options, function(option) {
					if (option.value.indexOf(conditionText) > -1) store.add(option);
				});
			}
		}

		if (columnText && store.getCount() < 7) {
			//We don't have a column, but have a part of a column so show those that match the current search
			Ext.each(this.grid.columns, function(column) {
				if (column.filterable && column.text.toLowerCase().indexOf(columnText.toLowerCase()) === 0) {
					store.add({
						name: Ext.String.format('<b>{0}</b>{1}', column.text.substring(0, columnText.length), column.text.substring(columnText.length)),
						value: column.text,
						type: column.dataType,
						displayName: column.text + ' '
					});
					if (store.getCount() >= 6) return false;
				}
			});
		}

		if (store.getCount() < 7) {
			//No column matches, so we need to search the data
			var query = value.toLowerCase(),
				columnData = [],
				recordData = [],
				records = [];
			this.grid.store.each(function(record) {
				Ext.Object.each(record.data, function(data) {
					var tempValue = record.data[data];
					if (Ext.isBoolean(tempValue)) tempValue = tempValue.toString()
					if (Ext.isDate(tempValue)) tempValue = Ext.Date.format(tempValue, this.dateFormat)
					if (Ext.isNumber(tempValue)) tempValue = tempValue.toString()
					if (tempValue && Ext.isString(tempValue) && tempValue.toLowerCase().indexOf(query.toLowerCase()) > -1 && recordData.indexOf(tempValue.toLowerCase()) == -1) {
						if (columnData.indexOf(data) == -1) columnData.push(data);
						recordData.push(tempValue.toLowerCase());
						records.push({
							column: data,
							data: tempValue
						});
					}
					if (columnData.length >= 6 || recordData.length >= 6) return false;
				}, this);
			}, this);

			//Now we have a list of columns.  Display the columns with the appropriate comparisons for the data
			Ext.each(columnData, function(column) {
				if (store.getCount() >= 6) return false;
				Ext.each(this.grid.columns, function(gridColumn) {
					if (store.getCount() >= 6) return false;
					if (gridColumn.dataIndex == column && column.filterable) {
						var conditionText = RS.common.locale.contains;
						if (gridColumn.dataType == 'date') conditionText = RS.common.locale.on;
						store.add({
							name: Ext.String.format('{0} {1} <b>{2}</b>', gridColumn.text, conditionText, query),
							value: conditionText,
							displayName: Ext.String.format('{0} {1} {2}', gridColumn.text, conditionText, query)
						});
					}
				});
			}, this);

			//Now we have a list of the records and their column dataIndexes so we need to turn those into proper queries based on their types
			Ext.each(records, function(record) {
				if (store.getCount() >= 6) return false;
				Ext.each(this.grid.columns, function(column) {
					if (column.dataIndex == record.column && column.filterable) {
						record.columnText = column.text;
						record.columnDataType = column.dataType;
						if (column.dataType == 'date') record.conditionText = RS.common.locale.on;
						else {
							var isClobField = false;
							Ext.each(me.clobTypeFields, function(clobField){
								if(column.dataIndex == clobField){
									isClobField = true;
									return;
								}
							})
							record.conditionText = isClobField ? RS.common.locale.contains : RS.common.locale.equals;
						}
						return false;
					}
				});
				if (record.columnText) {
					//Now that we have a record with all the information we could need, we need to add it to the store
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', record.columnText, record.conditionText, record.data),
						value: record.data,
						displayName: Ext.String.format('{0} {1} {2}', record.columnText, record.conditionText, record.data)
					});
				}
			}, this);
		}

		if (store.getCount() === 0) {
			//No records match the search criteria as of yet (could be another page or something), so load up the columns with their types based on the type of query
			var queryType = 'string',
				conditionText = RS.common.locale.contains;
			if (Ext.isNumber(Number(value))) {
				queryType = 'float';
				conditionText = RS.common.locale.equals
			}
			if (Ext.isDate(Ext.Date.parse(value, this.dateFormat))) {
				queryType = 'date';
				conditionText = RS.common.locale.on;
			}

			Ext.each(this.grid.columns, function(column) {
				if (column.filterable && column.dataType == queryType) {
					store.add({
						name: Ext.String.format('{0} {1} <b>{2}</b>', column.text, conditionText, value),
						value: value,
						displayName: Ext.String.format('{0} {1} {2}', column.text, conditionText, value)
					});
				}
				if (store.getCount() >= 6) return false;
			});
		}
	},
	getInternalValue: function(value) {
		var returnText = '',
			split = value.split(' ');

		var columnText = '',
			i;
		//Get column dataIndex
		for (i = 0; i < split.length && this.couldBeColumn(Ext.String.trim(columnText + ' ' + split[i])); i++) {
			columnText += ' ' + split[i];
			columnText = Ext.String.trim(columnText);
		}
		Ext.each(this.grid.columns, function(column) {
			if (column.text.toLowerCase() == columnText.toLowerCase()) returnText += column.dataIndex + ' ';
		});

		var conditionText = '';
		for (; i < split.length && this.couldBeCondition(Ext.String.trim(conditionText + ' ' + split[i])); i++) {
			conditionText += ' ' + split[i];
			conditionText = Ext.String.trim(conditionText);
		}

		//Get operator equivalent
		Ext.Object.each(this.conditions, function(con) {
			var c = con;
			if (this.conditions[con] == conditionText) {
				if (con.indexOf('short') == 0) {
					c = this.shortConditions[conditionText]
				}
				returnText += c;
				return false;
			}
		}, this);
		returnText += ' ';

		var val = split.slice(i).join(' '),
			matchDataValue = false;
		//Get value equivalent
		Ext.Object.each(this.dataValues, function(value) {
			if (this.dataValues[value] == val) {
				matchDataValue = true;
				returnText += value;
				return false;
			}
		}, this);
		if (!matchDataValue) returnText += val;

		return returnText;
	},
	getExternalValue: function(filter) {
		var split = filter.split(' '),
			columnText = split[0],
			condition = split[1];
		split.splice(0, 2);
		var value = split.join(' ');
		//Convert column
		Ext.each(this.grid.columns, function(column) {
			if (column.dataIndex == columnText) {
				columnText = column.text;
				return false;
			}
		});
		//Convert condition
		condition = RS.common.locale[condition];

		return Ext.String.format('{0} {1} {2}', columnText, condition, value);
	},
	couldBeColumn: function(columnText) {
		var exists = false;
		Ext.each(this.grid.columns, function(column) {
			if (column.filterable && column.text.toLowerCase().indexOf(columnText.toLowerCase()) == 0) {
				exists = true;
				return false;
			}
		});
		return exists;
	},
	couldBeCondition: function(condition) {
		condition = condition.toLowerCase();
		var returnVal = false;
		Ext.Object.each(this.conditions, function(con) {
			if (this.conditions[con].indexOf(condition) == 0) {
				returnVal = true;
				return false;
			}
		}, this);
		return returnVal;
	},
	filterChanged: function(a,b,c,d) {
		//Reset to page 1 whenever the filter changes because the current page might not be around anymore and the server is naive to know
		this.grid.store.loadPage(1);
	},
	getWhereFilter: function() {
		var filters = this.filterBar.getFilters(),
			whereFilter = [];

		Ext.each(filters, function(filter, index) {
			var split = filter.split(/\s+/),
				column = split[0],
				condition = split[1],
				value = split.slice(2).join(' '),
				col = {},
				field, val;

			//get the column based on the column dataIndex
			Ext.each(this.grid.columns, function(gridColumn) {
				if (gridColumn.dataIndex == column) {
					col = gridColumn;
					return false;
				}
			});

			if (col) {
				if (col.dataType == 'date') {
					val = Ext.Date.parse(value, this.dateFormat);
					if (!Ext.isDate(val)) val = Ext.Date.parse(value, 'c')
					if (Ext.isDate(val)) {
						value = Ext.Date.format(val, 'c');
					}
				}

				if (col.dataType == 'bool') {
					value = value === 'true' || value === true || value === 1 || value === '1'
				}

				field = col.dataIndex;

				whereFilter.push({
					field: field,
					type: col.dataType,
					condition: condition,
					value: value
				});
			}
		}, this);

		return whereFilter;
	},
	getWhereClause: function() {
		var filters = this.filterBar.getFilters(),
			whereClause = '';
		//Parse through filters to get the where clause
		Ext.each(filters, function(filter, index) {
			var split = filter.split(' '),
				column = split[0],
				condition = split[1],
				value = split.splice(2).join(' '),
				where = '(',
				addValue = true;

			where += column + ' ';
			var values = value.split('|');
			//get the column based on the column dataIndex
			var col = {};
			Ext.each(this.grid.columns, function(gridColumn) {
				if (gridColumn.dataIndex == column) {
					col = gridColumn;
					return false;
				}
			});
			Ext.each(values, function(value, index) {
				if (value) {
					if (index > 0) where += 'OR ' + column + ' ';
					if (col.dataType == 'bool') {
						value = value === 'true' ? '1' : '0';
					}
					//Get condition equivalent
					switch (condition.toLowerCase()) {
						case 'on':
							where += "BETWEEN (date '{0}') AND (date '{1}')";
							break;
						case 'after':
							where += "> (date '{0}')";
							break;
						case 'before':
							where += "< (date '{0}')";
							break;
						case 'equals':
							var eqCond = Ext.isNumber(Number(value)) ? '=' : "= '{0}'";
							if (value === 'NULL') {
								eqCond = 'IS NULL';
								addValue = false;
							}
							if (value === 'NIL') {
								eqCond = Ext.String.format("IS NULL OR {0} = ''", column);
								addValue = false;
							}
							where += eqCond;
							break;
						case 'notequals':
							var eqCond = Ext.isNumber(Number(value)) ? '<>' : "<> '{0}'";
							if (value === 'NULL') {
								eqCond = 'IS NOT NULL';
								addValue = false;
							}
							if (value === 'NIL') {
								eqCond = Ext.String.format("IS NOT NULL OR {0} <> ''", column);
								addValue = false;
							}
							where += eqCond;
							break;
						case 'contains':
							where += "LIKE '%{0}%'";
							break;
						case 'notcontains':
							where += "NOT LIKE '%{0}%'";
							break;
						case 'startswith':
							where += "LIKE '{0}%'";
							break;
						case 'notstartswith':
							where += "NOT LIKE '{0}%'";
							break;
						case 'endswith':
							where += "LIKE '%{0}'";
							break;
						case 'notendswith':
							where += "NOT LIKE '%{0}'";
							break;
						case 'greaterthan':
							where += '> {0}';
							break;
						case 'greaterthanorequalto':
							where += '>= {0}';
							break;
						case 'lessthan':
							where += '< {0}';
							break;
						case 'lessthanorequalto':
							where += '<= {0}';
							break;
						default:
							where += '=';
					}

					where += ' ';

					switch (value) {
						case 'today':
							where = Ext.String.format(where, Ext.Date.format(new Date(), this.dateFormat), Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, 1), this.dateFormat));
							break;
						case 'yesterday':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.DAY, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						case 'lastweek':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.WEEK, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						case 'lastmonth':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.MONTH, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						case 'lastyear':
							where = Ext.String.format(where, Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, -1), this.dateFormat), Ext.Date.format(Ext.Date.format(new Date(), this.dateFormat)));
							break;
						default:
							if (addValue) {
								var val = value,
									secondVal = '';
								if (col.dataType == 'date') {
									val = Ext.Date.parse(value, this.dateFormat)
									if (!Ext.isDate(val)) val = Ext.Date.parse(value, 'c');
									if (Ext.isDate(val)) {
										value = Ext.Date.format(val, this.submitFormat);
										secondVal = Ext.Date.format(Ext.Date.add(val, Ext.Date.DAY, 1), this.submitFormat);
									}
								}

								if (where.indexOf('{0}') > -1) where = Ext.String.format(where, value.replace(/'/g, "''"), secondVal.replace(/'/g, "''"));
								else where += value.replace(/'/g, "''");
							}
					}
				}
			}, this);

			whereClause += (index > 0 ? ' AND ' : ' ') + where + ')';
		}, this);
		return whereClause;
	},
	getOptionsForDataType: function(column, isClobField) {
		var me = this,
			options = [];
		switch (column.dataType) {
			case 'date':
				options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.on),
					value: RS.common.locale.on,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.on)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.before),
					value: RS.common.locale.before,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.before)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.after),
					value: RS.common.locale.after,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.after)
				}];
				break;
			case 'float':
			case 'int':
				options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
					value: RS.common.locale.equals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEquals),
					value: RS.common.locale.notEquals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEquals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.greaterThan),
					value: RS.common.locale.greaterThan,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.greaterThan)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.greaterThanOrEqualTo),
					value: RS.common.locale.greaterThanOrEqualTo,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.greaterThanOrEqualTo)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.lessThan),
					value: RS.common.locale.lessThan,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.lessThan)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.lessThanOrEqualTo),
					value: RS.common.locale.lessThanOrEqualTo,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.lessThanOrEqualTo)
				}];
				break;
			case 'bool':
				options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
					value: RS.common.locale.equals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEquals),
					value: RS.common.locale.notEquals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEquals)
				}];
				break;
			default:
				options = [];
				if(!isClobField){
					options = [{
						name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
						value: RS.common.locale.equals,
						displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
					}, {
						name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEquals),
						value: RS.common.locale.notEquals,
						displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEquals)
					}]
				}
				options.push({
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.contains),
					value: RS.common.locale.contains,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.contains)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notContains),
					value: RS.common.locale.notContains,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notContains)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.startsWith),
					value: RS.common.locale.startsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.startsWith)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notStartsWith),
					value: RS.common.locale.notStartsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notStartsWith)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.endsWith),
					value: RS.common.locale.endsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.endsWith)
				}, {
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.notEndsWith),
					value: RS.common.locale.notEndsWith,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.notEndsWith)
				});
				if (!me.showComparisons && !isClobField) options = [{
					name: Ext.String.format('{0} <b>{1}</b>', column.text, RS.common.locale.equals),
					value: RS.common.locale.equals,
					displayName: Ext.String.format('{0} {1} ', column.text, RS.common.locale.equals)
				}]
				break;
		}
		return options;
	},
	getShortOptionsForDataType: function(type, isClobField) {
		var options = [];
		switch (type) {
			case 'date':
				options = [{
					name: Ext.String.format('{0}', RS.common.locale.on),
					value: Ext.String.format('{0}', RS.common.locale.on)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.before),
					value: Ext.String.format('{0}', RS.common.locale.before)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.after),
					value: Ext.String.format('{0}', RS.common.locale.after)
				}];
				break;
			case 'float':
			case 'int':
				options = [{
					name: Ext.String.format('{0}', RS.common.locale.shortEquals),
					value: Ext.String.format('{0}', RS.common.locale.shortEquals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortNotEquals),
					value: Ext.String.format('{0}', RS.common.locale.shortNotEquals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortGreaterThan),
					value: Ext.String.format('{0}', RS.common.locale.shortGreaterThan)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortGreaterThanOrEqualTo),
					value: Ext.String.format('{0}', RS.common.locale.shortGreaterThanOrEqualTo)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortLessThan),
					value: Ext.String.format('{0}', RS.common.locale.shortLessThan)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.shortLessThanOrEqualTo),
					value: Ext.String.format('{0}', RS.common.locale.shortLessThanOrEqualTo)
				}];
				break;
			case 'bool':
				options = [{
					name: Ext.String.format('{0}', RS.common.locale.equals),
					value: Ext.String.format('{0}', RS.common.locale.equals)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notEquals),
					value: Ext.String.format('{0}', RS.common.locale.notEquals)
				}];
				break;
			default:
				options = [];
				if(!isClobField){
					options.push({
						name: Ext.String.format('{0}', RS.common.locale.equals),
						value: Ext.String.format('{0}', RS.common.locale.equals)
					},{
						name: Ext.String.format('{0}', RS.common.locale.notEquals),
						value: Ext.String.format('{0}', RS.common.locale.notEquals)
					});
				}
				options.push({
					name: Ext.String.format('{0}', RS.common.locale.contains),
					value: Ext.String.format('{0}', RS.common.locale.contains)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notContains),
					value: Ext.String.format('{0}', RS.common.locale.notContains)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.startsWith),
					value: Ext.String.format('{0}', RS.common.locale.startsWith)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notStartsWith),
					value: Ext.String.format('{0}', RS.common.locale.notStartsWith)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.endsWith),
					value: Ext.String.format('{0}', RS.common.locale.endsWith)
				}, {
					name: Ext.String.format('{0}', RS.common.locale.notEndsWith),
					value: Ext.String.format('{0}', RS.common.locale.notEndsWith)
				});
		}
		return options;
	},
	getValueOptionsForDataType: function(column, columnText, operatorText, dataValue) {
		var options = [],
			returnOptions = [];
		switch (column.dataType) {
			case 'date':
				options = [{
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.today),
					value: RS.common.locale.today,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.today)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.yesterday),
					value: RS.common.locale.yesterday,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.yesterday)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.lastWeek),
					value: RS.common.locale.lastWeek,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.lastWeek)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.lastMonth),
					value: RS.common.locale.lastMonth,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.lastMonth)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, RS.common.locale.lastYear),
					value: RS.common.locale.lastYear,
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, RS.common.locale.lastYear)
				}, {
					name: Ext.String.format('{0} {1} <b>{2}</b>', columnText, operatorText, Ext.Date.format(new Date(), this.dateFormat)),
					value: Ext.Date.format(new Date(), this.dateFormat),
					displayName: Ext.String.format('{0} {1} {2}', columnText, operatorText, Ext.Date.format(new Date(), this.dateFormat))
				}];
				break;
		}
		Ext.each(options, function(option) {
			if (option.value.indexOf(dataValue) > -1) returnOptions.push(option);
		});
		return returnOptions;
	},
	getFiltersString: function() {
		var filters = this.filterBar.getFilters(),
			filterString = '';
		Ext.each(filters, function(filter, index) {
			if (index > 0) filterString += '|&&|';
			filterString += filter.replace(/ /g, '^')
		});
		return filterString;
	},
	setFilter: function(filter) {
		if (!filter) return;
		var filterStrings = filter.split('|&&|'),
			filters = [];
		Ext.each(filterStrings, function(filterString) {
			var parsed = filterString.replace(/\^/g, ' ');
			filters.push({
				value: this.getExternalValue(parsed),
				internalValue: parsed
			});
		}, this);
		this.filterBar.setFilters(filters);
	},

	selectView: function(item, checked) {
		if (checked) {
			Ext.Ajax.request({
				url: '/resolve/service/view/read',
				params: {
					table: item.tableName,
					view: item.text
				},
				scope: item,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (response.success) {
						if (response.data && response.data.fields) {
							var modelFields = [],
								columns = [];
							configureColumns(response.data.fields, modelFields, columns, this.grid.serverData.autoSizeColumns);
	
							Ext.define('RS.customtable.Table', {
								extend: 'Ext.data.Model',
								idProperty: 'sys_id',
								fields: modelFields
							});
							//reconfigure the grid to use the new columns
							this.grid.reconfigure(this.grid.store, columns); //reconfigure will fire the load automatically
							this.grid.fireEvent('viewSelected', this.grid, response.data);
						}
					} else {
						clientVM.displayError(response.message);
					}
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				}
			});
		}
	},
	parseWindowParameters: function() {
		if (!this.useWindowParams) return false

		//Parse through url parameters matching the dataIndex of the columns
		var me = this,
			windowLocation = window.location.href,
			winParams = {},
			addedFilter = false,
			windowParams = windowLocation.split('?')[1],
			hashParams = windowLocation.split('#')[1],
			windowParamsSplit = [],
			suppressChangeEvt = true;

		if (windowParams) {
			windowParamsSplit = windowParams.split('#'); //remove hastag navigation
			windowParams = windowParamsSplit[0]
			winParams = Ext.Object.fromQueryString(windowParams)
		}

		if (hashParams && clientVM && clientVM.routes) {
			Ext.Object.each(clientVM.routes, function(route) {
				var params = RS.util.routeMatcher(route).parse(hashParams)
				if (params && params.params) {
					var p = Ext.Object.fromQueryString(params.params)
					Ext.apply(winParams, p)
					return false
				}
			})
		}

		var clearBeforeAdd = true;
		//handle 'All XXX' menu item
		var noFilter = winParams['noFilter'];		
		if (noFilter == true || noFilter == "true") {
			me.clearFilter();
			me.filterBar.clearFilter(me,false,suppressChangeEvt);
		}
		else{
			Ext.Object.each(winParams, function(param) {
				Ext.each(me.grid.columns, function(column) {
					if (column.dataIndex == param && column.filterable) {
						var tmp = winParams[param];

						if (Ext.isString(tmp)) tmp = [tmp];

						Ext.each(tmp, function(tmpString) {
							var split = tmpString.split('~'),
								value = split[split.length - 1],
								comp = RS.common.locale.equals;
							if (split.length > 1) {
								switch (split[0]) {
									case 'on':
										comp = RS.common.locale.on;
										break;
									case 'after':
										comp = RS.common.locale.after;
										break;
									case 'before':
										comp = RS.common.locale.before;
										break;
									case 'equals':
									case 'eq':
										comp = RS.common.locale.equals;
										break;
									case 'notEquals':
									case 'neq':
										comp = RS.common.locale.notEquals;
										break;
									case 'contains':
										comp = RS.common.locale.contains;
										break;
									case 'notContains':
										comp = RS.common.locale.notContains;
										break;
									case 'startsWith':
										comp = RS.common.locale.startsWith;
										break;
									case 'notStartsWith':
										comp = RS.common.locale.notStartsWith;
										break;
									case 'endsWith':
										comp = RS.common.locale.endsWith;
										break;
									case 'notEndsWith':
										comp = RS.common.locale.notEndsWith;
										break;
									case 'greaterThan':
									case 'gt':
										comp = RS.common.locale.greaterThan;
										break;
									case 'greaterThanOrEqualTo':
									case 'gteq':
										comp = RS.common.locale.greaterThanOrEqualTo;
										break;
									case 'lessThan':
									case 'lt':
										comp = RS.common.locale.lessThan;
										break;
									case 'lessThanOrEqualTo':
									case 'lteq':
										comp = RS.common.locale.lessThanOrEqualTo;
										break;
									default:
										comp = RS.common.locale.equals;
										break;
								}
							}

							if (clearBeforeAdd) {
								me.filterBar.clearFilter(me,false,suppressChangeEvt);
								clearBeforeAdd = false
							}

							me.addFilter(column.text + ' ' + comp + ' ' + value.replace(/\${USERID}/g, user.name || me.grid.serverData.currentUsername), suppressChangeEvt);
							addedFilter = true;
						});
						return false;
					}
				});

				if (param == 'filter') {
					var filterParam = winParams[param];
					//Set the filter by name
					me.filterStore.on('load', function() {
						var rec = this.filterStore.findRecord('name', winParams[param], 0, false, false, true);
						if (rec) {
							this.toolbar.down('#filter').setValue(rec.data.sysId);
						}
					}, me, {
						single: true
					});
				}
			});
		}
		return addedFilter
	},

	setDisplayName: function(name) {
		this.toolbar.down('#tableMenu').setText(name)
		if (this.allowPersistFilter) this.filterStore.load()
		if (this.grid.tableName && !this.hideMenu) this.viewStore.load()
	},

	setDisplayFilter: function(value) {
		this.displayFilter = value
		this.toolbar.down('#searchText').setVisible(value)
		this.filterBar.setVisible(value)
	}
});
glu.defModel('RS.incident.SecurityIncidentForm',  {
	//pos: [],
	resolveSelected: true,
	title: '',
	investigationType: '',
	playbook: '',
	severity: '',
	dueBy: '',
	owner: '',
	sourceSystem: 'Resolve',
	externalReferenceId: 'allow',
	playbookNameIsValidated: false,
	severityStore: {
		mtype: 'store',
		fields: ['severity'],
		data: [{severity: 'Critical'}, {severity: 'High'}, {severity: 'Medium'}, {severity: 'Low'}]
	},

	originStore: {
		mtype: 'store',
		fields: ['origin'],
		data: [{origin: 'Resolve'}, {origin: 'Splunk'}]
	},

	add: function() {
		this.fireEvent('sirAdded');
	},

	mSecondsInADay: 86399000, // 24*60*60*1000-1000  number of milli seconds in a day minus 1

	addNewSir: function(data) {
		data.dueBy = (new Date(data.dueBy)).getTime() + this.mSecondsInADay;

		//Orgs Info
		Ext.apply(data, {
			'sysOrg' : clientVM.orgId  || null
		})
		this.ajax({
			url: '/resolve/service/playbook/securityIncident/save',
			jsonData: data,
			method: 'POST',
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					this.parentVM.store.reload();
				} else {
					clientVM.displayError(response.message)
				}
			},
			failure: function (resp) {
				clientVM.displayFailure(resp);
			}
		});
		this.doClose();
	},

	cancel: function() {
		this.doClose();
	},

	adjustWindowSize: function(v) {
		var me = this,
			parentSize = Ext.getBody().getViewSize();
		/*v.setWidth(parentSize.width *.4);
		v.setHeight(parentSize.height *.27);*/
		this.on('sirAdded', function() {
			me.addNewSir(v.getValues());
		});
		//v.up().setPagePosition(this.pos);
	},

	playbookIsValid$: function() {
		return !!this.playbook && !!this.playbook.length && this.playbookNameIsValidated;
	},

	addIsEnabled$: function() {
		var isValid = function(val) {
			return (val && val.length > 0);
		}
		return (isValid(this.title) && isValid(this.investigationType) && isValid(this.severity)
					&& isValid(this.owner) && isValid(this.sourceSystem)&& isValid(this.externalReferenceId)
					&& isValid(this.playbook) && this.playbookNameIsValidated);
	},

	originSelected: function(record) {
		this.set('resolveSelected', record[0].get('origin') == 'Resolve')
	},

	handlePlaybookSelect: function() {
		this.set('playbookNameIsValidated', true);
	},

	validateTemplateName: function(v) {
		var r = v.store.findRecord('ufullname', v.getValue(), 0, false, false, true);
		this.set('playbookNameIsValidated', r? true: false);
	},

	init: function() {
		this.parentVM.refreshTypes();
		this.parentVM.playbookTemplateStore.on('load', function(store) {
			var r = store.findRecord('ufullname', this.playbook, 0, false, false, true);
			this.set('playbookNameIsValidated', r? true: false);
		}, this);
	}

});
glu.defModel('RS.incident.WikiMainModelFactory', function(cfg) {
	var main = RS.wiki.viewmodels.Main,
		defaultData = Ext.clone(main.defaultData),
		fields = Ext.clone(main.fields);

	Ext.applyIf(cfg, RS.wiki.viewmodels.Main);

	return Ext.apply(cfg, {
		fields: fields.concat([
			{name: 'udisplayMode', type: 'string'},
			{name: 'uisTemplate', type: 'bool'},
			{name: 'uisDeletable', type: 'bool'}
		]),
		defaultData: Ext.apply(defaultData, {
			udisplayMode: 'playbook',
			uisTemplate: true,
			uisDeletable: false
		}),
		wikiMainInit: main.init,
		wikiMainActivate: main.activate,
		wikiMaingetPageContent: main.getPageContent,
		wikiMainReloadContent: main.reloadContent
	});
});

glu.defView('RS.incident.ArtifactAction',{
	title : 'Actions',
	id: 'artifact-action',
	padding : 15,
	width : 600,
	height : 400,
	modal : true,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	cls : 'rs-modal-popup',
	defaults : {
		margin : '4 0'
	},
	items : [{
		xtype : 'component',
		html : '@{valuePairInfo}'
	},{
		xtype : 'grid',
		flex : 1,
		margin : '4 0 10 0',
		cls : 'rs-grid-dark',
		autoScroll : true,		
		store : '@{actionStore}',
		viewConfig: {
	        getRowClass: function (record, index) {
	            if (record.get('isDisabled')) 
	            	return 'disabled-row';
	        }        
	    },
		columns : [{
			text : '~~name~~',
			flex : 3,
			dataIndex : 'uname'
		},{
			xtype: 'actioncolumn',
			text : 'Action',		
			hideable: false,
			sortable: false,
			align: 'center',			
			width: 60,	
			items: [{
				glyph : 0xF04B,	
				tooltip : 'Execute',		
				handler: function(view, rowIndex, colIndex, item, e, record) {
					this.up('grid').fireEvent('handleActionClick', this, record, view);
				},
				isDisabled : function(view, rowIndex, colIndex, item, record) {
					return record.get('isDisabled');
				}	
			}]
		}],
		listeners: {	
			handleActionClick : '@{actionHandler}'
		}
	}],
	buttons : [{
		name : 'close',
		cls : 'rs-med-btn rs-btn-dark'
	}]
})
glu.defView('RS.incident.ArtifactConfigEntry',{
	header:{
        titlePosition: 0,     
        items:[{        	
            xtype:'button',
            text: 'Delete',
            cls : 'rs-small-btn rs-btn-dark',
            handler: function(btn){            	
            	var vm = btn.up().up()._vm;
            	vm.removeType();
            },
            listeners : {
            	afterrender : function(btn){
            		var vm = btn.up().up()._vm;
            		if(!vm.isCustomType)
            			btn.hide();
            	}
            },
        }],
        listeners : {
        	click : function(){
        		var parentCnt = this.up();
        		parentCnt.collapsed == false ? parentCnt.collapse() : parentCnt.expand();
        	}
        }
    },
	title : '@{artifactType}',
	htmlEncodeTitle : true,
	collapsible : true,
	collapsed : true,
	animCollapse : false,
	bodyPadding : 15,
	margin : '0 0 5 0',
	cls : 'block-model',		
	layout : {
		type : 'hbox',
		align: 'stretch'
	},
	defaults : {
		height : 400,
		autoScroll : true
	},
	items : [{
		//Action
		xtype : 'grid',
		cls : 'rs-grid-dark',
		name : 'action',		
		flex : 3,
		margin : '0 20 0 0',
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-btn-light rs-small-btn'
			},
			items :[{
				xtype: 'tbtext',
				cls: 'artifact-tbtext',
				text: '~~action~~',				
			},'|','addAction','removeAction']
		}],
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTooltip: RS.common.locale.editColumnTooltip,
			glyph : 0xF044,	
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'uname'			
		},		
		store : '@{actionStore}',
		columns : [{
			text : '~~name~~',
			flex : 3,
			dataIndex : 'uname'
		},{
			text : '~~automation~~',
			flex : 2,
			dataIndex : 'executionItem'
		},{
			text : '~~executionType~~',
			flex : 1,
			dataIndex : 'executionType'
		},{
			text : '~~parameter~~',
			width : 200,
			dataIndex : 'param'
		}],
		listeners: {
			editAction: '@{editAction}',			
		}
	},{
		//Alias Keys		
		xtype : 'grid',
		cls : 'rs-grid-dark',
		store : '@{aliasStore}',
		name : 'aliasKeys',
		selModel : {
	        mode : 'single'
	    },
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items :[{
				xtype: 'tbtext',
				cls: 'artifact-tbtext',
				text: '~~aliasKeys~~',					
			},'|','addAlias','removeAlias']
		}],	
		flex : 2,
		columns : [{
			text : '~~ushortName~~',
			dataIndex : 'ushortName',
			flex : 2
		},{
			text : '~~uartifactTypeSource~~',
			dataIndex : 'uartifactTypeSource',
			width : 100
		},{
			text :'~~ufullName~~',
			dataIndex : 'ufullName',
			flex : 3
		}]
	}]	
})
glu.defView('RS.incident.ArtifactConfiguration', {
	bodyPadding : 15,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},	
	autoScroll : true,
	dockedItems : [{
		xtype: 'tbtext',
		cls: 'rs-display-name',
		text: '~~articfactConfigTitle~~',
		padding : '15 0 5 15'		
	},{
		xtype : 'toolbar',
		cls: 'actionBar actionBar-form',
		padding : '5 0 5 15',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items : ['newType']
	}],
	items : '@{configList}'
})
glu.defView('RS.incident.NewTypeForm',{
	title : '~~createNewType~~',
	padding : 15,
	width : 450,
	height : 200,
	modal : true,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	cls : 'rs-modal-popup',
	items : [{
		xtype : 'textfield',
		name : 'name'		
	}],
	buttons : [{
		name : 'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})
glu.defView('RS.incident.NewActionForm',{
	title : '@{title}',
	cls : 'rs-modal-popup',
	modal : true,
	width : 650,	
	padding : 15,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : '4 0',
		labelWidth : 130
	},
	items : [{
		xtype : 'textfield',
		name : 'uname',
		readOnly : '@{edit}'
	},{
		xtype : 'fieldcontainer',
		hideLabel : true,
		layout : 'hbox',
		margin : '0 0 0 135',
		defaultType : 'radio',	
		items : [{
			boxLabel : '~~automation~~',
			value : '@{!actionTaskIsSelected}',
			margin : '0 10 0 0'
		},{
			boxLabel : '~~actiontask~~',
			value : '@{actionTaskIsSelected}'			
		}]
	},{
		xtype: 'triggerfield',
		name: 'automationName',
		fieldLabel : '~~automation~~',
		hidden : '@{actionTaskIsSelected}',
		editable : false,
		triggerCls: 'x-form-search-trigger',
		onTriggerClick: function() {
			this.fireEvent('openAutomationSearch');
		},
		listeners: {
			openAutomationSearch: '@{openAutomationSearch}'
		}
	},{
		xtype: 'triggerfield',
		hidden : '@{!actionTaskIsSelected}',
		name: 'taskName',
		fieldLabel : '~~actiontask~~',
		editable : false,
		triggerCls: 'x-form-search-trigger',
		onTriggerClick: function() {
			this.fireEvent('openActiontaskSearch');
		},
		listeners: {
			openActiontaskSearch: '@{openActiontaskSearch}'
		}
	},{
		xtype : 'combo',
		displayField : 'name',
		valueField : 'name',
		name : 'param',		
		fieldLabel : '~~parameter~~',
		store : '@{parameterStore}',
		editable : false,
		margin : '4 0 20 0',
		queryMode : 'local'
	}],
	buttons : [{
		name : 'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})
glu.defView('RS.incident.NewAliasForm',{
	title : '~~newAliasForm~~',
	cls : 'rs-modal-popup',
	modal : true,
	width : 600,	
	padding : 15,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : '4 0'
	},
	items : [{
		xtype : 'displayfield',
		name : 'type',
		cls : 'rs-displayfield-value'		
	},{
		xtype : 'combo',
		name : 'uartifactTypeSource',
		store : '@{sourceStore}',
		valueField : 'value',
		displayField : 'display',
		editable : false
	},{
		xtype : 'combo',
		name : 'ushortName',
		displayField : 'ushortName',
		valueField : 'ushortName',
		queryMode : 'local',
		store : '@{aliasStore}'
	},{
		xtype : 'textfield',
		name : 'ufullName',
		readOnly : '@{!isCustomAlias}'
	},{
		xtype : 'textarea',
		name : 'udescription',
		readOnly : '@{!isCustomAlias}',
		height : 130,
		margin : '4 0 10'
	}],
	buttons : [{
		name : 'save',
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})
glu.defView('RS.incident.Dashboard.ChartSettings', {
	xtype: 'form',
	trackResetOnLoad: true,
	itemId: 'chartsettings',
	target: '@{target}',
	chartId: '@{chartId}',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		labelWidth: 175
	},

	items: [{
		xtype: 'combo',
		name: 'chartType',
		store: '@{chartTypeFilterStore}',
		queryMode: 'local',
		displayField: 'chartType',
		valueField: 'chartType',
		editable: false
	}, {
		xtype: 'fieldcontainer',
		hidden: '@{hideScopeConfig}',
		fieldLabel: '~~scope~~',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'numberfield',
			itemId: 'scopeValue',
			name: 'scope',
			allowBlank: false,
			labelSeparator: '',
			fieldLabel: '',
			anchor: '100%',
			flex: 2,
			margin: '0 5 0 0',
			minValue: 1,
			maxValue: '@{maxScope}'
		}, {
			xtype: 'combo',
			name: 'scopeUnit',
			labelSeparator: '',
			fieldLabel: '',
			width: 90,
			editable: false,
			store: '@{scopeUnitFilterStore}',
			queryMode: 'local',
			displayField: 'scopeUnit',
			valueField: 'scopeUnit',
			listeners: {
				select: '@{scopeUnitChange}'
			}
		}]
	}, {
		xtype: 'fieldcontainer',
		hidden: '@{hideScopeConfig}',
		labelSeparator: '',
		fieldLabel: ' ',
		items: [{
			xtype: 'label',
			style: 'font-style:italic;',
			text: '@{tips}'
		}]
	}, {
		xtype: 'numberfield',
		hidden: '@{hideColumnConfig}',
		name: 'numberOfCols',
		allowBlank: false,
		anchor: '100%',
		minValue: '@{minNColumn}',
		maxValue: '@{maxNColumn}'
	}, {
		xtype: 'agebucketbuilder',
		graphType: '@{graphType}',
		unitStore: '@{timeframeUnitFilterStore}',
		hidden: '@{hideAgeBucketConfig}',
		listeners: {
			validitychange: '@{agingBucketValidityChange}'
		}
	},{
		xtype:'fieldset',
		itemId: 'filtersSet',
		title: '~~filters~~',
		defaultType: 'chartfiltercombo',
		defaults: {
			labelWidth: 175
		},
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: []
	}, {
		xtype: 'fieldcontainer',
		fieldLabel: '~~graphType~~',
		defaultType: 'radiofield',
		layout: 'hbox',
		items: [{
			boxLabel: '~~barGraph~~',
			checked : true,
			name: 'graphType',
			margin: '0 20 0 0',
			inputValue: 'Bar Graph'

		}, {
			boxLabel: '~~pieChart~~',
			checked : false,
			name: 'graphType',
			inputValue: 'Pie Chart'
		}]

	}],

	dockedItems: [{
		xtype: 'toolbar',
		dock: 'bottom',
		hidden: '@{layoutContext}',
		defaults: {
			cls: 'rs-med-btn rs-btn-light'
		},
		items: ['->', {
			name : 'apply',
			cls : 'rs-med-btn rs-btn-dark'
		}, 'cancel']
	}],

	listeners: {
		beforerender: function() {
			var form = this.down('#chartsettings');
			this.fireEvent('loadform', this, this);
		},
		calculateposition: '@{calculatePosition}',
		loadform: '@{loadForm}',
		dirtychange: '@{dirtyChange}'
	},

	fValid: '@{fvalid}',
	setFValid: function(val) {
		this.down('#scopeValue').clearInvalid();
	},

	formLoaded: '@{formLoaded}',
	setFormLoaded: function(val) {
		this.getForm().reset();
	},
	alignPosition: '@{alignPosition}',
	setAlignPosition: function(pos) {
		this.alignPosition = pos;
	},
	asWindow: {
		title: '~~chartSettings~~',
		width: 590,
		cls : 'rs-modal-popup',
		padding : 15,
		closeAction: 'hide',
		modal: true,
		listeners: {
			boxready: function(win) {
				var me = this.down('#chartsettings');
				me.fireEvent('calculateposition');
				win.alignTo(me.target.getEl().dom.id, me.alignPosition);
				}
			}
	}
});

glu.defView('RS.incident.Dashboard', {
	hidden: '@{!pSirDashboardView}',
	itemId: 'sirDashboardPanel',
	layout: 'vbox',
	defaults: {
		width: '100%'
	},
	cls: 'rs-sir-dashboard',
	items: [{
		height: 80,
		xtype: 'panel',
		hidden: '@{!pSirDashboardCharts}',
		layout: {
			type: 'hbox',
			pack: 'center',
			align: 'stretch'
		},
		defaults: {
			height: '100%',
			style: 'border:1px solid #d2d2d2;border-right-style: none;',
		},
		items: [{
			xtype: 'container',
			width: 300,
			layout: {
				type: 'vbox',
				align: 'center',
				pack: 'center'
			},
			items: [{
				xtype: 'label',
				cls: 'rs-sir-dashboard-banner',
				text: '~~irDashboard~~',
			}, {
				xtype: 'button',
				margin: '10 0 0 0',
				name: 'openGraphicReports',
				cls: 'rs-small-btn rs-btn-light',
				tooltip: '~~openGraphicReports~~',
				text: 'Analytic Reports'
			}]
		}, {
			xtype: 'panel',
			flex: 8,
			tbar: ['->', {
				name: 'configureMetrics',
				text: '',
				glyph: 0xF013,
				tooltip: '~~configureMetrics~~'
			}],
			layout: {
				type: 'hbox',
				align: 'center',
				pack: 'center'
			},
			defaults: {
				flex: 1
			},
			items: [{
				xtype: 'dashboardmetric',
				hidden: '@{!dashboardShowOpen}',
				metricLabel: '~~open~~',
				metric: '@{openMetric}'
			}, {
				xtype: 'dashboardmetric',
				hidden: '@{!dashboardShowInProgress}',
				metricLabel: '~~inProgress~~',
				metric: '@{inProgressMetric}'
			}, {
				xtype: 'dashboardmetric',
				hidden: '@{!dashboardShowClosed}',
				metricLabel: '~~closed~~',
				metric: '@{closedMetric}'
			}, {
				xtype: 'dashboardalertmetric',
				hidden: '@{!dashboardShowPastDue}',
				metricLabel: '~~pastDue~~',
				metric: '@{pastDueMetric}'
			}, {
				xtype: 'dashboardalertmetric',
				hidden: '@{!dashboardShowSLAatRisk}',
				metricLabel: '~~slaAtRisk~~',
				metric: '@{aLAatRiskMetric}'
			}]
		}, {
			xtype: 'panel',
			width: 163,
			dockedItems: [{
				xtype: 'toolbar',
				margin: '0 0 12 0',
				cls: 'rs-dashboard-refresh-tbar',
				dock: 'top',
				items: [{
					xtype: 'combo',
					width: '100%',
					editable: false,
					fieldLabel: '~~refresh~~',
					labelStyle: 'font-weight: bold;',
					labelWidth: 60,
					displayField: 'rate',
					valueField: 'rate',
					value: '@{dashboardrefreshRate}',
					store: '@{refreshRateStore}',
					listeners: {
						change: '@{refreshRateChange}',
					}
				}],
			}],
			layout: {
				type: 'hbox',
				pack: 'center',
				align: 'center'
			},
			defaultType: 'button',
			defaults: {
				style: 'border-style: none; background-color: transparent;',
				text: ''
			},
			items: [{
				glyph: 0xF01e,
				name: 'manualRefresh',
				tooltip: '~~refresh~~',
			}, {
				glyph: 0xF04b,
				name: 'autoRefreshPlay',
				tooltip: '~~play~~',
				margin: '0 30 0 30'
			}, {
				glyph: 0xF04c,
				name: 'autoRefreshPause',
				tooltip: '~~pause~~'
			}]
		}]
	}, {
		xtype: 'grid',
		hidden: '@{!pSirDashboardInvestigationListView}',
		flex: 5,
		name: 'securityIncident',
		displayName: 'Investigations',
		cls: 'rs-grid-dark',
		itemId: 'sirDashboard',
		padding: '10 10 10 10',
		modIndicatorIcon: '<img src="/resolve/images/droparrow.png" style="float:right;width:12px;height:12px;position:relative;left:4px;">',
		modIndicatorCalendarIcon: '<img class="rs-playbook-indicator-img" src="/resolve/images/dropcalendar.png" style="float:right;width:17px;height:15px;position:relative;left:7px;top:-1px">',
		hasEditPermission: function(record, field) {
			if (this.itemId != 'sirDashboard') {
				return;
			}
			var hasAdminRole = (clientVM.user.roles.indexOf('admin') != -1);
			// Sponsorship are owner + team members
			var owner = (record.get('owner') || '').trim();
			var sponsorship =  (record.get('members') || '').split(',').map(function(m) {
				return m.trim();
			});
			sponsorship.push(owner);
			var isOwnerOrTeamMember = sponsorship.indexOf(clientVM.user.name) != -1;
			field = field || this.editingContext.field;
			return  hasAdminRole || isOwnerOrTeamMember || this.up('#sirDashboardPanel').pDashboardSirMap[field];
		},
		getIcon: function(record, icon, field) {
			var pEdit = this.hasEditPermission(record, field);
			if (pEdit) {
				return icon;
			} else {
				return '';
			}
		},
		viewConfig: {
			markDirty: false,
			getRowClass: function(rec) {
				return 'rs-sir-dashboard-record'
			}
		},
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: [{
				name: 'newInvestigation',
				disabled: '@{!pSirInvestigationsCreateNew}',
				text: '~~newSecurityIncident~~',
			},'export',{
				xtype: 'tbseparator'
			}, {
				xtype: 'checkboxgroup',
				height: 24,
				width: 400,
				fieldLabel: 'Show',
				labelWidth: 50,
				columns: 4,
				vertical: false,
				defaultType : 'checkbox',
				items: [
					{checked: true, boxLabel: 'Open', inputValue: 'Open', width: 50},
					{checked: true, boxLabel: 'In Progress', inputValue: 'In Progress', width: 110},
					{checked: true, boxLabel: 'Complete', inputValue: 'Complete', width: 95},
					{checked: false, boxLabel: 'Closed', inputValue: 'Closed', width: 50}
				],
				listeners: {
					change: '@{handleFilterChange}'
				}
			}]
		}],
		buttonAlign: 'left',
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true
		}, {
			ptype: 'pager'
		}, {
			ptype: 'cellediting',
			clicksToEdit: 1,
			listeners: {
				beforeedit: function( editor, context, eOpts ) {
					context.grid.editingContext = context;   // Saving editing context to be used later (in controlling the template data from being modified)

					// User who is neither the case owner nor team members is not allowed to edit investigation
					var pEdit = context.grid.hasEditPermission(context.record);
					if (!pEdit) {
						return false;
					}

					// Don't want clicking on the link to enter edit mode
					// Also do not allow columns to be edited if status is closed except to reopen an incident
					if ((context.colIdx == 0) || ((context.field !== 'status' && context.record.get('status').toLowerCase() === 'closed'))) {
						return false;
					}

					if (context.grid.activityEditor) {
						if (context.record.get('sirStarted')) {
							// Tickets are not allowed to be modified once investigation has started
							context.grid.activityEditor.setDisabled(true);
						}
						else {
							context.grid.activityEditor.setDisabled(false);
						}
					}
					return true;
				}
			}
		}],

		store: '@{store}',
		columns: [{
			dataIndex: 'sir',
			text: '~~id~~',
			sortable: true,
			filterable: true,
			flex:.9,
			renderer: function(value, meta, record) {
				if (value) {
					return Ext.String.format('<a target="_blank" class="sc-link" href="/resolve/jsp/rsclient.jsp?{0}#RS.incident.Playbook/sir={1}">{2}</a>',
						record.store.rootVM.rootcsrftoken, record.get('sir'), record.get('sir'));
				}
				return ''
			}
		}, {
			dataIndex: 'playbook',
			text: '~~playbook~~',
			sortable: true,
			filterable: true,
			flex: 1.1,
			editor: {
				xtype: 'documentfield',
				store: '@{playbookTemplateStore}',
				displayField: 'ufullname',
				valueField: 'ufullname',
				editable: false,
				trigger2Cls: '',
				inline: true,   // inline cell editing
				listeners: {
					beforerender: function( v, eOpts ) {
						var grid = this.up('#sirDashboard');
						grid.activityEditor = v;
						if (grid.editingContext) {
							// Can't modify playbook after the investigation has started
							v.setDisabled(!!grid.editingContext.record.get('sirStarted'));
						}
					},
					focus: function(editor) {
						editor.store.load({
							callback: function(records, operation, success) {
								editor.expand();
							}
						});
					},
					select: function(editor) {
						editor.findParentByType('editor').completeEdit();
					}
				}
			},
			renderer: function(val, meta, record, rowIndex, colIndex) {
				val = Ext.String.htmlEncode(val || '');
				var pencilIcon = '';
				var field = this.getHeaderContainer().getHeaderAtIndex(colIndex).dataIndex;

				if (!record.get('sirStarted')) {
					pencilIcon = this.getIcon.call(this, record, this.modIndicatorIcon, field);
				}
				meta.tdCls += ' rs-playbook-modindicator';
				return Ext.String.format('<span style="float:left">{0}</span>{1}', val, pencilIcon);
			}
		}, {
			dataIndex: 'title',
			text: '~~title~~',
			sortable: true,
			filterable: true,
			flex: 1.2,
			editor: {
				xtype: 'textfield',
				allowBlank: false,
				listeners: {
					focus: function(editor) {
						editor.focus();
					}
				}
			},
			renderer: 'htmlEncode'
		}, {
			dataIndex: 'sourceSystem',
			text: '~~origin~~',
			sortable: true,
			filterable: true,
			hidden: true,
			flex: .8
		}, {
			dataIndex: 'id',
			text: RS.common.locale.sysId,
			sortable: true,
			hidden: true,
			sortable: false,
			filterable: false,
			flex: .8
		}, {
			dataIndex: 'investigationType',
			text: '~~type~~',
			sortable: true,
			filterable: true,
			flex: .8,
			editor: {
				xtype: 'combo',
				store: '@{..typeStore}',
				queryMode: 'local',
				displayField: 'type',
				valueField: 'type',
				editable: false,
				listeners: {
					focus: function(editor) {
						this.fireEvent('refreshTypes', null, editor);
					},
					select: function(editor) {
						editor.findParentByType('editor').completeEdit();
					},
					refreshTypes: '@{refreshTypes}'
				}
			},
			renderer: function(val, meta, record, rowIndex, colIndex) {
				val = Ext.String.htmlEncode(val || '');
				var field = this.getHeaderContainer().getHeaderAtIndex(colIndex).dataIndex;
				if (meta.record.get('status').toLowerCase() !== 'closed') {
					meta.tdCls += ' rs-playbook-modindicator';
					return Ext.String.format('<span style="float:left">{0}</span>{1}', val, this.getIcon.call(this, meta.record, this.modIndicatorIcon, field));
				} else {
					return val;
				}
			}
		}, {
			dataIndex: 'severity',
			text: '~~severity~~',
			sortable: true,
			filterable: true,
			flex: .5,
			editor: {
				xtype: 'combo',
				store: ['Critical', 'High', 'Medium', 'Low'],
				editable: false,
				listeners: {
					focus: function(editor) {
						editor.expand();
					},
					select: function(editor) {
						editor.findParentByType('editor').completeEdit();
					}
				}
			},
			renderer: function(val, meta, record, rowIndex, colIndex) {
				val = Ext.String.htmlEncode(val || '');
				var field = this.getHeaderContainer().getHeaderAtIndex(colIndex).dataIndex;
				var mVal = (val.toLocaleLowerCase() == 'close')? 'Closed': val;
				meta.tdCls += ' rs-playbook-modindicator';
				return Ext.String.format('<span style="float:left">{0}</span>{1}', mVal, this.getIcon.call(this, record, this.modIndicatorIcon, field));

			}
		}, {
			dataIndex: 'status',
			text: '~~status~~',
			sortable: true,
			filterable: false,
			flex: .6,
			editor: {
				xtype: 'combo',
				store: ['Open', 'In Progress', 'Complete', 'Closed'],
				editable: false,
				listeners: {
					focus: function(editor) {
						editor.expand();
					},
					select: function(editor) {
						editor.findParentByType('editor').completeEdit();
					}
				}
			},
			renderer: function(val, meta, record, rowIndex, colIndex) {
				val = Ext.String.htmlEncode(val || '');
				var field = this.getHeaderContainer().getHeaderAtIndex(colIndex).dataIndex;
				var mVal = (val.toLocaleLowerCase() == 'close')? 'Closed': val;
				meta.tdCls += ' rs-playbook-modindicator';
				return Ext.String.format('<span style="float:left">{0}</span>{1}', mVal, this.getIcon.call(this, record, this.modIndicatorIcon, field));

			}
		}, {
			dataIndex: 'sla',
			flex: .8,
			text: '~~sla~~',
			renderer: function (slaInSeconds, metaData, record, rowIndex, colIndex, store, view) {
				var days = 0;
				var hours = 0;
				if (slaInSeconds) {
					// 86400 = n_seconds_per_day
					// 3600 = n_seconds_per_hour
					days = Math.floor(slaInSeconds/86400);        // IE does't support Math.trunc
					hours = Math.floor(slaInSeconds%86400)/3600;  // using floor assuming positive inputs
				}
				if (days || hours) {
					return Ext.String.format('{0} Days {1} Hours', days, hours);
				} else {
					return 'None';
				}
			}
		}, {
			dataIndex: 'dueBy',
			text: '~~dueDate~~',
			sortable: true,
			filterable: true,
			flex: .8,
			editor: {
				xtype: 'datetimefield',
				allowBlank: true,
				//minValue : new Date(),
				editable: false,
				submitFormat: 'Y-m-d',
				listeners: {
					focus: function(editor) {
						editor.expand();
					},
					select: function(editor) {
						editor.findParentByType('editor').completeEdit();
					},
					deactivate: function(editor) {
						editor.findParentByType('editor').completeEdit();
					}
				}
			},
			renderer: function(field, meta, record, rowIndex, colIndex) {
				field = Ext.String.htmlEncode(field || '');
				var fld = this.getHeaderContainer().getHeaderAtIndex(colIndex).dataIndex;
				meta.tdCls += 'rs-playbook-modindicator';
				return Ext.String.format('<span style="float:left">{0}</span>{1}', field, this.getIcon.call(this, meta.record, this.modIndicatorCalendarIcon, fld));
			}
		}, {
			dataIndex: 'owner', //display owner/ownerName
			text: '~~owner~~',
			sortable: true,
			filterable: true,
			flex: 1,
			editor: {
				xtype: 'combo',
				store: '@{ownerStore}',
				queryMode: 'local',
				displayField: 'userId', //display owner/ownerName
				valueField: 'userId',
				editable: false,
				//value: '',
				listeners: {
					focus: function(editor) {
						this.fireEvent('refreshMembers', null, editor);
					},
					select: function(editor) {
						editor.findParentByType('editor').completeEdit();
					},
					refreshMembers: '@{refreshMembers}'
				}
			},
			renderer: function(val, meta, record, rowIndex, colIndex) {
				val = Ext.String.htmlEncode(val || '');
				var field = this.getHeaderContainer().getHeaderAtIndex(colIndex).dataIndex;
				if (meta.record.get('status').toLowerCase() !== 'closed') {
					meta.tdCls += ' rs-playbook-modindicator';
					return Ext.String.format('<span style="float:left">{0}</span>{1}', val, this.getIcon.call(this, meta.record, this.modIndicatorIcon, field));
				} else {
					return val
				}
			}
		}, {
			xtype: 'datecolumn',
			dataIndex: 'sysCreatedOn',
			text: '~~createdOn~~',
			sortable: true,
			hidden: false,
			initialShow: true,
			filterable: true,
			flex: 1,
			renderer: function(val) {
				return (val === null) ? '' : Ext.util.Format.htmlEncode(Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat()))
			}
		}, {
			xtype: 'datecolumn',
			dataIndex: 'closedOn',
			text: '~~closedOn~~',
			sortable: true,
			filterable: true,
			flex: 1,
			renderer: function(val) {
				return (val === null) ? '' : Ext.util.Format.htmlEncode(Ext.util.Format.date(new Date(val), clientVM.getUserDefaultDateFormat()))
			}
		}, {
			dataIndex: 'closedBy',
			text: '~~closedBy~~',
			sortable: true,
			filterable: true,
			flex: 1,
			renderer: 'htmlEncode'
		}, {
			header: '~~organization~~',
			dataIndex: 'sysOrg',
			initialShow: true,
			sortable: true,
			renderer: function(value, metaData, record) {
				if (value) {
					if(!record.store.id2NameMap) {
						record.store.id2NameMap = {};
						clientVM.user.orgs.forEach(function(e) {
							if(e.uname != 'None')
								record.store.id2NameMap[e.id] = e.uname;
						});
					}
					return RS.common.grid.plainRenderer() (record.store.id2NameMap[value]);
				}
				return '';
			}
		}],
		listeners: {
			edit: '@{handleEdit}'
		}
	}],

	listeners: {
		chartdatachange: '@{chartDataChange}',
		afterrender: '@{loadSettings}',
		beforedestroy : '@{beforeDestroyComponent}'
	},

	scope: '@{scope}',
	setScope: function(val) {
		this.scope = val;
	},
	scopeUnit: '@{scopeUnit}',
	setScopeUnit: function(val) {
		this.scopeUnit = val;
	},

	sirPermissions: '@{sirPermissions}',
	setSirPermissions: function(val) {
		this.sirPermissions = val;
		this.pDashboardSirMap = {
			playbook: this.sirPermissions.sirDashboardPlaybook ? this.sirPermissions.sirDashboardPlaybook.modify : false,
			title: this.sirPermissions.sirDashboardTitle ? this.sirPermissions.sirDashboardTitle.modify : false,
			investigationType: this.sirPermissions.sirDashboardType ? this.sirPermissions.sirDashboardType.modify : false,
			severity: this.sirPermissions.sirDashboardSeverity ? this.sirPermissions.sirDashboardSeverity.modify : false,
			status: this.sirPermissions.sirDashboardStatus ? this.sirPermissions.sirDashboardStatus.modify : false,
			dueBy: this.sirPermissions.sirDashboardDueDate ? this.sirPermissions.sirDashboardDueDate.modify : false,
			owner: this.sirPermissions.sirDashboardOwner ? this.sirPermissions.sirDashboardOwner.modify : false
		}
	}
});

glu.defView('RS.incident.ChangeForm', {
	asWindow: {
		title: '@{title}',
		modal: true,
		padding: 10
	},
	xtype: 'form',
	buttons: [{
		name: 'apply',
		formBind: true
	}, 'cancel'],
	buttonAlign: 'left',
	listeners: {
		afterrender: function(v) {
			this.fireEvent('initview', this, v);
		},
		initview: '@{initView}'
	}
});
glu.defView('RS.incident.Dashboard.PDFConverter',{
	modal: true,
	width : 450,
	height: 500,
	padding : 15,
	cls: 'rs-modal-popup',
	title : '~~exportPDF~~',
	layout : {
		type: 'vbox',
		align : 'stretch'
	},
	items : [{
		xtype : 'component',
		html : '~~info~~',
		margin : '0 0 5 0'
	},{
		xtype: 'grid',
		autoScroll : true,
		flex: 1,
		store : '@{store}',
		name: 'entry',
		cls: 'rs-grid-dark',
		columns: [{
			dataIndex: 'display',
			text: '~~name~~',
			flex: 1
		}],
		selType:'checkboxmodel',
	    selModel : {
	        mode : 'simple'
	    },
	    margin: '0 0 10 0',
	}],
	buttons : [{
		name: 'start',
		cls : 'rs-med-btn rs-btn-dark',
	},{
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light',
	}]
})
glu.defView('RS.incident.ExportAuditLog', {
    asWindow: {
        title: '~~exportingAuditLogs~~',
        modal: true,
        padding: 15,
        cls : 'rs-modal-popup',
        minWidth : 600,
        minHeight: 300
    },
    xtype: 'form',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        labelWidth: 150
    },
    items: [{
        xtype: 'fieldcontainer',
        fieldLabel: '~~additionalSir~~',
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        items: [{
            xtype: 'documentfield',
            flex: 8,
            fieldLabel: '',
            name: 'sir',
            value: '@{sir}',
            store: '@{sirStore}',
            allowBlank: true,
            inline: true,   // inline cell editing
            displayField: 'sir',
            valueField: 'sir',
            trigger2Cls: '',
            listeners: {
                select : '@{handleSirSelect}',
                validatesirname: '@{validateSirName}',
                blur: function(v) {
                    v.fireEvent('validatesirname', this, v.store);
                }
            },
            margin: '0 5 0 0'
        }, {
            xtype: 'button',
            cls : 'rs-small-btn rs-btn-dark',
            flex: 2,
            name: 'add'
        }]
    },{
        xtype: 'textarea',
        name: 'sirList',
        readOnly: true
    }, {
        xtype: 'combo',
        name: 'fileType',
        store: '@{fileTypeStore}',
        editable: false,
        allowBlank: false,
        displayField: 'fileType',
        valueField: 'fileType'
    },  {
        xtype: 'textfield',
        name: 'outputFileName',
        allowBlank: false
    }],
    exporting: '@{exporting}',
    setExporting: function() {
        var btn = this.down('#exportBnt');
        btn.setText('Exporting...');
        btn.setDisabled(true);
    },
    buttons: [{
        name: 'exportAll',
        itemId: 'exportBnt',
        formBind: true,
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name :'cancel',
        cls : 'rs-med-btn rs-btn-light'
    }],
});
glu.defView('RS.incident.ExportDashboard', {   
    title: '~~export~~',
    modal: true,
    padding: 15,
    cls : 'rs-modal-popup',
    width : 600, 
    minHeight : 500,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        labelWidth: 150
    },
    items: [{
        xtype: 'textfield',
        name: 'outputFileName',
        allowBlank: false
    },{
        xtype: 'combo',
        name: 'fileType',
        store: '@{fileTypeStore}',
        editable: false,
        allowBlank: false,
        displayField: 'fileType',
        valueField: 'fileType'
    },{
        xtype : 'component',
        hidden : '@{fileTypeIsNotPDF}',  
        html : '~~info~~',
        margin : '0 0 5 0'
    },{
        xtype: 'grid',
        hidden : '@{fileTypeIsNotPDF}',  
        autoScroll : true,
        flex: 1,
        store : '@{columnSelectionStore}',
        name: 'entry',
        cls: 'rs-grid-dark',
        columns: [{
            dataIndex: 'display',
            text: '~~name~~',
            flex: 1
        }],
        selType:'checkboxmodel',
        selModel : {
            mode : 'simple'
        },
        margin: '0 0 10 0',
    }],   
    buttons: [{
        name: 'exportAll',      
        cls : 'rs-med-btn rs-btn-dark'
    },{
        name :'cancel',
        cls : 'rs-med-btn rs-btn-light'
    }],
});
glu.defView('RS.incident.Dashboard.KPI', {
	cls: 'rs-sir-dashboard',
	dockedItems: [{
		xtype: 'toolbar',
		padding : '10 25 0 15',
		defaults: {
			margin: '0 5 0 5'
		},
		items: [{
			xtype: 'label',
			cls: 'rs-sir-dashboard-banner',
			text: '@{bannerText}'
		}, '->', {
			xtype: 'panel',
			hidden: '@{!viewMode}',
			layout: 'hbox',
			defaultType: 'button',
			defaults: {
				style: 'border-style: none; background-color: transparent;',
				text: ''
			},
			items: [{
				glyph: 0xF01e,
				padding: '5',
				name: 'manualRefresh',
				tooltip: '~~refresh~~'
			}, {
				xtype: 'combo',
				width: 80,
				editable: false,
				labelStyle: 'font-weight: bold;',
				displayField: 'rate',
				valueField: 'rate',
				value: '@{dashboardrefreshRate}',
				store: '@{refreshRateStore}',
				listeners: {
					change: '@{refreshRateChange}'
				}
			}]
		}, {
			name: 'addSection',
			hidden: '@{viewMode}',
			iconCls: 'rs-icon-button icon-plus-sign-alt',
			cls : 'rs-small-btn rs-btn-light'
		}, {
			name: 'save',
			hidden: '@{viewMode}',
			iconCls: 'rs-social-button icon-save',
			text: '',
			tooltip: '~~save~~'
		}, {
			iconCls: 'rs-social-button icon-reply-all',
			hidden: '@{viewMode}',
			name: 'exitEdit',
			text: '',
			tooltip: '~~exitEdit~~'
		}, {
			glyph: 0xF013,
			hidden: '@{!viewMode}',
			name: 'configureSettings',
			text: '',
			tooltip: '~~configureSettings~~'
		}, {
			name: 'refreshSettings',
			hidden: true, // '@{viewMode}', to-be-supported
			iconCls: 'rs-social-button icon-repeat',
			text: '',
			tooltip: '~~refresh~~'
		}]
	}],
	layout: {
		type: 'card',
		deferredRender: true
	},
	activeItem: '@{activeItem}',
	defaults: {
		hideMode: 'offsets'
	},
	items: [{
		xtype: '@{kpiView}'
	}, {
		xtype: '@{kpiLayout}'
	}]
});

glu.defView('RS.incident.Dashboard.KPILayout', {
	overflowX: 'auto',
	overflowY: 'auto',
	margin: 5,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: '@{layoutSections}'
});

glu.defView('RS.incident.Dashboard.KPILayoutSection', {
	minHeight: 370,
	minWidth: 544,
	margin: '2 15 15 15',
	bodyPadding: '0 8 8 8',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	style: {
		borderWidth: '1px',
		borderColor: 'silver',
		borderStyle: 'solid'
	},
	dockedItems: [{
		xtype: 'toolbar',
		cls: 'actionBar rs-dockedtoolbar',
		margin: '5 5 0 5',
		items: ['->', {
			name: 'addKPIEntry',
			iconCls: 'rs-icon-button icon-plus-sign-alt',
			cls : 'rs-small-btn rs-btn-light'
		}, {
			iconCls: 'rs-social-button icon-remove-sign',
			name: 'remove',
			text: '',
			tooltip: '~~deleteSection~~'
		}]
	}],
	defaults: {
		flex: 1
	},
	items: '@{KPILayoutSectionEntries}'
});

glu.defView('RS.incident.Dashboard.KPILayoutSectionEntry', {
	margin : '5 8 10 8',
	layout: 'anchor',
	style: {
		borderWidth: '1px',
		borderColor: 'silver',
		borderStyle: 'dashed'
	},
	dockedItems: [{
		xtype: 'toolbar',
		cls: 'actionBar rs-dockedtoolbar',
		items: ['->', {
			iconCls: 'rs-social-button icon-remove-sign',
			name: 'remove',
			text: '',
			tooltip: '~~deleteSection~~'
		}]
	}],
	items:[{
		xtype: '@{chartSetting}',
		anchor: '100%',
		margin: 10
	}]
});

glu.defView('RS.incident.Dashboard.KPIView', {
	overflowY: 'auto',
	defaults: {
		flex: 1
	},
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: '@{viewSections}',
	listeners: {
		listeners: {
			beforedestroy : '@{beforeDestroyComponent}'
		}
	}
});

glu.defView('RS.incident.Dashboard.KPIViewSection', {
	minHeight: 300,
	padding : 8,
	overflowX: 'auto',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	defaults: {
		flex: 1
	},
	items: '@{viewSectionEntries}'
});

glu.defView('RS.incident.Dashboard.KPIViewSectionEntry', {
	minHeight: 290,
	margin : '2 2 2 2',
	overflowX: 'auto',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	items:[{
		xtype: 'chart',
		chartId: '@{chartId}',
		flex: 1,
		chartSettingsHandler: '@{chartSettingsHandler}',
		chartConfigs: '@{chartConfigs}',
		refreshMonitor: '@{refreshMonitor}'
	}],
	listeners: {
		afterrender: function () {
			var chart = this.down('chart');
			chart.setChartConfigs(chart.chartConfigs);
		}
	},
	chartConfigs: '@{chartConfigs}',
	setChartConfigs: function(config) {
		var chart = this.down('chart');
		chart.setChartConfigs(config);
	}
});

glu.defView('RS.incident.Dashboard.MetricsSetting', {
	xtype: 'form',
	itemId: 'metricssetting',
	target: '@{target}',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		margin: '4 0',
		labelWidth: 130,
		flex: 1
	},
	items: [{
		xtype: 'fieldcontainer',
		margin : 0,
		fieldLabel: '~~show~~',
		layout: 'hbox',
		defaultType: 'checkbox',
		defaults: {
			hideLabel : true,
			margin : '0 15 0 0'
		},
		items: [{
			name: 'dashboardShowOpen',
			boxLabel : '~~dashboardShowOpen~~',
			value: false
		}, {
			name: 'dashboardShowInProgress',
			boxLabel : '~~dashboardShowInProgress~~',
			value: false
		}, {
			name: 'dashboardShowClosed',
			boxLabel : '~~dashboardShowClosed~~',
			value: false
		}, {
			name: 'dashboardShowPastDue',
			boxLabel : '~~dashboardShowPastDue~~',
			value: false
		}, {
			name: 'dashboardShowSLAatRisk',
			boxLabel : '~~dashboardShowSLAatRisk~~',
			value: false
		}]
	}, {
		xtype: 'combo',
		editable: false,
		store: '@{slaPolicyStore}',
		name: 'sLAatRiskPolicy',
		value: '',
		displayField: 'label',
		valueField: 'nDays'
	}, {
		xtype: 'fieldcontainer',
		fieldLabel: 'Scope',
		layout: {
			type: 'hbox',
			align: 'stretch'
		},
		items: [{
			xtype: 'numberfield',
			name: 'scope',
			allowBlank: false,
			labelSeparator: '',
			fieldLabel: '',
			anchor: '100%',
			flex: 1,
			margin: '0 5 0 0',
			minValue: 1,
			maxValue: '@{maxScope}'
		}, {
			xtype: 'combo',
			name: 'scopeUnit',
			labelSeparator: '',
			fieldLabel: '',
			width: 120,
			editable: false,
			store: '@{scopeUnitStore}',
			displayField: 'unit',
			valueField: 'unit',
			listeners: {
				select: '@{scopeUnitChange}'
			}
		}]
    }, {
		xtype: 'fieldcontainer',
		labelSeparator: '',
		fieldLabel: ' ',
		//margin: '2 0 10 10',
		items: [{
			xtype: 'label',
			style: 'font-style:italic;',
			text: '@{tips}'
		}]
	}],

	buttons: [{
		name: 'apply',
		formBind: true,
		cls : 'rs-med-btn rs-btn-dark'
	} ,{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}],

	listeners: {
		loadform: '@{loadForm}'
	},

	fValid: '@{fvalid}',
	setFValid: function(val) {
		this.down('numberfield').clearInvalid();
	},

	asWindow: {
		title: '~~metricsSetting~~',
		closeAction: 'hide',
		border: false,
		closable: true,
		cls : 'rs-modal-popup',
		padding :15,
		listeners: {
			beforerender: function() {
				var form = this.down('#metricssetting');
				form.fireEvent('loadform', this, form);
			},
			show: function(win) {
				win.alignTo(this.down('#metricssetting').target.id, 'tr-br');

			}
		}
	}
});

glu.defView('RS.incident.Activities', {
	xtype: 'container',
	layout: 'anchor',
	items: [{
		itemId: 'playbook-activitiesgrid',
		hidden: '@{..isActivitiesView}',
		anchor: '100% 100%',
		xtype: 'grid',
		cls: 'rs-grid-dark sir-activities-grid',
		border: false,
		bodyBorder: false,
		modifiable: '@{..pSirInvestigationViewerActivitiesModify}',
		store: '@{activitiesStore}',

		//Icon Paths
		modIndicator: function() {
			return this.modifiable? '<img class="rs-playbook-indicator-img" src="/resolve/images/droparrow.png" style="float:right;width:12px;height:12px;position:relative;left:4px;">': '';
		},

		modIndicatorText:  function() {
			return this.modifiable? '<img class="rs-playbook-indicator-img" src="/resolve/images/edittsk_tsk_gray.gif" style="float:left;margin-left:5px;height:12px;">': '';
		},

		modIndicatorCalendar: function() {
			return this.modifiable? '<img class="rs-playbook-indicator-img" src="/resolve/images/dropcalendar.png" style="float:right;width:17px;height:15px;position:relative;left:7px;top:-1px">': '';
		},

		placeholderUserActIcon: '<span style=display:inline-block;height:10px;width:15px;float:left;position:relative;left:-5px;></span>',

		userActIcon: '<img src="/resolve/images/user_add.png" style="height: 15px;float:left;position:relative;left:-5px;">',

		//View Config
		viewConfig: {
			markDirty: false,
			plugins: {
				ptype: 'gridviewdragdrop',
				dropZone: {
					onNodeOver: function( nodeData, source, e, data ) {
						var rec = this.view.getRecord(nodeData);
						this.positionIndicator(nodeData, data, e);
						return (data.records[0] != rec && data.records[0].get('phase') == rec.get('phase'))? this.dropAllowed: this.dropNotAllowed;
					}
				}
			},
			stripeRows: false,
			getRowClass: function (record, rowIndex, rowParams, store) {
				if(!record.get('activityName') && !record.get('wikiName') && !record.get('wikiName') && !record.get('description')) {
					return 'rs-playbook-hide-row-expander';
				}
			}
		},

		//Grid Features
		features: [
			Ext.create('Ext.grid.feature.Grouping',{
				groupHeaderTpl: '{name}',
				onGroupClick: function(view, rowElement, groupName, e)  {
					// This is the workaround to fix the collapse issue on phase with html RBA-15274
					// Basically, the browser crashes on collapsing the phase with html. Subsequent clicks on the
					// 'Add' (activity) button causes black screen
					Ext.grid.feature.Grouping.prototype.onGroupClick.apply(this, [view, rowElement, Ext.htmlEncode(groupName), e]);
				}
			})
		],

		//features: [{
		//	ftype: 'grouping',
		//	groupHeaderTpl: '{name}',
		//}],

		//Grid Plugins
		plugins: [/*{
			ptype: 'rowexpander',
			columnWidth: 32,
			rowBodyTpl : ['<div style=padding-top:0px;word-wrap:break-word;><span style=font-weight:bold;>Description: </span>{description}</div>']
		}, */{
			ptype: 'cellediting',
			clicksToEdit: 1,
			listeners: {
				beforeedit: function(editor, context, eOpts) {
					if (context.record.get('isPreview')) {
						// preview activities are not modifiable
						return false;
					} else if (!context.grid.modifiable) {
						return false;
					} else if (context.colIdx == 0 &&
						(context.record.get('templateActivity') ||  // Template activities are not modifiable
								window.event && window.event.srcElement && window.event.srcElement.tagName.toLowerCase() == 'a')) {
						return false; // Don't want clicking on the link to enter edit mode
					}
					context.grid.editingContext = context; // Saving editing context to be used later (in controlling the template data from
					return (Ext.firefoxVersion !== 0)? (!context.record.get('templateActivity') ||  context.field != 'activityName'): true;
				}
			}
		}],

		//Docked Toolbar
		dockedItems: [{
			xtype: 'toolbar',
			cls: 'rs-dockedtoolbar',
			dock: 'top',
			disabled: '@{activityToolBarDisabled}',
			items: [{
				xtype: 'button',
				disabled: '@{!..pSirInvestigationViewerActivitiesCreate}',
				cls: 'rs-small-btn rs-btn-light',
				name: 'addActivity',
				maxWidth: 100
			}, {
				xtype: 'tbseparator'
			},
			{
				xtype: 'checkboxgroup',
				height: 24,
				width: 400,
				fieldLabel: 'Show',
				labelWidth: 50,
				columns: 4,
				vertical: false,
				defaultType : 'checkbox',
				items: [
					{checked: true, boxLabel: 'Open', inputValue: 'Open', width: 50},
					{checked: true, boxLabel: 'In Progress', inputValue: 'In Progress', width: 110},
					{checked: true, boxLabel: 'Complete', inputValue: 'Complete', width: 95},
					{checked: true, boxLabel: 'N/A', inputValue: 'N/A', width: 50}
				],
				listeners: {
					change: '@{handleFilterChange}'
				}
			}, '->', {
				xtype: 'tbtext',
				text: 'Progress',
				style: {
					fontWeight: 'bold'
				}
			}, {
				xtype: 'progressbar',
				itemId: 'playbook-progressBar',
				height: 12,
				width: 250,
				value : '@{playbookProgress}',
				listeners: {
					afterrender: function(progressBar) {
						this._vm.on('updateProgressBar', function(newProgress) {
							progressBar.updateProgress(newProgress);
						})
					}
				}
			}]
		}],

		columns: [{
			dataIndex: 'activityName',
			text: '~~activity~~',
			flex: 2,
			sortable: false,
			groupable: false,
			draggable: false,
			hideable: false,
			editor: {
				xtype: 'textfield',
				allowBlank: false,
				listeners: {
					focus: function(editor) {
						editor.focus();
					}
				}
			},
			renderer: function(value, meta, record) {
				if (record.getData().id === null) {
					return value
				} else {
					var vm = this._vm;
					var id = Ext.id();
					/*Ext.defer(function () {
						//Make sure this el still exist. TEMP fix.
						var el = document.getElementById(id);
						if(!el)
							return;
						Ext.widget('button', {
							renderTo: id,
							tooltip: vm.localize('moreTip'),
							iconCls : 'icon-ellipsis-horizontal',
							cls : ' sir-action-icon',
							menu: {
								xtype: 'menu',
								items: [{
									text: vm.localize('addNotes'),
									handler: function(){
										vm.addNotes();
									}
								}, {
									text: vm.localize('addArtifacts'),
									handler: function(){
										vm.addArtifacts();
									}
								}, {
									text: vm.localize('addAttachments'),
									handler: function() {
										vm.addAttachments();
									}
								}, {
									xtype: 'menuseparator'
								}, {
									text: vm.localize('viewAutomationResults'),
									handler: function() {
										vm.viewAutomationResults();
									}
								}],
								listeners: {
									mouseleave: function() {
										this.hide();
									}
								}
							},
						});
					}, 500);*/
					var actionBtn = Ext.String.format('<span style="float:right;position:relative;left:4px;" id="{0}"></span>', id)
					var isTemplateActivity = record.get('templateActivity');
					// Double-encoding is needed for qtip
					// User-added activities are already encoded by the backend so they are only needed to encode once more.
					var qTip = isTemplateActivity? Ext.htmlEncode(Ext.htmlEncode(record.get('description'))) : Ext.htmlEncode(record.get('description'));
					meta.tdAttr = 'data-qtip="' + qTip + '"';
					if (isTemplateActivity) {
						var href = '';
						if (record.get('isPreview')) {
							href = 'javascript:void(0)';
						} else {
							href =  Ext.String.format('#RS.incident.Playbook/sir={0}&activityId={1}', this._vm.parentVM.ticketInfo.sir, record.get('id'));
						}
						return Ext.String.format(
							'<div class="sir-activity-value">{0}<a style="float:left" class="sc-link rs-activityname-link" href="'+href+'">{1}</a>{2}</div>',
							this.placeholderUserActIcon,
							value,
							actionBtn
						)
					} else {
						meta.tdCls += 'rs-playbook-modindicator';
						return Ext.String.format(
							'<div class="sir-activity-value">{0}<a style="float:left" class="sc-link rs-activityname-link" href="#RS.incident.Playbook/sir={1}&activityId={2}">{3}</a>{4}{5}</div>',
							this.userActIcon,
							this._vm.parentVM.ticketInfo.sir,
							record.get('id'),
							record.activityNameDirty? Ext.htmlEncode(value): value, // Encode if not done so by backend
							this.modIndicatorText.call(this),
							actionBtn
						)
					}
				}
			}
		}, {
			dataIndex: 'days',
			text: '~~sla~~',
			flex: 1,
			sortable: false,
			groupable: false,
			draggable: false,
			hideable: false,
			renderer: function(val, meta, record) {
				var days = parseInt(record.getData().days) || 0;
				var hours = parseInt(record.getData().hours) || 0;
				var minutes = parseInt(record.getData().minutes) || 0;
				if ((days + hours + minutes) === 0) {
					return 'None'
				} else {
					var returnStrArr = [];
					if (days !== 0) {
						returnStrArr.push((days === 1) ? days + ' Day' : days + ' Days');
					}
					if (hours !== 0) {
						returnStrArr.push((hours === 1) ? hours + ' Hour' : hours + ' Hours');
					}
					if (minutes !== 0) {
						returnStrArr.push((minutes === 1) ? minutes + ' Minute' : minutes + ' Minutes');
					}
					return returnStrArr.join(' ')
				}
			}
		}, {
			dataIndex: 'dueDate',
			text: '~~dueBy~~',
			flex: 1,
			sortable: false,
			groupable: false,
			draggable: false,
			hideable: false,
			editor: {
				xtype: 'datetimefield',
				allowBlank: true,
				//minValue : new Date(),
				editable: false,
				format: Ext.state.Manager.get('userDefaultDateFormat', 'Y-m-d H:i:s'),
				submitFormat: 'Y-m-d',
				listeners: {
					focus: function(editor) {
						editor.expand();
					},
					select: function(editor) {
						editor.findParentByType('editor').completeEdit();
					},
					deactivate: function(editor) {
						editor.findParentByType('editor').completeEdit();
					}
				}
			},
			renderer: function(field, meta, record) {
				meta.tdCls += 'rs-playbook-modindicator';
				return Ext.String.format('<span style="float:left">{0}</span>{1}', field, record.get('isPreview') ? '' : this.modIndicatorCalendar.call(this));
			}
		}, {
			dataIndex: 'assignee',
			text: '~~assignee~~',
			flex: 1,
			sortable: false,
			groupable: false,
			draggable: false,
			hideable: false,
			editor: {
				xtype: 'combobox',
				allowBlank: true,
				displayField: 'userId',
				valueField: 'userId',
				store: '@{membersStoreGrid}',
				queryMode : 'local',
				editable: false,
				listeners: {
					focus: function(editor) {
						editor.expand();
					},
					select: function(editor) {
						editor.findParentByType('editor').completeEdit();
					}
				}
			},
			renderer: function(field, meta, record) {
				meta.tdCls += 'rs-playbook-modindicator';
				return Ext.String.format('<span style="float:left">{0}</span>{1}', field, record.get('isPreview') ? '' : this.modIndicator.call(this));
			}
		}, {
			dataIndex: 'status',
			text: '~~status~~',
			flex: 1,
			sortable: false,
			groupable: false,
			draggable: false,
			hideable: false,
			editor: {
				xtype: 'combobox',
				allowBlank: true,
				displayField: 'type',
				valueField: 'type',
				store: '@{statusStore}',
				queryMode : 'local',
				editable: false,
				listeners: {
					focus: function(editor) {
						editor.expand();
					},
					select: function(editor) {
						editor.findParentByType('editor').completeEdit();
					}
				}
			},
			renderer: function(field, meta, record) {
				meta.tdCls += 'rs-playbook-modindicator';
				return Ext.String.format('<span style="float:left">{0}</span>{1}', field, record.get('isPreview') ? '' : this.modIndicator.call(this));
			}
		}],
		listeners: {
			activate: '@{tabInit}',
			edit: '@{handleActivitiesEdit}',
			//Drag & Drop functionality
			afterrender: function() {
				this.view.on('beforedrop', function(node, data, dropRec, dropPosition, dropHandlers) {
					var store = dropRec.store,
						insertB4 = dropPosition == 'before',
						droppable = !data.records[0].get('templateActivity') && (data.records[0].get('phase') == dropRec.get('phase')); // Template's activity can't be modified
					if (droppable) {
						var recB4DropRec = store.getAt(store.indexOf(dropRec)-1);
						if (insertB4 && (!recB4DropRec || recB4DropRec.get('phase') !== data.records[0].get('phase'))) {
							// We have to manually do two insertions after to get around the issue of grouping if inserting before the leading node.
							store.remove(data.records, true);
							store.insert(store.indexOf(dropRec) + 1, data.records);
							store.remove(dropRec, true);
							store.insert(store.indexOf(data.records[0]) + 1, dropRec);
							this.getSelectionModel().select(data.records);
							dropHandlers.cancelDrop();
							store.fireEvent('recordmoved', data.records[0], data.records[0], dropPosition);
						}
					}
					return !!droppable;
				});
				this.view.on('drop', function(node, data, overModel, dropPosition, eOpts) {
					overModel.store.fireEvent('recordmoved', data.records[0], data.records[0], dropPosition);
				});
			},
			focuschange: '@{focuschange}',
			itemmouseenter: '@{itemmouseenter}'
		}
	}, {
		xtype: '@{activitiesView}',
		anchor: '100% 93%',
		itemId: 'playbook-activitiesview',
		style: {
			fontWeight: 'bold',
		},
		hidden: '@{!..isActivitiesView}'
	}]
});


glu.defView('RS.incident.ActivityModal', {
	itemId: 'playbook-addactivity',
	padding : 15,
	title: '~~addActivityModal~~',
	cls : 'rs-modal-popup',
	height: 500,
	width: 800,
	modal: true,
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults : {
		margin : '4 0'
	},
	items: [{
		xtype: 'combo',
		fieldLabel: '~~phase~~',
		name: 'phase',
		displayField: 'type',
		valueField: 'type',
		store: '@{phasesStore}',
		queryMode: 'local',
		allowBlank: false
	}, {
		xtype: 'textfield',
		fieldLabel: '~~name~~',
		name: 'name',
		maxLength: 200,
		enforceMaxLength: true,
		allowBlank: false
	}, {
		xtype: 'container',
		layout: {
			type: 'hbox',
		},
		items: [{
			xtype: 'spinnerfield',
			fieldLabel: '~~sla~~',
			value: '@{slaDays}',
			allowBlank: false,
			width: 175,
			step: 1,
			listeners: {
				spinUp: function(spinner) {
					var value = parseInt(spinner.getValue() || 0);
					spinner.setValue((value + 1).toString());
				},
				spinDown: function(spinner) {
					var value = parseInt(spinner.getValue() || 0);
					if (value > 0) {
						spinner.setValue((value - 1).toString());
					}
				},
				change: function(spinner, newVal) {
					var value = parseInt(newVal) || 0;
					if (value < 0) {
						spinner.setValue('0');
					} else {
						spinner.setValue(value);
					}
				}
			}
		}, {
			xtype: 'label',
			height: 26,
			width: 43,
			padding: '5 0 0 10',
			text: '@{calcDaysText}'
		}, {
			xtype: 'spinnerfield',
			margin: '0 0 0 20',
			value: '@{slaHours}',
			allowBlank: false,
			hideLabel: true,
			width: 60,
			step: 1,
			listeners: {
				spinUp: function(spinner) {
					var value = parseInt(spinner.getValue() || 0);
					if (value < 23) {
						spinner.setValue((value + 1).toString());
					}
				},
				spinDown: function(spinner) {
					var value = parseInt(spinner.getValue() || 0);
					if (value > 0) {
						spinner.setValue((value - 1).toString());
					}
				},
				change: function(spinner, newVal) {
					var value = parseInt(newVal) || 0;
					if (value < 0) {
						spinner.setValue('0');
					} else if (value > 23) {
						spinner.setValue('23');
					} else {
						spinner.setValue(value);
					}
				}
			}
		}, {
			xtype: 'label',
			height: 26,
			width: 49,
			padding: '5 0 0 10',
			text: '@{calcHoursText}'
		}, {
			xtype: 'spinnerfield',
			margin: '0 0 0 20',
			value: '@{slaMinutes}',
			regex: /[0-9]+/,
			allowBlank: false,
			hideLabel: true,
			width: 60,
			step: 1,
			listeners: {
				spinUp: function(spinner) {
					var value = parseInt(spinner.getValue() || 0);
					if (value < 59) {
						spinner.setValue((value + 1).toString());
					}
				},
				spinDown: function(spinner) {
					var value = parseInt(spinner.getValue() || 0);
					if (value > 0) {
						spinner.setValue((value - 1).toString());
					}
				},
				change: function(spinner, newVal) {
					var value = parseInt(newVal) || 0;
					if (value < 0) {
						spinner.setValue('0');
					} else if (value > 59) {
						spinner.setValue('59');
					} else {
						spinner.setValue(value);
					}
				}
			}
		}, {
			xtype: 'label',
			height: 26,
			width: 61,
			padding: '5 0 0 10',
			text: '@{calcMinutesText}'
		}]
	}, {
		xtype: 'combo',
		editable: false,
		fieldLabel: 'Status',
		name: 'status',
		displayField: 'type',
		valueField: 'type',
		store: '@{..statusStore}',
		queryMode: 'local',
		allowBlank: false
	}, {
		xtype: 'combo',
		editable: false,
		fieldLabel: 'Assignee',
		name: 'assignee',
		displayField: 'userId', //display name/userId
		valueField: 'userId',
		store: '@{..membersStore}',
		queryMode: 'local',
		allowBlank: false
	}, {
		xtype: 'documentfield',
		itemId: 'wikiNameField',
		name: 'wikiName',
		store: '@{runbookStore}',
		allowBlank: false,
		validateBlank: true,
		displayField: 'ufullname',
		valueField: 'ufullname',
		maxLength: 2000,
		enforceMaxLength: true,
		editable: true,
		inline: false,  // NOT and inline editing
		width: '100%',
		listeners: {
			searchdocument: '@{searchRunbook}',
			select : '@{handleRunbookSelect}',
			validatewikiname: '@{validateWikiName}',
			blur: function(v) {
				v.fireEvent('validatewikiname', this, v)
			}
		}
	}, {
		xtype: 'textarea',
		fieldLabel: 'Description',
		name: 'description',
		flex : 1,
		margin : '4 0 10 0'
	}],
	buttons: [{
		name: 'saveActivity',
		cls : 'rs-med-btn rs-btn-dark',
		disabled: '@{isSaving}'
	}, {
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})
glu.defView('RS.incident.ActivitiesView', {
	xtype: 'container',
	itemId: 'playbook-activitiesview',
	items: [{
		xtype: 'toolbar',
		anchor: '100% 5%',
		padding: 8,
		style: {
			fontSize: '16px',
		},
		items: [{
			xtype: 'button',
			iconCls: 'rs-social-button icon-reply-all',
			tooltip: 'Return to Activities',
			handler: '@{returnToActivities}'
		}, {
			xtype: 'tbseparator'
		}, {
			xtype: 'text',
			text: '@{returnHeader}'
		},
		'->',
		/*{
			xtype: 'button',
			hidden: '@{dtDisplaymode}',
			cls: 'rs-execute-button',
			iconCls: 'rs-social-button icon-play',
			tooltip: 'Execute',
			handler: function(button) {
				button.fireEvent('executeActivity', button, button);
			},
			listeners: {
				executeActivity: '@{execute}'
			}
		},*/ {
			xtype: 'button',
			hidden: '@{dtDisplaymode}',
			name: 'notes',
			text: '',
			tooltip: 'Show Notes',
			iconCls: 'rs-social-button icon-copy'
		}]
	}, {
		xtype: '@{dtView}',
		hidden: '@{!..dtDisplaymode}',
		style: {
			borderTop: '1px #b5b8c8 solid'
		},
		listeners: {
			show: function(v) {
				var p = v.up('#playbook-activitiesview');
				v.setWidth(p.getWidth());
				v.setHeight(p.getHeight() *.95);  // 5% is for the toolbar
			}
		}
	}, {
		xtype: 'component',
		id: 'playbook-iframe',
		hidden: '@{dtDisplaymode}',
		height: '100%',
		style: {
			borderTop: '1px #b5b8c8 solid'
		}
	}]
});
//Parent: RS.incident.Artifacts
glu.defView('RS.incident.Artifacts', {
	xtype: 'grid',
	itemId: 'playbook-artifacts',
	cls: 'rs-grid-dark',
	padding: 0,
	store: '@{artifactsStore}',
	multiSelect: true,
	name: 'artifacts',
	viewConfig: {
		stripeRows: true,
		enableTextSelection: true
	},
	dockedItems: [{
		xtype: 'toolbar',
		cls: 'rs-dockedtoolbar',
		items: [{
			xtype: 'button',
			disabled: '@{!..pSirInvestigationViewerArtifactsCreate}',
			name: 'addArtifact',
			text: '~~add~~',
			cls: 'rs-small-btn rs-btn-light'		
		}, {
			xtype: 'button',	
			name: 'removeArtifact',
			text: '~~remove~~',
			cls: 'rs-small-btn rs-btn-light'		
		}],
	}],
	plugins: [{
		ptype: 'rowexpander',
		pluginId: 'playbook-rowexpander',
		columnWidth: 32,
		selectRowOnExpand: true,
		rowBodyTpl : [
			'<div><b>Related SIR:</b> {sirList}</div>',
			'<div style=margin-top:5px;><b>Added By: </b>{sysCreatedBy} on {sysCreatedOn}</div>' +
			'<div style=margin-top:5px;><b>Updated By: </b>{sysUpdatedBy} on {sysUpdatedOn}</div>' +
			'<div style=margin-top:15px;word-wrap:break-word;><b>Key: </b>{name}</div>' +
			'<div style=margin-top:5px;word-wrap:break-word;><b>Value: </b>{encodedValue}</div>' +
			'<div style=margin-top:5px;word-wrap:break-word;><b>Description: </b>{encodedDescription}</div>'
		]
	}],
	columns: [
	{
		dataIndex: 'sysCreatedBy',
		text: '~~addedBy~~',
		flex: 1
	}, {
		dataIndex: 'name',
		text: '~~ushortName~~',
		flex: 1,
	}, {
		dataIndex: 'value',
		text: '~~value~~',
		flex: 3,
		renderer : function(value, metaData, record){
			var vm = this._vm;
			var id = Ext.id();
			Ext.defer(function () {
				Ext.widget('button', {
					renderTo: id,
					tooltip: vm.localize('lookupTootltip'),
					iconCls : 'icon-search',
					cls : ' sir-action-icon',
					handler: function(){
						vm.lookupIncidentByArtifact(record);
					}
				});
			}, 50);
			return Ext.String.format('<div class="sir-artifact-value">' + Ext.String.htmlEncode(value) + '<span style=float:right; id="{0}"></span></div>', id);
		}
	},{
		xtype: 'actioncolumn',		
		hideable: false,
		sortable: false,
		align: 'center',			
		width: 45,			
		items: [{
			glyph: 0xF0AE,
			tooltip : 'Action',			
			handler: function(view, rowIndex, colIndex, item, e, record) {
				this.up('grid').fireEvent('handleActionClick', this, record);
			}
		}]
	},{
		dataIndex: 'sysCreatedOn',
		text: '~~addedOn~~',
		flex: 1,
	}, {
		dataIndex: 'sysUpdatedOn',
		text: '~~updatedOn~~',
		flex: 1,
	},{
		text: RS.common.locale.sysId,
		dataIndex: 'id',
		hidden: true,
		autoWidth: true,
		sortable: false,
		filterable: false,
		groupable: false,
		width: 300
	}],
	listeners: {	
		activate: '@{tabInit}',
		handleActionClick : '@{actionHandler}'
	}
});
//Artifacts Modal
//Parent: RS.incident.Artifacts
glu.defView('RS.incident.ArtifactModal', {
	itemId: 'playbook-artifactmodal',
	title: '~~addArtifact~~',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	padding: 15,
	height: 450,
	width: 700,
	modal: true,
	cls : 'rs-modal-popup',
	defaults : {
		margin : '4 0'
	},
	items: [{
		xtype: 'combobox',
		store : '@{artifactTypeStore}',
		displayField : 'uname',
		valueField : 'uname',
		queryMode : 'local',		
		name: 'type',
		editable : false,
	},{
		xtype: 'combobox',
		store : '@{aliasStore}',		
		displayField : 'ushortName',
		valueField : 'ushortName',
		queryMode : 'local',		
		name: 'ushortName'
	},{
		xtype: 'textareafield',
		fieldLabel: '~~value~~',		
		flex : 1,
		name : 'value',
		renderer: function(field) {
			return field.replace(/(\r\n|\n|\r)/gm, "<br />")
		}
	}, {
		xtype: 'textareafield',
		fieldLabel: '~~description~~',
		flex : 1,
		value: '@{description}',
		renderer: function(field) {
			return field.replace(/(\r\n|\n|\r)/gm, "<br />")
		},
		margin : '4 0 10 0'
	}],
	buttons: [{
		name: 'saveArtifact',
		cls : 'rs-med-btn rs-btn-dark',
		disabled: '@{isSaving}'
	},{
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});
glu.defView('RS.incident.Attachments', {
	itemId: 'playbook-attachments',
	api: '@{api}',
	dockedItems: [{
		xtype: 'toolbar',
		name: 'actionBar',
		cls : 'rs-dockedtoolbar',
		items: [{
			xtype: 'button',
			disabled: '@{!..pSirInvestigationViewerAttachmentsCreate}',
			text: '~~upload~~',
			name: 'addAttachment',
			cls: 'rs-btn-light rs-small-btn',
			maxWidth: 80
		}, {
			xtype: 'button',
			name: 'deleteAttachment',
			cls: 'rs-btn-light rs-small-btn',
			maxWidth: 80,
			disabled: true,
		}, '->', {
			name: 'reload',
			tooltip: 'Reload',
			text: '',
			iconCls: 'x-tbar-loading',
		}]
	}],
	modifiable: '@{..pSirInvestigationViewerAttachmentsModify}',
	setModifiable: function(val) {
		this.down('#maliciousCheckBox').setDisabled(!val);
	},
	xtype: 'grid',
	cls : 'rs-grid-dark',
	name: 'attachments',
	layout : 'fit',
	plugins: [{
		ptype: 'cellediting',
		clicksToEdit: 1,
		listeners: {
			beforeedit: function(editor, context, eOpts) {
				if (!context.grid.modifiable) {
					return false;
				}
				return true;
			}
		}
	}],
	selModel: {
		selType: 'rowmodel'
	},
	store: '@{store}',
	columns: [{
		text: '~~name~~',
		flex: 3,
		sortable: false,
		dataIndex: 'name'
	}, {
		text: '~~description~~',
		flex: 3,
		sortable: false,
		dataIndex: 'description',
		renderer: RS.common.grid.plainRenderer(),
		editor: 'textfield'
	}, {
		text: '~~size~~',
		width: 90,
		sortable: true,
		renderer: Ext.util.Format.fileSize,
		dataIndex: 'size'
	}, {
		xtype: 'checkcolumn',
		itemId: 'maliciousCheckBox',
		text: '~~malicious~~',
		width: 90,
		sortable: true,
		dataIndex: 'malicious'
	}, {
		text: '~~uploadedOn~~',
		width: 200,
		sortable: true,
		dataIndex: 'sysCreatedOn',
		renderer: function(value) {
			var r = Ext.util.Format.dateRenderer(clientVM.getUserDefaultDateFormat());
			if (Ext.isString(value))
				return value;
			return r(value);
		}
	}, {
		text: '~~uploadedBy~~',
		sortable: true,
		flex: 1,
		dataIndex: 'sysCreatedBy',
	}, {
		xtype: 'actioncolumn',
		hideable: false,
		width: 38,
		items: [{
			icon: '/resolve/images/arrow_down.png',
			// Use a URL in the icon config
			tooltip: RS.common.locale.download,
			handler: function(gridView, rowIndex, colIndex) {
				var rec = gridView.getStore().getAt(rowIndex),
					api = gridView.ownerCt.api,
					sys_id = rec.get('id');

				if (sys_id) gridView.ownerCt.downloadURL(Ext.String.format(api.downloadViaDocName, sys_id, gridView.ownerCt._vm.csrftoken));
				else clientVM.displayError(RS.incident.locale.noDownloadBody)
			}
		}]
	}],
	downloadURL: function(url) {
		var iframe = document.getElementById("hiddenDownloader");
		if (iframe === null) {
			iframe = document.createElement('iframe');
			iframe.id = "hiddenDownloader";
			iframe.style.visibility = 'hidden';
			iframe.style.display = 'none';
			document.body.appendChild(iframe);
		}
		iframe.src = url;
	},
	listeners: {
		selectionchange: function(v, selected, eOpts) {
			this.fireEvent('selectionChanged', this, this, selected);
		},
		selectionChanged: '@{selectionChanged}',
		beforerender: function(v, eOpts) {
			this.fireEvent('initView', this, this);
		},
		initview: '@{initView}',
		activate: '@{tabInit}'
	}
});


//Parent: RS.incident.Playbook
glu.defView('RS.incident.AuditLog', {
	xtype: 'grid',
	cls: 'rs-grid-dark',
	itemId: 'playbook-auditlog',
	plugins: [{
		ptype: 'searchpager',
		allowPersistFilter: false,
		hideMenu: true,
		toolbarCls : 'rs-dockedtoolbar',
		buttons: [{
			xtype: 'button',
			text: 'Export',
			tooltip: 'Export to PDF, JSON or CSV',
			cls: 'rs-btn-light rs-small-btn',
			handler: function() {
				this.up('grid').fireEvent('exportauditlog');
			}
		}]
	}],
	store: '@{auditlogStore}',
	columns: [{
		dataIndex: 'sysCreatedBy',
		text: '~~user~~',
		sortable: true,
		filterable: true,
		flex: 1,
		initialShow: true
	}, {
		dataIndex: 'ipAddress',
		text: '~~IPAddress~~',
		sortable: true,
		filterable: true,
		flex: 1
	}, {
		dataIndex: 'description',
		text: '~~description~~',
		sortable: true,
		filterable: true,
		flex: 5
	}, {
		dataIndex: 'sysCreatedOn',
		text: '~~loggedOn~~',
		flex: 1,
		sortable: true,
		filterable: false,
		hidden: false,
		initialShow: true,
		renderer: function(field) {
			return Ext.util.Format.date(new Date(field), clientVM.getUserDefaultDateFormat());
		}
	}],
	listeners: {
		activate: '@{tabInit}',
		exportauditlog: '@{exportAuditlog}'
	}
});
glu.defView('RS.incident.AutomationResults',{
	layout : 'fit',
	dockedItems : [{
		xtype : 'toolbar',
		cls : 'rs-dockedtoolbar',
		items : [{
			xtype : 'combo',
			name : 'activityFilter',
			editable : false,
			labelWidth : 150,
			width : 800,		
			labelStyle : 'font-weight:bold',
			store : '@{..activityFilterStore}',
			queryMode : 'local',
			displayField : 'activityName',
			valueField : 'activityName',
			hidden: '@{isModal}',
		},'->',{		
			iconCls: 'rs-social-button icon-repeat',
			name : 'refresh',
			text : '',
			tooltip: 'Refresh',
		}]
	},{
		dock : 'bottom',
		xtype : 'toolbar',
		hidden: '@{!isModal}',
		items : [{
	        text : '~~close~~',
	        cls : 'rs-med-btn rs-btn-light',		
	        handler : '@{close}',
	    }]
	}],
	items : '@{resultList}',
	listeners : {
		activate: '@{tabInit}'
	},
	padding: '@{padding}',
	asWindow: {
		minHeight: 400,
		minWidth: 700,
		title: '@{modalTitle}',
		modal: '@{modal}',
		cls :'rs-modal-popup',
		listeners: {
			beforeshow: function() {
				this.setWidth(Math.round(Ext.getBody().getWidth() * 0.6));
				this.setHeight(Math.round(Ext.getBody().getHeight() * 0.6));
			}
		}
	}	
})
glu.defView('RS.incident.IncidentLookup',{
	xtype : 'container',
	padding : 15,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	items : [{
		xtype : 'displayfield',
		htmlEncode: false,	
		value : '@{lookupInfo}',	
	},{
		xtype : 'grid',
		flex : 1,
		dockedItems: [{
			xtype: 'toolbar',		
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',			
			items: []
		}],
		cls : 'rs-grid-dark',	
		plugins: [{
			ptype: 'pager'
		}],
		columns : [{
			dataIndex: 'sir',
			text: '~~id~~',
			sortable: true,
			filterable: true,
			flex:.9,
			renderer: function(value, meta, record) {
				if (value) {
					return Ext.String.format('<a target="_blank" class="sc-link" href="/resolve/jsp/rsclient.jsp{0}#RS.incident.Playbook/sir={1}">{2}</a>', 
						record.store.rootVM.rootcsrftoken, record.get('sir'), record.get('sir'));
				}
				return ''
			}
		},{
			dataIndex: 'playbook',
			text: '~~playbook~~',
			sortable: true,
			filterable: true,
			flex: 1.1			
		},{
			dataIndex: 'title',
			text: '~~title~~',
			sortable: true,
			filterable: true,
			flex: 1.2
		},{
			dataIndex: 'sourceSystem',
			text: '~~origin~~',
			sortable: true,
			filterable: true,
			hidden: true,
			flex: .8
		},{
			dataIndex: 'id',
			text: RS.common.locale.sysId,
			sortable: true,
			hidden: true,
			sortable: false,
			filterable: false,
			flex: .8
		},{
			dataIndex: 'investigationType',
			text: '~~type~~',
			sortable: true,
			filterable: true,
			flex: .8,
		},{
			dataIndex: 'severity',
			text: '~~severity~~',
			sortable: true,
			filterable: true,
			flex: .5
		},{
			dataIndex: 'status',
			text: '~~status~~',
			sortable: true,
			filterable: false,
			flex: .6,
		},{
			dataIndex: 'dueBy',
			text: '~~dueBy~~',
			sortable: true,
			filterable: true,
			flex: .6,
			renderer: //Ext.util.Format.dateRenderer('Y-m-d H:i:s')
			function(val) {
				return (val? Ext.util.Format.dateRenderer('Y-m-d')(new Date(val)): val);
			}
		},{
			dataIndex: 'owner', //display owner/ownerName
			text: '~~owner~~',
			sortable: true,
			filterable: true,
			flex: 1
		},{
			dataIndex: 'sysCreatedOn',
			text: '~~createdOn~~',
			renderer: function(value) {
				var formated = Ext.util.Format.date(new Date(value), clientVM.getUserDefaultDateFormat());
				return formated;
			},
			sortable: true,
			hidden: false,
			filterable: true,
			flex: 1
		},{
			dataIndex: 'closedOn',
			text: '~~closedOn~~',
			sortable: true,
			filterable: true,
			flex: 1,
			renderer: function(value) {
				return value ? Ext.util.Format.date(new Date(value), clientVM.getUserDefaultDateFormat()) : '';
			}
		},{
			dataIndex: 'closedBy',
			text: '~~closedBy~~',
			sortable: true,
			filterable: true,
			flex: 1
		}],
		store : '@{incidentStore}',
	}]
})
glu.defView('RS.incident.IncidentLookupPopup',{
	title : '@{title}',
	cls : 'rs-modal-popup',		
	width : 700,
	plugins : [{
		ptype: "headericons",
	  	index : 1,
        headerButtons : [
            {
                xtype: 'button',
                iconCls: 'icon-external-link',
                cls : 'sir-external-link-header-icon', 
                handler: function(){
                	var vm = this.up().up()._vm;
                	vm.expandResultInTab();
                },
                listeners : {
                	render : function(){
                		this.setTooltip(glu.localize('expandResultInTab', {ns : 'RS.incident'}));
                	}
                }               	              
            }
        ]
    }],
	padding : 15,
	items : [{
		xtype : 'component',
		html : '@{noRecordFoundText}',
		hidden : '@{!noRecordFound}'
	},{
		xtype : 'displayfield',
		value : '@{lookupInfo}',
		hidden : '@{noRecordFound}',
		htmlEncode: false
	},{
		xtype : 'grid',
		dockedItems: [{
			xtype: 'toolbar',		
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',			
			items: []
		}],
		cls : 'rs-grid-dark',
		hidden : '@{noRecordFound}',
		plugins: [{
			ptype: 'pager',
			showSysInfo : false
		}],
		columns : [{
			dataIndex : 'sir',
			text: '~~id~~',
			width : 150,
			renderer: function(value, meta, record) {
				if (value) {
					return Ext.String.format('<a target="_blank" class="sc-link" href="/resolve/jsp/rsclient.jsp?{0}#RS.incident.Playbook/sir={1}">{2}</a>', 
						record.store.rootVM.rootcsrftoken, record.get('sir'), record.get('sir'));
				}
				return ''
			}
		},{
			dataIndex : 'title',
			text: '~~title~~',
			flex : 1
		},{
			dataIndex : 'investigationType',
			text: '~~type~~',
			width : 150
		},{
			dataIndex : 'severity',
			text: '~~severity~~',
			align : 'center',
			width : 80
		},{
			dataIndex : 'status',
			text: '~~status~~',
			align : 'center',
			width : 80
		}],
		store : '@{incidentStore}',
	}]	
})
//Parent: RS.incident.Playbook
glu.defView('RS.incident.Notes', {
	xtype: 'grid',
	itemId: 'playbook-notes',
	cls: 'rs-grid-dark',
	padding: 0,
	store: '@{notesStore}',
	dockedItems: [{
		xtype: 'toolbar',
		cls: 'rs-dockedtoolbar',
		items: [{
			xtype: 'button',
			disabled: '@{!..pSirInvestigationViewerNotesCreate}',
			name: 'addNote',
			text: '~~add~~',
			cls: 'rs-small-btn rs-btn-light',
		}/*, {
			xtype: 'button',
			disabled: '@{!..pSirInvestigationViewerNotesModify}',
			name: 'editNote',
			text: '@{editButtonText}',
			cls: 'rs-small-btn rs-btn-light',
		}, {
			xtype: 'button',
			name: 'deleteNote',
			text: 'Delete',
			cls: 'rs-small-btn rs-btn-light',
			maxWidth: 80,
		}*/]
	}],
	plugins: [/*{
		ptype: 'searchpager',
		allowPersistFilter: false,
		hideMenu: true,
		toolbarCls : 'rs-dockedtoolbar'
	}, */{
		ptype: 'rowexpander',
		columnWidth: 32,
		rowBodyTpl : [
			'<div style=margin-top:0px;><span style=font-weight:bold;>Added By: </span>{sysCreatedBy} on {sysCreatedOn}</div>' +
			//'<div style=margin-top:5px;><span style=font-weight:bold;>Updated By: </span>{sysUpdatedBy} on {sysUpdatedOn}</div>' +
			'<div style=margin-top:10px;word-wrap:break-word;><span style=font-weight:bold;>Note: </span>{fullNote}</div>'
		]
	}],
	columns: [{
		dataIndex: 'activityName',
		text: '~~activity~~',
		flex: 1,
		renderer: function(value) {
			return (value === null) ? 'N/A' : value
		}
	}, {
		dataIndex: 'postedBy',
		text: '~~addedBy~~',
		flex: 1
	}, {
		dataIndex: 'note',
		text: '~~notePreview~~',
		flex: 4
	}, {
		dataIndex: 'sysCreatedOn',
		text: '~~addedOn~~',
		flex: 1
	}/*, {
		dataIndex: 'sysUpdatedOn',
		text: '~~updatedOn~~',
		flex: 1
	}*/],
	listeners: {
		itemclick: '@{selectNote}',
		activate: '@{tabInit}'
	}
});

//Notes Modal
//Parent: RS.incident.Notes
glu.defView('RS.incident.NoteModal', {
	itemId: 'playbook-notemodal',
	padding : 10,
	cls : 'rs-modal-popup',
	title: '@{returnTitle}',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	width: 600,
	modal: true,
	items: [{
		xtype: 'textareafield',
		hideLabel: true,
		height: 350,
		fieldStyle: {
			border: 0
		},
		name: 'note',
		renderer: function(field) {
			return field.replace(/(\r\n|\n|\r)/gm, "<br />")
		}
	}],
	buttons: [{
		name: 'saveNote',
		text: '@{saveButtonText}',
		cls : 'rs-med-btn rs-btn-dark',
		disabled: '@{isSaving}'
	},{
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
})
glu.defView('RS.incident.Overview', {
	itemId: 'playbook-overview',
	cls: 'playbook-overview',
	xtype: 'container',
	defaults: {
		margin: '0 10 0 0',
		padding: 10
	},
	items: [{
		//Summary
		itemId: 'playbook-summary',
		defaultType: 'displayfield',
		defaults: {
			labelWidth: 140,
			cls: 'rs-displayfield-value',
			margin: '0 0 5 0',
			fieldStyle: {
				fontWeight: 'bold'
			}
		},
		items: [{
			xtype: 'label',
			text: '~~summary~~',
			style: {
				fontWeight: 'bold'
			},
		}, {
			fieldLabel: '~~id~~',
			value: '@{..returnId}'
		}, {
			fieldLabel: '~~severity~~',
			value: '@{..returnSeverity}'
		}, {
			fieldLabel: '~~createdOn~~',
			value: '@{..returnDateCreated}'
		}, {
			fieldLabel: '~~type~~',
			value: '@{..returnType}',
			renderer: function(value) {
				if (value) {
					return value
				} else {
					return 'None'
				}
			}
		}, {
			xtype: 'combobox',
			fieldLabel: '~~dataCompromised~~',
			displayField: 'display',
			valueField: 'value',
			value: '@{..returnDataCompromised}',
			store: '@{dataCompromisedStore}',
			queryMode : 'local',
			editable: false,
			listeners: {
				change: '@{updateDataCompromised}'
			},
			readOnly: '@{readOnlyDataCompromised}'
		}, {
			xtype: 'combo',
			fieldLabel: '~~status~~',
			store: '@{statusStore}',
			displayField: 'display',
			valueField: 'value',
			queryMode : 'local',
			editable: false,
			value: '@{..returnStatus}',
			listeners: {
				change: '@{updateStatus}'
			},
			readOnly: '@{readOnlyStatus}'
		}]
	}, {
		//Team
		itemId: 'playbook-team',
		layout: {
			type: 'vbox',
		},
		items: [{
			xtype: 'label',
			text: '~~team~~',
			style: {
				fontWeight: 'bold'
			}
		}, {
			xtype: 'displayfield',
			fieldLabel: '~~owner~~',
			value: '@{owner}',
			labelWidth: 140,
			cls: 'rs-displayfield-value',
			fieldStyle: {
				fontWeight: 'bold'
			},
			margin: '0 0 5 0',
			renderer: function(value) {
				if (value) {
					return value
				} else {
					return 'None'
				}
			}
		}, {
			xtype: 'displayfield',
			fieldLabel: '~~createdBy~~',
			value: '@{..returnCreatedBy}',
			labelWidth: 140,
			cls: 'rs-displayfield-value',
			fieldStyle: {
				fontWeight: 'bold'
			},
			margin: '0 0 5 0'
		}, {
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [{
				width: 135,
				xtype: 'displayfield',
				cls: 'rs-displayfield-value',
				fieldStyle: {
					fontWeight: 'bold'
				},
				fieldLabel: '~~members~~'
			}, {
				xtype: 'component',
				layout: 'vbox',
				flex: 1,
				width: '100%',
				xtype: 'fieldset',
				defaults: {
					margin: '5 0 0 0'
				},
				maxHeight: 150,
				autoScroll: true,
				items: '@{membersList}',
				border: false,
			}]
		}, {
			xtype: 'button',
			cls: 'rs-small-btn rs-btn-light',
			height: 24,
			margin: '0 0 0 140',
			text: '~~addRemoveMember~~',
			handler: '@{addMember}',
			disabled: '@{readOnlyTeamMebers}'
		}]
	}, {
		itemId: 'playbook-relatedinvestigations',
		layout: {
			type: 'vbox',
		},
		defaultType: 'displayfield',
		defaults: {
			labelWidth: 140,
			cls: 'rs-displayfield-value',
			margin: '0 0 5 0',
			fieldStyle: {
				fontWeight: 'bold'
			},
		},
		items: [{
			xtype: 'label',
			text: '~~relatedInvestigations~~',
			style: {
				fontWeight: 'bold'
			}
		}, {
			fieldLabel: '~~referenceId~~',
			value: '@{referenceId}',
			hidden: '@{referenceIdIsBlank}'
		}, {
			fieldLabel: '~~alertId~~',
			value: '@{alertId}',
			hidden: '@{alertIdIsBlank}'
		}, {
			fieldLabel: '~~correlationId~~',
			value: '@{correlationId}',
			hidden: '@{correlationIdIsBlank}'
		}, {
			xtype: 'label',
			text: '~~none~~',
			hidden: '@{noRelatedIsVisible}'
		}]
	}]
});


glu.defView('RS.incident.MemberModal', {
	itemId: 'playbook-addmember',
	width: 700,
	padding: 15,
	modal : true,
	cls : 'rs-modal-popup',
	title: '~~manageMembersModal~~',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	items: [{
		xtype: 'combobox',
		fieldLabel: '~~owner~~',
		store: '@{..securityGroupStore}',
		queryMode: 'local',
		displayField: 'userId',
		valueField: 'userId',
		value: '@{ownerSelection}',
		queryMode : 'local',
		editable: false,
	}, {
		xtype: 'displayfield',
		fieldLabel: '~~members~~',
		margin: '5 0 0 0'
	}, {
		xtype: 'grid',
		cls : 'rs-grid-dark',
		maxHeight: 500,
		margin : '0 0 10 0',
		viewConfig: {
			markDirty: false
		},
		store: '@{selectedMembersStore}',
		name : 'member',
		selType:'checkboxmodel',
    selModel : {
        mode : 'simple'
    },
		columns: [
			{text: '~~userId~~', dataIndex: 'userId', flex: 1,},
			{text: '~~displayName~~', dataIndex: 'displayName', flex: 1,}
		],
		listeners : {
			afterrender : function(grid) {
				grid._vm.on('selectMember', function(selectedMember) {
					for (var i = 0; i < selectedMember.length; i++){
					    grid.getSelectionModel().select(selectedMember[i], true);
					}
				})
				grid._vm.populateList();
			},
		},
	}],

	buttons: [{
		name: 'saveMembers',
		cls : 'rs-med-btn rs-btn-dark'
	}, {
		name: 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]
});

//Allow for member tooltips
glu.defView('RS.incident.MemberLabel', {
	xtype: 'label',
	//itemId: '@{elementId}',
	userId: '@{userId}',
	text: '@{userId}',
	memberName : '@{memberName}',
	style: {
		fontWeight: 'bold'
	},
	listeners : {
		afterrender : function(panel) {
			var tooltipUserId = (this.userId.indexOf('(Owner)')) ? this.userId.slice(0, -7) : this.userId;
			Ext.QuickTips.register({
				target: this.getEl(),
				text: 'Name: ' + this.memberName
			});
		}
	}
})
// This module renders Playbook builder's content and can be extended to render wiki page builder content
Ext.define('RS.incident.PageViewer', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.pageviewer',

	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	overflowY: 'auto',
	csrftoken: '',

	initComponent: function() {
		this.callParent(arguments);
		clientVM.getCSRFToken_ForURI('/resolve/service/wiki/download', function(token_pair) {
			this.csrftoken = token_pair[0] + '=' + token_pair[1];
		}.bind(this));
	},

	renderContent: function(v, content, ufullName) {
		var re = /(\{section[^\}]*\})([\s\S]*?)\{section\}/gmi, match, secProp, viewConfigs = [], secHeight, config;
		var fullName = ufullName.split('.');
		while (match = re.exec(content)) {
			secProp = this.getProperties(match[1]);
			config = {
				xtype: 'panel',
				cls: 'playbook-desc-view',
				width: '100%',
				layout:{
					type: 'hbox',
					align: 'stretch'
				},
				items: []
			};
			if (secProp.height) {
				config.height = (secProp.height.search('%') == -1)? parseInt(secProp.height): secProp.height;
			} else {
				config.flex = 1;
			}
			// START TEST
			if ((match[1].search('procedure') != -1) || (match[1].search('result') != -1)) {
				// Skip procedure and result sections
				continue;
			}
			// END TEST
			this.renderSection(config, match[2], fullName);
			viewConfigs.push(config);
		}
		if (viewConfigs) {
			v.removeAll();
			v.add(viewConfigs);
		}
	},

	renderSection: function(secConfigs, sec, fullName) {
		var re = /(<td.*class="wiki-builder">)([\s\S]*?)<\/td class="wiki-builder">/gmi, match, prop, content, table;
		var tableTagRegex = new RegExp(RS.wiki.pagebuilder.Constant.TABLETAG, 'gmi');
		if (table = tableTagRegex.exec(sec)) {
			while(match = re.exec(sec)) {
				this.renderComponent(secConfigs, match[2], match[1], fullName);
			}
		} else {
			this.renderComponent(secConfigs, sec, null, fullName);
		}
	},

	renderComponent: function(secConfigs, comContent, tdTag, fullName) {
		var content, tdTagConfigs;
		if (tdTag) {
			tdTagConfigs = this.getProperties(tdTag);
		}
		if(content = /<novelocity.*>([\s\S]*)<\/novelocity>/gmi.exec(comContent)) {
			this.renderTxtComponent(secConfigs, tdTagConfigs, content[1], fullName);
		} else if (content = /(\{image[^\}]*\})/gmi.exec(comContent)) {
			this.renderImgComponent(secConfigs, tdTagConfigs, content[1], fullName);
		}
	},

	getComTemplate: function(type, content, fullName) {
		var item, t, url;
		if (type == 'image') {
			item = {
				xtype: 'image',
				src: Ext.String.format('/resolve/service/wiki/download?wiki={0}.{1}&attachFileName={2}&{3}', fullName[0], fullName[1], content, this.csrftoken),
				style: 'border:0px solid black;'
			}
		} else if (type == 'texteditor') {
			item = {
				xtype: 'panel',
				html: content,
				flex: 1
			}
		}
		t = {
				xtype: 'panel',
				layout: 'auto',
				items: [item]
		};
		return t;
	},

	renderTxtComponent: function(secConfigs, colConfigs, content, fullName) {
		var txtViewConfig =  this.getComTemplate('texteditor', content, fullName);
		if (colConfigs && colConfigs.style) {
			var style = this.extractColumnStyleInfo(colConfigs.style);
			txtViewConfig.flex = (parseInt(style.width)/100)*100;
			txtViewConfig.layout = {
				type: 'hbox',
				align: style['vertical-align']
			}
		} else {
			txtViewConfig.flex = 1;
		}
		secConfigs.items.push(txtViewConfig);
	},

	renderImgComponent: function(secConfigs, colConfigs, content, fullName) {
		var img = content.split('|')[0].split(':')[1].split('}')[0],
			imgProp = this.getProperties(content),
			imgViewConfig =  this.getComTemplate('image', img, fullName),
			item = imgViewConfig.items[0];
		if (colConfigs && colConfigs.style) {
			var style = this.extractColumnStyleInfo(colConfigs.style);
			imgViewConfig.flex = (parseInt(style.width)/100)*100;
			imgViewConfig.layout = {
				type: 'hbox',
				align: style['vertical-align']
			}
		} else {
			imgViewConfig.flex = 1;
		}

		if(!imgProp || !imgProp.width || !imgProp.height
			|| typeof(imgProp.width) == 'string' || typeof(imgProp.height) == 'string') {
			item.listeners = {
				afterrender: function(v) {
					var img = new Image();
					img.src = item.src;
					img.onload = function() {
						var setHeightOrWidth = function(v, unit, actualHeightOrWidth, type) {
							if (unit.search('%') != -1) {
								// Set as percentage
								v['set'+type].bind(v)(actualHeightOrWidth*parseInt((unit))/100);
							} else {
								// Set actual size
								v['set'+type].bind(v)(parseInt(unit));
							}
						}
						if (!imgProp || (imgProp && !imgProp.height && !imgProp.width)) {
							// Neither height and width is configured; set natural size
							v.setHeight(img.naturalHeight);
							v.setWidth(img.naturalWidth);
						} else if (imgProp && imgProp.height && imgProp.width) {
							// Both height and width are configured
							setHeightOrWidth(v, imgProp.height, img.naturalHeight, 'Height');
							setHeightOrWidth(v, imgProp.width, img.naturalWidth, 'Width');
						} else if (imgProp && imgProp.height && !imgProp.width) {
							// Only height is configured
							setHeightOrWidth(v, imgProp.height, img.naturalHeight, 'Height');
							v.setWidth(img.naturalWidth);
						} else if (imgProp && !imgProp.height && imgProp.width) {
							// Only width is configured
							v.setHeight(img.naturalHeight);
							setHeightOrWidth(v, imgProp.width, img.naturalWidth, 'Width');
						}
					}
				}
			}
		}
		secConfigs.items.push(imgViewConfig);
	},

	extractColumnStyleInfo: function(style) {
		var match = /width:([^;]+);vertical-align:([^;]+);/gi.exec(style)
		return {
			'width': match[1],
			'vertical-align': match[2]
		};
	},

	getProperties: function(el) {
		var prop = {};
		var match,
			re = new RegExp('([a-z]+)+=[\'"]*([^\|\}\'"]*)[\'"]*', 'gi');
		while (match = re.exec(el)) {
			prop[match[1]] = match[2];
		}
		return prop;
	}
});

glu.defView('RS.incident.dummy') //to make sure the ns is init before creating the factory
RS.incident.views.basePlaybookFactory = function(actualView) {
  var output = {
	itemId: 'playbook-viewport',
	overflowY: 'auto',
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	bodyPadding : 15,
	ticketInfo: '@{ticketInfo}',
	cls: 'playbook-main',
	items: [{
		xtype: '@{overviewComponent}',
		width: 330,
		hidden: '@{..isReadOnly}',
	}, {
		xtype: 'container',
		layout: 'anchor',
		flex: 1,
		items: [{
			anchor: '100% 25%',
			padding: 10,
			style: {
				border: '1px silver solid',
			},
			layout: {
				type: 'vbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'label',
				text: '~~description~~',
				style: {
					fontWeight: 'bold'
				}
			}, {
				//Description
				itemId: 'playbook-description',
				xtype: 'pageviewer',
				ucontent: '@{ticketInfo}',
				padding: '4px 0px 0px 0px',
				flex: 1,
				cls: 'rs-playbook-description',

				bodyStyle: {
					background: 'none'
				},
				listeners: {
					afterrender: function() {
						var me = this;
						this.body.on('dblclick', function() {
							me._vm.expandDescription();
						});
						this._vm.on('loadDescription', function(ucontent, fullName) {
							this.renderContent(this, ucontent, fullName);
						}, this);
						Ext.QuickTips.register({
							target: this.getEl(),
							text: me._vm.localize('doubleClickExpand')
						});
					}
				},
			}]
		}, {
			xtype: 'tabpanel',
			anchor: '100% 75%',
			itemId: 'playbook-tabpanel',
			cls: 'playbook-tabpanel rs-tabpanel-dark',
			margin: '10 0 0 0',
			header: false,
			activeTab: '@{activeTab}',
			defaults: {
				overflowY: 'auto',			
				disabled: '@{..isPreview}',
				hidden: '@{..isReadOnly}'
			},
			items: [{
				title: '~~activitiesTab~~',
				itemId: 'activitiesTab',
				xtype: '@{activitiesTab}',
				disabled: false,
				hidden: false
			}, {
				title: '~~notesTab~~',
				itemId: 'notesTab',
				xtype: '@{notesTab}'
			}, {
				title: '~~artifactsTab~~',
				itemId: 'artifactsTab',
				xtype: '@{artifactsTab}'
			}, {
				title: '~~attachmentsTab~~',
				itemId: 'attachmentsTab',
				xtype: '@{attachmentsTab}'
			}, {
				title: '~~automationresultsTab~~',
				itemId: 'automationresultsTab',
				xtype: '@{automationresultsTab}'
			}, {
				title: '~~auditlogTab~~',
				itemId: 'auditlogTab',
				xtype: '@{auditlogTab}'
			},  {
				title: '~~siemdataTab~~',
				itemId: 'siemdataTab',
				xtype: '@{siemdataTab}'
			}/*, {
				title: '~~relatedinvestigationsTab~~',
				itemId: 'relatedinvestigationsTab',
				xtype: '@{relatedinvestigationsTab}'
			}, {
				title: 'React',
				id: 'reactroot',
				listeners: {
					afterrender: function() {
						Ext.Loader.loadScript('/resolve/js/build.js');
					}
				}
			}*/]
		}]
	}],
	listeners: {
		afterrender: function(panel) {
			function beforedestroyHandler(){
				panel.fireEvent('beforedestroy');
			};
			document.addEventListener("beforedestroy", beforedestroyHandler);
			panel._vm.on("removeEventReference", function(){
				document.removeEventListener("beforedestroy", beforedestroyHandler);
			})
		},
		beforedestroy : '@{beforeDestroyComponent}'
	},
	activitiesModifiable: '@{pSirInvestigationViewerActivitiesModify}',
	setActivitiesModifiable: function(value) {
		var activities = this.down('#playbook-activitiesgrid');
		if (activities) {
			activities.modifiable = value;
		}
	}
  }

  Ext.apply(output, actualView.propertyPanel);
  return output;
}

glu.defView('RS.incident.Playbook', {
	parentLayout: 'basePlaybook',
	propertyPanel: {
		dockedItems: [{
			xtype : 'toolbar',
			margin : '15 15 0 0',
			items : [{
				xtype: 'tbtext',
				cls: 'rs-display-name',
				text: '@{returnTitle}',
			},'->', {
				cls : 'rs-small-btn rs-btn-light',
				name : 'exportPDF'
			}]
		}]
	}
})

//Parent: RS.incident.Playbook
glu.defView('RS.incident.DescriptionModal', {
	itemId: 'playbook-descriptionmodal',
	title: '~~playbookDescription~~',
	height: '75%',
	width: '75%',
	modal: true,
	cls : 'rs-modal-popup',
	bodyPadding: 10,
	overflowY: 'auto',
	items: [{
		//Description
		itemId: 'playbook-description',
		xtype: 'pageviewer',
		ucontent: '@{..ticketInfo}',
		flex: 1,
		cls: 'rs-playbook-description',
		bodyStyle: {
			background: 'none'
		},
		listeners: {
			afterrender: function() {
				this._vm.on('loadDescriptionModal', function(ucontent, fullName) {
					this.renderContent(this, ucontent, fullName);
				}, this);
			}
		}
	}]
});

glu.defView('RS.incident.SIEMData', {
	xtype: 'container',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	itemId: 'playbook-siemdata',
	style: {
		border: '1px silver solid'
	},
	defaults: {
		margin: 4
	},
	items: [{
		xtype: 'label',
		style: {
			fontWeight: 'bold',
		},
		text: '~~rawData~~'
	}, {
		xtype: 'text',
		text: '@{data}'
	}],
	listeners: {
		activate: '@{tabInit}'
	}
});
glu.defView('RS.incident.UploadAttachment', {
	parentLayout: 'FileUploadManager',
	propertyPanel: {
		listeners: {
			beforerender : function(){
				var me = this;
				var toolbar = this.down('button[name="uploadFile"]').ownerCt;
				var deleteBtn = this.down('button[name="delete"]');
				var reloadBtn = this.down('button[name="reload"]');
				deleteBtn.hide();
				reloadBtn.hide();
				
				var attachments_grid = this.down('#attachments_grid');
				attachments_grid.hide();

				var items = this.down('#fineUploaderTemplate').ownerCt;
				var fineuploader = this.down('#fineUploaderTemplate');

				items.add([{
					layout: {
						type: 'vbox',
						align: 'stretch'
					},
					margin: '4 0',
					items: [{
						xtype: 'textarea',
						name: 'description',
						fieldLabel: RS.incident.locale.description,
						rows: 5,
						//grow: true,
						//growMax: 60,
						emptyText: RS.incident.locale.emptyAttachmentDescription,
						listeners: {
							blur: function() {
								me.fireEvent('descriptionchanged', this, this.value);
							}
						}
					}, {
						xtype: 'checkbox',
						name: 'malicious',
						hideLabel: true,
						boxLabel: RS.incident.locale.malicious,
						margin: '0 0 0 104',
						handler: function() {
							me.fireEvent('maliciouschanged', this, this.checked);
						}
					}]
				}]);

				var buttons = this.down('#cancelBnt').ownerCt;
				buttons.insert(0, {
					name: 'submit',
					text: RS.incident.locale.submit,
					cls: 'rs-med-btn rs-btn-dark',
					handler: function() {
						me.fireEvent('submit', this)
					}
				});

				this.on({
					render: function() {
						this.down('button[name="submit"]').disable();
					},
					enableSubmitBtn: function() {
						this.down('button[name="submit"]').enable();
					},
					disableSubmitBtn: function() {
						this.down('button[name="submit"]').disable();
					},
					afterrender: function() {
						 this.fireEvent('afterRendered', this, this);
					}
				});
			},
			submit: '@{submit}',
			afterRendered: '@{viewRenderCompleted}',
			maliciouschanged: '@{maliciousChanged}',
			descriptionchanged: '@{descriptionChanged}',
		}
	}
});

glu.defView('RS.incident.dummy');
RS.incident.views.FileUploadManagerFactory = function(subView) {
	var config = RS.common.views.FileUploadManagerFactory();
	if (subView && subView.propertyPanel) {
		Ext.applyIf(config, subView.propertyPanel);
	}
	return config;
};

/*
glu.defView('RS.incident.UploadAttachment', {
    xtype: 'panel',
	incidentId: '@{incidentId}',
    getConfig: function() {
        return {
            autoStart: false,
            url: '/resolve/service/playbook/attachment/upload',
            browse_button: this.getBrowseButton(),
            drop_element: this.getDropArea(),
            container: this.getContainer(),
            chunk_size: null,
			max_file_size: '2020mb',
            runtimes: "html5,flash,silverlight,html4",
            flash_swf_url: '/resolve/js/plupload/Moxie.swf',
            unique_names: true,
            multipart: true,
            multipart_params: {},
            multi_selection: true,
            required_features: null,
            ownerCt: this
        }
    },
    getBrowseButton: function() {
        //return this.up().down('#browse').btnEl.dom.id;
		return this.down('#browse').getEl().dom.id;
    },
    getDropArea: function() {
        return this.down('#dropzone').getEl().dom.id;
    },
    getContainer: function() {
        var btn = this.down('#browse');
        return btn.ownerCt.id;
    },
    uploaderListeners: {
        beforestart: function(uploader, data) {
			//console.log('beforestart');
			this.owner.fireEvent('beforeStart', this.owner, uploader, data);
        },
        // uploadready: function(uploader, data) {
        //     var me = uploader.uploaderConfig.ownerCt;
        //     var inputs = Ext.query('#' + me.getEl().id + ' input[type="file"]');
        //     var file = inputs[0];
        //     var runtime = mOxie.Runtime.getRuntime(file.id);
        //     me.fireEvent('runtimedecided', null, runtime);
        // },
        uploadstarted: function(uploader, data) {},
        uploadcomplete: function(uploader, data) {
			//console.log('uploadcomplete')
            this.owner.fireEvent('uploadComplete', this.owner, uploader, data);
        },
        uploaderror: function(uploader, data) {
			//console.log('uploaderror')
            this.owner.fireEvent('uploadError', this.owner, uploader, data);
        },
        filesadded: function(uploader, data) {
			//console.log('filesadded');
            this.owner.fireEvent('filesAdded', this.owner, uploader, data);
        },
        beforeupload: function(uploader, data) {
            //console.log('beforeupload')
        },
        fileuploaded: function(uploader, data, response){
			this.owner.fireEvent('fileUploaded', this.owner, uploader, data, response);
		},
        uploadprogress: function(uploader, file, name, size, percent) {
            //this.owner.percent = file.percent;
			this.owner.fireEvent('uploadProgress', this.owner, name, size, percent);
        },
		uploadstopped: function(uploader, data) {
            //console.log('uploadstopped')
        },
        storeempty: function(uploader, data) {
            //console.log('storeempty')
        }
    },

    listeners: {
        uploadError: '@{uploadError}',
        filesAdded: '@{filesAdded}',
        uploadComplete: '@{uploadComplete}',
        runtimedecided: '@{runtimeDecided}',
        afterrender: function() {
			this.fireEvent('afterRendered', this, this);
            this.myUploader = Ext.create('RS.common.SinglePlupUpload', this);
            this.uploader = this.getConfig();
            this.myUploader.initialize(this);
        },
		afterRendered: '@{viewRenderCompleted}',
		beforeStart: '@{beforeStart}',
		startUpload: function(malicious, description) {
			Ext.apply(this.uploader.multipart_params, {
				docFullName: this.incidentId,
				malicious: malicious,
				description: description
            })
			this.myUploader.start();
		},
		uploadProgress: '@{uploadProgress}',
		cancelUpload: function() {
			this.myUploader.stop();
		},
		clearQueue: function() {
			this.myUploader.clearQueue();
		}
    },
  
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
	overflowY: 'auto',
    defaults : {
        margin : '4 0'
    },
    items: [{
		html: '@{busyText}',
		hidden: '@{!uploading}'
	}, {
		hidden: '@{uploading}',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			layout: {
				type: 'hbox',
				//align: 'stretch'
			},
			items: [{
				xtype: 'textarea',
				name: 'filenames',
				emptyText: '~~emptyFilename~~',
				flex: 4,
				readOnly: true,
				grow: true,
				growMin: 24,
				growPad: -8,
				growAppend: '',
				growMax: 90,
			}, {
				xtype: 'button',
				name: 'browse',
				itemId: 'browse',
				flex: 1,
				margin: '0 4',
				text: '~~browse~~',
				listeners: {
					hide: function() {
						this.up().el.query('.moxie-shim')[0].style.width = '0px';
					},
					show: function() {
						this.up().el.query('.moxie-shim')[0].style.width = '100%'
					}
				}
			}]
		}, {
			xtype: 'textarea',
			name: 'description',
			rows: 2,
			grow: true,
			growMax: 90,
			emptyText: '~~emptyAttachmentDescription~~'
		}, {
			xtype: 'checkbox',
			hideLabel: true,
			boxLabel: '~~malicious~~',
			value: '@{malicious}',
			margin: '0 0 0 104'
		}, {
			xtype: 'form',
			itemId: 'dropzone',
			style: 'border: 2px dashed #ccc;margin: 10px 0px 0px 0px',
			flex: 1,
			height: 155,
			margin : '4 0',
			layout: {
				type: 'vbox',
				pack: 'center',
				align: 'center'
			},
			items: [{
				xtype: 'label',
				text: '~~dragHere~~',
				style: {
					'font-size': '30px',
					'color': '#CCCCCC'
				}
			}]
		}]
	}],

    asWindow: {
        width: 600,
        height: 400,
        padding : 15,
        title: '~~addAttachment~~',
        cls : 'rs-modal-popup',
        modal: true,
        closable: false
    },  
    buttons: [{
        name : 'submit',
        cls : 'rs-med-btn rs-btn-dark',
		hidden: '@{uploading}',
    }, {
        name : 'cancel',
        cls : 'rs-med-btn rs-btn-light',
		hidden: '@{!uploading}'
    }, {
        name :'close',
        cls : 'rs-med-btn rs-btn-light',
		disabled: '@{uploading}'
	}]
})
*/

Ext.define('RS.incident.PlaybookTemplate.Spinnerfield', {
	extend: 'Ext.form.field.Spinner',
	alias: 'widget.rsspinnerfield',
	maxVal: Number.MAX_VALUE,
	value: 0,
	step: 1,
	allowBlank: false,
	initComponent: function() {
		var me = this;
		this.listeners = {
			spinUp: function(spinner) {
				var value = parseInt(spinner.getValue() || 0);
				if (value < me.maxVal) {
					spinner.setValue((value + 1).toString());
				}
			},
			spinDown: function(spinner) {
				var value = parseInt(spinner.getValue() || 0);
				if (value > 0) {
					spinner.setValue((value - 1).toString());
				}
			},
			change: function(spinner, newVal) {
				var value = parseInt(newVal) || 0;
				if (value < 0) {
					spinner.setValue('0');
				} else if (value > me.maxVal) {
					spinner.setValue(me.maxVal);
				} else {
					spinner.setValue(value);
				}
				var label = this.up().down('label[name='+this.name+'label]');
				label.setText(this.name.slice(0, (value==1)? this.name.length-1: undefined)); // i.e. 1 hour/2 hours
				this.up().fireEvent('slachanged');
			}
		};
		this.callParent(arguments);
	}
});

Ext.define('RS.incident.PlaybookTemplate.Numberfield', {
	extend: 'Ext.form.FieldContainer',
	alias: 'widget.rsnumberfield',
	defaults: {
		flex: 1
	},
	layout: {
		type: 'hbox',
		align: 'stretch'
	},
	paddingBetweenUnit: 0,
	validateOnChange: false,
	initComponent: function() {
		var hideHour = false;
		var me = this;
		var hideMinute = false;
		if (this.precision == 'day') {
			hideHour = hideMinute = true;
		} else if (this.precision == 'hour') {
			hideMinute = true;
		}
		var padding = '5 ' + this.paddingBetweenUnit + ' 0 5';
		this.items = [{
			xtype: 'rsspinnerfield',
			name: 'days'
		}, {
			xtype: 'label',
			name: 'dayslabel',
			text: 'days',
			padding: padding
		}, {
			xtype: 'rsspinnerfield',
			name: 'hours',
			hidden: hideHour,
			maxVal: 23
		}, {
			xtype: 'label',
			name: 'hourslabel',
			text: 'hours',
			hidden: hideHour,
			padding: padding
		}, {
			xtype: 'rsspinnerfield',
			name: 'minutes',
			regex: /[0-9]+/,
			hidden: hideMinute,
			maxVal: 59
		}, {
			xtype: 'label',
			name: 'minuteslabel',
			text: 'minutes',
			hidden: hideMinute,
			padding: padding
		}];
		this.callParent(arguments);
	}
});

Ext.define('RS.incident.store.RequiredStates', {
	extend: 'Ext.data.Store',
	alias: 'store.requiredstates',
	fields: ['name', 'value']
});

glu.defView('RS.incident.PlaybookTemplate', {
	cls: 'playbooktemplate',
	hidden: '@{!pSirPlaybookTemplatesTemplateBuilderChange}',
	itemId: 'playbookTemplate',
	padding : '15 0',
	layout : 'fit',
	dockedItems : [{
		xtype: 'tbtext',
		cls: 'rs-display-name',
		text: '@{wikiTitle}',
		margin : '0 0 15 0',
		padding : '0 20',
	},{
		xtype : 'toolbar',
		padding : '0 15',
		cls : 'rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items : [{
			iconCls: 'rs-icon-button icon-file-text',
			text: '~~viewFileOptions~~',
			menu: [{
				name: 'save',
				width : 200
			}, 'saveAs', 'copy', {
				xtype: 'menuseparator',
			}, 'preview', {
				xtype: 'menuseparator',
			}, {
				name: 'source', hidden: true
			}, {
				name: 'exitEdit'
			}, {
				name: 'returnToTemplates'
			}]
		},
		'->', {
			iconCls: 'rs-icon-button icon-plus-sign-alt',
			name: 'addLayoutSection',
			text: '~~section~~',
			cls : 'rs-small-btn rs-btn-light'
		}, {
			xtype: 'tbseparator'
		}, {
			name: 'preview',
			iconCls: 'rs-social-button icon-eye-open',
			text: '',
			tooltip: '~~preview~~'
		}, {
			name: 'save',
			iconCls: 'rs-social-button icon-save',
			text: '',
			tooltip: '~~save~~'
		}, {
			name: 'exitEdit',
			iconCls: 'rs-social-button icon-reply-all',
			text: '',
			tooltip: '~~exitEdit~~'
		}, {
			iconCls: 'rs-social-button icon-repeat',
			name: 'reload',
			text: '',
			tooltip: '~~reload~~'
		}]
	}],
	items: [{
		overflowY: 'scroll',
		flex : 1,
		style : 'border-top:1px solid silver',
		bodyPadding : '8 0 8 15',
		items: [{
			xtype: '@{sectionsLayout}',
			itemId : 'section-layout',
			listeners : {
				afterrender : function(panel){
					//Fired from sectionLayout
					this._vm.on('new_section_added', function(){
						//Lazy way to do scroll.
						setTimeout(function(){
							var d = panel.ownerCt.body.dom;
							var distance = d.scrollHeight - d.offsetHeight;
							panel.ownerCt.body.scroll('b',distance,true);
						},0);
					})
				}
			}
		}, {
			xtype : 'tabpanel',
			cls : 'rs-tabpanel-dark',
			style: 'border: #b5b8c8 1px solid;',
			padding : 15,
			defaults : {
				minHeight : 350,
				flex : 1,
			},
			items : [{
				xtype: 'panel',
				title: '~~procedure~~',
				layout: {
					type : 'hbox',
					align : 'stretch'
				},
				items: [{
					xtype: 'treepanel',
					cls : 'rs-grid-dark',
					flex: 2,
					margin : '0 15 0 0',
					useArrows: true,
					rootVisible: false,
					itemId: 'activityGrid',
					store: '@{procedures}',
					dockedItems: [{
						xtype: 'toolbar',
						dock: 'top',
						padding: '0 0 6 10',
						items: [{
							xtype: 'form',
							itemId: 'sla',
							layout: 'fit',
							width: 400,
							items: [{
								xtype: 'rsnumberfield',
								fieldLabel: '~~sla~~',
								labelWidth: 60,
								precision: 'hour',
								validateOnChange: true,
								listeners: {
									slachanged: function() {
										this.fireEvent('templateslachanged', this, this.up().getForm().getFieldValues());
									},
									templateslachanged: '@{templateSlaChanged}'
								}
							}]
						}, {
							xtype: 'tbtext',
							hidden: '@{!templateSlaWarning}',
							htmlEncode: false,
							padding: '2 0 0 0',
							text: '@{templateSlaWarning}'
						}]
					}],
					plugins: [{
						ptype: 'cellediting',
						clicksToEdit: 2,
						listeners: {
							beforeedit: function(editor, context, eOpts) {
								if (context.record.get('leaf')) {
									context.grid.editingContext = context;
								} else {
									return false;
								}
							}
						}
					}],
					multiSelect: true,
					viewConfig: {
						markDirty: false,
						plugins: {
							ptype: 'treeviewdragdrop'
						},
						listeners: {
							beforedrop: function(node, data, overModel, dropPosition, dropHandlers, eOpts) {
								var r = data.records[0];
								if (dropPosition == 'append') {
									if (!r.isLeaf()) // Nested structure is not allowed
										return false;
								} else if (dropPosition == 'before' || dropPosition == 'after') {
									if (r.isLeaf() && !overModel.isLeaf()) // Moving out of parent is not allowed
										return false;
									if (!r.isLeaf() && overModel.isLeaf()) // Nested structure is not allowed
										return false;
								}
								return true;
							}
						}
					},
					columns: [{
						xtype: 'treecolumn',
						text: '~~phaseActivity~~',
						dataIndex: 'activityName',
						hideable: false,
						sortable: false,
						flex: 2,
						editor: {
							xtype: 'phaseeditor',
							validator: function (val) {
								var context = this.up('treepanel').editingContext;
								if (!context.record.isLeaf() && val.trim() == '') {
									return 'Phase can\'t be empty';
								}
								this.up('#playbookTemplate').fireEvent('validateeditprocedure', this, context, val);
								return context.dupPhaseFound? 'Duplicate phase name found': true;
							}
						}
					}, {
						header: '~~runbook~~',
						dataIndex: 'wikiName',
						hideable: false,
						sortable: false,
						flex: 4,
						renderer: function(value, meta, record) {
							if (value) {
								return Ext.String.format('<a target="_blank" class="sc-link" href="/resolve/jsp/rsclient.jsp?{0}#RS.wiki.Main/name={1}">{2}</a>',
								record.store.treeStore.rootVM.csrftoken, record.get('wikiName'), value);
							}
							return '';
						},
						editor: {
							xtype: 'documentfield',
							name: 'documentName',
							value: '@{..documentName}',
							store: '@{..documentStore}',
							labelWidth: 0,
							labelSeparator: '&nbsp',
							allowBlank: false,
							inline: true,   // inline cell editing
							displayField: 'ufullname',
							valueField: 'ufullname',
							editable: false,
							listeners: {
								searchdocument: '@{..searchRunbook}'
							}
						}
					}, {
						header: '~~required~~',
						dataIndex: 'isRequired',
						flex: 1.2,
						filterable: true,
						renderer:  function(value, meta, record) { //,
							if (value === false || value === 'false' || !record.isLeaf()) {
							return '';
							}
							return '<span class="icon-large icon-ok-sign rs-boolean-renderer"></span>'
						},
						align: 'center',
						editor: {
							xtype: 'combobox',
							editable: false,
							queryMode: 'local',
							store: { type: 'requiredstates', data: [{name: 'Yes', value: true}, {name: 'No', value: false}] },
							displayField: 'name',
							valueField: 'value'
						},
					}, {
						header: '~~sla~~',
						dataIndex: 'days',
						renderer: function(value, meta, record) {
							var days = parseInt(record.getData().days);
							var hours = parseInt(record.getData().hours);
							var minutes = parseInt(record.getData().minutes);
							var daysText = '';
							var hoursText = '';
							var minutesText = '';
							if (days === 1) {
								daysText = 'Day';
							} else if (days > 1) {
								daysText = 'Days';
							} else {
								days = '';
							}
							if (hours === 1) {
								hoursText = 'Hour';
							} else if (hours > 1) {
								hoursText = 'Hours';
							} else {
								hours = '';
							}
							if (minutes === 1) {
								minutesText = 'Minute';
							} else if (minutes > 1) {
								minutesText = 'Minutes';
							} else {
								minutes = '';
							}
							if ((record.get('leaf')) && (days === "") && (hours === "") && (minutes === "")) {
								return 'N/A'
							} else if (!isNaN(days) || !isNaN(hours) ||!isNaN(minutes)) {
								return ([days, daysText, hours, hoursText, minutes, minutesText].filter(function(item) {return item !== ''}).join(' '));
							} else {
								return ''
							}
						},
						flex: 1.5,
						sortable: false,
						hideable: false
					}, {
						header: '~~description~~',
						dataIndex: 'description',
						hideable: false,
						sortable: false,
						flex: 4,
						renderer: function(value, metaData, record, rowIndex, colIndex, store) {
							var val = Ext.htmlEncode(value);
							var cont = (value && value.length > 64)? '... ': '';
							metaData.tdAttr = 'data-qtip="' + Ext.htmlEncode(val) + '"';
							return val.substr(0, 64) + cont;
						},
						editor: 'textarea'
					}, {
						xtype: 'actioncolumn',
						menuDisabled: true,
						sortable: false,
						width: 60,
						align: 'center',
						items: [ {
							glyph: 0xF146,
							iconCls: 'rs-sir-procedure-button-show',
							tooltip : 'Remove',
							handler: function(view, rowIndex, colIndex, item, e, record) {
								this.up('#playbookTemplate').fireEvent('removeactivity', this, record);
							}
						}]
					}]
				},{
					xtype: 'fieldset',
					flex: 1,
					maxHeight: 350,
					padding : '5 10 10',
					margin : 0,
					layout : 'fit',
					title: '~~createActivity~~',
					items: [{
						flex : 1,
						xtype: 'form',
						itemId: 'activityForm',
						layout: {
							type :'vbox',
							align : 'stretch'
						},
						defaults : {
							labelWidth : 120,
							margin : '4 0'
						},
						items: [{
							xtype: 'combo',
							itemId: 'availablePhasesField',
							editable: true,
							name: 'phase',
							queryMode: 'local',
							store: '@{knownPhases}',
							displayField: 'phase',
							valueField: 'phase',
							listeners: {
								expand: function(field, eOpts) {
									this.up('#playbookTemplate').fireEvent('populatephases', this, field, eOpts);
								}
							}
						},{
							xtype: 'textfield',
							name: 'activityName',
							maxLength: 200,
							enforceMaxLength: true
						}, {
							xtype: 'rsnumberfield',
							itemId: 'activitySla',
							fieldLabel: '~~sla~~',
							listeners: {
								slachanged: function() {
									this.fireEvent('activityslachanged', this, this.up().getForm().getFieldValues());
								},
								activityslachanged: '@{activitySlaChanged}'
							}
						}, {
							xtype: 'tbtext',
							hidden: '@{!activitySlaWarning}',
							htmlEncode: false,
							padding: '0 0 0 125',
							text: '@{activitySlaWarning}'
						}, {
							xtype: 'documentfield',
							itemId: 'wikiNameField',
							name: 'wikiName',
							store: '@{runbookStore}',
							allowBlank: false,
							displayField: 'ufullname',
							valueField: 'ufullname',
							editable: true,
							inline: false,  // NOT and inline editing
							listeners: {
								searchdocument: '@{searchRunbook}',
								select : '@{handleRunbookSelect}',
								validatewikiname: '@{validateWikiName}',
								blur: function(v) {
									v.fireEvent('validatewikiname', this, v)
								}
							}
						}, {
							xtype: 'fieldcontainer',
							fieldLabel: '~~required~~',
							defaultType: 'checkbox',
							items: [{
								name: 'isRequired',
								checked: true,
								value: true,
								hideLabel: true
							}]
						}, {
							xtype: 'textarea',
							name: 'description',
							value: '',
							maxLength: 2000,
							flex : 1,
							enforceMaxLength: true,
							fieldLabel: '~~description~~',
							allowBlank: true
						},{
							xtype : 'button',
							margin : '4 0 0 125',
							maxWidth : 120,
							name : 'addActivity',
							cls : 'rs-small-btn rs-btn-dark'
						}]
					}]
				}]
			},{
				title : '~~automationFilter~~',
				xtype : '@{automationFilter}'
			}]
		}]
	}],

	listeners: {
		removeactivity: '@{removeActivity}',
		populatephases: '@{populatePhases}',
		validateeditprocedure: '@{validateEditProcedure}',
		beforedestroy : '@{beforeDestroyComponent}'
	}
});

Ext.define('RS.incident.PlaybookTemplate.PhaseEditor', {
	extend: 'Ext.form.field.Text',
	xtype: 'phaseeditor'
});

glu.defView('RS.incident.PlaybookTemplate.AutomationFilter', {
	minHeight: 400,
	layout : {
		type : 'vbox',
		align : 'stretch'
	},
	defaults : {
		margin : '4 0',
		labelWidth : 130
	},
	items : [{
		xtype: 'combobox',
		name: 'filterValue',
		store: '@{filterStore}',
		displayField: 'name',
		valueField: 'value',
		queryMode: 'local',
		multiSelect: true
	},{
		xtype: 'combobox',
		name: 'order',
		store: '@{orderStore}',
		displayField: 'name',
		valueField: 'value',
		queryMode: 'local',
		editable : false
	}, {
		xtype: 'numberfield',
		name: 'refreshInterval',
		minValue: 5,
	}, {
		xtype: 'numberfield',
		name: 'refreshCountMax',
		minValue: 1,
	}, {
		xtype: 'checkbox',
		name: 'encodeSummary',
		boxLabel: '~~encodeSummary~~',
		hideLabel : true,
		margin : '0 0 0 135'
	},{
		xtype: 'checkbox',
		name: 'selectAll',
		boxLabel: '~~showAllActionTasks~~',
		hideLabel : true,
		margin : '0 0 0 135'
	},{
		xtype: 'checkbox',
		disabled: '@{!selectAll}',
		margin: '0 0 0 155',
		name: 'includeStartEnd',
		hideLabel : true,
		boxLabel: '~~includeStartEndTasks~~'
	},{
		disabled: '@{selectAll}',
		xtype: 'treepanel',
		cls : 'actionTaskFilters rs-grid-dark',
		name: 'actionTasks',
		title: '~~actionTaskFilters~~',
		columns: [{
			xtype: 'treecolumn',
			dataIndex: 'description',
			text: '~~description~~',
			flex: 1,
			renderer: function(value, meta, record) {
				var markup = '';
				var task = record.get('task');
				var tags = record.get('tags');

				if (value && task) {
					markup = value;
					markup += '&nbsp;&nbsp;&nbsp;';
					markup += '<span class="rs-accent">';
					markup += 'Task: ' + record.get('task');
					var namespace = record.get('namespace');

					if (namespace) {
						markup += '&nbsp;&nbsp;&nbsp;';
						markup += 'Namespace: ';
						markup += namespace;
					}

					var wiki = record.get('wiki');

					if (wiki) {
						markup += 'Wiki: ';
						markup += '&nbsp;&nbsp;&nbsp;';
						markup += wiki;
					}

					markup += '</span>';
				} else if (tags) {
					tags = tags.split(',');

					if (tags.length > 0) {
						markup = "#";
						markup += tags.join(' #');
					}
				} else {
					markup = value || record.get('task');
				}

				return markup;
			}
		}],
		rootVisible: false,
		dockedItems : [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items :['addGroup', 'addActionTask','edit', 'remove',
			{
				xtype: 'checkbox',
				name: 'preserveTaskOrder',
				boxLabel: '~~preserveTaskOrder~~',
				hideLabel: true
			}]
		}],
		flex: 1,
		viewConfig: {
			plugins: {
				ptype: 'treeviewdragdrop'
			}
		},
		plugins: [{
			ptype: 'cellediting',
			pluginId: 'celledit',
			clicksToEdit: 1,
			listeners: {
				beforeEdit: function(editor, e) {
					if (!e.record.isLeaf() && (e.column.dataIndex != 'description' && e.column.dataIndex != 'autoCollapse')) {
						return false
					}
				},
				edit: function(editor, e) {
					e.record.commit()
				}
			}
		}],
		/*listeners: {
			selectionchange: '@{selectionchange}',
			itemdblclick: '@{itemdblclick}'
		},*/
	}]
})

glu.defView('RS.incident.PlaybookTemplateNamer', {
	title: '~~newTemplate~~',
	cls : 'rs-modal-popup',
	modal: true,
	height : 250,   
    width: 600, 
	xtype: 'form',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	padding : 15,
	defaultType: 'textfield',
	items: [{
		xtype: 'documentfield',
		trigger2Cls: '',
		fieldLabel: '~~namespace~~',
		value: '@{namespace}',
		store: '@{namespaceStore}',
		queryMode: 'remote',
		displayField: 'unamespace',
		valueField: 'unamespace',
		allowBlank: false,
		regex: RS.common.validation.TaskNamespace,
		regexText: '~~invalidNamespace~~'
	}, {
		xtype: 'textfield',
		fieldLabel: '~~name~~',
		value: '@{name}',
		regex: RS.common.validation.TaskName,
		regexText: '~~invalidName~~',
		allowBlank: false
	}],
	
	buttons: [{
		name: 'create',
		cls : 'rs-med-btn rs-btn-dark',
		formBind: true
	},{
		name : 'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}]	
});

glu.defView('RS.incident.PlaybookTemplatePreview', {
	parentLayout: 'basePlaybook',
	propertyPanel: {
		asWindow: {
			padding : 15,
			title: '~~preview~~',
			cls : 'rs-modal-popup',
			modal : true,
			listeners: {
				beforeshow: function() {
					this.setWidth(Math.round(Ext.getBody().getWidth() * .90));
					this.setHeight(Math.round(Ext.getBody().getHeight() * .90));
				}
			},
			/*
			buttons: [{
				hidden: '@{!isPreview}',
				name: 'close',
				cls : 'rs-med-btn rs-btn-light',
			}] 
			*/
		}
	}
})

glu.defView('RS.incident.PlaybookTemplateSearchPreview', {
	parentLayout: 'basePlaybook',
	propertyPanel: {
		id: 'playbookTemplateSearchPreview'
	}
})


glu.defView('RS.incident.PlaybookTemplateRead', {
	parentLayout: 'basePlaybook',
	propertyPanel: {
		padding : '15 0',
		dockedItems : [{
			margin : '0 0 15 0',
			padding : '0 20',
			layout: 'hbox',
			items: [{
				xtype: 'tbtext',
				cls: 'rs-display-name',
				text: '@{returnTitle}',
			}, {
				xtype: 'tbtext',
				text: '~~readOnlyTxt~~'
			}]
 		}, {
			xtype : 'toolbar',
			margin : '0 15 0 15',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
 			},
			items : [{
				iconCls: 'rs-icon-button icon-file-text',
				text: '~~viewFileOptions~~',
				menu: [{
					name: 'edit',
					disabled: '@{!editIsEnabled}',
					width : 200
				}, {
					xtype: 'menuseparator',
				}, {
					name: 'copy',
					disabled: '@{!copyIsEnabled}'
				}, {
					xtype: 'menuseparator',
				}, 'preview', {
					xtype: 'menuseparator',
				}, {
					name: 'returnToTemplates'
				}]
			},
			'->', {
				name: 'preview',
				iconCls: 'rs-social-button icon-eye-open',
				text: '',
				tooltip: '~~preview~~'
 			}, {
				name: 'edit',
				iconCls: 'rs-social-button icon-pencil',
				text: '',
				tooltip: '~~edit~~'
 			}, {
				iconCls: 'rs-social-button icon-repeat',
				name: 'reload',
				text: '',
				tooltip: '~~reload~~'
			}]
		}]
	}
})

glu.defView('RS.incident.PlaybookTemplates', {
	layout: 'fit',
	padding : 15,
	items: [{
		xtype: 'grid',
		hideSelModel: '@{!pSirPlaybookTemplatesTemplateBuilderView}',
		hidden: '@{!pSirPlaybookTemplatesView}',
		cls : 'rs-grid-dark',
		displayName: '~~playbookTemplates~~',
		dockedItems: [{
			xtype: 'toolbar',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items: [{
				name: 'createTemplate',
				disabled: '@{!pSirPlaybookTemplatesCreate}'
			}, {
				name: 'inactivate'
			}, {
				name: 'activatePB',
				hidden: '@{!inactiveColumnShow}'
			}, {
				name: 'deleteTemplate',
				hidden: true
			}]
		}],
		name: 'template',
		plugins: [{
			ptype: 'searchfilter',
			allowPersistFilter: false,
			hideMenu: true,
			useWindowParams: false
		}, {
			ptype: 'pager'
		}, {
			ptype: 'columnautowidth'
		}],

		stateId: 'rsplaybookTemplates',
		store: '@{store}',
		columns: '@{columns}',
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'id'
		},
		listeners: {
			editAction: '@{editTemplate}'
		}
	}]
});
glu.defView('RS.incident.ReportTemplates', {
	padding : 15,
	displayName: '~~reportTemplates~~',
	name : 'reportTemplate',
	dockedItems : [{
		xtype : 'toolbar',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'	
		},
		items : ['new','copy','purge','|','goToDashboard']
	}],
	xtype : 'grid',
	cls : 'rs-grid-dark',
	store : '@{reportTemplateStore}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true,
		useWindowParams: false
	}, {
		ptype: 'pager'
	}], 
	columns: [{
		header: '~~name~~',
		dataIndex: 'uname',
		filterable: true,
		flex: 1,
	}, {
		header: '~~namespace~~',
		dataIndex: 'unamespace',
		flex: 1,
		filterable: true
	}, {
		header: '~~revision~~',
		dataIndex: 'uversion',
		flex: 0.5,
		filterable: true
	}].concat((function(columns) {
		Ext.each(columns, function(col) {
			if (col.dataIndex == 'sysCreatedOn') {
				col.hidden = false;
				col.width = 200,
				col.initialShow = true;
			}
			if (col.dataIndex == 'sysCreatedBy') {
				col.hidden = false;
				col.initialShow = true;
			}
			if (col.dataIndex == 'sysUpdatedOn') {
				col.hidden = false;
				col.initialShow = true;
			}
			if (col.dataIndex == 'sysUpdatedBy') {
				col.hidden = false;
				col.initialShow = true;
			}
			if (col.dataIndex == 'id') {
				col.flex = 1.5;
			}
		});
		return columns;
	})(RS.common.grid.getSysColumns())),
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},
	listeners: {
		editAction: '@{editTemplate}'
	}
})
glu.defView('RS.incident.SecurityIncidentForm', {
	asWindow: {
		title: '~~newInvestigation~~',
		modal: true,
		padding: 15,
		cls : 'rs-modal-popup',
		width : 700
	},
	xtype: 'form',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		labelWidth: 190,
	},
	items: [{
		xtype: 'textfield',
		fieldLabel: '~~title~~',
		name: 'title',
		maxLength: 200,
		enforceMaxLength: true,
		allowBlank: false
	},{
		xtype: 'combo',
		fieldLabel: '~~type~~',
		name: 'investigationType',
		allowBlank: false,
		store: '@{..typeStore}',
		queryMode: 'local',
		displayField: 'type',
		valueField: 'type',
		editable: false
	},{
		xtype: 'documentfield',
		fieldLabel: '~~playbookTemplate~~',
		name: 'playbook',
		store: '@{..playbookTemplateStore}',
		allowBlank: false,
		inline: true,   // inline cell editing
		displayField: 'ufullname',
		valueField: 'ufullname',
		trigger2Cls: '',
		listeners: {
			select : '@{handlePlaybookSelect}',
			validatetemplatename: '@{validateTemplateName}',
			blur: function(v) {
				v.fireEvent('validatetemplatename', this, v)
			}
		}
	},{
		xtype: 'combo',
		fieldLabel: '~~severity~~',
		name: 'severity',
		store: '@{severityStore}',
		displayField: 'severity',
		valueField: 'severity',
		editable: false
	}, {
		xtype: 'datefield',
		fieldLabel: '~~dueDate~~',
		name: 'dueBy',
		allowBlank: true,
		minValue : new Date(),
		editable: false
	}, {
		xtype: 'combo',
		fieldLabel: '~~owner~~',
		name: 'owner',
		allowBlank: true,
		store: '@{..ownerStore}',
		displayField: 'userId', //display name/userId
		valueField: 'userId',
		queryMode: 'local',
		editable: false
	}, {
		xtype: 'combo',
		fieldLabel: '~~origin~~',
		name: 'sourceSystem',
		store: '@{originStore}',
		editable: true,
		displayField: 'origin',
		valueField: 'origin',
		listeners: {
			select: '@{originSelected}'
		}
	} ,{
		xtype: 'textfield',
		name: 'externalReferenceId',
		value: '',
		hidden: '@{resolveSelected}',
		allowBlank: '@{resolveSelected}',
		setAllowBlank: function(allow) {
			this.allowBlank = allow;
			this.setValue(allow? 'allow': ''); // Trick to by pass allowBlank for Resolve
		}
	},{
		xtype: 'textfield',
		name: 'status',
		value: 'Open',
		hidden: true
	}],

	listeners: {
		afterrender: function() {
			this.fireEvent('adjustwindowsize', this, this);
		},
		adjustwindowsize: '@{adjustWindowSize}'
	},

	buttons: [{
		name: 'add',
		formBind: true,
		cls : 'rs-med-btn rs-btn-dark'
	},{
		name :'cancel',
		cls : 'rs-med-btn rs-btn-light'
	}],	
});
Ext.define('RS.incident.Dashboard.AgingBucket.UnitStore', {
	extend: 'Ext.data.Store',
	fields: [
		{name: 'timeframeUnit', type: 'string'}
	]
});

Ext.define('RS.incident.Dashboard.AgeBucketBuilder', {
	extend: 'Ext.form.FieldSet',
	alias: 'widget.agebucketbuilder',
	itemId: 'ageBucketBuilder',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},

	items: [],
	frames: [],
	nFrame: 0,
	initComponent: function() {
		var me = this;
		Ext.apply(me, {
			title: RS.incident.Dashboard.locale.AgeBucketBuilder.ageBuckets
		});
		me.callParent(arguments);
		me.add({
			xtype: 'toolbar',
			margin: '5 0 5 0',
			layout: 'hbox',
			items: ['->', {
				text: RS.incident.Dashboard.locale.AgeBucketBuilder.newTimeFrame,
				style: 'border: 1px solid gray;border-radius: 0px;',
				itemId: 'newTimeFrameButton',
				handler: function() {
					var start = me.getNextStartTimeframe();
					var valUnit = start.split(' ');
					me.createPriorToEndTimeframe(start, [valUnit[0]-0+1 /*add 1 to string*/, valUnit[1]].join(' '), true);
				}
			}, {
				text: RS.incident.Dashboard.locale.AgeBucketBuilder.deleteLastTimeframe,
				style: 'border: 1px solid gray;border-radius: 0px;',
				disabled: true,
				itemId: 'deleteTimeframeButton',
				handler: me.deleteLastTimeframe.bind(me)
			}]
		});
	},
	loadData: function(data) {
		// '0 Days-1 Days,3 Days,7 Days,21 Days,365 Days'
		var buckets = data.split(','); // ['0 Days-1 Days','3 Days','7 Days','21 Days','365 Days']
		var bucket1 = buckets.shift().split('-');
		// First entry must always be a time frame with two values, start/end time
		this.createStartToEndTimeframe(bucket1[0], bucket1[1]);
		var priorEndTime = bucket1[1];
		buckets.forEach(function(bucket) {
			this.createPriorToEndTimeframe(priorEndTime, bucket);
			priorEndTime = bucket;
		}, this);
	},

	getData: function() {
		var data = [];
		this.items.items.forEach(function(frame) {
			if(typeof(frame.getData) == 'function') {
				data.push(frame.getData());
			}
		}, this);
		return data.join(',');
	},

	// This method is called after data is loaded
	createStartToEndTimeframe: function(start, end) {
		if (this.nFrame) {
			console.error('There must only be one "FirstTimeFrame"');
			return;
		}
		this.insert(this.nFrame, {
			xtype: 'timeframe',
			start: start,
			end: end,
			order: ++this.nFrame,
			store: this.unitStore
		});
		this.down('#deleteTimeframeButton').setDisabled(true); // First time frame is mandatory
	},

	// This method is called after data is loaded
	createPriorToEndTimeframe: function(start, end, noisy) {
		this.insert(this.nFrame, {
			xtype: 'nextframe',
			start: start,
			end: end,
			order: ++this.nFrame,
			store: this.unitStore
		}, noisy);
		this.down('#deleteTimeframeButton').setDisabled(false);
		this.down('#newTimeFrameButton').setDisabled(this.nFrame >= 10);
	},

	deleteLastTimeframe: function() {
		var selector = '#' + RS.incident.Dashboard.locale.AgeBucketBuilder.column + this.nFrame;
		var removedFrame = this.down(selector);
		if (removedFrame) {
			this.remove(removedFrame);
			if (--this.nFrame == 1) {
				this.down('#deleteTimeframeButton').setDisabled(true);
			}
			this.down('#newTimeFrameButton').setDisabled(false);
		}
	},

	insert: function(nFrame, frame, noisy) {
		var newElem = this.callParent(arguments);
		if (this.frames.length) {
			this.frames[this.frames.length-1].nextframe = newElem;
		}
		this.frames.push(newElem);
		if (noisy) {
			this.up('form').fireEvent('dirtychange', this, true);
		}
	},

	remove: function() {
		this.frames.pop();
		this.frames[this.frames.length-1].nextframe = null; // Assuming there is at least one element in the frame
		this.callParent(arguments);
		this.up('form').fireEvent('dirtychange', this, true);
	},

	getNextStartTimeframe: function() {
		var f = this.frames[this.frames.length-1];
		return f.down('#endvalue').getValue()+ ' ' + f.down('#endunit').getValue();
	}
});

Ext.define('RS.incident.Dashboard.BaseFrame', {
	extend: 'Ext.form.FieldContainer',
	alias: 'widget.baseframe',
	config: {
		order: 1
	},
	layout: {
		type: 'hbox',
		align: 'stretch',
		pack: 'center'
	},
	defaults: {
		labelSeparator: '',
		fieldLabel: ''
	},
	labelWidth: 163,
	items: [],

	initComponent: function() {
		var frameName = 'Column ' + this.order;
		var itemId  = frameName.replace(/\s/g, '');
		var sValUnit = this.start.split(' ');
		var valUnit = this.end.split(' ');
		var units = [];
		var unit;
		this.store.each(function(r) {
			unit = r.get('timeframeUnit');
			(unit == sValUnit[1])? units.push(r.getData()): units.length? units.push(r.getData()): null;
		}, this);
		var unitStore = new RS.incident.Dashboard.AgingBucket.UnitStore();
		unitStore.loadData(units);
		this.items.push({
			xtype: 'label',
			text: RS.incident.Dashboard.locale.TimeFrame.to,
			style: 'text-align:center;margin-top:4px;font-style:italic;',
			flex: 12
		}, {
			xtype: 'agingbucketvaluefield',
			itemId: 'endvalue',
			flex: 10,
			margin: '0 2 0 0',
			minValue: sValUnit[0]-0+1, // 'Integer' add 1 to string
			value: valUnit[0]
		}, {
			xtype: 'agingbucketunitfield',
			itemId: 'endunit',
			store: unitStore,
			displayField: 'timeframeUnit',
			valueField: 'timeframeUnit',
			value: valUnit[1],
			queryMode: 'local',
			editable: false,
			flex: 15
		});
		Ext.apply(this, {
			fieldLabel: frameName,
			itemId: itemId,
			items: this.items
		});
		this.callParent(arguments);
	},

	triggerChange: function(type, v, newValue, oldValue) {
		var endField = this.down('#end' + type);
		if (v['start'+type]) {
			endField.fireEvent(type+'change', newValue);
		}
		var currentValue = endField.getValue();
		var nextFrame = this.nextframe;
		while(nextFrame) {
			endField = nextFrame.down('#end'+type);
			endField.fireEvent(type+'change', currentValue);
			currentValue = endField.getValue();
			nextFrame = nextFrame.nextframe;
		}
	},

	listeners: {
		bvaluechanged: function(oArgs) {
			this.triggerChange('value', oArgs[0], oArgs[1], oArgs[2]);
		},
		bunitchanged: function(oArgs) {
			this.triggerChange('unit', oArgs[0], oArgs[1], oArgs[2]);
		}
	}
});

Ext.define('RS.incident.Dashboard.TimeFrame', {
	extend: 'RS.incident.Dashboard.BaseFrame',
	alias: 'widget.timeframe',
	isFirstFrame: true,
	initComponent: function() {
		// start: '1 days', end: '5 days'
		var valUnit = this.start.split(' ');
		this.items = [{
			xtype: 'agingbucketvaluefield',
			itemId: 'startvalue',
			startvalue: true,
			flex: 10,
			margin: '0 2 0 0',
			minValue: 1,
			maxValue: 99,
			value: valUnit[0]
		}, {
			xtype: 'agingbucketunitfield',
			itemId: 'startunit',
			startunit: true,
			store: this.store,
			displayField: 'timeframeUnit',
			valueField: 'timeframeUnit',
			value: valUnit[1],
			queryMode: 'local',
			editable: false,
			flex: 15
		}];
		this.callParent(arguments);
	},
	getData: function() {
		return (this.down('#startvalue').getValue() + ' ' + this.down('#startunit').getValue() + '-' +
		this.down('#endvalue').getValue() + ' ' + this.down('#endunit').getValue());
	}
});

Ext.define('RS.incident.Dashboard.NextFrame', {
	extend: 'RS.incident.Dashboard.BaseFrame',
	alias: 'widget.nextframe',
	isFirstFrame: false,
	initComponent: function() {
		this.items = [{
			xtype: 'label',
			text: RS.incident.Dashboard.locale.TimeFrame.priorTimeFrameEnd,
			style: 'text-align:center;margin-top:4px;margin-right:3px',
			flex: 25
		}];
		this.callParent(arguments);
	},
	getData: function() {
		return this.down('#endvalue').getValue() + ' ' + this.down('#endunit').getValue();
	}
});

Ext.define('RS.incident.Dashboard.AgingBucket.Valuefield', {
	extend: 'Ext.form.field.Number',
	alias: 'widget.agingbucketvaluefield',
	allowBlank: false,
	initComponent: function() {
		this.enableBubble('bvaluechanged');
		this.callParent(arguments);
		this.on('valuechange', this.reInit, this);
		this.on('change', function (v) {
			v.fireEvent('bvaluechanged', arguments);
		});
		this.on('validitychange', function(v, isValid, eOpts) {
			this.findParentByType('agebucketbuilder').fireEvent('validitychange', v, isValid, eOpts);
		})
	},
	reInit: function(newVal) {
		if (this.getValue() <= newVal) {
			// If current value is LTE new value, set newVal + 1 as current value
			this.setValue(newVal+1);
		}
		this.setMinValue(newVal+1);
	}
});

Ext.define('RS.incident.Dashboard.AgingBucket.Unitfield', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.agingbucketunitfield',

	initComponent: function() {
		this.enableBubble('bunitchanged');
		this.callParent(arguments);
		this.on('unitchange', this.reInit, this);
		this.on('change', function (v) {
			v.fireEvent('bunitchanged', arguments);
		});
	},

	reInit: function(newVal) {
		var currentVal = this.getValue();
		var unitMap = {
			Hours: [{timeframeUnit: 'Hours'}, {timeframeUnit: 'Days'}, {timeframeUnit: 'Weeks'}],
			Days: [{timeframeUnit: 'Days'}, {timeframeUnit: 'Weeks'}],
			Weeks: [{timeframeUnit: 'Weeks'}]
		};
		var weightMap = {
			Hours: 1,
			Days: 2,
			Weeks: 3
		}
		this.store.loadData(unitMap[newVal]);
		if (weightMap[currentVal] < weightMap[newVal] ) {
			this.setValue(unitMap[newVal][0].timeframeUnit);
		}
	}
});
Ext.define('RS.incident.Dashboard.Chart', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.chart',
	style: 'border-style: solid; border-width: 1px;border-color: #dadadf',

	layout: 'anchor',
	config :{
		chartId: '',
		chartConfigs: undefined,
		chartSettingsHandler: undefined
	},

	setChartConfigs: function(configs) {
		this.chartConfigs = configs;
		this.renderChart(configs);
	},

	setRefreshMonitor: function() {
		if (!this.up('panel').hidden) {
			this.refreshchart();
		}
	},

	initComponent: function() {
		var me = this;
		this.dockedItems =  [{
			xtype: 'toolbar',
			dock: 'top',
			height: 35,
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'label',
				itemId: 'chartTitle',
				style: 'text-align: right;font-weight: bold;margin-top:5px',
				flex: 1,
				text: this.chartTitle,
			}, {
				xtype: 'toolbar',
				flex:  0.6,
				layout: 'hbox',
				items: ['->', {
					glyph: 0xF013,
					itemId: 'gearIcon',
					hidden: true,
					tooltip: RS.incident.Dashboard.locale.ChartSettings.configureChartSettings,
					chartId: this.chartId,
					graphType: me.graphBy,
					handler: me.chartSettingsHandler
				}]
			}],
			listeners: {
				afterrender: function(tb) {
					var me = tb;
					me.getEl().on({
						'mouseenter': function (e, t, eOpts) {
								me.down('#gearIcon').show();
						},
						'mouseleave': function() {
							me.down('#gearIcon').hide();
						}
					});
				}
			}
		}];
		this.callParent(arguments);
		this.on('refreshchart', this.refreshchart, this);
	},

	setTitle: function(title) {
		this.down('#chartTitle').setText(title);
	},

	// Sample of configs object
	// configs = {
	//		ageBuckets: "0 Days-1 Days,3 Days,7 Days"
	//		chartType: "Investigations by Type" // "Investigations by age", "Investigations 'Investigations by Severity'"
	//		graphType: "Pie Chart",             // "Bar Graph"
	//		id: 1
	//		numberOfCols: 6
	//		ownerFilter: "All"
	//		scope: "23"
	//		scopeUnit: "Days"
	//		severityFilter: "All"
	//		statusFilter: "All"
	//		teamFilter: "All"
	//		typeFilter: "All"
	// }
	renderChart: function(configs) {
		this.removeAll();
		this.setTitle(configs.chartType);
		this.add({
			xtype: configs.graphType == RS.incident.Dashboard.locale.ChartSettings.pieChart? 'sirpiechart': 'sircolumnchart',
			graphBy: configs.chartType,
			filters: configs,
			itemId: 'graphView'
		});
	},
	refreshchart: function() {
		this.down('#graphView').refreshchart();
	}
});

Ext.define('RS.incident.Dashboard.SIRChart', {
	extend: 'Ext.Component',
	alias: 'widget.sirchart',
	anchor: '100% 100%',

	initComponent: function() {
		this.dataByTypeFuncMap = {};
		this.dataByTypeFuncMap[RS.incident.locale.investigationByType] = function () {
			var dashboard = this.up('#sirDashboardPanel');
			var payload = this.createFilters(this.up('chart').chartConfigs);
			this.getReport('/resolve/service/playbook/getsircountbytypereport', payload);
		};
		this.dataByTypeFuncMap[RS.incident.locale.investigationBySeverity] = function() {
			var dashboard = this.up('#sirDashboardPanel');
			var payload = this.createFilters(this.up('chart').chartConfigs);
			this.getReport('/resolve/service/playbook/getsircountbyseverityreport', payload);
		};
		this.dataByTypeFuncMap[RS.incident.locale.investigationByAge] = function() {
			var chartConfigs = this.up('chart').chartConfigs;
			var payload = this.createFilters(chartConfigs);
			payload.ageBuckets = chartConfigs.ageBuckets;
			this.getReport('/resolve/service/playbook/getsircountbyagereport', payload);
		};
		this.dataByTypeFuncMap[RS.incident.locale.investigationOverTimes] = function() {
			var chartConfigs = this.up('chart').chartConfigs;
			var payload = this.createFilters(chartConfigs);
			payload.ageBuckets = chartConfigs.ageBuckets;
			payload.numberOfCols = chartConfigs.numberOfCols;
			this.getReport('/resolve/service/playbook/getsircountovertimes', payload);
		};
		this.dataByTypeFuncMap[RS.incident.locale.investigationByTeamMember] = function() {
			var dashboard = this.up('#sirDashboardPanel');
			var payload = this.createFilters(this.up('chart').chartConfigs);
			this.getReport('/resolve/service/playbook/getsircountbyteammember', payload);
		};
		this.dataByTypeFuncMap[RS.incident.locale.investigationByStatus] = function() {
			var dashboard = this.up('#sirDashboardPanel');
			var payload = this.createFilters(this.up('chart').chartConfigs);
			this.getReport('/resolve/service/playbook/getsircountbystatusreport', payload)
		};
		this.dataByTypeFuncMap[RS.incident.locale.investigationBySLA] = function() {
			var dashboard = this.up('#sirDashboardPanel');
			var payload = this.createFilters(this.up('chart').chartConfigs);
			RS.incident.model.DashboardSettings.load(1, {  // Load settings from cache
				scope: this,
				callback: function (record, operation, success) {
					payload.nDaysBAtRisk = success ? record.get('sLAatRiskPolicy') : 5; // 5 is the default
					this.getReport('/resolve/service/playbook/getsircountbyslareport', payload);
				}
			});
		};
		this.dataByTypeFuncMap[RS.incident.locale.investigationByWorkload] = function() {
			var dashboard = this.up('#sirDashboardPanel');
			var payload = this.createFilters(this.up('chart').chartConfigs);
			this.getReport('/resolve/service/playbook/getsircountbyworkloadreport', payload);
		};

		var me = this;
		this.chartConfig = Ext.apply({}, {
			theme: 'light',
			valueField: 'value',
			colorField: 'color',
			categoryAxis: {
				labelRotation: 40
			},
			chartCursor: {
				cursorAlpha: 0.3,
				zoomable: false,
				categoryBalloonEnabled: true
			},
			graphs: [{
				valueField: 'value',
				colorField: 'color'
			}]
		 });

		this.callParent(arguments);
		this.on('boxReady', this.boxReady, this);
		this.resizeHandler = function() {
			me.up('panel').doLayout();
		};
		Ext.EventManager.onWindowResize(this.resizeHandler, this);

		this.on('destroy', function() {
			// Remove resize handler when chart is destroyed.
			Ext.EventManager.removeResizeListener(this.resizeHandler, this);
		});
	},


	boxReady: function(v, width, height, eOpts ) {
		Ext.apply(v.chartConfig, {
			width: width,
			height: height,
			categoryField: v.graphBy.split(' ')[1].toLowerCase()
		});
		v.createChart(this.getId(), v.chartConfig);
		v.syncChart();
	},


	syncChart: function() {
		this.dataByTypeFuncMap[this.graphBy].call(this);
	},

	byTypeColors: function(k){
		// By type must use customized colors
		return {
			'Malware': '#118fde',
			'Phishing': '#ef9911',
			'Network Protection': '#2b9221',
			'Access Protection': '#b52753',
			'Data Protection': '#ae73e0'
		}[k];
	},

	byServerityColors: function(k) {
		// By serverity must use customized colors
		return {
			Critical: '#c03131',
			High: "#f17954",
			Medium: '#e0dc3a',
			Low: '#85cc47'
		}[k];
	},

	getColorCode: function() {
		var colorSet = [
			'#85CC47', '#E0DC3A', '#F17954', '#C03131', '#F80606', '#88221B', '#880F07', '#722380', '#44164C', '#1D3ADC',
			"#FF6600", "#FCD202", "#B0DE09", "#0D8ECF", "#2A0CD0", "#CC0000", "#00CC00", "#821432", "#0C5B8E", "#FAFEAB", "#2EAB64", "#F8298F", "#AF661D",
			"#2E7CEF", "#7F9004", "#F947FB", "#FA847B", "#E2A3F7", "#9CC4FA", "#4E3049", "#DAF756", "#288D98", "#FB1558", "#2E9F0A", "#80FAC4", "#900E04",
			"#F9B873", "#69FB81", "#946AFE", "#EBFBE2", "#760F5B", "#E983B4", "#FE56D0", "#F8790D", "#2A3F92", "#2A650F", "#32555B", "#653F09", "#0B613A",
			"#EDDA60", "#2E9A7C", "#4A382D", "#B0EF96", "#8A94E8", "#DA78F9", "#BFECF9", "#4ECEF4", "#B50A7E", "#FE6183", "#BB2839", "#F77D50", "#552D6E",
			"#F09C3C", "#178FC2", "#29CAB6", "#525B00", "#F4A896", "#93C90E", "#FFE3BC", "#6D3527", "#A4FAEC", "#BD0BA3", "#897606", "#B8A50C", "#D8C6EF",
			"#BF276A", "#363A65", "#B088FB", "#0D82D9", "#FE4E4F", "#7C2F08", "#25E444", "#C91D1E", "#D5F30B", "#25B44C", "#EAE32F", "#C942D1", "#D4F3BD",
			"#1DDF9E", "#EF84DE", "#F967A6", "#D45311", "#20F7DC", "#BBEF6E", "#08BBCB", "#FCE089", "#725FD9", "#84F75F", "#3AC092", "#7B3443", "#AC4815",
			"#FECA64", "#760A41", "#D19C10", "#FFA67C", "#21463A", "#63A2EA", "#FD5C36", "#CB7603", "#248543", "#497F04", "#F3FA7F", "#075F77", "#91FEA9",
			"#5842A6", "#1C4305", "#7D5A02", "#FE88A5", "#4C494C", "#C859F4", "#642757", "#E9B2E4", "#F8C893", "#1D6E59", "#F39857", "#BCFE54", "#8B1E1C",
			"#ACED0C", "#1C64AB", "#A2258D", "#28BC1A", "#FB34D4", "#224480", "#F79FA6", "#E090FA", "#BFD514", "#464931", "#345274", "#85FBDC", "#91D0F1",
			"#F2645A", "#23FA71", "#FC0E23", "#A1AB00", "#9B2063", "#C2FFD6", "#5C4C05", "#DCB9FF", "#0DB4EB", "#EE0B3E", "#3D3D52", "#6C74FC", "#44D481",
			"#F029AF", "#9BEB7E", "#6AD511", "#F8307C", "#C4EA81", "#FFE1D1", "#667A0F", "#D1D2E8", "#D71946", "#3FA249", "#643286", "#ACB0F8", "#129B69",
			"#F9A0C1", "#3BB0AF", "#03741C", "#5E3E41", "#237F53", "#234226", "#453C7F", "#1FAED2", "#F5CE49", "#3B66E0", "#F1822C", "#268C16", "#E95DC1",
			"#8B4A1A", "#2C625A", "#A7700B", "#29739D", "#A947CC", "#0259AF", "#ED70B6", "#AA2655", "#664229", "#A55AEA", "#0EB335", "#FF5BB1", "#642345",
			"#DFEEF2", "#FC336D", "#B3093F", "#E8568A", "#DD329E", "#197E97", "#234052", "#950469", "#C4F9B6", "#61FF97", "#C3AAFE", "#F72BA1", "#6C2B6A",
			"#27F8BB", "#3C5D0A", "#2ACE12", "#6AEAA4", "#E986CE", "#70BA01", "#EC6CF5", "#076C2E", "#F1AD3F", "#8A2646", "#2B9CDE", "#BF73FC", "#FE8566",
			"#FBF795", "#418E79", "#397C82", "#3A4AAD", "#702621", "#48C264", "#275F42", "#938AFF", "#7F3C99", "#96083B", "#EC2FBE", "#4A3156", "#F85F23",
			"#39E5B1", "#3473CD", "#D8C3FC", "#D4EC71", "#9C78FF", "#B5220E", "#D20014", "#C382FE", "#F4D2D0", "#53A507", "#EEE5C4", "#4B3286", "#8CA801",
			"#F781FE", "#7C031A", "#D50230", "#A2E3F9", "#A1C415", "#B2A728", "#DE0E84", "#A0E223", "#B7CFFC", "#C20479", "#ACF7F5", "#3CEA78", "#FD8287",
			"#134A9A", "#FAE0AC", "#3E6FDB", "#CBFC5C", "#790A66", "#2D81E2", "#FE3242", "#7060E6", "#FEAE77", "#D1D722", "#3D92DC", "#2C4204", "#D6871C",
			"#FC5DDE", "#62D0FF", "#454621", "#E46904", "#4C2C63", "#FD8F8B", "#55F2D2", "#807517", "#ECFCF6", "#E8AAE7", "#1A5432", "#55ED85", "#FCD98E",
			"#5F4113", "#F5C5DD", "#F7BF6F", "#FC507B", "#3EF9B1", "#6E332F", "#3AEDFE", "#EDFBA0", "#2CA1C7", "#EB8635", "#43B916", "#9C0504", "#CB4309",
			"#CBFEDD", "#1C6932", "#2B6A99", "#75F676", "#ACC2FA", "#860664", "#9A2D9F", "#5082EF", "#2B8A03", "#B047D3", "#F83913", "#0086BB", "#2A4400",
			"#A5F7F1"
		];
		this.colorAssigned = (this.colorAssigned >= colorSet.length)? 0: this.colorAssigned || 0; // Rotate if > colorSet.length; initialized to 0 if not
		return colorSet[this.colorAssigned++];
	},

	graphByMap: function(type) {
		switch(type) {
			case RS.incident.locale.investigationByType:
				return 'byTypeColors';
			case RS.incident.locale.investigationBySeverity:
				return 'byServerityColors';
			default:
				return 'getColorCode';
		}
	},

	getColor: function(graphBy, value) {
		return this[this.graphByMap(graphBy)].call(this, value);

	},

	unpackData: function(recs, graphBy) {
		var me = this;
		var data = [];
		recs.forEach(function (rec) {
			Object.keys(rec).forEach(function(k) {
				var d = {
					value: rec[k],
					color: me.getColor(graphBy, k),
					showInLegend: rec[k]? true: false  // Only show in legend non-zero items
				};
				d[graphBy.split(' ')[1].toLowerCase()] = k;
				data.push(d);
			});
		});
		return data;
	},

	getReport: function(url, params) {
		params = params || {};
		params.orgId = clientVM.orgId;
		Ext.Ajax.request({
			scope: this,
			url: url,
			method: 'POST',
			params: params,
			success: function(resp) {
				var respData  = RS.common.parsePayload(resp);
				if (!respData .success) {
					clientVM.displayError(respData.message);
					return;
				}
				this.chart.dataProvider = this.unpackData(respData .records, this.graphBy);
				if (!this.chart.dataProvider || !this.chart.dataProvider.length) {
					this.chart.addLabel('50%', '50%', RS.incident.Dashboard.locale.ChartSettings.noDataMsg, 'middle', 12);
					if (this.chart.type == 'pie') {
						// Add bogus data
						var dp = {
							showInLegend: false
						};
						dp[this.chart.titleField] = "";
						dp[this.chart.valueField] = 0;
						this.chart.dataProvider.push(dp)
						this.chart.alpha = 0.3;
					}
				} else {
					this.chart.clearLabels();
				}
				this.chart.validateData();
			},
			failure: function(resp) {
				clientVM.displayFailure(resp);
			}
		});
	},

	createFilters: function(chartConfigs) {
		return {
			scopeUnits: chartConfigs.scope,
			unitType: chartConfigs.scopeUnit,
			typeFilter: chartConfigs.typeFilter,
			severityFilter: chartConfigs.severityFilter,
			ownerFilter: chartConfigs.ownerFilter,
			teamFilter: chartConfigs.teamFilter,
			statusFilter: chartConfigs.statusFilter
		};
	},

	createChart: function(id) {
		var me = this;
		this.getCustomChartConfig(this.chartConfig);
		this.chart = AmCharts.makeChart(id, this.chartConfig);
		Ext.EventManager.onWindowResize(function() {
			me.chart.invalidateSize(); // Force the chart to resize to it's current container size.
		});
	},

	setChartInvestigateBy: function(val, silent) {
		var map = {
			'1': 'pieChartInvestigateBy',
			'2': 'column1ChartInvestigateBy',
			'3': 'column2ChartInvestigateBy'
		};
		this.down('#combobox').setValue(val);
		this.graphBy = val;
		if (!silent){
			// control -> viewmodel binding
			this.up('#sirDashboardPanel').fireEvent('chartdatachange', this, map[this.chartId], val);
		}
		if (val != RS.incident.locale.investigationByAge) {
			this.down('#chartTitle').setText('Investigations ' + val);
		} else {
			this.down('#chartTitle').setText(RS.incident.locale.investigationByAge);
		}
		this.refreshchart();
	},

	setHideChartDataPicker: function(val) {
		if (val) {
			this.down('#combobox').hide();
			this.down('#dropDownIcon').show();
		} else {
			var combo = this.down('#combobox');
			combo.show();
			this.down('#dropDownIcon').hide();
		}
	},

	refreshchart: function() {
		this.colorAssigned = 0; // Reset colorAssigned for 'Investigations by age' report.
		this.syncChart();
	}
});

Ext.define('RS.incident.Dashboard.PieChart', {
	extend: 'RS.incident.Dashboard.SIRChart',
	alias: 'widget.sirpiechart',

	getCustomChartConfig: function(chartConfig) {
		Ext.apply(chartConfig, {
			type: 'pie',
			titleField: this.graphBy.split(' ')[1].toLowerCase(),
			labelText: '[[percents]]%',
			labelRadius: 5,
			marginBottom: 0,
			marginTop: 0,
			innerRadius: '30%',
			visibleInLegendField: 'showInLegend',
			legend: {
				position: 'right',
				align: 'center',
				autoMargins: true,
				labelWidth: 160,
				markerSize: 10,
				verticalGap: 0,
				switchable: false,
				valueText: '',
				fontSize: 12
			},
			balloon:{
				fixedPosition:true
			}
		})
	},

	setChartInvestigateBy: function(investigateBy, silent) {
		if (this.chart) {
			this.chart.titleField = investigateBy.split(' ')[1].toLowerCase();
			this.chart.validateNow();
		}

		this.callParent(arguments);
	}
});

Ext.define('RS.incident.Dashboard.ColumnChart', {
	extend: 'RS.incident.Dashboard.SIRChart',
	alias: 'widget.sircolumnchart',

	getCustomChartConfig: function(chartConfig) {
		Ext.apply(this.chartConfig, {
			type: 'serial',
			valueAxes: [{
				integersOnly: true
			}]
		});
		Ext.apply(this.chartConfig.graphs[0], {
			type: 'column',
			columnWidth: 0.5,
			fillAlphas: 1,
			lineAlpha: 0
		});
	},

	setChartInvestigateBy: function(investigateBy, silent) {
		if (this.chart) {
			this.chart.categoryField = investigateBy.split(' ')[1].toLowerCase();
			this.chart.validateNow();
		}

		this.callParent(arguments);
	}
});

Ext.define('RS.incident.Dashboard.AreaChart', {
	extend: 'RS.incident.Dashboard.SIRChart',
	alias: 'widget.sirareachart',

	getCustomChartConfig: function(chartConfig) {
		Ext.apply(this.chartConfig, {
			type: 'serial'
		});
		Ext.apply(this.chartConfig.graphs[0], {
			type: 'line',
			fillAlphas: 0.6,
		});
		Ext.apply(this.chartConfig.categoryAxis, {
			startOnAxis: true
		});
	}
});

Ext.define('RS.incident.Dashboard.ChartFilterCombo', {
	extend: 'Ext.form.field.ComboBox',

	alias: 'widget.chartfiltercombo',
	editable: false,
	queryMode: 'local',

	setValue: function(value) {
		var me = this;
		// If 'All' is selected, deselect all but 'All'
		// If an item other than 'All' is selected, deselect 'All' if it is select
		// In other words, 'All' and other options are mutually exclusive.
		var locale = RS.incident.Dashboard.locale.ChartSettings;
		if (Array.isArray(value) && value.length > 1) {
			if(me.allSelected) { // || me.getValue()[0] == locale.all) {
				arguments[0] = value.filter(function(r) {
					return r.get(me.valueField) == locale.all;
				})
			} else {
				arguments[0] = value.filter(function(r) {
					return r.get(me.valueField) != locale.all;
				})
			}
		}
		arguments[1] = true;  // Make sure  the row will be selected/deselected in the list
		this.callParent(arguments);
	},

	listeners: {
		beforeselect: function( combo, record, index, eOpts ) {
			var locale = RS.incident.Dashboard.locale.ChartSettings;
			combo.allSelected = record.get(combo.valueField) == locale.all? true: false;
		}
	},

	initComponent: function() {
		var me = this;
		var locale = RS.incident.Dashboard.locale.ChartSettings;
		Ext.apply(this, {
			value: this.value || locale.all,
			fieldLabel: locale['investigation'+Ext.String.capitalize(me.filterOn)],
			name: me.filterOn+'Filter',
			displayField: me.filterOn,
			valueField: me.filterOn,
			multiSelect: true,
			allowBlank: false
		});
		me.callParent(arguments);
	}
});
Ext.define('RS.incident.Dashboard.Metric', {
    extend: 'Ext.container.Container',
    alias: 'widget.dashboardmetric',
    defaultType: 'label',
    layout: {
        type: 'vbox',
        align: 'center',
        pack: 'top'
    },
    defaults: {
        width: '100%',
        flex: 1
    },
    items: [{
        itemId:  'metric',
        style: 'text-align: center;font-size: 23px;font-weight: bold;'
    }, {
        itemId: 'metricLabel',
        style: 'text-align: center;font-size: 13px;font-weight: bold;color: #686eb3;'
    }],
    initComponent: function() {
        this.items[0].text = this.metric;
        this.items[1].text = this.metricLabel;
        this.callParent(arguments);
    },

    setMetric: function(val) {
        this.down('#metric').setText(val);
    }
});


Ext.define('RS.incident.Dashboard.AlertMetric', {
    extend: 'RS.incident.Dashboard.Metric',
    alias: 'widget.dashboardalertmetric',
    initComponent: function() {
        this.items[0].style = 'text-align: center;font-size: 22px;font-weight: bold;color:#f06f6f';
        this.callParent(arguments);
    }
});
Ext.define('RS.investigation.Overview', {
	extend: 'Ext.form.Panel',
	itemId: 'overview',
	xtype: 'overview',
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	defaults: {
		margin: '0 0 40 0'
	},
	items: [{
		xtype: 'fieldset',
		title: 'Summary',
		border: false,
		defaultType: 'displayfield',
		defaults: {			
			labelWidth: 130,
			margin : '5 5 0'
		},
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			fieldLabel: 'ID',
			name: 'id'
		}, {
			fieldLabel: 'Serverity',
			name: 'severity'
		}, {

			fieldLabel: 'Created',
			name: 'created'
		}, {

			fieldLabel: 'Data Compromised',
			name: 'comprosised'
		}, {

			fieldLabel: 'Investigation Type',
			name: 'type'
		}]
	}, {
		xtype: 'fieldset',
		title: 'Team',
		border: false,
		defaults: {
			xtype: 'displayfield',
			labelWidth: 130,
			margin : '5 5 0'
		},
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			fieldLabel: 'Created By',
			name: 'createdBy'
		}, {
			fieldLabel: 'Owner',
			name: 'owner'
		}, {

			fieldLabel: 'Members',
			itemId: 'members',
			name: 'members'
		}, {
			xtype: 'button',
			cls: 'sc-add-member-btn rs-small-btn rs-btn-dark',	
			text : 'Add Member',
			itemId: 'addMemberBtn',
			handler: function(btn) {
				var getCandidates = function(members, curMem) {
					var candidates = [];
					members.forEach(function(e) {
						if (curMem.indexOf(e) == -1) {
							candidates.push(e);
						}
					});
					return candidates;
				};
				var curMem = btn.up().down('#members').getValue().split('<br />') || [],
					members = btn.up('viewport').data.prospectiveMembers
				Ext.create('Ext.window.Window', {
					title: 'Add Member',
					height: 140,
					width: 300,
					modal: true,
					items: [{
						xtype: 'form',
						cls: 'sc-form',
						padding: '10 10 10 10',
						layout: {
							type: 'vbox',
							align: 'stretch'
						},
						itemId: 'addMember',
						items: {
							xtype: 'combo',
							itemId: 'memberInput',
							editable: false,
							fieldLabel: 'Member',
							labelWidth: 70,
							allowBlank: false,
							store: getCandidates(members, curMem)
						},
						buttons: [{
								text: 'Add',							
								viewport: btn.up('viewport'),
								formBind: true,
								handler: function(b) {
									var newMember = b.up('form').down('#memberInput').getValue();
									curMem.push(newMember);
									Ext.Ajax.request({
										scope: this,
										url: '/resolve/service/demo/saveJson',
										params: {
											companyName: 'Resolve',
											iRid: clientVM.activeScreen.parentVM.name,
											jsonContent: Ext.encode(curMem),
											type: 'members'
										},
										success: function(response, opts) {
											this.viewport.data.members = curMem;
											this.viewport.down('#members').setValue(curMem.join('<br />'));
											this.viewport.fireEvent('memberadded', curMem, newMember);
											this.up('window').close();
										},
										failure: function(response, opts) {
											clientVM.displayFailure(response);
										}
									});
								}
							}, {
							  	text: 'Cancel',
							  	handler: function(b) {
							  		b.up('window').close();
							  	}
							}
						]
				    }]
				}).show();
			}
		}]
	}, {
		xtype: 'fieldset',
		title: 'Associated Investigations',
		border: false,
		defaults: {
			xtype: 'displayfield',
			labelWidth: 130,
			fieldStyle: 'font-weight:normal;',
			margin : '5 5 0'
		},
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			name: 'associatedInvestigations'
		}]
	}, {
		xtype: 'fieldset',
		title: 'Associated SIEM Records',
		border: false,
		defaults: {
			xtype: 'displayfield',
			labelWidth: 130,
			fieldStyle: 'font-weight:normal;',
			margin : '5 5 0'
		},
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			name: 'associatedSIEMRecords'
		}]
	}, {
		xtype: 'fieldset',
		title: 'Associated ITSM  Tickets',
		border: false,
		defaults: {
			xtype: 'displayfield',
			labelWidth: 130,
			fieldStyle: 'font-weight:normal;',
			margin : '5 5 0'
		},
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [{
			name: 'associatedITSMTickets'
		}]
	}]
});

glu.namespace('RS.incident').locale = (function(
	investigationByType,
	investigationBySeverity,
	investigationByAge,
	investigationByTeamMember,
	investigationByOwner,
	investigationByStatus,
	investigationByPhases,
	investigationOverTimes,
	investigationBySLA,
	investigationByWorkload
) {
	return {
		additionalSir: 'Additional SIR',
		fileType: 'File Type',
		outputFileName: 'Output File Name',
		exportingAuditLogs: 'Exporting SIR Audit Logs',
		exportAll: 'Export',
		sirList: 'SIR list',
		add: 'Add',
		isAlreadyOnSIRList: ' is already on the SIR list',
		inValidSirName: 'The SIR name in this field is invalid',
		edit: 'Edit',
		createPhase: 'Create Phase',
		activityPhase: 'Phase',
		invalidPhase: 'Phase is required and may only contain alphanumeric characters, spaces, commas, dashes, or underscores.',
		invalidActivityName: 'Name is required and may only contain alphanumeric characters, spaces, commas, dashes, or underscores.',
		createActivity: 'Create Activity',
		runbook: 'Wiki Name',
		wikiName: 'Wiki Name',
		required: 'Required',
		activityName: 'Name',
		sla: 'SLA',
		id: 'ID',
		phaseActivity: 'Phase/Activity',
		description: 'Description',
		addPhase: 'Add Phase',
		addActivity: 'Add',
		addActivityModal: 'Add Activity',
		removeActivity: 'Remove',
		deletePhaseTitle: 'Delete Phase',
		deletePhaseBody: 'Deleting selected phase will delete all associated activities?',
		yes: 'Yes',
		cancel: 'Cancel',
		confirm: 'Confirm',
		save: 'Save',
		saveAs: 'Save As',
		reload: 'Reload',
		preview: 'Preview',
		returnToTemplates: 'Exit to Templates',
		exitEdit: 'Exit to View',
		readOnlyTxt: '(read-only)',
		saveAndCommit: 'Commit',
		copy: 'Copy',
		source: 'Source',
		addLayoutSection: 'Section',
		viewFileOptions: 'File',
		revision: 'Revision',
		serverError: 'Server Error: {0}',
		autoCreatedTitle: 'New',
		autoCreatedMessage: 'This playbook template does not currently exist, click save to create the template.',
		WikiSaved: 'Playbook template saved successfully.',
		reloadWithoutSaveBody: 'Reload will lose all changes you have made to the document.<br >Proceed to reload?',
		reloadWithoutSaveTitle: 'Reload without saving?',
		allMembersAdded: 'All prospective members have been added.',
		openGraphicReports: 'Open Graphic Reports in new browser tab',
		Activities: {
			moreTip: 'More',
			addNotes: 'Add Note',
			addArtifacts: 'Add Artifact',
			addAttachments: 'Upload Attachment',
			viewAutomationResults: 'View Automation Results',
		},

		playbookTemplates: 'Playbook Templates',
		createTemplate: 'New',
		editTemplate: 'Edit',
		deleteTemplate: 'Delete',
		templates: 'Templates',
		name: 'Name',
		namespace: 'Namespace',
		activatePB: 'Activate',
		inactivate: 'Inactivate',
		active: 'Active',
		inactive: 'Inactive',
		successfullyActivatePBs: 'Successfully activate playbook templates',
		successfullyInactivatePBs: 'Successfully inactivate playbook templates',
		listTemplatesError: 'Cannot get templates from the server:{0}',
		deleteTemplateMsg: 'Delete Selected Template?',
		deleteTemplatesMsg: 'Delete Selected Template?',
		deleteTemplateError: 'Cannot delete the selected schemas:{0}',
		deleteTemplateSuccess: 'The selected schemas have been deleted.',
		insufficientPermissionMsgTitle: 'Insufficient Permissions',
		insufficientPermissionMsg: 'You don\'t have permissions to view this page. Click close to go back to the previous page.',
		ReportTemplates: {
			reportTemplates : 'Report Templates',
			new : 'New',
			copy : 'Copy',
			purge : 'Purge',
			goToDashboard : 'Investigation Dashboard',
			listTemplatesError: 'Cannot get the list of Templates from the server.[{0}]',

			purgeTemplate : 'Purge',
			purgeMsg: 'Are you sure you want to purge the selected templates? Selected templates:{names}',
			purgesMsg: 'Are you sure you want to purge the selected templates? Selected templates({num} selected):{names}',
			purgeSucMsg: 'The selected records have been purged.',
			purgeErrMsg: 'Cannot purge the selected templates.',

			copyTemplate : 'Copy',
			copyMsg: 'Are you sure you want to copy the selected templates? Selected templates:{names}',
			copysMsg: 'Are you sure you want to copy the selected templates? Selected templates({num} selected):{names}',
			copySucMsg: 'The selected records have been copied.',
			copyErrMsg: 'Cannot copy the selected templates.',
		},
		// PlaybookTemplateNamer
		invalidNamespace: 'Namespace is required and may only contain alphanumeric characters, spaces, dashes, or underscores.',
		newTemplate: 'New Playbook Template',
		name: 'Name',
		namespace: 'Namespace',
		create: 'Create',
		saveGeneralSuccess: 'Successfully saved the general information of the playbook template.',
		saveSuccess: 'Successfully saved playbook template.',

		//Playbook Template
		lockedBy: '<b>{0}</b> is editing',
		lockedByTooltip: 'Click to override lock',
		overrideSoftLock: 'Override the soft lock',
		overrideSoftLockAction: 'Override',
		confirmOverride: 'Are you sure you want to lock the wiki?',

		// Misc
		uploadImageOnNotExistDoc: 'Please save the document before uploading images.',
		docNotExist: 'Document not saved',

		// Dashboard
		id: 'ID',
		playbook: 'Playbook',
		title: 'Title',
		origin: 'Origin',
		type: 'Type',
		primary: 'Primary',
		severity: 'Severity',
		status: 'Status',
		owner: 'Owner',
		createdOn: 'Created On',
		closedOn: 'Closed On',
		closedBy: 'Closed By',
		organization: 'Organization',
		add: 'Add',
		newInvestigation: 'Create New Investigation',
		externalReferenceId: 'External Reference ID',
		close: 'Close',
		cancel: 'Cancel',
		prospectiveMembers: 'Prospective Members',
		newSecurityIncident: 'Create',
		playbookTemplate: 'Playbook Template',
		associatedIRRecords: 'Associated IR Records',
		origin: 'Origin',
		owner: 'Owner',
		addedBy: 'Added By',
		addedOn: 'Added On',
		updatedBy: 'Updated By',
		updatedOn: 'Updated On',
		createdBy: 'Created By',
		createdOn: 'Created On',
		value: 'Value',
		dateOpened: 'Date Opened',
		changeType: 'Change Type',
		changeStatus: 'Change Status',
		changeOwner: 'Change Owner',
		apply: 'Apply',
		selectType: 'Select Type',
		selectStatus: 'Select Status',
		selectOwner: 'Select Owner',
		successfullyUpdatedIncident: 'Incident updated',
		failedToUpdatedIncident: 'Failed to update incident',
		attemptUpdatePlaybook: 'Playbook of an in progress incident can not be changed',
		successfullyCloseTicket: 'Successfully closed the ticket(s)',
		successfullySaveTicket: 'Successfully updating the ticket(s)',
		failedToCloseTicket: 'Failed to close one or more tickets',
		successfullyUpdatingPlaybookActivity: 'Successfully updating playbook activity',
		successfullyUpdatingPlaybookActivities: 'Successfully updating playbook activities',
		failedLoadingSecurityUserGroup: 'Failed loading the system \'s security user group',
		referenceId: 'Reference ID',
		reference: 'Reference ID',
		alertId: 'Alert ID',
		correlationId: 'Correlation ID',
		relatedInvestigations: 'Associated SIEM Records',
		loadRelatedInvestigationsFail: 'Failed to load related investigations',
		none: 'None',
		configureSecurityGroup : 'Configure Security Group',
		configureChartType: 'Configure Chart Type',


		// Dashboard Metrics
		irDashboard: 'Incident Response Dashboard',
		open: 'Open',
		inProgress: 'In Progress',
		closed: 'Closed',
		pastDue: 'Past Due',
		slaAtRisk: 'SLA at Risk',
		refresh: 'Refresh',
		play: 'Play',
		pause: 'Pause',
		configureMetrics: 'Configure Metrics',
		slaPolicy: 'SLA Policy',
		show: 'Show',

		// Dashboard charts
		investigationByType: investigationByType,
		investigationBySeverity: investigationBySeverity,
		investigationByAge: investigationByAge,
		investigationBySLA: investigationBySLA,
		investigationByWorkload: investigationByWorkload,
		configureChartSettings: 'Chart Settings',

		SecurityGroupConfiguration : {
			defaultGroup : 'Default Security Group',
			defaultOwner : 'Default Investigation Owner',
			incidentType : 'Security Incident Type',
			securityGroupConfiguration : 'Security Group Configuration'
		},

		// Playbook Summary
		activity: 'Activity',
		summary: 'Summary',
		dueDate: 'Due Date',
		dueBy: 'Due By',
		assignee: 'Assignee',
		status: 'Status',
		phase: 'Phase',
		team: 'Team',
		user: 'User',
		add: 'Add',
		edit: 'Edit',
		goToActivity: 'Go To Activity',
		notePreview: 'Note Preview',
		source: 'Source',
		severity: 'Severity',

		dataCompromised: 'Data Compromised',
		IPAddress: 'IP Address',
		description: 'Description',
		loggedOn: 'Logged On',
		members: 'Members',
		playbookDescription: 'Playbook Description',
		activitiesTab: 'Activities',
		notesTab: 'Notes',
		artifactsTab: 'Artifacts',
		attachmentsTab: 'Attachments',
		automationresultsTab: 'Automation Results',
		auditlogTab: 'Audit Log',

		auditlogLogFail: 'Failed to log event',
		ticketInfoLoadFail: 'Failed to load incident information',
		membersLoadFail: 'Failed to load incident members',
		securitygroupLoadFail: 'Failed to load security group members',
		toggleDataCompromised: 'Toggle',
		saveMembers: 'Save',
		saveActivity: 'Add',
		saveNote: 'Add',
		saveArtifact: 'Add',
		cancel: 'Cancel',
		invalidIncidentTitle: 'Incident does not exist',
		invalidIncident: 'The referenced incident ID is not valid or does not exist. Click close to return to the Incident Dashboard.',
		changeDataCompromisedSuccess: 'Data compromised status updated',
		changeDataCompromisedFail: 'Failed to change data compromised status',
		changeStatusSuccess: 'Status updated',
		changeStatusFail: 'Failed to change status',
		addMemberSuccess: 'Incident members saved',
		addMemberFail: 'Failed to save members',
		addRemoveMember: 'Manage Team',
		userId: 'User ID',
		displayName: 'Display Name',
		manageMembersModal: 'Manage Members',
		securityGroup: 'Security Group',
		loadActivitiesFail: 'Failed to load activities',
		addActivitySuccess: 'Activity added',
		addActivityFail: 'Failed to add activity',
		updateActivityFail: 'Failed to update activity',
		updateActivitySuccess: 'Activity updated',
		loadRunbooksFail: 'Failed to load runbooks',
		generateiFrameFail: 'Could not load Wiki contents',
		artifactsLoadFail: 'Failed to load artifacts',
		artifactAddSuccess: 'Artifact added',
		artifactAddFail: 'Failed to add artifact',
		artifactDeleteSuccess: 'Artifact(s) removed successfully',
		artifactDeleteFail: 'Failed to remove selected artifact(s)',
		addArtifact: 'Add artifact',
		addArtifactTitle: 'Add artifact to {0}',
		editArtifact: 'Edit artifact',
		notesLoadFail: 'Failed to load notes',
		noteAddSuccess: 'Note added',
		noteAddFail: 'Failed to add note',
		noteEditSuccess: 'Note updated',
		noteEditFail: 'Failed to update note',
		addNote: 'Add Note',
		addNoteTitle: 'Add note to {0}',
		editNote: 'Edit Note',

		addAttachment: 'Upload Attachments',
		attachmentsUploadSuccess: 'Attachments uploaded',
		attachmentsDeleteSuccess: 'Attachments removed successfully',
		attachmentsDeleteFail: 'Failed to remove selected attachment(s)',
		deleteAttachment: 'Remove',
		deleteAttachmentWarningMsg: 'Are you sure you want to remove the selected attachment(s)?',
		browse: 'Browse',
		malicious: 'Malicious',
		dragHere: 'DROP FILES HERE',
		filenames: 'Files',
		submit: 'Submit',
		upload: 'Upload',
		emptyAttachmentDescription: 'Enter a description',
		emptyFilename: 'Browse for files',
		UploadError: 'Upload Error',
		attachmentsUpdateSuccess: 'Attachment updated',
		attachmentsUpdateFail: 'Attachment update failed',
		attachmentsUploadNotAllowed: 'Upload not allowed',
		attachmentsUploadCancelled: 'Upload cancelled',

		noDownloadTitle: 'Upload not done',
		noDownloadBody: 'Please wait for the upload to complete before downloading this file',

		auditlogLoadFail: 'Failed to load audit log',
		doubleClickExpand: 'Double click to expand',

		//Audit Log Strings
		incidentOpenedLog: 'User {0} opened incident {1}',
		incidentStatusChangedLog: 'User {0} changed the incident status to {1}',
		incidentOwnerChangedLog: 'User {0} changed the incident owner to {1}',
		incidentTypeChangedLog: 'User {0} changed the incident type to {1}',

		addActivityLog: 'User {0} added user activity {1}',
		editActivityLog: 'User {0} changed the {1} of activity {2} to {3}',

		addNoteLog: 'User {0} added a note: {1}',
		editNoteLog: 'User {0} edited a note to: {1}',

		addArtifactLog: 'User {0} added artifact {1} with value: {2}',
		editArtifactLog: 'User {0} edited the value of artifact {1} to: {2}',

		attachmentUploadLog: 'User {0} added {1} {2}: {3}',

		addMemberLog: 'User {0} added member {1}',
		dataCompromisedChangedLog: 'User {0} changed the Data Compromised status to {1}',

		addAttachmentsTitle: 'Upload Attachment to {0}',
		viewAutomationResultsTitle: 'Automation Results for {0}',

		Attachments : {
			activityFilter : 'Filter on Activity:'
		},
		AutomationResults : {
			activityFilter : 'Filter on Activity'
		},
		Artifacts : {
			lookupTootltip : 'Look up incident on this name and value'
		},

		SIEMData: {
			siemdataTab: 'SIEM Data',
			rawData: 'Raw SIEM Data:',
			noRequestData: 'No data present.'
		},

		//Incident Lookup
		noRecordFound : 'No record found for artifact <b>{0}</b> with value <b>{1}</b>',
		lookupInfo : 'Found {0} incident(s) that use artifact <b>{1}</b> with value <b>{2}</b>.',
		expandResultInTab : 'Show full results in new tab.',
		investigationByType: investigationByType,
		investigationBySeverity: investigationBySeverity,
		investigationByAge: investigationByAge,
		investigationByTeamMember: investigationByTeamMember,
		investigationByOwner: investigationByOwner,
		investigationByStatus: investigationByStatus,
		investigationByPhases: investigationByPhases,
		investigationOverTimes: investigationOverTimes,
		investigationBySLA: investigationBySLA,
		investigationByWorkload: investigationByWorkload,
		barGraph: 'Bar Graph',
		pieChart: 'Pie Chart',

		//Print Preview
		notesStoreIsEmpty : 'There is no notes added.',
		artifactsStoreIsEmpty : 'There is no artifacts added.',
		attachmentsStoreIsEmpty : 'There is no attachments uploaded.',
		noActivityDefined : '( There is no activities for this phase. )',
		automationResultsStoreIsEmpty : 'There is no automation results',
		size : 'Size',
		taskSummary : 'Description',
		result : 'Result',
		uploadedOn : 'Uploaded On',
		uploadedBy : 'Uploaded By',
		exportPDF : 'Export to PDF',
		convertPDFMsg : 'Converting Incident Record to PDF format. Please wait.',
		convertPDFTitle : 'Export to PDF',
		abort : 'Abort',

		section: 'Section',
		procedure: 'Procedure',
		remove: 'Remove',

		download: 'Download',

		//Artifacts
		automation : 'Automation',
		actiontask : 'ActionTask',
		keyReqs : 'Key is required and needs to be valid.',
		invalidName: 'Name is required and must be in alphanumerics, but may contain single spaces, underscores \'_\' or dash \'-\' between alphanumeric characters.',
		duplicatedType : 'Type cannot be duplicated.',
		duplicatedName : 'Name cannot be duplicated.',
		valueReqs : 'Value is required.',
		uname : 'Name',
		ushortName : 'Key',
		ufullName : 'Full Name',
		uartifactTypeSource : 'Source',
		udescription : 'Description',
		type : 'Type',
		automation : 'Automation',
		executionType : 'Execution Type',
		parameter : 'Mapped Parameter',
		name : 'Name',
		group : 'Group',
		createNewType : 'Create New Type',
		nameReqs : 'Name is required.',
		sourceReqs : 'Source is required.',
		removeArtifactTitle: 'Confirm Remove Artifact',
		removeArtifactBody: 'Are you sure you want to remove selected artifacts? This action cannot be undone.',
		executionSubmitSucc : 'The execution is successfully submitted.',
		ArtifactConfiguration : {
			articfactConfigTitle : 'Artifact Configuration',
			newType : 'New Type'
		},
		ArtifactConfigEntry : {
			aliasKeys : 'Keys',
			addAlias: 'Add',
			removeAlias : 'Remove',
			action : 'Action Definition',
			addAction : 'Add',
			removeAction : 'Remove'	,
			comfirmRemoveTypeTitle : 'Confirm Remove Artifact Type',
			comfirmRemoveTypeBody : 'Are sure you want to remove artifact type <b>{0}</b>. This action cannot be undone.',
			comfirmRemoveAliasTitle : 'Confirm Remove Key',
			comfirmRemoveAliasBody : 'Are sure you want to remove key <b>{0}</b>. This action cannot be undone.',
			comfirmRemoveActionTitle : 'Confirm Remove Action',
			comfirmRemoveActionBody : 'Are sure you want to remove action <b>{0}</b>. This action cannot be undone.'
		},
		NewAliasForm : {
			newAliasForm : 'New Key',
			CEF : 'CEF Standard',
			CUSTOM : 'Custom',
			keySaveSuccess : 'Key is saved successfully.'
		},
		NewActionForm : {
			newActionForm : 'New Action',
			invalidAutomation : 'Automation needs to have at least 1 parameter.',
			invalidActiontask : 'ActionTask needs to have at least 1 input parameter.',
			automationReqs : 'Automation is required.',
			actiontaskReqs : 'ActionTask is required.',
			parameterReqs : 'Parameter is required.',
			actionSaveSuccess : 'Action is {0} successfully.'
		},
		export : 'Export',
		ExportDashboard : {
			info : 'Select columns to include in the PDF:',	
			cancel : 'Cancel',
			sir : 'ID',
			id : 'System ID',
			playbook : 'Playbook',
			title : 'Title',
			sourceSystem : 'Origin',
			investigationType : 'Type',
			severity : 'Severity',
			status : 'Status',
			sla : 'SLA',
			dueBy : 'Due Date',
			owner : 'Owner',
			sysCreatedOn : 'Created On',
			closedOn : 'Closed On',
			closedBy : 'Closed By',
			sysOrg : 'Organization'
		}
	};

})(
	'Investigations by Type',
	'Investigations by Severity',
	'Investigations by Age',
	 'Investigations by Team Member',
	'Investigations by Owner',
	'Investigations by Status',
	'Investigations by Phases',
	'New Investigations Over Times',
	'Investigations by SLA',
	'Investigations by Workload'
);

glu.namespace('RS.incident.Dashboard').locale = {
	MetricsSetting: {
		show: 'Show',
		metricsSetting: 'Metrics Setting',
		apply: 'Apply',
		cancel: 'Cancel',
		dashboardShowOpen: 'Open',
		dashboardShowInProgress: 'In Progress',
		dashboardShowClosed: 'Closed',
		dashboardShowPastDue: 'Past Due',
		dashboardShowSLAatRisk: 'SLA at Risk',
		sLAatRiskPolicy: 'SLA at Risk Policy',
		scope: 'Scope',
		between1To365days:'Enter a value from 1 to 365 (days)',
		between1To24Hours: 'Enter a value from 1 to 24 (hours)',
		between1To120Minutes: 'Enter a value from 1 to 120 (minutes)'
	},

	Panel: {
		configureLabel: 'Configure'
	},

	ChartSettings: {
		chartSettings: 'Chart Settings',
		chartType: 'Investigation Report',
		investigationByType: 		RS.incident.locale.investigationByType,
		investigationBySeverity: 	RS.incident.locale.investigationBySeverity,
		investigationByAge: 		RS.incident.locale.investigationByAge,
		investigationByTeamMember: 	RS.incident.locale.investigationByTeamMember,
		investigationByOwner: 		RS.incident.locale.investigationByOwner,
		investigationByStatus: 		RS.incident.locale.investigationByStatus,
		investigationByPhases: 		RS.incident.locale.investigationByPhases,
		investigationOverTimes: 	RS.incident.locale.investigationOverTimes,
		investigationBySLA: 		RS.incident.locale.investigationBySLA,
		investigationByWorkload: 	RS.incident.locale.investigationByWorkload,
		investigationType: 'Investigation Type',
		all: 'All',
		dynamicUpdateBasedOnScope: 'Dynamic Update Based on Scope',
		investigationSeverity: 'Investigation Severity',
		critical: 'Critical',
		high: 'High',
		medium: 'Medium',
		low: 'Low',
		investigationOwner: 'Investigation Owner',
		listResolveUsers: 'List Resolve Users',
		investigationTeam: 'Investigation Team',
		investigationStatus: 'Investigation Status',
		open: 'Open',
		inProgress: 'In Progress',
		closed: 'Closed',
		configureChartSettings: 'Chart Settings',
		scope: 'Scope',
		numberOfCols: 'Number of Columns',
		weeks: 'Weeks',
		days: 'Days',
		hours: 'Hours',
		minutes: 'Minutes',
		filters: 'Filters',
		apply: 'Apply',
		cancel: 'Cancel',
		hour: 'Hour(s)',
		day: 'Day(s)',
		week: 'Week(s)',
		graphType: 'Graph Type',
		barGraph: 'Bar Graph',
		pieChart: 'Pie Chart',
		between1To365days:'Enter a value from 1 to 365 (days)',
		between1To23Hours: 'Enter a value from 1 to 23 (hours)',
		between1To59Minutes: 'Enter a value from 1 to 59 (minutes)',
		failedLoadingSecurityUserGroupAndTypes: 'Failed loading the system\'s SIR user group and types.',
		failedLoadingSecurityTypes: 'Failed loading the system\'s SIR types.',
		inValidNumberOfCols: 'Number of Columns must be from 1 to 10.',
		noDataMsg: 'The chart contains no data.'
	},
	AgeBucketBuilder: {
		ageBuckets: 'Age Buckets',
		column: 'Column',
		newTimeFrame: 'Add New Timeframe',
		deleteLastTimeframe: 'Remove Last Timeframe'
	},
	TimeFrame: {
		to: 'To',
		priorTimeFrameEnd: 'Prior timeframe end'
	},

	// KPI
	KPI: {
		addSection: 'Section',
		configureSettings: 'Edit Settings',
		save: 'Save',
		irDashboard: 'Analytic Reports',
		irDashboardBuilder: 'Analytic Reports Builder',
		refresh: 'Refresh',
		play: 'Play',
		pause: 'Pause',
		exitEdit: 'Exit to View'
	},

	KPISectionEntry: {
		addGraph: 'Graph',
		deleteSection: 'Delete Section'
	},
	addKPIEntry: 'Graph',
	deleteSection: 'Delete',

	investigationByType: 		RS.incident.locale.investigationByType,
	investigationBySeverity: 	RS.incident.locale.investigationBySeverity,
	investigationByAge: 		RS.incident.locale.investigationByAge,
	investigationByTeamMember: 	RS.incident.locale.investigationByTeamMember,
	investigationByOwner: 		RS.incident.locale.investigationByOwner,
	investigationByStatus: 		RS.incident.locale.investigationByStatus,
	investigationByPhases: 		RS.incident.locale.investigationByPhases,
	investigationOverTimes: 	RS.incident.locale.investigationOverTimes,
	investigationBySLA: 		RS.incident.locale.investigationBySLA,
	investigationByWorkload: 	RS.incident.locale.investigationByWorkload,

	name : 'Name',
	
};

glu.namespace('RS.incident.PlaybookTemplate').locale = {
	description: 'Description',
	AutomationFilter : {
		automationFilter : 'Automation Result Filter',
		resultConfiguration: 'Result',
		description: 'Description',
		addActionTask: 'Add Task',
		addGroup: 'Add Group',
		remove: 'Remove',
		filter: 'Filter',
		filterValue: 'Severity Filter',
		title: 'Title',
		preserveTaskOrder: 'Preserve Task Order',
		order: 'Order',
		refreshInterval: 'Refresh Interval',
		refreshCountMax: 'Refresh Attempts',
		descriptionWidth: 'Description Width',
		autoHide: 'Auto Hide',
		actionTasks: 'Action Tasks',
		actionTaskFilters: 'Action Task Filters',
		encodeSummary: 'Encode Summary',
		GOOD: 'GOOD',
		WARNING: 'WARNING',
		SEVERE: 'SEVERE',
		CRITICAL: 'CRITICAL',

		ASC: 'Ascending',
		DESC: 'Descending',

		newTask: 'New Task',
		newGroup: 'New Group',

		tags: 'Tags',
		addTag: 'Add Tag',
		removeTag: 'Remove',
		edit: 'Edit',

		progress: 'Progress',

		showAllActionTasks: 'Show All Action Tasks in Current Worksheet',
		includeStartEndTasks: 'Include Hidden Action Tasks',

		showResultsFrom: 'Show Results From',
		showAnyWiki: 'Any Automation',
		showCurrentWikiOnly: 'Current Automation Only',
		showCustomWikiOnly: 'Only this Automation'
	}
}

