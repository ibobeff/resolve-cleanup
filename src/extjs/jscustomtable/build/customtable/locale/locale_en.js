/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.customtable').locale = {

	showList: '← Back',
	cancel: 'Cancel',
	error: 'Error',
	communicationError: 'There was an error communicating with the server',
	save: 'Save',

	//Column headers
	editColumn: 'Edit',
	created: 'Created',
	number: 'Number',
	condition: 'Condition',
	severity: 'Severity',
	summary: 'Summary',
	reference: 'Reference',
	alertId: 'Alert ID',
	correlationId: 'Correlation ID',
	assignedTo: 'Assigned To',
	systemId: 'System ID',


	//Actions
	newAction: 'New',
	editAction: 'Edit',
	createView: 'New',
	deleteView: 'Delete',

	deleteTitle: 'Confirm Delete',
	deleteTableMessage: 'Are you sure you want to delete the selected table?  This action cannot be undone.',
	deletesTableMessage: 'Are you sure you want to delete the {num} selected tables?  This action cannot be undone.',

	deletedTable: 'Custom table deleted sucessfully',
	deletedTables: 'Custom tables deleted sucessfully',

	deleteAction: 'Delete',
	setActive: 'SetActive',

	actionCompleted: 'Action executed successfully',

	deleteViewMessage: 'Are you sure you want to delete the selected view?  This action cannot be undone.',
	deletesViewMessage: 'Are you sure you want to delete the {0} selected views?  This action cannot be undone.',

	views: 'Views',

	deletesMessage: 'Are you sure you want to delete the {0} selected records?',
	deleteMessage: 'Are you sure you want to delete the selected record?',

	//ViewLookup.jsp
	ViewLookupTable: 'View Lookup Table',
	id: 'ID',
	appName: 'Application Name',
	view: 'View',
	roles: 'Roles',
	NewLookupItem: 'New Lookup',
	EditLookupItem: 'Edit Lookup',

	appNameRequired: 'Please provide an application name',
	viewRequired: 'Please provide a view',
	rolesRequired: 'Please choose at least one role',

	//Definition
	ServerError: 'Server Error',
	u_name: 'Name',
	utype: 'Type',
	displayName: 'Display Name',
	u_display_name: 'Display Name',
	createRecord: 'New',
	deleteRecords: 'Delete',

	back: 'Back',
	save: 'Save',

	TableDefinition: {
		windowTitle: 'Custom Table',
		customTableDefinition: 'Custom Table',
		columns: 'Columns'
	},

	//Table View Definition
	utableName: 'Table Name',
	Main: {
		displayName: 'Custom Table',
		windowTitle: 'Custom Table',
		ListRecordsErr: 'Cannot get custom table view definitions from the server.'
	},
	ViewDefinition: {
		windowTitle: 'Custom Table View',
		customTableDefinition: 'Custom Table',
		columns: 'Columns',

		createFormId: 'Create Form',
		editFormId: 'Edit Form',

		target: 'Target',
		advanced: 'Advanced Configuration',
		metaNewLink: 'Create Url',
		metaFormLink: 'Edit Url',

		sameWindow: 'Same Window',
		newWindow: 'New Window',

		AccessRights: 'Access Rights',
		addAccessRight: 'Add',
		removeAccessRight: 'Remove',

		savedViewDefinition: 'View Definition successfully saved'
	},

	name: 'Name',
	tableName: 'Table Name',

	Name: 'Name',
	adminRight: 'Admin',
	editRight: 'Edit',
	viewRight: 'View',
	executeRight: 'Execute',

	viewForms: 'Forms',
	viewView: 'View',
	SaveTableDefintionSuc: 'Custom Table has successfully saved'
}