/*
******************************************************************************
 (C) Copyright 2016

 Resolve Systems, LLC

 All rights reserved
 This software is distributed under license and may not be used, copied,
 modified, or distributed without the express written permission of
 Resolve Systems, LLC.

******************************************************************************
*/
Ext.define('RS.customtable.Table', {
	extend: 'Ext.data.Model',
	idProperty: 'sys_id',
	fields: ['sys_id']
});
Ext.define('RS.customtable.ViewLookupTable', {
	extend: 'Ext.data.Model',
	idProperty: 'sys_id',
	fields: ['sys_id', 'u_app_name', 'u_view_name', 'u_roles', {
		name: 'u_order',
		type: 'number'
	}].concat(RS.common.grid.getSysFields())
});
glu.defModel('RS.customtable.Main', {
	displayName: '',

	columns: [],

	stateId: 'customtabletabledefinitions',

	store: {
		mtype: 'store',

		fields: ['uname', 'udisplayName'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'uname',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/customtable/list',
			reader: {
				type: 'json',
				root: 'records',
				totalProperty: 'total',
				successProperty: 'success',
				messageProperty: 'message'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	recordsSelections: [],
	wait: false,

	activate: function() {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.store.load();
	},

	init: function() {
		this.set('displayName', this.localize('displayName'));

		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'udisplayName',
			filterable: true,
			sortable: true,
			flex: 2
		}, {
			header: '~~tableName~~',
			filterable: true,
			sortable: true,
			dataIndex: 'uname',
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))

		this.store.on('beforeload', function(store, operation) {
			operation.params = operation.params || {}
			Ext.apply(operation.params, {
				modelName: 'CustomTable',
				useSql: false,
				type: 'table',
				orderBy: 'u_order ASC'
			})
		}, this)
	},

	editRecord: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.customtable.TableDefinition',
			params: {
				id: id
			}
		})
	},

	createRecord: function() {
		clientVM.handleNavigation({
			modelName: 'RS.customtable.TableDefinition',
			params: {
				id: ''
			}
		})
	},

	deleteRecords: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.recordsSelections.length > 1 ? 'deletesTableMessage' : 'deleteTableMessage', {
				num: this.recordsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},

			fn: this.sendDeleteReq
		})
	},

	sendDeleteReq: function(btn) {
		if (btn != 'yes')
			return;

		this.set('wait', true)

		var ids = [];

		Ext.each(this.recordsSelections, function(r) {
			ids.push(r.get('id'));
		})

		this.ajax({
			url: '/resolve/service/customtable/delete',
			params: {
				ids: ids,
				all: false
			},
			scope: this,
			success: function(r) {
				this.handleDeleteSucces(r, ids.length)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false)
			}
		})
	},

	handleDeleteSucces: function(r, len) {
		this.set('wait', false);
		var response = RS.common.parsePayload(r);
		if (!response.success)
			clientVM.displayError(response.message)
		else
			clientVM.displaySuccess(len === 1 ? this.localize('deletedTable') : this.localize('deletedTables'))
		this.store.load()
	},

	createRecordIsEnabled$: function() {
		return !this.wait;
	},

	deleteRecordsIsEnabled$: function() {
		return this.recordsSelections.length > 0;
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	view: function(id) {
		if (!Ext.isString(id) && this.recordsSelections.length > 0) id = this.recordsSelections[0].get('id')

		if (Ext.isString(id))
			clientVM.handleNavigation({
				modelName: 'RS.customtable.Table',
				params: {
					table: this.store.getById(id).get('uname'),
					view: 'Default'
				}
			})
	}
})
glu.defModel('RS.customtable.Table', {
	API : {
		list : '/resolve/service/customtable/list',
		read : '/resolve/service/view/read',
		submit : '/resolve/service/form/submit',
		deleteData : '/resolve/service/customtable/deletedata'
	},
	table: 'Custom Table',
	displayName: '',
	tableSysId: 'table_sys_id',
	view: 'Default',
	detailUrl: '',
	metaNewLink: '',
	metaFormLink: '',
	createFormId: '',
	editFormId: '',

	activeCard: 0,
	showList: function() {
		this.set('activeCard', 0)
	},
	showDetails: function() {
		this.set('activeCard', 1)
	},
	gridSelections: [],
	allSelected: false,

	customTableStore: {
		mtype: 'store',
		model: 'RS.customtable.Table',
		remoteSort: true,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/dbapi/getRecordData',
			reader: {
				type: 'json',
				root: 'records',
				totalProperty: 'total',
				successProperty: 'success',
				messageProperty: 'message'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	columns: [],
	serverData: {},

	fields: ['displayName', 'metaNewLink', 'metaFormLink', 'createFormId', 'editFormId', 'target'],
	target: 'default',

	toolbarItems: {
		mtype: 'list'
	},

	formFields: [],

	activate: function(screen, params) {
		if (params && params.table) this.set('table', params.table)
		if (params && params.view) this.set('view', params.view)

		while (this.toolbarItems.length > 2) this.toolbarItems.removeAt(2)

		this.set('activeCard', 0)

		this.ajax({
			url: this.API['read'],
			params: {
				table: this.table,
				view: this.view
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					if (response.data && response.data.fields) {
						this.loadData(response.data)
						this.set('serverData', response.data)

						if (!this.createFormId && !this.metaNewLink) this.set('metaNewLink', '/resolve/service/wiki/view/${table}/${view}?sys_id=${sys_id}&view=${view}&table=${table}')
						if (!this.editFormId && !this.metaFormLink) this.set('metaFormLink', '/resolve/service/wiki/view/${table}/${view}?sys_id=${sys_id}&view=${view}&table=${table}')

						if (this.createFormId) this.newForm.activate(null, {
							formId: this.createFormId
						})

						this.configureColumnsAndButtons()
					}
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})

		this.ajax({
			url: this.API['list'],
			params: {
				start: 0,
				limit: 1,
				filter: Ext.encode([{
					field: 'uname',
					condition: 'equals',
					value: this.table
				}])
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					if (response.records && response.records.length > 0) {
						this.set('tableSysId', response.records[0].id)
					}
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(r){
				clientVM.displayFailure(r);
			}
		})
	},

	configureColumnsAndButtons: function() {
		// this.set('editFormId', '8a80dac03a98d3d2013a98dce5f4000a') //Content Request for testing
		if (this.editFormId) {
			this.editForm.activate(null, {
				formId: this.editFormId
			})
		} else {
			var modelFields = [],
				columns = [];
			configureColumns(this.serverData.fields, modelFields, columns, false, true);
			this.customTableStore.model.setFields(modelFields, 'sys_id')
			this.set('columns', columns) //setting columns will reconfigure which calls load automatically
		}
	},

	init: function() {
		var me = this;
		me.customTableStore.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}
			Ext.apply(operation.params, {
				tableName: me.table,
				useSql: false,
				type: 'table'
			})
		})

		this.editForm.on('formLoaded', function() {
			var modelFields = [],
				columns = [],
				form = this.editForm.form.viewSpec,
				buttons = form.buttons || form.tbar.items;

			configureColumns(this.serverData.fields, modelFields, columns, false, true, form);
			this.customTableStore.model.setFields(modelFields, 'sys_id')
			this.set('columns', columns)

			this.customTableStore.load()

			while (this.toolbarItems.length > 2) this.toolbarItems.removeAt(2)

			Ext.Array.forEach(buttons || [], function(button) {
				if (button.customTableDisplay) {
					this.toolbarItems.add(this.model({
						mtype: 'viewmodel',
						text: button.displayName,
						button: button,
						action: function() {
							var count = 0,
								len = this.parentVM.gridSelections.length,
								callback = function() {
									if (count + 1 == len)
										clientVM.displaySuccess(this.parentVM.localize('actionCompleted'))
								}
							Ext.Array.forEach(this.parentVM.gridSelections, function(selection) {
								this.ajax({
									url: this.API['submit'],
									jsonData: {
										viewName: this.parentVM.view,
										formSysId: this.parentVM.editFormId,
										tableName: this.parentVM.table,
										userId: '',
										timezone: '',
										crudAction: this.button.crudAction,
										query: '',
										controlItemSysId: this.button.sysId,
										dbRowData: {
											data: {
												sys_id: selection.get('sys_id')
											}
										},
										sourceRowData: {
											data: {
												'RESOLVE.ORG_NAME' : clientVM.orgName,
												'RESOLVE.ORG_ID' : clientVM.orgId
											}
										}										
									},
									success: function(r) {
										var response = RS.common.parsePayload(r)
										if (response.success) callback()
										else clientVM.displayError(response.message)
									},
									failure: function(r) {
										clientVM.displayFailure(r);
									}
								})
							}, this)
						},
						actionIsEnabled$: function() {
							return this.parentVM.gridSelections.length > 0 && !this.parentVM.allSelected
						}
					}))
				}
			}, this)
		}, this)

		this.toolbarItems.add(this.model({
			mtype: 'viewmodel',
			text: this.localize('newAction'),
			action: function() {
				this.parentVM.newAction()
			}
		}))

		this.toolbarItems.add(this.model({
			mtype: 'viewmodel',
			text: this.localize('deleteAction'),
			action: function() {
				this.parentVM.deleteAction()
			},
			actionIsEnabled$: function() {
				return this.parentVM.gridSelections.length > 0
			}
		}))
	},

	newAction: function() {
		if (this.createFormId) {
			clientVM.handleNavigation({
				modelName: 'RS.formbuilder.Viewer',
				params: {
					formId: this.createFormId,
					recordId: '',
					embed: true
				}
			})
			// this.newForm.resetForm()
			// this.set('activeCard', 2)
		} else {
			//Create a new record
			var url = this.randomizeUrl(this.metaNewLink.replace(/\${table}/g, this.table).replace(/\${view}/g, this.view).replace(/\${sys_id}/g, ''));
			if (this.target == 'default') {
				this.openTokenizedUrl(url);
				//Switch to the card behind the scenes with a blank sys_id
				// this.set('detailUrl', url)
				// this.showDetails()
			} else window.open(url)
		}
	},
	newActionIsEnabled$: function() {
		return this.metaNewLink || this.createFormId
	},
	editAction: function(sys_id) {
		if (this.editFormId) {
			clientVM.handleNavigation({
				modelName: 'RS.formbuilder.Viewer',
				params: {
					formId: this.editFormId,
					recordId: sys_id,
					embed: true
				}
			})
			// this.set('activeCard', 3)
			// this.editForm.setFormRecordId(sys_id)
		} else {
			//Edit selected record
			var url = this.randomizeUrl(
				this.metaFormLink.replace(/\${table}/g, this.table)
				.replace(/\${view}/g, this.view)
				.replace(/\${sys_id}/g, sys_id)
				.replace(/\?sys_id=/g, '?recordId=')   // SUB-292 sys_id is to also get the WS. We only want the record.
				, {});
			if (url.indexOf('/resolve/') === 0 && url.indexOf('OWASP_CSRFTOKEN') === -1) {
				const urlData = url.split('#');
				const uriData = urlData[0].split('?');
    			clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
    				var csrftoken = '?' + data[0] + '=' + data[1];
    				var tokenizedData = uri + csrftoken;
    				if (uriData.length > 1) {
    					tokenizedData += '&' + uriData[1];
    				}
    				if (urlData.length > 1) {
    					tokenizedData += '#' + urlData[1];
    				}
    				url = tokenizedData;
    			});
    		}
			if (this.target == 'default') {
				this.openTokenizedUrl(url);
				//Switch to the card behind the scenes with the provided sys_id
				// this.set('detailUrl', url)
				// this.showDetails()
			} else window.open(url)
		}
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll);
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false);
	},

	deleteAction: function() {
		//Delete the selected record(s), but first confirm the action
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.gridSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.gridSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		});
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			var ids = [];
			Ext.each(this.gridSelections, function(selection) {
				ids.push(selection.get('sys_id'));
			});
			this.ajax({			
				url: this.API['deleteData'],
				params: {
					tableName: this.table,
					ids: ids
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.customTableStore.load()
					else clientVM.displayError(response.message)
				},
				failure: function(r){
					clientVM.displayFailure(r);
				}
			});
		}
	},
	deleteActionIsEnabled$: function() {
		return this.gridSelections.length > 0
	},
	reloadAndShowList: function() {
		this.customTableStore.load()
		this.showList()
		return false
	},
	randomizeUrl: function(url, extraParams) {
		var val = url,
			split = url.split('?');
		if (split.length > 1) {
			var params = Ext.Object.fromQueryString(split[1]);
			params['a' + Math.random()] = Math.random();
			Ext.applyIf(params, extraParams)
			val = split[0] + '?' + Ext.Object.toQueryString(params);
		}
		return val
	},

	viewSelected: function(data) {
		this.set('serverData', data)
		this.set('view', data.name)
		this.set('metaFormLink', data.metaFormLink)
		this.set('metaNewLink', data.metaNewLink)
		this.set('createFormId', data.createFormId)
		this.set('editFormId', data.editFormId)
		this.set('displayName', data.displayName)
		this.configureColumnsAndButtons()
	},

	openTokenizedUrl: function(url) {
        if (url) {
          var split = url.split('?');
          if (split.length > 1) {
            clientVM.getCSRFToken_ForURI(split[0], function(data){
				clientVM.handleNavigation({
					modelName: 'RS.customtable.WikiViewer',
					params: {
						url: split[0] + '?' + data[0] + '=' + data[1] + '&' + split[1]
					}
				})
            })
          }
        }
	},

	newForm: {
		mtype: 'RS.formbuilder.Viewer',
		embed: true,
		formdefinition: true
	},
	editForm: {
		mtype: 'RS.formbuilder.Viewer',
		embed: true,
		formdefinition: true
	}
});
glu.defModel('RS.customtable.TableDefinition', {
	wait: false,

	title$: function() {
		return this.localize('customTableDefinition') + (this.u_name ? ' - ' + this.u_name : '')
	},

	propCollapsed: false,

	id: '',
	name: '',

	sys_created_on: '',
	sys_created_by: '',
	sys_updated_on: '',
	sys_updated_by: '',
	sysOrganizationName: '',

	u_name: '',
	u_display_name: '',
	u_meta_table_sys_id: '',

	defaultData: {
		id: '',
		u_meta_table_sys_id: '',

		sys_created_on: '',
		sys_created_by: '',
		sys_updated_on: '',
		sys_updated_by: '',
		sysOrganizationName: '',

		u_name: '',
		u_display_name: ''
	},

	fields: ['u_name', 'u_display_name'].concat(RS.common.grid.getCustomSysFields()),
	store: {
		mtype: 'store',
		fields: ['u_name', 'u_display_name', 'u_type'].concat(RS.common.grid.getCustomSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/dbapi/getRecordData',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	columns: [{
		text: '~~u_name~~',
		dataIndex: 'u_name',
		flex: 1
	}, {
		text: '~~u_display_name~~',
		dataIndex: 'u_display_name',
		flex: 1
	}, {
		text: '~~utype~~',
		dataIndex: 'u_type',
		flex: 1
	}].concat(RS.common.grid.getCustomSysColumns()),

	views: {
		mtype: 'store',
		remoteSort: true,
		fields: ['name'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/view/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	viewsSelections: [],
	allSelected: false,

	viewsColumns: [{
		text: '~~u_name~~',
		dataIndex: 'name',
		flex: 1
	}], //.concat(RS.common.grid.getSysColumns()),

	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.resetForm()
		this.set('id', params && params.id ? params.id : '')
		this.set('name', params && params.name ? params.name : '')
		this.loadRecord()
	},

	init: function() {
		this.store.on('beforeload', function(store, operation) {
			operation.params = operation.params || {}
			Ext.apply(operation.params, {
				//modelName: 'MetaField',
				tableName: 'meta_field',
				type: 'table',
				filter: Ext.encode([{
					field: 'u_custom_table_sys_id',
					condition: 'equals',
					type: 'auto',
					value: this.id
				}])
			})
		}, this)

		this.views.on('beforeload', function(store, operation) {
			operation.params = operation.params || {}
			Ext.apply(operation.params, {
				table: this.u_name
			})
			// Ext.apply(operation.params, {
			// 	modelName: 'MetaTableView',
			// 	type: 'table',
			// 	filter: Ext.encode([{
			// 		field: 'u_meta_table_sys_id',
			// 		condition: 'equals',
			// 		type: 'auto',
			// 		value: this.u_meta_table_sys_id
			// 	}])
			// })
		}, this)
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	loadRecord: function() {
		if (this.id || this.name) {
			this.set('wait', true)
			this.store.removeAll()
			this.views.removeAll()

			this.ajax({
				url: '/resolve/service/dbapi/getRecordData',
				params: {
					//modelName: 'CustomTable',
					tableName: 'custom_table',
					type: 'form',
					start: 0,
					limit: 1,
					filter: Ext.encode([{
						field: this.id ? 'sys_id' : 'u_name',
						condition: 'equals',
						type: 'auto',
						value: this.id || this.name
					}])
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (!response.success)
						clientVM.displayError(response.message)
					else {
						this.loadData(response.data)
						this.store.load()
						this.ajax({
							url: '/resolve/service/dbapi/getRecordData',
							params: {
								//modelName: 'MetaTable',
								tableName: 'meta_table',
								start: 0,
								limit: 1,
								type: 'form',
								filter: Ext.encode([{
									field: 'u_name',
									type: 'auto',
									condition: 'equals',
									value: this.u_name
								}])
							},
							success: function(r) {
								var response = RS.common.parsePayload(r)
								if (response.success) {
									if (response.data) {
										this.set('u_meta_table_sys_id', response.data.id)
										this.views.load()
									}
								} else {
									clientVM.displayError(response.message);
								}
							},
							failure: function(r){
								clientVM.displayFailure(r);
							}
						})
					}
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				},
				callback: function() {
					this.set('wait', false)
				}
			})
		}
	},

	saveRecord: function(exitAfterSave) {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/customtable/save',
			jsonData: this.asObject(),
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success)
					clientVM.displayError(this.localize('SaveTableDefintionErr') + '[' + respData.message + ']');
				else {
					clientVM.displaySuccess(this.localize('SaveTableDefintionSuc'))
					this.loadData(respData.data)
				}
			},

			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},

	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveRecord, this, [exitAfterSave])
		this.task.delay(500)
	},
	saveIsEnabled$: function() {
		return !this.wait;
	},
	saveAndExit: function() {
		if (!this.saveIsEnabled)
			return;
		if (this.task) this.task.cancel()
		this.save(true)
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.customtable.Main'
		});
	},
	backIsVisible$: function() {
		return hasPermission('admin');
	},
	resetForm: function() {
		this.loadData(this.defaultData);
	},
	refresh: function() {
		this.loadRecord()
	},

	view: function() {
		clientVM.handleNavigation({
			modelName: 'RS.customTable.Table',
			params: {
				table: this.u_name,
				view: 'Default'
			}
		})
	},

	createView: function() {
		clientVM.handleNavigation({
			modelName: 'RS.customtable.ViewDefinition',
			params: {
				tableName: this.u_name
			}
		})
	},
	createViewIsEnabled$: function() {
		return this.id
	},
	editView: function(name) {
		clientVM.handleNavigation({
			modelName: 'RS.customtable.ViewDefinition',
			params: {
				name: name,
				tableName: this.u_name
			}
		})
	},
	deleteView: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.viewsSelections.length > 1 ? 'deletesViewMessage' : 'deleteViewMessage', {
				num: this.viewsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			fn: this.reallyDeleteRecords
		})
	},
	deleteViewIsEnabled$: function() {
		return this.viewsSelections.length > 0
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/view/delete',
					params: {
						// tableName: 'meta_table_view',
						whereClause: this.views.lastWhereClause ? this.views.lastWhereClause : '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r);
						if (response.success) this.views.load()
						else clientVM.displayError(response.message)
					},
					failure: function(r){
						clientVM.displayFailure(r);
					}
				});
			} else {
				var ids = [];
				Ext.each(this.viewsSelections, function(selection) {
					ids.push(selection.get('id'));
				})
				this.ajax({
					url: '/resolve/service/view/delete',
					params: {
						// tableName: this.table,
						// whereClause: 'id in (\'' + ids.join('\',\'') + '\')'
						sysIds: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r);
						if (response.success) this.views.load()
						else clientVM.displayError(response.message)
					},
					failure: function(r){
						clientVM.displayFailure(r);
					}
				});
			}
		}
	},

	viewView: function() {
		clientVM.handleNavigation({
			modelName: 'RS.customtable.Table',
			params: {
				table: this.u_name,
				view: this.viewsSelections[0].get('name')
			}
		})
	},
	viewViewIsEnabled$: function() {
		return this.viewsSelections.length == 1
	},

	viewForms: function() {
		clientVM.handleNavigation({
			modelName: 'RS.formbuilder.Forms',
			params: {
				utableName: this.u_name
			}
		})
	}
})
glu.defModel('RS.customtable.ViewDefinition', {
	wait: false,

	propCollapsed: false,

	title$: function() {
		return this.localize('windowTitle') + (this.displayName ? ' - ' + this.displayName : '')
	},

	id: '',
	tableName: '',

	type: 'Shared',

	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrganizationName: '',

	name: '',
	displayName: '',

	advanced: false,
	metaNewLink: '',
	metaFormLink: '',
	createFormId: '',
	editFormId: '',
	target: '',
	targetIsVisible$: function() {
		return this.advanced
	},

	metaNewLinkIsVisible$: function() {
		return this.advanced
	},
	metaFormLinkIsVisible$: function() {
		return this.advanced
	},

	createFormIdIsVisible$: function() {
		return !this.advanced
	},
	editFormIdIsVisible$: function() {
		return !this.advanced
	},

	targetStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	formStore: {
		mtype: 'store',
		fields: ['sys_id', 'u_form_name', 'displayName'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/dbapi/getRecordData',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	defaultData: {
		id: '',
		tableName: '',

		type: 'Shared',

		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrganizationName: '',

		name: '',
		displayName: '',

		advanced: false,
		metaNewLink: '',
		metaFormLink: '',
		createFormId: '',
		editFormId: '',
		target: ''
	},

	accessRight: {},
	accessRights: {
		mtype: 'store',
		fields: ['name', 'value', {
			name: 'admin',
			type: 'bool'
		}, {
			name: 'edit',
			type: 'bool'
		}, {
			name: 'view',
			type: 'bool'
		}, {
			name: 'execute',
			type: 'bool'
		}],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	accessRightsSelections: [],

	fields: ['name', 'displayName', 'metaNewLink', 'metaFormLink', 'createFormId', 'editFormId', 'target', 'id', 'metaTableSysId', 'type'].concat(RS.common.grid.getSysFields()),

	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.resetForm()
		this.set('id', params && params.id ? params.id : '')
		this.set('tableName', params && params.tableName ? params.tableName : '')
		this.set('name', params && params.name ? params.name : '')
		this.formStore.load()
		this.loadRecord()
	},

	init: function() {
		this.targetStore.add([{
			name: this.localize('newWindow'),
			value: '_blank'
		}, {
			name: this.localize('sameWindow'),
			value: 'default'
		}])
		this.targetStore.sort('name')

		this.formStore.on('beforeload', function(store, operation) {
			operation.params = operation.params || {}
			Ext.apply(operation.params, {
				//modelName: 'MetaFormView',
				tableName: 'meta_form_view',
				type: 'table',
				start: 0,
				limit: -1,
				filter: Ext.encode([{
					field: 'u_table_name',
					condition: 'equals',
					value: this.tableName
				}])
			})
		}, this)
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	loadRecord: function() {
		if (this.name && this.tableName) {
			this.set('wait', true)
			this.ajax({
				url: '/resolve/service/view/read',
				params: {
					table: this.tableName,
					view: this.name
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.loadData(response.data)
						if (this.metaNewLink || this.metaFormLink) this.set('advanced', true)
						this.processAccessRights(response.data)
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				},
				callback: function() {
					this.set('wait', false)
				}
			})
		}

		this.ajax({
			url: '/resolve/service/dbapi/getRecordData',
			params: {
				//modelName: 'MetaTable',
				tableName: 'meta_table',
				type: 'form',
				start: 0,
				limit: 1,
				filter: Ext.encode([{
					field: 'u_name',
					condition: 'equals',
					type: 'auto',
					value: this.tableName
				}])
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (!response.success)
					clientVM.displayError(response.message)
				else {
					this.set('metaTableSysId', response.data.id)
				}
			},
			failure: function(f) {
				this.set('wait', false)
				clientVM.displayFailure(f);
			}
		})
	},

	saveRecord: function(exitAfterSave) {
		this.set('wait', true)
		var obj = this.asObject();
		obj.metaTableName = this.tableName

		if (!this.advanced) {
			obj.metaNewLink = ''
			obj.metaFormLink = ''
		}

		//Add in access rights
		var viewRoles = [],
			editRoles = [],
			adminRoles = [],
			executeRoles = [];
		this.accessRights.each(function(accessRight) {
			if (accessRight.data.view) viewRoles.push(accessRight.data.name);
			if (accessRight.data.edit) editRoles.push(accessRight.data.name);
			if (accessRight.data.admin) adminRoles.push(accessRight.data.name);
			if (accessRight.data.execute) executeRoles.push(accessRight.data.name);
		});

		Ext.apply(obj, {
			viewRoles: viewRoles.join(','),
			editRoles: editRoles.join(','),
			adminRoles: adminRoles.join(',')
		})

		this.ajax({
			url: '/resolve/service/view/save',
			jsonData: obj,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (!response.success)
					clientVM.displayError(response.message)
				else {
					clientVM.displaySuccess(this.localize('savedViewDefinition'))
					if (exitAfterSave === true) {
						clientVM.handleNavigation({
							modelName: 'RS.customtable.Main'
						})
					} else
						this.loadData(response.data)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false)
			}
		})
	},

	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveRecord, this, [exitAfterSave])
		this.task.delay(500)
	},
	saveIsEnabled$: function() {
		return !this.wait;
	},
	saveAndExit: function() {
		if (!this.saveIsEnabled)
			return;
		if (this.task) this.task.cancel()
		this.save(true)
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.customtable.TableDefinition',
			params: {
				name: this.tableName
			}
		})
	},
	resetForm: function() {
		this.loadData(this.defaultData)
	},
	refresh: function() {
		this.loadRecord()
	},

	addAccessRight: function() {
		var rights = [];
		this.accessRights.each(function(right) {
			rights.push(right.data);
		});
		this.open({
			mtype: 'RS.formbuilder.AccessRightsPicker',
			currentRights: rights
		});
	},
	addRealAccessRights: function(accessRights) {
		Ext.each(accessRights, function(accessRight) {
			this.accessRights.add(Ext.applyIf(accessRight.data, {
				view: true,
				edit: true,
				admin: true
			}));
		}, this);
	},
	removeAccessRightIsEnabled: {
		on: ['accessRightsSelectionsChanged'],
		formula: function() {
			return this.accessRightsSelections.length > 0;
		}
	},
	removeAccessRight: function() {
		for (var i = 0; i < this.accessRightsSelections.length; i++) {
			this.accessRights.remove(this.accessRightsSelections[i]);
		}
	},

	processAccessRights: function(rightObj) {
		var viewRoles = rightObj.viewRoles ? rightObj.viewRoles.split(',') : [],
			editRoles = rightObj.editRoles ? rightObj.editRoles.split(',') : [],
			adminRoles = rightObj.adminRoles ? rightObj.adminRoles.split(',') : [],
			// executeRoles = rightObj.uexecuteAccess ? rightObj.uexecuteAccess.split(',') : [],
			accessRights = [],
			i;
		for (i = 0; i < viewRoles.length; i++)
			this.addToAccessRights(accessRights, viewRoles[i], 'view', true);
		for (i = 0; i < editRoles.length; i++)
			this.addToAccessRights(accessRights, editRoles[i], 'edit', true);
		for (i = 0; i < adminRoles.length; i++)
			this.addToAccessRights(accessRights, adminRoles[i], 'admin', true);
		// for (i = 0; i < executeRoles.length; i++)
		// 	this.addToAccessRights(accessRights, executeRoles[i], 'execute', true);

		this.accessRights.removeAll()
		this.accessRights.add(accessRights)
	},
	addToAccessRights: function(accessRights, name, right, value) {
		var i, len = accessRights.length;
		for (i = 0; i < len; i++) {
			if (accessRights[i].name == name) {
				accessRights[i][right] = value;
				return;
			}
		}

		var newName = {
			name: name,
			value: name
		};
		newName[right] = value;
		accessRights.push(newName);
	},

	view: function() {
		clientVM.handleNavigation({
			modelName: 'RS.customtable.Table',
			params: {
				table: this.tableName,
				view: this.name
			}
		})
	},
	viewIsEnabled$: function() {
		return this.id && this.name
	}
});
/**
 * Model definition for the main entry point to view lookup table
 */
glu.defModel('RS.customtable.ViewLookup', {
	table: 'meta_view_lookup',
	modelName: 'MetaViewLookup',
	tableDisplayName: '~~ViewLookupTable~~',
	tableSysId: 'table_sys_id',
	view: 'Default',

	detailUrl: '',

	gridSelections: [],
	customTableStore: {
		mtype: 'store',
		model: 'RS.customtable.ViewLookupTable',
		pageSize: 0,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/dbapi/getRecordData',
			reader: {
				type: 'json',
				root: 'records',
				totalProperty: 'total',
				successProperty: 'success',
				messageProperty: 'message'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	roleStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getRoles',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	columns: [],

	init: function() {
		var me = this;

		me.set('columns', [{
			text: '~~appName~~',
			dataIndex: 'u_app_name',
			filterable: true,
			autoWidth: true,
			sortable: false
		}, {
			text: '~~view~~',
			dataIndex: 'u_view_name',
			filterable: true,
			autoWidth: true,
			sortable: false
		}, {
			text: '~~roles~~',
			dataIndex: 'u_roles',
			filterable: true,
			autoWidth: true,
			sortable: false,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))

		me.customTableStore.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				modelName: me.modelName,
				useSql: false,
				type: 'table',
				orderBy: 'u_order ASC',
				limit: -1
			});
		});

		me.roleStore.load();

		me.set('tableDisplayName', me.localize('~~ViewLookupTable~~'));
	},

	newAction: function() {
		//Create a new record
		this.open({
			mtype: 'ViewLookupItem',
			order: this.customTableStore.getCount()
		});
	},
	editAction: function(sys_id) {
		//Edit selected record
		var record = this.customTableStore.getById(sys_id),
			model = this.model({
				mtype: 'ViewLookupItem'
			});
		if (record) {
			Ext.apply(record.data, {
				id: record.data.sys_id,
				appName: record.data.u_app_name,
				view: record.data.u_view_name,
				order: record.data.u_order
			});
			model.loadData(record.data);
			model.set('roles', record.data.u_roles.split(','));
			model.set('isEdit', true);
		}
		this.open(model);
	},

	allSelected: false,
	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll);
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false);
	},

	deleteAction: function() {
		//Delete the selected record(s), but first confirm the action
		Ext.MessageBox.show({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.gridSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.gridSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		});
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			if (this.allSelected) {
				Ext.Ajax.request({
					url: '/resolve/service/dbapi/delete',
					params: {
						tableName: this.table,
						whereClause: this.customTableStore.lastWhereClause ? this.customTableStore.lastWhereClause : '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r);
						if (response.success) this.customTableStore.load();
						else{
							clientVM.displayError( response.message, null,this.localize('error'))
						}
					},
					failure: function(r){
						clientVM.displayFailure(r);
					}
				});
			} else {
				var ids = [];
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('sys_id'));
				});
				Ext.Ajax.request({
					url: '/resolve/service/dbapi/delete',
					params: {
						tableName: this.table,
						whereClause: 'sys_id in (\'' + ids.join('\',\'') + '\')'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r);
						if (response.success) this.customTableStore.load();
						else{
							clientVM.displayError(response.message);
						}
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				});
			}
		}
	},
	deleteActionIsEnabled$: function() {
		return this.gridSelections.length > 0
	},
	reload: function() {
		this.customTableStore.load();
	},

	dropRecord: function(node, data, overModel, dropPosition, eOpts) {
		//Get the order of the sys_ids for the update to call into the server
		var ids = [];
		this.customTableStore.each(function(record) {
			ids.push(record.internalId);
		});
		var idString = ids.join('|&|');
		Ext.Ajax.request({
			url: '/resolve/service/lookuptable/updateorder',
			params: {
				sysIds: idString
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					// this.customTableStore.load();
					//Do nothing, the grid is already updated so no need to reload the grid data
				} else {
					//if there was an error then reload the grid to reset it to the server settings
					this.customTableStore.load();
					clientVM.displayError(response.message);
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	}
});
glu.defModel('RS.customtable.ViewLookupItem', {
	sys_id: '',
	appName: '',
	appNameIsValid$: function() {
		if(!this.appName) return this.localize('appNameRequired');
		return true;
	},
	view: '',
	viewIsValid$: function() {
		if(!this.view) return this.localize('viewRequired');
		return true;
	},
	roles: '',
	rolesIsValid$: function() {
		if(!this.roles) return this.localize('rolesRequired');
		return true;
	},
	order: 0,

	fields: ['sys_id', 'appName', 'view', 'roles', 'order'],

	isEdit: false,

	itemTitle$: function() {
		return this.isEdit ? this.localize('EditLookupItem') : this.localize('NewLookupItem');
	},

	save: function() {
		var params = this.asObject();
		Ext.Ajax.request({
			url: '/resolve/service/lookuptable/save',
			params: params,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if(response.success) {
					this.parentVM.reload();
					this.doClose();
				} else{
					clientVM.displayError(response.message);
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		});
	},
	saveIsEnabled$: function() {
		return this.isValid;
	},
	cancel: function() {
		this.doClose();
	}
});
glu.defModel('RS.customtable.WikiViewer', {

	activate: function(screen, params) {
		this.set('url', params.url)
	},

	url: '',
	html$: function() {
		return this.url ? Ext.String.format('<iframe class="rs_iframe" src="{0}" style="height:100%;width:100%;border:0px" frameboder="0"></iframe>', this.url) : '&nbsp;'
	}
})
glu.defView('RS.customtable.Main', {
	padding : 15,
	cls : 'rs-grid-dark',
	xtype: 'grid',
	layout: 'fit',
	stateId: '@{stateId}',
	stateful: true,
	displayName: '@{displayName}',
	name: 'records',
	store: '@{store}',
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		name: 'actionBar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: ['view', 'deleteRecords']
	}],
	columns: '@{columns}',
	plugins: [{
		ptype: 'searchfilter',
		allowPersistFilter: false,
		hideMenu: true
	}, {
		ptype: 'pager'
	}, {
		ptype: 'columnautowidth'
	}],
	selModel: {
		selType: 'resolvecheckboxmodel',		
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'id'
	},

	listeners: {
		editAction: '@{editRecord}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}'
	}
});
glu.defView('RS.customtable.Table', {
	layout: 'card',
	activeItem: '@{activeCard}',
	padding: 15,
	items: [{
		dockedItems: [{
			xtype: 'toolbar',			
			name: 'actionBar',
			items: '@{toolbarItems}',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			itemTemplate: {
				xtype: 'button',
				text: '@{text}',
				name: 'action'
			}
		}],
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'grid',
		displayName: '@{displayName}',
		setDisplayName: function(name) {
			this.getPlugin('search').setDisplayName(name)
		},
		tableName: '@{table}',
		setTableName: function(name) {
			this.tableName = name
		},
		tableSysId: '@{tableSysId}',
		setTableSysId: function(id) {
			this.tableSysId = id
		},
		viewName: '@{view}',
		serverData: '@{serverData}',
		setServerData: function(data) {
			this.serverData = data
			this.fireEvent('serverDataChanged')
		},
		// stateId: '@{tableSysId}',
		// stateful: true,
		plugins: [{
			ptype: 'searchfilter',
			pluginId: 'search'
		}, {
			ptype: 'pager',
			showSysInfo: false
		}, {
			ptype: 'columnautowidth'
		}],
		store: '@{customTableStore}',
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'sys_id'
		},		
		columns: '@{columns}',
		viewConfig: {
			enableTextSelection: true
		},
		setColumns: function(columns) {
			this.reconfigure(null, columns)
		},
		listeners: {
			editAction: '@{editAction}',
			selectAllAcrossPages: '@{selectAllAcrossPages}',
			selectionChange: '@{selectionChanged}',
			viewSelected: '@{viewSelected}'
		}
	}, {
		xtype: 'panel',
		url: '@{detailUrl}',
		frameTemplate: '<iframe class="rs_iframe" src="{0}" style="height:100%;width:100%;border:0" />',
		border: false,
		html: '',
		setUrl: function(url) {
			this.update(Ext.String.format(this.frameTemplate, url));
			this.getEl().select('iframe').on('load', function(e, t, eOpts) {
				var me = this;
				t.contentWindow.registerForm = function(form) {
					form.on('beforeclose', function() {
						this.fireEvent('reloadAndShowList');
						return false;
					}, me);
					form.on('titleChange', function() {
						return false;
					});
					form.on('beforeredirect', function(url) {
						window.open(url, '_self');
						return false;
					})
				}
			}, this);
		},
		listeners: {
			reloadAndShowList: '@{reloadAndShowList}'
		}
	}, {
		xtype: '@{newForm}',
		listeners: {
			beforeclose: '@{..reloadAndShowList}'
		}
	}, {
		xtype: '@{editForm}',
		listeners: {
			beforeclose: '@{..reloadAndShowList}'
		}
	}]
});
glu.defView('RS.customtable.TableDefinition', {
	padding : 15,	
	autoScroll: true,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls : 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{title}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'		
		}, 'viewForms', {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sys_created_on}',
			sysCreatedBy: '@{sys_created_by}',
			sysUpdated: '@{sys_updated_on}',
			sysUpdatedBy: '@{sys_updated_by}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	layout: {
		type: 'vbox',
		align: 'stretch'
	},	
	items: [{
		xtype : 'container',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},	
		style : 'border-top:1px solid silver',
		padding : '8 0',
		defaults : {
			labelWidth : 120
		},
		items : [{
			xtype: 'displayfield',
			name : 'u_name' 
		},{
			xtype: 'textfield',
			name: 'u_display_name'
		}]
	}, {
		title: '~~columns~~',
		flex: 1,
		minHeight: 200,		
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'columns',
		store: '@{store}',
		columns: '@{columns}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			items: []
		}],
		plugins: [{
			ptype: 'pager'
		}]
	}, {
		title: '~~views~~',
		flex: 1,
		minHeight: 200,
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'views',
		columns: '@{viewsColumns}',		
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light',
			},
			name: 'actionBar',
			items: ['createView', 'deleteView', 'viewView']
		}],
		selModel: {
			selType: 'resolvecheckboxmodel',	
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'name'
		},
		plugins: [{
			ptype: 'pager',
			showSysInfo: false
		}],
		listeners: {
			editAction: '@{editView}'
		}
	}]
});

glu.defView('RS.customtable.ViewDefinition', {
	autoScroll: true,
	padding: 15,	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls : 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{title}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'		
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, 'view', '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],	
	items: [{
		xtype : 'container',
		layout : {
			type : 'vbox',
			align : 'stretch'
		},
		style : 'border-top:1px solid silver',
		padding : '8 0',
		defaultType: 'textfield',
		defaults : {
			labelWidth : 120,
			margin : '4 0'
		},
		items : ['name', 'displayName', {
			xtype: 'displayfield',
			name: 'tableName'
		}, {
			name: 'advanced',
			xtype: 'checkbox',
			hideLabel: true,
			margin: '0px 0px 0px 125px',
			boxLabel: '~~advanced~~'
		},
		'metaNewLink', 'metaFormLink', {
			xtype: 'combobox',
			editable: false,
			name: 'createFormId',
			store: '@{formStore}',
			displayField: 'u_display_name',
			valueField: 'sys_id',
			queryMode: 'local',
			displayTpl: '<tpl for=".">{[typeof values === "string" ? values : values["u_display_name"] || values["u_form_name"]]}</tpl>',
			tpl: ['<ul class="x-list-plain"><tpl for=".">',
				'<li role="option" class="x-boundlist-item">',
				'{[values["u_display_name"] || values["u_form_name"]]}</li>',
				'</tpl></ul>'
			].join('')
		}, {
			xtype: 'combobox',
			editable: false,
			name: 'editFormId',
			store: '@{formStore}',
			displayField: 'u_display_name',
			valueField: 'sys_id',
			queryMode: 'local',
			displayTpl: '<tpl for=".">{[typeof values === "string" ? values : values["u_display_name"] || values["u_form_name"]]}</tpl>',
			tpl: ['<ul class="x-list-plain"><tpl for=".">',
				'<li role="option" class="x-boundlist-item">',
				'{[values["u_display_name"] || values["u_form_name"]]}</li>',
				'</tpl></ul>'
			].join('')
		}, {
			xtype: 'combobox',
			name: 'target',
			store: '@{targetStore}',
			displayField: 'name',
			valueField: 'value',
			queryMode: 'local'
		}]
	},{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		autoScroll : true,
		flex: 1,
		minHeight: 200,		
		title: '~~AccessRights~~',
		name: 'accessRights',
		dockedItems: [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items : ['addAccessRight','removeAccessRight']
		}],
		selModel: {
			selType: 'cellmodel'
		},
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 2
		}],
		columns: [{
			header: '~~Name~~',
			dataIndex: 'name',
			hideable: false,
			flex: 1,
			editor: {
				allowBlank: false
			}
		}, {
			xtype: 'checkcolumn',
			stopSelection: false,
			header: '~~adminRight~~',
			dataIndex: 'admin',
			hideable: false,
			width: 80
		}, {
			xtype: 'checkcolumn',
			stopSelection: false,
			header: '~~editRight~~',
			dataIndex: 'edit',
			hideable: false,
			width: 80
		}, {
			xtype: 'checkcolumn',
			stopSelection: false,
			header: '~~viewRight~~',
			dataIndex: 'view',
			hideable: false,
			width: 80
		}
		/*, {
			xtype: 'checkcolumn',
			stopSelection: false,
			header: '~~executeRight~~',
			dataIndex: 'execute',
			hideable: false,
			width: 75
		}*/
		]
	}]
});

glu.defView('RS.customtable.ViewLookup', {
	padding: '10px',
	xtype: 'grid',
	name: 'grid',
	displayName: '@{tableDisplayName}',
	tableName: '@{table}',
	tableSysId: '@{tableSysId}',
	viewName: '@{view}',
	stateId: 'viewLookupList',
	stateful: true,
	plugins: [{
		ptype: 'searchfilter',
		hideMenu: true,
		allowPersistFilter: false
	}, {
		ptype: 'pager',
		pageable: false
	}],
	store: '@{customTableStore}',
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.editColumnTooltip,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'sys_id'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		name: 'actionBar',
		items: [{
			name: 'newAction'
		}, {
			name: 'deleteAction'
		}]
	}],
	columns: '@{columns}',
	viewConfig: {
		enableTextSelection: false,
		plugins: {
			ptype: 'gridviewdragdrop',
			dragText: 'Drag and drop to reorganize'
		},
		listeners: {
			drop: function() {
				this.ownerCt.fireEvent('drop', arguments);
			}
		}
	},
	listeners: {
		editAction: '@{editAction}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}',
		drop: '@{dropRecord}'
	}
});
glu.defView('RS.customtable.ViewLookupItem', {
	modal: true,
	width: 450,
	height: 200,
	title: '@{itemTitle}',
	layout: 'fit',
	items: [{
		bodyPadding: '10px',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults: {
			labelWidth: 125
		},
		items: [{
			xtype: 'textfield',
			name: 'appName'
		}, {
			xtype: 'textfield',
			name: 'view'
		}, {
			xtype: 'combobox',
			name: 'roles',
			store: '@{..roleStore}',
			displayField: 'name',
			valueField: 'value',
			multiSelect: true,
			queryMode: 'local'
		}],
		buttonAlign: 'left',
		buttons: [{
			name: 'save'
		}, {
			name: 'cancel'
		}]
	}]
});
glu.defView('RS.customtable.WikiViewer', {
	html: '@{html}'
})
/**
 * Language Translation File - key:value pairs that are used by glu to do translations
 */
Ext.namespace('RS.customtable').locale = {

	showList: '← Back',
	cancel: 'Cancel',
	error: 'Error',
	communicationError: 'There was an error communicating with the server',
	save: 'Save',

	//Column headers
	editColumn: 'Edit',
	created: 'Created',
	number: 'Number',
	condition: 'Condition',
	severity: 'Severity',
	summary: 'Summary',
	reference: 'Reference',
	alertId: 'Alert ID',
	correlationId: 'Correlation ID',
	assignedTo: 'Assigned To',
	systemId: 'System ID',


	//Actions
	newAction: 'New',
	editAction: 'Edit',
	createView: 'New',
	deleteView: 'Delete',

	deleteTitle: 'Confirm Delete',
	deleteTableMessage: 'Are you sure you want to delete the selected table?  This action cannot be undone.',
	deletesTableMessage: 'Are you sure you want to delete the {num} selected tables?  This action cannot be undone.',

	deletedTable: 'Custom table deleted sucessfully',
	deletedTables: 'Custom tables deleted sucessfully',

	deleteAction: 'Delete',
	setActive: 'SetActive',

	actionCompleted: 'Action executed successfully',

	deleteViewMessage: 'Are you sure you want to delete the selected view?  This action cannot be undone.',
	deletesViewMessage: 'Are you sure you want to delete the {0} selected views?  This action cannot be undone.',

	views: 'Views',

	deletesMessage: 'Are you sure you want to delete the {0} selected records?',
	deleteMessage: 'Are you sure you want to delete the selected record?',

	//ViewLookup.jsp
	ViewLookupTable: 'View Lookup Table',
	id: 'ID',
	appName: 'Application Name',
	view: 'View',
	roles: 'Roles',
	NewLookupItem: 'New Lookup',
	EditLookupItem: 'Edit Lookup',

	appNameRequired: 'Please provide an application name',
	viewRequired: 'Please provide a view',
	rolesRequired: 'Please choose at least one role',

	//Definition
	ServerError: 'Server Error',
	u_name: 'Name',
	utype: 'Type',
	displayName: 'Display Name',
	u_display_name: 'Display Name',
	createRecord: 'New',
	deleteRecords: 'Delete',

	back: 'Back',
	save: 'Save',

	TableDefinition: {
		windowTitle: 'Custom Table',
		customTableDefinition: 'Custom Table',
		columns: 'Columns'
	},

	//Table View Definition
	utableName: 'Table Name',
	Main: {
		displayName: 'Custom Table',
		windowTitle: 'Custom Table',
		ListRecordsErr: 'Cannot get custom table view definitions from the server.'
	},
	ViewDefinition: {
		windowTitle: 'Custom Table View',
		customTableDefinition: 'Custom Table',
		columns: 'Columns',

		createFormId: 'Create Form',
		editFormId: 'Edit Form',

		target: 'Target',
		advanced: 'Advanced Configuration',
		metaNewLink: 'Create Url',
		metaFormLink: 'Edit Url',

		sameWindow: 'Same Window',
		newWindow: 'New Window',

		AccessRights: 'Access Rights',
		addAccessRight: 'Add',
		removeAccessRight: 'Remove',

		savedViewDefinition: 'View Definition successfully saved'
	},

	name: 'Name',
	tableName: 'Table Name',

	Name: 'Name',
	adminRight: 'Admin',
	editRight: 'Edit',
	viewRight: 'View',
	executeRight: 'Execute',

	viewForms: 'Forms',
	viewView: 'View',
	SaveTableDefintionSuc: 'Custom Table has successfully saved'
}
