<!-- ******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
********************************************************************************* -->

<%@ page import="org.owasp.esapi.ESAPI" %>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" prefix="csrf" %>
<html>
  <head>
    <meta http-equiv="refresh" content="0; url=/resolve/jsp/rsclient.jsp?displayClient=false&<csrf:tokenname/>=<csrf:tokenvalue uri="/resolve/jsp/rsclient.jsp"/>#RS.customtable.Table/table=<%=ESAPI.encoder().encodeForURL(request.getParameter("table"))%>&view=<%=ESAPI.encoder().encodeForURL(request.getParameter("view"))%>" />
  </head>
  <body>
  </body>
</html>
