Ext.define('RS.customtable.ViewLookupTable', {
	extend: 'Ext.data.Model',
	idProperty: 'sys_id',
	fields: ['sys_id', 'u_app_name', 'u_view_name', 'u_roles', {
		name: 'u_order',
		type: 'number'
	}].concat(RS.common.grid.getSysFields())
});