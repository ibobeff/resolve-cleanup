glu.defModel('RS.customtable.TableDefinition', {
	wait: false,

	title$: function() {
		return this.localize('customTableDefinition') + (this.u_name ? ' - ' + this.u_name : '')
	},

	propCollapsed: false,

	id: '',
	name: '',

	sys_created_on: '',
	sys_created_by: '',
	sys_updated_on: '',
	sys_updated_by: '',
	sysOrganizationName: '',

	u_name: '',
	u_display_name: '',
	u_meta_table_sys_id: '',

	defaultData: {
		id: '',
		u_meta_table_sys_id: '',

		sys_created_on: '',
		sys_created_by: '',
		sys_updated_on: '',
		sys_updated_by: '',
		sysOrganizationName: '',

		u_name: '',
		u_display_name: ''
	},

	fields: ['u_name', 'u_display_name'].concat(RS.common.grid.getCustomSysFields()),
	store: {
		mtype: 'store',
		fields: ['u_name', 'u_display_name', 'u_type'].concat(RS.common.grid.getCustomSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/dbapi/getRecordData',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	columns: [{
		text: '~~u_name~~',
		dataIndex: 'u_name',
		flex: 1
	}, {
		text: '~~u_display_name~~',
		dataIndex: 'u_display_name',
		flex: 1
	}, {
		text: '~~utype~~',
		dataIndex: 'u_type',
		flex: 1
	}].concat(RS.common.grid.getCustomSysColumns()),

	views: {
		mtype: 'store',
		remoteSort: true,
		fields: ['name'].concat(RS.common.grid.getSysFields()),
		proxy: {
			type: 'ajax',
			url: '/resolve/service/view/list',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},
	viewsSelections: [],
	allSelected: false,

	viewsColumns: [{
		text: '~~u_name~~',
		dataIndex: 'name',
		flex: 1
	}], //.concat(RS.common.grid.getSysColumns()),

	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.resetForm()
		this.set('id', params && params.id ? params.id : '')
		this.set('name', params && params.name ? params.name : '')
		this.loadRecord()
	},

	init: function() {
		this.store.on('beforeload', function(store, operation) {
			operation.params = operation.params || {}
			Ext.apply(operation.params, {
				//modelName: 'MetaField',
				tableName: 'meta_field',
				type: 'table',
				filter: Ext.encode([{
					field: 'u_custom_table_sys_id',
					condition: 'equals',
					type: 'auto',
					value: this.id
				}])
			})
		}, this)

		this.views.on('beforeload', function(store, operation) {
			operation.params = operation.params || {}
			Ext.apply(operation.params, {
				table: this.u_name
			})
			// Ext.apply(operation.params, {
			// 	modelName: 'MetaTableView',
			// 	type: 'table',
			// 	filter: Ext.encode([{
			// 		field: 'u_meta_table_sys_id',
			// 		condition: 'equals',
			// 		type: 'auto',
			// 		value: this.u_meta_table_sys_id
			// 	}])
			// })
		}, this)
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	loadRecord: function() {
		if (this.id || this.name) {
			this.set('wait', true)
			this.store.removeAll()
			this.views.removeAll()

			this.ajax({
				url: '/resolve/service/dbapi/getRecordData',
				params: {
					//modelName: 'CustomTable',
					tableName: 'custom_table',
					type: 'form',
					start: 0,
					limit: 1,
					filter: Ext.encode([{
						field: this.id ? 'sys_id' : 'u_name',
						condition: 'equals',
						type: 'auto',
						value: this.id || this.name
					}])
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r);
					if (!response.success)
						clientVM.displayError(response.message)
					else {
						this.loadData(response.data)
						this.store.load()
						this.ajax({
							url: '/resolve/service/dbapi/getRecordData',
							params: {
								//modelName: 'MetaTable',
								tableName: 'meta_table',
								start: 0,
								limit: 1,
								type: 'form',
								filter: Ext.encode([{
									field: 'u_name',
									type: 'auto',
									condition: 'equals',
									value: this.u_name
								}])
							},
							success: function(r) {
								var response = RS.common.parsePayload(r)
								if (response.success) {
									if (response.data) {
										this.set('u_meta_table_sys_id', response.data.id)
										this.views.load()
									}
								} else {
									clientVM.displayError(response.message);
								}
							},
							failure: function(r){
								clientVM.displayFailure(r);
							}
						})
					}
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				},
				callback: function() {
					this.set('wait', false)
				}
			})
		}
	},

	saveRecord: function(exitAfterSave) {
		this.set('wait', true);
		this.ajax({
			url: '/resolve/service/customtable/save',
			jsonData: this.asObject(),
			scope: this,
			success: function(resp) {
				var respData = RS.common.parsePayload(resp);
				if (!respData.success)
					clientVM.displayError(this.localize('SaveTableDefintionErr') + '[' + respData.message + ']');
				else {
					clientVM.displaySuccess(this.localize('SaveTableDefintionSuc'))
					this.loadData(respData.data)
				}
			},

			failure: function(resp) {
				clientVM.displayFailure(resp);
			},
			callback: function() {
				this.set('wait', false);
			}
		})
	},

	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveRecord, this, [exitAfterSave])
		this.task.delay(500)
	},
	saveIsEnabled$: function() {
		return !this.wait;
	},
	saveAndExit: function() {
		if (!this.saveIsEnabled)
			return;
		if (this.task) this.task.cancel()
		this.save(true)
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.customtable.Main'
		});
	},
	backIsVisible$: function() {
		return hasPermission('admin');
	},
	resetForm: function() {
		this.loadData(this.defaultData);
	},
	refresh: function() {
		this.loadRecord()
	},

	view: function() {
		clientVM.handleNavigation({
			modelName: 'RS.customTable.Table',
			params: {
				table: this.u_name,
				view: 'Default'
			}
		})
	},

	createView: function() {
		clientVM.handleNavigation({
			modelName: 'RS.customtable.ViewDefinition',
			params: {
				tableName: this.u_name
			}
		})
	},
	createViewIsEnabled$: function() {
		return this.id
	},
	editView: function(name) {
		clientVM.handleNavigation({
			modelName: 'RS.customtable.ViewDefinition',
			params: {
				name: name,
				tableName: this.u_name
			}
		})
	},
	deleteView: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.viewsSelections.length > 1 ? 'deletesViewMessage' : 'deleteViewMessage', {
				num: this.viewsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			fn: this.reallyDeleteRecords
		})
	},
	deleteViewIsEnabled$: function() {
		return this.viewsSelections.length > 0
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			if (this.allSelected) {
				this.ajax({
					url: '/resolve/service/view/delete',
					params: {
						// tableName: 'meta_table_view',
						whereClause: this.views.lastWhereClause ? this.views.lastWhereClause : '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r);
						if (response.success) this.views.load()
						else clientVM.displayError(response.message)
					},
					failure: function(r){
						clientVM.displayFailure(r);
					}
				});
			} else {
				var ids = [];
				Ext.each(this.viewsSelections, function(selection) {
					ids.push(selection.get('id'));
				})
				this.ajax({
					url: '/resolve/service/view/delete',
					params: {
						// tableName: this.table,
						// whereClause: 'id in (\'' + ids.join('\',\'') + '\')'
						sysIds: ids.join(',')
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r);
						if (response.success) this.views.load()
						else clientVM.displayError(response.message)
					},
					failure: function(r){
						clientVM.displayFailure(r);
					}
				});
			}
		}
	},

	viewView: function() {
		clientVM.handleNavigation({
			modelName: 'RS.customtable.Table',
			params: {
				table: this.u_name,
				view: this.viewsSelections[0].get('name')
			}
		})
	},
	viewViewIsEnabled$: function() {
		return this.viewsSelections.length == 1
	},

	viewForms: function() {
		clientVM.handleNavigation({
			modelName: 'RS.formbuilder.Forms',
			params: {
				utableName: this.u_name
			}
		})
	}
})