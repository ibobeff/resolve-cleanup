glu.defModel('RS.customtable.Table', {
	API : {
		list : '/resolve/service/customtable/list',
		read : '/resolve/service/view/read',
		submit : '/resolve/service/form/submit',
		deleteData : '/resolve/service/customtable/deletedata'
	},
	table: 'Custom Table',
	displayName: '',
	tableSysId: 'table_sys_id',
	view: 'Default',
	detailUrl: '',
	metaNewLink: '',
	metaFormLink: '',
	createFormId: '',
	editFormId: '',

	activeCard: 0,
	showList: function() {
		this.set('activeCard', 0)
	},
	showDetails: function() {
		this.set('activeCard', 1)
	},
	gridSelections: [],
	allSelected: false,

	customTableStore: {
		mtype: 'store',
		model: 'RS.customtable.Table',
		remoteSort: true,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/dbapi/getRecordData',
			reader: {
				type: 'json',
				root: 'records',
				totalProperty: 'total',
				successProperty: 'success',
				messageProperty: 'message'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	columns: [],
	serverData: {},

	fields: ['displayName', 'metaNewLink', 'metaFormLink', 'createFormId', 'editFormId', 'target'],
	target: 'default',

	toolbarItems: {
		mtype: 'list'
	},

	formFields: [],

	activate: function(screen, params) {
		if (params && params.table) this.set('table', params.table)
		if (params && params.view) this.set('view', params.view)

		while (this.toolbarItems.length > 2) this.toolbarItems.removeAt(2)

		this.set('activeCard', 0)

		this.ajax({
			url: this.API['read'],
			params: {
				table: this.table,
				view: this.view
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					if (response.data && response.data.fields) {
						this.loadData(response.data)
						this.set('serverData', response.data)

						if (!this.createFormId && !this.metaNewLink) this.set('metaNewLink', '/resolve/service/wiki/view/${table}/${view}?sys_id=${sys_id}&view=${view}&table=${table}')
						if (!this.editFormId && !this.metaFormLink) this.set('metaFormLink', '/resolve/service/wiki/view/${table}/${view}?sys_id=${sys_id}&view=${view}&table=${table}')

						if (this.createFormId) this.newForm.activate(null, {
							formId: this.createFormId
						})

						this.configureColumnsAndButtons()
					}
				} else clientVM.displayError(response.message)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})

		this.ajax({
			url: this.API['list'],
			params: {
				start: 0,
				limit: 1,
				filter: Ext.encode([{
					field: 'uname',
					condition: 'equals',
					value: this.table
				}])
			},
			success: function(r) {
				var response = RS.common.parsePayload(r)
				if (response.success) {
					if (response.records && response.records.length > 0) {
						this.set('tableSysId', response.records[0].id)
					}
				} else {
					clientVM.displayError(response.message);
				}
			},
			failure: function(r){
				clientVM.displayFailure(r);
			}
		})
	},

	configureColumnsAndButtons: function() {
		// this.set('editFormId', '8a80dac03a98d3d2013a98dce5f4000a') //Content Request for testing
		if (this.editFormId) {
			this.editForm.activate(null, {
				formId: this.editFormId
			})
		} else {
			var modelFields = [],
				columns = [];
			configureColumns(this.serverData.fields, modelFields, columns, false, true);
			this.customTableStore.model.setFields(modelFields, 'sys_id')
			this.set('columns', columns) //setting columns will reconfigure which calls load automatically
		}
	},

	init: function() {
		var me = this;
		me.customTableStore.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {}
			Ext.apply(operation.params, {
				tableName: me.table,
				useSql: false,
				type: 'table'
			})
		})

		this.editForm.on('formLoaded', function() {
			var modelFields = [],
				columns = [],
				form = this.editForm.form.viewSpec,
				buttons = form.buttons || form.tbar.items;

			configureColumns(this.serverData.fields, modelFields, columns, false, true, form);
			this.customTableStore.model.setFields(modelFields, 'sys_id')
			this.set('columns', columns)

			this.customTableStore.load()

			while (this.toolbarItems.length > 2) this.toolbarItems.removeAt(2)

			Ext.Array.forEach(buttons || [], function(button) {
				if (button.customTableDisplay) {
					this.toolbarItems.add(this.model({
						mtype: 'viewmodel',
						text: button.displayName,
						button: button,
						action: function() {
							var count = 0,
								len = this.parentVM.gridSelections.length,
								callback = function() {
									if (count + 1 == len)
										clientVM.displaySuccess(this.parentVM.localize('actionCompleted'))
								}
							Ext.Array.forEach(this.parentVM.gridSelections, function(selection) {
								this.ajax({
									url: this.API['submit'],
									jsonData: {
										viewName: this.parentVM.view,
										formSysId: this.parentVM.editFormId,
										tableName: this.parentVM.table,
										userId: '',
										timezone: '',
										crudAction: this.button.crudAction,
										query: '',
										controlItemSysId: this.button.sysId,
										dbRowData: {
											data: {
												sys_id: selection.get('sys_id')
											}
										},
										sourceRowData: {
											data: {
												'RESOLVE.ORG_NAME' : clientVM.orgName,
												'RESOLVE.ORG_ID' : clientVM.orgId
											}
										}										
									},
									success: function(r) {
										var response = RS.common.parsePayload(r)
										if (response.success) callback()
										else clientVM.displayError(response.message)
									},
									failure: function(r) {
										clientVM.displayFailure(r);
									}
								})
							}, this)
						},
						actionIsEnabled$: function() {
							return this.parentVM.gridSelections.length > 0 && !this.parentVM.allSelected
						}
					}))
				}
			}, this)
		}, this)

		this.toolbarItems.add(this.model({
			mtype: 'viewmodel',
			text: this.localize('newAction'),
			action: function() {
				this.parentVM.newAction()
			}
		}))

		this.toolbarItems.add(this.model({
			mtype: 'viewmodel',
			text: this.localize('deleteAction'),
			action: function() {
				this.parentVM.deleteAction()
			},
			actionIsEnabled$: function() {
				return this.parentVM.gridSelections.length > 0
			}
		}))
	},

	newAction: function() {
		if (this.createFormId) {
			clientVM.handleNavigation({
				modelName: 'RS.formbuilder.Viewer',
				params: {
					formId: this.createFormId,
					recordId: '',
					embed: true
				}
			})
			// this.newForm.resetForm()
			// this.set('activeCard', 2)
		} else {
			//Create a new record
			var url = this.randomizeUrl(this.metaNewLink.replace(/\${table}/g, this.table).replace(/\${view}/g, this.view).replace(/\${sys_id}/g, ''));
			if (this.target == 'default') {
				this.openTokenizedUrl(url);
				//Switch to the card behind the scenes with a blank sys_id
				// this.set('detailUrl', url)
				// this.showDetails()
			} else window.open(url)
		}
	},
	newActionIsEnabled$: function() {
		return this.metaNewLink || this.createFormId
	},
	editAction: function(sys_id) {
		if (this.editFormId) {
			clientVM.handleNavigation({
				modelName: 'RS.formbuilder.Viewer',
				params: {
					formId: this.editFormId,
					recordId: sys_id,
					embed: true
				}
			})
			// this.set('activeCard', 3)
			// this.editForm.setFormRecordId(sys_id)
		} else {
			//Edit selected record
			var url = this.randomizeUrl(
				this.metaFormLink.replace(/\${table}/g, this.table)
				.replace(/\${view}/g, this.view)
				.replace(/\${sys_id}/g, sys_id)
				.replace(/\?sys_id=/g, '?recordId=')   // SUB-292 sys_id is to also get the WS. We only want the record.
				, {});
			if (url.indexOf('/resolve/') === 0 && url.indexOf(clientVM.CSRFTOKEN_NAME) === -1) {
				const urlData = url.split('#');
				const uriData = urlData[0].split('?');
    			clientVM.getCSRFToken_ForURI(uriData[0], function(data, uri) {
    				var csrftoken = '?' + data[0] + '=' + data[1];
    				var tokenizedData = uri + csrftoken;
    				if (uriData.length > 1) {
    					tokenizedData += '&' + uriData[1];
    				}
    				if (urlData.length > 1) {
    					tokenizedData += '#' + urlData[1];
    				}
    				url = tokenizedData;
    			});
    		}
			if (this.target == 'default') {
				this.openTokenizedUrl(url);
				//Switch to the card behind the scenes with the provided sys_id
				// this.set('detailUrl', url)
				// this.showDetails()
			} else window.open(url)
		}
	},

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll);
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false);
	},

	deleteAction: function() {
		//Delete the selected record(s), but first confirm the action
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.gridSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.gridSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		});
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			var ids = [];
			Ext.each(this.gridSelections, function(selection) {
				ids.push(selection.get('sys_id'));
			});
			this.ajax({			
				url: this.API['deleteData'],
				params: {
					tableName: this.table,
					ids: ids
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) this.customTableStore.load()
					else clientVM.displayError(response.message)
				},
				failure: function(r){
					clientVM.displayFailure(r);
				}
			});
		}
	},
	deleteActionIsEnabled$: function() {
		return this.gridSelections.length > 0
	},
	reloadAndShowList: function() {
		this.customTableStore.load()
		this.showList()
		return false
	},
	randomizeUrl: function(url, extraParams) {
		var val = url,
			split = url.split('?');
		if (split.length > 1) {
			var params = Ext.Object.fromQueryString(split[1]);
			params['a' + Math.random()] = Math.random();
			Ext.applyIf(params, extraParams)
			val = split[0] + '?' + Ext.Object.toQueryString(params);
		}
		return val
	},

	viewSelected: function(data) {
		this.set('serverData', data)
		this.set('view', data.name)
		this.set('metaFormLink', data.metaFormLink)
		this.set('metaNewLink', data.metaNewLink)
		this.set('createFormId', data.createFormId)
		this.set('editFormId', data.editFormId)
		this.set('displayName', data.displayName)
		this.configureColumnsAndButtons()
	},

	openTokenizedUrl: function(url) {
        if (url) {
          var split = url.split('?');
          if (split.length > 1) {
            clientVM.getCSRFToken_ForURI(split[0], function(data){
				clientVM.handleNavigation({
					modelName: 'RS.customtable.WikiViewer',
					params: {
						url: split[0] + '?' + data[0] + '=' + data[1] + '&' + split[1]
					}
				})
            })
          }
        }
	},

	newForm: {
		mtype: 'RS.formbuilder.Viewer',
		embed: true,
		formdefinition: true
	},
	editForm: {
		mtype: 'RS.formbuilder.Viewer',
		embed: true,
		formdefinition: true
	}
});