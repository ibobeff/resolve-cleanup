glu.defModel('RS.customtable.WikiViewer', {

	activate: function(screen, params) {
		this.set('url', params.url)
	},

	url: '',
	html$: function() {
		return this.url ? Ext.String.format('<iframe class="rs_iframe" src="{0}" style="height:100%;width:100%;border:0px" frameboder="0"></iframe>', this.url) : '&nbsp;'
	}
})