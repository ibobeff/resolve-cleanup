glu.defModel('RS.customtable.Main', {
	displayName: '',

	columns: [],

	stateId: 'customtabletabledefinitions',

	store: {
		mtype: 'store',

		fields: ['uname', 'udisplayName'].concat(RS.common.grid.getSysFields()),
		remoteSort: true,
		sorters: [{
			property: 'uname',
			direction: 'ASC'
		}],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/customtable/list',
			reader: {
				type: 'json',
				root: 'records',
				totalProperty: 'total',
				successProperty: 'success',
				messageProperty: 'message'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	recordsSelections: [],
	wait: false,

	activate: function() {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.store.load();
	},

	init: function() {
		this.set('displayName', this.localize('displayName'));

		this.set('columns', [{
			header: '~~name~~',
			dataIndex: 'udisplayName',
			filterable: true,
			sortable: true,
			flex: 2
		}, {
			header: '~~tableName~~',
			filterable: true,
			sortable: true,
			dataIndex: 'uname',
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))

		this.store.on('beforeload', function(store, operation) {
			operation.params = operation.params || {}
			Ext.apply(operation.params, {
				modelName: 'CustomTable',
				useSql: false,
				type: 'table',
				orderBy: 'u_order ASC'
			})
		}, this)
	},

	editRecord: function(id) {
		clientVM.handleNavigation({
			modelName: 'RS.customtable.TableDefinition',
			params: {
				id: id
			}
		})
	},

	createRecord: function() {
		clientVM.handleNavigation({
			modelName: 'RS.customtable.TableDefinition',
			params: {
				id: ''
			}
		})
	},

	deleteRecords: function() {
		this.message({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.recordsSelections.length > 1 ? 'deletesTableMessage' : 'deleteTableMessage', {
				num: this.recordsSelections.length
			}),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},

			fn: this.sendDeleteReq
		})
	},

	sendDeleteReq: function(btn) {
		if (btn != 'yes')
			return;

		this.set('wait', true)

		var ids = [];

		Ext.each(this.recordsSelections, function(r) {
			ids.push(r.get('id'));
		})

		this.ajax({
			url: '/resolve/service/customtable/delete',
			params: {
				ids: ids,
				all: false
			},
			scope: this,
			success: function(r) {
				this.handleDeleteSucces(r, ids.length)
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false)
			}
		})
	},

	handleDeleteSucces: function(r, len) {
		this.set('wait', false);
		var response = RS.common.parsePayload(r);
		if (!response.success)
			clientVM.displayError(response.message)
		else
			clientVM.displaySuccess(len === 1 ? this.localize('deletedTable') : this.localize('deletedTables'))
		this.store.load()
	},

	createRecordIsEnabled$: function() {
		return !this.wait;
	},

	deleteRecordsIsEnabled$: function() {
		return this.recordsSelections.length > 0;
	},

	allSelected: false,

	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll)
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false)
	},

	view: function(id) {
		if (!Ext.isString(id) && this.recordsSelections.length > 0) id = this.recordsSelections[0].get('id')

		if (Ext.isString(id))
			clientVM.handleNavigation({
				modelName: 'RS.customtable.Table',
				params: {
					table: this.store.getById(id).get('uname'),
					view: 'Default'
				}
			})
	}
})