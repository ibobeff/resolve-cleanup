/**
 * Model definition for the main entry point to view lookup table
 */
glu.defModel('RS.customtable.ViewLookup', {
	table: 'meta_view_lookup',
	modelName: 'MetaViewLookup',
	tableDisplayName: '~~ViewLookupTable~~',
	tableSysId: 'table_sys_id',
	view: 'Default',

	detailUrl: '',

	gridSelections: [],
	customTableStore: {
		mtype: 'store',
		model: 'RS.customtable.ViewLookupTable',
		pageSize: 0,
		proxy: {
			type: 'ajax',
			url: '/resolve/service/dbapi/getRecordData',
			reader: {
				type: 'json',
				root: 'records',
				totalProperty: 'total',
				successProperty: 'success',
				messageProperty: 'message'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	roleStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/form/getRoles',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	columns: [],

	init: function() {
		var me = this;

		me.set('columns', [{
			text: '~~appName~~',
			dataIndex: 'u_app_name',
			filterable: true,
			autoWidth: true,
			sortable: false
		}, {
			text: '~~view~~',
			dataIndex: 'u_view_name',
			filterable: true,
			autoWidth: true,
			sortable: false
		}, {
			text: '~~roles~~',
			dataIndex: 'u_roles',
			filterable: true,
			autoWidth: true,
			sortable: false,
			flex: 1
		}].concat(RS.common.grid.getSysColumns()))

		me.customTableStore.on('beforeload', function(store, operation, eOpts) {
			operation.params = operation.params || {};
			Ext.apply(operation.params, {
				modelName: me.modelName,
				useSql: false,
				type: 'table',
				orderBy: 'u_order ASC',
				limit: -1
			});
		});

		me.roleStore.load();

		me.set('tableDisplayName', me.localize('~~ViewLookupTable~~'));
	},

	newAction: function() {
		//Create a new record
		this.open({
			mtype: 'ViewLookupItem',
			order: this.customTableStore.getCount()
		});
	},
	editAction: function(sys_id) {
		//Edit selected record
		var record = this.customTableStore.getById(sys_id),
			model = this.model({
				mtype: 'ViewLookupItem'
			});
		if (record) {
			Ext.apply(record.data, {
				id: record.data.sys_id,
				appName: record.data.u_app_name,
				view: record.data.u_view_name,
				order: record.data.u_order
			});
			model.loadData(record.data);
			model.set('roles', record.data.u_roles.split(','));
			model.set('isEdit', true);
		}
		this.open(model);
	},

	allSelected: false,
	selectAllAcrossPages: function(selectAll) {
		this.set('allSelected', selectAll);
	},
	selectionChanged: function() {
		this.selectAllAcrossPages(false);
	},

	deleteAction: function() {
		//Delete the selected record(s), but first confirm the action
		Ext.MessageBox.show({
			title: this.localize('deleteTitle'),
			msg: this.localize(this.gridSelections.length == 1 ? 'deleteMessage' : 'deletesMessage', this.gridSelections.length),
			buttons: Ext.MessageBox.YESNO,
			buttonText: {
				yes: this.localize('deleteAction'),
				no: this.localize('cancel')
			},
			scope: this,
			fn: this.reallyDeleteRecords
		});
	},
	reallyDeleteRecords: function(button) {
		if (button === 'yes') {
			//Delete the selected records... as well as multi-selected records (across pages - yeah, and you thought this was going to be easy?)
			if (this.allSelected) {
				Ext.Ajax.request({
					url: '/resolve/service/dbapi/delete',
					params: {
						tableName: this.table,
						whereClause: this.customTableStore.lastWhereClause ? this.customTableStore.lastWhereClause : '1=1'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r);
						if (response.success) this.customTableStore.load();
						else{
							clientVM.displayError( response.message, null,this.localize('error'))
						}
					},
					failure: function(r){
						clientVM.displayFailure(r);
					}
				});
			} else {
				var ids = [];
				Ext.each(this.gridSelections, function(selection) {
					ids.push(selection.get('sys_id'));
				});
				Ext.Ajax.request({
					url: '/resolve/service/dbapi/delete',
					params: {
						tableName: this.table,
						whereClause: 'sys_id in (\'' + ids.join('\',\'') + '\')'
					},
					scope: this,
					success: function(r) {
						var response = RS.common.parsePayload(r);
						if (response.success) this.customTableStore.load();
						else{
							clientVM.displayError(response.message);
						}
					},
					failure: function(r) {
						clientVM.displayFailure(r);
					}
				});
			}
		}
	},
	deleteActionIsEnabled$: function() {
		return this.gridSelections.length > 0
	},
	reload: function() {
		this.customTableStore.load();
	},

	dropRecord: function(node, data, overModel, dropPosition, eOpts) {
		//Get the order of the sys_ids for the update to call into the server
		var ids = [];
		this.customTableStore.each(function(record) {
			ids.push(record.internalId);
		});
		var idString = ids.join('|&|');
		Ext.Ajax.request({
			url: '/resolve/service/lookuptable/updateorder',
			params: {
				sysIds: idString
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (response.success) {
					// this.customTableStore.load();
					//Do nothing, the grid is already updated so no need to reload the grid data
				} else {
					//if there was an error then reload the grid to reset it to the server settings
					this.customTableStore.load();
					clientVM.displayError(response.message);
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		})
	}
});