glu.defModel('RS.customtable.ViewLookupItem', {
	sys_id: '',
	appName: '',
	appNameIsValid$: function() {
		if(!this.appName) return this.localize('appNameRequired');
		return true;
	},
	view: '',
	viewIsValid$: function() {
		if(!this.view) return this.localize('viewRequired');
		return true;
	},
	roles: '',
	rolesIsValid$: function() {
		if(!this.roles) return this.localize('rolesRequired');
		return true;
	},
	order: 0,

	fields: ['sys_id', 'appName', 'view', 'roles', 'order'],

	isEdit: false,

	itemTitle$: function() {
		return this.isEdit ? this.localize('EditLookupItem') : this.localize('NewLookupItem');
	},

	save: function() {
		var params = this.asObject();
		Ext.Ajax.request({
			url: '/resolve/service/lookuptable/save',
			params: params,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if(response.success) {
					this.parentVM.reload();
					this.doClose();
				} else{
					clientVM.displayError(response.message);
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			}
		});
	},
	saveIsEnabled$: function() {
		return this.isValid;
	},
	cancel: function() {
		this.doClose();
	}
});