glu.defModel('RS.customtable.ViewDefinition', {
	wait: false,

	propCollapsed: false,

	title$: function() {
		return this.localize('windowTitle') + (this.displayName ? ' - ' + this.displayName : '')
	},

	id: '',
	tableName: '',

	type: 'Shared',

	sysCreatedOn: '',
	sysCreatedBy: '',
	sysUpdatedOn: '',
	sysUpdatedBy: '',
	sysOrganizationName: '',

	name: '',
	displayName: '',

	advanced: false,
	metaNewLink: '',
	metaFormLink: '',
	createFormId: '',
	editFormId: '',
	target: '',
	targetIsVisible$: function() {
		return this.advanced
	},

	metaNewLinkIsVisible$: function() {
		return this.advanced
	},
	metaFormLinkIsVisible$: function() {
		return this.advanced
	},

	createFormIdIsVisible$: function() {
		return !this.advanced
	},
	editFormIdIsVisible$: function() {
		return !this.advanced
	},

	targetStore: {
		mtype: 'store',
		fields: ['name', 'value'],
		proxy: {
			type: 'memory'
		}
	},

	formStore: {
		mtype: 'store',
		fields: ['sys_id', 'u_form_name', 'displayName'],
		proxy: {
			type: 'ajax',
			url: '/resolve/service/dbapi/getRecordData',
			reader: {
				type: 'json',
				root: 'records'
			},
			listeners: {
				exception: function(e, resp, op) {
					clientVM.displayExceptionError(e, resp, op);
				}
			}
		}
	},

	defaultData: {
		id: '',
		tableName: '',

		type: 'Shared',

		sysCreatedOn: '',
		sysCreatedBy: '',
		sysUpdatedOn: '',
		sysUpdatedBy: '',
		sysOrganizationName: '',

		name: '',
		displayName: '',

		advanced: false,
		metaNewLink: '',
		metaFormLink: '',
		createFormId: '',
		editFormId: '',
		target: ''
	},

	accessRight: {},
	accessRights: {
		mtype: 'store',
		fields: ['name', 'value', {
			name: 'admin',
			type: 'bool'
		}, {
			name: 'edit',
			type: 'bool'
		}, {
			name: 'view',
			type: 'bool'
		}, {
			name: 'execute',
			type: 'bool'
		}],
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	},
	accessRightsSelections: [],

	fields: ['name', 'displayName', 'metaNewLink', 'metaFormLink', 'createFormId', 'editFormId', 'target', 'id', 'metaTableSysId', 'type'].concat(RS.common.grid.getSysFields()),

	activate: function(screen, params) {
		clientVM.setWindowTitle(this.localize('windowTitle'))
		this.resetForm()
		this.set('id', params && params.id ? params.id : '')
		this.set('tableName', params && params.tableName ? params.tableName : '')
		this.set('name', params && params.name ? params.name : '')
		this.formStore.load()
		this.loadRecord()
	},

	init: function() {
		this.targetStore.add([{
			name: this.localize('newWindow'),
			value: '_blank'
		}, {
			name: this.localize('sameWindow'),
			value: 'default'
		}])
		this.targetStore.sort('name')

		this.formStore.on('beforeload', function(store, operation) {
			operation.params = operation.params || {}
			Ext.apply(operation.params, {
				//modelName: 'MetaFormView',
				tableName: 'meta_form_view',
				type: 'table',
				start: 0,
				limit: -1,
				filter: Ext.encode([{
					field: 'u_table_name',
					condition: 'equals',
					value: this.tableName
				}])
			})
		}, this)
	},

	userDateFormat$: function() {
		return clientVM.getUserDefaultDateFormat()
	},

	loadRecord: function() {
		if (this.name && this.tableName) {
			this.set('wait', true)
			this.ajax({
				url: '/resolve/service/view/read',
				params: {
					table: this.tableName,
					view: this.name
				},
				scope: this,
				success: function(r) {
					var response = RS.common.parsePayload(r)
					if (response.success) {
						this.loadData(response.data)
						if (this.metaNewLink || this.metaFormLink) this.set('advanced', true)
						this.processAccessRights(response.data)
					} else clientVM.displayError(response.message)
				},
				failure: function(r) {
					clientVM.displayFailure(r);
				},
				callback: function() {
					this.set('wait', false)
				}
			})
		}

		this.ajax({
			url: '/resolve/service/dbapi/getRecordData',
			params: {
				//modelName: 'MetaTable',
				tableName: 'meta_table',
				type: 'form',
				start: 0,
				limit: 1,
				filter: Ext.encode([{
					field: 'u_name',
					condition: 'equals',
					type: 'auto',
					value: this.tableName
				}])
			},
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (!response.success)
					clientVM.displayError(response.message)
				else {
					this.set('metaTableSysId', response.data.id)
				}
			},
			failure: function(f) {
				this.set('wait', false)
				clientVM.displayFailure(f);
			}
		})
	},

	saveRecord: function(exitAfterSave) {
		this.set('wait', true)
		var obj = this.asObject();
		obj.metaTableName = this.tableName

		if (!this.advanced) {
			obj.metaNewLink = ''
			obj.metaFormLink = ''
		}

		//Add in access rights
		var viewRoles = [],
			editRoles = [],
			adminRoles = [],
			executeRoles = [];
		this.accessRights.each(function(accessRight) {
			if (accessRight.data.view) viewRoles.push(accessRight.data.name);
			if (accessRight.data.edit) editRoles.push(accessRight.data.name);
			if (accessRight.data.admin) adminRoles.push(accessRight.data.name);
			if (accessRight.data.execute) executeRoles.push(accessRight.data.name);
		});

		Ext.apply(obj, {
			viewRoles: viewRoles.join(','),
			editRoles: editRoles.join(','),
			adminRoles: adminRoles.join(',')
		})

		this.ajax({
			url: '/resolve/service/view/save',
			jsonData: obj,
			scope: this,
			success: function(r) {
				var response = RS.common.parsePayload(r);
				if (!response.success)
					clientVM.displayError(response.message)
				else {
					clientVM.displaySuccess(this.localize('savedViewDefinition'))
					if (exitAfterSave === true) {
						clientVM.handleNavigation({
							modelName: 'RS.customtable.Main'
						})
					} else
						this.loadData(response.data)
				}
			},
			failure: function(r) {
				clientVM.displayFailure(r);
			},
			callback: function() {
				this.set('wait', false)
			}
		})
	},

	save: function(exitAfterSave) {
		if (this.task) this.task.cancel()
		this.task = new Ext.util.DelayedTask(this.saveRecord, this, [exitAfterSave])
		this.task.delay(500)
	},
	saveIsEnabled$: function() {
		return !this.wait;
	},
	saveAndExit: function() {
		if (!this.saveIsEnabled)
			return;
		if (this.task) this.task.cancel()
		this.save(true)
	},
	back: function() {
		clientVM.handleNavigation({
			modelName: 'RS.customtable.TableDefinition',
			params: {
				name: this.tableName
			}
		})
	},
	resetForm: function() {
		this.loadData(this.defaultData)
	},
	refresh: function() {
		this.loadRecord()
	},

	addAccessRight: function() {
		var rights = [];
		this.accessRights.each(function(right) {
			rights.push(right.data);
		});
		this.open({
			mtype: 'RS.formbuilder.AccessRightsPicker',
			currentRights: rights
		});
	},
	addRealAccessRights: function(accessRights) {
		Ext.each(accessRights, function(accessRight) {
			this.accessRights.add(Ext.applyIf(accessRight.data, {
				view: true,
				edit: true,
				admin: true
			}));
		}, this);
	},
	removeAccessRightIsEnabled: {
		on: ['accessRightsSelectionsChanged'],
		formula: function() {
			return this.accessRightsSelections.length > 0;
		}
	},
	removeAccessRight: function() {
		for (var i = 0; i < this.accessRightsSelections.length; i++) {
			this.accessRights.remove(this.accessRightsSelections[i]);
		}
	},

	processAccessRights: function(rightObj) {
		var viewRoles = rightObj.viewRoles ? rightObj.viewRoles.split(',') : [],
			editRoles = rightObj.editRoles ? rightObj.editRoles.split(',') : [],
			adminRoles = rightObj.adminRoles ? rightObj.adminRoles.split(',') : [],
			// executeRoles = rightObj.uexecuteAccess ? rightObj.uexecuteAccess.split(',') : [],
			accessRights = [],
			i;
		for (i = 0; i < viewRoles.length; i++)
			this.addToAccessRights(accessRights, viewRoles[i], 'view', true);
		for (i = 0; i < editRoles.length; i++)
			this.addToAccessRights(accessRights, editRoles[i], 'edit', true);
		for (i = 0; i < adminRoles.length; i++)
			this.addToAccessRights(accessRights, adminRoles[i], 'admin', true);
		// for (i = 0; i < executeRoles.length; i++)
		// 	this.addToAccessRights(accessRights, executeRoles[i], 'execute', true);

		this.accessRights.removeAll()
		this.accessRights.add(accessRights)
	},
	addToAccessRights: function(accessRights, name, right, value) {
		var i, len = accessRights.length;
		for (i = 0; i < len; i++) {
			if (accessRights[i].name == name) {
				accessRights[i][right] = value;
				return;
			}
		}

		var newName = {
			name: name,
			value: name
		};
		newName[right] = value;
		accessRights.push(newName);
	},

	view: function() {
		clientVM.handleNavigation({
			modelName: 'RS.customtable.Table',
			params: {
				table: this.tableName,
				view: this.name
			}
		})
	},
	viewIsEnabled$: function() {
		return this.id && this.name
	}
});