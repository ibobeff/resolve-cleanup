glu.defView('RS.customtable.ViewDefinition', {
	autoScroll: true,
	padding: 15,	
	layout: {
		type: 'vbox',
		align: 'stretch'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls : 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{title}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'		
		}, {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, 'view', '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sysCreatedOn}',
			sysCreatedBy: '@{sysCreatedBy}',
			sysUpdated: '@{sysUpdatedOn}',
			sysUpdatedBy: '@{sysUpdatedBy}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],	
	items: [{
		xtype : 'container',
		layout : {
			type : 'vbox',
			align : 'stretch'
		},
		style : 'border-top:1px solid silver',
		padding : '8 0',
		defaultType: 'textfield',
		defaults : {
			labelWidth : 120,
			margin : '4 0'
		},
		items : ['name', 'displayName', {
			xtype: 'displayfield',
			name: 'tableName'
		}, {
			name: 'advanced',
			xtype: 'checkbox',
			hideLabel: true,
			margin: '0px 0px 0px 125px',
			boxLabel: '~~advanced~~'
		},
		'metaNewLink', 'metaFormLink', {
			xtype: 'combobox',
			editable: false,
			name: 'createFormId',
			store: '@{formStore}',
			displayField: 'u_display_name',
			valueField: 'sys_id',
			queryMode: 'local',
			displayTpl: '<tpl for=".">{[typeof values === "string" ? values : values["u_display_name"] || values["u_form_name"]]}</tpl>',
			tpl: ['<ul class="x-list-plain"><tpl for=".">',
				'<li role="option" class="x-boundlist-item">',
				'{[values["u_display_name"] || values["u_form_name"]]}</li>',
				'</tpl></ul>'
			].join('')
		}, {
			xtype: 'combobox',
			editable: false,
			name: 'editFormId',
			store: '@{formStore}',
			displayField: 'u_display_name',
			valueField: 'sys_id',
			queryMode: 'local',
			displayTpl: '<tpl for=".">{[typeof values === "string" ? values : values["u_display_name"] || values["u_form_name"]]}</tpl>',
			tpl: ['<ul class="x-list-plain"><tpl for=".">',
				'<li role="option" class="x-boundlist-item">',
				'{[values["u_display_name"] || values["u_form_name"]]}</li>',
				'</tpl></ul>'
			].join('')
		}, {
			xtype: 'combobox',
			name: 'target',
			store: '@{targetStore}',
			displayField: 'name',
			valueField: 'value',
			queryMode: 'local'
		}]
	},{
		xtype: 'grid',
		cls : 'rs-grid-dark',
		autoScroll : true,
		flex: 1,
		minHeight: 200,		
		title: '~~AccessRights~~',
		name: 'accessRights',
		dockedItems: [{
			xtype : 'toolbar',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			items : ['addAccessRight','removeAccessRight']
		}],
		selModel: {
			selType: 'cellmodel'
		},
		plugins: [{
			ptype: 'cellediting',
			clicksToEdit: 2
		}],
		columns: [{
			header: '~~Name~~',
			dataIndex: 'name',
			hideable: false,
			flex: 1,
			editor: {
				allowBlank: false
			}
		}, {
			xtype: 'checkcolumn',
			stopSelection: false,
			header: '~~adminRight~~',
			dataIndex: 'admin',
			hideable: false,
			width: 80
		}, {
			xtype: 'checkcolumn',
			stopSelection: false,
			header: '~~editRight~~',
			dataIndex: 'edit',
			hideable: false,
			width: 80
		}, {
			xtype: 'checkcolumn',
			stopSelection: false,
			header: '~~viewRight~~',
			dataIndex: 'view',
			hideable: false,
			width: 80
		}
		/*, {
			xtype: 'checkcolumn',
			stopSelection: false,
			header: '~~executeRight~~',
			dataIndex: 'execute',
			hideable: false,
			width: 75
		}*/
		]
	}]
});
