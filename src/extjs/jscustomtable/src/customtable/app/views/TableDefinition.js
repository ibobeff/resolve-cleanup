glu.defView('RS.customtable.TableDefinition', {
	padding : 15,	
	autoScroll: true,
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		ui: 'display-toolbar',
		defaultButtonUI: 'display-toolbar-button',
		cls : 'rs-dockedtoolbar',
		margin : '0 0 15 0',
		items: [{
			xtype: 'tbtext',
			cls: 'rs-display-name',
			text: '@{title}'
		}]
	}, {
		xtype: 'toolbar',
		dock: 'top',
		cls: 'actionBar rs-dockedtoolbar',
		defaults : {
			cls : 'rs-small-btn rs-btn-light'
		},
		items: [{
			name: 'back',
			iconCls: 'icon-large icon-reply-all rs-icon'		
		}, 'viewForms', {
			name: 'save',
			listeners: {
				doubleClickHandler: '@{saveAndExit}',
				render: function(button) {
					button.getEl().on('dblclick', function() {
						button.fireEvent('doubleClickHandler')
					});
					clientVM.updateSaveButtons(button);
				}
			}
		}, '->', {
			xtype: 'sysinfobutton',
			sysId: '@{id}',
			sysCreated: '@{sys_created_on}',
			sysCreatedBy: '@{sys_created_by}',
			sysUpdated: '@{sys_updated_on}',
			sysUpdatedBy: '@{sys_updated_by}',
			sysOrg: '@{sysOrganizationName}',
			dateFormat: '@{userDateFormat}'
		}, {
			iconCls: 'x-tbar-loading',
			handler: '@{refresh}',
			listeners: {
				render: function(button) {
					clientVM.updateRefreshButtons(button);
				}
			}
		}]
	}],
	layout: {
		type: 'vbox',
		align: 'stretch'
	},	
	items: [{
		xtype : 'container',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},	
		style : 'border-top:1px solid silver',
		padding : '8 0',
		defaults : {
			labelWidth : 120
		},
		items : [{
			xtype: 'displayfield',
			name : 'u_name' 
		},{
			xtype: 'textfield',
			name: 'u_display_name'
		}]
	}, {
		title: '~~columns~~',
		flex: 1,
		minHeight: 200,		
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'columns',
		store: '@{store}',
		columns: '@{columns}',
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			name: 'actionBar',
			items: []
		}],
		plugins: [{
			ptype: 'pager'
		}]
	}, {
		title: '~~views~~',
		flex: 1,
		minHeight: 200,
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'views',
		columns: '@{viewsColumns}',		
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'top',
			cls: 'actionBar rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light',
			},
			name: 'actionBar',
			items: ['createView', 'deleteView', 'viewView']
		}],
		selModel: {
			selType: 'resolvecheckboxmodel',	
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'name'
		},
		plugins: [{
			ptype: 'pager',
			showSysInfo: false
		}],
		listeners: {
			editAction: '@{editView}'
		}
	}]
});
