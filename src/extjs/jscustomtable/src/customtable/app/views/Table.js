glu.defView('RS.customtable.Table', {
	layout: 'card',
	activeItem: '@{activeCard}',
	padding: 15,
	items: [{
		dockedItems: [{
			xtype: 'toolbar',			
			name: 'actionBar',
			items: '@{toolbarItems}',
			cls : 'rs-dockedtoolbar',
			defaults : {
				cls : 'rs-small-btn rs-btn-light'
			},
			itemTemplate: {
				xtype: 'button',
				text: '@{text}',
				name: 'action'
			}
		}],
		xtype: 'grid',
		cls : 'rs-grid-dark',
		name: 'grid',
		displayName: '@{displayName}',
		setDisplayName: function(name) {
			this.getPlugin('search').setDisplayName(name)
		},
		tableName: '@{table}',
		setTableName: function(name) {
			this.tableName = name
		},
		tableSysId: '@{tableSysId}',
		setTableSysId: function(id) {
			this.tableSysId = id
		},
		viewName: '@{view}',
		serverData: '@{serverData}',
		setServerData: function(data) {
			this.serverData = data
			this.fireEvent('serverDataChanged')
		},
		// stateId: '@{tableSysId}',
		// stateful: true,
		plugins: [{
			ptype: 'searchfilter',
			pluginId: 'search'
		}, {
			ptype: 'pager',
			showSysInfo: false
		}, {
			ptype: 'columnautowidth'
		}],
		store: '@{customTableStore}',
		selModel: {
			selType: 'resolvecheckboxmodel',
			columnTarget: '_self',
			columnEventName: 'editAction',
			columnIdField: 'sys_id'
		},		
		columns: '@{columns}',
		viewConfig: {
			enableTextSelection: true
		},
		setColumns: function(columns) {
			this.reconfigure(null, columns)
		},
		listeners: {
			editAction: '@{editAction}',
			selectAllAcrossPages: '@{selectAllAcrossPages}',
			selectionChange: '@{selectionChanged}',
			viewSelected: '@{viewSelected}'
		}
	}, {
		xtype: 'panel',
		url: '@{detailUrl}',
		frameTemplate: '<iframe class="rs_iframe" src="{0}" style="height:100%;width:100%;border:0" />',
		border: false,
		html: '',
		setUrl: function(url) {
			this.update(Ext.String.format(this.frameTemplate, url));
			this.getEl().select('iframe').on('load', function(e, t, eOpts) {
				var me = this;
				t.contentWindow.registerForm = function(form) {
					form.on('beforeclose', function() {
						this.fireEvent('reloadAndShowList');
						return false;
					}, me);
					form.on('titleChange', function() {
						return false;
					});
					form.on('beforeredirect', function(url) {
						window.open(url, '_self');
						return false;
					})
				}
			}, this);
		},
		listeners: {
			reloadAndShowList: '@{reloadAndShowList}'
		}
	}, {
		xtype: '@{newForm}',
		listeners: {
			beforeclose: '@{..reloadAndShowList}'
		}
	}, {
		xtype: '@{editForm}',
		listeners: {
			beforeclose: '@{..reloadAndShowList}'
		}
	}]
});