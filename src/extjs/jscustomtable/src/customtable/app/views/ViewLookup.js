glu.defView('RS.customtable.ViewLookup', {
	padding: '10px',
	xtype: 'grid',
	name: 'grid',
	displayName: '@{tableDisplayName}',
	tableName: '@{table}',
	tableSysId: '@{tableSysId}',
	viewName: '@{view}',
	stateId: 'viewLookupList',
	stateful: true,
	plugins: [{
		ptype: 'searchfilter',
		hideMenu: true,
		allowPersistFilter: false
	}, {
		ptype: 'pager',
		pageable: false
	}],
	store: '@{customTableStore}',
	selModel: {
		selType: 'resolvecheckboxmodel',
		columnTooltip: RS.common.locale.editColumnTooltip,
		columnTarget: '_self',
		columnEventName: 'editAction',
		columnIdField: 'sys_id'
	},
	dockedItems: [{
		xtype: 'toolbar',
		dock: 'top',
		name: 'actionBar',
		items: [{
			name: 'newAction'
		}, {
			name: 'deleteAction'
		}]
	}],
	columns: '@{columns}',
	viewConfig: {
		enableTextSelection: false,
		plugins: {
			ptype: 'gridviewdragdrop',
			dragText: 'Drag and drop to reorganize'
		},
		listeners: {
			drop: function() {
				this.ownerCt.fireEvent('drop', arguments);
			}
		}
	},
	listeners: {
		editAction: '@{editAction}',
		selectAllAcrossPages: '@{selectAllAcrossPages}',
		selectionChange: '@{selectionChanged}',
		drop: '@{dropRecord}'
	}
});