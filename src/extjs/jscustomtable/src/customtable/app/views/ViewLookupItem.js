glu.defView('RS.customtable.ViewLookupItem', {
	modal: true,
	width: 450,
	height: 200,
	title: '@{itemTitle}',
	layout: 'fit',
	items: [{
		bodyPadding: '10px',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		defaults: {
			labelWidth: 125
		},
		items: [{
			xtype: 'textfield',
			name: 'appName'
		}, {
			xtype: 'textfield',
			name: 'view'
		}, {
			xtype: 'combobox',
			name: 'roles',
			store: '@{..roleStore}',
			displayField: 'name',
			valueField: 'value',
			multiSelect: true,
			queryMode: 'local'
		}],
		buttonAlign: 'left',
		buttons: [{
			name: 'save'
		}, {
			name: 'cancel'
		}]
	}]
});