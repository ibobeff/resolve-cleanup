/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.dto;

import com.resolve.services.interfaces.DTO;



public class AttachmentDTO extends DTO
{
    private static final long serialVersionUID = -989086572498493617L;
    
    //use this to download the file
    private String tableName;
    
    //other file info
    private String fileSysId;//mapped to the wikiattachment table
    private String fileName;
    private int size;
    private String uploadedBy;
    private String uploadedOn;
    private String downloadUrl;
    private boolean global;//used by UI to identify global attachment
    
    
    public boolean isGlobal()
    {
        return global;
    }
    public void setGlobal(boolean global)
    {
        this.global = global;
    }
    public String getFileName()
    {
        return fileName;
    }
    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }
    public int getSize()
    {
        return size;
    }
    public void setSize(int size)
    {
        this.size = size;
    }
    public String getUploadedBy()
    {
        return uploadedBy;
    }
    public void setUploadedBy(String uploadedBy)
    {
        this.uploadedBy = uploadedBy;
    }
    public String getUploadedOn()
    {
        return uploadedOn;
    }
    public void setUploadedOn(String uploadedOn)
    {
        this.uploadedOn = uploadedOn;
    }

    public String getTableName()
    {
        return tableName;
    }

    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }

    public String getFileSysId()
    {
        return fileSysId;
    }

    public void setFileSysId(String fileSysId)
    {
        this.fileSysId = fileSysId;
    }
    public String getDownloadUrl()
    {
        return downloadUrl;
    }
    public void setDownloadUrl(String downloadUrl)
    {
        this.downloadUrl = downloadUrl;
    }

    
    
    
}
