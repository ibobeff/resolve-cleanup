/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.wiki.web.vo;

import java.util.Date;

/**
 * This is a value object to hold data that will be displayed on the UI
 * 
 * @author jeet.marwah
 * 
 */
public class DocumentInfoVO
{

	private String namespace;
	private String documentName;
	private String documentFullName;
	private String viewLink;
	private String editLink;
	private String modifiedBy;
	private Date modifiedOn;

	private String roleName;//used for homepage list display

	public String getNamespace()
	{
		return namespace;
	}

	public void setNamespace(String namespace)
	{
		this.namespace = namespace;
	}

	public String getDocumentName()
	{
		return documentName;
	}

	public void setDocumentName(String documentName)
	{
		this.documentName = documentName;
	}

	public String getDocumentFullName()
	{
		return documentFullName;
	}

	public void setDocumentFullName(String documentFullName)
	{
		this.documentFullName = documentFullName;
	}

	public String getViewLink()
	{
		return viewLink;
	}

	public void setViewLink(String viewLink)
	{
		this.viewLink = viewLink;
	}

	public String getEditLink()
	{
		return editLink;
	}

	public void setEditLink(String editLink)
	{
		this.editLink = editLink;
	}

	public String getModifiedBy()
	{
		if(modifiedBy == null)
			modifiedBy = "";
		
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn()
	{
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn)
	{
		this.modifiedOn = modifiedOn;
	}
	public String getRoleName()
	{
		return roleName;
	}

	public void setRoleName(String roleName)
	{
		this.roleName = roleName;
	}


}
