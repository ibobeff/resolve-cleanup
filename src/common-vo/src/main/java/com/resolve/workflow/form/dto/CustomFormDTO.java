/******************************************************************************
* (C) Copyright 2017
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.workflow.form.dto;

import java.util.Date;

import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

/**
 * @author hemant.phanasgaonkar
 */
public class CustomFormDTO
{
    private String id;
    private String UFormName;
    private String UViewName;
    private String UDisplayName;
    private String UTableName;
    private String UTableDisplayName;
    private Date sysCreatedOn;
    private String sysCreatedBy;
    private Date sysUpdatedOn;
    private String sysUpdatedBy;
    
    public String getId()
    {
        return id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getUFormName()
    {
        return UFormName;
    }
    
    public void setUFormName(String uFormName)
    {
        UFormName = uFormName;
    }
    
    public String getUViewName()
    {
        return UViewName;
    }
    
    public void setUViewName(String uViewName)
    {
        UViewName = uViewName;
    }
    
    public String getUDisplayName()
    {
        return UDisplayName;
    }

    public void setUDisplayName(String uDisplayName)
    {
        UDisplayName = uDisplayName;
    }
    
    public String getUTableName()
    {
        return UTableName;
    }

    public void setUTableName(String uTableName)
    {
        UTableName = uTableName;
    }
    
    public String getUTableDisplayName()
    {
        return UTableDisplayName;
    }

    public void setUTableDisplayName(String uTableDisplayName)
    {
        UTableDisplayName = uTableDisplayName;
    }
    
    public Date getSysCreatedOn()
    {
        return sysCreatedOn;
    }
    
    public void setSysCreatedOn(Date sysCreatedOn)
    {
        this.sysCreatedOn = sysCreatedOn;
    }
    
    public String getSysCreatedBy()
    {
        String result = this.sysCreatedBy;
        if(StringUtils.isEmpty(result))
        {
            result = null;
        }

        return result;
    }
    
    public void setSysCreatedBy(String sysCreatedBy)
    {
        if (StringUtils.isNotBlank(sysCreatedBy) || StringUtils.isBlank(this.sysCreatedBy) || this.sysCreatedBy.equals(VO.STRING_DEFAULT))
        {
            this.sysCreatedBy = sysCreatedBy != null ? sysCreatedBy.trim() : sysCreatedBy;
        }
    }
    
    public Date getSysUpdatedOn()
    {
        return sysUpdatedOn;
    }
    
    public void setSysUpdatedOn(Date sysUpdatedOn)
    {
        this.sysUpdatedOn = sysUpdatedOn;
    }
    
    public String getSysUpdatedBy()
    {
        String result = this.sysUpdatedBy;
        if(StringUtils.isEmpty(result))
        {
            result = null;
        }
        
        return result;
    }
    
    public void setSysUpdatedBy(String sysUpdatedBy)
    {
        if (StringUtils.isNotBlank(sysUpdatedBy) || StringUtils.isBlank(this.sysUpdatedBy) || this.sysUpdatedBy.equals(VO.STRING_DEFAULT))
        {
            this.sysUpdatedBy = sysUpdatedBy != null ? sysUpdatedBy.trim() : sysUpdatedBy;
        }
    }
}
