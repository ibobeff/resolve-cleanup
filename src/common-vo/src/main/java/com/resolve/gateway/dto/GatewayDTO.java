/******************************************************************************
* (C) Copyright 2019
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.dto;

import java.util.Date;

import com.resolve.services.hibernate.vo.GatewayPropertiesVO;
import com.resolve.util.DateUtils;
import com.resolve.util.StringUtils;

public class GatewayDTO {
    private String containerName;
    private String containerIp;
    private String type;
    private Date updatedOn;   // Last deployed filter time stamp
    private String updatedBy; // Last deployed filter user name
    private boolean originalPrimary;
    private GatewayPropertiesVO gatewayProperties;

    public String getContainerName() {
        return containerName;
    }
    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }
    public String getContainerIp() {
        return containerIp;
    }
    public void setContainerIp(String containerIp) {
        this.containerIp = containerIp;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public GatewayPropertiesVO getGatewayProperties() {
        return gatewayProperties;
    }
    public void setGatewayProperties(GatewayPropertiesVO gatewayProperties) {
        this.gatewayProperties = gatewayProperties;
    }
    public Date getUpdatedOn() {
        return updatedOn;
    }
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }
    public String getUpdatedBy() {
        return updatedBy;
    }
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    public boolean isOriginalPrimary() {
        return originalPrimary;
    }
    public void setOriginalPrimary(boolean originalPrimary) {
        this.originalPrimary = originalPrimary;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 43;

        result = prime * result + gatewayProperties.getUGatewayName().hashCode();
        result = prime * result + type.hashCode();
        result = prime * result + gatewayProperties.getUOrgName().hashCode();
        result = prime * result + gatewayProperties.getURSRemoteName().hashCode();

        return result;
    }
    
    @Override
    public String toString() {
    	StringBuilder strBldr = new StringBuilder();
    	
    	strBldr.append(GatewayDTO.class.getSimpleName()).append("{").append("Container Name: ")
    	.append(StringUtils.isNotBlank(containerName) ? containerName : "")
    	.append(", Container IP: ").append(StringUtils.isNotBlank(containerIp) ? containerIp : "").append(", type").append(type)
    	.append(updatedOn != null ? String.format(", Updated On: %s", DateUtils.getISODateString(updatedOn.getTime())) : "")
    	.append(StringUtils.isNotBlank(updatedBy) ? String.format(", Updated By: %s", updatedBy) : "")
    	.append(", Original Primary: ").append(originalPrimary)
    	.append(", Gateway Properties as Map: [")
    	.append(gatewayProperties != null ? StringUtils.mapToString(gatewayProperties.toMap(), "=", ", ") : "")
    	.append("]}");
    	
    	return strBldr.toString();
    }
}
