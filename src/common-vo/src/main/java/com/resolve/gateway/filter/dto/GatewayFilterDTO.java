/******************************************************************************
* (C) Copyright 2019
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.filter.dto;

import com.resolve.services.hibernate.vo.GatewayFilterVO;

public class GatewayFilterDTO extends GatewayFilterVO {
    private static final long serialVersionUID = 1L;
    public GatewayFilterDTO() {
        super();
    }
}
