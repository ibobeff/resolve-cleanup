/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Map;

import com.resolve.services.interfaces.IndexComponent;
import com.resolve.services.interfaces.VO;
import com.resolve.services.util.JsonUtils;

public class ResolveActionTaskMockDataVO extends VO implements IndexComponent
{
    private static final long serialVersionUID = 3092790539672880425L;
    
    private String sys_id;
    private String UName;
    private String UData;
    private String UParams;
    private String UInputs;
    private String UFlows;
    private Boolean UIsActive;
    private String UDescription;
    
    private ResolveActionInvocVO resolveActionInvoc;
    
    public String getSys_id()
    {
        return this.sys_id;
    }
    public void setSys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }

    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    public String getUData()
    {
        return this.UData;
    }
    public void setUData(String UData)
    {
        this.UData = UData;
    }

    public String getUParams()
    {
        return this.UParams;
    }
    public void setUParams(String UParams)
    {
        this.UParams = UParams;
    }

    public String getUInputs()
    {
        return this.UInputs;
    }
    public void setUInputs(String UInputs)
    {
        this.UInputs = UInputs;
    }

    public String getUFlows()
    {
        return this.UFlows;
    }
    public void setUFlows(String UFlows)
    {
        this.UFlows = UFlows;
    }
    
    public String getUDescription()
    {
        return UDescription;
    }

    public void setUDescription(String uDescription)
    {
        UDescription = uDescription;
    }
    
    public Map convertParamsToMap()
    {
        return JsonUtils.convertJsonStringToMap(this.UParams);
    }
    
    public void convertMapToParams(Map params)
    {
        this.UParams = JsonUtils.convertMapToJsonString(params);
    }

    public Map convertInputsToMap()
    {
        return JsonUtils.convertJsonStringToMap(this.UInputs);
    }
    
    public void convertMapToInputs(Map inputs)
    {
        this.UInputs = JsonUtils.convertMapToJsonString(inputs);
    }

    public Map convertFlowsToMap()
    {
        return JsonUtils.convertJsonStringToMap(this.UFlows);
    }
    
    public void convertMapToFlows(Map flows)
    {
        this.UFlows = JsonUtils.convertMapToJsonString(flows);
    }

    public Boolean getUIsActive()
    {
        return this.UIsActive;
    }
    public void setUIsActive(Boolean active)
    {
        this.UIsActive = active;
    }
    public ResolveActionInvocVO getResolveActionInvoc()
    {
        return resolveActionInvoc;
    }

    public void setResolveActionInvocVO(ResolveActionInvocVO resolveActionInvocVO)
    {
        this.resolveActionInvoc = resolveActionInvocVO;
    }
    
    public String ugetIndexContent()
    {
        return UName + " " + UData;
    }
    
    
}
