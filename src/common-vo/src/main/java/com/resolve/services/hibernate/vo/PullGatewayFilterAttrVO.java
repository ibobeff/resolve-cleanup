package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;

public class PullGatewayFilterAttrVO extends VO
{
    private static final long serialVersionUID = 1L;
    
    private String UName;
    private String UValue;
    private String UPullFilterId;
    
    @MappingAnnotation(columnName="NAME")
    public String getUName()
    {
        return UName;
    }
    
    public void setUName(String uName)
    {
        UName = uName;
    }
    
    @MappingAnnotation(columnName="VALUE")
    public String getUValue()
    {
        return UValue;
    }
    
    public void setUValue(String uValue)
    {
        UValue = uValue;
    }
    
    @MappingAnnotation(columnName="PULL_FILTER_ID")
    public String getUPullFilterId()
    {
        return UPullFilterId;
    }
    
    public void setUPullFilterId(String uPullFilterId)
    {
        UPullFilterId = uPullFilterId;
    }
    
} // class PullGatewayFilterAttrVO
