/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Set;

import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.StringUtils;

/**
 * 
 * represents the Graph Node model in SQL
 * 
 * @author jeet.marwah
 *
 */
public class ResolveNodeVO  extends VO
{
    private static final long serialVersionUID = 8310594750273638520L;
    
    private static final String DISPLAYNAME = "displayName";
    
    //wiki namespace - for the root node. This will be applied to all the nodes if the namespace of the doc is not present
    private String UCompSysId;
    private String UCompName;
    private NodeType UType;
    private Boolean ULock;
    private Boolean UPinned;
    private Boolean UMarkDeleted;
    private String UReadRoles;
    private String UEditRoles;
    private String UPostRoles;

    private Set<ResolveEdgeVO> edges;
    private Set<ResolveNodePropertiesVO> properties;
    
    public ResolveNodeVO() {}
    public ResolveNodeVO(String sysId) 
    {
        this.setSys_id(sysId);
    }
    
    @Override
    public boolean validate() throws Exception
    {
        boolean valid = true;
        
        if(UType == null || StringUtils.isEmpty(UCompSysId) || StringUtils.isEmpty(UCompName))
        {
            throw new Exception("SysId, Name and Type are mandatory.");
        }
        
        return valid;
    }

    public Set<ResolveEdgeVO> getEdges()
    {
        return edges;
    }

    public void setEdges(Set<ResolveEdgeVO> edges)
    {
        this.edges = edges;
    }
    
    public String getUCompSysId()
    {
        return UCompSysId;
    }

    public void setUCompSysId(String uCompSysId)
    {
        UCompSysId = uCompSysId;
    }
    
    public String getUCompName()
    {
        return UCompName;
    }
    public void setUCompName(String uName)
    {
        UCompName = uName;
    }
    public NodeType getUType()
    {
        return UType;
    }
    public void setUType(NodeType uType)
    {
        UType = uType;
    }
    
    public Boolean getULock()
    {
        return ULock;
    }

    public void setULock(Boolean uLock)
    {
        ULock = uLock;
    }
    
    public Boolean ugetULock()
    {
        if (this.ULock == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.ULock;
        }
    }
    
    public Boolean getUPinned()
    {
        return UPinned;
    }

    public void setUPinned(Boolean uPinned)
    {
        UPinned = uPinned;
    }
    
    public Boolean ugetUPinned()
    {
        if (this.UPinned == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UPinned;
        }
    }
    
    public Boolean getUMarkDeleted()
    {
        return UMarkDeleted;
    }
    
    public Boolean ugetUMarkDeleted()
    {
        if (this.UMarkDeleted == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UMarkDeleted;
        }
        
    }

    public void setUMarkDeleted(Boolean uMarkDeleted)
    {
        UMarkDeleted = uMarkDeleted;
    }

    public Set<ResolveNodePropertiesVO> getProperties()
    {
        return properties;
    }

    public void setProperties(Set<ResolveNodePropertiesVO> properties)
    {
        this.properties = properties;
    }
    
    public String getUReadRoles()
    {
        return UReadRoles;
    }

    public void setUReadRoles(String uReadRoles)
    {
        UReadRoles = uReadRoles;
    }

    public String getUEditRoles()
    {
        return UEditRoles;
    }

    public void setUEditRoles(String uEditRoles)
    {
        UEditRoles = uEditRoles;
    }

    public String getUPostRoles()
    {
        return UPostRoles;
    }

    public void setUPostRoles(String uPostRoles)
    {
        UPostRoles = uPostRoles;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        boolean result = false;
        
        if(obj != null && obj instanceof ResolveNodeVO)
        {
            ResolveNodeVO that = (ResolveNodeVO) obj;
            if(StringUtils.isNotEmpty(this.getSys_id()) && StringUtils.isNotEmpty(that.getSys_id()))
            {
                if(this.getSys_id().equalsIgnoreCase(that.getSys_id()))
                {
                    result = true;
                }
            }
            else if(StringUtils.isNotEmpty(this.getUCompSysId()) && StringUtils.isNotEmpty(that.getUCompSysId())
                            && StringUtils.isNotEmpty(this.getUCompName()) && StringUtils.isNotEmpty(that.getUCompName())
                            )
            {
                if(this.getUCompSysId().equalsIgnoreCase(that.getUCompSysId()) && this.getUCompName().equalsIgnoreCase(that.getUCompName()) )
                {
                    result = true;
                }
            }
            
        }
        return result;
    }

    @Override
    public int hashCode() 
    {
        int hash = 3;
        
        if(StringUtils.isNotBlank(this.getSys_id()))
        {
            hash = 7 * hash + (StringUtils.isNotEmpty(this.getSys_id()) ? this.getSys_id().hashCode() : 1);
        }
        else
        {
            hash = 7 * hash + (StringUtils.isNotEmpty(this.getUCompSysId()) ? this.getUCompSysId().hashCode() : 1);
            hash = 7 * hash + (StringUtils.isNotEmpty(this.getUCompName()) ? this.getUCompName().hashCode() : 1);
            hash = 7 * hash + (this.getUType() != null ? this.getUType().name().hashCode() : 1);
        }
        return hash;
    }
    
    @Override
    public String toString()
    {
        return this.getId() + "-" + this.getUCompSysId() + "-" + this.getUCompName() + "-" + this.getUType().name();
    }

    public String getDisplayName()
    {
        String displayName = getUCompName();
        
        if(properties != null && !properties.isEmpty())
        {
            for(ResolveNodePropertiesVO prop : properties)
            {
                if(prop.getUKey().equalsIgnoreCase(DISPLAYNAME))
                {
                    String propValue = prop.getUValue();
                    
                    if(StringUtils.isNotBlank(propValue))
                    {
                        displayName = propValue;
                        break;
                    }
                }
            }
        }
        
        return displayName;
    }
}
