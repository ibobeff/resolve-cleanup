package com.resolve.services.hibernate.vo;

import java.util.ArrayList;
import java.util.List;

import com.resolve.services.interfaces.VO;

public class CaseAttributeVO extends VO {
	private static final long serialVersionUID = -6988807923303323107L;

	private String columnName;
	private String displayName;
	private Boolean active;
	private Boolean showOnDashboard;
	private Boolean showOnIncident;
	private Boolean useAsFilter;
	private List<CaseValueVO> valueRange = new ArrayList<>();
	private String description;

	public CaseAttributeVO() {}
	
	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getShowOnDashboard() {
		return showOnDashboard;
	}

	public void setShowOnDashboard(Boolean showOnDashboard) {
		this.showOnDashboard = showOnDashboard;
	}

	public Boolean getShowOnIncident() {
		return showOnIncident;
	}

	public void setShowOnIncident(Boolean showOnIcident) {
		this.showOnIncident = showOnIcident;
	}

	public Boolean getUseAsFilter() {
		return useAsFilter;
	}

	public void setUseAsFilter(Boolean useAsFilter) {
		this.useAsFilter = useAsFilter;
	}

	public List<CaseValueVO> getValueRange() {
		return valueRange;
	}

	public void setValueRange(List<CaseValueVO> valueRange) {
		this.valueRange = valueRange;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
