/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;


import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;

public class ResolveImpexWikiVO extends VO
{
    private static final long serialVersionUID = 8037233675524458995L;
    
    private String UType;
    private String UValue;
    private Boolean UScan;
    private String UOptions;
    private String UDescription;

    private ResolveImpexModuleVO resolveImpexModule; // UModule;
	
    
    public ResolveImpexWikiVO()
    {
    }

    
    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    public String getUValue()
    {
        return this.UValue;
    }

    public void setUValue(String UValue)
    {
        this.UValue = UValue;
    }

    public ResolveImpexModuleVO getResolveImpexModule()
    {
        return resolveImpexModule;
    }

    public void setResolveImpexModule(ResolveImpexModuleVO resolveImpexModule)
    {
        this.resolveImpexModule = resolveImpexModule;
        
        if (resolveImpexModule != null)
        {
            Collection<ResolveImpexWikiVO> coll = resolveImpexModule.getResolveImpexWikis();
            if (coll == null)
            {
				coll = new HashSet<ResolveImpexWikiVO>();
	            coll.add(this);
	            
                resolveImpexModule.setResolveImpexWikis(coll);
            }
        }
    }

    public Boolean getUScan()
    {
        return UScan;
    }

    public void setUScan(Boolean scan)
    {
        UScan = scan;
    }


    public String getUOptions()
    {
        return UOptions;
    }

    public void setUOptions(String uOptions)
    {
        UOptions = uOptions;
    }


    public String getUDescription()
    {
        return UDescription;
    }


    public void setUDescription(String uDescription)
    {
        UDescription = uDescription;
    }

}
