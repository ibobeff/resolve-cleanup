package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class JMSGatewayPropertiesAttrVO extends VO
{
    private static final long serialVersionUID = 1L;
    
    private String UName;
    private String UValue;
    private String UJMSPropertiesId;
    
    public String getUName()
    {
        return UName;
    }
    public void setUName(String uName)
    {
        UName = uName;
    }
    public String getUValue()
    {
        return UValue;
    }
    public void setUValue(String uValue)
    {
        UValue = uValue;
    }
    public String getUJMSPropertiesId()
    {
        return UJMSPropertiesId;
    }
    public void setUJMSPropertiesId(String uPushPropertiesId)
    {
        UJMSPropertiesId = uPushPropertiesId;
    }

} // class JMSGatewayPropertiesAttrVO
