/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.menu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MenuDTO implements Serializable
{
    private static final long serialVersionUID = -1116585244920671133L;
    
    private List<MenuSetModel> sets;
    private List<MenuItemDTO> sections;

    public MenuDTO()
    {
    }

    public List<MenuSetModel> getSets()
    {
        return sets;
    }

    public void setSets(List<MenuSetModel> sets)
    {
        this.sets = sets;
    }

    public List<MenuItemDTO> getSections()
    {
        return sections;
    }

    public void setSections(List<MenuItemDTO> sections)
    {
        this.sections = sections;
    }

    public void addToSections(MenuItemDTO section)
    {
        if (this.sections == null) this.sections = new ArrayList<MenuItemDTO>();
        this.sections.add(section);

    }
}