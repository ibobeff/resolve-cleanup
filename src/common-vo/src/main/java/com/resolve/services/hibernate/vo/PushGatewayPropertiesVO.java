package com.resolve.services.hibernate.vo;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;

public class PushGatewayPropertiesVO extends GatewayPropertiesVO
{
    private static final long serialVersionUID = 1L;
    
    protected Integer UHttpPort;
    protected String UHttpUser;
    protected String UHttpPass;
    
    protected Boolean UHttpSsl;
    protected String USslCertificate ;
    protected String USslPassword;
    
    protected Integer UExecuteTimeout = 120;
    protected Integer UScriptTimeout = 120;
    protected Integer UWaitTimeout = 120;
    
    protected Collection<PushGatewayPropertiesAttrVO> attrs;
    
    @MappingAnnotation(columnName="HTTP_PORT")
    public Integer getUHttpPort()
    {
        return UHttpPort;
    }

    public void setUHttpPort(Integer uHttpPort)
    {
        UHttpPort = uHttpPort;
    }

    @MappingAnnotation(columnName="HTTP_USER")
    public String getUHttpUser()
    {
        return UHttpUser;
    }

    public void setUHttpUser(String uHttpUser)
    {
        UHttpUser = uHttpUser;
    }

    @MappingAnnotation(columnName="HTTP_PASS")
    public String getUHttpPass()
    {
        return UHttpPass;
    }

    public void setUHttpPass(String uHttpPass)
    {
        UHttpPass = uHttpPass;
    }

    @MappingAnnotation(columnName="HTTP_SSL")
    public Boolean getUHttpSsl()
    {
        return UHttpSsl;
    }

    public void setUHttpSsl(Boolean uHttpSsl)
    {
        UHttpSsl = uHttpSsl;
    }

    @MappingAnnotation(columnName="SSL_CERTIFICATE")
    public String getUSslCertificate()
    {
        return USslCertificate;
    }

    public void setUSslCertificate(String uSslCertificate)
    {
        USslCertificate = uSslCertificate;
    }

    @MappingAnnotation(columnName="SSL_PASSSWORD")
    public String getUSslPassword()
    {
        return USslPassword;
    }

    public void setUSslPassword(String uSslPassword)
    {
        USslPassword = uSslPassword;
    }

    @MappingAnnotation(columnName="EXECUTE_TIMEOUT")
    public Integer getUExecuteTimeout()
    {
        return UExecuteTimeout;
    }

    public void setUExecuteTimeout(Integer uExecuteTimeout)
    {
        UExecuteTimeout = uExecuteTimeout;
    }

    @MappingAnnotation(columnName="SCRIPT_TIMEOUT")
    public Integer getUScriptTimeout()
    {
        return UScriptTimeout;
    }

    public void setUScriptTimeout(Integer uScriptTimeout)
    {
        UScriptTimeout = uScriptTimeout;
    }

    @MappingAnnotation(columnName="WAIT_TIMEOUT")
    public Integer getUWaitTimeout()
    {
        return UWaitTimeout;
    }

    public void setUWaitTimeout(Integer uWaitTimeout)
    {
        UWaitTimeout = uWaitTimeout;
    }

    public Collection<PushGatewayPropertiesAttrVO> getAttrs()
    {
        return attrs;
    }

    public void setAttrs(Collection<PushGatewayPropertiesAttrVO> attrs)
    {
        this.attrs = attrs;
    }
    
    public Map<String, String> toMap() {
        
        Collection<PushGatewayPropertiesAttrVO> attrs = getAttrs();
        
        Map<String, String> map = new HashMap<String, String>();

        map.put("id", this.getSys_id());

        Method[] methods = this.getClass().getMethods();
        
        for(Method method:methods) {
            String name = method.getName();
            if(method.getAnnotation(MappingAnnotation.class) != null) {
                try {
                    String attr = name.substring(3).toLowerCase();
                    Object value = method.invoke(this, (Object[])null);

                    if(value != null)
                        map.put(attr, value.toString());
                } catch(Exception e) {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
        
        for(Iterator<PushGatewayPropertiesAttrVO> it=attrs.iterator(); it.hasNext();) {
            PushGatewayPropertiesAttrVO attr = it.next();
            if(attr != null)
                map.put(attr.getUName(), attr.getUValue());
        }

        return map;
    }
    
    public static PushGatewayPropertiesVO fromMap(Map<String, Object> map) {
        
        PushGatewayPropertiesVO vo = new PushGatewayPropertiesVO();
        
        Method[] methods = vo.getClass().getMethods();
        
        for(Method method:methods) {
            String name = method.getName();
            
            if(StringUtils.isNotBlank(name) && name.length() > 4) {
                if(!name.startsWith("set"))
                    continue;
                
                try {
                    String attr = name.substring(4);
                    Object value = map.get(attr.toUpperCase());

                    if(value != null) {
                        Object[] args = null;
                        
                        if(value instanceof String) {
                            if(value.equals("UNDEFINED"))
                                value = "";

                            else {
                                String methodName = "getU" + attr;
                                for(Method getMethod:methods) {
                                    String getMethodName = getMethod.getName();
                                    if(!getMethodName.startsWith("get"))
                                        continue;
                                    try {
                                        if(getMethodName.contains(methodName)) {
                                            Object obj = getMethod.invoke(vo, (Object[])null);
                                            
                                            if(obj instanceof String) {
                                                args = new String[1];
                                                args[0] = (String)value;
                                            }
                                            
                                            else if(obj instanceof Integer) {
                                                args = new Integer[1];
                                                args[0] = Integer.parseInt((String)value);
                                            }
                                            
                                            else if(obj instanceof Boolean) {
                                                args = new Boolean[1];
                                                args[0] = Boolean.parseBoolean((String)value);
                                            }
                                            
                                            else if(obj instanceof Long) {
                                                args = new Long[1];
                                                args[0] = Long.parseLong((String)value);
                                            }
                                            
                                            break;
                                        }
                                    } catch(Exception e) {
                                        Log.log.error(e.getMessage(), e);
                                    }
                                }
                            }
                        }
                        
                        else if(value instanceof Integer) {
                            args = new Integer[1];
                            args[0] = (Integer)value;
                        }
                        
                        else if(value instanceof Boolean) {
                            args = new Boolean[1];
                            args[0] = (Boolean)value;
                        }
                        
                        else if(value instanceof Long) {
                            args = new Long[1];
                            args[0] = (Long)value;
                        }

                        method.invoke(vo, (Object[])args);
                    }
                    
                    map.remove(attr.toUpperCase());
                } catch(Exception e) {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
        
        if(map.size() != 0) {
            Collection<PushGatewayPropertiesAttrVO> attrs = new ArrayList<PushGatewayPropertiesAttrVO>();
            
            for(Iterator<String> it=map.keySet().iterator(); it.hasNext();) {
                PushGatewayPropertiesAttrVO attr = new PushGatewayPropertiesAttrVO();
                String key = it.next();
                Object value = map.get(key);
                
                attr.setUName(key);
                if(value != null)
                    attr.setUValue(value.toString());
                
                attrs.add(attr);
            }
            
            vo.setAttrs(attrs);
        }
        
        return vo;
    }
    
} // class PushGatewayPropertiesVO