/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Date;

import com.resolve.services.interfaces.VO;

public class MetricThresholdVO extends VO
{
    private static final long serialVersionUID = 8867830240737111078L;

    private String UName;
    private String UGroup;
    private String UGuid;
    private String UHigh=VO.STRING_DEFAULT;
    private String UEqual=VO.STRING_DEFAULT;
    private String ULow=VO.STRING_DEFAULT;
    private String UAlertStatus;
    private String UType;
    private String UIpaddress;
    private String UComponenttype;
    private Boolean isOutdated;

//    private Boolean UDefault;
    private String UActive;
    private String UAction;
    private String USource;
    private String URuleModule;
    private String URuleName;
    private Integer UVersion;

    private String UConditionValue=VO.STRING_DEFAULT;
    private String UConditionOperator=VO.STRING_DEFAULT;
    
    private String UFromMCP;
    
    public MetricThresholdVO()
    {
        
    }

//    public MetricThresholdVO(String UName, String UGroup, String UGuid, String UHigh, String ULow, String UAlertStatus, String UType, String UIpaddress, String UComponentType, Date sysCreatedOn, String sysCreatedBy, Boolean uDefault, Boolean uActive, String uAction, String uSource, String uRuleModule, String uRuleName)
    public MetricThresholdVO(String UName, String UGroup, String UGuid, String UHigh, String ULow, String UAlertStatus, String UType, String UIpaddress, String UComponentType, Date sysCreatedOn, String sysCreatedBy, String uActive, String uAction, String uSource, String uRuleModule, String uRuleName, String uConditionValue, String uConditionOperator)
    {
        setUName(UName);
        setUGroup(UGroup);
        setUGuid(UGuid);
        setUType(UType);
        setUHigh(UHigh);
        setULow(ULow);
        setUAlertStatus(UAlertStatus);
        setUIpaddress(UIpaddress);
        setUComponenttype(UComponentType);
        setSysCreatedOn(sysCreatedOn);
        setSysCreatedBy(sysCreatedBy);
//        setUDefault(uDefault);
        setUActive(uActive);
        setUAction(uAction);
        setUSource(uSource);
        setURuleModule(uRuleModule);
        setURuleName(uRuleName);
        
        setUConditionValue(uConditionValue);
        setUConditionOperator(uConditionOperator);
    }

    public MetricThresholdVO(String UName, String UGroup, String UHigh, String ULow, String UAlertStatus, String UType, String UIpaddress, String UComponentType, Date sysCreatedOn, String sysCreatedBy, Boolean uDefault, String uActive, String uAction, String uSource, String uRuleModule, String uRuleName, String uConditionValue, String uConditionOperator)
    {
        setUName(UName);
        setUGroup(UGroup);
        setUHigh(UHigh);
        setULow(ULow);
        setUAlertStatus(UAlertStatus);
        setUComponenttype(UComponentType);
        setSysCreatedOn(sysCreatedOn);
        setSysCreatedBy(sysCreatedBy);
//        setUDefault(uDefault);
        setUActive(uActive);
        setUAction(uAction);
        setUSource(uSource);
        setURuleModule(uRuleModule);
        setURuleName(uRuleName);
        
        setUConditionValue(uConditionValue);
        setUConditionOperator(uConditionOperator);
    }

    public String getUConditionValue()
    {
        return UConditionValue;
    } // getUConditionValue

    public void setUConditionValue(String u)
    {
        UConditionValue = u;
    } //setUConditionvalue
    
    public String getUFromMCP()
    {
        return UFromMCP;
    } // getUFromMCP

    public void setUFromMCP(String u)
    {
        UFromMCP = u;
    } //setUFromMCP
    
    public String getUConditionOperator()
    {
        return UConditionOperator;
    } // getUConditionOperator

    public void setUConditionOperator(String u)
    {
        UConditionOperator = u;
    } //setUConditionOperator
    
    
    
    public String getUActive()
    {
        return UActive;
    } // getUActive

    public void setUActive(String u)
    {
        UActive = u;
    } //setUActive
    
    public String getUAction()
    {
        return UAction;
    } // getUAction

    public void setUAction(String u)
    {
        UAction = u;
    } //setUAction

    public String getUSource()
    {
        return USource;
    } // getUName

    public void setUSource(String u)
    {
        USource = u;
    } //setUSource

    public String getURuleModule()
    {
        return URuleModule;
    } // getUName

    public void setURuleModule(String u)
    {
        URuleModule = u;
    } //setURuleModule

    public String getURuleName()
    {
        return URuleName;
    } // getUName

    public void setURuleName(String u)
    {
        URuleName = u;
    } //setURuleName

    public Integer getUVersion()
    {
        return UVersion;
    } // getUVersion

    public void setUVersion(Integer u)
    {
        UVersion = u;
    } //setUVersion

    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    public String getUGroup()
    {
        return this.UGroup;
    }

    public void setUGroup(String UGroup)
    {
        this.UGroup = UGroup;
    }
    
    public String getUGuid()
    {
        return this.UGuid;
    }

    public void setUGuid(String UGuid)
    {
        this.UGuid = UGuid;
    }
    
    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    public String getUHigh()
    {
        return this.UHigh;
    }

    public void setUHigh(String UHigh)
    {
        this.UHigh = UHigh;
    }
    
    public String getULow()
    {
        return this.ULow;
    }

    public void setULow(String ULow)
    {
        this.ULow = ULow;
    }

    public String getUEqual()
    {
        return this.UEqual;
    } 

    public void setUEqual(String UEqual)
    {
        this.UEqual = UEqual;
    } 
    
    /**
     * @return the uAlertStatus
     */
    public String getUAlertStatus()
    {
        return this.UAlertStatus;
    }

    /**
     * @param uAlertStatus the uAlertStatus to set
     */
    public void setUAlertStatus(String uAlertStatus)
    {
        this.UAlertStatus = uAlertStatus;
    }

    public String getUIpaddress()
    {
        return UIpaddress;
    }

    public void setUIpaddress(String uIpaddress)
    {
        UIpaddress = uIpaddress;
    }

    public String getUComponenttype()
    {
        return UComponenttype;
    }

    public void setUComponenttype(String uComponentType)
    {
        UComponenttype = uComponentType;
    }
    
    public Boolean getIsOutdated()
	{
		return isOutdated;
	}
	public void setIsOutdated(Boolean isOutdated)
	{
		this.isOutdated = isOutdated;
	}

} // MetricThresholdVO
