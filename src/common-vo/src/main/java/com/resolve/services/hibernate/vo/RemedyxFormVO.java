/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;

public class RemedyxFormVO extends GatewayVO
{
    private static final long serialVersionUID = -126913425200971012L;

    private String UName;
    private String UFieldList;

    public RemedyxFormVO()
    {
    }

    @MappingAnnotation(columnName = "NAME")
    public String getUName()
    {
        return UName;
    } // getUName

    public void setUName(String uName)
    {
        this.UName = uName;
    } // setUName

    @MappingAnnotation(columnName = "FIELD_LIST")
    public String getUFieldList()
    {
        return UFieldList;
    } // getUFieldList

    public void setUFieldList(String uFieldList)
    {
        UFieldList = uFieldList;
    } // setUFieldList

    @Override
    public String getUniqueId()
    {
        return this.UName;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((UFieldList == null) ? 0 : UFieldList.hashCode());
        result = prime * result + ((UName == null) ? 0 : UName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        RemedyxFormVO other = (RemedyxFormVO) obj;
        if (UFieldList == null)
        {
            if (other.UFieldList != null) return false;
        }
        else if (!UFieldList.equals(other.UFieldList)) return false;
        if (UName == null)
        {
            if (other.UName != null) return false;
        }
        else if (!UName.equals(other.UName)) return false;
        return true;
    }
    
}
