/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.interfaces;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class DTO implements Serializable
{
    private static final long serialVersionUID = -7346805896340746437L;

    private String id;
    private String sys_id;
    private Date sys_created_on;
    private String sys_created_by;
    private Date sys_updated_on;
    private String sys_updated_by;
    private Integer sys_mod_count;
    
    private String sys_org;

    public DTO()
    {
    }

    public DTO(Map<String, Object> data)
    {
        updateFromMap(data);
    }
    
    public Map<String, Object> convertDTOToMap()
    {
        Map<String, Object> data = new HashMap<String, Object>();
        
        try
        {
            BeanInfo info = Introspector.getBeanInfo(this.getClass());  
            PropertyDescriptor[] props = info.getPropertyDescriptors();  
            for (PropertyDescriptor pd : props) 
            {  
                //get the setter method for this property
                Method m = pd.getReadMethod();
                if(m != null)
                {
                    String name = pd.getName();
                    if(name.equalsIgnoreCase("class") || name.equalsIgnoreCase("$type$"))
                    {
                        continue;
                    }

                    try
                    {
                       Object value = m.invoke(this);
                       data.put(name, value);
                    }
                    catch (Exception e)
                    {
                        // ignore this
                    }
                }
            }
        }
        catch(Exception e)
        {
            Log.log.error("error in conversion of object to map", e);
        }
        
        
        
        return data;
    }

    private void updateFromMap(Map<String, Object> data)
    {
        if (data != null)
        {
            try
            {
                BeanInfo info = Introspector.getBeanInfo(this.getClass());
                PropertyDescriptor[] props = info.getPropertyDescriptors();
                for (PropertyDescriptor pd : props)
                {
                    String name = pd.getName();

                    //get the setter method for this property
                    Method m = pd.getWriteMethod();
                    if (m != null && data.containsKey(name))
                    {
                        //get the default value based on the type
                        Object value = data.get(name);

                        //invoke the setter method
                        try
                        {
                            m.invoke(this, value);
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }
            catch (Throwable t)
            {
                //ignore it
                Log.log.error("error in populating data:", t);
            }
        }
    }


    
    //setters/getters
    
    public String getSys_id()
    {
        return sys_id;
    }

    public void setSys_id(String sys_id)
    {
        this.sys_id = sys_id;
        
        if(this.id == null) 
            this.id = sys_id;
    }

    public Date getSys_created_on()
    {
        return sys_created_on;
    }

    public void setSys_created_on(Date sys_created_on)
    {
        this.sys_created_on = sys_created_on;
    }

    public String getSys_created_by()
    {
        return sys_created_by;
    }

    public void setSys_created_by(String sys_created_by)
    {
        this.sys_created_by = sys_created_by;
    }

    public Date getSys_updated_on()
    {
        return sys_updated_on;
    }

    public void setSys_updated_on(Date sys_updated_on)
    {
        this.sys_updated_on = sys_updated_on;
    }

    public String getSys_updated_by()
    {
        return sys_updated_by;
    }

    public void setSys_updated_by(String sys_updated_by)
    {
        this.sys_updated_by = sys_updated_by;
    }

    public Integer getSys_mod_count()
    {
        return sys_mod_count;
    }

    public void setSys_mod_count(Integer sys_mod_count)
    {
        this.sys_mod_count = sys_mod_count;
    }

    public String getSys_org()
    {
        return sys_org;
    }

    public void setSys_org(String sys_org)
    {
        this.sys_org = sys_org;
    }
    
    public String getSysCreatedBy() {
        return this.sys_created_by;
    }
    public Date getSysCreatedOn() {
        return this.sys_created_on;
    }
    public String getSysUpdatedBy() {
        return this.sys_updated_by;
    }
    public Date getSysUpdatedOn() {
        return this.sys_updated_on;
    }
    public String getSysOrg() {
        return this.sys_org;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        if(StringUtils.isEmpty(this.sys_id))
        {
            this.sys_id = id;
        }
        this.id = id;
    }
    
    

}
