/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
public class RBTaskHeaderVO extends VO
{
    private static final long serialVersionUID = 1026045799079949966L;
    
	private String name;
    private String value;

    private RBTaskVO task;
    
    public RBTaskHeaderVO()
    {
        super();
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public RBTaskVO getTask()
    {
        return task;
    }

    public void setTask(RBTaskVO task)
    {
        this.task = task;
    }
}
