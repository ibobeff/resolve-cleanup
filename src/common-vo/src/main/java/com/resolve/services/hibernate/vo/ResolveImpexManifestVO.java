/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveImpexManifestVO extends VO
{
    private static final long serialVersionUID = -2067537805992227619L;
    
    private String UName;
    private String UModule;
    private String UFullName;
    private String UDisplayType;
    private String UCompSysId;
    private String UTableName;
    private String UFileName;
    private String UStatus;
    private String UOperation;
    private String UOperationType;
    private Boolean UInclude;
    private String UModuleName;
    
    public ResolveImpexManifestVO()
    {
    }

    public String getUName()
    {
        return UName;
    }

    public void setUName(String uName)
    {
        UName = uName;
    }

    public String getUModule()
    {
        return UModule;
    }

    public void setUModule(String uModule)
    {
        UModule = uModule;
    }

    public String getUFullName()
    {
        return UFullName;
    }

    public void setUFullName(String uFullName)
    {
        UFullName = uFullName;
    }

    public String getUDisplayType()
    {
        return UDisplayType;
    }

    public void setUDisplayType(String uDisplayType)
    {
        UDisplayType = uDisplayType;
    }

    public String getUCompSysId()
    {
        return UCompSysId;
    }

    public void setUCompSysId(String uCompSysId)
    {
        UCompSysId = uCompSysId;
    }

    public String getUTableName()
    {
        return UTableName;
    }

    public void setUTableName(String uTableName)
    {
        UTableName = uTableName;
    }

    public String getUFileName()
    {
        return UFileName;
    }

    public void setUFileName(String uFileName)
    {
        UFileName = uFileName;
    }

    public String getUStatus()
    {
        return UStatus;
    }
    public void setUStatus(String uStatus)
    {
        this.UStatus = uStatus;
    }

    public String getUOperation()
    {
        return UOperation;
    }

    public void setUOperation(String uOperation)
    {
        UOperation = uOperation;
    }

    public String getUOperationType()
    {
        return UOperationType;
    }

    public void setUOperationType(String uOperationType)
    {
        UOperationType = uOperationType;
    }

    public Boolean getUInclude()
    {
        return UInclude;
    }

    public void setUInclude(Boolean uInclude)
    {
        UInclude = uInclude;
    }

    public String getUModuleName()
    {
        return UModuleName;
    }

    public void setUModuleName(String uModuleName)
    {
        UModuleName = uModuleName;
    }
}
