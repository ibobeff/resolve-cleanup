/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;


public class ResolveActionTaskResolveTagRelVO  extends VO
{
    private static final long serialVersionUID = -4418889158683521580L;

    private ResolveActionTaskVO task;
    private ResolveTagVO tag;
    
    public ResolveActionTaskResolveTagRelVO()
    {
    }
    
    public ResolveTagVO getTag()
    {
        return tag;
    }

    public void setTag(ResolveTagVO tag)
    {
        this.tag = tag;
    }
    
    
    public ResolveActionTaskVO getTask()
    {
        return task;
    }

    public void setTask(ResolveActionTaskVO task)
    {
        this.task = task;
    }
    

}
