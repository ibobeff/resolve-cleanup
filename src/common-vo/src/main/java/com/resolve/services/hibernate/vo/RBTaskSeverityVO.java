/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
public class RBTaskSeverityVO extends VO
{
    private static final long serialVersionUID = -4068139688840091336L;
	private String severityId;
    private String severity; // GOOD, BAD
    private String criteria; // ANY, ALL
    private String variableSource;
    private String variable;
    private String comparison; // EQUALS, >, >= etc.
    private String source;
    private String sourceName;
    private Double order;
    
    private RBTaskVO task;

    public RBTaskSeverityVO()
    {
        super();
    }

    public String getSeverityId()
    {
        return severityId;
    }

    public void setSeverityId(String severityId)
    {
        this.severityId = severityId;
    }
    
    public String getSeverity()
    {
        return severity;
    }

    public void setSeverity(String severity)
    {
        this.severity = severity;
    }

    public String getCriteria()
    {
        return criteria;
    }

    public void setCriteria(String criteria)
    {
        this.criteria = criteria;
    }

    public String getVariableSource()
    {
        return variableSource;
    }

    public void setVariableSource(String variableSource)
    {
        this.variableSource = variableSource;
    }

    public String getVariable()
    {
        return variable;
    }

    public void setVariable(String variable)
    {
        this.variable = variable;
    }

    public String getComparison()
    {
        return comparison;
    }

    public void setComparison(String comparison)
    {
        this.comparison = comparison;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public String getSourceName()
    {
        return sourceName;
    }

    public void setSourceName(String sourceName)
    {
        this.sourceName = sourceName;
    }

    public Double getOrder()
    {
        return order;
    }

    public void setOrder(Double order)
    {
        this.order = order;
    }

    public RBTaskVO getTask()
    {
        return task;
    }

    public void setTask(RBTaskVO task)
    {
        this.task = task;
    }
}
