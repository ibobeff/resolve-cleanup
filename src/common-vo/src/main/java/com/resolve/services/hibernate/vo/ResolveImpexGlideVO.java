/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;


import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;

public class ResolveImpexGlideVO extends VO
{
    private static final long serialVersionUID = -8525153482541603474L;
    
    private String UType;
    private String UTablename;
    private String UQueryNames;
    private String UQueryValues;
    private String UActiontaskName;
    private String UActiontaskModule;
    private String UTriggerName;
    private String UTriggerNamespace;
    private String UDescription;
    private String UPropertyName;
    private String UPropertyModule;
    private String UValue;

    private String UModule;
    private String UName;
    private Boolean UScan;
    private String UOptions;

    private ResolveImpexModuleVO resolveImpexModule;

    // object referenced by

    public ResolveImpexGlideVO()
    {
    }

    public ResolveImpexModuleVO getResolveImpexModule()
    {
        return resolveImpexModule;
    }

    public void setResolveImpexModule(ResolveImpexModuleVO resolveImpexModule)
    {
        this.resolveImpexModule = resolveImpexModule;
        
        if (resolveImpexModule != null)
        {
            Collection<ResolveImpexGlideVO> coll = resolveImpexModule.getResolveImpexGlides();
            if (coll == null)
            {
				coll = new HashSet<ResolveImpexGlideVO>();
	            coll.add(this);
	            
                resolveImpexModule.setResolveImpexGlides(coll);
            }
        }
    }

    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    public String getUTablename()
    {
        return this.UTablename;
    }

    public void setUTablename(String UTablename)
    {
        this.UTablename = UTablename;
    }

    public String getUQueryNames()
    {
        return this.UQueryNames;
    }

    public void setUQueryNames(String UQueryNames)
    {
        this.UQueryNames = UQueryNames;
    }

    public String getUQueryValues()
    {
        return this.UQueryValues;
    }

    public void setUQueryValues(String UQueryValues)
    {
        this.UQueryValues = UQueryValues;
    }

    public String getUActiontaskName()
    {
        return this.UActiontaskName;
    }

    public void setUActiontaskName(String UActiontaskName)
    {
        this.UActiontaskName = UActiontaskName;
    }

    public String getUActiontaskModule()
    {
        return this.UActiontaskModule;
    }

    public void setUActiontaskModule(String UActiontaskModule)
    {
        this.UActiontaskModule = UActiontaskModule;
    }

    public String getUTriggerName()
    {
        return this.UTriggerName;
    }

    public void setUTriggerName(String UTriggerName)
    {
        this.UTriggerName = UTriggerName;
    }

    public String getUTriggerNamespace()
    {
        return this.UTriggerNamespace;
    }

    public void setUTriggerNamespace(String UTriggerNamespace)
    {
        this.UTriggerNamespace = UTriggerNamespace;
    }

    public String getUDescription()
    {
        return this.UDescription;
    }

    public void setUDescription(String UDescription)
    {
        this.UDescription = UDescription;
    }

    public String getUPropertyName()
    {
        return this.UPropertyName;
    }

    public void setUPropertyName(String UPropertyName)
    {
        this.UPropertyName = UPropertyName;
    }

    public String getUPropertyModule()
    {
        return this.UPropertyModule;
    }

    public void setUPropertyModule(String UPropertyModule)
    {
        this.UPropertyModule = UPropertyModule;
    }

    public String getUValue()
    {
        return this.UValue;
    }

    public void setUValue(String UValue)
    {
        this.UValue = UValue;
    }

    public String getUModule()
    {
        return UModule;
    }

    public void setUModule(String module)
    {
        UModule = module;
    }

    public String getUName()
    {
        return UName;
    }

    public void setUName(String name)
    {
        UName = name;
    }

    public Boolean getUScan()
    {
        return UScan;
    }

    public void setUScan(Boolean scan)
    {
        UScan = scan;
    }

    public String getUOptions()
    {
        return UOptions;
    }

    public void setUOptions(String uOptions)
    {
        UOptions = uOptions;
    }

}
