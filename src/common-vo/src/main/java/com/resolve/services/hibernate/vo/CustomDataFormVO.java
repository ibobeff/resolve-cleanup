package com.resolve.services.hibernate.vo;

import java.util.HashMap;
import java.util.Map;

import com.resolve.services.interfaces.VO;

public class CustomDataFormVO extends VO {
	private static final long serialVersionUID = 31168893009459523L;

	private String wikiId;
	private Map<String, String> formRecord = new HashMap<>();
	String incidentId;

	public CustomDataFormVO() {}

	public String getWikiId() {
		return wikiId;
	}

	public void setWikiId(String wikiId) {
		this.wikiId = wikiId;
	}

	public Map<String, String> getFormRecord() {
		return formRecord;
	}

	public void setFormRecord(Map<String, String> formRecord) {
		this.formRecord = formRecord;
	}

	public String getIncidentId() {
		return incidentId;
	}

	public void setIncidentId(String incidentId) {
		this.incidentId = incidentId;
	}
}
