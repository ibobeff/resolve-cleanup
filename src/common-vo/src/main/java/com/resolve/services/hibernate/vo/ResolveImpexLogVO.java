/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;


public class ResolveImpexLogVO extends VO
{
    private static final long serialVersionUID = 4195974738013201581L;
    
    private String UType;
    private String ULogHistory;
    private String ULogSummary;

    private ResolveImpexModuleVO resolveImpexModule;
    
    public ResolveImpexLogVO()
    {
    }


    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    public String getULogHistory()
    {
        return this.ULogHistory;
    }

    public void setULogHistory(String ULogHistory)
    {
        this.ULogHistory = ULogHistory;
    }
    
    public String getULogSummary()
    {
        return ULogSummary;
    }

    public void setULogSummary(String uLogSummary)
    {
        ULogSummary = uLogSummary;
    }
    
    public ResolveImpexModuleVO getResolveImpexModule()
    {
        return resolveImpexModule;
    }

    public void setResolveImpexModule(ResolveImpexModuleVO resolveImpexModule)
    {
        this.resolveImpexModule = resolveImpexModule;

    }    

}
