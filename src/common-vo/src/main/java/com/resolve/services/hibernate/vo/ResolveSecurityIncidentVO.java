package com.resolve.services.hibernate.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.services.interfaces.VO;

public class ResolveSecurityIncidentVO extends VO
{
	private static final long serialVersionUID = 7747766804430148091L;
	
	private static final Integer RISK_SCORE_NOT_SET = Integer.valueOf(VO.NON_NEGATIVE_INTEGER_DEFAULT.intValue() - 1);
	
	private String title;
	private String externalReferenceId;
	private String investigationType;
	private String type;
	private String severity;
	private String description;
	private String sourceSystem;
	private String playbook;
	private String externalStatus;
	private String status;
	private String owner;
	private Integer playbookVersion;
	private String sir;
	private Date closedOn;
	private String closedBy;
	private Boolean primary;
	private String members;
	private Boolean sirStarted;
	private String problemId;
	private String dataCompromised;
	private Date dueBy;
	private String requestData;
	private String auditMessage;
	private Long sla;
	private Map<String, String> highlightFields = new HashMap<>();
	private Integer riskScore;
	private Date assignmentDate;
	private String classification;

	private Map<String, String> duplicates = new HashMap<>();
	private String masterSir;
	private String masterSysId;
	private String priority;
	private Map<String, String> caseAttributes = new HashMap<>();
	private List<CustomDataFormVO> customFormWiki = new ArrayList<>();
	
	public ResolveSecurityIncidentVO()
	{
	    setPrimary(VO.BOOLEAN_DEFAULT);
	    setSirStarted(VO.BOOLEAN_DEFAULT);
		setRiskScore(RISK_SCORE_NOT_SET);
	}
	
	public String getTitle()
	{
		return title;
	}
	public void setTitle(String title)
	{
		this.title = title;
	}
	
	public String getExternalReferenceId()
	{
		return externalReferenceId;
	}
	public void setExternalReferenceId(String referenceId)
	{
		this.externalReferenceId = referenceId;
	}
	
	public String getInvestigationType()
	{
		return investigationType;
	}
	public void setInvestigationType(String investigationType)
	{
		this.investigationType = investigationType;
	}
	
	public String getType()
	{
		return type;
	}
	public void setType(String type)
	{
		this.type = type;
	}
	
	public String getSeverity()
	{
		return severity;
	}
	public void setSeverity(String severity)
	{
		this.severity = severity;
	}
	
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	public String getSourceSystem()
	{
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem)
	{
		this.sourceSystem = sourceSystem;
	}
	
	public String getPlaybook()
	{
		return playbook;
	}
	public void setPlaybook(String playbook)
	{
		this.playbook = playbook;
	}
	
	public String getExternalStatus()
	{
		return externalStatus;
	}
	public void setExternalStatus(String externalStatus)
	{
		this.externalStatus = externalStatus;
	}
	
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	
	public String getOwner()
	{
		return owner;
	}
	public void setOwner(String owner)
	{
		this.owner = owner;
	}
	
	public Integer getPlaybookVersion()
	{
		return playbookVersion;
	}
	public void setPlaybookVersion(Integer playbookVersion)
	{
		this.playbookVersion = playbookVersion;
	}
	
	public String getSir()
	{
		return sir;
	}
	public void setSir(String incidentId)
	{
		this.sir = incidentId;
	}
	
	public Date getClosedOn()
	{
		return closedOn;
	}
	public void setClosedOn(Date closedOn)
	{
		this.closedOn = closedOn;
	}
	
	public String getClosedBy()
	{
		return closedBy;
	}
	public void setClosedBy(String closedBy)
	{
		this.closedBy = closedBy;
	}
	
	public Boolean getPrimary()
	{
		return primary;
	}
	public void setPrimary(Boolean primary)
	{
		this.primary = primary;
	}
	
	public String getMembers()
	{
		return members;
	}
	public void setMembers(String members)
	{
		this.members = members;
	}
	
	public Boolean getSirStarted()
	{
		return sirStarted;
	}
	public void setSirStarted(Boolean sirStarted)
	{
		this.sirStarted = sirStarted;
	}
	
	public String getProblemId()
	{
		return problemId;
	}
	public void setProblemId(String problemId)
	{
		this.problemId = problemId;
	}
	
	public String getDataCompromised()
	{
		return dataCompromised;
	}
	public void setDataCompromised(String dataCompromised)
	{
		this.dataCompromised = dataCompromised;
	}
	
	public Date getDueBy()
    {
        return dueBy;
    }
    public void setDueBy(Date dueBy)
    {
        this.dueBy = dueBy;
    }

	public String getRequestData() 
	{
		return requestData;
	}

	public void setRequestData(String requestData) 
	{
		this.requestData = requestData;
	}
	
	public Long getSla()
    {
        return sla;
    }

    public void setSla(Long sla)
    {
        this.sla = sla;
    }
    
	public String getAuditMessage()
    {
        return auditMessage;
    }
    public void setAuditMessage(String auditMessage)
    {
        this.auditMessage = auditMessage;
    }

	public Map<String, String> getHighlightFields() {
		return highlightFields;
	}

	public void setHighlightFields(Map<String, String> highlightFields) {
		this.highlightFields = highlightFields;
	}
	
	public Integer getRiskScore()
    {
        return riskScore;
    }
	
    public void setRiskScore(Integer riskScore)
    {
        this.riskScore = riskScore;
    }

    public void setAssignmentDate(Date assignmentDate)
    {
        this.assignmentDate = assignmentDate;
    }
    
    public Date getAssignmentDate()
    {
        return assignmentDate;

    }
    
    public String getClassification()
    {
        return classification;
    }
    
    public void setClassification(String classification)
    {
        this.classification = classification;
    }

	public Map<String, String> getDuplicates() {
		return duplicates;
	}

	public void setDuplicates(Map<String, String> d) {
		this.duplicates = d;
	}
	
	public String getMasterSir()
	{
		return masterSir;
	}
	public void setMasterSir(String s)
	{
		this.masterSir = s;
	}

	public String getMasterSysId()
	{
		return masterSysId;
	}
	public void setMasterSysId(String s)
	{
		this.masterSysId = s;
	}
	
    public String getPriority()
    {
        return priority;
    }
    public void setPriority(String priority)
    {
        this.priority = priority;
    }

	public Map<String, String> getCaseAttributes() {
		return caseAttributes;
	}

	public void setCaseAttributes(Map<String, String> caseAttributes) {
		this.caseAttributes = caseAttributes;
	}

	public List<CustomDataFormVO> getCustomFormWiki() {
		return customFormWiki;
	}

	public void setCustomFormWiki(List<CustomDataFormVO> customFormWiki) {
		this.customFormWiki = customFormWiki;
	}
}
