package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ArchiveSirArtifactVO extends VO
{
	private static final long serialVersionUID = -2485029137329949551L;
	
	private String name;
    private String value;
    private String activityId;
    private String description;
    private String incidentId;
    private String worksheetId;
    private String sir;
    private String source;         // name of a source which created this note.
    private String sourceValue;    // the value of the source.
    private String sourceAndValue;
    
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getValue()
    {
        return value;
    }
    public void setValue(String value)
    {
        this.value = value;
    }
    
    public String getActivityId()
    {
        return activityId;
    }
    public void setActivityId(String activityId)
    {
        this.activityId = activityId;
    }
    
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    public String getIncidentId()
    {
        return incidentId;
    }
    public void setIncidentId(String incidentId)
    {
        this.incidentId = incidentId;
    }
    
    public String getWorksheetId()
    {
        return worksheetId;
    }
    public void setWorksheetId(String worksheetId)
    {
        this.worksheetId = worksheetId;
    }
    
    public String getSir()
    {
        return sir;
    }
    public void setSir(String sir)
    {
        this.sir = sir;
    }
    
    public String getSource()
    {
        return source;
    }
    public void setSource(String source)
    {
        this.source = source;
    }
    
    public String getSourceValue()
    {
        return sourceValue;
    }
    public void setSourceValue(String sourceValue)
    {
        this.sourceValue = sourceValue;
    }
    
    public String getSourceAndValue()
    {
        return sourceAndValue;
    }
    public void setSourceAndValue(String sourceAndValue)
    {
        this.sourceAndValue = sourceAndValue;
    }
}