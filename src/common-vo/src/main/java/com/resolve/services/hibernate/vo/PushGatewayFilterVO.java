package com.resolve.services.hibernate.vo;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class PushGatewayFilterVO extends GatewayFilterVO
{
    private static final long serialVersionUID = 1L;

    private Boolean UDeployed;
    private Boolean UUpdated;
    private String UGatewayName;
    private Integer UPort;
    private String UBlocking;
    private String UUri;
    private Boolean USsl;
    
//    private Collection<PushGatewayFilterAttrVO> attrs;
    private Map<String, String> attrs;
    private Collection<PushGatewayFilterAttrVO> attrsCol;

	@MappingAnnotation(columnName="DEPLOYED")
    public Boolean getUDeployed()
    {
        return UDeployed;
    }

    public void setUDeployed(Boolean uDeployed)
    {
        UDeployed = uDeployed;
    }
    
    @MappingAnnotation(columnName="UPDATED")
    public Boolean getUUpdated()
    {
        return UUpdated;
    }

    public void setUUpdated(Boolean uUpdated)
    {
        UUpdated = uUpdated;
    }

    @MappingAnnotation(columnName="GATEWAY_NAME")
    public String getUGatewayName()
    {
        return UGatewayName;
    }

    public void setUGatewayName(String uGatewayName)
    {
        UGatewayName = uGatewayName;
    }
    
    @MappingAnnotation(columnName="PORT")
    public Integer getUPort() {
        return this.UPort;
    }

    public void setUPort(Integer uPort) {
        this.UPort = uPort;
    }

    @MappingAnnotation(columnName="BLOCKING")
    public String getUBlocking() {
        return this.UBlocking;
    }

    public void setUBlocking(String uBlocking) {
        this.UBlocking = uBlocking;
    }

    @MappingAnnotation(columnName="URI")
    public String getUUri() {
        return this.UUri;
    }

    public void setUUri(String uUri) {
        this.UUri = uUri;
    }

    @MappingAnnotation(columnName="SSL")
    public Boolean getUSsl() {
        return this.USsl;
    }

    public void setUSsl(Boolean uSsl) {
        this.USsl = uSsl;
    }

    
    
    public Collection<PushGatewayFilterAttrVO> getAttrsCol() {
    	if(this.attrsCol == null)
    		this.attrsCol = new ArrayList<PushGatewayFilterAttrVO>();
        return this.attrsCol;
	}

	public void setAttrsCol(Collection<PushGatewayFilterAttrVO> attrsCol) {
		this.attrsCol = attrsCol;
	}

	public Map<String, String> getAttrs() {
        return this.attrs;
    }

    public void setAttrs(Map<String, String> attrs) {
        this.attrs = attrs;
    }

    private String getAttrVal(Object value) {
        return value == null ? "" : value.toString();
    }

    public Map<String, String> toMap() {
    	
    	Map<String, String> attrs = getAttrs();
    	if(this.attrs == null)
    		attrs = new HashMap<String, String>();

    	attrs.put("id", this.getSys_id());

        Method[] methods = this.getClass().getMethods();
        
        for(Method method:methods) {
            String name = method.getName();
            if(method.getAnnotation(MappingAnnotation.class) != null) {
                try {
                    String attr = name.substring(3);
                    Object value = method.invoke(this, (Object[])null);
                    
                    if(attr.equalsIgnoreCase("uport")) {
                    	attrs.put("uport", getAttrVal(value));
                        continue;
                    }
                    
                    if(attr.equalsIgnoreCase("ublocking")) {
                    	attrs.put("ublocking", getAttrVal(value));
                        continue;
                    }
                    
                    if(attr.equalsIgnoreCase("uuri")) {
                    	attrs.put("uuri", getAttrVal(value));
                        continue;
                    }
                    
                    if(attr.equalsIgnoreCase("ussl")) {
                    	attrs.put("ussl", getAttrVal(value));
                        continue;
                    }
                    if(attr.equalsIgnoreCase("ueventeventid")) {
                        attrs.put("ueventEventId", getAttrVal(value));
                        continue;
                    }
                    
                    if(attr.equalsIgnoreCase("ugatewayname")) {
                    	attrs.put("ugatewayName", getAttrVal(value));
                        continue;
                    }
                    
                    if(attr.equalsIgnoreCase("uname")) {
                    	attrs.put("uname", getAttrVal(value));
                        continue;
                    }
                    if(attr.equalsIgnoreCase("urunbook")) {
                    	attrs.put("urunbook", getAttrVal(value));
                        continue;
                    }
                    if(attr.equalsIgnoreCase("uorder")) {
                    	attrs.put("uorder", getAttrVal(value));
                        continue;
                    }
                    if(attr.equalsIgnoreCase("uactive")) {
                    	attrs.put("uactive", getAttrVal(value));
                        continue;
                    }
                    if(attr.equals("uupdated"))
                        attr = "changed";
                    
                    if(value != null)
                    	attrs.put(attr, getAttrVal(value));
                } catch(Exception e) {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }

    	
		return attrs;
	}

    
//    public Map<String, String> toMap() {
//
//        Map<String, String> map = new HashMap<String, String>();
//
//        map.put("id", this.getSys_id());
//
//        Method[] methods = this.getClass().getMethods();
//        
//        for(Method method:methods) {
//            MappingAnnotation mappingAnnotation = method.getAnnotation(MappingAnnotation.class);
//            
//            if (mappingAnnotation != null) {
//                try {
//                    String attr = mappingAnnotation.columnName();
//                    Object value = method.invoke(this, (Object[])null);
//
//                    if(value != null)
//                        map.put(attr, value.toString());
//                } catch(Exception e) {
//                    Log.log.error(e.getMessage(), e);
//                }
//            }
//        }
//        
//        Collection<PushGatewayFilterAttrVO> attrs = getAttrsCol();
//        
//        if(attrs != null) {
//            for(Iterator<PushGatewayFilterAttrVO> it=attrs.iterator(); it.hasNext();) {
//                PushGatewayFilterAttrVO attr = it.next();
//                if(attr != null)
//                    map.put(attr.getUName(), attr.getUValue());
//            }
//        }
//        
//        return map;
//    }
    
    public static PushGatewayFilterVO fromMap(Map<String, String> map) {
        
        PushGatewayFilterVO vo = new PushGatewayFilterVO();
        
        Method[] methods = vo.getClass().getMethods();
        
        for(Method method:methods) {
            String name = method.getName();
            if(!name.startsWith("get"))
                continue;
            
            try {
                MappingAnnotation mappingAnnotation = method.getAnnotation(MappingAnnotation.class);
                if(mappingAnnotation == null)
                    continue;
                
                String columnName = mappingAnnotation.columnName();
                String value = map.get(columnName);
                
                if(value != null) {
                    Object[] args = null;
                    
                    if(value instanceof String) {
                        if(value.equals("UNDEFINED"))
                            value = "";
    
                        else {
                            try {
                                Object obj = method.invoke(vo, (Object[])null);
                                
                                if(obj instanceof String) {
                                    args = new String[1];
                                    args[0] = (String)value;
                                }
                                
                                else if(obj instanceof Integer) {
                                    args = new Integer[1];
                                    args[0] = Integer.parseInt((String)value);
                                }
                                
                                else if(obj instanceof Boolean) {
                                    args = new Boolean[1];
                                    args[0] = Boolean.parseBoolean((String)value);
                                }
                                
                                else if(obj instanceof Long) {
                                    args = new Long[1];
                                    args[0] = Long.parseLong((String)value);
                                }
                                
                                String attr = name.substring(4);
                                String methodName = "setU" + attr;
                                
                                for(Method setMethod:methods) {
                                    String setMethodName = setMethod.getName();
                                    if(!setMethodName.startsWith("set"))
                                        continue;
                                    
                                    if(setMethodName.contains(methodName)) {
                                        setMethod.invoke(vo, (Object[])args);
                                        break;
                                    }
                                }
                            } catch(Exception e) {
                                Log.log.error(e.getMessage(), e);
                            }
                        }
                    }
                }
                
                map.remove(columnName);
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
        
//        if(map.size() != 0) {
//            Collection<PushGatewayFilterAttrVO> attrs = new ArrayList<PushGatewayFilterAttrVO>();
//            
//            for(Iterator<String> it=map.keySet().iterator(); it.hasNext();) {
//                PushGatewayFilterAttrVO attr = new PushGatewayFilterAttrVO();
//                String key = it.next();
//                Object value = map.get(key);
//                
//                attr.setUName(key);
//                if(value != null)
//                    attr.setUValue(value.toString());
//                
//                attr.setPushGatewayFilter(vo);
//                
//                attrs.add(attr);
//            }
//            
//            vo.setAttrs(attrs);
//        }
        
        return vo;
    }
    
    @Override
    public boolean equals(Object obj) {

        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;

        PushGatewayFilterVO other = (PushGatewayFilterVO)obj;
        
        if(!super.equals(other))
            return false;
        
        if (UPort == null) {
            if (other.UPort != null) 
                return false;
        }

        else if (!UPort.equals(other.UPort)) 
            return false;
        
        if (UBlocking == null) {
            if (StringUtils.isNotBlank(other.UBlocking)) 
                return false;
        }

        else if (!UBlocking.trim().equals(other.UBlocking == null ? "" : other.UBlocking.trim())) 
            return false;
        
        if (UUri == null) {
            if (StringUtils.isNotBlank(other.UUri)) 
                return false;
        }

        else if (!UUri.trim().equals(other.UUri == null ? "" : other.UUri.trim())) 
            return false;
        
        if (USsl == null) {
            if (other.USsl != null) 
                return false;
        }

        else if (!USsl.equals(other.USsl)) 
            return false;
        
//        Collection<PushGatewayFilterAttrVO> otherAttrs = other.getAttrsCol();
        
//        for(Iterator<PushGatewayFilterAttrVO> it=attrsCol.iterator(); it.hasNext();) {
//            PushGatewayFilterAttrVO vo = it.next();
//            
//            String name = vo.getUName();
//            String value = vo.getUValue();
//            boolean found = false;
//            
//            for(Iterator<PushGatewayFilterAttrVO> otherIt=otherAttrs.iterator(); otherIt.hasNext();) {
//                PushGatewayFilterAttrVO otherVo = otherIt.next();
//                
//                String otherName = otherVo.getUName();
//                if(name == null && otherName != null)
//                    continue;
//                
//                else if(!name.equals(otherName)) 
//                    continue;
//                
//                String otherValue = otherVo.getUValue();
//                
//                if(value == null && otherValue != null) 
//                    continue;
//                
//                else if(!value.equals(otherValue)) 
//                    continue;
//                
//                else // both name and value are equals
//                    found = true;
//            }
//
//            if(found)
//                continue;
//            else
//                return false;
//        }
        Map<String, String> otherAttrs = other.getAttrs();
        
        for(Iterator<String> it=otherAttrs.keySet().iterator();it.hasNext();) {
            String key = it.next();
            String value = attrs.get(key);
            String otherValue = otherAttrs.get(key);
            
            if (value == null) {
                if (otherValue != null) 
                    return false;
            }

            else if (!value.equals(otherValue)) 
                return false;
        }
        return true;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UGatewayName == null) ? 0 : UGatewayName.hashCode());
        result = prime * result + ((UPort == null) ? 0 : UPort.hashCode());
        result = prime * result + ((UUri == null) ? 0 : UUri.hashCode());
        result = prime * result + ((USsl == null) ? 0 : USsl.hashCode());
        return result;
    }
} // class PushGatewayFilterVO