package com.resolve.services.hibernate.vo;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;

public class JMSGatewayPropertiesVO extends GatewayPropertiesVO
{
    private static final long serialVersionUID = 1L;
    
    protected Integer UJMSPort;
    protected String UJMSUser;
    protected String UJMSPass;
    
    protected Boolean UJMSSsl;
    protected String UJMSSslCertificate ;
    protected String UJMSSslPassword;
    
    protected Collection<JMSGatewayPropertiesAttrVO> attrs;
    
    @MappingAnnotation(columnName="JMS_PORT")
    public Integer getUJMSPort()
    {
        return UJMSPort;
    }

    public void setUJMSPort(Integer uJMSPort)
    {
        UJMSPort = uJMSPort;
    }

    @MappingAnnotation(columnName="JMS_USER")
    public String getUJMSUser()
    {
        return UJMSUser;
    }

    public void setUJMSUser(String uJMSUser)
    {
        UJMSUser = uJMSUser;
    }

    @MappingAnnotation(columnName="JMS_PASS")
    public String getUJMSPass()
    {
        return UJMSPass;
    }

    public void setUJMSPass(String uJMSPass)
    {
        UJMSPass = uJMSPass;
    }

    @MappingAnnotation(columnName="JMS_SSL")
    public Boolean getUJMSSsl()
    {
        return UJMSSsl;
    }

    public void setUJMSSsl(Boolean uJMSSsl)
    {
        UJMSSsl = uJMSSsl;
    }

    @MappingAnnotation(columnName="JMS_SSL_CERTIFICATE")
    public String getUJMSSslCertificate()
    {
        return UJMSSslCertificate;
    }

    public void setUJMSSslCertificate(String uJMSSslCertificate)
    {
        UJMSSslCertificate = uJMSSslCertificate;
    }

    @MappingAnnotation(columnName="JMS_SSL_PASSSWORD")
    public String getUJMSSslPassword()
    {
        return UJMSSslPassword;
    }

    public void setUJMSSslPassword(String uJMSSslPassword)
    {
        UJMSSslPassword = uJMSSslPassword;
    }

    public Collection<JMSGatewayPropertiesAttrVO> getAttrs()
    {
        return attrs;
    }

    public void setAttrs(Collection<JMSGatewayPropertiesAttrVO> attrs)
    {
        this.attrs = attrs;
    }
    
    public Map<String, String> toMap() {
        
        Collection<JMSGatewayPropertiesAttrVO> attrs = getAttrs();
        
        Map<String, String> map = new HashMap<String, String>();

        map.put("id", this.getSys_id());

        Method[] methods = this.getClass().getMethods();
        
        for(Method method:methods) {
            String name = method.getName();
            if(method.getAnnotation(MappingAnnotation.class) != null) {
                try {
                    String attr = name.substring(3).toLowerCase();
                    Object value = method.invoke(this, (Object[])null);

                    if(value != null)
                        map.put(attr, value.toString());
                } catch(Exception e) {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
        
        for(Iterator<JMSGatewayPropertiesAttrVO> it=attrs.iterator(); it.hasNext();) {
            JMSGatewayPropertiesAttrVO attr = it.next();
            if(attr != null)
                map.put(attr.getUName(), attr.getUValue());
        }

        return map;
    }
    
    public static JMSGatewayPropertiesVO fromMap(Map<String, Object> map) {
        
        JMSGatewayPropertiesVO vo = new JMSGatewayPropertiesVO();
        
        Method[] methods = vo.getClass().getMethods();
        
        for(Method method:methods) {
            String name = method.getName();
            
            if(StringUtils.isNotBlank(name) && name.length() > 4) {
                if(!name.startsWith("set"))
                    continue;
                
                try {
                    String attr = name.substring(4).toUpperCase();
                    Object value = map.get(attr);
                    
                    if(value != null) {
                        Object[] args = null;
                        
                        if(value instanceof String) {
                            args = new String[1];
                            args[0] = (String)value;
                        }
                        
                        else if(value instanceof Integer) {
                            args = new Integer[1];
                            args[0] = (Integer)value;
                        }
                        
                        else if(value instanceof Boolean) {
                            args = new Boolean[1];
                            args[0] = (Boolean)value;
                        }
                        
                        else if(value instanceof Long) {
                            args = new Long[1];
                            args[0] = (Long)value;
                        }
                        method.invoke(vo, value);
                        map.remove(attr);
                    }
                } catch(Exception e) {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
        
        if(map.size() != 0) {
            Collection<JMSGatewayPropertiesAttrVO> attrs = new ArrayList<JMSGatewayPropertiesAttrVO>();
            
            for(Iterator<String> it=map.keySet().iterator(); it.hasNext();) {
                JMSGatewayPropertiesAttrVO attr = new JMSGatewayPropertiesAttrVO();
                String key = it.next();
                Object value = map.get(key);
                
                attr.setUName(key);
                if(value != null)
                    attr.setUValue(value.toString());
                
                attrs.add(attr);
            }
            
            vo.setAttrs(attrs);
        }
        
        return vo;
    }

} // class JMSGatewayPropertiesVO