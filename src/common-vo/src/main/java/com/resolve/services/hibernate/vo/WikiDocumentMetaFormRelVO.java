/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

public class WikiDocumentMetaFormRelVO extends VO
{
    private static final long serialVersionUID = 2898826796885648732L;

    public final static String RESOURCE_TYPE = "wikidoc_metaform_rel";

    private String UType;
    private String UName;//template name
    private String UWikiDocName; //added for convenince and to be able to search
    private String UFormName;//added for convenince and to be able to search
    private String UDescription;
    private Boolean UIsDefaultRole;
    // Needed for new Impex UI
    private String UFullname; // populate UWiki in this field
    private String USummary; // populate Udescription in this field

    // object references
    private AccessRightsVO accessRights;
    private WikiDocumentVO wikidoc;
    private MetaFormViewVO form;

    // object referenced by

    public WikiDocumentMetaFormRelVO()
    {
    }

    public boolean validate()
    {
        boolean valid = true;
        
        if(StringUtils.isEmpty(getUName()) || (StringUtils.isNotBlank(getUName()) && getUName().equalsIgnoreCase(VO.STRING_DEFAULT)))
        {
            valid = false;
            throw new RuntimeException("Template name cannot be empty.");
        }
        
        valid = getUName().matches(HibernateConstants.REGEX_ALPHANUMERIC_NO_SPACE);
        if(!valid)
        {
            throw new RuntimeException("Template name can have only Alpha numeric and _");
        }
        
        return valid;
    }
    
    public String getUType()
    {
        return UType;
    }

    public void setUType(String uType)
    {
        UType = uType;
    }

    public AccessRightsVO getAccessRights()
    {
        return accessRights;
    }

    public void setAccessRights(AccessRightsVO accessRights)
    {
        this.accessRights = accessRights;
    }

    public WikiDocumentVO getWikidoc()
    {
        return wikidoc;
    }

    public void setWikidoc(WikiDocumentVO wikidoc)
    {
        this.wikidoc = wikidoc;
    }

    public MetaFormViewVO getForm()
    {
        return form;
    }

    public void setForm(MetaFormViewVO form)
    {
        this.form = form;
    }

    public String getUWikiDocName()
    {
        return UWikiDocName;
    }

    public void setUWikiDocName(String uWikiDocName)
    {
        UWikiDocName = uWikiDocName;
    }

    public String getUFormName()
    {
        return UFormName;
    }

    public void setUFormName(String uFormName)
    {
        UFormName = uFormName;
    }

    public String getUDescription()
    {
        return UDescription;
    }

    public void setUDescription(String uDescription)
    {
        UDescription = uDescription;
    }
    
    public Boolean getUIsDefaultRole()
    {
            return UIsDefaultRole;
    }

    public void setUIsDefaultRole(Boolean isDefaultRole)
    {
        UIsDefaultRole = isDefaultRole;
    }

    public String getUName()
    {
        return UName;
    }

    public void setUName(String uName)
    {
        UName = uName;
    }
    
    public String getUFullname()
    {
        return UFullname;
    }

    public void setUFullname(String uFullname)
    {
        UFullname = uFullname;
    }

    public String getUSummary()
    {
        return USummary;
    }

    public void setUSummary(String uSummary)
    {
        USummary = uSummary;
    }
}
