package com.resolve.services.hibernate.vo;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;

public class PullGatewayPropertiesVO extends GatewayPropertiesVO
{
	private static final long serialVersionUID = 1L;
	
    private Boolean UPoll;
    private Integer UPollingInterval;
    
    protected Collection<PullGatewayPropertiesAttrVO> attrs;
    
    @MappingAnnotation(columnName="POLL")
    public Boolean getUPoll()
    {
        return UPoll;
    }

    public void setUPoll(Boolean uPoll)
    {
        UPoll = uPoll;
    }
    
    @MappingAnnotation(columnName="POLLING_INTERVAL")
    public Integer getUPollingInterval()
    {
        return UPollingInterval;
    }

    public void setUPollingInterval(Integer uPollingInterval)
    {
        UPollingInterval = uPollingInterval;
    }

    public Collection<PullGatewayPropertiesAttrVO> getAttrs()
    {
        return attrs;
    }

    public void setAttrs(Collection<PullGatewayPropertiesAttrVO> attrs)
    {
        this.attrs = attrs;
    }
    
    public Map<String, String> toMap() {
        
        Collection<PullGatewayPropertiesAttrVO> attrs = getAttrs();
        
        Map<String, String> map = new HashMap<String, String>();

        map.put("id", this.getSys_id());

        Method[] methods = this.getClass().getMethods();
        
        for(Method method:methods) {
            String name = method.getName();
            if(method.getAnnotation(MappingAnnotation.class) != null) {
                try {
                    String attr = name.substring(3).toLowerCase();
                    Object value = method.invoke(this, (Object[])null);
                    
                    if(value != null)
                        map.put(attr, value.toString());
                } catch(Exception e) {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
        
        for(Iterator<PullGatewayPropertiesAttrVO> it=attrs.iterator(); it.hasNext();) {
            PullGatewayPropertiesAttrVO attr = it.next();
            if(attr != null)
                map.put(attr.getUName(), attr.getUValue());
        }

        return map;
    }

    public static PullGatewayPropertiesVO fromMap(Map<String, Object> map) {
        
        PullGatewayPropertiesVO vo = new PullGatewayPropertiesVO();
        
        Method[] methods = vo.getClass().getMethods();
        
        for(Method method:methods) {
            String name = method.getName();
            
            if(StringUtils.isNotBlank(name) && name.length() > 4) {
                if(!name.startsWith("set"))
                    continue;
                
                try {
                    String attr = name.substring(4).toUpperCase();
                    Object value = map.get(attr);
                    
                    if(value != null) {
                        Object[] args = null;
                        
                        if(value instanceof String) {
                            args = new String[1];
                            args[0] = (String)value;
                        }
                        
                        else if(value instanceof Integer) {
                            args = new Integer[1];
                            args[0] = (Integer)value;
                        }
                        
                        else if(value instanceof Boolean) {
                            args = new Boolean[1];
                            args[0] = (Boolean)value;
                        }
                        
                        else if(value instanceof Long) {
                            args = new Long[1];
                            args[0] = (Long)value;
                        }                        method.invoke(vo, value);
                        map.remove(attr);
                    }
                } catch(Exception e) {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
        
        if(map.size() != 0) {
            Collection<PullGatewayPropertiesAttrVO> attrs = new ArrayList<PullGatewayPropertiesAttrVO>();
            
            for(Iterator<String> it=map.keySet().iterator(); it.hasNext();) {
                PullGatewayPropertiesAttrVO attr = new PullGatewayPropertiesAttrVO();
                String key = it.next();
                Object value = map.get(key);
                
                attr.setUName(key);
                if(value != null)
                    attr.setUValue(value.toString());
                
                attrs.add(attr);
            }
            
            vo.setAttrs(attrs);
        }
        
        return vo;
    }

} // class PullGatewayPropertiesVO