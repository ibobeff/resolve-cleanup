/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.resolve.util.Log;

public class QuerySort
{
    public static final String SORT_PROP_NAME_PREFIX = "resolve_sort_param_";
    
	public enum SortOrder {
        ASC,
        DESC
    }

    private String property;
    private SortOrder direction;

    
    public QuerySort() {}

	@Deprecated
    public QuerySort(String property, String direction)
    {
        Log.log.warn("{@link #QuerySort(String, String)} is deprecated. Use {@link #QuerySort(String, SortOrder)} instead.");
        try
        {
            SortOrder dir = SortOrder.valueOf(direction);
            if (StringUtils.isNotBlank(property) && property.startsWith("u") && !property.startsWith("u_"))
            {
                property = property.substring(0, 2).toUpperCase() + property.substring(2);
            }

            this.property = property;
            this.direction = dir;
        }
        catch (IllegalArgumentException e)
        {
            throw new RuntimeException("Invalid sorting direction value");
        }
    }

    public QuerySort(String property, SortOrder direction)
    {
        if (StringUtils.isNotBlank(property) && property.startsWith("u") && !property.startsWith("u_"))
        {
            property = property.substring(0, 2).toUpperCase() + property.substring(2);
        }
        this.property = property;
        this.direction = direction;
    }

    public static List<QuerySort> decodeJson(String json)
    {
        List<QuerySort> sorts = new ArrayList<QuerySort>();
        if (StringUtils.isNotEmpty(json))
        {
			try {
				List<Map<String, String>> sortList = new ObjectMapper().readValue(json,
						new TypeReference<List<Map<String, String>>>() {
						});
				for (Map<String, String> sort : sortList) {
					SortOrder sortOrder = SortOrder.valueOf(sort.get("direction").toUpperCase());
					if (StringUtils.isNotBlank(sort.get("direction")) && sortOrder == null) {
						throw new IllegalArgumentException("Ivalid sorting direction");
					}
					sorts.add(new QuerySort(sort.get("property"), sortOrder));
				}
			} catch (IOException e) {
				Log.log.error("Unable to decode incoming parameter");
			}
        }
        return sorts;
    }

    public static String encodeJson(List<QuerySort> sorts)
    {
        String json = null;

        if(sorts != null && sorts.size() > 0)
        {
            try
            {
                json = new ObjectMapper().writeValueAsString(sorts);
            }
            catch(Exception e)
            {

            }
        }

        return json;
    }

    public String getProperty()
    {
        return property;
    }

    public void setProperty(String property)
    {
        this.property = property;
    }

    public SortOrder getDirection()
    {
        return direction;
    }

    public void setDirection(SortOrder direction)
    {
        this.direction = direction;
    }
    
    @Override
    public String toString()
    {
        return "QuerySort [property=" + property + ", direction=" + direction + "]";
    }
}
