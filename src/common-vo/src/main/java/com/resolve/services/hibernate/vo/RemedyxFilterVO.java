/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class RemedyxFilterVO extends GatewayFilterVO
{
    private static final long serialVersionUID = -826204288198774979L;

    private String UFormName;
    private String UQuery;
    private String ULastValueField;
    private Integer ULimit;

    public RemedyxFilterVO()
    {
        super();
    }

    @MappingAnnotation(columnName = "FORM_NAME")
    public String getUFormName()
    {
        return UFormName;
    }

    public void setUFormName(String uFormName)
    {
        if (StringUtils.isNotBlank(uFormName) || UFormName.equals(VO.STRING_DEFAULT))
        {
            UFormName = uFormName != null ? uFormName.trim() : uFormName;
        }
    }

    @MappingAnnotation(columnName = "QUERY")
    public String getUQuery()
    {
        return this.UQuery;
    } // getUQuery

    public void setUQuery(String uQuery)
    {
        if (StringUtils.isNotBlank(uQuery) || UQuery.equals(VO.STRING_DEFAULT))
        {
            this.UQuery = uQuery != null ? uQuery.trim() : uQuery;
        }
    } // setUQuery
    
    @MappingAnnotation(columnName="LIMIT")
    public Integer getULimit()
    {
        return this.ULimit;
    } // getULimit

    public void setULimit(Integer ULimit)
    {
        this.ULimit = ULimit;
    } // setULimit

    @MappingAnnotation(columnName = "LAST_VALUE_FIELD")
    public String getULastValueField()
    {
        return ULastValueField;
    }

    public void setULastValueField(String uLastValueField)
    {
        if (StringUtils.isNotBlank(uLastValueField) || ULastValueField.equals(VO.STRING_DEFAULT))
        {
            ULastValueField = uLastValueField != null ? uLastValueField.trim() : uLastValueField;
        }
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UFormName == null) ? 0 : UFormName.hashCode());
        result = prime * result + ((ULastValueField == null) ? 0 : ULastValueField.hashCode());
        result = prime * result + ((UQuery == null) ? 0 : UQuery.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        RemedyxFilterVO other = (RemedyxFilterVO) obj;
        if (UFormName == null)
        {
            if (StringUtils.isNotBlank(other.UFormName)) return false;
        }
        else if (!UFormName.trim().equals(other.UFormName == null ? "" : other.UFormName.trim())) return false;
        if (ULastValueField == null)
        {
            if (StringUtils.isNotBlank(other.ULastValueField)) return false;
        }
        else if (!ULastValueField.trim().equals(other.ULastValueField == null ? "" : other.ULastValueField.trim())) return false;
        if (UQuery == null)
        {
            if (StringUtils.isNotBlank(other.UQuery)) return false;
        }
        else if (!UQuery.trim().equals(other.UQuery == null ? "" : other.UQuery.trim())) return false;
        return true;
    }
}
