/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;

public class MetaxFieldDependencyVO extends VO
{
    private static final long serialVersionUID = -2505086164552909088L;
    
    private String UTarget;
    private String UAction;
    private String UCondition;
    private String UValue;
    private String UActionOptions;

    private MetaFieldPropertiesVO metaFieldProperties;
    private MetaControlItemVO metaControlItem;
    private MetaFormTabVO metaFormTab;

    public MetaxFieldDependencyVO()
    {
    }

    public MetaFieldPropertiesVO getMetaFieldProperties()
    {
        return metaFieldProperties;
    }

    public void setMetaFieldProperties(MetaFieldPropertiesVO metaFieldProperties)
    {
        this.metaFieldProperties = metaFieldProperties;

        if (metaFieldProperties != null)
        {
            Collection<MetaxFieldDependencyVO> coll = metaFieldProperties.getMetaxFieldDependencys();
            if (coll == null)
            {
                coll = new HashSet<MetaxFieldDependencyVO>();
                coll.add(this);

                metaFieldProperties.setMetaxFieldDependencys(coll);
            }
        }
    }

    public MetaControlItemVO getMetaControlItem()
    {
        return metaControlItem;
    }

    public void setMetaControlItem(MetaControlItemVO metaControlItem)
    {
        this.metaControlItem = metaControlItem;

        if (metaControlItem != null)
        {
            Collection<MetaxFieldDependencyVO> coll = metaControlItem.getMetaxFieldDependencys();
            if (coll == null)
            {
                coll = new HashSet<MetaxFieldDependencyVO>();
                coll.add(this);

                metaControlItem.setMetaxFieldDependencys(coll);
            }
        }
    }

    public MetaFormTabVO getMetaFormTab()
    {
        return metaFormTab;
    }

    public void setMetaFormTab(MetaFormTabVO metaFormTab)
    {
        this.metaFormTab = metaFormTab;

        if (metaFormTab != null)
        {
            Collection<MetaxFieldDependencyVO> coll = metaFormTab.getMetaxFieldDependencys();
            if (coll == null)
            {
                coll = new HashSet<MetaxFieldDependencyVO>();
                coll.add(this);

                metaFormTab.setMetaxFieldDependencys(coll);
            }
        }
    }

    public String getUTarget()
    {
        return UTarget;
    }

    public void setUTarget(String uTarget)
    {
        UTarget = uTarget;
    }

    public String getUAction()
    {
        return UAction;
    }

    public void setUAction(String uAction)
    {
        UAction = uAction;
    }

    public String getUCondition()
    {
        return UCondition;
    }

    public void setUCondition(String uCondition)
    {
        UCondition = uCondition;
    }

    public String getUValue()
    {
        return UValue;
    }

    public void setUValue(String uValue)
    {
        UValue = uValue;
    }

    public String getUActionOptions()
    {
        return UActionOptions;
    }

    public void setUActionOptions(String uActionOptions)
    {
        UActionOptions = uActionOptions;
    }

}
