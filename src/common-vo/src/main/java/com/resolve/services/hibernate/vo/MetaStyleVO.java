/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;



public class MetaStyleVO extends VO
{
    private static final long serialVersionUID = -6684352794911268222L;
    
    private String UStyle;
    private String UValue;
    private String UFormValidation;

    public MetaStyleVO()
    {
    }
    
    public String getUStyle()
    {
        return this.UStyle;
    }

    public void setUStyle(String UStyle)
    {
        this.UStyle = UStyle;
    }
    
    public String getUValue()
    {
        return UValue;
    }
    
    public void setUValue(String UValue)
    {
        this.UValue = UValue;
    }
    
    
    public String getUFormValidation()
    {
        return UFormValidation;
    }
    public void setUFormValidation(String uFormValidation)
    {
        UFormValidation = uFormValidation;
    }
    
}

