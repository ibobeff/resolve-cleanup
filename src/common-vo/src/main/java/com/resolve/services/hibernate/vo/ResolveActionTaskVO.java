/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.hibernate.vo.exception.ValidationException;
import com.resolve.services.interfaces.IndexComponent;
import com.resolve.services.interfaces.UIDisplayAPI;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.ReferenceDTO;
import com.resolve.util.StringUtils;

public class ResolveActionTaskVO extends VO implements UIDisplayAPI, IndexComponent
{
    private static final long serialVersionUID = -3567908551408379666L;

    public final static String RESOURCE_TYPE = "actiontask";

    private String UName;
    private String UNamespace;
    private String UFullName;
 	private String UTypes;
    private Boolean ULogresult;
    private String USummary;
    private String UDescription;
    private String URoles;//this will be same as UExecuteRoles
    private Boolean UActive;
    private String UMenuPath;
    private Boolean UIsDeleted;
    private Boolean UIsDefaultRole;
    private Boolean UIsHidden;
    private String wizardType;

    private Integer UVersion;
    private Boolean UIsStable;

    public Integer getUVersion()
    {
        return this.UVersion;
    }

    public void setUVersion(Integer UVersion)
    {
        this.UVersion = UVersion;
    }
    
    public Boolean getUIsStable() {
		return UIsStable;
	}

	public void setUIsStable(Boolean uIsStable) {
		UIsStable = uIsStable;
	}

    // object references
//    private ResolvePreprocessVO preprocess;           // UPreprocess;

    // object referenced by
    private ResolveActionInvocVO resolveActionInvoc;  // UInvocation
    
    private UsersVO assignedTo;                       // UAssignedTo;
    private AccessRightsVO accessRights;
    
    private Set<String> tags;
    
    //for UI
    private String inputOutputDesc;
    private String assignedToUsername;
    
    private List<ReferenceDTO> referencedIn;
    
    private Collection<ResolveActionTaskResolveTagRelVO> atTagRels;
    private boolean hasChildren;

    public ResolveActionTaskVO()
    {
    }
    
    public ResolveActionTaskVO(String sysId)
    {
        super.setSys_id(sysId);
    }

    public boolean validate() throws ValidationException
    {
        boolean valid = true;

        if(StringUtils.isEmpty(getUName()) || StringUtils.isEmpty(getUNamespace()))
        {
            valid = false;
            throw new ValidationException("ActionTask Name and Namespace are mandatory");
        }
        
        //validate the name
        valid = getUName().matches(HibernateConstants.REGEX_ACTIONTASK_NAME_VALIDATION);
        valid = valid && !StringUtils.isWhitespace(getUName());
        if(!valid)
        {
            throw new ValidationException(String.format("ActionTask name (%s) can have only alphanumerics,'_', '-' and space", getUName()));
        }
        
        //validate the namespace
        valid = getUNamespace().matches(HibernateConstants.REGEX_ACTIONTASK_NAMESPACE_VALIDATION);
        if(!valid) 
        {
            throw new ValidationException(String.format("ActionTask namespace (%s) can have only alphanumerics,'_','-' and '.'", getUNamespace()));
        }
        
        
        //make sure that the fullname is correctly set
        setUFullName(getUName() + "#" + getUNamespace());

        //validate the fullname
        valid = getUFullName().matches(HibernateConstants.REGEX_ACTIONTASK_FULLNAME_VALIDATION);
        if(!valid)
        {
            throw new ValidationException(String.format("ActionTask fullname (%s) can have only alphanumerics,'_','#','-' and '.'",getUFullName()));
        }

        return valid;
    }
    
    public ResolveActionTaskVO usetsys_id(String sys_id)
    {
        setSys_id(sys_id);
        return this;
    }

    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    public String getUNamespace()
    {
        return this.UNamespace;
    }

    public void setUNamespace(String UNamespace)
    {
        this.UNamespace = UNamespace;
    }

    public String getUFullName()
	{
		return UFullName;
	}

	public void setUFullName(String fullName)
	{
		this.UFullName = fullName;
	}

    
    public String getUTypes()
    {
        return this.UTypes;
    }

    public void setUTypes(String UTypes)
    {
        this.UTypes = UTypes;
    }

    public Boolean getULogresult()
    {
        return this.ULogresult;
    }

    public Boolean ugetULogresult()
    {
        if (this.ULogresult == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.ULogresult;
        }
    } // ugetULogresult

    public void setULogresult(Boolean ULogresult)
    {
        this.ULogresult = ULogresult;
    }

    public String getUSummary()
    {
        return this.USummary;
    }

    public void setUSummary(String USummary)
    {
        this.USummary = USummary;
    }

    public String getUDescription()
    {
        return this.UDescription;
    }

    public void setUDescription(String UDescription)
    {
        this.UDescription = UDescription;
    }

    public String getURoles()
    {
        return this.URoles;
    }

    public void setURoles(String URoles)
    {
        this.URoles = URoles;
    }

    public Boolean getUActive()
    {
        return this.UActive;
    } // getUActive

    public Boolean ugetUActive()
    {
        if (this.UActive == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UActive;
        }
    } // getUActive

    public void setUActive(Boolean UActive)
    {
        this.UActive = UActive;
    }

//    public ResolvePreprocessVO getPreprocess()
//    {
//        return preprocess;
//    }
//
//    public void setPreprocess(ResolvePreprocessVO preprocess)
//    {
//        this.preprocess = preprocess;
//
////        if (preprocess != null)
////        {
////            Collection<ResolveActionTaskVO> coll = preprocess.getResolveActionTasks();
////            if (coll == null)
////            {
////				coll = new HashSet<ResolveActionTaskVO>();
////	            coll.add(this);
////	            
////                preprocess.setResolveActionTasks(coll);
////            }
////        }
//    }

    public String getUMenuPath()
    {
        return this.UMenuPath;
    }

    public void setUMenuPath(String UMenuPath)
    {
        this.UMenuPath = UMenuPath;
    }
    
    public Boolean getUIsDefaultRole()
    {
            return UIsDefaultRole;
    }
    
    public Boolean ugetUIsDefaultRole()
    {
        if (this.UIsDefaultRole == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsDefaultRole;
        }
    } // ugetUIsDefaultRole

    public void setUIsDefaultRole(Boolean isDefaultRole)
    {
        UIsDefaultRole = isDefaultRole;
    }
    

    public Boolean getUIsDeleted()
    {
            return UIsDeleted;
    }
    
    public Boolean ugetUIsDeleted()
    {
        if (this.UIsDeleted == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsDeleted;
        }
    } // ugetUIsDeleted

    public void setUIsDeleted(Boolean isDeleted)
    {
        UIsDeleted = isDeleted;
    }

    public Boolean getUIsHidden()
    {
        return UIsHidden;
    }

    public void setUIsHidden(Boolean uIsHidden)
    {
        UIsHidden = uIsHidden;
    }

	public String getWizardType()
	{
		return wizardType;
	}
	public void setWizardType(String wizardType)
	{
		this.wizardType = wizardType;
	}

	public ResolveActionInvocVO getResolveActionInvoc()
    {
        return this.resolveActionInvoc;
    } // getResolveActionInvoc

    public void setResolveActionInvoc(ResolveActionInvocVO actionInvocation)
    {
        this.resolveActionInvoc = actionInvocation;
    } // setResolveActionInvoc
    
	public UsersVO getAssignedTo()
	{
		return assignedTo;
	}

	public void setAssignedTo(UsersVO assignedTo)
	{
		this.assignedTo = assignedTo;
		
        // one-way only - no collections on the other side
	}

	public AccessRightsVO getAccessRights()
    {
        return accessRights;
    }
	
	public void setAccessRights(AccessRightsVO accessRights)
    {
        this.accessRights = accessRights;
        
        // one-way only - no collections on the other side
    }

    public boolean equals(Object otherObj)
    {
        boolean isEquals = false;
        if (otherObj instanceof ResolveActionTaskVO)
        {
            ResolveActionTaskVO otherTask = (ResolveActionTaskVO) otherObj;
            // if sys_ids are not equal, they are diff
            if (otherTask.getSys_id().equals(this.getSys_id()))
            {
                isEquals = true;
            }
        }
        return isEquals;
    }

    @Override
    public int hashCode()
    {
        int hash = 1;
        hash = hash * 31 + this.UName.hashCode() + this.UNamespace.hashCode();
        hash = hash * 31 + (getSys_id() == null ? 0 : getSys_id().hashCode());
        return hash;
    }

    @Override
    public String toString()
    {
        StringBuffer str = new StringBuffer();

        str.append("Action Task Namespace:").append(UNamespace).append("\n");
        str.append("Action Task Name:").append(UName).append("\n");
        str.append("Action Task Summary:").append(USummary).append("\n");

        return str.toString();
    }

	public String ui_getDisplayName()
	{
		return UNamespace + "." + UName;
	}


	public String ui_getDisplayType()
	{
		return "Action Task";
	}

    public String ugetIndexContent()
    {
        return      UName
            + " " + UNamespace
            + " " + UFullName
            + " " + UDescription
            + " " + USummary;
    }//ugetIndexContent

    public String getInputOutputDesc()
    {
        return inputOutputDesc;
    }

    public void setInputOutputDesc(String inputOutputDesc)
    {
        this.inputOutputDesc = inputOutputDesc;
    }

    public Set<String> getTags()
    {
        return tags;
    }

    public void setTags(Set<String> tags)
    {
        this.tags = tags;
    }

    public String getTagsAsCSV()
    {
        StringBuilder result = new StringBuilder();;
        if(tags != null && tags.size() > 0)
        {
            for(String tag : tags)
            {
                result.append(tag);
                result.append(",");
            }
        }
        return result.toString();
    }
    
    public void setTagsAsCSV(String tags)
    {
    	/*
    	 * Dummy setter, just to make UI happy.
    	 * Needed so that if UI sends back the same json back, JsonParser will not throw an exception as 'tagsAsCSV' is not a model property.
    	 */
    }

    public List<ReferenceDTO> getReferencedIn()
    {
        return referencedIn;
    }

    public void setReferencedIn(List<ReferenceDTO> referencedIn)
    {
        this.referencedIn = referencedIn;
    }

    public String getAssignedToUsername()
    {
        return assignedToUsername;
    }

    public void setAssignedToUsername(String assignedToUsername)
    {
        this.assignedToUsername = assignedToUsername;
    }

    public Collection<ResolveActionTaskResolveTagRelVO> getAtTagRels()
    {
        return atTagRels;
    }

    public void setAtTagRels(Collection<ResolveActionTaskResolveTagRelVO> atTagRels)
    {
        this.atTagRels = atTagRels;
    }

    public boolean isHasChildren()
    {
        return hasChildren;
    }
    public void setHasChildren(boolean hasChildren)
    {
        this.hasChildren = hasChildren;
    }
} // ResolveActionTask
