package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ArchiveWorksheetDataVO extends VO
{
	private static final long serialVersionUID = 5691997084024094662L;
	
	private String worksheetId;
    private String propertyName;
    private Object propertyValue;
    
    public String getWorksheetId() {
        return worksheetId;
    }
    
    public void setWorksheetId(String worksheetId) {
        this.worksheetId = worksheetId;
    }
    
    public String getPropertyName() {
        return propertyName;
    }
    
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }
    
    public Object getPropertyValue() {
    	return this.propertyValue;
    }
    
    public void setPropertyValue(Object propertyValue) {
    	this.propertyValue = propertyValue;
    }
    
    @Override
    public String toString() {
    	return String.format("%s: [Sys_id: %s, Worksheet Id: %s, Property Name: %s, Property Value%s: %s]",
    						 ArchiveWorksheetDataVO.class.getSimpleName(), getSys_id(), worksheetId, propertyName, 
    						 (propertyValue != null ? "(" + propertyValue.getClass().getSimpleName() + ")" : ""),
    						 (propertyValue != null ? propertyValue : "null"));
    }
}
