/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.vo.social;


public enum NodeType
{
    DOCUMENT,
    RUNBOOK,
    DECISIONTREE,
    
    NAMESPACE,
    ACTIONTASK,
    WORKSHEET,
    
    TAG,
    ResolveTag, //only for backward compatibility - DO NOT USE UNLESS DATA COMING IN FROM 5.0
    
    USER,
    
    PROCESS,
    TEAM,
    FORUM,
    RSS,
    
    //for catalog - currently used in Impex
    //only for backward compatibility - DO NOT USE UNLESS DATA COMING IN FROM 5.0
    CatalogItem,
    CatalogFolder,
    CatalogGroup, 
    CatalogReference,
    
    //For the Social UI - IS NOT PERSISTED AND IS VIRTUAL IN NATURE
    INBOX,
    STARRED,
    USER_ALL,
    DEFAULT, //for Activity, Starred, Blog, Sent on the Social Screen
    SYSTEM, // used to indicate the Default SYSTEM stream on the Social UI
    
//    boolean isValidDisplaytype = EnumUtils.isValidEnum(CatalogDisplayType.class, localCatalog.getDisplayType());
    
    // Below type is for backward compatibility (5.1.x) only
    CHILD_OF_CATALOG,
    // Below type is subcatagory of RUNBOOK generated from AB 
    AB_RUNBOOK,
    
    // Security Incident Response (SIR) specific types
    PLAYBOOK_TEMPLATE,
    SIR_PLAYBOOK // Updated Playbook Template associated with specific SIR  
}
