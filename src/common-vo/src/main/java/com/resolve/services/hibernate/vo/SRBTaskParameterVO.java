/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
public class SRBTaskParameterVO extends VO
{
    private static final long serialVersionUID = 8460609988816259215L;
	private String variable;
    private String sourceName;
    private String source;
    
    private RBTaskVO task;

    public SRBTaskParameterVO()
    {
        super();
    }

    public String getVariable()
    {
        return variable;
    }

    public void setVariable(String variable)
    {
        this.variable = variable;
    }

    public String getSourceName()
    {
        return sourceName;
    }

    public void setSourceName(String sourceName)
    {
        this.sourceName = sourceName;
    }
    
    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }
    
    public RBTaskVO getTask()
    {
        return task;
    }

    public void setTask(RBTaskVO task)
    {
        this.task = task;
    }
}
