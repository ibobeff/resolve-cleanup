/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;

public class ArchiveExecuteRequestVO extends VO
{

    private static final long serialVersionUID = 4418506104188105743L;
 
    private String UExecuteStatus;
    private String UTrigger;
    private String UNumber;
    private String UWiki;
    private String UNodeId;
    private String UNodeLabel;
    private String UEventId;
    private Integer UDuration;

    // object references
    private ArchiveWorksheetVO problem; // UProblem;
    private ArchiveProcessRequestVO process; // UProcess;
    private ResolveActionTaskVO task; // UTask;

    // object referenced by
    private Collection<ArchiveExecuteResultVO> archiveExecuteResults;
    private Collection<ArchiveActionResultVO> archiveActionResults;
    private Collection<ArchiveExecuteDependencyVO> archiveExecuteDependencies;

    public ArchiveExecuteRequestVO()
    {
    }

    public ArchiveExecuteRequestVO usetsys_id(String sys_id)
    {
        this.setSys_id(sys_id);
        return this;
    }

    public String getUExecuteStatus()
    {
        return this.UExecuteStatus;
    }

    public void setUExecuteStatus(String UExecuteStatus)
    {
        this.UExecuteStatus = UExecuteStatus;
    }

    public ArchiveProcessRequestVO getProcess()
    {
        return process;
    }

    public void setProcess(ArchiveProcessRequestVO process)
    {
        this.process = process;

        if (process != null)
        {
            Collection<ArchiveExecuteRequestVO> coll = process.getArchiveExecuteRequests();
            if (coll == null)
            {
                coll = new HashSet<ArchiveExecuteRequestVO>();
                coll.add(this);

                process.setArchiveExecuteRequests(coll);
            }
        }
    } // setProcess

    public ResolveActionTaskVO getTask()
    {
        return task;
    }

    public void setTask(ResolveActionTaskVO task)
    {
        this.task = task;

        // one-way only - no collections on the other side
    }

    public String getUTrigger()
    {
        return this.UTrigger;
    }

    public void setUTrigger(String UTrigger)
    {
        this.UTrigger = UTrigger;
    }

    public ArchiveWorksheetVO getProblem()
    {
        return problem;
    }

    public void setProblem(ArchiveWorksheetVO problem)
    {
        this.problem = problem;

        if (problem != null)
        {
            Collection<ArchiveExecuteRequestVO> coll = problem.getArchiveExecuteRequests();
            if (coll == null)
            {
                coll = new HashSet<ArchiveExecuteRequestVO>();
                coll.add(this);

                problem.setArchiveExecuteRequests(coll);
            }
        }
    }

    public String getUNumber()
    {
        return this.UNumber;
    }

    public void setUNumber(String UNumber)
    {
        this.UNumber = UNumber;
    }

    public String getUWiki()
    {
        return this.UWiki;
    }

    public void setUWiki(String UWiki)
    {
        this.UWiki = UWiki;
    }

    public String getUNodeId()
    {
        return this.UNodeId;
    }

    public void setUNodeId(String UNodeId)
    {
        this.UNodeId = UNodeId;
    }

    public String getUNodeLabel()
    {
        return this.UNodeLabel;
    }

    public void setUNodeLabel(String UNodeLabel)
    {
        this.UNodeLabel = UNodeLabel;
    }

    public String getUEventId()
    {
        return this.UEventId;
    }

    public void setUEventId(String UEventId)
    {
        this.UEventId = UEventId;
    }

    public Integer getUDuration()
    {
        return this.UDuration;
    }

    public void setUDuration(Integer UDuration)
    {
        this.UDuration = UDuration;
    }

    public Collection<ArchiveExecuteResultVO> getArchiveExecuteResults()
    {
        return archiveExecuteResults;
    }

    public void setArchiveExecuteResults(Collection<ArchiveExecuteResultVO> archiveExecuteResults)
    {
        this.archiveExecuteResults = archiveExecuteResults;
    }

    public ArchiveExecuteResultVO ugetArchiveExecuteResult()
    {
        ArchiveExecuteResultVO result = null;

        if (archiveExecuteResults != null && archiveExecuteResults.size() > 0)
        {
            result = archiveExecuteResults.iterator().next();
        }

        return result;
    } // ugetArchiveExecuteResult

    public void usetArchiveExecuteResult(ArchiveExecuteResultVO archiveExecuteResult)
    {
        archiveExecuteResult.setExecuteRequest(this);
    } // usetArchiveExecuteResult

    public Collection<ArchiveActionResultVO> getArchiveActionResults()
    {
        return archiveActionResults;
    }

    public void setArchiveActionResults(Collection<ArchiveActionResultVO> archiveActionResults)
    {
        this.archiveActionResults = archiveActionResults;
    }

    public ArchiveActionResultVO ugetArchiveActionResult()
    {
        ArchiveActionResultVO result = null;

        if (archiveActionResults != null && archiveActionResults.size() > 0)
        {
            result = archiveActionResults.iterator().next();
        }

        return result;
    } // ugetArchiveActionResult

    public void usetArchiveActionResult(ArchiveActionResultVO archiveActionResult)
    {
        archiveActionResult.setExecuteRequest(this);
    } // usetArchiveActionResult

    public Collection<ArchiveExecuteDependencyVO> getArchiveExecuteDependencies()
    {
        return archiveExecuteDependencies;
    }

    public void setArchiveExecuteDependencies(Collection<ArchiveExecuteDependencyVO> archiveExecuteDependencies)
    {
        this.archiveExecuteDependencies = archiveExecuteDependencies;
    }

} // ArchiveExecuteRequest
