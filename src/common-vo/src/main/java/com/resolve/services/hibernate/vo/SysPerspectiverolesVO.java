/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class SysPerspectiverolesVO extends VO
{
    private static final long serialVersionUID = -4817627097614604257L;
    
    private String value;
    private Integer sequence;

	
    // object referenced by
    private SysPerspectiveVO sysPerspective; // parentId;
    
    public SysPerspectiverolesVO()
    {
    }

    public String getValue()
    {
        return this.value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public Integer getSequence()
    {
        return this.sequence;
    }

    public void setSequence(Integer sequence)
    {
        this.sequence = sequence;
    }

    public SysPerspectiveVO getSysPerspective()
    {
        return sysPerspective;
    }

    public void setSysPerspective(SysPerspectiveVO sysPerspective)
    {
        this.sysPerspective = sysPerspective;

//        if (sysPerspective != null)
//        {
//            Collection<SysPerspectiverolesVO> coll = sysPerspective.getSysPerspectiveroles();
//            if (coll == null)
//            {
//				coll = new HashSet<SysPerspectiverolesVO>();
//	            coll.add(this);
//	            
//                sysPerspective.setSysPerspectiveroles(coll);
//            }
//        }
    }

}
