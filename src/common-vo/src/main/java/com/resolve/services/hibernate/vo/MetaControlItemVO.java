/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;

public class MetaControlItemVO extends VO
{
    private static final long serialVersionUID = 9215283662617710623L;

    private String UName;
    private String UDisplayName;
    private String UGroupName;
    private String UGroupDisplayName;
    private String UType;
    private String UParam;
    private Integer USequence;// sequence #

    private boolean customTableDisplay;

    // object references
    private MetaControlVO metaControl;
    private Collection<MetaFormActionVO> metaFormActions;

    private Collection<MetaxFieldDependencyVO> metaxFieldDependencys;

    // RBA-13999 : adding extra properties, like font, color, etc. - JSON text field
    private String UPropertiesJSON;

    public MetaControlItemVO()
    {
    }

    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    public String getUDisplayName()
    {
        return this.UDisplayName;
    }

    public void setUDisplayName(String UDisplayName)
    {
        this.UDisplayName = UDisplayName;
    }

    public String getUGroupName()
    {
        return this.UGroupName;
    }

    public void setUGroupName(String UGroupName)
    {
        this.UGroupName = UGroupName;
    }

    public String getUGroupDisplayName()
    {
        return this.UGroupDisplayName;
    }

    public void setUGroupDisplayName(String UGroupDisplayName)
    {
        this.UGroupDisplayName = UGroupDisplayName;
    }

    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    public String getUParam()
    {
        return this.UParam;
    }

    public void setUParam(String UParam)
    {
        this.UParam = UParam;
    }

    public Integer getUSequence()
    {
        return USequence;
    }

    public void setUSequence(Integer uSequence)
    {
        USequence = uSequence;
    }

    public MetaControlVO getMetaControl()
    {
        return metaControl;
    }

    public void setMetaControl(MetaControlVO metaControl)
    {
        this.metaControl = metaControl;

        if (metaControl != null)
        {
            Collection<MetaControlItemVO> coll = metaControl.getMetaControlItems();
            if (coll == null)
            {
                coll = new HashSet<MetaControlItemVO>();
                coll.add(this);

                metaControl.setMetaControlItems(coll);
            }
        }
    }

    public Collection<MetaFormActionVO> getMetaFormActions()
    {
        return metaFormActions;
    }

    public void setMetaFormActions(Collection<MetaFormActionVO> metaFormActions)
    {
        this.metaFormActions = metaFormActions;
    }

    public Collection<MetaxFieldDependencyVO> getMetaxFieldDependencys()
    {
        return this.metaxFieldDependencys;
    }

    public void setMetaxFieldDependencys(Collection<MetaxFieldDependencyVO> metaxFieldDependencys)
    {
        this.metaxFieldDependencys = metaxFieldDependencys;
    }

    public boolean isCustomTableDisplay()
    {
        return customTableDisplay;
    }

    public void setCustomTableDisplay(boolean customTableDisplay)
    {
        this.customTableDisplay = customTableDisplay;
    }

    public String getUPropertiesJSON()
    {
        return UPropertiesJSON;
    }

    public void setUPropertiesJSON(String uPropertiesJSON)
    {
        UPropertiesJSON = uPropertiesJSON;
    }

}
