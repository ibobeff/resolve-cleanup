/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveEventHandlerVO extends VO
{
    private static final long serialVersionUID = -8255035283677280837L;
    
    private String UName;
    private String UType;
    private String UDescription;
    private String UScript;

    public ResolveEventHandlerVO()
    {
    }

    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    public String getUDescription()
    {
        return this.UDescription;
    }

    public void setUDescription(String UDescription)
    {
        this.UDescription = UDescription;
    }

    public String getUScript()
    {
        return this.UScript;
    }

    public void setUScript(String UScript)
    {
        this.UScript = UScript;
    }


}
