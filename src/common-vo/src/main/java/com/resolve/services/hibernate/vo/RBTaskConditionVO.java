/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
public class RBTaskConditionVO extends VO
{
    private static final long serialVersionUID = 5232567596282403945L;
	private String conditionId; // identity for one GOOD or BAD condition group
    private String result; // GOOD, BAD
    private String criteria; // ANY, ALL
    private String variableSource;
    private String variable;
    private String comparison; // EQUALS, >, >= etc.
    private String source;
    private String sourceName;
    private Integer order;
    
    private RBTaskVO task;

    public RBTaskConditionVO()
    {
        super();
    }

    public String getConditionId()
    {
        return conditionId;
    }

    public void setConditionId(String conditionId)
    {
        this.conditionId = conditionId;
    }

    public String getResult()
    {
        return result;
    }

    public void setResult(String result)
    {
        this.result = result;
    }

    public String getCriteria()
    {
        return criteria;
    }

    public void setCriteria(String criteria)
    {
        this.criteria = criteria;
    }

    public String getVariableSource()
    {
        return variableSource;
    }

    public void setVariableSource(String variableSource)
    {
        this.variableSource = variableSource;
    }

    public String getVariable()
    {
        return variable;
    }

    public void setVariable(String variable)
    {
        this.variable = variable;
    }

    public String getComparison()
    {
        return comparison;
    }

    public void setComparison(String comparison)
    {
        this.comparison = comparison;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public String getSourceName()
    {
        return sourceName;
    }

    public void setSourceName(String sourceName)
    {
        this.sourceName = sourceName;
    }
    
    public Integer getOrder()
    {
        return order;
    }

    public void setOrder(Integer order)
    {
        this.order = order;
    }

    public RBTaskVO getTask()
    {
        return task;
    }

    public void setTask(RBTaskVO task)
    {
        this.task = task;
    }
}
