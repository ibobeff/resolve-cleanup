/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.resolve.services.interfaces.VO;

public class ArchiveActionResultVO extends VO
{
    private static final long serialVersionUID = -8401091194184014095L;

    private String UCompletion;
    private String UCondition;
    private String USeverity;
    private Integer UDuration;
    private Long UTimestamp;
    private String UAddress;
    private String UEsbAddr;
    private String UTarget;
    private String UTargetGUID;
    private String UWiki;
    private String UNodeId;
    private Boolean UHidden;
    
    private String UProcessId; //transient
    private String UProcessNumber; //transient
    private String UTaskId; //transient
    private String UTaskName; //transient
    private String USummary; //transient

    // object references
    private ResolveActionTaskVO task; // UActiontask;
    private ArchiveProcessRequestVO process; // UProcess;
    private ArchiveWorksheetVO problem; // UProblem;
    private ArchiveExecuteRequestVO executeRequest; // UExecuteRequest;
    private ArchiveExecuteResultVO executeResult; // UExecuteResult;
    private ArchiveActionResultLobVO actionResultLob;

    // object referenced by

    public ArchiveActionResultVO()
    {
    }

    public ArchiveActionResultVO usetsys_id(String sys_id)
    {
        this.setSys_id(sys_id);
        return this;
    }

    public String getUCompletion()
    {
        return this.UCompletion;
    }

    public void setUCompletion(String UCompletion)
    {
        this.UCompletion = UCompletion;
    }

    public String getUCondition()
    {
        return this.UCondition;
    }

    public void setUCondition(String UCondition)
    {
        this.UCondition = UCondition;
    }

    public String getUSeverity()
    {
        return this.USeverity;
    }

    public void setUSeverity(String USeverity)
    {
        this.USeverity = USeverity;
    }

    public ResolveActionTaskVO getTask()
    {
        return task;
    }

    public void setTask(ResolveActionTaskVO task)
    {
        this.task = task;

        // one-way only - no collections on the other side
    }

    @JsonIgnore
    public ArchiveProcessRequestVO getProcess()
    {
        return process;
    }

    public void setProcess(ArchiveProcessRequestVO process)
    {
        this.process = process;

        if (process != null)
        {
            Collection<ArchiveActionResultVO> coll = process.getArchiveActionResults();
            if (coll == null)
            {
                coll = new HashSet<ArchiveActionResultVO>();
                coll.add(this);

                process.setArchiveActionResults(coll);
            }
        }
    }

    public Integer getUDuration()
    {
        return this.UDuration;
    }

    public void setUDuration(Integer UDuration)
    {
        this.UDuration = UDuration;
    }

    public Long getUTimestamp()
    {
        return this.UTimestamp;
    }

    public void setUTimestamp(Long UTimestamp)
    {
        this.UTimestamp = UTimestamp;
    }

    @JsonIgnore
    public ArchiveWorksheetVO getProblem()
    {
        return problem;
    }

    public void setProblem(ArchiveWorksheetVO problem)
    {
        this.problem = problem;

        if (problem != null)
        {
            Collection<ArchiveActionResultVO> coll = problem.getArchiveActionResults();
            if (coll == null)
            {
                coll = new HashSet<ArchiveActionResultVO>();
                coll.add(this);

                problem.setArchiveActionResults(coll);
            }
        }
    }

    @JsonIgnore
    public ArchiveExecuteRequestVO getExecuteRequest()
    {
        return executeRequest;
    }

    public void setExecuteRequest(ArchiveExecuteRequestVO executeRequest)
    {
        this.executeRequest = executeRequest;

        if (executeRequest != null)
        {
            Collection<ArchiveActionResultVO> coll = executeRequest.getArchiveActionResults();
            if (coll == null)
            {
                coll = new HashSet<ArchiveActionResultVO>();
                coll.add(this);

                executeRequest.setArchiveActionResults(coll);
            }
        }
    }

    @JsonIgnore
    public ArchiveExecuteResultVO getExecuteResult()
    {
        return executeResult;
    }

    public void setExecuteResult(ArchiveExecuteResultVO executeResult)
    {
        this.executeResult = executeResult;

        // one-way only - no collections on the other side
    }

    public String getUAddress()
    {
        return this.UAddress;
    }

    public void setUAddress(String UAddress)
    {
        this.UAddress = UAddress;
    }

    public String getUEsbAddr()
    {
        return this.UEsbAddr;
    }

    public void setUEsbAddr(String UEsbAddr)
    {
        this.UEsbAddr = UEsbAddr;
    }
    
    public String getUTarget()
    {
        return UTarget;
    }

    public void setUTarget(String UTarget)
    {
        this.UTarget = UTarget;
    }

    public String getUTargetGUID()
    {
        return UTargetGUID;
    }

    public void setUTargetGUID(String UTargetGUID)
    {
        this.UTargetGUID = UTargetGUID;
    }

    public String getUWiki()
    {
        return UWiki;
    }

    public void setUWiki(String uWiki)
    {
        UWiki = uWiki;
    }

    public String getUNodeId()
    {
        return UNodeId;
    }

    public void setUNodeId(String uNodeId)
    {
        UNodeId = uNodeId;
    }

    public Boolean getUHidden()
    {
        return UHidden;
    }

    public void setUHidden(Boolean uHidden)
    {
        UHidden = uHidden;
    }

    public ArchiveActionResultLobVO getActionResultLob()
    {
        return this.actionResultLob;
    }

    public void setActionResultLob(ArchiveActionResultLobVO actionResultLob)
    {
        this.actionResultLob = actionResultLob;

    }

    public String getUProcessId()
    {
        return UProcessId;
    }

    public void setUProcessId(String uProcessId)
    {
        UProcessId = uProcessId;
    }

    public String getUProcessNumber()
    {
        return UProcessNumber;
    }

    public void setUProcessNumber(String uProcessNumber)
    {
        UProcessNumber = uProcessNumber;
    }

    public String getUTaskId()
    {
        UTaskId = null;
        if(task != null)
        {
            UTaskId = task.getSys_id();
        }
        return UTaskId;
    }

    public String getUTaskName()
    {
        UTaskName = null;
        if(task != null)
        {
            UTaskName = task.getUName();
        }
        return UTaskName;
    }

    public String getUSummary()
    {
        USummary = null;
        if(actionResultLob != null)
        {
            USummary = actionResultLob.getUSummary();
        }
        return USummary;
    }
} // ArchiveActionResult

