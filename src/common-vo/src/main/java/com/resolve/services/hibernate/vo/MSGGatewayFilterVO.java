package com.resolve.services.hibernate.vo;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class MSGGatewayFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 1L;
    
    private static final int MSG_NAME_NOT_AVAILABLE = -3;
    private static final String MSG_NAME_NOT_AVAILABLE_ERR = "Queue/Topic Name is not available.";

    public MSGGatewayFilterVO() {
    }

    private Boolean UDeployed;
    private Boolean UUpdated;
    private String UGatewayName;
    private String UUsername;
    private String UPassword;
    private String USsl;

    private Map<String, String> attrs;

    @MappingAnnotation(columnName="DEPLOYED")
    public Boolean getUDeployed()
    {
        return UDeployed;
    }

    public void setUDeployed(Boolean uDeployed)
    {
        UDeployed = uDeployed;
    }
    
    @MappingAnnotation(columnName="UPDATED")
    public Boolean getUUpdated()
    {
        return UUpdated;
    }

    public void setUUpdated(Boolean uUpdated)
    {
        UUpdated = uUpdated;
    }

    @MappingAnnotation(columnName="GATEWAY_NAME")
    public String getUGatewayName()
    {
        return UGatewayName;
    }

    public void setUGatewayName(String uGatewayName)
    {
        UGatewayName = uGatewayName;
    }
    
    @MappingAnnotation(columnName = "USER")
    public String getUUsername() {
        return this.UUsername;
    }

    public void setUUsername(String uUsername) {
        this.UUsername = uUsername;
    }

    @MappingAnnotation(columnName = "PASS")
    public String getUPassword() {
        return this.UPassword;
    }

    public void setUPassword(String uPassword) {
        this.UPassword = uPassword;
    }

    @MappingAnnotation(columnName = "SSL")
    public String getUSsl() {
        return this.USsl;
    }

    public void setUSsl(String uSsl) {
        this.USsl = uSsl;
    }

    public Map<String, String> getAttrs() {
        return this.attrs;
    }

    public void setAttrs(Map<String, String> attrs) {
        this.attrs = attrs;
    }
    
    public Map<String, String> toMap() {
        
        Map<String, String> attrs = getAttrs();
        
        if(attrs == null)
            attrs = new HashMap<String, String>();

        attrs.put("id", this.getSys_id());

        Method[] methods = this.getClass().getMethods();
        
        for(Method method:methods) {
            String name = method.getName();
            if(method.getAnnotation(MappingAnnotation.class) != null) {
                try {
                    String attr = name.substring(3).toLowerCase();
                    Object value = method.invoke(this, (Object[])null);
                    
                    if(attr.equals("uupdated"))
                        attr = "changed";
                    
                    if(value != null)
                        attrs.put(attr, value.toString());
                } catch(Exception e) {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }

        return attrs;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;

        MSGGatewayFilterVO other = (MSGGatewayFilterVO) obj;
        
        if(!super.equals(other))
            return false;
            
        if (UUsername == null) {
            if (StringUtils.isNotBlank(other.UUsername)) 
                return false;
        }

        else if (!UUsername.trim().equals(other.UUsername == null ? "" : other.UUsername.trim())) 
            return false;
        
        if (UPassword == null) {
            if (StringUtils.isNotBlank(other.UPassword)) 
                return false;
        }

        else if (!UPassword.trim().equals(other.UPassword == null ? "" : other.UPassword.trim())) 
            return false;
        
        if (USsl == null) {
            if (other.USsl != null) 
                return false;
        }

        else if (!USsl.equals(other.USsl)) 
            return false;
        
        Map<String, String> otherAttrs = other.getAttrs();
        
        for(Iterator<String> it=attrs.keySet().iterator();it.hasNext();) {
            String key = it.next();
            String value = attrs.get(key);
            String otherValue = otherAttrs.get(key);
            
            if (value == null) {
                if (otherValue != null) 
                    return false;
            }

            else if (!value.equals(otherValue)) 
                return false;
        }
        
        return true;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UGatewayName == null) ? 0 : UGatewayName.hashCode());
        result = prime * result + ((UUsername == null) ? 0 : UUsername.hashCode());
        result = prime * result + ((UPassword == null) ? 0 : UPassword.hashCode());
        result = prime * result + ((USsl == null) ? 0 : USsl.hashCode());
        return result;
    }
}