package com.resolve.services.hibernate.vo;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import com.resolve.services.interfaces.VO;

public class SIRPhaseMetaDataVO extends VO {
    
    private static final long serialVersionUID = 173127907255935663L;
    
    private String name;
    private String source;
    
    public SIRPhaseMetaDataVO() {}
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getSource() {
        return source;
    }
    
    public void setSource(String source) {
        this.source = source;
    }
    
    @Override
    public String toString() {
        return "SIR Phase Meta Data VO [" +
               (isNotBlank(getSys_id()) ? "Sys Id: " + getSys_id() + ", " : "") + 
               "Sys Org: " + (isNotBlank(getSysOrg()) ? getSysOrg() : "") + 
               ", Name: " + name + ", Source: " + source + "]";
    }
}
