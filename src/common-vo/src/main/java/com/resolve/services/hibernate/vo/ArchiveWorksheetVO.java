/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;

import com.resolve.services.interfaces.VO;

public class ArchiveWorksheetVO extends VO
{
    private static final long serialVersionUID = 4107197607502741519L;
    
    private String UNumber;
    private String USummary;
    private String UDescription;
    private String UWorkNotes;
    private String UCorrelationId;
    private String UWorksheet;
    private String UReference;
    private String UAlertId;
    private String UDebug;
    private String UCondition;
    private String USeverity;
    private String UAssignedTo;
    private String UAssignedToName;
    private String sirId;
    
    // object referenced by
	private Collection<ArchiveProcessRequestVO> resolveProcessRequests;
	private Collection<ArchiveExecuteRequestVO> resolveExecuteRequests;
	private Collection<ArchiveActionResultVO> resolveActionResults;
    
    public ArchiveWorksheetVO()
    {
    }
    
    public ArchiveWorksheetVO usetsys_id(String sys_id)
    {
        this.setSys_id(sys_id);
        return this;
    }

    public String getUNumber()
    {
        return this.UNumber;
    }

    public void setUNumber(String UNumber)
    {
        this.UNumber = UNumber;
    }

    public String getUSummary()
    {
        return this.USummary;
    }

    public void setUSummary(String USummary)
    {
        this.USummary = USummary;
    }

    public String getUCondition()
    {
        return this.UCondition;
    }

    public void setUCondition(String UCondition)
    {
        this.UCondition = UCondition;
    }

    public String getUSeverity()
    {
        return this.USeverity;
    }

    public void setUSeverity(String USeverity)
    {
        this.USeverity = USeverity;
    }

    public String getUDescription()
    {
        return this.UDescription;
    }

    public void setUDescription(String UDescription)
    {
        this.UDescription = UDescription;
    }

    public String getUWorkNotes()
    {
        return this.UWorkNotes;
    }

    public void setUWorkNotes(String UWorkNotes)
    {
        this.UWorkNotes = UWorkNotes;
    }

    public String getUCorrelationId()
    {
        return this.UCorrelationId;
    }

    public void setUCorrelationId(String UCorrelationId)
    {
        this.UCorrelationId = UCorrelationId;
    }

    public String getUWorksheet()
    {
        return this.UWorksheet;
    }

    public void setUWorksheet(String UWorksheet)
    {
        this.UWorksheet = UWorksheet;
    }

    public String getUReference()
    {
        return this.UReference;
    }

    public void setUReference(String UReference)
    {
        this.UReference = UReference;
    }

    public String getUAlertId()
    {
        return this.UAlertId;
    }

    public void setUAlertId(String UAlertId)
    {
        this.UAlertId = UAlertId;
    }

    public String getUDebug()
    {
        return this.UDebug;
    }

    public void setUDebug(String UDebug)
    {
        this.UDebug = UDebug;
    }

    public String getUAssignedTo()
    {
        return UAssignedTo;
    }

    public void setUAssignedTo(String uAssignedTo)
    {
        UAssignedTo = uAssignedTo;
    }

    public String getUAssignedToName()
    {
        return UAssignedToName;
    }

    public void setUAssignedToName(String uAssignedToName)
    {
        UAssignedToName = uAssignedToName;
    }
    
    public String getSirId()
	{
		return sirId;
	}
	public void setSirId(String sirId)
	{
		this.sirId = sirId;
	}

	public Collection<ArchiveProcessRequestVO> getArchiveProcessRequests()
	{
		return resolveProcessRequests;
	}

	public void setArchiveProcessRequests(Collection<ArchiveProcessRequestVO> resolveProcessRequests)
	{
		this.resolveProcessRequests = resolveProcessRequests;
	}

	public Collection<ArchiveExecuteRequestVO> getArchiveExecuteRequests()
	{
		return resolveExecuteRequests;
	}

	public void setArchiveExecuteRequests(Collection<ArchiveExecuteRequestVO> resolveExecuteRequests)
	{
		this.resolveExecuteRequests = resolveExecuteRequests;
	}

	public Collection<ArchiveActionResultVO> getArchiveActionResults()
	{
		return resolveActionResults;
	}

	public void setArchiveActionResults(Collection<ArchiveActionResultVO> resolveActionResults)
	{
		this.resolveActionResults = resolveActionResults;
	}

} // Worksheet
