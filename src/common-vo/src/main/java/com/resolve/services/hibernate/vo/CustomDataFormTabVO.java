/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

public class CustomDataFormTabVO extends VO implements Comparable<CustomDataFormTabVO>
{
    private static final long serialVersionUID = 5637145074853044342L;
    
    private String UName;
    private Boolean UIsActive;
    private Integer UOrder;
    
    // Referenced Objects
    private WikiDocumentVO baseWiki;
    
    // Referencing Objects
    private SIRConfigVO sirConfig;
        
    public CustomDataFormTabVO()
    {
    }
    
    public CustomDataFormTabVO(boolean defaultValues)
    {
        super(defaultValues);
    }
    
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    public Boolean getUIsActive()
    {
        Boolean result = UIsActive;
        
        if (result == null)
        {
            result = Boolean.FALSE;
        }
        
        return result;
    }
    
    public void setUIsActive(Boolean UIsActive)
    {
        this.UIsActive = UIsActive;
    }
    
    public Integer getUOrder()
    {
        Integer result = UOrder;
        
        if (result == null)
        {
            result = VO.INTEGER_DEFAULT;
        }
        
        return result;
    }
    
    public void setUOrder(Integer UOrder)
    {
        this.UOrder = UOrder;
    }
    
    public WikiDocumentVO getBaseWiki()
    {
        return baseWiki;
    }

    public void setBaseWiki(WikiDocumentVO baseWiki)
    {
        this.baseWiki = baseWiki;
    }
    
    public SIRConfigVO getSirConfig()
    {
        return sirConfig;
    }
    
    public void setSirConfig(SIRConfigVO sirConfig)
    {
        this.sirConfig = sirConfig;
        
//        if (sirConfig != null)
//        {
//            Collection<CustomDataFormTabVO> customDataFormTabVOs = sirConfig.getCustomDataFormTabs();
//            
//            if (customDataFormTabVOs == null)
//            {
//                customDataFormTabVOs = new HashSet<CustomDataFormTabVO>();
//            }
//            
//            customDataFormTabVOs.add(this);
//            sirConfig.setCustomDataFormTabs(customDataFormTabVOs);
//        }
    }
    
    public boolean validate()
    {
        boolean valid = true;
        
        if(StringUtils.isBlank(getUName()) || (StringUtils.isNotBlank(getUName()) && getUName().equalsIgnoreCase(VO.STRING_DEFAULT)))
        {
            valid = false;
            throw new RuntimeException("Custom Data Form Tab Name cannot be null, blank or " + VO.STRING_DEFAULT + "(case insensitive).");
        }
        
        return valid;
    }

    @Override
    public String toString()
    {
        return "Name: " + getUName() + ", Is Active: " + getUIsActive() + ", Base Wiki: " + 
               (getBaseWiki() != null ? getBaseWiki().getUFullname() : "") +
               ", Order: " + getUOrder().intValue() +  ", Org: " + getSysOrg();
    } // toString

    @Override
    public int compareTo(CustomDataFormTabVO that)
    {
        if (this != that)
        {
            return this.getUOrder().compareTo(that.getUOrder());
        }
        
        return 0;
    }
    
    @Override
    public boolean equals(Object otherObj)
    {
        boolean isEquals = false;
        
        if (otherObj instanceof CustomDataFormTabVO)
        {
            CustomDataFormTabVO otherCustomDataFormTabVO = (CustomDataFormTabVO)otherObj;
            
            if (otherCustomDataFormTabVO != this)
            {
                if (((otherCustomDataFormTabVO.getSysOrg() == null && this.getSysOrg() == null) ||
                     (otherCustomDataFormTabVO.getSysOrg() != null && this.getSysOrg() != null &&
                      otherCustomDataFormTabVO.getSysOrg().equals(this.getSysOrg()))) &&
                    (otherCustomDataFormTabVO.getUName().equals(this.getUName())))
                {
                    isEquals = true;
                }
            }
            else
                isEquals = true; 
        }
        
        return isEquals;
    }
    
    @Override
    public int hashCode()
    {
        int hash = 1;
        hash = hash * 31 + (this.getSysOrg() == null ? 0 : this.getSysOrg().hashCode());
        hash = hash * 31 + (this.getUName() == null ? 0 : this.getUName().hashCode());
        return hash;
    }
}
