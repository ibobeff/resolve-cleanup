/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class RecentItemVO extends VO
{
    private static final long serialVersionUID = 4733609828202409539L;

    private String itemSysId;
    
    private String itemType;
    
    private String name;
    
    private String namespace;
    
	public String getItemSysId() {
		return itemSysId;
	}
	
	public void setItemSysId(String itemSysId) {
		this.itemSysId = itemSysId;
	}
	
	public String getItemType() {
		return itemType;
	}
	
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getNamespace() {
		return namespace;
	}
	
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
}
