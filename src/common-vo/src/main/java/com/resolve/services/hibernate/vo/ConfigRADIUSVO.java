/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;


import com.resolve.services.interfaces.VO;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;

public class ConfigRADIUSVO extends VO
{
    private static final long serialVersionUID = -8106259688696420517L;
    
    private Boolean UActive;
    private String UPrimaryHost;
    private String USecondaryHost;
    private String USharedSecret;
    private String UAuthProtocol;
    private Integer UAuthPort;
    private Integer UAcctPort;
    private String UDefaultDomain;    
    private Boolean UFallback;
    private String UDomain;// this is mandatory from the UI and should be unique
    private Boolean UIsDefault;
    private String UGateway;
    
    //RADIUS domain conf belongs to this organization in Resolve
    private OrganizationVO belongsToOrganization;

    public ConfigRADIUSVO()
    {
    }
    
    public Integer getUAuthPort()
    {
        return UAuthPort;
    }

    public void setUAuthPort(Integer uAuthPort)
    {
        UAuthPort = uAuthPort;
    }
    
    public Integer getUAcctPort()
    {
        return UAcctPort;
    }

    public void setUAcctPort(Integer uAcctPort)
    {
        UAcctPort = uAcctPort;
    }
    
    public Boolean getUActive()
    {
        return UActive;
    }
    
    public Boolean ugetUActive()
    {
        Boolean result = false;
        if(getUActive() != null)
        {
            result = getUActive();
        }
        
        return result;
    }

    public void setUActive(Boolean uActive)
    {
        UActive = uActive;
    }

    public String getUDomain()
    {
        return UDomain;
    }

    public void setUDomain(String uDomain)
    {
        UDomain = uDomain;
    }

    public String getUPrimaryHost()
    {
        return UPrimaryHost;
    }

    public void setUPrimaryHost(String uPrimaryHost)
    {
        UPrimaryHost = uPrimaryHost;
    }
    
    public String getUSecondaryHost()
    {
        return USecondaryHost;
    }

    public void setUSecondaryHost(String uSecondaryHost)
    {
        USecondaryHost = uSecondaryHost;
    }
    
    public String getUSharedSecret()
    {
        String decryptedSharedSecret = "";
        
        try
        {
            decryptedSharedSecret = CryptUtils.decrypt(USharedSecret);
        }
        catch (Exception e)
        {
            Log.log.error("Error " + e.getMessage() + " in decrypting shared secret.", e);
        }
        
        return decryptedSharedSecret;
        //return USharedSecret;
    }

    public void setUSharedSecret(String uSharedSecret)
    {
        /*
        try
        {
            USharedSecret = CryptUtils.encrypt(uSharedSecret);
        }
        catch (Exception e)
        {
            Log.log.error("Error " + e.getMessage() + " in encrypting shared secret.", e);
        }*/
        USharedSecret = uSharedSecret;
    }
    
    public String getUAuthProtocol()
    {
        return UAuthProtocol;
    }

    public void setUAuthProtocol(String uAuthProtocol)
    {
        UAuthProtocol = uAuthProtocol;
    }
    
    public Boolean getUFallback()
    {
        return UFallback;
    }
    
    public Boolean ugetUFallback()
    {
        Boolean result = true;
        if(getUFallback() != null)
        {
            result = getUFallback();
        }
        
        return result;
    }

    public void setUFallback(Boolean uFallback)
    {
        UFallback = uFallback;
    }
    
    public String getUDefaultDomain()
    {
        return UDefaultDomain;
    }

    public void setUDefaultDomain(String uDefaultDomain)
    {
        UDefaultDomain = uDefaultDomain;
    }
    
    public OrganizationVO getBelongsToOrganization()
    {
        return belongsToOrganization;
    }

    public void setBelongsToOrganization(OrganizationVO belongsToOrganization)
    {
        this.belongsToOrganization = belongsToOrganization;
    }

    public Boolean getUIsDefault()
    {
        return UIsDefault;
    }

    public void setUIsDefault(Boolean uIsDefault)
    {
        UIsDefault = uIsDefault;
    }

    public Boolean ugetUIsDefault()
    {
        Boolean result = false;
        if(getUIsDefault() != null)
        {
            result = getUIsDefault();
        }
        
        return result;
    };
    
    public String getUGateway()
    {
        return UGateway;
    }

    public void setUGateway(String uGateway)
    {
        UGateway = uGateway;
    }
    
    public String toString()
    {
        return "ConfigRADIUSVO: Active = " + getUActive() + ", Primary Host = " + getUPrimaryHost() + ", Secondary Host = " + getUSecondaryHost() +
               ", Shared Secret = " + getUSharedSecret() + ", Authentication Protocol = " + getUAuthProtocol() + 
               ", Authentication Port # = " + getUAuthPort() + ", Accounting Port # = " + getUAcctPort() +
               ", Fall Back = " + getUFallback() + ", Domain = " + getUDomain() + ", Gateway = " + getUGateway();
    }
}
