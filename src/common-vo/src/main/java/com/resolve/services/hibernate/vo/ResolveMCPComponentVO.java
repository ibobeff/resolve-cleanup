package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveMCPComponentVO extends VO
{
    private static final long serialVersionUID = -4821367663293033101L;
    
    // Name of the component. RSView, RSRemote etc.
    private String UComponentName;
    // Component GUID
    private String UGuid;
    
    // The host to which this component belongs
    private ResolveMCPHostVO UComponentHostVO;
    
    

    public String getUComponentName()
    {
        return UComponentName;
    }

    public void setUComponentName(String uComponentName)
    {
        UComponentName = uComponentName;
    }

    public String getUGuid()
    {
        return UGuid;
    }

    public void setUGuid(String uGuid)
    {
        UGuid = uGuid;
    }

    public ResolveMCPHostVO getUComponentHostVO()
    {
        return UComponentHostVO;
    }

    public void setUComponentHostVO(ResolveMCPHostVO uComponentHost)
    {
        UComponentHostVO = uComponentHost;
    }
}
