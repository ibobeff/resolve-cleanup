/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveAppRoleRelVO extends VO
{
    private static final long serialVersionUID = -5533630550090484156L;
    
    private ResolveAppsVO resolveApp;
    private RolesVO roles;
    
    public ResolveAppRoleRelVO()
    {
    }

    public ResolveAppsVO getResolveApp()
    {
        return resolveApp;
    }

    public void setResolveApp(ResolveAppsVO resolveApp)
    {
        this.resolveApp = resolveApp;
        
    }

    public RolesVO getRoles()
    {
        return roles;
    }

    public void setRoles(RolesVO roles)
    {
        this.roles = roles;
    }

} // ResolveControllersVO
