/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class TIBCOBespokeFilterVO extends GatewayFilterVO
{
    private static final long serialVersionUID = 7126449098772818493L;

    private static final int REPLY_AND_CERTIFIED_ERRORCODE = -1;
    private static final int MISSING_BUS_URI_ERRORCODE = -2;
    private static final int NAME_TOO_LONG_ERRORCODE = -3;
    private static final int MISSING_TOPIC_ERRORCODE = -4;
    private static final String REPLY_AND_CERTIFIED_ERRORMESSAGE = "Filter cannot use both 'Save Message for Reply' and 'Certified Messaging'";
    private static final String MISSING_BUS_URI_ERRORMESSAGE = "Filter is missing 'BusUri'";
    private static final String NAME_TOO_LONG_ERRORMESSAGE = "Filter name exceeds 40 characters";
    private static final String MISSING_TOPIC_ERRORMESSAGE = "Filter topic is missing";
    
    private String URegex;
    private String UBusUri;
    private String UTopic;
    private Boolean UCertifiedMessaging;
    private String UXmlQuery;
    private Boolean UUnescapeXml;
    private Boolean UProcessAsXml;
    private Boolean URequireReply;
    private Integer UReplyTimeout;

    public TIBCOBespokeFilterVO()
    {
        super();
    }

    @MappingAnnotation(columnName = "REGEX")
    public String getURegex()
    {
        return this.URegex;
    }
    
    @MappingAnnotation(columnName = "BUS_URI")
    public String getUBusUri()
    {
        return this.UBusUri;
    }
    
    @MappingAnnotation(columnName = "TOPIC")
    public String getUTopic()
    {
        return this.UTopic;
    }
    
    @MappingAnnotation(columnName = "CERTIFIED_MESSAGING")
    public Boolean getUCertifiedMessaging()
    {
        return this.UCertifiedMessaging;
    }
    
    @MappingAnnotation(columnName = "XML_QUERY")
    public String getUXmlQuery()
    {
        return this.UXmlQuery;
    }
    
    @MappingAnnotation(columnName = "PROCESS_AS_XML")
    public Boolean getUProcessAsXml()
    {
        return this.UProcessAsXml;
    }
    
    @MappingAnnotation(columnName = "UNESCAPE_XML")
    public Boolean getUUnescapeXml()
    {
        return this.UUnescapeXml;
    }
    
    @MappingAnnotation(columnName = "REQUIRE_REPLY")
    public Boolean getURequireReply()
    {
        return this.URequireReply;
    }
    
    @MappingAnnotation(columnName = "REPLY_TIMEOUT")
    public Integer getUReplyTimeout()
    {
        return this.UReplyTimeout;
    }
    
    public void setURegex(String uRegex)
    {
        if (StringUtils.isNotBlank(uRegex) || URegex.equals(VO.STRING_DEFAULT))
        {
            this.URegex = uRegex != null ? uRegex.trim() : uRegex;
        }
    }    
    
    public void setUBusUri(String uBusUri)
    {
        if (StringUtils.isNotBlank(uBusUri) || UBusUri.equals(VO.STRING_DEFAULT))
        {
            this.UBusUri = uBusUri != null ? uBusUri.trim() : uBusUri;
        }
    }
    
    public void setUTopic(String uTopic)
    {
        if (StringUtils.isNotBlank(uTopic) || UTopic.equals(VO.STRING_DEFAULT))
        {
            this.UTopic = uTopic != null ? uTopic.trim() : uTopic;
        }
    }
    
    public void setUCertifiedMessaging(Boolean uCertifiedMessaging)
    {
        this.UCertifiedMessaging = uCertifiedMessaging;
    }
    
    public void setUXmlQuery(String uXmlQuery)
    {
        if (StringUtils.isNotBlank(uXmlQuery) || UXmlQuery.equals(VO.STRING_DEFAULT))
        {
            this.UXmlQuery = uXmlQuery != null ? uXmlQuery.trim() : uXmlQuery;
        }
    }
    
    public void setUProcessAsXml(Boolean uProcessAsXml)
    {
        this.UProcessAsXml = uProcessAsXml;
    }
    
    public void setUUnescapeXml(Boolean uUnescapeXml)
    {
        this.UUnescapeXml = uUnescapeXml;
    }
    
    public void setURequireReply(Boolean uRequireReply)
    {
        this.URequireReply = uRequireReply;
    }
    
    public void setUReplyTimeout(Integer uReplyTimeout)
    {
        this.UReplyTimeout = uReplyTimeout;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((URegex == null) ? 0 : URegex.hashCode());
        result = prime * result + ((UBusUri == null) ? 0 : UBusUri.hashCode());
        result = prime * result + ((UTopic == null) ? 0 : UTopic.hashCode());
        result = prime * result + ((UCertifiedMessaging == null) ? 0 : UCertifiedMessaging.hashCode());
        result = prime * result + ((UXmlQuery == null) ? 0 : UXmlQuery.hashCode());
        result = prime * result + ((UProcessAsXml == null) ? 0 : UProcessAsXml.hashCode());
        result = prime * result + ((UUnescapeXml == null) ? 0 : UUnescapeXml.hashCode());
        result = prime * result + ((URequireReply == null) ? 0 : URequireReply.hashCode());
        result = prime * result + ((UReplyTimeout == null) ? 0 : UReplyTimeout.hashCode());
        return result;
    }
    
    @Override
    public int isValid()
    {
        if(URequireReply != null && UCertifiedMessaging != null && URequireReply && UCertifiedMessaging)
        {
            return REPLY_AND_CERTIFIED_ERRORCODE;
        }
        if(UBusUri == null || UBusUri.equals(""))
        {
            return MISSING_BUS_URI_ERRORCODE;
        }
        if(getUName() != null && getUName().length() > 40)
        {
            return NAME_TOO_LONG_ERRORCODE;
        }
        if(StringUtils.isEmpty(UTopic))
        {
            return MISSING_TOPIC_ERRORCODE;
        }
        
        return 0;
    }
    
    @Override
    public String getValidationError(int errorCode)
    {   
        switch(errorCode)
        {
            case REPLY_AND_CERTIFIED_ERRORCODE:
                return REPLY_AND_CERTIFIED_ERRORMESSAGE;
            case MISSING_BUS_URI_ERRORCODE:
                return MISSING_BUS_URI_ERRORMESSAGE;
            case NAME_TOO_LONG_ERRORCODE:
                return NAME_TOO_LONG_ERRORMESSAGE;
            case MISSING_TOPIC_ERRORCODE:
                return MISSING_TOPIC_ERRORMESSAGE;
                
            default:
                return "Error code not found.";
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        TIBCOBespokeFilterVO other = (TIBCOBespokeFilterVO) obj;
        
        if (URegex == null)
        {
            if (StringUtils.isNotBlank(other.URegex)) return false;
        }
        else if (!URegex.trim().equals(other.URegex == null ? "" : other.URegex.trim())) return false;
        
        if (UBusUri == null)
        {
            if (StringUtils.isNotBlank(other.UBusUri)) return false;
        }
        else if (!UBusUri.trim().equals(other.UBusUri == null ? "" : other.UBusUri.trim())) return false;
        
        if (UTopic == null)
        {
            if (StringUtils.isNotBlank(other.UTopic)) return false;
        }
        else if (!UTopic.trim().equals(other.UTopic == null ? "" : other.UTopic.trim())) return false;      
 
        if (UCertifiedMessaging == null)
        {
            if (other.UCertifiedMessaging != null) return false;
        }
        else if(!UCertifiedMessaging.equals(other.UCertifiedMessaging)) return false;
        
        if (UXmlQuery == null)
        {
            if(StringUtils.isNotBlank(other.UXmlQuery)) return false;
        }else if(!UXmlQuery.trim().equals(other.UXmlQuery == null ? "" : other.UXmlQuery.trim())) return false;
        
        if (UProcessAsXml == null)
        {
            if (other.UProcessAsXml != null) return false;
        }
        else if(!UProcessAsXml.equals(other.UProcessAsXml)) return false;
        
        if (UUnescapeXml == null)
        {
            if (other.UUnescapeXml != null) return false;
        }
        else if(!UUnescapeXml.equals(other.UUnescapeXml)) return false;
        
        if (URequireReply == null)
        {
            if (other.URequireReply != null) return false;
        }
        else if(!URequireReply.equals(other.URequireReply)) return false;
        
        if (UReplyTimeout == null)
        {
            if (other.UReplyTimeout != null) return false;
        }
        else if(!UReplyTimeout.equals(other.UReplyTimeout)) return false;
        
        return true;        
    }
}
