package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class CustomDictionaryItemVO extends VO {
	private static final long serialVersionUID = -4816563807626730123L;

	private String UShortName;
	private String UFullName;
	private String UDescription;

	public String getUShortName() {
		return UShortName;
	}
	public void setUShortName(String uShortName) {
		UShortName = uShortName;
	}
	public String getUFullName() {
		return UFullName;
	}
	public void setUFullName(String uFullName) {
		UFullName = uFullName;
	}
	public String getUDescription() {
		return UDescription;
	}
	public void setUDescription(String uDescription) {
		UDescription = uDescription;
	}
}
