/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;

public class WikidocStatisticsVO  extends VO
{
    private static final long serialVersionUID = -5464595438646353383L;
    
    private String UContentChecksum;
	private String UModelChecksum;
	private String UExceptionChecksum;
	private String UFinalChecksum;
	private Integer UViewCount;
	private Integer UEditCount;
	private Integer UExecuteCount;
	
	private Integer UReviewCount;
	private Integer UUsefulNoCount;
	private Integer UUsefulYesCount;

    // object references
	private WikiDocumentVO wikidoc;
	
    // object referenced by
	
	public WikidocStatisticsVO()
	{
	}


    public WikiDocumentVO getWikidoc()
    {
        return wikidoc;
    }

    public void setWikidoc(WikiDocumentVO wikidoc)
    {
        this.wikidoc = wikidoc;

        if (wikidoc != null)
        {
            Collection<WikidocStatisticsVO> coll = wikidoc.getWikidocStatistics();
            if (coll == null)
            {
				coll = new HashSet<WikidocStatisticsVO>();
	            coll.add(this);
	            
                wikidoc.setWikidocStatistics(coll);
            }
        }
    }
	public String getUContentChecksum()
	{
		return this.UContentChecksum;
	}

	public void setUContentChecksum(String UContentChecksum)
	{
		this.UContentChecksum = UContentChecksum;
	}

	public String getUModelChecksum()
	{
		return this.UModelChecksum;
	}

	public void setUModelChecksum(String UModelChecksum)
	{
		this.UModelChecksum = UModelChecksum;
	}

	public String getUExceptionChecksum()
	{
		return this.UExceptionChecksum;
	}

	public void setUExceptionChecksum(String UExceptionChecksum)
	{
		this.UExceptionChecksum = UExceptionChecksum;
	}

	public String getUFinalChecksum()
	{
		return this.UFinalChecksum;
	}

	public void setUFinalChecksum(String UFinalChecksum)
	{
		this.UFinalChecksum = UFinalChecksum;
	}

	public Integer getUViewCount()
	{
		return this.UViewCount;
	}

	public void setUViewCount(Integer UViewCount)
	{
		this.UViewCount = UViewCount;
	}

	public Integer getUEditCount()
	{
		return this.UEditCount;
	}

	public void setUEditCount(Integer UEditCount)
	{
		this.UEditCount = UEditCount;
	}

	public Integer getUExecuteCount()
	{
		return this.UExecuteCount;
	}

	public void setUExecuteCount(Integer UExecuteCount)
	{
		this.UExecuteCount = UExecuteCount;
	}

    public Integer getUReviewCount()
    {
        return UReviewCount;
    }

    public void setUReviewCount(Integer uReviewCount)
    {
        UReviewCount = uReviewCount;
    }

    public Integer getUUsefulNoCount()
    {
        return UUsefulNoCount;
    }

    public void setUUsefulNoCount(Integer uUsefulNoCount)
    {
        UUsefulNoCount = uUsefulNoCount;
    }

    public Integer getUUsefulYesCount()
    {
        return UUsefulYesCount;
    }

    public void setUUsefulYesCount(Integer uUsefulYesCount)
    {
        UUsefulYesCount = uUsefulYesCount;
    }
	
	@Override
	public String toString()
	{
	    return " Statistics : View Count = " + UViewCount.intValue() +
	           ", Edit Count = " + UEditCount.intValue() + ", Execute Count = " + UExecuteCount + 
	           ", Review Count = " + UReviewCount.intValue() + ", Found Useful No Count = " + 
	           UUsefulNoCount.intValue() + ", Found Useful Yes Count = " + UUsefulYesCount.intValue();
	}

}
