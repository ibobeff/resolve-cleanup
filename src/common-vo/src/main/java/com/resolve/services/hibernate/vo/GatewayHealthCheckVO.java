/******************************************************************************
* (C) Copyright 2019
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Map;
import java.util.TimeZone;


import org.apache.commons.collections.MapUtils;

import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

public class GatewayHealthCheckVO extends VO {
	
	private static final long serialVersionUID = 4171672512682667353L;
    
	private String orgName;
    private String remoteName; 	// Container (RSRemote) GUID-NAME-GROUP
    private String gtwType;
    private String queueName;	// Composite queue name suffixed with Org (if set)
    private String report;		// Health Check Report JSON
    private boolean stale;		// Report is stale i.e. last update time is more than 2 times the health check interval

    public GatewayHealthCheckVO() {
    	super();
    }
    
    public GatewayHealthCheckVO(boolean defaultValues) {
    	super(defaultValues);
    }
    
    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getRemoteName() {
        return remoteName;
    }

    public void setRemoteName(String remoteName) {
        this.remoteName = remoteName;
    }

    public String getGtwType() {
        return gtwType;
    }

    public void setGtwType(String gtwType) {
        this.gtwType = gtwType;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public String getReport() {
        return report;
    }

	public Map<String, String> getReportAsMap() {
		Map<String, String> reportAsMap = Collections.emptyMap();
		
		if (StringUtils.isNotBlank(report)) {
			reportAsMap = StringUtils.jsonObjectToMap((StringUtils.stringToJSONObject(report)));
		}
		
		return reportAsMap;
	}
	
    public void setReport(String report)
    {
        this.report = report;
    }
    
    public void mapToReport(Map<String, String> reportAsMap) {
    	if (MapUtils.isNotEmpty(reportAsMap)) {
    		setReport(StringUtils.mapToJson(reportAsMap));
    	}
    }
    
    public boolean getStale() {
    	return stale;
    }
    
    public void setStale(boolean stale) {
    	this.stale = stale;
    }
    
    public void markStale(int healthCheckIntrvlInMin, long currentMilliSecs) {
    	if ((currentMilliSecs - getSysUpdatedOn().getTime()) >= (2 * healthCheckIntrvlInMin * 60 * 1000)) {
    		setStale(true);
    	} else {
    		setStale(false);
    	}
    }
    
    @Override
    public String toString() {
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
   	 	sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
   	 	
    	return String.format("[Org Id: %s, Created On: %s, Updated On: %s, Created By: %s, Updated By: %s, " +
    						 "Modification Count: %d, Org Name: %s, Remote Name: %s, Type: %s, Queue Name: %s," +
    						 "Report (JSON): %s %s]", getSysOrg(), sdf.format(getSysCreatedOn()), sdf.format(getSysUpdatedOn()), 
    						 getSysCreatedBy(), getSysUpdatedBy(), 
    						 (getSysModCount() != null ? getSysModCount().intValue() : 0), getOrgName(), 
    						 getRemoteName(), getGtwType(), getQueueName(), getReport(), (stale ? "is stale" : "is current"));
    }

} // ResolveRegistration
