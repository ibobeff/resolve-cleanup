/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;


public class MetaFormActionVO extends VO
{
    private static final long serialVersionUID = -4679184461109018685L;
    
    private String UOperation;//check the Operation.java for valid types
    private String UCrudAction;//for DB, mapped to CRUDAction.java
    private String URunbookName;//for EXECUTEPROCESS
    private String UScriptName;//for BUSINESSRULE
    private String UActionTaskName;
    private Integer USequence;//sequence # 
    private String UAdditionalParams;
    private String URedirectUrl;
    private String URedirectTarget;
    
    private MetaControlItemVO metaControlItem;
    
    public MetaFormActionVO()
    {
    }
       
    
    public String getUOperation()
    {
        return UOperation;
    }
    /**
     * @param uOperation the uOperation to set
     */
    public void setUOperation(String uOperation)
    {
        UOperation = uOperation;
    }
    
    public String getUCrudAction()
    {
        return UCrudAction;
    }
    /**
     * @param uCrudAction the uCrudAction to set
     */
    public void setUCrudAction(String uCrudAction)
    {
        UCrudAction = uCrudAction;
    }


    public String getURunbookName()
    {
        return URunbookName;
    }
    /**
     * @param uRunbookName the uRunbookName to set
     */
    public void setURunbookName(String uRunbookName)
    {
        URunbookName = uRunbookName;
    }

    public String getUScriptName()
    {
        return UScriptName;
    }
    /**
     * @param uScriptName the uScriptName to set
     */
    public void setUScriptName(String uScriptName)
    {
        UScriptName = uScriptName;
    }
    
    
    public String getUActionTaskName()
    {
        return UActionTaskName;
    }

    public void setUActionTaskName(String uActionTaskName)
    {
        UActionTaskName = uActionTaskName;
    }

    
    public Integer getUSequence()
    {
        return USequence;
    }
    /**
     * @param uSequence the uSequence to set
     */
    public void setUSequence(Integer uSequence)
    {
        USequence = uSequence;
    }

    public String getUAdditionalParams()
    {
        return UAdditionalParams;
    }


    public void setUAdditionalParams(String uAdditionalParams)
    {
        UAdditionalParams = uAdditionalParams;
    }
    
    public String getURedirectUrl()
    {
        return URedirectUrl;
    }

    public void setURedirectUrl(String uRedirectUrl)
    {
        URedirectUrl = uRedirectUrl;
    }


    public String getURedirectTarget()
    {
        return URedirectTarget;
    }


    public void setURedirectTarget(String uRedirectTarget)
    {
        URedirectTarget = uRedirectTarget;
    }


    public MetaControlItemVO getMetaControlItem()
    {
        return metaControlItem;
    }


    /**
     * @param metaControlItem the metaControlItem to set
     */
    public void setMetaControlItem(MetaControlItemVO metaControlItem)
    {
        this.metaControlItem = metaControlItem;
        
        if (metaControlItem != null)
        {
            Collection<MetaFormActionVO> coll = metaControlItem.getMetaFormActions();
            if (coll == null)
            {
                coll = new HashSet<MetaFormActionVO>();
                coll.add(this);
              
                metaControlItem.setMetaFormActions(coll);
            }
        }
    }
    
    
    

}
