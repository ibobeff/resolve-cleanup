/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.interfaces;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONNull;
import net.sf.json.JSONObject;

public abstract class VO implements java.io.Serializable
{
    private static final long serialVersionUID = 4683078489694259743L;

    //DEfault setting for all the VOs
    public static final String STRING_DEFAULT = "UNDEFINED";
    public static final Date DATE_DEFAULT = new Date(0);
    public static final Integer INTEGER_DEFAULT = Integer.MIN_VALUE;
    public static final Integer NON_NEGATIVE_INTEGER_DEFAULT = new Integer(0);
    public static final Integer POSITIVE_INTEGER_DEFAULT = new Integer(1);
    public static final Double DOUBLE_DEFAULT = Double.MIN_VALUE;
    public static final Long LONG_DEFAULT = Long.MIN_VALUE;
    public static final Long NON_NEGATIVE_LONG_DEFAULT = new Long(0);
    public static final Long POSITIVE_LONG_DEFAULT = new Long(1);
    public static final Float FLOAT_DEFAULT = Float.MIN_VALUE;
    public static final Short SHORT_DEFAULT = Short.MIN_VALUE;
    public static final Byte BYTE_DEFAULT = Byte.MIN_VALUE;
    public static final BigDecimal BIGDECIMAL_DEFAULT = new BigDecimal(DOUBLE_DEFAULT);
    public static final Boolean BOOLEAN_DEFAULT = Boolean.FALSE;
    public static final String STARS = "*****************";
    public static final String STAR = "*";
    
    public static final byte[] BYTE_ARRAY_DEFAULT = new byte[0];
    
    public static final Set<String> JAVA_PRIMITIVE_TYPES = new HashSet<String>();

    static 
    {
        JAVA_PRIMITIVE_TYPES.add("boolean");
        JAVA_PRIMITIVE_TYPES.add("byte");
        JAVA_PRIMITIVE_TYPES.add("short");
        JAVA_PRIMITIVE_TYPES.add("int");
        JAVA_PRIMITIVE_TYPES.add("long");
        JAVA_PRIMITIVE_TYPES.add("float");
        JAVA_PRIMITIVE_TYPES.add("double");
    }
    
    // sys fields
    private String id;//used by UI, by default same as sys_id
    private String sys_id;
    private Date sysCreatedOn;
    private String sysCreatedBy;
    private Date sysUpdatedOn;
    private String sysUpdatedBy;
    private Integer sysModCount;
    
    private String sysPerm;
    private String sysOrg;
    
    private Boolean sysIsDeleted;
    
    //properties used for display purpose 
    private String sysOrganizationName;
    private Integer checksum;
    
    public VO()
    {
        setDefaultValues();
    }
    
    public VO(boolean defaultValues)
    {
        if (defaultValues)
        {
            setDefaultValues();
        }
    }
    
    public VO(VO vo)
    {
        setId(vo.getId());
        setSys_id(vo.getSys_id());
        setSysCreatedOn(vo.getSysCreatedOn());
        setSysCreatedBy(vo.getSysCreatedBy());
        setSysUpdatedOn(vo.getSysUpdatedOn());
        setSysUpdatedBy(vo.getSysUpdatedBy());
        setSysModCount(vo.getSysModCount());
        setSysPerm(vo.getSysPerm());
        setSysOrg(vo.getSysOrg());
        setSysIsDeleted(vo.getSysIsDeleted());
        setSysOrganizationName(vo.getSysOrganizationName());
    }
    
    public static Object evaluateValue(Object val, Object defaultVal)
    {
        Object result = val;
        
        if(val != null)
        {
            Class<? extends Object> param = val.getClass();
            if(param.equals(String.class))
            {
                String valStr = (String) val;
                result = StringUtils.isNotBlank(valStr) && valStr.equals(STRING_DEFAULT) ? defaultVal : valStr;
            }
            else if(param.equals(Date.class))
            {
                Date valDate = (Date) val;
                result = valDate.equals(DATE_DEFAULT) ? defaultVal : valDate;
            }
            else if(param.equals(Integer.class))
            {
                Integer valInteger = (Integer) val;
                result = valInteger.equals(INTEGER_DEFAULT) ? defaultVal : valInteger;
            }
            else if(param.equals(Double.class))
            {
                Double valDouble = (Double) val;
                result = valDouble.equals(DOUBLE_DEFAULT) ? defaultVal : valDouble;
            }
            else if(param.equals(Long.class))
            {
                Long valLong = (Long) val;
                result = valLong.equals(LONG_DEFAULT) ? defaultVal : valLong;
            }
            else if(param.equals(Float.class))
            {
                Float valFloat = (Float) val;
                result = valFloat.equals(FLOAT_DEFAULT) ? defaultVal : valFloat;
            }
            else if(param.equals(BigDecimal.class))
            {
                BigDecimal valBD = (BigDecimal) val;
                result = valBD.equals(BIGDECIMAL_DEFAULT) ? defaultVal : valBD;
            }
            else if(param.equals(byte[].class))
            {
                byte[] valByte = (byte[]) val;
                result = valByte.equals(BYTE_ARRAY_DEFAULT) ? defaultVal : valByte;
            }
        }
        
        return result;
    }
    
    /**
     * Override this api to validate the mandatory attributes of the VOs
     * 
     * if(vo.validate())
     * {
     *  //add ur logic here
     * }
     * else
     * {
     *      throw new Exception("some mandatory fields are missing");
     * }
     * 
     * 
     * @return
     */
    public boolean validate() throws Exception
    {
        return true;
    }
    
    public Map<String, Object> covertVOToMap()
    {
        Map<String, Object> result = new HashMap<String, Object>();
        
        try
        {
            BeanInfo info = Introspector.getBeanInfo(this.getClass());  
            PropertyDescriptor[] props = info.getPropertyDescriptors();  
            for (PropertyDescriptor pd : props) 
            {  
                //get the setter method for this property
                Method m = pd.getReadMethod();
                if(m != null)
                {
                    String name = pd.getName();
                    if(name.equalsIgnoreCase("class") || name.equalsIgnoreCase("$type$"))
                    {
                        continue;
                    }
//                    System.out.println(name + " - " + pd.getPropertyType());
//                    System.out.println();// -->java.lang.ref.WeakReference;
                    Object value = null;
                    try
                    {
                       value = m.invoke(this);
                       result.put(name, value);
                    }
                    catch (Exception e)
                    {
                        // ignore this
                    }
                }
            }
        }
        catch(Exception e)
        {
            Log.log.error("error in conversion of object to map", e);
        }
        
        
        return result;
    }
    
    @SuppressWarnings("rawtypes")
    protected void setDefaultValues()
    {
        try
        {
            BeanInfo info = Introspector.getBeanInfo(this.getClass());  
            PropertyDescriptor[] props = info.getPropertyDescriptors();  
            for (PropertyDescriptor pd : props) 
            {  
                //get the setter method for this property
                Method m = pd.getWriteMethod();
                if(m != null)
                {
                    //get the type of parameter that setter method needs
                    Class[] c = m.getParameterTypes();
                    if(c != null && c.length > 0)
                    {
                        Class param = c[0];
                        String type = param.toString();
                        
                        //get the default value based on the type
                        Object defaultVal = null;
                        if(JAVA_PRIMITIVE_TYPES.contains(type))
                        {
                            defaultVal = getDefaultPrimitiveTypeValue(type);
                        }
                        else
                        {
                            defaultVal = getDefaultValue(param);
                        }
                        
                        //invoke the setter method
                        try
                        {
                            m.invoke(this, defaultVal);
                        }
                        catch (Exception e)
                        {
                            // ignore this
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            // ignore this
        }
    }
    
    @SuppressWarnings("rawtypes")
    private Object getDefaultValue(Class param)
    {
        Object defaultVal = null;
        
        if(param.equals(String.class))
        {
            defaultVal = STRING_DEFAULT;
        }
        else if(param.equals(Date.class))
        {
            defaultVal = DATE_DEFAULT;
        }
        else if(param.equals(Integer.class))
        {
            defaultVal = INTEGER_DEFAULT;
        }
        else if(param.equals(Double.class))
        {
            defaultVal = DOUBLE_DEFAULT;
        }
        else if(param.equals(Long.class))
        {
            defaultVal = LONG_DEFAULT;
        }
        else if(param.equals(Float.class))
        {
            defaultVal = FLOAT_DEFAULT;
        }
        else if(param.equals(BigDecimal.class))
        {
            defaultVal = BIGDECIMAL_DEFAULT;
        }
        else if(param.equals(byte[].class))
        {
            defaultVal = BYTE_ARRAY_DEFAULT;
        }
        else if(param.equals(Boolean.class))
        {
            defaultVal = BOOLEAN_DEFAULT;
        }
        
        return defaultVal;
    }
    
    private Object getDefaultPrimitiveTypeValue(String param)
    {
        Object defaultVal = null;
        
        if(param.equals("boolean"))
        {
            defaultVal = false;
        }
        else if(param.equals("byte"))
        {
            defaultVal = BYTE_DEFAULT;
        }
        else if(param.equals("short"))
        {
            defaultVal = SHORT_DEFAULT;
        }
        else if(param.equals("int"))
        {
            defaultVal = INTEGER_DEFAULT;
        }
        else if(param.equals("long"))
        {
            defaultVal = LONG_DEFAULT;
        }
        else if(param.equals("float"))
        {
            defaultVal = FLOAT_DEFAULT;
        }
        else if(param.equals("double"))
        {
            defaultVal = DOUBLE_DEFAULT;
        }

        return defaultVal;
    }
	
    public String getSys_id()
    {
        String id = this.sys_id;
        if(id == null || id.trim().length() == 0 || id.equalsIgnoreCase(STRING_DEFAULT))
        {
            id = getId();
            this.sys_id = getId();
            if(id != null && id.trim().length() == 0 )
            {
                this.sys_id = null;
                id = null;
            }
        }

        return sys_id;
    }
    public void setSys_id(String sys_id)
    {
        if (StringUtils.isNotBlank(sys_id) || StringUtils.isBlank(this.sys_id) || StringUtils.isBlank(this.id) || (this.sys_id.equals(STRING_DEFAULT) && this.id.equals(STRING_DEFAULT)))
        {
            sys_id =  sys_id != null ? sys_id.trim() : sys_id;
        }
        
        this.sys_id = sys_id;
        this.id = this.sys_id;
    }

    public Date getSysCreatedOn()
    {
        return sysCreatedOn;
    }
    public void setSysCreatedOn(Date sysCreatedOn)
    {
        this.sysCreatedOn = sysCreatedOn;
    }
    public String getSysCreatedBy()
    {
        String result = this.sysCreatedBy;
        if(StringUtils.isEmpty(result))
        {
            result = null;
        }

        return result;
    }
    public void setSysCreatedBy(String sysCreatedBy)
    {
        if (StringUtils.isNotBlank(sysCreatedBy) || StringUtils.isBlank(this.sysCreatedBy) || this.sysCreatedBy.equals(STRING_DEFAULT))
        {
            this.sysCreatedBy = sysCreatedBy != null ? sysCreatedBy.trim() : sysCreatedBy;
        }
    }
    public Date getSysUpdatedOn()
    {
        return sysUpdatedOn;
    }
    public void setSysUpdatedOn(Date sysUpdatedOn)
    {
        this.sysUpdatedOn = sysUpdatedOn;
    }
    public String getSysUpdatedBy()
    {
        String result = this.sysUpdatedBy;
        if(StringUtils.isEmpty(result))
        {
            result = null;
        }
        
        return result;
    }
    public void setSysUpdatedBy(String sysUpdatedBy)
    {
        if (StringUtils.isNotBlank(sysUpdatedBy) || StringUtils.isBlank(this.sysUpdatedBy) || this.sysUpdatedBy.equals(STRING_DEFAULT))
        {
            this.sysUpdatedBy = sysUpdatedBy != null ? sysUpdatedBy.trim() : sysUpdatedBy;
        }
    }
    public Integer getSysModCount()
    {
        return sysModCount;
    }
    public void setSysModCount(Integer sysModCount)
    {
        this.sysModCount = sysModCount;
    }

    public String getSysPerm()
    {
        return sysPerm;
    }

    public void setSysPerm(String sysPerm)
    {
        if (StringUtils.isNotBlank(sysPerm) || StringUtils.isBlank(this.sysPerm) || this.sysPerm.equals(STRING_DEFAULT))
        {
            this.sysPerm = sysPerm != null ? sysPerm.trim() : sysPerm;
        }
    }

    public String getSysOrg()
    {
        return sysOrg;
    }

    public void setSysOrg(String sysOrg)
    {
        if (StringUtils.isNotBlank(sysOrg) || StringUtils.isBlank(this.sysOrg) || this.sysOrg.equals(STRING_DEFAULT))
        {
            this.sysOrg = sysOrg != null ? sysOrg.trim() : sysOrg;
        }
    }

    public String getId()
    {
        return this.id;
    }

    public void setId(String id)
    {
//        if (StringUtils.isNotBlank(id) || this.id.equals(STRING_DEFAULT))
//        {
//            this.id = id != null ? id.trim() : id;
//        }
    	if (StringUtils.isNotBlank(id) || StringUtils.isBlank(this.id) || (this.id.equals(STRING_DEFAULT)))
        {
    		
    		id =  id != null ? id.trim() : id;
        }
        
        this.id = id;
    }

    public String getSysOrganizationName()
    {
        return sysOrganizationName;
    }

    public void setSysOrganizationName(String organizationName)
    {
        if (StringUtils.isNotBlank(organizationName) || StringUtils.isBlank(this.sysOrganizationName) || this.sysOrganizationName.equals(STRING_DEFAULT))
        {
            this.sysOrganizationName = organizationName != null ? organizationName.trim() : organizationName;
        }
    }
    
    public Integer getChecksum()
    {
        return checksum;
    }

    public void setChecksum(Integer checksum)
    {
        this.checksum = checksum;
    }

    public Boolean getSysIsDeleted()
    {
        return sysIsDeleted;
    }

    public Boolean ugetSysIsDeleted()
    {
        Boolean isDeleted = false;
        
        if(getSysIsDeleted() != null)
        {
            isDeleted = getSysIsDeleted();
        }
        
        return isDeleted;
    }

    public void setSysIsDeleted(Boolean sysIsDeleted)
    {
        this.sysIsDeleted = sysIsDeleted;
    }
    
    public static final Map<String, String> createUI2BackEndPropNameMapping(Map<String, String> unsafe2SafePropNames)
    {
        Map<String, String> ui2BackEndPropNameMap = new HashMap<String, String>();
        
        if (unsafe2SafePropNames == null || unsafe2SafePropNames.isEmpty())
        {
            return ui2BackEndPropNameMap;
        }
        
        for (String unsafePropName : unsafe2SafePropNames.keySet())
        {
            String safePropName = unsafe2SafePropNames.get(unsafePropName);
            
            if (unsafePropName.length() > 2 && safePropName.length() > 2)
            {
                String unsafePropName1st2Chars = unsafePropName.substring(0, 2).toLowerCase();
                String safePropName1st2Chars = safePropName.substring(0, 2).toLowerCase();
                
                ui2BackEndPropNameMap.put(unsafePropName1st2Chars + unsafePropName.substring(2), 
                                          safePropName1st2Chars + safePropName.substring(2));
            }
        }
        
        return ui2BackEndPropNameMap;
    }
    
    public static String getBackEndJSON(String uiJSONString)
    {
        return getBackEndJSON(uiJSONString, null);
    }
    
    public static String getBackEndJSON(String uiJSONString, Map<String, String> ui2BackEndPropNameMap)
    {
        if (ui2BackEndPropNameMap == null || ui2BackEndPropNameMap.isEmpty())
        {
            return uiJSONString;
        }
        
        String backendJSONString = JSONNull.getInstance().toString();
        
        JSONObject uiJSONObj = StringUtils.stringToJSONObject(uiJSONString);
        JSONObject backendJSONObj = new JSONObject();
        
        if (uiJSONObj != null && !JSONNull.getInstance().equals(uiJSONObj))
        {
            for (Object key : uiJSONObj.keySet())
            {
                if (key instanceof String)
                {
                    String keyString = (String)key;
                    
                    if (ui2BackEndPropNameMap.containsKey(keyString))
                    {
                        Object value = uiJSONObj.get(key);
                        backendJSONObj.put(ui2BackEndPropNameMap.get(keyString), value);
                    }
                    else
                    {
                        backendJSONObj.put(key, uiJSONObj.get(key));
                    }
                }
            }
            
            backendJSONString = backendJSONObj.toString();
        }
        
        Log.log.trace("UI JSON String : [" + uiJSONString + "], Backend JSON String = [" + backendJSONString + "]");
        
        return backendJSONString;
    }
    
    /*
     * Use below to set **************** in objects instead of VO.STARS to avoid it being flagged
     * as hard coded password vulnerability. CWE - 259 (http://cwe.mitre.org/data/definitions/259.html)
     */
    public static String getStars() {
    	StringBuilder sb = new StringBuilder();
    	
    	sb.append(STAR).append(STAR);
    	sb.append(sb.toString()).append(sb.toString()).append(sb.toString());
    	sb.append(STAR); // Match with VO.STARS
    	
    	return sb.toString();
    }
}
