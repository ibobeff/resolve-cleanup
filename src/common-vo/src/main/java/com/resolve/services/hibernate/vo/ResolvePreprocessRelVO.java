/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolvePreprocessRelVO extends VO
{
    private static final long serialVersionUID = -2212182741582194038L;
    
    private ResolvePreprocessVO preprocess;
    private String URefPreprocess; 

    public ResolvePreprocessRelVO()
    {
    }

    public ResolvePreprocessVO getPreprocess()
    {
        return preprocess;
    }

    public void setPreprocess(ResolvePreprocessVO preprocess)
    {
        this.preprocess = preprocess;
    }

    public String getURefPreprocess()
    {
        return URefPreprocess;
    }

    public void setURefPreprocess(String uRefPreprocess)
    {
        URefPreprocess = uRefPreprocess;
    }
    
   
}
