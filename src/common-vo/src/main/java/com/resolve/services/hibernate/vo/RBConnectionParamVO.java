/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
public class RBConnectionParamVO extends VO
{
    private static final long serialVersionUID = 5027878281160188483L;
	private String name;
    private String displayName;
    private String dataType;
    private String defaultValue;
    private String source; //WSDATA,PARAM etc.
    private String sourceName;
    
    private RBConnectionVO connection;
    
    public RBConnectionParamVO()
    {
        super();
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public String getDataType()
    {
        return dataType;
    }

    public void setDataType(String dataType)
    {
        this.dataType = dataType;
    }

    public String getDefaultValue()
    {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue)
    {
        this.defaultValue = defaultValue;
    }

    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public String getSourceName()
    {
        return sourceName;
    }

    public void setSourceName(String sourceName)
    {
        this.sourceName = sourceName;
    }

    public RBConnectionVO getConnection()
    {
        return connection;
    }

    public void setConnection(RBConnectionVO connection)
    {
        this.connection = connection;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((connection == null) ? 0 : connection.hashCode());
        result = prime * result + ((dataType == null) ? 0 : dataType.hashCode());
        result = prime * result + ((defaultValue == null) ? 0 : defaultValue.hashCode());
        result = prime * result + ((displayName == null) ? 0 : displayName.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((source == null) ? 0 : source.hashCode());
        result = prime * result + ((sourceName == null) ? 0 : sourceName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        RBConnectionParamVO other = (RBConnectionParamVO) obj;
        if (connection == null)
        {
            if (other.connection != null) return false;
        }
        else if (!connection.equals(other.connection)) return false;
        if (dataType == null)
        {
            if (other.dataType != null) return false;
        }
        else if (!dataType.equals(other.dataType)) return false;
        if (defaultValue == null)
        {
            if (other.defaultValue != null) return false;
        }
        else if (!defaultValue.equals(other.defaultValue)) return false;
        if (displayName == null)
        {
            if (other.displayName != null) return false;
        }
        else if (!displayName.equals(other.displayName)) return false;
        if (name == null)
        {
            if (other.name != null) return false;
        }
        else if (!name.equals(other.name)) return false;
        if (source == null)
        {
            if (other.source != null) return false;
        }
        else if (!source.equals(other.source)) return false;
        if (sourceName == null)
        {
            if (other.sourceName != null) return false;
        }
        else if (!sourceName.equals(other.sourceName)) return false;
        return true;
    }
}
