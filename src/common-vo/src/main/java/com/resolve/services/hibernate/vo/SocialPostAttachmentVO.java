/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class SocialPostAttachmentVO extends VO
{
    private static final long serialVersionUID = 1023831818295092284L;
    
    private String UFilename;
    private String UDisplayName;
    private String UType;
    private String ULocation;
    private Integer USize;
    private byte[] UContent;
    private String UPostId;
    private Boolean UIsValid;

    public SocialPostAttachmentVO()
    {
    }

    public SocialPostAttachmentVO(String sys_id, String UFilename, String UType)
    {
        this.setSys_id(sys_id);
        this.setUFilename(UFilename);
        this.setUType(UType);
    }

    public String getUFilename()
    {
        return this.UFilename;
    }

    public void setUFilename(String UFilename)
    {
        this.UFilename = UFilename;
    }
    

    public String getUDisplayName()
    {
        return UDisplayName;
    }

    public void setUDisplayName(String uDisplayName)
    {
        UDisplayName = uDisplayName;
    }

    
    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    public String getULocation()
    {
        return this.ULocation;
    }

    public void setULocation(String ULocation)
    {
        this.ULocation = ULocation;
    }

    public Integer getUSize()
    {
        return this.USize;
    }

    public void setUSize(Integer USize)
    {
        this.USize = USize;
    }

    public byte[] getUContent()
    {
        return UContent;
    }

    public void setUContent(byte[] uContent)
    {
        UContent = uContent;
    }

    public String getUPostId()
    {
        return UPostId;
    }

    public void setUPostId(String uPostId)
    {
        UPostId = uPostId;
    }

    public Boolean getUIsValid()
    {
        return UIsValid;
    }
    
    public Boolean ugetUIsValid()
    {
        if (this.UIsValid == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsValid;
        }
    }

    public void setUIsValid(Boolean uIsValid)
    {
        UIsValid = uIsValid;
    }

    
}
