/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;

public class EWSAddressVO extends GatewayVO
{
    private static final long serialVersionUID = -8031428719545711230L;
	private String UEWSAddress;
    private String UEWSPassword;

    // object referenced by
    public EWSAddressVO()
    {
    } // EmailAddressVO
    
    public EWSAddressVO(boolean secured)
    {
    	if (secured) {
    		String stars = VO.getStars()/*STARS*/;
    		UEWSPassword = stars;
    	}
    } // EmailAddressVO
    
    @MappingAnnotation(columnName = "EWSADDRESS")
    public String getUEWSAddress()
    {
        return this.UEWSAddress;
    } // getUEWSAddress

    public void setUEWSAddress(String UEWSAddress)
    {
        this.UEWSAddress = UEWSAddress;
    } // setUEWSAddress

    @MappingAnnotation(columnName = "EWSPASSWORD")
    public String getUEWSPassword()
    {
        return this.UEWSPassword;
    } // getUEWSPassword

    public void setUEWSPassword(String UEWSPassword)
    {
        this.UEWSPassword = UEWSPassword;
    } // setUEWSPassword

    @Override
    public String getUniqueId()
    {
        return this.UEWSAddress;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((UEWSAddress == null) ? 0 : UEWSAddress.hashCode());
        result = prime * result + ((UEWSPassword == null) ? 0 : UEWSPassword.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EWSAddressVO other = (EWSAddressVO) obj;
        if (UEWSAddress == null)
        {
            if (other.UEWSAddress != null) return false;
        }
        else if (!UEWSAddress.equals(other.UEWSAddress)) return false;
        if (UEWSPassword == null)
        {
            if (other.UEWSPassword != null) return false;
        }
        else if (!UEWSPassword.equals(other.UEWSPassword)) return false;
        return true;
    }
}