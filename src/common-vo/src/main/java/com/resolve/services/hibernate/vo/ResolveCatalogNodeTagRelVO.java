package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveCatalogNodeTagRelVO extends VO
{
    private static final long serialVersionUID = -7589692771737894578L;

    public ResolveCatalogNodeTagRelVO() {}

    private ResolveCatalogNodeVO catalogNode;
    private ResolveTagVO tag;
    private ResolveCatalogVO catalog; 
    
    
    public ResolveCatalogNodeVO getCatalogNode()
    {
        return catalogNode;
    }

    public void setCatalogNode(ResolveCatalogNodeVO catalogNode)
    {
        this.catalogNode = catalogNode;
    }

    public ResolveTagVO getTag()
    {
        return tag;
    }

    public void setTag(ResolveTagVO tag)
    {
        this.tag = tag;
    }

    public ResolveCatalogVO getCatalog()
    {
        return catalog;
    }

    public void setCatalog(ResolveCatalogVO catalog)
    {
        this.catalog = catalog;
    }
    
    
    
}
