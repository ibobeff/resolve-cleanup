/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class HTTPFilterVO extends GatewayFilterVO
{
    private static final long serialVersionUID = -8727854282447449449L;
	private Integer UPort;
    private String UUri;
    private Boolean USsl;
    private String UAllowedIP;
    private Integer UBlocking;
    private String U_bas_icAu_thUs_ername;
    private String U_bas_icAu_thPas_sword; 
   
    //GAT-27 changes to accomodate content type and response filter
    private String UContentType;
    private String UResponsefilter;
    
    public HTTPFilterVO()
    {
        super();
    }

    @MappingAnnotation(columnName = "PORT")
    public Integer getUPort()
    {
        return UPort;
    }

    public void setUPort(Integer uPort)
    {
        UPort = uPort;
    }

    @MappingAnnotation(columnName = "URI")
    public String getUUri()
    {
        return UUri;
    }

    public void setUUri(String uUri)
    {
        if (StringUtils.isNotBlank(uUri) || UUri.equals(VO.STRING_DEFAULT))
        {
            UUri = uUri != null ? uUri.trim() : uUri;
        }
    }

    @MappingAnnotation(columnName = "SSL")
    public Boolean getUSsl()
    {
        return USsl;
    }

    public void setUSsl(Boolean uSsl)
    {
        USsl = uSsl;
    }

    @MappingAnnotation(columnName = "ALLOWED_IP")
    public String getUAllowedIP()
    {
        return UAllowedIP;
    }

    public void setUAllowedIP(String uAllowedIP)
    {
        if (StringUtils.isNotBlank(uAllowedIP) || UAllowedIP.equals(VO.STRING_DEFAULT))
        {
            UAllowedIP = uAllowedIP != null ? uAllowedIP.trim() : uAllowedIP;
        }
    }
    
    @MappingAnnotation(columnName="BLOCKING")
    public Integer getUBlocking()
    {
        return UBlocking;
    }

    public void setUBlocking(Integer uBlocking)
    {
        UBlocking = uBlocking;
    }

    
    
    @MappingAnnotation(columnName="BASIC_AUTH_UN")
    public String getU_bas_icAu_thUs_ername()
    {
        return U_bas_icAu_thUs_ername;
    }

    public void setU_bas_icAu_thUs_ername(String uBasicAuthUsername)
    {
        if (StringUtils.isNotBlank(uBasicAuthUsername) || this.U_bas_icAu_thUs_ername.equals(VO.STRING_DEFAULT))
           U_bas_icAu_thUs_ername = uBasicAuthUsername;
    }

    @MappingAnnotation(columnName="BASIC_AUTH_PW")
    public String getU_bas_icAu_thPas_sword()
    {
        return U_bas_icAu_thPas_sword;
    }

    public void setU_bas_icAu_thPas_sword(String uBasicAuthPassword)
    {
        if (StringUtils.isNotBlank(uBasicAuthPassword) || this.U_bas_icAu_thPas_sword.equals(VO.STRING_DEFAULT))
            U_bas_icAu_thPas_sword = uBasicAuthPassword;
    }

    @MappingAnnotation(columnName="CONTENT_TYPE")
    public String getUContentType() {
    	return UContentType;
    }
    
    public void setUContentType(String uContentType) {
    	UContentType = uContentType;
    }
    
    @MappingAnnotation(columnName="RESPONSE_FILTER")
    public String getUResponsefilter() {
    	return UResponsefilter;
    }
    public void setUResponsefilter(String uResponsefilter) {
    	UResponsefilter = uResponsefilter;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UAllowedIP == null) ? 0 : UAllowedIP.hashCode());
        result = prime * result + ((UPort == null) ? 0 : UPort.hashCode());
        result = prime * result + ((USsl == null) ? 0 : USsl.hashCode());
        result = prime * result + ((UUri == null) ? 0 : UUri.hashCode());
        result = prime * result + ((U_bas_icAu_thPas_sword == null) ? 0 : U_bas_icAu_thPas_sword.hashCode());
        result = prime * result + ((U_bas_icAu_thUs_ername == null) ? 0 : U_bas_icAu_thUs_ername.hashCode());
        result = prime * result + ((UContentType == null) ? 0 : UContentType.hashCode());
        result = prime * result + ((UResponsefilter == null) ? 0 : UResponsefilter.hashCode());
        
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        
        HTTPFilterVO other = (HTTPFilterVO) obj;
        
        if (UAllowedIP == null)
        {
            if (StringUtils.isNotBlank(other.UAllowedIP)) return false;
        }
        
        else if (!UAllowedIP.trim().equals(other.UAllowedIP == null ? "" : other.UAllowedIP.trim())) return false;
        
        if (UBlocking == null)
        {
            if (other.UBlocking != null) return false;
        }
        
        else if (!UBlocking.equals(other.UBlocking)) return false;
        
        if (UPort == null)
        {
            if (other.UPort != null) return false;
        }
        
        else if (!UPort.equals(other.UPort)) return false;
        
        if (USsl == null)
        {
            if (other.USsl != null) return false;
        }
        else if (!USsl.equals(other.USsl)) return false;
        
        if (UUri == null)
        {
            if (StringUtils.isNotBlank(other.UUri)) return false;
        }
        
        else if (!UUri.trim().equals(other.UUri == null ? "" : other.UUri.trim())) return false;
        
        //For backward compatibility this is required
        if (StringUtils.isBlank(UContentType))
        {
            if (StringUtils.isNotBlank(other.UContentType)) return false;
        }
        
        else if (!UContentType.trim().equals(other.UContentType == null ? "" : other.UContentType.trim())) return false;
        //GAT-42 Added check for null, if the password is null or AUTh is not enabled skip this check
        if(U_bas_icAu_thPas_sword != null && !compareTrimmedBlankasNull(U_bas_icAu_thPas_sword, other.U_bas_icAu_thPas_sword))
            return false;
        //GAT-42 Added check for null, if the username is null or AUTh is not enabled skip this check
        if(U_bas_icAu_thUs_ername != null && !compareTrimmedBlankasNull(U_bas_icAu_thUs_ername, other.U_bas_icAu_thUs_ername))
            return false;

        return true;
    }

    private boolean compareTrimmedBlankasNull(String a, String b)
    {   
        
        if(VO.STRING_DEFAULT.equals(a))
            a = null;
        if(VO.STRING_DEFAULT.equals(b))
            b = null;
    
        
        if (StringUtils.isBlank(a))
        {
            if (StringUtils.isNotBlank(b) ) return false;
        }
        
        else if (!a.trim().equals(b == null ? "" : b.trim())) return false;
        
        return true;
        
    }
    
    
    
    
}
