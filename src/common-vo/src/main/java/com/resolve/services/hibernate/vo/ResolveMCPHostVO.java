package com.resolve.services.hibernate.vo;

import java.util.Collection;

import com.resolve.services.interfaces.VO;

public class ResolveMCPHostVO extends VO
{
    private static final long serialVersionUID = -7966361906623219707L;
    
    private String UHostName;
    private String UBluePrint;
    private String UHostIp;
    private String UStatus;
    private Boolean UIsMonitored;
    
    private ResolveMCPClusterVO cluster;
    private ResolveMCPGroupVO groupVO;
    
    private Collection<ResolveMCPHostStatusVO> UComponentStatusStatuses;
    private Collection<ResolveMCPComponentVO> mcpComponents;

    public String getUHostName()
    {
        return UHostName;
    }
    public void setUHostName(String uHostName)
    {
        UHostName = uHostName;
    }

    public String getUBluePrint()
    {
        return UBluePrint;
    }
    public void setUBluePrint(String uBluePrint)
    {
        UBluePrint = uBluePrint;
    }
    
    public String getUHostIp()
    {
        return UHostIp;
    }
    public void setUHostIp(String uHostIp)
    {
        UHostIp = uHostIp;
    }

    public String getUStatus()
    {
        return UStatus;
    }
    public void setUStatus(String uStatus)
    {
        UStatus = uStatus;
    }
    
    public Boolean getUIsMonitored()
	{
		return UIsMonitored;
	}
	public void setUIsMonitored(Boolean uIsMonitored)
	{
		UIsMonitored = uIsMonitored;
	}
	
	public ResolveMCPClusterVO getCluster()
    {
        return cluster;
    }
    public ResolveMCPGroupVO getGroupVO()
    {
        return groupVO;
    }
    
    public void setGroupVO(ResolveMCPGroupVO groupVO)
    {
        this.groupVO = groupVO;
    }
    public void setCluster(ResolveMCPClusterVO cluster)
    {
        this.cluster = cluster;
    }
    
    public Collection<ResolveMCPHostStatusVO> getUComponentStatusStatuses()
    {
        return UComponentStatusStatuses;
    }

    public void setUComponentStatusStatuses(Collection<ResolveMCPHostStatusVO> uComponentStatusStatuses)
    {
        UComponentStatusStatuses = uComponentStatusStatuses;
    }
    
    public Collection<ResolveMCPComponentVO> getMcpComponents()
    {
        return mcpComponents;
    }
    public void setMcpComponents(Collection<ResolveMCPComponentVO> mcpComponents)
    {
        this.mcpComponents = mcpComponents;
    }
}
