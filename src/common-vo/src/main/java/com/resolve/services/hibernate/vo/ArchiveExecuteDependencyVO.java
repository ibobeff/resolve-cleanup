/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

// Generated Dec 11, 2009 11:15:20 AM by Hibernate Tools 3.2.4.GA

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;

public class ArchiveExecuteDependencyVO extends VO
{
    private static final long serialVersionUID = -1669089976928203003L;
    
    private String UType;
    private String UExecute;
    private Boolean UCompletion;
    private String UCondition;
    private String USeverity;
    private String UExpression;
    private String UMerge;

	
    // object references
    private ArchiveExecuteRequestVO executeRequest;   // UExecuteRequest;
	
    // object referenced by
	
    public ArchiveExecuteDependencyVO()
    {
    }

    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    public ArchiveExecuteRequestVO getExecuteRequest()
    {
        return executeRequest;
    }

    public void setExecuteRequest(ArchiveExecuteRequestVO executeRequest)
    {
        this.executeRequest = executeRequest;

        if (executeRequest != null)
        {
            Collection<ArchiveExecuteDependencyVO> coll = executeRequest.getArchiveExecuteDependencies();
            if (coll == null)
            {
				coll = new HashSet<ArchiveExecuteDependencyVO>();
	            coll.add(this);
	            
                executeRequest.setArchiveExecuteDependencies(coll);
            }
        }
    }
    
    public String getUExecute()
    {
        return this.UExecute;
    }

    public void setUExecute(String UExecute)
    {
        this.UExecute = UExecute;
    }

    public Boolean getUCompletion()
    {
        return this.UCompletion;
    }

    public void setUCompletion(Boolean UCompletion)
    {
        this.UCompletion = UCompletion;
    }

    public String getUCondition()
    {
        return this.UCondition;
    }

    public void setUCondition(String UCondition)
    {
        this.UCondition = UCondition;
    }

    public String getUSeverity()
    {
        return this.USeverity;
    }

    public void setUSeverity(String USeverity)
    {
        this.USeverity = USeverity;
    }

    public String getUExpression()
    {
        return this.UExpression;
    }

    public void setUExpression(String UExpression)
    {
        this.UExpression = UExpression;
    }

    public String getUMerge()
    {
        return this.UMerge;
    }

    public void setUMerge(String UMerge)
    {
        this.UMerge = UMerge;
    }

}
