/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;

/**
 * This is a base VO for all the gateway filter to hold
 * all the common properties.
 *
 */

public abstract class GatewayVO extends VO
{
    private static final long serialVersionUID = -1860700906890660008L;
	private String UQueue;
    private boolean changed;
    
    public GatewayVO(boolean defaultValues) {
    	super(defaultValues);
    }
    
    // object referenced by
    protected GatewayVO()
    {
        super();
    } // GatewayFilterVO

    @MappingAnnotation(columnName="QUEUE")
    public String getUQueue()
    {
        return this.UQueue;
    } // getUQueue

    public void setUQueue(String UQueue)
    {
        this.UQueue = UQueue;
    } // setUQueue

    public boolean isChanged()
    {
        return changed;
    }

    public void setChanged(boolean changed)
    {
        this.changed = changed;
    }

    public abstract String getUniqueId();

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((UQueue == null) ? 0 : UQueue.hashCode());
        result = prime * result + (changed ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        GatewayVO other = (GatewayVO) obj;
        if (UQueue == null)
        {
            if (other.UQueue != null) return false;
        }
        else if (!UQueue.equals(other.UQueue)) return false;
        if (changed != other.changed) return false;
        return true;
    }
}
