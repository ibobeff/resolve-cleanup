package com.resolve.services.hibernate.vo;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import com.resolve.services.interfaces.VO;

public class SIRPhaseRuntimeDataVO extends VO {
    
    private static final long serialVersionUID = 8203349258577576338L;
    
    private static final Integer LAST_POSITION = Integer.MAX_VALUE;
    
    private Integer position;
    private SIRPhaseMetaDataVO phaseMetaData;         
    private ResolveSecurityIncidentVO referencedRSI;
    
    public SIRPhaseRuntimeDataVO() {}
    
    public Integer getPosition() {
        return position;
    }
    
    public Integer ugetPosition() {
        if (getPosition() != null && getPosition() > NON_NEGATIVE_INTEGER_DEFAULT) {
            return getPosition();
        } else {
            return LAST_POSITION;
        }
    }
    
    public void setPosition(Integer position) {
        this.position = position;
    }
    
    public SIRPhaseMetaDataVO getPhaseMetaData() {
        return phaseMetaData;
    }
    
    public void setPhaseMetaData(SIRPhaseMetaDataVO phaseMetaData) {
        this.phaseMetaData = phaseMetaData;
    }
    
    public ResolveSecurityIncidentVO getReferencedRSI() {
        return referencedRSI;
    }
    
    public void setReferencedRSI(ResolveSecurityIncidentVO referencedRSI) {
        this.referencedRSI = referencedRSI;
    }
    
    @Override
    public String toString() {
        return "SIR Phase Runtime Data VO [" +
               (isNotBlank(getSys_id()) ? "Sys Id: " + getSys_id() + ", " : "") + 
               "Sys Org: " + (isNotBlank(getSysOrg()) ? getSysOrg() : "") +
               ", Position: " + ugetPosition() + 
               ", Phase Meta Data: " + getPhaseMetaData() + 
               ", Referenced Rsolve Security Incident: " + getReferencedRSI() + "]";
    }
}
