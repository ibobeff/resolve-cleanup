package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class MSGGatewayPropertiesAttrVO extends VO
{
    private static final long serialVersionUID = 1L;
    
    private String UName;
    private String UValue;
    private String UMSGPropertiesId;
    
    public String getUName()
    {
        return UName;
    }
    public void setUName(String uName)
    {
        UName = uName;
    }
    public String getUValue()
    {
        return UValue;
    }
    public void setUValue(String uValue)
    {
        UValue = uValue;
    }
    public String getUMSGPropertiesId()
    {
        return UMSGPropertiesId;
    }
    public void setUMSGPropertiesId(String uPushPropertiesId)
    {
        UMSGPropertiesId = uPushPropertiesId;
    }

} // class MSGGatewayPropertiesAttrVO
