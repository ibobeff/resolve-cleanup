/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

// Generated Jan 15, 2010 12:12:58 PM by Hibernate Tools 3.2.4.GA

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import com.resolve.services.interfaces.VO;

public class UserRoleRelVO  extends VO
{
    private static final long serialVersionUID = 4269861186595019647L;
    
    // object references
	private UsersVO user;
	private RolesVO role;
	
	public UserRoleRelVO()
	{
	}
	
	public String toString()
	{
	    return this.user+" = "+role;
	} // toString

	public UsersVO getUser()
	{
		return user;
	}

	public void setUser(UsersVO user)
	{
		this.user = user;
		
        if (user != null)
        {
            Collection<UserRoleRelVO> coll = user.getUserRoleRels();
            if (coll == null)
            {
                coll = new HashSet<UserRoleRelVO>();
	            coll.add(this);
	            
                user.setUserRoleRels(coll);
            }
        }
	}

	public RolesVO getRole()
	{
		return role;
	}

	public void setRole(RolesVO role)
	{
		this.role = role;
		
        List<UserRoleRelVO> coll = role.getUserRoleRels();
        if (coll == null)
        {
            coll = new ArrayList<UserRoleRelVO>();
            coll.add(this);
            
            role.setUserRoleRels(coll);
        }
	}

}
