package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveMCPBlueprintVO extends VO
{
    private static final long serialVersionUID = -4905643545983607335L;
    
    private String UBlueprint;
    private Integer UVersion;
    private String UMcpHostId;
    
    public String getUBlueprint()
    {
        return UBlueprint;
    }
    public void setUBlueprint(String uBlueprint)
    {
        UBlueprint = uBlueprint;
    }
    
    public Integer getUVersion()
    {
        return UVersion;
    }
    public void setUVersion(Integer uVersion)
    {
        UVersion = uVersion;
    }
    
    public String getUMcpHostId()
    {
        return UMcpHostId;
    }
    public void setUMcpHostId(String uMcpHostId)
    {
        UMcpHostId = uMcpHostId;
    }
}
