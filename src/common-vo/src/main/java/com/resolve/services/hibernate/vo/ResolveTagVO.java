package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.Date;

import com.resolve.search.model.query.Suggest;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

public class ResolveTagVO extends VO
{
    private static final long serialVersionUID = -6068225537594138300L;
    
    private String UName;
    private String UDescription;
    
    // object references
    private Collection<ResolveCatalogNodeTagRelVO> catalogTagRels; //catalog and tag rels
    private Collection<WikidocResolveTagRelVO> wikiTagRels; //catalog and tag rels
    private Collection<ResolveActionTaskResolveTagRelVO> atTagRels;// actiontask and tag rels

    //just for UI - maybe removed if we change the UI
    private String name;
    private String description;
    
    // for backward compatibility only
    private String sysId;

    public ResolveTagVO() {}


    public String getUName()
    {
        return UName;
    }


    public void setUName(String uName)
    {
        UName = uName;
        name = uName;
    }


    public String getUDescription()
    {
        return UDescription;
    }


    public void setUDescription(String uDescription)
    {
        UDescription = uDescription;
        description = uDescription;
    }
    
    public Collection<ResolveCatalogNodeTagRelVO> getCatalogTagRels()
    {
        return catalogTagRels;
    }

    public void setCatalogTagRels(Collection<ResolveCatalogNodeTagRelVO> catalogTagRels)
    {
        this.catalogTagRels = catalogTagRels;
    }

    public Collection<WikidocResolveTagRelVO> getWikiTagRels()
    {
        return wikiTagRels;
    }

    public void setWikiTagRels(Collection<WikidocResolveTagRelVO> wikiTagRels)
    {
        this.wikiTagRels = wikiTagRels;
    }

    public Collection<ResolveActionTaskResolveTagRelVO> getAtTagRels()
    {
        return atTagRels;
    }

    public void setAtTagRels(Collection<ResolveActionTaskResolveTagRelVO> atTagRels)
    {
        this.atTagRels = atTagRels;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
        this.UName = name;
    }

    public String getDescription()
    {
        return description;
    }


    public void setDescription(String description)
    {
        this.description = description;
    }


    public boolean validate()
    {
        boolean valid = true;
        
        if(StringUtils.isEmpty(this.getUName()))
        {
            throw new RuntimeException("Tag name is mandatory and cannot be empty");
        }
        
        return valid;
    }


    public void setSysId(String sysId)
    {
        super.setSys_id(sysId);
    }
    
    /*************************************************************
     * Following code is just to have 5.1.x compatibility
     ************************************************************/
    
    Date sysUpdatedDt;
    Date sysCreatedDt;
    Suggest suggest;
    long weight;

    public Date getSysUpdatedDt()
    {
        return sysUpdatedDt;
    }

    public void setSysUpdatedDt(Date sysUpdatedDt)
    {
        super.setSysUpdatedOn(sysUpdatedDt);
    }


    public Date getSysCreatedDt()
    {
        return sysCreatedDt;
    }

    public void setSysCreatedDt(Date sysCreatedDt)
    {
        super.setSysCreatedOn(sysCreatedDt);
    }

    public Suggest getSuggest()
    {
        return suggest;
    }


    public void setSuggest(Suggest suggest)
    {
        this.suggest = suggest;
    }


    public long getWeight()
    {
        return weight;
    }


    public void setWeight(long weight)
    {
        this.weight = weight;
    }
    
}
