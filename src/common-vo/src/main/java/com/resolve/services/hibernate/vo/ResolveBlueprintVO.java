/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveBlueprintVO extends VO
{
    private static final long serialVersionUID = -4542465821545674958L;
    
    private String UGuid;
    private String ULocalhost;
    private String UBlueprint;

    public ResolveBlueprintVO()
    {
    }

    public ResolveBlueprintVO usetsys_id(String sys_id)
    {
        this.setSys_id(sys_id);
        return this;
    }

    public String getUGuid()
    {
        return this.UGuid;
    }

    public void setUGuid(String UGuid)
    {
        this.UGuid = UGuid;
    }

    public String getULocalhost()
    {
        return this.ULocalhost;
    }

    public void setULocalhost(String ULocalhost)
    {
        this.ULocalhost = ULocalhost;
    }

    public String getUBlueprint()
    {
        return this.UBlueprint;
    }

    public void setUBlueprint(String UBlueprint)
    {
        this.UBlueprint = UBlueprint;
    }

    public boolean isModified(ResolveBlueprintVO reg)
    {
        boolean result = false;

        if (reg != null)
        {
            if ((this.UGuid != null && !this.UGuid.equals(reg.getUGuid())) || (this.ULocalhost != null && !this.ULocalhost.equals(reg.getULocalhost()))
                            || (this.UBlueprint != null && !this.UBlueprint.equals(reg.getUBlueprint())))
            {
                result = true;
            }
        }

        return result;
    } // isModified

    public void update(ResolveBlueprintVO reg)
    {
        if (reg != null)
        {
            this.setUGuid(reg.getUGuid());
            this.setULocalhost(reg.getULocalhost());
            this.setUBlueprint(reg.getUBlueprint());
        }
    } // update

} // ResolveRegistration
