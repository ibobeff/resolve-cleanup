/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class SysAppModulerolesVO extends VO
{
    private static final long serialVersionUID = -2399634710563048984L;
    
    private String value;
    private Integer sequence;

	
    // object references
    private SysAppModuleVO sysAppModule;
	
    // object referenced by
	
    public SysAppModulerolesVO()
    {
    }

    public String getValue()
    {
        return this.value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    /*
     * @Column(name = "parent_id", length = 32) public String getParentId() {
     * return this.parentId; }
     * 
     * public void setParentId(String parentId) { this.parentId = parentId; }
     */

    public Integer getSequence()
    {
        return this.sequence;
    }

    public void setSequence(Integer sequence)
    {
        this.sequence = sequence;
    }

    public SysAppModuleVO getSysAppModule()
    {
        return this.sysAppModule;
    }

    public void setSysAppModule(SysAppModuleVO sysAppModule)
    {
        this.sysAppModule = sysAppModule;
        
//        if (sysAppModule != null)
//        {
//            Collection<SysAppModulerolesVO> coll = sysAppModule.getSysAppModuleroles();
//            if (coll == null)
//            {
//				coll = new HashSet<SysAppModulerolesVO>();
//	            coll.add(this);
//	            
//                sysAppModule.setSysAppModuleroles(coll);
//            }
//        }
    }
}
