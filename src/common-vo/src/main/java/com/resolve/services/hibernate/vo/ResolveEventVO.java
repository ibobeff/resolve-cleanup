/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.TimeZone;

import org.apache.commons.lang3.tuple.Pair;

import com.resolve.services.interfaces.VO;

public class ResolveEventVO extends VO
{
    private static final long serialVersionUID = 5957140746197511503L;
    
    private String UValue;
	
    public ResolveEventVO()
    {
    }

    public ResolveEventVO(String UValue) {
    	this.UValue = UValue;
    }
    
    public String getUValue()
    {
        return this.UValue;
    }

    public void setUValue(String UValue)
    {
        this.UValue = UValue;
    }
    
    @Override
    public String toString() {
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    	sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
    	
    	return "UValue=" + UValue + ", Updated On=" + sdf.format(getSysUpdatedOn());
    }
    
    public Pair<String, Boolean> getModuleNameAndHasStage(String operation) {
    	Pair<String, Boolean> moduleNameAndHasStage = null;
    	
    	if (StringUtils.isNotBlank(UValue)) {
    		String[] rsEventElements = UValue.split(",");
    		
    		if (rsEventElements.length >= 0) {
    			String firstEventElement = rsEventElements[0];
    			
    			if (StringUtils.isNotBlank(operation) && !firstEventElement.toLowerCase().contains(operation.toLowerCase())) {
    				firstEventElement = null;
    			}
    			
    			if (StringUtils.isNotBlank(firstEventElement) && firstEventElement.contains(":")) {
    				moduleNameAndHasStage = Pair.of(StringUtils.substringAfter(firstEventElement, ":"), 
    												Boolean.valueOf(StringUtils.substringBefore(firstEventElement, ":")
    																.toLowerCase()
    																.contains("-" + (StringUtils.isNotBlank(operation) ? 
    																 operation.toLowerCase() : ""))));
    			}
    		}
    	}
    	
    	if (Log.log.isTraceEnabled()) {
    		Log.log.trace(String.format("ResolveEventVO [%s] getModuleName(%s) returning %s", this, 
    									(StringUtils.isNotBlank(operation) ? operation : "null"),
    									(moduleNameAndHasStage != null ? moduleNameAndHasStage : "null")));
    	}
    	
    	return moduleNameAndHasStage;
    }
    
    public String getStage() {
    	String stage = null;
    	
    	if (StringUtils.isNotBlank(UValue)) {
    		String[] rsEventElements = UValue.split(",");
    		
    		if (rsEventElements.length >= 0) {
    			String firstEventElement = rsEventElements[0];
    			
	    		if (StringUtils.isNotBlank(firstEventElement) && firstEventElement.contains(":")) {
	    			stage = StringUtils.substringBefore(firstEventElement, ":");
	    			
	    			if (StringUtils.isNotBlank(stage) && stage.contains("-")) {
	    				stage = StringUtils.substringAfter(stage, "-");
	    			} else {
	    				stage = null;
	    			}
	    		}
    		}
    	}
    	
    	return stage;
    }
} // ResolveCron
