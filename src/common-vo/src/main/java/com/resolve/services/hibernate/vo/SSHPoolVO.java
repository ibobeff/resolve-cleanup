/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;

public class SSHPoolVO extends GatewayVO
{
    private static final long serialVersionUID = -8127206557352216387L;

    private String USubnetMask;
    private Integer UMaxConn;
    private Integer UTimeout;

    public SSHPoolVO()
    {
    } // DatabaseConnectionPool

    @MappingAnnotation(columnName="SUBNETMASK")
    public String getUSubnetMask()
    {
        return USubnetMask;
    }

    public void setUSubnetMask(String uSubnetMask)
    {
        USubnetMask = uSubnetMask;
    }

    @MappingAnnotation(columnName="MAXCONNECTION")
    public Integer getUMaxConn()
    {
        return UMaxConn;
    }

    public void setUMaxConn(Integer uMaxConn)
    {
        UMaxConn = uMaxConn;
    }

    @MappingAnnotation(columnName="TIMEOUT")
    public Integer getUTimeout()
    {
        return UTimeout;
    }

    public void setUTimeout(Integer uTimeout)
    {
        UTimeout = uTimeout;
    }

    @Override
    public String getUniqueId()
    {
        return USubnetMask;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UMaxConn == null) ? 0 : UMaxConn.hashCode());
        result = prime * result + ((USubnetMask == null) ? 0 : USubnetMask.hashCode());
        result = prime * result + ((UTimeout == null) ? 0 : UTimeout.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        //no need of super
        //if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        SSHPoolVO other = (SSHPoolVO) obj;
        if (UMaxConn == null)
        {
            if (other.UMaxConn != null) return false;
        }
        else if (!UMaxConn.equals(other.UMaxConn)) return false;
        if (USubnetMask == null)
        {
            if (other.USubnetMask != null) return false;
        }
        else if (!USubnetMask.equals(other.USubnetMask)) return false;
        if (UTimeout == null)
        {
            if (other.UTimeout != null) return false;
        }
        else if (!UTimeout.equals(other.UTimeout)) return false;
        return true;
    }
}

