/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;


import com.resolve.services.interfaces.IndexComponent;
import com.resolve.services.interfaces.VO;

public class ResolveActionParameterVO extends VO implements IndexComponent
{

    private static final long serialVersionUID = 1571416493855981998L;
    
    private String UName;
    private String UType;
    private String UDescription;
    private String UDefaultValue;
    private Boolean URequired;
    private Boolean UInclude;
    private String UPrefix;
    private String UPrefixSeparator;
    private Boolean UEncrypt;
    private Integer UOrder;
    private String UGroupName;
    private Integer UGroupOrder;
    private Boolean UProtected;

    // object references
    private ResolveActionInvocVO invocation; // UInvocation;

    // object referenced by

    public ResolveActionParameterVO()
    {
    }

    public ResolveActionInvocVO getInvocation()
    {
        return invocation;
    }

    public void setInvocation(ResolveActionInvocVO invocation)
    {
        this.invocation = invocation;

    }

    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    public String getUDescription()
    {
        return this.UDescription;
    }

    public void setUDescription(String UDescription)
    {
        this.UDescription = UDescription;
    }

    public String getUDefaultValue()
    {
        return this.UDefaultValue;
    }

    public void setUDefaultValue(String UDefaultValue)
    {
        this.UDefaultValue = UDefaultValue;
    }

    public Boolean getURequired()
    {
        return this.URequired;
    }

    public void setURequired(Boolean URequired)
    {
        this.URequired = URequired;
    }

    public Boolean getUInclude()
    {
        return this.UInclude;
    }

    public void setUInclude(Boolean UInclude)
    {
        this.UInclude = UInclude;
    }

    public String getUPrefix()
    {
        return this.UPrefix;
    }

    public void setUPrefix(String UPrefix)
    {
        this.UPrefix = UPrefix;
    }

    public String getUPrefixSeparator()
    {
        return this.UPrefixSeparator;
    }

    public void setUPrefixSeparator(String UPrefixSeparator)
    {
        this.UPrefixSeparator = UPrefixSeparator;
    }

    public Boolean getUEncrypt()
    {
        return this.UEncrypt;
    }

    public void setUEncrypt(Boolean UEncrypt)
    {
        this.UEncrypt = UEncrypt;
    }
    
    public Integer getUOrder()
    {
        return UOrder;
    }

    public void setUOrder(Integer uOrder)
    {
        UOrder = uOrder;
    }


    public String getUGroupName()
	{
		return UGroupName;
	}

	public void setUGroupName(String uGroupName)
	{
		UGroupName = uGroupName;
	}

	public Integer getUGroupOrder()
	{
		return UGroupOrder;
	}

	public void setUGroupOrder(Integer uGroupOrder)
	{
		UGroupOrder = uGroupOrder;
	}
	
	public Boolean getUProtected()
    {
        return UProtected;
    }
	
	public void setUProtected(Boolean uProtected)
    {
	    UProtected = uProtected;
    }
	
	@Override
    public String toString()
    {
        StringBuffer str = new StringBuffer();

        str.append("Parameter Name:").append(UName).append("\n");
        str.append("Parameter Description:").append(UDescription).append("\n");
        str.append("Parameter Default Value :").append(UDefaultValue).append("\n");
        str.append("Parameter is Protected :").append(UProtected).append("\n");

        return str.toString();
    }

    public String ugetIndexContent()
    {
        return UName + " " + UDescription;
    }

}
