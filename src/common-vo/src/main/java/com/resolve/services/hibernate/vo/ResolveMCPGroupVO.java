package com.resolve.services.hibernate.vo;

import java.util.Collection;

import com.resolve.services.interfaces.VO;

public class ResolveMCPGroupVO extends VO
{
    private static final long serialVersionUID = 48764745454608721L;
    
    private String UGroupName;
    
    private Collection<ResolveMCPClusterVO> children;

    public String getUGroupName()
    {
        return UGroupName;
    }

    public void setUGroupName(String groupName)
    {
        this.UGroupName = groupName;
    }

    public void setChildren(Collection<ResolveMCPClusterVO> children)
    {
        this.children = children;
    }

    public Collection<ResolveMCPClusterVO> getChildren()
    {
        return children;
    }
}
