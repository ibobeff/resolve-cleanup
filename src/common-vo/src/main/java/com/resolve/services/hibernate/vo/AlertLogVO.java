/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class AlertLogVO extends VO
{
    private static final long serialVersionUID = 4669538861839130608L;
    
    private String USeverity;
	private String UComponent;
	private String UType;
	private String UMessage;

	
	public AlertLogVO()
	{
	}

	public AlertLogVO(String sys_id)
	{
		this.setSys_id(sys_id);
	}

	public String getUSeverity()
	{
		return this.USeverity;
	}

	public void setUSeverity(String USeverity)
	{
		this.USeverity = USeverity;
	}

	public String getUComponent()
	{
		return this.UComponent;
	}

	public void setUComponent(String UComponent)
	{
		this.UComponent = UComponent;
	}

	public String getUType()
	{
		return this.UType;
	}

	public void setUType(String UType)
	{
		this.UType = UType;
	}

	public String getUMessage()
	{
		return this.UMessage;
	}

	public void setUMessage(String UMessage)
	{
		this.UMessage = UMessage;
	}

	
} // AlertLog
