/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;

public class EmailAddressVO extends GatewayVO
{
    private static final long serialVersionUID = -8024699208143898355L;
	private String UEmailAddress;
    private String UEmailPassword;

    // object referenced by
    public EmailAddressVO()
    {
    } // EmailAddress
    
    public EmailAddressVO(boolean secured)
    {
    	if (secured) {
    		UEmailPassword = VO.getStars()/*STARS*/;
    	}
    }
    
    @MappingAnnotation(columnName = "EMAILADDRESS")
    public String getUEmailAddress()
    {
        return this.UEmailAddress;
    } // getUEmailAddress

    public void setUEmailAddress(String UEmailAddress)
    {
        this.UEmailAddress = UEmailAddress;
    } // setUEmailAddress

    @MappingAnnotation(columnName = "EMAILPASSWORD")
    public String getUEmailPassword()
    {
        return this.UEmailPassword;
    } // getUEmailPassword

    public void setUEmailPassword(String UEmailPassword)
    {
        this.UEmailPassword = UEmailPassword;
    } // setUEmailPassword

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UEmailAddress == null) ? 0 : UEmailAddress.hashCode());
        result = prime * result + ((UEmailPassword == null) ? 0 : UEmailPassword.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
//        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        EmailAddressVO other = (EmailAddressVO) obj;
        if (UEmailAddress == null)
        {
            if (other.UEmailAddress != null) return false;
        }
        else if (!UEmailAddress.equals(other.UEmailAddress)) return false;
        if (UEmailPassword == null)
        {
            if (other.UEmailPassword != null) return false;
        }
        else if (!UEmailPassword.equals(other.UEmailPassword)) return false;
        return true;
    }

    @Override
    public String getUniqueId()
    {
       return this.UEmailAddress;
    }
    
    
}
