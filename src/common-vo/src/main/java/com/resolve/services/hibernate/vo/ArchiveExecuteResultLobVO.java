/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ArchiveExecuteResultLobVO extends VO
{
    private static final long serialVersionUID = 3371574478161887559L;
    
    private String UCommand;
    private String URaw;

    public ArchiveExecuteResultLobVO(String UCommand, String URaw)
    {
        this.UCommand = UCommand;
        this.URaw = URaw;
    }

    public ArchiveExecuteResultLobVO()
    {
    }

    public ArchiveExecuteResultLobVO usetsys_id(String sys_id)
    {
        this.setSys_id(sys_id);
        return this;
    }

    public String getURaw()
    {
        return this.URaw;
    }

    public void setURaw(String URaw)
    {
        this.URaw = URaw;
    }

    public String getUCommand()
    {
        return this.UCommand;
    }

    public void setUCommand(String UCommand)
    {
        this.UCommand = UCommand;
    }


}
