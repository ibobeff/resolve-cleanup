/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.List;

import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

public class GroupsVO extends VO implements Comparable<GroupsVO>
{
    private static final long serialVersionUID = 3182962728243928277L;

    private String UName;
    private String UDescription;
    
    private Boolean UIsLinkToTeam = new Boolean(false);
    private Boolean UHasExternalLink = new Boolean(false);

    // object referenced by
    private Collection<UserGroupRelVO> userGroupRels;
    private Collection<GroupRoleRelVO> groupRoleRels;
    private Collection<OrgGroupRelVO> orgGroupRels;
    
    //for display purpose only
    private String roles;
    private List<RolesVO> groupRoles;
    private List<UsersVO> groupUsers;
    private List<OrgsVO> groupOrgs;

    public GroupsVO()
    {
    }
    
    public boolean validate()
    {
        boolean valid = true;
        
        if(StringUtils.isEmpty(getUName()) || (StringUtils.isNotBlank(getUName()) && getUName().equalsIgnoreCase(VO.STRING_DEFAULT)))
        {
            valid = false;
            throw new RuntimeException("Group name cannot be empty.");
        }
        
        valid = getUName().matches(HibernateConstants.REGEX_LDAP_AD_DN);
        if(!valid)
        {
            throw new RuntimeException("Group name can not have (/) (\\) ([) (]) (:) (;) (|) (=) (+) (*) (?) (<) (>) (\") (@).");
        }
        
        if(groupRoles == null || groupRoles.size() == 0)
        {
            throw new RuntimeException("Group must have atleast 1 role.");
        }
        
        
        return valid;
    }

    public String toString()
    {
        return this.UName;
    } // toString

    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    public String getUDescription()
    {
        return this.UDescription;
    }

    public void setUDescription(String UDescription)
    {
        this.UDescription = UDescription;
    }

    public Collection<UserGroupRelVO> getUserGroupRels()
    {
        return userGroupRels;
    }

    public void setUserGroupRels(Collection<UserGroupRelVO> userGroupRels)
    {
        this.userGroupRels = userGroupRels;
    }

    public Collection<GroupRoleRelVO> getGroupRoleRels()
    {
        return groupRoleRels;
    }

    public void setGroupRoleRels(Collection<GroupRoleRelVO> groupRoleRels)
    {
        this.groupRoleRels = groupRoleRels;
    }
    
    public Collection<OrgGroupRelVO> getOrgGroupRels()
    {
        return orgGroupRels;
    }
    
    public void setOrgGroupRels(Collection<OrgGroupRelVO> orgGroupRels)
    {
        this.orgGroupRels = orgGroupRels;
    }
    
    public Boolean getUIsLinkToTeam()
    {
        Boolean result = UIsLinkToTeam;
        if(result == null)
        {
            result = false;
        }
        
        return result;
    }


    public void setUIsLinkToTeam(Boolean uIsLinkToTeam)
    {
        UIsLinkToTeam = uIsLinkToTeam;
    }
    
    public Boolean getUHasExternalLink()
    {
        Boolean result = UHasExternalLink;
        if(result == null)
        {
            result = false;
        }
        
        return result;
    }
    
    public void setUHasExternalLink(Boolean uHasExternalLink)
    {
        UHasExternalLink = uHasExternalLink;
    }

    public String getRoles()
    {
        return roles;
    }

    public void setRoles(String roles)
    {
        this.roles = roles;
    }

    public List<RolesVO> getGroupRoles()
    {
        return groupRoles;
    }

    public void setGroupRoles(List<RolesVO> groupRoles)
    {
        this.groupRoles = groupRoles;
    }

    public List<UsersVO> getGroupUsers()
    {
        return groupUsers;
    }

    public void setGroupUsers(List<UsersVO> groupUsers)
    {
        this.groupUsers = groupUsers;
    }

    public List<OrgsVO> getGroupOrgs()
    {
        return groupOrgs;
    }
    
    public void setGroupOrgs(List<OrgsVO> groupOrgs)
    {
        this.groupOrgs = groupOrgs;
    }
    
    public int compareTo(GroupsVO that)
    {
        final int EQUAL = 0;
//      final int BEFORE = -1;
//      final int AFTER = 1;
      int result = EQUAL;
      
      //this optimization is usually worthwhile
      if ( this == that ) 
          result = EQUAL;
      else
      {
          String roleThis = this.getUName();
          String roleThat = that.getUName();
          
          if(StringUtils.isNotEmpty(roleThis) && StringUtils.isNotBlank(roleThat))
          {
              result = roleThis.compareTo(roleThat);
          }
          else
          {
              roleThis = this.getSys_id();
              roleThat = that.getSys_id();
              
              result = roleThis.compareTo(roleThat);
          }
      }
      
      return result;
    }


    @Override
    public boolean equals(Object aThat)
    {
        boolean result = false;
        
        if (this == aThat) 
            result = true;
        else if (!(aThat instanceof GroupsVO)) 
            result = false;
        else 
        {
            GroupsVO that = (GroupsVO) aThat;
            result = this.getUName().equalsIgnoreCase(that.getUName());
        }
        
        return result;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 43;

        result = prime * result + ((getUName() == null) ? 0 : getUName().hashCode());

        return result;
    }
    
    

}
