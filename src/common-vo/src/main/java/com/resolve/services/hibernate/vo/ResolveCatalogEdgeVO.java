/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveCatalogEdgeVO extends VO
{
    private static final long serialVersionUID = 5472274247622427909L;

    private ResolveCatalogVO catalog;//may have to change to String sysId to avoid the cyclic reference. But test if hibernate creates issue
    private ResolveCatalogNodeVO sourceNode;
    private ResolveCatalogNodeVO destinationNode;
    private Integer UOrder;

    public ResolveCatalogEdgeVO() {}

    public Integer getUOrder()
    {
        return UOrder;
    }

    public void setUOrder(Integer uOrder)
    {
        UOrder = uOrder;
    }

    public ResolveCatalogNodeVO getSourceNode()
    {
        return sourceNode;
    }

    public void setSourceNode(ResolveCatalogNodeVO sourceNode)
    {
        this.sourceNode = sourceNode;
    }

    public ResolveCatalogNodeVO getDestinationNode()
    {
        return destinationNode;
    }

    public void setDestinationNode(ResolveCatalogNodeVO destinationNode)
    {
        this.destinationNode = destinationNode;
    }

    public ResolveCatalogVO getCatalog()
    {
        return catalog;
    }

    public void setCatalog(ResolveCatalogVO catalog)
    {
        this.catalog = catalog;
    }
}
