package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;

public class PushGatewayFilterAttrVO extends VO
{
    private static final long serialVersionUID = 1L;
    
    private String UName;
    private String UValue;
//    private String UPushFilterId;
    private enum fieldVals { ublocking,ussl,uport, uuri, u };

	// object reference
    private PushGatewayFilterVO pushGatewayFilter;

    @MappingAnnotation(columnName="NAME")
    public String getUName()
    {
        return UName;
    }
    
    public void setUName(String uName)
    {
        UName = uName;
    }
    
    @MappingAnnotation(columnName="VALUE")
    public String getUValue()
    {
        return UValue;
    }
    
    public void setUValue(String uValue)
    {
        UValue = uValue;
    }

//    @MappingAnnotation(columnName="PUSH_FILTER_ID")
//    public String getUPushFilterId() {
//		return UPushFilterId;
//	}
//
//	public void setUPushFilterId(String uPushFilterId) {
//		UPushFilterId = uPushFilterId;
//	}
    
    public PushGatewayFilterVO getPushGatewayFilter()
    {
        return pushGatewayFilter;
    }

    public void setPushGatewayFilter(PushGatewayFilterVO pushGatewayFilter)
    {
        this.pushGatewayFilter = pushGatewayFilter;
        
        if (pushGatewayFilter != null) {
            Collection<PushGatewayFilterAttrVO> coll = pushGatewayFilter.getAttrsCol();
            
            if (coll == null) {
                coll = new HashSet<PushGatewayFilterAttrVO>();
                coll.add(this);

                pushGatewayFilter.setAttrsCol(coll);
            }
        }
    }

} // class PushGatewayFilterAttrVO
