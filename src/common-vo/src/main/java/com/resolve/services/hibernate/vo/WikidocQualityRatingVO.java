/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;

public class WikidocQualityRatingVO  extends VO
{
    private static final long serialVersionUID = -5258875557802721765L;
    
    private Integer ustarCount;
	private String ufeedback;
	
    // object references
	private WikiDocumentVO wikidoc; 
	
	public WikidocQualityRatingVO()
	{
	}

    public WikiDocumentVO getWikidoc()
    {
        return wikidoc;
    }

    public void setWikidoc(WikiDocumentVO wikidoc)
    {
        this.wikidoc = wikidoc;

        if (wikidoc != null)
        {
            Collection<WikidocQualityRatingVO> coll = wikidoc.getWikidocQualityRating();
            if (coll == null)
            {
				coll = new HashSet<WikidocQualityRatingVO>();
	            coll.add(this);
	            
                wikidoc.setWikidocQualityRating(coll);
            }
        }
    }

	public Integer getUstarCount()
	{
		return this.ustarCount;
	}

	public void setUstarCount(Integer ustarCount)
	{
		this.ustarCount = ustarCount;
	}

    public String getUfeedback()
    {
        return this.ufeedback;
    }

    public void setUfeedback(String ufeedback)
    {
        this.ufeedback = ufeedback;
    }
	
}
