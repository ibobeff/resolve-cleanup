/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.menu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MenuItemDTO implements  Serializable
{
    private static final long serialVersionUID = -6522808623241761072L;
    
    private String menuId;
    private String modelName;
    private String location;
    private String text;
    private String color;
    private String textColor;
    private String group;
    private int order;
    private List<String> sets;
    private List<MenuItemDTO> items;

    public MenuItemDTO()
    {
    }

    public String getMenuId()
    {
        return menuId;
    }

    public void setMenuId(String menuId)
    {
        this.menuId = menuId;
    }

    public String getModelName()
    {
        return modelName;
    }

    public void setModelName(String modelName)
    {
        this.modelName = modelName;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public String getTextColor()
    {
        return textColor;
    }

    public void setTextColor(String textColor)
    {
        this.textColor = textColor;
    }

    public List<String> getSets()
    {
        return sets;
    }

    public void setSets(List<String> sets)
    {
        this.sets = sets;
    }

    public List<MenuItemDTO> getItems()
    {
        return items;
    }

    public void setItems(List<MenuItemDTO> items)
    {
        this.items = items;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public void fromSection(Map<String, String> section)
    {
        this.setText(section.get("displayName"));
        this.setMenuId(section.get("value"));
        this.setColor(section.get("color"));
        this.setTextColor(section.get("textColor"));
        this.setOrder(Integer.parseInt(section.get("order")));
    }

    public void fromMap(Map<String, String> menuItem)
    {
        this.setText(menuItem.get("name"));
        this.setMenuId(menuItem.get("id"));
        this.setLocation(menuItem.get("query"));
        this.setGroup(menuItem.get("group"));
        if (".".equals(this.group)) this.setGroup("");
        this.setOrder(Integer.parseInt(menuItem.get("order")));
    }

    public void addItem(MenuItemDTO item)
    {
        if (this.items == null) this.items = new ArrayList<MenuItemDTO>();
        this.items.add(item);
    }

    public void addSet(MenuSetModel menuSet)
    {
        if (this.sets == null) this.sets = new ArrayList<String>();
        this.sets.add(menuSet.getMenuSetID());
    }

    public String getGroup()
    {
        return group;
    }

    public void setGroup(String group)
    {
        this.group = group;
    }

    public int getOrder()
    {
        return order;
    }

    public void setOrder(int order)
    {
        this.order = order;
    }

}
