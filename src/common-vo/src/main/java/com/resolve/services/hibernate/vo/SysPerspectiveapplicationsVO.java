/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class SysPerspectiveapplicationsVO extends VO  implements Comparable<SysPerspectiveapplicationsVO>
{
    private static final long serialVersionUID = -8270136095606658834L;
    
    private String value;
    private Integer sequence;
    
    //for UI only
    private String displayName;

    // object referenced by
    private SysPerspectiveVO sysPerspective; // parentId;

    public SysPerspectiveapplicationsVO()
    {
    }

    public String getValue()
    {
        return this.value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public Integer getSequence()
    {
        return this.sequence;
    }

    public void setSequence(Integer sequence)
    {
        this.sequence = sequence;
    }

    public SysPerspectiveVO getSysPerspective()
    {
        return sysPerspective;
    }

    public void setSysPerspective(SysPerspectiveVO sysPerspective)
    {
        this.sysPerspective = sysPerspective;
//        if (sysPerspective != null)
//        {
//            Collection<SysPerspectiveapplicationsVO> coll = sysPerspective.getSysPerspectiveapplications();
//            if (coll == null)
//            {
//				coll = new HashSet<SysPerspectiveapplicationsVO>();
//	            coll.add(this);
//	            
//                sysPerspective.setSysPerspectiveapplications(coll);
//            }
//        }
    }
    
    public int compareTo(SysPerspectiveapplicationsVO that)
    {
        final int EQUAL = 0;
        final int BEFORE = -1;
        final int AFTER = 1;
        int result = EQUAL;

        //this optimization is usually worthwhile
        if (this == that)
            result = EQUAL;
        else
        {
            long orderThis = this.getSequence() != null ? this.getSequence().longValue() : 0;
            long orderThat = that.getSequence() != null ? that.getSequence().longValue() : 0;

            if (orderThis > orderThat)
            {
                result = AFTER;
            }
            else
            {
                result = BEFORE;
            }
        }

        return result;
    }
    
    
    @Override
    public boolean equals(Object aThat)
    {
        boolean result = false;
        
        if (this == aThat) 
            result = true;
        else if (!(aThat instanceof SysPerspectiveapplicationsVO)) 
            result = false;
        else 
        {
            SysPerspectiveapplicationsVO that = (SysPerspectiveapplicationsVO) aThat;
            result = this.getValue().equalsIgnoreCase(that.getValue());
        }
        
        return result;
    }

    @Override
    public int hashCode()
    {
        final int prime = 47;
        int result = 43;

        result = prime * result + ((getSys_id() == null) ? 0 : getSys_id().hashCode());

        return result;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }
    
    

}
