package com.resolve.services.hibernate.vo;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import com.resolve.services.interfaces.VO;

public class SIRActivityRuntimeDataVO extends VO {
    
    private static final long serialVersionUID = -8733336268307084187L;
    
    private static final Integer LAST_POSITION = Integer.MAX_VALUE;
    
    private String assignee;                        
    private String status;
    private Integer position;
    
    // object references
    private SIRPhaseMetaDataVO phaseMetaData;        
    private SIRActivityMetaDataVO activityMetaData;   
    private ResolveSecurityIncidentVO referencedRSI;  
    private UsersVO assignedUser;
    
    public SIRActivityRuntimeDataVO() {}
    
    public String getAssignee() {
        return assignee;
    }
    
    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }
    
    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    
    public Integer getPosition() {
        return position;
    }
    
    public Integer ugetPosition() {
        if (getPosition() != null) {
            return getPosition();
        } else {
            return LAST_POSITION;
        }
    }
    
    public void setPosition(Integer position) {
        this.position = position;
    }
    
    public SIRPhaseMetaDataVO getPhaseMetaData() {
        return phaseMetaData;
    }
    
    public void setPhaseMetaData(SIRPhaseMetaDataVO phaseMetaData) {
        this.phaseMetaData = phaseMetaData;
    }
    
    public SIRActivityMetaDataVO getActivityMetaData() {
        return activityMetaData;
    }
    
    public void setActivityMetaData(SIRActivityMetaDataVO activityMetaData) {
        this.activityMetaData = activityMetaData;
    }
    
    public ResolveSecurityIncidentVO getReferencedRSI() {
        return referencedRSI;
    }
    
    public void setReferencedRSI(ResolveSecurityIncidentVO referencedRSI) {
        this.referencedRSI = referencedRSI;
    }
    
    public UsersVO getAssignedUser() {
        return assignedUser;
    }
    
    public void setAssignedUser(UsersVO assignedUser) {
        this.assignedUser = assignedUser;
    }
    
    @Override
    public String toString() {
        return "SIR Activity Runtime Data VO [" +
               (isNotBlank(getSys_id()) ? "Sys Id: " + getSys_id() + ", " : "") + 
               "Sys Org: " + (isNotBlank(getSysOrg()) ? getSysOrg() : "") +
               ", Assignee: " + getAssignee() + ", Status: " + getStatus() + ", Position: " + ugetPosition() +
               ", Phase Meta Data: " + getPhaseMetaData() + 
               ", Activity Meta Data: " + getActivityMetaData() + 
               ", Referenced Rsolve Security Incident: " + getReferencedRSI() + 
               ", Assigned User: " + (getAssignedUser() != null ? getAssignedUser() : "") +
               "]";
    }
}
