/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.List;

import com.resolve.services.interfaces.VO;

public class ResolveAppsVO extends VO
{
    private static final long serialVersionUID = -4406772995588791375L;

    private String UAppName;
    private String UDescription;
    
    private Collection<ResolveAppControllerRelVO> resolveAppControllerRels;
    private Collection<ResolveAppRoleRelVO> resolveAppRoleRels;
    private Collection<ResolveAppOrganizationRelVO> resolveAppOrgRel;
    
    //for UI only
    //for the grid
    private String roles; 
    
    //for editing/saving
    private List<ResolveControllersVO> appControllers;
    private List<RolesVO> appRoles;
    private List<OrganizationVO> appOrgs;
    

    public ResolveAppsVO()
    {
    }

    public String getUAppName()
    {
        return UAppName;
    }

    public void setUAppName(String uAppName)
    {
        UAppName = uAppName;
    }

    public String getUDescription()
    {
        return UDescription;
    }

    public void setUDescription(String uDescription)
    {
        UDescription = uDescription;
    }
    
    public Collection<ResolveAppControllerRelVO> getResolveAppControllerRels()
    {
        return resolveAppControllerRels;
    }

    public void setResolveAppControllerRels(Collection<ResolveAppControllerRelVO> resolveAppControllerRels)
    {
        this.resolveAppControllerRels = resolveAppControllerRels;
    }
    
    public Collection<ResolveAppRoleRelVO> getResolveAppRoleRels()
    {
        return resolveAppRoleRels;
    }

    public void setResolveAppRoleRels(Collection<ResolveAppRoleRelVO> resolveAppRoleRels)
    {
        this.resolveAppRoleRels = resolveAppRoleRels;
    }

    public String getRoles()
    {
        return roles;
    }

    public void setRoles(String roles)
    {
        this.roles = roles;
    }
    public Collection<ResolveAppOrganizationRelVO> getResolveAppOrgRel()
    {
        return resolveAppOrgRel;
    }

    public void setResolveAppOrgRel(Collection<ResolveAppOrganizationRelVO> resolveAppOrgRel)
    {
        this.resolveAppOrgRel = resolveAppOrgRel;
    }

    public List<ResolveControllersVO> getAppControllers()
    {
        return appControllers;
    }

    public void setAppControllers(List<ResolveControllersVO> appControllers)
    {
        this.appControllers = appControllers;
    }

    public List<RolesVO> getAppRoles()
    {
        return appRoles;
    }

    public void setAppRoles(List<RolesVO> appRoles)
    {
        this.appRoles = appRoles;
    }

    public List<OrganizationVO> getAppOrgs()
    {
        return appOrgs;
    }

    public void setAppOrgs(List<OrganizationVO> appOrgs)
    {
        this.appOrgs = appOrgs;
    }

    

} // AlertLog
