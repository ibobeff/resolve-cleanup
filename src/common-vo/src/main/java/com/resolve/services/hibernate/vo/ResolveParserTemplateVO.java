/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;


public class ResolveParserTemplateVO extends VO 
{
    private static final long serialVersionUID = -6570583778459044004L;
    
    private String UType;
    private String UMatchRegex;
    private String UNamePos;
    private String UValuePos;

    // object references
    private ResolveParserVO parser; // UParser;
	
    // object referenced by
	
    public ResolveParserTemplateVO()
    {
    }

    public ResolveParserVO getParser()
    {
        return parser;
    }

    public void setParser(ResolveParserVO parser)
    {
        this.parser = parser;

        if (parser != null)
        {
            Collection<ResolveParserTemplateVO> coll = parser.getResolveParserTemplates();
            if (coll == null)
            {
				coll = new HashSet<ResolveParserTemplateVO>();
	            coll.add(this);
	            
	            parser.setResolveParserTemplates(coll);
            }
        }
    }

    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    public String getUMatchRegex()
    {
        return this.UMatchRegex;
    }

    public void setUMatchRegex(String UMatchRegex)
    {
        this.UMatchRegex = UMatchRegex;
    }

    public String getUNamePos()
    {
        return this.UNamePos;
    }

    public void setUNamePos(String UNamePos)
    {
        this.UNamePos = UNamePos;
    }

    public String getUValuePos()
    {
        return this.UValuePos;
    }

    public void setUValuePos(String UValuePos)
    {
        this.UValuePos = UValuePos;
    }

}
