/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import com.resolve.services.interfaces.CdataProperty;
import com.resolve.services.interfaces.VO;


public class MetaTableViewVO extends VO implements CdataProperty
{
    private static final long serialVersionUID = 873815005547241960L;

    public final static String RESOURCE_TYPE = "metatableview";
    
    private String UName;
    private String UDisplayName;
    private String UType;
    private Boolean UIsGlobal;
    private String UUser;
    private String UMetaFormLink;
    private String UMetaNewLink;
    private String UTarget;
    private String UParams;
    //private Boolean UIsDefaultRole;
    
    private String createFormId;
    private String editFormId;
    
    // object references
    private MetaTableVO metaTable;  
    //private MetaStyle metaStyle;
    private MetaControlVO metaControl;
    private MetaAccessRightsVO metaAccessRights;
    
    private MetaTableViewVO parentMetaTableView;
    private Collection<MetaTableViewVO> childMetaTableViews;
    
    // object referenced by
    private Collection<MetaTableViewFieldVO> metaTableViewFields;
    
    public MetaTableViewVO()
    {
    }
    
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    public String getUDisplayName()
    {
        return this.UDisplayName;
    }

    public void setUDisplayName(String UDisplayName)
    {
        this.UDisplayName = UDisplayName;
    }
    
    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }
    
    public Boolean getUIsGlobal()
    {
        return this.UIsGlobal;
    } 

    public Boolean ugetUIsGlobal()
    {
        if (this.UIsGlobal == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsGlobal;
        }
    } 
    
    public void setUIsGlobal(Boolean UIsGlobal)
    {
        this.UIsGlobal = UIsGlobal;
    }
    
    public String getUUser()
    {
        return this.UUser;
    }

    public void setUUser(String UUser)
    {
        this.UUser = UUser;
    }
    
    public String getUMetaFormLink()
    {
        return this.UMetaFormLink;
    }

    public void setUMetaFormLink(String UMetaFormLink)
    {
        this.UMetaFormLink = UMetaFormLink;
    }
    
    public String getUMetaNewLink()
    {
        return this.UMetaNewLink;
    }

    public void setUMetaNewLink(String UMetaNewLink)
    {
        this.UMetaNewLink = UMetaNewLink;
    }
    
    public String getUTarget()
    {
        return this.UTarget;
    }

    public void setUTarget(String UTarget)
    {
        this.UTarget = UTarget;
    }
    
    public String getUParams()
    {
        return this.UParams;
    }

    public void setUParams(String UParams)
    {
        this.UParams = UParams;
    }
    
    public MetaTableVO getMetaTable()
    {
        return metaTable;
    }

    public void setMetaTable(MetaTableVO metaTable)
    {
        this.metaTable = metaTable;

        if (metaTable != null)
        {
            Collection<MetaTableViewVO> coll = metaTable.getMetaTableViews();
            if (coll == null)
            {
                coll = new HashSet<MetaTableViewVO>();
                coll.add(this);
                
                metaTable.setMetaTableViews(coll);
            }
        }
    }
    
    public MetaControlVO getMetaControl()
    {
        return metaControl;
    }

    public void setMetaControl(MetaControlVO metaControl)
    {
        this.metaControl = metaControl;
    }
    
    public MetaAccessRightsVO getMetaAccessRights()
    {
        return metaAccessRights;
    }

    public void setMetaAccessRights(MetaAccessRightsVO metaAccessRights)
    {
        this.metaAccessRights = metaAccessRights;
    }
    
    public Collection<MetaTableViewFieldVO> getMetaTableViewFields()
    {
        return this.metaTableViewFields;
    }

    public void setMetaTableViewFields(Collection<MetaTableViewFieldVO> metaTableViewFields)
    {
        this.metaTableViewFields = metaTableViewFields;
    }
    
    
    public MetaTableViewVO getParentMetaTableView()
    {
        return parentMetaTableView;
    }

    public void setParentMetaTableView(MetaTableViewVO metaTableView)
    {
        this.parentMetaTableView = metaTableView;
        
        if (metaTableView != null)
        {
            Collection<MetaTableViewVO> coll = metaTableView.getMetaTableViews();
            if (coll == null)
            {
                coll = new HashSet<MetaTableViewVO>();
                coll.add(this);
                
                metaTableView.setMetaTableViews(coll);
            }
        }

    }

    public Collection<MetaTableViewVO> getMetaTableViews()
    {
        return this.childMetaTableViews;
    }

    public void setMetaTableViews(Collection<MetaTableViewVO> childMetaTableViews)
    {
        this.childMetaTableViews = childMetaTableViews;
    }

    public List<String> ugetCdataProperty()
    {
        List<String> list = new ArrayList<String>();
        list.add("UMetaFormLink");
        list.add("UMetaNewLink");

        return list;
    }

    public String getCreateFormId()
    {
        return createFormId;
    }

    public void setCreateFormId(String createFormId)
    {
        this.createFormId = createFormId;
    }

    public String getEditFormId()
    {
        return editFormId;
    }

    public void setEditFormId(String editFormId)
    {
        this.editFormId = editFormId;
    }
  
}
