/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import com.resolve.services.interfaces.VO;

public class RoleWikidocHomepageRelVO extends VO
{
    private static final long serialVersionUID = 2236407031422301267L;

    // object references
	private RolesVO role;
	private WikiDocumentVO wikidoc;
	
    // object referenced by
	
	public RoleWikidocHomepageRelVO()
	{
	}

	public RolesVO getRole()
	{
		return role;
	}

	public void setRole(RolesVO role)
	{
		this.role = role;
		
        if (role != null)
        {
            List<RoleWikidocHomepageRelVO> coll = role.getRoleWikidocHomepageRels();
            if (coll == null)
            {
				coll = new ArrayList<RoleWikidocHomepageRelVO>();
	            coll.add(this);
	            
                role.setRoleWikidocHomepageRels(coll);
            }
        }
	}

	
	public WikiDocumentVO getWikidoc()
	{
		return this.wikidoc;
	}

	public void setWikidoc(WikiDocumentVO wikidoc)
	{
		this.wikidoc = wikidoc;
        if (wikidoc != null)
        {
            Collection<RoleWikidocHomepageRelVO> coll = wikidoc.getRoleWikidocHomepageRels();
            if (coll == null)
            {
				coll = new HashSet<RoleWikidocHomepageRelVO>();
	            coll.add(this);
	            
                wikidoc.setRoleWikidocHomepageRels(coll);
            }
        }
	}
}
