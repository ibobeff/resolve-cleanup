package com.resolve.services.hibernate.vo;

import java.util.Collection;

import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
public class RRSchemaVO extends VO
{
    private static final long serialVersionUID = 5824047264484182288L;
	private String name;
    private Integer order;
    private String gatewayQueues;
    private String jsonFields;
    private String description;
    private String source;
    private Collection<RRRuleVO> rules;
    
    public RRSchemaVO()
    {
        super();
    }
    
    public RRSchemaVO(RRSchemaVO rrSchemaVO)
    {
        super(rrSchemaVO);
        
        // Shallow copy
        setName(rrSchemaVO.getName());
        setOrder(rrSchemaVO.getOrder());
        setGatewayQueues(rrSchemaVO.getGatewayQueues());
        setJsonFields(rrSchemaVO.getJsonFields());
        setDescription(rrSchemaVO.getDescription());
        setSource(rrSchemaVO.getSource());
        setRules(rrSchemaVO.getRules());
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    public Integer getOrder()
    {
        return order;
    }
    public void setOrder(Integer order)
    {
        this.order = order;
    }
    public String getGatewayQueues()
    {
        return gatewayQueues;
    }
    public void setGatewayQueues(String gatewayQueues)
    {
        this.gatewayQueues = gatewayQueues;
    }
    public String getJsonFields()
    {
        return jsonFields;
    }
    public void setJsonFields(String jsonFields)
    {
        this.jsonFields = jsonFields;
    }
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    public String getSource()
    {
        return source;
    }
    public void setSource(String source)
    {
        this.source = source;
    }
    public Collection<RRRuleVO> getRules()
    {
        return rules;
    }
    public void setRules(Collection<RRRuleVO> rules)
    {
        this.rules = rules;
    }
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((gatewayQueues == null) ? 0 : gatewayQueues.hashCode());
        result = prime * result + ((jsonFields == null) ? 0 : jsonFields.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((order == null) ? 0 : order.hashCode());
        result = prime * result + ((rules == null) ? 0 : rules.hashCode());
        result = prime * result + ((source == null) ? 0 : source.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        RRSchemaVO other = (RRSchemaVO) obj;
        if (description == null)
        {
            if (other.description != null) return false;
        }
        else if (!description.equals(other.description)) return false;
        if (gatewayQueues == null)
        {
            if (other.gatewayQueues != null) return false;
        }
        else if (!gatewayQueues.equals(other.gatewayQueues)) return false;
        if (jsonFields == null)
        {
            if (other.jsonFields != null) return false;
        }
        else if (!jsonFields.equals(other.jsonFields)) return false;
        if (name == null)
        {
            if (other.name != null) return false;
        }
        else if (!name.equals(other.name)) return false;
        if (order == null)
        {
            if (other.order != null) return false;
        }
        else if (!order.equals(other.order)) return false;
        if (rules == null)
        {
            if (other.rules != null) return false;
        }
        else if (!rules.equals(other.rules)) return false;
        if (source == null)
        {
            if (other.source != null) return false;
        }
        else if (!source.equals(other.source)) return false;
        return true;
    }
    
    @Override
    public String toString()
    {
        return "Schema : Name = " + getName() + ", Order = " + getOrder();
    }
}
