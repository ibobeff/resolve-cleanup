/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;


import com.resolve.services.interfaces.VO;

public class ConfigLDAPVO extends VO
{
    private static final long serialVersionUID = -539699221536711520L;
    
    private Boolean UActive;
    private String UMode;
    private Integer UVersion;
    private String UIpAddress;
    private Integer UPort;
    private Boolean USsl;
    private String UCryptType;
    private String UCryptPrefix;
    private String UBindDn;
    private String UBindPassword;
    private String UDefaultDomain;    
    private String UUidAttribute;
    private String UPasswordAttribute;
    private Boolean UFallback;
    private Boolean groupRequired;
    private String UDomain;// this is mandatory from the UI and should be unique
    private String UBaseDNList;//semi-colon separated string
    private String UProperties;//data that is in the ldap.properties file
    private Boolean UIsDefault;
    private String UGateway;
    
    //active directory conf belongs to this organization
    private OrganizationVO belongsToOrganization;

    public ConfigLDAPVO()
    {
    }
    
    public Integer getUPort()
    {
        return UPort;
    }

    public void setUPort(Integer uPort)
    {
        UPort = uPort;
    }

    public Boolean getUActive()
    {
        return UActive;
    }
    
    public Boolean ugetUActive()
    {
        Boolean result = false;
        if(getUActive() != null)
        {
            result = getUActive();
        }
        
        return result;
    }

    public void setUActive(Boolean uActive)
    {
        UActive = uActive;
    }

    public String getUDomain()
    {
        return UDomain;
    }

    public void setUDomain(String uDomain)
    {
        UDomain = uDomain;
    }

    public String getUIpAddress()
    {
        return UIpAddress;
    }

    public void setUIpAddress(String uIpAddress)
    {
        UIpAddress = uIpAddress;
    }
    
    public Boolean getUSsl()
    {
        return USsl;
    }
    
    public Boolean ugetUSsl()
    {
        Boolean result = false;
        if(getUSsl() != null)
        {
            result = getUSsl();
        }
        
        return result;
    }

    public void setUSsl(Boolean uSsl)
    {
        USsl = uSsl;
    }

    public Integer getUVersion()
    {
        return UVersion;
    }

    public void setUVersion(Integer uVersion)
    {
        UVersion = uVersion;
    }

    public Boolean getUFallback()
    {
        return UFallback;
    }
    
    public Boolean ugetUFallback()
    {
        Boolean result = true;
        if(getUFallback() != null)
        {
            result = getUFallback();
        }
        
        return result;
    }

    public void setUFallback(Boolean uFallback)
    {
        UFallback = uFallback;
    }
    
    public String getUMode()
    {
        return UMode;
    }

    public void setUMode(String uMode)
    {
        UMode = uMode;
    }
    
    public String getUBindDn()
    {
        return UBindDn;
    }

    public void setUBindDn(String uBindDn)
    {
        UBindDn = uBindDn;
    }

    public String getUBindPassword()
    {
        return UBindPassword;
    }

    public void setUBindPassword(String uBindPassword)
    {
        UBindPassword = uBindPassword;
    }

    public String getUBaseDNList()
    {
        return UBaseDNList;
    }

    public void setUBaseDNList(String uBaseDNList)
    {
        UBaseDNList = uBaseDNList;
    }
    
    public String getUUidAttribute()
    {
        return UUidAttribute;
    }

    public void setUUidAttribute(String uUidAttribute)
    {
        UUidAttribute = uUidAttribute;
    }
    
    public String getUDefaultDomain()
    {
        return UDefaultDomain;
    }

    public void setUDefaultDomain(String uDefaultDomain)
    {
        UDefaultDomain = uDefaultDomain;
    }
    
    public String getUCryptType()
    {
        return UCryptType;
    }

    public void setUCryptType(String uCryptType)
    {
        UCryptType = uCryptType;
    }
    
    public String getUCryptPrefix()
    {
        return UCryptPrefix;
    }

    public void setUCryptPrefix(String uCryptPrefix)
    {
        UCryptPrefix = uCryptPrefix;
    }

    public String getUPasswordAttribute()
    {
        return UPasswordAttribute;
    }

    public void setUPasswordAttribute(String uPasswordAttribute)
    {
        UPasswordAttribute = uPasswordAttribute;
    }

    public String getUProperties()
    {
        return UProperties;
    }

    public void setUProperties(String uLdapProperties)
    {
        UProperties = uLdapProperties;
    }

    public OrganizationVO getBelongsToOrganization()
    {
        return belongsToOrganization;
    }

    public void setBelongsToOrganization(OrganizationVO belongsToOrganization)
    {
        this.belongsToOrganization = belongsToOrganization;
    }

    public Boolean getUIsDefault()
    {
        return UIsDefault;
    }

    public void setUIsDefault(Boolean uIsDefault)
    {
        UIsDefault = uIsDefault;
    }

    public Boolean ugetUIsDefault()
    {
        Boolean result = false;
        if(getUIsDefault() != null)
        {
            result = getUIsDefault();
        }
        
        return result;
    };
    
    public String getUGateway()
    {
        return UGateway;
    }

    public void setUGateway(String uGateway)
    {
        UGateway = uGateway;
    }

    public Boolean getGroupRequired()
    {
        return groupRequired;
    }

    public void setGroupRequired(Boolean groupRequired)
    {
        this.groupRequired = groupRequired;
    }
}
