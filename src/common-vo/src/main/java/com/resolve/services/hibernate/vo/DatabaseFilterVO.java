/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class DatabaseFilterVO extends GatewayFilterVO
{
    private static final long serialVersionUID = -8193419615023079477L;

    private String USql;
    private String UUpdateSql;
    private String UPool;
    private String ULastValue;
    private String ULastValueColumn;
    private String UPrefix;
    private Boolean ULastValueQuote;

    public DatabaseFilterVO()
    {
        super();
    } // DatabaseFilter

    @MappingAnnotation(columnName = "SQL")
    public String getUSql()
    {
        return USql;
    }

    public void setUSql(String uSql)
    {
        if (StringUtils.isNotBlank(uSql) || USql.equals(VO.STRING_DEFAULT))
        {
            USql = uSql != null ? uSql.trim() : uSql;
        }
    }

    @MappingAnnotation(columnName = "UPDATESQL")
    public String getUUpdateSql()
    {
        return UUpdateSql;
    }

    public void setUUpdateSql(String uUpdateSql)
    {
        if (StringUtils.isNotBlank(uUpdateSql) || UUpdateSql.equals(VO.STRING_DEFAULT))
        {
            UUpdateSql = uUpdateSql != null ? uUpdateSql.trim() : uUpdateSql;
        }
    }

    @MappingAnnotation(columnName = "POOL")
    public String getUPool()
    {
        return UPool;
    }

    public void setUPool(String uPool)
    {
        if (StringUtils.isNotBlank(uPool) || UPool.equals(VO.STRING_DEFAULT))
        {
            UPool = uPool != null ? uPool.trim() : uPool;
        }
    }

    @MappingAnnotation(columnName = "LASTVALUE")
    public String getULastValue()
    {
        return ULastValue;
    }

    public void setULastValue(String uLastValue)
    {
        if (StringUtils.isNotBlank(uLastValue) || ULastValue.equals(VO.STRING_DEFAULT))
        {
            ULastValue = uLastValue != null ? uLastValue.trim() : uLastValue;
        }
    }

    @MappingAnnotation(columnName = "LASTVALUECOLUMN")
    public String getULastValueColumn()
    {
        return ULastValueColumn;
    }

    public void setULastValueColumn(String uLastValueColumn)
    {
        if (StringUtils.isNotBlank(uLastValueColumn) || ULastValueColumn.equals(VO.STRING_DEFAULT))
        {
            ULastValueColumn = uLastValueColumn != null ? uLastValueColumn.trim() : uLastValueColumn;
        }
    }

    @MappingAnnotation(columnName = "PREFIX")
    public String getUPrefix()
    {
        return UPrefix;
    }

    public void setUPrefix(String uPrefix)
    {
        if (StringUtils.isNotBlank(uPrefix) || UPrefix.equals(VO.STRING_DEFAULT))
        {
            UPrefix = uPrefix != null ? uPrefix.trim() : uPrefix;
        }
    }

    @MappingAnnotation(columnName = "LASTVALUEQUOTE")
    public Boolean getULastValueQuote()
    {
        return ULastValueQuote;
    }

    public void setULastValueQuote(Boolean uLastValueQuote)
    {
        ULastValueQuote = uLastValueQuote;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((ULastValue == null) ? 0 : ULastValue.hashCode());
        result = prime * result + ((ULastValueColumn == null) ? 0 : ULastValueColumn.hashCode());
        result = prime * result + ((ULastValueQuote == null) ? 0 : ULastValueQuote.hashCode());
        result = prime * result + ((UPool == null) ? 0 : UPool.hashCode());
        result = prime * result + ((UPrefix == null) ? 0 : UPrefix.hashCode());
        result = prime * result + ((USql == null) ? 0 : USql.hashCode());
        result = prime * result + ((UUpdateSql == null) ? 0 : UUpdateSql.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        DatabaseFilterVO other = (DatabaseFilterVO) obj;
        if (ULastValueColumn == null)
        {
            if (StringUtils.isNotBlank(other.ULastValueColumn)) return false;
        }
        else if (!ULastValueColumn.trim().equals(other.ULastValueColumn == null ? "" : other.ULastValueColumn.trim())) return false;
        if (ULastValueQuote == null)
        {
            if (other.ULastValueQuote != null) return false;
        }
        else if (!ULastValueQuote.equals(other.ULastValueQuote)) return false;
        if (UPool == null)
        {
            if (StringUtils.isNotBlank(other.UPool)) return false;
        }
        else if (!UPool.trim().equals(other.UPool == null ? "" : other.UPool.trim())) return false;
        if (UPrefix == null)
        {
            if (StringUtils.isNotBlank(other.UPrefix)) return false;
        }
        else if (!UPrefix.trim().equals(other.UPrefix == null ? "" : other.UPrefix.trim())) return false;
        if (USql == null)
        {
            if (StringUtils.isNotBlank(other.USql)) return false;
        }
        else if (!USql.trim().equals(other.USql == null ? "" : other.USql.trim())) return false;
        if (UUpdateSql == null)
        {
            if (StringUtils.isNotBlank(other.UUpdateSql)) return false;
        }
        else if (!UUpdateSql.trim().equals(other.UUpdateSql == null ? "" : other.UUpdateSql.trim())) return false;
        return true;
    }
    
    
}
