/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;

import com.resolve.services.interfaces.VO;

public class MetaFormViewVO extends VO
{
    private static final long serialVersionUID = 9152895345797436974L;

    public final static String RESOURCE_TYPE = "meta_form_view";

    private String UFormName;
    private String UViewName;
    private String UDisplayName;
    private String UWikiName;
    private String UTableName;
    private String UTableDisplayName;
    private Boolean UIsHeaderVisible;
    private Boolean UIsViewCollapsible;
    private Boolean UIsWizard;
    
    private String UWindowTitle;
    

    //TODO
    // object referenced by
    private MetaAccessRightsVO metaAccessRights;
    private Collection<MetaFormTabVO> metaFormTabs;//this is DEPRECATED and used for the older version only - HAVE TO CREATE A TOOL TO CONVERT THIS FROM OLDER TO NEWER USING the MetaxFormViewPanel
    private Collection<MetaSourceVO> metaSources;//all the sources/feilds that are for this specific view
    private MetaControlVO metaControl;
    private MetaFormViewPropertiesVO metaFormViewProperties;

    private Collection<MetaxFormViewPanelVO> metaxFormViewPanels;
    private String formJsonTree;
    private boolean hasChildren;

    public MetaFormViewVO()
    {
    }
    
    public String getUFormName()
    {
        return UFormName;
    }
    public void setUFormName(String uFormName)
    {
        UFormName = uFormName;
    }


    public String getUViewName()
    {
        return UViewName;
    }
    public void setUViewName(String uViewName)
    {
        UViewName = uViewName;
    }
    

    public String getUWikiName()
    {
        return UWikiName;
    }
    public void setUWikiName(String uWikiName)
    {
        UWikiName = uWikiName;
    }
    
    public String getUTableName()
    {
        return UTableName;
    }

    public void setUTableName(String uTableName)
    {
        UTableName = uTableName;
    }
    
    public String getUTableDisplayName()
    {
        return UTableDisplayName;
    }

    public void setUTableDisplayName(String uTableDisplayName)
    {
        UTableDisplayName = uTableDisplayName;
    }
    
    public String getUDisplayName()
    {
        return UDisplayName;
    }

    public void setUDisplayName(String uDisplayName)
    {
        UDisplayName = uDisplayName;
    }
    
    public String getUWindowTitle()
    {
        return UWindowTitle;
    }

    public void setUWindowTitle(String uWindowTitle)
    {
        UWindowTitle = uWindowTitle;
    }

    public Boolean getUIsHeaderVisible()
    {
        return UIsHeaderVisible;
    }
    public Boolean ugetUIsHeaderVisible()
    {
        if(this.UIsHeaderVisible != null)
        {
            return UIsHeaderVisible;    
        }
        else
        {
            return false;
        }
    }
    public void setUIsHeaderVisible(Boolean uIsHeaderVisible)
    {
        UIsHeaderVisible = uIsHeaderVisible;
    }

    public Boolean getUIsViewCollapsible()
    {
        return UIsViewCollapsible;
    }
    public Boolean ugetUIsViewCollapsible()
    {
        if(this.UIsViewCollapsible != null)
        {
            return UIsViewCollapsible;    
        }
        else
        {
            return false;
        }
    }
    public void setUIsViewCollapsible(Boolean uIsViewCollapsible)
    {
        UIsViewCollapsible = uIsViewCollapsible;
    }
    
    public Boolean getUIsWizard()
    {
        return UIsWizard;
    }
    public Boolean ugetUIsWizard()
    {
        if(this.UIsWizard != null)
        {
            return UIsWizard;    
        }
        else
        {
            return false;
        }
    }
    public void setUIsWizard(Boolean uIsWizard)
    {
        UIsWizard = uIsWizard;
    }


    public MetaAccessRightsVO getMetaAccessRights()
    {
        return metaAccessRights;
    }

    public void setMetaAccessRights(MetaAccessRightsVO metaAccessRights)
    {
        this.metaAccessRights = metaAccessRights;
    }

    public Collection<MetaFormTabVO> getMetaFormTabs()
    {
        return this.metaFormTabs;
    }

    public void setMetaFormTabs(Collection<MetaFormTabVO> metaFormTabs)
    {
        this.metaFormTabs = metaFormTabs;
    }
    
    
    public Collection<MetaSourceVO> getMetaSources()
    {
        return this.metaSources;
    }

    public void setMetaSources(Collection<MetaSourceVO> metaSources)
    {
        this.metaSources = metaSources;
    }

    public MetaControlVO getMetaControl()
    {
        return metaControl;
    }

    public void setMetaControl(MetaControlVO metaControl)
    {
        this.metaControl = metaControl;
    }
    
    public MetaFormViewPropertiesVO getMetaFormViewProperties()
    {
        return metaFormViewProperties;
    }
    public void setMetaFormViewProperties(MetaFormViewPropertiesVO metaFormViewProperties)
    {
        this.metaFormViewProperties = metaFormViewProperties;
    }

    public Collection<MetaxFormViewPanelVO> getMetaxFormViewPanels()
    {
        return metaxFormViewPanels;
    }

    public void setMetaxFormViewPanels(Collection<MetaxFormViewPanelVO> metaxFormViewPanels)
    {
        this.metaxFormViewPanels = metaxFormViewPanels;
    }
    
    public String getFormJsonTree()
    {
        return formJsonTree;
    }
    public void setFormJsonTree(String formJsonTree)
    {
        this.formJsonTree = formJsonTree;
    }

    public boolean isHasChildren()
    {
        return hasChildren;
    }
    public void setHasChildren(boolean hasChildren)
    {
        this.hasChildren = hasChildren;
    }

}//MetaFormView
