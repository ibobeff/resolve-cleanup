/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class EWSFilterVO extends GatewayFilterVO
{
    private static final long serialVersionUID = -311791542506763007L;

    private String UQuery;
    private Boolean UIncludeAttachment;
    private String UFolderNames;
    private Boolean UMarkAsRead;
	private Boolean UOnlyNew;
    // object referenced by
    public EWSFilterVO()
    {
        super();
    } // EWSFilter

    @MappingAnnotation(columnName = "QUERY")
    public String getUQuery()
    {
        return this.UQuery;
    } // getUQuery

    public void setUQuery(String uQuery)
    {
        if (StringUtils.isNotBlank(uQuery) || UQuery.equals(VO.STRING_DEFAULT))
        {
            this.UQuery = uQuery != null ? uQuery.trim() : uQuery;
        }
    } // setUQuery

    @MappingAnnotation(columnName = "INCLUDE_ATTACHMENT")
    public Boolean getUIncludeAttachment()
    {
        return this.UIncludeAttachment;
    } // getUIncludeAttachment

    public void setUIncludeAttachment(Boolean uIncludeAttachment)
    {
        this.UIncludeAttachment = uIncludeAttachment;
    }
    
    @MappingAnnotation(columnName = "ONLY_NEW")
    public Boolean getUOnlyNew()
    {
        return this.UOnlyNew;
    } // getUOnlyNew

    public void setUOnlyNew(Boolean uOnlyNew)
    {
        this.UOnlyNew = uOnlyNew;
    } // setUOnlyNew
    
    @MappingAnnotation(columnName = "FOLDER_NAMES")
    public String getUFolderNames()
    {
        return UFolderNames;
    }

    public void setUFolderNames(String folderNames)
    {
        this.UFolderNames = folderNames;
    }
    @MappingAnnotation(columnName = "MARK_AS_READ")
    public Boolean getUMarkAsRead() {
		return UMarkAsRead;
	}

	public void setUMarkAsRead(Boolean uMarkAsRead) {
		UMarkAsRead = uMarkAsRead;
	}

	@Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UIncludeAttachment == null) ? 0 : UIncludeAttachment.hashCode());
        result = prime * result + ((UQuery == null) ? 0 : UQuery.hashCode());
        result = prime * result + ((UFolderNames == null) ? 0 : UFolderNames.hashCode());
        result = prime * result + ((UMarkAsRead == null) ? 0 : UMarkAsRead.hashCode());
		result = prime * result + ((UOnlyNew == null) ? 0 : UOnlyNew.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        EWSFilterVO other = (EWSFilterVO) obj;
        if (UIncludeAttachment == null)
        {
            if (other.UIncludeAttachment != null) return false;
        }
        else if (!UIncludeAttachment.equals(other.UIncludeAttachment)) return false;
        if (UQuery == null)
        {
            if (StringUtils.isNotBlank(other.UQuery)) return false;
        }
        else if (!UQuery.trim().equals(other.UQuery == null ? "" : other.UQuery.trim())) return false;
        if (UFolderNames == null)
        {
            if (other.UFolderNames != null) return false;
        }
        else if (!UFolderNames.equals(other.UFolderNames)) return false;
        if (UMarkAsRead == null)
        {
        	if (other.UMarkAsRead != null) return false;
        }
        else if (!UMarkAsRead.equals(other.UMarkAsRead)) return false;
		if (UOnlyNew == null)
        {
            if (other.UOnlyNew != null) return false;
        }
        else if (!UOnlyNew.equals(other.UOnlyNew)) return false;
        return true;
    }
}
