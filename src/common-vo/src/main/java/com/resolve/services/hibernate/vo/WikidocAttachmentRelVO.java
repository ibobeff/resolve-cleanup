/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;

public class WikidocAttachmentRelVO extends VO
{

    private static final long serialVersionUID = 5369083717603540897L;

    // object references
	private WikiDocumentVO wikidoc;
	private WikiDocumentVO parentWikidoc;
	private WikiAttachmentVO wikiAttachment;
	
	//used to indicate if the attachment is global
	private boolean global;
	
	
	public WikidocAttachmentRelVO()
	{
	}

	public WikiDocumentVO getWikidoc()
	{
		return wikidoc;
	}

	public void setWikidoc(WikiDocumentVO wikidoc)
	{
		this.wikidoc = wikidoc;
        if (wikidoc != null)
        {
            Collection<WikidocAttachmentRelVO> coll = wikidoc.getWikidocAttachmentRels();
            if (coll == null)
            {
				coll = new HashSet<WikidocAttachmentRelVO>();
	            coll.add(this);
	            
                wikidoc.setWikidocAttachmentRels(coll);
            }
        }
	}
	public WikiAttachmentVO getWikiAttachment()
	{
		return wikiAttachment;
	}

	public void setWikiAttachment(WikiAttachmentVO wikiAttachment)
	{
		this.wikiAttachment = wikiAttachment;
		
        if (wikiAttachment != null)
        {
            Collection<WikidocAttachmentRelVO> coll = wikiAttachment.getWikidocAttachmentRels();
            if (coll == null)
            {
				coll = new HashSet<WikidocAttachmentRelVO>();
	            coll.add(this);
	            
                wikiAttachment.setWikidocAttachmentRels(coll);
            }
        }
	}

    public WikiDocumentVO getParentWikidoc()
    {
        return parentWikidoc;
    }

    public void setParentWikidoc(WikiDocumentVO parentWikidoc)
    {
        this.parentWikidoc = parentWikidoc;

        if (parentWikidoc != null)
        {
            Collection<WikidocAttachmentRelVO> coll = parentWikidoc.getWikidocAttachmentRels();
            if (coll == null)
            {
				coll = new HashSet<WikidocAttachmentRelVO>();
	            coll.add(this);
	            
                parentWikidoc.setWikidocAttachmentRels(coll);
            }
        }
    }

    public boolean isGlobal()
    {
        return global;
    }

    public void setGlobal(boolean global)
    {
        this.global = global;
    }
    
    
    
}
