package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveMCPLoginVO extends VO
{
    private static final long serialVersionUID = -7999702887270590062L;
    
    private String UGroupName;
    private String UClusterName;
    private String UPWDValue;
    
    public String getUGroupName()
    {
        return UGroupName;
    }

    public void setUGroupName(String groupName)
    {
        this.UGroupName = groupName;
    }

    public String getUClusterName()
    {
        return UClusterName;
    }

    public void setUClusterName(String clusterName)
    {
        this.UClusterName = clusterName;
    }

    public String getUPWDValue()
    {
        return UPWDValue;
    }

    public void setUPWDValue(String value)
    {
        this.UPWDValue = value;
    }

}








