/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;

import com.resolve.services.interfaces.VO;


public class MetaControlVO  extends VO
{
    private static final long serialVersionUID = 347035824638127570L;
    
    private String UName;
    private String UDisplayName;
    
    // object referenced by
    private Collection<MetaControlItemVO> metaControlItems;
    
    public MetaControlVO()
    {
    }
    
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    public String getUDisplayName()
    {
        return this.UDisplayName;
    }

    public void setUDisplayName(String UDisplayName)
    {
        this.UDisplayName = UDisplayName;
    }
    
    public Collection<MetaControlItemVO> getMetaControlItems()
    {
        return this.metaControlItems;
    }

    public void setMetaControlItems(Collection<MetaControlItemVO> metaControlItems)
    {
        this.metaControlItems = metaControlItems;
    }
}
