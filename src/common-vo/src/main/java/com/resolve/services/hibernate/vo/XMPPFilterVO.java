/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class XMPPFilterVO extends GatewayFilterVO
{
    private static final long serialVersionUID = 5490326708202475185L;

    private String URegex;

    public XMPPFilterVO()
    {
        super();
    } // TSRMFilter

    @MappingAnnotation(columnName = "REGEX")
    public String getURegex()
    {
        return this.URegex;
    } // getURegex

    public void setURegex(String uRegex)
    {
        if (StringUtils.isNotBlank(uRegex) || URegex.equals(VO.STRING_DEFAULT))
        {
            this.URegex = uRegex != null ? uRegex.trim() : uRegex;
        }
    } // setURegex

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((URegex == null) ? 0 : URegex.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        XMPPFilterVO other = (XMPPFilterVO) obj;
        if (URegex == null)
        {
            if (StringUtils.isNotBlank(other.URegex)) return false;
        }
        else if (!URegex.trim().equals(other.URegex == null ? "" : other.URegex.trim())) return false;
        return true;
    }
}
