/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class NetcoolFilterVO extends GatewayFilterVO
{
    private static final long serialVersionUID = -2471220368094551999L;

    private String USql;

    public NetcoolFilterVO()
    {
        super();
    }

    @MappingAnnotation(columnName = "SQL")
    public String getUSql()
    {
        return this.USql;
    }

    public void setUSql(String USql)
    {
        if (StringUtils.isNotBlank(USql) || this.USql.equals(VO.STRING_DEFAULT))
        {
            this.USql = USql != null ? USql.trim() : USql;
        }
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((USql == null) ? 0 : USql.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        NetcoolFilterVO other = (NetcoolFilterVO) obj;
        if (USql == null)
        {
            if (StringUtils.isNotBlank(other.USql)) return false;
        }
        else if (!USql.trim().equals(other.USql == null ? "" : other.USql.trim())) return false;
        return true;
    }
}
