package com.resolve.services.hibernate.vo;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;

public class GatewayPropertiesVO extends GatewayVO
{
    private static final long serialVersionUID = 7127236913261371771L;

    protected Boolean UActive;
    protected String UOrgName;
    protected String URSRemoteName;
    protected String UGatewayName;
    protected String UDisplayName;
    protected String UPackageName;
    protected String UClassName;
    protected String ULicenseCode;
    protected String UEventType;

    protected Boolean UPrimary;
    protected Boolean USecondary;
    protected Boolean UWorker;
    protected Boolean UMock;
    protected Boolean UUppercase;
    protected Integer UInterval = 120;
    protected Integer UFailover = 120;
    protected Integer UHeartbeat = 120;

    protected Boolean USsl;
    protected String USslType;
    protected String UHost;
    protected Integer UPort;
    protected String UUser;
    protected String UPass;
    
    public GatewayPropertiesVO() {
    	super();
    }
    
    public GatewayPropertiesVO(boolean defaultValues) {
    	super(defaultValues);
    }
    
    @MappingAnnotation(columnName="ACTIVE")
    public Boolean getUActive()
    {
        return UActive;
    }
    public void setUActive(Boolean uActive)
    {
        UActive = uActive;
    }
    
    @MappingAnnotation(columnName="ORG_NAME")
    public String getUOrgName()
    {
        return UOrgName;
    }
    public void setUOrgName(String uOrgName)
    {
        UOrgName = uOrgName;
    }
    
    @MappingAnnotation(columnName="RSREMOTE_NAME")
    public String getURSRemoteName()
    {
        return URSRemoteName;
    }
    public void setURSRemoteName(String uRSRemoteName)
    {
        URSRemoteName = uRSRemoteName;
    }
    
    @MappingAnnotation(columnName="GATEWAY_NAME")
    public String getUGatewayName()
    {
        return UGatewayName;
    }
    public void setUGatewayName(String uGatewayName)
    {
        UGatewayName = uGatewayName;
    }
    
    @MappingAnnotation(columnName="DISPLAY_NAME")
    public String getUDisplayName()
    {
        return UDisplayName;
    }
    public void setUDisplayName(String uDisplayName)
    {
        UDisplayName = uDisplayName;
    }
    
    @MappingAnnotation(columnName="PACKAGE_NAME")
    public String getUPackageName()
    {
        return UPackageName;
    }
    public void setUPackageName(String uPackageName)
    {
        UPackageName = uPackageName;
    }
    
    @MappingAnnotation(columnName="CLASS_NAME")
    public String getUClassName()
    {
        return UClassName;
    }
    public void setUClassName(String uClassName)
    {
        UClassName = uClassName;
    }
    
    @MappingAnnotation(columnName="LICENSE_CODE")
    public String getULicenseCode()
    {
        return ULicenseCode;
    }
    public void setULicenseCode(String uLicenseCode)
    {
        ULicenseCode = uLicenseCode;
    }
    
    @MappingAnnotation(columnName="EVENT_TYPE")
    public String getUEventType()
    {
        return UEventType;
    }
    public void setUEventType(String uEventType)
    {
        UEventType = uEventType;
    }
    
    @MappingAnnotation(columnName="PRIMARY")
    public Boolean getUPrimary()
    {
        return UPrimary;
    }
    public void setUPrimary(Boolean uPrimary)
    {
        UPrimary = uPrimary;
    }
    
    @MappingAnnotation(columnName="SECONDARY")
    public Boolean getUSecondary()
    {
        return USecondary;
    }
    public void setUSecondary(Boolean uSecondary)
    {
        USecondary = uSecondary;
    }
    
    @MappingAnnotation(columnName="WORKER")
    public Boolean getUWorker()
    {
        return UWorker;
    }
    public void setUWorker(Boolean uWorker)
    {
        UWorker = uWorker;
    }
    
    @MappingAnnotation(columnName="MOCK")
    public Boolean getUMock()
    {
        return UMock;
    }
    public void setUMock(Boolean uMock)
    {
        UMock = uMock;
    }
    
    @MappingAnnotation(columnName="UPPERCASE")
    public Boolean getUUppercase()
    {
        return UUppercase;
    }
    public void setUUppercase(Boolean uUppercase)
    {
        UUppercase = uUppercase;
    }
    
    @MappingAnnotation(columnName="INTERVAL")
    public Integer getUInterval()
    {
        return UInterval;
    }
    public void setUInterval(Integer uInterval)
    {
        UInterval = uInterval;
    }
    
    @MappingAnnotation(columnName="FAILOVER")
    public Integer getUFailover()
    {
        return UFailover;
    }
    public void setUFailover(Integer uFailover)
    {
        UFailover = uFailover;
    }
    
    @MappingAnnotation(columnName="HEARTBEAT")
    public Integer getUHeartbeat()
    {
        return UHeartbeat;
    }
    public void setUHeartbeat(Integer uHeartbeat)
    {
        UHeartbeat = uHeartbeat;
    }
    
    @MappingAnnotation(columnName="SSL")
    public Boolean getUSsl()
    {
        return USsl;
    }
    public void setUSsl(Boolean uSsl)
    {
        USsl = uSsl;
    }
    
    @MappingAnnotation(columnName="SSLTYPE")
    public String getUSslType()
    {
        return USslType;
    }
    public void setUSslType(String uSslType)
    {
        USslType = uSslType;
    }
    
    @MappingAnnotation(columnName="HOST")
    public String getUHost()
    {
        return UHost;
    }
    public void setUHost(String uHost)
    {
        UHost = uHost;
    }
    
    @MappingAnnotation(columnName="PORT")
    public Integer getUPort()
    {
        return UPort;
    }
    public void setUPort(Integer uPort)
    {
        UPort = uPort;
    }
    
    @MappingAnnotation(columnName="USER")
    public String getUUser()
    {
        return UUser;
    }
    public void setUUser(String uUser)
    {
        UUser = uUser;
    }
    
    @MappingAnnotation(columnName="PASS")
    public String getUPass()
    {
        return UPass;
    }
    public void setUPass(String uPass)
    {
        UPass = uPass;
    }

    @Override
    public String getUniqueId()
    {
        return getUQueue();
    }
    
    public Map<String, String> toMap() {
        
        Map<String, String> attrs = new HashMap<String, String>();

        attrs.put("id", this.getSys_id());

        Method[] methods = this.getClass().getMethods();
        
        for(Method method:methods) {
            String name = method.getName();
            if(method.getAnnotation(MappingAnnotation.class) != null) {
                try {
                    String attr = name.substring(3).toLowerCase();
                    Object value = method.invoke(this, (Object[])null);

                    if(value != null)
                        attrs.put(attr, value.toString());
                } catch(Exception e) {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }

        return attrs;
    }    

} // class GatewayProperties
