/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Date;

import org.quartz.CronExpression;

import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

public class ResolveCronVO extends VO
{
    private static final long serialVersionUID = 8596808980412148466L;
    
    private String UName;
	private String UModule;
	private String URunbook;
	private String UExpression;
	private String UParams;
	private Date UStartTime;
	private Date UEndTime;
	private Boolean UActive;

	public ResolveCronVO() {
	}
	
	@Override
    public boolean validate()
    {
        boolean valid = true;
        if(StringUtils.isEmpty(getUName()))
        {
            throw new RuntimeException("Cron Job Name is mandatory");
        }
        
        if(getUName().startsWith("."))
        {
            throw new RuntimeException("Cron Job Name cannot start with '.'");
        }
        
//        valid = this.UExpression.matches(JavaUtils.buildRegexForValidatingTheCronExpression());//CronExpression.isValidExpression(this.UExpression);
      //NOTE: this still allows bad ones to go...need to find a better way for validating the cron expression
        valid = CronExpression.isValidExpression(this.UExpression);
        if(!valid)
        {
            throw new RuntimeException("Invalid cron expression.");
        }
        
        //validate the name
//        valid = getUName().matches(HibernateConstants.REGEX_ALPHANUMERIC_WITH_UNDERSCORE_DOT_DASH_SPACE);
//        if(!valid)
//        {
//            throw new RuntimeException("Cron Job Name can have only alphanumerics,'_', space and '.'");
//        }

        return valid;
    }

	public ResolveCronVO(String sys_id) 
	{
		this.setSys_id(sys_id);
	}

	public String getUName() 
	{
		return this.UName;
	}

	public void setUName(String UName) 
	{
		this.UName = UName;
	}

	public String getUModule() 
	{
		return this.UModule;
	}

	public void setUModule(String UModule) 
	{
		this.UModule = UModule;
	}

	public String getURunbook() 
	{
		return this.URunbook;
	}

	public void setURunbook(String URunbook) 
	{
		this.URunbook = URunbook;
	}

	public String getUExpression() 
	{
		return this.UExpression;
	}

	public void setUExpression(String UExpression) 
	{
		this.UExpression = UExpression;
	}

	public String getUParams() 
	{
		return this.UParams;
	}

	public void setUParams(String UParams) 
	{
		this.UParams = UParams;
	}

	public Date getUStartTime() 
	{
		return this.UStartTime;
	}

	public void setUStartTime(Date UStartTime) 
	{
		this.UStartTime = UStartTime;
	}

	public Date getUEndTime() 
	{
		return this.UEndTime;
	}

	public void setUEndTime(Date UEndTime) 
	{
		this.UEndTime = UEndTime;
	}

    public Boolean getUActive()
    {
        return this.UActive;
    } // getUActive

    public Boolean ugetUActive()
    {
        // NOTE: null (old record) means active
        if (this.UActive == null)
        {
            return new Boolean(true);
        }
        else
        {
            return this.UActive;
        }
    } // getUActive

    public void setUActive(Boolean UActive)
    {
        this.UActive = UActive;
    }
	
} // ResolveCron
