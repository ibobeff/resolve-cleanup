/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
public class MCPFileVO extends VO
{
    private static final long serialVersionUID = -1155386104718181683L;
	private String name;
    private String type;
    private String version;
    private Date date;
    private String serverID;
    private String serverName;
    private Integer size;
    
    private MCPFileContentVO mcpFileContent;
    
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    public String getVersion()
    {
        return version;
    }
    public void setVersion(String version)
    {
        this.version = version;
    }
    public Date getDate()
    {
        return date;
    }
    public void setDate(Date date)
    {
        this.date = date;
    }
    public String getServerID()
    {
        return serverID;
    }
    public void setServerID(String serverID)
    {
        this.serverID = serverID;
    }
    public String getServerName()
    {
        return serverName;
    }
    public void setServerName(String serverName)
    {
        this.serverName = serverName;
    }
    public MCPFileContentVO ugetMCPFileContent()
    {
        return mcpFileContent;
    }
    public void usetMCPFileContent(MCPFileContentVO mcpFileContent)
    {
        this.mcpFileContent = mcpFileContent;
    }
    public Integer getSize()
    {
        return size;
    }
    public void setSize(Integer size)
    {
        this.size = size;
    }
    public String getDownloadUrl()
    {
        if (StringUtils.isNotBlank(getSys_id()))
        {
            return "/resolve/service/mcp/file/download?MCP_FILE_ID=" + getSys_id();
        }
        else
        {
            return "";
        }
    }
}
