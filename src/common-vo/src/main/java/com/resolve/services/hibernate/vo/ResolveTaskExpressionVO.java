/******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveTaskExpressionVO extends VO
{
	private static final long serialVersionUID = 4603781802035258616L;
	
	private String criteria; //ANY, ALL
    private String leftOperandType;
    private String leftOperandName;
    private String operator; //EQUALS, >, >= etc.
    private String rightOperandType;
    private String rightOperandName;
    private Integer order;
    private String expressionType; // condition or severity
    private Integer resultLevel; // 0-Good (both condition and severity), 1-Warning (Severify) and BAD (Condition), 2-Severe (severity), 3-Critical (severity)
    private String script;
    
    private ResolveActionInvocVO invocation;
    
	public String getCriteria()
	{
		return criteria;
	}
	public void setCriteria(String criteria)
	{
		this.criteria = criteria;
	}
	
	public String getLeftOperandType()
	{
		return leftOperandType;
	}
	public void setLeftOperandType(String leftOperandType)
	{
		this.leftOperandType = leftOperandType;
	}
	
	public String getLeftOperandName()
	{
		return leftOperandName;
	}
	public void setLeftOperandName(String leftOperandName)
	{
		this.leftOperandName = leftOperandName;
	}
	
	public String getOperator()
	{
		return operator;
	}
	public void setOperator(String operator)
	{
		this.operator = operator;
	}
	
	public String getRightOperandType()
	{
		return rightOperandType;
	}
	public void setRightOperandType(String rightOperandType)
	{
		this.rightOperandType = rightOperandType;
	}
	
	public String getRightOperandName()
	{
		return rightOperandName;
	}
	public void setRightOperandName(String rightOperandName)
	{
		this.rightOperandName = rightOperandName;
	}
	
	public Integer getOrder()
	{
		return order;
	}
	public void setOrder(Integer order)
	{
		this.order = order;
	}
	
	public String getExpressionType()
	{
		return expressionType;
	}
	public void setExpressionType(String expressionType)
	{
		this.expressionType = expressionType;
	}
	
	public Integer getResultLevel()
	{
		return resultLevel;
	}
	public void setResultLevel(Integer resultLevel)
	{
		this.resultLevel = resultLevel;
	}

	public String getScript()
	{
		return script;
	}
	public void setScript(String script)
	{
		this.script = script;
	}

	public ResolveActionInvocVO getInvocation()
	{
		return invocation;
	}
	public void setInvocation(ResolveActionInvocVO invocation)
	{
		this.invocation = invocation;
	}
}
