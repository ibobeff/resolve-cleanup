/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

public class RemedyFilterVO extends GatewayFilterVO
{
    private static final long serialVersionUID = 4733609828202409539L;

    private String UFormName;
    private String UQuery;

    public RemedyFilterVO()
    {
        super();
    }

    public String getUFormName()
    {
        return UFormName;
    }

    public void setUFormName(String uFormName)
    {
        if (StringUtils.isNotBlank(uFormName) || UFormName.equals(VO.STRING_DEFAULT))
        {
            UFormName = uFormName != null ? uFormName.trim() : uFormName;
        }
    }

    public String getUQuery()
    {
        return this.UQuery;
    } // getUQuery

    public void setUQuery(String uQuery)
    {
        if (StringUtils.isNotBlank(uQuery) || UQuery.equals(VO.STRING_DEFAULT))
        {
            this.UQuery = uQuery != null ? uQuery.trim() : uQuery;
        }
    } // setUQuery

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UFormName == null) ? 0 : UFormName.hashCode());
        result = prime * result + ((UQuery == null) ? 0 : UQuery.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        RemedyFilterVO other = (RemedyFilterVO) obj;
        if (UFormName == null)
        {
            if (StringUtils.isNotBlank(other.UFormName)) return false;
        }
        else if (!UFormName.trim().equals(other.UFormName == null ? "" : other.UFormName.trim())) return false;
        if (UQuery == null)
        {
            if (StringUtils.isNotBlank(other.UQuery)) return false;
        }
        else if (!UQuery.trim().equals(other.UQuery == null ? "" : other.UQuery.trim())) return false;
        return true;
    }
}

