/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class RemedyFormVO extends VO
{
    private static final long serialVersionUID = -7457965421929975584L;

    private String UName;
    private String UQueue;
    private String UFieldList;

    public RemedyFormVO()
    {
    }

    public String getUQueue()
    {
        return this.UQueue;
    } // getUQueue

    public void setUQueue(String UQueue)
    {
        this.UQueue = UQueue;
    } // setUQueue

    public String getUName()
    {
        return UName;
    } // getUName

    public void setUName(String uName)
    {
        this.UName = uName;
    } // setUName

    public String getUFieldList()
    {
        return UFieldList;
    } // getUFieldList

    public void setUFieldList(String uFieldList)
    {
        UFieldList = uFieldList;
    } // setUFieldList

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((UFieldList == null) ? 0 : UFieldList.hashCode());
        result = prime * result + ((UName == null) ? 0 : UName.hashCode());
        result = prime * result + ((UQueue == null) ? 0 : UQueue.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        RemedyFormVO other = (RemedyFormVO) obj;
        if (UFieldList == null)
        {
            if (other.UFieldList != null) return false;
        }
        else if (!UFieldList.equals(other.UFieldList)) return false;
        if (UName == null)
        {
            if (other.UName != null) return false;
        }
        else if (!UName.equals(other.UName)) return false;
        if (UQueue == null)
        {
            if (other.UQueue != null) return false;
        }
        else if (!UQueue.equals(other.UQueue)) return false;
        return true;
    }

}

