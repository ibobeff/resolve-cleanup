/******************************************************************************
	* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveAppControllerRelVO extends VO
{
    private static final long serialVersionUID = -2289315075328545038L;
    
    private ResolveAppsVO resolveApp;
    private ResolveControllersVO resolveController;
    
    
    public ResolveAppControllerRelVO()
    {
    }

    public ResolveAppsVO getResolveApp()
    {
        return resolveApp;
    }

    public void setResolveApp(ResolveAppsVO resolveApp)
    {
        this.resolveApp = resolveApp;
        
    }

    public ResolveControllersVO getResolveController()
    {
        return resolveController;
    }

    public void setResolveController(ResolveControllersVO resolveController)
    {
        this.resolveController = resolveController;
        
    }
    
    

} // ResolveControllersVO
