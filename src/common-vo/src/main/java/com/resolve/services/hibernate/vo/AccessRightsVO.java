/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class AccessRightsVO extends VO
{
    private static final long serialVersionUID = 2657585291318840654L;

    private String UResourceType;
    private String UResourceName;
    private String UResourceId;
    private String UReadAccess;
    private String UWriteAccess;
    private String UAdminAccess;
    private String UExecuteAccess;
    
    //for UI
//    private Boolean UDefaultRights;

    public AccessRightsVO()
    {
    }

    public String getUResourceType()
    {
        return this.UResourceType;
    }

    public void setUResourceType(String UResourceType)
    {
        this.UResourceType = UResourceType;
    }

    public String getUResourceName()
    {
        return this.UResourceName;
    }

    public void setUResourceName(String UResourceName)
    {
        this.UResourceName = UResourceName;
    }

    public String getUResourceId()
    {
        return this.UResourceId;
    }

    public void setUResourceId(String UResourceId)
    {
        this.UResourceId = UResourceId;
    }

    public String getUReadAccess()
    {
        return this.UReadAccess;
    }

    public void setUReadAccess(String UReadAccess)
    {
        this.UReadAccess = UReadAccess;
    }

    public String getUWriteAccess()
    {
        return this.UWriteAccess;
    }

    public void setUWriteAccess(String UWriteAccess)
    {
        this.UWriteAccess = UWriteAccess;
    }

    public String getUAdminAccess()
    {
        return this.UAdminAccess;
    }

    public void setUAdminAccess(String UAdminAccess)
    {
        this.UAdminAccess = UAdminAccess;
    }

    public String getUExecuteAccess()
    {
        return this.UExecuteAccess;
    }

    public void setUExecuteAccess(String UExecuteAccess)
    {
        this.UExecuteAccess = UExecuteAccess;
    }

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccessRightsVO other = (AccessRightsVO) obj;
		if (UAdminAccess == null) {
			if (other.UAdminAccess != null)
				return false;
		} else if (!UAdminAccess.equals(other.UAdminAccess))
			return false;
		if (UExecuteAccess == null) {
			if (other.UExecuteAccess != null)
				return false;
		} else if (!UExecuteAccess.equals(other.UExecuteAccess))
			return false;
		if (UReadAccess == null) {
			if (other.UReadAccess != null)
				return false;
		} else if (!UReadAccess.equals(other.UReadAccess))
			return false;
		if (UResourceId == null) {
			if (other.UResourceId != null)
				return false;
		} else if (!UResourceId.equals(other.UResourceId))
			return false;
		if (UResourceName == null) {
			if (other.UResourceName != null)
				return false;
		} else if (!UResourceName.equals(other.UResourceName))
			return false;
		if (UResourceType == null) {
			if (other.UResourceType != null)
				return false;
		} else if (!UResourceType.equals(other.UResourceType))
			return false;
		if (UWriteAccess == null) {
			if (other.UWriteAccess != null)
				return false;
		} else if (!UWriteAccess.equals(other.UWriteAccess))
			return false;
		return true;
	}

    
//    public Boolean getUDefaultRights()
//    {
//        return UDefaultRights;
//    }
//
//    public void setUDefaultRights(Boolean uDefaultRights)
//    {
//        UDefaultRights = uDefaultRights;
//    }

}
