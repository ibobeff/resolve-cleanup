/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

public class ResolveSysScriptVO extends VO
{
    private static final long serialVersionUID = 1167505375455993180L;
    
	private String UName;
	private String UScript;
	private String UDescription;
	
	public ResolveSysScriptVO()
	{
	}

	public ResolveSysScriptVO(String sys_id)
	{
		this.setSys_id(sys_id);
	}

	@Override
    public boolean validate()
    {
        boolean valid = true;
        if(StringUtils.isEmpty(getUName()))
        {
            throw new RuntimeException("System Script Name is mandatory");
        }
        
        if(getUName().startsWith("."))
        {
            throw new RuntimeException("System Script Name cannot start with '.'");
        }
        
        //validate the name
        valid = getUName().matches(HibernateConstants.REGEX_ALPHANUMERIC_WITH_UNDERSCORE_DOT_DASH_SPACE);
        if(!valid)
        {
            throw new RuntimeException("System Script Name can have only alphanumerics,'_', space and '.'");
        }

        return valid;
    }
	
	public String getUName()
	{
		return this.UName;
	}

	public void setUName(String UName)
	{
		this.UName = UName;
	}

	public String getUScript()
	{
		return this.UScript;
	}

	public void setUScript(String UScript)
	{
		this.UScript = UScript;
	}

	public String getUDescription()
	{
		return this.UDescription;
	}

	public void setUDescription(String UDescription)
	{
		this.UDescription = UDescription;
	}
	
	@Override
	public String toString()
	{
		StringBuffer str = new StringBuffer();

		str.append("SysScript Name:").append(UName).append("\n");
		str.append("SysScript Description:").append(UDescription).append("\n");
		
		return str.toString();
	}

} // ResolveSysScript
