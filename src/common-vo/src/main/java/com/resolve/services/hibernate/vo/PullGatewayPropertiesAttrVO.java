package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class PullGatewayPropertiesAttrVO extends VO
{
    private static final long serialVersionUID = 1L;
    
    private String UName;
    private String UValue;
    private String UPullPropertiesId;
    
    public String getUName()
    {
        return UName;
    }
    public void setUName(String uName)
    {
        UName = uName;
    }
    public String getUValue()
    {
        return UValue;
    }
    public void setUValue(String uValue)
    {
        UValue = uValue;
    }
    public String getUPullPropertiesId()
    {
        return UPullPropertiesId;
    }
    public void setUPullPropertiesId(String uPushPropertiesId)
    {
        UPullPropertiesId = uPushPropertiesId;
    }

} // class PullGatewayPropertiesAttrVO
