/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

public class OrgsVO extends VO implements Comparable<OrgsVO>
{
    private static final long serialVersionUID = -8922522286562296069L;
    public static final String NONE_ORG_NAME = "None";
    
    private String UName;
    private String UDescription;
    private OrgsVO UParentOrg;
    private Set<OrgsVO> UChildOrgs;
    private Boolean UHasNoOrgAccess = Boolean.FALSE;     // Users belonging to this Org has access to no org or "None" org
    
    // object referenced by
    private Set<OrgGroupRelVO> orgGroupRels;
    
    //for display purpose only
    private String groups;
    private List<GroupsVO> orgGroups;
    
    private Boolean isDefaultOrg; 
    
    public OrgsVO()
    {
    }
    
    public boolean validate()
    {
        boolean valid = true;
        
        if(StringUtils.isEmpty(getUName()) || (StringUtils.isNotBlank(getUName()) && getUName().equalsIgnoreCase(VO.STRING_DEFAULT)))
        {
            valid = false;
            throw new RuntimeException("Org name cannot be empty.");
        }
        
        return valid;
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        
        sb.append(this.UName).append(" Parent Org: ").append(UParentOrg != null ? UParentOrg.getUName() : "No Parent Org").append(" Child Orgs: ");
        
        if (this.UChildOrgs != null && !this.UChildOrgs.isEmpty())
        {
            for (OrgsVO childOrgsVO : UChildOrgs)
            {
                sb.append(childOrgsVO.getUName()).append(", ");
            }
        }
        
        sb.append(" Has No Org Access: ").append(this.UHasNoOrgAccess).append(" Is Default Org: ").append(isDefaultOrg).append(" Groups: ").append(groups);
        
        return sb.toString();
    } // toString

    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    public String getUDescription()
    {
        return this.UDescription;
    }

    public void setUDescription(String UDescription)
    {
        this.UDescription = UDescription;
    }
    
    public OrgsVO getUParentOrg()
    {
        return UParentOrg;
    }
    
    public void setUParentOrg(OrgsVO UParentOrg)
    {
        this.UParentOrg = UParentOrg;
    }
    
    public Set<OrgsVO> getUChildOrgs()
    {
        return UChildOrgs;
    }
    
    public void setUChildOrgs(Set<OrgsVO> UChildOrgs)
    {
        this.UChildOrgs = UChildOrgs;
    }
    
    public String getGroups()
    {
        return groups;
    }

    public void setGroups(String groups)
    {
        this.groups = groups;
    }
    
    public Boolean getUHasNoOrgAccess()
    {
        Boolean result = UHasNoOrgAccess;
        
        if (result == null)
        {
            result = Boolean.FALSE;
        }
        
        return result;
    }
    
    public void setUHasNoOrgAccess(Boolean uHasNoOrgAccess)
    {
        UHasNoOrgAccess = uHasNoOrgAccess;
    }
    
    public Collection<OrgGroupRelVO> getOrgGroupRels()
    {
        return orgGroupRels;
    }

    public void setOrgGroupRels(Set<OrgGroupRelVO> orgGroupRels)
    {
        this.orgGroupRels = orgGroupRels;
    }
    
    public List<GroupsVO> getOrgGroups()
    {
        return orgGroups;
    }

    public void setOrgGroups(List<GroupsVO> orgGroups)
    {
        this.orgGroups = orgGroups;
    }
    
    public Boolean getIsDefaultOrg()
    {
        if (isDefaultOrg != null)
        {
            return isDefaultOrg;
        }
        else
        {
            return Boolean.FALSE;
        }
    }
    
    public void setIsDefaultOrg(Boolean isDefaultOrg)
    {
        this.isDefaultOrg = isDefaultOrg;
    }
    
    public int compareTo(OrgsVO that)
    {
        final int EQUAL = 0;
//      final int BEFORE = -1;
//      final int AFTER = 1;
      int result = EQUAL;
      
      //this optimization is usually worthwhile
      if ( this == that ) 
          result = EQUAL;
      else
      {
          String roleThis = this.getUName();
          String roleThat = that.getUName();
          
          if(StringUtils.isNotEmpty(roleThis) && StringUtils.isNotBlank(roleThat))
          {
              result = roleThis.compareTo(roleThat);
          }
          else
          {
              roleThis = this.getSys_id();
              roleThat = that.getSys_id();
              
              result = roleThis.compareTo(roleThat);
          }
      }
      
      return result;
    }


    @Override
    public boolean equals(Object aThat)
    {
        boolean result = false;
        
        if (this == aThat) 
            result = true;
        else if (!(aThat instanceof OrgsVO)) 
            result = false;
        else 
        {
            OrgsVO that = (OrgsVO) aThat;
            result = this.getUName().equalsIgnoreCase(that.getUName());
        }
        
        return result;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 43;

        result = prime * result + ((getUName() == null) ? 0 : getUName().hashCode());

        return result;
    }
    
    public static OrgsVO getNoneOrgsVO(boolean UHasNoOrgAccess)
    {
        OrgsVO noneOrgsVO = new OrgsVO();
        
        noneOrgsVO.setSys_id(null);
        noneOrgsVO.setId(null);
        noneOrgsVO.setUName(NONE_ORG_NAME);
        noneOrgsVO.setUHasNoOrgAccess(Boolean.valueOf(UHasNoOrgAccess));
        
        return noneOrgsVO;
    }
}
