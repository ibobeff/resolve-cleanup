package com.resolve.services.hibernate.vo;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;

public class MSGGatewayPropertiesVO extends GatewayPropertiesVO
{
    private static final long serialVersionUID = 1L;
    
    protected Integer UMSGPort;
    protected String UMSGUser;
    protected String UMSGPass;
    
    protected Boolean UMSGSsl;
    protected String UMSGSslCertificate ;
    protected String UMSGSslPassword;
    
    protected Collection<MSGGatewayPropertiesAttrVO> attrs;
    
    @MappingAnnotation(columnName="MSG_PORT")
    public Integer getUMSGPort()
    {
        return UMSGPort;
    }

    public void setUMSGPort(Integer uMSGPort)
    {
        UMSGPort = uMSGPort;
    }

    @MappingAnnotation(columnName="MSG_USER")
    public String getUMSGUser()
    {
        return UMSGUser;
    }

    public void setUMSGUser(String uMSGUser)
    {
        UMSGUser = uMSGUser;
    }

    @MappingAnnotation(columnName="MSG_PASS")
    public String getUMSGPass()
    {
        return UMSGPass;
    }

    public void setUMSGPass(String uMSGPass)
    {
        UMSGPass = uMSGPass;
    }

    @MappingAnnotation(columnName="MSG_SSL")
    public Boolean getUMSGSsl()
    {
        return UMSGSsl;
    }

    public void setUMSGSsl(Boolean uMSGSsl)
    {
        UMSGSsl = uMSGSsl;
    }

    @MappingAnnotation(columnName="MSG_SSL_CERTIFICATE")
    public String getUMSGSslCertificate()
    {
        return UMSGSslCertificate;
    }

    public void setUMSGSslCertificate(String uMSGSslCertificate)
    {
        UMSGSslCertificate = uMSGSslCertificate;
    }

    @MappingAnnotation(columnName="MSG_SSL_PASSSWORD")
    public String getUMSGSslPassword()
    {
        return UMSGSslPassword;
    }

    public void setUMSGSslPassword(String uMSGSslPassword)
    {
        UMSGSslPassword = uMSGSslPassword;
    }

    public Collection<MSGGatewayPropertiesAttrVO> getAttrs()
    {
        return attrs;
    }

    public void setAttrs(Collection<MSGGatewayPropertiesAttrVO> attrs)
    {
        this.attrs = attrs;
    }
    
    public Map<String, String> toMap() {
        
        Collection<MSGGatewayPropertiesAttrVO> attrs = getAttrs();
        
        Map<String, String> map = new HashMap<String, String>();

        map.put("id", this.getSys_id());

        Method[] methods = this.getClass().getMethods();
        
        for(Method method:methods) {
            String name = method.getName();
            if(method.getAnnotation(MappingAnnotation.class) != null) {
                try {
                    String attr = name.substring(3).toLowerCase();
                    Object value = method.invoke(this, (Object[])null);

                    if(value != null)
                        map.put(attr, value.toString());
                } catch(Exception e) {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
        
        for(Iterator<MSGGatewayPropertiesAttrVO> it=attrs.iterator(); it.hasNext();) {
            MSGGatewayPropertiesAttrVO attr = it.next();
            if(attr != null)
                map.put(attr.getUName(), attr.getUValue());
        }

        return map;
    }
    
    public static MSGGatewayPropertiesVO fromMap(Map<String, Object> map) {
        
        MSGGatewayPropertiesVO vo = new MSGGatewayPropertiesVO();
        
        Method[] methods = vo.getClass().getMethods();
        
        for(Method method:methods) {
            String name = method.getName();
            
            if(StringUtils.isNotBlank(name) && name.length() > 4) {
                if(!name.startsWith("set"))
                    continue;
                
                try {
                    String attr = name.substring(4).toUpperCase();
                    Object value = map.get(attr);
                    
                    if(value != null) {
                        Object[] args = null;
                        
                        if(value instanceof String) {
                            args = new String[1];
                            args[0] = (String)value;
                        }
                        
                        else if(value instanceof Integer) {
                            args = new Integer[1];
                            args[0] = (Integer)value;
                        }
                        
                        else if(value instanceof Boolean) {
                            args = new Boolean[1];
                            args[0] = (Boolean)value;
                        }
                        
                        else if(value instanceof Long) {
                            args = new Long[1];
                            args[0] = (Long)value;
                        }
                        method.invoke(vo, value);
                        map.remove(attr);
                    }
                } catch(Exception e) {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
        
        if(map.size() != 0) {
            Collection<MSGGatewayPropertiesAttrVO> attrs = new ArrayList<MSGGatewayPropertiesAttrVO>();
            
            for(Iterator<String> it=map.keySet().iterator(); it.hasNext();) {
                MSGGatewayPropertiesAttrVO attr = new MSGGatewayPropertiesAttrVO();
                String key = it.next();
                Object value = map.get(key);
                
                attr.setUName(key);
                if(value != null)
                    attr.setUValue(value.toString());
                
                attrs.add(attr);
            }
            
            vo.setAttrs(attrs);
        }
        
        return vo;
    }

}