package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QuerySort.SortOrder;

public class SavedQuerySortVO extends VO {
	private static final long serialVersionUID = -6283259122710739073L;
	private String property;
    private SortOrder direction;

    public SavedQuerySortVO(String property, SortOrder direction) {
		this.property = property;
		this.direction = direction;
	}

    public String getProperty() {
		return property;
	}

    public void setProperty(String property) {
		this.property = property;
	}

    public SortOrder getDirection() {
		return direction;
	}

    public void setDirection(SortOrder direction) {
		this.direction = direction;
	}
}
