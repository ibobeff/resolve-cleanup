/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class RBGeneralVO extends VO
{
    private static final long serialVersionUID = 406902034242957638L;
	private String name;
    private String namespace;
    private String summary;
    private Integer noOfColumn; //for the custom form for parameters
    private Boolean newWorksheet;
    private String wikiId; //sys id of the wiki attached to this RB
    private Boolean generated;
    
    private String wikiParameters;

    private String lockedByUser; // Locked by user to return to UI. This is non-persistent field
    
    public RBGeneralVO()
    {
        super();
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getNamespace()
    {
        return namespace;
    }

    public void setNamespace(String namespace)
    {
        this.namespace = namespace;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public Integer getNoOfColumn()
    {
        return noOfColumn;
    }

    public void setNoOfColumn(Integer noOfColumn)
    {
        this.noOfColumn = noOfColumn;
    }

    public Boolean getNewWorksheet()
    {
        return newWorksheet;
    }

    public void setNewWorksheet(Boolean newWorksheet)
    {
        this.newWorksheet = newWorksheet;
    }

    public String getWikiId()
    {
        return wikiId;
    }

    public void setWikiId(String wikiId)
    {
        this.wikiId = wikiId;
    }

    public Boolean getGenerated()
    {
        return generated;
    }

    public void setGenerated(Boolean generated)
    {
        this.generated = generated;
    }

    /**
     * JSON representing parameters, managed by UI
     * @return
     */
    public String getWikiParameters()
    {
        return wikiParameters;
    }

    public void setWikiParameters(String wikiParameters)
    {
        this.wikiParameters = wikiParameters;
    }

    public String getLockedByUser()
    {
        return lockedByUser;
    }
    
    public void setLockedByUser(String lockedByUser)
    {
        this.lockedByUser = lockedByUser;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((namespace == null) ? 0 : namespace.hashCode());
        result = prime * result + ((newWorksheet == null) ? 0 : newWorksheet.hashCode());
        result = prime * result + ((noOfColumn == null) ? 0 : noOfColumn.hashCode());
        result = prime * result + ((summary == null) ? 0 : summary.hashCode());
        result = prime * result + ((wikiId == null) ? 0 : wikiId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        RBGeneralVO other = (RBGeneralVO) obj;
        if (name == null)
        {
            if (other.name != null) return false;
        }
        else if (!name.equals(other.name)) return false;
        if (namespace == null)
        {
            if (other.namespace != null) return false;
        }
        else if (!namespace.equals(other.namespace)) return false;
        if (newWorksheet == null)
        {
            if (other.newWorksheet != null) return false;
        }
        else if (!newWorksheet.equals(other.newWorksheet)) return false;
        if (noOfColumn == null)
        {
            if (other.noOfColumn != null) return false;
        }
        else if (!noOfColumn.equals(other.noOfColumn)) return false;
        if (summary == null)
        {
            if (other.summary != null) return false;
        }
        else if (!summary.equals(other.summary)) return false;
        if (wikiId == null)
        {
            if (other.wikiId != null) return false;
        }
        else if (!wikiId.equals(other.wikiId)) return false;
        return true;
    }
}
