/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;

public class MetaFilterVO extends VO
{
    private static final long serialVersionUID = 8333488526429377105L;
    
    private String UName;
    private String UValue;
    private Boolean UIsGlobal;
    private Boolean UIsSelf;
    private Boolean UIsRole;
    private String UUserId;
    
    
    private MetaAccessRightsVO metaAccessRights;
    private MetaTableVO metaTable;  
    
    public MetaFilterVO()
    {
    }
    
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    public String getUValue()
    {
        return this.UValue;
    }
    
    public void setUValue(String UValue)
    {
        this.UValue = UValue;
    }
    
    public Boolean getUIsGlobal()
    {
        return this.UIsGlobal;
    } 
    
    public Boolean ugetUIsGlobal()
    {
        if (this.UIsGlobal == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsGlobal;
        }
    } 
    
    public void setUIsGlobal(Boolean UIsGlobal)
    {
        this.UIsGlobal = UIsGlobal;
    }
    
    public Boolean getUIsSelf()
    {
        return this.UIsSelf;
    }
    
    public void setUIsSelf(Boolean UIsSelf)
    {
        this.UIsSelf = UIsSelf;
    }
    
    public Boolean ugetUIsSelf()
    {
        if(this.UIsSelf == null)
        {
            return new Boolean(false);
        }
        else
        {
            return new Boolean(true);
        }
    }
    
    public Boolean getUIsRole()
    {
        return this.UIsRole;
    }
    
    public void setUIsRole(Boolean UIsRole)
    {
        this.UIsRole = UIsRole;
    }
    
    public Boolean ugetUIsRole()
    {
        if(this.UIsRole == null)
        {
            return new Boolean(false);
        }
        else
        {
            return new Boolean(true);
        }
    }
    
    public String getUUserId()
    {
        return this.UUserId;
    }

    public void setUUserId(String UUserId)
    {
        this.UUserId = UUserId;
    }
    
    public MetaAccessRightsVO getMetaAccessRights()
    {
        return metaAccessRights;
    }

    public void setMetaAccessRights(MetaAccessRightsVO metaAccessRights)
    {
        this.metaAccessRights = metaAccessRights;
    }
    
    public MetaTableVO getMetaTable()
    {
        return metaTable;
    }
    
    public void setMetaTable(MetaTableVO metaTable)
    {
        this.metaTable = metaTable;
        
        if(metaTable != null)
        {
            Collection<MetaFilterVO> coll = metaTable.getMetaFilters();
            if (coll == null)
            {
                coll = new HashSet<MetaFilterVO>();
                coll.add(this);
                
                metaTable.setMetaFilters(coll);
            }
        }
    }
}
