package com.resolve.services.hibernate.vo;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class PullGatewayFilterVO extends GatewayFilterVO
{
    private static final long serialVersionUID = 1L;
    
    private Boolean UDeployed;
    private Boolean UUpdated;
    private String UGatewayName;
    private String UQuery;
    private String UTimeRange;
    private String ULastId;
    private String ULastValue;
    private String ULastTimestamp;
    
    private Map<String, String> attrs;
    
    @MappingAnnotation(columnName="DEPLOYED")
    public Boolean getUDeployed()
    {
        return UDeployed;
    }

    public void setUDeployed(Boolean uDeployed)
    {
        UDeployed = uDeployed;
    }
    
    @MappingAnnotation(columnName="UPDATED")
    public Boolean getUUpdated()
    {
        return UUpdated;
    }

    public void setUUpdated(Boolean uUpdated)
    {
        UUpdated = uUpdated;
    }

    @MappingAnnotation(columnName="GATEWAY_NAME")
    public String getUGatewayName()
    {
        return UGatewayName;
    }

    public void setUGatewayName(String uGatewayName)
    {
        UGatewayName = uGatewayName;
    }
    
    @MappingAnnotation(columnName = "QUERY")
    public String getUQuery() {
        return this.UQuery;
    }

    public void setUQuery(String uQuery) {
        this.UQuery = uQuery;
    }

    @MappingAnnotation(columnName="TIME_RANGE")
    public String getUTimeRange()
    {
        return UTimeRange;
    }

    public void setUTimeRange(String uTimeRange)
    {
        UTimeRange = uTimeRange;
    }

    @MappingAnnotation(columnName="LAST_ID")
    public String getULastId()
    {
        return ULastId;
    }

    public void setULastId(String uLastId)
    {
        ULastId = uLastId;
    }
    
    @MappingAnnotation(columnName="LAST_VALUE")
    public String getULastValue()
    {
        return ULastValue;
    }

    public void setULastValue(String uLastValue)
    {
        ULastValue = uLastValue;
    }

    @MappingAnnotation(columnName="LAST_TIMESTAMP")
    public String getULastTimestamp()
    {
        return ULastTimestamp;
    }

    public void setULastTimestamp(String uLastTimestamp)
    {
        ULastTimestamp = uLastTimestamp;
    }

    public Map<String, String> getAttrs() {
        return this.attrs;
    }

    public void setAttrs(Map<String, String> attrs) {
        this.attrs = attrs;
    }
    
    public Map<String, String> toMap() {
        
        Map<String, String> attrs = getAttrs();
        
        if(attrs == null)
            attrs = new HashMap<String, String>();

        attrs.put("id", this.getSys_id());

        Method[] methods = this.getClass().getMethods();
        
        for(Method method:methods) {
            String name = method.getName();
            if(method.getAnnotation(MappingAnnotation.class) != null) {
                try {
                    String attr = name.substring(3).toLowerCase();
                    Object value = method.invoke(this, (Object[])null);
                    
                    if(attr.equalsIgnoreCase("ulastvalue")) {
                        attrs.put("ulastValue", getAttrVal(value));
                        continue;
                    }
                    
                    if(attr.equalsIgnoreCase("ulastid")) {
                        attrs.put("ulastId", getAttrVal(value));
                        continue;
                    }
                    
                    if(attr.equalsIgnoreCase("ulasttimestamp")) {
                        attrs.put("ulastTimestamp", getAttrVal(value));
                        continue;
                    }
                    
                    if(attr.equalsIgnoreCase("utimerange")) {
                        attrs.put("utimeRange", getAttrVal(value));
                        continue;
                    }
                    
                    if(attr.equalsIgnoreCase("ueventeventid")) {
                        attrs.put("ueventeventid", getAttrVal(value));
                        continue;
                    }
                    
                    if(attr.equalsIgnoreCase("ugatewayname")) {
                        attrs.put("ugatewayName", getAttrVal(value));
                        continue;
                    }
                    
                    if(attr.equals("uupdated"))
                        attr = "changed";
                    
                    if(value != null)
                        attrs.put(attr, getAttrVal(value));
                } catch(Exception e) {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }

        return attrs;
    }
    
    private String getAttrVal(Object value) {
        return value == null ? "" : value.toString();
    }
    
    @Override
    public boolean equals(Object obj) {

        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;

        PullGatewayFilterVO other = (PullGatewayFilterVO) obj;
        
        if(!super.equals(other))
            return false;
        
        if (UQuery == null) {
            if (StringUtils.isNotBlank(other.UQuery)) 
                return false;
        }

        else if (!UQuery.trim().equals(other.UQuery == null ? "" : other.UQuery.trim())) 
            return false;

        if (UTimeRange == null) {
            if (StringUtils.isNotBlank(other.UTimeRange)) 
                return false;
        }

        else if (!UTimeRange.trim().equals(other.UTimeRange == null ? "" : other.UTimeRange.trim())) 
            return false;
        
        if (ULastId == null) {
            if (StringUtils.isNotBlank(other.ULastId)) 
                return false;
        }

        else if (!ULastId.trim().equals(other.ULastId == null ? "" : other.ULastId.trim())) 
            return false;
        
        if (ULastValue == null) {
            if (StringUtils.isNotBlank(other.ULastValue)) 
                return false;
        }

        else if (!ULastValue.trim().equals(other.ULastValue == null ? "" : other.ULastValue.trim())) 
            return false;
        
        if (ULastTimestamp == null) {
            if (StringUtils.isNotBlank(other.ULastTimestamp)) 
                return false;
        }

        else if (!ULastTimestamp.trim().equals(other.ULastTimestamp == null ? "" : other.ULastTimestamp.trim())) 
            return false;
        
        Map<String, String> otherAttrs = other.getAttrs();
        
        for(Iterator<String> it=attrs.keySet().iterator();it.hasNext();) {
            String key = it.next();
            String value = attrs.get(key);
            String otherValue = otherAttrs.get(key);
            
            if (value == null) {
                if (otherValue != null) 
                    return false;
            }

            else if (!value.equals(otherValue)) 
                return false;
        }
        
        return true;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UGatewayName == null) ? 0 : UGatewayName.hashCode());
        result = prime * result + ((UQuery == null) ? 0 : UQuery.hashCode());
        return result;
    }
} // class PullFilterVO
