/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveAppOrganizationRelVO extends VO
{
    private static final long serialVersionUID = 5425510790921132855L;
    
    private ResolveAppsVO resolveApp;
    private OrganizationVO organization;
    
    public ResolveAppOrganizationRelVO()
    {
    }

    public ResolveAppsVO getResolveApp()
    {
        return resolveApp;
    }

    public void setResolveApp(ResolveAppsVO resolveApp)
    {
        this.resolveApp = resolveApp;
        
    }

    public OrganizationVO getOrganization()
    {
        return organization;
    }

    public void setOrganization(OrganizationVO organization)
    {
        this.organization = organization;
    }

    

} // ResolveControllersVO
