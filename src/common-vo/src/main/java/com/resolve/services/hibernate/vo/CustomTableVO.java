/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;

import com.resolve.services.interfaces.VO;



public class CustomTableVO extends VO
{
    private static final long serialVersionUID = -6338622065474970123L;
    
    private String UName;
    private String UModelName;
    private String UDisplayName;
    private String USchemaDefinition;
    private String UType;
    private String USource;
    private String UDestination;
    private String UReferenceTableNames;
    
    // object references
    private MetaTableVO metaTable;
    
    // Object refrences by
    private Collection<MetaFieldVO> metaFields;
    
    public CustomTableVO()
    {
    }
    
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    public String getUModelName()
    {
        return this.UModelName;
    }

    public void setUModelName(String UModelName)
    {
        this.UModelName = UModelName;
    }
    
    public String getUDisplayName()
    {
        return this.UDisplayName;
    }

    public void setUDisplayName(String UDisplayName)
    {
        this.UDisplayName = UDisplayName;
    }
    
    public String getUSchemaDefinition()
    {
        return this.USchemaDefinition;
    }

    public void setUSchemaDefinition(String USchemaDefinition)
    {
        this.USchemaDefinition = USchemaDefinition;
    }
    
    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }
    
    public String getUSource()
    {
        return this.USource;
    }

    public void setUSource(String USource)
    {
        this.USource = USource;
    }
    
    public String getUDestination()
    {
        return this.UDestination;
    }

    public void setUDestination(String UDestination)
    {
        this.UDestination = UDestination;
    }

    public MetaTableVO getMetaTable()
    {
        return metaTable;
    }
    
    public void setMetaTable(MetaTableVO metaTable)
    {
        this.metaTable = metaTable;
    }
    
    
    public Collection<MetaFieldVO> getMetaFields()
    {
        return this.metaFields;
    }

    public void setMetaFields(Collection<MetaFieldVO> metaFields)
    {
        this.metaFields = metaFields;
    }

    public String getUReferenceTableNames()
    {
        return UReferenceTableNames;
    }

    public void setUReferenceTableNames(String uReferenceTableNames)
    {
        UReferenceTableNames = uReferenceTableNames;
    }
    
    
    
}
