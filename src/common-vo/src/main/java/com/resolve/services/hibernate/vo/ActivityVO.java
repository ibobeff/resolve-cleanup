package com.resolve.services.hibernate.vo;

import java.util.Date;

public class ActivityVO
{
	private String id;
	private String activityName;
	private String description;
	private String phase;
	private String wikiName;
	private String incidentId;
	private String worksheetId;
	private Integer days;
	private Integer hours;
	private Integer minutes;
	private String status;
	private String assignee;
	private String altActivityId;
	private Date dueDate;
	private Boolean templateActivity;
	private String auditMessage;
	private Boolean isRequired;
	private String metaPhaseId;
	private String metaActivityId;
	private Integer position;
	
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	
	public String getActivityName()
	{
		return activityName;
	}
	public void setActivityName(String activityName)
	{
		this.activityName = activityName;
	}
	
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	public String getPhase()
	{
		return phase;
	}
	public void setPhase(String phase)
	{
		this.phase = phase;
	}
	
	public String getWikiName()
	{
		return wikiName;
	}
	public void setWikiName(String wikiName)
	{
		this.wikiName = wikiName;
	}
	
	public String getIncidentId()
	{
		return incidentId;
	}
	public void setIncidentId(String incidentId)
	{
		this.incidentId = incidentId;
	}
	
	public String getWorksheetId()
	{
		return worksheetId;
	}
	public void setWorksheetId(String worksheetId)
	{
		this.worksheetId = worksheetId;
	}
	
	public Integer getDays()
	{
		return days;
	}
	public void setDays(Integer days)
	{
		this.days = days;
	}
	
	public Integer getHours()
	{
		return hours;
	}
	public void setHours(Integer hours)
	{
		this.hours = hours;
	}
	
	public Integer getMinutes()
	{
		return minutes;
	}
	public void setMinutes(Integer minutes)
	{
		this.minutes = minutes;
	}
	
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	
	public String getAssignee()
	{
		return assignee;
	}
	public void setAssignee(String assignee)
	{
		this.assignee = assignee;
	}
	
	public String getAltActivityId()
	{
		return altActivityId;
	}
	public void setAltActivityId(String altActivityId)
	{
		this.altActivityId = altActivityId;
	}
	
	public Date getDueDate()
	{
		return dueDate;
	}
	public void setDueDate(Date dueDate)
	{
		this.dueDate = dueDate;
	}
	
	public Boolean getTemplateActivity()
	{
		return templateActivity;
	}
	public void setTemplateActivity(Boolean templateActivity)
	{
		this.templateActivity = templateActivity;
	}
	
	public String getAuditMessage()
    {
        return auditMessage;
    }
    public void setAuditMessage(String auditMessage)
    {
        this.auditMessage = auditMessage;
    }
    
    public Boolean getIsRequired()
    {
        if (isRequired != null)
        {
            return isRequired;
        }
        else
        {
            return Boolean.TRUE;
        }
    }
    public void setIsRequired(Boolean isRequired)
    {
        this.isRequired = isRequired;
    }
    
    public String getMetaPhaseId()
    {
        return metaPhaseId;
    }
    
    public void setMetaPhaseId(String metaPhaseId)
    {
        this.metaPhaseId = metaPhaseId;
    }
    
    public String getMetaActivityId()
    {
        return metaActivityId;
    }
    
    public void setMetaActivityId(String metaActivityId)
    {
        this.metaActivityId = metaActivityId;
    }
    
    public Integer getPosition()
	{
		return position;
	}
    
	public void setPosition(Integer position)
	{
		this.position = position;
	}
}
