/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;

import com.resolve.services.interfaces.VO;

public class ResolveCatalogVO extends VO
{
    private static final long serialVersionUID = -5726758889698905327L;
    
    private String UName;
    private String UType;
    
    private Collection<ResolveCatalogNodeVO> catalogNodes;
    
    public ResolveCatalogVO() {}
    public ResolveCatalogVO(String sysId)
    {
        super.setSys_id(sysId);
    }
    
    public String getUName()
    {
        return UName;
    }

    public void setUName(String uName)
    {
        UName = uName;
    }
    
    public String getUType()
    {
        return UType;
    }

    public void setUType(String uType)
    {
        UType = uType;
    }
    
    public Collection<ResolveCatalogNodeVO> getCatalogNodes()
    {
        return catalogNodes;
    }

    public void setCatalogNodes(Collection<ResolveCatalogNodeVO> catalogNodes)
    {
        this.catalogNodes = catalogNodes;
    }

}
