/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;

import com.resolve.services.interfaces.VO;


public class MetaTableVO extends VO
{
    private static final long serialVersionUID = -5725517291520887699L;

    private String UName;
    
    // object referenced by
    private Collection<MetaTableViewVO> metaTableViews;
    private Collection<MetaFilterVO> metaFilters;
    
    public MetaTableVO()
    {
    }
    
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    public Collection<MetaTableViewVO> getMetaTableViews()
    {
        return this.metaTableViews;
    }

    public void setMetaTableViews(Collection<MetaTableViewVO> metaTableViews)
    {
        this.metaTableViews = metaTableViews;
    }
    
    public Collection<MetaFilterVO> getMetaFilters()
    {
        return this.metaFilters;
    }

    public void setMetaFilters(Collection<MetaFilterVO> metaFilters)
    {
        this.metaFilters = metaFilters;
    }
}
