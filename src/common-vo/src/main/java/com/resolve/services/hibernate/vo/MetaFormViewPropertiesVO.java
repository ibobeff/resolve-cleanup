/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;


public class MetaFormViewPropertiesVO extends VO
{
    private static final long serialVersionUID = 1678242494525723185L;
    
    private Integer ULabelWidth;
    private String ULabelAlign;
    
    
    public MetaFormViewPropertiesVO()
    {
    }
    
    public Integer getULabelWidth()
    {
        return this.ULabelWidth;
    }

    public void setULabelWidth(Integer labelWidth)
    {
        this.ULabelWidth = labelWidth;
    }
    
    public String getULabelAlign()
    {
        return ULabelAlign;
    }

        
}
