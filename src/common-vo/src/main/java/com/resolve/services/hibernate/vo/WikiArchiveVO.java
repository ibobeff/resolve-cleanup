/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class WikiArchiveVO extends VO
{
    private static final long serialVersionUID = -7185242087826319970L;
    
    private String UTableId;
    private String UTableName;
    private String UTableColumn;
    private Integer UVersion;
    private String UPatch;
    private String UComment;
    private Boolean UUserArchive;
    private Long USIRRefCount; // # of references to this wiki from SIR
    private Boolean UIsStable;

    public WikiArchiveVO()
    {
    }

    public String getUTableId()
    {
        return this.UTableId;
    }

    public void setUTableId(String UTableId)
    {
        this.UTableId = UTableId;
    }

    public String getUTableName()
    {
        return this.UTableName;
    }

    public void setUTableName(String UTableName)
    {
        this.UTableName = UTableName;
    }

    public String getUTableColumn()
    {
        return this.UTableColumn;
    }

    public void setUTableColumn(String UTableColumn)
    {
        this.UTableColumn = UTableColumn;
    }

    public Integer getUVersion()
    {
        return this.UVersion;
    }

    public void setUVersion(Integer UVersion)
    {
        this.UVersion = UVersion;
    }

    public String getUPatch()
    {
        return this.UPatch;
    }

    public void setUPatch(String UPatch)
    {
        this.UPatch = UPatch;
    }
    
    public String getUComment()
    {
        return this.UComment;
    }

    public void setUComment(String UComment)
    {
        this.UComment = UComment;
    }
    
    public Boolean getUUserArchive()
    {
        return UUserArchive;
    }

    public void setUUserArchive(Boolean uUserArchive)
    {
        UUserArchive = uUserArchive;
    }
    
    public Long getUSIRRefCount()
    {
        return this.USIRRefCount;
    }

    public void setUSIRRefCount(Long USIRRefCount)
    {
        this.USIRRefCount = USIRRefCount;
    }
    
    public Boolean getUIsStable() {
		return UIsStable;
	}

	public void setUIsStable(Boolean UIsStable) {
		this.UIsStable = UIsStable;
	}
}
