/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveAssessRelVO extends VO
{
    private static final long serialVersionUID = -2587616886708886199L;
    
    private ResolveAssessVO assessor;
    private String URefAssessor; 

    public ResolveAssessRelVO()
    {
    }
    
    public ResolveAssessVO getAssessor()
    {
        return assessor;
    }

    public void setAssessor(ResolveAssessVO assessor)
    {
        this.assessor = assessor;
    }
    
    public String getURefAssessor()
    {
        return URefAssessor;
    }

    public void setURefAssessor(String refAssessor)
    {
        this.URefAssessor = refAssessor;
    }

}
