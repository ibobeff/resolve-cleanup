/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.interfaces.VO;

/**
 *
 * This is the assembler table where we say a User is registered to receive a specific Nofication based on a Notification Rule for a specific Node
 * 
 * @author jeet.marwah
 *
 */
public class SocialUserNotificationVO  extends VO
{
    private static final long serialVersionUID = -5706289972956396947L;
    
    private UsersVO user;
//    private SocialNotificationsVO notification;
//    private SocialNotificationRuleVO notificationRule;//MAYBE WE CAN REMOVE THIS FOR NOW
    private ResolveNodeVO node;
    
    //maybe we don't have to create a table for the rule.
    private UserGlobalNotificationContainerType UNotification;// Mapped to UserGlobalNotificationContainerType
    private String UNotificationRule;// Mapped to NotificationRuleEnum
    private String UNotificationValue;
    
    public SocialUserNotificationVO() {}
    public SocialUserNotificationVO(String sysId) 
    {
        this.setSys_id(sysId);
    }
    
    public UsersVO getUser()
    {
        return user;
    }

    public void setUser(UsersVO user)
    {
        this.user = user;
    }
    
    public ResolveNodeVO getNode()
    {
        return node;
    }
    
    public void setNode(ResolveNodeVO node)
    {
        this.node = node;
    }
    
    public String getUNotificationRule()
    {
        return UNotificationRule;
    }

    public void setUNotificationRule(String uNotificationRule)
    {
        UNotificationRule = uNotificationRule;
    }
    
    public String getUNotificationValue()
    {
        return UNotificationValue;
    }

    public void setUNotificationValue(String uNotificationValue)
    {
        UNotificationValue = uNotificationValue;
    }
    
    public UserGlobalNotificationContainerType getUNotification()
    {
        return UNotification;
    }
    
    public void setUNotification(UserGlobalNotificationContainerType uNotification)
    {
        UNotification = uNotification;
    }
}