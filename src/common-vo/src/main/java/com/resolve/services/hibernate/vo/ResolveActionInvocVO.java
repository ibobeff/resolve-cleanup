/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.ArrayList;
import java.util.Collection;

import com.resolve.services.interfaces.IndexComponent;
import com.resolve.services.interfaces.VO;


public class ResolveActionInvocVO extends VO implements IndexComponent
{
    private static final long serialVersionUID = -1419889060709961579L;
    
    private String UType;
    private String UCommand;
    private String UArgs;
    private Integer UTimeout;
    private String UMatchregex;
    private String UMatchvalue;
    private String UDescription;
    private Boolean UActive;
    private Integer UOrder;
    private String UTask;
    
    // object references
    private ResolvePreprocessVO preprocess;       // UPreprocess;
    private ResolveParserVO parser;               // UParser;
    private ResolveAssessVO assess;               // UAssess;
    
    private Collection<ResolveTaskOutputMappingVO> outputMappings;
	private Collection<ResolveTaskExpressionVO> expressions;
    
    // object referenced by
    private Collection<ResolveActionInvocOptionsVO> resolveActionInvocOptions;
	private Collection<ResolveActionParameterVO> resolveActionParameters;
	private Collection<ResolveActionTaskMockDataVO> resolveActionTaskMockData;

    public ResolveActionInvocVO()
    {
    	outputMappings = new ArrayList<ResolveTaskOutputMappingVO>();
    	expressions = new ArrayList<ResolveTaskExpressionVO>();
    	resolveActionInvocOptions = new ArrayList<ResolveActionInvocOptionsVO>();
    	resolveActionParameters = new ArrayList<ResolveActionParameterVO>();
    	resolveActionTaskMockData = new ArrayList<ResolveActionTaskMockDataVO>();
    }

    public String getUTask()
    {
        return this.UTask;
    } // getTask

    public void setUTask(String UTask)
    {
        this.UTask = UTask;
        
//        if(task != null)
//        {
//        	task.usetResolveActionInvoc(this);
//        }
    } // setTask

    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    public String getUCommand()
    {
        return this.UCommand;
    }

    public void setUCommand(String UCommand)
    {
        this.UCommand = UCommand;
    }

    public String getUArgs()
    {
        return this.UArgs;
    }

    public void setUArgs(String UArgs)
    {
        this.UArgs = UArgs;
    }

	public ResolvePreprocessVO getPreprocess()
	{
		return preprocess;
	}

	public void setPreprocess(ResolvePreprocessVO preprocess)
	{
		this.preprocess = preprocess;
	}

	public ResolveParserVO getParser()
	{
		return parser;
	}

	public void setParser(ResolveParserVO parser)
	{
		this.parser = parser;
	}
    
	public ResolveAssessVO getAssess()
	{
		return assess;
	}

	public void setAssess(ResolveAssessVO assess)
	{
		this.assess = assess;
		
	}
	
	public Collection<ResolveTaskOutputMappingVO> getOutputMappings()
	{
		return outputMappings;
	}
	public void setOutputMappings(Collection<ResolveTaskOutputMappingVO> outputMappings)
	{
		this.outputMappings = outputMappings;
	}
	
	public Collection<ResolveTaskExpressionVO> getExpressions()
	{
		return expressions;
	}
	
	public void setExpressions(Collection<ResolveTaskExpressionVO> expressions)
	{
		this.expressions = expressions;
	}

	public Integer getUTimeout()
    {
        return this.UTimeout;
    }

    public void setUTimeout(Integer UTimeout)
    {
        this.UTimeout = UTimeout;
    }

    public String getUMatchregex()
    {
        return this.UMatchregex;
    }

    public void setUMatchregex(String UMatchregex)
    {
        this.UMatchregex = UMatchregex;
    }

    public String getUMatchvalue()
    {
        return this.UMatchvalue;
    }

    public void setUMatchvalue(String UMatchvalue)
    {
        this.UMatchvalue = UMatchvalue;
    }

    public String getUDescription()
    {
        return this.UDescription;
    }

    public void setUDescription(String UDescription)
    {
        this.UDescription = UDescription;
    }

    public Boolean getUActive()
    {
        return this.UActive;
    }

    public void setUActive(Boolean UActive)
    {
        this.UActive = UActive;
    }

    public Integer getUOrder()
    {
        return this.UOrder;
    }

    public void setUOrder(Integer UOrder)
    {
        this.UOrder = UOrder;
    }

    public Collection<ResolveActionInvocOptionsVO> getResolveActionInvocOptions()
    {
        return resolveActionInvocOptions;
    } // getResolveActionInvocOptions

    public void setResolveActionInvocOptions(Collection<ResolveActionInvocOptionsVO> options)
    {
        this.resolveActionInvocOptions = options;
    } // setResolveActionInvocOptions

	public Collection<ResolveActionParameterVO> getResolveActionParameters()
	{
		return resolveActionParameters;
	}

	public void setResolveActionParameters(Collection<ResolveActionParameterVO> resolveActionParameters)
	{
		this.resolveActionParameters = resolveActionParameters;
	}

	public Collection<ResolveActionTaskMockDataVO> getResolveActionTaskMockData()
    {
        return resolveActionTaskMockData;
    }

    public void setResolveActionTaskMockData(Collection<ResolveActionTaskMockDataVO> resolveActionTaskMockData)
    {
        this.resolveActionTaskMockData = resolveActionTaskMockData;
    }

    @Override
	public String toString()
	{
		StringBuffer str = new StringBuffer();

		str.append("Action Invocation Type:").append(UType).append("\n");
		str.append("Action Invocation Description:").append(UDescription).append("\n");
		
		return str.toString();
	}

    public String ugetIndexContent()
    {
        StringBuilder indexContent = new StringBuilder();
        indexContent.append(UCommand + " " + UArgs);
        indexContent.append(" ");
        indexContent.append(UArgs);
        if(preprocess != null)
        {
            indexContent.append(" ");
            indexContent.append(preprocess.ugetIndexContent());
        }
        if(parser != null)
        {
            indexContent.append(" ");
            indexContent.append(parser.ugetIndexContent());
        }
        if(assess != null)
        {
            indexContent.append(" ");
            indexContent.append(assess.ugetIndexContent());
        }
        if(resolveActionInvocOptions != null)
        {
            for(ResolveActionInvocOptionsVO resolveActionInvocOption : resolveActionInvocOptions)
            {
                indexContent.append(" ");
                indexContent.append(resolveActionInvocOption.ugetIndexContent());
            }
        }
        if(resolveActionParameters != null)
        {
            for(ResolveActionParameterVO resolveActionParameter : resolveActionParameters)
            {
                indexContent.append(" ");
                indexContent.append(resolveActionParameter.ugetIndexContent());
            }
        }
        return indexContent.toString();
    }

} // ResolveActionInvoc
