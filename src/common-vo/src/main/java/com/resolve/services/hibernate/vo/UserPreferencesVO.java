/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class UserPreferencesVO extends VO
{
    private static final long serialVersionUID = 116015956962950284L;
    
    private String UPrefGroup;
    private String UPrefKey;
    private String UPrefValue;
    private String UPrefDescription;
    
    private UsersVO user;
    
    public String getUPrefGroup()
    {
        return UPrefGroup;
    }

    public void setUPrefGroup(String prefGroup)
    {
        this.UPrefGroup = prefGroup;
    }

    public String getUPrefDescription()
    {
        return UPrefDescription;
    }

    public void setUPrefDescription(String prefDescription)
    {
        this.UPrefDescription = prefDescription;
    }

    public String getUPrefValue()
    {
        return UPrefValue;
    }

    public void setUPrefValue(String prefValue)
    {
        this.UPrefValue = prefValue;
    }
    
    public String getUPrefKey()
    {
        return UPrefKey;
    }
    
    public void setUPrefKey (String prefKey)
    {
        this.UPrefKey = prefKey;
    }
    
   
    public UsersVO getUser()
    {
        return this.user;
    }

    public void setUser(UsersVO user)
    {
        this.user = user;
    }


}
