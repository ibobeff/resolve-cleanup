/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.interfaces.UIDisplayAPI;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

public class ResolvePropertiesVO extends VO implements UIDisplayAPI
{
    private static final long serialVersionUID = 8978549831799583052L;
    
    private String UName;
    private String UValue;
    private String UModule;
    private String UType;

    public ResolvePropertiesVO()
    {
    }
    
    public boolean validate()
    {
        boolean valid = true;

        if(StringUtils.isEmpty(getUName()) || StringUtils.isEmpty(getUModule()) ||StringUtils.isEmpty(getUType()))
        {
            valid = false;
            throw new RuntimeException("Name, Module name, and Type are mandatory");
        }

        valid = getUName().matches(HibernateConstants.REGEX_ALPHANUMERIC_WITH_UNDERSCORE_DOT_DASH);
        if(!valid)
        {
            throw new RuntimeException("Name can have only alphanumerics,'_' and '.'");
        }
        
        valid = getUModule().matches(HibernateConstants.REGEX_ALPHANUMERIC_WITH_UNDERSCORE_DOT_DASH_SPACE);
        if(!valid)
        {
            throw new RuntimeException("Module name can have only alphanumerics and '_'");
        }
        

        return valid;
    }

    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    public String getUValue()
    {
        return this.UValue;
    }

    public void setUValue(String UValue)
    {
        this.UValue = UValue;
    }

    public String getUModule()
    {
        return this.UModule;
    }

    public void setUModule(String UModule)
    {
        this.UModule = UModule;
    }

    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    public String ui_getDisplayName()
	{
		return UName;
	}

	
	public String ui_getDisplayType()
	{
		return "Property";
	}

}
