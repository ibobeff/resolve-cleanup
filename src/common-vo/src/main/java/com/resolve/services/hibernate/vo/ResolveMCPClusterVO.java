package com.resolve.services.hibernate.vo;

import java.util.Collection;

import com.resolve.services.interfaces.VO;

public class ResolveMCPClusterVO extends VO
{
    private static final long serialVersionUID = 1632331462775841232L;
    
    private String UClusterName;
    private Boolean UIsMonitored;
    private ResolveMCPGroupVO group;
    
    private Collection<ResolveMCPHostVO> children;

    public String getUClusterName()
    {
        return UClusterName;
    }

    public void setUClusterName(String UClusterName)
    {
        this.UClusterName = UClusterName;
    }

    public Boolean getUIsMonitored()
	{
		return UIsMonitored;
	}
	public void setUIsMonitored(Boolean uIsMonitored)
	{
		UIsMonitored = uIsMonitored;
	}

	public ResolveMCPGroupVO getGroup()
    {
        return group;
    }

    public void setGroup(ResolveMCPGroupVO group)
    {
        this.group = group;
    }

    public Collection<ResolveMCPHostVO> getChildren()
    {
        return children;
    }

    public void setChildren(Collection<ResolveMCPHostVO> children)
    {
        this.children = children;
    }
}
