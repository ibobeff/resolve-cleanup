/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;

import com.resolve.services.interfaces.VO;


public class MetaFormVO extends VO
{
    private static final long serialVersionUID = 5236704147212793950L;

    private String UName;
    
    
    // object referenced by
    private Collection<MetaFormViewVO> metaFormViews;
    
    public MetaFormVO()
    {
    }
    
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    public Collection<MetaFormViewVO> getMetaFormViews()
    {
        return this.metaFormViews;
    }

    public void setMetaFormViews(Collection<MetaFormViewVO> metaFormViews)
    {
        this.metaFormViews = metaFormViews;
    }
}
