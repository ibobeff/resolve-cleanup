/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;



public class MetaViewFieldVO extends VO
{
    private static final long serialVersionUID = 4766858572636459619L;
    
    private String UId;
    private String UHeader;
    private Integer UWidth;
    private Boolean URowHeader;
    private String UAlignment;
    //private String URendererJavascript;
    private Boolean UResizeable;
    private Boolean USortable;
    private String UToolTip;
    private Boolean UGroupable;
    private Boolean UFixed;
    private String UDateTimeFormat;
    
    public MetaViewFieldVO()
    {
    }
    
    public String getUId()
    {
        return this.UId;
    }

    public void setUId(String UId)
    {
        this.UId = UId;
    }
    
    public String getUHeader()
    {
        return this.UHeader;
    }

    public void setUHeader(String UHeader)
    {
        this.UHeader = UHeader;
    }
    
    public Integer getUWidth()
    {
        return this.UWidth;
    }
    
    public void setUWidth(Integer UWidth)
    {
        this.UWidth = UWidth;
    }
    
    public Boolean getURowHeader()
    {
        return this.URowHeader;
    }
    
    public void setURowHeader(Boolean URowHeader)
    {
        this.URowHeader = URowHeader;
    }
    
    public String getUAlignment()
    {
        return this.UAlignment;
    }

    public void setUAlignment(String UAlignment)
    {
        this.UAlignment = UAlignment;
    }
    
    public Boolean getUResizeable()
    {
        return this.UResizeable;
    }
    
    public void setUResizeable(Boolean UResizeable)
    {
        this.UResizeable = UResizeable;
    }
    
    public Boolean getUSortable()
    {
        return this.USortable;
    }
    
    public void setUSortable(Boolean USortable)
    {
        this.USortable = USortable;
    }
    
    public String getUToolTip()
    {
        return this.UToolTip;
    }

    public void setUToolTip(String UToolTip)
    {
        this.UToolTip = UToolTip;
    }
    
    public Boolean getUGroupable()
    {
        return this.UGroupable;
    }
    
    public void setUGroupable(Boolean UGroupable)
    {
        this.UGroupable = UGroupable;
    }
    
    public Boolean getUFixed()
    {
        return this.UFixed;
    }
    
    public void setUFixed(Boolean UFixed)
    {
        this.UFixed = UFixed;
    }
    
    public String getUDateTimeFormat()
    {
        return this.UDateTimeFormat;
    }

    public void setUDateTimeFormat(String UDateTimeFormat)
    {
        this.UDateTimeFormat = UDateTimeFormat;
    }
    
}
