/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
public class NamePropertyVO extends VO
{
    private static final long serialVersionUID = -1941260150536313381L;
	private String UName;
    private String UQueue;
    private byte[] UContent;

    public NamePropertyVO()
    {
    }

    public String getUName()
    {
        return UName;
    }

    public void setUName(String uName)
    {
        UName = uName;
    }

    public String getUQueue()
    {
        return UQueue;
    }

    public void setUQueue(String uQueue)
    {
        UQueue = uQueue;
    }

    public byte[] getUContent()
    {
        return UContent;
    }

    public void setUContent(byte[] uContent)
    {
        UContent = uContent;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UName == null) ? 0 : UName.hashCode());
        result = prime * result + ((UQueue == null) ? 0 : UQueue.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        NamePropertyVO other = (NamePropertyVO) obj;
        if (UName == null)
        {
            if (other.UName != null) return false;
        }
        else if (!UName.equals(other.UName)) return false;
        if (UQueue == null)
        {
            if (other.UQueue != null) return false;
        }
        else if (!UQueue.equals(other.UQueue)) return false;
        return true;
    }
}
