/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;


public class MetaxFormViewPanelVO extends VO
{
    private static final long serialVersionUID = 7826080231199133154L;
    
    private String UPanelTitle;
    private Integer UOrder;
    private String UUrl;//if this has value, the 'metaFormTabFields' will be NULL or EMPTY
    private Integer UNoOfVerticalColumns; //1 or 2
    private Integer UPanelWidth;
//    private Boolean UIsTabPanel;
    private String UXType;//this will be rspanel or rstabpanel based on what kind of panel it is.

    
    // object referenced by
    private MetaFormViewVO metaFormView;
//    private Collection<MetaxPanelFieldRel> metaxPanelFieldRels;
    private Collection<MetaFormTabVO> metaFormTabs;
    
    
    public MetaxFormViewPanelVO()
    {
    }
    
    public Integer getUOrder()
    {
        return UOrder;
    }
    /**
     * @param uOrder the uOrder to set
     */
    public void setUOrder(Integer uOrder)
    {
        UOrder = uOrder;
    }
    
    /**
     * @return the uNoOfVerticalColumns
     */
    public Integer getUNoOfVerticalColumns()
    {
        return UNoOfVerticalColumns;
    }
    public void setUNoOfVerticalColumns(Integer uNoOfVerticalColumns)
    {
        UNoOfVerticalColumns = uNoOfVerticalColumns;
    }

    /**
     * @return the uSource
     */
    public String getUUrl()
    {
        return UUrl;
    }
    public void setUUrl(String uSource)
    {
        UUrl = uSource;
    }


    public String getUPanelTitle()
    {
        return UPanelTitle;
    }

    public void setUPanelTitle(String uPanelTitle)
    {
        UPanelTitle = uPanelTitle;
    }

    public Integer getUPanelWidth()
    {
        return UPanelWidth;
    }

    public void setUPanelWidth(Integer uPanelWidth)
    {
        UPanelWidth = uPanelWidth;
    }

    public MetaFormViewVO getMetaFormView()
    {
        return metaFormView;
    }

    public void setMetaFormView(MetaFormViewVO metaFormView)
    {
        this.metaFormView = metaFormView;

        if (metaFormView != null)
        {
            Collection<MetaxFormViewPanelVO> coll = metaFormView.getMetaxFormViewPanels();
            if (coll == null)
            {
                coll = new HashSet<MetaxFormViewPanelVO>();
                coll.add(this);
                
                metaFormView.setMetaxFormViewPanels(coll);
            }
        }
    }

//    @OneToMany(mappedBy = "metaxFormViewPanel")
//    @NotFound(action = NotFoundAction.IGNORE)
//    public Collection<MetaxPanelFieldRel> getMetaxPanelFieldRels()
//    {
//        return metaxPanelFieldRels;
//    }
//
//    public void setMetaxPanelFieldRels(Collection<MetaxPanelFieldRel> metaxPanelFieldRels)
//    {
//        this.metaxPanelFieldRels = metaxPanelFieldRels;
//    }
    
    public Collection<MetaFormTabVO> getMetaFormTabs()
    {
        return this.metaFormTabs;
    }

    public void setMetaFormTabs(Collection<MetaFormTabVO> metaFormTabs)
    {
        this.metaFormTabs = metaFormTabs;
    }

    public String getUXType()
    {
        return UXType;
    }

    public void setUXType(String uXType)
    {
        UXType = uXType;
    }
    
    
    

}//MetaFormTab
