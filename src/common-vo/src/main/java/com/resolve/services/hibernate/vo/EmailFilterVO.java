/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class EmailFilterVO extends GatewayFilterVO
{
    private static final long serialVersionUID = 2628896675742698526L;

    private String UQuery;

    // object referenced by
    public EmailFilterVO()
    {
        super();
    } // EmailFilter

    @MappingAnnotation(columnName="QUERY")
    public String getUQuery()
    {
        return this.UQuery;
    } // getUQuery

    public void setUQuery(String uQuery)
    {
        if (StringUtils.isNotBlank(uQuery) || UQuery.equals(VO.STRING_DEFAULT))
        {
            this.UQuery = uQuery != null ? uQuery.trim() : uQuery;
        }
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UQuery == null) ? 0 : UQuery.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        EmailFilterVO other = (EmailFilterVO) obj;
        if (UQuery == null)
        {
            if (StringUtils.isNotBlank(other.UQuery)) return false;
        }
        else if (!UQuery.trim().equals(other.UQuery == null ? "" : other.UQuery.trim())) return false;
        return true;
    }
}
