/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;


public class MetaSourceVO extends VO
{   
    private static final long serialVersionUID = 3664275851357692340L;
    
    private String UName;
    private String UDisplayName;
    private String USource; 
    private String UType;
    private String UDbType;

    
    // object referenced
    private MetaFormViewVO metaFormView;
//    private MetaAccessRights metaAccessRights;//will be based on view access rights 
    private MetaFieldPropertiesVO metaFieldProperties;
    
    public MetaSourceVO()
    {
    }
    
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    public String getUSource()
    {
        return this.USource;
    }

    public void setUSource(String USource)
    {
        this.USource = USource;
    }
    
    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }
    
    public String getUDbType()
    {
        return this.UDbType;
    }

    public void setUDbType(String UDbType)
    {
        this.UDbType = UDbType;
    }
    
    public String getUDisplayName()
    {
        return this.UDisplayName;
    }

    public void setUDisplayName(String UDisplayName)
    {
        this.UDisplayName = UDisplayName;
    }
    

    public MetaFieldPropertiesVO getMetaFieldProperties()
    {
        return this.metaFieldProperties;
    }

    public void setMetaFieldProperties(MetaFieldPropertiesVO metaFieldProperties)
    {
        this.metaFieldProperties = metaFieldProperties;
    }
    
    public MetaFormViewVO getMetaFormView()
    {
        return metaFormView;
    }

    public void setMetaFormView(MetaFormViewVO metaFormView)
    {
        this.metaFormView = metaFormView;

        if (metaFormView != null)
        {
            Collection<MetaSourceVO> coll = metaFormView.getMetaSources();
            if (coll == null)
            {
                coll = new HashSet<MetaSourceVO>();
                coll.add(this);
                
                metaFormView.setMetaSources(coll);
            }
        }
    }
    
    
    
}
