package com.resolve.services.hibernate.vo;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.time.Duration;

import com.resolve.services.interfaces.VO;

public class SIRActivityMetaDataVO extends VO {
    
    private static final long serialVersionUID = 3687001309169329736L;
    
    private static final Duration DEFAULT_SLA_DURATION = Duration.ZERO;
    private static final Boolean DEFAULT_IS_REQAUIRED = Boolean.TRUE;
    
    private String name;
    private String description;
    private Boolean isRequired;
    private String refWikiSysId;
    private Boolean isTemplateActivity;
    private String legacyName;
    private Duration sLA;
    private String altActivityId;
    
    public SIRActivityMetaDataVO() {
        isTemplateActivity = DEFAULT_IS_REQAUIRED;
        sLA = DEFAULT_SLA_DURATION;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public Boolean getIsRequired() {
        return isRequired;
    }
    
    public void setIsRequired(Boolean isRequired) {
        if (isRequired != null) {
            this.isRequired = isRequired;
        } else {
            isRequired = Boolean.TRUE;
        }
    }
    
    public String getRefWikiSysId() {
        return refWikiSysId;
    }
    
    public void setRefWikiSysId(String refWikiSysId) {
        this.refWikiSysId = refWikiSysId;
    }
    
    public Boolean getIsTemplateActivity() {
        return isTemplateActivity;
    }
    
    public void setIsTemplateActivity(Boolean isTemplateActivity) {
        this.isTemplateActivity = isTemplateActivity;
    }
    
    public String getLegacyName() {
        return legacyName;
    }
    
    public void setLegacyName(String legacyName) {
        this.legacyName = legacyName;
    }
    
    public Duration getSLA() {
        return sLA;
    }
    
    public void setSLA(Duration sLA) {
        if (sLA != null && sLA.toNanos() >= 0) {
            this.sLA = Duration.ofNanos(sLA.toNanos());
        } else {
            sLA = DEFAULT_SLA_DURATION;
        }
    }
    
    public String getAltActivityId() {
        return altActivityId;
    }
    
    public void setAltActivityId(String altActivityId) {
        this.altActivityId = altActivityId;
    }
    
    @Override
    public String toString() {
        return "SIR Activity Meta Data VO [" +
               (isNotBlank(getSys_id()) ? "Sys Id: " + getSys_id() + ", " : "") + 
               "Sys Org: " + (isNotBlank(getSysOrg()) ? getSysOrg() : "") + 
               ", Name: " + name + ", Description: " + 
               (isNotBlank(getDescription()) ? (getDescription().length() > 50 ? getDescription().substring(0, 46) + "..." : getDescription()) : "" ) +
               ", Is Required: " + (getIsRequired() != null ? getIsRequired() : Boolean.TRUE) + ", Referenced Wiki Sys Id: " + getRefWikiSysId() +
               ", Is Template Activity: " + (getIsTemplateActivity() != null ? getIsTemplateActivity() : Boolean.TRUE) + 
               ", Legacy Name: " + (isNotBlank(getLegacyName()) ? getLegacyName() : "") +
               ", SLA: " + getSLA() +
               ", Alternate Activity Id: " + getAltActivityId() + 
               "]";
    }
}
