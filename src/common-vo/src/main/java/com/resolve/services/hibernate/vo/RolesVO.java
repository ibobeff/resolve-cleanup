/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Date;
import java.util.List;

import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;


public class RolesVO extends VO  implements Comparable<RolesVO>
{
    private static final long serialVersionUID = -3443149169453469851L;
    
    private String UName;
    private String UDescription;
    private JSONObject UPermissions;

    // object referenced by
    private List<UserRoleRelVO> userRoleRels;
    private List<GroupRoleRelVO> groupRoleRels;
    private List<RoleWikidocHomepageRelVO> roleWikidocHomepageRels;

    public RolesVO()
    {
    }

    public RolesVO(String sys_id)
    {
        this.setSys_id(sys_id);
    }

    public RolesVO(String sys_id, String UName, String UDescription, String UPermissions, String sysUpdatedBy, Date sysUpdatedOn, String sysCreatedBy, Date sysCreatedOn,
                    Integer sysModCount)
    {
        this.setSys_id(sys_id);
        this.setUName(UName);
        this.setUDescription(UDescription);
        this.setUDescription(UPermissions);
        this.setSysUpdatedBy(sysUpdatedBy);
        this.setSysUpdatedOn(sysUpdatedOn);
        this.setSysCreatedBy(sysCreatedBy);
        this.setSysCreatedOn(sysCreatedOn);
        this.setSysModCount(sysModCount);
    }
    
    public boolean validate()
    {
        boolean valid = true;
        
        if(StringUtils.isEmpty(getUName()) || (StringUtils.isNotBlank(getUName()) && getUName().equalsIgnoreCase(VO.STRING_DEFAULT)))
        {
            valid = false;
            throw new RuntimeException("Role name cannot be empty.");
        }
        
        valid = getUName().matches(HibernateConstants.REGEX_ALPHANUMERIC_NO_SPACE);
        if(!valid)
        {
            throw new RuntimeException("Role name can have only Alpha numeric, & and _.");
        }
        
        
        return valid;
    }

	public String toString()
	{
	    return this.UName;
	} // toString

    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    public String getUDescription()
    {
        return this.UDescription;
    }

    public void setUDescription(String UDescription)
    {
        this.UDescription = UDescription;
    }

    public JSONObject getUPermissions()
    {
        return this.UPermissions;
    }

    public void setUPermissions(JSONObject UPermissions)
    {
        this.UPermissions = UPermissions;
    }

    public List<UserRoleRelVO> getUserRoleRels()
    {
        return userRoleRels;
    }

    public void setUserRoleRels(List<UserRoleRelVO> userRoleRels)
    {
        this.userRoleRels = userRoleRels;
    }

    public List<GroupRoleRelVO> getGroupRoleRels()
    {
        return groupRoleRels;
    }

    public void setGroupRoleRels(List<GroupRoleRelVO> groupRoleRels)
    {
        this.groupRoleRels = groupRoleRels;
    }

    public List<RoleWikidocHomepageRelVO> getRoleWikidocHomepageRels()
    {
        return roleWikidocHomepageRels;
    }

    public void setRoleWikidocHomepageRels(List<RoleWikidocHomepageRelVO> roleWikidocHomepageRels)
    {
        this.roleWikidocHomepageRels = roleWikidocHomepageRels;
    }

    public int compareTo(RolesVO that)
    {
        final int EQUAL = 0;
//        final int BEFORE = -1;
//        final int AFTER = 1;
        int result = EQUAL;
        
        //this optimization is usually worthwhile
        if ( this == that ) 
            result = EQUAL;
        else
        {
            String roleThis = this.getUName();
            String roleThat = that.getUName();
            
            if(StringUtils.isNotEmpty(roleThis) && StringUtils.isNotBlank(roleThat))
            {
                result = roleThis.compareTo(roleThat);
            }
            else
            {
                roleThis = this.getSys_id();
                roleThat = that.getSys_id();
                
                result = roleThis.compareTo(roleThat);
            }
        }
        
        return result;
    }
    
    @Override
    public boolean equals(Object aThat)
    {
        boolean result = false;
        
        if (this == aThat) 
            result = true;
        else if (!(aThat instanceof RolesVO)) 
            result = false;
        else 
        {
            RolesVO that = (RolesVO) aThat;
            result = this.getUName().equalsIgnoreCase(that.getUName());
        }
        
        return result;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 43;

        result = prime * result + ((getUName() == null) ? 0 : getUName().hashCode());

        return result;
    }

}
