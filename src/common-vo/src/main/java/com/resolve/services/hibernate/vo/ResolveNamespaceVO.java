package com.resolve.services.hibernate.vo;

import java.util.Set;

import com.resolve.services.interfaces.VO;

public class ResolveNamespaceVO extends VO {
	private static final long serialVersionUID = 1372045816283680838L;

	private String UName;
	private AccessRightsVO accessRights;
	private ResolveNamespaceVO parentNamespace;
	private Set<ResolveNamespaceVO> childNamespaces;

	public String getUName() {
		return UName;
	}

	public void setUName(String uName) {
		UName = uName;
	}

	public AccessRightsVO getAccessRights() {
		return accessRights;
	}

	public void setAccessRights(AccessRightsVO accessRights) {
		this.accessRights = accessRights;
	}

	public ResolveNamespaceVO getParentNamespace() {
		return parentNamespace;
	}

	public void setParentNamespace(ResolveNamespaceVO parentNamespace) {
		this.parentNamespace = parentNamespace;
	}

	public Set<ResolveNamespaceVO> getChildNamespaces() {
		return childNamespaces;
	}

	public void setChildNamespaces(Set<ResolveNamespaceVO> childNamespaces) {
		this.childNamespaces = childNamespaces;
	}
}
