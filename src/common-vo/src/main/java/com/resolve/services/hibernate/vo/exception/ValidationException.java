package com.resolve.services.hibernate.vo.exception;

public class ValidationException extends Exception {

    public ValidationException(String message) {
        super(message);
    }
}
