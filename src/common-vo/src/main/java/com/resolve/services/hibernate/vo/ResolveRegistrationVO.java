/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import com.resolve.services.interfaces.VO;

public class ResolveRegistrationVO extends VO
{
    private static final long serialVersionUID = 6980237502751169012L;
    
    private String UGuid;
    private String UName;
    private String UType;
    private String UStatus;
    private Date UCreatetime;
    private Date UUpdatetime;
    private Integer UCronoffset;
    private String UDescription;
    private String ULocation;
    private String UGroup;
    private String UConfig;
    private String UIpaddress;
    private String UVersion;
    private String UOrg;
    private String OS;

	
    // object referenced by
    private Collection<ResolveRegistrationPropertyVO> resolveRegistrationProperties;
	
    public ResolveRegistrationVO()
    {
    }


    public String getUGuid()
    {
        return this.UGuid;
    }

    public void setUGuid(String UGuid)
    {
        this.UGuid = UGuid;
    }

    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    public String getUStatus()
    {
        return this.UStatus;
    }

    public void setUStatus(String UStatus)
    {
        this.UStatus = UStatus;
    }

    public Date getUCreatetime()
    {
        return this.UCreatetime;
    }

    public void setUCreatetime(Date UCreatetime)
    {
        this.UCreatetime = UCreatetime;
    }

    public Date getUUpdatetime()
    {
        return this.UUpdatetime;
    }

    public void setUUpdatetime(Date UUpdatetime)
    {
        this.UUpdatetime = UUpdatetime;
    }

    public Integer getUCronoffset()
    {
        return this.UCronoffset;
    }

    public void setUCronoffset(Integer UCronoffset)
    {
        this.UCronoffset = UCronoffset;
    }

    public String getUDescription()
    {
        return this.UDescription;
    }

    public void setUDescription(String UDescription)
    {
        this.UDescription = UDescription;
    }

    public String getULocation()
    {
        return this.ULocation;
    }

    public void setULocation(String ULocation)
    {
        this.ULocation = ULocation;
    }

    public String getUGroup()
    {
        return this.UGroup;
    }

    public void setUGroup(String UGroup)
    {
        this.UGroup = UGroup;
    }

    public String getUConfig()
    {
        return this.UConfig;
    }

    public void setUConfig(String UConfig)
    {
        this.UConfig = UConfig;
    }

    public String getUIpaddress()
    {
        return this.UIpaddress;
    }

    public void setUIpaddress(String UIpaddress)
    {
        this.UIpaddress = UIpaddress;
    }

    public String getUVersion()
    {
        return this.UVersion;
    }

    public void setUVersion(String UVersion)
    {
        this.UVersion = UVersion;
    }

    public String getUOrg()
    {
        return UOrg;
    }
    
    public void setUOrg(String UOrg)
    {
        this.UOrg = UOrg;
    }

    public String getOS() {
        return OS;
    }
    public void setOS(String oS) {
        OS = oS;
    }


    public Collection<ResolveRegistrationPropertyVO> getResolveRegistrationProperties()
	{
		return resolveRegistrationProperties;
	}

	public void setResolveRegistrationProperties(Collection<ResolveRegistrationPropertyVO> resolveRegistrationProperties)
	{
		this.resolveRegistrationProperties = resolveRegistrationProperties;
	}

    @SuppressWarnings("rawtypes")
    public void initRegistration(Map data)
    {
        setUGuid((String) data.get("GUID"));
        setUName((String) data.get("NAME"));
        setUType((String) data.get("TYPE"));
        setUIpaddress((String) data.get("IPADDRESS"));
        setUVersion((String) data.get("VERSION"));
        setUDescription((String) data.get("DESCRIPTION"));
        setULocation((String) data.get("LOCATION"));
        setUGroup((String) data.get("GROUP"));
        setUConfig((String) data.get("CONFIG"));
        setUStatus("ACTIVE");
        setUCreatetime((Date) data.get("CREATETIME"));
        setUUpdatetime((Date) data.get("UPDATETIME"));
        setUOrg((String)data.get("ORG"));
        setOS((String)data.get("OS"));
    } // initRegistration

    public boolean isModified(ResolveRegistrationVO reg)
    {
        boolean result = false;

        if (reg != null)
        {
            if ((this.UName != null && !this.UName.equals(reg.getUName())) || (this.UType != null && !this.UType.equals(reg.getUType()))
                            || (this.UConfig != null && !this.UConfig.equals(reg.getUConfig()))
                            || (this.UIpaddress != null && !this.UIpaddress.equals(reg.getUIpaddress()))
                            || (this.UVersion != null && !this.UVersion.equals(reg.getUVersion()))
                            || (this.ULocation != null && !this.ULocation.equals(reg.getULocation()))
                            || (this.UGroup != null && !this.UGroup.equals(reg.getUGroup()))
                            || (this.UOrg != null && !this.UOrg.equals(reg.getUOrg()))
                            || (reg.getUOrg() != null && !reg.getUOrg().equals(this.UOrg))
                            || (reg.getOS() != null && !reg.getOS().equals(this.getOS())))
            {
                result = true;
            }
        }

        return result;
    } // isModified

    public void update(ResolveRegistrationVO reg)
    {
        if (reg != null)
        {
            this.setUName(reg.getUName());
            this.setUType(reg.getUType());
            this.setUConfig(reg.getUConfig());
            this.setUIpaddress(reg.getUIpaddress());
            this.setUVersion(reg.getUVersion());
            this.setULocation(reg.getULocation());
            this.setUGroup(reg.getUGroup());
            this.setUOrg(reg.getUOrg());
            this.setOS(reg.getOS());
        }
    } // update

} // ResolveRegistration
