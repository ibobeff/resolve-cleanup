/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

/**
 * This is a base VO for all the gateway filter to hold
 * all the common properties.
 *
 */
public class GatewayFilterVO extends GatewayVO
{
    private static final long serialVersionUID = 7127236913261371773L;
	private Integer UOrder;
    private String UEventEventId;
    private String URunbook;
    private Boolean UActive;
    private String UName;
    private Integer UInterval;
    private String UScript;

    // object referenced by
    protected GatewayFilterVO()
    {
        super();
    } // GatewayFilterVO

    @MappingAnnotation(columnName="ORDER")
    public Integer getUOrder()
    {
        return this.UOrder;
    } // getUOrder

    public void setUOrder(Integer UOrder)
    {
        this.UOrder = UOrder;
    } // setUOrder

    @MappingAnnotation(columnName="EVENT_EVENTID")
    public String getUEventEventId()
    {
        return UEventEventId;
    }

    public void setUEventEventId(String uEventEventId)
    {
        if (StringUtils.isNotBlank(uEventEventId) || UEventEventId.equals(STRING_DEFAULT))
        {
            UEventEventId = uEventEventId != null ? uEventEventId.trim() : uEventEventId;
        }
    }

    @MappingAnnotation(columnName="RUNBOOK")
    public String getURunbook()
    {
        return this.URunbook;
    } // getURunbook

    public void setURunbook(String URunbook)
    {
        if (StringUtils.isNotBlank(URunbook) || this.URunbook.equals(STRING_DEFAULT))
        {
            this.URunbook = URunbook != null ? URunbook.trim() : URunbook;
        }
    } // setURunbook

    @MappingAnnotation(columnName="ACTIVE")
    public Boolean getUActive()
    {
        return this.UActive;
    } // getUActive

    public void setUActive(Boolean UActive)
    {
        this.UActive = UActive;
    } // setUActive

    @MappingAnnotation(columnName="ID")
    public String getUName()
    {
        return this.UName;
    } // getName

    public void setUName(String UName)
    {
        if (StringUtils.isNotBlank(UName) || this.UName.equals(STRING_DEFAULT))
        {
            this.UName = UName != null ? UName.trim() : UName;
        }
    } // setUName

    @MappingAnnotation(columnName="INTERVAL")
    public Integer getUInterval()
    {
        return this.UInterval;
    } // getUInterval

    public void setUInterval(Integer UInterval)
    {
        this.UInterval = UInterval;
    } // setUInterval

    @MappingAnnotation(columnName="SCRIPT")
    public String getUScript()
    {
        return UScript;
    } //getUScript

    public void setUScript(String uScript)
    {
        if (StringUtils.isNotBlank(uScript) || UScript.equals(STRING_DEFAULT))
        {
            UScript = uScript != null ? uScript.trim() : uScript;
        }
    } //setUScript

    @Override
    public int hashCode()
    {
        
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UActive == null) ? 0 : UActive.hashCode());
        result = prime * result + ((UEventEventId == null) ? 0 : UEventEventId.hashCode());
        result = prime * result + ((UInterval == null) ? 0 : UInterval.hashCode());
        result = prime * result + ((UName == null) ? 0 : UName.hashCode());
        result = prime * result + ((UOrder == null) ? 0 : UOrder.hashCode());
        result = prime * result + ((URunbook == null) ? 0 : URunbook.hashCode());
        result = prime * result + ((UScript == null) ? 0 : UScript.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        /*
         * Do not call super.equals since gateway name in GatewayVO of deployed filter 
         * and saved filter is always different.
         * 
         * Never set queue name of any gateway to <|Default|Queue|>
         */
        
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;

        GatewayFilterVO other = (GatewayFilterVO) obj;
        if (UActive == null)
        {
            if (other.UActive != null) return false;
        }
        else if (!UActive.equals(other.UActive)) return false;
        if (UEventEventId == null)
        {
            if (StringUtils.isNotBlank(other.UEventEventId)) return false;
        }
        else if (!UEventEventId.trim().equals(other.UEventEventId == null ? "" : other.UEventEventId.trim())) return false;
        if (UInterval == null)
        {
            if (other.UInterval != null) return false;
        }
        else if (!UInterval.equals(other.UInterval)) return false;
        if (UName == null)
        {
            if (StringUtils.isNotBlank(other.UName)) return false;
        }
        else if (!UName.trim().equals(other.UName.trim())) return false;
        if (UOrder == null)
        {
            if (other.UOrder != null) return false;
        }
        else if (!UOrder.equals(other.UOrder)) return false;
//        if (UQueue == null)
//        {
//            if (other.UQueue != null) return false;
//        }
//        else if (!UQueue.equals(other.UQueue)) return false;
        if (URunbook == null)
        {
            if (StringUtils.isNotBlank(other.URunbook)) return false;
        }
        else if (!URunbook.trim().equals(other.URunbook == null ? "" : other.URunbook.trim())) return false;
        if (UScript == null)
        {
            if (StringUtils.isNotBlank(other.UScript)) return false;
        }
        else
        {
            if (!UScript.trim().equals(other.UScript == null ? "" : other.UScript.trim())) 
            {
                return false;
            }
        }
        return true;
    }

    @Override
    public String getUniqueId()
    {
        return this.UName;
    }
    
    // Extending filters should override the method as required, 0 - no error 
    public int isValid()
    {
        return 0;
    }
    
    // Extending filters should override the method as required
    public String getValidationError(int errorCode)
    {
        return "";
    }
}
