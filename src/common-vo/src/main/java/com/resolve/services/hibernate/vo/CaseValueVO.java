package com.resolve.services.hibernate.vo;

import java.io.Serializable;

public class CaseValueVO implements Serializable {
	private static final long serialVersionUID = 4428869428714537452L;
	private String value;
	private Boolean defaultValue = false;

	public CaseValueVO() {}
	
	public CaseValueVO(String value, Boolean defaultValue) {
		this.value = value;
		this.defaultValue = defaultValue;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Boolean getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(Boolean defaultValue) {
		this.defaultValue = defaultValue;
	}
}

