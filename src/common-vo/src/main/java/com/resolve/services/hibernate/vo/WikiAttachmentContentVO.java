/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class WikiAttachmentContentVO extends VO
{
    private static final long serialVersionUID = -3149803147893117413L;

    private byte[] UContent;

    // object references
    private WikiAttachmentVO wikiAttachment;

    // object referenced by

    public WikiAttachmentContentVO()
    {
    }

    public byte[] getUContent()
    {
        return UContent;
    }

    public void setUContent(byte[] UContent)
    {
        if (UContent == null)
            this.UContent = new byte[0];
        else
        {
            this.UContent = UContent;
        }
    }

    public WikiAttachmentVO getWikiAttachment()
    {
        return wikiAttachment;
    }

    public void setWikiAttachment(WikiAttachmentVO wikiAttachment)
    {
        this.wikiAttachment = wikiAttachment;

        // one-way only - no collections on the other side
    }

}
