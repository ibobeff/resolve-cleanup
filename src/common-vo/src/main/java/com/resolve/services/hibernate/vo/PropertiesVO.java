/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Date;

import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

public class PropertiesVO extends VO
{
    private static final long serialVersionUID = 8867830240737111078L;

    private String UName;
    private String UValue;
    private String UType;
    private String UDescription;
    
    public PropertiesVO()
    {
    }

    public PropertiesVO(String UName, String UValue, String UType, String UDescription, Date sysCreatedOn, String sysCreatedBy)
    {
        setUName(UName);
        setUValue(UValue);
        setUType(UType);
        setUDescription(UDescription);
        setSysCreatedOn(sysCreatedOn);
        setSysCreatedBy(sysCreatedBy);
    }
    
    @Override
    public boolean validate()
    {
        boolean valid = true;
        if(StringUtils.isEmpty(getUName()))
        {
            throw new RuntimeException("Properties Name is mandatory");
        }
        
        if(getUName().startsWith("."))
        {
            throw new RuntimeException("Properties Name cannot start with '.'");
        }
        
        //validate the name
        valid = getUName().matches(HibernateConstants.REGEX_ALPHANUMERIC_WITH_UNDERSCORE_DOT_DASH);
        if(!valid)
        {
            throw new RuntimeException("Name can have only alphanumerics,'_' and '.'");
        }

        
        return valid;
    }

    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    public String getUValue()
    {
        return this.UValue;
    }

    public void setUValue(String UValue)
    {
        this.UValue = UValue;
    }

    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    public String getUDescription()
    {
        return this.UDescription;
    }

    public void setUDescription(String UDescription)
    {
        this.UDescription = UDescription;
    }

}
