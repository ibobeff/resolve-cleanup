/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
public class RBTaskVariableVO extends VO
{
    private static final long serialVersionUID = 1026045799079949963L;
	private String name;
    private String color;
    private Boolean isList;
    private Boolean isFlow;
    private Boolean isWsdata;

    private RBTaskVO task;
    
    public RBTaskVariableVO()
    {
        super();
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public Boolean isList()
    {
        return isList;
    }

    public void setList(Boolean isList)
    {
        this.isList = isList;
    }

    public Boolean isFlow()
    {
        return isFlow;
    }

    public void setFlow(Boolean isFlow)
    {
        this.isFlow = isFlow;
    }

    public Boolean isWsdata()
    {
        return isWsdata;
    }

    public void setWsdata(Boolean isWsdata)
    {
        this.isWsdata = isWsdata;
    }

    public RBTaskVO getTask()
    {
        return task;
    }

    public void setTask(RBTaskVO task)
    {
        this.task = task;
    }
}
