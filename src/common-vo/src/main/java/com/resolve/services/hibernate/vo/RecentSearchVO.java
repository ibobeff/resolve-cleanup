/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.List;

import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.ContentType;
import com.resolve.util.StringUtils;

public class RecentSearchVO extends VO
{
    private static final long serialVersionUID = 4733609828202409539L;
    
	private String term;

	private List<String> searchFields;

	private String type = "ALL";

	private List<String> authors;

	private List<String> tags;

	private String createdOn;

	private String updatedOn;

	private String sortBy;

	private String size;

	private String from;

	private List<String> namespaces;

	private List<String> menupaths;

	public String getTerm() {
		return this.term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public List<String> getSearchFields() {
		return this.searchFields;
	}

	public void setSearchFields(List<String> searchFields) {
		this.searchFields = searchFields;
	}

	public ContentType getType() {
		return ContentType.valueOf(this.type.toUpperCase());
	}

	public void setType(String type) {
		if (StringUtils.isNotBlank(type)) {
			this.type = type;
		}
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getSortBy() {
		return this.sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public int getSize() {
		return Integer.valueOf(this.size);
	}

	public void setSize(String size) {
		this.size = size;
	}

	public int getFrom() {
		return Integer.valueOf(this.from);
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public List<String> getNamespaces() {
		return namespaces;
	}

	public void setNamespaces(List<String> namespaces) {
		this.namespaces = namespaces;
	}

	public List<String> getMenupaths() {
		return menupaths;
	}

	public void setMenupaths(List<String> menupaths) {
		this.menupaths = menupaths;
	}

}

