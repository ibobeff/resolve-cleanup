/******************************************************************************
	* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class GroupRoleRelVO extends VO
{
    private static final long serialVersionUID = -7658563530102073143L;

    // object references
    private GroupsVO group;
    private RolesVO role;

    // object referenced by

    public GroupRoleRelVO()
    {
    }

	public String toString()
	{
	    return this.group+" = "+role;
	} // toString

    public GroupsVO getGroup()
    {
        return group;
    }

    public void setGroup(GroupsVO group)
    {
        this.group = group;

//        if (group != null)
//        {
//            Collection<GroupRoleRelVO> coll = group.getGroupRoleRels();
//            if (coll == null)
//            {
//                coll = new HashSet<GroupRoleRelVO>();
//	            coll.add(this);
//	            
//                group.setGroupRoleRels(coll);
//            }
//            //coll.add(this);
//        }
    }

    public RolesVO getRole()
    {
        return role;
    }

    public void setRole(RolesVO role)
    {
        this.role = role;

//        if (role != null)
//        {
//            Collection<GroupRoleRelVO> coll = role.getGroupRoleRels();
//            if (coll == null)
//            {
//                coll = new HashSet<GroupRoleRelVO>();
//	            coll.add(this);
//	            
//                role.setGroupRoleRels(coll);
//            }
//        }
    }

}
