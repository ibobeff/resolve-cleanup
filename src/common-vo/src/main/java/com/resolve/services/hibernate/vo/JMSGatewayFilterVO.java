package com.resolve.services.hibernate.vo;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class JMSGatewayFilterVO extends GatewayFilterVO {

    private static final long serialVersionUID = 1L;
    
    private static final int TYPE_NOT_AVAILABLE = -1;
    private static final String TYPE_NOT_AVAILABLE_ERR = "Type is not available.";
    
    private static final int PORT_NOT_AVAILABLE = -2;
    private static final String PORT_NOT_AVAILABLE_ERR = "Port is not available.";
    
    private static final int JMS_NAME_NOT_AVAILABLE = -3;
    private static final String JMS_NAME_NOT_AVAILABLE_ERR = "Queue/Topic Name is not available.";

    private static final int ACK_NOT_AVAILABLE = -4;
    private static final String ACK_NOT_AVAILABLE_ERR = "Acknowledgement mode is not available.";

    public JMSGatewayFilterVO() {
    }

    private Boolean UDeployed;
    private Boolean UUpdated;
    private String UGatewayName;
    private String UType;
    private Integer UPort;
    private String UJMSName;
    private String UAck;
    private String UUsername;
    private String UPassword;
    private String USsl;

    private Map<String, String> attrs;

    @MappingAnnotation(columnName="DEPLOYED")
    public Boolean getUDeployed()
    {
        return UDeployed;
    }

    public void setUDeployed(Boolean uDeployed)
    {
        UDeployed = uDeployed;
    }
    
    @MappingAnnotation(columnName="UPDATED")
    public Boolean getUUpdated()
    {
        return UUpdated;
    }

    public void setUUpdated(Boolean uUpdated)
    {
        UUpdated = uUpdated;
    }

    @MappingAnnotation(columnName="GATEWAY_NAME")
    public String getUGatewayName()
    {
        return UGatewayName;
    }

    public void setUGatewayName(String uGatewayName)
    {
        UGatewayName = uGatewayName;
    }
    
    @MappingAnnotation(columnName = "TYPE")
    public String getUType() {
        return this.UType;
    }

    public void setUType(String uType) {
        this.UType = uType;
    }

    @MappingAnnotation(columnName = "PORT")
    public Integer getUPort() {
        return this.UPort;
    }

    public void setUPort(Integer uPort) {
        this.UPort = uPort;
    }

    @MappingAnnotation(columnName = "JMS_NAME")
    public String getUJMSName() {
        return this.UJMSName;
    }

    public void setUJMSName(String uJMSName) {
        this.UJMSName = uJMSName;
    }

    @MappingAnnotation(columnName = "ACK")
    public String getUAck() {
        return this.UAck;
    }

    public void setUAck(String uAck) {
        this.UAck = uAck;
    }

    @MappingAnnotation(columnName = "USER")
    public String getUUsername() {
        return this.UUsername;
    }

    public void setUUsername(String uUsername) {
        this.UUsername = uUsername;
    }

    @MappingAnnotation(columnName = "PASS")
    public String getUPassword() {
        return this.UPassword;
    }

    public void setUPassword(String uPassword) {
        this.UPassword = uPassword;
    }

    @MappingAnnotation(columnName = "SSL")
    public String getUSsl() {
        return this.USsl;
    }

    public void setUSsl(String uSsl) {
        this.USsl = uSsl;
    }

    public Map<String, String> getAttrs() {
        return this.attrs;
    }

    public void setAttrs(Map<String, String> attrs) {
        this.attrs = attrs;
    }
    
    @Override
    public int isValid() {

        int valid = 0;
        
        if(StringUtils.isBlank(UType))
            valid = TYPE_NOT_AVAILABLE;
        
        if(UPort == null || UPort.intValue() == 0)
            valid = PORT_NOT_AVAILABLE;
        
        if(StringUtils.isBlank(UJMSName))
            valid = JMS_NAME_NOT_AVAILABLE;

        if(StringUtils.isBlank(UAck))
            valid = ACK_NOT_AVAILABLE;
        
        return valid;
    }
    
    @Override
    public String getValidationError(int errorCode)
    {
        String error = null;
        
        switch(errorCode)
        {
            case PORT_NOT_AVAILABLE:
                error = PORT_NOT_AVAILABLE_ERR;
                break;

            case TYPE_NOT_AVAILABLE:
                error = TYPE_NOT_AVAILABLE_ERR;
                break;

            case JMS_NAME_NOT_AVAILABLE:
                error = JMS_NAME_NOT_AVAILABLE_ERR;
                break;
                
            case ACK_NOT_AVAILABLE:
                error = ACK_NOT_AVAILABLE_ERR;
                break;

            default:
                break;
        }
        
        return error;
    }
    
    public Map<String, String> toMap() {
        
        Map<String, String> attrs = getAttrs();
        
        if(attrs == null)
            attrs = new HashMap<String, String>();

        attrs.put("id", this.getSys_id());

        Method[] methods = this.getClass().getMethods();
        
        for(Method method:methods) {
            String name = method.getName();
            if(method.getAnnotation(MappingAnnotation.class) != null) {
                try {
                    String attr = name.substring(3).toLowerCase();
                    Object value = method.invoke(this, (Object[])null);
                    
                    if(attr.equals("uupdated"))
                        attr = "changed";
                    
                    if(value != null)
                        attrs.put(attr, value.toString());
                } catch(Exception e) {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }

        return attrs;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;

        JMSGatewayFilterVO other = (JMSGatewayFilterVO) obj;
        
        if(!super.equals(other))
            return false;
            
        if (UType == null) {
            if (StringUtils.isNotBlank(other.UType)) 
                return false;
        }

        else if (!UType.trim().equals(other.UType == null ? "" : other.UType.trim())) 
            return false;
        
        if (UPort == null) {
            if (other.UPort != null) 
                return false;
        }

        else if (!UPort.equals(other.UPort)) 
            return false;
        
        if (UAck == null) {
            if (StringUtils.isNotBlank(other.UAck)) 
                return false;
        }

        else if (!UAck.trim().equals(other.UAck == null ? "" : other.UAck.trim())) 
            return false;
        
        if (UJMSName == null) {
            if (StringUtils.isNotBlank(other.UJMSName)) 
                return false;
        }

        else if (!UJMSName.trim().equals(other.UJMSName == null ? "" : other.UJMSName.trim())) 
            return false;
        
        if (UUsername == null) {
            if (StringUtils.isNotBlank(other.UUsername)) 
                return false;
        }

        else if (!UUsername.trim().equals(other.UUsername == null ? "" : other.UUsername.trim())) 
            return false;
        
        if (UPassword == null) {
            if (StringUtils.isNotBlank(other.UPassword)) 
                return false;
        }

        else if (!UPassword.trim().equals(other.UPassword == null ? "" : other.UPassword.trim())) 
            return false;
        
        if (USsl == null) {
            if (other.USsl != null) 
                return false;
        }

        else if (!USsl.equals(other.USsl)) 
            return false;
        
        Map<String, String> otherAttrs = other.getAttrs();
        
        for(Iterator<String> it=attrs.keySet().iterator();it.hasNext();) {
            String key = it.next();
            String value = attrs.get(key);
            String otherValue = otherAttrs.get(key);
            
            if (value == null) {
                if (otherValue != null) 
                    return false;
            }

            else if (!value.equals(otherValue)) 
                return false;
        }
        
        return true;
    }
    
} // class JMSGatewayFilterVO