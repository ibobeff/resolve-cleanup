/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class NamespaceVO extends VO
{
    private static final long serialVersionUID = 7835005296021168158L;
    
    private String UNamespace;
    private long count = 0;
    
    public NamespaceVO()
    {
    }
    
    public NamespaceVO(String name)
    {
        this.UNamespace = name;
    }

    public String getUNamespace()
    {
        return UNamespace;
    }

    public void setUNamespace(String uNamespace)
    {
        UNamespace = uNamespace;
    }

    public long getCount()
    {
        return count;
    }

    public void setCount(long count)
    {
        this.count = count;
    }
    
    
    
}
