package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ArchiveSirAuditLogVO extends VO
{
	private static final long serialVersionUID = -7541048200310110982L;
	
	private String userName;
	private String ipAddress;
	private String description;
	private String incidentId;
	
	public String getUserName()
    {
        return userName;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    public String getIpAddress()
	{
		return ipAddress;
	}
	public void setIpAddress(String ipAddress)
	{
		this.ipAddress = ipAddress;
	}
	
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	
    public String getIncidentId()
    {
        return incidentId;
    }
    public void setIncidentId(String incidentId)
    {
        this.incidentId = incidentId;
    }
}
