/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;

import com.resolve.services.interfaces.VO;

public class ResolveControllersVO extends VO
{
    private static final long serialVersionUID = -5626431834685560657L;
    
    private String UControllerName;
    private String UDescription;
    
    private Collection<ResolveAppControllerRelVO> resolveAppControllerRels;

    public ResolveControllersVO()
    {
    }

    public String getUControllerName()
    {
        return UControllerName;
    }

    public void setUControllerName(String uControllerName)
    {
        UControllerName = uControllerName;
    }

    public String getUDescription()
    {
        return UDescription;
    }

    public void setUDescription(String uDescription)
    {
        UDescription = uDescription;
    }

    public Collection<ResolveAppControllerRelVO> getResolveAppControllerRels()
    {
        return resolveAppControllerRels;
    }

    public void setResolveAppControllerRels(Collection<ResolveAppControllerRelVO> resolveAppControllerRels)
    {
        this.resolveAppControllerRels = resolveAppControllerRels;
    }
    
    

} // ResolveControllersVO
