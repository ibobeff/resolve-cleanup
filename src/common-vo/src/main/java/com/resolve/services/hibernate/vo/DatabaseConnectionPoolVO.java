/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class DatabaseConnectionPoolVO extends GatewayVO
{
    private static final long serialVersionUID = -8568985772207577545L;

    private String UPoolName;
    private String UDriverClass;
    private String U_db_us_ern_ame;
    private String U_db_pas_sword;
    private String UDBConnectURI;
    private Integer UDBMaxConn;

    public DatabaseConnectionPoolVO()
    {
    } // DatabaseConnectionPool
    
    public DatabaseConnectionPoolVO(boolean secured)
    {
    	if (secured) {
    		U_db_pas_sword = VO.getStars()/*STARS*/;
    	}
    } // DatabaseConnectionPool
    
    @MappingAnnotation(columnName = "NAME")
    public String getUPoolName()
    {
        return UPoolName;
    } // getUPoolName

    public void setUPoolName(String uPoolName)
    {
        UPoolName = uPoolName;
    } // setUPoolName

    @MappingAnnotation(columnName = "DRIVERCLASS")
    public String getUDriverClass()
    {
        return UDriverClass;
    } // getUDriverClass

    public void setUDriverClass(String uDriverClass)
    {
        UDriverClass = uDriverClass;
    } // setUDriverClass

    @MappingAnnotation(columnName = "USERNAME")
    public String getU_db_us_ern_ame()
    {
        return U_db_us_ern_ame;
    } // getUDBUsername

    public void setU_db_us_ern_ame(String uDBUsername)
    {
        U_db_us_ern_ame = uDBUsername;
    } // setUDBUsername

    @MappingAnnotation(columnName = "CONNECTURI")
    public String getUDBConnectURI()
    {
        return UDBConnectURI;
    } // getUDBConnectURI

    public void setUDBConnectURI(String uDBConnectURI)
    {
        UDBConnectURI = uDBConnectURI;
    } // setUDBConnectURI

    @MappingAnnotation(columnName = "MAXCONNECTION")
    public Integer getUDBMaxConn()
    {
        return UDBMaxConn;
    } // getUDBMaxConn

    public void setUDBMaxConn(Integer uDBMaxConn)
    {
        UDBMaxConn = uDBMaxConn;
    } // setUDBMaxConn

    @MappingAnnotation(columnName = "PASSWORD")
    public String getU_db_pas_sword()
    {
        return this.U_db_pas_sword;
    } // getUDBPassword

    public void setU_db_pas_sword(String UDBPassword)
    {
        this.U_db_pas_sword = UDBPassword;
    } // setUDBPassword

    public void usetPassword(String password)
    {
        if (!StringUtils.isEmpty(password) && password.charAt(password.length() - 1) != '=')
        {
            try
            {
                this.U_db_pas_sword = CryptUtils.encrypt(password);
            }
            catch (Exception e)
            {
                Log.log.error("Failed to update Database Connector Password", e);
            }
        }
    } // usetPassord

    @Override
    public String getUniqueId()
    {
        return this.UPoolName;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((UDBConnectURI == null) ? 0 : UDBConnectURI.hashCode());
        result = prime * result + ((UDBMaxConn == null) ? 0 : UDBMaxConn.hashCode());
        result = prime * result + ((U_db_pas_sword == null) ? 0 : U_db_pas_sword.hashCode());
        result = prime * result + ((U_db_us_ern_ame == null) ? 0 : U_db_us_ern_ame.hashCode());
        result = prime * result + ((UDriverClass == null) ? 0 : UDriverClass.hashCode());
        result = prime * result + ((UPoolName == null) ? 0 : UPoolName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        DatabaseConnectionPoolVO other = (DatabaseConnectionPoolVO) obj;
        if (UDBConnectURI == null)
        {
            if (other.UDBConnectURI != null) return false;
        }
        else if (!UDBConnectURI.equals(other.UDBConnectURI)) return false;
        if (UDBMaxConn == null)
        {
            if (other.UDBMaxConn != null) return false;
        }
        else if (!UDBMaxConn.equals(other.UDBMaxConn)) return false;
        if (U_db_pas_sword == null)
        {
            if (other.U_db_pas_sword != null) return false;
        }
        else if (!U_db_pas_sword.equals(other.U_db_pas_sword)) return false;
        if (U_db_us_ern_ame == null)
        {
            if (other.U_db_us_ern_ame != null) return false;
        }
        else if (!U_db_us_ern_ame.equals(other.U_db_us_ern_ame)) return false;
        if (UDriverClass == null)
        {
            if (other.UDriverClass != null) return false;
        }
        else if (!UDriverClass.equals(other.UDriverClass)) return false;
        if (UPoolName == null)
        {
            if (other.UPoolName != null) return false;
        }
        else if (!UPoolName.equals(other.UPoolName)) return false;
        return true;
    }
    
    public String toString()
    {
    	return "Pool Name:" + UPoolName + ", Driver Class:" + UDriverClass + ", U_db_us_ern_ame:" + 
    		   U_db_us_ern_ame + ", U_db_pas_sword:" + U_db_pas_sword + ", UDBConnectURI:" + UDBConnectURI +
    		   ", UDBMaxConn:" + UDBMaxConn;
    }
}
	