/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;


public class AuditLogVO extends VO
{
    private static final long serialVersionUID = 3422464406405774249L;
    
    private String UUserName;
	private String USource;
	private String UMessage;

	public AuditLogVO()
	{
	}

	public String getUUserName()
	{
		return this.UUserName;
	}

	public void setUUserName(String UUserName)
	{
		this.UUserName = UUserName;
	}

	public String getUSource()
	{
		return this.USource;
	}

	public void setUSource(String USource)
	{
		this.USource = USource;
	}

	public String getUMessage()
	{
		return this.UMessage;
	}

	public void setUMessage(String UMessage)
	{
		this.UMessage = UMessage;
	}

	
} // AuditLog
