/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
public class TCPFilterVO extends GatewayFilterVO
{
    private static final long serialVersionUID = -8923688545842625691L;
	private Integer UPort;
    private String UBeginSeparator;
    private String UEndSeparator;
    private Boolean UIncludeBeginSeparator;
    private Boolean UIncludeEndSeparator;
    
    public TCPFilterVO()
    {
        super();
    }

    @MappingAnnotation(columnName = "PORT")
    public Integer getUPort()
    {
        return UPort;
    }

    public void setUPort(Integer uPort)
    {
        UPort = uPort;
    }

    @MappingAnnotation(columnName = "BEGIN_SEPARATOR")
    public String getUBeginSeparator()
    {
        return UBeginSeparator;
    }

    public void setUBeginSeparator(String uBeginSeparator)
    {
        if (StringUtils.isNotBlank(uBeginSeparator) || UBeginSeparator.equals(VO.STRING_DEFAULT))
        {
            UBeginSeparator = uBeginSeparator != null ? uBeginSeparator.trim() : uBeginSeparator;
        }
    }

    @MappingAnnotation(columnName = "END_SEPARATOR")
    public String getUEndSeparator()
    {
        return UEndSeparator;
    }

    public void setUEndSeparator(String uEndSeparator)
    {
        if (StringUtils.isNotBlank(uEndSeparator) || UEndSeparator.equals(VO.STRING_DEFAULT))
        {
            UEndSeparator = uEndSeparator != null ? uEndSeparator.trim() : uEndSeparator;
        }
    }

    @MappingAnnotation(columnName = "INCLUDE_BEGIN_SEPARATOR")
    public Boolean getUIncludeBeginSeparator()
    {
        return UIncludeBeginSeparator;
    }

    public void setUIncludeBeginSeparator(Boolean uIncludeBeginSeparator)
    {
        UIncludeBeginSeparator = uIncludeBeginSeparator;
    }

    @MappingAnnotation(columnName = "INCLUDE_END_SEPARATOR")
    public Boolean getUIncludeEndSeparator()
    {
        return UIncludeEndSeparator;
    }

    public void setUIncludeEndSeparator(Boolean uIncludeEndSeparator)
    {
        UIncludeEndSeparator = uIncludeEndSeparator;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UBeginSeparator == null) ? 0 : UBeginSeparator.hashCode());
        result = prime * result + ((UEndSeparator == null) ? 0 : UEndSeparator.hashCode());
        result = prime * result + ((UIncludeBeginSeparator == null) ? 0 : UIncludeBeginSeparator.hashCode());
        result = prime * result + ((UIncludeEndSeparator == null) ? 0 : UIncludeEndSeparator.hashCode());
        result = prime * result + ((UPort == null) ? 0 : UPort.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        TCPFilterVO other = (TCPFilterVO) obj;
        if (UBeginSeparator == null)
        {
            if (StringUtils.isNotBlank(other.UBeginSeparator)) return false;
        }
        else if (!UBeginSeparator.trim().equals(other.UBeginSeparator == null ? "" : other.UBeginSeparator.trim())) return false;
        if (UEndSeparator == null)
        {
            if (StringUtils.isNotBlank(other.UEndSeparator)) return false;
        }
        else if (!UEndSeparator.trim().equals(other.UEndSeparator == null ? "" : other.UEndSeparator.trim())) return false;
        if (UIncludeBeginSeparator == null)
        {
            if (other.UIncludeBeginSeparator != null) return false;
        }
        else if (!UIncludeBeginSeparator.equals(other.UIncludeBeginSeparator)) return false;
        if (UIncludeEndSeparator == null)
        {
            if (other.UIncludeEndSeparator != null) return false;
        }
        else if (!UIncludeEndSeparator.equals(other.UIncludeEndSeparator)) return false;
        if (UPort == null)
        {
            if (other.UPort != null) return false;
        }
        else if (!UPort.equals(other.UPort)) return false;
        return true;
    }
}
