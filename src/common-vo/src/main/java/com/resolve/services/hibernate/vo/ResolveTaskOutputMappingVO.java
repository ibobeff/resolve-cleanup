/******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveTaskOutputMappingVO extends VO
{
	private static final long serialVersionUID = -3235931545749297014L;
	
	private String outputType;
	private String outputName;
	private String sourceType;
	private String sourceName;
	private String script;
	
	private ResolveActionInvocVO invocation;
	
	public String getOutputType()
	{
		return outputType;
	}

	public void setOutputType(String outputType)
	{
		this.outputType = outputType;
	}
	
	public String getOutputName()
	{
		return outputName;
	}
	public void setOutputName(String outputName)
	{
		this.outputName = outputName;
	}
	
	public String getSourceType()
	{
		return sourceType;
	}
	public void setSourceType(String sourceType)
	{
		this.sourceType = sourceType;
	}
	
	public String getSourceName()
	{
		return sourceName;
	}
	public void setSourceName(String sourceName)
	{
		this.sourceName = sourceName;
	}
	
	public String getScript()
	{
		return script;
	}

	public void setScript(String script)
	{
		this.script = script;
	}
	
	public ResolveActionInvocVO getInvocation()
	{
		return invocation;
	}
	public void setInvocation(ResolveActionInvocVO invocation)
	{
		this.invocation = invocation;
	}
}
