/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

public class ResolveBusinessRuleVO extends VO
{
    private static final long serialVersionUID = 2445897718159694248L;
    
    private String UScript;
    private Boolean UAsync; 
    private Boolean UBefore; 
    private Boolean UActive; 
    private String UType;
    private String UName;
    private Integer UOrder;
    private String UTable;
    
    @Override
    public boolean validate()
    {
        boolean valid = true;
        
        if(StringUtils.isEmpty(getUName()) || (StringUtils.isNotBlank(getUName()) && getUName().equalsIgnoreCase(VO.STRING_DEFAULT)))
        {
            valid = false;
            throw new RuntimeException("Name cannot be empty.");
        }
        
        valid = getUName().matches(HibernateConstants.REGEX_ALPHANUMERIC_WITH_SPACE);
        if(!valid)
        {
            throw new RuntimeException("Name can have only Alpha numeric, space and _.");
        }
        
        return valid;
    }
    
    public ResolveBusinessRuleVO()
    {
    }

    public String getUScript()
    {
        return this.UScript;
    }

    public void setUScript(String UScript)
    {
        this.UScript = UScript;
    }

    public Boolean getUAsync()
    {
        return this.UAsync;
    }
    public void setUAsync(Boolean UAsync)
    {
        this.UAsync = UAsync;
    }
    
    public Boolean getUBefore()
    {
        return this.UBefore;
    }
    public void setUBefore(Boolean UBefore)
    {
        this.UBefore = UBefore;
    }
    
    public Boolean getUActive()
    {
        return this.UActive;
    }
    public void setUActive(Boolean UActive)
    {
        this.UActive = UActive;
    }
    
    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String name)
    {
        this.UName = name;
    }

    public Integer getUOrder()
    {
        return this.UOrder;
    }

    public void setUOrder(Integer UOrder)
    {
        this.UOrder = UOrder;
    }

    public String getUTable()
    {
        return this.UTable;
    }

    public void setUTable(String table)
    {
        this.UTable = table;
    }
}
