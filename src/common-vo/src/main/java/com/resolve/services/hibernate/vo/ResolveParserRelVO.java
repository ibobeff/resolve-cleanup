/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveParserRelVO extends VO
{
    private static final long serialVersionUID = 280494416516035771L;
    
    private ResolveParserVO parser;
    private String URefParser; 

    public ResolveParserRelVO()
    {
    }
    
    public ResolveParserVO getParser()
    {
        return parser;
    }

    public void setParser(ResolveParserVO parser)
    {
        this.parser = parser;
    }
    
    public String getURefParser()
    {
        return URefParser;
    }

    public void setURefParser(String refAssessor)
    {
        this.URefParser = refAssessor;
    }

}
