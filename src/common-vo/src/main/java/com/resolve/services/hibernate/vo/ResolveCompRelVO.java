package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveCompRelVO extends VO
{
	private static final long serialVersionUID = 4078854783999605579L;
	
	String parentId;
	String parentType;
	String childId;
	String childType;
	
	public String getParentId()
	{
		return parentId;
	}
	public void setParentId(String parentId)
	{
		this.parentId = parentId;
	}
	
	public String getParentType()
	{
		return parentType;
	}
	public void setParentType(String parentType)
	{
		this.parentType = parentType;
	}
	
	public String getChildId()
	{
		return childId;
	}
	public void setChildId(String childId)
	{
		this.childId = childId;
	}
	
	public String getChildType()
	{
		return childType;
	}
	public void setChildType(String childType)
	{
		this.childType = childType;
	}
}
