/******************************************************************************
* (C) Copyright 2017
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class OrgGroupRelVO extends VO
{
    private static final long serialVersionUID = -657534030035822550L;
    
    // object references
    private OrgsVO org;
    private GroupsVO group;
    

    // object referenced by

    public OrgGroupRelVO()
    {
    }

	public String toString()
	{
	    return org + " associated with " + group;
	} // toString
	
	public OrgsVO getOrg()
    {
        return org;
    }

    public void setOrg(OrgsVO org)
    {
        this.org = org;
    }
    
    public GroupsVO getGroup()
    {
        return group;
    }

    public void setGroup(GroupsVO group)
    {
        this.group = group;
    }
}
