/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;

public class ResolveRegistrationPropertyVO extends VO
{

    private static final long serialVersionUID = -2238430208012996942L;
    
    private String UName;
    private String UValue;
    private String UType;

    // object references
    private ResolveRegistrationVO registration;   // URegistration;
	
    // object referenced by
	
    public ResolveRegistrationPropertyVO()
    {
    }


    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    public String getUValue()
    {
        return this.UValue;
    }

    public void setUValue(String UValue)
    {
        this.UValue = UValue;
    }

    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    public ResolveRegistrationVO getRegistration()
    {
        return registration;
    }

    public void setRegistration(ResolveRegistrationVO registration)
    {
        this.registration = registration;

        if (registration != null)
        {
            Collection<ResolveRegistrationPropertyVO> coll = registration.getResolveRegistrationProperties();
            if (coll == null)
            {
				coll = new HashSet<ResolveRegistrationPropertyVO>();
	            coll.add(this);
	            
                registration.setResolveRegistrationProperties(coll);
            }
        }
    }

}
