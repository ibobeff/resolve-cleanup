/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Set;

import com.resolve.services.interfaces.VO;

public class SIRConfigVO extends VO
{
    private static final long serialVersionUID = -2786998401618522286L;
    
    // Referenced Objects
    private Set<CustomDataFormTabVO> customDataFormTabs;
    
    public SIRConfigVO()
    {
    }
    
    public SIRConfigVO(boolean defaultValues)
    {
        super(defaultValues);
    }
    
    public Set<CustomDataFormTabVO> getCustomDataFormTabs()
    {
        return customDataFormTabs;
    }
    
    public void setCustomDataFormTabs(Set<CustomDataFormTabVO> customDataFormTabs)
    {
        if (customDataFormTabs != null && !customDataFormTabs.isEmpty())
        {
            for (CustomDataFormTabVO customDataFormTabVO : customDataFormTabs)
            {
                customDataFormTabVO.setSysOrg(getSysOrg());
            }
        }
        
        this.customDataFormTabs = customDataFormTabs;
    }
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        
        sb.append("Org: ").append(getSysOrg());
        
        if (getCustomDataFormTabs() != null && !getCustomDataFormTabs().isEmpty())
        {            
            sb.append("# of Custom Data Form Tabs: " + getCustomDataFormTabs().size());
            
            sb.append(" [");
            
            for (CustomDataFormTabVO customDataFormTabVO : getCustomDataFormTabs())
            {
                sb.append(" ");
                sb.append(customDataFormTabVO);
            }
            
            sb.append("] ");
        }
        
        return sb.toString();
    } // toString
}
