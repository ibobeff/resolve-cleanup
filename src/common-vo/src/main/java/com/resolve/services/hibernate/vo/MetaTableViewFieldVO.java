/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;


public class MetaTableViewFieldVO extends VO
{
    private static final long serialVersionUID = -1786125953880443631L;
    
    // object referenced
    private MetaTableViewVO metaTableView;
    private MetaFieldVO metaField;
    private MetaStyleVO metaStyle;
    private MetaViewFieldVO metaViewField;
    
    //this will be for the DB field - and mapped to form - so 2 forms can have 1 metafield that can have different properties
    //if this is null, then use the metafield properties
    private MetaFieldPropertiesVO metaFieldTableProperties;

    private Integer UOrder;
    
    public MetaTableViewFieldVO()
    {
    }
    
    public MetaTableViewVO getMetaTableView()
    {
        return metaTableView;
    }

    public void setMetaTableView(MetaTableViewVO metaTableView)
    {
        this.metaTableView = metaTableView;

        if (metaTableView != null)
        {
            Collection<MetaTableViewFieldVO> coll = metaTableView.getMetaTableViewFields();
            if (coll == null)
            {
                coll = new HashSet<MetaTableViewFieldVO>();
                coll.add(this);
                
                metaTableView.setMetaTableViewFields(coll);
            }
        }
    }
    
    public MetaFieldVO getMetaField()
    {
        return metaField;
    }

    public void setMetaField(MetaFieldVO metaField)
    {
        this.metaField = metaField;
    }
    
    public MetaStyleVO getMetaStyle()
    {
        return metaStyle;
    }
    
    public void setMetaStyle(MetaStyleVO metaStyle)
    {
        this.metaStyle = metaStyle;
    }
    
    public MetaViewFieldVO getMetaViewField()
    {
        return metaViewField;
    }
    
    public void setMetaViewField(MetaViewFieldVO metaViewField)
    {
        this.metaViewField = metaViewField;
    }
    
    public Integer getUOrder()
    {
        return this.UOrder;
    }

    public void setUOrder(Integer UOrder)
    {
        this.UOrder = UOrder;
    }
    
    public MetaFieldPropertiesVO getMetaFieldTableProperties()
    {
        return metaFieldTableProperties;
    }

    public void setMetaFieldTableProperties(MetaFieldPropertiesVO metaFieldFormProperties)
    {
        this.metaFieldTableProperties = metaFieldFormProperties;
    }
   
}
