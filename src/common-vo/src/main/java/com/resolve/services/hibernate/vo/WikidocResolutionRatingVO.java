/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;

public class WikidocResolutionRatingVO  extends VO
{
	private static final long serialVersionUID = 6510557792584841378L;

	private Long u1StarCount;
	private Long u2StarCount;
	private Long u3StarCount;
	private Long u4StarCount;
	private Long u5StarCount;
	private Long uinitTotal;
	private Long uinitCount;
	
    // object references
	private WikiDocumentVO wikidoc; 

	public WikidocResolutionRatingVO()
	{
	}

    public WikiDocumentVO getWikidoc()
    {
        return wikidoc;
    }

    public void setWikidoc(WikiDocumentVO wikidoc)
    {
        this.wikidoc = wikidoc;

        if (wikidoc != null)
        {
            Collection<WikidocResolutionRatingVO> coll = wikidoc.getWikidocResolutionRating();
            if (coll == null)
            {
				coll = new HashSet<WikidocResolutionRatingVO>();
	            coll.add(this);
	            
                wikidoc.setWikidocRatingResolution(coll);
            }
        }
    }

	public Long getU1StarCount()
	{
		return this.u1StarCount;
	}

	public void setU1StarCount(Long u1StarCount)
	{
		this.u1StarCount = u1StarCount;
	}

	public Long getU2StarCount()
	{
		return this.u2StarCount;
	}

	public void setU2StarCount(Long u2StarCount)
	{
		this.u2StarCount = u2StarCount;
	}

	public Long getU3StarCount()
	{
		return this.u3StarCount;
	}

	public void setU3StarCount(Long u3StarCount)
	{
		this.u3StarCount = u3StarCount;
	}

	public Long getU4StarCount()
	{
		return this.u4StarCount;
	}

	public void setU4StarCount(Long u4StarCount)
	{
		this.u4StarCount = u4StarCount;
	}

	public Long getU5StarCount()
	{
		return this.u5StarCount;
	}

	public void setU5StarCount(Long u5StarCount)
	{
		this.u5StarCount = u5StarCount;
	}
	
    public Long getUinitTotal()
    {
        return uinitTotal;
    }

    public void setUinitTotal(Long uinitTotal)
    {
        this.uinitTotal = uinitTotal;
    }

    public Long getUinitCount()
    {
        return uinitCount;
    }

    public void setUinitCount(Long uinitCount)
    {
        this.uinitCount = uinitCount;
    }

}
