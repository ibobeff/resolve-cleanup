/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;


public class ArchiveActionResultLobVO extends VO
{
    private static final long serialVersionUID = 7907157103949144628L;
    
    private String USummary;
    private String UDetail;
    private String URaw;

    public ArchiveActionResultLobVO()
    {
    }

    public ArchiveActionResultLobVO(String USummary, String UDetail, String URaw)
    {
        this.USummary = USummary;
        this.UDetail = UDetail;
        this.URaw = URaw;
    }

    public ArchiveActionResultLobVO usetsys_id(String sys_id)
    {
        this.setSys_id(sys_id);
        return this;
    }
    public String getUSummary()
    {
        return this.USummary;
    }

    public void setUSummary(String USummary)
    {
        this.USummary = USummary;
    }

    public String getUDetail()
    {
        return this.UDetail;
    }

    public void setUDetail(String UDetail)
    {
        this.UDetail = UDetail;
    }

    public String getURaw()
    {
        return URaw;
    }

    public void setURaw(String uRaw)
    {
        URaw = uRaw;
    }

} // ResolveActionResultLob
