package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ArchiveSirNoteVO extends VO
{
	private static final long serialVersionUID = 2019984268385994124L;
	
	private String note;           // note content
    private String postedBy;       // user who posted the note
    private String activityId;     // id of the Activity which added this note
    private String activityName;   // name of the Activity which added this note
    private String componentId;    // id of the Wiki component which added this note
    private String incidentId;     // id of the incident to which this note belongs
    private String worksheetId;    // worksheet id belongs to the incident to which this note belongs
    private String source;         // name of a source which created this note.
    private String sourceValue;    // the value of the source.
    private String title;          // Title of a note.
    private String sourceAndValue;
	
	public String getNote()
	{
		return note;
	}
	public void setNote(String note)
	{
		this.note = note;
	}
	
	public String getPostedBy()
	{
		return postedBy;
	}
	public void setPostedBy(String postedBy)
	{
		this.postedBy = postedBy;
	}
	
    public String getActivityId()
    {
        return activityId;
    }
    public void setActivityId(String activityId)
    {
        this.activityId = activityId;
    }
    
    public String getActivityName()
    {
        return activityName;
    }
    public void setActivityName(String activityName)
    {
        this.activityName = activityName;
    }
    
    public String getComponentId()
    {
        return componentId;
    }
    public void setComponentId(String componentId)
    {
        this.componentId = componentId;
    }
    
    public String getIncidentId()
    {
        return incidentId;
    }
    public void setIncidentId(String incidentId)
    {
        this.incidentId = incidentId;
    }
    
    public String getWorksheetId()
    {
        return worksheetId;
    }
    public void setWorksheetId(String worksheetId)
    {
        this.worksheetId = worksheetId;
    }
    
    public String getSource()
    {
        return source;
    }
    public void setSource(String source)
    {
        this.source = source;
    }
    
    public String getSourceValue()
    {
        return sourceValue;
    }
    public void setSourceValue(String sourceValue)
    {
        this.sourceValue = sourceValue;
    }
    
    public String getTitle()
    {
        return title;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }
    
    public String getSourceAndValue()
    {
        return sourceAndValue;
    }
    public void setSourceAndValue(String sourceAndValue)
    {
        this.sourceAndValue = sourceAndValue;
    }
}
