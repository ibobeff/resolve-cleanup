package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveCatalogNodeWikidocRelVO extends VO
{

    private static final long serialVersionUID = 5770923040484280532L;

    // object references
	private WikiDocumentVO wikidoc;
	private ResolveCatalogNodeVO catalogNode;
	
	public ResolveCatalogNodeWikidocRelVO()
	{
	}

	public WikiDocumentVO getWikidoc()
	{
		return wikidoc;
	}

	public void setWikidoc(WikiDocumentVO wikidoc)
	{
		this.wikidoc = wikidoc;
	}

    public ResolveCatalogNodeVO getCatalogNode()
    {
        return catalogNode;
    }

    public void setCatalogNode(ResolveCatalogNodeVO catalogNode)
    {
        this.catalogNode = catalogNode;
    }
	
	
	
    
    
}
