package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class SavedQueryFilterVO extends VO {
	private static final long serialVersionUID = -5654136545939771081L;
    private String type;
    private String value;
    private String startValue;
    private String endValue;
    private String field;
    private String condition;
    private String paramName;
    private Boolean caseSensitive;
    private int startCount;

    public String getType() {
		return type;
	}

    public void setType(String type) {
		this.type = type;
	}

    public String getValue() {
		return value;
	}

    public void setValue(String value) {
		this.value = value;
	}

    public String getStartValue() {
		return startValue;
	}

    public void setStartValue(String startValue) {
		this.startValue = startValue;
	}

    public String getEndValue() {
		return endValue;
	}

    public void setEndValue(String endValue) {
		this.endValue = endValue;
	}

    public String getField() {
		return field;
	}

    public void setField(String field) {
		this.field = field;
	}

    public String getCondition() {
		return condition;
	}

    public void setCondition(String condition) {
		this.condition = condition;
	}

    public String getParamName() {
		return paramName;
	}

    public void setParamName(String paramName) {
		this.paramName = paramName;
	}

    public Boolean getCaseSensitive() {
		return caseSensitive;
	}

    public void setCaseSensitive(Boolean caseSensitive) {
		this.caseSensitive = caseSensitive;
	}

    public int getStartCount() {
		return startCount;
	}

    public void setStartCount(int startCount) {
		this.startCount = startCount;
	}
}
