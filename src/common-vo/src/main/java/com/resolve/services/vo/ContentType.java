package com.resolve.services.vo;

public enum ContentType {

    DOCUMENT, AUTOMATION, DECISIONTREE, ACTIONTASK, CUSTOMFORM, PROPERTY, PLAYBOOK, ALL
}
