/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;


public class SysAppApplicationrolesVO extends VO
{
    private static final long serialVersionUID = 6907338718753731942L;
    
    private String value;
    private Integer sequence;

    // object references
    private SysAppApplicationVO sysAppApplication;
	
	
    public SysAppApplicationrolesVO()
    {
    }

    public String getValue()
    {
        return this.value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    /**
     * @Column(name = "parent_id", length = 32) public String getParentId() {
     *              return this.parentId; }
     * 
     *              public void setParentId(String parentId) { this.parentId =
     *              parentId; }
     */

    public Integer getSequence()
    {
        return this.sequence;
    }

    public void setSequence(Integer sequence)
    {
        this.sequence = sequence;
    }

    public SysAppApplicationVO getSysAppApplication()
    {
        return sysAppApplication;
    }

    public void setSysAppApplication(SysAppApplicationVO sysAppApplication)
    {
        this.sysAppApplication = sysAppApplication;
        
//        if (sysAppApplication != null)
//        {
//            Collection<SysAppApplicationrolesVO> coll = sysAppApplication.getSysAppApplicationroles();
//            if (coll == null)
//            {
//				coll = new HashSet<SysAppApplicationrolesVO>();
//	            coll.add(this);
//	            
//                sysAppApplication.setSysAppApplicationroles(coll);
//            }
//        }
    }
}
