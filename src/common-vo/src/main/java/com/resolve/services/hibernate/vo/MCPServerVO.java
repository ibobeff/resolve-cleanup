/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
public class MCPServerVO extends VO
{
    private static final long serialVersionUID = -4505125236597725421L;
	private String serverIp;
    private String name;
    private String tag;
    private String description;
    private String status;
    private String blueprint;
    private String actualBlueprint;
    private String worksheetId;
    private String worksheet;
    private String componentStatus;
    private String version;
    private String build;
    private String components;

    private String resolveHome;
    private Integer sshPort;
    private String remoteUser;
    private String remotePassword;
    private String terminalPrompt;
    private String privateKeyLocation;
    public String getServerIp()
    {
        return serverIp;
    }
    public void setServerIp(String serverIp)
    {
        this.serverIp = serverIp;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getTag()
    {
        return tag;
    }
    public void setTag(String tag)
    {
        this.tag = tag;
    }
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }
    public String getStatus()
    {
        return status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }
    public String getBlueprint()
    {
        return blueprint;
    }
    public void setBlueprint(String blueprint)
    {
        this.blueprint = blueprint;
    }
    public String getActualBlueprint()
    {
        return actualBlueprint;
    }
    public void setActualBlueprint(String actualBlueprint)
    {
        this.actualBlueprint = actualBlueprint;
    }
    public String getWorksheetId()
    {
        return worksheetId;
    }
    public void setWorksheetId(String worksheetId)
    {
        this.worksheetId = worksheetId;
    }
    public String getWorksheet()
    {
        return worksheet;
    }
    public void setWorksheet(String worksheet)
    {
        this.worksheet = worksheet;
    }
    public String getComponentStatus()
    {
        return componentStatus;
    }
    public void setComponentStatus(String componentStatus)
    {
        this.componentStatus = componentStatus;
    }
    public String getVersion()
    {
        return version;
    }
    public void setVersion(String version)
    {
        this.version = version;
    }
    public String getBuild()
    {
        return build;
    }
    public void setBuild(String build)
    {
        this.build = build;
    }
    
    public String getResolveHome()
    {
        return resolveHome;
    }
    public void setResolveHome(String resolveHome)
    {
        this.resolveHome = resolveHome;
    }
    public Integer getSshPort()
    {
        return sshPort;
    }
    public void setSshPort(Integer sshPort)
    {
        this.sshPort = sshPort;
    }
    public String getRemoteUser()
    {
        return remoteUser;
    }
    public void setRemoteUser(String remoteUser)
    {
        this.remoteUser = remoteUser;
    }
    public String getRemotePassword()
    {
        return remotePassword;
    }
    public void setRemotePassword(String remotePassword)
    {
        this.remotePassword = remotePassword;
    }
    public String getTerminalPrompt()
    {
        return terminalPrompt;
    }
    public void setTerminalPrompt(String terminalPrompt)
    {
        this.terminalPrompt = terminalPrompt;
    }
    public String getPrivateKeyLocation()
    {
        return privateKeyLocation;
    }
    public void setPrivateKeyLocation(String privateKeyLocation)
    {
        this.privateKeyLocation = privateKeyLocation;
    }
    //Do NOT have correcposing persistent property in the Entity
    public String getComponents()
    {
        return components;
    }
    public void setComponents(String components)
    {
        this.components = components;
    }
}
