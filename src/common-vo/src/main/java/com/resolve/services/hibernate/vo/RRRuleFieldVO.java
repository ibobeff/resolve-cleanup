package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
public class RRRuleFieldVO extends VO
{
    private static final long serialVersionUID = -28878848758488002L;
	private String name;
    private String value;
    private Integer order;
    private RRRuleVO rule;
    public RRRuleFieldVO() {
        super();
    }
    public RRRuleFieldVO(String id,String name, String value, Integer order,RRRuleVO ruleVO) {
        super();
        this.setSys_id(id);
        this.name = name;
        this.value = value;
        this.rule = ruleVO;
        this.order = order;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getValue()
    {
        return value;
    }
    public void setValue(String value)
    {
        this.value = value;
    }
    public Integer getOrder()
    {
        return order;
    }
    public void setOrder(Integer order)
    {
        this.order = order;
    }
    public RRRuleVO getRule()
    {
        return rule;
    }
    public void setRule(RRRuleVO rule)
    {
        this.rule = rule;
    }
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((order == null) ? 0 : order.hashCode());
        result = prime * result + ((rule == null) ? 0 : rule.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        RRRuleFieldVO other = (RRRuleFieldVO) obj;
        if (name == null)
        {
            if (other.name != null) return false;
        }
        else if (!name.equals(other.name)) return false;
        if (order == null)
        {
            if (other.order != null) return false;
        }
        else if (!order.equals(other.order)) return false;
        if (rule == null)
        {
            if (other.rule != null) return false;
        }
        else if (!rule.equals(other.rule)) return false;
        if (value == null)
        {
            if (other.value != null) return false;
        }
        else if (!value.equals(other.value)) return false;
        return true;
    }
    
}
