/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;

@SuppressWarnings("serial")
public class XMPPAddressVO extends GatewayVO
{
    private static final long serialVersionUID = -6644235398311189433L;
	private String UXMPPAddress;
    private String UXMPPPassword;

    // object referenced by
    public XMPPAddressVO()
    {
    } // XMPPFilter

    @MappingAnnotation(columnName = "XMPPADDRESS")
    public String getUXMPPAddress()
    {
        return this.UXMPPAddress;
    } // getUXMPPAddress

    public void setUXMPPAddress(String UXMPPAddress)
    {
        this.UXMPPAddress = UXMPPAddress;
    } // setUXMPPAddress

    @MappingAnnotation(columnName = "XMPPPASSWORD")
    public String getUXMPPPassword()
    {
        return this.UXMPPPassword;
    } // getUXMPPPassword

    public void setUXMPPPassword(String UXMPPPassword)
    {
        this.UXMPPPassword = UXMPPPassword;
    } // setUXMPPPassword


    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UXMPPAddress == null) ? 0 : UXMPPAddress.hashCode());
        result = prime * result + ((UXMPPPassword == null) ? 0 : UXMPPPassword.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        //do not call super
        //if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        XMPPAddressVO other = (XMPPAddressVO) obj;
        if (UXMPPAddress == null)
        {
            if (other.UXMPPAddress != null) return false;
        }
        else if (!UXMPPAddress.equals(other.UXMPPAddress)) return false;
        if (UXMPPPassword == null)
        {
            if (other.UXMPPPassword != null) return false;
        }
        else if (!UXMPPPassword.equals(other.UXMPPPassword)) return false;
        return true;
    }

    @Override
    public String getUniqueId()
    {
        return this.UXMPPAddress;
    }
}
