package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class PlaybookActivitiesVO extends VO
{
    private static final long serialVersionUID = 1012545719582514474L;
    
    private String UFullname;           // Full name (name space.document name) of playbook template, activities
                                        // are part of
                                        //                          OR
                                        // Blank for activities associated with a specific SIR having one or more
                                        // activity added to it and which is/are not part of the playbook template
    private Integer UVersion;           // Version of playbook template, activity was added to first time
                                        //                         OR
                                        // 0 for activities associated with a specific SIR having one or more
                                        // activity added to it and which is/are not part of the playbook template
    private String USirSysId;           // Sys Id of SIR these activities are associated  with                                    
    private String UActivitiesJSON;     // Activities JSON (Hierarchichal structure)
                                        // Array of Phases (in order), Each Phase contianing array of activities
                                        // in order (if not not empty)
        
	public String getUFullname()
	{
		return UFullname;
	}
	
	public void setUFullname(String UFullname)
	{
		this.UFullname = UFullname;
	}
	
	public Integer getUVersion()
    {
        return this.UVersion;
    }

    public void setUVersion(Integer UVersion)
    {
        this.UVersion = UVersion;
    }
    
    public String getUSirSysId()
    {
        return this.USirSysId;
    }
    
    public void setUSirSysId(String USirSysId)
    {
        this.USirSysId = USirSysId;
    }
    
    public String getUActivitiesJSON()
    {
        return this.UActivitiesJSON;
    }

    public void setUActivitiesJSON(String UActivitiesJSON)
    {
        this.UActivitiesJSON = UActivitiesJSON;
    }
}
