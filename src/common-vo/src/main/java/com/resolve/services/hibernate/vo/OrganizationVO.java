/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class OrganizationVO extends VO
{
    private static final long serialVersionUID = -3967982951457322617L;
    
    private String UOrganizationName;
    private String UDescription;
    private Boolean UDisable;

    public OrganizationVO()
    {
    }
    
    public OrganizationVO(String org_id)
    {
        setSys_id(org_id);
    }

    public String getUOrganizationName()
    {
        return UOrganizationName;
    }

    public void setUOrganizationName(String uOrganizationName)
    {
        UOrganizationName = uOrganizationName;
    }

    public String getUDescription()
    {
        return UDescription;
    }

    public void setUDescription(String uDescription)
    {
        UDescription = uDescription;
    }
    
    public Boolean getUDisable()
    {
        return UDisable;
    }

    public void setUDisable(Boolean uDisable)
    {
        UDisable = uDisable;
    }
    
    public boolean ugetUDisable()
    {
        boolean result;
        
        if (UDisable == null)
        {
            result = false;
        }
        else
        {
            result = UDisable;
        }
        return result;
    }

}
