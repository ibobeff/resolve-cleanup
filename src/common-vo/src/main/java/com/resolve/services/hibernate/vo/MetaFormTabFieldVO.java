/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;


public class MetaFormTabFieldVO extends VO
{
    private static final long serialVersionUID = 4957935803465888020L;
    
    // object referenced
    private MetaFormTabVO metaFormTab;
    private MetaFieldVO metaField;//for DB source
    private MetaSourceVO metaSource;//for INPUT,PROPERTIES source type
    private MetaStyleVO metaStyle;
    
    //this will be for the DB field - and mapped to form - so 2 forms can have 1 metafield that can have different properties
    //if this is null, then use the metafield properties
    private MetaFieldPropertiesVO metaFieldFormProperties;
    
    private Integer UColumnNumber;//will be 1 or 2, to indicate if the field is on the 1st column or the 2nd
    private Integer UOrder;//order # it will rendered
    
    public MetaFormTabFieldVO()
    {
    }
    
    public MetaFormTabVO getMetaFormTab()
    {
        return metaFormTab;
    }

    public void setMetaFormTab(MetaFormTabVO MetaFormTab)
    {
        this.metaFormTab = MetaFormTab;

        if (metaFormTab != null)
        {
            Collection<MetaFormTabFieldVO> coll = metaFormTab.getMetaFormTabFields();
            if (coll == null)
            {
                coll = new HashSet<MetaFormTabFieldVO>();
                coll.add(this);
                
                metaFormTab.setMetaFormTabFields(coll);
            }
        }
    }
    
    public MetaSourceVO getMetaSource()
    {
        return metaSource;
    }

    public void setMetaSource(MetaSourceVO metaSource)
    {
        this.metaSource = metaSource;
    }
    
    public MetaFieldVO getMetaField()
    {
        return metaField;
    }

    public void setMetaField(MetaFieldVO metaField)
    {
        this.metaField = metaField;
    }
    
    public MetaStyleVO getMetaStyle()
    {
        return metaStyle;
    }
    
    public void setMetaStyle(MetaStyleVO metaStyle)
    {
        this.metaStyle = metaStyle;
    }
    
    public Integer getUOrder()
    {
        return this.UOrder;
    }

    public void setUOrder(Integer UOrder)
    {
        this.UOrder = UOrder;
    }
    
    public Integer getUColumnNumber()
    {
        return this.UColumnNumber;
    }

    public void setUColumnNumber(Integer UColumnNumber)
    {
        this.UColumnNumber = UColumnNumber;
    }
    
    public MetaFieldPropertiesVO getMetaFieldFormProperties()
    {
        return metaFieldFormProperties;
    }

    public void setMetaFieldFormProperties(MetaFieldPropertiesVO metaFieldFormProperties)
    {
        this.metaFieldFormProperties = metaFieldFormProperties;
    }

   
}//MetaFormTabField
