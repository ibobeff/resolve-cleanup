/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class ExchangeserverFilterVO extends GatewayFilterVO
{
    private static final long serialVersionUID = -5806073652175196359L;

    private String USubject;
    private String UContent;
    private String USendername;
    private String USenderaddress;

    // object referenced by
    public ExchangeserverFilterVO()
    {
        super();
    }

    @MappingAnnotation(columnName = "SUBJECT")
    public String getUSubject()
    {
        return this.USubject;
    }

    public void setUSubject(String USubject)
    {
        if (StringUtils.isNotBlank(USubject) || this.USubject.equals(VO.STRING_DEFAULT))
        {
            this.USubject = USubject != null ? USubject.trim() : USubject;
        }
    }

    @MappingAnnotation(columnName = "CONTENT")
    public String getUContent()
    {
        return this.UContent;
    }

    public void setUContent(String UContent)
    {
        if (StringUtils.isNotBlank(UContent) || this.UContent.equals(VO.STRING_DEFAULT))
        {
            this.UContent = UContent != null ? UContent.trim() : UContent;
        }
    }

    @MappingAnnotation(columnName = "SENDERNAME")
    public String getUSendername()
    {
        return this.USendername;
    }

    public void setUSendername(String USendername)
    {
        if (StringUtils.isNotBlank(USendername) || this.USendername.equals(VO.STRING_DEFAULT))
        {
            this.USendername = USendername != null ? USendername.trim() : USendername;
        }
    }

    @MappingAnnotation(columnName = "SENDERADDRESS")
    public String getUSenderaddress()
    {
        return this.USenderaddress;
    }

    public void setUSenderaddress(String USenderaddress)
    {
        if (StringUtils.isNotBlank(USenderaddress) || this.USenderaddress.equals(VO.STRING_DEFAULT))
        {
            this.USenderaddress = USenderaddress != null ? USenderaddress.trim() : USenderaddress;
        }
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UContent == null) ? 0 : UContent.hashCode());
        result = prime * result + ((USenderaddress == null) ? 0 : USenderaddress.hashCode());
        result = prime * result + ((USendername == null) ? 0 : USendername.hashCode());
        result = prime * result + ((USubject == null) ? 0 : USubject.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        ExchangeserverFilterVO other = (ExchangeserverFilterVO) obj;
        if (UContent == null)
        {
            if (StringUtils.isNotBlank(other.UContent)) return false;
        }
        else if (!UContent.trim().equals(other.UContent == null ? "" : other.UContent.trim())) return false;
        if (USenderaddress == null)
        {
            if (StringUtils.isNotBlank(other.USenderaddress)) return false;
        }
        else if (!USenderaddress.trim().equals(other.USenderaddress == null ? "" : other.USenderaddress.trim())) return false;
        if (USendername == null)
        {
            if (StringUtils.isNotBlank(other.USendername)) return false;
        }
        else if (!USendername.trim().equals(other.USendername == null ? "" : other.USendername.trim())) return false;
        if (USubject == null)
        {
            if (StringUtils.isNotBlank(other.USubject)) return false;
        }
        else if (!USubject.trim().equals(other.USubject == null ? "" : other.USubject.trim())) return false;
        return true;
    }
}
