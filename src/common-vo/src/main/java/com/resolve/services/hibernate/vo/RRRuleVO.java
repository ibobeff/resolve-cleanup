package com.resolve.services.hibernate.vo;

import java.util.List;

import com.resolve.services.interfaces.VO;

public class RRRuleVO extends VO
{
    private static final long serialVersionUID = 5215416933251598785L;
	private String rid;
    private List<RRRuleFieldVO> ridFields;
    private RRSchemaVO schema;
    private String wikiId;
    private String wiki;// for UI
    private String runbookId;
    private String runbook;// for UI
    private String automationId;
    private String automation;//for UI
    private String module;
    private String eventId;
    private Boolean active;
    private Boolean newWorksheetOnly;
    // RBA-14814 : Implementation - Create and Process Resolution Routing Rule for Security IR
    private Boolean sir;
    private String sirSource;
    private String sirTitle;
    private String sirType;
    private String sirPlaybook;
    private String sirSeverity;
    private String sirOwner;
    private String sirId;
    private String sirProblemId;
    
    public String getRid()
    {
        return rid;
    }
    public void setRid(String rid)
    {
        this.rid = rid;
    }
    public List<RRRuleFieldVO> getRidFields()
    {
        return ridFields;
    }
    public void setRidFields(List<RRRuleFieldVO> ridFields)
    {
        this.ridFields = ridFields;
    }
    public RRSchemaVO getSchema()
    {
        return schema;
    }
    public void setSchema(RRSchemaVO schema)
    {
        this.schema = schema;
    }
    public String getWikiId()
    {
        return wikiId;
    }
    public void setWikiId(String wikiId)
    {
        this.wikiId = wikiId;
    }
    public String getWiki()
    {
        return wiki;
    }
    public void setWiki(String wiki)
    {
        this.wiki = wiki;
    }
    public String getRunbookId()
    {
        return runbookId;
    }
    public void setRunbookId(String runbookId)
    {
        this.runbookId = runbookId;
    }
    public String getRunbook()
    {
        return runbook;
    }
    public void setRunbook(String runbook)
    {
        this.runbook = runbook;
    }
    public String getAutomationId()
    {
        return automationId;
    }
    public void setAutomationId(String automationId)
    {
        this.automationId = automationId;
    }
    public String getAutomation()
    {
        return automation;
    }
    public void setAutomation(String automation)
    {
        this.automation = automation;
    }
    public String getModule()
    {
        return module;
    }
    public void setModule(String module)
    {
        this.module = module;
    }
    public String getEventId()
    {
        return eventId;
    }
    public void setEventId(String eventId)
    {
        this.eventId = eventId;
    }
    public Boolean getActive()
    {
        return active;
    }
    public void setActive(Boolean active)
    {
        this.active = active;
    }
    public Boolean getNewWorksheetOnly()
    {
        return newWorksheetOnly;
    }
    public void setNewWorksheetOnly(Boolean newWorksheetOnly)
    {
        this.newWorksheetOnly = newWorksheetOnly;
    }
    
    // RBA-14814 : Implementation - Create and Process Resolution Routing Rule for Security IR
    public Boolean getSir()
    {
		return sir;
	}
	public void setSir(Boolean sir)
	{
		this.sir = sir;
	}
	public String getSirSource() {
		return sirSource;
	}
	public void setSirSource(String sirSource) {
		this.sirSource = sirSource;
	}
	public String getSirTitle()
	{
		return sirTitle;
	}
	public void setSirTitle(String sirTitle)
	{
		this.sirTitle = sirTitle;
	}
	public String getSirType()
	{
		return sirType;
	}
	public void setSirType(String sirType)
	{
		this.sirType = sirType;
	}
	public String getSirPlaybook()
	{
		return sirPlaybook;
	}
	public void setSirPlaybook(String sirPlaybook)
	{
		this.sirPlaybook = sirPlaybook;
	}
	public String getSirSeverity()
	{
		return sirSeverity;
	}
	public void setSirSeverity(String sirSeverity)
	{
		this.sirSeverity = sirSeverity;
	}
	public String getSirOwner() {
		return sirOwner;
	}
	public void setSirOwner(String sirOwner) {
		this.sirOwner = sirOwner;
	}
	public String getSirId() {
		return sirId;
	}
	public void setSirId(String sirId) {
		this.sirId = sirId;
	}
	public String getProblemId() {
		return sirProblemId;
	}
	public void setProblemId(String problemId) {
		this.sirProblemId = problemId;
	}
	
	@Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((active == null) ? 0 : active.hashCode());
        result = prime * result + ((automation == null) ? 0 : automation.hashCode());
        result = prime * result + ((automationId == null) ? 0 : automationId.hashCode());
        result = prime * result + ((eventId == null) ? 0 : eventId.hashCode());
        result = prime * result + ((module == null) ? 0 : module.hashCode());
        result = prime * result + ((newWorksheetOnly == null) ? 0 : newWorksheetOnly.hashCode());
        result = prime * result + ((rid == null) ? 0 : rid.hashCode());
        result = prime * result + ((ridFields == null) ? 0 : ridFields.hashCode());
        result = prime * result + ((runbook == null) ? 0 : runbook.hashCode());
        result = prime * result + ((runbookId == null) ? 0 : runbookId.hashCode());
        result = prime * result + ((schema == null) ? 0 : schema.hashCode());
        result = prime * result + ((wiki == null) ? 0 : wiki.hashCode());
        result = prime * result + ((wikiId == null) ? 0 : wikiId.hashCode());
        // RBA-14814 : Implementation - Create and Process Resolution Routing Rule for Security IR
        result = prime * result + ((sir == null) ? 0 : sir.hashCode());
        result = prime * result + ((sirSource == null) ? 0 : sirSource.hashCode());
        result = prime * result + ((sirTitle == null) ? 0 : sirTitle.hashCode());
        result = prime * result + ((sirType == null) ? 0 : sirType.hashCode());
        result = prime * result + ((sirPlaybook == null) ? 0 : sirPlaybook.hashCode());
        result = prime * result + ((sirSeverity == null) ? 0 : sirSeverity.hashCode());
        result = prime * result + ((sirOwner == null) ? 0 : sirOwner.hashCode());
        result = prime * result + ((sirId == null) ? 0 : sirId.hashCode());
        result = prime * result + ((sirProblemId == null) ? 0 : sirProblemId.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        RRRuleVO other = (RRRuleVO) obj;
        if (active == null)
        {
            if (other.active != null) return false;
        }
        else if (!active.equals(other.active)) return false;
        if (automation == null)
        {
            if (other.automation != null) return false;
        }
        else if (!automation.equals(other.automation)) return false;
        if (automationId == null)
        {
            if (other.automationId != null) return false;
        }
        else if (!automationId.equals(other.automationId)) return false;
        if (eventId == null)
        {
            if (other.eventId != null) return false;
        }
        else if (!eventId.equals(other.eventId)) return false;
        if (module == null)
        {
            if (other.module != null) return false;
        }
        else if (!module.equals(other.module)) return false;
        if (newWorksheetOnly == null)
        {
            if (other.newWorksheetOnly != null) return false;
        }
        else if (!newWorksheetOnly.equals(other.newWorksheetOnly)) return false;
        if (rid == null)
        {
            if (other.rid != null) return false;
        }
        else if (!rid.equals(other.rid)) return false;
        if (ridFields == null)
        {
            if (other.ridFields != null) return false;
        }
        else if (!ridFields.equals(other.ridFields)) return false;
        if (runbook == null)
        {
            if (other.runbook != null) return false;
        }
        else if (!runbook.equals(other.runbook)) return false;
        if (runbookId == null)
        {
            if (other.runbookId != null) return false;
        }
        else if (!runbookId.equals(other.runbookId)) return false;
        if (schema == null)
        {
            if (other.schema != null) return false;
        }
        else if (!schema.equals(other.schema)) return false;
        if (wiki == null)
        {
            if (other.wiki != null) return false;
        }
        else if (!wiki.equals(other.wiki)) return false;
        if (wikiId == null)
        {
            if (other.wikiId != null) return false;
        }
        else if (!wikiId.equals(other.wikiId)) return false;
        
        // RBA-14814 : Implementation - Create and Process Resolution Routing Rule for Security IR
        if (sir == null)
        {
            if (other.sir != null) return false;
        }
        else if (!sir.equals(other.sir)) return false;
        
        if (sirSource == null)
        {
            if (other.sirSource != null) return false;
        }
        else if (!sirSource.equals(other.sirSource)) return false;
        
        if (sirTitle == null)
        {
            if (other.sirTitle != null) return false;
        }
        else if (!sirTitle.equals(other.sirTitle)) return false;
        
        if (sirType == null)
        {
            if (other.sirType != null) return false;
        }
        else if (!sirType.equals(other.sirType)) return false;
        
        if (sirPlaybook == null)
        {
            if (other.sirPlaybook != null) return false;
        }
        else if (!sirPlaybook.equals(other.sirPlaybook)) return false;
        
        if (sirSeverity == null)
        {
            if (other.sirSeverity != null) return false;
        }
        else if (!sirSeverity.equals(other.sirSeverity)) return false;
        
        if (sirOwner == null)
        {
            if (other.sirOwner != null) return false;
        }
        else if (!sirOwner.equals(other.sirOwner)) return false;
        
                if (sirId == null)
        {
            if (other.sirId != null) return false;
        }
        else if (!sirId.equals(other.sirId)) return false;
                
        if (sirProblemId == null)
        {
            if (other.sirProblemId != null) return false;
        }
        else if (!sirProblemId.equals(other.sirProblemId)) return false;

        return true;
    }
    
}
