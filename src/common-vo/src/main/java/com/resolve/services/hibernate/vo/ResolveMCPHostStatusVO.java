package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveMCPHostStatusVO extends VO
{
    private static final long serialVersionUID = -2067432963645274302L;
    
    // Memory, CPU etc.
    private String UType;
    // warning, critical etc.
    private String UStatus;
    // hard disk is at critical level, CPU is at warning level. etc.
    private String UDescription;
    
    // The component to which this status belongs
    private ResolveMCPHostVO mcpHostVO;

    public String getUType()
    {
        return UType;
    }

    public void setUType(String uType)
    {
        UType = uType;
    }

    public String getUStatus()
    {
        return UStatus;
    }

    public void setUStatus(String uStatus)
    {
        UStatus = uStatus;
    }

    public String getUDescription()
    {
        return UDescription;
    }

    public void setUDescription(String uDescription)
    {
        this.UDescription = uDescription;
    }

    public ResolveMCPHostVO getMcpHostVO()
    {
        return mcpHostVO;
    }

    public void setMcpHostVO(ResolveMCPHostVO mcpHostVO)
    {
        this.mcpHostVO = mcpHostVO;
    }
}
