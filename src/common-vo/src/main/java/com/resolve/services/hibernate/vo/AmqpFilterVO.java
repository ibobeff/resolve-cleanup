/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import org.apache.commons.lang3.StringUtils;

import com.resolve.services.interfaces.VO;
import com.resolve.util.MappingAnnotation;

public class AmqpFilterVO extends GatewayFilterVO
{
    private static final long serialVersionUID = 6663504642323607870L;
    
    private String UTargetQueue;
    private Boolean UExchange;

    // object referenced by
    public AmqpFilterVO()
    {
        super();
    } // AmqpFilterVO

    @MappingAnnotation(columnName = "IS_EXCHANGE")
    public Boolean getUExchange()
    {
        return this.UExchange;
    } // getUIsExchange

    public void setUExchange(Boolean uExchange)
    {
        this.UExchange = uExchange;
    } // setUIsExchange

    @MappingAnnotation(columnName = "TARGET_QUEUE")
    public String getUTargetQueue()
    {
        return UTargetQueue;
    }

    public void setUTargetQueue(String uTargetQueue)
    {
        if (StringUtils.isNotBlank(uTargetQueue) || UTargetQueue.equals(VO.STRING_DEFAULT))
        {
            UTargetQueue = uTargetQueue != null ? uTargetQueue.trim() : uTargetQueue;
        }
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UExchange == null) ? 0 : UExchange.hashCode());
        result = prime * result + ((UTargetQueue == null) ? 0 : UTargetQueue.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        AmqpFilterVO other = (AmqpFilterVO) obj;
        if (UExchange == null || !UExchange)
        {
            if (other.UExchange != null) return false;
        }
        else if (!UExchange.equals(other.UExchange)) return false;
        
        if (UTargetQueue == null)
        {
            if (StringUtils.isNotBlank(other.UTargetQueue)) return false;
        }
        else if (!UTargetQueue.trim().equals(other.UTargetQueue == null ? "" : other.UTargetQueue.trim())) return false;
        return true;
    }
}
