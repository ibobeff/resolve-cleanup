/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.dom4j.Element;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.resolve.services.interfaces.IndexComponent;
import com.resolve.services.interfaces.UIDisplayAPI;
import com.resolve.services.interfaces.VO;
import com.resolve.services.util.JsonUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class WikiDocumentVO extends VO implements UIDisplayAPI, IndexComponent
{
    private static final long serialVersionUID = -860941202885580118L;

    public final static String TABLE_NAME = "wikidoc";
    public final static String RESOURCE_TYPE = "wikidoc";
    public final static String COL_CONTENT = "CONTENT";
    public final static String COL_MODEL_PROCESS = "MODEL_PROCESS";
    public final static String COL_MODEL_EXCEPTION = "MODEL_EXCEPTION";
    public final static String COL_MODEL_FINAL = "MODEL_FINAL";
    public final static String COL_DECISION_TREE = "DECISION_TREE";
    
    public final static String WIKI = "wiki";
    public final static String DECISIONTREE = "decisiontree";
    public final static String CATALOG = "catalog";
    public final static String PLAYBOOK = "playbook";
    
    public static final String PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY = "wikiName";
    public static final String PLAYBOOK_ACTIVITY_MAP_PHASE_KEY = "phase";
    public static final String PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY = "activityName";
    public static final String PLAYBOOK_ACTIVITY_MAP_INCIDENT_ID_KEY = "incidentId";
    public static final String PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY = "altActivityId";
    public static final String PLAYBOOK_ACTIVITY_MAP_TEMPLATE_ACTIVITY_KEY = "templateActivity";
    public static final String PLAYBOOK_ACTIVITY_MAP_ACTIVITY_IS_REQUIRED_KEY = "isRequired";
    public static final String PLAYBOOK_ACTIVITY_MAP_ACTIVITY_DESCRIPTION_KEY = "description";
    public static final String PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY = "days";
    public static final String PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY = "hours";
    public static final String PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY = "minutes";
    public static final String PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_ASSIGNEE_KEY = "assignee";
    public static final String PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_STATUS_KEY = "status";
    public static final String PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_DUEDATE_KEY = "dueDate";
    public static final String PLAYBOOK_ACTIVITY_RUNTIME_STATUS_DEFAULT_VALUE = "Open";
    
    public static final String PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY = "metaPhaseId";
    public static final String PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY = "metaActivityId";
    public static final String PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_POSITION_KEY = "position";
    
    protected boolean isArchievable = true;

    private String UName;
    private String UFullname;
    private String UTitle;
    private String USummary;
    private String UContent;
    private String UWysiwyg;
    private String UModelProcess;
    private String UModelException;//this is the abort model
    private String UModelFinal;
    private String UDecisionTree;
    private String ULanguage;
    private String UNamespace;
    private String UTag;
    private String UCatalog;
    private Integer UVersion;
    private Boolean UIsDeleted;
    private Boolean UIsLocked;
    private String ULockedBy;
    private Boolean UIsHidden;
    private Boolean UIsActive;
    private Boolean UIsDefaultRole;
    private Boolean UIsRoot;
    private Boolean UHasActiveModel;
    private Double URatingBoost;
    
    private Boolean UIsStable;

    public Boolean getUIsStable() {
		return UIsStable;
	}

	public void setUIsStable(Boolean uIsStable) {
		UIsStable = uIsStable;
	}

    private Boolean UIsRequestSubmission;
    private Date UReqestSubmissionOn;
    private Date ULastReviewedOn;
    private String ULastReviewedBy;

    private Date UExpireOn;

    private Integer UDTAbortTime;
    
    private String UDisplayMode;
    private String UCatalogId;
    private String UWikiParameters;
    
    private String UResolutionBuilderId; //Resolution Builder Id
    private Boolean UHasResolutionBuilder;
    private RBGeneralVO rbGeneralVO;
    
    // transient
    private String UReadRoles;
    private String UWriteRoles;
    private String UAdminRoles;
    private String UExecuteRoles;
    private String UImpexSysId;
    
    //social related flags
    private Boolean UIsDocumentStreamLocked;
    private Boolean UIsCurrentUserFollowing;

    private String UDTOptions;
    
    private Boolean UIsTemplate;
    private Long USIRRefCount; // # of references to this wiki from SIR
    
    // object references
    private AccessRightsVO accessRights;
    private RBGeneralVO rbGeneral;

    // object referenced by
    private Collection<WikidocAttachmentRelVO> wikidocAttachmentRels;
    private Collection<WikidocResolveTagRelVO> wikidocResolveTagRels;
    private Collection<WikidocStatisticsVO> wikidocStatistics;
    private Collection<RoleWikidocHomepageRelVO> roleWikidocHomepageRels;
    private Collection<WikidocResolutionRatingVO> wikidocResolutionRating;
    private Collection<WikidocQualityRatingVO> wikidocQualityRating;
    private Set<PlaybookActivitiesVO> pbActivities;
    private ResolveNamespaceVO namespace;
    
    //for UI
    private boolean softLock = false;
    private String lockedByUsername;
    
    private boolean hasChildren = false;

    public WikiDocumentVO()
    {
        setUIsActive(true);
        setUIsDeleted(false);
        setUIsHidden(false);
        setUIsLocked(false);
        setUIsRoot(false);
    }
    
    public WikiDocumentVO(String sysId)
    {
        this();
        super.setSys_id(sysId);
    }

    public String getUReadRoles()
    {
        return UReadRoles;
    }

    public void setUReadRoles(String readRoles)
    {
        UReadRoles = readRoles;
    }

    public String getUWriteRoles()
    {
        return UWriteRoles;
    }

    public void setUWriteRoles(String writeRoles)
    {
        UWriteRoles = writeRoles;
    }

    public String getUAdminRoles()
    {
        return UAdminRoles;
    }

    public void setUAdminRoles(String adminRoles)
    {
        UAdminRoles = adminRoles;
    }

    public String getUExecuteRoles()
    {
        return UExecuteRoles;
    }

    public void setUExecuteRoles(String executeRoles)
    {
        UExecuteRoles = executeRoles;
    }
    
    public String getUImpexSysId()
    {
        return UImpexSysId;
    }

    public void setUImpexSysId(String uImpexSysId)
    {
        UImpexSysId = uImpexSysId;
    }

    public Map<String, String> UgetArchievableColumns()
    {
        // should have column name, object attribute name mapped
        Map<String, String> hm = new HashMap<String, String>();
        hm.put(COL_CONTENT, "getUContent");
        hm.put(COL_MODEL_PROCESS, "getUModelProcess");
        hm.put(COL_MODEL_EXCEPTION, "getUModelException");
        hm.put(COL_DECISION_TREE, "getUDecisionTree");

        return hm;
    }

    public String UgetTableName()
    {
        return TABLE_NAME;
    }

    public boolean UisArchievable()
    {
        return isArchievable;
    }

    @Override
    public boolean equals(Object otherObj)
    {
        boolean isEquals = false;
        if (otherObj instanceof WikiDocumentVO)
        {
            WikiDocumentVO otherWiki = (WikiDocumentVO) otherObj;
            // if sys_id are not equal, they are diff
            if (otherWiki.getSys_id().equals(this.getSys_id()))
            {
                isEquals = true;
            }
        }
        return isEquals;
    }

    @Override
    public int hashCode()
    {
        int hash = 1;
        hash = hash * 31 + this.UFullname.hashCode();
        hash = hash * 31 + (this.getSys_id() == null ? 0 : this.getSys_id().hashCode());
        return hash;
    }

    /**
     * compares 2 wiki objs with the same sys_id to check if they have the same
     * data
     *
     * @param dirtyWikiObj
     * @return
     */
    public boolean hasSameData(WikiDocumentVO dirtyWikiObj)
    {
        boolean hasSameData = true;

        // only if the objs share the same sys_id
        outerIf: if (this.getSys_id().equals(dirtyWikiObj.getSys_id()))
        {
            String tempStr1 = this.getUContent() != null ? this.getUContent().trim() : "";
            String tempStr2 = dirtyWikiObj.getUContent() != null ? dirtyWikiObj.getUContent().trim() : "";
            if (!tempStr1.equals(tempStr2))
            {
                hasSameData = false;
                break outerIf;
            }

            tempStr1 = this.getUWysiwyg() != null ? this.getUWysiwyg().trim() : "";
            tempStr2 = dirtyWikiObj.getUWysiwyg() != null ? dirtyWikiObj.getUWysiwyg().trim() : "";
            if (!tempStr1.equals(tempStr2))
            {
                hasSameData = false;
                break outerIf;
            }

            tempStr1 = this.getUModelProcess() != null ? this.getUModelProcess().trim() : "";
            tempStr2 = dirtyWikiObj.getUModelProcess() != null ? dirtyWikiObj.getUModelProcess().trim() : "";
            if (!tempStr1.equals(tempStr2))
            {
                hasSameData = false;
                break outerIf;
            }

            tempStr1 = this.getUModelException() != null ? this.getUModelException().trim() : "";
            tempStr2 = dirtyWikiObj.getUModelException() != null ? dirtyWikiObj.getUModelException().trim() : "";
            if (!tempStr1.equals(tempStr2))
            {
                hasSameData = false;
                break outerIf;
            }

            tempStr1 = this.getUModelFinal() != null ? this.getUModelFinal().trim() : "";
            tempStr2 = dirtyWikiObj.getUModelFinal() != null ? dirtyWikiObj.getUModelFinal().trim() : "";
            if (!tempStr1.equals(tempStr2))
            {
                hasSameData = false;
                break outerIf;
            }

            tempStr1 = this.getUDecisionTree() != null ? this.getUDecisionTree().trim() : "";
            tempStr2 = dirtyWikiObj.getUDecisionTree() != null ? dirtyWikiObj.getUDecisionTree().trim() : "";
            if (!tempStr1.equals(tempStr2))
            {
                hasSameData = false;
                break outerIf;
            }

            tempStr1 = this.getUTitle() != null ? this.getUTitle().trim() : "";
            tempStr2 = dirtyWikiObj.getUTitle() != null ? dirtyWikiObj.getUTitle().trim() : "";
            if (!tempStr1.equals(tempStr2))
            {
                hasSameData = false;
                break outerIf;
            }

            tempStr1 = this.getUSummary() != null ? this.getUSummary().trim() : "";
            tempStr2 = dirtyWikiObj.getUSummary() != null ? dirtyWikiObj.getUSummary().trim() : "";
            if (!tempStr1.equals(tempStr2))
            {
                hasSameData = false;
                break outerIf;
            }

            tempStr1 = this.getUTag() != null ? this.getUTag().trim() : "";
            tempStr2 = dirtyWikiObj.getUTag() != null ? dirtyWikiObj.getUTag().trim() : "";
            if (!tempStr1.equals(tempStr2))
            {
                hasSameData = false;
                break outerIf;
            }

            tempStr1 = this.getUReadRoles() != null ? this.getUReadRoles().trim() : "";
            tempStr2 = dirtyWikiObj.getUReadRoles() != null ? dirtyWikiObj.getUReadRoles().trim() : "";
            if (!tempStr1.equals(tempStr2))
            {
                hasSameData = false;
                break outerIf;
            }

            tempStr1 = this.getUWriteRoles() != null ? this.getUWriteRoles().trim() : "";
            tempStr2 = dirtyWikiObj.getUWriteRoles() != null ? dirtyWikiObj.getUWriteRoles().trim() : "";
            if (!tempStr1.equals(tempStr2))
            {
                hasSameData = false;
                break outerIf;
            }

            tempStr1 = this.getUAdminRoles() != null ? this.getUAdminRoles().trim() : "";
            tempStr2 = dirtyWikiObj.getUAdminRoles() != null ? dirtyWikiObj.getUAdminRoles().trim() : "";
            if (!tempStr1.equals(tempStr2))
            {
                hasSameData = false;
                break outerIf;
            }

            tempStr1 = this.getUExecuteRoles() != null ? this.getUExecuteRoles().trim() : "";
            tempStr2 = dirtyWikiObj.getUExecuteRoles() != null ? dirtyWikiObj.getUExecuteRoles().trim() : "";
            if (!tempStr1.equals(tempStr2))
            {
                hasSameData = false;
                break outerIf;
            }
            
            tempStr1 = this.getUDTOptions() != null ? this.getUDTOptions().trim() : "";
            tempStr2 = dirtyWikiObj.getUDTOptions() != null ? dirtyWikiObj.getUDTOptions().trim() : "";
            if (!tempStr1.equals(tempStr2))
            {
                hasSameData = false;
                break outerIf;
            }

            if (!this.getUVersion().equals(dirtyWikiObj.getUVersion()))
            {
                hasSameData = false;
                break outerIf;
            }
        }
        else
        {
            hasSameData = false;
        }

        return hasSameData;
    }

    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    public String getUFullname()
    {
        return this.UFullname;
    }

    public void setUFullname(String UFullname)
    {
        this.UFullname = UFullname;
    }

    public String getUTitle()
    {
        return this.UTitle;
    }

    public void setUTitle(String UTitle)
    {
        this.UTitle = UTitle;
    }

    public String getUSummary()
    {
        return this.USummary;
    }

    public void setUSummary(String USummary)
    {
        this.USummary = USummary;
    }

    public String getUContent()
    {
        return this.UContent;
    }

    public void setUContent(String UContent)
    {
        this.UContent = UContent;
    }

    public String getUWysiwyg()
    {
        return this.UWysiwyg;
    }

    public void setUWysiwyg(String UWysiwyg)
    {
        this.UWysiwyg = UWysiwyg;
    }

    public String getUModelProcess()
    {
        return this.UModelProcess;
    }

    public void setUModelProcess(String UModelProcess)
    {
        this.UModelProcess = UModelProcess;
    }

    public String getUModelException()
    {
        return this.UModelException;
    }

    public void setUModelException(String UModelException)
    {
        this.UModelException = UModelException;
    }

    public String getUModelFinal()
    {
        return this.UModelFinal;
    }

    public void setUModelFinal(String UModelFinal)
    {
        this.UModelFinal = UModelFinal;
    }

    public String getUDecisionTree()
    {
        return this.UDecisionTree;
    }

    public void setUDecisionTree(String UDecisionTree)
    {
        this.UDecisionTree = UDecisionTree;
    }

    public String getULanguage()
    {
        return this.ULanguage;
    }

    public void setULanguage(String ULanguage)
    {
        this.ULanguage = ULanguage;
    }

    public String getUNamespace()
    {
        return this.UNamespace;
    }

    public void setUNamespace(String UNamespace)
    {
        this.UNamespace = UNamespace;
    }

    public String getUTag()
    {
        return this.UTag;
    }

    public void setUTag(String UTag)
    {
        this.UTag = UTag;
    }
    
    public String getUCatalog()
    {
        return UCatalog;
    }

    public void setUCatalog(String uCatalog)
    {
        UCatalog = uCatalog;
    }

    public Integer getUVersion()
    {
        return this.UVersion;
    }

    public void setUVersion(Integer UVersion)
    {
        this.UVersion = UVersion;
    }

    public Boolean getUIsDeleted()
    {
        return UIsDeleted;
    }

    public Boolean ugetUIsDeleted()
    {
        if (this.UIsDeleted == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsDeleted;
        }
    }

    public void setUIsDeleted(Boolean isDeleted)
    {
        UIsDeleted = isDeleted;
    }

    public Boolean getUIsLocked()
    {
        return UIsLocked;
    }

    public Boolean ugetUIsLocked()
    {
        if (this.UIsLocked == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsLocked;
        }
    }

    public String getULockedBy()
    {
        return this.ULockedBy;
    }

    public void setULockedBy(String lockedBy)
    {
        this.ULockedBy = lockedBy;
    }

    public void setUIsLocked(Boolean isLocked)
    {
        UIsLocked = isLocked;
    }

    public Boolean getUIsHidden()
    {
        return UIsHidden;
    }

    public Boolean ugetUIsHidden()
    {
        if (this.UIsHidden == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsHidden;
        }
    }

    public void setUIsHidden(Boolean isHidden)
    {
        UIsHidden = isHidden;
    }

    public Boolean getUIsActive()
    {
        return UIsActive;
    }

    public Boolean ugetUIsActive()
    {
        if (this.UIsActive == null)
        {
            return new Boolean(true);
        }
        else
        {
            return this.UIsActive;
        }
    }

    public void setUIsActive(Boolean isActive)
    {
        UIsActive = isActive;
    }

    public Boolean getUHasActiveModel()
    {
        return UHasActiveModel;
    }

    public Boolean ugetUHasActiveModel()
    {
        if (this.UHasActiveModel == null)
        {
            String model = getUModelProcess();
            if (model != null && model.trim().length() > 0)
            {
                return new Boolean(true);
            }
            else
            {
                return new Boolean(false);
            }
        }
        else
        {
            return this.UHasActiveModel;
        }
    }

    public void setUHasActiveModel(Boolean hasActiveModel)
    {
        UHasActiveModel = hasActiveModel;
    }

    public Boolean getUIsDefaultRole()
    {
        return UIsDefaultRole;
    }

    public Boolean ugetUIsDefaultRole()
    {
        if (this.UIsDefaultRole == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsDefaultRole;
        }
    }

    public void setUIsDefaultRole(Boolean isDefaultRole)
    {
        UIsDefaultRole = isDefaultRole;
    }

    public Boolean getUIsRoot()
    {
        return UIsRoot;
    }

    public Boolean ugetUIsRoot()
    {
        if (UIsRoot == null)
        {
            return false;
        }
        else
        {
            return UIsRoot;
        }
    }

    public void setUIsRoot(Boolean uIsRoot)
    {
        UIsRoot = uIsRoot;
    }

    //flag to indicate it has Resolution builder
    public Boolean ugetUHasRB()
    {
        return StringUtils.isNotBlank(UResolutionBuilderId);
    }

    public Double getURatingBoost()
    {
        return URatingBoost;
    }

    public void setURatingBoost(Double uRatingBoost)
    {
        URatingBoost = uRatingBoost;
    }

    public AccessRightsVO getAccessRights()
    {
        return accessRights;
    }

    public void setAccessRights(AccessRightsVO accessRights)
    {
        this.accessRights = accessRights;

        // one-way only - no collections on the other side
    }

    public RBGeneralVO getRbGeneral()
    {
        return rbGeneral;
    }

    public void setRbGeneral(RBGeneralVO rbGeneral)
    {
        this.rbGeneral = rbGeneral;
    }
    
    @JsonIgnore
    public String getUDTOptions()
    {
        return this.UDTOptions;
    }

    public void setUDTOptions(String UDTOptions)
    {
        this.UDTOptions = UDTOptions;
    }
    
    public Boolean getUIsTemplate()
    {
        return UIsTemplate;
    }

    public Boolean ugetUIsTemplate()
    {
        if (UIsTemplate == null)
        {
            return false;
        }
        else
        {
            return UIsTemplate;
        }
    }

    public void setUIsTemplate(Boolean uIsTemplate)
    {
        UIsTemplate = uIsTemplate;
    }
    
    public Long getUSIRRefCount()
    {
        return this.USIRRefCount;
    }

    public void setUSIRRefCount(Long USIRRefCount)
    {
        this.USIRRefCount = USIRRefCount;
    }
    
    public Collection<WikidocAttachmentRelVO> getWikidocAttachmentRels()
    {
        return wikidocAttachmentRels;
    }

    public void setWikidocAttachmentRels(Collection<WikidocAttachmentRelVO> wikiAttachments)
    {
        this.wikidocAttachmentRels = wikiAttachments;
    }

    public Collection<WikidocResolveTagRelVO> getWikidocResolveTagRels()
    {
        return wikidocResolveTagRels;
    }

    public void setWikidocResolveTagRels(Collection<WikidocResolveTagRelVO> wikidocResolveTagRels)
    {
        this.wikidocResolveTagRels = wikidocResolveTagRels;
    }

    public Collection<WikidocStatisticsVO> getWikidocStatistics()
    {
        return wikidocStatistics;
    }

    public void setWikidocStatistics(Collection<WikidocStatisticsVO> WikidocStatistics)
    {
        this.wikidocStatistics = WikidocStatistics;
    }

    public Collection<RoleWikidocHomepageRelVO> getRoleWikidocHomepageRels()
    {
        return roleWikidocHomepageRels;
    }

    public void setRoleWikidocHomepageRels(Collection<RoleWikidocHomepageRelVO> roleWikidocHomepageRels)
    {
        this.roleWikidocHomepageRels = roleWikidocHomepageRels;
    }

    public Collection<WikidocResolutionRatingVO> getWikidocResolutionRating()
    {
        return wikidocResolutionRating;
    }

    public void setWikidocRatingResolution(Collection<WikidocResolutionRatingVO> wikidocResolutionRating)
    {
        this.wikidocResolutionRating = wikidocResolutionRating;
    }

    // Wikidoc-WikidocResolution relationship
    public Collection<WikidocQualityRatingVO> getWikidocQualityRating()
    {
        return wikidocQualityRating;
    }

    public void setWikidocQualityRating(Collection<WikidocQualityRatingVO> wikidocQualityRating)
    {
        this.wikidocQualityRating = wikidocQualityRating;
    }

    public Boolean getUIsRequestSubmission()
    {
        return UIsRequestSubmission;
    }

    public Boolean ugetUIsRequestSubmission()
    {
        if (UIsRequestSubmission == null)
        {
            return false;
        }
        else
        {
            return UIsRequestSubmission;
        }
    }

    public void setUIsRequestSubmission(Boolean uIsRequestSubmission)
    {
        UIsRequestSubmission = uIsRequestSubmission;
    }

    public Date getUReqestSubmissionOn()
    {
        return UReqestSubmissionOn;
    }

    public void setUReqestSubmissionOn(Date uReqestSubmissionOn)
    {
        UReqestSubmissionOn = uReqestSubmissionOn;
    }

    public Date getULastReviewedOn()
    {
        return ULastReviewedOn;
    }

    public void setULastReviewedOn(Date uLastReviewedOn)
    {
        ULastReviewedOn = uLastReviewedOn;
    }

    public String getULastReviewedBy()
    {
        return ULastReviewedBy;
    }

    public void setULastReviewedBy(String uLastReviewedBy)
    {
        ULastReviewedBy = uLastReviewedBy;
    }
    
    public Date getUExpireOn()
    {
        return UExpireOn;
    }

    public void setUExpireOn(Date uExpireOn)
    {
        UExpireOn = uExpireOn;
    }

    public Integer getUDTAbortTime()
    {
        return UDTAbortTime;
    }

    public void setUDTAbortTime(Integer uDTAbortTimeInSecs)
    {
        UDTAbortTime = uDTAbortTimeInSecs;
    }

    public String getUDisplayMode()
    {
        return UDisplayMode;
    }

    public void setUDisplayMode(String uDisplayMode)
    {
        UDisplayMode = uDisplayMode;
    }

    public String getUCatalogId()
    {
        return UCatalogId;
    }

    public void setUCatalogId(String uCatalogId)
    {
        UCatalogId = uCatalogId;
    }
    
    public String getUWikiParameters()
    {
        return UWikiParameters;
    }

    public void setUWikiParameters(String uWikiParameters)
    {
        UWikiParameters = uWikiParameters;
    }
    
    public Boolean getUIsDocumentStreamLocked()
    {
        return UIsDocumentStreamLocked;
    }

    public void setUIsDocumentStreamLocked(Boolean uIsDocumentStreamLocked)
    {
        UIsDocumentStreamLocked = uIsDocumentStreamLocked;
    }

    public Boolean getUIsCurrentUserFollowing()
    {
        return UIsCurrentUserFollowing;
    }

    public void setUIsCurrentUserFollowing(Boolean uIsCurrentUserFollowing)
    {
        UIsCurrentUserFollowing = uIsCurrentUserFollowing;
    }

    public String getUResolutionBuilderId()
    {
        return UResolutionBuilderId;
    }

    public void setUResolutionBuilderId(String uResolutionBuilderId)
    {
        UResolutionBuilderId = uResolutionBuilderId;
    }

    public Boolean getUHasResolutionBuilder()
    {
        return UHasResolutionBuilder;
    }

    public Boolean ugetUHasResolutionBuilder()
    {
        if (this.UHasResolutionBuilder == null)
        {
            String resolutionBuilderId = getUResolutionBuilderId();
            if (resolutionBuilderId != null && resolutionBuilderId.trim().length() > 0)
            {
                return new Boolean(true);
            }
            else
            {
                return new Boolean(false);
            }
        }
        else
        {
            return this.UHasResolutionBuilder;
        }
    }

    public void setUHasResolutionBuilder(Boolean hasResolutionBuilder)
    {
        UHasResolutionBuilder = hasResolutionBuilder;
    }

    //This is transient
    public RBGeneralVO getRbGeneralVO()
    {
        return rbGeneralVO;
    }

    public void setRbGeneralVO(RBGeneralVO rbGeneralVO)
    {
        this.rbGeneralVO = rbGeneralVO;
    }
    
    public void trim()
    {
        this.UName = UName != null ? UName.trim() : null;
        this.UFullname = UFullname != null ? UFullname.trim() : null;
        this.UTitle = UTitle != null ? UTitle.trim() : null;
        this.USummary = USummary != null ? USummary.trim() : null;
        this.UNamespace = UNamespace != null ? UNamespace.trim() : null;
        this.UTag = UTag != null ? UTag.trim() : null;
    }

    public String ui_getDisplayName()
    {
        return getUFullname();
    }

    public String ui_getDisplayType()
    {
        return "Wiki Document";
    }

    public String ugetIndexContent()
    {
        String indexContent = UName + " " + UNamespace + " " + UFullname;
        if (StringUtils.isNotEmpty(UTitle) && !UFullname.equals(UTitle))
        {
            indexContent += " " + UTitle;
        }
        indexContent += " " + USummary + "##%" + UContent;
        if (StringUtils.isNotEmpty(UModelProcess))
        {
            indexContent += " " + UModelProcess;
        }
        if (StringUtils.isNotEmpty(UModelException))
        {
            indexContent += " " + UModelException;
        }
        if (StringUtils.isNotBlank(UDTOptions))
        {
            indexContent += " " + UDTOptions;
        }
        indexContent += "%##";
        if (StringUtils.isNotEmpty(UTag))
        {
            indexContent += " " + UTag;
        }
        return indexContent;
    }
    
    // Wikidoc-PbActivities relationship
    public Set<PlaybookActivitiesVO> getPbActivities()
    {
        return pbActivities;
    }

    public void setPbActivities(Set<PlaybookActivitiesVO> pbActivities)
    {
        this.pbActivities = pbActivities;
    }
    
    public WikiDocumentVO createCopy()
    {
        WikiDocumentVO newWikiDocument = new WikiDocumentVO();
        newWikiDocument.setSys_id(getSys_id());
        newWikiDocument.setUName(getUName());
        newWikiDocument.setUFullname(getUFullname());
        newWikiDocument.setUTitle(getUTitle());
        newWikiDocument.setUSummary(getUSummary());
        newWikiDocument.setUContent(getUContent());
        newWikiDocument.setUWysiwyg(getUWysiwyg());
        newWikiDocument.setUModelProcess(getUModelProcess());
        newWikiDocument.setUModelException(getUModelException());
        newWikiDocument.setUModelFinal(getUModelFinal());
        newWikiDocument.setULanguage(getULanguage());
        newWikiDocument.setUNamespace(getUNamespace());
        newWikiDocument.setUTag(getUTag());
        newWikiDocument.setUVersion(getUVersion());
        newWikiDocument.setSysCreatedOn(GMTDate.getDate());

        newWikiDocument.setUIsActive(getUIsActive());
        newWikiDocument.setUIsDefaultRole(getUIsDefaultRole());
        newWikiDocument.setUIsDeleted(getUIsDeleted());
        newWikiDocument.setUIsHidden(getUIsHidden());
        newWikiDocument.setUIsLocked(getUIsLocked());
        
        newWikiDocument.setUDTOptions(getUDTOptions());
        newWikiDocument.setNamespace(getNamespace());
        
        return newWikiDocument;
    }

    public void cleanContent()
    {
        if(StringUtils.isNotBlank(this.getUContent()))
        {
            Pattern p = Pattern.compile("\\{section[^\\}]*\\}.*?\\{section\\}", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
            String[] contents = p.split(this.UContent);
            for (String content : contents)
            {
                if (StringUtils.isNotBlank(content))
                {
                    this.UContent = this.UContent.replace(content, "\n{section}\n" + content + "\n{section}\n");
                }
            }
        }
    }
    
    public boolean isSoftLock()
    {
        return softLock;
    }

    public void setSoftLock(boolean softLock)
    {
        this.softLock = softLock;
    }

    public String getLockedByUsername()
    {
        return lockedByUsername;
    }

    public void setLockedByUsername(String lockedByUsername)
    {
        this.lockedByUsername = lockedByUsername;
    }
    
    public void transformModelProcessDescToCDATA()
    {
        String modelXml = getUModelProcess();
        String result = "";
        
        if (StringUtils.isNotEmpty(modelXml))
        {
            modelXml = modelXml.replace("\n", "&#xa;");
            try
            {
                result = transformDescToCDATA(modelXml);
            }
            catch (Exception e)
            {
                Log.log.error("Error in transformDescToCDATA:" + e.getMessage(), e);
                result = modelXml;
            }
            
            //set the new value
            this.setUModelProcess(result);
        }
    }
    
    public void transformModelExceptionDescToCDATA()
    {
        String modelXml = getUModelException();
        String result = "";
        
        if (StringUtils.isNotEmpty(modelXml))
        {
            try
            {
                result = transformDescToCDATA(modelXml);
            }
            catch (Exception e)
            {
                Log.log.error("Error in transformDescToCDATA:" + e.getMessage(), e);
                result = modelXml;
            }
            
            //set the new value
            this.setUModelException(result);
        }
    }

    
    public void transformModelProcessCDATAToDesc()
    {
        String modelXml = getUModelProcess();
        String result = null;
        
        if (StringUtils.isNotEmpty(modelXml))
        {
            try
            {
                result = transformCDATAToDesc(modelXml);
            }
            catch (Exception e)
            {
                Log.log.error("Error in transformCDATAToDesc:" + e.getMessage(), e);
                result = modelXml;
            }
            
            //set the new value
            this.setUModelProcess(result);
        }
    }
    
    public void transformModelExceptionCDATAToDesc()
    {
        String modelXml = getUModelException();
        String result = null;
        
        if (StringUtils.isNotEmpty(modelXml))
        {
            try
            {
                result = transformCDATAToDesc(modelXml);
            }
            catch (Exception e)
            {
                Log.log.error("Error in transformCDATAToDesc:" + e.getMessage(), e);
                result = modelXml;
            }
            
            //set the new value
            this.setUModelException(result);
        }
    }
    
    @SuppressWarnings("unchecked")
    private String transformCDATAToDesc(String modelXml)
    {
        String result = null;
        
        if (StringUtils.isNotEmpty(modelXml))
        {
            try
            {
                XDoc doc = new XDoc(modelXml);
                
                transformCDATAToDescForElements(doc, doc.getNodes("/mxGraphModel/root/Start"), "Start");
                transformCDATAToDescForElements(doc, doc.getNodes("/mxGraphModel/root/End"), "End");
                transformCDATAToDescForElements(doc, doc.getNodes("/mxGraphModel/root/Task"), "Task");
                transformCDATAToDescForElements(doc, doc.getNodes("/mxGraphModel/root/Event"), "Event");

                result = prepareXml(doc);
            }
            catch (Exception e)
            {
                Log.log.error("Error in transformCDATAToDesc:" + e.getMessage(), e);
                result = modelXml;
            }
        }
        
        return result;
    }
    
    private void transformCDATAToDescForElements(XDoc doc, List<Element> elements, String type)
    {
        if(elements != null && elements.size() > 0 && doc != null)
        {
            for (Element taskEl : elements)
            {
                String desc = "";
    
                Element elName = taskEl.element("name");
                if(elName != null)
                {                    
                    String name = elName.getTextTrim();
                    
                    Element paramsEl = taskEl.element("params");
                    
                    if (paramsEl != null)
                    {
	                    Element elInputsParam = paramsEl.element("inputs");
	                    String jsonInputsParams = elInputsParam.getStringValue();
	                    
	                    Element elOutputsParam = paramsEl.element("outputs");
	                    String jsonOutputsParams = elOutputsParam.getStringValue();
	                    
	                    if(type.equalsIgnoreCase("Task"))
	                    {
	                        desc = name + "?" + transformParamsToDesc(jsonInputsParams, jsonOutputsParams);
	                    }
	                    else
	                    {
	                        desc = transformParamsToDesc(jsonInputsParams, jsonOutputsParams);
	                    }
	                    
	                    taskEl.addAttribute("description", desc);
                    }
                }
            }
        }
    }
    
    private static String prepareXml(XDoc doc) throws Exception
    {
        doc.removeResolveNamespaces();
        
        return doc.toPrettyString().replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "").trim();
    }
    
    @SuppressWarnings("unchecked")
    private String transformDescToCDATA(String modelXml)
    {
        String result = "";
        
        if (StringUtils.isNotEmpty(modelXml))
        {
            try
            {
                XDoc doc = new XDoc(modelXml);
                
                //add the version
                Element rootEl = (Element) doc.getNode("/mxGraphModel/root");
                if(rootEl != null)
                {
                    String version = rootEl.valueOf("@version");
                    if(StringUtils.isBlank(version))
                    {
                        rootEl.addAttribute("version", "5.0");
                    }
                }
                
                transformDescToCDATAForElements(doc, doc.getNodes("/mxGraphModel/root/Start"), "Start");
                transformDescToCDATAForElements(doc, doc.getNodes("/mxGraphModel/root/End"), "End");
                transformDescToCDATAForElements(doc, doc.getNodes("/mxGraphModel/root/Task"), "Task");
                transformDescToCDATAForElements(doc, doc.getNodes("/mxGraphModel/root/Event"), "Event");

                result = prepareXml(doc);
            }
            catch (Exception e)
            {
                Log.log.error("Error in transformDescToCDATA:" + e.getMessage(), e);
                result = modelXml;
            }
        }
        
        return result;
    }
    
    private void transformDescToCDATAForElements(XDoc doc, List<Element> elements, String type)
    {
        if(elements != null && elements.size() > 0 && doc != null)
        {
            for (Element taskEl : elements)
            {
                String desc = taskEl.valueOf("@description");
                
                if("Task".equalsIgnoreCase(type))
                {
                    String atName = StringUtils.isNotEmpty(desc) && desc.indexOf('?') > -1 ? desc.substring(0, desc.indexOf('?')) : taskEl.valueOf("@href");
                    doc.setStringValue(taskEl, "./name", atName);
                }
    
                Map<String, String> paramsMap = transformDescToParams(desc, type);
                String inputParams = paramsMap.get("INPUTS");
                String outputParams = paramsMap.get("OUTPUTS");
    
                //adding the CDATA concatenates the value. So have to remove the element and readd it
                doc.removeElement(taskEl, "./params");
                taskEl.addElement("params");
                doc.setCDataValue(taskEl, "./params/inputs", "" + inputParams);
                doc.setCDataValue(taskEl, "./params/outputs", "" + outputParams);
            }
        }
    }
    
    
    
    /**
     * gets the description of the AT element in the model, prepares a map, converts to json string and return
     *  
     * @param desc
     * @return
     */
    private Map<String, String> transformDescToParams(String desc, String type)
    {
        Map<String, String> result = new LinkedHashMap<String, String>();
        String inputsJson = "";
        String outputsJson = "";
        
        if(StringUtils.isNotBlank(desc))
        {
            if("Task".equalsIgnoreCase(type))
            {
                if(desc.indexOf('?') > -1)
                {
                    desc = desc.substring(desc.indexOf('?')+1).trim();
                }
                else
                {
                    //do not process if the desc does not have '?' for Task as that is a Task name
                    desc = null;
                }
            }
            else
            {
                if(desc.indexOf('?') > -1)
                {
                    desc = desc.substring(desc.indexOf('?')+1).trim();
                }
            }
        }
        
        
        if(StringUtils.isNotBlank(desc))
        {
            Map<String, String> inputs = new LinkedHashMap<String, String>();
            Map<String, String> outputs = new LinkedHashMap<String, String>();
            
            try
            {
                // unescape html
                String unescapedStr = StringEscapeUtils.unescapeHtml(desc);

                // url decode
                String decodedStr = java.net.URLDecoder.decode(unescapedStr, "UTF-8");
                String[] params = decodedStr.split("&");
                for (int i = 0; i < params.length; i++)
                {
                    String name = "";
                    String value = "";

                    String nameValue = java.net.URLDecoder.decode(params[i], "UTF-8");

                    int valIdx = nameValue.indexOf("=");
                    if (valIdx <= 0)
                    {
                        name = nameValue;
                        value = "";
                    }
                    else
                    {
                        name = nameValue.substring(0, valIdx);
                        value = nameValue.substring(valIdx + 1, nameValue.length());
                    }

                    if (StringUtils.isNotEmpty(name))
                    {
                        if(name.trim().endsWith(":"))
                        {
                            outputs.put(name, value);
                        }
                        else
                        {
                            inputs.put(name, value);
                        }
                    }
                }
                
                inputsJson = JsonUtils.convertMapToJsonString(inputs);
                outputsJson = JsonUtils.convertMapToJsonString(outputs);
            }
            catch (Exception e)
            {
                Log.log.debug(e.getMessage(), e);
            }
        }
        
        result.put("INPUTS", inputsJson);
        result.put("OUTPUTS", outputsJson);
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    private String transformParamsToDesc(String jsonInputsParams, String jsonOutputsParams)
    {
        String desc = "";
        
        Map<String, String> paramsMap = new HashMap<String, String>();
        if(StringUtils.isNotEmpty(jsonInputsParams))
        {
            paramsMap.putAll(JsonUtils.convertJsonStringToMap(jsonInputsParams));
        }
        
        if(StringUtils.isNotEmpty(jsonOutputsParams))
        {
            paramsMap.putAll(JsonUtils.convertJsonStringToMap(jsonOutputsParams));
        }

        try
        {
            desc = StringUtils.encodeMapForUI(paramsMap);
        }
        catch (Exception e)
        {
            Log.log.debug(e.getMessage(), e);
        }

        return desc;
    }
    
    @JsonAnySetter
    public void anySetter(String name, Object value)
    {
        if (value != null && value instanceof Map)
        {
            @SuppressWarnings("rawtypes")
			String uDTOptionsStr = StringUtils.mapToJson((Map) value);
            Log.log.trace("anySetter(" + name + ", " + uDTOptionsStr + ")");
            
            if (name.equalsIgnoreCase("udtoptions"))
            {
                setUDTOptions(uDTOptionsStr);
            }
        }
    }
    
    @JsonAnyGetter
    public Map<String, Object> anyGetter()
    {
        Map<String, Object> propsMap = new HashMap<String, Object>();
        
        propsMap.put("udtoptions", getUDTOptions() != null ? getUDTOptions() : null);
        
        return propsMap;
    }

	public ResolveNamespaceVO getNamespace() {
		return namespace;
	}

	public void setNamespace(ResolveNamespaceVO namespace) {
		this.namespace = namespace;
	}

    public boolean isHasChildren()
    {
        return hasChildren;
    }
    public void setHasChildren(boolean hasChildren)
    {
        this.hasChildren = hasChildren;
    }
}