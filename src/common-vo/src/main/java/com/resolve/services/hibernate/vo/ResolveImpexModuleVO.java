/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

// Generated Dec 11, 2009 11:15:20 AM by Hibernate Tools 3.2.4.GA

import java.util.Collection;

import com.resolve.services.interfaces.ImpexDefinitionDTO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

public class ResolveImpexModuleVO extends VO
{
    private static final long serialVersionUID = -1139748516128744871L;
    public static final int MAX_NAME_SIZE = 333;
    public static final int MAX_ZIP_FILE_NAME_SIZE = 400;
    
    private String UName;
    private String ULocation;
    private String UDescription;
    private String UForwardWikiDocument;
    private String UScriptName;
    private String UZipFileName;
    private byte[] UZipFileContent;
    private Boolean UDirty;
    private String UVersion;
    private String excludeList;
    private String manifestGraph;
    private Boolean isNew;  // transient field to specify whether the impex definition is created in Resolve 6.3 or later (new) or otherwise.
    
    private Collection<ImpexDefinitionDTO> impexDefinition;
    
    private Collection<ResolveImpexWikiVO> resolveImpexWikis;
    private Collection<ResolveImpexGlideVO> resolveImpexGlides;
    
    public ResolveImpexModuleVO()
    {
    }

    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
    	String tmpUName = StringUtils.isNotBlank(UName) && UName.length() > MAX_NAME_SIZE ? 
    					  UName.substring(0, MAX_NAME_SIZE - 4 ) + "..." : UName;
    	
        this.UName = tmpUName;
    }

    public String getULocation()
    {
        return this.ULocation;
    }

    public void setULocation(String ULocation)
    {
        this.ULocation = ULocation;
    }

    public String getUDescription()
    {
        return this.UDescription;
    }

    public void setUDescription(String UDescription)
    {
        this.UDescription = UDescription;
    }
    
    public String getUForwardWikiDocument()
    {
        return UForwardWikiDocument;
    }

    public void setUForwardWikiDocument(String uForwardWikiDocument)
    {
        UForwardWikiDocument = uForwardWikiDocument;
    }
    
    public String getUScriptName()
    {
        return UScriptName;
    }

    public void setUScriptName(String uScriptName)
    {
        UScriptName = uScriptName;
    }

    public String getUZipFileName()
    {
        return UZipFileName;
    }

    public void setUZipFileName(String uZipFileName)
    {
    	String tmpZipFileName = StringUtils.isNotBlank(uZipFileName) && uZipFileName.length() > MAX_ZIP_FILE_NAME_SIZE ? 
    							uZipFileName.substring(0, MAX_ZIP_FILE_NAME_SIZE - 5 ) + ".zip" : uZipFileName;
    	
        UZipFileName = tmpZipFileName;
    }
    
    public Boolean getUDirty()
    {
        return this.UDirty;
    }

    public void setUDirty(Boolean dirty)
    {
        this.UDirty = dirty;
    }
    
    public String getUVersion()
    {
        return UVersion;
    }

    public void setUVersion(String version)
    {
        this.UVersion = version;
    }
    
    public String getExcludeList()
    {
        return excludeList;
    }
    public void setExcludeList(String excludeList)
    {
        this.excludeList = excludeList;
    }
    
    public String getManifestGraph()
    {
        return manifestGraph;
    }
    public void setManifestGraph(String manifestGraph)
    {
        this.manifestGraph = manifestGraph;
    }

    public Collection<ImpexDefinitionDTO> getImpexDefinition()
    {
        return impexDefinition;
    }

    public Boolean getIsNew()
    {
        return isNew;
    }
    public void setIsNew(Boolean isNew)
    {
        this.isNew = isNew;
    }

    public void setImpexDefinition(Collection<ImpexDefinitionDTO> impexDefinition)
    {
        this.impexDefinition = impexDefinition;
    }

    public byte[] getUZipFileContent()
    {
        return UZipFileContent;
    }

    public void setUZipFileContent(byte[]  uZipFileContent)
    {
        UZipFileContent = uZipFileContent;
    }


    public Collection<ResolveImpexWikiVO> getResolveImpexWikis()
    {
        return resolveImpexWikis;
    }

    public void setResolveImpexWikis(Collection<ResolveImpexWikiVO> resolveImpexWikis)
    {
        this.resolveImpexWikis = resolveImpexWikis;
    }
    public Collection<ResolveImpexGlideVO> getResolveImpexGlides()
    {
        return resolveImpexGlides;
    }

    public void setResolveImpexGlides(Collection<ResolveImpexGlideVO> resolveImpexGlides)
    {
        this.resolveImpexGlides = resolveImpexGlides;
    }

}