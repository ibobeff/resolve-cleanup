/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

// Generated Dec 11, 2009 11:15:20 AM by Hibernate Tools 3.2.4.GA

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.interfaces.CdataProperty;
import com.resolve.services.interfaces.IndexComponent;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.ATReferenceDTO;
import com.resolve.util.StringUtils;

public class ResolvePreprocessVO extends VO implements IndexComponent, CdataProperty
{
    private static final long serialVersionUID = -1467104800026266820L;

    public final static String RESOURCE_TYPE = "resolvepreprocess";

	private String UName;
	private String UScript;
	private String UDescription;

    // object referenced by
	private Collection<ResolveActionTaskVO> resolveActionTasks;
	private Collection<ResolveActionInvocVO> resolveActionInvoc;
	
	private Collection<ResolvePreprocessRelVO> resolvePreprocessRel;
	
	private Set<String> refPreprocesses = new HashSet<String>();
    private Set<ATReferenceDTO> references = new HashSet<ATReferenceDTO>();
	
//    private AccessRightsVO accessRights;

	public ResolvePreprocessVO()
	{
	    UName="";
	}

	public ResolvePreprocessVO(String sys_id)
	{
		setSys_id(sys_id);
	}
	
	public boolean validate()
    {
        boolean valid = true;

        if(StringUtils.isEmpty(getUName()))
        {
            valid = false;
            throw new RuntimeException("Name is mandatory");
        }

        valid = getUName().matches(HibernateConstants.REGEX_ACTIONTASK_FULLNAME_VALIDATION);
        if(!valid)
        {
            throw new RuntimeException("Name can have only alphanumerics,'_','#','-' and '.'");
        }

        return valid;
    }

	public String getUName()
	{
		return this.UName;
	}

	public void setUName(String UName)
	{
		this.UName = UName;
	}

	public String getUScript()
	{
		return this.UScript;
	}

	public void setUScript(String UScript)
	{
		this.UScript = UScript;
	}

	public String getUDescription()
	{
		return this.UDescription;
	}

	public void setUDescription(String UDescription)
	{
		this.UDescription = UDescription;
	}

	public Collection<ResolveActionTaskVO> getResolveActionTasks()
	{
		return resolveActionTasks;
	}

	public void setResolveActionTasks(Collection<ResolveActionTaskVO> resolveActionTasks)
	{
		this.resolveActionTasks = resolveActionTasks;
	}

	public Collection<ResolveActionInvocVO> getResolveActionInvocs()
	{
		return resolveActionInvoc;
	}

	public void setResolveActionInvocs(Collection<ResolveActionInvocVO> resolveActionInvoc)
	{
		this.resolveActionInvoc = resolveActionInvoc;
	}
	
    public Collection<ResolvePreprocessRelVO> getResolvePreprocessRel()
    {
        return resolvePreprocessRel;
    }

    public void setResolvePreprocessRel(Collection<ResolvePreprocessRelVO> resolvePreprocessRel)
    {
        this.resolvePreprocessRel = resolvePreprocessRel;
    }

    public Set<String> getRefPreprocesses()
    {
        return refPreprocesses;
    }

    public void setRefPreprocesses(Set<String> refPreprocesses)
    {
        this.refPreprocesses = refPreprocesses;
    }

    public Set<ATReferenceDTO> getReferences()
    {
        return references;
    }

    public void setReferences(Set<ATReferenceDTO> references)
    {
        this.references = references;
    }


//    public AccessRightsVO getAccessRights()
//    {
//        return accessRights;
//    }
//
//    public void setAccessRights(AccessRightsVO accessRights)
//    {
//        this.accessRights = accessRights;
//
//        // one-way only - no collections on the other side
//    }

	@Override
	public String toString()
	{
		StringBuffer str = new StringBuffer();

		str.append("Preprocessor Name:").append(UName).append("\n");
		str.append("Preprocessor Description:").append(UDescription).append("\n");
		
		return str.toString();
	}
	
    /**
     * method for getting a copy ...similar to clone()
     * 
     * @return
     */
    public ResolvePreprocessVO copy()
    {
    	ResolvePreprocessVO copy = new ResolvePreprocessVO();
    	copy.setSys_id(getSys_id());
    	copy.setSysCreatedBy(getSysCreatedBy());
    	copy.setSysCreatedOn(getSysCreatedOn());
    	copy.setSysUpdatedBy(getSysUpdatedBy());
    	copy.setSysUpdatedOn(getSysUpdatedOn());
    	
    	copy.setUDescription(UDescription);
    	copy.setUName(UName);
    	copy.setUScript(UScript);
    	
    	return copy;
    }


    public String ugetIndexContent()
    {
        return          UName
                + " " + UScript;
    }

    public List<String> ugetCdataProperty()
    {
        List<String> list = new ArrayList<String>();
        list.add("UScript");
        
        return list;
    }//ugetCdataProperty
    

}
