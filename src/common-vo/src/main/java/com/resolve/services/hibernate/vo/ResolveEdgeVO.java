/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;

import com.resolve.services.interfaces.VO;

public class ResolveEdgeVO extends VO
{
    private static final long serialVersionUID = 6324093186661515285L;
    
    private ResolveNodeVO sourceNode;
    private ResolveNodeVO destinationNode;
    private Integer UOrder;
    private String URelType;
    
    private Collection<ResolveEdgePropertiesVO> properties;

    public ResolveEdgeVO() {}
    public ResolveEdgeVO(String sysId) 
    {
        this.setSys_id(sysId);
    }

    
    public ResolveNodeVO getSourceNode()
    {
        return sourceNode;
    }

    public void setSourceNode(ResolveNodeVO sourceNode)
    {
        this.sourceNode = sourceNode;
    }

    public ResolveNodeVO getDestinationNode()
    {
        return destinationNode;
    }

    public void setDestinationNode(ResolveNodeVO destinationNode)
    {
        this.destinationNode = destinationNode;
    }

    public Integer getUOrder()
    {
        return UOrder;
    }

    public void setUOrder(Integer uOrder)
    {
        UOrder = uOrder;
    }
    
    public String getURelType()
    {
        return URelType;
    }

    public void setURelType(String uRelType)
    {
        URelType = uRelType;
    }
    
    public Collection<ResolveEdgePropertiesVO> getProperties()
    {
        return properties;
    }

    public void setProperties(Collection<ResolveEdgePropertiesVO> properties)
    {
        this.properties = properties;
    }

}
