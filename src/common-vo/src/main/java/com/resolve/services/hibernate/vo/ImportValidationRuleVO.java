/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;


public class ImportValidationRuleVO extends VO
{
    private static final long serialVersionUID = 4195974738013201581L;
    
    private String UEntityName;
    private String UDependency;
    private String UUniqueColumnNames;
    private Integer UDependencyCount;
    
    private String UTableName;
    private String UDependentTables;
    private String UUniqueTableColumnNames;
    private String foreignRefs;
    private String chidren;
    
    public ImportValidationRuleVO()
    {
    }
    
    public ImportValidationRuleVO(String tableName, String dependentTableNames, String uniqueTableColumnNames, String foreignRefs, String children)
    {
        this.setUTableName(tableName);
        this.setUDependentTables(dependentTableNames);
        this.setUUniqueTableColumnNames(uniqueTableColumnNames);
        
        this.setForeignRefs(foreignRefs);
        this.setChidren(children);
    }

    public String getUEntityName()
    {
        return UEntityName;
    }

    public void setUEntityName(String uTableName)
    {
        UEntityName = uTableName;
    }

    public String getUDependency()
    {
        return UDependency;
    }

    public void setUDependency(String uDependency)
    {
        UDependency = uDependency;
    }

    public String getUUniqueColumnNames()
    {
        return UUniqueColumnNames;
    }

    public void setUUniqueColumnNames(String uUniqueColumnNames)
    {
        UUniqueColumnNames = uUniqueColumnNames;
    }

    public Integer getUDependencyCount()
    {
        return UDependencyCount;
    }

    public void setUDependencyCount(Integer uDependencyCount)
    {
        UDependencyCount = uDependencyCount;
    }

    public String getUTableName()
    {
        return UTableName;
    }

    public void setUTableName(String uTableName)
    {
        UTableName = uTableName;
    }

    public String getUDependentTables()
    {
        return UDependentTables;
    }

    public void setUDependentTables(String uDependentTables)
    {
        UDependentTables = uDependentTables;
    }
    
    public String getUUniqueTableColumnNames()
    {
        return UUniqueTableColumnNames;
    }

    public void setUUniqueTableColumnNames(String uUniqueColumnNames)
    {
        UUniqueTableColumnNames = uUniqueColumnNames;
    }

    public String getForeignRefs()
    {
        return foreignRefs;
    }

    public void setForeignRefs(String foreignRefs)
    {
        this.foreignRefs = foreignRefs;
    }

    public String getChidren()
    {
        return chidren;
    }

    public void setChidren(String chidren)
    {
        this.chidren = chidren;
    }
}
