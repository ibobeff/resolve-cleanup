/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.math.BigDecimal;
import java.util.Set;

import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

public class SysAppModuleVO extends VO implements Comparable<SysAppModuleVO>
{
    private static final long serialVersionUID = -5006673512611966591L;
    
    private String name;
    private String title;
    private Boolean active;
    private String query;
    private String filter;
    private String viewName;
    private String windowName;
    private BigDecimal order;
    private String image;
    private String hint;
    private String appModuleGroup;
    private String linkType;

    // object references
    private SysAppApplicationVO sysAppApplication;
	
    // object referenced by
//    private List<SysAppModulerolesVO> sysAppModuleroles;
    
    //for UI
    private Set<String> roles;
	
    public SysAppModuleVO()
    {
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getTitle()
    {
        return this.title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    /*
     * @Column(name = "application", length = 32) public String getApplication()
     * { return this.application; }
     * 
     * public void setApplication(String application) { this.application =
     * application; }
     */

    public Boolean getActive()
    {
        return this.active;
    }

    public void setActive(Boolean active)
    {
        this.active = active;
    }

    public String getQuery()
    {
        return this.query;
    }

    public void setQuery(String query)
    {
        this.query = query;
    }

    public String getFilter()
    {
        return this.filter;
    }

    public void setFilter(String filter)
    {
        this.filter = filter;
    }

    public String getViewName()
    {
        return this.viewName;
    }

    public void setViewName(String viewName)
    {
        this.viewName = viewName;
    }

    public String getWindowName()
    {
        return this.windowName;
    }

    public void setWindowName(String windowName)
    {
        this.windowName = windowName;
    }

    public BigDecimal getOrder()
    {
        return this.order;
    }

    public void setOrder(BigDecimal order)
    {
        this.order = order;
    }

    public String getImage()
    {
        return this.image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    public String getHint()
    {
        return this.hint;
    }

    public void setHint(String hint)
    {
        this.hint = hint;
    }

    public String getAppModuleGroup()
    {
        return this.appModuleGroup;
    }

    public void setAppModuleGroup(String appModuleGroup)
    {
        this.appModuleGroup = appModuleGroup;
    }

    public String getLinkType()
    {
        return this.linkType;
    }

    public void setLinkType(String linkType)
    {
        this.linkType = linkType;
    }

    public SysAppApplicationVO getSysAppApplication()
    {
        return sysAppApplication;
    }

    public void setSysAppApplication(SysAppApplicationVO sysAppApplication)
    {
        this.sysAppApplication = sysAppApplication;
        
//        if (sysAppApplication != null)
//        {
//            Collection<SysAppModuleVO> coll = sysAppApplication.getSysAppModules();
//            if (coll == null)
//            {
//				coll = new HashSet<SysAppModuleVO>();
//	            coll.add(this);
//	            
//                sysAppApplication.setSysAppModules(coll);
//            }
//        }
    }

//    public List<SysAppModulerolesVO> getSysAppModuleroles()
//    {
//        return this.sysAppModuleroles;
//    }
//
//    public void setSysAppModuleroles(List<SysAppModulerolesVO> sysAppModuleroles)
//    {
//        this.sysAppModuleroles = sysAppModuleroles;
//    }
    
    public Set<String> getRoles()
    {
        return roles;
    }

    public void setRoles(Set<String> roles)
    {
        this.roles = roles;
    }

    public int compareTo(SysAppModuleVO that)
    {
        final int EQUAL = 0;
        final int BEFORE = -1;
        final int AFTER = 1;
        int result = EQUAL;

        //this optimization is usually worthwhile
        if (this == that)
            result = EQUAL;
        else
        {
            long orderThis = this.getOrder() != null ? this.getOrder().longValue() : 0;
            long orderThat = that.getOrder() != null ? that.getOrder().longValue() : 0;

            if (orderThis > orderThat)
            {
                result = AFTER;
            }
            else
            {
                result = BEFORE;
            }
        }

        return result;
    }
    
    
    @Override
    public boolean equals(Object aThat)
    {
        boolean result = false;
        
        if (this == aThat) 
            result = true;
        else if (!(aThat instanceof SysAppModuleVO)) 
            result = false;
        else 
        {
            SysAppModuleVO that = (SysAppModuleVO) aThat;
            
            if ((StringUtils.isNotBlank(this.query) && StringUtils.isBlank(that.query)) ||
                (StringUtils.isBlank(this.query) && StringUtils.isNotBlank(that.query)))
            {
                return result;
            }
            
            if (!this.query.equals(that.query))
            {
                return result;
            }
            
            if ((StringUtils.isNotBlank(this.title) && StringUtils.isBlank(that.title)) ||
                (StringUtils.isBlank(this.title) && StringUtils.isNotBlank(that.title)))
            {
                return result;
            }
            
            if (!this.title.equals(that.title))
            {
                return result;
            }
            
            if ((this.order != null && that.order == null) || (this.order == null && that.order != null))
            {
                return result;
            }
            
            result = this.order.equals(that.order);
        }
        
        return result;
    }

    @Override
    public int hashCode()
    {
        final int prime = 37;
        int result = 43;

        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());

        return result;
    }
}
