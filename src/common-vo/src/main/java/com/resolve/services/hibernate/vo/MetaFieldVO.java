/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;


public class MetaFieldVO  extends VO
{   
    private static final long serialVersionUID = -7221159225628486238L;

    public final static String RESOURCE_TYPE = "metafield";
    
    private String UName;
    private String UTable;
    private String UColumn;
    private String UColumnModelName;
    private String UDisplayName;
    private String UType;
    private String UDbType;
    //private Boolean UIsDefaultRole;
    
    // object referenced
    private CustomTableVO customTable;
    private MetaAccessRightsVO metaAccessRights;
    private MetaFieldPropertiesVO metaFieldProperties;
    
    public MetaFieldVO()
    {
    }
    
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    public String getUTable()
    {
        return this.UTable;
    }

    public void setUTable(String UTable)
    {
        this.UTable = UTable;
    }
    
    public String getUColumn()
    {
        return this.UColumn;
    }

    public void setUColumn(String UColumn)
    {
        this.UColumn = UColumn;
    }
    
    public String getUColumnModelName()
    {
        return this.UColumnModelName;
    }

    public void setUColumnModelName(String UColumnModelName)
    {
        this.UColumnModelName = UColumnModelName;
    }
    
    public String getUDisplayName()
    {
        return this.UDisplayName;
    }

    public void setUDisplayName(String UDisplayName)
    {
        this.UDisplayName = UDisplayName;
    }
    
    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }
    
    public String getUDbType()
    {
        return this.UDbType;
    }

    public void setUDbType(String UDbType)
    {
        this.UDbType = UDbType;
    }
       
    public CustomTableVO getCustomTable()
    {
        return customTable;
    }

    public void setCustomTable(CustomTableVO customTable)
    {
        this.customTable = customTable;

        if (customTable != null)
        {
            Collection<MetaFieldVO> coll = customTable.getMetaFields();
            if (coll == null)
            {
                coll = new HashSet<MetaFieldVO>();
                coll.add(this);
                
                customTable.setMetaFields(coll);
            }
        }
    }
    
    public MetaAccessRightsVO getMetaAccessRights()
    {
        return metaAccessRights;
    }

    public void setMetaAccessRights(MetaAccessRightsVO metaAccessRights)
    {
        this.metaAccessRights = metaAccessRights;
    }

    public MetaFieldPropertiesVO getMetaFieldProperties()
    {
        return this.metaFieldProperties;
    }

    public void setMetaFieldProperties(MetaFieldPropertiesVO metaFieldProperties)
    {
        this.metaFieldProperties = metaFieldProperties;
    }
    
}
