/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class CatalogAttachmentVO extends VO
{
    
    private static final long serialVersionUID = 1505394787791956320L;
    
    private String filename;
    private String displayname;
    private String type;
    private String location;
    private Integer u_size;
    private byte[] content;

    
    public CatalogAttachmentVO() {}

    public String getFilename()
    {
        return filename;
    }
    public void setFilename(String fileName)
    {
        this.filename = fileName;
    }
    
    public String getDisplayname()
    {
        return displayname;
    }
    public void setDisplayname(String displayName)
    {
        this.displayname = displayName;
    }
    
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    
    public String getLocation()
    {
        return location;
    }
    public void setLocation(String location)
    {
        this.location = location;
    }
    
    public Integer getU_size()
    {
        return u_size;
    }
    public void setU_size(Integer size)
    {
        this.u_size = size;
    }
    
    public byte[] getContent()
    {
        return content;
    }
    public void setContent(byte[] content)
    {
        this.content = content;
    }
    
}
