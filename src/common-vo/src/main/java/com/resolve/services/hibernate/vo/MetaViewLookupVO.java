/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;


public class MetaViewLookupVO extends VO
{
    private static final long serialVersionUID = 8200613209521967230L;
    
    private String UAppName;
    private String UViewName;
    private String URoles;
    private Integer UOrder;
    
    
    public MetaViewLookupVO()
    {
    }
    

    public String getUAppName()
    {
        return UAppName;
    }

    public void setUAppName(String uAppName)
    {
        UAppName = uAppName;
    }

    public String getUViewName()
    {
        return UViewName;
    }

    public void setUViewName(String uViewName)
    {
        UViewName = uViewName;
    }

    public String getURoles()
    {
        return URoles;
    }

    public void setURoles(String uRoles)
    {
        URoles = uRoles;
    }

    public Integer getUOrder()
    {
        return UOrder;
    }

    public void setUOrder(Integer uOrder)
    {
        UOrder = uOrder;
    }

    
    
}
