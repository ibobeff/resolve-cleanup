package com.resolve.services.hibernate.vo;

import java.util.List;

import com.resolve.services.interfaces.VO;

public class SavedSearchVO extends VO {
	private static final long serialVersionUID = 6961598854428409837L;
	private String name;
	private List<SavedQueryFilterVO> filters;
	private List<SavedQuerySortVO> sorts;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SavedQueryFilterVO> getFilters() {
		return filters;
	}

	public void setFilters(List<SavedQueryFilterVO> filters) {
		this.filters = filters;
	}

	public List<SavedQuerySortVO> getSorts() {
		return sorts;
	}

	public void setSorts(List<SavedQuerySortVO> sorts) {
		this.sorts = sorts;
	}
}
