/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.lang3.BooleanUtils;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.resolve.enums.ColorTheme;
import com.resolve.services.interfaces.VO;
import com.resolve.util.EncryptUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstants;

public class UsersVO extends VO implements Comparable<UsersVO>
{
    private static final long serialVersionUID = -7362013654130301368L;

    public static Map<String, String> uiToBackEndPropNameMap;
    
	private String UUserName;
	private String UUserP_assword;
	private Boolean UPasswordNeedsReset;
	private String UPasswordHistory;
	private String UPreferredLanguage;
	private String UIntroduction;
	private String UFirstName;
	private String UMiddleName;
	private String ULastName;
	private String UName;
	private String UPhone;
	private String UHomePhone;
	private String UMobilePhone;
	private String UFax;
	private String UEmail;
	private String UIM;
	private String UTitle;
	private String UGender;
	private String UTimeZone;
	private String UDateFormat;
	private String UDateTimeFormat;
	private String UTimeFormat;
	private String ULastPassword;
	private Date ULastLogin;
	private Boolean ULockedOut;
	private String ULastLoginDevice;
	private String UManager;
	private Integer UNotification;
	private Integer UCalendarIntegration;
	private String UPhoto;
	private String UDefaultPerspective;
	private String ULocation;
	private String UDepartment;
	private String UEduStatus;
	private String UCompany;
	private String USource;
	private String UStreet;
	private String UAddress1;
	private String UAddress2;
	private String UCity;
	private String UState;
	private String UZip;
	private String UCountry;
	private String UEmployeeNumber;
	private Boolean UVip;
	private String UBuilding;
	private Integer UFailedAttempts;
	private String USummary;
	private byte[] UImage;
	private byte[] UTempImage;
	private String UStartPage;
	private String UHomePage;
	private String userSettings;
	//Password Recovery
    private String UQuestion1;
    private String UAnswer1;
    private String UQuestion2;
    private String UAnswer2;
    private String UQuestion3;
    private String UAnswer3;

    // object referenced by
	private Collection<UserRoleRelVO> userRoleRels;
	private Collection<UserGroupRelVO> userGroupRels;
	private List<UserPreferencesVO> socialPreferences;

	//for display purpose only
	private String roles;//used for VO - not a persistent attribute
	private String groups;
//	private List<UserPreferencesVO> socialPreferencesList ; // Not a persistent attribute

	private List<GroupsVO> userGroups;
	private List<RolesVO> userRoles;
	private SortedSet<OrgsVO> userOrgs;
	
	private String cloneUsername;

	private OrganizationVO belongsToOrganization;

    private String UUserSaltedPasswordHash;
    private String UUserSaltedPasswordHashHistory;
    
    private String analyticSettings;
    private Integer refreshRate;
    private ColorTheme colorTheme;
    
    private Set<String> accessibleOrgIds;
    private boolean hasNoOrgAccess;
    
    static
    {
        Map<String, String> unsafe2SafePropNames = new HashMap<String, String>();
        
        unsafe2SafePropNames.put("UUserPassword", "UUserP_assword");
        
        uiToBackEndPropNameMap = VO.createUI2BackEndPropNameMapping(unsafe2SafePropNames);
    }
    
	public UsersVO()
	{
	}

	public boolean validate()
    {
        boolean valid = true;

        if(StringUtils.isEmpty(getUUserName()) || (StringUtils.isNotBlank(getUUserName()) && getUUserName().equalsIgnoreCase(VO.STRING_DEFAULT)))
        {
            valid = false;
            throw new RuntimeException("Username cannot be empty.");
        }
        
        //make sure that the username has no spaces, make it lowercase
        setUUserName(getUUserName().trim().toLowerCase());

//        valid = getUUserName().matches("[\\p{L}\\p{Nd}!#$%&'*+-/=?^_`{|}~.(),:;<>@]*");
        valid = getUUserName().matches(HibernateConstants.REGEX_USERNAME_VALIDATION);
        if(!valid)
        {
            throw new RuntimeException("Username contain some invalid foreign characters. Please change the Username.");
        }
        
        if(StringUtils.isNotBlank(getUFirstName()))
        {
            valid = getUFirstName().matches(HibernateConstants.REGEX_USER_FIRST_LAST_NAME);
            if(!valid)
            {
                throw new RuntimeException("First Name has some invalid character. Please change the First Name");
            }
        }

        if(StringUtils.isNotBlank(getULastName()))
        {
            valid = getULastName().matches(HibernateConstants.REGEX_USER_FIRST_LAST_NAME);
            if(!valid)
            {
                throw new RuntimeException("Last Name has some invalid character. Please change the Last Name");
            }
        }
        
        if (StringUtils.isNotBlank(getUSummary()) && getUSummary().length() > 4000)
        {
            throw new RuntimeException("Summary cannot exceed 4000 characters (text and markup inclusive).");
        }

        return valid;
    }

	public String toString()
	{
	    return this.UUserName;
	} // toString

	public UsersVO usetsys_id(String sys_id)
	{
		this.setSys_id(sys_id);
		return this;
	}

	public String getUUserName()
	{
		return this.UUserName;
	}

	public void setUUserName(String UUserName)
	{
		this.UUserName = UUserName;
	}

	public String getUUserP_assword()
	{
		return this.UUserP_assword;
	}

	public void setUUserP_assword(String UUserPassword)
	{
		this.UUserP_assword = UUserPassword;
	}

	public void usetPassword(String password)
	{
	    if (!StringUtils.isEmpty(password) && password.charAt(password.length()-1) != '=')
	    {
	        String submitPas_sword = null;
	        
	        if (this.getUUserSaltedPasswordHash() != null && !this.getUUserSaltedPasswordHash().equalsIgnoreCase(VO.STRING_DEFAULT))
	        {
	            submitPas_sword = password;
	        }
	        else
	        {
	            submitPas_sword = EncryptUtil.encryptPassword(password);
	        }
	        
		    this.UUserP_assword = submitPas_sword;
	    }
	} // usetPassord

	public Boolean getUPasswordNeedsReset()
	{
		return this.UPasswordNeedsReset;
	}

	public void setUPasswordNeedsReset(Boolean UPasswordNeedsReset)
	{
		this.UPasswordNeedsReset = UPasswordNeedsReset;
	}

    public Boolean ugetUPasswordNeedsReset()
    {
    	return BooleanUtils.toBoolean(this.UPasswordNeedsReset);
    } // ugetUPasswordNeedsReset

    public String ugetDisplayName()
    {
        String result = "";

        if (this.UFirstName != null)
        {
            result += this.UFirstName;
        }
        if (this.ULastName != null)
        {
            result += " "+this.ULastName;
        }
        result.trim();
        return result;
    } // ugetDisplayName

	public String getUPreferredLanguage()
	{
		return this.UPreferredLanguage;
	}

	public void setUPreferredLanguage(String UPreferredLanguage)
	{
		this.UPreferredLanguage = UPreferredLanguage;
	}

	public String getUIntroduction()
	{
		return this.UIntroduction;
	}

	public void setUIntroduction(String UIntroduction)
	{
		this.UIntroduction = UIntroduction;
	}

	public String getUFirstName()
	{
		return this.UFirstName;
	}

	public void setUFirstName(String UFirstName)
	{
		this.UFirstName = UFirstName;
	}

	public String getUMiddleName()
	{
		return this.UMiddleName;
	}

	public void setUMiddleName(String UMiddleName)
	{
		this.UMiddleName = UMiddleName;
	}

	public String getULastName()
	{
		return this.ULastName;
	}

	public void setULastName(String ULastName)
	{
		this.ULastName = ULastName;
	}

	public String getUName()
	{
		return this.UName;
	}

	public void setUName(String UName)
	{
		this.UName = UName;
	}

	public String getUPhone()
	{
		return this.UPhone;
	}

	public void setUPhone(String UPhone)
	{
		this.UPhone = UPhone;
	}

	public String getUHomePhone()
	{
		return this.UHomePhone;
	}

	public void setUHomePhone(String UHomePhone)
	{
		this.UHomePhone = UHomePhone;
	}

	public String getUMobilePhone()
	{
		return this.UMobilePhone;
	}

	public void setUMobilePhone(String UMobilePhone)
	{
		this.UMobilePhone = UMobilePhone;
	}

	public String getUFax()
    {
        return this.UFax;
    }

    public void setUFax(String UFax)
    {
        this.UFax = UFax;
    }

	public String getUEmail()
	{
		return this.UEmail;
	}

	public void setUEmail(String UEmail)
	{
		this.UEmail = UEmail;
	}

    public String getUIM()
    {
        return this.UIM;
    }

    public void setUIM(String IM)
    {
        this.UIM = IM;
    }

	public String getUTitle()
	{
		return this.UTitle;
	}

	public void setUTitle(String UTitle)
	{
		this.UTitle = UTitle;
	}

	public String getUGender()
	{
		return this.UGender;
	}

	public void setUGender(String UGender)
	{
		this.UGender = UGender;
	}

	public String getUTimeZone()
	{
		return this.UTimeZone;
	}

	public void setUTimeZone(String UTimeZone)
	{
		this.UTimeZone = UTimeZone;
	}

	public String getUDateFormat()
	{
		return this.UDateFormat;
	}

	public void setUDateFormat(String UDateFormat)
	{
		this.UDateFormat = UDateFormat;
	}

	public String getUDateTimeFormat()
	{
		return this.UDateTimeFormat;
	}

	public void setUDateTimeFormat(String UDateTimeFormat)
	{
		this.UDateTimeFormat = UDateTimeFormat;
	}

	public String getUTimeFormat()
	{
		return this.UTimeFormat;
	}

	public void setUTimeFormat(String UTimeFormat)
	{
		this.UTimeFormat = UTimeFormat;
	}

	public String getUPasswordHistory()
	{
		return this.UPasswordHistory;
	}

	public void setUPasswordHistory(String UPasswordHistory)
	{
		this.UPasswordHistory = UPasswordHistory;
	}

	public String getULastPassword()
	{
		return this.ULastPassword;
	}

	public void setULastPassword(String ULastPassword)
	{
		this.ULastPassword = ULastPassword;
	}

	public Date getULastLogin()
	{
		return this.ULastLogin;
	}

	public void setULastLogin(Date ULastLogin)
	{
		this.ULastLogin = ULastLogin;
	}

	public Boolean getULockedOut()
	{
		return this.ULockedOut;
	}

	public void setULockedOut(Boolean ULockedOut)
	{
		this.ULockedOut = ULockedOut;
	}

	public Boolean ugetULockedOut()
    {
		return BooleanUtils.toBoolean(this.ULockedOut);
    } // ugetULockedOut
	
	public String getULastLoginDevice()
	{
		return this.ULastLoginDevice;
	}

	public void setULastLoginDevice(String ULastLoginDevice)
	{
		this.ULastLoginDevice = ULastLoginDevice;
	}

	public String getUManager()
	{
		return this.UManager;
	}

	public void setUManager(String UManager)
	{
		this.UManager = UManager;
	}

	public Integer getUNotification()
	{
		return this.UNotification;
	}

	public void setUNotification(Integer UNotification)
	{
		this.UNotification = UNotification;
	}

	public Integer getUCalendarIntegration()
	{
		return this.UCalendarIntegration;
	}

	public void setUCalendarIntegration(Integer UCalendarIntegration)
	{
		this.UCalendarIntegration = UCalendarIntegration;
	}

	public String getUPhoto()
	{
		return this.UPhoto;
	}

	public void setUPhoto(String UPhoto)
	{
		this.UPhoto = UPhoto;
	}

	public String getUDefaultPerspective()
	{
		return this.UDefaultPerspective;
	}

	public void setUDefaultPerspective(String UDefaultPerspective)
	{
		this.UDefaultPerspective = UDefaultPerspective;
	}

	public String getULocation()
	{
		return this.ULocation;
	}

	public void setULocation(String ULocation)
	{
		this.ULocation = ULocation;
	}

	public String getUDepartment()
	{
		return this.UDepartment;
	}

	public void setUDepartment(String UDepartment)
	{
		this.UDepartment = UDepartment;
	}

	public String getUEduStatus()
	{
		return this.UEduStatus;
	}

	public void setUEduStatus(String UEduStatus)
	{
		this.UEduStatus = UEduStatus;
	}

	public String getUCompany()
	{
		return this.UCompany;
	}

	public void setUCompany(String UCompany)
	{
		this.UCompany = UCompany;
	}

	public String getUSource()
	{
		return this.USource;
	}

	public void setUSource(String USource)
	{
		this.USource = USource;
	}

	public String getUStreet()
	{
		return this.UStreet;
	}

	public void setUStreet(String UStreet)
	{
		this.UStreet = UStreet;
	}

    public String getUAddress1()
    {
        return this.UAddress1;
    }

    public void setUAddress1(String UAddress1)
    {
        this.UAddress1 = UAddress1;
    }

    public String getUAddress2()
    {
        return this.UAddress2;
    }

    public void setUAddress2(String UAddress2)
    {
        this.UAddress2 = UAddress2;
    }

	public String getUCity()
	{
		return this.UCity;
	}

	public void setUCity(String UCity)
	{
		this.UCity = UCity;
	}

	public String getUState()
	{
		return this.UState;
	}

	public void setUState(String UState)
	{
		this.UState = UState;
	}

	public String getUZip()
	{
		return this.UZip;
	}

	public void setUZip(String UZip)
	{
		this.UZip = UZip;
	}

	public String getUCountry()
    {
        return this.UCountry;
    }

    public void setUCountry(String UCountry)
    {
        this.UCountry = UCountry;
    }

	public String getUEmployeeNumber()
	{
		return this.UEmployeeNumber;
	}

	public void setUEmployeeNumber(String UEmployeeNumber)
	{
		this.UEmployeeNumber = UEmployeeNumber;
	}

	public Boolean getUVip()
	{
		return this.UVip;
	}

	public void setUVip(Boolean UVip)
	{
		this.UVip = UVip;
	}

	public String getUBuilding()
	{
		return this.UBuilding;
	}

	public void setUBuilding(String UBuilding)
	{
		this.UBuilding = UBuilding;
	}

	public Integer getUFailedAttempts()
	{
		return this.UFailedAttempts;
	}

	public void setUFailedAttempts(Integer UFailedAttempts)
	{
		this.UFailedAttempts = UFailedAttempts;
	}

    public String getUSummary()
    {
        return this.USummary;
    }

    public void setUSummary(String summary)
    {
        this.USummary = summary;
    }

    public byte[] getUImage()
    {
        return UImage;
    }

    public void setUImage(byte[] UImage)
    {
        this.UImage = UImage;
    }

    public byte[] getUTempImage()
    {
        return UTempImage;
    }

    public void setUTempImage(byte[] UTempImage)
    {
        this.UTempImage = UTempImage;
    }

    public String getUQuestion1()
    {
        return UQuestion1;
    }

    public void setUQuestion1(String uQuestion1)
    {
        UQuestion1 = uQuestion1;
    }

    public String getUAnswer1()
    {
        return UAnswer1;
    }

    public void setUAnswer1(String uAnswer1)
    {
        UAnswer1 = uAnswer1;
    }

    public String getUQuestion2()
    {
        return UQuestion2;
    }

    public void setUQuestion2(String uQuestion2)
    {
        UQuestion2 = uQuestion2;
    }

    public String getUAnswer2()
    {
        return UAnswer2;
    }

    public void setUAnswer2(String uAnswer2)
    {
        UAnswer2 = uAnswer2;
    }

    public String getUQuestion3()
    {
        return UQuestion3;
    }

    public void setUQuestion3(String uQuestion3)
    {
        UQuestion3 = uQuestion3;
    }

    public String getUAnswer3()
    {
        return UAnswer3;
    }

    public void setUAnswer3(String uAnswer3)
    {
        UAnswer3 = uAnswer3;
    }


	public Collection<UserRoleRelVO> getUserRoleRels()
	{
		return userRoleRels;
	}

	public void setUserRoleRels(Collection<UserRoleRelVO> userRoleRels)
	{
		this.userRoleRels = userRoleRels;
	}

	public Collection<UserGroupRelVO> getUserGroupRels()
	{
		return userGroupRels;
	}

	public void setUserGroupRels(Collection<UserGroupRelVO> userGroupRels)
	{
		this.userGroupRels = userGroupRels;
	}

    public List<UserPreferencesVO> getSocialPreferences()
    {
        return socialPreferences;
    }

    public void setSocialPreferences(List<UserPreferencesVO> pref)
    {
        this.socialPreferences = pref;
    }


	public String getRoles()
    {
        return roles;
    }

    public void setRoles(String roles)
    {
        this.roles = roles;
    }

    public String getGroups()
    {
        return groups;
    }

    public void setGroups(String groups)
    {
        this.groups = groups;
    }

    @Override
    public int compareTo(UsersVO o)
    {
        if (o instanceof UsersVO)
        {
            UsersVO compareToUser = (UsersVO)o;
            return this.UUserName.compareToIgnoreCase(compareToUser.UUserName);
        }
        return 0;
    }

    @Override
    public boolean equals(Object obj)
    {
    	boolean flag = false;
    	
    	if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (!(obj instanceof UsersVO))
        {
            return false;
        }
    	
    	UsersVO vo = (UsersVO) obj;
    	if (this.getUUserName().equals(vo.getUUserName()))
    	{
    		flag = true;
    	}
    	
    	return flag;
    }
    
    @Override
    public int hashCode()
    {
    	int hashcode = 1;
        hashcode = 31 * hashcode + ((this.getId() == null) ? 0 : this.getId().hashCode());
        return hashcode;
    }

    public OrganizationVO getBelongsToOrganization()
    {
        return belongsToOrganization;
    }

    public void setBelongsToOrganization(OrganizationVO uBelongsToOrganization)
    {
        belongsToOrganization = uBelongsToOrganization;
    }

    public List<GroupsVO> getUserGroups()
    {
        return userGroups;
    }

    public void setUserGroups(List<GroupsVO> userGroups)
    {
        this.userGroups = userGroups;
    }

    public List<RolesVO> getUserRoles()
    {
        return userRoles;
    }

    public void setUserRoles(List<RolesVO> userRoles)
    {
        this.userRoles = userRoles;
    }
    
    public SortedSet<OrgsVO> getUserOrgs()
    {
        return userOrgs;
    }

    public void setUserOrgs(SortedSet<OrgsVO> userOrgs)
    {
        if (userOrgs != null && !userOrgs.isEmpty())
        {
            OrgsVO[] urserOrgsArray = userOrgs.toArray(new OrgsVO[1]);
            
            boolean addNoneOrg = false;
            boolean foundNoneOrg = false;
            SortedSet<OrgsVO> tmpUserOrgs = new TreeSet<OrgsVO>();
            boolean hasDefaultOrg = false;
            
            for (OrgsVO userOrgsVO : urserOrgsArray)
            {
                if (userOrgsVO.getUHasNoOrgAccess().booleanValue())
                {
                    if (!userOrgsVO.getUName().equalsIgnoreCase(OrgsVO.NONE_ORG_NAME) && !addNoneOrg && !foundNoneOrg)
                    {
                        addNoneOrg = true;
                    }
                }
                
                if (userOrgsVO.getUName().equalsIgnoreCase(OrgsVO.NONE_ORG_NAME))
                {
                    foundNoneOrg = true;
                }
                
                tmpUserOrgs.add(userOrgsVO);
                
                if (!hasDefaultOrg)
                {
                    if (userOrgsVO.getUName().equalsIgnoreCase(OrgsVO.NONE_ORG_NAME))
                    {
                        hasDefaultOrg = userOrgsVO.getIsDefaultOrg();
                    }
                    else
                    {
                        hasDefaultOrg = userOrgsVO.getSys_id().equals(this.getSysOrg());
                    }
                }
                
                Log.log.trace(userOrgsVO.getUName() + " : " + userOrgsVO.hashCode() + " added to tmpUserOrgs new size is " + tmpUserOrgs.size());
            }
            
            if (addNoneOrg && !foundNoneOrg)
            {
                tmpUserOrgs.add(OrgsVO.getNoneOrgsVO(!hasDefaultOrg));
            }
            
            this.userOrgs = tmpUserOrgs;
        }
        else
        {
            this.userOrgs = userOrgs;
        }
    }
    
    public String getUStartPage()
    {
        return UStartPage;
    }

    public void setUStartPage(String uStartPage)
    {
        UStartPage = uStartPage;
    }
    @JsonIgnore
    public String getUserSettings()
    {
        return userSettings;
    }
    
    public void setUserSettings(String userSettings)
    {
        this.userSettings = userSettings;
    }
    public String getUHomePage()
    {
        return UHomePage;
    }

    public void setUHomePage(String uHomePage)
    {
        UHomePage = uHomePage;
    }

    /**
     * Convenient method for getting the display name of this user.
     * Here is the order of precedence:
     * 
     * 1. First and Last name.
     * 2. Name
     * 3. Username (Login Id)
     * 
     * @return
     */
    public String getUDisplayName()
    {
        String result = null;
        if(StringUtils.isNotBlank(UFirstName) && StringUtils.isNotBlank(ULastName))
        {
            String middleName = (UMiddleName == null ? "" : UMiddleName.trim() + " ");
            result = UFirstName.trim() + " " + middleName + ULastName.trim();
        }
        else
        {
            if(StringUtils.isNotBlank(UName))
            {
                result = UName;
            }
            else
            {
                result = UUserName;
            }
        }
        return result;
    }
    
    public void setUDisplayName(String displayName)
    {
    	/*
    	 * Dummy setter, just to make UI happy.
    	 * Needed so that if UI sends back the same json back, JsonParser will not throw an exception as 'uDisplayName' is not a model property.
    	 */
    }

    public String getCloneUsername()
    {
        return cloneUsername;
    }

    public void setCloneUsername(String cloneUsername)
    {
        this.cloneUsername = cloneUsername;
    }
    
    public String getUUserSaltedPasswordHash()
    {
        return this.UUserSaltedPasswordHash;
    }

    public void setUUserSaltedPasswordHash(String UUserSaltedPasswordHash)
    {
        this.UUserSaltedPasswordHash = UUserSaltedPasswordHash;
    }
    
    public String getUUserSaltedPasswordHashHistory()
    {
        return this.UUserSaltedPasswordHashHistory;
    }

    public void setUUserSaltedPasswordHashHistory(String UUserSaltedPasswordHashHistory)
    {
        this.UUserSaltedPasswordHashHistory = UUserSaltedPasswordHashHistory;
    }
    
    @JsonAnySetter
    public void anySetter(String name, Object value)
    {
        if (value != null && value instanceof Map)
        {
            String userSettingsStr = StringUtils.mapToJson((Map)value);
            Log.log.trace("anySetter(" + name + ", " + userSettingsStr + ")");
            
            if (name.equalsIgnoreCase("usersettings"))
            {
                setUserSettings(userSettingsStr);
            }
        }
    }
    
    @JsonAnyGetter
    public Map<String, Object> anyGetter()
    {
        Map<String, Object> propsMap = new HashMap<String, Object>();
        
        propsMap.put("userSettings", getUserSettings() != null ? 
                     /*StringUtils.jsonObjectToMap((StringUtils.stringToJSONObject(*/getUserSettings()/*)))*/ : null);
        
        return propsMap;
    }
    
    public static String getBackEndJSON(String uiJSONString)
    {
        return getBackEndJSON(uiJSONString, uiToBackEndPropNameMap);
    } 
    private static class UserSettings
    {
        private boolean autoRefreshEnabled;

        public boolean isAutoRefreshEnabled()
        {
            return autoRefreshEnabled;
        }

        public void setAutoRefreshEnabled(boolean autoRefreshEnabled)
        {
            this.autoRefreshEnabled = autoRefreshEnabled;
        }
    }
    
    public String getAnalyticSettings() {
        return analyticSettings;
    }

    public void setAnalyticSettings(String analyticSettings) {
        this.analyticSettings = analyticSettings;
    }

    public Integer getRefreshRate() {
        return refreshRate;
    }

    public void setRefreshRate(Integer refreshRate) {
        this.refreshRate = refreshRate;
    }

    public ColorTheme getColorTheme() {
        return colorTheme;
    }

    public void setColorTheme(ColorTheme colorTheme) {
        this.colorTheme = colorTheme;
    }
    
    public Set<String> getAccessibleOrgIds() {
    	return accessibleOrgIds;
    }
    
    public void setAccessibleOrgIds(Set<String> accessibleOrgIds) {
    	this.accessibleOrgIds = accessibleOrgIds;
    }
    
    public boolean isOrgIdAccessible(String orgId) {
    	return StringUtils.isNotBlank(orgId) && accessibleOrgIds != null ? accessibleOrgIds.contains(orgId) : false;
    }
    
    public boolean getHasNoOrgAccess() {
    	return hasNoOrgAccess;
    }
    
    public void setHasNoOrgAccess(boolean hasNoOrgAccess) {
    	this.hasNoOrgAccess = hasNoOrgAccess;
    }
} // Users
