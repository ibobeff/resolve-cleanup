/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;


import com.resolve.services.interfaces.VO;


public class ConfigActiveDirectoryVO extends VO
{
    private static final long serialVersionUID = -4884267427740738257L;
    
    public final static String AUTH_MODE_BIND = "BIND";            // uses CN=username,...
    public final static String AUTH_MODE_USERNAME = "USERNAME";    // uses <uidAttribute>=username, no password validation
    
    private Boolean UActive;
    private Integer UVersion;
    private String UIpAddress;
    private Integer UPort;
    private Boolean USsl;
    private Boolean groupRequired;
    private String UDefaultDomain;    
    private String UUidAttribute;
    private String UMode;
    private String UBindDn;
    private String UBindPassword;
    private Boolean UFallback;
    private String UDomain;// this is mandatory from the UI and should be unique
    private String UBaseDNList;//semi-colon separated string
    private String UProperties;//data that is in the ldap.properties file
    private Boolean UIsDefault;
    private String UGateway;
    
    //active directory conf belongs to this organization
    private OrganizationVO belongsToOrganization;

    public ConfigActiveDirectoryVO()
    {
    }
    
    public Integer getUPort()
    {
        return UPort;
    }

    public void setUPort(Integer uPort)
    {
        UPort = uPort;
    }

    public Boolean getUActive()
    {
        return UActive;
    }
    
    public void setUActive(Boolean uActive)
    {
        UActive = uActive;
    }

    public String getUDomain()
    {
        return UDomain;
    }

    public void setUDomain(String uDomain)
    {
        UDomain = uDomain;
    }

    public String getUIpAddress()
    {
        return UIpAddress;
    }

    public void setUIpAddress(String uIpAddress)
    {
        UIpAddress = uIpAddress;
    }
    
    public Boolean getUSsl()
    {
        return USsl;
    }
    
    public void setUSsl(Boolean uSsl)
    {
        USsl = uSsl;
    }

    public Integer getUVersion()
    {
        return UVersion;
    }

    public void setUVersion(Integer uVersion)
    {
        UVersion = uVersion;
    }

    public Boolean getUFallback()
    {
        return UFallback;
    }

    public void setUFallback(Boolean uFallback)
    {
        UFallback = uFallback;
    }
    
    public String getUMode()
    {
        return UMode;
    }

    public void setUMode(String uMode)
    {
        UMode = uMode;
    }
    
    public String getUBindDn()
    {
        return UBindDn;
    }

    public void setUBindDn(String uBindDn)
    {
        UBindDn = uBindDn;
    }

    public String getUBindPassword()
    {
        return UBindPassword;
    }

    public void setUBindPassword(String uBindPassword)
    {
        UBindPassword = uBindPassword;
    }

    public String getUBaseDNList()
    {
        return UBaseDNList;
    }

    public void setUBaseDNList(String uBaseDNList)
    {
        UBaseDNList = uBaseDNList;
    }
    
    public String getUUidAttribute()
    {
        return UUidAttribute;
    }

    public void setUUidAttribute(String uUidAttribute)
    {
        UUidAttribute = uUidAttribute;
    }

    public OrganizationVO getBelongsToOrganization()
    {
        return belongsToOrganization;
    }

    public void setBelongsToOrganization(OrganizationVO belongsToOrganization)
    {
        this.belongsToOrganization = belongsToOrganization;
    }
    
    public String getUDefaultDomain()
    {
        return UDefaultDomain;
    }

    public void setUDefaultDomain(String uDefaultDomain)
    {
        UDefaultDomain = uDefaultDomain;
    }
    public String getUProperties()
    {
        return UProperties;
    }

    public void setUProperties(String uLdapProperties)
    {
        UProperties = uLdapProperties;
    }

    public Boolean getUIsDefault()
    {
        return UIsDefault;
    }

    public void setUIsDefault(Boolean uIsDefault)
    {
        UIsDefault = uIsDefault;
    }
    public String getUGateway()
    {
        return UGateway;
    }

    public void setUGateway(String uGateway)
    {
        UGateway = uGateway;
    }
    
    public Boolean getGroupRequired()
    {
        return groupRequired;
    }

    public void setGroupRequired(Boolean groupRequired)
    {
        this.groupRequired = groupRequired;
    }

}
