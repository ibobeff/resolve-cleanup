package com.resolve.services.hibernate.vo;

import java.util.Date;
import java.util.Set;

import com.resolve.services.interfaces.VO;

public class ArtifactTypeVO extends VO {

	private static final long serialVersionUID = 5806230504262573905L;

	private String UName;
	private Set<CEFDictionaryItemVO> cefItems;
	private Set<CustomDictionaryItemVO> customItems;
	private Boolean UStandard;
	private Date sysUpdatedOn;

	public String getUName() {
		return UName;
	}

	public void setUName(String uName) {
		UName = uName;
	}

	public Set<CEFDictionaryItemVO> getCefItems() {
		return cefItems;
	}

	public void setCefItems(Set<CEFDictionaryItemVO> cefItems) {
		this.cefItems = cefItems;
	}

	public Set<CustomDictionaryItemVO> getCustomItems() {
		return customItems;
	}

	public void setCustomItems(Set<CustomDictionaryItemVO> customItems) {
		this.customItems = customItems;
	}

	public Boolean getUStandard() {
		return UStandard;
	}

	public void setUStandard(Boolean uStandard) {
		UStandard = uStandard;
	}

    public Date getSysUpdatedOn() {
        return sysUpdatedOn;
    }

    public void setSysUpdatedOn(Date sysUpdatedOn) {
        this.sysUpdatedOn = sysUpdatedOn;
    }
}
