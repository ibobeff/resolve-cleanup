/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.beans.Transient;
import java.util.Collection;

import com.resolve.services.interfaces.VO;

public class MetaFieldPropertiesVO extends VO
{
    private static final long serialVersionUID = -3950910366389811922L;
    
    private String UName;
    private String UDisplayName;//this we have to add so that the same field can be shown with different display name on different forms
    private String UTable;
    private String UUIType;// FOR GXT
    private Integer UOrder;
    private String UJavascript;
    private Integer USize;
    private Boolean UIsMandatory;
    private Boolean UIsCrypt;
    private String UDefaultValue;
    private Integer UWidth;
    private Integer UHeight;
    private String ULabelAlign;
    private Integer UStringMinLength;
    private Integer UStringMaxLength;
    private Integer UUIStringMaxLength;
    private Integer UBooleanMaxLength;
    private Boolean USelectIsMultiSelect;
    private Boolean USelectUserInput;
    private Integer USelectMaxDisplay;
    private String USelectValues;
    private String UChoiceValues;
    private String UCheckboxValues;
    private Boolean UDateTimeHasCalendar;
    private Integer UIntegerMinValue;
    private Integer UIntegerMaxValue;
    private Double UDecimalMinValue;
    private Double UDecimalMaxValue;
    private Integer UJournalRows;
    private Integer UJournalColumns;
    private Integer UJournalMinValue;
    private Integer UJournalMaxValue;
    private Integer UListRows;
    private Integer UListColumns;
    private Integer UListMaxDisplay;
    private String UListValues;

    private String UReferenceTable;
    private String UReferenceDisplayColumn;
    private String UReferenceDisplayColumnList;
    private String UReferenceTarget;
    private String UReferenceParams;

    private String ULinkTarget;
    private String ULinkParams;
    private String ULinkTemplate;
    private String USequencePrefix;
    private String UHiddenValue;

    private String UGroups;
    private String UUsersOfTeams;
    private Boolean URecurseUsersOfTeam;
    private String UTeamsOfTeams;
    private Boolean URecurseTeamsOfTeam;
    private Boolean UAutoAssignToMe;
    private Boolean UAllowAssignToMe;

    private String UTeamPickerTeamsOfTeams;

    private Boolean UIsHidden;
    private Boolean UIsReadOnly;

    private String UTooltip;

    // file upload widget
    private String UFileUploadTableName;
    private String UFileUploadReferenceColumnName;
    private Boolean UAllowUpload;
    private Boolean UAllowDownload;
    private Boolean UAllowRemove;
    private String UAllowFileTypes;

    private Integer UWidgetColumns;

    // for Reference grid
    // select a, b, c from B where u_A_sys_id = 'ddddddd'
    private String URefGridReferenceByTable;// B
    private String URefGridSelectedReferenceColumns; // a, b, c
    private String URefGridReferenceColumnName; // u_A_sys_id
    private String UAllowReferenceTableAdd;
    private String UAllowReferenceTableRemove;
    private String UAllowReferenceTableView;
    private String UReferenceTableUrl;
    private String URefGridViewPopup;
    private String URefGridViewPopupWidth;
    private String URefGridViewPopupHeight;
    private String URefGridViewPopupTitle;

    // for choice additions
    private String UChoiceTable;
    private String UChoiceDisplayField;
    private String UChoiceValueField;
    private String UChoiceWhere;
    private String UChoiceField;
    private String UChoiceSql;
    private int UChoiceOptionSelection;

    // For columns
    private int USectionNumberOfColumns;
    private String USectionType;
    private String USectionLayout;
    private String USectionTitle;
    private String USectionColumns;
    
    //for spacer
    private Boolean UShowHorizontalRule;
    
    //for journal
    private Boolean UAllowJournalEdit;
    
    private String jsonString;

    private MetaStyleVO metaStyle;

    private Collection<MetaxFieldDependencyVO> metaxFieldDependencys;

    public MetaFieldPropertiesVO()
    {
    }

    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }
    
    public String getUDisplayName()
    {
        return this.UDisplayName;
    }

    public void setUDisplayName(String UDisplayName)
    {
        this.UDisplayName = UDisplayName;
    }

    public String getUTable()
    {
        return this.UTable;
    }

    public void setUTable(String UTable)
    {
        this.UTable = UTable;
    }

    public String getUUIType()
    {
        return this.UUIType;
    }

    public void setUUIType(String UUIType)
    {
        this.UUIType = UUIType;
    }

    public Integer getUOrder()
    {
        return this.UOrder;
    }

    public Integer ugetUOrder()
    {
        if (this.UOrder != null)
        {
            return this.UOrder;
        }
        else
        {
            return 0;
        }
    }

    public void setUOrder(Integer UOrder)
    {
        this.UOrder = UOrder;
    }

    public String getUJavascript()
    {
        return this.UJavascript;
    }

    public void setUJavascript(String UJavascript)
    {
        this.UJavascript = UJavascript;
    }

    public Integer getUSize()
    {
        return this.USize;
    }

    public Integer ugetUSize()
    {
        if (this.USize != null)
        {
            return this.USize;
        }
        else
        {
            return 0;
        }
    }

    public void setUSize(Integer USize)
    {
        this.USize = USize;
    }

    public Boolean getUIsMandatory()
    {
        return this.UIsMandatory;
    }

    public Boolean ugetUIsMandatory()
    {
        if (this.UIsMandatory == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsMandatory;
        }
    }

    public void setUIsMandatory(Boolean UIsMandatory)
    {
        this.UIsMandatory = UIsMandatory;
    }

    public Boolean getUIsCrypt()
    {
        return this.UIsCrypt;
    }

    public Boolean ugetUIsCrypt()
    {
        if (this.UIsCrypt == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsCrypt;
        }
    }

    public void setUIsCrypt(Boolean UIsCrypt)
    {
        this.UIsCrypt = UIsCrypt;
    }

    public String getUDefaultValue()
    {
        return this.UDefaultValue;
    }

    public void setUDefaultValue(String UDefaultValue)
    {
        this.UDefaultValue = UDefaultValue;
    }

    public Integer getUWidth()
    {
        return UWidth;
    }

    public void setUWidth(Integer uWidth)
    {
        UWidth = uWidth;
    }

    public Integer getUHeight()
    {
        return UHeight;
    }

    public void setUHeight(Integer uHeight)
    {
        UHeight = uHeight;
    }

    public String getULabelAlign()
    {
        return ULabelAlign;
    }

    public void setULabelAlign(String uLabelAlign)
    {
        this.ULabelAlign = uLabelAlign;
    }

    public Integer getUStringMinLength()
    {
        return UStringMinLength;
    }

    public void setUStringMinLength(Integer uStringMinLength)
    {
        this.UStringMinLength = uStringMinLength;
    }

    public Integer getUStringMaxLength()
    {
        return this.UStringMaxLength;
    }

    public void setUStringMaxLength(Integer UStringMaxLength)
    {
        this.UStringMaxLength = UStringMaxLength;
    }

    public Integer getUUIStringMaxLength()
    {
        return UUIStringMaxLength;
    }

    public void setUUIStringMaxLength(Integer uUIStringMaxLength)
    {
        UUIStringMaxLength = uUIStringMaxLength;
    }

    public Integer getUBooleanMaxLength()
    {
        return this.UBooleanMaxLength;
    }

    public void setUBooleanMaxLength(Integer UBooleanMaxLength)
    {
        this.UBooleanMaxLength = UBooleanMaxLength;
    }

    public Boolean getUSelectIsMultiSelect()
    {
        return this.USelectIsMultiSelect;
    }

    public Boolean ugetUSelectIsMultiSelect()
    {
        if (this.USelectIsMultiSelect == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.USelectIsMultiSelect;
        }
    }

    public void setUSelectIsMultiSelect(Boolean USelectIsMultiSelect)
    {
        this.USelectIsMultiSelect = USelectIsMultiSelect;
    }

    public Boolean getUSelectUserInput()
    {
        return USelectUserInput;
    }

    public Boolean ugetUSelectUserInput()
    {
        if (this.USelectUserInput == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.USelectUserInput;
        }
    }

    public void setUSelectUserInput(Boolean uSelectUserInput)
    {
        USelectUserInput = uSelectUserInput;
    }

    public Integer getUSelectMaxDisplay()
    {
        return this.USelectMaxDisplay;
    }

    public Integer ugetUSelectMaxDisplay()
    {
        if (this.USelectMaxDisplay != null)
        {
            return this.USelectMaxDisplay;
        }
        else
        {
            return 0;
        }
    }

    public void setUSelectMaxDisplay(Integer USelectMaxDisplay)
    {
        this.USelectMaxDisplay = USelectMaxDisplay;
    }

    public String getUSelectValues()
    {
        return this.USelectValues;
    }

    public void setUSelectValues(String USelectValues)
    {
        this.USelectValues = USelectValues;
    }

    public String getUChoiceValues()
    {
        return this.UChoiceValues;
    }

    public void setUChoiceValues(String UChoiceValues)
    {
        this.UChoiceValues = UChoiceValues;
    }

    public String getUCheckboxValues()
    {
        return this.UCheckboxValues;
    }

    public void setUCheckboxValues(String UCheckboxValues)
    {
        this.UCheckboxValues = UCheckboxValues;
    }

    public Boolean getUDateTimeHasCalendar()
    {
        return this.UDateTimeHasCalendar;
    }

    public Boolean ugetUDateTimeHasCalendar()
    {
        if (this.UDateTimeHasCalendar == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UDateTimeHasCalendar;
        }
    }

    public void setUDateTimeHasCalendar(Boolean UDateTimeHasCalendar)
    {
        this.UDateTimeHasCalendar = UDateTimeHasCalendar;
    }

    public Integer getUIntegerMinValue()
    {
        return this.UIntegerMinValue;
    }

    public Integer ugetUIntegerMinValue()
    {
        if (this.UIntegerMinValue != null)
        {
            return this.UIntegerMinValue;
        }
        else
        {
            return 0;
        }
    }

    public void setUIntegerMinValue(Integer UIntegerMinValue)
    {
        this.UIntegerMinValue = UIntegerMinValue;
    }

    public Integer getUIntegerMaxValue()
    {
        return this.UIntegerMaxValue;
    }

    public Integer ugetUIntegerMaxValue()
    {
        if (this.UIntegerMaxValue != null)
        {
            return this.UIntegerMaxValue;
        }
        else
        {
            return 0;
        }
    }

    public void setUIntegerMaxValue(Integer UIntegerMaxValue)
    {
        this.UIntegerMaxValue = UIntegerMaxValue;
    }

    public Double getUDecimalMinValue()
    {
        return this.UDecimalMinValue;
    }

    public Double ugetUDecimalMinValue()
    {
        if (this.UDecimalMinValue != null)
        {
            return this.UDecimalMinValue;
        }
        else
        {
            return 0D;
        }
    }

    public void setUDecimalMinValue(Double UDecimalMinValue)
    {
        this.UDecimalMinValue = UDecimalMinValue;
    }

    public Double getUDecimalMaxValue()
    {
        return this.UDecimalMaxValue;
    }

    public Double ugetUDecimalMaxValue()
    {
        if (this.UDecimalMaxValue != null)
        {
            return this.UDecimalMaxValue;
        }
        else
        {
            return 0D;
        }
    }

    public void setUDecimalMaxValue(Double UDecimalMaxValue)
    {
        this.UDecimalMaxValue = UDecimalMaxValue;
    }

    public Integer getUJournalRows()
    {
        return this.UJournalRows;
    }

    public Integer ugetUJournalRows()
    {
        if (this.UJournalRows != null)
        {
            return this.UJournalRows;
        }
        else
        {
            return 0;
        }
    }

    public void setUJournalRows(Integer UJournalRows)
    {
        this.UJournalRows = UJournalRows;
    }

    public Integer getUJournalColumns()
    {
        return this.UJournalColumns;
    }

    public Integer ugetUJournalColumns()
    {
        if (this.UJournalColumns != null)
        {
            return this.UJournalColumns;
        }
        else
        {
            return 0;
        }
    }

    public void setUJournalColumns(Integer UJournalColumns)
    {
        this.UJournalColumns = UJournalColumns;
    }

    public Integer getUJournalMinValue()
    {
        return this.UJournalMinValue;
    }

    public Integer ugetUJournalMinValue()
    {
        if (this.UJournalMinValue != null)
        {
            return this.UJournalMinValue;
        }
        else
        {
            return 0;
        }
    }

    public void setUJournalMinValue(Integer UJournalMinValue)
    {
        this.UJournalMinValue = UJournalMinValue;
    }

    public Integer getUJournalMaxValue()
    {
        return this.UJournalMaxValue;
    }

    public Integer ugetUJournalMaxValue()
    {
        if (this.UJournalMaxValue != null)
        {
            return this.UJournalMaxValue;
        }
        else
        {
            return 0;
        }
    }

    public void setUJournalMaxValue(Integer UJournalMaxValue)
    {
        this.UJournalMaxValue = UJournalMaxValue;
    }

    public Integer getUListRows()
    {
        return this.UListRows;
    }

    public Integer ugetUListRows()
    {
        if (this.UListRows != null)
        {
            return this.UListRows;
        }
        else
        {
            return 0;
        }
    }

    public void setUListRows(Integer UListRows)
    {
        this.UListRows = UListRows;
    }

    public Integer getUListColumns()
    {
        return this.UListColumns;
    }

    public Integer ugetUListColumns()
    {
        if (this.UListColumns != null)
        {
            return this.UListColumns;
        }
        else
        {
            return 0;
        }
    }

    public void setUListColumns(Integer UListColumns)
    {
        this.UListColumns = UListColumns;
    }

    public Integer getUListMaxDisplay()
    {
        return this.UListMaxDisplay;
    }

    public Integer ugetUListMaxDisplay()
    {
        if (this.UListMaxDisplay != null)
        {
            return this.UListMaxDisplay;
        }
        else
        {
            return 0;
        }
    }

    public void setUListMaxDisplay(Integer UListMaxDisplay)
    {
        this.UListMaxDisplay = UListMaxDisplay;
    }

    public String getUListValues()
    {
        return UListValues;
    }

    public void setUListValues(String uListValues)
    {
        UListValues = uListValues;
    }

    public String getUReferenceTable()
    {
        return this.UReferenceTable;
    }

    public void setUReferenceTable(String UReferenceTable)
    {
        this.UReferenceTable = UReferenceTable;
    }

    public String getUReferenceDisplayColumn()
    {
        return this.UReferenceDisplayColumn;
    }

    public void setUReferenceDisplayColumn(String UReferenceDisplayColumnList)
    {
        this.UReferenceDisplayColumn = UReferenceDisplayColumnList;
    }

    public String getUReferenceDisplayColumnList()
    {
        return this.UReferenceDisplayColumnList;
    }

    public void setUReferenceDisplayColumnList(String UReferenceDisplayColumnList)
    {
        this.UReferenceDisplayColumnList = UReferenceDisplayColumnList;
    }

    public String getUReferenceTarget()
    {
        return this.UReferenceTarget;
    }

    public void setUReferenceTarget(String UReferenceTarget)
    {
        this.UReferenceTarget = UReferenceTarget;
    }

    public String getUReferenceParams()
    {
        return this.UReferenceParams;
    }

    public void setUReferenceParams(String UReferenceParams)
    {
        this.UReferenceParams = UReferenceParams;
    }

    public String getULinkTarget()
    {
        return this.ULinkTarget;
    }

    public void setULinkTarget(String ULinkTarget)
    {
        this.ULinkTarget = ULinkTarget;
    }

    public String getULinkParams()
    {
        return this.ULinkParams;
    }

    public void setULinkParams(String ULinkParams)
    {
        this.ULinkParams = ULinkParams;
    }

    public String getULinkTemplate()
    {
        return this.ULinkTemplate;
    }

    public void setULinkTemplate(String ULinkTemplate)
    {
        this.ULinkTemplate = ULinkTemplate;
    }

    public String getUSequencePrefix()
    {
        return this.USequencePrefix;
    }

    public void setUSequencePrefix(String USequencePrefix)
    {
        this.USequencePrefix = USequencePrefix;
    }

    public String getUHiddenValue()
    {
        return UHiddenValue;
    }

    public void setUHiddenValue(String uHiddenValue)
    {
        UHiddenValue = uHiddenValue;
    }

    public MetaStyleVO getMetaStyle()
    {
        return metaStyle;
    }

    public void setMetaStyle(MetaStyleVO metaStyle)
    {
        this.metaStyle = metaStyle;
    }

    public String getUGroups()
    {
        return UGroups;
    }

    public void setUGroups(String uGroups)
    {
        UGroups = uGroups;
    }

    public String getUUsersOfTeams()
    {
        return UUsersOfTeams;
    }

    public void setUUsersOfTeams(String uUsersOfTeams)
    {
        UUsersOfTeams = uUsersOfTeams;
    }

    public Boolean getURecurseUsersOfTeam()
    {
        return URecurseUsersOfTeam;
    }

    public Boolean ugetURecurseUsersOfTeam()
    {
        if (this.URecurseUsersOfTeam == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.URecurseUsersOfTeam;
        }
    }

    public void setURecurseUsersOfTeam(Boolean uRecurseUsersOfTeam)
    {
        URecurseUsersOfTeam = uRecurseUsersOfTeam;
    }

    public String getUTeamsOfTeams()
    {
        return UTeamsOfTeams;
    }

    public void setUTeamsOfTeams(String uTeamsOfTeams)
    {
        UTeamsOfTeams = uTeamsOfTeams;
    }

    public String getUTeamPickerTeamsOfTeams()
    {
        return UTeamPickerTeamsOfTeams;
    }

    public void setUTeamPickerTeamsOfTeams(String uTeamPickerTeamsOfTeams)
    {
        UTeamPickerTeamsOfTeams = uTeamPickerTeamsOfTeams;
    }

    public Boolean getURecurseTeamsOfTeam()
    {
        return URecurseTeamsOfTeam;
    }

    public void setURecurseTeamsOfTeam(Boolean uRecurseTeamsOfTeam)
    {
        URecurseTeamsOfTeam = uRecurseTeamsOfTeam;
    }

    public Boolean ugetURecurseTeamsOfTeam()
    {
        if (this.URecurseTeamsOfTeam == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.URecurseTeamsOfTeam;
        }
    }

    public Boolean getUIsHidden()
    {
        return UIsHidden;
    }

    public void setUIsHidden(Boolean uIsHidden)
    {
        UIsHidden = uIsHidden;
    }

    public Boolean ugetUIsHidden()
    {
        if (this.UIsHidden == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsHidden;
        }
    }

    public Boolean getUIsReadOnly()
    {
        return UIsReadOnly;
    }

    public void setUIsReadOnly(Boolean uIsReadOnly)
    {
        UIsReadOnly = uIsReadOnly;
    }

    public Boolean ugetUIsReadOnly()
    {
        if (this.UIsReadOnly == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UIsReadOnly;
        }
    }

    public Boolean getUAutoAssignToMe()
    {
        return UAutoAssignToMe;
    }

    public Boolean ugetUAutoAssignToMe()
    {
        if (this.UAutoAssignToMe == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UAutoAssignToMe;
        }
    }

    public void setUAutoAssignToMe(Boolean uAutoAssignToMe)
    {
        UAutoAssignToMe = uAutoAssignToMe;
    }

    public Boolean getUAllowAssignToMe()
    {
        return UAllowAssignToMe;
    }

    public Boolean ugetUAllowAssignToMe()
    {
        if (this.UAllowAssignToMe == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UAllowAssignToMe;
        }
    }

    public void setUAllowAssignToMe(Boolean uAllowAssignToMe)
    {
        UAllowAssignToMe = uAllowAssignToMe;
    }

    public Collection<MetaxFieldDependencyVO> getMetaxFieldDependencys()
    {
        return this.metaxFieldDependencys;
    }

    public void setMetaxFieldDependencys(Collection<MetaxFieldDependencyVO> metaxFieldDependencys)
    {
        this.metaxFieldDependencys = metaxFieldDependencys;
    }

    public Boolean getUAllowUpload()
    {
        return UAllowUpload;
    }

    public Boolean ugetUAllowUpload()
    {
        if (this.UAllowUpload == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UAllowUpload;
        }
    }

    public void setUAllowUpload(Boolean uAllowUpload)
    {
        UAllowUpload = uAllowUpload;
    }

    public Boolean getUAllowDownload()
    {
        return UAllowDownload;
    }

    public Boolean ugetUAllowDownload()
    {
        if (this.UAllowDownload == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UAllowDownload;
        }
    }

    public void setUAllowDownload(Boolean uAllowDownload)
    {
        UAllowDownload = uAllowDownload;
    }

    public Boolean getUAllowRemove()
    {
        return UAllowRemove;
    }

    public Boolean ugetUAllowRemove()
    {
        if (this.UAllowRemove == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UAllowRemove;
        }
    }

    public void setUAllowRemove(Boolean uAllowRemove)
    {
        UAllowRemove = uAllowRemove;
    }

    public String getUAllowFileTypes()
    {
        return UAllowFileTypes;
    }

    public void setUAllowFileTypes(String uAllowFileTypes)
    {
        UAllowFileTypes = uAllowFileTypes;
    }

    public Integer getUWidgetColumns()
    {
        return UWidgetColumns;
    }

    public void setUWidgetColumns(Integer uWidgetColumns)
    {
        UWidgetColumns = uWidgetColumns;
    }

    public String getUFileUploadTableName()
    {
        return UFileUploadTableName;
    }

    public void setUFileUploadTableName(String uFileUploadTableName)
    {
        UFileUploadTableName = uFileUploadTableName;
    }

    public String getUFileUploadReferenceColumnName()
    {
        return UFileUploadReferenceColumnName;
    }

    public void setUFileUploadReferenceColumnName(String uFileUploadReferenceColumnName)
    {
        UFileUploadReferenceColumnName = uFileUploadReferenceColumnName;
    }

    public String getURefGridReferenceByTable()
    {
        return URefGridReferenceByTable;
    }

    public void setURefGridReferenceByTable(String uRefGridReferenceByTable)
    {
        URefGridReferenceByTable = uRefGridReferenceByTable;
    }

    public String getURefGridSelectedReferenceColumns()
    {
        return URefGridSelectedReferenceColumns;
    }

    public void setURefGridSelectedReferenceColumns(String uRefGridSelectedReferenceColumns)
    {
        URefGridSelectedReferenceColumns = uRefGridSelectedReferenceColumns;
    }

    public String getURefGridReferenceColumnName()
    {
        return URefGridReferenceColumnName;
    }

    public void setURefGridReferenceColumnName(String uRefGridReferenceColumnName)
    {
        URefGridReferenceColumnName = uRefGridReferenceColumnName;
    }

    public String getUAllowReferenceTableAdd()
    {
        return UAllowReferenceTableAdd;
    }

    public void setUAllowReferenceTableAdd(String uAllowReferenceTableAdd)
    {
        UAllowReferenceTableAdd = uAllowReferenceTableAdd;
    }

    public String getUAllowReferenceTableRemove()
    {
        return UAllowReferenceTableRemove;
    }

    public void setUAllowReferenceTableRemove(String uAllowReferenceTableRemove)
    {
        UAllowReferenceTableRemove = uAllowReferenceTableRemove;
    }

    public String getUAllowReferenceTableView()
    {
        return UAllowReferenceTableView;
    }

    public void setUAllowReferenceTableView(String uAllowReferenceTableView)
    {
        UAllowReferenceTableView = uAllowReferenceTableView;
    }

    public String getUReferenceTableUrl()
    {
        return UReferenceTableUrl;
    }

    public void setUReferenceTableUrl(String uReferenceTableUrl)
    {
        UReferenceTableUrl = uReferenceTableUrl;
    }

    public String getURefGridViewPopup()
    {
        return URefGridViewPopup;
    }

    public void setURefGridViewPopup(String uRefGridViewPopup)
    {
        URefGridViewPopup = uRefGridViewPopup;
    }

    public String getURefGridViewPopupWidth()
    {
        return URefGridViewPopupWidth;
    }

    public void setURefGridViewPopupWidth(String uRefGridViewPopupWidth)
    {
        URefGridViewPopupWidth = uRefGridViewPopupWidth;
    }

    public String getURefGridViewPopupHeight()
    {
        return URefGridViewPopupHeight;
    }

    public void setURefGridViewPopupHeight(String uRefGridViewPopupHeight)
    {
        URefGridViewPopupHeight = uRefGridViewPopupHeight;
    }

    public String getURefGridViewPopupTitle()
    {
        return URefGridViewPopupTitle;
    }

    public void setURefGridViewPopupTitle(String uRefGridViewPopupTitle)
    {
        URefGridViewPopupTitle = uRefGridViewPopupTitle;
    }

    public String getUTooltip()
    {
        return UTooltip;
    }

    public void setUTooltip(String tooltip)
    {
        UTooltip = tooltip;
    }

    @Transient
    public String getUChoiceTable()
    {
        return UChoiceTable;
    }

    public void setUChoiceTable(String uChoiceTable)
    {
        UChoiceTable = uChoiceTable;
    }

    @Transient
    public String getUChoiceDisplayField()
    {
        return UChoiceDisplayField;
    }

    public void setUChoiceDisplayField(String uChoiceDisplayField)
    {
        UChoiceDisplayField = uChoiceDisplayField;
    }

    @Transient
    public String getUChoiceValueField()
    {
        return UChoiceValueField;
    }

    public void setUChoiceValueField(String uChoiceValueField)
    {
        UChoiceValueField = uChoiceValueField;
    }

    @Transient
    public String getUChoiceWhere()
    {
        return UChoiceWhere;
    }

    public void setUChoiceWhere(String uChoiceWhere)
    {
        UChoiceWhere = uChoiceWhere;
    }

    @Transient
    public String getUChoiceField()
    {
        return UChoiceField;
    }

    public void setUChoiceField(String uChoiceField)
    {
        UChoiceField = uChoiceField;
    }

    @Transient
    public String getUChoiceSql()
    {
        return UChoiceSql;
    }

    public void setUChoiceSql(String uChoiceSql)
    {
        UChoiceSql = uChoiceSql;
    }

    @Transient
    public int getUChoiceOptionSelection()
    {
        return UChoiceOptionSelection;
    }

    public void setUChoiceOptionSelection(int uChoiceOptionSelection)
    {
        UChoiceOptionSelection = uChoiceOptionSelection;
    }

    @Transient
    public String getJsonString()
    {
        return jsonString;
    }

    public void setJsonString(String jsonString)
    {
        this.jsonString = jsonString;
    }

    @Transient
    public int getUSectionNumberOfColumns()
    {
        return USectionNumberOfColumns;
    }

    public void setUSectionNumberOfColumns(int uSectionNumberOfColumns)
    {
        USectionNumberOfColumns = uSectionNumberOfColumns;
    }

    @Transient
    public String getUSectionType()
    {
        return USectionType;
    }

    public void setUSectionType(String uSectionType)
    {
        USectionType = uSectionType;
    }

    @Transient
    public String getUSectionLayout()
    {
        return USectionLayout;
    }

    public void setUSectionLayout(String uSectionLayout)
    {
        USectionLayout = uSectionLayout;
    }

    @Transient
    public String getUSectionTitle()
    {
        return USectionTitle;
    }

    public void setUSectionTitle(String uSectionTitle)
    {
        USectionTitle = uSectionTitle;
    }

    @Transient
    public String getUSectionColumns()
    {
        return USectionColumns;
    }

    public void setUSectionColumns(String uSectionColumns)
    {
        USectionColumns = uSectionColumns;
    }

    @Transient
    public Boolean getUShowHorizontalRule()
    {
        return UShowHorizontalRule;
    }

    public void setUShowHorizontalRule(Boolean uShowHorizontalRule)
    {
        UShowHorizontalRule = uShowHorizontalRule;
    }

    @Transient
    public Boolean getUAllowJournalEdit()
    {
        return UAllowJournalEdit;
    }

    public void setUAllowJournalEdit(Boolean uAllowJournalEdit)
    {
        UAllowJournalEdit = uAllowJournalEdit;
    }
}
