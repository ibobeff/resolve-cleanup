/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveWikiLookupVO extends VO implements Comparable<ResolveWikiLookupVO>
{
    private static final long serialVersionUID = -8043845343605910620L;
    
    private String URegex;
    private String UWiki;
    private String UModule;
    private Integer UOrder;
    private String UDescription;
    // Needed for new Impex UI
    private String UFullname; // populate UWiki in this field
    private String USummary; // populate Udescription in this field

    public ResolveWikiLookupVO()
    {
    }

    public String getURegex()
    {
        return this.URegex;
    }

    public void setURegex(String URegex)
    {
        this.URegex = URegex;
    }

    public String getUWiki()
    {
        return this.UWiki;
    }

    public void setUWiki(String UWiki)
    {
        this.UWiki = UWiki;
    }

    public String getUModule()
    {
        return this.UModule;
    }

    public void setUModule(String UModule)
    {
        this.UModule = UModule;
    }
    
    public String getUDescription()
    {
        return UDescription;
    }

    public void setUDescription(String uDescription)
    {
        UDescription = uDescription;
    }

    public Integer getUOrder()
    {
        return this.UOrder;
    }

    public void setUOrder(Integer UOrder)
    {
        this.UOrder = UOrder;
    }
    
    public String getUFullname()
    {
        return UFullname;
    }

    public void setUFullname(String uFullname)
    {
        UFullname = uFullname;
    }

    public String getUSummary()
    {
        return USummary;
    }

    public void setUSummary(String uSummary)
    {
        USummary = uSummary;
    }

    public int compareTo(ResolveWikiLookupVO obj)
    {
        int result = 0;
        if (!this.getSys_id().equals(obj.getSys_id()))
        {
            if (this.getUOrder() > obj.getUOrder())
            {
                result = 1;
            }
            else if (this.getUOrder() < obj.getUOrder())
            {
                result = -1;
            }
        }

        return result;
    }

}
