/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveSessionVO extends VO
{
    private static final long serialVersionUID = 3985924879458768463L;
    
    private String UChangeRequest;      // deprecated
    private String UActiveType;

    private ArchiveWorksheetVO archiveWorksheet;
    private String UWorksheetId;
    private Boolean UIsArchive;
    private String worksheetNum;
    
    public ResolveSessionVO()
    {
    }

    public String getUChangeRequest()
    {
        return this.UChangeRequest;
    }

    public void setUChangeRequest(String UChangeRequest)
    {
        this.UChangeRequest = UChangeRequest;
    }

    public String getUActiveType()
    {
        return this.UActiveType;
    }

    public void setUActiveType(String UActiveType)
    {
        this.UActiveType = UActiveType;
    }

    public String getUWorksheetId()
    {
        return UWorksheetId;
    }

    public void setUWorksheetId(String uWorksheetId)
    {
        UWorksheetId = uWorksheetId;
    }

    public Boolean getUIsArchive()
    {
        return UIsArchive;
    }

    public Boolean ugetUIsArchive()
    {
        Boolean isArchive = getUIsArchive();
        if(isArchive == null)
        {
            isArchive = false;
        }
        return isArchive;
    }

    public void setUIsArchive(Boolean uIsArchive)
    {
        UIsArchive = uIsArchive;
    }

    public ArchiveWorksheetVO getArchiveWorksheet()
    {
        return archiveWorksheet;
        
        // one-way only - no collections on the other side
    }

    public void setArchiveWorksheet(ArchiveWorksheetVO archiveWorksheet)
    {
        this.archiveWorksheet = archiveWorksheet;
    }
    
    public String getWorksheetNum() {
    	return worksheetNum;
    }
    
    public void setWorksheetNum(String worksheetNum) {
    	this.worksheetNum = worksheetNum;
    }
}
