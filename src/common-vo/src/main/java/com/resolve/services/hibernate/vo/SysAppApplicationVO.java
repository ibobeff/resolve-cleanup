/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;


public class SysAppApplicationVO extends VO
{
    private static final long serialVersionUID = 8123679605480842103L;
    
    private String name;
    private String title;
    private Boolean active;
    private String viewName;
    private BigDecimal order;
    private String hint;

    private String color;
    private String textColor;
    
    // object referenced by
//    private List<SysAppApplicationrolesVO> sysAppApplicationroles;
    private List<SysAppModuleVO> sysAppModules;
    
    //for UI
    private Set<String> roles;
	
    public SysAppApplicationVO()
    {
    }
    
    @Override
    public boolean validate()
    {
        boolean valid = true;
        
        if(StringUtils.isEmpty(getTitle()) || (StringUtils.isNotBlank(getTitle()) && getTitle().equalsIgnoreCase(VO.STRING_DEFAULT)))
        {
            valid = false;
            throw new RuntimeException("Title cannot be empty.");
        }
        
        valid = getTitle().matches(HibernateConstants.REGEX_ALPHANUMERIC_WITH_SPACE);
        if(!valid)
        {
            throw new RuntimeException("Title can have only Alpha numeric, space and _.");
        }
        
        return valid;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getTitle()
    {
        return this.title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public Boolean getActive()
    {
        return this.active;
    }

    public void setActive(Boolean active)
    {
        this.active = active;
    }

    public String getViewName()
    {
        return this.viewName;
    }

    public void setViewName(String viewName)
    {
        this.viewName = viewName;
    }

    public BigDecimal getOrder()
    {
        return this.order;
    }

    public void setOrder(BigDecimal order)
    {
        this.order = order;
    }

    public String getHint()
    {
        return this.hint;
    }

    public void setHint(String hint)
    {
        this.hint = hint;
    }

//    public List<SysAppApplicationrolesVO> getSysAppApplicationroles()
//    {
//        return sysAppApplicationroles;
//    }
//
//    public void setSysAppApplicationroles(List<SysAppApplicationrolesVO> sysAppApplicationroles)
//    {
//        this.sysAppApplicationroles = sysAppApplicationroles;
//    }


    public List<SysAppModuleVO> getSysAppModules()
    {
        return this.sysAppModules;
    }

    public void setSysAppModules(List<SysAppModuleVO> sysAppModules)
    {
        this.sysAppModules = sysAppModules;
    }
    
    public Set<String> getRoles()
    {
        return roles;
    }

    public void setRoles(Set<String> roles)
    {
        this.roles = roles;
    }
    
    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public String getTextColor()
    {
        return textColor;
    }

    public void setTextColor(String textColor)
    {
        this.textColor = textColor;
    }
}
