/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class MCPBlueprintVO extends VO
{
    private static final long serialVersionUID = 5121195006830440352L;

    private String blueprint;
    private String version;
    private Boolean active;

    public String getBlueprint()
    {
        return blueprint;
    }
    public void setBlueprint(String blueprint)
    {
        this.blueprint = blueprint;
    }
    public String getVersion()
    {
        return version;
    }
    public void setVersion(String version)
    {
        this.version = version;
    }
    public Boolean getActive()
    {
        return active;
    }
    public void setActive(Boolean active)
    {
        this.active = active;
    }
}
