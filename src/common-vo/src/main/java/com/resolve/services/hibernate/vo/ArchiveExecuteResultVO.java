/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;

public class ArchiveExecuteResultVO extends VO
{
    private static final long serialVersionUID = 4531310172300380063L;
    
    private String UTarget;
    private Integer UDuration;
    private Integer UReturncode;
    private String UAddress;

    // object references
    private ArchiveExecuteRequestVO executeRequest;   // URequest;
    private ArchiveExecuteResultLobVO executeResultLob;
	
    // object referenced by
	
    public ArchiveExecuteResultVO()
    {
    }

    public ArchiveExecuteResultVO usetsys_id(String sys_id)
    {
        this.setSys_id(sys_id);
        return this;
    }
    public String getUTarget()
    {
        return this.UTarget;
    }

    public void setUTarget(String UTarget)
    {
        this.UTarget = UTarget;
    }

	public ArchiveExecuteRequestVO getExecuteRequest()
	{
		return executeRequest;
	}

	public void setExecuteRequest(ArchiveExecuteRequestVO executeRequest)
	{
		this.executeRequest = executeRequest;
		
		if (executeRequest != null)
		{
			Collection<ArchiveExecuteResultVO> coll = executeRequest.getArchiveExecuteResults();
			if (coll == null)
			{
				coll = new HashSet<ArchiveExecuteResultVO>();
				coll.add(this);
				
			    executeRequest.setArchiveExecuteResults(coll);
			}
		}
	}
    
    public Integer getUDuration()
    {
        return this.UDuration;
    }

    public void setUDuration(Integer UDuration)
    {
        this.UDuration = UDuration;
    }

    public Integer getUReturncode()
    {
        return this.UReturncode;
    }

    public void setUReturncode(Integer UReturncode)
    {
        this.UReturncode = UReturncode;
    }

    public String getURaw()
    {
        if(this.executeResultLob != null)
        {
            return this.executeResultLob.getURaw();
        }
        return null;
    }

    public String getUCommand()
    {
        if(this.executeResultLob != null)
        {
            return this.executeResultLob.getUCommand();
        }
        return null;
    }

    public String getUAddress()
    {
        return this.UAddress;
    }

    public void setUAddress(String UAddress)
    {
        this.UAddress = UAddress;
    }

    public ArchiveExecuteResultLobVO getExecuteResultLob()
    {
        return this.executeResultLob;
    }

    public void setExecuteResultLob(ArchiveExecuteResultLobVO executeResultLob)
    {
        this.executeResultLob= executeResultLob;
    }
}
