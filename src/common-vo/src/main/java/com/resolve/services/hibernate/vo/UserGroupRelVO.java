/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class UserGroupRelVO extends VO
{
    private static final long serialVersionUID = -3182497918859319363L;
    
    // object references
    private UsersVO user;
    private GroupsVO group;
	
    // object referenced by
	
    public UserGroupRelVO()
    {
    }
    
	public String toString()
	{
	    return this.user+" = "+group;
	} // toString


    public UsersVO getUser()
    {
        return user;
    }

    public void setUser(UsersVO user)
    {
        this.user = user;
        
//        if (user != null)
//        {
//            Collection<UserGroupRelVO> coll = user.getUserGroupRels();
//            if (coll == null)
//            {
//				coll = new HashSet<UserGroupRelVO>();
//	            coll.add(this);
//	            
//                user.setUserGroupRels(coll);
//            }
//        }
    }

    public GroupsVO getGroup()
    {
        return group;
    }

    public void setGroup(GroupsVO group)
    {
        this.group = group;
//
//        if (group != null)
//        {
//            Collection<UserGroupRelVO> coll = group.getUserGroupRels();
//            if (coll == null)
//            {
//				coll = new HashSet<UserGroupRelVO>();
//	            coll.add(this);
//	            
//                group.setUserGroupRels(coll);
//            }
//        }
    }
	
}
