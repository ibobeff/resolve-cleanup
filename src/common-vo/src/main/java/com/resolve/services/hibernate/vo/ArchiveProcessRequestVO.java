/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

// Generated Dec 11, 2009 11:15:20 AM by Hibernate Tools 3.2.4.GA

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;

public class ArchiveProcessRequestVO extends VO
{
    private static final long serialVersionUID = -2382142973138999308L;

    private String UStatus;
    private String UWiki;
    private String UTimeout;
    private String UNumber;
    private Integer UDuration;
    private Integer atExecCount;

    // object references
    ArchiveWorksheetVO problem; 

    // object referenced by
    private Collection<ArchiveExecuteRequestVO> archiveExecuteRequests;
    private Collection<ArchiveActionResultVO> archiveActionResults;

    public ArchiveProcessRequestVO()
    {
    }

    public ArchiveProcessRequestVO usetsys_id(String sys_id)
    {
        setSys_id(sys_id);
        return this;
    }

    public String getUStatus()
    {
        return this.UStatus;
    }

    public void setUStatus(String UStatus)
    {
        this.UStatus = UStatus;
    }

    public String getUWiki()
    {
        return this.UWiki;
    }

    public void setUWiki(String UWiki)
    {
        this.UWiki = UWiki;
    }

    public String getUTimeout()
    {
        return this.UTimeout;
    }

    public void setUTimeout(String UTimeout)
    {
        this.UTimeout = UTimeout;
    }

    public ArchiveWorksheetVO getProblem()
    {
        return problem;
    }

    public void setProblem(ArchiveWorksheetVO problem)
    {
        this.problem = problem;

        if (problem != null)
        {
            Collection<ArchiveProcessRequestVO> coll = problem.getArchiveProcessRequests();
            if (coll == null)
            {
				coll = new HashSet<ArchiveProcessRequestVO>();
	            coll.add(this);
	            
                problem.setArchiveProcessRequests(coll);
            }
        }
    }

    public String getUNumber()
    {
        return this.UNumber;
    }

    public void setUNumber(String UNumber)
    {
        this.UNumber = UNumber;
    }

    public Integer getUDuration()
    {
        return this.UDuration;
    }

    public void setUDuration(Integer UDuration)
    {
        this.UDuration = UDuration;
    }
    
    public Integer getAtExecCount() {
    	return atExecCount;
    }
    
    public void setAtExecCount(Integer atExecCount) {
    	this.atExecCount = atExecCount;
    }
    
    public Collection<ArchiveExecuteRequestVO> getArchiveExecuteRequests()
    {
        return archiveExecuteRequests;
    }

    public void setArchiveExecuteRequests(Collection<ArchiveExecuteRequestVO> archiveExecuteRequests)
    {
        this.archiveExecuteRequests = archiveExecuteRequests;
    }

    public Collection<ArchiveActionResultVO> getArchiveActionResults()
    {
        return archiveActionResults;
    }

    public void setArchiveActionResults(Collection<ArchiveActionResultVO> archiveActionResults)
    {
        this.archiveActionResults = archiveActionResults;
    }

} // ArchiveProcessRequest
