/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveEdgePropertiesVO extends VO
{
    private static final long serialVersionUID = 798246469484094457L;
    
    private String UKey;
    private String UValue;

    private ResolveEdgeVO edge;

    public ResolveEdgePropertiesVO() {}
    public ResolveEdgePropertiesVO(String sysId) 
    {
        this.setSys_id(sysId);
    }

    
    public String getUKey()
    {
        return UKey;
    }

    public void setUKey(String key)
    {
        this.UKey = key;
    }

    public String getUValue()
    {
        return UValue;
    }

    public void setUValue(String value)
    {
        this.UValue = value;
    }
    
    public ResolveEdgeVO getEdge()
    {
        return edge;
    }

    public void setEdge(ResolveEdgeVO edge)
    {
        this.edge = edge;
    }

}
