/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;


import com.resolve.services.interfaces.VO;


public class MetaAccessRightsVO  extends VO
{
    private static final long serialVersionUID = -5240181120288533085L;
    
    private String UResourceType;
    private String UResourceName;
    private String UResourceId;
    private String UReadAccess;
    private String UWriteAccess;
    private String UAdminAccess;
    private String UExecuteAccess;

    public MetaAccessRightsVO()
    {
    }

    public String getUResourceType()
    {
        return this.UResourceType;
    }

    public void setUResourceType(String UResourceType)
    {
        this.UResourceType = UResourceType;
    }

    public String getUResourceName()
    {
        return this.UResourceName;
    }

    public void setUResourceName(String UResourceName)
    {
        this.UResourceName = UResourceName;
    }

    public String getUResourceId()
    {
        return this.UResourceId;
    }

    public void setUResourceId(String UResourceId)
    {
        this.UResourceId = UResourceId;
    }

    public String getUReadAccess()
    {
        return this.UReadAccess;
    }

    public void setUReadAccess(String UReadAccess)
    {
        this.UReadAccess = UReadAccess;
    }

    public String getUWriteAccess()
    {
        return this.UWriteAccess;
    }

    public void setUWriteAccess(String UWriteAccess)
    {
        this.UWriteAccess = UWriteAccess;
    }

    public String getUAdminAccess()
    {
        return this.UAdminAccess;
    }

    public void setUAdminAccess(String UAdminAccess)
    {
        this.UAdminAccess = UAdminAccess;
    }

    public String getUExecuteAccess()
    {
        return this.UExecuteAccess;
    }

    public void setUExecuteAccess(String UExecuteAccess)
    {
        this.UExecuteAccess = UExecuteAccess;
    }

}
