/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Date;

import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
public class MCPComponentVO extends VO
{
    private static final long serialVersionUID = 7299621995956172825L;
	private String componentName;
    //sometime RSRemote may have more instances in a server
    private String instanceName;
    private String guid;
    private String serverIp;
    private String serverName;
    private String status;
    private Date lastStatusUpdatedOn;
    private String worksheetId;
    private String worksheet;
    public String getComponentName()
    {
        return componentName;
    }
    public void setComponentName(String componentName)
    {
        this.componentName = componentName;
    }
    public String getInstanceName()
    {
        return instanceName;
    }
    public void setInstanceName(String instanceName)
    {
        this.instanceName = instanceName;
    }
    public String getGuid()
    {
        return guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }
    public String getServerIp()
    {
        return serverIp;
    }
    public void setServerIp(String serverIp)
    {
        this.serverIp = serverIp;
    }
    public String getServerName()
    {
        return serverName;
    }
    public void setServerName(String serverName)
    {
        this.serverName = serverName;
    }
    public String getStatus()
    {
        return status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }
    public Date getLastStatusUpdatedOn()
    {
        return lastStatusUpdatedOn;
    }
    public void setLastStatusUpdatedOn(Date lastStatusUpdatedOn)
    {
        this.lastStatusUpdatedOn = lastStatusUpdatedOn;
    }
    public String getWorksheetId()
    {
        return worksheetId;
    }
    public void setWorksheetId(String worksheetId)
    {
        this.worksheetId = worksheetId;
    }
    public String getWorksheet()
    {
        return worksheet;
    }
    public void setWorksheet(String worksheet)
    {
        this.worksheet = worksheet;
    }
}
