/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.resolve.services.interfaces.VO;

/**
 * 
 * represents the ResolveCatalog model in SQL
 * 
 * @author jeet.marwah
 *
 */
public class ResolveCatalogNodeVO  extends VO
{
    private static final long serialVersionUID = -5301159629066131460L;
    
    //constants
    public static final String DISPLAY_TYPE_WIKI = "wiki";
    public static final String DISPLAY_TYPE_LIST_OF_DOCUMENTS = "listOfDocuments";
    public static final String DISPLAY_TYPE_URL = "url";
    
    //attributes

    //wiki namespace - for the root node. This will be applied to all the nodes if the namespace of the doc is not present
    private String UNamespace = null;
    private String UName;
    private String UType;
    protected String UOperation;

//    private Integer UOrder;

    private String URoles;
    private String UEditRoles;

    private String UTitle;
    private String UDescription;
    private String UImage;
    private String UImageName;
    private String UTooltip;
    private String UWiki;
    private String ULink;
    private String UForm;
    private String UDisplayType;
    private Integer UMaxImageWidth;
    private String UPath;
    private Boolean UOpenInNewTab;
    private Integer USideWidth;

    private String UCatalogType;

    private String UWikidocSysID;

    private Boolean UIsRoot;
    private Boolean UIsRootRef;
    
    private String UInternalName;
    private String UIcon;
    
    //attributes for manipulating the object to sql and vice versa
//    private Integer ULft;
//    private Integer URgt;
//    private Integer UDepth;
    
    private String UReferenceCatalogSysId;
    
    //references
    private ResolveCatalogVO catalog; //belongs to this catalog
    private Collection<ResolveCatalogNodeWikidocRelVO> catalogWikiRels; //referenceing which wiki
    private Collection<ResolveCatalogNodeTagRelVO> catalogTagRels; //catalog and tag rels
 
    private Collection<ResolveCatalogEdgeVO> catalogEdges;
    
    //for client/UI
//    private List<String> wikiSysIds = new ArrayList<String>();
    private List<ResolveTagVO> tags = new ArrayList<ResolveTagVO>();;
    
    
    public ResolveCatalogNodeVO() {}
    public ResolveCatalogNodeVO(String sysId) 
    {
        super.setSys_id(sysId);
    }


    public String getUNamespace()
    {
        return UNamespace;
    }


    public void setUNamespace(String uNamespace)
    {
        UNamespace = uNamespace;
    }


    public String getUName()
    {
        return UName;
    }


    public void setUName(String uName)
    {
        UName = uName;
    }


    public String getUType()
    {
        return UType;
    }


    public void setUType(String uType)
    {
        UType = uType;
    }


    public String getUOperation()
    {
        return UOperation;
    }


    public void setUOperation(String uOperation)
    {
        UOperation = uOperation;
    }


//    public Integer getUOrder()
//    {
//        return UOrder;
//    }
//
//
//    public void setUOrder(Integer uOrder)
//    {
//        UOrder = uOrder;
//    }


    public String getURoles()
    {
        return URoles;
    }


    public void setURoles(String uRoles)
    {
        URoles = uRoles;
    }


    public String getUEditRoles()
    {
        return UEditRoles;
    }


    public void setUEditRoles(String uEditRoles)
    {
        UEditRoles = uEditRoles;
    }


    public String getUTitle()
    {
        return UTitle;
    }


    public void setUTitle(String uTitle)
    {
        UTitle = uTitle;
    }


    public String getUDescription()
    {
        return UDescription;
    }


    public void setUDescription(String uDescription)
    {
        UDescription = uDescription;
    }


    public String getUImage()
    {
        return UImage;
    }


    public void setUImage(String uImage)
    {
        UImage = uImage;
    }


    public String getUImageName()
    {
        return UImageName;
    }


    public void setUImageName(String uImageName)
    {
        UImageName = uImageName;
    }


    public String getUTooltip()
    {
        return UTooltip;
    }


    public void setUTooltip(String uTooltip)
    {
        UTooltip = uTooltip;
    }


    public String getUWiki()
    {
        return UWiki;
    }


    public void setUWiki(String uWiki)
    {
        UWiki = uWiki;
    }


    public String getULink()
    {
        return ULink;
    }


    public void setULink(String uLink)
    {
        ULink = uLink;
    }


    public String getUForm()
    {
        return UForm;
    }


    public void setUForm(String uForm)
    {
        UForm = uForm;
    }


    public String getUDisplayType()
    {
        return UDisplayType;
    }


    public void setUDisplayType(String uDisplayType)
    {
        UDisplayType = uDisplayType;
    }


    public Integer getUMaxImageWidth()
    {
        return UMaxImageWidth;
    }


    public void setUMaxImageWidth(Integer uMaxImageWidth)
    {
        UMaxImageWidth = uMaxImageWidth;
    }


    public String getUPath()
    {
        return UPath;
    }


    public void setUPath(String uPath)
    {
        UPath = uPath;
    }


    public Boolean getUOpenInNewTab()
    {
        return UOpenInNewTab;
    }


    public void setUOpenInNewTab(Boolean uOpenInNewTab)
    {
        UOpenInNewTab = uOpenInNewTab;
    }


    public Integer getUSideWidth()
    {
        return USideWidth;
    }


    public void setUSideWidth(Integer uSideWidth)
    {
        USideWidth = uSideWidth;
    }


    public String getUCatalogType()
    {
        return UCatalogType;
    }


    public void setUCatalogType(String uCatalogType)
    {
        UCatalogType = uCatalogType;
    }


    public String getUWikidocSysID()
    {
        return UWikidocSysID;
    }


    public void setUWikidocSysID(String uWikidocSysID)
    {
        UWikidocSysID = uWikidocSysID;
    }


    public Boolean getUIsRoot()
    {
        return UIsRoot;
    }


    public void setUIsRoot(Boolean uIsRoot)
    {
        UIsRoot = uIsRoot;
    }


    public Boolean getUIsRootRef()
    {
        return UIsRootRef;
    }


    public void setUIsRootRef(Boolean uIsRootRef)
    {
        UIsRootRef = uIsRootRef;
    }


    public String getUInternalName()
    {
        return UInternalName;
    }


    public void setUInternalName(String uInternalName)
    {
        UInternalName = uInternalName;
    }


    public String getUIcon()
    {
        return UIcon;
    }


    public void setUIcon(String uIcon)
    {
        UIcon = uIcon;
    }


//    public Integer getULft()
//    {
//        return ULft;
//    }
//
//
//    public void setULft(Integer uLft)
//    {
//        ULft = uLft;
//    }
//
//
//    public Integer getURgt()
//    {
//        return URgt;
//    }
//
//
//    public void setURgt(Integer uLgt)
//    {
//        URgt = uLgt;
//    }
//
//
//    public Integer getUDepth()
//    {
//        return UDepth;
//    }
//
//
//    public void setUDepth(Integer uDepth)
//    {
//        UDepth = uDepth;
//    }


    public String getUReferenceCatalogSysId()
    {
        return UReferenceCatalogSysId;
    }


    public void setUReferenceCatalogSysId(String uReferenceCatalogSysId)
    {
        UReferenceCatalogSysId = uReferenceCatalogSysId;
    }
    
    public ResolveCatalogVO getCatalog()
    {
        return catalog;
    }

    public void setCatalog(ResolveCatalogVO catalog)
    {
        this.catalog = catalog;
    }
    
    public Collection<ResolveCatalogNodeWikidocRelVO> getCatalogWikiRels()
    {
        return catalogWikiRels;
    }

    public void setCatalogWikiRels(Collection<ResolveCatalogNodeWikidocRelVO> catalogWikiRels)
    {
        this.catalogWikiRels = catalogWikiRels;
    }
    
    public Collection<ResolveCatalogNodeTagRelVO> getCatalogTagRels()
    {
        return catalogTagRels;
    }

    public void setCatalogTagRels(Collection<ResolveCatalogNodeTagRelVO> catalogTagRels)
    {
        this.catalogTagRels = catalogTagRels;
    }

    public Collection<ResolveCatalogEdgeVO> getCatalogEdges()
    {
        return catalogEdges;
    }

    public void setCatalogEdges(Collection<ResolveCatalogEdgeVO> catalogEdges)
    {
        this.catalogEdges = catalogEdges;
    }

    public List<ResolveTagVO> getTags()
    {
        return tags;
    }


    public void setTags(List<ResolveTagVO> tags)
    {
        this.tags = tags;
    }
    
    public void addTag(ResolveTagVO tag)
    {
        this.tags.add(tag);
    }
    
    
    

}
