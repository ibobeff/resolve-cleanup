/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;

import com.resolve.services.interfaces.VO;

public class WikiAttachmentVO extends VO
{
    private static final long serialVersionUID = -8137591013341363351L;

    private String UFilename;
    private String UType;
    private String ULocation;
    private Integer USize;

    // transient
    private WikiAttachmentContentVO wikiAttachmentContent;

    // object referenced by
    private Collection<WikidocAttachmentRelVO> wikidocAttachmentRels;

    public WikiAttachmentVO()
    {
    }

    public WikiAttachmentVO(String sys_id, String UFilename, String UType)
    {
        String filename = UFilename != null ? UFilename.replaceAll("\\s+", " ") : null;
        filename = filename != null ? filename.replaceAll("--", "-") : null;

        setSys_id(sys_id);
        setUFilename(filename);
        setUType(UType);
    }

    public String getUFilename()
    {
        return this.UFilename;
    }

    public void setUFilename(String UFilename)
    {
        // this.UFilename = UFilename;
        String filename = UFilename != null ? UFilename.replaceAll("\\s+", " ") : null;
        filename = filename != null ? filename.replaceAll("--", "-") : null;
        this.UFilename = filename;
    }

    public String getUType()
    {
        return this.UType;
    }

    public void setUType(String UType)
    {
        this.UType = UType;
    }

    public String getULocation()
    {
        return this.ULocation;
    }

    public void setULocation(String ULocation)
    {
        this.ULocation = ULocation;
    }

    public Integer getUSize()
    {
        return this.USize;
    }

    public void setUSize(Integer USize)
    {
        this.USize = USize;
    }

    public Collection<WikidocAttachmentRelVO> getWikidocAttachmentRels()
    {
        return wikidocAttachmentRels;
    }

    public void setWikidocAttachmentRels(Collection<WikidocAttachmentRelVO> wikidocAttachmentRels)
    {
        this.wikidocAttachmentRels = wikidocAttachmentRels;
    }

    public WikiAttachmentContentVO ugetWikiAttachmentContent()
    {
        return wikiAttachmentContent;
    }

    public void usetWikiAttachmentContent(WikiAttachmentContentVO wikiAttachmentContent)
    {
        this.wikiAttachmentContent = wikiAttachmentContent;
    }
}
