/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.util.MappingAnnotation;


public class SNMPFilterVO extends GatewayFilterVO
{
    private static final long serialVersionUID = 5425026376057288431L;

    private Boolean UTrapReceiver;
    private String USnmpTrapOid;
    private Integer USnmpVersion;
    private String UIpAddresses;
    private String UReadCommunity;
    private Integer UTimeout;
    private Integer URetries;
    private String UOid;
    private String URegex;
    private String UComparator;
    private Integer UValue;

    public SNMPFilterVO()
    {
        super();
    }

    @MappingAnnotation(columnName = "TRAP_RECEIVER")
    public Boolean getUTrapReceiver()
    {
        return UTrapReceiver;
    }

    public void setUTrapReceiver(Boolean uTrapReceiver)
    {
        UTrapReceiver = uTrapReceiver;
    }

    @MappingAnnotation(columnName = "SNMP_TRAP_OID")
    public String getUSnmpTrapOid()
    {
        return USnmpTrapOid;
    }

    public void setUSnmpTrapOid(String uSnmpTrapOid)
    {
        USnmpTrapOid = uSnmpTrapOid;
    }

    @MappingAnnotation(columnName = "SNMP_VERSION")
    public Integer getUSnmpVersion()
    {
        return USnmpVersion;
    }

    public void setUSnmpVersion(Integer uSNMPVersion)
    {
        USnmpVersion = uSNMPVersion;
    }

    @MappingAnnotation(columnName = "IP_ADDRESSES")
    public String getUIpAddresses()
    {
        return UIpAddresses;
    }

    public void setUIpAddresses(String uIPAddresses)
    {
        UIpAddresses = uIPAddresses;
    }

    @MappingAnnotation(columnName = "READ_COMMUNITY")
    public String getUReadCommunity()
    {
        return UReadCommunity;
    }

    public void setUReadCommunity(String uReadCommunity)
    {
        this.UReadCommunity = uReadCommunity;
    }

    @MappingAnnotation(columnName = "TIMEOUT")
    public Integer getUTimeout()
    {
        return UTimeout;
    }

    public void setUTimeout(Integer uTimeout)
    {
        UTimeout = uTimeout;
    }

    @MappingAnnotation(columnName = "RETRIES")
    public Integer getURetries()
    {
        return URetries;
    }

    public void setURetries(Integer uRetries)
    {
        URetries = uRetries;
    }

    @MappingAnnotation(columnName = "OID")
    public String getUOid()
    {
        return UOid;
    }

    public void setUOid(String uOid)
    {
        UOid = uOid;
    }

    @MappingAnnotation(columnName = "REGEX")
    public String getURegex()
    {
        return this.URegex;
    } // getURegex

    public void setURegex(String uRegex)
    {
        this.URegex = uRegex;
    } // setURegex

    @MappingAnnotation(columnName = "COMPARATOR")
    public String getUComparator()
    {
        return UComparator;
    }

    public void setUComparator(String uComparator)
    {
        UComparator = uComparator;
    }

    @MappingAnnotation(columnName = "VALUE")
    public Integer getUValue()
    {
        return UValue;
    }

    public void setUValue(Integer uValue)
    {
        UValue = uValue;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((UComparator == null) ? 0 : UComparator.hashCode());
        result = prime * result + ((UIpAddresses == null) ? 0 : UIpAddresses.hashCode());
        result = prime * result + ((UOid == null) ? 0 : UOid.hashCode());
        result = prime * result + ((UReadCommunity == null) ? 0 : UReadCommunity.hashCode());
        result = prime * result + ((URegex == null) ? 0 : URegex.hashCode());
        result = prime * result + ((URetries == null) ? 0 : URetries.hashCode());
        result = prime * result + ((USnmpVersion == null) ? 0 : USnmpVersion.hashCode());
        result = prime * result + ((USnmpTrapOid == null) ? 0 : USnmpTrapOid.hashCode());
        result = prime * result + ((UTimeout == null) ? 0 : UTimeout.hashCode());
        result = prime * result + ((UTrapReceiver == null) ? 0 : UTrapReceiver.hashCode());
        result = prime * result + ((UValue == null) ? 0 : UValue.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        if (getClass() != obj.getClass()) return false;
        SNMPFilterVO other = (SNMPFilterVO) obj;
        if (UComparator == null)
        {
            if (other.UComparator != null) return false;
        }
        else if (!UComparator.equals(other.UComparator)) return false;
        if (UIpAddresses == null)
        {
            if (other.UIpAddresses != null) return false;
        }
        else if (!UIpAddresses.equals(other.UIpAddresses)) return false;
        if (UOid == null)
        {
            if (other.UOid != null) return false;
        }
        else if (!UOid.equals(other.UOid)) return false;
        if (UReadCommunity == null)
        {
            if (other.UReadCommunity != null) return false;
        }
        else if (!UReadCommunity.equals(other.UReadCommunity)) return false;
        if (URegex == null)
        {
            if (other.URegex != null) return false;
        }
        else if (!URegex.equals(other.URegex)) return false;
        if (URetries == null)
        {
            if (other.URetries != null) return false;
        }
        else if (!URetries.equals(other.URetries)) return false;
        /*
        if (USNMPVersion == null)
        {
            if (other.USNMPVersion != null) return false;
        }
        else if (!USNMPVersion.equals(other.USNMPVersion)) return false;*/
        if (USnmpTrapOid == null)
        {
            if (other.USnmpTrapOid != null) return false;
        }
        else if (!USnmpTrapOid.equals(other.USnmpTrapOid)) return false;
        if (UTimeout == null)
        {
            if (other.UTimeout != null) return false;
        }
        else if (!UTimeout.equals(other.UTimeout)) return false;
        if (UTrapReceiver == null)
        {
            if (other.UTrapReceiver != null) return false;
        }
        else if (!UTrapReceiver.equals(other.UTrapReceiver)) return false;
        if (UValue == null)
        {
            if (other.UValue != null) return false;
        }
        else if (!UValue.equals(other.UValue)) return false;
        return true;
    }
}
