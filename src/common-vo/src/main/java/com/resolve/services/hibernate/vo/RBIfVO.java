/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
public class RBIfVO extends VO
{
    private static final long serialVersionUID = 6627413619577812130L;
	private String entityType = "if";
    private String description;
    private Double order;
    private String parentId;
    private String parentType; // e.g., "connection"
    
    public String getEntityType()
    {
        return entityType;
    }
    public void setEntityType(String entityType)
    {
        this.entityType = entityType;
    }
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }
    public Double getOrder()
    {
        return order;
    }
    public void setOrder(Double order)
    {
        this.order = order;
    }
    public String getParentId()
    {
        return parentId;
    }
    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }
    public String getParentType()
    {
        return parentType;
    }
    public void setParentType(String parentType)
    {
        this.parentType = parentType;
    }
}
