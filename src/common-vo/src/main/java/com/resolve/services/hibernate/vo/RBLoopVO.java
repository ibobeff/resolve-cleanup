/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
public class RBLoopVO extends VO
{
    private static final long serialVersionUID = 7089708638846151633L;
	private String entityType = "loop";
    private String description;
    private Double order;
    private Integer count; //if count is provided it loops that many number
    private String itemSource; //WSDATA, PARAMS etc.
    private String itemSourceName; //name of the WSDATA or PARAM
    private String itemDelimiter; //comma or space
    private String parentId;
    private String parentType; //"builder" (for the root)
    
    public String getEntityType()
    {
        return entityType;
    }
    public void setEntityType(String entityType)
    {
        this.entityType = entityType;
    }
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }
    public Double getOrder()
    {
        return order;
    }
    public void setOrder(Double order)
    {
        this.order = order;
    }
    public Integer getCount()
    {
        return count;
    }
    public void setCount(Integer count)
    {
        this.count = count;
    }
    public String getItemSource()
    {
        return itemSource;
    }
    public void setItemSource(String itemSource)
    {
        this.itemSource = itemSource;
    }
    public String getItemSourceName()
    {
        return itemSourceName;
    }
    public void setItemSourceName(String itemSourceName)
    {
        this.itemSourceName = itemSourceName;
    }
    public String getItemDelimiter()
    {
        return itemDelimiter;
    }
    public void setItemDelimiter(String itemDelimiter)
    {
        this.itemDelimiter = itemDelimiter;
    }
    public String getParentId()
    {
        return parentId;
    }
    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }
    public String getParentType()
    {
        return parentType;
    }
    public void setParentType(String parentType)
    {
        this.parentType = parentType;
    }
}
