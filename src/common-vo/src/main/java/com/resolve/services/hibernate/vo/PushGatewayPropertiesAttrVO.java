package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class PushGatewayPropertiesAttrVO extends VO
{
    private static final long serialVersionUID = 1L;

    private String UName;
    private String UValue;
    private String UPushPropertiesId;

    public String getUName()
    {
        return UName;
    }
    public void setUName(String uName)
    {
        UName = uName;
    }
    public String getUValue()
    {
        return UValue;
    }
    public void setUValue(String uValue)
    {
        UValue = uValue;
    }
    public String getUPushPropertiesId()
    {
        return UPushPropertiesId;
    }
    public void setUPushPropertiesId(String uPushPropertiesId)
    {
        UPushPropertiesId = uPushPropertiesId;
    }

} // class PushGatewayPropertiesAttrVO
