/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.List;

import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
public class RBTaskVO extends VO
{
    private static final long serialVersionUID = 2516967089675447208L;
	public static final String TASK_ENTITY_TYPE = "task";
    public static final String SUBRUNBOOK_ENTITY_TYPE = "subrunbook";
    
    private String name;
    //TODO Introduce new runbook components object with reference to actual task, (sub)runbook 
    private String entityType = TASK_ENTITY_TYPE; // can also be "subrunbook"
    private String generalId;
    private String parentId;
    private String parentType; // connection or condition
    private String connectionType; // SSH, Telnet, HTTP/S etc.
    private Double order;
    private String command;
    private String commandUrl;
    private String encodedCommand; //this is encoded command from the UI
    private String promptSource;
    private String promptSourceName;
    private String parserType; // TEXT, XML, JSON etc.
    private String sampleOutput; // sample output of the command
    private Boolean repeat; // does pattern repeat?
    private String description;
    private String summary;
    private String detail;
    private String method;
    private String body;
    private String defaultAssess; // GOOD, BAD
    private String defaultSeverity; // GOOD, WARN, SEVERE, CRITICAL
    private String continuation; // ALWAYS, STOPONBAD
    private String queueName;
    private String queueNameSource;
    private String parserConfig;
    private String assessorScript;
    private String parserScript;
    private Boolean assessorAutoGenEnabled;
    private Boolean parserAutoGenEnabled;
    private RBGeneralVO refRunbook; //Referenced (sub-)runbook
    private String refRunbookParams; //Referenced (sub-)runbook parameters as JSON object string
    private Integer timeout;
    private Integer expectTimeout;
    private String inputfile;
    
    private List<RBTaskConditionVO> conditions;
    private List<RBTaskSeverityVO> severities;
    private List<RBTaskVariableVO> variables;
    
    private List<RBTaskHeaderVO> headers;

    public RBTaskVO()
    {
        super();
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getEntityType()
    {
        return entityType;
    }

    public void setEntityType(String entityType)
    {
        this.entityType = entityType;
    }

    public String getGeneralId()
    {
        return generalId;
    }

    public void setGeneralId(String generalId)
    {
        this.generalId = generalId;
    }

    public String getParentId()
    {
        return parentId;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    public String getParentType()
    {
        return parentType;
    }

    public void setParentType(String parentType)
    {
        this.parentType = parentType;
    }

    public String getConnectionType()
    {
        return connectionType;
    }

    public void setConnectionType(String connectionType)
    {
        this.connectionType = connectionType;
    }

    public Double getOrder()
    {
        return order;
    }

    public void setOrder(Double order)
    {
        this.order = order;
    }

    public String getCommand()
    {
        return command;
    }

    public void setCommand(String command)
    {
        this.command = command;
    }

    public String getEncodedCommand()
    {
        return encodedCommand;
    }

    public void setEncodedCommand(String encodedCommand)
    {
        this.encodedCommand = encodedCommand;
    }

    public String getPromptSource()
    {
        return promptSource;
    }

    public void setPromptSource(String promptSource)
    {
        this.promptSource = promptSource;
    }

    public String getPromptSourceName()
    {
        return promptSourceName;
    }

    public void setPromptSourceName(String promptSourceName)
    {
        this.promptSourceName = promptSourceName;
    }

    public String getParserType()
    {
        return parserType;
    }

    public void setParserType(String parserType)
    {
        this.parserType = parserType;
    }

    public String getSampleOutput()
    {
        return sampleOutput;
    }

    public void setSampleOutput(String sampleOutput)
    {
        this.sampleOutput = sampleOutput;
    }

    public Boolean getRepeat()
    {
        return repeat;
    }

    public void setRepeat(Boolean repeat)
    {
        this.repeat = repeat;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public String getDetail()
    {
        return detail;
    }

    public void setDetail(String detail)
    {
        this.detail = detail;
    }
    public String getBody()
    {
        return body;
    }

    public void setBody(String body)
    {
        this.body = body;
    }
    public String getDefaultAssess()
    {
        return defaultAssess;
    }

    public void setDefaultAssess(String defaultAssess)
    {
        this.defaultAssess = defaultAssess;
    }

    public String getDefaultSeverity()
    {
        return defaultSeverity;
    }

    public void setDefaultSeverity(String defaultSeverity)
    {
        this.defaultSeverity = defaultSeverity;
    }

    public String getContinuation()
    {
        return continuation;
    }

    public void setContinuation(String continuation)
    {
        this.continuation = continuation;
    }

    public String getQueueName()
    {
        return queueName;
    }

    public void setQueueName(String queueName)
    {
        this.queueName = queueName;
    }

    public String getQueueNameSource()
    {
        return queueNameSource;
    }

    public void setQueueNameSource(String queueNameSource)
    {
        this.queueNameSource = queueNameSource;
    }

    public String getParserConfig()
    {
        return parserConfig;
    }

    public void setParserConfig(String parserConfig)
    {
        this.parserConfig = parserConfig;
    }

    public String getAssessorScript()
    {
        return assessorScript;
    }

    public void setAssessorScript(String assessorCode)
    {
        this.assessorScript = assessorCode;
    }

    public String getParserScript()
    {
        return parserScript;
    }

    public void setParserScript(String parserScript)
    {
        this.parserScript = parserScript;
    }

    public Boolean getAssessorAutoGenEnabled()
    {
        return assessorAutoGenEnabled;
    }

    public void setAssessorAutoGenEnabled(Boolean assessorAutoGenEnabled)
    {
        this.assessorAutoGenEnabled = assessorAutoGenEnabled;
    }

    public Boolean getParserAutoGenEnabled()
    {
        return parserAutoGenEnabled;
    }

    public void setParserAutoGenEnabled(Boolean parserAutoGenEnabled)
    {
        this.parserAutoGenEnabled = parserAutoGenEnabled;
    }

    public List<RBTaskConditionVO> getConditions()
    {
        return conditions;
    }

    public void setConditions(List<RBTaskConditionVO> conditions)
    {
        this.conditions = conditions;
    }

    public List<RBTaskSeverityVO> getSeverities()
    {
        return severities;
    }

    public void setSeverities(List<RBTaskSeverityVO> severities)
    {
        this.severities = severities;
    }

    public List<RBTaskVariableVO> getVariables()
    {
        return variables;
    }

    public void setVariables(List<RBTaskVariableVO> variables)
    {
        this.variables = variables;
    }

    public RBGeneralVO getRefRunbook()
    {
        return refRunbook;
    }

    public void setRefRunbook(RBGeneralVO refRunbook)
    {
        this.refRunbook = refRunbook;
    }
    
    public String getRefRunbookParams()
    {
        return refRunbookParams;
    }

    public void setRefRunbookParams(String refRunbookParams)
    {
        this.refRunbookParams = refRunbookParams;
    }
    
    public Integer getTimeout()
    {
        return timeout;
    }

    public void setTimeout(Integer timeout)
    {
        this.timeout = timeout;
    }

    public Integer getExpectTimeout()
    {
        return expectTimeout;
    }

    public void setExpectTimeout(Integer expectTimeout)
    {
        this.expectTimeout = expectTimeout;
    }

    public String getInputfile()
    {
        return inputfile;
    }

    public void setInputfile(String inputfile)
    {
        this.inputfile = inputfile;
    }
    
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((assessorAutoGenEnabled == null) ? 0 : assessorAutoGenEnabled.hashCode());
        result = prime * result + ((assessorScript == null) ? 0 : assessorScript.hashCode());
        result = prime * result + ((command == null) ? 0 : command.hashCode());
        result = prime * result + ((conditions == null) ? 0 : conditions.hashCode());
        result = prime * result + ((connectionType == null) ? 0 : connectionType.hashCode());
        result = prime * result + ((continuation == null) ? 0 : continuation.hashCode());
        result = prime * result + ((defaultAssess == null) ? 0 : defaultAssess.hashCode());
        result = prime * result + ((defaultSeverity == null) ? 0 : defaultSeverity.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((detail == null) ? 0 : detail.hashCode());
        result = prime * result + ((encodedCommand == null) ? 0 : encodedCommand.hashCode());
        result = prime * result + ((entityType == null) ? 0 : entityType.hashCode());
        result = prime * result + ((expectTimeout == null) ? 0 : expectTimeout.hashCode());
        result = prime * result + ((generalId == null) ? 0 : generalId.hashCode());
        result = prime * result + ((inputfile == null) ? 0 : inputfile.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((order == null) ? 0 : order.hashCode());
        result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
        result = prime * result + ((parentType == null) ? 0 : parentType.hashCode());
        result = prime * result + ((parserAutoGenEnabled == null) ? 0 : parserAutoGenEnabled.hashCode());
        result = prime * result + ((parserConfig == null) ? 0 : parserConfig.hashCode());
        result = prime * result + ((parserScript == null) ? 0 : parserScript.hashCode());
        result = prime * result + ((parserType == null) ? 0 : parserType.hashCode());
        result = prime * result + ((promptSource == null) ? 0 : promptSource.hashCode());
        result = prime * result + ((promptSourceName == null) ? 0 : promptSourceName.hashCode());
        result = prime * result + ((queueName == null) ? 0 : queueName.hashCode());
        result = prime * result + ((queueNameSource == null) ? 0 : queueNameSource.hashCode());
        result = prime * result + ((refRunbook == null) ? 0 : refRunbook.hashCode());
        result = prime * result + ((repeat == null) ? 0 : repeat.hashCode());
        result = prime * result + ((sampleOutput == null) ? 0 : sampleOutput.hashCode());
        result = prime * result + ((severities == null) ? 0 : severities.hashCode());
        result = prime * result + ((summary == null) ? 0 : summary.hashCode());
        result = prime * result + ((timeout == null) ? 0 : timeout.hashCode());
        result = prime * result + ((variables == null) ? 0 : variables.hashCode());
        result = prime * result + ((refRunbook == null) ? 0 : refRunbook.hashCode());
        result = prime * result + ((refRunbookParams == null) ? 0 : refRunbookParams.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        RBTaskVO other = (RBTaskVO) obj;
        if (assessorAutoGenEnabled == null)
        {
            if (other.assessorAutoGenEnabled != null) return false;
        }
        else if (!assessorAutoGenEnabled.equals(other.assessorAutoGenEnabled)) return false;
        if (assessorScript == null)
        {
            if (other.assessorScript != null) return false;
        }
        else if (!assessorScript.equals(other.assessorScript)) return false;
        if (command == null)
        {
            if (other.command != null) return false;
        }
        else if (!command.equals(other.command)) return false;
        if (conditions == null)
        {
            if (other.conditions != null) return false;
        }
        else if (!conditions.equals(other.conditions)) return false;
        if (connectionType == null)
        {
            if (other.connectionType != null) return false;
        }
        else if (!connectionType.equals(other.connectionType)) return false;
        if (continuation == null)
        {
            if (other.continuation != null) return false;
        }
        else if (!continuation.equals(other.continuation)) return false;
        if (defaultAssess == null)
        {
            if (other.defaultAssess != null) return false;
        }
        else if (!defaultAssess.equals(other.defaultAssess)) return false;
        if (defaultSeverity == null)
        {
            if (other.defaultSeverity != null) return false;
        }
        else if (!defaultSeverity.equals(other.defaultSeverity)) return false;
        if (description == null)
        {
            if (other.description != null) return false;
        }
        else if (!description.equals(other.description)) return false;
        if (detail == null)
        {
            if (other.detail != null) return false;
        }
        else if (!detail.equals(other.detail)) return false;
        if (encodedCommand == null)
        {
            if (other.encodedCommand != null) return false;
        }
        else if (!encodedCommand.equals(other.encodedCommand)) return false;
        if (entityType == null)
        {
            if (other.entityType != null) return false;
        }
        else if (!entityType.equals(other.entityType)) return false;
        if (expectTimeout == null)
        {
            if (other.expectTimeout != null) return false;
        }
        else if (!expectTimeout.equals(other.expectTimeout)) return false;
        if (generalId == null)
        {
            if (other.generalId != null) return false;
        }
        else if (!generalId.equals(other.generalId)) return false;
        if (inputfile == null)
        {
            if (other.inputfile != null) return false;
        }
        else if (!inputfile.equals(other.inputfile)) return false;
        if (name == null)
        {
            if (other.name != null) return false;
        }
        else if (!name.equals(other.name)) return false;
        if (order == null)
        {
            if (other.order != null) return false;
        }
        else if (!order.equals(other.order)) return false;
        if (parentId == null)
        {
            if (other.parentId != null) return false;
        }
        else if (!parentId.equals(other.parentId)) return false;
        if (parentType == null)
        {
            if (other.parentType != null) return false;
        }
        else if (!parentType.equals(other.parentType)) return false;
        if (parserAutoGenEnabled == null)
        {
            if (other.parserAutoGenEnabled != null) return false;
        }
        else if (!parserAutoGenEnabled.equals(other.parserAutoGenEnabled)) return false;
        if (parserConfig == null)
        {
            if (other.parserConfig != null) return false;
        }
        else if (!parserConfig.equals(other.parserConfig)) return false;
        if (parserScript == null)
        {
            if (other.parserScript != null) return false;
        }
        else if (!parserScript.equals(other.parserScript)) return false;
        if (parserType == null)
        {
            if (other.parserType != null) return false;
        }
        else if (!parserType.equals(other.parserType)) return false;
        if (promptSource == null)
        {
            if (other.promptSource != null) return false;
        }
        else if (!promptSource.equals(other.promptSource)) return false;
        if (promptSourceName == null)
        {
            if (other.promptSourceName != null) return false;
        }
        else if (!promptSourceName.equals(other.promptSourceName)) return false;
        if (queueName == null)
        {
            if (other.queueName != null) return false;
        }
        else if (!queueName.equals(other.queueName)) return false;
        if (queueNameSource == null)
        {
            if (other.queueNameSource != null) return false;
        }
        else if (!queueNameSource.equals(other.queueNameSource)) return false;
        if (refRunbook == null)
        {
            if (other.refRunbook != null) return false;
        }
        else if (!refRunbook.equals(other.refRunbook)) return false;
        if (repeat == null)
        {
            if (other.repeat != null) return false;
        }
        else if (!repeat.equals(other.repeat)) return false;
        if (sampleOutput == null)
        {
            if (other.sampleOutput != null) return false;
        }
        else if (!sampleOutput.equals(other.sampleOutput)) return false;
        if (severities == null)
        {
            if (other.severities != null) return false;
        }
        else if (!severities.equals(other.severities)) return false;
        if (summary == null)
        {
            if (other.summary != null) return false;
        }
        else if (!summary.equals(other.summary)) return false;
        if (timeout == null)
        {
            if (other.timeout != null) return false;
        }
		if (variables == null)
		{
			if (other.variables != null) return false;
		}
        else if (!variables.equals(other.variables)) return false;
        if (refRunbook == null)
        {
            if (other.refRunbook != null) return false;
        }
        else if (!refRunbook.equals(other.refRunbook)) return false;
        if (refRunbookParams == null)
        {
            if (other.refRunbookParams != null) return false;
        }
        return true;
    }
    
    public String getCommandUrl()
    {
        return commandUrl;
    }

    public void setCommandUrl(String commandUrl)
    {
        this.commandUrl = commandUrl;
    }

    public String getMethod()
    {
        return method;
    }

    public void setMethod(String method)
    {
        this.method = method;
    }

    public List<RBTaskHeaderVO> getHeaders()
    {
        return headers;
    }

    public void setHeaders(List<RBTaskHeaderVO> headers)
    {
        this.headers = headers;
    }

    @Override
    public String toString()
    {
        return "RBTaskVO [name=" + name + ", entityType=" + entityType + 
               ", parentId=" + (parentId != null ? parentId : "") + ", parentType=" + (parentType != null ? parentType : "") + 
               ", connectionType=" + (connectionType != null ? connectionType : "") + ", order=" + order + 
               ", command=" + (command != null ? command : "") + ", parserType=" + (parserType != null ? parserType : "") + 
               ", sampleOutput=" + (sampleOutput != null ? sampleOutput : "") + ", repeat=" + repeat + 
               ", description=" + (description != null ? description : "") + ", summary=" + (summary != null ? summary : "") + 
               ", detail=" + (detail != null ? detail : "") + ", defaultAssess=" + (defaultAssess != null ? defaultAssess : "") + 
               ", defaultSeverity=" + (defaultSeverity != null ? defaultSeverity : "") + ", continuation=" + (continuation != null ? continuation : "") +
               ", queueName=" + (queueName != null ? queueName : "") + ", queueNameSource=" + (queueNameSource != null ? queueNameSource : "") + 
               ", parserConfig=" + (parserConfig != null ? parserConfig : "") + ", assessorScript=" + (assessorScript != null ? assessorScript : "") + 
               ", parserScript=" + (parserScript != null ? parserScript : "") + ", refRunbook=" + (refRunbook != null ? refRunbook : "") +
               ", commandUrl=" + (commandUrl != null ? commandUrl : "") + ", method=" + (method != null ? method : "") + 
               ", headers =" + (headers != null ? headers : "") + ", body =" + (body != null ? body : "") + 
               ", refRunbookParams=" + (refRunbookParams != null ? refRunbookParams : "") +
               ", conditions=" + (conditions != null ? conditions : "") + ", severities=" + (severities != null ? severities : "") + 
               ", variables=" + (variables != null ? variables : "") + "]";
    }
}
