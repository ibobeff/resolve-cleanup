/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.List;

import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
public class RBConditionVO extends VO
{
    private static final long serialVersionUID = 2602493061762253410L;
	private String entityType = "condition";
    private String expression; // Transient groovy expression for the condition, do we need this?
    private String parentId; //connectionId
    private String parentType; //currently only "connection" 
    private Double order;
    
    private List<RBCriterionVO> criteria;
    
    public RBConditionVO()
    {
        super();
    }

    public String getEntityType()
    {
        return entityType;
    }
    public void setEntityType(String entityType)
    {
        this.entityType = entityType;
    }

    public String getExpression()
    {
        return expression;
    }

    public void setExpression(String expression)
    {
        this.expression = expression;
    }

    public String getParentId()
    {
        return parentId;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    public String getParentType()
    {
        return parentType;
    }

    public void setParentType(String parentType)
    {
        this.parentType = parentType;
    }

    public Double getOrder()
    {
        return order;
    }

    public void setOrder(Double order)
    {
        this.order = order;
    }

    public List<RBCriterionVO> getCriteria()
    {
        return criteria;
    }

    public void setCriteria(List<RBCriterionVO> criteria)
    {
        this.criteria = criteria;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((order == null) ? 0 : order.hashCode());
        result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
        result = prime * result + ((parentType == null) ? 0 : parentType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        RBConditionVO other = (RBConditionVO) obj;
        if (order == null)
        {
            if (other.order != null) return false;
        }
        else if (!order.equals(other.order)) return false;
        if (parentId == null)
        {
            if (other.parentId != null) return false;
        }
        else if (!parentId.equals(other.parentId)) return false;
        if (parentType == null)
        {
            if (other.parentType != null) return false;
        }
        else if (!parentType.equals(other.parentType)) return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "RBConditionVO [parentId=" + parentId + ", parentType=" + parentType + ", order=" + order + ", toString()=" + super.toString() + "]";
    }
}
