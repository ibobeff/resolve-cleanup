/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.List;
import java.util.Set;

import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

public class SysPerspectiveVO extends VO
{
    private static final long serialVersionUID = 2216240621063341556L;
    
    private String name;
    private String application;
    private Integer sequence;
    private Boolean active;

    // object referenced by
    private List<SysPerspectiverolesVO> sysPerspectiveroles;
    private List<SysPerspectiveapplicationsVO> sysPerspectiveapplications;
    
    //for UI
    private Set<String> roles;
    private List<String> applications;
	
    public SysPerspectiveVO()
    {
    }
    
    @Override
    public boolean validate()
    {
        boolean valid = true;
        
        if(StringUtils.isEmpty(getName()) || (StringUtils.isNotBlank(getName()) && getName().equalsIgnoreCase(VO.STRING_DEFAULT)))
        {
            valid = false;
            throw new RuntimeException("Name cannot be empty.");
        }
        
        valid = getName().matches(HibernateConstants.REGEX_ALPHANUMERIC_WITH_SPACE);
        if(!valid)
        {
            throw new RuntimeException("Name can have only Alpha numeric, space and _.");
        }
        
        return valid;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getApplication()
    {
        return this.application;
    }

    public void setApplication(String application)
    {
        this.application = application;
    }

    public Integer getSequence()
    {
        return this.sequence;
    }

    public void setSequence(Integer sequence)
    {
        this.sequence = sequence;
    }
    
    public Boolean getActive()
    {
        return this.active;
    }

    public void setActive(Boolean active)
    {
        this.active = active;
    }

    public List<SysPerspectiverolesVO> getSysPerspectiveroles()
    {
        return sysPerspectiveroles;
    }

    public void setSysPerspectiveroles(List<SysPerspectiverolesVO> sysPerspectiveroles)
    {
        this.sysPerspectiveroles = sysPerspectiveroles;
    }


    public List<SysPerspectiveapplicationsVO> getSysPerspectiveapplications()
    {
        return sysPerspectiveapplications;
    }

    public void setSysPerspectiveapplications(List<SysPerspectiveapplicationsVO> sysPerspectiveapplications)
    {
        this.sysPerspectiveapplications = sysPerspectiveapplications;
    }

    public Set<String> getRoles()
    {
        return roles;
    }

    public void setRoles(Set<String> roles)
    {
        this.roles = roles;
    }

    public List<String> getApplications()
    {
        return applications;
    }

    public void setApplications(List<String> applications)
    {
        this.applications = applications;
    }
    
    

}
