/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.List;

import com.resolve.services.interfaces.VO;

@SuppressWarnings("serial")
public class RBConnectionVO extends VO
{
    private static final long serialVersionUID = 9183166627137369782L;
	private String name;
    private String type; //SSH, Telnet
    private String entityType = "connection";
    private String parentId; //Loop Id
    private String parentType; //"loop" or "builder" (for the root)
    private Double order;
    
    private List<RBConnectionParamVO> params;
    
    public RBConnectionVO()
    {
        super();
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getEntityType()
    {
        return entityType;
    }
    public void setEntityType(String entityType)
    {
        this.entityType = entityType;
    }

    public String getParentId()
    {
        return parentId;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    public String getParentType()
    {
        return parentType;
    }

    public void setParentType(String parentType)
    {
        this.parentType = parentType;
    }

    public Double getOrder()
    {
        return order;
    }

    public void setOrder(Double order)
    {
        this.order = order;
    }

    public List<RBConnectionParamVO> getParams()
    {
        return params;
    }

    public void setParams(List<RBConnectionParamVO> params)
    {
        this.params = params;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((order == null) ? 0 : order.hashCode());
        result = prime * result + ((params == null) ? 0 : params.hashCode());
        result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
        result = prime * result + ((parentType == null) ? 0 : parentType.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        RBConnectionVO other = (RBConnectionVO) obj;
        if (name == null)
        {
            if (other.name != null) return false;
        }
        else if (!name.equals(other.name)) return false;
        if (order == null)
        {
            if (other.order != null) return false;
        }
        else if (!order.equals(other.order)) return false;
        if (params == null)
        {
            if (other.params != null) return false;
        }
        else if (!params.equals(other.params)) return false;
        if (parentId == null)
        {
            if (other.parentId != null) return false;
        }
        else if (!parentId.equals(other.parentId)) return false;
        if (parentType == null)
        {
            if (other.parentType != null) return false;
        }
        else if (!parentType.equals(other.parentType)) return false;
        if (type == null)
        {
            if (other.type != null) return false;
        }
        else if (!type.equals(other.type)) return false;
        return true;
    }
}
