/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;


public class WikidocResolveTagRelVO  extends VO
{
    private static final long serialVersionUID = -4418889158683521580L;

    // object references
    private WikiDocumentVO wikidoc;
    private ResolveTagVO tag;
	
    public WikidocResolveTagRelVO()
    {
    }
    
    public WikidocResolveTagRelVO(String sysId)
    {
        super.setSys_id(sysId);
    }

    public WikiDocumentVO getWikidoc()
    {
        return wikidoc;
    }

    public void setWikidoc(WikiDocumentVO wikidoc)
    {
        this.wikidoc = wikidoc;
    }
    
    public ResolveTagVO getTag()
    {
        return tag;
    }

    public void setTag(ResolveTagVO tag)
    {
        this.tag = tag;
    }
}
