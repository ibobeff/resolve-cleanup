package com.resolve.services.hibernate.vo;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

import static com.resolve.util.StringUtils.*;

import com.resolve.services.interfaces.VO;

public class AggrExecSummaryVO extends VO implements Comparable<AggrExecSummaryVO> {
    
	private static final long serialVersionUID = 8420097177509893692L;
	
	public static final Long NO_USAGE_COUNT = VO.NON_NEGATIVE_LONG_DEFAULT;	
	public static final String START_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	
	private Date startDate;				// start date (UTC)
	private String orgId;				// org id ("None" for No Org)
	private String rbcSource;			// RBC source ("None" for missing RBC source)
	private Long usageCount;			// usage count
    
    public AggrExecSummaryVO() {}
    
    public Date getStartDate() {
        return startDate;
    }
    
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    public String getOrgId() {
    	return orgId;
    }
    
    public void setOrgId(String orgId) {
    	this.orgId = orgId;
    }
    
    public String getRbcSource() {
    	return rbcSource;
    }
    
    public void setRbcSource(String rbcSource) {
    	this.rbcSource = rbcSource;
    }
    
    public Long getUsageCount() {
    	return usageCount;
    }
    
    public Long ugetUsageCount() {
    	return getUsageCount() != null ? getUsageCount() : NO_USAGE_COUNT;
    }
    
    public void setUsageCount(Long usageCount) {
    	this.usageCount = usageCount;
    }
    
    @Override
    public String toString() {
    	SimpleDateFormat sdf = new SimpleDateFormat(START_DATE_FORMAT);
    	sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
    	
        return "Aggregated Execution Summary VO [" +
        	   "Start Date: " + sdf.format(startDate) + 
        	   ", Org: " + (isNotBlank(orgId) ? orgId : "Blank or null") +
        	   ", RBC Source: " + (isNotBlank(rbcSource) ? rbcSource : "Blank or null") + 
        	   ", Usage Count: " + ugetUsageCount() +
               "]";
    }

	@Override
	public int compareTo(AggrExecSummaryVO o) throws NullPointerException {
		if (o == null) {
			throw new NullPointerException();
		}
		
		// Compare Start Dates
		
		if (getStartDate() != null && o.getStartDate() == null) {
			return 1;
		}
		
		if (getStartDate() == null && o.getStartDate() != null) {
			return -1;
		}
		
		if (getStartDate() != null && o.getStartDate() != null && 
			getStartDate().compareTo(o.getStartDate()) != 0) {
			return getStartDate().compareTo(o.getStartDate());
		}
		
		// Compare Org Ids
		
		if (isNotBlank(getOrgId()) && isBlank(o.getOrgId())) {
			return 1;
		}
		
		if (isBlank(getOrgId()) && isNotBlank(o.getOrgId())) {
			return -11;
		}
		
		if (isNotBlank(getOrgId()) && isNotBlank(o.getOrgId()) &&
		    getOrgId().compareTo(o.getOrgId()) != 0) {
			return getOrgId().compareTo(o.getOrgId());
		}
		
		// Compare RBC Source
		
		if (isNotBlank(getRbcSource()) && isBlank(o.getRbcSource())) {
			return 1;
		}
		
		if (isBlank(getRbcSource()) && isNotBlank(o.getRbcSource())) {
			return -11;
		}
		
		if (isNotBlank(getRbcSource()) && isNotBlank(o.getRbcSource()) &&
			getRbcSource().compareTo(o.getRbcSource()) != 0) {
			return getRbcSource().compareTo(o.getRbcSource());
		}
		
		return 0;
	}
}
