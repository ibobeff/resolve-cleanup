package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveCaseConfigVO extends VO
{
    private static final long serialVersionUID = 3887597687543575317L;
    
    private String configType;
    private String configValue;
    private Integer priorityRank;
    private String attribValue;
    private String iocType;
    private String iocKey;
    private String iocDisplayName;
    private String iocSource;
    private Boolean iocRegex;
    private Boolean active;
    
    public String getConfigType()
    {
        return configType;
    }
    public void setConfigType(String configType)
    {
        this.configType = configType;
    }
    
    public String getConfigValue()
    {
        return configValue;
    }
    public void setConfigValue(String configValue)
    {
        this.configValue = configValue;
    }
    
    public Integer getPriorityRank()
    {
        return priorityRank;
    }
    public void setPriorityRank(Integer priorityRank)
    {
        this.priorityRank = priorityRank;
    }
    
    public String getAttribValue()
    {
        return attribValue;
    }
    public void setAttribValue(String attribValue)
    {
        this.attribValue = attribValue;
    }
    
    public String getIocType()
    {
        return iocType;
    }
    public void setIocType(String iocType)
    {
        this.iocType = iocType;
    }
    
    public String getIocKey()
    {
        return iocKey;
    }
    public void setIocKey(String iocKey)
    {
        this.iocKey = iocKey;
    }
    
    public String getIocDisplayName()
    {
        return iocDisplayName;
    }
    public void setIocDisplayName(String iocDisplayName)
    {
        this.iocDisplayName = iocDisplayName;
    }
    
    public String getIocSource()
    {
        return iocSource;
    }
    public void setIocSource(String iocSource)
    {
        this.iocSource = iocSource;
    }
    
    public Boolean getIocRegex()
    {
        return iocRegex;
    }
    public void setIocRegex(Boolean iocRegex)
    {
        this.iocRegex = iocRegex;
    }
    
    public Boolean getActive()
    {
        return active;
    }
    public void setActive(Boolean active)
    {
        this.active = active;
    }
}
