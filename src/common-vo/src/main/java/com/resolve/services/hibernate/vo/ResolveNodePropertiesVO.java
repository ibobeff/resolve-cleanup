/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ResolveNodePropertiesVO extends VO
{
    private static final long serialVersionUID = 798246469484094457L;
    
    private String UKey;
    private String UValue;

    private ResolveNodeVO node;

    public ResolveNodePropertiesVO() {}
    public ResolveNodePropertiesVO(String sysId) 
    {
        this.setSys_id(sysId);
    }

    
    public String getUKey()
    {
        return UKey;
    }

    public void setUKey(String key)
    {
        this.UKey = key;
    }

    public String getUValue()
    {
        return UValue;
    }

    public void setUValue(String value)
    {
        this.UValue = value;
    }
    
    public ResolveNodeVO getNode()
    {
        return node;
    }

    public void setNode(ResolveNodeVO node)
    {
        this.node = node;
    }

}
