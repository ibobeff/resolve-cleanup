/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.menu;

import java.util.List;
import java.util.Map;

import com.resolve.services.vo.ResolveHashMap;


public class MenuSetModel extends ResolveHashMap
{
    private static final long serialVersionUID = -7052932270871431532L;

    public MenuSetModel() 
	  {
	  } //MenuSet
	  
	  public MenuSetModel(String name, String applications, String roles, String menuSetID)
	  {
		  this.setName(name);
		  this.setApplications(applications);
		  this.setRoles(roles);
		  this.setMenuSetID(menuSetID);
		  
	  } //MenuSet
	  
	  public String getName() 
	  {
	    return (String) get("name");
	  } //getName
	  
	  public void setName(String name)
	  {
		  set("name", name);
	  } //setName
	  
	  public Boolean isActive() 
      {
        return (Boolean) get("active");
      } //getName
      
      public void setIsActive(Boolean isActive)
      {
          set("active", isActive);
      } //setName

	  public void setApplications(String applications) 
	  {
	    set("applications", applications);
	  } //setApplications

	  public String getApplications() 
	  {
	    return (String) get("applications");
	  } //getApplications

	  public void setRoles(String roles) 
	  {
	    set("roles", roles);
	  } //setRoles

	  public String getRoles() 
	  {
	    return (String) get("roles");
	  } //getRoles
	  
	  public void setMenuSetID(String menuSetID)
	  {
		  set("menuSetLink", menuSetID);
	  }// setMenuSetID
	  
	  public String getMenuSetID()
	  {
		  return (String) get("menuSetLink");
	  }// getMenuSetID
	  
	  public void setSequence(Integer sequence)
	  {
		  set("sequence", sequence);
	  } //setSequence
	  
	  public Integer getSequence()
	  {
		  return (Integer) get("sequence");
	  }//getSequence
	 
	  public void setMenuSetApplicationsList(List<Map<String, String>> menuSetApplicationsList)
	  {
		  set("menuSetApplicationsList", menuSetApplicationsList);
	  }//setMenuSetApplicationsList
	  
	  public List<Map<String, String>> getMenuSetApplicationsList()
	  {
		  return (List<Map<String, String>>) get("menuSetApplicationsList");
	  }//getMenuSetApplicationsList
	  
	  public void setMenuSetRoles(List<Map<String, String>> menuSetRolesList)
	  {
		  set("menuSetRolesList", menuSetRolesList);
	  } // setMenuSetRoles
	  
	  public List<Map<String, String>> getMenuSetRolesList()
	  {
		  return (List<Map<String, String>>) get("menuSetRolesList");
	  }	//getMenuSetRolesList  
	  
	  public void setAvailableApplicationsList(List<Map<String, String>> availableApplicationsList)
	  {
		  set("availableApplicationsList", availableApplicationsList);
	  }
	  
	  public List<Map<String, String>> getAvailableApplicationsList()
	  {
		  return (List<Map<String, String>>) get("availableApplicationsList");
	  }
	  
	  public void setAvailableRolesList(List<Map<String, String>> availableRolesList)
	  {
		  set("availableRoles", availableRolesList);
	  }
	  
	  public List<Map<String, String>> getAvailableRolesList()
	  {
		  return (List<Map<String, String>>) get("availableRoles");
	  }
 }

