package com.resolve.services.hibernate.vo;

import java.util.Date;

import com.resolve.services.interfaces.VO;

public class ArchiveSirActivityVO extends VO
{
	private static final long serialVersionUID = -1722107962338349762L;
	
	private String activityId;
    private String altActivityId;
    private String worksheetId;
    private String incidentId;
    private Date dueDate;
    private String status;
    private String assignee;
    
    public String getActivityId()
    {
        return activityId;
    }
    public void setActivityId(String activityId)
    {
        this.activityId = activityId;
    }
    
    public String getAltActivityId()
    {
        return altActivityId;
    }
    public void setAltActivityId(String altActivityId)
    {
        this.altActivityId = altActivityId;
    }
    
    public String getWorksheetId()
    {
        return worksheetId;
    }
    public void setWorksheetId(String worksheetId)
    {
        this.worksheetId = worksheetId;
    }
    
    public String getIncidentId()
    {
        return incidentId;
    }
    public void setIncidentId(String incidentId)
    {
        this.incidentId = incidentId;
    }
    
    public Date getDueDate()
    {
        return dueDate;
    }
    public void setDueDate(Date dueDate)
    {
        this.dueDate = dueDate;
    }
    
    public String getStatus()
    {
        return status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }
    
    public String getAssignee()
    {
        return assignee;
    }
    public void setAssignee(String assignee)
    {
        this.assignee = assignee;
    }
}
