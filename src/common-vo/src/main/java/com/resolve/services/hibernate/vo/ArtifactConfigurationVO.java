package com.resolve.services.hibernate.vo;

import com.resolve.services.interfaces.VO;

public class ArtifactConfigurationVO extends VO {
	private static final long serialVersionUID = 3665847006238507995L;

	private String UName;
	private ArtifactTypeVO artifactType;
	private ResolveActionTaskVO task;
	private WikiDocumentVO automation;
	private String param;

	public String getUName() {
		return UName;
	}

	public void setUName(String uName) {
		UName = uName;
	}

	public ArtifactTypeVO getArtifactType() {
		return artifactType;
	}

	public void setArtifactType(ArtifactTypeVO artifactType) {
		this.artifactType = artifactType;
	}

	public ResolveActionTaskVO getTask() {
		return task;
	}

	public void setTask(ResolveActionTaskVO task) {
		this.task = task;
	}

	public WikiDocumentVO getAutomation() {
		return automation;
	}

	public void setAutomation(WikiDocumentVO automation) {
		this.automation = automation;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}
}
