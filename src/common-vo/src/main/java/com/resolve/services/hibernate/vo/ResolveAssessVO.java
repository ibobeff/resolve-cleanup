/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.interfaces.CdataProperty;
import com.resolve.services.interfaces.IndexComponent;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.ATReferenceDTO;
import com.resolve.util.StringUtils;

public class ResolveAssessVO extends VO implements IndexComponent, CdataProperty
{
    private static final long serialVersionUID = -8219101224364013680L;

    public final static String RESOURCE_TYPE = "resolveassess";

    private String UName;
    private String UScript;
    private String UDescription;
    private Boolean UParse;
    private Boolean UOnlyCompleted;
    private String UExpressionScript;
    private String UOutputMappingScript;
    private String summaryFormat;
    private String detailFormat;
    private String summaryRule;
    private String detailRule;

    // object referenced by
    private Collection<ResolveActionInvocVO> resolveActionInvocs;
    private Collection<ResolveAssessRelVO> resolveAssessRels;
    
    private Set<String> refAssessors = new HashSet<String>();
    private Set<ATReferenceDTO> references = new HashSet<ATReferenceDTO>();
    
//    private AccessRightsVO accessRights;

    public ResolveAssessVO()
    {
    }
    
    public boolean validate()
    {
        boolean valid = true;

        if(StringUtils.isEmpty(getUName()))
        {
            valid = false;
            throw new RuntimeException("Name is mandatory");
        }

        valid = getUName().matches(HibernateConstants.REGEX_ACTIONTASK_FULLNAME_VALIDATION);
        if(!valid)
        {
            throw new RuntimeException("Name can have only alphanumerics,'_','#','-' and '.'");
        }
        

        return valid;
    }
    
    public String getUName()
    {
        return this.UName;
    }

    public void setUName(String UName)
    {
        this.UName = UName;
    }

    public String getUScript()
    {
        return this.UScript;
    }

    public void setUScript(String UScript)
    {
        this.UScript = UScript;
    }
    
    public String getUDescription()
    {
        return this.UDescription;
    }

    public void setUDescription(String UDescription)
    {
        this.UDescription = UDescription;
    }

    public Boolean getUParse()
    {
        return this.UParse;
    }

    public void setUParse(Boolean UParse)
    {
        this.UParse = UParse;
    }

    public Boolean getUOnlyCompleted()
    {
        return this.UOnlyCompleted;
    }

    public Boolean ugetUOnlyCompleted()
    {
        if (this.UOnlyCompleted == null)
        {
            return new Boolean(false);
        }
        else
        {
            return this.UOnlyCompleted;
        }
    } // ugetUOnlyCOmpleted

    public void setUOnlyCompleted(Boolean UOnlyCompleted)
    {
        this.UOnlyCompleted = UOnlyCompleted;
    }

    
    public String getUExpressionScript()
	{
		return UExpressionScript;
	}

	public void setUExpressionScript(String uExpressionScript)
	{
		UExpressionScript = uExpressionScript;
	}

	public String getUOutputMappingScript()
	{
		return UOutputMappingScript;
	}

	public void setUOutputMappingScript(String uOutputMappingScript)
	{
		UOutputMappingScript = uOutputMappingScript;
	}
	
	public String getSummaryFormat()
	{
		return summaryFormat;
	}

	public void setSummaryFormat(String summaryFormat)
	{
		this.summaryFormat = summaryFormat;
	}

	public String getDetailFormat()
	{
		return detailFormat;
	}

	public void setDetailFormat(String detailFormat)
	{
		this.detailFormat = detailFormat;
	}

	public String getSummaryRule()
	{
		return summaryRule;
	}
	public void setSummaryRule(String summaryRule)
	{
		this.summaryRule = summaryRule;
	}

	public String getDetailRule()
	{
		return detailRule;
	}
	public void setDetailRule(String detailRule)
	{
		this.detailRule = detailRule;
	}

	public Collection<ResolveActionInvocVO> getResolveActionInvocs()
    {
        return resolveActionInvocs;
    }

    public void setResolveActionInvocs(Collection<ResolveActionInvocVO> resolveActionInvocs)
    {
        this.resolveActionInvocs = resolveActionInvocs;
    }

//    public AccessRightsVO getAccessRights()
//    {
//        return accessRights;
//    }
//    
//    public void setAccessRights(AccessRightsVO accessRights)
//    {
//        this.accessRights = accessRights;
//        
//        // one-way only - no collections on the other side
//    }

    public Collection<ResolveAssessRelVO> getResolveAssessRels()
    {
        return resolveAssessRels;
    }

    public void setResolveAssessRels(Collection<ResolveAssessRelVO> resolveAssessRels)
    {
        this.resolveAssessRels = resolveAssessRels;
    }
    
    @Override
    public String toString()
    {
        StringBuffer str = new StringBuffer();

        str.append("Assessor Name:").append(UName).append("\n");
        str.append("Assessor Description:").append(UDescription).append("\n");

        return str.toString();
    }
    
    /**
     * method for getting a copy ...similar to clone()
     * 
     * @return
     */
    public ResolveAssessVO copy()
    {
    	ResolveAssessVO copy = new ResolveAssessVO();
    	copy.setSys_id(getSys_id());
    	copy.setSysCreatedBy(getSysCreatedBy());
    	copy.setSysCreatedOn(getSysCreatedOn());
    	copy.setSysUpdatedBy(getSysUpdatedBy());
    	copy.setSysUpdatedOn(getSysUpdatedOn());
    	
    	copy.setUDescription(UDescription);
    	copy.setUName(UName);
    	copy.setUOnlyCompleted(UOnlyCompleted);
    	copy.setUParse(UParse);
    	copy.setUScript(UScript);
    	
    	return copy;
    }

    public String ugetIndexContent()
    {
        return          UName
                + " " + UScript;
    }

    public List<String> ugetCdataProperty()
    {
        List<String> list = new ArrayList<String>();
        list.add("UScript");
        
        return list;
    }//ugetCdataProperty

    public Set<String> getRefAssessors()
    {
        return refAssessors;
    }

    public void setRefAssessors(Set<String> refAssessors)
    {
        this.refAssessors = refAssessors;
    }

    public Set<ATReferenceDTO> getReferences()
    {
        return references;
    }

    public void setReferences(Set<ATReferenceDTO> references)
    {
        this.references = references;
    }
}
