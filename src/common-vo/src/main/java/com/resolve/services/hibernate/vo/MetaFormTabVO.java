/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.vo;

import java.util.Collection;
import java.util.HashSet;

import com.resolve.services.interfaces.VO;

public class MetaFormTabVO extends VO
{
    private static final long serialVersionUID = -3734029570685958938L;
    
    private String UTabName;
    private Integer UOrder;
    private String UPosition;
    private Integer UTabWidth;
    private Boolean UIsSelectedDefault;
    private Integer UNoOfVerticalColumns; //1 or 2
    private String UUrl;//if this has value, the 'metaFormTabFields' will be NULL or EMPTY

    
    // object referenced by
    private MetaFormViewVO metaFormView;
    private Collection<MetaFormTabFieldVO> metaFormTabFields;
    
    private MetaxFormViewPanelVO metaxFormViewPanel;
    
    private Collection<MetaxFieldDependencyVO> metaxFieldDependencys;

    public MetaFormTabVO()
    {
    }
    
    public String getUTabName()
    {
        return UTabName;
    }
    /**
     * @param uTabName the uTabName to set
     */
    public void setUTabName(String uTabName)
    {
        UTabName = uTabName;
    }

    public Integer getUOrder()
    {
        return UOrder;
    }
    /**
     * @param uOrder the uOrder to set
     */
    public void setUOrder(Integer uOrder)
    {
        UOrder = uOrder;
    }

    /**
     * @return the uPosition
     */
    public String getUPosition()
    {
        return UPosition;
    }
    /**
     * @param uPosition the uPosition to set
     */
    public void setUPosition(String uPosition)
    {
        UPosition = uPosition;
    }

    /**
     * @return the uTabWidth
     */
    public Integer getUTabWidth()
    {
        return UTabWidth;
    }
    /**
     * @param uTabWidth the uTabWidth to set
     */
    public void setUTabWidth(Integer uTabWidth)
    {
        UTabWidth = uTabWidth;
    }

    /**
     * @return the uIsSelectedDefault
     */
    public Boolean getUIsSelectedDefault()
    {
        return UIsSelectedDefault;
    }
    /**
     * @param uIsSelectedDefault the uIsSelectedDefault to set
     */
    public void setUIsSelectedDefault(Boolean uIsSelectedDefault)
    {
        UIsSelectedDefault = uIsSelectedDefault;
    }

    
    /**
     * @return the uNoOfVerticalColumns
     */
    public Integer getUNoOfVerticalColumns()
    {
        return UNoOfVerticalColumns;
    }
    public void setUNoOfVerticalColumns(Integer uNoOfVerticalColumns)
    {
        UNoOfVerticalColumns = uNoOfVerticalColumns;
    }

    /**
     * @return the uSource
     */
    public String getUUrl()
    {
        return UUrl;
    }
    public void setUUrl(String uSource)
    {
        UUrl = uSource;
    }

    
     public Collection<MetaFormTabFieldVO> getMetaFormTabFields()
    {
        return this.metaFormTabFields;
    }

    public void setMetaFormTabFields(Collection<MetaFormTabFieldVO> metaFormTabField)
    {
        this.metaFormTabFields = metaFormTabField;
    }

    public MetaFormViewVO getMetaFormView()
    {
        return metaFormView;
    }

    public void setMetaFormView(MetaFormViewVO metaFormView)
    {
        this.metaFormView = metaFormView;

        if (metaFormView != null)
        {
            Collection<MetaFormTabVO> coll = metaFormView.getMetaFormTabs();
            if (coll == null)
            {
                coll = new HashSet<MetaFormTabVO>();
                coll.add(this);
                
                metaFormView.setMetaFormTabs(coll);
            }
        }
    }

 
    public MetaxFormViewPanelVO getMetaxFormViewPanel()
    {
        return metaxFormViewPanel;
    }

    public void setMetaxFormViewPanel(MetaxFormViewPanelVO metaxFormViewPanel)
    {
        this.metaxFormViewPanel = metaxFormViewPanel;
        
        if (metaxFormViewPanel != null)
        {
            Collection<MetaFormTabVO> coll = metaxFormViewPanel.getMetaFormTabs();
            if (coll == null)
            {
                coll = new HashSet<MetaFormTabVO>();
                coll.add(this);
                
                metaxFormViewPanel.setMetaFormTabs(coll);
            }
        }
    }
    
    public Collection<MetaxFieldDependencyVO> getMetaxFieldDependencys()
    {
        return this.metaxFieldDependencys;
    }

    public void setMetaxFieldDependencys(Collection<MetaxFieldDependencyVO> metaxFieldDependencys)
    {
        this.metaxFieldDependencys = metaxFieldDependencys;
    }
    
    

}//MetaFormTab
