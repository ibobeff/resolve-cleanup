/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.dto;

public class ViewLookupTableDTO
{
    private String id;
    private String appName;
    private String view;
    private String roles;
    private int order;
    
    public String getId()
    {
        return id;
    }
    public void setId(String id)
    {
        this.id = id;
    }
    public String getAppName()
    {
        return appName;
    }
    public void setAppName(String appName)
    {
        this.appName = appName;
    }
    public String getView()
    {
        return view;
    }
    public void setView(String view)
    {
        this.view = view;
    }
    public String getRoles()
    {
        return roles;
    }
    public void setRoles(String roles)
    {
        this.roles = roles;
    }
    public int getOrder()
    {
        return order;
    }
    public void setOrder(int order)
    {
        this.order = order;
    }
}
