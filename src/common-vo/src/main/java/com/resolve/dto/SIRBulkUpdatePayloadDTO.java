package com.resolve.dto;

import java.io.Serializable;
import java.util.List;
import java.util.NoSuchElementException;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SIRBulkUpdatePayloadDTO implements Serializable
{

    private static final long serialVersionUID = 1L;

    private List<String> sysIds;
    private PropertyName propertyName;
    private String value;

    public List<String> getSysIds()
    {
        return sysIds;
    }

    @JsonProperty(required = true)
    public void setSysIds(List<String> sysIds)
    {
        this.sysIds = sysIds;
    }

    public PropertyName getPropertyName()
    {
        return propertyName;
    }

    @JsonProperty(required = true)
    public void setPropertyName(String propertyName)
    {
        this.propertyName = PropertyName.forName(propertyName);
    }

    public String getValue()
    {
        return value;
    }

    @JsonProperty(required = true)
    public void setValue(String value)
    {
        this.value = value;
    }

    public static enum PropertyName
    {
        OWNER("owner"), STATUS("status"), SEVERITY("severity"), MASTER_ID("masterId");

        private final String name;

        PropertyName(String name)
        {
            this.name = name;
        }

        public String getName()
        {
            return this.name;
        }

        public static PropertyName forName(String name)
        {
            if (name == null) throw new IllegalArgumentException("Missing property name in SIR bulk update payload");
            switch (name.toUpperCase())
            {
                case "OWNER":
                    return OWNER;
                case "STATUS":
                    return STATUS;
                case "SEVERITY":
                    return SEVERITY;
                case "MASTERID":
                    return MASTER_ID;
                default:
                    throw new NoSuchElementException("Unknown property name in SIR bulk update payload");
            }
        }
    }
}
