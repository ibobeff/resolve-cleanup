/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.dto;

import java.util.ArrayList;
import java.util.List;


public enum SectionType
{
	WYSIWYG("WYSIWYG"),
	SOURCE("SOURCE"),
	TABLE("TABLE"),
	RELATIONS("RELATIONS"),
	FORM("FORM"),
	REPORTS("REPORTS"),
	IMAGE("IMAGE"),
	ATTACHMENT("ATTACHMENT"),
	URL("URL"),
	INFOBAR("INFOBAR"),
	TABLEOFCONTENTS("TABLEOFCONTENTS");
	
	private final String tagName;
	
	private SectionType(String tagName)
	{
		this.tagName = tagName;
	}
	
	public String getTagName()
	{
		return this.tagName;
	}
	
	public static List<String> getAllSectionTypes()
	{
        List<String> list = new ArrayList<String>();

        for( SectionType type : SectionType.values()) {
	        list.add(type.getTagName());
	    }
        
	    return list;
	}
	
	public static List<ComboBoxModel> getAllSectionTypesModel()
    {
        List<ComboBoxModel> list = new ArrayList<ComboBoxModel>();

        for( SectionType type : SectionType.values()) {
            list.add(new ComboBoxModel(type.getTagName(), type.toString()));
        }
        
        return list;
    }
	
	
	public static SectionType getRevisionType(String tagName)
	{
		for( SectionType type : SectionType.values()) {
		    if( type.toString().equals(tagName))return type;
		}
		return null;	
	}
}
