/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.dto;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.resolve.services.hibernate.vo.SocialPostAttachmentVO;

public class UploadFileDTO
{
    private String sys_id;
    private Date sysCreatedOn;
    private String sysCreatedBy;
    private Date sysUpdatedOn;
    private String sysUpdatedBy;

    private String type;
    private String fileName;
//    private SocialPostAttachmentVO attachment;

    public UploadFileDTO()
    {
    }

    public UploadFileDTO(String id, String type, String fileName)
    {
        this.sys_id = id;
        this.type = type;
        this.fileName = fileName;
    }

    public UploadFileDTO(SocialPostAttachmentVO attachment)
    {
        this.sys_id = attachment.getSys_id();
        this.type = attachment.getUType();
        this.fileName = attachment.getUFilename();
//        this.attachment = attachment;
    }

    public String getSys_id()
    {
        return sys_id;
    }

    public void setSys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }
    
    public Date getSysCreatedOn()
    {
        return sysCreatedOn;
    }

    public void setSysCreatedOn(Date sysCreatedOn)
    {
        this.sysCreatedOn = sysCreatedOn;
    }

    public String getSysCreatedBy()
    {
        return sysCreatedBy;
    }

    public void setSysCreatedBy(String sysCreatedBy)
    {
        this.sysCreatedBy = sysCreatedBy;
    }

    public Date getSysUpdatedOn()
    {
        return sysUpdatedOn;
    }

    public void setSysUpdatedOn(Date sysUpdatedOn)
    {
        this.sysUpdatedOn = sysUpdatedOn;
    }

    public String getSysUpdatedBy()
    {
        return sysUpdatedBy;
    }

    public void setSysUpdatedBy(String sysUpdatedBy)
    {
        this.sysUpdatedBy = sysUpdatedBy;
    }

    public String getUrl()
    {
        String previewLink = "/resolve/service/social/download?preview=true&id=" + this.getSys_id();
        String fullData = "/resolve/service/social/download?id=" + this.getSys_id();
        if (this.isImage())
        {
            return String.format("<img src=\"%s\" style=\"width:200px\" fullData=\"%s\"/>", previewLink, fullData);
        }
        else
        {
            return String.format("<a href=\"\" fullData=\"%s\" dataType=\"%s\">%s</a>", fullData, this.isPDF() ? "pdf" : "", this.getFileName());
        }
    }

    public boolean isImage()
    {
        boolean image = false;
        
        if(StringUtils.isNotBlank(getType()))
        {
            image = (getType().toLowerCase().contains("png") || getType().toLowerCase().contains("jpg") 
                            || getType().toLowerCase().contains("jpeg")  || getType().toLowerCase().contains("jpe") 
                            || getType().toLowerCase().contains("jfif") || getType().toLowerCase().contains("gif"));
        }
        
        return image;
    }

    public boolean isPDF()
    {
        boolean pdf = false;
        
        if(StringUtils.isNotBlank(getType()))
        {
            pdf = getType().toLowerCase().contains("pdf");
        }
        
        return pdf;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }
}
