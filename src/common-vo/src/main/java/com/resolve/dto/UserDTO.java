/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import org.apache.commons.collections.CollectionUtils;

import com.resolve.enums.ColorTheme;
import com.resolve.rsbase.Release;
import com.resolve.services.hibernate.vo.GroupsVO;
import com.resolve.services.hibernate.vo.OrganizationVO;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.RolesVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.menu.MenuDTO;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

public class UserDTO
{
    private String UUserName;
    private Boolean UPasswordNeedsReset;
    private String UPreferredLanguage;
    private String UIntroduction;
    private String UFirstName;
    private String UMiddleName;
    private String ULastName;
    private String UName;
    private String UPhone;
    private String UHomePhone;
    private String UMobilePhone;
    private String UFax;
    private String UEmail;
    private String UIM;
    private String UTitle;
    private String UGender;
    private String UTimeZone;
    private String UDateFormat;
    private String UDateTimeFormat;
    private String UTimeFormat;
    private Date ULastLogin;
    private Boolean ULockedOut;
    private String ULastLoginDevice;
    private String UManager;
    private Integer UNotification;
    private Integer UCalendarIntegration;
    private String UPhoto;
    private String UDefaultPerspective;
    private String ULocation;
    private String UDepartment;
    private String UEduStatus;
    private String UCompany;
    private String USource;
    private String UStreet;
    private String UAddress1;
    private String UAddress2;
    private String UCity;
    private String UState;
    private String UZip;
    private String UCountry;
    private String UEmployeeNumber;
    private Boolean UVip;
    private String UBuilding;
    private Integer UFailedAttempts;
    private String USummary;
    private byte[] UImage;
    private byte[] UTempImage;
    private String UStartPage;
    private String UHomePage;
    private String userSettings;
    //Password Recovery
    private String UQuestion1;
    private String UAnswer1;
    private String UQuestion2;
    private String UAnswer2;
    private String UQuestion3;
    private String UAnswer3;
    
    private String name;
    private String fullName;
    private boolean bannerAllowToggle;
    private String defaultDateFormat;
    private String bannerLogo;
    private String bannerTitle;
    private String bannerURL;
    private int maxRecentlyUsed;
    private String initialScreen;
    private String toolbar;
    private MenuDTO menu;
    private OrganizationVO organization;
    private String[] roles;
    private List<RolesVO> userRoles;
    private String id;
    private String sys_id;
    private String problemId;
    private String problemNumber;
    private String startPage;
    private String homePage;
    private String helpContextUrl;
    private String helpNamespace;
    private Release release;
    private List<Object> notifications;
    private String source;
    private SortedSet<OrgsVO> orgs;
    private SortedSet<OrgsVO> userOrgs;
    private JSONObject permissions;
    private Map<String, String> orgNameToProbIdMap;
    private Map<String, String> probIdToProbNumberMap;
    private Date sysCreatedOn;
    private String sysCreatedBy;
    private Date sysUpdatedOn;
    private String sysUpdatedBy;
    private String sysOrganizationName;
    private String[] groups;
    private List<GroupsVO> userGroups;    
    private String udisplayName;
    private JSONObject newPermissions;
    private String analyticSettings;
    private Integer refreshRate;
    private ColorTheme colorTheme;
    private boolean isDisplayPRB;

    public JSONObject getPermissions()
    {
        return permissions;
    }

    public void setPermissions(JSONObject permissions)
    {
        this.permissions = permissions;
    }

    public UserDTO()
    {
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public boolean isBannerAllowToggle()
    {
        return bannerAllowToggle;
    }

    public void setBannerAllowToggle(boolean bannerAllowToggle)
    {
        this.bannerAllowToggle = bannerAllowToggle;
    }

    public String getDefaultDateFormat()
    {
        return defaultDateFormat;
    }

    public void setDefaultDateFormat(String defaultDateFormat)
    {
        this.defaultDateFormat = defaultDateFormat;
    }

    public String getBannerLogo()
    {
        return bannerLogo;
    }

    public void setBannerLogo(String bannerLogo)
    {
        this.bannerLogo = bannerLogo;
    }

    public String getBannerTitle()
    {
        return bannerTitle;
    }

    public void setBannerTitle(String bannerTitle)
    {
        this.bannerTitle = bannerTitle;
    }

    public String getBannerURL()
    {
        return bannerURL;
    }

    public void setBannerURL(String bannerURL)
    {
        this.bannerURL = bannerURL;
    }

    public int getMaxRecentlyUsed()
    {
        return maxRecentlyUsed;
    }

    public void setMaxRecentlyUsed(int maxRecentlyUsed)
    {
        this.maxRecentlyUsed = maxRecentlyUsed;
    }

    public String getInitialScreen()
    {
        return initialScreen;
    }

    public void setInitialScreen(String initialScreen)
    {
        this.initialScreen = initialScreen;
    }

    public MenuDTO getMenu()
    {
        return menu;
    }

    public void setMenu(MenuDTO menu)
    {
        this.menu = menu;
    }

    public String getUQuestion1() {
        return UQuestion1;
    }

    public void setUQuestion1(String uQuestion1) {
        UQuestion1 = uQuestion1;
    }

    public String getUAnswer1() {
        return UAnswer1;
    }

    public void setUAnswer1(String uAnswer1) {
        UAnswer1 = uAnswer1;
    }

    public String getUQuestion2() {
        return UQuestion2;
    }

    public void setUQuestion2(String uQuestion2) {
        UQuestion2 = uQuestion2;
    }

    public String getUAnswer2() {
        return UAnswer2;
    }

    public void setUAnswer2(String uAnswer2) {
        UAnswer2 = uAnswer2;
    }

    public String getUQuestion3() {
        return UQuestion3;
    }

    public void setUQuestion3(String uQuestion3) {
        UQuestion3 = uQuestion3;
    }

    public String getUAnswer3() {
        return UAnswer3;
    }

    public void setUAnswer3(String uAnswer3) {
        UAnswer3 = uAnswer3;
    }

    public void fromUserVO(UsersVO vo)
    {
        this.setUUserName(vo.getUUserName());
        this.setUPasswordNeedsReset(vo.getUPasswordNeedsReset());
        this.setUPreferredLanguage(vo.getUPreferredLanguage());
        this.setUIntroduction(vo.getUIntroduction());
        this.setUFirstName(vo.getUFirstName());
        this.setUMiddleName(vo.getUMiddleName());
        this.setULastName(vo.getULastName());
        this.setUName(vo.getUName());
        this.setUPhone(vo.getUPhone());
        this.setUHomePhone(vo.getUHomePhone());
        this.setUMobilePhone(vo.getUMobilePhone());
        this.setUFax(vo.getUFax());
        this.setUEmail(vo.getUEmail());
        this.setUIM(vo.getUIM());
        this.setUTitle(vo.getUTitle());
        this.setUGender(vo.getUGender());
        this.setUTimeZone(vo.getUTimeZone());
        this.setUDateFormat(vo.getUDateFormat());
        this.setUDateTimeFormat(vo.getUDateTimeFormat());
        this.setULastLogin(vo.getULastLogin());
        this.setUTimeFormat(vo.getUTimeFormat());
        this.setULockedOut(vo.getULockedOut());
        this.setULastLoginDevice(vo.getULastLoginDevice());
        this.setUManager(vo.getUManager());
        this.setUNotification(vo.getUNotification());
        this.setUCalendarIntegration(vo.getUCalendarIntegration());
        this.setUPhoto(vo.getUPhoto());
        this.setUDefaultPerspective(vo.getUDefaultPerspective());
        this.setULocation(vo.getULocation());
        this.setUDepartment(vo.getUDepartment());
        this.setUEduStatus(vo.getUEduStatus());
        this.setUCompany(vo.getUCompany());
        this.setUSource(vo.getUSource());
        this.setUStreet(vo.getUStreet());
        this.setUAddress1(vo.getUAddress1());
        this.setUAddress2(vo.getUAddress2());
        this.setUCity(vo.getUCity());
        this.setUState(vo.getUState());
        this.setUZip(vo.getUZip());
        this.setUCountry(vo.getUCountry());
        this.setUEmployeeNumber(vo.getUEmployeeNumber());
        this.setUVip(vo.getUVip());
        this.setUBuilding(vo.getUBuilding());
        this.setUFailedAttempts(vo.getUFailedAttempts());
        this.setUSummary(vo.getUSummary());
        this.setUImage(vo.getUImage());
        this.setUTempImage(vo.getUTempImage());
        this.setUStartPage(vo.getUStartPage());
        this.setUHomePage(vo.getUHomePage());
        
        this.setName(vo.getUUserName());
        this.setFullName((StringUtils.isNotBlank(vo.getUFirstName())?vo.getUFirstName() : "" ) + " " + (StringUtils.isNotBlank(vo.getULastName()) ? vo.getULastName() : ""));
        this.setOrganization(vo.getBelongsToOrganization());
        this.setId(vo.getId());
        this.setSys_id(vo.getSys_id());
        this.setUserSettings(vo.getUserSettings());
        this.setUdisplayName(vo.getUDisplayName());
        this.setAnalyticSettings(vo.getAnalyticSettings());
        this.setRefreshRate(vo.getRefreshRate());
        this.setColorTheme(vo.getColorTheme());
        
        //roles
        if(StringUtils.isNotEmpty(vo.getRoles()))
        {
            String[] roles = vo.getRoles().split(",");
            this.setRoles(roles);
        }

        if (!CollectionUtils.isEmpty(vo.getUserRoles())) {
            this.userRoles = new ArrayList<>(vo.getUserRoles());
        }
        
        //groups
        if(StringUtils.isNotEmpty(vo.getGroups()))
        {
            String[] groups = vo.getGroups().split(",");
            this.groups = groups;
        }

        if (!CollectionUtils.isEmpty(vo.getUserGroups())) {
            this.userGroups = new ArrayList<>(vo.getUserGroups());
        }

        // orgs
        if (!CollectionUtils.isEmpty(vo.getUserOrgs())) {
            this.setOrgs(vo.getUserOrgs());
            this.setUserOrgs(vo.getUserOrgs());
        }
        
        // sys properties
        this.sysCreatedBy = vo.getSysCreatedBy();
        this.sysCreatedOn = vo.getSysCreatedOn();
        this.sysUpdatedBy = vo.getSysUpdatedBy();
        this.sysUpdatedOn = vo.getSysUpdatedOn();
        this.sysOrganizationName = vo.getSysOrganizationName();
        
        if (StringUtils.isNotEmpty(vo.getUAnswer1())) {
            this.UAnswer1 = VO.STARS;
        }

        if (StringUtils.isNotEmpty(vo.getUAnswer2())) {
            this.UAnswer2 = VO.STARS;
        }

        if (StringUtils.isNotEmpty(vo.getUAnswer3())) {
            this.UAnswer3 = VO.STARS;
        }

        if (StringUtils.isNotEmpty(vo.getUQuestion1())) {
            this.UQuestion1 = VO.STARS;
        }

        if (StringUtils.isNotEmpty(vo.getUQuestion2())) {
            this.UQuestion2 = VO.STARS;
        }

        if (StringUtils.isNotEmpty(vo.getUQuestion3())) {
            this.UQuestion3 = VO.STARS;
        }
    }

    public String getToolbar()
    {
        return toolbar;
    }

    public void setToolbar(String toolbar)
    {
        this.toolbar = toolbar;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public OrganizationVO getOrganization()
    {
        return organization;
    }

    public void setOrganization(OrganizationVO organization)
    {
        this.organization = organization;
    }

    public String[] getRoles()
    {
        return roles;
    }

    public void setRoles(String[] roles)
    {
        this.roles = roles;
    }

    public List<RolesVO> getUserRoles()
    {
        return this.userRoles;
    }

    public boolean isDisplayPRB()
    {
        return isDisplayPRB;
    }
    
    public void setDisplayPRB(boolean isDisplayPRB) {
        this.isDisplayPRB = isDisplayPRB;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getProblemId()
    {
        return problemId;
    }

    public void setProblemId(String problemId)
    {
        this.problemId = problemId;
    }

    public String getProblemNumber()
    {
        return problemNumber;
    }

    public void setProblemNumber(String problemNumber)
    {
        this.problemNumber = problemNumber;
    }

    public String getStartPage()
    {
        return startPage;
    }

    public void setStartPage(String startPage)
    {
        this.startPage = startPage;
    }

    public String getHomePage()
    {
        return homePage;
    }

    public void setHomePage(String homePage)
    {
        this.homePage = homePage;
    }

    public String getHelpContextUrl()
    {
        return helpContextUrl;
    }

    public void setHelpContextUrl(String helpContextUrl)
    {
        this.helpContextUrl = helpContextUrl;
    }

    public String getHelpNamespace()
    {
        return helpNamespace;
    }

    public void setHelpNamespace(String helpNamespace)
    {
        this.helpNamespace = helpNamespace;
    }

    public Release getRelease()
    {
        return release;
    }

    public void setRelease(Release release)
    {
        this.release = release;
    }

    public List<Object> getNotifications()
    {
        return notifications;
    }

    public void setNotifications(List<Object> socialStreams)
    {
        this.notifications = socialStreams;
    }
    
    public String getSource()
    {
        return source;
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public String getUserSettings()
    {
        return userSettings;
    }

    public void setUserSettings(String userSettings)
    {
        this.userSettings = userSettings;
    }
    
    public SortedSet<OrgsVO> getOrgs()
    {
        return orgs;
    }

    public void setOrgs(SortedSet<OrgsVO> orgs)
    {
        this.orgs = orgs;
    }
    
    public SortedSet<OrgsVO> getUserOrgs()
    {
        return userOrgs;
    }

    public void setUserOrgs(SortedSet<OrgsVO> userOrgs)
    {
        this.userOrgs = userOrgs;
    }
    
    public Map<String, String> getOrgNameToProbIdMap()
    {
        return orgNameToProbIdMap;
    }
    
    public void  setOrgNameToProbIdMap(Map<String, String> orgNameToProbIdMap)
    {
        this.orgNameToProbIdMap = orgNameToProbIdMap;
    }
    
    public Map<String, String> getProbIdToProbNumberMap()
    {
        return probIdToProbNumberMap;
    }
    
    public void  setProbIdToProbNumberMap(Map<String, String> probIdToProbNumberMap)
    {
        this.probIdToProbNumberMap = probIdToProbNumberMap;
    }

    public Date getSysCreatedOn() {
        return sysCreatedOn;
    }

    public void setSysCreatedOn(Date sysCreatedOn) {
        this.sysCreatedOn = sysCreatedOn;
    }

    public String getSysCreatedBy() {
        return sysCreatedBy;
    }

    public void setSysCreatedBy(String sysCreatedBy) {
        this.sysCreatedBy = sysCreatedBy;
    }

    public Date getSysUpdatedOn() {
        return sysUpdatedOn;
    }

    public void setSysUpdatedOn(Date sysUpdatedOn) {
        this.sysUpdatedOn = sysUpdatedOn;
    }

    public String getSysUpdatedBy() {
        return sysUpdatedBy;
    }

    public void setSysUpdatedBy(String sysUpdatedBy) {
        this.sysUpdatedBy = sysUpdatedBy;
    }

    public String getSysOrganizationName() {
        return sysOrganizationName;
    }

    public void setSysOrganizationName(String sysOrganizationName) {
        this.sysOrganizationName = sysOrganizationName;
    }

    public String getUUserName() {
        return UUserName;
    }

    public void setUUserName(String uUserName) {
        UUserName = uUserName;
    }

    public Boolean getUPasswordNeedsReset() {
        return UPasswordNeedsReset;
    }

    public void setUPasswordNeedsReset(Boolean uPasswordNeedsReset) {
        UPasswordNeedsReset = uPasswordNeedsReset;
    }

    public String getUPreferredLanguage() {
        return UPreferredLanguage;
    }

    public void setUPreferredLanguage(String uPreferredLanguage) {
        UPreferredLanguage = uPreferredLanguage;
    }

    public String getUIntroduction() {
        return UIntroduction;
    }

    public void setUIntroduction(String uIntroduction) {
        UIntroduction = uIntroduction;
    }

    public String getUFirstName() {
        return UFirstName;
    }

    public void setUFirstName(String uFirstName) {
        UFirstName = uFirstName;
    }

    public String getUMiddleName() {
        return UMiddleName;
    }

    public void setUMiddleName(String uMiddleName) {
        UMiddleName = uMiddleName;
    }

    public String getULastName() {
        return ULastName;
    }

    public void setULastName(String uLastName) {
        ULastName = uLastName;
    }

    public String getUName() {
        return UName;
    }

    public void setUName(String uName) {
        UName = uName;
    }

    public String getUPhone() {
        return UPhone;
    }

    public void setUPhone(String uPhone) {
        UPhone = uPhone;
    }

    public String getUHomePhone() {
        return UHomePhone;
    }

    public void setUHomePhone(String uHomePhone) {
        UHomePhone = uHomePhone;
    }

    public String getUMobilePhone() {
        return UMobilePhone;
    }

    public void setUMobilePhone(String uMobilePhone) {
        UMobilePhone = uMobilePhone;
    }

    public String getUFax() {
        return UFax;
    }

    public void setUFax(String uFax) {
        UFax = uFax;
    }

    public String getUEmail() {
        return UEmail;
    }

    public void setUEmail(String uEmail) {
        UEmail = uEmail;
    }

    public String getUIM() {
        return UIM;
    }

    public void setUIM(String uIM) {
        UIM = uIM;
    }

    public String getUTitle() {
        return UTitle;
    }

    public void setUTitle(String uTitle) {
        UTitle = uTitle;
    }

    public String getUGender() {
        return UGender;
    }

    public void setUGender(String uGender) {
        UGender = uGender;
    }

    public String getUTimeZone() {
        return UTimeZone;
    }

    public void setUTimeZone(String uTimeZone) {
        UTimeZone = uTimeZone;
    }

    public String getUDateFormat() {
        return UDateFormat;
    }

    public void setUDateFormat(String uDateFormat) {
        UDateFormat = uDateFormat;
    }

    public String getUDateTimeFormat() {
        return UDateTimeFormat;
    }

    public void setUDateTimeFormat(String uDateTimeFormat) {
        UDateTimeFormat = uDateTimeFormat;
    }

    public String getUTimeFormat() {
        return UTimeFormat;
    }

    public void setUTimeFormat(String uTimeFormat) {
        UTimeFormat = uTimeFormat;
    }

    public Date getULastLogin() {
        return ULastLogin;
    }

    public void setULastLogin(Date uLastLogin) {
        ULastLogin = uLastLogin;
    }

    public Boolean getULockedOut() {
        return ULockedOut;
    }

    public void setULockedOut(Boolean uLockedOut) {
        ULockedOut = uLockedOut;
    }

    public String getULastLoginDevice() {
        return ULastLoginDevice;
    }

    public void setULastLoginDevice(String uLastLoginDevice) {
        ULastLoginDevice = uLastLoginDevice;
    }

    public String getUManager() {
        return UManager;
    }

    public void setUManager(String uManager) {
        UManager = uManager;
    }

    public Integer getUNotification() {
        return UNotification;
    }

    public void setUNotification(Integer uNotification) {
        UNotification = uNotification;
    }

    public Integer getUCalendarIntegration() {
        return UCalendarIntegration;
    }

    public void setUCalendarIntegration(Integer uCalendarIntegration) {
        UCalendarIntegration = uCalendarIntegration;
    }

    public String getUPhoto() {
        return UPhoto;
    }

    public void setUPhoto(String uPhoto) {
        UPhoto = uPhoto;
    }

    public String getUDefaultPerspective() {
        return UDefaultPerspective;
    }

    public void setUDefaultPerspective(String uDefaultPerspective) {
        UDefaultPerspective = uDefaultPerspective;
    }

    public String getULocation() {
        return ULocation;
    }

    public void setULocation(String uLocation) {
        ULocation = uLocation;
    }

    public String getUDepartment() {
        return UDepartment;
    }

    public void setUDepartment(String uDepartment) {
        UDepartment = uDepartment;
    }

    public String getUEduStatus() {
        return UEduStatus;
    }

    public void setUEduStatus(String uEduStatus) {
        UEduStatus = uEduStatus;
    }

    public String getUCompany() {
        return UCompany;
    }

    public void setUCompany(String uCompany) {
        UCompany = uCompany;
    }

    public String getUSource() {
        return USource;
    }

    public void setUSource(String uSource) {
        USource = uSource;
    }

    public String getUStreet() {
        return UStreet;
    }

    public void setUStreet(String uStreet) {
        UStreet = uStreet;
    }

    public String getUAddress1() {
        return UAddress1;
    }

    public void setUAddress1(String uAddress1) {
        UAddress1 = uAddress1;
    }

    public String getUAddress2() {
        return UAddress2;
    }

    public void setUAddress2(String uAddress2) {
        UAddress2 = uAddress2;
    }

    public String getUCity() {
        return UCity;
    }

    public void setUCity(String uCity) {
        UCity = uCity;
    }

    public String getUState() {
        return UState;
    }

    public void setUState(String uState) {
        UState = uState;
    }

    public String getUZip() {
        return UZip;
    }

    public void setUZip(String uZip) {
        UZip = uZip;
    }

    public String getUCountry() {
        return UCountry;
    }

    public void setUCountry(String uCountry) {
        UCountry = uCountry;
    }

    public String getUEmployeeNumber() {
        return UEmployeeNumber;
    }

    public void setUEmployeeNumber(String uEmployeeNumber) {
        UEmployeeNumber = uEmployeeNumber;
    }

    public Boolean getUVip() {
        return UVip;
    }

    public void setUVip(Boolean uVip) {
        UVip = uVip;
    }

    public String getUBuilding() {
        return UBuilding;
    }

    public void setUBuilding(String uBuilding) {
        UBuilding = uBuilding;
    }

    public Integer getUFailedAttempts() {
        return UFailedAttempts;
    }

    public void setUFailedAttempts(Integer uFailedAttempts) {
        UFailedAttempts = uFailedAttempts;
    }

    public String getUSummary() {
        return USummary;
    }

    public void setUSummary(String uSummary) {
        USummary = uSummary;
    }

    public byte[] getUImage() {
        return UImage;
    }

    public void setUImage(byte[] uImage) {
        UImage = uImage;
    }

    public byte[] getUTempImage() {
        return UTempImage;
    }

    public void setUTempImage(byte[] uTempImage) {
        UTempImage = uTempImage;
    }

    public String getUStartPage() {
        return UStartPage;
    }

    public void setUStartPage(String uStartPage) {
        UStartPage = uStartPage;
    }

    public String getUHomePage() {
        return UHomePage;
    }

    public void setUHomePage(String uHomePage) {
        UHomePage = uHomePage;
    }

    public void setUserRoles(List<RolesVO> userRoles) {
        this.userRoles = userRoles;
    }

    public String getSys_id() {
        return sys_id;
    }

    public void setSys_id(String sys_id) {
        this.sys_id = sys_id;
    }
    
    public String[] getGroups()
    {
        return groups;
    }

    public void setGroups(String[] groups)
    {
        this.groups = groups;
    }

    public List<GroupsVO> getUserGroups()
    {
        return userGroups;
    }

    public void setUserGroups(List<GroupsVO> userGroups)
    {
        this.userGroups = userGroups;
    }
    
    public String getUdisplayName() {
        return udisplayName;
    }

    public void setUdisplayName(String udisplayName) {
        this.udisplayName = udisplayName;
    }
    
    public JSONObject getNewPermissions() {
        return newPermissions;
    }

    public void setNewPermissions(JSONObject newPermissions)
    {
        this.newPermissions = newPermissions;
    }

    public String getAnalyticSettings() {
        return analyticSettings;
    }

    public void setAnalyticSettings(String analyticSettings) {
        this.analyticSettings = analyticSettings;
    }

    public Integer getRefreshRate() {
        return refreshRate;
    }

    public void setRefreshRate(Integer refreshRate) {
        this.refreshRate = refreshRate;
    }

    public ColorTheme getColorTheme() {
        return colorTheme;
    }

    public void setColorTheme(ColorTheme colorTheme) {
        this.colorTheme = colorTheme;
    }
}