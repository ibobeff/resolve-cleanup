/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.dto;

import com.resolve.services.vo.ResolveHashMap;

public class SectionModel extends ResolveHashMap
{
    private static final long serialVersionUID = 4824610226096593666L;
    
    public final static String ID  = "id";
    public final static String TYPE  = "type";
    public final static String HEIGHT  = "height";
    public final static String TITLE  = "title";
    public final static String CONTENT  = "content";
    public final static String TYPEMODEL  = "typeModel";
    public final static String IS_DELETE = "isDelete";
    
    
    public final static String ID_PREFIX = "rssection_";
    public final static String TABLE_PREFIX = "/resolve/customtable/customtable.jsp?";//"/resolve/jsp/rstable.jsp?";
    public final static String FORM_VIEWER = "/resolve/formbuilder/formviewer.jsp?";
    public final static String DEFAULT_IFRAME_HEIGHT = "300";

    public String getId()
    {
        return (String) get(ID);
    }

    public void setId(String id)
    {
        set(ID, id);
    }
    
    public String getType()
    {
        return  (String)get(TYPE);
    }

    public void setType(String type)
    {
        set(TYPE, type);
    }
    
    public String getHeight()
    {
        return  (String)get(HEIGHT);
    }

    public void setHeight(String height)
    {
        set(HEIGHT, height);
    }
    
    public String getTitle()
    {
        return  (String)get(TITLE);
    }

    public void setTitle(String title)
    {
        set(TITLE, title);
    }
    
    public String getContent()
    {
        return  (String)get(CONTENT);
    }

    public void setContent(String content)
    {
        set(CONTENT, content);
    }

//    public ResolveBaseModel getTypeModel()
//    {
//        return (ResolveBaseModel) get(TYPEMODEL);
//    }
//
//    public void setContent(ResolveBaseModel typeModel)
//    {
//        set(TYPEMODEL, typeModel);
//    }

    public Boolean isDelete()
    {
        Boolean isDelete = false;
        if(get(IS_DELETE) != null)
        {
            isDelete = (Boolean) get(IS_DELETE);
        }
        return isDelete;
    }
    
    public void setIsDelete(Boolean isDelete)
    {
        set(IS_DELETE, isDelete);
    }
    
}//SectionModel
