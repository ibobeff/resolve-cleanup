/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.dto;

import com.resolve.rsbase.MainBase;

public class AboutDTO
{
    private String version;
    private String year;

    public AboutDTO()
    {
        this.version = MainBase.main.release.version;
        this.year = MainBase.main.release.year;
    }

    public AboutDTO(String version)
    {
        super();
        this.version = version;
    }

    public AboutDTO(String version, String year)
    {
        super();
        this.version = version;
        this.year = year;
    }
    
    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getYear()
    {
        return year;
    }
    
    public void setYear(String year)
    {
        this.year = year;
    }
}
