package com.resolve.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Provides parsing utility methods.
 */
public class ParseUtil {
	private static final Pattern INCLUDE_FORM_PATTERN = Pattern.compile("\\{section:type=procedure(.*?)\\{section\\}",
			Pattern.DOTALL);

	private static final String JSON_ARRAY_PREFIX = "[";
	private static final String JSON_ARRAY_SUFFIX = "]";
	private static final String PROCEDURE = "procedure";
	private static final String JSON_OBJ_PREFIX = "{";
	private static final String JSON_OBJ_SUFFIX = "}";

	/**
	 * Returns the content with in {procedure} in sections of type procedure.
	 * 
	 * @param content
	 * @return
	 */
	public static List<String> getListOfProcedures(final String content) 
	{
		if (StringUtils.isBlank(content)) 
		{
			return Collections.<String>emptyList();
		}

		List<String> list = new ArrayList<String>();

		Matcher matcher = INCLUDE_FORM_PATTERN.matcher(content);
		while (matcher.find()) 
		{
			String str = matcher.group();
			list.add(str);
		}

		return list;
	}

	/**
	 * Returns the list of activity maps by parsing content with in {procedure} in
	 * sections of type procedure.
	 */
	public static List<Map<String, String>> getListOfActivityMaps(final String procedureContent) 
	{
		final String activitiesJSONVal = StringUtils.substringBetween(procedureContent, JSON_ARRAY_PREFIX, 
				JSON_ARRAY_SUFFIX);
		if (StringUtils.isBlank(activitiesJSONVal))
		{
			return Collections.<Map<String, String>> emptyList();
		}
		
		return StringUtils.jsonArrayStringToList(PROCEDURE, JSON_OBJ_PREFIX + "\"" + PROCEDURE + "\":"
				+ JSON_ARRAY_PREFIX + activitiesJSONVal + JSON_ARRAY_SUFFIX + JSON_OBJ_SUFFIX);
	}
}