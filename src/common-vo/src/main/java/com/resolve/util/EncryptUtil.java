package com.resolve.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;

/**
 * Provides SHA-512 password encryption.
 */
public class EncryptUtil {
	private static final String HASH_FUNCTION = "SHA-512";
	
	public static String encryptPassword(final String password)
    {
        if (StringUtils.isBlank(password) || password.charAt(password.length() - 1) == '=') {
        	return StringUtils.EMPTY;
        }
        
        MessageDigest digest = null;
		try {
			digest = MessageDigest.getInstance(HASH_FUNCTION);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(String.format("No such hashing algorithm '%s'", HASH_FUNCTION));
		}
        digest.reset();
        digest.update(password.getBytes());
        byte[] bytes = digest.digest();
        
        // convert to apache encoding
        return new String(Base64.encodeBase64(bytes)); 
    }
}