/******************************************************************************

(C) Copyright 2006
 
Resolve Systems, Inc.
 Delaware, USA

This software may not be given to any party other than authorised
employees of Resolve Systems, Inc.

******************************************************************************/

package com.resolve.rsbase;

import java.io.File;
import java.io.IOException;

import com.resolve.util.FileUtils;
import com.resolve.util.XDoc;

public abstract class ConfigBase
{
    public XDoc xdoc;
    
    public ConfigBase(XDoc xdoc) throws Exception
    {
        this.xdoc = xdoc;
    } // ConfigBase
    
    public abstract void load() throws Exception;
    
    public abstract void save() throws Exception;
    
    public void start() throws Exception
    {
    } // start
    
    public void stop() throws Exception
    {
    } // stop
    
    public static boolean backup(String configFilename, int revisions) throws IOException
    {
        boolean result = false;
        
        for (int i=revisions; i > 0; i--)
        {
            int dstIdx = i;
            int srcIdx = i-1;
            String dstFilename = configFilename+"."+dstIdx;
            String srcFilename;
            if (srcIdx > 0)
            {
	            srcFilename = configFilename+"."+srcIdx;
            }
            else
            {
                srcFilename = configFilename;
            }
            
            File srcFile = FileUtils.getFile(srcFilename);
            if (srcFile.exists())
            {
                File dstFile = FileUtils.getFile(dstFilename);
                FileUtils.copyFile(srcFile, dstFile);
            }
        }
        result = true;
        
        return result;
    } // backup
    
} // ConfigBase