/******************************************************************************

(C) Copyright 2006
 
Resolve Systems, Inc.
 Delaware, USA

This software may not be given to any party other than authorised
employees of Resolve Systems, Inc.

******************************************************************************/

package com.resolve.rsbase;

import java.io.File;

import com.resolve.rsbase.MainBase;
import com.resolve.util.ConfigMap;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.XDoc;

public class ConfigGSE extends ConfigMap
{
    private static final long serialVersionUID = -371269084385503890L;
	public String path = MainBase.main.getProductHome()+"/service";
    
    public ConfigGSE(XDoc config) throws Exception
    {
        super(config);
        
        define("path", STRING, "./GSE/@PATH");
    } // ConfigGSE
    
    public void load() throws Exception
    {
        loadAttributes();
        
        path = path.replace('\\', '/');
        
        File file = FileUtils.getFile(path);
        if (!file.exists() || !file.isDirectory())
        {
            Log.log.error("Unable to initialise ScriptEngine. Invalid GSE path: "+path);
        }
    } // load

    public void save()
    {
        saveAttributes();
    } // save

    public String getPath()
    {
        return path;
    }

    public void setPath(String path)
    {
        this.path = path;
    }
    
} // ConfigGSE
