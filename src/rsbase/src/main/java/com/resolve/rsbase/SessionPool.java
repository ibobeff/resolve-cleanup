package com.resolve.rsbase;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SessionPool extends ConcurrentHashMap<String, SessionMap>
{
    private static final long serialVersionUID = -8100581049067745808L;

	/**
     * The entire method is synchronized since expireSessionMap needed to be synchronized.
     * 
     * @param processId
     * @param processTimeout
     * @param flows
     * @return
     */
    public synchronized SessionMap getSessionMap(String processId, int processTimeout)
    {
        SessionMap result = null;
        
        if (!StringUtils.isEmpty(processId))
        {
	        result = this.get(processId);
	        if (result == null)
	        {
	            Log.log.debug("Creating new session processId: "+processId);
		        result = new SessionMap(processId, processTimeout);
		        
		        this.put(processId, result);
	        }
	        
	        // remove expired SessionMap
	        expireSessionMap();
        }
        
        return result;
    } // getSessionMap
    
    public synchronized void expireSessionMap()
    {
        long currentTime = System.currentTimeMillis();
        
        for (SessionMap sess : this.values())
        {
            if (currentTime > sess.getExpiry())
            {
                String processid = sess.getProcessId();
                
                Log.log.debug("Expiring session processId: "+processid);
                sess.clear();
                
                this.remove(processid);
            }
        }
    } // expireSessionMap
    
    public void removeSessionMap(String processid)
    {
        Log.log.debug("SessionPool SessionMaps: "+this);
        SessionMap sess = this.get(processid);
        if (sess != null)
        {
	        Log.log.debug("Expiring session processId: "+processid);
            sess.clear();
            this.remove(processid);
        }
    } // removeSessionMap
    
    public int sessionMapSize()
    {
        int result = 0;
        long currentTime = System.currentTimeMillis();
        
        for (SessionMap sess : this.values())
        {
            if (currentTime > sess.getExpiry())
            {
                String processid = sess.getProcessId();
                
                Log.log.debug("Expiring session processId: " + processid);
                sess.clear();
                
                this.remove(processid);
            }
            else if (sess.size() > 0)
            {
                result++;
            }
        }
        
        return result;
    }

} // SessionPool
