/******************************************************************************

(C) Copyright 2006
 
Resolve Systems, Inc.
 Delaware, USA

This software may not be given to any party other than authorised
employees of Resolve Systems, Inc.

******************************************************************************/

package com.resolve.rsbase;

import java.util.Iterator;
import java.util.List;
import java.util.Hashtable;

import com.resolve.rsbase.MainBase;
import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;

public class ConfigExecute extends ConfigMap
{
    private static final long serialVersionUID = 2904608650878986539L;
	// static cached classpath result
    static String groovyClasspath = null;
    static String antClasspath = null;
    
    // const
    String execpath = MainBase.main.configGeneral.home;
    
    // config fields
    String jrepath = MainBase.main.configGeneral.home+"/jdk";
    String groovylibpath = MainBase.main.configGeneral.home+"/lib";
    String antlibpath = MainBase.main.configGeneral.home+"/lib";
    
    // list of maps
    List groovylibs;
    List antlibs;
    
    public ConfigExecute(XDoc config) throws Exception
    {
        super(config);
        
        execpath = MainBase.main.configGeneral.home;
        jrepath = MainBase.main.configGeneral.home+"/jdk";
        groovylibpath = MainBase.main.configGeneral.home+"/lib";
        antlibpath = MainBase.main.configGeneral.home+"/lib";
        
        define("jrepath", STRING, "./EXECUTE/@JREPATH");
        define("groovylibpath", STRING, "./EXECUTE/GROOVY/@LIBPATH");
        define("antlibpath", STRING, "./EXECUTE/ANT/@LIBPATH");
    } // ConfigExecute
    
    public void load() throws Exception
    {
        loadAttributes();
        
        // groovy
        groovylibs = xdoc.getListMapValue("./EXECUTE/GROOVY/LIB", null);
        if (groovylibs == null || groovylibs.size() == 0)
        {
            Hashtable entry = new Hashtable();
            entry.put("NAME", "groovy.jar");
            groovylibs.add(entry);
            entry = new Hashtable();
            entry.put("NAME", "commons-cli.jar");
            groovylibs.add(entry);
        }
        
        // ant
        antlibs = xdoc.getListMapValue("./EXECUTE/ANT/LIB", null);
        if (antlibs == null || antlibs.size() == 0)
        {
            Hashtable entry = new Hashtable();
            entry.put("NAME", "ant.jar");
            antlibs.add(entry);
            entry = new Hashtable();
            entry.put("NAME", "ant-launcher.jar");
            antlibs.add(entry);
            entry = new Hashtable();
            entry.put("NAME", "ant-xalan2.jar");
            antlibs.add(entry);
        }
    } // load

    public void save()
    {
        saveAttributes();
        
        // groovy
        xdoc.setListMapValue("./EXECUTE/GROOVY/LIB", groovylibs);
        
        // ant
        xdoc.setListMapValue("./EXECUTE/ANT/LIB", antlibs);
    } // save
    
    public String getGroovyClasspath()
    {
        if (groovyClasspath == null)
        {
            String classpath = "";
            for (Iterator i=groovylibs.iterator(); i.hasNext();)
            {
                Hashtable entry = (Hashtable)i.next();
                
                classpath += groovylibpath+"/"+entry.get("NAME")+";";
            }
            
            groovyClasspath = classpath;
        }
        
        return groovyClasspath;
    } // getGroovyClasspath
    
    public String getAntClasspath()
    {
        if (antClasspath == null)
        {
            String classpath = "";
            for (Iterator i=antlibs.iterator(); i.hasNext();)
            {
                Hashtable entry = (Hashtable)i.next();
                
                classpath += antlibpath+"/"+entry.get("NAME")+";";
            }
            
            antClasspath = classpath;
        }
        
        return antClasspath;
    } // getAntClasspath

    public String getExecpath()
    {
        return execpath;
    }

    public String getJrepath()
    {
        return jrepath;
    }

    public String getGroovylibpath()
    {
        return groovylibpath;
    }

    public void setGroovylibpath(String groovylibpath)
    {
        this.groovylibpath = groovylibpath;
    }

    public String getAntlibpath()
    {
        return antlibpath;
    }

    public void setAntlibpath(String antlibpath)
    {
        this.antlibpath = antlibpath;
    }

    public void setExecpath(String execpath)
    {
        this.execpath = execpath;
    }

    public void setJrepath(String jrepath)
    {
        this.jrepath = jrepath;
    }
    
} // ConfigExecute
