/******************************************************************************

(C) Copyright 2006
 
Resolve Systems, Inc.
 Delaware, USA

This software may not be given to any party other than authorised
employees of Resolve Systems, Inc.

******************************************************************************/

package com.resolve.rsbase;

import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import com.resolve.esb.ESB;
import com.resolve.esb.MMsgHeader;
import com.resolve.rsbase.MainBase;
import com.resolve.util.LockUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MESB
{
    public void callback(MMsgHeader header, Map params)
    {
        String sid = header.getSid();
        
        if (StringUtils.isNotEmpty(sid))
        {
            ReentrantLock lock = LockUtils.getLock(sid);
            if (lock != null)
            {
                try
                {
                    lock.lock();
                    
                    ESB.setCallbackResult(sid, params);
                    Condition condition = ESB.getAndRemoveCondition(sid);
                    if (condition != null)
                    {
                        condition.signalAll();
                    }
                }
                finally
                {
                    lock.unlock();
                }
            }
        }
        
    } // callback
    
    public String subscribe(Map params) throws Exception
    {
        String result = null;
        
        String group = ((String)params.get("GROUP")).toUpperCase();
        if (MainBase.main.configESB.subscribe(group))
        {
            result = "SUCCESS: subscription to group: "+group;
        }
        else
        {
            result = "FAIL: subscription to group: "+group;
        }
        Log.log.info(result);
        return result;
    } // subscribe
    
    public String unsubscribe(Map params) throws Exception
    {
        String result = null;
        
        String group = ((String)params.get("GROUP")).toUpperCase();
        if (MainBase.main.configESB.unsubscribe(group))
        {
            result = "SUCCESS: unsubscribe to group: "+group;
        }
        else
        {
            result = "FAIL: unsubscribe to group: "+group;
        }
        Log.log.info(result);
        return result;
    } // unsubscribe
    
    public Hashtable listSubscription(Map params)
    {
        return MainBase.main.configESB.getSubscriptions();
    } // listSubscription
    
    public String setUserPass(Map params) throws Exception
    {
        String result = null;
        
        String olduser = (String)params.get("OLDUSER");
        String oldpass = (String)params.get("OLDPASS");
        String newuser = (String)params.get("NEWUSER");
        String newpass = (String)params.get("NEWPASS");
        if (MainBase.main.configESB.setUserPass(olduser, oldpass, newuser, newpass))
        {
            result = "SUCCESS: ESB username and password set";
        }
        else
        {
            result = "FAIL: unable to set ESB username and password";
        }
        Log.log.info(result);
        return result;
    } // setUserPass
    
    public String setAddress(Map params) throws Exception
    {
        String result = null;
        
        String brokerAddr = (String)params.get("BROKERADDR");
        String brokerPort = (String)params.get("BROKERPORT");
        if (MainBase.main.configESB.setAddress(brokerAddr, brokerPort))
        {
            result = "SUCCESS: set ESB address";
        }
        else
        {
            result = "FAIL: unable to set ESB address";
        }
        Log.log.info(result);
        return result;
    } // setAddress
    
} // MESB
