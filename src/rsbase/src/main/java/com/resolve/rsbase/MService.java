/******************************************************************************

(C) Copyright 2006
 
Resolve Systems, Inc.
 Delaware, USA

This software may not be given to any party other than authorised
employees of Resolve Systems, Inc.

******************************************************************************/

package com.resolve.rsbase;

import groovy.lang.Binding;

import java.io.File;

import com.resolve.esb.MMsgHeader;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MService
{
    static final Object scriptLock = new Object();
    public MService() throws Exception
    {
    } // MService

    public Object gse(MMsgHeader header, Object params, String scriptName) throws Exception
    {
        Object result = null;
        
        try
        {
            Log.log.debug("Calling service script: "+scriptName);
            
            // remove MService
            if (scriptName.startsWith("MService."))
            {
                scriptName = scriptName.substring(8);
            }
            
            // update filepath
            String filename = MainBase.main.getGSEHome()+"/"+scriptName;
            if (!filename.endsWith(".groovy"))
            {
                filename = filename.replace('.', '/') + ".groovy";
            }
            Log.log.debug("Filename: "+filename);
            
            // read content from file
            boolean found = false;
            File scriptFile = FileUtils.getFile(filename);
            if (scriptFile.exists())
            {
                found = true;
            }
            // try to match files in the dir ignoring case
            else
            {
                File dir = FileUtils.getFile(scriptFile.getParent());
                if (dir.exists())
                {
                    File[] files = dir.listFiles();
                    for (int i=0; !found && i < files.length; i++)
                    {
                        if (scriptFile.getName().equalsIgnoreCase(files[i].getName()))
                        {
                            found = true;
                            scriptFile = files[i];
                        }
                    }
                }
            }
                    
            if (found)
            {
                String script = FileUtils.readFileToString(scriptFile, "UTF-8");
                if (!StringUtils.isEmpty(script))
                {
                    Binding binding = new Binding();
                    binding.setVariable(Constants.GROOVY_BINDING_HEADER, header);
                    binding.setVariable(Constants.GROOVY_BINDING_PARAMS, params);
                    binding.setVariable(Constants.GROOVY_BINDING_MAIN, MainBase.main);
                    binding.setVariable(Constants.GROOVY_BINDING_ESB, MainBase.esb);
                    binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
                    
                    synchronized(scriptLock){
                        result = GroovyScript.execute(script, scriptName, true, binding);
                    }
                }
                else
                {
	                Log.log.warn("Missing content for file: "+filename);
                }
            }
            else
            {
                Log.log.warn("Missing file: "+filename);
            }
        }
        catch (Throwable t)
        {
            Log.log.warn(t.getMessage(), t);
        }
        
        return result;
    } // gse
    
} // MService
