/******************************************************************************

(C) Copyright 2006
 
Resolve Systems, Inc.
 Delaware, USA

This software may not be given to any party other than authorised
employees of Resolve Systems, Inc.

******************************************************************************/

package com.resolve.rsbase;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.thread.TaskExecutor;
import com.resolve.util.DBPoolUtils;

public class MInfo
{
	@SuppressWarnings({ "unchecked", "rawtypes" })
    public Map info(Map params)
    {
        Map result = new HashMap(params);
        
        result.put("NAME", MainBase.main.configId.name);
        result.put("DESCRIPTION", MainBase.main.configId.description);
        result.put("TYPE", MainBase.main.release.type);
        result.put("GUID", MainBase.main.configId.guid);
        result.put("NODEID", MainBase.main.configId.nodeid);
        result.put("VERSION", MainBase.main.release.version);
        result.put("CONFIGVERSION", MainBase.main.configGeneral.configFileVersion);
        result.put("IPADDRESS", ""+MainBase.main.configId.ipaddress);
        result.put("ESBUSER", MainBase.main.configESB.config.getUsername());
            
        return result;
    } // info
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public Map version(Map params)
    {
        Map result = new HashMap(params);
        
        result.put("VERSION", MainBase.main.release.version);
        
        return result;
    } // version
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public Map threadpool(Map params)
    {
        Map result = new HashMap(params);
        
        result.put("SYS_ACTIVE_THREADS", ScheduledExecutor.getInstance().getActiveThreads());
        result.put("SYS_MAX_THREADS", ScheduledExecutor.getInstance().getMaxThread());
        result.put("SYS_QUEUE_SIZE", ScheduledExecutor.getInstance().getQueueSize());
        result.put("SYS_CORE_SIZE", ScheduledExecutor.getInstance().getCorePoolSize());
        result.put("SYS_POOL_SIZE", ScheduledExecutor.getInstance().getPoolSize());
        result.put("SYS_PEAK_SIZE", ScheduledExecutor.getInstance().getPeakPoolSize());
        result.put("SYS_MAX_SIZE", ScheduledExecutor.getInstance().getMaxPoolSize());
        
        return result;
    } // threadpool
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public Map threadpoolRatio(Map params) {
        
        Map result = new HashMap(params);
        
        int maxSize = ScheduledExecutor.getInstance().getMaxPoolSize();
        
        result.put("SYSTEM_EXECUTOR_ACTIVE_THREADS", ScheduledExecutor.getInstance().getActiveThreads());
        result.put("SYSTEM_EXECUTOR_POOL_SIZE", ScheduledExecutor.getInstance().getPoolSize());
        result.put("SYSTEM_EXECUTOR_MAX_THREAD_OR_POOL_SIZE", maxSize);
        result.put("SYSTEM_EXECUTOR_BUSY_RATIO", ((float)ScheduledExecutor.getInstance().getActiveThreads())/maxSize);
        

        result.put("EXECUTOR_ACTIVE_THREADS", ((ThreadPoolExecutor)TaskExecutor.getCachedThreadPool()).getCorePoolSize());
        result.put("EXECUTOR_POOL_SIZE", ((ThreadPoolExecutor)TaskExecutor.getCachedThreadPool()).getPoolSize());
        
        result.put("GUID", MainBase.main.configId.guid);
        
        return result;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public Map dbpoolUsage (Map params) {

        Map result = new HashMap(params);
        
        result.put("DB_POOL_SIZE", DBPoolUtils.getPoolSize());
        result.put("DB_MAX_POOL_SIZE", DBPoolUtils.getMaxPoolSize());
        result.put("DB_AVAILABLE_POOL_SIZE", DBPoolUtils.getAvailableSize());
        result.put("DB_USED_POOL_SIZE", (DBPoolUtils.getTotalSize() - DBPoolUtils.getAvailableSize()));
        result.put("DB_POOL_BUSY_RATIO", (((DBPoolUtils.getTotalSize() - DBPoolUtils.getAvailableSize()) * 1.0F) / 
        								  DBPoolUtils.getMaxPoolSize()));
        result.put("GUID", MainBase.main.configId.guid);
        
        return result;
    }

    public void sendMetrics(Map params)
    {
        MainBase.main.metric.sendMetrics();
    } // sendMetrics
    
} // MInfo
