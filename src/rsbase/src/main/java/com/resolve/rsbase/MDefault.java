/******************************************************************************

(C) Copyright 2006
 
Resolve Systems, Inc.
 Delaware, USA

This software may not be given to any party other than authorised
employees of Resolve Systems, Inc.

******************************************************************************/

package com.resolve.rsbase;

import java.util.Map;
import java.util.Hashtable;

import com.resolve.esb.MMsgHeader;
import com.resolve.esb.MUtils;
import com.resolve.util.Log;

public class MDefault
{
    public Hashtable unknown(MMsgHeader header, Map params) throws Exception
    {
    	if (Log.log.isTraceEnabled()) {
    		Log.log.trace(String.format("Header: %s", header));
    		Log.log.trace(String.format("Params: %s", MUtils.dumpMessage(params)));
    	}
        
        return null;
    } // unknown
    
    public Hashtable ignore(MMsgHeader header, Map params) throws Exception
    {
    	if (Log.log.isTraceEnabled()) {
    		Log.log.trace(String.format("Header: %s", header));
    		Log.log.trace(String.format("Params: %s", MUtils.dumpMessage(params)));
    	}
        
        return null;
    } // ignore
    
    public Hashtable ignore(Map params) throws Exception
    {
        return null;
    } // ignore
    
} // MDefault
