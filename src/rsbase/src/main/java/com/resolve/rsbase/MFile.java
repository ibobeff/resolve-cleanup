package com.resolve.rsbase;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Hashtable;
import java.util.Map;

import com.resolve.esb.MMsgHeader;
import com.resolve.esb.MMsgOptions;
import com.resolve.rsbase.MainBase;
import com.resolve.util.CryptMD5;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;

public class MFile
{
    public Hashtable receiveFile(Map params)
    {
       Hashtable result = new Hashtable();
       result.put("RETURN", -1);
       
       try
       {
           MMsgOptions options = (MMsgOptions) params;
	       Log.log.debug("RECIEVE FILE PARAMETERS: "  + options.toString());
	       String fileName = options.getFileName();
	       
	       String fileComplete = options.getFileComplete();
		   int piece = 0;
		   if (options.getFilePiece() != null)
		   {
		       piece = Integer.valueOf(options.getFilePiece());
	       }
		   String fileChecksum = options.getFileChecksum();
		   String partChecksum = options.getChecksum();
		   
           byte[] fileContent = (byte[]) params.get("FILE_CONTENT");
		   
           String filePath = MainBase.main.getProductHome()+"/file/"+fileName;
           File file = FileUtils.getFile(filePath);
           
           boolean readFile = false;

           if (!file.exists())
           {
               if (!file.createNewFile())
               {
                   Log.log.error("Could Not Create Local File:" + fileName);
                   result.put("RETURN", -1);
                   result.put("ERROR_TEXT", "Could Not Create Local File: " +  fileName);
               }
               else
               {
                   readFile = true;
               }
           }
           else
           {
               if (piece == 0 && !file.delete())
               {
                   Log.log.error("Could Not Delete Local File:" + fileName);
                   result.put("RETURN", -1);
                   result.put("ERROR_TEXT", "Could Not Delete Local File: " + fileName);
               }
               else
               {
                   readFile = true;
               }
           }
           if (readFile)
           {
               result.put("FILE_NAME", fileName);

               //read transfer piece into file
               OutputStream os = new FileOutputStream(file, true);
               os.write(fileContent); 
               os.close();

               Log.log.info("Size Read: " + fileContent.length);
               Log.log.info("Transfer Complete: " + fileComplete);
               if (fileComplete.equals("true"))
               {
				   String checksum = CryptMD5.encrypt(file);
				   if (checksum.equals(fileChecksum))
				   {
		               result.put("RETURN", 0);
				   }
				   else
				   {
				       result.put("RETURN", -1);
		               result.put("ERROR_TEXT", "Checksum of Transmitted File Did Not Match");
				   }
               }
               else
               {
                   piece++;
                   Log.log.info("Return Request For Piece: " + piece);
                   result.put("RETURN", piece);
               }
               
           }
	       if (!result.get("RETURN").equals(-1))
	       {
	          String checksum = CryptMD5.encrypt(fileContent); 
	          if (!checksum.equals(partChecksum))
	          {
	              result.put("RETURN", -1);
	              result.put("ERROR_TEXT", "Checksum of Transmitted Part Did Not Match");
	          }
	       }
       }
       catch (Exception e)
       {
           Log.log.error("Exception Occured during File Transfer: " + e.getMessage(), e);
           result.put("RETURN", -1);
           result.put("ERROR_TEXT","Exception Occured during File Transfer: " + e.getMessage());
       }
       
       return result;
    }// RecieveFile(Map)

    public Hashtable receiveFile(byte[] params)
    {
       Hashtable result = new Hashtable();
       int fileNameSize = 0;
       String fileName = "";
       int maxTransferSize = 0;
       byte appendFlag = 1;

       result.put("RETURN", -1);
       try
       {
           appendFlag = params[0];
           //Read out Size of File Name
           for (int i=0; i<4; i++)
           {
               fileNameSize <<= 8;
               fileNameSize ^= (int) params[i+1] & 0xFF;
           }
           //read out File Name
           byte[] fileNameBytes = new byte[fileNameSize];

           for (int i=0; i<fileNameSize; i++)
           {
               fileNameBytes[i] = params[i+5];
           }

           //get File
           fileName = new String(fileNameBytes);
           String filePath = MainBase.main.getProductHome()+"/file/"+fileName;
           File file = FileUtils.getFile(filePath);

           boolean readFile = false;

           if (!file.exists())
           {
               if (!file.createNewFile())
               {
                   Log.log.error("Could Not Create Local File:" + fileName);
                   result.put("RETURN", -1);
                   result.put("ERROR_TEXT", "Could Not Create Local File: " +  fileName);
               }
               else
               {
                   readFile = true;
               }
           }
           else
           {
               if (appendFlag == 0 && !file.delete())
               {
                   Log.log.error("Could Not Delete Local File:" + fileName);
                   result.put("RETURN", -1);
                   result.put("ERROR_TEXT", "Could Not Delete Local File: " + fileName);
               }
               else
               {
                   readFile = true;
               }
           }
           if (readFile)
           {
               result.put("FILE_NAME", fileName);
               //get max transfer size of file
               int offset = fileNameSize + 4 + 1;
               for (int i=0; i<4; i++)
               {
                   maxTransferSize <<= 8;
                   maxTransferSize ^= (int) params[i + offset] & 0xFF;
               }

               offset = offset + 4;

               //get size of file transfer
               int readSize = params.length - offset;

               //read transfer piece into file
               OutputStream os = new FileOutputStream(file, true);
               os.write(params, offset, readSize); 
               os.close();

               Log.log.info("Size Read: " + readSize);
               Log.log.info("Max Transfer Size: " + maxTransferSize);
               result.put("RETURN", 0);

               //if this transfered piece was of of max transfer size, assume there is more
               if (readSize == maxTransferSize)
               {
                   int returnVal = (int) file.length() / maxTransferSize;
                   result.put("RETURN", returnVal);
               }
           }
       }
       catch (Exception e)
       {
           Log.log.error("Exception Occured during File Transfer: " + e.getMessage(), e);
           result.put("RETURN", -1);
           result.put("ERROR_TEXT","Exception Occured during File Transfer: " + e.getMessage());
       }

       return result;
    } //RecieveFile(byte[])
    
    public Object receiveFileResults(MMsgHeader header, Map params)
    {
        Object result = null;
        
        Log.log.debug("File Transfer Status Values: " + params.toString());
        
        try
        {
	        Integer returnVal = (Integer) params.get("RETURN");
	        if (returnVal == null)
	        {
	            Log.log.error("Unable to Determine File Transfer Status");
	            Log.log.error("Error Text Returned: " + params.get("ERROR_TEXT"));
	            result = params;
	        }
	        else if (returnVal == -1)
	        {
	            Log.log.warn("File Transfer Failed");
	            Log.log.warn("Error Text Returned: " + params.get("ERROR_TEXT"));
	            result = params;
	        }
	        else if (returnVal == 0)
	        {
	            Log.log.info("File Transfer Succeeded");
	            result = params;
	        }
	        else
	        {
	            String fileName = (String) params.get("FILE_NAME");
	            String filePath = MainBase.main.getProductHome()+"/file/"+fileName;
	            File file = FileUtils.getFile(filePath);
	            if (!file.exists())
	            {
	                Log.log.error("File at " + filePath + " was not found, aborting file transfer");
	                params.put("RETURN", -1);
	                params.put("ERROR_TEXT", "Transfer File: " + fileName + " cannot be found");
	                result = params;
	            }
	            else
	            {
                    header.insertNextRouteDest(header.getSource());
	                result = file;
	                MMsgOptions options = new MMsgOptions();
	                options.setFilePiece(returnVal);
	                header.setOptions(options);
	            }
	        }
        }
        catch (Exception e)
        {
            Log.log.error("Exception Occured Durring File Transfer: " + e.getMessage(), e);
            params.put("RETURN", -1);
            params.put("ERROR_TEXT", "Exception Occured Durring File Transfer: " + e.getMessage());
            result = params;
        }
        
        return result;
    }// recieveFileResults
} // MFile
