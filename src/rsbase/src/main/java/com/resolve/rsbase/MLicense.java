package com.resolve.rsbase;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;

import com.resolve.esb.MMsgFilter;
import com.resolve.service.LicenseService;
import com.resolve.util.Constants;
import com.resolve.util.LicenseEnum;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * Serves as a manager for license related request from other component or
 * license upload by rsconsole.
 *
 */
public class MLicense extends MMsgFilter
{
	private boolean checkLicenseServiceReady() {
		boolean ready = true;
		
		LicenseService licenseService = MainBase.main.getLicenseService();
		
		if (licenseService == null) {
			String errMsg = String.format("%s's license service is not ready yet.", MainBase.main.configId.getDescription() + 
 					  					  "(" + MainBase.main.configId.getGuid() + ")");
			
			Log.log.warn(errMsg);
			ready = false;
		}
		
		return ready;
	}
    /**
     * Gets License Information.
     *
     * @param params
     *            if there is value with a key LICENSE_KEY then try to load that
     *            particular license, otherwise it's the cummulative license.
     * @return
     */
    @SuppressWarnings("unchecked")
	public Map<String, String> getLicenseInfo(Map<String, String> msgParams)
    {
        Map<String, String> result = new HashMap<String, String>();

        if (checkLicenseServiceReady()) {
        	Map<String, String> params = (Map<String, String>) filterMsg(msgParams);
        	
	        if (MapUtils.isEmpty(params))
	        {
	            result = MainBase.main.getLicenseService().getLicenseInfo();
	        }
	        else
	        {
	            String key = params.get(LicenseEnum.LICENSE_KEY.getKeygenName());
	            if (StringUtils.isNotBlank(key))
	            {
	                result = MainBase.main.getLicenseService().getLicenseInfo(key);
	            }
	        }
        } else {
        	result.put("ERROR", String.format("%s's license service is not ready yet.", 
        									  MainBase.main.configId.getDescription() + 
        									  "(" + MainBase.main.configId.getGuid() + ")"));
        }

        return result;
    }

    /**
     * Uploads a license.
     *
     * @param params
     *            key is "LICENSE_CONTENT" and value is the license's content.
     *            key is ID guid of the component which generated this message.
     * @return
     */
    @SuppressWarnings("unchecked")
	public void upload(Map<String, String>msgParams)
    {
    	Map<String, String> params = (Map<String, String>) filterMsg(msgParams);
    	
        if (MapUtils.isEmpty(params))
        {
            Log.log.warn("params is Null or Empty, can't do anything, check the calling code.");
        }
        else
        {
            if (!params.containsKey(Constants.LICENSE_CONTENT) || StringUtils.isBlank(params.get(Constants.LICENSE_CONTENT)))
            {
                throw new RuntimeException("License file content is empty.");
            }
            else if (!params.containsKey(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID) ||
            		 StringUtils.isBlank(params.get(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID))) {
            	throw new RuntimeException("Id guid of component which generated this message is missing.");
            }
            else
            {
            	if (!params.get(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID).equals(MainBase.main.configId.guid)) {
            		Log.log.debug(String.format("Component with id guid %s processing Upload License Key [%s] message " +
            									"received from resolve component with id guid %s...", 
            									MainBase.main.configId.guid, params, 
            					  				params.get(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID)));
            		
            		if (checkLicenseServiceReady()) {
	            		Log.log.info(String.format("Uploading license key [%s]", params));
	            		LicenseService licenseService = MainBase.main.getLicenseService();
	            		licenseService.uploadLicense(params);
            		}
            	} else {
            		Log.log.debug("Component with id guid " + MainBase.main.configId.guid + 
      					  " ignoring processing of Upload License Key " + params + " message received from self!!!");
            	}
            }
        }
    }

    /**
     * Remove one or more licenses.
     *
     * @param params
     *            keys of the licenses.
     * @return
     */
    @SuppressWarnings("unchecked")
	public void remove(Map<String, String> msgParams)
    {
    	Map<String, String> params = (Map<String, String>) filterMsg(msgParams);
    	
        if (params == null || params.size() == 0)
        {
            Log.log.warn("params is Null or Empty, can't do anything, check the calling code.");
        }
        else if (!params.containsKey(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID) ||
       		 	 StringUtils.isBlank(params.get(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID))) {
        	throw new RuntimeException("Id guid of component which generated this message is mising.");
        } else {
        	if (!params.get(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID).equals(MainBase.main.configId.guid)) {
        		Log.log.debug("Component with id guid " + MainBase.main.configId.guid + 
        					  " processing Remove License Key(s) " + params + " message received from " + 
        					  "resolve component with id guid " + params.get(Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID) + 
        					  "...");
        		if (checkLicenseServiceReady()) {
        			LicenseService licenseService = MainBase.main.getLicenseService();
        			licenseService.removeLicense(params);
        		}
        	} else {
        		Log.log.debug("Component with id guid " + MainBase.main.configId.guid + 
  					  " ignoring processing of Remove License Key(s) " + params + " message received from self!!!");
        	}
        }
    }

    /**
     * Receives license from another Resolve Component and synchronizes its store.
     *
     * @param params
     *            .
     * @return
     */
    @SuppressWarnings("unchecked")
	public void synchronizeLicense(Map<String, String> msgParams)
    {
    	Map<String, String> params = (Map<String, String>) filterMsg(msgParams);
    	
        if (params == null || params.size() == 0)
        {
            Log.log.warn("params is Null or Empty, can't do anything, check the calling code.");
        }
        else
        {
        	if (checkLicenseServiceReady()) {
        		LicenseService licenseService = MainBase.main.getLicenseService();
        		licenseService.synchronizeLicense(params);
        	}
        }
    }

    /**
     * Uploads a license.
     *
     * @param params
     *            key is "RETURN_QUEUE".
     * @return
     */
    @SuppressWarnings("unchecked")
	public void sendLicense(Map<String, String> msgParams)
    {
    	Map<String, String> params = (Map<String, String>) filterMsg(msgParams);
    	
        if (params == null || params.size() == 0)
        {
            Log.log.warn("params is Null or Empty, can't do anything, check the calling code.");
        }
        else if (!params.containsKey(LicenseEnum.RETURN_QUEUE.getKeygenName()) ||
    		 	 StringUtils.isBlank(params.get(LicenseEnum.RETURN_QUEUE.getKeygenName()))) {
        	throw new RuntimeException("Id guid of component requesting other components to send license info is missing.");
        }
        else
        {
        	if (!params.get(LicenseEnum.RETURN_QUEUE.getKeygenName()).equals(MainBase.main.configId.guid)) {
        		if (checkLicenseServiceReady()) {
        			if (checkLicenseServiceReady()) {
        				LicenseService licenseService = MainBase.main.getLicenseService();
        				licenseService.sendLicense(params);
        			}
        		}
        	} else {
        		Log.log.debug("Component with id guid " + MainBase.main.configId.guid + 
  					  		  " ignoring processing of Send License " + params + " message received from self!!!");
        	}
        }
    }

    /**
     * This method is called by Resolve components if the license is a Community
     * License. If it's a community license then Resolve components must run in a
     * single server.
     *
     * @param params
     * @return
     */
    @SuppressWarnings("unchecked")
	public Map<String, String> getHostIPAddress(Map<String, String> params)
    {
        Map<String, String> result = new HashMap<String, String>();
        
        if (checkLicenseServiceReady()) {
        	LicenseService licenseService = MainBase.main.getLicenseService();
        	result = licenseService.getHostIPAddress((Map<String, String>)filterMsg(params));
        }
        
        return result;
    }

    /**
     * This is the method that is used when every component (RSCONTROL, RSVIEW, RSREMOTE) announces its
     * presence to the environment.
     *
     * @param params with TYPE, GUID and IP
     */
    @SuppressWarnings("unchecked")
	public void registerComponent(Map<String, String> msgParams)
    {
    	Map<String, String> params = (Map<String, String>) filterMsg(msgParams);
    	
        if (params == null || params.size() == 0)
        {
            Log.log.warn("params is Null or Empty, can't do anything, check the calling code.");
        }
        else
        {
        	if (checkLicenseServiceReady()) {
        		LicenseService licenseService = MainBase.main.getLicenseService();
        		licenseService.registerComponent((Map<String, String>)filterMsg(params));
        	}
        }
    }

    /**
     * Gateways send message once it starts or re-starts.
     * @param params
     */
    @SuppressWarnings("unchecked")
	public void registerGateway(Map<String, String> msgParams)
    {
    	Map<String, String> params = (Map<String, String>) filterMsg(msgParams);
    	
        if (params == null || params.size() == 0)
        {
            Log.log.warn("params is Null or Empty, can't do anything, check the calling code.");
        }
        else
        {
            Log.log.debug("Received gateway registration message " + params);
            if (checkLicenseServiceReady()) {
            	LicenseService licenseService = MainBase.main.getLicenseService();
            	licenseService.registerGateway(params);
            }
        }
    }
    
    /**
     * Gateways send message this message periodically (30 min) to indicate they are still running.
     * @param params
     */
    @SuppressWarnings("unchecked")
	public void updateGatewayRunningInstance(Map<String, String> msgParams)
    {
    	Map<String, String> params = (Map<String, String>) filterMsg(msgParams);
    	
        if (params == null || params.size() == 0)
        {
            Log.log.warn("params is Null or Empty, can't do anything, check the calling code.");
        }
        else
        {
            Log.log.debug("Received gateway update running instance message " + params);
            if (checkLicenseServiceReady()) {
            	LicenseService licenseService = MainBase.main.getLicenseService();
            	licenseService.updateGatewayRunningInstance(params);
            }
        }
    }
    
    /**
     * Gateways send message this message when they stop running.
     * @param params
     */
    @SuppressWarnings("unchecked")
	public void removeGatewayRunningInstance(Map<String, String> msgParams)
    {
    	Map<String, String> params = (Map<String, String>) filterMsg(msgParams);
    	
        if (params == null || params.size() == 0)
        {
            Log.log.warn("params is Null or Empty, can't do anything, check the calling code.");
        }
        else
        {
            Log.log.debug("Received gateway remove running instance message " + params);
            if (checkLicenseServiceReady()) {
            	LicenseService licenseService = MainBase.main.getLicenseService();
            	licenseService.removeGatewayRunningInstance(params);
            }
        }
    }
    
    /**
     * Clustered SECONDARY Gateways send message this message to become PRIMARY 
     * on fail over and to become SECONDARY when original PRIMARY comes back up.
     * 
     * @param params
     */
    @SuppressWarnings("unchecked")
	public void switchGatewayRunningInstanceType(Map<String, String> msgParams)
    {
    	Map<String, String> params = (Map<String, String>) filterMsg(msgParams);
    	
        if (params == null || params.size() == 0)
        {
            Log.log.warn("params is Null or Empty, can't do anything, check the calling code.");
        }
        else
        {
            Log.log.debug("Received gateway switch running instance type message " + params);
            if (checkLicenseServiceReady()) {
            	LicenseService licenseService = MainBase.main.getLicenseService();
            	licenseService.switchGatewayRunningInstanceType(params);
            }
        }
    }
}
