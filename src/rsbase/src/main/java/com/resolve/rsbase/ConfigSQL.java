/******************************************************************************

(C) Copyright 2006
 
Resolve Systems, Inc.
 Delaware, USA

This software may not be given to any party other than authorised
employees of Resolve Systems, Inc.

******************************************************************************/

package com.resolve.rsbase;

import com.resolve.util.XDoc;
import com.resolve.util.ConfigMap;

public class ConfigSQL extends ConfigMap
{
    private static final long serialVersionUID = 3869287685376012547L;
	boolean debug = false;
    String dbtype = "mysql";
    String dbname = "resolve";
    String host = "localhost";
    String username = "resolve";
    String p_assword = "resolve";
    String url = "";
    int maxpoolsize = 100;
    int minpoolsize = 3;
    
    //maxIdleTime/timeout to 300 (5 minutes) is quite aggressive. For most databases, several hours may be more appropriate.
    int timeout = 3600;
    
    //Number of seconds that Connections in excess of minPoolSize should be permitted to remain idle in the pool before being culled.
    //Intended for applications that wish to aggressively minimize the number of open Connections, shrinking the pool back towards minPoolSize if, following a spike, 
    //the load level diminishes and Connections acquired are no longer needed.
    int maxIdleTimeExcessConnections  = 300;
    boolean perfDebug = false;
    
    public ConfigSQL(XDoc config) throws Exception
    {
        super(config);
        
        define("debug", BOOLEAN, "./SQL/@DEBUG");
        define("dbtype", STRING, "./SQL/@DBTYPE");
        define("dbname", STRING, "./SQL/@DBNAME");
        define("host", STRING, "./SQL/@HOST");
        define("username", STRING, "./SQL/@USERNAME");
        define("p_assword", SECURE, "./SQL/@PASSWORD");
        define("url", STRING, "./SQL/@URL");
        define("maxpoolsize", INTEGER, "./SQL/@MAXPOOLSIZE");
        define("minpoolsize", INTEGER, "./SQL/@MINPOOLSIZE");
        define("timeout", INTEGER, "./SQL/@TIMEOUT");
        define("maxIdleTimeExcessConnections", INTEGER, "./SQL/@MAXIDLETIMEEXCESSCONNECTIONS");
        define("perfDebug", BOOLEAN, "./SQL/@PERFDEBUG");
    } // ConfigSQL
    
    public void load() throws Exception
    {
        loadAttributes();
    } // load

    public void save() throws Exception
    {
        saveAttributes();
    } // save

    public boolean isDebug()
    {
        return debug;
    }

    public void setDebug(boolean debug)
    {
        this.debug = debug;
    }

    public String getDbtype()
    {
        return dbtype;
    }

    public void setDbtype(String dbtype)
    {
        this.dbtype = dbtype;
    }

    public String getDbname()
    {
        return dbname;
    }

    public void setDbname(String dbname)
    {
        this.dbname = dbname;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getP_assword()
    {
        return p_assword;
    }

    public void setP_assword(String password)
    {
        this.p_assword = password;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }
    
    public int getMaxpoolsize()
    {
        return maxpoolsize;
    }

    public void setMaxpoolsize(int maxpoolsize)
    {
        this.maxpoolsize = maxpoolsize;
    }

    public int getMinpoolsize()
    {
        return minpoolsize;
    }

    public void setMinpoolsize(int min)
    {
        this.minpoolsize = min;
    }

    public int getTimeout()
    {
        return timeout;
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }
    
    public boolean isPerfDebug()
    {
        return perfDebug;
    }

    public void setPerfDebug(boolean b)
    {
        this.perfDebug = b;
    }
    
    public int getMaxIdleTimeExcessConnections() {
      return maxIdleTimeExcessConnections;
    }
    
    public void setMaxIdleTimeExcessConnections(int maxIdleTimeExcessConnections) {
      this.maxIdleTimeExcessConnections = maxIdleTimeExcessConnections;
    }

    
} // ConfigSQL