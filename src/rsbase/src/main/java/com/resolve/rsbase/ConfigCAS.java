package com.resolve.rsbase;

import com.resolve.util.ConfigMap;
import com.resolve.util.XDoc;

public class ConfigCAS extends ConfigMap
{

    private static final long serialVersionUID = -3600886754364977144L;
	String cluster="resolve";
    String keyspace="resolve";
    String seeds="127.0.0.1:9160";
    String username="admin";
    String p_assword="resolve";
    int maxConnsPerHost=100;
    int maxExhaustedTimeout=10000;
    int maxBlockedThreads=200;
    String readConsistency = "QUORUM";
    String writeConsistency = "QUORUM";
    boolean active = true;
    String schedule = "0 0 2 * * ?";     // 2am daily 
    boolean secure = false;
    String truststore = "cassandra/conf/.truststore";
    String truststoreP_assword = "resolve";
    boolean perfDebug = false;
    int retrySleepTimeMs = 500;
    int retryMaxAttempts = 20;
    
    private int ttl = 60*60*24*30; //default TTL is one month
    
    public ConfigCAS(XDoc config) throws Exception
    {
        super(config);
        
        define("cluster", STRING, "./NOSQL/@CLUSTER");
        define("keyspace", STRING, "./NOSQL/@KEYSPACE");
        define("seeds", STRING, "./NOSQL/@SEEDS");
        define("p_assword", SECURE, "./NOSQL/@PASSWORD");
        define("maxExhaustedTimeout", INTEGER, "./NOSQL/@MAXTIMEOUT");
        define("maxBlockedThreads", INTEGER, "./NOSQL/@MAXBLOCKEDTHREADS");
        define("maxConnsPerHost", INTEGER, "./NOSQL/@MAXHOSTCONNS");
        define("readConsistency", STRING, "./NOSQL/@READCONSISTENCY");
        define("writeConsistency", STRING, "./NOSQL/@WRITECONSISTENCY");
        define("ttl", INTEGER, "./NOSQL/@TTL");
        define("active", BOOLEAN, "./NOSQL/COMPACTION/@ACTIVE"); 
        define("schedule", STRING, "./NOSQL/COMPACTION/@SCHEDULE"); 
        define("secure", BOOLEAN, "./NOSQL/SECURE/@ACTIVE"); 
        define("truststore", STRING, "./NOSQL/SECURE/@TRUSTSTORE"); 
        define("truststoreP_assword", STRING, "./NOSQL/SECURE/@TRUSTSTORE_PASSWORD"); 
        define("perfDebug", BOOLEAN, "./NOSQL/@PERFDEBUG");
        define("retrySleepTimeMs", INTEGER, "./NOSQL/@RETRYSLEEPTIMEMS");
        define("retryMaxAttempts", INTEGER, "./NOSQL/@RETRYMAXATTEMPTS");
     
    } // ConfigCAS

    @Override
    public void load() throws Exception
    {
        loadAttributes();
    }

    @Override
    public void save() throws Exception
    {
        saveAttributes();
    }
    
    public String getCluster()
    {
        return cluster;
    }

    public void setCluster(String c)
    {
        this.cluster = c;
    }

    
    public String getKeyspace()
    {
        return keyspace;
    }

    public void setKeyspace(String k)
    {
        this.keyspace = k;
    }
    
    public String getSeeds()
    {
        return seeds;
    }

    public void setSeeds(String s)
    {
        this.seeds = s;
    }

    public int getMaxConnsPerHost()
    {
        return maxConnsPerHost;
    }

    public void setMaxConnsPerHost(int m)
    {
        this.maxConnsPerHost = m;
    }

    public int getMaxExhaustedTimeout()
    {
        return maxExhaustedTimeout;
    }

    public void setMaxExhaustedTimeout(int m)
    {
        this.maxExhaustedTimeout = m;
    }

    public int getMaxBlockedThreads()
    {
        return maxBlockedThreads;
    }

    public void setMaxBlockedThreads(int m)
    {
        this.maxBlockedThreads = m;
    }
    
    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getP_assword()
    {
        return p_assword;
    }

    public void setP_assword(String password)
    {
        this.p_assword = password;
    }

    public String getReadConsistency()
    {
        return readConsistency;
    }

    public void setReadConsistency(String c)
    {
        this.readConsistency=c;
    }

    public String getWriteConsistency()
    {
        return writeConsistency;
    }

    public void setWriteConsistency(String c)
    {
        this.writeConsistency=c;
    }

    public int getTtl()
    {
        return ttl;
    }

    public void setTtl(int t)
    {
        this.ttl = t;
    }
    public String getSchedule()
    {
        return this.schedule;
    }

    public void setSchedule(String schedule)
    {
        this.schedule = schedule;
    }
    public boolean isActive()
    {
        return this.active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean getSecure()
    {
        return secure;
    }

    public void setSecure(boolean secure)
    {
        this.secure = secure;
    }

    public String getTruststore()
    {
        return truststore;
    }

    public void setTruststore(String truststore)
    {
        this.truststore = truststore;
    }

    public String getTruststoreP_assword()
    {
        return truststoreP_assword;
    }

    public void setTruststoreP_assword(String truststorePassword)
    {
        this.truststoreP_assword = truststorePassword;
    }
    
    public boolean isPerfDebug()
    {
        return perfDebug;
    }

    public void setPerfDebug(boolean b)
    {
        this.perfDebug = b;
    }

    public int getRetrySleepTimeMs()
    {
        return retrySleepTimeMs;
    }

    public void setRetrySleepTimeMs(int retrySleepTimeMs)
    {
        this.retrySleepTimeMs = retrySleepTimeMs;
    }

    public int getRetryMaxAttempts()
    {
        return retryMaxAttempts;
    }

    public void setRetryMaxAttempts(int retryMaxAttempts)
    {
        this.retryMaxAttempts = retryMaxAttempts;
    }
    
}
