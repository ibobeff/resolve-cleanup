package com.resolve.rsbase;

import java.util.HashMap;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.rsbase.MetricInterface;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.MetricThreshold;
import com.resolve.util.MetricThresholdInit;
import com.resolve.util.MetricThresholdType;

public abstract class BaseMetric implements MetricInterface
{
    //read  and load the threshold values at component startup
    public static MetricThreshold  mthreshold;
    
    protected boolean hasDBAccess;
    
    public BaseMetric(MetricThresholdInit metricThresholdInit)
    {
        this.hasDBAccess = hasDBAccess;
        mthreshold = new MetricThreshold(metricThresholdInit);
    }
    
    public static Map<String, MetricThresholdType> getThresholdProperties()
    {
        return mthreshold.thresholdProperties;
    } // getThresholdProperties
    
    public static void addToThresholdProperties(String name, MetricThresholdType mtype)
    {
        mthreshold.thresholdProperties.put(name, mtype);
    } // addToThresholdProperties
    
    public void initializeMetricThreshold()
    {
        
    }
    
    public static void updateMetricThresholds(Map<String, MetricThresholdType> params)
    {
        if(params != null && !params.keySet().isEmpty())
        {
            mthreshold.thresholdProperties.putAll(params);
            Log.log.debug("Updating the metric thresholds");
        }
    }
    
    public static void requestThresholdUpdate(long delaySecs)
    {
        Log.log.debug("Sending request to update metric threshold properties - delay: "+delaySecs);
        Map<String, String> content = new HashMap<String, String>();
        content.put("GUID", MainBase.main.configId.getGuid());
        MainBase.main.esb.sendDelayedMessage(Constants.ESB_NAME_METRIC, "MMetric.updateMetricThresholdProperties", content, delaySecs);
    } // requestThresholdUpdate
    
} // BaseMetric
