package com.resolve.rsbase;

import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SessionBinding
{
    SessionMap sessionMap;
    Map flows;
    
    public SessionBinding(SessionMap sessionMap, Map flows)
    {
        this.sessionMap = sessionMap;
        this.flows = flows;
    } // SessionBinding
    
    public Object get()
    {
        String sessionId = (String)this.flows.get(Constants.SESSION_ID);
        
        return get(sessionId);
    } // get
    
    public Object get(int sessionId)
    {
        return get(""+sessionId);
    } // get
    
    public Object get(String sessionId)
    {
        Object result = null;
        
        Log.log.debug("Getting SessionObject sessionId: "+sessionId+" sessions: "+this);
        
        SessionObjectInterface sessObj = sessionMap.get(sessionId);
        if (sessObj != null)
        {
            if (sessObj instanceof SessionObject)
            {
                result = ((SessionObject)sessObj).get();
            }
            else
            {
                result = sessObj;
            }
        }
        Log.log.debug("  sessionObject: "+result);
        
        return result;
    } // get
    
    
    public String setAffinity()
    {
        Integer obj = new Integer(0);
        String sessionid = ""+ obj.hashCode();
        
        return put(sessionid, obj);
    } // setAffinity
    
    public String put(Object obj)
    {
        String sessionid = ""+ obj.hashCode();
        
        return put(sessionid, obj);
    } // put
    
    public String put(String sessionid, Object obj)
    {
        // set SESSION_AFFINITY = guid of the rsremote
        flows.put(Constants.SESSION_AFFINITY, MainBase.main.configId.guid);
        
        // set SESSION_ID = sessionid
        flows.put(Constants.SESSION_ID, sessionid);
        
        Log.log.debug("Putting SessionObject sessionId: "+sessionid+" sessions: "+this+" obj: "+obj);
        if (obj instanceof SessionObjectInterface)
        {
            // put object
            sessionMap.put(sessionid, (SessionObjectInterface)obj);
        }
        else
        {
            // wrap raw object in SessionObject
            SessionObject sessObj = new SessionObject(obj);
            sessionMap.put(sessionid, sessObj);
        }
        
        return sessionid;
    } // put
    
    public void remove()
    {
        try
        {
            String sessionId = (String)flows.get(Constants.SESSION_ID);
            if (!StringUtils.isEmpty(sessionId))
            {
                SessionObjectInterface obj = sessionMap.get(sessionId);
                if (obj != null)
                {
                    obj.close();
                }
                
                // remove SESSION_AFFINITY and SESSION_ID
                this.flows.remove(Constants.SESSION_AFFINITY);
                this.flows.remove(Constants.SESSION_ID);
            }
        }
        catch (Throwable e)
        {
            Log.log.debug("Remove session error: "+e.getMessage());
        }
    } // remove
    
    public void remove(String sessionId)
    {
        try
        {
            SessionObjectInterface obj = sessionMap.get(sessionId);
            if (obj != null)
            {
                obj.close();
            }
            
            // remove SESSION_AFFINITY_<type>
            this.flows.remove(Constants.SESSION_AFFINITY);
            this.flows.remove(Constants.SESSION_ID);
        }
        catch (Throwable e)
        {
            Log.log.debug("Remove session error: "+e.getMessage());
        }
    } // remove
    
} // SessionBinding
