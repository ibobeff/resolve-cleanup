package com.resolve.rsbase;

import com.resolve.rsbase.SessionObjectInterface;

public class SessionObject implements SessionObjectInterface
{
    Object obj = null;
    
    public SessionObject(Object obj)
    {
        this.obj = obj;
    } // SessionObject
    
    public void close()
    {
        this.obj = null;
    } // close

    public void finalize()
    {
        this.obj = null;
    } // finalize
    
    public Object get()
    {
        return this.obj;
    } // get
    
    public void set(Object obj)
    {
        this.obj = obj;
    } // set
    
} // SessionObject