/******************************************************************************

(C) Copyright 2006
 
Resolve Systems, Inc.
 Delaware, USA

This software may not be given to any party other than authorised
employees of Resolve Systems, Inc.

******************************************************************************/

package com.resolve.rsbase;

import java.util.Hashtable;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MControl
{
    public static Hashtable terminate(Map params)
    {
        String msg = (String)params.get("MSG");
        String save = (String)params.get("SAVE");
        
        if (StringUtils.isNotBlank(msg))
        {
            Log.log.info(msg);
        }
        
        if (save != null && save.equalsIgnoreCase("FALSE"))
        {
            MainBase.overrideSaveConfigOnExit = true;
        }
        
        // terminate
        MainBase.main.exit(null);
        
        return null;
    } // terminate

} // MControl
