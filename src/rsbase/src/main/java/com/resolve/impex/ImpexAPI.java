/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.impex;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;

/**
 * This is a utility class which provides convenient ways to import or export
 * resolve modules between Resolve instances.
 */
public class ImpexAPI
{
    public static final String MODULE_URL = "MODULE_URL";
    public static final String CONNECTION_TIMEOUT = "CONNECTION_TIMEOUT";
    public static final String READ_TIMEOUT = "READ_TIMEOUT";
    public static final String RESOLVE_USERNAME = "RESOLVE_USERNAME";
    public static final String RESOLVE_P_ASSWORD = "RESOLVE_PASSWORD";
    public static final String IMPORT_FLAG = "IMPORT_FLAG";

    /**
     * This method provides a convenient way to import resolve module from
     * another resolve instance.
     * 
     * <pre>
     * {@code
     * import com.resolve.impex.ImpexAPI;
     * 
     * try
     * {
     *     String moduleUrl="http://resolve_server:port/resolve/service/wiki/impex/download?filename=testmodule.zip";
     *     Integer connectionTimeout = 20; //in seconds, you can set null as well.
     *     Integer readTimeout = 20; //in seconds, you can set null as well.
     *     String username = "<resolve_user>";
     *     String password = "<resolve_user_password>";
     *     ImpexAPI.importModule(moduleUrl, connectionTimeout, readTimeout, username, password)
     * }
     * catch (Exception e)
     * {
     *      //do something with this exception.
     * }
     * }
     * </pre>
     * 
     * @param moduleUrl
     *            to download the module (e.g.,
     *            http://remote_server:port/resolve
     *            /service/wiki/impex/download?filename=doc3.zip)
     * @param connectionTimeout
     *            - Optinal number of seconds until this method will timeout if
     *            no connection could be established to the source.
     * @param readTimeout
     *            - Optional number of seconds until this method will timeout if
     *            no data could be read from the source.
     * @param username
     *            - resolve user name.
     * @param password
     *            - resolve user password.
     * @throws Exception
     */
    public static void importModule(String moduleUrl, Integer connectionTimeout, Integer readTimeout, String username, String password) throws Exception
    {
        try
        {
            // this is just to validate the URL, if it errors out then
            // we don't go deeper into the system and get out right here.
            new URL(moduleUrl);

            Map<String, String> params = new HashMap<String, String>();
            params.put(MODULE_URL, moduleUrl);
            params.put(CONNECTION_TIMEOUT, (connectionTimeout == null ? null : connectionTimeout.toString()));
            params.put(READ_TIMEOUT, (readTimeout == null ? null : readTimeout.toString()));
            params.put(RESOLVE_USERNAME, username);
            params.put(RESOLVE_P_ASSWORD, password);
            params.put(IMPORT_FLAG, Boolean.TRUE.toString());

            String classMethod = "MImpex.importModule";

            boolean isSuccess = MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSVIEW, classMethod, params);
            if (isSuccess)
            {
                Log.log.debug("Import Module message submitted to RSView for processing.");
            }
            else
            {
                Log.log.warn("Import Module message failed during transmission to RSView for processing.");
            }
        }
        catch (MalformedURLException e)
        {
            Log.log.error("Bad URL provided: " + moduleUrl + ", actual message: " + e.getMessage(), e);
            throw new Exception("Bad URL provided: " + moduleUrl + ", actual message: " + e.getMessage(), e);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
}
