package com.resolve.exception;

/**
 * Thrown in case the number of items requested from an endpoint exceeds 
 * maximum number of items as specified by rsview.properties.
 * 
 * @author Martin Toshev
 */
public class LimitExceededException extends Exception {

	public LimitExceededException() {
		super();
	}
	
	public LimitExceededException(String message) {
		super(message);
	}

	public LimitExceededException(Throwable throwable) {
		super(throwable);
	}
	
	public LimitExceededException(String message, Throwable throwable) {
		super(message, throwable);
	}

	
}
