/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.vo;

import java.util.HashMap;
import java.util.Map;

import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ActionTaskResultVO extends HashMap<String, String>
{
    private static final long serialVersionUID = 4022089020763984564L;

	// consolidated VO
    public static String VO = Constants.EXECUTE_VO;

    // system params
    public static String WIKI = Constants.EXECUTE_WIKI;
    public static String ACTIONNAME = Constants.EXECUTE_ACTIONNAME;
    public static String ACTIONNAMESPACE = Constants.EXECUTE_ACTIONNAMESPACE;
    public static String ACTIONSUMMARY = Constants.EXECUTE_ACTIONSUMMARY;
    public static String ACTIONHIDDEN = Constants.EXECUTE_ACTIONHIDDEN;
    public static String ACTIONTAGS = Constants.EXECUTE_ACTIONTAGS;
    public static String ACTIONROLES = Constants.EXECUTE_ACTIONROLES;
    public static String ACTIONID = Constants.EXECUTE_ACTIONID;
    public static String PROCESSID = Constants.EXECUTE_PROCESSID;
    public static String EXECUTEID = Constants.EXECUTE_EXECUTEID;
    public static String PROBLEMID = Constants.EXECUTE_PROBLEMID;
    public static String PARSERID = Constants.EXECUTE_PARSERID;
    public static String ASSESSID = Constants.EXECUTE_ASSESSID;
    public static String DURATION = Constants.EXECUTE_DURATION;
    public static String LOGRESULT = Constants.EXECUTE_LOGRESULT;
    public static String REFERENCE = Constants.EXECUTE_REFERENCE;
    public static String TARGET = Constants.EXECUTE_TARGET;
    public static String AFFINITY = Constants.EXECUTE_AFFINITY;
    public static String RETURNCODE = Constants.EXECUTE_RETURNCODE;
    public static String TIMEOUT = Constants.EXECUTE_TIMEOUT;
    public static String PROCESS_TIMEOUT = Constants.EXECUTE_PROCESS_TIMEOUT;
    public static String METRICID = Constants.EXECUTE_METRICID;
    public static String MOCK = Constants.EXECUTE_MOCK;
    public static String FLOWSSTR = Constants.EXECUTE_FLOWSSTR;
    public static String INPUTSSTR = Constants.EXECUTE_INPUTSSTR;
    public static String DEBUGSTR = Constants.EXECUTE_DEBUGSTR;

    // interactive params
    public static String USERID = Constants.EXECUTE_USERID;
    public static String COMPLETION = Constants.EXECUTE_COMPLETION;
    public static String COMMAND = Constants.EXECUTE_COMMAND;
    public static String RAW = Constants.EXECUTE_RAW;
    public static String ADDRESS = Constants.EXECUTE_ADDRESS;
    public static String REQUEST_TIMESTAMP = Constants.EXECUTE_REQUEST_TIMESTAMP;

    public ActionTaskResultVO()
    {
    } // ActionTaskResultVO

    /**
     * Create object from string with Base64 decode
     * 
     * @param voStr
     * @throws Exception
     */
    public ActionTaskResultVO(String voStr) throws Exception
    {
        super((Map) StringUtils.stringToObj(voStr));
    } // ActionTaskResultVO

    /**
     * Create object from string with Base64 and URL decode
     * 
     * @param voStr
     * @throws Exception
     */
    public ActionTaskResultVO(String voStr, boolean urlDecode) throws Exception
    {
        super((Map) StringUtils.stringToObj(java.net.URLDecoder.decode(voStr, "UTF-8")));
    } // ActionTaskResultVO

    public ActionTaskResultVO(Map<String, String> params)
    {
        put(WIKI, params.get(WIKI));
        put(ACTIONNAME, params.get(ACTIONNAME));
        put(ACTIONNAMESPACE, params.get(ACTIONNAMESPACE));
        put(ACTIONSUMMARY, params.get(ACTIONSUMMARY));
        put(ACTIONHIDDEN, params.get(ACTIONHIDDEN));
        put(ACTIONID, params.get(ACTIONID));
        put(PROCESSID, params.get(PROCESSID));
        put(EXECUTEID, params.get(EXECUTEID));
        put(PROBLEMID, params.get(PROBLEMID));
        put(PARSERID, params.get(PARSERID));
        put(ASSESSID, params.get(ASSESSID));
        put(DURATION, params.get(DURATION));
        put(LOGRESULT, params.get(LOGRESULT));
        put(REFERENCE, params.get(REFERENCE));
        put(TARGET, params.get(TARGET));
        put(AFFINITY, params.get(AFFINITY));
        put(RETURNCODE, params.get(RETURNCODE));
        put(TIMEOUT, params.get(TIMEOUT));
        put(PROCESS_TIMEOUT, params.get(PROCESS_TIMEOUT));
        put(METRICID, params.get(METRICID));
        put(FLOWSSTR, params.get(FLOWSSTR));
        put(INPUTSSTR, params.get(INPUTSSTR));
        put(DEBUGSTR, params.get(DEBUGSTR));

        // interactive params
        put(USERID, params.get(USERID));
        put(COMPLETION, params.get(COMPLETION));
        put(COMMAND, params.get(COMMAND));
        put(RAW, params.get(RAW));
        put(ADDRESS, params.get(ADDRESS));
        put(REQUEST_TIMESTAMP, params.get(REQUEST_TIMESTAMP));
    } // ActionTaskResultVO

    public String createURL(String url, String form, String params)
    {
        String result = url;

        try
        {
            // init form
            form = form.replace('.', '/');

            // init params
            if (params == null)
            {
                params = "";
            }

            // prefix
            result += "/resolve/service/wiki/view/" + form + "?" + params;

            // append vo fields
            result += "&" + VO + "=" + this.encode();
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    } // createURL

    /**
     * Encode the object to base64 string, then URLEncoded
     * 
     * @return
     * @throws Exception
     */
    public String encode() throws Exception
    {
        String result = StringUtils.objToString(this);
        result = java.net.URLEncoder.encode(result, "UTF-8");

        return result;
    } // encode

    public String getActionId()
    {
        return get(ACTIONID);
    }

    public void setActionID(String actionId)
    {
        put(ACTIONID, actionId);
    }

    public String getWiki()
    {
        return get(WIKI);
    }

    public void setWiki(String wiki)
    {
        put(WIKI, wiki);
    }

    public String getActionName()
    {
        return get(ACTIONNAME);
    }

    public void setActionName(String actionname)
    {
        put(ACTIONNAME, actionname);
    }

    public String getActionNamespace()
    {
        return get(ACTIONNAMESPACE);
    }

    public void setActionNamespace(String actionnamespace)
    {
        put(ACTIONNAMESPACE, actionnamespace);
    }

    public String getActionSummary()
    {
        return get(ACTIONSUMMARY);
    }

    public void setActionSummary(String actionsummary)
    {
        put(ACTIONSUMMARY, actionsummary);
    }

    public String getActionHidden()
    {
        return get(ACTIONHIDDEN);
    }

    public void setActionHidden(String actionhidden)
    {
        put(ACTIONHIDDEN, actionhidden);
    }

    public String getActionTags()
    {
        return get(ACTIONTAGS);
    }

    public void setActionTags(String actiontags)
    {
        put(ACTIONTAGS, actiontags);
    }

    public String getActionRoles()
    {
        return get(ACTIONROLES);
    }

    public void setActionRoles(String actionroles)
    {
        put(ACTIONROLES, actionroles);
    }
    
    public String getProcessId()
    {
        return get(PROCESSID);
    }

    public void setProcessID(String processid)
    {
        put(PROCESSID, processid);
    }

    public String getExecuteId()
    {
        return get(EXECUTEID);
    }

    public void setExecuteID(String executeid)
    {
        put(EXECUTEID, executeid);
    }

    public String getProblemId()
    {
        return get(PROBLEMID);
    }

    public void setProblemID(String problemid)
    {
        put(PROBLEMID, problemid);
    }

    public String getParserId()
    {
        return get(PARSERID);
    }

    public void setParserID(String parserid)
    {
        put(PARSERID, parserid);
    }

    public String getAssessId()
    {
        return get(ASSESSID);
    }

    public void setAssessID(String assessid)
    {
        put(ASSESSID, assessid);
    }

    public String getDuration()
    {
        return get(DURATION);
    }

    public void setDuration(String duration)
    {
        put(DURATION, duration);
    }

    public String getLogResult()
    {
        return get(LOGRESULT);
    }

    public void setLogResult(String logresult)
    {
        put(LOGRESULT, logresult);
    }

    public String getReference()
    {
        return get(REFERENCE);
    }

    public void setReference(String reference)
    {
        put(REFERENCE, reference);
    }

    public String getTarget()
    {
        return get(TARGET);
    }

    public void setTarget(String target)
    {
        put(TARGET, target);
    }

    public String getReturnCode()
    {
        return get(RETURNCODE);
    }

    public void setReturnCode(String returncode)
    {
        put(RETURNCODE, returncode);
    }

    public String getTimeout()
    {
        return get(TIMEOUT);
    }

    public void setTimeout(String timeout)
    {
        put(TIMEOUT, timeout);
    }

    public String getProcessTimeout()
    {
        return get(PROCESS_TIMEOUT);
    }

    public void setProcessTimeout(String process_timeout)
    {
        put(PROCESS_TIMEOUT, process_timeout);
    }

    public String getMetricId()
    {
        return get(METRICID);
    }

    public void setMetricId(String metricId)
    {
        put(METRICID, metricId);
    }

    public String getMock()
    {
        return get(MOCK);
    }

    public void setMock(String mock)
    {
        put(MOCK, mock);
    }

    public String getFlowsStr()
    {
        return get(FLOWSSTR);
    }

    public void setFlowsStr(String flowsstr)
    {
        put(FLOWSSTR, flowsstr);
    }

    public String getInputsStr()
    {
        return get(INPUTSSTR);
    }

    public void setInputsStr(String inputsstr)
    {
        put(INPUTSSTR, inputsstr);
    }

    public String getDebugStr()
    {
        return get(DEBUGSTR);
    }

    public void setDebugStr(String debugstr)
    {
        put(DEBUGSTR, debugstr);
    }

    public String getUserId()
    {
        return get(USERID);
    }

    public void setUserID(String userid)
    {
        put(USERID, userid);
    }

    public String getCompletion()
    {
        return get(COMPLETION);
    }

    public void setCompletion(String completion)
    {
        put(COMPLETION, completion);
    }

    public String getCommand()
    {
        return get(COMMAND);
    }

    public void setCommand(String command)
    {
        put(COMMAND, command);
    }

    public String getRaw()
    {
        return get(RAW);
    }

    public void setRaw(String raw)
    {
        put(RAW, raw);
    }

    public String getAddress()
    {
        return get(ADDRESS);
    }

    public void setAddress(String address)
    {
        put(ADDRESS, address);
    }

    public String getAffinity()
    {
        return get(AFFINITY);
    }

    public void setAffinity(String affinity)
    {
        put(AFFINITY, affinity);
    }

    public String getRequestTimestamp()
    {
        return get(REQUEST_TIMESTAMP);
    }

    public void setRequestTimestamp(String requestTimestamp)
    {
        put(REQUEST_TIMESTAMP, requestTimestamp);
    }

} // ActionTaskResultVO
