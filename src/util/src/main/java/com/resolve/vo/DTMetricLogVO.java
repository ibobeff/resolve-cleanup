/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.vo;

import java.io.Serializable;
import java.util.List;


public class DTMetricLogVO implements Serializable
{
    private static final long serialVersionUID = -3371553257872138712L;
    
    private String executionId;//execution Id for a DT
    private String problemId;//workshee that this DT execution is mapped to
    private String rootDtFullName;//name of the root DT wiki
    private List<TMetricLogVO> logs; //list of QA items for this DT
    
    
    public String getExecutionId()
    {
        return executionId;
    }
    public void setExecutionId(String executionId)
    {
        this.executionId = executionId;
    }
    public String getProblemId()
    {
        return problemId;
    }
    public void setProblemId(String problemId)
    {
        this.problemId = problemId;
    }
    public String getRootDtFullName()
    {
        return rootDtFullName;
    }
    public void setRootDtFullName(String rootDtFullName)
    {
        this.rootDtFullName = rootDtFullName;
    }
    public List<TMetricLogVO> getLogs()
    {
        return logs;
    }
    public void setLogs(List<TMetricLogVO> logs)
    {
        this.logs = logs;
    }
    
    
    

}
