/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.vo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.resolve.util.StringUtils;


/**
 * VO that represents the data collected from the UI and sending it to the rsmgmt
 * 
 * @author jeet.marwah
 *
 */
public class TMetricLogVO extends HashMap<String, String> implements Serializable, Comparable<TMetricLogVO>
{
    private static final long serialVersionUID = -7272418247064307445L;

    public final static String EXECUTIONID = "executionId";     //execution id generated when the user starts the DT

    public final static String CURR_WIKI_NODE = "currWikiNode";        //fullname of the wiki
    public final static String START_TIME_IN_MS =  "startTimeInMS";     //time when the result of the DT is loaded on the UI

    public final static String PREV_WIKI_NODE = "prevWikiNode";
    public final static String PREV_WIKI_START_TIME_IN_MS = "prevWikiStartTimeInMS";
    public final static String STOP_TIME_IN_MS = "stopTimeInMS";      //time when the next page is viewed and user is done with this page
    
    public final static String IS_PATH_COMPLETE = "isPathComplete";
    public final static String IS_LEAF = "isLeaf";
    
    public final static String ABORT_TIME_IN_SECS = "abortTimeInSecs";
    
    //path info
//    public final static String PREV_DT_PATH = "prevDTPath";
    public final static String CURR_DT_PATH = "currDTPath";
    public final static String CURR_DT_PATH_TIME_IN_MS = "currDTPathTimeInMS";
    
    public final static String USERNAME = "username";
    
    public final static String ROOT_DT = "root_dt";
    
    public TMetricLogVO() 
    {
        setIsPathComplete("false");
        setIsLeaf("false");
    }
    
    @SuppressWarnings("unchecked")
    public TMetricLogVO(String voStr)
    {
        super((Map<String, String>) StringUtils.stringToObj(voStr));
    }
    
    @SuppressWarnings("unchecked")
    public TMetricLogVO(String voStr, boolean urlDecode) throws Exception
    {
        super((Map<String, String>) StringUtils.stringToObj(java.net.URLDecoder.decode(voStr, "UTF-8")));
    } // TMetricLog
    
    public String encode() throws Exception
    {
        String result = StringUtils.objToString(this);
        result = java.net.URLEncoder.encode(result, "UTF-8");

        return result;
    } // encode
    
    //setters - getters
    public String getExecutionId()
    {
        return get(EXECUTIONID);
    }
    public void setExecutionId(String executionId)
    {
        put(EXECUTIONID, executionId);
    }
    public String getCurrWikiNode()
    {
        return get(CURR_WIKI_NODE);
    }
    public void setCurrWikiNode(String currWikiNode)
    {
        put(CURR_WIKI_NODE, currWikiNode);
    }
    public String getStartTimeInMS()
    {
        return get(START_TIME_IN_MS);
    }
    public void setStartTimeInMS(String startTimeInMS)
    {
        put(START_TIME_IN_MS, startTimeInMS);
    }
    public String getPrevWikiNode()
    {
        return get(PREV_WIKI_NODE);
    }
    public void setPrevWikiNode(String prevWikiNode)
    {
        put(PREV_WIKI_NODE, prevWikiNode);
    }
    public String getPrevWikiStartTimeInMS()
    {
        return get(PREV_WIKI_START_TIME_IN_MS);
    }
    public void setPrevWikiStartTimeInMS(String prevWikiStartTimeInMS)
    {
        put(PREV_WIKI_START_TIME_IN_MS, prevWikiStartTimeInMS);
    }
    
    public String getStopTimeInMS()
    {
        return get(STOP_TIME_IN_MS);
    }
    public void setStopTimeInMS(String stopTimeInMS)
    {
        put(STOP_TIME_IN_MS, stopTimeInMS);
    }

    public String getIsPathComplete()
    {
        return get(IS_PATH_COMPLETE);
    }
    public void setIsPathComplete(String Status)
    {
        put(IS_PATH_COMPLETE, Status);
    }
    
    public String getIsLeaf()
    {
        return get(IS_LEAF);
    }
    public void setIsLeaf(String Status)
    {
        put(IS_LEAF, Status);
    }
    
    public String getCurrDTPath()
    {
        return get(CURR_DT_PATH);
    }
    public void setCurrDTPath(String path)
    {
        put(CURR_DT_PATH, path);
    }

    public String getCurrDTPathTimeInMS()
    {
        return get(CURR_DT_PATH_TIME_IN_MS);
    }
    public void setCurrDTPathTimeInMS(String path)
    {
        put(CURR_DT_PATH_TIME_IN_MS, path);
    }
    
    public String getUsername()
    {
        return get(USERNAME);
    }
    public void setUsername(String username)
    {
        put(USERNAME, username);
    }
    
    public String getRootDt()
    {
        return get(ROOT_DT);
    }
    public void setRootDt(String rootDt)
    {
        put(ROOT_DT, rootDt);
    }

    public String getAbortTimeInSecs()
    {
        return get(ABORT_TIME_IN_SECS);
    }
    public void setAbortTimeInSecs(String AbortTimeInSecs)
    {
        put(ABORT_TIME_IN_SECS, AbortTimeInSecs);
    }
    
    public String toString()
    {
        return CURR_WIKI_NODE + "=" + getCurrWikiNode() + ", "
              + START_TIME_IN_MS + "=" + getStartTimeInMS() + ", "
              + PREV_WIKI_NODE + "=" + getPrevWikiNode() + ", "
              + PREV_WIKI_START_TIME_IN_MS + "=" + getPrevWikiStartTimeInMS() + ", "
              + STOP_TIME_IN_MS + "=" + getStopTimeInMS() + ", "
              + IS_PATH_COMPLETE + "=" + getIsPathComplete() + ", "
              + CURR_DT_PATH + "=" + getCurrDTPath() + ","
              + CURR_DT_PATH_TIME_IN_MS + "=" + getCurrDTPathTimeInMS() + ", "
              + "NODE_TIME=" + getNodeTimeInMS() + ", "
              + IS_LEAF + "=" + getIsLeaf() + ", "
              + ROOT_DT + "=" + getRootDt() + ", "
              + EXECUTIONID + "=" + getExecutionId() + ", ";
        
    }

    public long getNodeTimeInMS()
    {
        long time = -1;
        
        try
        {
            Long startTime = Long.parseLong(getPrevWikiStartTimeInMS());
            Long stopTime = Long.parseLong(getStopTimeInMS());
            
            time = stopTime - startTime;
        }
        catch(Throwable t)
        {
            //some issue in the calculation
        }
        
        
        
        return time;
    }
    
    public int compareTo(TMetricLogVO o)
    {
        //order by start time which gets us the sequence of events in the asc order
        String o1 = getStartTimeInMS();
        String o2 = o.getStartTimeInMS();
        int returnValue = 0;
        
        if(o1 != null && o2 != null && o1.trim().length() > 0 && o2.trim().length() > 0)
        {
            Long o1Long = Long.parseLong(o1);
            Long o2Long = Long.parseLong(o2);
            
            returnValue = (o1Long < o2Long) ? -1 : (o1Long > o2Long ? 1 : 0);
        }
        else if (o1 != null)
        {
            returnValue = -1;
        }
        else
        {
            System.out.println("METRIC: This case where o1 and o2 both are null should not exist. Please verify what is going wrong");
        }
        
        
        return returnValue;
    }

    
    
}
