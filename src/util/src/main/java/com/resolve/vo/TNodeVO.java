/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.vo;

import java.io.Serializable;


/**
 * representing the node in the tree for the DT or Runbook 
 * 
 * @author jeet.marwah
 *
 */
public class TNodeVO implements Serializable, Comparable<TNodeVO>
{
    private static final long serialVersionUID = 3523115861902789070L;
    
    private String wikiFullName;
    private Long startTimeInMS;
    private Long stopTimeInMS;
    private String username;
    private Boolean isPathComplete = false;
    private Boolean isLeaf = false;
    private Integer abortTimeInSecs;
    private String currWikiFullName;
    private String rootDtFullName;
    
    public TNodeVO() {}
    
    public TNodeVO(String wikiFullName, Long startTime)
    {
        setWikiFullName(wikiFullName);
        setStartTimeInMS(startTime);
    }
    
    public TNodeVO(String wikiFullName, Long startTime, Long stopime)
    {
        this(wikiFullName, startTime);
        setStopTimeInMS(stopime);
    }

    public int compareTo(TNodeVO o)
    {
        //order by start time which gets us the sequence of events in the asc order
        int returnValue = getStartTimeInMS() < o.getStartTimeInMS() ? -1 : (getStartTimeInMS() > o.getStartTimeInMS() ? 1 : 0);
        return returnValue;
    }
    
    public long getNodeTimeInMS()
    {
        long time = -1;
        
        if(getStopTimeInMS() != null)
        {
            time = getStopTimeInMS() - getStartTimeInMS();
        }
        
        return time;
    }

    //setters-getters
    public String getWikiFullName()
    {
        return wikiFullName;
    }

    public void setWikiFullName(String wikiFullName)
    {
        this.wikiFullName = wikiFullName;
    }

    public Long getStartTimeInMS()
    {
        return startTimeInMS;
    }

    public void setStartTimeInMS(Long startTime)
    {
        this.startTimeInMS = startTime;
    }

    public Long getStopTimeInMS()
    {
        return stopTimeInMS;
    }

    public void setStopTimeInMS(Long stopTime)
    {
        this.stopTimeInMS = stopTime;
    }

    public Boolean getIsPathComplete()
    {
        return isPathComplete;
    }

    public void setIsPathComplete(Boolean isPathComplete)
    {
        this.isPathComplete = isPathComplete;
    }
    
    public Boolean getIsLeaf()
    {
        return isLeaf;
    }

    public void setIsLeaf(Boolean isLeaf)
    {
        this.isLeaf = isLeaf;
    }
    
    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }
    
    public Integer getAbortTimeInSecs()
    {
        return abortTimeInSecs;
    }

    public void setAbortTimeInSecs(Integer abortTimeInSecs)
    {
        this.abortTimeInSecs = abortTimeInSecs;
    }

    public String getCurrWikiFullName()
    {
        return currWikiFullName;
    }

    public void setCurrWikiFullName(String currWikiFullName)
    {
        this.currWikiFullName = currWikiFullName;
    }
    
    public String getRootDtFullName()
    {
        return rootDtFullName;
    }

    public void setRootDtFullName(String rootDtFullName)
    {
        this.rootDtFullName = rootDtFullName;
    }

    public String toString()
    {
        return "wikiFullName=" + getWikiFullName() + ", "
              + "rootDtFullName=" + getRootDtFullName() + ", "
              + "NODE_TIME=" + getNodeTimeInMS() + ", "
              + "IsPathComplete=" + getIsPathComplete() + ", "
              + "isLeaf=" + getIsLeaf() + ", "
              + "user=" + getUsername() + ", "
              + "currWiki=" + getCurrWikiFullName() + ", "
              + "abortTimeInSecs=" + getAbortTimeInSecs();
    }
}
