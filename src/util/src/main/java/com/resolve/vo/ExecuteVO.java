/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.vo;

import java.util.HashMap;

public class ExecuteVO extends HashMap<String, String>
{

	private static final long serialVersionUID = 8657634310804063284L;
    /*
     * public static String ACTIONID = Constants.ACTIONID; public static String
     * ACTIONNAME = Constants.ACTIONNAME; public static String NAMESPACE =
     * Constants.NAMESPACE; public static String PROCESSID =
     * Constants.PROCESSID;
     * 
     * public ExecuteVO(Map<String,String> params) { put(ACTIONID,
     * params.get(ACTIONID)); }
     * 
     * public String getActionId() { return get(ACTIONID); }
     * 
     * public void setActionId(String actionId) { put(ACTIONID, actionId); }
     */

} // ExecuteVO
