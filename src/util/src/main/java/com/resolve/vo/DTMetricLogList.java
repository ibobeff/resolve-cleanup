/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.vo;

import java.io.Serializable;
import java.util.List;

//list creates an issue, so creating a VO for saving the list in WSDATA
public class DTMetricLogList implements Serializable
{
    private static final long serialVersionUID = -3883151452382256255L;
    
//    private String rootDt;
    private List<DTMetricLogVO> dtMetricLogs;
    
//    public String getRootDt()
//    {
//        return rootDt;
//    }
//    public void setRootDt(String rootDt)
//    {
//        this.rootDt = rootDt;
//    }
    public List<DTMetricLogVO> getDtMetricLogs()
    {
        return dtMetricLogs;
    }
    public void setDtMetricLogs(List<DTMetricLogVO> dtMetricLogs)
    {
        this.dtMetricLogs = dtMetricLogs;
    }

}
