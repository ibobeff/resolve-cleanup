/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.html;

import java.util.Map;

import com.resolve.util.StringUtils;

public class HtmlUtil
{

    public static String createFormScript(String renderTo, String viewName, String formId, String sys_id, String query, int height)
    {
        if (renderTo == null) renderTo = "";
        if (viewName == null) viewName = "";
        // clean the viewName
        viewName = viewName.replace("'", "\\\\\'");
        if (formId == null) formId = "";
        if (sys_id == null) sys_id = "";
        if (query == null) query = "";

        StringBuffer script = new StringBuffer("");
        script.append("{pre}");
        script.append("<script type=\"text/javascript\">\n");
        script.append("Ext.onReady(function(){\n");
        script.append("Ext.QuickTips.init();\n");
        script.append("            Ext.create('RS.formbuilder.viewer.desktop.RSForm', {\n");
        script.append("                    renderTo : '" + renderTo + "',\n");
        script.append("                    formName : '" + viewName + "',\n");
        script.append("                    formId : '" + formId + "',\n");
        script.append("                    queryString: '" + query + "',\n");
        script.append("                    height: " + height + ",\n");
        script.append("                    recordId: '" + sys_id + "'\n");
        script.append("            });\n");
        script.append("});\n");
        script.append("</script>\n");
        script.append("{pre}");
        script.append("<div id=\"" + renderTo + "\"></div>");
        return script.toString();
    }

    public static String createFormVelocityScript(Map<String, String> viewsToForm)
    {
        StringBuffer velocity = new StringBuffer("");

        // Always need the sys_id
        velocity.append("#set($sys_id = $request.getParameter(\"sys_id\"))").append("\n");

        // Get the view
        velocity.append("#set($view = $request.getParameter(\"view\"))").append("\n");
        velocity.append("#set($view = $wiki.evaluateViewLookup($view))").append("\n");

        // Now deal with the view mapping
        String defaultView = "";
        StringBuffer sets = new StringBuffer();
        int count = 0;
        for (String key : viewsToForm.keySet())
        {
            if (key.equalsIgnoreCase("Default"))
            {
                defaultView = viewsToForm.get(key);
            }
            else
            {
                StringBuffer option = new StringBuffer("#");
                if (count > 0) option.append("else");
                option.append("if( $view==\"" + key + "\")\n");
                option.append("#set($form=\"" + viewsToForm.get(key) + "\")\n");
                sets.append(option);
                count++;
            }
        }

        if (count == 0)
        {
            velocity.append("#set($form=\"" + defaultView + "\")\n");
        }
        else
        {
            if (StringUtils.isNotEmpty(defaultView))
            {
                sets.append("#else\n");
                sets.append("#set($form=\"" + defaultView + "\")\n");
                count++;
            }
            sets.append("#end\n");
            velocity.append(sets);
        }

        return velocity.toString();
    }

    public static String createInfoBarMacro(String height, String displaySocialTab, String displayFeedbackTab, String displayRatingTab, String displayAttachmentsTab, String displayHistoryTab, String displayTagsTab, String displayPageInfoTab)
    {
        StringBuffer macro = new StringBuffer();
        macro.append("{infobar:");
        macro.append("height=" + height + "|");
        macro.append("social=" + displaySocialTab + "|");
        macro.append("feedback=" + displayFeedbackTab + "|");
        macro.append("rating=" + displayRatingTab + "|");
        macro.append("attachments=" + displayAttachmentsTab + "|");
        macro.append("history=" + displayHistoryTab + "|");
        macro.append("tags=" + displayTagsTab + "|");
        macro.append("pageInfo=" + displayPageInfoTab);
        macro.append("}");
        return macro.toString();
    }

    /**
     * <iframe src=
     * "/resolve/jsp/rstable.jsp?main=form&form=final51view1&table=final51&view=final51view1&sys_id=$sys_id&query="
     * style="position:absolute;width:99%;height:300;border:0"></iframe>
     * 
     * @param viewname
     * @param tablename
     * @param iframeHeight
     * @param params
     * @return
     */
    public static String createFormIframe(String viewname, String tablename, int iframeHeight, Map<String, String> params)
    {
        StringBuffer result = new StringBuffer("<iframe src=\"/resolve/formbuilder/formviewer.jsp?main=form");
        result.append("&form=").append(viewname).append("&table=").append(tablename);

        if (params != null && params.size() > 0)
        {
            result.append("&").append(StringUtils.mapToUrl(params));
        }
        result.append("\" style=\"position:absolute;width:99%;height:").append(iframeHeight).append(";border:0\"></iframe>");

        return result.toString();
    }// createFormIframe

    /*
    public static void main(String[] args)
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("sys_id", "abc123");
        params.put("key1", "val1");
        params.put("key2", "val2");

        System.out.println(createFormIframe("view1", "table1", 300, null));
        System.out.println(createFormIframe("view2", "table2", 300, params));
    }
    */

}
