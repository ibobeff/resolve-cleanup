/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.html;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.resolve.util.StringUtils;

public class Table
{
    // public static String SERVER_URL = "";
    /*
     * Print ListMap table
     */

    public static String print(List data)
    {
        return print(data, false, null);
    } // print

    public static String print(List data, Properties properties)
    {
        return print(data, false, properties);
    } // print

    public static String print(List data, boolean firstRowIsHeading)
    {
        return print(data, firstRowIsHeading, null);
    } // print

    public static String print(List data, boolean firstRowIsHeading, Properties properties)
    {
        String result = "";

        if (data != null && data.size() > 0)
        {
            Map row = (Map) data.iterator().next();
            if (row != null)
            {
                // get column heading / display names
                String[] keyNames = null;
                String[] displayNames = null;

                // init keyNames and displayNames
                Map firstRow = (Map) data.get(0);
                if (firstRow != null)
                {
                    keyNames = new String[firstRow.size()];
                    displayNames = new String[firstRow.size()];
                }

                int pos = 0;
                for (Iterator i = row.entrySet().iterator(); i.hasNext();)
                {
                    Map.Entry entry = (Map.Entry) i.next();
                    String key = (String) entry.getKey();
                    String value = (String) entry.getValue();

                    if (keyNames[pos] == null)
                    {
                        keyNames[pos] = "";
                    }
                    if (displayNames[pos] == null)
                    {
                        displayNames[pos] = "";
                    }

                    keyNames[pos] += key;
                    if (firstRowIsHeading)
                    {
                        displayNames[pos] += value;
                    }
                    else
                    {
                        displayNames[pos] += key;
                    }

                    if (i.hasNext())
                    {
                        pos++;
                    }
                }
                result = print(data, keyNames, displayNames, firstRowIsHeading, properties);
            }
        }

        return result;
    } // print

    public static String print(List data, String keyNames)
    {
        return print(data, keyNames, keyNames, false, null);
    } // print

    public static String print(List data, String keyNames, Properties properties)
    {
        return print(data, keyNames, keyNames, false, properties);
    } // print

    public static String print(List data, String keyNames, boolean skipFirstRow)
    {
        return print(data, keyNames, skipFirstRow, null);
    } // print

    public static String print(List data, String keyNames, boolean skipFirstRow, Properties properties)
    {
        return print(data, keyNames, keyNames, skipFirstRow, properties);
    } // print

    public static String print(List data, String keyNames, String displayNames)
    {
        return print(data, keyNames, displayNames, null);
    } // print

    public static String print(List data, String keyNames, String displayNames, Properties properties)
    {
        return print(data, keyNames, displayNames, false, properties);
    } // print

    public static String print(List data, Set keyNames)
    {
        return print(data, keyNames, null);
    } // print

    public static String print(List data, Set keyNames, Properties properties)
    {
        String[] columnNames = new String[keyNames.size()];

        int idx = 0;
        for (Iterator i = keyNames.iterator(); i.hasNext();)
        {
            columnNames[idx++] = (String) i.next();
        }
        return print(data, columnNames, columnNames, false, properties);
    } // print

    public static String print(List data, String keyNames, String displayNames, boolean skipFirstRow)
    {
        return print(data, keyNames, displayNames, skipFirstRow, null);
    } // print

    public static String print(List data, String keyNames, String displayNames, boolean skipFirstRow, Properties properties)
    {
        if (StringUtils.isEmpty(displayNames))
        {
            displayNames = keyNames;
        }
        return print(data, keyNames.split(","), displayNames.split(","), skipFirstRow, properties);
    } // print

    public static String print(List data, String[] keyNames, String[] displayNames, boolean skipFirstRow)
    {
        return print(data, keyNames, displayNames, skipFirstRow, null);
    } // print

    public static String print(List data, String[] keyNames, String[] displayNames, boolean skipFirstRow, Properties properties)
    {
        StringBuilder result = new StringBuilder();
        int start = 0;

        // skip heading row
        if (skipFirstRow)
        {
            start = 1;
        }

        if (keyNames != null && keyNames.length > 0)
        {
            if (displayNames == null)
            {
                displayNames = keyNames;
            }

            // print table header
            String style = getTableStyle(properties);
            result.append("<table style=\"" + style + "\" >");

            // print column headings
            result.append("<tr class=\"header\" >");
            for (int i = 0; i < displayNames.length; i++)
            {
                // get heading style property
                String keyName = keyNames[i].trim();
                style = getHeadingStyle(keyName, properties);

                result.append("<td style=\"padding-left: 5px; padding-right: 5px;" + style + "\" >");
                result.append(displayNames[i].trim());
                result.append("</td>");
            }
            result.append("</tr>");

            // print rows
            boolean isOdd = true;
            for (int i = start; i < data.size(); i++)
            {
                Map row = (Map) data.get(i);

                if (isOdd)
                {
                    result.append("<tr class=\"odd\" valign=\"top\">");
                    isOdd = !isOdd;
                }
                else
                {
                    result.append("<tr class=\"even\" valign=\"top\">");
                    isOdd = !isOdd;
                }

                for (int keyIdx = 0; keyIdx < keyNames.length; keyIdx++)
                {
                    String keyName = keyNames[keyIdx].trim();
                    String value = (String) row.get(keyName);
                    if (value == null)
                    {
                        value = "";
                    }
                    else
                    {
                        // value = encode(value.trim());
                        String doEscape = getEscapeStyle(properties);
                        if (doEscape == null || doEscape.equalsIgnoreCase("true"))
                        {
                            value = StringUtils.escapeHtml(value.trim());
                        }
                        else
                        {
                            value = value.trim();
                        }
                    }
                    String url = (String) row.get("LINK:" + keyName);

                    style = getDataStyle(keyName, value, properties);
                    result.append("<td style=\"" + style + "\" >");

                    if (url != null && url.indexOf("service") > -1)
                    {
                        result.append(getValueLink(value, url, keyName, true));
                    }
                    else if(url != null && url.indexOf("dtViewer") > -1)
                    {
                        result.append(getValueLink(value, url, keyName, true)); //exception case so dtview pops out like expected
                    }
                    else
                    {
                        result.append(getValueLink(value, url, keyName, false));
                    }

                    result.append("</td>");
                }
                result.append("</tr>");
            }

            // print table footer
            result.append("</table>");
        }

        return result.toString();
    } // print

    /*
     * Print MAP table methods
     */

    public static String print(Map data)
    {
        return print(data, "Name", "Value");
    } // print

    public static String print(Map data, boolean includeHeading)
    {
        String result = "";

        if (includeHeading)
        {
            result = print(data, "Name", "Value");
        }
        else
        {
            result = print(data, null, null);
        }

        return result;
    } // print

    public static String print(Map data, String nameHeading, String valueHeading)
    {
        StringBuilder result = new StringBuilder();

        // print table header
        result.append("<table cellspacing='2px'>");

        // print column headings
        if (!StringUtils.isEmpty(nameHeading) && !StringUtils.isEmpty(valueHeading))
        {
            result.append("<tr class=\"header\">");
            result.append("<td style=\"padding-left: 5px; padding-right: 5px;\" >");
            result.append(nameHeading);
            result.append("</td>");
            result.append("<td style=\"padding-left: 5px; padding-right: 5px;\" >");
            result.append(valueHeading);
            result.append("</td>");
            result.append("</tr>");
        }

        // print rows
        boolean isOdd = true;
        for (Iterator i = data.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry entry = (Map.Entry) i.next();
            String key = (String) entry.getKey();
            if (!StringUtils.isEmpty(key))
            {
                String value = (entry.getValue()).toString();

                if (isOdd)
                {
                    result.append("<tr class=\"odd\" valign=\"top\">");
                    isOdd = !isOdd;
                }
                else
                {
                    result.append("<tr class=\"even\" valign=\"top\">");
                    isOdd = !isOdd;
                }

                result.append("<td>");
                result.append(key);
                result.append("</td>");
                result.append("<td>");
                result.append(value);
                result.append("</td>");
                result.append("</tr>");
            }
        }

        // print table footer
        result.append("</table>");

        return result.toString();
    } // print

    /*
     * static String encode(String text) { String result = text;
     * 
     * result = result.replaceAll("<", "&lt;"); result = result.replaceAll(">",
     * "&gt;");
     * 
     * return result; } // encode
     */

    static String getEscapeStyle(Properties properties)
    {
        String style = null;

        if (properties != null)
        {
            String styleName = "html.escape";
            style = properties.getProperty(styleName);
        }

        return style;
    } // getEscapeStyle

    static String getTableStyle(Properties properties)
    {
        String style = "";

        if (properties != null)
        {
            String styleName = "table.style";
            style = properties.getProperty(styleName) != null ? properties.getProperty(styleName) : "";
        }

        style += "width: 100%;";

        return style;
    } // getTableStyle

    static String getHeadingStyle(String keyName, Properties properties)
    {
        String style = "";

        if (properties != null)
        {
            String styleName = "heading.style." + keyName.toLowerCase();
            style = properties.getProperty(styleName);
            if (style == null)
            {
                styleName = "heading.style.*";
                style = properties.getProperty(styleName);
                if (style == null)
                {
                    style = "";
                }
            }
        }
        return style;
    } // getHeadingStyle

    static String getDataStyle(String keyName, String value, Properties properties)
    {
        String style = "";

        // set default style
        if (properties != null)
        {
            String styleName = "data.style." + keyName.toLowerCase();
            style = properties.getProperty(styleName);
            if (style == null)
            {
                styleName = "data.style.*";
                style = properties.getProperty(styleName);
                if (style == null)
                {
                    style = "";
                }
            }

            if (!StringUtils.isEmpty(style))
            {
                style += ";";
            }

            // set value-specific style
            styleName = "data.style." + keyName.toLowerCase() + "." + value.trim().toLowerCase();
            style += properties.getProperty(styleName);
            if (style == null)
            {
                style = "";
            }
        }

        return style;
    } // getDataStyle

    static String getValueLink(String value, String url, String keyName, boolean newTab)
    {
        String result = value;

        if (!StringUtils.isEmpty(url))
        {
            String styleClass = "";
            
            if("DESCRIPTION".equals(keyName))
            {
                //If this is a description link, apply description style
                styleClass = "wiki-macro-result-description-link-custom";
            }
            else
            {
                //Otherwise it is detail - check if it is the default or a custom link
                if (url.startsWith("/resolve/service/result/task/detail"))
                {
                    styleClass = "wiki-macro-result-condition-link-default";
                }
                else
                {
                    styleClass = "wiki-macro-result-condition-link-custom";
                }
            }

            if(newTab)
            {
                result = "<a class=\"" + styleClass + "\" onClick ='gotoURL(\"" + url + "\", \"" + url + "\")'>" + value + "</a>";
            }
            else
            {
                result = "<a href=\"" + url + "\" class=\"" + styleClass + "\">" + value + "</a>";
            }
        }

        return result;
    } // getValueLink

    /*
    //Consolidated into one method: getValueLink
    static String getValueLinkOpenTab(String value, String url, String keyName)
    {
        String result = value;
        
        if (!StringUtils.isEmpty(url))
        {
            String style = null;
            if (url.startsWith("/resolve/service/result/task/detail"))
            {
                style = "wiki-macro-result-condition-link-default";
            }
            else
            {
                style = "wiki-macro-result-condition-link-custom";
            }
            
            result = "<a class=\"" + style + "\" onClick ='gotoURL(\"" + url + "\", \"" + url + "\")'>" + value + "</a>";
        }

        return result;
    } // getValueLinkOpenTab
    */

} // Table

