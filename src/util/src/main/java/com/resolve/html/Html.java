/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.html;

import java.util.List;
import java.util.Map;

import com.resolve.util.StringUtils;

public class Html
{
    public static final String HTML_BEGIN = "<!-- HTML_BEGIN -->";
    public static final String HTML_END = "<!-- HTML_END -->";
    public static final String COLOR_GREY = "grey";
    public static final String COLOR_RED = "red";
    public static final String COLOR_BLUE = "blue";
    public static final String COLOR_GREEN = "green";
    public static final String URL_CLOSE = "close";

    // html content
    StringBuffer content = new StringBuffer();

    public Html()
    {
    }

    public Html(String initString)
    {
        init(initString);
    } // Html

    public void init(String initString)
    {
        if (StringUtils.isNotEmpty(initString))
        {
            int beginPos = initString.indexOf(HTML_BEGIN);
            int endPos = initString.indexOf(HTML_END);

            // does not contain HTML tags
            if (beginPos == -1 && endPos == -1)
            {
                content.append(initString);
            }

            // has begin but no end
            else if (beginPos != -1 && endPos == -1)
            {
                initString = initString.substring(beginPos + HTML_BEGIN.length());
            }
            // has end but no begin
            else if (beginPos == -1 && endPos != -1)
            {
                initString = initString.substring(0, endPos);
            }
            // has begin and end
            else
            {
                initString = initString.substring(beginPos + HTML_BEGIN.length(), endPos);
            }
            content.append(initString);
        }
    } // init

    public StringBuffer getContent()
    {
        return content;
    } // getContent

    public String toString()
    {
        String result = HTML_BEGIN + content.toString() + HTML_END;
        return result;
    } // toString

    public Html print(final String str)
    {
        final String htmlString = convertToHtml(str);
        content.append(htmlString);
        return this;
    } // print

    public Html println(String str)
    {
        final String htmlString = convertToHtml(str);
        content.append(htmlString + "<br/>");
        return this;
    } // println

    public Html println()
    {
        return br();
    } // println

    public String convertToHtml(final String str)
    {
        String result = str.replaceAll("\n", "<br/>");
        return result;
    } // convertToHtml

    public Html button(String text, String url)
    {
        content.append(getButton(text, url));
        return this;
    } // button

    public Html button(String text, String url, String color)
    {
        content.append(getButton(text, url, color));
        return this;
    } // button

    public Html buttons(List<Map<String, Object>> buttons)
    {
        content.append("<table>");
        content.append("<tr>");

        for (Map<String, Object> button : buttons)
        {
            String text = StringUtils.getString("text", button);
            String url = StringUtils.getString("url", button);
            String color = StringUtils.getString("color", button);

            content.append("<td style='padding-right:5px'>");
            button(text, url, color);
            content.append("</td>");
        }

        content.append("</tr>");
        content.append("</table>");
        return this;
    } // buttons

    public String getButton(String text, String url)
    {
        return getButton(text, url, "grey");
    } // getButton

    public String getButton(String text, String url, String color)
    {
        String result = "";

        String bgcolor = null;
        String bordercolor = null;
        String textcolor = null;

        if (color.equalsIgnoreCase(COLOR_RED))
        {
            bgcolor = "#C53727";
            bordercolor = "rgba(197, 55, 39, 1)";
            textcolor = "#ffffff";
        }
        else if (color.equalsIgnoreCase(COLOR_BLUE))
        {
            bgcolor = "#357AE8";
            bordercolor = "rgba(47, 91, 183, 1)";
            textcolor = "#ffffff";
        }
        else if (color.equalsIgnoreCase(COLOR_GREEN))
        {
            bgcolor = "#53b02c";
            bordercolor = "rgba(0, 0, 0, 0.1)";
            textcolor = "#ffffff";
        }
        else
        {
            bgcolor = "#F1F1F1";
            bordercolor = "rgba(0, 0, 0, 0.1)";
            textcolor = "#333333";
        }

        // insert special url
        String onclick = "";
        String target = "target='_blank'";
        if (url.equals(URL_CLOSE))
        {
            onclick = "window.top.close();";
            target = "";
            url = "#";
        }

        // generate result
        result = "<table border='0' cellpadding='6' cellspacing='1' align=''><tbody><tr><td align='center' valign='middle' bgcolor='" + bgcolor + "' style='background: repeat-x scroll 100% 0 " + bgcolor + ";background-color:" + bgcolor + ";border:1px solid " + bordercolor + ";border-radius:4px;padding: 6px;'><div style='padding-right:10px;padding-left:10px'><a onclick='" + onclick + "' href='" + url + "' style='text-decoration:none' " + target
                        + "><span style='font-size:12px;font-family:Arial;font-weight:bold;color:" + textcolor + ";white-space:nowrap;display:block'>" + text + "</span></a></div></td></tr></tbody></table>";

        return result;
    } // getButton

    public Html br()
    {
        content.append("<br/>");
        return this;
    } // br

    public Html br(int repeat)
    {
        for (int i = 0; i < repeat; i++)
        {
            content.append("<br/>");
        }
        return this;
    } // br

    public Html table(String tablebody)
    {
        content.append("<table>");
        content.append(tablebody);
        content.append("</table>");
        return this;
    } // table

} // Html
