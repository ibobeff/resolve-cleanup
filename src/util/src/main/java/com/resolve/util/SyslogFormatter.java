package com.resolve.util;

public enum SyslogFormatter
{

    INSTANCE;

    private String SYSLOG_AUDIT_FORMAT = "[Audit] %s %s %s - %s";
    // TODO Please add more templates for the SYSLOG formatting here

    public String formatAudit(String remoteAddress, String username, String localAddress, String message)
    {
        if (remoteAddress == null || username == null || localAddress == null || message == null)
        {
            Log.log.error("Syslog format parameters are all required");
        }
        return String.format(this.SYSLOG_AUDIT_FORMAT, remoteAddress, username, localAddress, message);
    }

    // TODO Please add more formatters for the SYSLOG here

}
