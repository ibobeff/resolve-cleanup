/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class ReportUtils
{
    public static final String DEFAULT_REPORT_NAME = "Report.pdf";
    public static final String DEFAULT_PATH = "tmp/";
    public static final String DEFAULT_EXPORT_FORMAT = "&outtype=PDF&match=false";

    public static String exportReportAsPdf(String reportURL, String saveFile) throws IOException
    {
        String message = null;
        HttpURLConnection connection = null;
        BufferedInputStream in;
        FileOutputStream out;
        URL serverAddress = null;

        try
        {
            if (StringUtils.isEmpty(reportURL))
            {
                Log.log.error("Report URL is empty or null");
            }
            serverAddress = new URL(reportURL + DEFAULT_EXPORT_FORMAT);
            // set up out communications stuff
            connection = null;

            // Set up the initial connection
            connection = (HttpURLConnection) serverAddress.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setReadTimeout(10000);
            connection.connect();

            in = new BufferedInputStream(connection.getInputStream());

            if (StringUtils.isEmpty(saveFile))
            {
                saveFile = getFileName();
            }

            out = new FileOutputStream(saveFile);
            byte[] buf = new byte[256];
            int n = 0;
            while ((n = in.read(buf)) >= 0)
            {
                out.write(buf, 0, n);
            }
            out.flush();
            out.close();
            message = "Exported file successfully as " + saveFile;
        }
        catch (MalformedURLException e)
        {
            Log.log.error("Error during exporting report", e);
            throw e;
        }
        catch (ProtocolException e)
        {
            Log.log.error("Error during exporting report", e);
            throw e;
        }
        catch (IOException e)
        {
            Log.log.error("Error during exporting report", e);
            throw e;
        }
        finally
        {
            // close the connection, set all objects to null
            connection.disconnect();
            in = null;
            out = null;
            connection = null;
        }

        return message;

    } // exportReportAsPdf

    private static String getFileName()
    {
        String fileName = DEFAULT_PATH + DEFAULT_REPORT_NAME;
        return fileName;

    } // getFileName

    public static String exportReport(String reportName, String saveFile) throws IOException
    {
        if (StringUtils.isEmpty(reportName))
        {
            Log.log.error("Invalid Report Name and Report Path");
        }
        reportName = reportName.replace(" ", "%20");
        String reportURL = "http://localhost:8080/sree/Reports?op=vs&path=" + reportName;
        String result = exportReportAsPdf(reportURL, saveFile);
        return result;

    } // exportReport

    public static String exportReport(String reportPath, String reportName, String saveFile) throws IOException
    {
        if (StringUtils.isEmpty(reportName))
        {
            Log.log.error("Invalid Report Name");
        }
        reportName = reportName.replace(" ", "%20");
        String reportURL = reportPath + reportName;
        String result = exportReportAsPdf(reportURL, saveFile);
        return result;

    } // exportReport

    public static String exportReport(String hostname, String port, String reportName, String saveFile) throws IOException
    {
        if (StringUtils.isEmpty(reportName))
        {
            Log.log.error("Invalid Report Name and Report Path");
        }
        reportName = reportName.replace(" ", "%20");
        String reportURL = "http://" + hostname + ":" + port + "/sree/Reports?op=vs&path=" + reportName;
        String result = exportReportAsPdf(reportURL, saveFile);
        return result;

    } // exportReport

    /**
     * @param args
     */
//    public static void main(String[] args)
//    {
//        // Export report
//        try
//        {
//            ReportUtils.exportReportAsPdf("http://localhost:8080/sree/Reports?op=vs&path=/Resolve/Runbook/Weekly Execution Summary", "C:/project/resolve3/dist/tmp/Report.pdf");
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//        }
//    }

} // ReportUtils
