/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.resolve.util.FileUtils;

public class ImageUtil
{
    public static File resizeImage(File originalFile, int IMG_WIDTH, int IMG_HEIGHT) throws IOException
    {
        String fileName = originalFile.getName();
        String dir = originalFile.getParent();
        String extension = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());

        BufferedImage originalImage = ImageIO.read(originalFile);
        int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();

        BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        g.dispose();

        File file = FileUtils.getFile(dir + "resized-" + fileName);
        ImageIO.write(resizedImage, extension, file);

        return file;
    }
}
