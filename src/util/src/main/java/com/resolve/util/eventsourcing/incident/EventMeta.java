package com.resolve.util.eventsourcing.incident;

import java.io.Serializable;
import java.util.Map;

public class EventMeta implements Serializable
{
    private String source;
    private Map<String, String> incommingData;
    
    public EventMeta(String source, Map<String, String> params)
    {
        this.source = source;
        this.incommingData = params;
    }

    public void setIncommingData(Map<String, String> incommingData)
    {
        this.incommingData = incommingData;
    }
    
    public Map<String, String> getIncommingData()
    {
        return incommingData;
    }
    
    public void setSource(String source)
    {
        this.source = source;
    }
    
    public String getSource()
    {
        return source;
    }

}
