package com.resolve.util.clcapiclient;

import java.util.Map;

import javax.net.ssl.SSLSession;

import org.codehaus.jettison.json.JSONObject;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;


public class CLCInterface
{
    private CLCConnect conn;
    
    public CLCInterface(CLCConnect conn) {
        this.conn = conn;
    }
    
    public void connect() throws Exception {
        
        CenturyLinkClient.logon(conn);
        CenturyLinkClient.authenticate(conn);
    }
    
    /* No blocking synchronous API calls */
    
    public String get(String APIName, Object body) throws Exception {
        
        if(body == null)
            return CenturyLinkClient.callAPIWithJSON(conn, APIName, "GET", null, null);
        
        else if(body instanceof Map<?, ?>)
            return CenturyLinkClient.callAPIWithMap(conn, APIName, "GET", (Map<String, String>)body, null);
        
        else if(body instanceof JSONObject)
            return CenturyLinkClient.callAPIWithJSON(conn, APIName, "GET", (JSONObject)body, null);
        
        else
            throw new Exception("Invalid body format.");
    }
    
    public String post(String APIName, Object body) throws Exception {

        if(body == null)
            return CenturyLinkClient.callAPIWithJSON(conn, APIName, "POST", null, null);
        
        else if(body instanceof Map<?, ?>)
            return CenturyLinkClient.callAPIWithMap(conn, APIName, "POST", (Map<String, String>)body, null);
        
        else if(body instanceof JSONObject)
            return CenturyLinkClient.callAPIWithJSON(conn, APIName, "POST", (JSONObject)body, null);
        
        else
            throw new Exception("Invalid body format.");
    }
    
    /* Asychronous API call with status polling and return only after the task is completed. 
     * If timeout is 0, don't poll. Otherwise, check the status from the CLCConnect object. */
    
    public String call(String APIName, String httpMethod, Object body, String locationAlias, Integer timeout, Integer interval) throws Exception {
        
        if(StringUtils.isBlank(APIName))
            throw new Exception("APIName cannot be blank.");
        
        if(StringUtils.isBlank(locationAlias))
            throw new Exception("locationAlias cannot be blank.");
        
        String result = null;
        
        if(timeout == null)
            timeout = conn.getTimeout();
//        Log.log.debug("timeout = " + timeout);
        
        if(interval == null)
            interval = conn.getInterval();
//        Log.log.debug("interval = " + interval);
        
        if(body == null)
            result = CenturyLinkClient.callAPIWithJSON(conn, APIName, httpMethod, null, locationAlias, timeout, interval);
        
        else if(body instanceof Map<?, ?>)
            result = CenturyLinkClient.callAPIWithMap(conn, APIName, httpMethod, (Map<String, String>)body, locationAlias, timeout, interval);
        
        else
            result = CenturyLinkClient.callAPIWithJSON(conn, APIName, httpMethod, (JSONObject)body, locationAlias, timeout, interval);
        
        if(result == null)
            throw new Exception("No response is available.");
        
        return result;
    }
    
} // end of class CLCInterface.