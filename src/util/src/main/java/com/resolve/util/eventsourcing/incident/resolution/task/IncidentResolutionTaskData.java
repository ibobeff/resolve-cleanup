package com.resolve.util.eventsourcing.incident.resolution.task;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.resolve.util.eventsourcing.incident.EventData;

@JsonSerialize(include = Inclusion.NON_NULL)
public class IncidentResolutionTaskData extends EventData
{
    private String taskId;
    private String rootResolutionId;

    public IncidentResolutionTaskData(String incidentId, String taskId, String rootResolutionId)
    {
        super(incidentId);
        this.taskId = taskId;
        this.rootResolutionId = rootResolutionId;
    }

    public String getTaskId()
    {
        return taskId;
    }

    public void setTaskId(String taskId)
    {
        this.taskId = taskId;
    }

    public String getRootResolutionId()
    {
        return rootResolutionId;
    }

    public void setRootResolutionId(String rootResolutionId)
    {
        this.rootResolutionId = rootResolutionId;
    }

}
