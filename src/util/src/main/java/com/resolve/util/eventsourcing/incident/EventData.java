package com.resolve.util.eventsourcing.incident;

import java.io.Serializable;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include = Inclusion.NON_NULL)
public class EventData implements Serializable
{
    private String incidentId;

    public EventData(String incidentId)
    {
        super();
        this.incidentId = incidentId;
    }

    public String getIncidentId()
    {
        return incidentId;
    }

    public void setIncidentId(String incidentId)
    {
        this.incidentId = incidentId;
    }
}
