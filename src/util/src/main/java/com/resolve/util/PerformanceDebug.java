/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

public class PerformanceDebug {

	private static boolean debugRSControl = false;
	
	public static boolean debugRSControl()
	{
		return debugRSControl;
	}
	
	public static void setDebugRSControl(boolean b)
	{
		debugRSControl = b;
	}
}
