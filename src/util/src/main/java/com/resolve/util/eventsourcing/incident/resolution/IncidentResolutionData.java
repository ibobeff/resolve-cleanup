package com.resolve.util.eventsourcing.incident.resolution;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.resolve.util.eventsourcing.incident.EventData;

@JsonSerialize(include = Inclusion.NON_NULL)
public class IncidentResolutionData extends EventData
{
    private String rootResolutionId;

    public IncidentResolutionData(String incidentId, String rootResolutionId)
    {
        super(incidentId);
        this.rootResolutionId = rootResolutionId;
    }

    public String getRootResolutionId()
    {
        return rootResolutionId;
    }

    public void setRootResolutionId(String rootResolutionId)
    {
        this.rootResolutionId = rootResolutionId;
    }

}
