/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * This is a library of convenient n could be consumed by any Resolve component.
 *
 */
public class MCPUtils
{
    public static Map<String, Integer> componentOrder = new HashMap<String, Integer>();
    static
    {
        componentOrder.put("rsmq", 0);
        componentOrder.put("cassandra", 1);
        componentOrder.put("rsmgmt", 2);
        componentOrder.put("rscontrol", 3);
        componentOrder.put("rsview", 4);
        componentOrder.put("rsremote", 5);
    }

    private static boolean isConfigured(Properties blueprint, String component)
    {
        boolean isConfigured = false;

        String configuredKey = "resolve." + component.toLowerCase();
        String configuredValue = blueprint.get(configuredKey);

        if (StringUtils.isNotEmpty(configuredValue))
        {
            if (configuredValue.contains("${"))
            {
                configuredValue = blueprint.getReplacement(configuredKey);
            }
            isConfigured = "true".equalsIgnoreCase(configuredValue);
        }
        Log.log.debug("Component " + component + " is Configured: " + isConfigured);

        return isConfigured;
    } //isConfigured

    /**
     * Method to append GUIDs for every component if they are missing.
     *
     * @param blueprint
     * @return
     */
    public static String initBlueprint(String blueprint)
    {
        StringBuilder result = new StringBuilder(blueprint);

        Properties blueprintProperties = new Properties();
        try
        {
            blueprintProperties.load(new ByteArrayInputStream(blueprint.getBytes()));

            if (blueprintProperties != null)
            {
                boolean blueprintRSControl = isConfigured(blueprintProperties, "rscontrol");
                boolean blueprintRSView = isConfigured(blueprintProperties, "rsview");
                boolean blueprintRSConsole = isConfigured(blueprintProperties, "rsconsole");
                boolean blueprintRSMgmt = isConfigured(blueprintProperties, "rsmgmt");
                boolean blueprintRSRemote = isConfigured(blueprintProperties, "rsremote");
                //boolean blueprintRSMQ = isConfigured(blueprintProperties, "rsmq");
                //boolean blueprintCassandra = isConfigured(blueprintProperties, "cassandra");

                boolean isCommentSet = false;

                StringBuilder guidConfig = new StringBuilder();

                if (blueprintRSControl)
                {
                    if (!blueprintProperties.containsKey("rscontrol.id.guid") || StringUtils.isBlank(blueprintProperties.get("rscontrol.id.guid")))
                    {
                        if (!isCommentSet)
                        {

                        }
                        guidConfig.append(getPropertyWithNewGUID("rscontrol.id.guid"));
                    }
                }
                if (blueprintRSView)
                {
                    if (!blueprintProperties.containsKey("rsview.id.guid") || StringUtils.isBlank(blueprintProperties.get("rsview.id.guid")))
                    {
                        guidConfig.append(getPropertyWithNewGUID("rsview.id.guid"));
                    }
                }
                if (blueprintRSConsole)
                {
                    if (!blueprintProperties.containsKey("rsconsole.id.guid") || StringUtils.isBlank(blueprintProperties.get("rsconsole.id.guid")))
                    {
                        guidConfig.append(getPropertyWithNewGUID("rsconsole.id.guid"));
                    }
                }
                if (blueprintRSMgmt)
                {
                    if (!blueprintProperties.containsKey("rsmgmt.id.guid") || StringUtils.isBlank(blueprintProperties.get("rsmgmt.id.guid")))
                    {
                        guidConfig.append(getPropertyWithNewGUID("rsmgmt.id.guid"));
                    }
                }
                if (blueprintRSRemote)
                {
                    String rsremotes = blueprintProperties.get("rsremote.instance.count");
                    Log.log.info("rsremote instance count: " + rsremotes);
                    if (rsremotes != null && rsremotes.matches("\\d+"))
                    {
                        int count = Integer.parseInt(rsremotes);
                        for (int i = 1; i <= count; i++)
                        {
                            String instance = blueprintProperties.get("rsremote.instance" + i + ".name").toLowerCase();
                            Log.log.warn("rsremote.instance" + i + ".name=" + instance);
                            if (instance != null)
                            {
                                if (!blueprintProperties.containsKey(instance + ".id.guid") || StringUtils.isBlank(blueprintProperties.get(instance + ".id.guid")))
                                {
                                    guidConfig.append(getPropertyWithNewGUID(instance + ".id.guid"));
                                }
                                Log.log.info("Adding guid for rsremote instance: " + instance);
                            }
                        }
                    }
                }
                if (guidConfig.length() > 0)
                {
                    result.append("##########################################################" + MCPConstants.LINE_SEPARATOR);
                    result.append("####### Auto-generated GUIDs by MCP, DO NOT MODIFY #######" + MCPConstants.LINE_SEPARATOR);
                    result.append("##########################################################" + MCPConstants.LINE_SEPARATOR);
                    result.append(guidConfig);
                }
            }
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result.toString();
    }

    /**
     * Returns a string like rscontrol.id.guid=8a9482f13f8ba865013f8bcfbccc0009
     *
     * @param property example rscontrol.id.guid
     * @return
     */
    private static String getPropertyWithNewGUID(String property)
    {
        StringBuilder result = new StringBuilder();
        result.append(property);
        result.append("=");
        result.append(new Guid().toString());
        result.append(MCPConstants.LINE_SEPARATOR);
        return result.toString();
    }

    /**
     * This method compares two blueprints for keys and their values.
     *
     * @param mcpBlueprintContent
     * @param remoteBlueprintContent
     * @return {@link Map} with the keys those are different.
     */
    public static Map<String, Map<String, String>> compareBlueprint(String mcpBlueprintContent, String remoteBlueprintContent)
    {
        Map<String, Map<String, String>> result = new HashMap<String, Map<String, String>>();

        Properties mcpBlueprint = new Properties();
        try
        {
            mcpBlueprint.load(new ByteArrayInputStream(mcpBlueprintContent.getBytes()));

            Properties remoteBlueprint = new Properties();
            remoteBlueprint.load(new ByteArrayInputStream(remoteBlueprintContent.getBytes()));

            compare(mcpBlueprint, remoteBlueprint, "MCP", "ACTUAL", result);
            compare(remoteBlueprint, mcpBlueprint, "ACTUAL", "MCP", result);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }

    private static void compare(Properties prop1, Properties prop2, String prop1Type, String prop2Type, Map<String, Map<String, String>> blueprint)
    {
        for (Object key : prop1.keySet())
        {
            boolean isEqual = true;

            String tmpKey = (String) key;
            String prop1Value = prop1.get(tmpKey);
            String prop2Value = "";
            if (prop2.containsKey(tmpKey))
            {
                prop2Value = prop2.get(tmpKey);
                if (StringUtils.isNotBlank(prop1Value))
                {
                    String tmpProp1Value = prop1Value.replaceAll("\\s", "");
                    //prop2Value
                    if (StringUtils.isNotBlank(prop2Value))
                    {
                        String tmpProp2Value = prop2Value.replaceAll("\\s", "");
                        isEqual = tmpProp1Value.equals(tmpProp2Value);
                    }
                }
            }

            if (!isEqual)
            {
                Map<String, String> map = new HashMap<String, String>();
                map.put(prop1Type, prop1Value);
                map.put(prop2Type, prop2Value);
                if (!blueprint.containsKey(key))
                {
                    blueprint.put(tmpKey, map);
                }
            }
        }
    }

    public static boolean isSolaris(String unameResponse)
    {
        boolean result = false;
        if (StringUtils.isNotBlank(unameResponse) && unameResponse.toLowerCase().contains("sunos"))
        {
            Log.log.debug("uname response:" + unameResponse);
            result = true;
        }
        return result;
    }

    public static boolean isLinux(String unameResponse)
    {
        boolean result = false;
        if (StringUtils.isNotBlank(unameResponse) && unameResponse.toLowerCase().contains("linux"))
        {
            Log.log.debug("uname response:" + unameResponse);
            result = true;
        }
        return result;
    }

    /**
     * This provides the order of start/stop.
     *
     *      For example: stop.sh rscontrol rsview rsmgmt cassandra rsmq
     *      or      run.sh rsmq cassandra rscontrol rsview rsmgmt
     * @param components a space separated list of components.
     * @param isStop
     * @return
     */
    public static String arrangeComponents(String components, boolean isStop)
    {
        StringBuilder result = new StringBuilder();

        String[] componentList = new String[10];
        //for rsremote instances
        StringBuilder otherComponent = new StringBuilder();

        if(StringUtils.isNotBlank(components))
        {
            String[] stringArray = components.toLowerCase().split(" ");
            for(String component : stringArray)
            {
                if(componentOrder.containsKey(component))
                {
                    componentList[componentOrder.get(component)] = component;
                }
                else
                {
                    otherComponent.append(component + " ");
                }
            }
        }
        if(isStop)
        {
            result.append(otherComponent);
            for(int i = componentList.length-1; i >= 0; i--)
            {
                if(StringUtils.isNotBlank(componentList[i]))
                {
                    result.append(componentList[i] + " ");
                }
            }
        }
        else //this is start order
        {
            for(int i = 0; i < componentList.length; i++)
            {
                if(StringUtils.isNotBlank(componentList[i]))
                {
                    result.append(componentList[i] + " ");
                }
            }
            result.append(otherComponent);
        }
        return result.toString();
    }
}