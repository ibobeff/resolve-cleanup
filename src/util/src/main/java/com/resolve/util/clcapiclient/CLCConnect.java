package com.resolve.util.clcapiclient;

import java.util.Map;

import javax.net.ssl.SSLSession;

import org.codehaus.jettison.json.JSONObject;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;


public class CLCConnect
{
    
    private String APIKey = null;
    private String passwordV1 = null;
    private String username = null;
    private String passwordV2 = null;
    private String accountAlias = null;
    
    private String hostUrl = "https://api.ctl.io";
    private String logonUri = "/REST/Auth/Logon/JSON";
    private String logoutUri = "/REST/Auth/Logout/JSON";
    
    private String cookieDomain = "api.ctl.io";
    private String cookieName = "Tier3.API.Cookie";

    private String authUri = "/v2/authentication/login";
    private String queryUri = "/REST/Blueprint/GetDeploymentStatus/JSON";  // "/REST/Queue/GetRequestStatus/JSON";
    
    private int timeout = 100;
    private int interval = 10;
    
    private String cookie = null;
    private String token = null;
    
    private String reason = "";
    private String status = "";
    private int statusCode = -1;
    
    public CLCConnect(String APIKey, String passwordV1, String username, String passwordV2, String accountAlias) throws Exception {

        Log.log.debug("APIKey = " + APIKey);
        Log.log.debug("passwordV1 = " + passwordV1);
        Log.log.debug("username = " + username);
        Log.log.debug("passwordV2 = " + passwordV2);
        Log.log.debug("accountAlias = " + accountAlias);

        setAPIKey(APIKey);
        setPasswordV1(passwordV1);
        setUsername(username);
        setPasswordV2(passwordV2);
        setAccountAlias(accountAlias);
    }
    
    public String getAPIKey()
    {
        return APIKey;
    }
    public void setAPIKey(String APIKey) throws Exception
    {
        if(StringUtils.isBlank(APIKey))
            throw new Exception("APIKey is required.");
        this.APIKey = APIKey.trim();
    }
    public String getPasswordV1()
    {
        return passwordV1;
    }
    public void setPasswordV1(String passwordV1) throws Exception
    {
        if(StringUtils.isBlank(passwordV1)) 
            throw new Exception("passwordV1 is required.");
        this.passwordV1 = passwordV1.trim();
    }
    public String getUsername()
    {
        return username;
    }
    public void setUsername(String username) throws Exception
    {
        if(StringUtils.isBlank(username)) 
            throw new Exception("username is required.");
        this.username = username.trim();
    }
    public String getPasswordV2()
    {
        return passwordV2;
    }
    public void setPasswordV2(String passwordV2) throws Exception
    {
        if(StringUtils.isBlank(passwordV2)) 
            throw new Exception("passwordV2 is required.");
        this.passwordV2 = passwordV2.trim();
    }
    public String getAccountAlias()
    {
        return accountAlias;
    }
    public void setAccountAlias(String accountAlias) throws Exception
    {
        if(StringUtils.isBlank(accountAlias)) 
            throw new Exception("accountAlias is required.");

            this.accountAlias = accountAlias.trim();
    }
    
    public String getHostUrl()
    {
        return hostUrl;
    }
    public void setHostUrl(String hostUrl) throws Exception
    {
        if(StringUtils.isNotBlank(hostUrl)) {
            String hostURL = hostUrl.trim();
            if(hostURL.endsWith("/"))
                this.hostUrl = hostURL.substring(0, hostURL.length()-1);
            else
                this.hostUrl = hostURL;
        }
        else
            throw new Exception("hostUrl cannot be blank.");
    }
    public String getLogonUri()
    {
        return logonUri;
    }
    public void setLogonUri(String logonUri) throws Exception
    {
        if(StringUtils.isNotBlank(logonUri))
            this.logonUri = logonUri.trim();
        else
            throw new Exception("logonUri cannot be blank.");
    }
    public String getLogoutUri()
    {
        return logoutUri;
    }
    public void setLogoutUri(String logoutUri) throws Exception
    {
        if(StringUtils.isNotBlank(logoutUri))
            this.logoutUri = logoutUri.trim();
        else
            throw new Exception("logoutUri cannot be blank.");
    }
    public String getCookieDomain()
    {
        return cookieDomain;
    }
    public void setCookieDomain(String cookieDomain) throws Exception
    {
        if(StringUtils.isNotBlank(cookieDomain))
            this.cookieDomain = cookieDomain.trim();
        else
            throw new Exception("cookieDomain cannot be blank.");
    }
    public String getCookieName()
    {
        return cookieName;
    }
    public void setCookieName(String cookieName) throws Exception
    {
        if(StringUtils.isNotBlank(cookieName))
            this.cookieName = cookieName.trim();
        else
            throw new Exception("cookieName cannot be blank.");
    }
    
    public String getAuthUri()
    {
        return authUri;
    }
    public void setAuthUri(String authUri) throws Exception
    {
        if(StringUtils.isNotBlank(authUri))
            this.authUri = authUri.trim();
        else
            throw new Exception("authUri cannot be blank.");
    }
    public String getQueryUri()
    {
        return queryUri;
    }
    public void setQueryUri(String queryUri) throws Exception
    {
        if(StringUtils.isNotBlank(queryUri))
            this.queryUri = queryUri;
        else
            throw new Exception("queryUri cannot be blank.");
    }
    public Integer getTimeout()
    {
        return timeout;
    }
    public void setTimeout(Integer timeout)
    {
        if(timeout != null)
            this.timeout = timeout.intValue();
    }
    public Integer getInterval()
    {
        return interval;
    }
    public void setInterval(Integer interval) throws Exception
    {
        if(interval != null) {
            if(interval.intValue() == 0)
                throw new Exception("Interval cannot be 0.");
            else
                this.interval = interval.intValue();
        }
    }

    public String getCookie()
    {
        return cookie;
    }
    public void setCookie(String cookie)
    {
        this.cookie = cookie;
    }
    public String getToken()
    {
        return token;
    }
    public void setToken(String token)
    {
        this.token = token;
    }
    public String getReason()
    {
        return reason;
    }
    public void setReason(String reason)
    {
        this.reason = reason;
    }
    public String getStatus()
    {
        return status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }
    public int getStatusCode()
    {
        return statusCode;
    }
    public void setStatusCode(int statusCode)
    {
        this.statusCode = statusCode;
    }
    
} // end of class CLCConnect.