/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author alokika.dash
 *
 */
public class MetricThreshold
{
    //default map that stores the metric thresholds
    public Map<String, MetricThresholdType> thresholdProperties;
    public static final String ALERT_FATAL = "fatal";
    public static final String ALERT_WARN = "warning";
    public static final String ALERT_INFO = "info";
    
    /*
    public MetricThreshold()
    {
        thresholdProperties = new HashMap<String, MetricThresholdType>();
        QueryDTO query = new QueryDTO();
        query.setTableName("metric_threshold");
        query.setWhereClause(" u_guid='' or u_guid is null");
        query.setUseSql(true);
        List<MetricThresholdVO> data = ServiceJDBC.getMetricThreshold(query);
        for(MetricThresholdVO metric : data)
        {
            ServiceHibernate.processValuesInMap(metric, thresholdProperties);
        }
       
    } // MetricThreshold
    */
    
    public MetricThreshold(MetricThresholdInit metricThresholdInit)
    {
        thresholdProperties = metricThresholdInit.init();
    } // MetricThreshold
    
    public void checkThresholds(String metricgroup, Integer metricvalue, String metricname)
    {
//        String fullmetricname = metricgroup + "." + metricname;
//        String alertStatus = thresholdProperties.get(fullmetricname).getAlertstatus();
//        
//        // high threshold value check
//        if(!thresholdProperties.get(fullmetricname).getHigh().equalsIgnoreCase("undef") && !thresholdProperties.get(fullmetricname).getHigh().isEmpty())
//        {
//            if (metricvalue > Integer.parseInt(thresholdProperties.get(fullmetricname).getHigh()))
//            {
//                if(alertStatus.equalsIgnoreCase(ALERT_FATAL))
//                    Log.alert(ERR.E80001, "Metric " + fullmetricname + " with value: " + metricvalue + " greater than threshold value " + thresholdProperties.get(fullmetricname).getHigh());
//                if(alertStatus.equalsIgnoreCase(ALERT_INFO))
//                    Log.log.info("Metric " + fullmetricname + " with value: " + metricvalue + " greater than threshold value " + thresholdProperties.get(fullmetricname).getHigh());
//                if(alertStatus.equalsIgnoreCase(ALERT_WARN))
//                    Log.log.warn("Metric " + fullmetricname + " with value: " + metricvalue + " greater than threshold value " + thresholdProperties.get(fullmetricname).getHigh());
//            }
//        }
//        // low threshold value check
//        if(!thresholdProperties.get(fullmetricname).getLow().equalsIgnoreCase("undef") && !thresholdProperties.get(fullmetricname).getLow().isEmpty())
//        {
//            if (metricvalue < Integer.parseInt(thresholdProperties.get(fullmetricname).getLow()))
//            {
//                if(alertStatus.equalsIgnoreCase(ALERT_FATAL))
//                    Log.alert(ERR.E80001, "Metric " + fullmetricname + " with value: " + metricvalue + " less than threshold value " + thresholdProperties.get(fullmetricname).getLow());
//                if(alertStatus.equalsIgnoreCase(ALERT_INFO))
//                    Log.log.info("Metric " + fullmetricname + " with value: " + metricvalue + " less than threshold value " + thresholdProperties.get(fullmetricname).getLow());
//                if(alertStatus.equalsIgnoreCase(ALERT_WARN))
//                    Log.log.warn("Metric " + fullmetricname + " with value: " + metricvalue + " less than threshold value " + thresholdProperties.get(fullmetricname).getLow());
//            }
//        }
    } // checkThresholds
    
    /**
     * This is the default properties assigned from the threshold.properties
     */
    public void setThresholdProperties()
    {
        Log.log.debug("Default Metrics");
        thresholdProperties = new HashMap<String, MetricThresholdType>();
        /* Default JVM Metrics */
        MetricThresholdType mtype = new MetricThresholdType("undef", "10", "fatal");
        thresholdProperties.put("jvm.mem_free", mtype);
        mtype = new MetricThresholdType("95", "undef", "warning");
        thresholdProperties.put("jvm.thread_count", mtype);
        
        
        /* Default JMS Messages*/
        mtype = new MetricThresholdType("undef", "undef", "warning");
        thresholdProperties.put("jms_queue.msgs_count", mtype);
        
        
        /* Default Database Metrics */
        mtype = new MetricThresholdType("undef", "50", "fatal");
        thresholdProperties.put("database.free_space", mtype);
        mtype = new MetricThresholdType("undef", "undef", "info");
        thresholdProperties.put("database.size", mtype);
        mtype = new MetricThresholdType("90","undef", "fatal");
        thresholdProperties.put("database.percentage_used", mtype);
        mtype = new MetricThresholdType("5000","undef", "fatal");
        thresholdProperties.put("database.response_time", mtype);
        mtype = new MetricThresholdType("undef","undef", "warning");
        thresholdProperties.put("database.query_count", mtype);
        mtype = new MetricThresholdType("90","undef", "warning");
        thresholdProperties.put("database.percentage_wait", mtype);
        
        /* Default Latency Metrics */
        mtype = new MetricThresholdType("undef","undef", "warning");
        thresholdProperties.put("latency.wiki", mtype);
        mtype = new MetricThresholdType("5000","undef", "warning");
        thresholdProperties.put("latency.wikiresponsetime", mtype);
        mtype = new MetricThresholdType("undef","undef", "info");
        thresholdProperties.put("latency.social", mtype);
        mtype = new MetricThresholdType("5000","undef", "warning");
        thresholdProperties.put("latency.socialresponsetime", mtype);
        
        /* Default Runbook Metrics */
        mtype = new MetricThresholdType("undef","undef", "info");
        thresholdProperties.put("runbook.aborted", mtype);
        mtype = new MetricThresholdType("undef","undef", "info");
        thresholdProperties.put("runbook.completed", mtype);
        mtype = new MetricThresholdType("undef","undef", "info");
        thresholdProperties.put("runbook.duration", mtype);
        mtype = new MetricThresholdType("undef","undef", "info");
        thresholdProperties.put("runbook.gseverity", mtype);
        mtype = new MetricThresholdType("undef","undef", "info");
        thresholdProperties.put("runbook.cseverity", mtype);
        mtype = new MetricThresholdType("undef","undef", "info");
        thresholdProperties.put("runbook.wseverity", mtype);
        mtype = new MetricThresholdType("undef","undef", "info");
        thresholdProperties.put("runbook.sseverity", mtype);
        mtype = new MetricThresholdType("undef","undef", "info");
        thresholdProperties.put("runbook.useverity", mtype);
        mtype = new MetricThresholdType("undef","undef", "info");
        thresholdProperties.put("runbook.gcondition", mtype);
        mtype = new MetricThresholdType("undef","undef", "info");
        thresholdProperties.put("runbook.bcondition", mtype);
        mtype = new MetricThresholdType("undef","undef", "info");
        thresholdProperties.put("runbook.ucondition", mtype);
        
        /* Default CPU Metrics */
        mtype = new MetricThresholdType("8", "undef", "fatal");
        thresholdProperties.put("server.load1", mtype);
        mtype = new MetricThresholdType("8", "undef", "fatal");
        thresholdProperties.put("server.load5", mtype);
        mtype = new MetricThresholdType("8", "undef", "fatal");
        thresholdProperties.put("server.load15", mtype);
        mtype = new MetricThresholdType("90", "undef", "fatal");
        thresholdProperties.put("server.disk_space", mtype);
        
        /* Default Transaction Metrics */
        mtype = new MetricThresholdType("undef","undef", "warning");
        thresholdProperties.put("transaction.transaction", mtype);
        mtype = new MetricThresholdType("30","undef", "warning");
        thresholdProperties.put("transaction.latency", mtype);
        
        /* Default User Metrics */
        mtype = new MetricThresholdType("500","undef", "warning");
        thresholdProperties.put("users.active", mtype);
        
        /* Default CR Metrics */
        mtype = new MetricThresholdType("undef","undef", "info");
        thresholdProperties.put("cr.status", mtype);
                                  
    }//setThresholdProperties

    public Map<String, MetricThresholdType> getThresholdProperties()
    {
        return thresholdProperties;
    } // getThresholdProperties

    public void setThresholdProperties(Map<String, MetricThresholdType> thresholdProperties)
    {
        Log.log.debug("Set Metrics");
        this.thresholdProperties = thresholdProperties;
    } //setThresholdProperties
    
} // MetricThreshold