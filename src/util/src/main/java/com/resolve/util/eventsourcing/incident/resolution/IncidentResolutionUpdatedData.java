package com.resolve.util.eventsourcing.incident.resolution;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include = Inclusion.NON_NULL)
public class IncidentResolutionUpdatedData extends IncidentResolutionData
{
    private String summary;
    private String condition;
    private String severity;

    public IncidentResolutionUpdatedData(String incidentId, String rootResolutionId, String summary, String condition, String severity)
    {
        super(incidentId, rootResolutionId);
        this.summary = summary;
        this.condition = condition;
        this.severity = severity;
    }

    public String getSummary()
    {
        return summary;
    }

    public void setSummary(String summary)
    {
        this.summary = summary;
    }

    public String getCondition()
    {
        return condition;
    }

    public void setCondition(String condition)
    {
        this.condition = condition;
    }

    public String getSeverity()
    {
        return severity;
    }

    public void setSeverity(String severity)
    {
        this.severity = severity;
    }

}
