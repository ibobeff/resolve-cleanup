/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DirectedTreeNode
{
    String id;
    String type;
    Object value;
    boolean isParentAndChildToSelf = false;

    DirectedTree tree;
    Map<String, DirectedTreeNode> parents;
    Map<String, DirectedTreeNode> children;
    
    /*
     *  Map of paths to this (action task type, sub-runbook, ) node from root pre-condition node
     *  There could be multiple paths to this node from root pre-condition node
     *  keyed on (root) pre-condition node id.
     *  Root pre-condition node is a node with no pre-conidition parents.
     *  Value is map of pre-condition node id keyed by order (1, 2, .....)
     */
    Map<String, Map<Integer, Map<Integer, String>>> pathsFromPreCondition;
    Map<String, Map<Integer, Integer>> maxOrdersMap;
    
    public DirectedTreeNode(DirectedTree tree, String id, String type)
    {
        this.id = id;
        this.type = type;
        this.tree = tree;
        this.parents = new /*Hashtable*/HashMap<String, DirectedTreeNode>();
        this.children = new /*Hashtable*/HashMap<String, DirectedTreeNode>();
        
        this.tree.addNode(this);
    } // DirectedTreeNode

    public List<DirectedTreeNode> getParents()
    {
        return new ArrayList<DirectedTreeNode>(parents.values());
    } // getParents

    public List<DirectedTreeNode> getChildren()
    {
        return new ArrayList<DirectedTreeNode>(children.values());
    } // getChildren

    public void removeNode()
    {
        // remove from each parent
        for (Iterator<DirectedTreeNode> i = getParents().iterator(); i.hasNext();)
        {
            DirectedTreeNode parent = i.next();

            removeParent(parent);
        }

        // remove from each child
        for (Iterator<DirectedTreeNode> i = getChildren().iterator(); i.hasNext();)
        {
            DirectedTreeNode child = i.next();

            removeChild(child);
        }

        // remove node from tree
        this.tree.removeNode(this);
    } // removeNode

    // addParent
    public boolean addParent(DirectedTreeNode parent)
    {
        boolean success = false;
        
        if (id.equals(parent.getId()))
        {
            isParentAndChildToSelf = true;
            return success;
        }
                        
        if (!parents.containsKey(parent.getId()))
        {
            // add this node to parent
            parent.attachChild(this);
    
            // add parent to this node
            parents.put(parent.getId(), parent);
            success = true;
        }
        
        return success;
    } // addParent

    // addChild
    public boolean addChild(DirectedTreeNode child)
    {
        boolean success = false;
        
        if (id.equals(child.getId()))
        {
            isParentAndChildToSelf = true;
            return success;
        }
        
        if (!children.containsKey(child.getId()))
        {
            // add this node to child
            child.attachParent(this);
    
            // add child to this node
            children.put(child.getId(), child);
            success = true;
        }
        
        return success;
    } // addChild

    // attachParent
    void attachParent(DirectedTreeNode parent)
    {
        parents.put(parent.getId(), parent);
    } // attachParent

    // attachChild
    void attachChild(DirectedTreeNode child)
    {
        children.put(child.getId(), child);
    } // attachChild

    // removeParent
    public void removeParent(DirectedTreeNode parent)
    {
        // remove this node from parent
        parent.detachChild(this);

        // remove parent from this node
        parents.remove(parent.getId());
    } // removeParent

    // removeChild
    public void removeChild(DirectedTreeNode child)
    {
        // remove this node from child
        child.detachParent(this);

        // remove child from this node
        children.remove(child.getId());
    } // removeChild

    // detachParent
    void detachParent(DirectedTreeNode parent)
    {
        parents.remove(parent.getId());
    } // detachChild

    // detachChild
    void detachChild(DirectedTreeNode child)
    {
        children.remove(child.getId());
    } // detachChild

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Object getValue()
    {
        return value;
    }

    public void setValue(Object value)
    {
        this.value = value;
    }
    
    public Map<String, Map<Integer, Map<Integer, String>>> getPathsFromPreCondition()
    {
        return pathsFromPreCondition;
    }

    public void setPathsFromPreCondition(Map<String, Map<Integer, Map<Integer, String>>> pathsFromPreCondition)
    {
        this.pathsFromPreCondition = pathsFromPreCondition;
    }
    
    public Map<String, Map<Integer, Integer>> getMaxOrdersMap()
    {
        return maxOrdersMap;
    }
    
    public void setMaxOrdersMap(Map<String, Map<Integer, Integer>> maxOrdersMap)
    {
        this.maxOrdersMap = maxOrdersMap;
    }
    
    public int getMaxOrderFor(String fromNodeId, Integer order)
    {
        int maxOrder = -1;
        
        if (maxOrdersMap != null && maxOrdersMap.containsKey(fromNodeId) && maxOrdersMap.get(fromNodeId).containsKey(order))
        {
            maxOrder = maxOrdersMap.get(fromNodeId).get(order).intValue();
        }
        
        return maxOrder;
    }
    
    public int setMaxOrderFor(String fromNodeId, Integer pathNo, Integer maxOrder)
    {
        int oldMaxOrder = -1;
        
        if (maxOrdersMap == null)
        {
            maxOrdersMap = new HashMap<String, Map<Integer, Integer>>();
        }
        
        if (!maxOrdersMap.containsKey(fromNodeId))
        {
            maxOrdersMap.put(fromNodeId, new HashMap<Integer, Integer>());
        }
        
        if (maxOrdersMap.containsKey(fromNodeId))
        {
            Integer oldMaxOrderInt = maxOrdersMap.get(fromNodeId).put(pathNo, maxOrder);
        
            if (oldMaxOrderInt != null)
            {
                oldMaxOrder = oldMaxOrderInt.intValue();
            }
        }
        
        return oldMaxOrder;
    }
    
    public int getCountOfPathsToThisNodeFrom(String fromNodeId)
    {
        int cntOfPathsToThisNodeFrom = 0;
        
        if (maxOrdersMap != null && maxOrdersMap.containsKey(fromNodeId))
        {
            cntOfPathsToThisNodeFrom = maxOrdersMap.get(fromNodeId).size();
        }
        
        return cntOfPathsToThisNodeFrom;
    }
    
    public Map<Integer, String> getPathToThisNodeFrom(String fromNodeId, Integer pathNo)
    {
        Map<Integer, String> path = null;
        
        if (pathNo == null)
        {
            return path;
        }
        
        if (pathsFromPreCondition == null || !pathsFromPreCondition.containsKey(fromNodeId))
        {
            return path;
        }
        
        return pathsFromPreCondition.get(fromNodeId).get(pathNo);
    }
    
    private Map<Integer, String> makeDeepCopyOfPathUptoMaxOrder(Map<Integer, String> path, Integer maxOrder)
    {
        Map<Integer, String> copyByValue = new HashMap<Integer, String>();
        
        if (path != null && !path.isEmpty())
        {
            for (int i = 1; i <= maxOrder.intValue(); i++)
            {
                Integer key = new Integer(i);
                copyByValue.put(key, path.get(key));
            }
        }
        
        return copyByValue;
    }
    
    public int setPathToThisNodeFrom(String fromNodeId, Map<Integer, String> path, Integer maxOrder)
    {
        int newPathNo = -1;
        
        if (pathsFromPreCondition == null)
        {
            pathsFromPreCondition = new HashMap<String, Map<Integer, Map<Integer, String>>>();
        }
        
        if (!pathsFromPreCondition.containsKey(fromNodeId))
        {
            pathsFromPreCondition.put(fromNodeId, new HashMap<Integer, Map<Integer, String>>());
        }
        
        if (pathsFromPreCondition.containsKey(fromNodeId))
        {
            newPathNo = getCountOfPathsToThisNodeFrom(fromNodeId) + 1;
            Integer newPathNoInteger = new Integer(newPathNo);
            
            pathsFromPreCondition.get(fromNodeId).put(newPathNoInteger, makeDeepCopyOfPathUptoMaxOrder(path, maxOrder));
            setMaxOrderFor(fromNodeId, newPathNo, maxOrder);
        }
        
        return newPathNo;
    }
    
    public DirectedTreeNode getNode(String id)
    {
        if (this.id.equals(id))
            return this;
        else
            return tree.getNode(id);
    }
    
    public List<DirectedTreeNode> getAllParents()
    {
        if (isParentAndChildToSelf)
        {
            List<DirectedTreeNode> tParents = getParents();
            tParents.add(this);
            return tParents;
        }
        else
        {
            return getParents();
        }
    }
    
    public List<DirectedTreeNode> getAllChildren()
    {
        if (isParentAndChildToSelf)
        {
            List<DirectedTreeNode> tChildrens = getChildren();
            tChildrens.add(this);
            return tChildrens;
        }
        else
        {
            return getChildren();
        }
    }
    
    public Map<String, EdgeNode> getEdgeNodes()
    {
        return tree.getEdgeNodes();
    }
    
    public List<DirectedTreeNode> getParentPreConditionNodesExcludingSelfAndSource(DirectedTreeNode srcNode)
    {
        List<DirectedTreeNode> parentPreConditionNodes = new ArrayList<DirectedTreeNode>();
        
        for(DirectedTreeNode parent : parents.values())
        {
            if (parent.getType().equals(Constants.MODEL_NODE_TYPE_PRECONDITION) && 
                !parent.getId().equals(getId()) && !parent.getId().equals(srcNode.getId()))
            {
                parentPreConditionNodes.add(parent);
            }
        }
        
        return parentPreConditionNodes;
    }
} // DirectedTreeNode
