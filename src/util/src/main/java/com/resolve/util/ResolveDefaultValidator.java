package com.resolve.util;

import java.io.File;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.Logger;
import org.owasp.esapi.ValidationErrorList;
import org.owasp.esapi.ValidationRule;
import org.owasp.esapi.Validator;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;
import org.owasp.esapi.reference.DefaultValidator;

public class ResolveDefaultValidator implements org.owasp.esapi.Validator {
	public static final String HQL_SQL_SAFE_STRING_VALIDATOR = "HQLSQLSafeString";
	public static final String HQL_SQL_SAFE_TABLE_COL_NAME_STRING_VALIDATOR = "HQLSQLSafeMetaDataString";
	public static final int MAX_HQL_SQL_SAFE_TABLE_COL_NAME_STRING_LENGTH = 64;
	public static final int MAX_HQL_SQL_SAFE_STRING_LENGTH = 32768;
	public static final String HQL_SQL_ENTITY_VALIDATOR = "HQLSQLEntity";
	public static final int MAX_HQL_SQL_ENTITY_LENGTH = 512;
	
    private static final int MAX_URL_LENGTH = 8192;
    private static final String TYPE_REDIRECT = "Redirect";
    
    private static final String GET_VALID_INPUT_1_LOG = 
    												"getValidInput(%s, %s, %s, " + MAX_URL_LENGTH + ", %b) returned %s";
    private static final String GET_VALID_INPUT_2_LOG = 
    												"getValidInput(%s, %s, %s, " + MAX_URL_LENGTH + 
    												", %b, ValidationErrorList) returned %s";
    private static final String GET_VALID_INPUT_3_LOG = 
													"getValidInput(%s, %s, %s, " + MAX_URL_LENGTH + 
													", %b, %b, ValidationErrorList) returned %s";
    private static final String GET_VALID_REDIRECT_LOCATION_1_LOG = "getValidRedirectLocation(%s, %s, %b) returned %s";
    private static final String GET_VALID_REDIRECT_LOCATION_2_LOG = 
    												"getValidRedirectLocation(%s, %s, %b, ValidationErrorList) returned %s";
    private static final String IS_VALID_INPUT_1_LOG = "isValidInput(%s, %s, %s, " + MAX_URL_LENGTH + ", %b) returned %b";
    private static final String IS_VALID_INPUT_2_LOG = "isValidInput(%s, %s, %s, " + MAX_URL_LENGTH + 
    												   ", %b, ValidationErrorList) returned %b";
    
    private static volatile Validator instance = null;
    private Validator defaultValidator;

    public static Validator getInstance() {
        if (instance == null) {
            synchronized (ResolveDefaultValidator.class) {
                if (instance == null) {
                    instance = new ResolveDefaultValidator();
                }
            }
        }
        return instance;
    }

    /** The logger. */
	private final Logger logger = ESAPI.getLogger("DefaultValidator");
	
    private ResolveDefaultValidator() {
        defaultValidator = DefaultValidator.getInstance();  
    }
    
    @Override
    public void addRule(ValidationRule arg0) {
        defaultValidator.addRule(arg0);
    }

    @Override
    public void assertValidFileUpload(String arg0, String arg1, String arg2, File arg3, byte[] arg4, int arg5,
            List<String> arg6, boolean arg7) throws ValidationException, IntrusionException {
        defaultValidator.assertValidFileUpload(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    }

    @Override
    public void assertValidFileUpload(String arg0, String arg1, String arg2, File arg3, byte[] arg4, int arg5,
            List<String> arg6, boolean arg7, ValidationErrorList arg8) throws IntrusionException {
        defaultValidator.assertValidFileUpload(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
    }

    @Override
    public void assertValidHTTPRequestParameterSet(String arg0, HttpServletRequest arg1, Set<String> arg2,
            Set<String> arg3) throws ValidationException, IntrusionException {
        defaultValidator.assertValidHTTPRequestParameterSet(arg0, arg1, arg2, arg3);
    }

    @Override
    public void assertValidHTTPRequestParameterSet(String arg0, HttpServletRequest arg1, Set<String> arg2,
            Set<String> arg3, ValidationErrorList arg4) throws IntrusionException {
        defaultValidator.assertValidHTTPRequestParameterSet(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public ValidationRule getRule(String arg0) {
        return defaultValidator.getRule(arg0);
    }

    @Override
    public String getValidCreditCard(String arg0, String arg1, boolean arg2)
            throws ValidationException, IntrusionException {
        return defaultValidator.getValidCreditCard(arg0, arg1, arg2);
    }

    @Override
    public String getValidCreditCard(String arg0, String arg1, boolean arg2, ValidationErrorList arg3)
            throws IntrusionException {
        return defaultValidator.getValidCreditCard(arg0, arg1, arg2, arg3);
    }

    @Override
    public Date getValidDate(String arg0, String arg1, DateFormat arg2, boolean arg3)
            throws ValidationException, IntrusionException {
        return defaultValidator.getValidDate(arg0, arg1, arg2, arg3);
    }

    @Override
    public Date getValidDate(String arg0, String arg1, DateFormat arg2, boolean arg3, ValidationErrorList arg4)
            throws IntrusionException {
        return defaultValidator.getValidDate(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public String getValidDirectoryPath(String arg0, String arg1, File arg2, boolean arg3)
            throws ValidationException, IntrusionException {
        return defaultValidator.getValidDirectoryPath(arg0, arg1, arg2, arg3);
    }

    @Override
    public String getValidDirectoryPath(String arg0, String arg1, File arg2, boolean arg3, ValidationErrorList arg4)
            throws IntrusionException {
        return defaultValidator.getValidDirectoryPath(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public Double getValidDouble(String arg0, String arg1, double arg2, double arg3, boolean arg4)
            throws ValidationException, IntrusionException {
        return defaultValidator.getValidDouble(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public Double getValidDouble(String arg0, String arg1, double arg2, double arg3, boolean arg4,
            ValidationErrorList arg5) throws IntrusionException {
        return defaultValidator.getValidDouble(arg0, arg1, arg2, arg3, arg4, arg5);
    }

    @Override
    public byte[] getValidFileContent(String arg0, byte[] arg1, int arg2, boolean arg3)
            throws ValidationException, IntrusionException {
        return defaultValidator.getValidFileContent(arg0, arg1, arg2, arg3);
    }

    @Override
    public byte[] getValidFileContent(String arg0, byte[] arg1, int arg2, boolean arg3, ValidationErrorList arg4)
            throws IntrusionException {
        return defaultValidator.getValidFileContent(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public String getValidFileName(String arg0, String arg1, List<String> arg2, boolean arg3)
            throws ValidationException, IntrusionException {
        return defaultValidator.getValidFileName(arg0, arg1, arg2, arg3);
    }

    @Override
    public String getValidFileName(String arg0, String arg1, List<String> arg2, boolean arg3, ValidationErrorList arg4)
            throws IntrusionException {
        return defaultValidator.getValidFileName(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public String getValidInput(String arg0, String arg1, String arg2, int arg3, boolean arg4)
            throws ValidationException, IntrusionException {
    	if (TYPE_REDIRECT.equals(arg2)) {
    		String validInput = defaultValidator.getValidInput(arg0, arg1, arg2, MAX_URL_LENGTH, arg4, false);
    		logger.trace(Logger.EVENT_UNSPECIFIED, 
    					 String.format(GET_VALID_INPUT_1_LOG, arg0, arg1, arg2, arg4, validInput));
    		return validInput;
    	}
        return defaultValidator.getValidInput(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public String getValidInput(String arg0, String arg1, String arg2, int arg3, boolean arg4, boolean arg5)
            throws ValidationException, IntrusionException {
        return defaultValidator.getValidInput(arg0, arg1, arg2, arg3, arg4, arg5);
    }

    @Override
    public String getValidInput(String arg0, String arg1, String arg2, int arg3, boolean arg4, ValidationErrorList arg5)
            throws IntrusionException {
    	if (TYPE_REDIRECT.equals(arg2)) {
    		String validInput = defaultValidator.getValidInput(arg0, arg1, arg2, MAX_URL_LENGTH, arg4, false, arg5);
    		logger.trace(Logger.EVENT_UNSPECIFIED,
    					 String.format(GET_VALID_INPUT_2_LOG, arg0, arg1, arg2, arg4, validInput));
    		return validInput;
    	}
        return defaultValidator.getValidInput(arg0, arg1, arg2, arg3, arg4, arg5);
    }

    @Override
    public String getValidInput(String arg0, String arg1, String arg2, int arg3, boolean arg4, boolean arg5,
            ValidationErrorList arg6) throws IntrusionException {
    	if (TYPE_REDIRECT.equals(arg2)) {
    		String validInput = defaultValidator.getValidInput(arg0, arg1, arg2, MAX_URL_LENGTH, arg4, false, arg6);
    		logger.trace(Logger.EVENT_UNSPECIFIED,
    					 String.format(GET_VALID_INPUT_3_LOG, arg0, arg1, arg2, arg4, arg5, validInput));
    		return validInput;
    	}
        return defaultValidator.getValidInput(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    }

    @Override
    public Integer getValidInteger(String arg0, String arg1, int arg2, int arg3, boolean arg4)
            throws ValidationException, IntrusionException {
        return defaultValidator.getValidInteger(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public Integer getValidInteger(String arg0, String arg1, int arg2, int arg3, boolean arg4, ValidationErrorList arg5)
            throws IntrusionException {
        return defaultValidator.getValidInteger(arg0, arg1, arg2, arg3, arg4, arg5);
    }

    @Override
    public String getValidListItem(String arg0, String arg1, List<String> arg2)
            throws ValidationException, IntrusionException {
        return defaultValidator.getValidListItem(arg0, arg1, arg2);
    }

    @Override
    public String getValidListItem(String arg0, String arg1, List<String> arg2, ValidationErrorList arg3)
            throws IntrusionException {
        return defaultValidator.getValidListItem(arg0, arg1, arg2, arg3);
    }

    @Override
    public Double getValidNumber(String arg0, String arg1, long arg2, long arg3, boolean arg4)
            throws ValidationException, IntrusionException {
        return defaultValidator.getValidNumber(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public Double getValidNumber(String arg0, String arg1, long arg2, long arg3, boolean arg4, ValidationErrorList arg5)
            throws IntrusionException {
        return defaultValidator.getValidNumber(arg0, arg1, arg2, arg3, arg4, arg5);
    }

    @Override
    public char[] getValidPrintable(String arg0, char[] arg1, int arg2, boolean arg3) throws ValidationException {
        return defaultValidator.getValidPrintable(arg0, arg1, arg2, arg3);
    }

    @Override
    public String getValidPrintable(String arg0, String arg1, int arg2, boolean arg3) throws ValidationException {
        return defaultValidator.getValidPrintable(arg0, arg1, arg2, arg3);
    }

    @Override
    public char[] getValidPrintable(String arg0, char[] arg1, int arg2, boolean arg3, ValidationErrorList arg4)
            throws IntrusionException {
        return defaultValidator.getValidPrintable(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public String getValidPrintable(String arg0, String arg1, int arg2, boolean arg3, ValidationErrorList arg4)
            throws IntrusionException {
        return defaultValidator.getValidPrintable(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public String getValidRedirectLocation(String arg0, String arg1, boolean arg2)
            throws ValidationException, IntrusionException {
		String validInput = defaultValidator.getValidInput(arg0, arg1, TYPE_REDIRECT, MAX_URL_LENGTH, arg2, false);
		logger.trace(Logger.EVENT_UNSPECIFIED,
					 String.format(GET_VALID_REDIRECT_LOCATION_1_LOG, arg0, arg1, arg2, validInput));
		return validInput;
    }

    @Override
    public String getValidRedirectLocation(String arg0, String arg1, boolean arg2, ValidationErrorList arg3)
            throws IntrusionException {
    	String validInput = defaultValidator.getValidInput(arg0, arg1, TYPE_REDIRECT, MAX_URL_LENGTH, arg2, false, arg3);
		logger.trace(Logger.EVENT_UNSPECIFIED,
					 String.format(GET_VALID_REDIRECT_LOCATION_2_LOG, arg0, arg1, arg2, validInput));
		return validInput;
    }

    @Override
    public String getValidSafeHTML(String arg0, String arg1, int arg2, boolean arg3)
            throws ValidationException, IntrusionException {
        return defaultValidator.getValidSafeHTML(arg0, arg1, arg2, arg3);
    }

    @Override
    public String getValidSafeHTML(String arg0, String arg1, int arg2, boolean arg3, ValidationErrorList arg4)
            throws IntrusionException {
        return defaultValidator.getValidSafeHTML(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public boolean isValidCreditCard(String arg0, String arg1, boolean arg2) throws IntrusionException {
        return defaultValidator.isValidCreditCard(arg0, arg1, arg2);
    }

    @Override
    public boolean isValidCreditCard(String arg0, String arg1, boolean arg2, ValidationErrorList arg3)
            throws IntrusionException {
        return defaultValidator.isValidCreditCard(arg0, arg1, arg2, arg3);
    }

    @Override
    public boolean isValidDate(String arg0, String arg1, DateFormat arg2, boolean arg3) throws IntrusionException {
        return defaultValidator.isValidDate(arg0, arg1, arg2, arg3);
    }

    @Override
    public boolean isValidDate(String arg0, String arg1, DateFormat arg2, boolean arg3, ValidationErrorList arg4)
            throws IntrusionException {
        return defaultValidator.isValidDate(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public boolean isValidDirectoryPath(String arg0, String arg1, File arg2, boolean arg3) throws IntrusionException {
        return defaultValidator.isValidDirectoryPath(arg0, arg1, arg2, arg3);
    }

    @Override
    public boolean isValidDirectoryPath(String arg0, String arg1, File arg2, boolean arg3, ValidationErrorList arg4)
            throws IntrusionException {
        return defaultValidator.isValidDirectoryPath(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public boolean isValidDouble(String arg0, String arg1, double arg2, double arg3, boolean arg4)
            throws IntrusionException {
        return defaultValidator.isValidDouble(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public boolean isValidDouble(String arg0, String arg1, double arg2, double arg3, boolean arg4,
            ValidationErrorList arg5) throws IntrusionException {
        return defaultValidator.isValidDouble(arg0, arg1, arg2, arg3, arg4, arg5);
    }

    @Override
    public boolean isValidFileContent(String arg0, byte[] arg1, int arg2, boolean arg3) throws IntrusionException {
        return defaultValidator.isValidFileContent(arg0, arg1, arg2, arg3);
    }

    @Override
    public boolean isValidFileContent(String arg0, byte[] arg1, int arg2, boolean arg3, ValidationErrorList arg4)
            throws IntrusionException {
        return defaultValidator.isValidFileContent(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public boolean isValidFileName(String arg0, String arg1, boolean arg2) throws IntrusionException {
        return defaultValidator.isValidFileName(arg0, arg1, arg2);
    }

    @Override
    public boolean isValidFileName(String arg0, String arg1, boolean arg2, ValidationErrorList arg3)
            throws IntrusionException {
        return defaultValidator.isValidFileName(arg0, arg1, arg2, arg3);
    }

    @Override
    public boolean isValidFileName(String arg0, String arg1, List<String> arg2, boolean arg3)
            throws IntrusionException {
        return defaultValidator.isValidFileName(arg0, arg1, arg2, arg3);
    }

    @Override
    public boolean isValidFileName(String arg0, String arg1, List<String> arg2, boolean arg3, ValidationErrorList arg4)
            throws IntrusionException {
        return defaultValidator.isValidFileName(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public boolean isValidFileUpload(String arg0, String arg1, String arg2, File arg3, byte[] arg4, int arg5,
            boolean arg6) throws IntrusionException {
        return defaultValidator.isValidFileUpload(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    }

    @Override
    public boolean isValidFileUpload(String arg0, String arg1, String arg2, File arg3, byte[] arg4, int arg5,
            boolean arg6, ValidationErrorList arg7) throws IntrusionException {
        return defaultValidator.isValidFileUpload(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    }

    @Override
    public boolean isValidHTTPRequestParameterSet(String arg0, HttpServletRequest arg1, Set<String> arg2,
            Set<String> arg3) throws IntrusionException {
        return defaultValidator.isValidHTTPRequestParameterSet(arg0, arg1, arg2, arg3);
    }

    @Override
    public boolean isValidHTTPRequestParameterSet(String arg0, HttpServletRequest arg1, Set<String> arg2,
            Set<String> arg3, ValidationErrorList arg4) throws IntrusionException {
        return defaultValidator.isValidHTTPRequestParameterSet(arg0, arg1, arg2, arg3, arg4);
    }
    
    /*
     * Resolve redirect URL has parameters which themselves are of type URL 
     * for example /service/login?url=/service/client/poll?USource={}&notificationSince=1527204855100
     * 
     * To successfully redirect, all query string parameter values need to be URL encoded.
     * So effective URL as follows ends up with multiple and mixed encoding.
     * 
     * /service/login?url=%2Fservice%2Fclient%2Fpoll%3FUSource%3D%7B%7D%26notificationsSince%3D1527204855100
     * 
     * Please note if query parameters are not URL encoded, URL reserved characters such as
     * ?, =, & in the parameter values will cause redirect to fail with 400 status i.e. bad URL
     * so to successfully redirect one is forced to create URL with mixed and multiple encoding.
     * 
     * ESAPI.DefaultHTTPUtilities.sendRedirect(response, location) for safe redirect uses below
     * common method to validate input which internally performs canonicalization.
     * 
     * By default ESAPI properties to allow multiple and mixed encoding are false so validation
     * fails with Intrusion exception due to reason of multiple and mixed encoding html and %encoding.
     * 
     * To avoid this issue only in case of "Redirect" context & type, canonicalization is disabled.
     * 
     * Please note this is done at as high level as possible in ESAPI without creating ResolveDefaultHttpUtilities
     * instead of at lower levels in ESAPI org.owasp.esapi.refrence.validation.StringValidationRule.getValid()
     * to limit the security exposure of not checking mixed and multiple encoding for "Redirect" context only.  
     */
    
    @Override
    public boolean isValidInput(String arg0, String arg1, String arg2, int arg3, boolean arg4)
            throws IntrusionException {
    	if (TYPE_REDIRECT.equals(arg2)) {
    		boolean valid = defaultValidator.isValidInput(arg0, arg1, arg2, MAX_URL_LENGTH, arg4, false);
    		logger.trace(Logger.EVENT_UNSPECIFIED, String.format(IS_VALID_INPUT_1_LOG, arg0, arg1, arg2, arg4, valid));
    		return valid;
    	}
        return defaultValidator.isValidInput(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public boolean isValidInput(String arg0, String arg1, String arg2, int arg3, boolean arg4, ValidationErrorList arg5)
            throws IntrusionException {
    	if (TYPE_REDIRECT.equals(arg2)) {
    		boolean valid = defaultValidator.isValidInput(arg0, arg1, arg2, arg3, arg4, false, arg5);
    		logger.trace(Logger.EVENT_UNSPECIFIED, 
    					 String.format(IS_VALID_INPUT_2_LOG, arg0, arg1, arg2, arg4, valid));
    		return valid;
    	}
        return defaultValidator.isValidInput(arg0, arg1, arg2, arg3, arg4, arg5);
    }

    @Override
    public boolean isValidInput(String arg0, String arg1, String arg2, int arg3, boolean arg4, boolean arg5)
            throws IntrusionException {
        return defaultValidator.isValidInput(arg0, arg1, arg2, arg3, arg4, arg5);
    }

    @Override
    public boolean isValidInput(String arg0, String arg1, String arg2, int arg3, boolean arg4, boolean arg5,
            ValidationErrorList arg6) throws IntrusionException {
        return defaultValidator.isValidInput(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
    }

    @Override
    public boolean isValidInteger(String arg0, String arg1, int arg2, int arg3, boolean arg4)
            throws IntrusionException {
        return defaultValidator.isValidInteger(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public boolean isValidInteger(String arg0, String arg1, int arg2, int arg3, boolean arg4, ValidationErrorList arg5)
            throws IntrusionException {
        return defaultValidator.isValidInteger(arg0, arg1, arg2, arg3, arg4, arg5);
    }

    @Override
    public boolean isValidListItem(String arg0, String arg1, List<String> arg2) throws IntrusionException {
        return defaultValidator.isValidListItem(arg0, arg1, arg2);
    }

    @Override
    public boolean isValidListItem(String arg0, String arg1, List<String> arg2, ValidationErrorList arg3)
            throws IntrusionException {
        return defaultValidator.isValidListItem(arg0, arg1, arg2, arg3);
    }

    @Override
    public boolean isValidNumber(String arg0, String arg1, long arg2, long arg3, boolean arg4)
            throws IntrusionException {
        return defaultValidator.isValidNumber(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public boolean isValidNumber(String arg0, String arg1, long arg2, long arg3, boolean arg4, ValidationErrorList arg5)
            throws IntrusionException {
        return defaultValidator.isValidNumber(arg0, arg1, arg2, arg3, arg4, arg5);
    }

    @Override
    public boolean isValidPrintable(String arg0, char[] arg1, int arg2, boolean arg3) throws IntrusionException {
        return defaultValidator.isValidPrintable(arg0, arg1, arg2, arg3);
    }

    @Override
    public boolean isValidPrintable(String arg0, String arg1, int arg2, boolean arg3) throws IntrusionException {
        return defaultValidator.isValidPrintable(arg0, arg1, arg2, arg3);
    }

    @Override
    public boolean isValidPrintable(String arg0, char[] arg1, int arg2, boolean arg3, ValidationErrorList arg4)
            throws IntrusionException {
        return defaultValidator.isValidPrintable(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public boolean isValidPrintable(String arg0, String arg1, int arg2, boolean arg3, ValidationErrorList arg4)
            throws IntrusionException {
        return defaultValidator.isValidPrintable(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public boolean isValidRedirectLocation(String context, String input, boolean allowNull) {
    	Log.auth.trace("isValidRedirectLocation(" + context + ", " + input + ", " + allowNull);
        return ESAPI.validator().isValidInput(context, input, TYPE_REDIRECT, MAX_URL_LENGTH, allowNull);
    }

    @Override
    public boolean isValidRedirectLocation(String context, String input, boolean allowNull,
            ValidationErrorList errors) {
        return ESAPI.validator().isValidInput(context, input, TYPE_REDIRECT, MAX_URL_LENGTH, allowNull, errors);
    }

    @Override
    public boolean isValidSafeHTML(String arg0, String arg1, int arg2, boolean arg3) throws IntrusionException {
        return defaultValidator.isValidSafeHTML(arg0, arg1, arg2, arg3);
    }

    @Override
    public boolean isValidSafeHTML(String arg0, String arg1, int arg2, boolean arg3, ValidationErrorList arg4)
            throws IntrusionException {
        return defaultValidator.isValidSafeHTML(arg0, arg1, arg2, arg3, arg4);
    }

    @Override
    public String safeReadLine(InputStream arg0, int arg1) throws ValidationException {
        return defaultValidator.safeReadLine(arg0, arg1);
    }
}
