/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.util;

import eu.medsea.mimeutil.MimeException;
import eu.medsea.mimeutil.MimeType;
import eu.medsea.mimeutil.MimeUtil2;

public class MIMEUtils
{
    
    public static MimeUtil2 mimeUtil2 = new MimeUtil2();
    public static MimeType MIME_TYPE_APPLICATION_OCTET_STREAM = new MimeType("application/octet-stream");
    
    static
    {
        mimeUtil2.registerMimeDetector("eu.medsea.mimeutil.detector.MagicMimeMimeDetector");
    }
    
    public static String getMIMEType(String fileName)
    {
        MimeType mimeType = MIME_TYPE_APPLICATION_OCTET_STREAM;
        
        try
        {
            mimeType = MimeUtil2.getMostSpecificMimeType(mimeUtil2.getMimeTypes(fileName)); 
        }
        catch (MimeException me)
        {
            Log.log.info("Error in identifying MIME type of file " + fileName + 
                         ", setting MIME type to " + MIME_TYPE_APPLICATION_OCTET_STREAM.toString(), me);
        }
        
        return mimeType.toString(); 
    }
    
    public static String getMIMEType(byte[] bytes)
    {
        MimeType mimeType = MIME_TYPE_APPLICATION_OCTET_STREAM;
        
        try
        {
            mimeType = MimeUtil2.getMostSpecificMimeType(mimeUtil2.getMimeTypes(bytes)); 
        }
        catch (MimeException me)
        {
            Log.log.info("Error in identifying MIME type of bytes " + 
                         ", setting MIME type to " + MIME_TYPE_APPLICATION_OCTET_STREAM.toString(), me);
        }
        
        return mimeType.toString(); 
    }
} // MIMEUtils
