/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;


public class Retry
{
    boolean status;
    int count;
    int maxRetry;
    int waitTimeout;

    public Retry()
    {
        status = false;
        count = 0;
        maxRetry = 10;
        waitTimeout = 5000;
    } // Retry

    public Retry(int max)
    {
        status = false;
        count = 0;
        maxRetry = max;
        waitTimeout = 5000;
    } // Retry

    public Retry(int max, int timeout)
    {
        status = false;
        count = 0;
        maxRetry = max;
        waitTimeout = timeout;
    } // Retry

    /**
     * Returns true if status is false and count < retryMax
     * 
     * @return
     */
    public boolean isRetry()
    {
        return !status && count < maxRetry;
    } // isRetry

    /**
     * Return transaction completion status
     * 
     * @return
     */
    public boolean getStatus()
    {
        return status;
    }

    /**
     * Set transaction completion status. Set to true if transactions completed
     * successfully
     * 
     * @param rc
     */
    public void setStatus(boolean rc)
    {
        status = rc;
    } // setStatus

    /**
     * Return maximum number of retry attempts
     * 
     * @return
     */
    public int getMaxRetry()
    {
        return maxRetry;
    }

    /**
     * Initialize the maximum retry attempts
     * 
     * @param max
     *            - maximum number of retry attempts
     */
    public void setMaxRetry(int max)
    {
        maxRetry = max;
    } // setMaxRetry

    /**
     * Return the current sleep timeout between retry attempts
     * 
     * @return
     */
    public int getWaitTimeout()
    {
        return waitTimeout;
    }

    /**
     * Initialize the sleep timeout before next retry
     * 
     * @param timeout
     */
    public void setWaitTimeout(int timeout)
    {
        waitTimeout = timeout;
    } // setWaitTimeout

    /**
     * Output exception error message, wait for the specified waitTimeout and
     * increment retry count.
     * 
     * @param exception
     */
    public void error(Exception exception)
    {
        // print log message
        Log.log.error(exception.getMessage());

        // wait timeout
        try
        {
            Thread.sleep(waitTimeout);
        }
        catch (Exception e)
        {
        }

        // increment retry attempt
        count++;
    } // error

    /**
     * Output exception error message, wait for the specified waitTimeout and
     * increment retry count.
     * 
     * @param exception
     */
    public void warn(Exception exception)
    {
        // print log message
        Log.log.warn(exception.getMessage());

        // wait timeout
        try
        {
            Thread.sleep(waitTimeout);
        }
        catch (Exception e)
        {
        }

        // increment retry attempt
        count++;
    } // warn

    /**
     * Output exception error message, wait for the specified waitTimeout and
     * increment retry count.
     * 
     * @param exception
     */
    public void info(Exception exception)
    {
        // print log message
        Log.log.info(exception.getMessage());

        // wait timeout
        try
        {
            Thread.sleep(waitTimeout);
        }
        catch (Exception e)
        {
        }

        // increment retry attempt
        count++;
    } // info

    /**
     * Output exception error message, wait for the specified waitTimeout and
     * increment retry count.
     * 
     * @param exception
     */
    public void debug(Exception exception)
    {
        // print log message
        Log.log.debug(exception.getMessage());

        // wait timeout
        try
        {
            Thread.sleep(waitTimeout);
        }
        catch (Exception e)
        {
        }

        // increment retry attempt
        count++;
    } // debug

} // Retry
