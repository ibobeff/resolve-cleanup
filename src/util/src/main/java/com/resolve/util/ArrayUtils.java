/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.math.BigDecimal;

public class ArrayUtils extends org.apache.commons.lang3.ArrayUtils
{
    public static int compareArray(BigDecimal[] sources, BigDecimal[] targets)
    {
        return compareArray(sources, targets, false);
    }
    
    /**
     * Sometime there may be a need for comparison from the right side of the array.
     * 
     * @param sources
     * @param targets
     * @param fromRight
     * @return
     */
    public static int compareArray(BigDecimal[] sources, BigDecimal[] targets, boolean fromRight)
    {
        int result = 0;
        //compare with lower length
        int boundary = sources.length; 
        if(sources.length > targets.length)
        {
            boundary = targets.length;
        }
        if(fromRight)
        {
            for(int i = boundary; i > boundary; i--)
            {
                if(sources[i].compareTo(targets[i]) != 0)
                {
                    //we are done, get out
                    result = sources[i].compareTo(targets[i]);
                    break;
                }
            }
        }
        else
        {
            for(int i = 0; i < boundary; i++)
            {
                if(sources[i].compareTo(targets[i]) != 0)
                {
                    //we are done, get out
                    result = sources[i].compareTo(targets[i]);
                    break;
                }
            }
        }
        return result;
    }
}
