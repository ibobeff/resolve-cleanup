/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util.json;

import java.lang.reflect.Field;

import net.sf.json.JSONException;
import net.sf.json.util.PropertySetStrategy;

import com.resolve.util.StringUtils;

public class PropertyStrategyWrapper extends PropertySetStrategy
{

    private PropertySetStrategy original;

    public PropertyStrategyWrapper(PropertySetStrategy original)
    {
        this.original = original;
    }

    @Override
    public void setProperty(Object bean, String key, Object value) throws JSONException
    {
        String realName = "";

        //go through the class fields looking for the matching key
        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field f : fields)
        {
            if (f.getName().equalsIgnoreCase(key)) realName = f.getName();
        }

        //If its still blank, then go through the VO base fields for things like id, sysCreated, etc
        if (StringUtils.isBlank(realName))
        {
            fields = bean.getClass().getSuperclass().getDeclaredFields();
            for (Field f : fields)
            {
                if (f.getName().equalsIgnoreCase(key)) realName = f.getName();
            }
        }

        if (StringUtils.isNotBlank(realName)) original.setProperty(bean, realName, value);
    }
}
