/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DirectedTree
{
    Map<String, DirectedTreeNode> nodes;
    DirectedTreeNode start;
    Set<DirectedTreeNode> startNodes;
    Set<DirectedTreeNode> endNodes;
    Set<DirectedTreeNode> eventNodes;
    Map<String, EdgeNode> edgeNodes;

    public DirectedTree()
    {
        this.nodes = new HashMap<String, DirectedTreeNode>();
        this.startNodes = new HashSet<DirectedTreeNode>();
        this.endNodes = new HashSet<DirectedTreeNode>();
        this.eventNodes = new HashSet<DirectedTreeNode>();
        this.edgeNodes = new HashMap<String, EdgeNode>();
    } // DirectedTree

    void addNode(DirectedTreeNode node)
    {
        this.nodes.put(node.getId(), node);
    } // addNode

    void removeNode(DirectedTreeNode node)
    {
        this.nodes.remove(node.getId());
    } // removeNode

    public DirectedTreeNode getNode(String id)
    {
        return (DirectedTreeNode) this.nodes.get(id);
    } // getNode

    public void removeNode(String id)
    {
        DirectedTreeNode node = (DirectedTreeNode) this.nodes.get(id);
        if (node != null)
        {
            node.removeNode();
        }
    } // removeNode

    void addEdge(EdgeNode edge)
    {
        this.edgeNodes.put(edge.getId(), edge);
    } // addEdge

    void removeEdge(EdgeNode edge)
    {
        this.edgeNodes.remove(edge.getId());
    } // removeEdge

    public EdgeNode getEdge(String id)
    {
        return (EdgeNode) this.edgeNodes.get(id);
    } // getEdge

    public void removeEdge(String id)
    {
        this.edgeNodes.remove(id);
    } // removeEdge

    public DirectedTreeNode getStartNode()
    {
        return this.start;
    } // getStartNode

    public void setStartNode(DirectedTreeNode start)
    {
        this.start = start;
    } // setStartNode

    public Set<DirectedTreeNode> getStartNodes()
    {
        return startNodes;
    }

    public Set<DirectedTreeNode> getEndNodes()
    {
        return endNodes;
    }

    public Set<DirectedTreeNode> getEventNodes()
    {
        return eventNodes;
    }

    public Map<String, EdgeNode> getEdgeNodes()
    {
        return edgeNodes;
    }

} // DirectedTree
