package com.resolve.util;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Namespace;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.xml.sax.SAXException;

public class XmlUtils
{

    /**
     * This method will take xml structured string and normalize it to map
     * of key:value pair.</br>
     *  
     * Example:</br>
     * <pre>
     * &lt;MyXML&gt;
     *   &lt;SubTask&gt;
     *      &lt;SubSubTask&gt; MyValue &lt;/SubSubTask&gt;
     *   &lt;/SubTask&gt;
     *   &lt;SubTask2&gt;
     *      MyValue2
     *   &lt;/SubTask2&gt;
     * &lt;/MyXML&gt;
     * </pre>
     * 
     * Will produce the map:</br>
     * [MyXml.SubTask.SubSubTask : "MyValue",</br>
     * MyXml.SubTask2 : "MyValue2"]
     *  
     * @param String - XML
     * @return Map<String, String> normalizedMap - Map of key:value pairs representing the XML
     * @throws DocumentException 
     * @throws SAXException 
     */
    public static Map<String, String> normalizeXml(String xml) throws DocumentException, SAXException
    {
        Map<String, String> normalizedMap = null;
        SAXReader reader = new SAXReader();
        reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
        reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        Document xmlDoc = reader.read(new ByteArrayInputStream(xml.getBytes()));
        //Document xmlDoc = DocumentHelper.parseText(xml);
        Element root = xmlDoc.getRootElement();           
        normalizedMap = getLeafValues(root);
        return normalizedMap; 
    }
    
    private static Map<String, String> getLeafValues(Element rootNode)
    {
        return getLeafValues(rootNode, rootNode.getQualifiedName());
    }
    
    private static Map<String, String> getLeafValues(Element rootNode, String curPath)
    {
         HashMap<String, String> returnMap = new HashMap<String, String>();
         for(int i = 0 ; i < rootNode.nodeCount(); i++)
         {
             Node curNode = rootNode.node(i);
             if( curNode instanceof Element )
             {
                 returnMap.putAll(getLeafValues((Element) curNode, curPath + "." + curNode.getName()));
             }
             else if( !(curNode instanceof Namespace) && !curNode.getStringValue().trim().isEmpty() )
             {                 
                 returnMap.put(curPath, curNode.getStringValue());
             }
         }
         
         return returnMap;
    }
}
