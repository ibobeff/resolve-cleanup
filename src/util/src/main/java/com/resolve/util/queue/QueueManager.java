/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util.queue;

public final class QueueManager
{
    private static QueueManager instance = null;

    private QueueManager()
    {
    }

    public static QueueManager getInstance()
    {
        if (instance == null)
        {
            instance = new QueueManager();
        }
        return instance;
    }

    public <U> ResolveConcurrentLinkedQueue<U> getResolveConcurrentLinkedQueue(QueueListener<U> listener)
    {
        ResolveConcurrentLinkedQueue<U> queue = new ResolveConcurrentLinkedQueue<U>();
        queue.addListener(listener);
        return queue;
    }
}
