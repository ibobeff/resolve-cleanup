/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.util.clcapiclient;

// @Grab(group='org.codehaus.groovy.modules.http-builder', module='http-builder', version='0.7')

import groovyx.net.http.HttpURLClient;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import org.codehaus.jettison.json.JSONObject;

import com.gargoylesoftware.htmlunit.CookieManager;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.util.Cookie;

import com.resolve.util.Log;

public class CenturyLinkClient
{

    /**
     * The method runs the logon process against the server and get the cookie
     * from the server to support CenturyLlink Cloud API V1.
     * 
     * @param username
     * @param password
     * @return CenturyLinkConnection with the encrypted cookie value. If the the
     *         authentication fails, check the status and the reason fields.
     */
    public static void logon(CLCConnect conn) throws Exception
    {

        if (conn == null) throw new Exception("No connection is available.");

        WebClient webClient = new WebClient();

        String urlStr = conn.getHostUrl() + conn.getLogonUri();
        URL url = null;

        try
        {
            try
            {
                url = new URL(conn.getHostUrl() + conn.getLogonUri());
            }
            catch (Exception ee)
            {
                throw new Exception("Malformed URL: " + urlStr);
            }

            Log.log.debug("url = " + url.toString());

            WebRequest webRequest = new WebRequest(url);
            webRequest.setAdditionalHeader("Content-Type", "application/json; charset=utf-8");
            webRequest.setHttpMethod(HttpMethod.valueOf("POST"));

            StringBuilder builder = new StringBuilder();
            builder.append("{ \"APIKey\":\"").append(conn.getAPIKey()).append("\", \"Password\":\"").append(conn.getPasswordV1()).append("\" }");

            String body = builder.toString();
            webRequest.setRequestBody(body);

            WebResponse response = webClient.loadWebResponse(webRequest);
            if (response == null) throw new Exception("Response is not available for logon.");

            int status = response.getStatusCode();
            Log.log.debug("status = " + status);

            if (status >= 300)
                throw new Exception("Failed on HTTP connection for logon." + response.getStatusMessage());
            else
            {
                String responseStr = response.getContentAsString();
                Log.log.debug("response = " + responseStr);

                JSONObject json = new JSONObject(responseStr);
                String statusCode = json.getString("StatusCode");
                int code = (new Integer(statusCode)).intValue();

                conn.setStatusCode(code);
                Log.log.debug("status code= " + statusCode);

                if (code > 0) throw new Exception("Failed to logon. Status code is: " + code);
            }

            CookieManager cookieManager = webClient.getCookieManager();
            Set<Cookie> cookies = cookieManager.getCookies();
            String cookie = null;
            for (Iterator<Cookie> it = cookies.iterator(); it.hasNext();)
            {
                Cookie next = it.next();

                cookie = next.getValue();
                if (cookie != null)
                    conn.setCookie(cookie);
                else
                    conn.setReason("Failed to download cookie.");
                break;
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        finally
        {
            webClient.close();
        }
    }

    public static void logout(CLCConnect conn) throws Exception
    {

        WebClient webClient = new WebClient();

        try
        {
            URL url = new URL(conn.getHostUrl() + conn.getLogoutUri());
            Log.log.debug("logout url = " + url.toString());

            WebRequest webRequestOut = new WebRequest(url);
            webRequestOut.setAdditionalHeader("Content-Type", "application/json; charset=utf-8");
            webRequestOut.setHttpMethod(HttpMethod.valueOf("POST"));

            webClient.addCookie(conn.getCookie(), url, new Object());
            WebResponse response = webClient.loadWebResponse(webRequestOut);

            if (response == null) throw new Exception("Response is not available for logout.");
            Log.log.debug("response = " + response.getContentAsString());

            int status = response.getStatusCode();
            Log.log.debug("status code= " + status);

            String statusMessage = response.getStatusMessage();
            Log.log.debug("status message = " + statusMessage);

            if (status >= 300) throw new Exception("Failed on HTTP connection for logout." + statusMessage);
        }
        catch (Exception e)
        {
            conn.setReason(e.getMessage());
            throw e;
        }
        finally
        {
            webClient.close();
        }
    }

    /**
     * The method runs the authentication against the server and get the bearer
     * token and alias back to support CenturyLlink Cloud API V2. It may store
     * the alias/bearer token in the cache and database for 14 days before they
     * are refreshed by running the authentication again to retrieve an updated
     * token.
     * 
     * @param username
     * @param password
     * @return CenturyLinkConnection with the alias and bearer token
     *         information. If the the authentication fails, check the status
     *         and the reason fields.
     * @throws Exception
     */
    public static void authenticate(CLCConnect conn) throws Exception
    {

        Map<String, Object> authMap = new HashMap<String, Object>();
        authMap.put("url", conn.getHostUrl() + conn.getAuthUri());
        Log.log.debug("authenticate url = " + authMap.get("url"));

        try
        {
            HttpURLClient httpClient = new HttpURLClient();
            Log.log.debug("https.protocols = " + System.getProperty("https.protocols"));

            HttpResponse resp = httpClient.requestForBasicAuthentication(getBasicAuthenticationMap(conn));

            if (resp == null)
            {
                conn.setReason("Response is not available.");
                throw new Exception("Response is not available.");
            }

            int status = resp.getStatusLine().getStatusCode();
            Log.log.debug("status code for authentication = " + status);

            if (status >= 300) throw new Exception("Failed on HTTP connection for authentication V2.");

            HttpEntity responseEntity = resp.getEntity();

            String respStr = EntityUtils.toString(responseEntity);
            Log.log.debug("response for authentication = " + respStr);

            if (responseEntity == null || StringUtils.isBlank(respStr)) throw new Exception("Response is empty.");

            JSONObject result = new JSONObject(respStr);

            String accountAlias = result.getString("accountAlias");
            String bearerToken = result.getString("bearerToken");

            if (StringUtils.isBlank(bearerToken)) throw new Exception("Failed to get token from API server.");

            if (conn.getAccountAlias() == null) conn.setAccountAlias(accountAlias);
            conn.setToken(bearerToken);
            conn.setStatusCode(0);

            Log.log.debug("bearerToken = " + bearerToken);
        }
        catch (Exception e)
        {
            Log.log.error("Authentication failed: " + e.getMessage());
            conn.setReason("Authentication failed: " + e.getMessage());
            throw e;
        }
    }

    public static String callAPIWithMap(CLCConnect conn, String APIName, String httpMethod, Map<String, String> body, String locationAlias) throws Exception
    {

        return callAPIWithMap(conn, APIName, httpMethod, body, locationAlias, null, null);

    }

    public static String callAPIWithMap(CLCConnect conn, String APIName, String httpMethod, Map<String, String> body, String locationAlias, Integer timeout, Integer interval) throws Exception
    {

        if (conn == null || StringUtils.isBlank(APIName) || StringUtils.isBlank(httpMethod)) throw new Exception("Invalid input parameters.");

        String resp = null;

        if (body == null || body.size() == 0) return callAPIWithJSON(conn, APIName, httpMethod, null, locationAlias, timeout, interval);

        body.remove("path");
        body.remove("query");
        body.remove("headers");
        body.remove("url");
        body.remove("body");
        body.remove("aruth");
        body.remove("Content-Type");
        body.remove("Request-Content-Type");

        if (body.size() == 0) return callAPIWithJSON(conn, APIName, httpMethod, null, locationAlias, timeout, interval);

        StringBuilder builder = new StringBuilder("{ ");

        try
        {
            for (Iterator<String> it = body.keySet().iterator(); it.hasNext();)
            {
                String key = it.next();
                String value = body.get(key);
                builder.append("\"" + key + "\": \"" + value + "\", ");
            }
            String bodyStr = builder.toString();
            bodyStr = bodyStr.substring(0, bodyStr.length() - 2) + " }";

            resp = callAPIWithJSON(conn, APIName, httpMethod, new JSONObject(bodyStr), locationAlias, timeout, interval);
        }
        catch (Exception e)
        {
            throw e;
        }

        return resp;
    }

    public static String callAPIWithJSON(CLCConnect conn, String APIName, String httpMethod, JSONObject body, String locationAlias) throws Exception
    {

        return callAPIWithJSON(conn, APIName, httpMethod, body, locationAlias, null, null);
    }

    public static String callAPIWithJSON(CLCConnect conn, String APIName, String httpMethod, JSONObject body, String locationAlias, Integer timeout, Integer interval) throws Exception
    {

        String resp = null;

        if (conn == null || StringUtils.isBlank(APIName) || StringUtils.isBlank(httpMethod)) throw new Exception("Invalid input parameters.");

        if (!APIName.startsWith("/")) APIName = "/" + APIName;
        if (APIName.contains("/v2"))
        {
            String alias = conn.getAccountAlias();
            String token = conn.getToken();
            Log.log.debug("alias = " + alias);
            Log.log.debug("token = " + token);

            if (alias == null || token == null) throw new Exception("Alias or token is not available for calling v2 API.");

            resp = sendRequestWithToken(conn, httpMethod, conn.getHostUrl() + APIName, body, locationAlias, timeout, interval);
        }
        else
        {
            String cookie = conn.getCookie();
            if (cookie == null) throw new Exception("cookie is not available to call v1 API.");

            resp = sendRequestWithCookie(conn, httpMethod, conn.getHostUrl() + APIName, body, locationAlias, timeout, interval);
        }

        return resp;
    }

    private static String sendRequestWithCookie(CLCConnect conn, String httpMethod, String url, JSONObject body, String locationAlias, Integer timeout, Integer interval) throws Exception
    {

        String resp = null;

        WebClient webClient = new WebClient();
        Log.log.debug("url = " + url);
        Log.log.debug("body = " + body);

        try
        {
            WebRequest webRequest = new WebRequest(new URL(url));
            webRequest.setAdditionalHeader("Content-Type", "application/json; charset=utf-8");
            webRequest.setHttpMethod(HttpMethod.valueOf(httpMethod));
            if (body != null) webRequest.setRequestBody(body.toString());

            Cookie cookieObj = new Cookie(conn.getCookieDomain(), conn.getCookieName(), conn.getCookie());
            CookieManager cookieManager = webClient.getCookieManager();
            cookieManager.addCookie(cookieObj);

            WebResponse response = webClient.loadWebResponse(webRequest);

            if (response == null) throw new Exception("Response is not available.");

            resp = response.getContentAsString();
            Log.log.debug("resp = " + resp);

            int status = response.getStatusCode();
            Log.log.debug("status = " + status);

            if (status >= 300) throw new Exception("Failed on HTTP connection: " + response.getStatusMessage());

            if (resp != null)
            {
                JSONObject json = new JSONObject(resp);

                int statusCode = json.getInt("StatusCode");
                conn.setStatusCode(statusCode);
                Log.log.debug("statusCode = " + statusCode);

                if (statusCode != 0)
                {
                    String message = json.getString("Message");
                    Log.log.debug("message = " + message);
                    conn.setStatus(message);
                    throw new Exception("Response status code is: " + statusCode + " for the reason of " + message);
                }

                if (timeout != null)
                {
                    Integer requestId = (Integer) json.get("RequestID");
                    if (requestId != null)
                        resp = callAsyncRequest(conn, requestId, locationAlias, timeout, interval);
                    else
                        throw new Exception("No request id is available.");
                }
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            webClient.close();
        }

        return resp;
    }

    private static String sendRequestWithToken(CLCConnect conn, String httpMethod, String url, JSONObject body, String locationAlias, Integer timeout, Integer interval) throws Exception
    {

        JSONObject json = null;
        HttpURLClient httpClient = new HttpURLClient();
        HttpResponse resp = httpClient.requestForBasicAuthentication(getRequestMapWithToken(conn.getToken(), httpMethod, url, body));

        Log.log.debug("url = " + url);
        Log.log.debug("body = " + body);
        Log.log.debug("locationAlias = " + locationAlias);

        if (resp == null) throw new Exception("No response is available.");

        HttpEntity responseEntity = resp.getEntity();
        String response = EntityUtils.toString(responseEntity);
        Log.log.debug("response = " + response);

        if (responseEntity != null && StringUtils.isNotBlank(response))
            json = new JSONObject(response);
        else
            throw new Exception("Response is blank.");

        if (timeout != null)
        {
            Integer requestId = (Integer) json.get("RequestID");
            if (requestId != null)
            {
                String result = callAsyncRequest(conn, requestId, locationAlias, timeout, interval);
                conn.setStatus(result);
            }
            else
                throw new Exception("No request id is available.");
        }

        return json.toString();
    }

    private static Map<String, ?> getBasicAuthenticationMap(CLCConnect conn) throws UnsupportedEncodingException
    {

        Map<String, Object> authMap = new HashMap<String, Object>();
        authMap.put("url", conn.getHostUrl() + conn.getAuthUri());

        authMap.put("method", "POST");
        authMap.put("contentType", "application/json");
        authMap.put("requestContentType", "application/json");

        StringBuilder builder = new StringBuilder();
        builder.append("{ \"username\": \"").append(conn.getUsername()).append("\", \"password\": \"").append(conn.getPasswordV2()).append("\" }");

        String json = builder.toString();
        authMap.put("body", json);

        return authMap;
    }

    private static Map<String, Object> getRequestMapWithToken(String token, String method, String url, JSONObject body) throws Exception
    {

        if (StringUtils.isBlank(token)) throw new Exception("Token cannot be empty.");

        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put("Authorization", "Bearer " + token);

        Map<String, Object> requestMap = new HashMap<String, Object>();

        requestMap.put("url", url);
        requestMap.put("method", method);
        requestMap.put("contentType", "application/json");
        requestMap.put("requestContentType", "application/json");
        requestMap.put("headers", headerMap);
        if (body != null) requestMap.put("body", body);

        return requestMap;
    }

    private static String callAsyncRequest(CLCConnect conn, Integer requestId, String locationAlias, Integer timeout, Integer interval) throws Exception
    {
        Log.log.debug("requestId = " + requestId);
        Log.log.debug("locationAlias = " + locationAlias);
        Log.log.debug("timeout = " + timeout);
        Log.log.debug("interval = " + interval);

        JSONObject body = new JSONObject();
        body.put("RequestID", requestId.toString());
        body.put("LocationAlias", locationAlias);

        int defaultInterval = conn.getInterval();
        if (interval != null && interval.intValue() != 0) 
        {
            defaultInterval = interval.intValue();
        }

        String status = "";
        int statusCode = -1;
        String resp = null;
        JSONObject json = null;

        for (int i = 0; i < timeout / defaultInterval; i++)
        {
            Thread.sleep(interval * 1000);

            resp = sendRequestWithCookie(conn, "POST", conn.getHostUrl() + conn.getQueryUri(), body, locationAlias, null, null);
            if (resp == null)
            {
                continue;
            }

            json = new JSONObject(resp);
            statusCode = ((Integer) json.get("StatusCode")).intValue();

            // When the API returns a bad status > 0, the checking is stopped.
            if (statusCode > 0)
            {
                status = (String) json.get("Message");
                break;
            }
            status = (String) json.get("CurrentStatus");
            Integer percentage = (Integer) json.get("PercentComplete");

            if (percentage.intValue() == 100 && status.equals("Succeeded") || status.equals("Failed"))
            {
                break;
            }
            else
            {
                continue;
            }
        } // end of for().

        conn.setStatusCode(statusCode);
        conn.setStatus(status);

        return resp;
    }
	
	/*
    public static void main(String[] args)
    {
        try
        {
            CLCConnect conn = new CLCConnect("e217d30ad3f643579e46528dbdf609c2", "0zor.W-l5@=J]rlV", "demei.cao", "P@ssw0rd1", "RSL");
            System.out.println("1.1 Test logon");
            CLCInterface clc = new CLCInterface(conn);
            clc.connect();
            if (conn.getStatusCode() > 0) System.exit(-1);
            System.out.println();
            System.out.println("1.2 Test call API GetAccountDetails");
            // Call GetAccountDetails API from Century Link Cloud v1
            Map<String, String> body = new HashMap<String, String>();
            body.put("AccountAlias", conn.getAccountAlias());

            String resp = clc.post("/REST/Account/GetAccountDetails/JSON", body);
            if (resp == null) System.exit(-1);
            System.out.println();
            System.out.println("1.3 Test call API RebootServer");
            // Call Reboot Server and check the status until it's completed.
            Map<String, String> body1 = new HashMap<String, String>();
            body1.put("AccountAlias", "RSL");
            body1.put("Name", "CA1RSL-PING-02");
            String resp1 = clc.call("/REST/Server/RebootServer/JSON", "POST", body1, "CA1", new Integer(100), null);
            System.out.println();
            System.out.println("1.4 Test logout");
            CenturyLinkClient.logout(conn);
            System.out.println();
            System.out.println("2.1 Test Authentication");
            // conn.setAlias("RSL");
            // conn.setToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ1cm46YXBpLXRpZXIzIiwiYXVkIjoidXJuOnRpZXIzLXVzZXJzIiwibmJmIjoxNDQ2NjAwMzE3LCJleHAiOjE0NDc4MDk5MTcsInVuaXF1ZV9uYW1lIjoiZGVtZWkuY2FvIiwidXJuOnRpZXIzOmFjY291bnQtYWxpYXMiOiJSU0wiLCJ1cm46dGllcjM6bG9jYXRpb24tYWxpYXMiOiJVQzEiLCJyb2xlIjpbIkFjY291bnRWaWV3ZXIiLCJTZXJ2ZXJBZG1pbiJdfQ.z2S-HU8wBqrp71OjE4zKmDAV4tc5rIc_HfGQoKJ7McY");
            /*
             * Call Create Server API String url =
             * "https://api.ctl.io/v2/servers/" + alias; JSONObject body = new
             * JSONObject(); body.put("name", "rhel"); body.put("description",
             * "My RedHat Server"); body.put("groupId",
             * "60582771e45fe111ba2500505682315a"); body.put("sourceServerId",
             * "RHEL-6-64-TEMPLATE"); body.put("password", "P@ssw0rd1");
             * body.put("cpu", 2); body.put("memoryGB", 4); body.put("type",
             * "standard"); body.put("storageType", "standard");
             *
            System.out.println();
            System.out.println("2.2 Test call API V2 GetServer");
            // Call GET Server API from Century Link Cloud v2
            String APIName = "v2/servers/RSL/CA1RSL-PING-02";
            String respV2 = clc.get(APIName, null);
            /*
             * JSONArray links = result.getJSONArray("details");
             * 
             * int len = links.length(); for(int i=0; i<len; i++) { JSONObject
             * link = links.getJSONObject(i); if(link instanceof Map) {
             * 
             * } }
             *
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }*/

} // end of class CenturyLinkClient.
