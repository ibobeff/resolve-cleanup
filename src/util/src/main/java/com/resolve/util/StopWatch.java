/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

public class StopWatch
{
    long starttime;

    public StopWatch()
    {
        reset();
    } // StopWatch

    public void reset()
    {
        starttime = System.currentTimeMillis();
    } // StopWatch

    public long getDuration()
    {
        return System.currentTimeMillis() - starttime;
    } // getDuration

    public int getDurationSecs()
    {
        return (int) ((System.currentTimeMillis() - starttime) / 1000);
    } // getDurationSecs

    public String msec()
    {
        long time = System.currentTimeMillis() - starttime;

        return time + " msec";
    } // msec

    public String sec()
    {
        long time = System.currentTimeMillis() - starttime;
        time = time / 1000;

        return time + " sec";
    } // sec

    public String min()
    {
        long time = System.currentTimeMillis() - starttime;
        time = time / 1000 / 60;

        return time + " min";
    } // min

    public String hr()
    {
        long time = System.currentTimeMillis() - starttime;
        time = time / 1000 / 60 / 60;

        return time + " hr";
    } // hr

} // StopWatch
