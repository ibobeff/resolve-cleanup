/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PasswordField
{
    /**
     * @param prompt
     *            - prompt message to display to the user
     * 
     * @return The password as entered by the user
     */
    public static String getPassword(String prompt)
    {
        String result = "";

        PasswordMaskingThread et = new PasswordMaskingThread(prompt);
        Thread mask = new Thread(et);
        mask.start();

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        // get pasword
        try
        {
            result = in.readLine();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }

        // stop masking
        et.stopMasking();

        return result;
    } // getPassword

} // PasswordField
