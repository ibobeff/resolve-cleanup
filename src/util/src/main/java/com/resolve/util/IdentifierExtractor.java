package com.resolve.util;

@FunctionalInterface
public interface IdentifierExtractor<T>
{
    public T extractIdentifier(Object data);
}
