/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.File;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class WikiProcessParser
{
    final static String SEQUENCE = "SEQUENCE";
    final static String PARALLEL = "PARALLEL";
    final static String COMMENT = "COMMENT";

    ThreadList thread; // current thread container object
    ThreadList start; // start thread - list of all threads contain by start
                      // thread
    String currTask; // name of the current task being processed
    String prevTask; // name of previous task processed
    String prevType; // type (SEQ or PAR) of previous task
    boolean isEndThread; // end thread
    int level; // indentation level of current task
    int prevLevel; // indentation level of previous task

    Map dependsOn = new LinkedHashMap(); // key task => set of tasks that key
                                         // task is dependent on
    Map isRequiredBy = new LinkedHashMap(); // key task => set of tasks
                                            // depending on key task (inverse of
                                            // nodes)
    Map threads = new Hashtable(); // map of all open thread container objects
                                   // (indexed by split task name)
    Map latestTaskForThread = new Hashtable(); // map to latest task for thread
    Map threadForLevel = new Hashtable(); // map to start thread for level
    Map latestSplitPointForLevel = new Hashtable(); // map to latest split for
                                                    // level

    /*
    public static void main(String[] args) throws Exception
    {
        if (args.length < 1)
        {
            System.out.println("Usage: <filename>");
            System.exit(0);
        }

        // init log
        Log.init("com.resolve");

        String filename = args[0];
        File file = FileUtils.getFile(filename);

        List content = FileUtils.readLines(file, "UTF-8");
        WikiProcessParser parser = new WikiProcessParser();
        Map nodes = parser.parse(content);

        // output initial content
        for (Iterator i = content.iterator(); i.hasNext();)
        {
            System.out.println(i.next());
        }
        System.out.println("\n");

        // output node dependency
        System.out.println(parser.printNodes(nodes));
    } // main
    */

    public WikiProcessParser()
    {
        currTask = null;
        prevTask = null;
        prevType = null;
        level = 0;
        prevLevel = 0;

        start = new ThreadList("START");
        thread = start;
        setThreadForLevel(level, start);
        setSplitPointForLevel(level, start);
    } // WikiParser

    /*
     * Generates map of task names with associated comma-separated list of
     * dependent task names (tasks is required to complete prior to execution)
     */

    public Map parse(List content)
    {
        for (Iterator i = content.iterator(); i.hasNext();)
        {
            String line = (String) i.next();
            line = line.trim();

            // get tag and task
            int separator = line.indexOf(' ');
            if (separator != -1)
            {
                String type;
                int level;

                String tag = line.substring(0, separator).trim();
                String task = line.substring(separator + 1, line.length()).trim();

                // init isEnd
                isEndThread = false;

                // check for comments
                if (tag.charAt(0) != '#')
                {
                    // parse tag
                    if (tag.charAt(0) == '*')
                    {
                        type = PARALLEL;
                        level = tag.length();
                    }
                    else
                    {
                        type = SEQUENCE;

                        // check if tag contains 1(##).
                        int bracketPos = tag.indexOf('(');
                        if (bracketPos == -1)
                        {
                            level = tag.length() - 1;
                        }
                        else
                        {
                            level = bracketPos;

                            // check if END ActionProcess
                            if (tag.charAt(bracketPos + 1) == '-')
                            {
                                isEndThread = true;
                                level = level - 1;
                            }
                        }
                    }

                    // parse task dependency
                    if (!StringUtils.isEmpty(task))
                    {
                        // init task dependency
                        initDependency(task);

                        // parse task
                        if (type.equals(PARALLEL))
                        {
                            parseParallel(task, level);
                        }
                        else
                        {
                            parseSequence(task, level);
                        }
                    }

                    // update
                    prevTask = task;
                    prevLevel = level;
                    if (isEndThread)
                    {
                        // ignore END THREAD sequence
                        prevType = PARALLEL;
                    }
                    else
                    {
                        prevType = type;
                    }
                }
            }
        }

        // rationalize dependencies
        rationalize();

        return dependsOn;
    } // parse

    public String printNodes()
    {
        return printNodes(null);
    } // printNodes

    public String printNodes(Map nodes)
    {
        String result = "";

        // init nodes
        if (nodes == null)
        {
            nodes = this.dependsOn;
        }

        // output node dependency
        for (Iterator i = nodes.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry entry = (Map.Entry) i.next();

            String task = (String) entry.getKey();
            Set dependency = (Set) entry.getValue();

            String dependents = "";
            for (Iterator depIdx = dependency.iterator(); depIdx.hasNext();)
            {
                dependents += (String) depIdx.next();
                if (depIdx.hasNext())
                {
                    dependents += ",";
                }
            }

            result += task + ": " + dependents + "\n";
        }

        return result;
    } // printNodes

    public String printNodesToHTML(Map nodes)
    {
        String result = "";

        // init nodes
        if (nodes == null)
        {
            nodes = this.dependsOn;
        }

        // output node dependency
        for (Iterator i = nodes.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry entry = (Map.Entry) i.next();

            String task = (String) entry.getKey();
            Set dependency = (Set) entry.getValue();

            String dependents = "";
            for (Iterator depIdx = dependency.iterator(); depIdx.hasNext();)
            {
                dependents += (String) depIdx.next();
                if (depIdx.hasNext())
                {
                    dependents += ",";
                }
            }

            result += task + ": " + dependents + "<br/>";
        }

        return result;
    } // printNodesToHTML

    void parseSequence(String task, int level)
    {
        if (!StringUtils.isEmpty(task))
        {
            // startup - prevLevel not defined
            if (prevLevel == 0)
            {
                // set thread for level
                setThreadForLevel(level, thread);
            }

            else if (prevLevel == level)
            {
                if (prevType.equals(SEQUENCE))
                {
                    // add dependency on previous task
                    addDependency(task, prevTask);
                }
                else if (prevType.equals(PARALLEL))
                {
                    // add dependency on latest dependent task for thread
                    addDependency(task, getLatestTaskForThread(thread));
                }
            }

            else if (prevLevel < level)
            {
                // set split point
                ThreadList prevThread = thread;
                thread = getThread(prevTask);
                setSplitPointForLevel(prevLevel, thread);
                prevThread.add(thread);

                // set thread for level
                setThreadForLevel(level, thread);

                // add dependency on previous task
                addDependency(task, prevTask);
            }

            // merge
            else if (prevLevel > level)
            {
                boolean isMerge = false;
                ThreadList mergeThread = null;

                // get thread for current level
                thread = getThreadForLevel(level);

                // check if parent split point exists
                if (thread != null)
                {
                    // get thread to merge
                    mergeThread = getSplitPointForLevel(level);
                }
                else
                {
                    // find parent thread
                    int parentLevel = level - 1;
                    mergeThread = getSplitPointForLevel(parentLevel);
                    while (mergeThread == null && parentLevel > 0)
                    {
                        parentLevel--;
                        mergeThread = getSplitPointForLevel(parentLevel);
                    }
                    if (mergeThread != null)
                    {
                        thread = getThreadForLevel(parentLevel);

                    }
                    else if (parentLevel == 0)
                    {
                        thread = start;
                    }
                }

                if (mergeThread != null)
                {
                    // get open dependencies on thread
                    Set dependencies = new HashSet();
                    mergeThread(dependencies, mergeThread);

                    // close mergeThread
                    removeThread(thread, mergeThread);

                    if (dependencies.size() > 0)
                    {
                        isMerge = true;

                        for (Iterator i = dependencies.iterator(); i.hasNext();)
                        {
                            String dependencyTask = (String) i.next();
                            addDependency(task, dependencyTask);
                        }
                    }
                }

                // if not merge, add dependency for current thread
                if (!isMerge)
                {
                    addDependency(task, getLatestTaskForThread(thread));
                }
            }

            if (!isEndThread)
            {
                // update thread with current task
                setLatestTaskForThread(thread, task);
            }
        }
    } // parseSequence

    void parseParallel(String task, int level)
    {
        if (!StringUtils.isEmpty(task))
        {
            // startup - prevLevel not defined
            if (prevLevel == 0)
            {
                // set thread for level
                setThreadForLevel(level, thread);
            }

            else if (prevLevel == level)
            {
                if (prevType.equals(SEQUENCE))
                {
                    // add dependency on previous task
                    addDependency(task, prevTask);
                }
                else if (prevType.equals(PARALLEL))
                {
                    // add dependency on latest dependent task for thread
                    addDependency(task, getLatestTaskForThread(thread));
                }
            }

            else if (prevLevel < level)
            {
                // set split point at prevTask
                ThreadList prevThread = thread;
                thread = getThread(prevTask);
                setSplitPointForLevel(prevLevel, thread);
                prevThread.add(thread);

                // set thread for level
                setThreadForLevel(level, thread);

                // add dependency on previous task (including split tasks due to
                // indentation)
                addDependency(task, prevTask);

                // set latest task for new thread
                setLatestTaskForThread(thread, prevTask);
            }

            else if (prevLevel > level)
            {
                // get thread at current level
                thread = getThreadForLevel(level);

                // check if parent split point exists
                if (thread != null)
                {
                    // add dependency on latest dependent task for thread
                    addDependency(task, getLatestTaskForThread(thread));
                }
                else
                {
                    // find parent thread
                    int parentLevel = level - 1;
                    ThreadList mergeThread = getSplitPointForLevel(parentLevel);
                    while (mergeThread == null && parentLevel > 0)
                    {
                        parentLevel--;
                        mergeThread = getSplitPointForLevel(parentLevel);
                    }
                    if (mergeThread != null)
                    {
                        thread = getThreadForLevel(parentLevel);
                        addDependency(task, getLatestTaskForThread(mergeThread));
                    }
                    else if (parentLevel == 0)
                    {
                        thread = start;
                    }
                }
            }

            // create thread for task
            ThreadList newThread = getThread(task);

            // add thread to current thread
            thread.add(newThread);

            // set latest task for new thread
            setLatestTaskForThread(newThread, task);
        }
    } // parseParallel

    /*
     * Recursive depth traversal from mergeThread to get latest dependency task
     * of all open threads and close each thread.
     */
    void mergeThread(Set result, ThreadList mergeThread)
    {
        if (mergeThread != null)
        {
            for (Iterator i = mergeThread.iterator(); i.hasNext();)
            {
                ThreadList thread = (ThreadList) i.next();

                if (thread != null)
                {
                    mergeThread(result, thread);
                }
            }

            // close threads
            mergeThread.clear();

            // add thread latest task
            result.add(getLatestTaskForThread(mergeThread));
        }
    } // mergeThread

    void initDependency(String task)
    {
        if (!dependsOn.containsKey(task))
        {
            dependsOn.put(task, new HashSet());
        }
    } // initDependency

    void addDependency(String task, String dependsOnTask)
    {
        if (!StringUtils.isEmpty(dependsOnTask))
        {
            // update task dependencies
            Set tasks = (Set) dependsOn.get(task);
            if (tasks == null)
            {
                tasks = new HashSet();
                dependsOn.put(task, tasks);
            }

            // add dependent task
            tasks.add(dependsOnTask);

            /*
             * // expanded dependent task and add sub-dependencies Set expanded
             * = (Set)dependsOn.get(dependsOnTask); if (expanded != null &&
             * expanded.size() > 0) { tasks.addAll(expanded); }
             */

            // update required by
            tasks = (Set) isRequiredBy.get(dependsOnTask);
            if (tasks == null)
            {
                tasks = new HashSet();
                isRequiredBy.put(dependsOnTask, tasks);
            }
            tasks.add(task);
        }
    } // addDependency

    String getLatestTaskForThread(ThreadList thread)
    {
        String result = "";

        if (thread != null)
        {
            result = (String) latestTaskForThread.get(thread.getName());
            if (result == null)
            {
                result = "";
            }
        }
        return result;
    } // getLatestTaskForThread

    void setLatestTaskForThread(ThreadList thread, String task)
    {
        latestTaskForThread.put(thread.getName(), task);
    } // setLatestTaskForThread

    ThreadList getThread(String task)
    {
        ThreadList result = (ThreadList) threads.get(task);
        if (result == null)
        {
            result = new ThreadList(task);
            threads.put(task, result);
        }
        return result;
    } // getThread

    void removeThread(ThreadList thread, ThreadList removeThread)
    {
        threads.remove(removeThread.getName());
        thread.remove(removeThread);
    } // removeThread

    ThreadList getThreadForLevel(int level)
    {
        return (ThreadList) threadForLevel.get("" + level);
    } // gettThreadForLevel

    void setThreadForLevel(int level, ThreadList thread)
    {
        threadForLevel.put("" + level, thread);
    } // setThreadForLevel

    ThreadList getSplitPointForLevel(int level)
    {
        return (ThreadList) latestSplitPointForLevel.get("" + level);
    } // getSplitPointForLevel

    void setSplitPointForLevel(int level, ThreadList thread)
    {
        latestSplitPointForLevel.put("" + level, thread);
    } // setSplitPointForLevel

    void rationalize()
    {
        // init nodes
        if (dependsOn != null)
        {
            // output node dependency
            for (Iterator i = dependsOn.entrySet().iterator(); i.hasNext();)
            {
                Map.Entry entry = (Map.Entry) i.next();

                String task = (String) entry.getKey();
                Set dependency = (Set) entry.getValue();

                Set resultset = new HashSet(dependency);
                for (Iterator depIdx = dependency.iterator(); depIdx.hasNext();)
                {
                    String dependent = (String) depIdx.next();

                    // update if resultset is modified
                    if (rationalize(dependent, dependent, resultset, task))
                    {
                        // remove task from resultset
                        resultset.remove(dependent);

                        // update resultset
                        dependsOn.put(task, resultset);
                    }
                }
            }
        }
    } // rationalize

    boolean rationalize(String removeTask, String traverseTask, Set resultset, String task)
    {
        boolean result = false;

        // get set of tasks that is depending on "dependent"
        Set requires = (Set) isRequiredBy.get(traverseTask);
        if (requires != null)
        {
            // check if any of the task in the set is in the "resultset"
            for (Iterator i = requires.iterator(); !result && i.hasNext();)
            {
                String requireTask = (String) i.next();

                if (!requireTask.equals(removeTask) && resultset.contains(requireTask))
                {
                    result = true;
                }
            }

            // if not found, recurse each of the "requires" tasks
            if (!result)
            {
                // Set dependents = (Set)dependsOn.get(traverseTask);
                for (Iterator i = requires.iterator(); !result && i.hasNext();)
                {
                    String requireTask = (String) i.next();

                    result = rationalize(removeTask, requireTask, resultset, task);
                }
            }

        }

        return result;
    } // rationalize

} // WikiParser

class ThreadList extends LinkedHashSet
{
    private static final long serialVersionUID = 6720576217759041384L;
	String name;

    public ThreadList(String name)
    {
        this.name = name;
    } // ThreadList

    public String getName()
    {
        return name;
    } // getName

    public void setName(String name)
    {
        this.name = name;
    } // setName

    public String toString()
    {
        return this.name;
    } // toString

    public boolean equals(Object thread)
    {
        return this.name.equals(((ThreadList) thread).getName());
    } // equals

} // ThreadList
