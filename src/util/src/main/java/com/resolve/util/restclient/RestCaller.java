/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util.restclient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpHost;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.KerberosCredentials;
import org.apache.http.auth.NTCredentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.ietf.jgss.GSSCredential;
import org.ietf.jgss.GSSException;
import org.ietf.jgss.GSSManager;

import com.resolve.util.Base64;
import com.resolve.util.Log;
import com.resolve.util.SSLUtils;
import com.resolve.util.StringUtils;

/**
 * This is a convenience class for consuming REST web services. This is most
 * generic and lowest level class. It is used by gateways such as TSRM and
 * ServiceNow.
 * 
 * All non standard vendor specific header processing and flow control 
 * logic should be in dervived or wrapper class. 
 */
public class RestCaller
{
    private final String baseUrl;
    private String protocol = "http";
    private int port = 80;
    protected boolean isSSL = false;
   // private String testUrl;
    private String httpbasicauthusername;
    private String httpbasicauthpassword;

    private String urlSuffix;

    private static SSLSocketFactory sslSocketFactory = null;
    private static String TLS = "TLS";
    
    // Response status and headers 
    protected int statusCode;
    protected String reasonPhrase;
    protected String responseEntity;
    protected Map<String, String> responseHeaders = new HashMap<String, String>();

    // REST service support
    
    protected String user = null;
    protected String pass = null;
    protected boolean basicAuth = true;
    protected boolean cookieEnabled = true;
    protected boolean selfSigned = true;
    protected boolean trustAll = false;
    protected static volatile CookieStore cookieStore = null;
    
    // Proxy Support

    protected boolean proxyEnabled = false;
    protected Credentials proxyCredentials = null;
    protected AuthScope proxyAuthScope = null;
    protected HttpHost proxy = null;
    
    private static final String lineSeparator = System.getProperty("line.separator");
    private static final String COOKIE = "Cookie";
    private static final String PATH = "path";
    private static final String SET_COOKIE = "Set-Cookie";
    private static final String COOKIE_VALUE_DELIMITER = ";";
    private static final char NAME_VALUE_SEPARATOR = '=';
    
    private static volatile CookieManager cookieManager = null;
    
    static {
        cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        cookieStore = cookieManager.getCookieStore();
// The CookieHandler honors the "httponly" portion of the cookie; that is why you don't see the value for the Set-Cookie header when using the CookieHandler.
//      CookieHandler.setDefault(cookieManager);

        String version = System.getProperty("java.version");
        int pos = version.indexOf('.');
        pos = version.indexOf('.', pos+1);
        double v = Double.parseDouble (version.substring (0, pos));
        if(v > 1.7)
            TLS = "TLSv1.2";
    }
    
    /**
     * Constructor
     * 
     * @param url
     *            in the form of
     *            http://tsrm_server:port/maxrest/rest/os/mxsr?_lid
     *            =maxadmin&_lpwd=maxadmin
     */
    public RestCaller(String baseUrl)
    {
        if(StringUtils.isBlank(baseUrl))
        {
            throw new IllegalStateException("baseUrl must be provided.");
        }
        
        //this is universally applicable 
        String trimmedBaseUrl = baseUrl.trim();
        this.baseUrl = trimmedBaseUrl + (trimmedBaseUrl.endsWith("/")? "": "/");
        
        URL theUrl;
        try
        {
            theUrl = new URL(baseUrl);
            if ("https".equals(theUrl.getProtocol()))
            {
                try
                {
                    SSLContext ctx = SSLContext.getInstance(TLS);
                    SSLUtils.FakeX509TrustManager tm = new SSLUtils.FakeX509TrustManager();
                    ctx.init(null, new TrustManager[] { tm }, null);
                    sslSocketFactory = new SSLSocketFactory(ctx, new SSLUtils.FakeX509HostnameVerifier());
                    isSSL = true;
                    protocol = "https";
                    if(theUrl.getPort() == -1)
                    {
                        port = 443;
                    }
                }
                catch (NoSuchAlgorithmException e)
                {
                    Log.log.error(e.getMessage(), e);
                }
                catch (KeyManagementException e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
            else
            {
                if(theUrl.getPort() == -1)
                {
                    port = 80;
                }
            }
            
            if (StringUtils.isBlank(httpbasicauthusername) || StringUtils.isBlank(httpbasicauthpassword)) {
            	basicAuth = false;
            }
        }
        catch (MalformedURLException e1)
        {
            Log.log.error(e1.getMessage(), e1);
        }
    }

    public RestCaller(String baseUrl, String httpbasicauthusername, String httpbasicauthpassword)
    {
        this(baseUrl);
        this.httpbasicauthusername = httpbasicauthusername;
        this.httpbasicauthpassword = httpbasicauthpassword;
        
        if (StringUtils.isBlank(this.httpbasicauthusername) || StringUtils.isBlank(this.httpbasicauthpassword)) {
        	basicAuth = false;
        } else {
        	basicAuth = true;
        }
    }
    
    public RestCaller(String baseUrl, String httpbasicauthusername, String httpbasicauthpassword,
                      ConfigProxy configProxy)
    {
        this(baseUrl, httpbasicauthusername, httpbasicauthpassword);
        
        if (configProxy != null && configProxy.proxyEnabled)
        {
            if (StringUtils.isNotBlank(configProxy.proxyHost) &&
                !configProxy.proxyHost.equals("127.0.0.1") && 
                configProxy.proxyPort > 0)
            {
                try
                {
                    URL theUrl = new URL(baseUrl);
                    
                    proxyEnabled = !configProxy.isNonProxyHost(theUrl);
                                    
                    if (proxyEnabled)
                    {
                        if (!configProxy.authType.equalsIgnoreCase(ConfigProxy.PROXY_AUTHENTICATION_TYPE_NONE))
                        {
                            switch(configProxy.authType)
                            {
                                case ConfigProxy.PROXY_AUTHENTICATION_TYPE_BASIC:
                                    proxyCredentials = new UsernamePasswordCredentials(configProxy.proxyUser, 
                                                                                       configProxy.proxyP_assword);
                                    break;
                                    
                                case ConfigProxy.PROXY_AUTHENTICATION_TYPE_NTLM:
                                    proxyCredentials = new NTCredentials(configProxy.proxyUser, 
                                                                         configProxy.proxyP_assword, 
                                                                         configProxy.ntlmWorkStation, 
                                                                         configProxy.ntlmDomain);
                                    break;
                                    
                                case ConfigProxy.PROXY_AUTHENTICATION_TYPE_KERBEROS:
                                    GSSManager gssMgr = GSSManager.getInstance();
                                    //GSSName name = gssMgr.createName(proxyUser, GSSName.NT_USER_NAME);                        
                                    proxyCredentials = new KerberosCredentials(gssMgr.createCredential(GSSCredential.ACCEPT_ONLY));
                                    break;
                            }
                        }
                        
                        proxyAuthScope = new AuthScope(configProxy.proxyHost, configProxy.proxyPort);
                        proxy = new HttpHost(configProxy.proxyHost, configProxy.proxyPort);
                    }
                }
                catch(MalformedURLException murle)
                {
                    Log.log.error(murle.getMessage(), murle);
                }
                catch(GSSException gsse)
                {
                    Log.log.error(gsse.getMessage(), gsse);
                }
            }
        }
    }
    
    protected void setupProxy(DefaultHttpClient client)
    {
        if (proxyCredentials != null && proxyAuthScope != null)
        {
            client.getCredentialsProvider().setCredentials(proxyAuthScope, proxyCredentials );
        }
        
        if (proxy != null)
        {
            client.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
        }
    }
    
    /**
     * Constructor with basic information and user preferences
     * 
     * @param baseUrl: contains protocol (HTTP/HTTPS), host name or ip, port number
     * @param username: used for Basic Authentication or login to get a token, can be blank when token is provided or token is set in the cookie store
     * @param password: used for Basic Authentication or login to get a token, can be blank when token is provided or token is set in the cookie store
     * @param token: used for each REST call by explicitly embedded it with the key name of "token" in HTTPRequest when cookie store is not used.
     *        If the key name is not "token", embed the key and value in the request params. 
     *        Token can also be set in the request headers or request params or in the body payload by the caller based on the server needs.
     *        If the server supports cookies, no token needs to be embedded.
     * @param baseAuth: if true, username and password will be encode with Base64 and embedded into HTTP header for authentication
     * @param timeout: timeout for HTTP/HTTPS connection
     * @param selfSigned: HTTPSURLConnection will be eatablished when server provides a self-signed certificate, no hsst name will be verified
     * @param trustAll: HTTPSURLConnection will be esatblished with fake certificates and even without any certificate is provided
     * @throws Exception
     */
    @Deprecated
    public RestCaller(String baseUrl, String user, String pass, boolean basicAuth, boolean cookieEnabled, boolean selfSigned, boolean trustAll) {
        
        this.baseUrl = baseUrl;
        this.user = user;
        this.pass = pass;
        this.basicAuth = basicAuth;
        this.cookieEnabled = cookieEnabled;
        this.selfSigned = selfSigned;
        this.trustAll = trustAll;
    }

    public String getBaseUrl()
    {
        return baseUrl;
    }

    public String getUrlSufix()
    {
        return urlSuffix;
    }

    public void setUrlSuffix(String urlSuffix)
    {
        if(StringUtils.isNotBlank(urlSuffix))
        {
            this.urlSuffix = urlSuffix.trim();
        }
        else
        {
            this.urlSuffix = urlSuffix;
        }
    }

    public String getHttpbasicauthusername()
    {
        return httpbasicauthusername;
    }

    public void setHttpbasicauthusername(String httpbasicauthusername)
    {
        this.httpbasicauthusername = httpbasicauthusername;
        
        if (StringUtils.isBlank(this.httpbasicauthusername) || StringUtils.isBlank(this.httpbasicauthpassword)) {
        	basicAuth = false;
        } else {
        	basicAuth = true;
        }
    }

    public String getHttpbasicauthpassword()
    {
        return httpbasicauthpassword;
    }

    public void setHttpbasicauthpassword(String httpbasicauthpassword)
    {
        this.httpbasicauthpassword = httpbasicauthpassword;
        
        if (StringUtils.isBlank(this.httpbasicauthusername) || StringUtils.isBlank(this.httpbasicauthpassword)) {
        	basicAuth = false;
        } else {
        	basicAuth = true;
        }
    }

    public int getStatusCode()
    {
        return statusCode;
    }
    
    public String getReasonPhrase()
    {
        return reasonPhrase;
    }
    
    public Map<String, String> getResponseHeaders()
    {
        return responseHeaders;
    }

    public CookieStore getCookieStore()
    {
        return cookieStore;
    }

    public void setCookieStore(CookieStore cookieStore)
    {
        this.cookieStore = cookieStore;
    }
    
    public String getResponseEntity()
    {
        return responseEntity;
    }

    /**
     * Method used to create or update objects in TSRM.
     * 
     * @param params
     * @return
     * @throws Exception
     */
    protected String executeMethod(HttpRequestBase method) throws Exception
    {
        String result = null;

        //Reset response status and headers
        statusCode = -1;
        reasonPhrase = "";
        responseHeaders = new HashMap<String, String>();
        
        // Create an instance of HttpClient.
        DefaultHttpClient client = new DefaultHttpClient();
        setupProxy(client);
        
        try
        {
            if (isSSL)
            {

                ClientConnectionManager clientConnectionManager = client.getConnectionManager();
                // register https protocol in httpclient's scheme registry
                SchemeRegistry schemeRegistry = clientConnectionManager.getSchemeRegistry();
                schemeRegistry.register(new Scheme(protocol, port, sslSocketFactory));
            }

            if(basicAuth) {
            	setHttpBasicAuth(client, method);
            }

            // Execute the method.
            HttpResponse response = client.execute(method);
            if(response.getStatusLine() != null)
            {
                statusCode = response.getStatusLine().getStatusCode();
                reasonPhrase = response.getStatusLine().getReasonPhrase();
            }
            
            Header[] httpRespHeaders = response.getAllHeaders();
            
            if(httpRespHeaders != null && httpRespHeaders.length > 0)
            {
                for(int i = 0; i < httpRespHeaders.length; i++)
                    responseHeaders.put(httpRespHeaders[i].getName(), httpRespHeaders[i].getValue());
            }
            
            HttpEntity entity = response.getEntity();

            if(entity != null)
            {
                // Read the response body.
                result = StringUtils.toString(entity.getContent(), "utf-8");
            }
        }
        catch (IOException e)
        {
            Log.log.error("Fatal transport error:: " + e.getMessage(), e);
           
            throw e;
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        finally
        {
            // Release the connection.
            client.getConnectionManager().shutdown();
        }
        return result;
    }

    protected void setHttpBasicAuth(DefaultHttpClient httpclient, HttpRequestBase method) throws Exception
    {
        if (StringUtils.isNotBlank(httpbasicauthusername) && StringUtils.isNotBlank(httpbasicauthpassword) && method.getURI() != null)
        {
            String user = httpbasicauthusername + ":" + httpbasicauthpassword;
            method.addHeader("Authorization", "Basic " + Base64.encodeBytes(user.getBytes()));
            httpclient.getCredentialsProvider().setCredentials(new AuthScope(method.getURI().getHost(), method.getURI().getPort()), new UsernamePasswordCredentials(httpbasicauthusername, httpbasicauthpassword));
        }
         if(StringUtils.isBlank(httpbasicauthusername) || StringUtils.isBlank(httpbasicauthpassword))
            throw new Exception("User is missing for Basic Authentication.");
         
         if(method.getURI()== null)
             throw new Exception("URI is missing for Basic Authentication.");
    }

    /**
     * Makes HTTP OPTIONS method request on resource (base URL) 
     * to get list of allowed methods.
     * 
     * @return List of allowed methods
     * @throws Exception
     */
    public List<String> optionsMethod() throws Exception
    {
        List<String> allowed = new ArrayList<String>();
        
        // Create method instance
        HttpOptions method = new HttpOptions(baseUrl);
        
        executeMethod(method);
        
        if(statusCode == HttpStatus.SC_OK && responseHeaders.containsKey(HttpHeaders.ALLOW))
        {
            allowed = StringUtils.stringToList(responseHeaders.get(HttpHeaders.ALLOW), ",");
        }
        
        return allowed;
    }
    
    /**
     * Makes HTTP HEAD method request on resource (base URL + suffix)
     * to get metainformation for GET method request.
     *   
     * @return Map of metainformation (name-value pairs)
     * @throws Exception
     */
    public Map<String, String> headMethod() throws Exception
    {
        Map<String, String> metaInfo = new HashMap<String, String>();
        
        // Create method instance
        HttpHead method  = new HttpHead(baseUrl + urlSuffix);
        
        executeMethod(method);
        
        if(statusCode == HttpStatus.SC_OK)
        {
            metaInfo = new HashMap<String, String>(responseHeaders);
        }
        else
        {
            throw new Exception(statusCode + " " + reasonPhrase);
        }
        
        return metaInfo;
    }
    
    /**
     * Makes HTTP GET method request (base URL + suffix) to get
     * resource(s).
     *
     * @param  Id of the resource to get (optional)
     * @param  Map of request parameters and values (optional)
     * @param  Map of request headers (optional - by default Accept is set to application/json)
     * @return resource(s) in "Accept" format
     * @throws Exception
     */
    public String getMethod(String resourceId, Map<String, String> reqParams, Map<String, String> reqHeaders) throws Exception
    {
        //Add resourceId and request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);
        
        if(StringUtils.isNotBlank(resourceId))
        {
            urlBuilder.append(urlBuilder.toString().endsWith("/")? "" : "/");
            urlBuilder.append(StringUtils.utf8Conversion(resourceId));
        }
        
        if(reqParams != null && !reqParams.isEmpty())
        {
            urlBuilder.append("?").append(StringUtils.mapToUrl(reqParams));
        }
        
        // Create method instance
        HttpGet method  = new HttpGet(urlBuilder.toString());
        
        //Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();
        
        for(String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }
        
        /*
         * Accept defaults to application/json and Cache-Control to no-cache, if not specified
         */
        
        method.setHeader(HttpHeaders.ACCEPT, StringUtils.getParam(tReqHeaders, HttpHeaders.ACCEPT, "application/json"));
        method.setHeader(HttpHeaders.CACHE_CONTROL, StringUtils.getParam(tReqHeaders, HttpHeaders.CACHE_CONTROL, "no-cache"));
        
        String entity = executeMethod(method);
        
        if(statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_NOT_FOUND)
        {
            throw new RestCallException(statusCode, reasonPhrase, entity);
        }
        
        return entity;
    }
    
    /**
     * Makes HTTP GET method request (base URL + suffix) to get
     * resource(s).
     *
     * @param  Id of the resource to get (optional)
     * @param  URL parameters (optional)
     * @param  Map of request headers (optional - by default Accept is set to application/json)
     * @return resource(s) in "Accept" format
     * @throws Exception
     */
    public String getMethod(String resourceId, String urlParams, Map<String, String> reqHeaders) throws Exception
    {
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);
        
        if(StringUtils.isNotBlank(resourceId))
        {
            urlBuilder.append(urlBuilder.toString().endsWith("/")? "" : "/");
            urlBuilder.append(StringUtils.utf8Conversion(resourceId));
        }
        
        if(StringUtils.isNotBlank(urlParams))
        {
            urlBuilder.append("?").append(urlParams);
        }
        
        // Create method instance
        HttpGet method  = new HttpGet(urlBuilder.toString());
        
        //Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();
        
        for(String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }
        
        /*
         * Accept defaults to application/json and Cache-Control to no-cache, if not specified
         */
        
        method.setHeader(HttpHeaders.ACCEPT, StringUtils.getParam(tReqHeaders, HttpHeaders.ACCEPT, "application/json"));
        method.setHeader(HttpHeaders.CACHE_CONTROL, StringUtils.getParam(tReqHeaders, HttpHeaders.CACHE_CONTROL, "no-cache"));
        
        String entity = executeMethod(method);
        
        if(statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_NOT_FOUND)
        {
            throw new Exception(statusCode + " " + reasonPhrase + ":" + entity);
        }
        
        return entity;
    }
    
    /**
     * Makes HTTP GET method request to specified URL.
     *
     * @param  url
     * @param  Map of request headers (optional - by default Accept is set to application/json) 
     * @throws Exception
     */
    public String getMethod(String url, Map<String, String> reqHeaders) throws Exception
    {
        // Create method instance
        HttpGet method  = new HttpGet(url);
        
      //Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();
        
        for(String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }
        
        /*
         * Accept defaults to application/json and Cache-Control to no-cache, if not specified
         */
        
        method.setHeader(HttpHeaders.ACCEPT, StringUtils.getParam(tReqHeaders, HttpHeaders.ACCEPT, "application/json"));
        method.setHeader(HttpHeaders.CACHE_CONTROL, StringUtils.getParam(tReqHeaders, HttpHeaders.CACHE_CONTROL, "no-cache"));
        
        String entity = executeMethod(method);
        
        if(statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_NOT_FOUND && statusCode != HttpStatus.SC_NO_CONTENT)
        {
            throw new Exception(statusCode + " " + reasonPhrase + ":" + entity);
        }
        
        return entity;
    }
    
    /**
     * Makes HTTP POST method request (base URL + suffix) to create resource.
     *
     * @param  Map of request parameters and values (optional)
     * @param  Map of request headers (optional - Content-Type and Accept is forced to application/json)
     * @param  Map of resource properties in name-value format (optional)
     * @return Newly created resource. Resource location is in "Location" (HTTPHeaders.LOCATION) response header.
     * @throws Exception
     */
    public String postMethod(Map<String, String> reqParams, Map<String, String> reqHeaders, Map<String, String> resourceProps) throws Exception
    {
        
        
        //Add request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);
                
        if(reqParams != null && !reqParams.isEmpty())
        {
            urlBuilder.append("?").append(StringUtils.mapToUrl(reqParams));
        }
        
        // Create method instance
        HttpPost method  = new HttpPost(urlBuilder.toString());
        
        //Content-Type and Accept is forced to application/json
        method.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        method.setHeader(HttpHeaders.ACCEPT, "application/json");
        
        //Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();
        
        for(String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }
        
        if(resourceProps != null && !resourceProps.isEmpty())
        {
            //Set entity
            HttpEntity entity = new ByteArrayEntity(StringUtils.mapToJson(resourceProps).getBytes("utf-8"));
            method.setEntity(entity);
        }
        
        String responseEntity = executeMethod(method);
        
        if(statusCode != HttpStatus.SC_CREATED)
        {
            throw new RestCallException(statusCode, reasonPhrase, responseEntity);
            //throw new Exception(statusCode + " " + reasonPhrase + ":" + StringUtils.mapToJson(resourceProps));
        }
        
        return responseEntity;
    }
    
    
    /**
     * Makes HTTP POST method request (base URL + suffix) to create resource.
     *
     * @param  Map of request parameters and values (optional)
     * @param  Map of request headers (optional - Content-Type and Accept is forced to application/json)
     * @param  Map of List in name-value format to set in the body of request as Urlencoded form data(optional)
     * @return Newly created resource. Resource location is in "Location" (HTTPHeaders.LOCATION) response header.
     * @throws Exception
     */
    public String callPost(Map<String, String> reqParams, Map<String, String> reqHeaders, List<NameValuePair> urlEncodedFormData,Collection<Integer> statusCodes) throws Exception
    {
        
        
        //Add request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);
                
        if(reqParams != null && !reqParams.isEmpty())
        {
            urlBuilder.append("?").append(StringUtils.mapToUrl(reqParams));
        }
        
        // Create method instance
        HttpPost method  = new HttpPost(urlBuilder.toString());
        
        //Content-Type and Accept is forced to application/json
        method.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        method.setHeader(HttpHeaders.ACCEPT, "application/json");
        
        //Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();
        
        for(String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }
        
        if(urlEncodedFormData != null && !urlEncodedFormData.isEmpty())
        {
            //Set entity
           UrlEncodedFormEntity entity = new UrlEncodedFormEntity(urlEncodedFormData, "UTF-8");
            method.setEntity(entity);
         }
        
        String responseEntity = executeMethod(method);
        
        Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
        expectedStatusCodes.add(new Integer(HttpStatus.SC_CREATED));
        
        if (statusCodes != null && !statusCodes.isEmpty())
        {
            for (Integer statusCode : statusCodes)
            {
                expectedStatusCodes.add(statusCode);
            }
        }
        
        boolean validStatus = false;
        
        for (Integer expectedStatusCode : expectedStatusCodes)
        {
            if (statusCode == expectedStatusCode.intValue())
            {
                validStatus = true;
                break;
            }
        }
        
        if(!validStatus)
        {
            throw new RestCallException(statusCode, reasonPhrase, responseEntity);
            //throw new Exception(statusCode + " " + reasonPhrase + ":" + StringUtils.mapToJson(resourceProps));
        }

       
        
        return responseEntity;
    }
    
    /**
     * Makes HTTP POST method request (base URL + suffix) to create resource.
     *
     * @param  Map of request parameters and values (optional)
     * @param  Map of request headers (optional - Accept forced to application/json, Content Type is forced to application/json if not specified)
     * @param  Content
     * @param  Collection of possible status codes (optional HTTP Status Code 201 is default)
     * @return Newly created resource. Resource location is in "Location" (HTTPHeaders.LOCATION) response header.
     * @throws Exception
     */
    public String postMethod(Map<String, String> reqParams, Map<String, String> reqHeaders, String content, Collection<Integer> statusCodes) throws Exception
    {
        //Add request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);
                
        if(reqParams != null && !reqParams.isEmpty())
        {
            urlBuilder.append("?").append(StringUtils.mapToUrl(reqParams));
        }
        
        // Create method instance
        HttpPost method  = new HttpPost(urlBuilder.toString());
        
        //Accept is forced to application/json
        method.setHeader(HttpHeaders.ACCEPT, "application/json");
        
        //Set request headers - Content Type should be in request headers, if not defaults to application/json
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();
        
        for(String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }
        
        Header[] contentTypes = method.getHeaders(HttpHeaders.CONTENT_TYPE);
        
        if (contentTypes == null || contentTypes.length == 0)
        {
            method.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        }
        
        if(StringUtils.isNotBlank(content))
        {
            //Set entity
            HttpEntity entity = new ByteArrayEntity(content.getBytes("utf-8"));
            method.setEntity(entity);
        }
        
        String responseEntity = executeMethod(method);
        
        Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
        expectedStatusCodes.add(new Integer(HttpStatus.SC_CREATED));
        
        if (statusCodes != null && !statusCodes.isEmpty())
        {
            for (Integer statusCode : statusCodes)
            {
                expectedStatusCodes.add(statusCode);
            }
        }
        
        boolean validStatus = false;
        
        for (Integer expectedStatusCode : expectedStatusCodes)
        {
            if (statusCode == expectedStatusCode.intValue())
            {
                validStatus = true;
                break;
            }
        }
        
        if(!validStatus)
        {
            throw new RestCallException(statusCode, reasonPhrase, responseEntity);
            //throw new Exception(statusCode + " " + reasonPhrase + ":" + StringUtils.mapToJson(resourceProps));
        }
        
        return responseEntity;
    }
    
    /**
     * Makes HTTP PUT method request (base URL + suffix) to update specified resource.
     *
     * @param  Id of the resource to update
     * @param  Map of request parameters and values (optional)
     * @param  Map of request headers (optional - Content-Type and Accept is forced to application/json)
     * @param  Map of resource properties to update in name-value format
     * @return Updated resource.
     * @throws Exception
     */
    public String putMethod(String resourceId, Map<String, String> reqParams, Map<String, String> reqHeaders, Map<String, String> resourceProps) throws Exception
    {
        if(resourceProps == null || resourceProps.isEmpty())
        {
            throw new Exception("Invalid arguments: Resource Id and/or Resource Properties to update are null or empty.");
        }
        
        //Add request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);
        
       if(! resourceId.isEmpty()) {
           urlBuilder.append(urlBuilder.toString().endsWith("/")? "" : "/");
           urlBuilder.append(StringUtils.utf8Conversion(resourceId));   
       }
        
        
        if(reqParams != null && !reqParams.isEmpty())
        {
            urlBuilder.append("?").append(StringUtils.mapToUrl(reqParams));
        }
        
        // Create method instance
        HttpPut method  = new HttpPut(urlBuilder.toString());
        
        //Content-Type and Accept is forced to application/json
        method.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        method.setHeader(HttpHeaders.ACCEPT, "application/json");
        
        //Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();
        
        for(String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }
        
        //Set entity
        HttpEntity entity = new ByteArrayEntity(StringUtils.mapToJson(resourceProps).getBytes("utf-8"));
        method.setEntity(entity);
        //sometime you receive status code as 401 and responseEntity  as e.g {"errorMessages":["You do not have the permission to see the specified issue.","Login Required"],"errors":{}}
        //in that case it thorws exception and looses the actual message receved in response entity. So adding one filed to RestCaller to hold value of  response entity
        responseEntity = executeMethod(method);
        if(statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_NO_CONTENT)
        {
            throw new Exception(statusCode + " " + reasonPhrase + ":" + (StringUtils.mapToJson(resourceProps)));
        }
        
        return responseEntity;
    }
    
    /**
     * Makes HTTP PUT method request (base URL + suffix) to update specified resource.
     *
     * @param  Id of the resource to update
     * @param  Map of paired request parameters and values (optional)
     * @param  Key prefix for paired map
     * @param  Value prefix for paired map
     * @param  Map of request parameters and values (optional)
     * @param  Map of request headers (optional - Content-Type and Accept is forced to application/json)
     * @param  Map of resource properties to update in name-value format
     * @return Updated resource.
     * @throws Exception
     */
    public String putMethod(String resourceId, Map<String, String> pairedReqParams, String keyPrefix, String valuePrefix, Map<String, String> reqParams, Map<String, String> reqHeaders, Map<String, String> resourceProps) throws Exception
    {
        if(resourceId == null || resourceId.isEmpty() || 
           ((pairedReqParams == null || pairedReqParams.isEmpty()) && 
            (resourceProps == null || resourceProps.isEmpty())))
        {
            throw new Exception("Invalid arguments: Resource Id and/or Resource Properties to update or " +
                                "Paired Request Parameters specifying resource properties to update are null or empty.");
        }
        
        //Add request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);
        
        urlBuilder.append(urlBuilder.toString().endsWith("/")? "" : "/");
        urlBuilder.append(StringUtils.utf8Conversion(resourceId));
        
        boolean urlHasParams = false;
        
        if(pairedReqParams != null && !pairedReqParams.isEmpty())
        {
            urlHasParams = true;
            urlBuilder.append("?").append(StringUtils.pairedMapToUrl(pairedReqParams, keyPrefix, valuePrefix));
        }
        
        if(reqParams != null && !reqParams.isEmpty())
        {
            String separator = "?";
            
            if (urlHasParams)
            {
                separator = "&";
            }

            urlBuilder.append(separator).append(StringUtils.mapToUrl(reqParams));
        }
        
        // Create method instance
        HttpPut method  = new HttpPut(urlBuilder.toString());
        
        //Content-Type and Accept is forced to application/json
        method.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        method.setHeader(HttpHeaders.ACCEPT, "application/json");
        
        //Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();
        
        for(String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }
        
        if (resourceProps != null && !resourceProps.isEmpty())
        {
            //Set entity
            HttpEntity entity = new ByteArrayEntity(StringUtils.mapToJson(resourceProps).getBytes("utf-8"));
            method.setEntity(entity);
        }
        
        String responseEntity = executeMethod(method);
        this.responseEntity =responseEntity;
        
        if(statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_NO_CONTENT)
        {
            throw new Exception(statusCode + " " + reasonPhrase + ":" + 
                                (pairedReqParams != null && !pairedReqParams.isEmpty() ? " Paired Request Parameters " + pairedReqParams + ", " : "") + 
                                (resourceProps != null && !resourceProps.isEmpty() ? " Resource Properties " + StringUtils.mapToJson(resourceProps) : ""));
        }
        
        return responseEntity;
    }
    
    /**
     * Makes HTTP PATCH method request (base URL + suffix) to patch(update) specified resource.
     *
     * @param  Id of the resource to patch(update)
     * @param  Map of request parameters and values (optional)
     * @param  Map of request headers (optional - Content-Type and Accept is forced to application/json)
     * @param  Map of resource properties to update in name-value format
     * @param  Key prefix for paired map
     * @param  Value prefix for paired map
     * @return Updated resource.
     * @throws Exception
     */
    public String patchMethod(String resourceId, Map<String, String> reqParams, Map<String, String> reqHeaders, Map<String, String> resourceProps) throws Exception
    {
        if(resourceId == null || resourceId.isEmpty() || resourceProps == null || resourceProps.isEmpty())
        {
            throw new Exception("Invalid arguments: Resource Id and/or Resource Properties to patch(update) are null or empty.");
        }
        
        //Add resource id and request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);
        
        urlBuilder.append(urlBuilder.toString().endsWith("/")? "" : "/");
        urlBuilder.append(StringUtils.utf8Conversion(resourceId));
        
        if(reqParams != null && !reqParams.isEmpty())
        {
            urlBuilder.append("?").append(StringUtils.mapToUrl(reqParams));
        }
        
        // Create method instance
        //HttpPatch method  = new HttpPatch(urlBuilder.toString()); // Uncomment when http client library version supports PATCH
        HttpPut method  = new HttpPut(urlBuilder.toString()); // Comment when http client library version supports PATCH
        
        //Content-Type and Accept is forced to application/json
        method.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        method.setHeader(HttpHeaders.ACCEPT, "application/json");
        
        //Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();
        
        for(String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }
        
        //Set entity
        HttpEntity entity = new ByteArrayEntity(StringUtils.mapToJson(resourceProps).getBytes("utf-8"));
        method.setEntity(entity);
        
        String responseEntity = executeMethod(method);
        
        if(statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_NO_CONTENT)
        {
            throw new Exception(statusCode + " " + reasonPhrase + ":" + StringUtils.mapToJson(resourceProps));
        }
        
        return responseEntity;
    }
    
    /**
     * Makes HTTP DELETE method request (base URL + suffix) to patch(update) specified resource.
     *
     * @param  Id of the resource to patch(update)
     * @param  Map of request parameters and values (optional)
     * @param  Map of request headers (optional - Content-Type and Accept is forced to application/json)
     * @param  Map of resource properties to update in name-value format
     * @return Updated resource.
     * @throws Exception
     */
    public String deleteMethod(String resourceId) throws Exception
    {
        if(resourceId == null || resourceId.isEmpty())
        {
            throw new Exception("Invalid arguments: Resource Id to delete is null or empty.");
        }
        
        //Add resource id to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);
        urlBuilder.append(urlBuilder.toString().endsWith("/")? "" : "/");
        
        urlBuilder.append(StringUtils.utf8Conversion(resourceId));
        
        // Create method instance
        HttpDelete method  = new HttpDelete(urlBuilder.toString());
        
        //Content-Type and Accept is forced to application/json
        method.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        method.setHeader(HttpHeaders.ACCEPT, "application/json");
        
        String responseEntity = executeMethod(method);
        
        if(statusCode != HttpStatus.SC_OK  && statusCode != HttpStatus.SC_NO_CONTENT)
        {
            throw new RestCallException(statusCode, reasonPhrase , resourceId + ":" + responseEntity);
        }
        
        return responseEntity;
    }
    
    /**
     * Makes HTTP GET method request (base URL + suffix) to get
     * resource(s).
     *
     * @param  Id of the resource to get (optional)
     * @param  Map of paired request parameters and values (optional)
     * @param  Key prefix for paired map
     * @param  Value prefix for paired map
     * @param  Map of request parameters and values (optional)
     * @param  Map of request headers (optional - by default Accept is set to application/json)
     * @return resource(s) in "Accept" format
     * @throws Exception
     */
    public String getMethod(String resourceId, Map<String, String> pairedReqParams, String keyPrefix, String valuePrefix, Map<String, String> reqParams, Map<String, String> reqHeaders) throws Exception
    {
        //Add resourceId and request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);
        
        if(StringUtils.isNotBlank(resourceId))
        {
            urlBuilder.append(urlBuilder.toString().endsWith("/")? "" : "/");
            urlBuilder.append(StringUtils.utf8Conversion(resourceId));
        }
        
        boolean urlHasParams = false;
        
        if(pairedReqParams != null && !pairedReqParams.isEmpty())
        {
            urlHasParams = true;
            urlBuilder.append("?").append(StringUtils.pairedMapToUrl(pairedReqParams, keyPrefix, valuePrefix));
        }
        
        if(reqParams != null && !reqParams.isEmpty())
        {
            String separator = "?";
            
            if (urlHasParams)
            {
                separator = "&";
            }

            urlBuilder.append(separator).append(StringUtils.mapToUrl(reqParams));
        }
        
        // Create method instance
        HttpGet method  = new HttpGet(urlBuilder.toString());
        
        //Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();
        
        for(String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }
        
        /*
         * Accept defaults to application/json and Cache-Control to no-cache, if not specified
         */
        
        method.setHeader(HttpHeaders.ACCEPT, StringUtils.getParam(tReqHeaders, HttpHeaders.ACCEPT, "application/json"));
        method.setHeader(HttpHeaders.CACHE_CONTROL, StringUtils.getParam(tReqHeaders, HttpHeaders.CACHE_CONTROL, "no-cache"));
        
        String entity = executeMethod(method);
        
        if(statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_NOT_FOUND)
        {
            throw new RestCallException(statusCode, reasonPhrase, entity);
        }
        
        return entity;
    }
    
    /**
     * Makes HTTP GET method request (base URL + suffix) to get
     * resource(s).
     *
     * @param  Id of the resource to get (optional)
     * @param  List of request values (optional)
     * @param  Key prefix for values
     * @param  Map of request parameters and list of values (optional)
     * @param  Map of request headers (optional - by default Accept is set to application/json)
     * @return resource(s) in "Accept" format
     * @throws Exception
     */
    public String getMethod(String resourceId, List<String> reqValues, String key, Map<String, List<String>> reqParams, Map<String, String> reqHeaders) throws Exception
    {
        //Add resourceId and request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);
        
        if(StringUtils.isNotBlank(resourceId))
        {
            urlBuilder.append(urlBuilder.toString().endsWith("/")? "" : "/");
            urlBuilder.append(StringUtils.utf8Conversion(resourceId));
        }
        
        boolean urlHasParams = false;
        
        if(CollectionUtils.isNotEmpty(reqValues))
        {
            urlHasParams = true;
            urlBuilder.append("?").append(StringUtils.listToUrl(reqValues, key));
        }
        
        if(reqParams != null && !reqParams.isEmpty())
        {
            for (String reqParamKey : reqParams.keySet())
            {
                for (String reqParamVal : reqParams.get(reqParamKey))
                {
                    String separator = urlHasParams ? "&" : "?";
                    Map<String, String> reqParamMap = new HashMap<String, String>();
                    reqParamMap.put(reqParamKey, reqParamVal);
                    urlBuilder.append(separator).append(StringUtils.mapToUrl(reqParamMap));
                    urlHasParams = true;
                }
            }
        }
        
        // Create method instance
        HttpGet method  = new HttpGet(urlBuilder.toString());
        
        //Set request headers
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();
        
        for(String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }
        
        /*
         * Accept defaults to application/json and Cache-Control to no-cache, if not specified
         */
        
        method.setHeader(HttpHeaders.ACCEPT, StringUtils.getParam(tReqHeaders, HttpHeaders.ACCEPT, "application/json"));
        method.setHeader(HttpHeaders.CACHE_CONTROL, StringUtils.getParam(tReqHeaders, HttpHeaders.CACHE_CONTROL, "no-cache"));
        
        String entity = executeMethod(method);
        
        if(statusCode != HttpStatus.SC_OK && statusCode != HttpStatus.SC_NOT_FOUND)
        {
            throw new RestCallException(statusCode, reasonPhrase, entity);
        }
        
        return entity;
    }
    
    /**
     * Makes HTTP POST method request (base URL + suffix) to create resource.
     *
     * @param  Map of request parameters and values (optional)
     * @param  Map of request headers (optional - Accept forced to application/json, Content Type is forced to application/json if not specified)
     * @param  Content
     * @param  Map of paired name value request parameters
     * @param  keyPrefix
     * @param  valuePrefix
     * @param  Collection of possible status codes (optional HTTP Status Code 201 is default)
     * @return Newly created resource. Resource location is in "Location" (HTTPHeaders.LOCATION) response header.
     * @throws Exception
     */
    public String postMethod(Map<String, String> reqParams, Map<String, String> reqHeaders, String content, Map<String, String> pairedReqParams, String keyPrefix, String valuePrefix, Collection<Integer> statusCodes) throws Exception
    {
        //Add request paremeters to URL
        StringBuilder urlBuilder = new StringBuilder(baseUrl + urlSuffix);
        
        boolean hasReqParams = false;
        
        if(reqParams != null && !reqParams.isEmpty())
        {
            urlBuilder.append("?").append(StringUtils.mapToUrl(reqParams));
            hasReqParams = true;
        }
        
        if (pairedReqParams != null && !pairedReqParams.isEmpty())
        {
            String separator = hasReqParams ? "&" : "?";            
            urlBuilder.append(separator).append(StringUtils.pairedMapToUrl(pairedReqParams, keyPrefix, valuePrefix));
        }
        // Create method instance
        HttpPost method  = new HttpPost(urlBuilder.toString());
        
        //Accept is forced to application/json
        method.setHeader(HttpHeaders.ACCEPT, "application/json");
        
        //Set request headers - Content Type should be in request headers, if not defaults to application/json
        Map<String, String> tReqHeaders = reqHeaders != null ? reqHeaders : new HashMap<String, String>();
        
        for(String reqHeaderName : tReqHeaders.keySet())
        {
            method.setHeader(reqHeaderName, StringUtils.getParam(tReqHeaders, reqHeaderName));
        }
        
        Header[] contentTypes = method.getHeaders(HttpHeaders.CONTENT_TYPE);
        
        if (contentTypes == null || contentTypes.length == 0)
        {
            method.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        }
        
        if(StringUtils.isNotBlank(content))
        {
            //Set entity
            HttpEntity entity = new ByteArrayEntity(content.getBytes("utf-8"));
            method.setEntity(entity);
        }
        
        String responseEntity = executeMethod(method);
        
        Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
        expectedStatusCodes.add(new Integer(HttpStatus.SC_CREATED));
        
        if (statusCodes != null && !statusCodes.isEmpty())
        {
            for (Integer statusCode : statusCodes)
            {
                expectedStatusCodes.add(statusCode);
            }
        }
        
        boolean validStatus = false;
        
        for (Integer expectedStatusCode : expectedStatusCodes)
        {
            if (statusCode == expectedStatusCode.intValue())
            {
                validStatus = true;
                break;
            }
        }
        
        if(!validStatus)
        {
            throw new RestCallException(statusCode, reasonPhrase, responseEntity);
            //throw new Exception(statusCode + " " + reasonPhrase + ":" + StringUtils.mapToJson(resourceProps));
        }
        
        return responseEntity;
    }
    
    /**
     * Call REST API with the uri defined by the server to get the token back.
     * The user credential can be embeded in HTTP header with Basic Authentication or in the request params or in the payload body based on the server configuration.
     * 
     * @param uri: REST interface/path defined by the server
     * @param body: data in the format of the content type that will be encoded with UTF-8 before sending
     * @param headers: any header properties caller needs to add, such as "Cache-Control": "no-cache"
     * @param contentType: tell the server about the content type encoding for the HTTP request, such as text/xml, application/xml, application/json, etc.
     * @param accept: tell the server what content type can be accepted, such as text/xml, application/xml, application/json
     * @return: a response string with token embedded
     * @throws Exception
     */
    public String login(String uri, String method, Map<String, String> params, String body, Map<String, String>headers, String contentType, String accept) throws Exception {
        
        if(StringUtils.isBlank(method))
            throw new Exception("Method cannot be blank.");

        String response = "";
        
        // Embed the user credential in params
        if(method.equalsIgnoreCase("GET"))
            response = callGet(uri, params, headers, accept);
        
        else if(method.equalsIgnoreCase("POST"))
            response = callPost(uri, params, body, headers, contentType, accept);
        
        else
            throw new Exception("Use GET or POST method for login.");
        
        // The token must be parsed by caller based on the format of the response.
        return response;
    }
    
    /**
     * Call REST API using HTTP GET method.
     * If token value is not empty, token will be embedded as key of "token" for each REST call.
     * If basicAuth is true, Basic Authentication will be used for each REST call.
     * If the method call fails with token expired or unauthorized, caller needs to catch the exception and call login again to get a new token.
     * 
     * @param uri: REST interface/path defined by the server
     * @param params: the query key/value pairs to send to the server
     * @param headers: any header properties caller needs to add, such as "Cache-Control": "no-cache"
     * @param accept: tell the server what content type can be accepted, such as text/xml, application/xml, application/json
     * @return: the response is string or binary
     * @throws Exception
     */
    @Deprecated
    public String callGet(String uri, Map<String, String> params, Map<String, String>headers, String accept) throws Exception {
        
        if(headers == null)
            headers = new HashMap<String, String>();
        
        if(StringUtils.isNotEmpty(accept))
            headers.put("Accept", accept);
        
        return callGet(uri, params, headers);
    }
    
    public String callGet(String uri, Map<String, String> params, Map<String, String>headers) throws Exception {

        HttpURLConnection conn = null;
        InputStream input = null;
        InputStream error = null;
        BufferedReader bufferedReader = null;
        StringBuilder path = new StringBuilder();
        StringBuilder response = new StringBuilder();

        if(StringUtils.isNotBlank(uri))
            path.append(uri);

        String url = "";
        
        if(MapUtils.isNotEmpty(params)) {
            if(uri.indexOf("?") != -1)
                path.append("&");
            else
                path.append("?");
            
            for(Iterator<String> it=params.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                String value = params.get(key);
                if(value == null)
                    continue;
                path.append(key).append("=").append(URLEncoder.encode(value, StandardCharsets.UTF_8.name())).append("&");
            }
            
            url = path.toString();
            url = url.substring(0, url.length()-1);
        } else {
            url = path.toString();
        }
        
        try {
            conn = getConnection(url, headers);
            
            conn.setRequestMethod("GET");
            
            if(basicAuth)
                doBasicAuthentication(conn);

            conn.setDoInput(true);
            
            // Get response code
            statusCode = conn.getResponseCode();
            
            // Get Response message
            reasonPhrase = conn.getResponseMessage();
            
            // Get response headers
            Map<String, List<String>> headerFields = conn.getHeaderFields();
            
            String domain = conn.getURL().getHost();
            
            for(Iterator<String> it=headerFields.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                
                if(StringUtils.isBlank(key))
                    continue;
                
                if(!key.equalsIgnoreCase(SET_COOKIE)) {
                    List<String> values = headerFields.get(key);
                    StringBuilder sb = new StringBuilder();
                    
                    if(values != null && values.size() > 0) {                
                        int size = values.size();
                        
                        for(int i=0; i<size; i++) {

                            String value = values.get(i);
                            sb.append(value);
                            if(i != size-1)
                                sb.append(",");
                        }
                    }
                    
                    responseHeaders.put(key, sb.toString());
                }

                else {
                    List<String> headerValues = headerFields.get(key);
                    
                    StringTokenizer st = new StringTokenizer(headerValues.get(0), COOKIE_VALUE_DELIMITER);

                    HttpCookie cookie = null;
                    
                    if (st.hasMoreTokens()) {
                        String token  = st.nextToken();
                        String name = token.substring(0, token.indexOf(NAME_VALUE_SEPARATOR));
                        String value = token.substring(token.indexOf(NAME_VALUE_SEPARATOR) + 1, token.length());
                        cookie = new HttpCookie(name, value);
                
                        while (st.hasMoreTokens()) {
                            token  = st.nextToken();
                            if(StringUtils.isBlank(token))
                                continue;
            
                            int index = token.indexOf(NAME_VALUE_SEPARATOR);
                            name = token;
                            value = "";
                            
                            if(index > 0) {
                                name = token.substring(0, index).trim();
                                value = token.substring(index + 1, token.length());
            
                                if(name.equalsIgnoreCase(PATH))
                                    cookie.setPath(value);
                            }
                            
                            else
                                cookie.setComment(name);
                        }
            
                        cookie.setDomain(domain);
                        cookieStore.add(new URI(domain), cookie);
                    }
                }
            }
            
            input = conn.getInputStream();

            bufferedReader = new BufferedReader(new InputStreamReader(input));

            String line = null;
            while ((line = bufferedReader.readLine()) != null)
                response.append(line).append(lineSeparator);
            
//            conn.getContent();
        } catch(Exception e) {
//            e.printStackTrace();
            if(conn == null)
                throw e;
            
            statusCode = conn.getResponseCode();
            reasonPhrase = conn.getResponseMessage();
            Log.log.error("Response code is: " + statusCode);
            Log.log.error("Response message is: " + reasonPhrase);

            String errorMsg = "";
            
            try {
                error = conn.getErrorStream();

                if(bufferedReader != null)
                    bufferedReader.close();
                
                bufferedReader = new BufferedReader(new InputStreamReader(error));
                
                String line = null;
                StringBuilder sb = new StringBuilder();
                while ((line = bufferedReader.readLine()) != null)
                    sb.append(line);
                
                errorMsg = sb.toString();
            } catch(Exception eee) {
                Log.log.debug(eee.getMessage(), eee);
            }
            
            String message = e.getMessage();
            if(StringUtils.isNotEmpty(message))
                message = message + lineSeparator + errorMsg;
            else
                message = errorMsg;
            
            if(StringUtils.isEmpty(message))
                message = "";
            
            Exception ee = new Exception(message);
            ee.setStackTrace(e.getStackTrace());
            
            Log.log.error(ee.getMessage(), ee);
            throw ee;
        } finally {
            try {
                if(input != null)
                    input.close();
                if(error != null)
                    error.close();
                if(bufferedReader != null)
                    bufferedReader.close();
                if(conn != null)
                    conn.disconnect();
            } catch(Exception ee) {}
        }
        
        return response.toString();
    }
    
    @Deprecated
    public byte[] callGetBinary(String uri, Map<String, String> params, Map<String, String>headers, String accept) throws Exception {
        
        if(headers == null)
            headers = new HashMap<String, String>();
        
        if(StringUtils.isNotEmpty(accept))
            headers.put("Accept", accept);
        
        return callGetBinary(uri, params, headers);
    }
    
    public byte[] callGetBinary(String uri, Map<String, String> params, Map<String, String>headers) throws Exception {
        
        HttpURLConnection conn = null;
        InputStream input = null;
        InputStream error = null;
        BufferedReader bufferedReader = null;
        StringBuilder path = new StringBuilder();
        byte[] response = null;

        if(StringUtils.isNotBlank(uri))
            path.append(uri);

        String url = "";
        
        if(params != null && params.size() != 0) {
            if(uri.indexOf("?") != -1)
                path.append("&");
            else
                path.append("?");
            
            for(Iterator<String> it=params.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                String value = params.get(key);
                if(value == null)
                    continue;
                path.append(key).append("=").append(URLEncoder.encode(value, StandardCharsets.UTF_8.name())).append("&");
            }
            
            url = path.toString();
            url = url.substring(0, url.length()-1);
        }
        
        else
            url = path.toString();
        
        try {
            conn = getConnection(url, headers);
            
            conn.setRequestMethod("GET");
            
            if(basicAuth)
                doBasicAuthentication(conn);

            conn.setDoInput(true);
            
            // Get response code
            statusCode = conn.getResponseCode();
            
            // Get Response message
            reasonPhrase = conn.getResponseMessage();
            
            // Get response headers
            Map<String, List<String>> headerFields = conn.getHeaderFields();
            
            String domain = conn.getURL().getHost();
            
            for(Iterator<String> it=headerFields.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                
                if(StringUtils.isBlank(key))
                    continue;
                
                if(!key.equalsIgnoreCase(SET_COOKIE)) {
                    List<String> values = headerFields.get(key);
                    StringBuilder sb = new StringBuilder();
                    
                    if(values != null && values.size() > 0) {                
                        int size = values.size();
                        
                        for(int i=0; i<size; i++) {

                            String value = values.get(i);
                            sb.append(value);
                            if(i != size-1)
                                sb.append(",");
                        }
                    }
                    
                    responseHeaders.put(key, sb.toString());
                }

                else {
                    List<String> headerValues = headerFields.get(key);
                    
                    StringTokenizer st = new StringTokenizer(headerValues.get(0), COOKIE_VALUE_DELIMITER);

                    HttpCookie cookie = null;
                    
                    if (st.hasMoreTokens()) {
                        String token  = st.nextToken();
                        String name = token.substring(0, token.indexOf(NAME_VALUE_SEPARATOR));
                        String value = token.substring(token.indexOf(NAME_VALUE_SEPARATOR) + 1, token.length());
                        cookie = new HttpCookie(name, value);
                
                        while (st.hasMoreTokens()) {
                            token  = st.nextToken();
                            if(StringUtils.isBlank(token))
                                continue;
    
                            int index = token.indexOf(NAME_VALUE_SEPARATOR);
                            name = token;
                            value = "";
                            
                            if(index > 0) {
                                name = token.substring(0, index).trim();
                                value = token.substring(index + 1, token.length());
    
                                if(name.equalsIgnoreCase(PATH))
                                    cookie.setPath(value);
                            }
                            
                            else
                                cookie.setComment(name);
                        }
    
                        cookie.setDomain(domain);
                        cookieStore.add(new URI(domain), cookie);
                    }
                }
            }
            
            input = conn.getInputStream();

            byte[] buffer = new byte[1024];
  
            ByteArrayOutputStream output = new ByteArrayOutputStream();

            int line = 0;
            while ((line = input.read(buffer)) != -1) {
                output.write(buffer, 0, line);
            }
            
            output.flush();
            output.close();
            response = output.toByteArray();
            
//            conn.getContent();
        } catch(Exception e) {
//            e.printStackTrace();
            statusCode = conn.getResponseCode();
            reasonPhrase = conn.getResponseMessage();
            Log.log.error("Response code is: " + statusCode);
            Log.log.error("Response message is: " + reasonPhrase);

            String errorMsg = "";
            
            try {
                error = conn.getErrorStream();

                if(bufferedReader != null)
                    bufferedReader.close();
                
                bufferedReader = new BufferedReader(new InputStreamReader(error));
                
                String line = null;
                StringBuilder sb = new StringBuilder();
                while ((line = bufferedReader.readLine()) != null)
                    sb.append(line);
                
                errorMsg = sb.toString();
            } catch(Exception eee) {
                Log.log.debug(eee.getMessage(), eee);
            }
            
            String message = e.getMessage();
            if(StringUtils.isNotEmpty(message))
                message = message + lineSeparator + errorMsg;
            else
                message = errorMsg;
            
            if(StringUtils.isEmpty(message))
                message = "";
            
            Exception ee = new Exception(message);
            ee.setStackTrace(e.getStackTrace());
            
            Log.log.error(ee.getMessage(), ee);
            throw ee;
        } finally {
            try {
                if(input != null)
                    input.close();
                if(error != null)
                    error.close();
                if(bufferedReader != null)
                    bufferedReader.close();
                if(conn != null)
                    conn.disconnect();
            } catch(Exception ee) {}
        }
        
        return response;
    }
    
    /**
     * Call REST API using HTTP POST method.
     * If token value is not empty, token will be embedded as key of "token" for each REST call.
     * If basicAuth is true, Basic Authentication will be used for each REST call.
     * If the method call fails with token expired or unauthorized, caller needs to catch the exception and call login again to get a new token.
     * 
     * @param uri: REST interface/path defined by the server
     * @param params: the query key/value pairs to send to the server
     * @param body: data encoded in the format of the content type to send to the server
     * @param headers: any header properties caller needs to add
     * @param contentType: tell the server about the content type encoding for the HTTP request, such as text/xml, application/xml, application/json, etc.
     * @param accept: tell the server what content type can be accepted, such as text/xml, application/xml, application/json
     * @return: the response string
     * @throws Exception
     */
    
    @Deprecated
    public String callPost(String uri, Map<String, String> params, String body, Map<String, String>headers, String contentType, String accept) throws Exception {
        
        if(headers == null)
            headers = new HashMap<String, String>();
        
        if(StringUtils.isNotEmpty(contentType))
            headers.put("Content-Type", contentType);
        
        if(StringUtils.isNotEmpty(accept))
            headers.put("Accept", accept);
        
        return callPost(uri, params, body, headers);
    }
    
    public String callPost(String uri, Map<String, String> params, String body, Map<String, String>headers) throws Exception {
        HttpURLConnection conn = null;
        
        InputStream input = null;
        InputStream error = null;
        OutputStream output = null;
        
        BufferedReader bufferedReader = null;
        BufferedWriter bufferedWriter = null;
        
        StringBuilder path = new StringBuilder();
        StringBuilder response = new StringBuilder();
        
        try {
            path.append(uri);
            
            String url = "";

            if(params != null && params.size() != 0) {
                if(uri.indexOf("?") != -1)
                    path.append("&");
                else
                    path.append("?");
                
                for(Iterator<String> it=params.keySet().iterator(); it.hasNext();) {
                    String key = it.next();
                    String value = params.get(key);
                    path.append(key).append("=").append(URLEncoder.encode(value, StandardCharsets.UTF_8.name())).append("&");
                    url = path.toString();
                    url = url.substring(0, url.length()-1);
                }
            }
            
            else
                url = path.toString();

            conn = getConnection(url, headers);
            
			conn.setRequestMethod("POST");

            conn.setDoInput(true);
            conn.setDoOutput(true);
         
            if(basicAuth)
                doBasicAuthentication(conn);
    
            if(body != null) {
                conn.addRequestProperty("Content-Length", Integer.toString(body.length()));
                output = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output, "UTF8"));
                writer.write(body.trim());
                writer.flush();
            }
            
//            conn.setInstanceFollowRedirects(false);
            
            // Get response code
            statusCode = conn.getResponseCode();
            
            // Get Response message
            reasonPhrase = conn.getResponseMessage();
            
            // Get response headers
            Map<String, List<String>> headerFields = conn.getHeaderFields();
            
            String domain = conn.getURL().getHost();
            
            for(Iterator<String> it=headerFields.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                
                if(StringUtils.isBlank(key))
                    continue;
                
                if(!key.equalsIgnoreCase(SET_COOKIE)) {
                    List<String> values = headerFields.get(key);
                    StringBuilder sb = new StringBuilder();
                    
                    if(values != null && values.size() > 0) {                
                        int size = values.size();
                        
                        for(int i=0; i<size; i++) {

                            String value = values.get(i);
                            sb.append(value);
                            if(i != size-1)
                                sb.append(",");
                        }
                    }
                    
                    responseHeaders.put(key, sb.toString());
                }

                else {
                    List<String> headerValues = headerFields.get(key);
                    
                    StringTokenizer st = new StringTokenizer(headerValues.get(0), COOKIE_VALUE_DELIMITER);

                    HttpCookie cookie = null;
                    
                    if (st.hasMoreTokens()) {
                        String token  = st.nextToken();
                        String name = token.substring(0, token.indexOf(NAME_VALUE_SEPARATOR));
                        String value = token.substring(token.indexOf(NAME_VALUE_SEPARATOR) + 1, token.length());
                        cookie = new HttpCookie(name, value);
                
                        while (st.hasMoreTokens()) {
                            token  = st.nextToken();
                            if(StringUtils.isBlank(token))
                                continue;
    
                            int index = token.indexOf(NAME_VALUE_SEPARATOR);
                            name = token;
                            value = "";
                            
                            if(index > 0) {
                                name = token.substring(0, index).trim();
                                value = token.substring(index + 1, token.length());
    
                                if(name.equalsIgnoreCase(PATH))
                                    cookie.setPath(value);
                            }
                            
                            else
                                cookie.setComment(name);
                        }
    
                        cookie.setDomain(domain);
                        cookieStore.add(new URI(domain), cookie);
                    }
                }
            }
            
            input = conn.getInputStream();

            bufferedReader = new BufferedReader(new InputStreamReader(input));
            
            String line = null;
            while ((line = bufferedReader.readLine()) != null)
                response.append(line).append(lineSeparator);

//            conn.getContent();
        } catch(Exception e) {
//            e.printStackTrace();
            if(conn == null)
                throw e;
            
            statusCode = conn.getResponseCode();
            reasonPhrase = conn.getResponseMessage();
            Log.log.error("Response code is: " + statusCode);
            Log.log.error("Response message is: " + reasonPhrase);

            String errorMsg = "";
            
            try {
                error = conn.getErrorStream();

                if(bufferedReader != null)
                    bufferedReader.close();
                
                bufferedReader = new BufferedReader(new InputStreamReader(error));
                
                String line = null;
                StringBuilder sb = new StringBuilder();
                while ((line = bufferedReader.readLine()) != null)
                    sb.append(line);
                
                errorMsg = sb.toString();
            } catch(Exception eee) {
                Log.log.debug(eee.getMessage(), eee);
            }
            
            String message = e.getMessage();
            if(StringUtils.isNotEmpty(message))
                message = message + lineSeparator + errorMsg;
            else
                message = errorMsg;
            
            if(StringUtils.isEmpty(message))
                message = "";
            
            Exception ee = new Exception(message);
            ee.setStackTrace(e.getStackTrace());
            
            Log.log.error(ee.getMessage(), ee);
            throw ee;
        } finally {
            try {
                if(input != null)
                    input.close();
                if(error != null)
                    error.close();
                if(output != null)
                    output.close();
                if(bufferedReader != null)
                    bufferedReader.close();
                if(bufferedWriter != null)
                    bufferedWriter.close();
                if(conn != null)
                    conn.disconnect();
            } catch(Exception ee) {}
        }
        
        Log.log.debug("Response code is: " + statusCode);
        Log.log.debug("Response message is: " + response.toString());
        
        return response.toString();
    }
    
    private void doBasicAuthentication(URLConnection conn) throws Exception {
        
        if(StringUtils.isBlank(user))
            throw new Exception("User is missing for Basic Authentication.");
        
        try {
            String auth = user + ":" + pass;
            String authEncoded = new String(org.apache.commons.codec.binary.Base64.encodeBase64(auth.getBytes()));
            conn.setRequestProperty("Authorization", String.format("Basic %s", authEncoded));
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw new Exception("Failed to do Basic Authentication.");
        }
    }
    
    private HttpURLConnection getConnection(String uri, Map<String, String>headers) throws Exception {
        
        HttpURLConnection conn = null;
        
        URL url = new URL(baseUrl + uri);
        Log.log.debug("url = " + url.toString());

        String protocol = url.getProtocol();
        if(StringUtils.isNotBlank(protocol) && protocol.equalsIgnoreCase("https")) {
            if(selfSigned) {
                HttpsURLConnection.setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier() {
                    public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
                        return true;
                    }
                });
            }
            
            if(trustAll) {
                TrustManager[] trustAllCerts = trustAllCertificates();
                
                String tls = System.getProperty("https.protocols");
                
                if(StringUtils.isNotEmpty(tls)) {
                    if(tls.contains(",")) {
                        String tlss[] = tls.split(",");
                        tls = tlss[0];
                    }
                }

                else
                    tls = TLS;
                Log.log.debug("TLS version is: " + tls);
                
                SSLContext sc = SSLContext.getInstance(tls);
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                
                HttpsURLConnection.setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier() {
                    public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
                        return true;
                    }
                });
            }

            conn = (HttpsURLConnection)url.openConnection();
        } // https
        
        else
            conn = (HttpURLConnection) url.openConnection();

        if(headers != null) {
            Set<String> keys = headers.keySet();
            for(Iterator it = keys.iterator(); it.hasNext();) {
                String key = (String)it.next();
                String value = headers.get(key);
                
                conn.addRequestProperty(key, value);
            }
        }
        
        if(cookieEnabled) {
            List<HttpCookie> cookies = cookieStore.getCookies();
            
            if (cookies != null && cookies.size() != 0) {
                StringBuilder sb = new StringBuilder();
                
                for (HttpCookie cookie : cookies) {
                    String cookieName = cookie.getName();
                    String cookieValue = cookie.getValue();
                    
                    if(url.getHost().equals(cookie.getDomain()))
                        sb.append(cookieName).append("=").append(cookieValue);
                }

//              Log.log.info("cookies = " + sb.toString());
                conn.addRequestProperty(COOKIE, sb.toString());
            }
        }
        
        conn.setConnectTimeout(30000);
        
        return conn;
    }
    
    private static TrustManager[] trustAllCertificates() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };
        
        return trustAllCerts;
    }
    
    /*
    public static void main(String... args) throws Exception
    {
        //ServiceNow
        String baseUrl = "https://ven01097.service-now.com/api/now/v1/table/";
        String httpBasicAuthUserName = "resolve_test_user";
        String httpBasicAuthPassword = "resolve_test_user";
        
        RestCaller restCaller = new RestCaller(baseUrl, null, null, httpBasicAuthUserName, httpBasicAuthPassword); */
        /*
        try
        {
            List<String> allowed = restCaller.optionsMethod();
            
            System.out.println("Status Code : " + restCaller.getStatusCode());
            System.out.println("Reason Phrase : " + restCaller.getReasonPhrase());
            System.out.println("Response Headers : " + StringUtils.mapToString(restCaller.getResponseHeaders(), "=", ","));
            System.out.println("Allowed Methods : " + StringUtils.listToString(allowed, ","));
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        */
        
        //restCaller.setUrlSuffix("incident");
        
        /*
        try
        {
            Map<String, String> metaInfo = restCaller.headMethod();
            
            System.out.println("Status Code : " + restCaller.getStatusCode());
            System.out.println("Reason Phrase : " + restCaller.getReasonPhrase());
            System.out.println("Response Headers : " + StringUtils.mapToString(restCaller.getResponseHeaders(), "=", ","));
            System.out.println("Meta Info : " + StringUtils.mapToString(metaInfo, "=", ","));
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }*/
        /*
        try
        {
            /*
            //Simple GET
            String entity = restCaller.getMethod(null, null, null);
            
            System.out.println("Status Code : " + restCaller.getStatusCode());
            System.out.println("Reason Phrase : " + restCaller.getReasonPhrase());
            System.out.println("Response Headers : " + StringUtils.mapToString(restCaller.getResponseHeaders(), "=", ","));
            System.out.println("Entity : [" + entity + "]");
            
            //GET by resource id 
            String entity = restCaller.getMethod("9c573169c611228700193229fff72400", null, null);
            
            System.out.println("Status Code : " + restCaller.getStatusCode());
            System.out.println("Reason Phrase : " + restCaller.getReasonPhrase());
            System.out.println("Response Headers : " + StringUtils.mapToString(restCaller.getResponseHeaders(), "=", ","));
            System.out.println("Entity : [" + entity + "]");*/
            /*
            //ServiceNow GET Flow Control Example
            int offset = 0;
            final int limit = 20;
            Map<String, String> reqParams = new HashMap<String, String>();
            boolean isAvailable = true;
            String entity;
            
            reqParams.put("sysparm_limit", Integer.toString(limit));
            
            while(isAvailable)
            {
                if(offset > 0)
                {
                    reqParams.put("sysparm_offset", Integer.toString(offset));
                }
                
                entity = restCaller.getMethod(null, reqParams, null);
                
                System.out.println("Starting Offset : " + offset);
                System.out.println("Status Code : " + restCaller.getStatusCode());
                System.out.println("Reason Phrase : " + restCaller.getReasonPhrase());
                System.out.println("Response Headers : " + StringUtils.mapToString(restCaller.getResponseHeaders(), "=", ","));
                System.out.println("Entity : [" + entity + "]");
                
                isAvailable = false;
                
                if(restCaller.getResponseHeaders().containsKey("Link") &&
                   restCaller.getResponseHeaders().get("Link").indexOf("rel=\"next\"") > 0 )
                {
                    isAvailable = true;
                    offset += limit;
                }
            }*
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }*/
        /*
        try
        {
            Map<String, String> resourceProps = new HashMap<String, String>();
            
            String timeStamp = new Date().toString();
            resourceProps.put("short_description", "Test creation of incident at " + timeStamp);
            resourceProps.put("comments", "Test comments at " + timeStamp);
            
            String entity = restCaller.postMethod(null, null, resourceProps);
            
            System.out.println("Status Code : " + restCaller.getStatusCode());
            System.out.println("Reason Phrase : " + restCaller.getReasonPhrase());
            System.out.println("Response Headers : " + StringUtils.mapToString(restCaller.getResponseHeaders(), "=", ","));
            System.out.println("Newly Created Resource Location : " + restCaller.getResponseHeaders().get(HttpHeaders.LOCATION));
            System.out.println("Newly Created Resource : [" + entity + "]");
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }*/
        /*
        try
        {
            Map<String, String> resourceProps = new HashMap<String, String>();
            
            String timeStamp = new Date().toString();
            resourceProps.put("short_description", "Updated incident at " + timeStamp);
            resourceProps.put("comments", "Updated comments at " + timeStamp);
            
            String entity = restCaller.putMethod("cdaa56c3374b3100a0b4097973990e1c", null, null, resourceProps);
            
            System.out.println("Status Code : " + restCaller.getStatusCode());
            System.out.println("Reason Phrase : " + restCaller.getReasonPhrase());
            System.out.println("Response Headers : " + StringUtils.mapToString(restCaller.getResponseHeaders(), "=", ","));
            System.out.println("Updated Resource : [" + entity + "]");
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }*/
        /*
        try
        {
            restCaller.deleteMethod("cdaa56c3374b3100a0b4097973990e1c");
            
            System.out.println("Status Code : " + restCaller.getStatusCode());
            System.out.println("Reason Phrase : " + restCaller.getReasonPhrase());
            System.out.println("Response Headers : " + StringUtils.mapToString(restCaller.getResponseHeaders(), "=", ","));
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }*/
    //}
}
