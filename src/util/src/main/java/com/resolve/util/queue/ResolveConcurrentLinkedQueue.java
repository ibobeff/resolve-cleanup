/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util.queue;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;

import com.resolve.util.Log;

@SuppressWarnings("serial")
public class ResolveConcurrentLinkedQueue<T> extends ConcurrentLinkedQueue<T>
{
    private static final long serialVersionUID = -2120868118740062006L;

	List<QueueListener<T>> listeners = new CopyOnWriteArrayList<QueueListener<T>>();
    
    //This property indicates if at least one of the listeners fails to process a message, should
    //we remove it from the queue or not. Default is true as it's the listener's job to handle failure.
    private boolean removeOnFailure = true;

    public boolean isRemoveOnFailure()
    {
        return removeOnFailure;
    }

    public void setRemoveOnFailure(boolean removeOnFailure)
    {
        this.removeOnFailure = removeOnFailure;
    }

    @Override
    public boolean add(T message)
    {
        super.add(message);
        notifyListeners(message);
        return true;
    }

    @Override
    public boolean offer(T message)
    {
        boolean result = super.offer(message);
        if (result)
        {
            notifyListeners(message);
        }
        return result;
    }

    private void notifyListeners(T message)
    {
        if(Log.log.isTraceEnabled())
        {
            Log.log.trace("Received message from the Queue: " + message);
        }

        boolean isProcessed = true;
        for (QueueListener<T> listener : listeners)
        {
            boolean response = listener.receive(message);
            if(isProcessed)
            {
                //once a listener fail to receive the message, most likely 
                //we will have to retain the message unless isRemoveOnFailure
                isProcessed = response;
            }
        }
        if(isProcessed || isRemoveOnFailure())
        {
            this.remove(message);
        }
        if(Log.log.isTraceEnabled())
        {
            Log.log.trace("Number of listeners: " + listeners.size());
            Log.log.trace("Notified all the listeners and removed the message from the queue");
        }
    }

    /**
     * Caller could keep adding {@link QueueListener} to the Queue.
     *
     * @param listener if null doesn't warn the caller, quietly ignores it.
     */
    public void addListener(QueueListener<T> listener)
    {
        if(listener != null)
        {
            Log.log.trace("Added listener: " + listener.getId());
            listeners.add(listener);
        }
        else
        {
            Log.log.info("Listener must be not null");
        }
    }
}
