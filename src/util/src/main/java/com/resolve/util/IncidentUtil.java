package com.resolve.util;

import java.util.HashMap;
import java.util.Map;

public class IncidentUtil
{
    
    private static Map<String, IdentifierExtractor<String>> GATEWAY_IDENTIFIER_EXTRACTORS = new HashMap<>();
    
    static
    {
        GATEWAY_IDENTIFIER_EXTRACTORS.put("ServiceNow", (data) -> extract((Map<String, String>) data, "NUMBER"));
        GATEWAY_IDENTIFIER_EXTRACTORS.put("Netcool", (data) -> extract((Map<String, String>) data, "IDENTIFIER"));
        GATEWAY_IDENTIFIER_EXTRACTORS.put("Zenoss", (data) -> extract((Map<String, String>) data, "ID"));
        GATEWAY_IDENTIFIER_EXTRACTORS.put("Splunk", (data) -> extract((Map<String, String>) data, "_BKT"));
        
        GATEWAY_IDENTIFIER_EXTRACTORS.put("Remedy", (data) -> extract((Map<String, String>) data, null));
        GATEWAY_IDENTIFIER_EXTRACTORS.put("RemedyX", (data) -> extract((Map<String, String>) data, null));
    }
    
    private static final String extract(Map<String, String> data, String key) {
        if ( data.containsKey(key) ) {
            return data.get(key);
        } else if(data.containsKey(key.toLowerCase())) {
            return data.get(key.toLowerCase());
        }
        return null;
    }
    
    public static String extractId(String source, Object data) {
    	String result = "";
    	if (GATEWAY_IDENTIFIER_EXTRACTORS.containsKey(source))
    		result = GATEWAY_IDENTIFIER_EXTRACTORS.get(source).extractIdentifier(data);
    	return result;
    }
    
    public static boolean isAllowed(String source) {
        return GATEWAY_IDENTIFIER_EXTRACTORS.containsKey(source);
    }
} 
