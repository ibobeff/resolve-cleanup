/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

public class DBPoolUtils
{
    private static DBPoolStatusProvider provider;

    public static void registerProvider(DBPoolStatusProvider p)
    {
        provider = p;
    }

    public static int getMaxPoolSize()
    {
        return provider.getMaxPoolSize();
    }

    public static int getPoolSize()
    {
        return provider.getPoolSize();
    }

    public static int getAvailableSize()
    {
        return provider.getAvailableSize();
    }
    
    public static int getTotalSize()
    {
        return provider.getTotalSize();
    }
}
