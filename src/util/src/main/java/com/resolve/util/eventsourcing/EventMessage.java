package com.resolve.util.eventsourcing;

import java.io.Serializable;
import java.util.UUID;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;


import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.util.eventsourcing.incident.EventData;

public class EventMessage implements Serializable
{
    private static final OffsetDateTime EPOCH_START = OffsetDateTime.of(1970, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC);

    private UUID uuid;
    private String type;
    private int version;
    @JsonProperty(value = "occurred_at")
    private long occurredAt;
    private String data;

    @JsonIgnore
    private String jsonValue;

    public EventMessage(String type, EventData data)
    {
        super();

        ObjectMapper objectMapper = new ObjectMapper();
        try
        {
            this.type = type;
            this.uuid = UUID.randomUUID();
            this.version = 1;
            this.data = objectMapper.writeValueAsString(data);
            this.occurredAt = ChronoUnit.MILLIS.between(EPOCH_START, OffsetDateTime.now(ZoneOffset.UTC));

            this.jsonValue = objectMapper.writeValueAsString(this);

        }
        catch (Exception e)
        {
            // OOPS
        }
    }

    public String toJSON()
    {
        return jsonValue;
    }

    public UUID getUuid()
    {
        return uuid;
    }

    public void setUuid(UUID uuid)
    {
        this.uuid = uuid;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public int getVersion()
    {
        return version;
    }

    public void setVersion(int version)
    {
        this.version = version;
    }

    public long getOccurredAt()
    {
        return occurredAt;
    }

    public void setOccurredAt(long occurredAt)
    {
        this.occurredAt = occurredAt;
    }

    public String getData()
    {
        return data;
    }

    public void setData(String data)
    {
        this.data = data;
    }

}
