/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

/**
 * This class attempts to erase characters echoed to the console.
 */

class PasswordMaskingThread extends Thread
{
    private volatile boolean stop;

    private char echochar = '*';

    /**
     * @param prompt
     *            - The prompt displayed to the user
     */
    public PasswordMaskingThread(String prompt)
    {
        System.out.print(prompt);
    } // MaskingThread

    /**
     * Begin masking until asked to stop.
     */
    public void run()
    {

        int priority = Thread.currentThread().getPriority();
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

        try
        {
            stop = false;
            while (!stop)
            {
                System.out.print("\010" + echochar);
                try
                {
                    // attempt masking at this rate
                    Thread.currentThread().sleep(1);
                }
                catch (InterruptedException iex)
                {
                    Thread.currentThread().interrupt();
                    return;
                }
            }
        }
        finally
        {
            // restore the original priority
            Thread.currentThread().setPriority(priority);
        }
    } // run

    /**
     * Instruct the thread to stop masking.
     */
    public void stopMasking()
    {
        this.stop = true;
    } // stopMasking

} // PasswordMaskingThread
