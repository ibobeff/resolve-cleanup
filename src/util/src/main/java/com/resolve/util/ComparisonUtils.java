/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/
package com.resolve.util;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

/**
 * Utility class for comparing objects.
 * 
 */
public class ComparisonUtils
{
    public static final String GTE = ">=";
    public static final String GT = ">";
    public static final String EQ = "==";
    public static final String NEQ = "!=";
    public static final String LT = "<";
    public static final String LTE = "<=";
    public static final String CONTAINS = "contains";
    public static final String NOT_CONTAINS = "notContains";
    public static final String STARTS_WITH = "startsWith";
    public static final String ENDS_WITH = "endsWith";
    
    /**
     * This method compares all variables with the source. Returns true only if ALL variables
     * complies with the operator.
     * 
     * Example: Map variables = abcd, xbcy, bcdz, returns true when compareAll(variables, "bc", "contains") is called
     * 
     * @param variables Collection or other object to be compared with source
     * @param source String or Number
     * @param operator one of ">=", ">", "==", "!=", "<", "<=", "contains"
     * @return
     */
    public static boolean compareAll(Object variables, Object source, String operator)
    {
        boolean result = false;
        Collection<Object> tmpVariables = new LinkedList<Object>();
        if (variables != null && source != null)
        {
            result = true;
            if (variables instanceof Collection)
            {
                tmpVariables = (Collection<Object>) variables;
            }
            else
            {
                if (variables instanceof String)
                {
                    tmpVariables.add(variables);
                }
                else
                {
                    tmpVariables.add(variables.toString());
                }
            }
            
            if (tmpVariables.size() > 0)
            {
                for (Object variable : tmpVariables)
                {
                    String variableString = variable.toString();
                    String sourceString = source.toString();
    
                    if (CONTAINS.equals(operator))
                    {
                        if (!variableString.toLowerCase().contains(sourceString.toLowerCase()))
                        {
                            result = false;
                            break;
                        }
                    }
                    else if (NOT_CONTAINS.equals(operator))
                    {
                        if (variableString.toLowerCase().contains(sourceString.toLowerCase()))
                        {
                            result = false;
                            break;
                        }
                    }
                    else if (STARTS_WITH.equals(operator))
                    {
                        if (!variableString.toLowerCase().startsWith(sourceString.toLowerCase()))
                        {
                            result = false;
                            break;
                        }
                    }
                    else if (ENDS_WITH.equals(operator))
                    {
                        if (!variableString.toLowerCase().endsWith(sourceString.toLowerCase()))
                        {
                            result = false;
                            break;
                        }
                    }
                    else if (EQ.equals(operator) && !NumberUtils.isNumber(variableString) && !NumberUtils.isNumber(sourceString))
                    {
                        if (!variableString.equalsIgnoreCase(sourceString))
                        {
                            result = false;
                            break;
                        }
                    }
                    else if (NEQ.equals(operator) && !NumberUtils.isNumber(variableString) && !NumberUtils.isNumber(sourceString))
                    {
                        if (variableString.equalsIgnoreCase(sourceString))
                        {
                            result = false;
                            break;
                        }
                    }
                    else
                    {
                        BigDecimal[] variableArray = NumberUtils.getBigDecimalArray(variableString);
                        BigDecimal[] targetArray = NumberUtils.getBigDecimalArray(sourceString);
    
                        if (GTE.equals(operator))
                        {
                            if (lessThan(variableArray, variableArray))
                            {
                                result = false;
                                break;
                            }
                        }
                        else if (GT.equals(operator))
                        {
                            if (lessThan(variableArray, targetArray) || equals(variableArray, targetArray))
                            {
                                result = false;
                                break;
                            }
                        }
                        else if (EQ.equals(operator))
                        {
                            if (!equals(variableArray, targetArray))
                            {
                                result = false;
                                break;
                            }
                        }
                        else if (NEQ.equals(operator))
                        {
                            if (equals(variableArray, targetArray))
                            {
                                result = false;
                                break;
                            }
                        }
                        else if (LT.equals(operator))
                        {
                            if (greaterThan(variableArray, targetArray) || equals(variableArray, targetArray))
                            {
                                result = false;
                                break;
                            }
                        }
                        else if (LTE.equals(operator))
                        {
                            if (greaterThan(variableArray, targetArray))
                            {
                                result = false;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                /*
                 * If tmpVariables is an empty array, any negative operation is going to result in true
                 */
                switch (operator)
                {
                    case NOT_CONTAINS :
                    case NEQ :
                    {
                        result = true;
                        break;
                    }
                    default :
                    {
                        result = false;
                        break;
                    }
                    
                }
            }
        }
        else if (variables == null)
        {
            /*
             * If variables is null, any negative operation is going to result in true
             */
            switch (operator)
            {
                case NOT_CONTAINS :
                case NEQ :
                {
                    result = true;
                    break;
                }
                default :
                {
                    result = false;
                    break;
                }
                
            }
        }
        return result;
    }

    /**
     * This method compares any variables with the source. Returns true if ANY variable
     * complies with the operator.
     * 
     * Example: Map variables = 2, 3, 1; returns true when compareAny(variables, 2, ">") is called.
     * 
     * @param variables Collection or other object to be compared with source
     * @param source String or number
     * @param operator one of ">=", ">", "==", "!=", "<", "<=", "contains"
     * @return
     */
    public static boolean compareAny(Object variables, Object source, String operator)
    {
        boolean result = false;
        Collection<Object> tmpVariables = new LinkedList<Object>();
        if (variables != null && source != null)
        {
            if (variables instanceof Collection)
            {
                tmpVariables = (Collection<Object>) variables;
            }
            else
            {
                if (variables instanceof String)
                {
                    tmpVariables.add(variables);
                }
                else
                {
                    tmpVariables.add(variables.toString());
                }
            }
            
            if (tmpVariables.size() > 0)
            {
                for (Object variable : tmpVariables)
                {
                    String variableString = variable.toString();
                    String sourceString = source.toString();
                    
                    if (CONTAINS.equals(operator))
                    {
                        if (variableString.toLowerCase().contains(sourceString.toLowerCase()))
                        {
                            result = true;
                            break;
                        }
                    }
                    else if (NOT_CONTAINS.equals(operator))
                    {
                        if (!variableString.toLowerCase().contains(sourceString.toLowerCase()))
                        {
                            result = true;
                            break;
                        }
                    }
                    else if (STARTS_WITH.equals(operator))
                    {
                        if (variableString.toLowerCase().startsWith(sourceString.toLowerCase()))
                        {
                            result = true;
                            break;
                        }
                    }
                    else if (ENDS_WITH.equals(operator))
                    {
                        if (variableString.toLowerCase().endsWith(sourceString.toLowerCase()))
                        {
                            result = true;
                            break;
                        }
                    }
                    else if (EQ.equals(operator) && (!NumberUtils.isNumber(variableString) && !NumberUtils.isNumber(sourceString)))
                    {
                        //this is pure string comparison
                        if (variableString.equalsIgnoreCase(sourceString))
                        {
                            result = true;
                            break;
                        }
                    }
                    else if (NEQ.equals(operator) && (!NumberUtils.isNumber(variableString) && !NumberUtils.isNumber(sourceString)))
                    {
                        //this is pure string comparison
                        if (!variableString.equalsIgnoreCase(sourceString))
                        {
                            result = true;
                            break;
                        }
                    }
                    else
                    {
                        BigDecimal[] variableArray;
                        BigDecimal[] targetArray;
                                        
                        try 
                        {
                            variableArray = NumberUtils.getBigDecimalArray(variableString);
                            targetArray = NumberUtils.getBigDecimalArray(sourceString);
                        }
                        catch(NumberFormatException e)
                        {
                            //if the message is not a number for the query is not a number for a comparator operator then we should assume false
                            return false;
                        }                    
    
                        if (GTE.equals(operator))
                        {
                            if (greaterThan(variableArray, targetArray) || equals(variableArray, targetArray))
                            {
                                result = true;
                                break;
                            }
                        }
                        else if (GT.equals(operator))
                        {
                            if (greaterThan(variableArray, targetArray))
                            {
                                result = true;
                                break;
                            }
                        }
                        else if (EQ.equals(operator))
                        {
                            if (equals(variableArray, targetArray))
                            {
                                result = true;
                                break;
                            }
                        }
                        else if (NEQ.equals(operator))
                        {
                            if (!equals(variableArray, targetArray))
                            {
                                result = true;
                                break;
                            }
                        }
                        else if (LT.equals(operator))
                        {
                            if (lessThan(variableArray, targetArray))
                            {
                                result = true;
                                break;
                            }
                        }
                        else if (LTE.equals(operator))
                        {
                            if (lessThan(variableArray, targetArray) || equals(variableArray, targetArray))
                            {
                                result = true;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                /*
                 * If tmpVariables is an empty array, any negative operation is going to result in true
                 */
                switch (operator)
                {
                    case NOT_CONTAINS :
                    case NEQ :
                    {
                        result = true;
                        break;
                    }
                    default :
                    {
                        result = false;
                        break;
                    }
                    
                }
            }
        }
        else if (variables == null)
        {
            /*
             * If variables is null, any negative operation is going to result in true
             */
            switch (operator)
            {
                case NOT_CONTAINS :
                case NEQ :
                {
                    result = true;
                    break;
                }
                default :
                {
                    result = false;
                    break;
                }
                
            }
        }
        return result;
    }
    
    /**
     * Used to compare two date objects based on operator 
     * 
     * @param varDate - variable Date to compare with source
     * @param srcDate - source Date to compare with variable
     * @param operator - One of ">=", ">", "==", "!=", "<", "<="
     * @return
     */
    public static boolean compareDate(Date varDate, Date srcDate, String operator)
    {
        if(EQ.equals(operator))
        {
                return equals(varDate, srcDate);
        }
        else if(LT.equals(operator))
        {
            return lessThan(varDate, srcDate);
        }
        else if(LTE.equals(operator))
        {
            if(lessThan(varDate, srcDate))
            {
                return true;
            }            
            return equals(varDate, srcDate);
        }
        else if(GT.equals(operator))
        {
            return greaterThan(varDate, srcDate);
        }
        else if(GTE.equals(operator))
        {
            if(greaterThan(varDate, srcDate))
            {
                return true;
            }            
            return equals(varDate, srcDate);
        }
        else
        {
            throw new RuntimeException("Invalid Operator in date comparison");
        }
    }

    private final static boolean greaterThan(BigDecimal variable, BigDecimal tmpSource)
    {
        return variable.compareTo(tmpSource) > 0;
    }

    private final static boolean lessThan(BigDecimal variable, BigDecimal tmpSource)
    {
        return variable.compareTo(tmpSource) < 0;
    }

    private final static boolean equals(BigDecimal variable, BigDecimal tmpSource)
    {
        return variable.equals(tmpSource);
    }
    
    private final static boolean greaterThan(BigDecimal[] variables, BigDecimal[] sources)
    {
        return ArrayUtils.compareArray(variables, sources) > 0;
    }

    private final static boolean lessThan(BigDecimal[] variables, BigDecimal[] sources)
    {
        return ArrayUtils.compareArray(variables, sources) < 0;
    }

    private final static boolean equals(BigDecimal[] variables, BigDecimal[] sources)
    {
        return ArrayUtils.compareArray(variables, sources) == 0;
    }
    
    protected static final boolean greaterThan(Date variableDate, Date sourceDate)
    {
        return variableDate.after(sourceDate);
    }
    
    protected static final boolean lessThan(Date variableDate, Date sourceDate)
    {
        return variableDate.before(sourceDate);
    }
    
    private static final boolean equals(Date variableDate, Date sourceDate)
    {
        return variableDate.equals(sourceDate);
    }
}
