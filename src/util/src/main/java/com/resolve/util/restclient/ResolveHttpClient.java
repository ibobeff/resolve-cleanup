/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util.restclient;

import java.net.MalformedURLException;

import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import com.resolve.util.SSLUtils;

public class ResolveHttpClient extends DefaultHttpClient
{
    /** The time it takes for our client to timeout */
    //public static final int HTTP_TIMEOUT = 30 * 1000; // milliseconds
    //public static final int SOCKET_TIMEOUT = 50 * 1000; // milliseconds
    private int httpPort = 80; //default HTTP port is 80
    private int httpsPort = 443; //default HTTPS port is 443

    public ResolveHttpClient(String url) throws MalformedURLException
    {
        int port = SSLUtils.getPort(url);
        if(SSLUtils.isSSL(url))
        {
            if(port > -1)
            {
                this.httpsPort = port;
            }
        }
        else
        {
            if(port > -1)
            {
                this.httpPort = port;
            }
        }
    }

    @Override
    protected ClientConnectionManager createClientConnectionManager()
    {
        SchemeRegistry registry = new SchemeRegistry();
        registry.register(new Scheme("http", this.httpPort, PlainSocketFactory.getSocketFactory()));
        // Register for port httpsPort our SSLSocketFactory to the ConnectionManager
        registry.register(new Scheme("https", this.httpsPort, new ResolveSSLSocketFactory()));

        // setting the httpparams
        HttpParams params = new BasicHttpParams();
        // params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 1);
        // params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new
        // ConnPerRouteBean(1));
        params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
        // HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(params, "utf8");

        return new SingleClientConnManager(registry);
    }
}
