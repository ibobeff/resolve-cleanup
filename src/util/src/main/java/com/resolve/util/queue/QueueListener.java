/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util.queue;


/**
 * Interface for the Resolve queue listener.
 *
 */
public interface QueueListener<T>
{
    /**
     * Retrunes a random ID (GUID) just in case for any housekeeping, logging etc.
     *
     * @return
     */
    String getId();

    /**
     * Process a message
     *
     * @param t
     * @return
     */
    boolean process(T t) throws Exception;
    
    /**
     * Receive message from the Queue.
     * 
     * @param t
     * @return
     */
    boolean receive(T t);
}
