/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SoapUtil
{

    /**
     * This method simply returns the XML content of a single node.
     * 
     * Example:
     * 
     * Here is a SOAP response and we want to get the value of the "response"
     * element:
     * 
     * <SOAP-ENV:Envelope
     * xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
     * xmlns:xsd="http://www.w3.org/2001/XMLSchema"
     * xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> <SOAP-ENV:Body>
     * <executeResponse> <response>2012-07-17 11:02:56</response>
     * </executeResponse> </SOAP-ENV:Body> </SOAP-ENV:Envelope>
     * 
     * @param soap
     * @param elementTagName
     * @return
     */
    public static String getSingleData(String soap, String elementTagName)
    {
        String result = null;

        MessageFactory factory;
        try
        {
            factory = MessageFactory.newInstance();
            SOAPMessage msg = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(soap.toString().getBytes(Charset.forName("UTF-8"))));
            msg.saveChanges();
            SOAPBody soapBody = msg.getSOAPBody();
            for (Element response : elements(soapBody.getElementsByTagName(elementTagName)))
            {
                result = response.getTextContent();
                break;
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error during parsing soap response", e);
        }

        return result;
    }

    public static Map<String, String> getData(String soap, String elementTagName)
    {
        Map<String, String> result = new HashMap<String, String>();

        MessageFactory factory;
        try
        {
            factory = MessageFactory.newInstance();
            SOAPMessage msg = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(soap.toString().getBytes(Charset.forName("UTF-8"))));
            msg.saveChanges();
            SOAPBody soapBody = msg.getSOAPBody();
            for (Element response : elements(soapBody.getElementsByTagName(elementTagName)))
            {

                NodeList nodes = response.getChildNodes();
                for (int i = 0; i < nodes.getLength(); i++)
                {
                    Node node = nodes.item(i);
                    result.put(node.getNodeName(), node.getTextContent());
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error during parsing soap response", e);
        }

        return result;
    }

    public static List<Map<String, String>> getList(String soap, String elementTagName)
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        MessageFactory factory;
        try
        {
            factory = MessageFactory.newInstance();
            SOAPMessage msg = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(soap.toString().getBytes(Charset.forName("UTF-8"))));
            msg.saveChanges();
            SOAPBody soapBody = msg.getSOAPBody();
            for (Element response : elements(soapBody.getElementsByTagName(elementTagName)))
            {
                Map<String, String> resultMap = new HashMap<String, String>();
                for (Node node : elements(response.getChildNodes()))
                {
                    resultMap.put(node.getNodeName(), node.getTextContent());
                }
                result.add(resultMap);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error during parsing soap response", e);
        }

        return result;
    }

    public static Map<String, String> getData(String soap, String reseponseElementTagName, String resultTagName)
    {
        Map<String, String> result = new HashMap<String, String>();

        MessageFactory factory;
        try
        {
            factory = MessageFactory.newInstance();
            SOAPMessage msg = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(soap.toString().getBytes(Charset.forName("UTF-8"))));
            msg.saveChanges();
            SOAPBody soapBody = msg.getSOAPBody();
            for (Element response : elements(soapBody.getElementsByTagName(reseponseElementTagName)))
            {
                NodeList nodes = response.getChildNodes();
                for (int i = 0; i < nodes.getLength(); i++)
                {
                    Node node = nodes.item(i);
                    result.put(node.getNodeName(), node.getTextContent());
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error during parsing soap response", e);
        }

        return result;
    }

    private static List<Element> elements(NodeList nodes)
    {
        List<Element> result = new ArrayList<Element>(nodes.getLength());
        for (int i = 0; i < nodes.getLength(); i++)
        {
            Node node = nodes.item(i);
            if (node instanceof Element) result.add((Element) node);
        }
        return result;
    }

    /*
    public static void main(String[] args) throws IOException, SOAPException
    {
        StringBuilder xml = new StringBuilder();
        /*
         * xml.append(
         * "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
         * ); xml.append("<SOAP-ENV:Body>"); xml.append("<getRecordsResponse>");
         * xml.append("<getRecordsResult>");
         * xml.append("   <active>0</active>");
         * xml.append("   <category>inquiry</category>");
         * xml.append("</getRecordsResult>"); xml.append("<getRecordsResult>");
         * xml.append("   <active>1</active>");
         * xml.append("   <category>hardware</category>");
         * xml.append("</getRecordsResult>");
         * xml.append("</getRecordsResponse>"); xml.append("</SOAP-ENV:Body>");
         * xml.append("</SOAP-ENV:Envelope>");
         */
        /*
         * MessageFactory factory = MessageFactory.newInstance(); SOAPMessage
         * msg = factory.createMessage(new MimeHeaders(), new
         * ByteArrayInputStream
         * (xml.toString().getBytes(Charset.forName("UTF-8"))));
         * msg.saveChanges(); SOAPBody soapBody = msg.getSOAPBody();
         * List<Map<String, String>> result = getList(xml.toString(),
         * "getRecordsResult"); for(Map<String, String> res : result) {
         * for(String key : res.keySet()) { System.out.println(key +
         * ">>>>>>>>>>" + res.get(key)); } for (Element result :
         * elements(response.getElementsByTagName("getRecordsResult"))) {
         * System.out.println(result.getNodeName() + ">>>>>>>>>>" +
         * result.getTextContent()); } }

        xml.append("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
        xml.append("<SOAP-ENV:Body>");
        xml.append("<executeResponse>");
        xml.append("<response>2012-07-17 10:05:29</response>");
        xml.append("</executeResponse>");
        xml.append("</SOAP-ENV:Body>");
        xml.append("</SOAP-ENV:Envelope>");

        System.out.println(getSingleData(xml.toString(), "response"));
    }
    */
}
