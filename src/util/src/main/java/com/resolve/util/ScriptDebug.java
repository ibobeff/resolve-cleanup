/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.collections.MapUtils;

public class ScriptDebug
{
    public boolean debug;
    public StringBuffer buffer;
    public String component;
    public String problemId;

    public Map inputs;
    public Map outputs;
    public Map params;
    public Map flows;

    public ScriptDebug(boolean isDebug, String problemId)
    {
        this.component = "";
        this.buffer = new StringBuffer();
        this.debug = false;
        this.debug = isDebug;
        this.problemId = problemId;
    } // ScriptDebug

    /*
     * public ScriptDebug(boolean isDebug) { this.component = ""; if
     * (StringUtils.isEmpty(debugStr)) { this.buffer = new StringBuffer();
     * this.debug = false; } else { this.buffer = new StringBuffer(debugStr);
     * this.debug = true; }
     * 
     * this.inputs = inputs; this.outputs = outputs; this.params = params;
     * this.flows = flows; } // ScriptDebug
     */

    public String toString()
    {
        String result = "";
        if (this.buffer != null)
        {
            result = this.buffer.toString();
        }
        return result;
    } // toString

    public String getProblemId()
    {
        return this.problemId;
    } // getProblemId

    public void setProblemId(String problemId)
    {
        this.problemId = problemId;
    } // setProblemId

    public void setMap(Map inputs, Map outputs, Map params, Map flows)
    {
        this.inputs = inputs;
        this.outputs = outputs;
        this.params = params;
        this.flows = flows;
        
        if (MapUtils.isNotEmpty(params)) {
        	if ((params.containsKey(Constants.EXECUTE_DEBUG) && 
        		 StringUtils.isNotEmpty((String) params.get(Constants.EXECUTE_DEBUG)) &&
        		 Boolean.parseBoolean((String) params.get(Constants.EXECUTE_DEBUG))) ||
        		(params.containsKey(Constants.EXECUTE_PROCESS_DEBUG) && 
               	 StringUtils.isNotEmpty((String) params.get(Constants.EXECUTE_PROCESS_DEBUG)) &&
            	 Boolean.parseBoolean((String) params.get(Constants.EXECUTE_PROCESS_DEBUG)))) {
        		debug = true;
        	} else {
        		debug = false;
        	}
        }
    } // setMap

    public void clear()
    {
        this.component = "";
        this.inputs = null;
        this.outputs = null;
        this.params = null;
        this.flows = null;
    } // clear

    public boolean isDebug()
    {
        return this.debug;
    } // isDebug

    public void setDebug(boolean isDebug)
    {
        this.debug = isDebug;
    } // setDebug

    public void newBuffer()
    {
        this.buffer = new StringBuffer();
    } // newBuffer

    public void setBuffer(StringBuffer buffer)
    {
        if (buffer == null)
        {
            buffer = new StringBuffer();
        }
        this.buffer = buffer;
    } // setBuffer

    public void setBuffer(String str)
    {
        if (str == null)
        {
            this.buffer = new StringBuffer();
        }
        else
        {
            this.buffer = new StringBuffer(debug ? str : "");
        }
    } // setBuffer

    public void append(String str)
    {
        if (str != null && debug)
        {
            this.buffer.append(str);
        }
    } // append

    public StringBuffer getBuffer()
    {
        return this.buffer;
    } // getBuffer

    public StringBuffer flush()
    {
        StringBuffer result = buffer;

        buffer = new StringBuffer();
        return debug ? result : buffer;
    } // flush

    public void setComponent(String component)
    {
        this.component = component;
    } // setComponent

    public static String getTimestamp()
    {
        SimpleDateFormat format = new SimpleDateFormat("EEE d MMM yyyy, HH:mm:ss.SSS z");
        return format.format(new Date());
    } // println

    public String println(String msg)
    {
        return println(component, msg);
    } // println

    public String println(String component, String msg)
    {
        String result = "";

        if (debug)
        {
            result = getTimestamp() + " [" + component + "] - " + msg + "\n";

            buffer.append(result);
        }

        return result;
    } // println

    public String println(String msg, Throwable e)
    {
        return println(component, msg, e);
    } // println

    public String println(String component, String msg, Throwable e)
    {
        return println(component, msg + StringUtils.getStackTrace(e));
    } // println

    public String print(String msg)
    {
        return println(msg);
    } // print

    public String print(String component, String msg)
    {
        // Log.log.debug(msg);
        return println(component, msg);
    } // print

    public String print(String msg, Throwable e)
    {
        return println(component, msg, e);
    } // print

    public String print(String component, String msg, Throwable e)
    {
        return println(component, msg + StringUtils.getStackTrace(e));
    } // println

    public String error(String msg)
    {
        return println(msg);
    } // error

    public String warn(String msg)
    {
        return println(msg);
    } // warn

    public String info(String msg)
    {
        return println(msg);
    } // info

    public String debug(String msg)
    {
        return println(msg);
    } // info

    public String printInputs()
    {
        String result = "";

        result += println("*** INPUTS ***");
        result += println("INPUTS: " + inputs);
        result += println("PARAMS: " + params);
        result += println("FLOWS:  " + flows);
        return result;
    } // printInputs

    public String printOutputs()
    {
        String result = "";

        result += println("*** OUTPUTS ***");
        result += println("OUTPUTS: " + outputs);
        result += println("PARAMS:  " + params);
        result += println("FLOWS:   " + flows);
        return result;
    } // printInputs
    
    public boolean isBufferEmpty()
    {
        return buffer.length()==0;
    }

} // ScriptDebug
