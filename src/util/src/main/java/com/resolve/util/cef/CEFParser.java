package com.resolve.util.cef;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;


/**
 * A Common Event Format (CEF) parser used to convert String into a Map containing the parsed CEF fields.
 */
public final class CEFParser {
	private static final String CEF_IDENTIFIER = "CEF:";
	private static final String CEF_DELIMITER = "|";
	private static final String CEF_IDENTIFICATION_PATTERN = "^(.*?)CEF:(\\d+)(\\|)(.+?)\\|(.+?)\\|(.+?)\\|(.+?)\\|(.+?)\\|(.+?)\\|(.+)";
	private static final String CEF_SPLIT_PATTERN = "(?<!\\\\)" + Pattern.quote(CEF_DELIMITER);
	private static final Pattern CEF_EXTENSION_SPLIT_PATTERN = Pattern.compile("(\\b\\w+)=(.*?(?=\\s\\w+=|$))");
	
	private static final String ERROR_WRONG_FORMAT = "Event is not in CEF format";
	private static final String ERROR_EMPTY_EVENT = "Event should not be null or empty";
	private static final String ERROR_EMPTY_EVENT_EXTENSIONS = "Event extensions should not be empty";
	
	/**
	 *  CEF formatted event fields. 
	 */
	enum CEF {
		CEF_VERSION("CEF Version"),
		DEVICE_VENDOR("Device Vendor"),
		DEVICE_PRODUCT("Device Product"),
		DEVICE_VERSION("Device Version"),
		SIGNATURE_ID("Signature ID"),
		EVENT_NAME("Name"),
		EVENT_SEVERITY("Severity"),
		EXTENSION("Extension");
		
		private final String cefName;

	    private CEF(String value) {
	    	cefName = value;
	    }

	    public String getCEFName() {
	        return cefName;
	    }
	}

	private CEFParser() {}

	/**
	 * Converts a CEF formatted event into a {@link CEFEvent} object
	 * @param rawEvent String containing the CEF event
	 * @return CEFEvent
	 * @throws CEFParseException
	 */
	public static CEFEvent parse(String rawEvent) throws CEFParseException {
		if (StringUtils.isEmpty(rawEvent)) {
			throw new CEFParseException(ERROR_EMPTY_EVENT);
		}

		if (isCEF(rawEvent)) {
			List<String> cefItems = getCEFItems(rawEvent);
			
			CEFEvent event = new CEFEvent();
			event.setHeaders(getHeaders(cefItems));
			event.setExtensions(getExtesions(cefItems.get(cefItems.size() - 1)));
			
			return event;
		} else {
			throw new CEFParseException(ERROR_WRONG_FORMAT);
		}
	}
	
	/**
	 * Verifies if incoming event is in CEF format.
	 * @param rawEvent String containing the CEF event
	 * @return true if event is in CEF format, otherwise false.
	 */
	public static boolean isCEF(String rawEvent) {
		return StringUtils.isEmpty(rawEvent) ? false : rawEvent.matches(CEF_IDENTIFICATION_PATTERN); 
	}

	private static List<String> getCEFItems(String rawEvent) throws CEFParseException {
		List<String> cefItems = Arrays.asList(rawEvent.split(CEF_SPLIT_PATTERN));
		if (cefItems.size() != CEF.values().length) {
			throw new CEFParseException(ERROR_WRONG_FORMAT);
		}

		return cefItems;
	}

	private static Map<String, String> getHeaders(List<String> cefItems) throws CEFParseException {
		Map<String, String> headers = new HashMap<>();
		String[] cefVersionArr = cefItems.get(0).split(CEF_IDENTIFIER);
		if (cefVersionArr.length != 2) {
			throw new CEFParseException(ERROR_WRONG_FORMAT);
		}

		headers.put(CEF.CEF_VERSION.getCEFName(), cefVersionArr[1]);
		headers.put(CEF.DEVICE_VENDOR.getCEFName(), cefItems.get(1));
		headers.put(CEF.DEVICE_PRODUCT.getCEFName(), cefItems.get(2));
		headers.put(CEF.DEVICE_VERSION.getCEFName(), cefItems.get(3));
		headers.put(CEF.SIGNATURE_ID.getCEFName(), cefItems.get(4));
		headers.put(CEF.EVENT_NAME.getCEFName(), cefItems.get(5));
		headers.put(CEF.EVENT_SEVERITY.getCEFName(), cefItems.get(6));
		
		return headers;
	}
	
	private static Map<String, String> getExtesions(String rawExtensions) throws CEFParseException {
		Map<String, String> extensions = new HashMap<>();
		Matcher matcher = CEF_EXTENSION_SPLIT_PATTERN.matcher(rawExtensions);
		
		while(matcher.find()) {
			String keyValue = matcher.group();
			// Keys cannot contain '=', while values can have escaped '='
			// symbols, so split by the first occurrence.
			String[] keyValueArr = keyValue.split("=", 2);
			extensions.put(keyValueArr[0], keyValueArr[1]);
		}		
		
		if (extensions.isEmpty()) {
			throw new CEFParseException(ERROR_EMPTY_EVENT_EXTENSIONS);
		}

		return extensions;
	}
}
