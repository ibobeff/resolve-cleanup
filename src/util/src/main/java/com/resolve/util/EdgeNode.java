/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

public class EdgeNode
{
    String id; // srcId-dstId
    String value; // label
    String condition; // Used specifically to differntiate Good/Bad edge from precondition type node
    String newStartId; // Subprocess -> Precondition, end node of subprocess connected to preconditon - new source node 
    String newEndId; // Precondition -> Subprocess, start node of subprocess connected to preconditon - new destination

    public EdgeNode(String srcId, String dstId)
    {
        id = srcId + "/" + dstId;
    } // EdgeNode

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }
    
    public String getCondition()
    {
        return condition;
    }

    public void setCondition(String condition)
    {
        this.condition = condition;
    }
    
    public String getNewStartId()
    {
        return newStartId;
    }
    
    public void setNewStartId(String newStartId)
    {
        this.newStartId = newStartId;
    }
    
    public String getNewEndId()
    {
        return newEndId;
    }
    
    public void setNewEndId(String newEndId)
    {
        this.newEndId = newEndId;
    }
    
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        
        sb.append("id = [" + id + "]");
        
        if (StringUtils.isNotBlank(value))
        {
            sb.append(", value = [" + value + "]");
        }
        
        if (StringUtils.isNotBlank(condition))
        {
            sb.append(", condition = [" + condition + "]");
        }
        
        if (StringUtils.isNotBlank(newStartId))
        {
            sb.append(", newStartId = [" + newStartId + "]");
        }
        
        if (StringUtils.isNotBlank(newEndId))
        {
            sb.append(", newEndId = [" + newEndId + "]");
        }
        
        return sb.toString();
    }
    
    public EdgeNode cloneWithNewSrcAndDestIds(String srcId, String dstId)
    {
        EdgeNode clonedEdgeNode = new EdgeNode(srcId, dstId);
        
        clonedEdgeNode.setValue(value);
        clonedEdgeNode.setCondition(condition);
        
        return clonedEdgeNode;
    }
    
    public String getNewId()
    {
        String newId = null;
        
        if (StringUtils.isNotBlank(newEndId))
        {
            newId = StringUtils.substringBefore(id, "/") + "/" + newEndId;
        }
        
        if (StringUtils.isBlank(newId))
        {
            newId = id;
        }
        
        if (StringUtils.isNotBlank(newStartId))
        {
            newId = newStartId + "/" + StringUtils.substringAfter(newId, "/");
        }
        
        if (id.equals(newId))
        {
            newId = null;
        }
        
        return newId;
    }
} // EdgeNode

