/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.util.Map;

public class GatewayEvent<T, S>
{
    public long timeStamp; // when this event is first received by Resolve
    public Map<T, S> content;

    public GatewayEvent(long t, Map<T, S> c)
    {
        timeStamp = t;
        content = c;
    }
}
