package com.resolve.util.eventsourcing.incident;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include = Inclusion.NON_NULL)
public class IncidentCreatedData extends EventData
{
    private String externalIncidentId;
    private EventMeta meta;

    public IncidentCreatedData(String incidentId, String externalIncidentId, EventMeta meta)
    {
        super(incidentId);
        this.meta = meta;
        this.externalIncidentId = externalIncidentId;
    }

    public void setExternalIncidentId(String externalIncidentId)
    {
        this.externalIncidentId = externalIncidentId;
    }
    
    public String getExternalIncidentId()
    {
        return externalIncidentId;
    }
    
    public EventMeta getMeta()
    {
        return meta;
    }

    public void setMeta(EventMeta meta)
    {
        this.meta = meta;
    }

}
