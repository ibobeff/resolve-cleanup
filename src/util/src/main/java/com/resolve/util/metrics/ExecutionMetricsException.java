package com.resolve.util.metrics;

public class ExecutionMetricsException extends Exception
{
    private static final long serialVersionUID = 1L;

    protected ExecutionMetricsException(String message)
    {
        super(message);
    }
}
