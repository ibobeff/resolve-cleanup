/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;

public class ThreadUtils
{
    public static void sleep(long millis)
    {
        try
        {
            Thread.sleep(millis);
        }
        catch (Exception e)
        {
            // do nothing
        }
    } // sleep

    /**
     * A simple method that checks for deadlocks in the system.
     *
     */
    public static void detectDeadlocks()
    {
        Log.log.trace("Checking for deadlocks.");
        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();

        long[] threadIds = threadMXBean.findDeadlockedThreads();

        if (threadIds == null || threadIds.length == 0)
        {
            Log.log.trace("No deadlocks found.");
        }
        else
        {
            StringBuilder sb = new StringBuilder();

            for (long threadId : threadIds)
            {
                ThreadInfo threadInfo = threadMXBean.getThreadInfo(threadId);

                if (sb.length() > 0)
                {
                    sb.append(", ");
                }

                sb.append(threadInfo.getThreadName()).append(" (ID: ").append(threadId).append(')');
            }

            // Raise alert here, etc.
            Log.log.error("Blocked Threads: " + sb);
        }
    }
} // ThreadUtils
