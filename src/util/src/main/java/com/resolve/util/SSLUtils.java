/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.UUID;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.http.conn.ssl.X509HostnameVerifier;

import com.resolve.util.FileUtils;

public class SSLUtils
{
    private static final String HTTPS = "https";

    /**
     * Hostname verifier for the Sun's deprecated API.
     *
     * @deprecated see {@link #_hostnameVerifier}.
     */
    private static com.sun.net.ssl.HostnameVerifier __hostnameVerifier;
    /**
     * Thrust managers for the Sun's deprecated API.
     *
     * @deprecated see {@link #_trustManagers}.
     */
    private static com.sun.net.ssl.TrustManager[] __trustManagers;
    /**
     * Hostname verifier.
     */
    private static HostnameVerifier _hostnameVerifier;
    /**
     * Thrust managers.
     */
    private static TrustManager[] _trustManagers;

    /**
     * Set the default Hostname Verifier to an instance of a fake class that
     * trust all hostnames. This method uses the old deprecated API from the
     * com.sun.ssl package.
     *
     * @deprecated see {@link #_trustAllHostnames()}.
     */
    private static void __trustAllHostnames()
    {
        // Create a trust manager that does not validate certificate chains
        if (__hostnameVerifier == null)
        {
            __hostnameVerifier = new _FakeHostnameVerifier();
        } // if
          // Install the all-trusting host name verifier
        com.sun.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(__hostnameVerifier);
    } // __trustAllHttpsCertificates

    /**
     * Set the default X509 Trust Manager to an instance of a fake class that
     * trust all certificates, even the self-signed ones. This method uses the
     * old deprecated API from the com.sun.ssl package.
     *
     * @deprecated see {@link #_trustAllHttpsCertificates()}.
     */
    private static void __trustAllHttpsCertificates()
    {
        com.sun.net.ssl.SSLContext context;

        // Create a trust manager that does not validate certificate chains
        if (__trustManagers == null)
        {
            __trustManagers = new com.sun.net.ssl.TrustManager[] { new _FakeX509TrustManager() };
        } // if
          // Install the all-trusting trust manager
        try
        {
            context = com.sun.net.ssl.SSLContext.getInstance("SSL");
            context.init(null, __trustManagers, new SecureRandom());
        }
        catch (GeneralSecurityException gse)
        {
            throw new IllegalStateException(gse.getMessage());
        } // catch
        com.sun.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
    } // __trustAllHttpsCertificates

    /**
     * Return true if the protocol handler property java. protocol.handler.pkgs
     * is set to the Sun's com.sun.net.ssl. internal.www.protocol deprecated
     * one, false otherwise.
     *
     * @return true if the protocol handler property is set to the Sun's
     *         deprecated one, false otherwise.
     */
    private static boolean isDeprecatedSSLProtocol()
    {
        return ("com.sun.net.ssl.internal.www.protocol".equals(System.getProperty("java.protocol.handler.pkgs")));
    } // isDeprecatedSSLProtocol

    /**
     * Set the default Hostname Verifier to an instance of a fake class that
     * trust all hostnames.
     */
    private static void _trustAllHostnames()
    {
        // Create a trust manager that does not validate certificate chains
        if (_hostnameVerifier == null)
        {
            _hostnameVerifier = new FakeHostnameVerifier();
        } // if
          // Install the all-trusting host name verifier:
        HttpsURLConnection.setDefaultHostnameVerifier(_hostnameVerifier);
    } // _trustAllHttpsCertificates

    /**
     * Set the default X509 Trust Manager to an instance of a fake class that
     * trust all certificates, even the self-signed ones.
     */
    private static void _trustAllHttpsCertificates()
    {
        SSLContext context;

        // Create a trust manager that does not validate certificate chains
        if (_trustManagers == null)
        {
            _trustManagers = new TrustManager[] { new FakeX509TrustManager() };
        } // if
          // Install the all-trusting trust manager:
        try
        {
            context = SSLContext.getInstance("SSL");
            context.init(null, _trustManagers, new SecureRandom());
        }
        catch (GeneralSecurityException gse)
        {
            throw new IllegalStateException(gse.getMessage());
        } // catch
        HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
    } // _trustAllHttpsCertificates

    /**
     * Set the default Hostname Verifier to an instance of a fake class that
     * trust all hostnames.
     */
    public static void trustAllHostnames()
    {
        // Is the deprecated protocol setted?
        if (isDeprecatedSSLProtocol())
        {
            __trustAllHostnames();
        }
        else
        {
            _trustAllHostnames();
        } // else
    } // trustAllHostnames

    /**
     * Set the default X509 Trust Manager to an instance of a fake class that
     * trust all certificates, even the self-signed ones.
     */
    public static void trustAllHttpsCertificates()
    {
        // Is the deprecated protocol setted?
        if (isDeprecatedSSLProtocol())
        {
            __trustAllHttpsCertificates();
        }
        else
        {
            _trustAllHttpsCertificates();
        } // else
    } // trustAllHttpsCertificates

    /**
     * This class implements a fake hostname verificator, trusting any host
     * name. This class uses the old deprecated API from the com.sun. ssl
     * package.
     *
     * @author Francis Labrie
     *
     * @deprecated see {@link SSLUtilities.FakeHostnameVerifier}.
     */
    public static class _FakeHostnameVerifier implements com.sun.net.ssl.HostnameVerifier
    {

        /**
         * Always return true, indicating that the host name is an acceptable
         * match with the server's authentication scheme.
         *
         * @param hostname
         *            the host name.
         * @param session
         *            the SSL session used on the connection to host.
         * @return the true boolean value indicating the host name is trusted.
         */
        public boolean verify(String hostname, String session)
        {
            return (true);
        } // verify
    } // _FakeHostnameVerifier

    /**
     * This class allow any X509 certificates to be used to authenticate the
     * remote side of a secure socket, including self-signed certificates. This
     * class uses the old deprecated API from the com.sun.ssl package.
     *
     * @author Francis Labrie
     *
     * @deprecated see {@link SSLUtilities.FakeX509TrustManager}.
     */
    public static class _FakeX509TrustManager implements com.sun.net.ssl.X509TrustManager
    {

        /**
         * Empty array of certificate authority certificates.
         */
        private static final X509Certificate[] _AcceptedIssuers = new X509Certificate[] {};

        /**
         * Always return true, trusting for client SSL chain peer certificate
         * chain.
         *
         * @param chain
         *            the peer certificate chain.
         * @return the true boolean value indicating the chain is trusted.
         */
        public boolean isClientTrusted(X509Certificate[] chain)
        {
            return (true);
        } // checkClientTrusted

        /**
         * Always return true, trusting for server SSL chain peer certificate
         * chain.
         *
         * @param chain
         *            the peer certificate chain.
         * @return the true boolean value indicating the chain is trusted.
         */
        public boolean isServerTrusted(X509Certificate[] chain)
        {
            return (true);
        } // checkServerTrusted

        /**
         * Return an empty array of certificate authority certificates which are
         * trusted for authenticating peers.
         *
         * @return a empty array of issuer certificates.
         */
        public X509Certificate[] getAcceptedIssuers()
        {
            return (_AcceptedIssuers);
        } // getAcceptedIssuers
    } // _FakeX509TrustManager

    /**
     * This class implements a fake hostname verificator, trusting any host
     * name.
     *
     * @author Francis Labrie
     */
    public static class FakeHostnameVerifier implements HostnameVerifier
    {

        /**
         * Always return true, indicating that the host name is an acceptable
         * match with the server's authentication scheme.
         *
         * @param hostname
         *            the host name.
         * @param session
         *            the SSL session used on the connection to host.
         * @return the true boolean value indicating the host name is trusted.
         */
        public boolean verify(String hostname, javax.net.ssl.SSLSession session)
        {
            return (true);
        } // verify
    } // FakeHostnameVerifier

    /**
     * This class allow any X509 certificates to be used to authenticate the
     * remote side of a secure socket, including self-signed certificates.
     *
     * @author Francis Labrie
     */
    public static class FakeX509TrustManager implements X509TrustManager
    {

        /**
         * Empty array of certificate authority certificates.
         */
        private static final X509Certificate[] _AcceptedIssuers = new X509Certificate[] {};

        /**
         * Always trust for client SSL chain peer certificate chain with any
         * authType authentication types.
         *
         * @param chain
         *            the peer certificate chain.
         * @param authType
         *            the authentication type based on the client certificate.
         */
        public void checkClientTrusted(X509Certificate[] chain, String authType)
        {
        } // checkClientTrusted

        /**
         * Always trust for server SSL chain peer certificate chain with any
         * authType exchange algorithm types.
         *
         * @param chain
         *            the peer certificate chain.
         * @param authType
         *            the key exchange algorithm used.
         */
        public void checkServerTrusted(X509Certificate[] chain, String authType)
        {
        } // checkServerTrusted

        /**
         * Return an empty array of certificate authority certificates which are
         * trusted for authenticating peers.
         *
         * @return a empty array of issuer certificates.
         */
        public X509Certificate[] getAcceptedIssuers()
        {
            return (_AcceptedIssuers);
        } // getAcceptedIssuers
    } // FakeX509TrustManager

    /**
     * This class allow any X509 certificates to be used to authenticate the
     * remote side of a secure socket, including self-signed certificates
     * for 
     *
     * @author Hemant Phanasgaonkar
     */
    public static class FakeX509HostnameVerifier implements X509HostnameVerifier
    {

        @Override
        public boolean verify(String hostname, SSLSession session)
        {
            return true;
        }

        @Override
        public void verify(String arg0, SSLSocket arg1) throws IOException
        {
            return;
        }

        @Override
        public void verify(String arg0, X509Certificate arg1) throws SSLException
        {
            return;
        }

        @Override
        public void verify(String arg0, String[] arg1, String[] arg2) throws SSLException
        {
            return;
        }
    }
    
    public static X509Certificate[] getServerCertificateChain(String host, int port) throws Exception
    {
        // Log.log.debug("In getServerCertificateChain with host & port:" + host
        // + ":" + port);
        X509Certificate[] chain;
        try
        {
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            SSLContext context = SSLContext.getInstance("TLS");
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(ks);
            X509TrustManager defaultTrustManager = (X509TrustManager) tmf.getTrustManagers()[0];
            SavingTrustManager tm = new SavingTrustManager(defaultTrustManager);
            context.init(null, new TrustManager[] { tm }, null);
            SSLSocketFactory factory = context.getSocketFactory();

            SSLSocket socket = (SSLSocket) factory.createSocket(host, port);
            socket.setSoTimeout(10000);
            try
            {
                socket.startHandshake();
                socket.close();
            }
            catch (SSLException e)
            {
                // this is fine, we know that there is no certificate and we're
                // going to retrieve it.
                // Log.log.error("SSLException:" + e.getMessage());
            }
            catch (IOException e)
            {
                Log.log.error("IOException:" + e.getMessage());
            }

            chain = tm.chain;
            if (chain == null)
            {
                throw new Exception("Something wrong, couldn't retrieve certificate chain.");
            }
        }
        catch (KeyStoreException e)
        {
            throw new Exception("KeyStoreException: " + e.getMessage());
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new Exception("NoSuchAlgorithmException: " + e.getMessage());
        }
        catch (KeyManagementException e)
        {
            throw new Exception("KeyManagementException: " + e.getMessage());
        }
        catch (Throwable e)
        { // openConnection() failed
            if (e.getCause() == null)
            {
                throw new Exception(e.getMessage());
            }
            else
            {
                throw new Exception(e.getCause().getMessage());
            }
        }

        return chain;
    }

    private static class SavingTrustManager implements X509TrustManager
    {

        private final X509TrustManager tm;
        private X509Certificate[] chain;

        SavingTrustManager(X509TrustManager tm)
        {
            this.tm = tm;
        }

        public X509Certificate[] getAcceptedIssuers()
        {
            throw new UnsupportedOperationException();
        }

        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException
        {
            throw new UnsupportedOperationException();
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException
        {
            this.chain = chain;
            tm.checkServerTrusted(chain, authType);
        }
    }

    /**
     * This method initiates a dynamic KeyStore and SSLContext to be used by SSL
     * communication.
     *
     * @param httpUrl
     * @return
     * @throws Exception
     */
    public static SSLContext getSSLContext(URL httpsUrl) throws Exception
    {
        try
        {
            KeyStore ks;
            ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(null);

            int port = httpsUrl.getDefaultPort();
            if (httpsUrl.getPort() != -1)
            {
                port = httpsUrl.getPort();
            }
            X509Certificate[] chain = getServerCertificateChain(httpsUrl.getHost(), port);
            for (X509Certificate cert : chain)
            {
                // add the certificates with a randomly generated alias.
                ks.setCertificateEntry(UUID.randomUUID().toString(), (Certificate) cert);
                Log.log.debug("PubKey:" + cert.getSubjectDN().getName());
            }

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(ks);

            X509TrustManager defaultTrustManager = (X509TrustManager) tmf.getTrustManagers()[0];
            SavingTrustManager tm = new SavingTrustManager(defaultTrustManager);

            SSLContext context = SSLContext.getInstance("TLS");
            // SSLContext context = SSLContext.getInstance("SSL");
            context.init(null, new TrustManager[] { tm }, null);
            return context;
        }
        catch (SSLHandshakeException e)
        {
            Log.log.error("SSLHandshake problem, " + e.getMessage());
            throw new Exception("SSL Handshake problem.");
        }
        catch (java.io.IOException e)
        { // openConnection() failed
            Log.log.error("Verify failed, couldn't connect to the URL, please check the server name, port and make sure that it's running, " + e.getMessage());
            throw new Exception("Verify failed, couldn't connect to the URL, please check the server name, port and make sure that it's running.");
        }
        catch (Throwable e)
        { // openConnection() failed
            if (e.getCause() == null)
            {
                throw new Exception(e.getMessage());
            }
            else
            {
                throw new Exception(e.getCause().getMessage());
            }
        }
    }

    public static String sendOverSSL(URL url, int timeout) throws Exception
    {
        StringBuilder result = new StringBuilder();
        try
        {
            SSLContext context = getSSLContext(url);
            SSLSocketFactory factory = context.getSocketFactory();
            Socket socket = factory.createSocket(url.getHost(), url.getPort());
            socket.setSoTimeout(timeout * 1000);

            try
            {
                Writer out = new OutputStreamWriter(socket.getOutputStream(), "ISO-8859-1");
                String file = "/";
                if (!(url.getFile().equals(""))) file = url.getFile();
                // out.write("Host: " + url.getHost() + ":" + url.getPort() +
                // "\r\n");
                out.write("GET " + file + " HTTP/1.0\r\n");
                out.write("Accept: text/plain, text/html, text/*\r\n");
                out.write("\r\n");
                out.flush();

                // BufferedReader in = new BufferedReader(new
                // InputStreamReader(socket.getInputStream(), "ISO-8859-1"));
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = in.readLine()) != null)
                {
                    result.append(line).append("\n");
                }
                in.close();
            }
            finally
            {
                socket.close();
            }
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        }
        return result.toString();
    }

    /**
     * Imports SSL certificate from the host and store in the keyStore by
     * extracting host and port from the incoming URL.
     *
     * @param keyStore
     * @param keyStorePassword
     * @param httpsUrl
     * @return
     * @throws Exception
     */
    public static boolean importCertificate(File keyStore, String keyStorePassword, URL httpsUrl) throws Exception
    {
        int port = httpsUrl.getDefaultPort();
        if (httpsUrl.getPort() != -1)
        {
            port = httpsUrl.getPort();
        }
        return importCertificate(keyStore, keyStorePassword, httpsUrl.getHost(), port);
    }

    /**
     * Imports SSL certificate from the host and store in the keyStore.
     *
     * @param keyStore
     * @param k_eyStoreP_assword
     * @param host
     * @param port
     * @return
     */
    public static boolean importCertificate(File keyStore, String k_eyStoreP_assword, String host, int port) throws Exception
    {
        boolean result = false;

        try
        {
            FileInputStream fis = new FileInputStream(keyStore);
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(fis, k_eyStoreP_assword.toCharArray());
            fis.close();

            // actual host name in SSL context may be different, hence we need
            // to use that.
            String actualHostName = getServerName(host, port);
            System.out.println("Actual host name is :" + actualHostName);

            X509Certificate[] chain = getServerCertificateChain(host, port);
            for (X509Certificate cert : chain)
            {
                if (cert.getSubjectDN().getName().toLowerCase().contains(host.toLowerCase()) || host.toLowerCase().contains(actualHostName))
                {
                    String alias = "Resolve_Import_" + actualHostName;
                    ks.setCertificateEntry(alias, (Certificate) cert);
                    Log.log.info("Actual host name is :" + actualHostName);
                    Log.log.info("Alias name :" + alias);
                    Log.log.info("PubKey:" + cert.getSubjectDN().getName());
                }
            }

            FileOutputStream fos = new FileOutputStream(keyStore);
            ks.store(fos, k_eyStoreP_assword.toCharArray());
            fos.close();
            result = true;
        }
        catch (FileNotFoundException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
        catch (KeyStoreException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
        catch (NoSuchAlgorithmException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
        catch (CertificateException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
        return result;
    }

    /**
     * This method connects to the host through SSL and retrieves the SSL
     * certificate and extract the server name from it.
     *
     * @param url
     * @return
     * @throws Exception
     */
    public static String getServerName(String url) throws Exception
    {
        String serverName = null;
        try
        {
            URL u = new URL(url);
            serverName = u.getHost();
            int port = u.getPort();
            Log.log.debug("Protocol:" + u.getProtocol());
            Log.log.debug("Port:" + port);
            return getServerName(serverName, port);
        }
        catch (Exception e)
        {
            throw new Exception(e.getCause().getMessage());
        }
        catch (Throwable e)
        { // openConnection() failed
            if (e.getCause() == null)
            {
                throw new Exception(e.getMessage());
            }
            else
            {
                throw new Exception(e.getCause().getMessage());
            }
        }
    }

    /**
     * This method connects to the host through SSL and retrieves the SSL
     * certificate and extract the server name from it.
     *
     * @param host
     * @param port
     * @return
     * @throws Exception
     */
    public static String getServerName(String host, int port) throws Exception
    {
        String serverName = null;
        try
        {
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            SSLContext context = SSLContext.getInstance("TLS");
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(ks);
            X509TrustManager defaultTrustManager = (X509TrustManager) tmf.getTrustManagers()[0];
            SavingTrustManager tm = new SavingTrustManager(defaultTrustManager);
            context.init(null, new TrustManager[] { tm }, null);
            SSLSocketFactory factory = context.getSocketFactory();

            SSLSocket socket = (SSLSocket) factory.createSocket(host, port);
            socket.setSoTimeout(60000);
            try
            {
                socket.startHandshake();
                socket.close();
            }
            catch (SSLException e)
            {
                // this is fine, we know that there is no certificate and we're
                // going to retrieve it.
                Log.log.info("SSLException, tis is fine we're going o import it in the next step:" + e.getMessage());
            }
            catch (IOException e)
            {
                Log.log.error("IOException:" + e.getMessage());
            }

            X509Certificate[] chain = tm.chain;
            if (chain == null)
            {
                throw new Exception("Server:" + host + " at port:" + port + " did not return the certificate chain. " + "\nPlease make sure that you have used a signed SSL certificate and follow your server manual to see how to properly configure SSL.");
            }

            // get the first record.
            if (chain.length > 0)
            {
                X509Certificate cert = chain[0];
                // System.out.println(cert.getSubjectDN().getName());
                // System.out.println(cert.toString());
                // if
                // (cert.getSubjectDN().getName().toLowerCase().contains(host.toLowerCase()))
                // {
                String[] splitted = cert.getSubjectDN().getName().split(",");
                serverName = splitted[0].substring(3);
                // break;
                // }
            }
        }
        catch (KeyStoreException e)
        {
            throw new Exception("KeyStoreException: " + e.getMessage());
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new Exception("NoSuchAlgorithmException: " + e.getMessage());
        }
        catch (KeyManagementException e)
        {
            throw new Exception("KeyManagementException: " + e.getMessage());
        }
        catch (UnknownHostException e)
        {
            throw new Exception("Inaccessible host : " + host + ", please check.");
        }
        catch (IOException e)
        {
            throw new Exception("IOException: " + e.getMessage());
        }
        catch (Throwable e)
        { // openConnection() failed
            if (e.getCause() == null)
            {
                throw new Exception(e.getMessage());
            }
            else
            {
                throw new Exception(e.getCause().getMessage());
            }
        }

        return serverName;
    }

    /**
     * This method provides a {@link SSLSocketFactory} which accepts any
     * SSL certificate from any server.
     *
     * @return
     */
    public static SSLSocketFactory getSSLSocketFactory() throws Exception
    {
        SSLSocketFactory sslSocketFactory = null;

        TrustManager[] trustManager = new TrustManager[] { new X509TrustManager()
        {
            public X509Certificate[] getAcceptedIssuers()
            {
                return null;
            }
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException
            {
            }
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException
            {
            }
        }};

        SSLContext sslContext = null;
        try {
            sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustManager, new SecureRandom());
        } catch (NoSuchAlgorithmException e) {
            throw new Exception(e.getMessage(), e);
        } catch (KeyManagementException e) {
            throw new Exception(e.getMessage(), e);
        }

        sslSocketFactory = sslContext.getSocketFactory();
        return sslSocketFactory;
    }

    public static boolean isSSL(String url) throws MalformedURLException
    {
        URL url1 = new URL(url);
        return url1.getProtocol().equals("https");
    }

    public static int getPort(String url) throws MalformedURLException
    {
        URL url1 = new URL(url);
        return url1.getPort();
    }

    /**
     * This main method is required for the sslutil.sh and sslutil.bat in rsremote/bin 
     * in order to import SSL certificate from external server. 
     * 
     * IMPORTANT: DO NOT comment this out.
     *   
     * @param args
     */
    public static void main(String... args)
    {
        if (args.length < 4)
        {
            System.out.println("Usage: SSLUtils <keystore> <keystore_pwd> <https_host> <https_port>");
            System.out.println("This utility imports SSL certificate from the host");
            System.exit(1);
        }
        Log.init();
        File keystore = FileUtils.getFile(args[0]);
        String keystorePwd = args[1];
        String host = args[2];
        int port = Integer.valueOf(args[3]);
        System.out.printf("Incoming parameters:\n");
        System.out.printf("  Keystore: %s\n", keystore.getName());
        System.out.printf("  Host: %s\n", host);
        System.out.printf("  Port: %d\n", port);
        try
        {
            boolean result = importCertificate(keystore, keystorePwd, host, port);
            if (result)
            {
                System.out.printf("Successfully imported SSL certificate from host %s on port %d into %s \n\n", host, port, keystore);
            }
        }
        catch (Exception e)
        {
            System.out.printf("Error during importing SSL certificate into %s from host %s on port %d\n\n", keystore, host, port);
        }
    }

} // SSLUtils
