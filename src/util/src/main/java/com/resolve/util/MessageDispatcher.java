/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import com.resolve.esb.ESBException;
import com.resolve.esb.MMsgHeader;
import com.resolve.rsbase.MainBase;
import com.resolve.thread.ScheduledExecutor;

public class MessageDispatcher
{
	private static final String CONNECTION_REFUSED_ERROR = "connection refused";
	
    // Flag to stop MessageDispatcher thread
    static public volatile boolean stop;

    // Default sleep interval in milliseconds when no event on queue
    static long sleepInterval = 2000;
    final static MMsgHeader msgHeader = new MMsgHeader();

    // Default time out is 15 minutes
    private static long defaultSendMessageTimeOut = 15 * 60 * 1000;
    public static int WAITING_TASK_THRESHOLD = 40; // if more than
                                                   // WAITING_TASK_THRESHOLD
                                                   // tasks waiting on executor
                                                   // queue, rsremote is
                                                   // "overloaded"

    // Default queue size
    private static int HIGH_QUEUE_SIZE = 32;
    private static int LOW_QUEUE_SIZE = 32;
    private static int NORMAL_QUEUE_SIZE = 1024;
    public static long MAXIMUM_EVENT_WAITING_TIME = 0; // '0' means wait forever
                                                       // on queue for
                                                       // processing. If it is
                                                       // not 0, after
                                                       // MAXIMUM_EVENT_WAITING_TIME
                                                       // passes, event will be
                                                       // dropped.
    public static int MESSAGE_BATCH_NUMBER = 200;
    public static long MESSAGE_BATCH_INTERVAL = 500;
    public static int MAXIMUM_RUNNING_RUNBOOKS = 500;
    public static long RSCONTROL_REPORT_TIME_WINDOW = 30 * 1000; // 30 seconds
    
    public static AtomicLong messageCounter = new AtomicLong(0l);;
    private static BlockingQueue<Message<?, ?>> high;

    private static BlockingQueue<Message<?, ?>> normal;

    private static BlockingQueue<Message<?, ?>> low;

    private static AtomicLong highestCompletedEventSerial = new AtomicLong();

    private static int eventQueueLimit = 1000; 
    
    public static void init() throws Exception
    {
        high = new LinkedBlockingQueue<Message<?, ?>>();
        low = new LinkedBlockingQueue<Message<?, ?>>();
        normal = new LinkedBlockingQueue<Message<?, ?>>();
        msgHeader.put(Constants.ESB_PARAM_EVENTTYPE, "RSVIEW");
        msgHeader.setRouteDest(Constants.ESB_NAME_EXECUTEQUEUE, "MEvent.eventResult");
        eventQueueLimit = MainBase.main.configESB.config.getEventQueueLimit();
        new Thread(new Dispatcher()).start();
    }

    private static boolean isEventQueueOverLimit()
    {
        boolean b = false;
        
        int count = MainBase.getESB().getMServer().getMessageCount(Constants.ESB_NAME_EXECUTEQUEUE);
        if(count > eventQueueLimit)
        {
            b = true;
        }
        
        if (!b)
        {
            count = MainBase.getESB().getMServer().getMessageCount(Constants.ESB_NAME_DURABLE_EXECUTEQUEUE);
            
            if(count > eventQueueLimit)
            {
                b = true;
            }
        }
        
        return b;
    }
    
    
//    public static void updateRSControlState(Map<String, String> msg)
//    {
//
//        Log.log.debug("Flow control update from rscontrol: msg=" + msg);
//        rscontrolLoadStates.put(msg.get(Constants.TARGET_GUID), new RSState(Boolean.parseBoolean(msg.get("OVERLOADED")), System.currentTimeMillis()));
//
//        String eventGuid = msg.get(Constants.FLOW_CONTROL_ID);
//        if (MainBase.main.configId.guid.equals(eventGuid))
//        {
//            long eventSerial = Long.valueOf(msg.get(FLOW_CONTROL_SERIAL));
//            if (eventSerial >= highestCompletedEventSerial.get())
//            {
//                highestCompletedEventSerial.set(eventSerial);
//            }
//
//            if (eventSerial > messageCounter.get())
//            {
//                messageCounter.set(eventSerial);
//            }
//        }
//    }

    public static <T, S> boolean sendMessage(Map params) throws Exception
    {
        GatewayEvent<String, String> ge = new GatewayEvent<String, String>(System.currentTimeMillis(), params);
        return sendMessage(ge);
    }

    public static <T, S> boolean sendMessage(GatewayEvent<T, S> content) throws Exception
    {
        return sendMessage(msgHeader, content, Priority.NORMAL, defaultSendMessageTimeOut);
    }

    // this method works like a blocking call because of long waiting time
    public static <T, S> boolean sendMessage(MMsgHeader header, GatewayEvent<T, S> content) throws Exception
    {
        return sendMessage(header, content, Priority.NORMAL, defaultSendMessageTimeOut);
    }

    // 'timeOut' (in milliseconds) means how long the caller is willing to be
    // blocked for message to be inserted into to global queue
    public static <T, S> boolean sendMessage(MMsgHeader header, GatewayEvent<T, S> content, long timeOut) throws Exception
    {
        return sendMessage(header, content, Priority.NORMAL, timeOut);
    }

    public static <T, S> boolean sendMessage(MMsgHeader header, GatewayEvent<T, S> content, Priority priority, long timeOut) throws Exception
    {
        Message<?, ?> msg = new Message<T, S>(header, content);

        boolean result = false;
        switch (priority)
        {
            case HIGH:
                while(high.size()>=HIGH_QUEUE_SIZE)
                {
                    Object o = high.poll();
                    if (o!=null)
                    {
                        Map m = ((Message)o).content.content;
                        Log.log.warn("Dispatcher high queue is full. Drop the oldest event from queue: " + m);
                    }
                }
                result = high.offer(msg, timeOut, TimeUnit.MILLISECONDS);
                break;

            case LOW:
                while(low.size()>=LOW_QUEUE_SIZE)
                {
                    Object o = low.poll();
                    if (o!=null)
                    {
                        Map m = ((Message)o).content.content;
                        Log.log.warn("Dispatcher low queue is full. Drop the oldest event from queue: " + m);
                    }
                }
                result = low.offer(msg, timeOut, TimeUnit.MILLISECONDS);
                break;

            // default will fall to NORMAL bucket
            default:
                while(normal.size()>=NORMAL_QUEUE_SIZE)
                {
                    Object o = normal.poll();
                    if (o!=null)
                    {
                        Map m = ((Message)o).content.content;
                        Log.log.warn("Dispatcher normal queue is full. Drop the oldest event from queue: " + m);
                    }
                }
                result = normal.offer(msg, timeOut, TimeUnit.MILLISECONDS);
                break;
        }

        return result;
    }

    private static class Dispatcher implements Runnable
    {
        private boolean queueOverloaded(ScheduledExecutor e)
        {
            // All threads in thread pool are busy and we have more than
            // WAITING_TASK_THRESHOLD tasks on queue.
            //return e.getPoolSize() == e.getCorePoolSize() && ((e.getTaskCount() - e.getCompletedTaskCount()) > WAITING_TASK_THRESHOLD);
        	// removing check as it looks like with enough gateways getTaskCount is higher than the default WAITING_TASK_THRESHOLD
        	return false;
        }

        private boolean hasTooMuchLoad()
        {
//            if (messageCounter.get() - highestCompletedEventSerial.get() > MAXIMUM_RUNNING_RUNBOOKS)
//            {
//                return true;
//            }
            if (isEventQueueOverLimit())
            {
                Log.log.debug("Request queue is over the limit. RSControls probably overloaded");
                return true;
            }
                
//        	if (RSControlOverloaded())
//        	{
//        		Log.log.debug("RSControl is overloaded. Stop sending events.");
//        		return true;
//        	}
        	
            // If rscontrols are not overloaded, check local rsremote executor
            // queue next
            ScheduledExecutor e = ScheduledExecutor.getInstance();
            // Log.log.debug("PoolSize=" + e.getPoolSize() + ", CorePoolSize=" +
            // e.getCorePoolSize() + ", TaskCount=" + e.getQueue().size());
            // Log.log.debug("CompletedTaskCount=" + e.getCompletedTaskCount() +
            // ", AllTaskCount=" + e.getTaskCount());

            // Double check is more efficient
            if (queueOverloaded(e))
            {
                // Only call purge() if it is overloaded
                e.purge();
                if (queueOverloaded(e))
                {
                    Log.log.debug("Local gateway is overloaded");
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /*
         * 
         * public static void updateRSControlState(String guid, boolean b, long
         * t) { rscontrolLoadStates.put(guid, new RSState(b, t));
         * Log.log.debug("Received rscontrol flow control message: overloaded ="
         * + b); }
         * 
         * 
         * private static Map<String, RSState> rsviewLoadStates = new
         * ConcurrentHashMap<String, RSState>(); public static void
         * updateRSViewState(String guid, boolean b, long t) {
         * rsviewLoadStates.put(guid, new RSState(b, t));
         * Log.log.debug("Received rsview flow control message: overloaded =" +
         * b); }
         * 
         * public static long RSCONTROL_RESPONSE_TIME = 5 * 60 * 1000; //
         * default is 5 minutes, if not receive flow status from a rscontrol for
         * this time, it is considered down. public static long
         * RSCONTROL_MESSAGE_TIME = 1000; // default is 1000ms, only flow
         * control status message received in the last 1000ms will be considered
         * valid.
         * 
         * private boolean hasTooMuchLoad() { // Check if any rscontrol is
         * overloaded first long now = System.currentTimeMillis(); RSState st =
         * null; if (!rscontrolLoadStates.isEmpty()) { int targetCount =
         * rscontrolLoadStates.keySet().size(); //how many rscontrols are
         * running now. int realCount = 0; // how many rscontrols are not
         * overloaded for (String key : rscontrolLoadStates.keySet()) { st =
         * rscontrolLoadStates.get(key); if (now - st.updateTime <
         * RSCONTROL_RESPONSE_TIME) // If doesn't // receive update from
         * rscontrol // for more than // 5*60 seconds, we // think this //
         * rscontrol is // down and remove // it from the // list. { if
         * (!st.overloaded && ((now - st.updateTime) < RSCONTROL_MESSAGE_TIME))
         * // Make // sure // we // only // count // the // latest // status //
         * update from rscontrol. // RScontrol // might // be // too // busy //
         * to // send // out // any // status // update for a while because it
         * might be too busy running other tasks // , so we need to make sure
         * not use the stale rscontrol status data // again and again in our
         * flow control decision. { realCount++; } } else // remove outdated
         * status entry, because rscontrol might be down. {
         * rscontrolLoadStates.remove(key); } }
         * 
         * if (realCount < targetCount) {
         * Log.log.debug("RSControl is overloaded"); return true; } } else // if
         * it is empty, it means there is no rscontrol is running, so it doesn't
         * make sense to send any message to // rscontrol { return true; }
         * 
         * // If rscontrols are ok, check local executor queue next Executor e =
         * Executor.getExecutor(); Log.log.debug("PoolSize=" + e.getPoolSize() +
         * ", CorePoolSize=" + e.getCorePoolSize() + ", TaskCount=" +
         * e.getQueue().size()); Log.log.debug("CompletedTaskCount=" +
         * e.getCompletedTaskCount() + ", AllTaskCount=" + e.getTaskCount());
         * 
         * // Double check is more efficient because of purge() call is
         * expensive if (queueOverloaded(e)) { // Only call purge() if it is
         * overloaded e.purge(); if (queueOverloaded(e)) { return true; } else {
         * return false; } } else { return false; } }
         */

        public void run()
        {
            Message<?, ?> msg = null;
            Log.log.info("Message Dispatch thread starts");
            int count = 0;
            int failureCount = 0;
            while (!stop)
            {
	            try
	            {
                    int sleepTimes = 0;
                    while (hasTooMuchLoad())
                    {
                        // Log.log.debug("Overloaded. Go to sleep and yield.");
                        Thread.sleep(sleepInterval);

                        sleepTimes++;
                        // If dispatcher sleeps more than 5 minutes, we need to
                        // send an event out to get things moving,
                        // just in case both rscontrols died or restarted at
                        // some points and all events
                        // count were lost.
                        if (sleepTimes > 150) break;
                    }

                    if (!high.isEmpty())
                    {
                        msg = high.take();
                    }
                    else if (!normal.isEmpty())
                    {
                        msg = normal.take();
                    }
                    else if (!low.isEmpty())
                    {
                        msg = low.take();
                    }
                    else
                    {
                        Log.log.trace("All message queues are empty. Go to sleep.");
                        Thread.sleep(sleepInterval);
                    }

                    if (msg != null)
                    {
                        if (canDropMessage(msg))
                        {
                            Log.log.warn("Event timed out in global event queue. Drop event: timestamp=" + msg.content.timeStamp + ", content=" + msg.content.content);
                        }
                        else
                        {

                            // Send event serial number which will be used for
                            // flow control
                            Map<String, String> msgMap = (Map<String, String>) msg.content.content;
                            // messageCounter++;
                            messageCounter.incrementAndGet();

                            MainBase.esb.sendMessage(msg.header, msg.content.content);
                            Log.log.trace("Sent new message: header = " + msg.header + ", waiting time=" + (System.currentTimeMillis() - msg.content.timeStamp) + ", content = " + msg.content.content);
                            Log.log.trace("Message counter = " + messageCounter);

                            count++;
                            if (count > MESSAGE_BATCH_NUMBER)
                            {
                                count = 0;
                                Thread.sleep(MESSAGE_BATCH_INTERVAL); // take a
                                                                      // break
                                                                      // after
                                                                      // sending
                                // MESSAGE_BATCH_NUMBER events, try not to send
                                // too many events to
                                // rscontrol in very short time.
                            }
                            msg = null;
                        }

                    }
                    failureCount = 0;
                }
	            catch (Throwable ex)
	            {
	            	if (!(ex instanceof IOException || 
	            		  (ex.getCause() != null && 
	            		   (ex.getCause() instanceof IOException || 
	            		    (ex.getCause() instanceof ESBException && 
	            			 StringUtils.isNotBlank(ex.getCause().getMessage()) &&
	            			 (ex.getCause().getMessage().toLowerCase().contains(
	            					 ESBException.ESB_COMPONENT_SHUTTING_DONW_ERR_MSG.toLowerCase()) ||
	            			  ex.getCause().getMessage().toLowerCase().contains(
	            					 ESBException.ESB_CONNECTION_CLOSED_ERR_MSG.toLowerCase()) ||
	            			  ex.getCause().getMessage().toLowerCase().contains(
		            					 ESBException.ESB_CONNECTION_REFUSED_ERR_MSG.toLowerCase()))))))) { 
		                Log.log.info("Message Dispatcher had an exception.", ex);
		                if (!stop)
		                {
		                	failureCount++;
		                	if (failureCount >= 20)
		                	{
		                		Log.alert(ERR.E40001, "Message Dispatcher Connection has Failed");
		                		stop = true;
		                	}
		                	else
		                	{
			                	try
			                	{
				                    Thread.sleep(MESSAGE_BATCH_INTERVAL * 2); // wait a second for connection to re-establish
			                	}
			                	catch (InterruptedException ie)
			                	{
			                		//do nothing
			                	}
		                	}
		                }
	            	} else {
	            		if (!stop) {
	            			try {
	            				// wait for 2 seconds for connection to re-establish
			                    Thread.sleep(MESSAGE_BATCH_INTERVAL * 4); 
		                	} catch (InterruptedException ie) {
		                		//safe to ignore
		                	}
	            		}
	            	}
	            }
            }
            Log.log.info("Message Dispatcher stopped. Stop: " + stop);
        }

        boolean canDropMessage(Message<?, ?> m)
        {
            if (MAXIMUM_EVENT_WAITING_TIME == 0)
            {
                // event never time out
                return false;
            }

            if ((System.currentTimeMillis() - m.content.timeStamp) > MAXIMUM_EVENT_WAITING_TIME)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public enum Priority
    {
        HIGH, NORMAL, LOW;
    }

    private static class Message<T, S>
    {
        MMsgHeader header;
        GatewayEvent<T, S> content;

        Message(MMsgHeader h, GatewayEvent<T, S> m)
        {
            header = h;
            content = m;
        }
    }

}
