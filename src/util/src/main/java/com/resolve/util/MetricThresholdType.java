/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.Serializable;

public class MetricThresholdType implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = -6660072395884776232L;
    private String high;
    private String low;
    private String alertstatus;
    
    public MetricThresholdType()
    {
        
    } // MetricThresholdType
    
    
    public String toString()
    {
        return "high: "+high+" low: "+low+" alert status: "+alertstatus;
    }
    
    
    public MetricThresholdType(String high, String low, String alertstatus)
    {
        this.high = high;
        this.low = low;
        this.setAlertstatus(alertstatus);
        
    } // MetricThresholdType
    
    public String getHigh()
    {
        return high;
    } //getHigh
        
    public void setHigh(String high)
    {
        this.high = high;
    } //setHigh
    
    public String getLow()
    {
        return low;
    } //getLow
    
    public void setLow(String low)
    {
        this.low = low;
    } //setLow

    /**
     * @return the alertstatus
     */
    public String getAlertstatus()
    {
        return alertstatus;
    }

    /**
     * @param alertstatus the alertstatus to set
     */
    public void setAlertstatus(String alertstatus)
    {
        this.alertstatus = alertstatus;
    }

} // MetricThresholdType

