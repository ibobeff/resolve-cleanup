/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * This is a utility class that can be used to communicate to any REST web service. By default
 * it supports json content.
 * 
 */
public class RESTUtils
{
    private static DefaultHttpClient getHttpClient(HttpRequestBase httpRequest, String username, String password)
    {
        DefaultHttpClient httpClient = new DefaultHttpClient();

        if (StringUtils.isNotBlank(username))
        {
            String host = httpRequest.getURI().getHost();
            int port = httpRequest.getURI().getPort();

            String basicAuth = Base64.encodeBytes((username + ":" + password).getBytes());
            httpRequest.setHeader("Authorization", "Basic " + basicAuth);

            httpClient.getCredentialsProvider().setCredentials(new AuthScope(host, port), new UsernamePasswordCredentials(username, password));
        }

        return httpClient;
    }

    /**
     * Validates if the REST web service is running by verifying the connection
     * to the URL.
     * 
     * @param url
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    public static boolean isExists(String url, String username, String password) throws Exception
    {
        boolean result = false;

        if (StringUtils.isNotBlank(url))
        {
            HttpGet httpGet = new HttpGet(url);

            DefaultHttpClient httpClient = getHttpClient(httpGet, username, password);

            HttpResponse response = httpClient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == 200)
            {
                result = true;
            }
        }

        return result;
    }

    public static String get(String url, String username, String password) throws Exception
    {
        StringBuilder result = new StringBuilder();

        if (StringUtils.isNotBlank(url))
        {
            HttpGet httpGet = new HttpGet(url);
            if (isExists(url, username, password))
            {
                DefaultHttpClient httpClient = getHttpClient(httpGet, username, password);
                HttpResponse response = httpClient.execute(httpGet);

                if (response != null && response.getStatusLine() != null && response.getStatusLine().getStatusCode() == 200)
                {
                    HttpEntity resEntity = response.getEntity();
                    if (resEntity != null)
                    {
                        result.append(StringUtils.toString(resEntity.getContent(), "UTF-8"));
                    }
                }
                else
                {
                    throw new Exception("Unfortunately REST GET request failed for totally unknown reason.");
                }
            }
            else
            {
                throw new Exception("REST end point does not exists.");
            }
        }

        return result.toString();
    }

    public static int put(String url, String json, String username, String password) throws Exception
    {
        return put(url, "application/json", json, username, password);
    }

    public static int put(String url, Map<String, String> params, String username, String password) throws Exception
    {
        return put(url, "application/json", params, username, password);
    }

    public static int put(String url, String contentType, Map<String, String> params, String username, String password) throws Exception
    {
        int result = 0;

        StringBuilder inputString = new StringBuilder();
        if ("application/json".equals(contentType))
        {
            if (params != null && params.size() > 0)
            {
                inputString.append("{");
                for (String key : params.keySet())
                {
                    inputString.append("\"" + key + "\":\"" + params.get(key) + "\",");
                }
                inputString.setLength(inputString.length() - 1);
                inputString.append("}");
                result = put(url, contentType, inputString.toString(), username, password);
            }
        }

        return result;
    }

    public static int put(String url, String contentType, String content, String username, String password) throws Exception
    {
        int result = 0;

        if (StringUtils.isNotBlank(url))
        {
            HttpPut httpPut = new HttpPut(url);
            httpPut.addHeader("Content-Type", contentType);

            DefaultHttpClient httpClient = getHttpClient(httpPut, username, password);

            // curl -i -v -u guest:guest -H ""content-type:application/json""
            // -XPUT -d"{""password"":""secret"",""tags"":""administrator""}"
            // http://localhost:15672/api/users/newUser
            HttpEntity input = new StringEntity(content);
            httpPut.setEntity(input);

            HttpResponse response = httpClient.execute(httpPut);
            if (response != null && response.getStatusLine() != null)
            ;
            {
                result = response.getStatusLine().getStatusCode();
                switch (result)
                {
                    case 401:
                        throw new Exception("Unauthorized, check the username and password.");
                    case 404:
                        throw new Exception("Invalid URL.");
                    case 500:
                        throw new Exception("Internal REST server error.");
                    default:
                        break;
                }
            }
        }

        return result;
    }

    public static int post(String url, String content, String username, String password) throws Exception
    {
        return post(url, "application/json", content, username, password);
    }

    public static int post(String url, Map<String, String> params, String username, String password) throws Exception
    {
        return post(url, "application/json", params, username, password);
    }

    public static int post(String url, String contentType, Map<String, String> params, String username, String password) throws Exception
    {
        int result = 0;

        StringBuilder inputString = new StringBuilder();
        if ("application/json".equals(contentType))
        {
            if (params != null && params.size() > 0)
            {
                inputString.append("{");
                for (String key : params.keySet())
                {
                    inputString.append("\"" + key + "\":\"" + params.get(key) + "\",");
                }
                inputString.setLength(inputString.length() - 1);
                inputString.append("}");
            }
        }

        result = post(url, contentType, inputString.toString(), username, password);

        return result;
    }

    public static int post(String url, String contentType, String content, String username, String password) throws Exception
    {
        int result = 0;

        if (StringUtils.isNotBlank(url))
        {
            HttpPost httpPost = new HttpPost(url);
            httpPost.addHeader("Content-Type", contentType);

            DefaultHttpClient httpClient = getHttpClient(httpPost, username, password);

            HttpEntity input = new StringEntity(content);
            httpPost.setEntity(input);

            HttpResponse response = httpClient.execute(httpPost);
            if (response != null && response.getStatusLine() != null)
            {
                result = response.getStatusLine().getStatusCode();
                switch (result)
                {
                    case 401:
                        throw new Exception("Unauthorized, check the username and password.");
                    case 404:
                        throw new Exception("Invalid URL.");
                    case 500:
                        throw new Exception("Internal REST server error.");
                    default:
                        break;
                }
            }
        }

        return result;
    }

    /**
     * Deletes an Object.
     * 
     * @param url
     * @param username
     * @param password
     * @throws Exception
     */
    public static int delete(String url, String username, String password) throws Exception
    {
        int result = 0;
        if (StringUtils.isNotBlank(url))
        {
            HttpDelete httpDelete = new HttpDelete(url);
            if (isExists(url, username, password))
            {
                DefaultHttpClient httpClient = getHttpClient(httpDelete, username, password);
                HttpResponse response = httpClient.execute(httpDelete);
                if (response != null && response.getStatusLine() != null)
                {
                    int status = response.getStatusLine().getStatusCode();
                    switch (status)
                    {
                        case 401:
                            throw new Exception("Unauthorized, check the username and password.");
                        case 404:
                            throw new Exception("Invalid URL.");
                        case 500:
                            throw new Exception("Internal REST server error.");
                        default:
                            result = status;
                            break;
                    }
                }
            }
            else
            {
                throw new Exception("REST end point does not exists.");
            }
        }
        
        return result;
    }
/*
    public static void main(String[] args) throws Exception
    {
        //test RabbitMQ REST web service.
        String username = "guest";
        String password = "guest";
        String rootUrl = "http://127.0.0.1:15672/api";

        //verify if the server is running or not.
        System.out.println("Does " + rootUrl + " exists: " + isExists(rootUrl, username, password));

        //get a list of RabbitMQ virtual hosts
        //URL looks like http://127.0.0.1:15672/api/vhosts
        String tmpUrl = rootUrl + "/vhosts";
        System.out.println("Get Virtual Hosts request result: " + get(tmpUrl, username, password));

        //get all users
        //URL looks like http://127.0.0.1:15672/api/users
        tmpUrl = rootUrl + "/users";
        System.out.println("Get All Users result: " + get(tmpUrl, username, password));

        //this is a new user we are going to create
        String newUsername = "majestic";
        String newPassword = "secret";

        //URL looks like http://127.0.0.1:15672/api/users/testuser
        tmpUrl = rootUrl + "/users/" + newUsername;
        Map<String, String> params = new HashMap<String, String>();
        params.put("password", newPassword);
        params.put("tags", "administrator");
        put(tmpUrl, params, username, password);

        // set permission
        // URL looks like http://127.0.0.1:15672/api/permissions/%2f/testuser
        // Note that the %2f is encoded value for the default host (/)
        tmpUrl = rootUrl + "/permissions/%2f/" + newUsername;

        params = new HashMap<String, String>();
        params.put("configure", ".*");
        params.put("write", ".*");
        params.put("read", ".*");
        put(tmpUrl, params, username, password);

        tmpUrl = rootUrl + "/users";
        String allUsers = get(tmpUrl, newUsername, newPassword);
        if(allUsers.contains(newUsername))
        {
            System.out.println("User created successfully");
        }
        
        tmpUrl = rootUrl + "/users/" + newUsername;
        delete(tmpUrl, username, password);

        tmpUrl = rootUrl + "/users";
        allUsers = get(tmpUrl, username, password);
        if(!allUsers.contains(newUsername))
        {
            System.out.println("User deleted successfully");
        }
    }
*/
}
