package com.resolve.util.metrics;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import com.google.gson.Gson;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;

public class ExecutionMetricsProcessor extends TimerTask
{   
    private final long measurementIntervalInMilliseconds;
    private ConcurrentHashMap<String, ExecutionMetricsDTO> statsMap;
    private final Object lock;
    private static boolean enableMetrics = initEnableMetrics();
    private static final String RESOLVE_METRICS_ENABLE_PROPERTY_NAME = "resolve.metrics.enable";
    private static final String DEFAULT_RESOLVE_METRICS_ENABLE_PROPERTY_VALUE = "false";

    private static boolean initEnableMetrics()
    {
        String enableMetricsPropVal = System.getProperty(RESOLVE_METRICS_ENABLE_PROPERTY_NAME, DEFAULT_RESOLVE_METRICS_ENABLE_PROPERTY_VALUE);
        
        return Boolean.parseBoolean(enableMetricsPropVal);
    }
    
    protected ExecutionMetricsProcessor(long measurementIntervalInMilliseconds)
    {
        this.statsMap = new ConcurrentHashMap<String, ExecutionMetricsDTO>();
        this.measurementIntervalInMilliseconds = measurementIntervalInMilliseconds;
        this.lock = new Object();
        Timer timer = new Timer();
        timer.schedule(this, 0, this.measurementIntervalInMilliseconds);
    }

    public void run()
    {
        synchronized (this.lock)
        {
            for (Map.Entry<String, ExecutionMetricsDTO> processStats : statsMap.entrySet())
            {
                if (processStats.getValue().getEventsCount() > 0)
                {
                    String json = calculateStatsJSON(processStats);
                    
                    try
                    {
                        Log.log.info(String.format("%s", enableMetrics ? json : CryptUtils.encrypt(json)));
                        Log.log.trace(String.format("%s", enableMetrics ? processStats : CryptUtils.encrypt(processStats.toString())));
                    }
                    catch (Exception e)
                    {
                        Log.log.error(e);
                    }
                    
                    statsMap.remove(processStats.getKey());
                }
            }
        }
    }

    private String calculateStatsJSON(Map.Entry<String, ExecutionMetricsDTO> processStats)
    {

        // processStats.getKey(), processStats.getValue().getEventsCount(),
        // processStats.getValue().getCumulativeDuration()
        double averageDurationInSeconds = (double) processStats.getValue().getCumulativeDuration() / processStats.getValue().getEventsCount() / 1e9;
        double cumulativeDirationInSeconds = (double) processStats.getValue().getCumulativeDuration() / 1e9;
        double executionsInSeconds = processStats.getValue().getEventsCount() / cumulativeDirationInSeconds * processStats.getValue().getThreadsCountMap().size();
        Map<String, Object> map = new LinkedHashMap<String, Object>(8);
        map.put("processName", processStats.getKey());
        map.put("measuringIntervalInSeconds", String.format("%s", measurementIntervalInMilliseconds / 1e3));
        map.put("executionsCount", String.format("%s", processStats.getValue().getEventsCount()));
        map.put("threadsCount", String.format("%s", processStats.getValue().getThreadsCountMap().size()));
        map.put("cumulativeDurationInSeconds", String.format("%.6f", processStats.getValue().getCumulativeDuration() / 1e9));
        map.put("averageDurationInSeconds", String.format("%.6f", averageDurationInSeconds));
        map.put("averageExecutionsPerSecond", String.format("%.3f", executionsInSeconds));
        Map<String, Map<String, Object>> mainMap = new HashMap<String, Map<String, Object>>(1);
        mainMap.put("Stats", map);
        Gson gson = new Gson();
        return gson.toJson(mainMap);
    }

    protected void registerEvent(String processName, String threadName, long durationInNanoseconds, int... predefinedExecutionsCount)
    {
        synchronized (this.lock)
        {
            ExecutionMetricsDTO processStats = null;
            if (this.statsMap.containsKey(processName))
            {
                processStats = this.statsMap.get(processName);
            }
            else
            {
                processStats = new ExecutionMetricsDTO();
                this.statsMap.put(processName, processStats);
            }
            if (predefinedExecutionsCount.length == 1 && predefinedExecutionsCount[0] > 1)
            {
                processStats.eventsCountAdd(predefinedExecutionsCount[0]);
            }
            else
            {
                processStats.eventsCountIncrement();
            }
            processStats.threadsCountIncrement(threadName);
            processStats.cumulativeDurationAdd(durationInNanoseconds);
        }
    }
}
