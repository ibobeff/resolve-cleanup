package com.resolve.util.eventsourcing.incident.resolution;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include = Inclusion.NON_NULL)
public class IncidentResolutionCreatedData extends IncidentResolutionData
{
    private String content;
    private String type;
    
    public IncidentResolutionCreatedData(String incidentId, String rootResolutionId, String content, String type)
    {
        super(incidentId, rootResolutionId);
        this.content = content;
        this.type = type;
    }
    
    public String getContent()
    {
        return content;
    }
    public void setContent(String content)
    {
        this.content = content;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    public String getType()
    {
        return type;
    }
    

}
