/******************************************************************************
* (C) Copyright 2016
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class DigestUtils extends org.apache.commons.codec.digest.DigestUtils
{
	/**
     * @return true iff the first 16 bytes of both hash1 and hash2 are equal;
     *         both hash1 and hash2 are null; or either hash array is less than
     *         16 bytes in length and their lengths and all of their bytes are
     *         equal.
     **/
    public static boolean hashesEqual(byte[] hash1, byte[] hash2)
    {
        if (hash1 == null) return hash2 == null;
        if (hash2 == null) return false;
        int targ = 16;
        if (hash1.length < 16)
        {
            if (hash2.length != hash1.length) return false;
            targ = hash1.length;
        }
        else if (hash2.length < 16)
        {
            return false;
        }
        for (int i = 0; i < targ; i++)
        {
            if (hash1[i] != hash2[i]) return false;
        }
        return true;
    }
    
    /**
     * Calculates and returns the hash of the contents of the given file.
     **/
    @Deprecated
    public static byte[] getHash(File f) throws IOException
    {
    	if (!f.exists()) throw new FileNotFoundException(f.toString());
    	return md5(new FileInputStream(f));
    }
    
    @Deprecated
    public static String asHex(byte hash[])
    {
    	return md5Hex(hash);
    }
}
