package com.resolve.util;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class ResolveHttpServlet extends HttpServlet implements Serializable
{
    private static final long serialVersionUID = -4844451589836786654L;

    protected final void doTrace(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Log.log.debug(request.getRequestURI());
        response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
    } // doTrace()
} // end of class ResolveHttpServlet
