package com.resolve.util.cef;

public class CEFParseException extends Exception {
	private static final long serialVersionUID = -652681560663603339L;

	public CEFParseException() {}

    public CEFParseException(String message) {
        super(message);
    }

    public CEFParseException(String message, Throwable cause) {
        super(message,cause);
    }
}
