/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util.restclient;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.UUID;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;

import com.resolve.rsbase.MainBase;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public final class RestAPI
{
    public static final String RESTWEB_SERVICE_ROOT = "/resolve/restservice";

    /**
     * Uploads a file or folder using the Resolve REST web service.
     *
     * <pre>
     * {@code
     *      import com.resolve.util.restclient.RestAPI;
     *      ...
     *      try
     *      {
     *          //the result is the absolute path to the file/directory that got uploaded in the RSMGMT server.
     *          //String serviceUrl = "http://<RSMGMT_IP>:<HTTP_PORT>/resolve/restservice";
     *          //String serviceUrl = "https://<RSMGMT_IP>:<HTTPS_PORT>/resolve/restservice";
     *          String serviceUrl = "http://127.0.0.1:8082/resolve/restservice";
     *          String fileOrDir = "/home/resolve/myfile.txt";
     *          String result = RestAPI.uploadFile(serviceUrl, fileOrDir);
     *          System.out.println(result);
     *      }
     *      catch (Exception e)
     *      {
     *          //do something with the exception
     *      }
     * }
     * </pre>
     *
     * @param url the URL of the Resolve REST web service (e.g.,
     *            http://127.0.0.1:8082/resolve/restservice)
     * @param fileOrDir absolute location and name of the file/dir to be uploaded.
     * @return the absolute location of the uploaded file/directory where it got
     *         uploaded in the server. Or a string with the prefix "ERROR:" for
     *         any error.
     * @throws Exception
     */
    public static String uploadFile(String url, String fileOrDir) throws Exception
    {
        String result = "";
        if(StringUtils.isNotBlank(url) && StringUtils.isNotBlank(fileOrDir))
        {
            File fileToUpload = FileUtils.getFile(fileOrDir);
            File zipFile = null;
            File tmpDir = null;
            boolean isFolder = false;
            if(fileToUpload.exists())
            {
                if(fileToUpload.isDirectory())
                {
                    tmpDir = getTempDir();
                    tmpDir.mkdir();
                    zipFile = FileUtils.getFile(tmpDir + "/" + fileToUpload.getName() + ".zip");
                    FileUtils.zip(fileToUpload, zipFile);
                    fileToUpload = zipFile;
                    //making sure the receiver knows to unzip the folder.
                    isFolder = true;
                }

                if(fileToUpload.isFile())
                {
                    try
                    {
                        InputStream inputStream = new FileInputStream(fileToUpload);
                        result = uploadFile(url, fileToUpload.getName(), inputStream, isFolder);
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                    finally
                    {
                        if(zipFile != null && zipFile.exists())
                        {
                            FileUtils.deleteQuietly(zipFile);
                            FileUtils.deleteDirectory(tmpDir);
                        }
                    }
                }
            }
        }

        return result;
    }

    /**
     * Uploads a {@link String} using the Resolve REST web service.
     *
     * <pre>
     * {@code
     *      import com.resolve.util.restclient.RestAPI;
     *      ...
     *      try
     *      {
     *          //the result is the absolute path to the file/directory that got uploaded in the RSMGMT server.
     *          //String serviceUrl = "http://<RSMGMT_IP>:<HTTP_PORT>/resolve/restservice";
     *          //String serviceUrl = "https://<RSMGMT_IP>:<HTTPS_PORT>/resolve/restservice";
     *          String serviceUrl = "http://127.0.0.1:8082/resolve/restservice";
     *          String file = "myfile.txt";
     *          String content = "Content of my file";
     *          String result = RestAPI.uploadFile(serviceUrl, file, content);
     *          System.out.println(result);
     *      }
     *      catch (Exception e)
     *      {
     *          //do something with the exception
     *      }
     * }
     * </pre>
     *
     * @param url the URL of the Resolve REST web service (e.g.,
     *            http://127.0.0.1:8082/resolve/restservice)
     * @param file is any arbitrary file name that caller provides to store the content in the server.
     * @param content to be stored in the file.
     * @return the absolute location of the uploaded file where it got
     *         uploaded in the server. Or a string with the prefix "ERROR:" for
     *         any error.
     * @throws Exception
     */
    public static String uploadFile(String url, String file, String content) throws Exception
    {
        String result = "";
        if(StringUtils.isNotBlank(url) && StringUtils.isNotBlank(file) && StringUtils.isNotBlank(content))
        {
            return uploadFile(url, file, new ByteArrayInputStream(content.getBytes()), false);
        }

        return result;
    }

    /**
     * Uploads a {@link String} using the Resolve REST web service.
     *
     * <pre>
     * {@code
     *      import com.resolve.util.restclient.RestAPI;
     *      ...
     *      try
     *      {
     *          //the result is the absolute path to the file/directory that got uploaded in the RSMGMT server.
     *          //String serviceUrl = "http://<RSMGMT_IP>:<HTTP_PORT>/resolve/restservice";
     *          //String serviceUrl = "https://<RSMGMT_IP>:<HTTPS_PORT>/resolve/restservice";
     *          String serviceUrl = "http://127.0.0.1:8082/resolve/restservice";
     *          String file = "myfile.txt";
     *          String content = "Content of my file";
     *          //this could actually come from anywhere, reading the content from
     *          //a String is for illustration only. So as long as the input is
     *          //any InputStream it'll work.
     *          InputStream input = new ByteArrayInputStream(content.getBytes());
     *          String result = RestAPI.uploadFile(serviceUrl, file, input, false);
     *          System.out.println(result);
     *      }
     *      catch (Exception e)
     *      {
     *          //do something with the exception
     *      }
     * }
     * </pre>
     *
     * @param url the URL of the Resolve REST web service (e.g.,
     *            http://127.0.0.1:8082/resolve/restservice)
     * @param file is any arbitrary file name that caller provides to store the content in the server.
     * @param inputStream is a {@link InputStream} to be stored in the file.
     * @param isFolder set to true if the inputStream is a zipped up folder. Note that if you
     * set it true and the InputStream is not a .zip file system will silently upload it and give you
     * wrong file name as a return. Careful when using this method.
     * @return the absolute location of the uploaded file where it got
     *         uploaded in the server. Or a string with the prefix "ERROR:" for
     *         any error.
     * @throws Exception
     */
    public static String uploadFile(String url, String file, InputStream inputStream, boolean isFolder) throws Exception
    {
        String result = "";
        if(StringUtils.isNotBlank(url) && StringUtils.isNotBlank(file) && inputStream != null)
        {
            //HttpClient httpClient = new ResolveHttpClient(url);
            HttpClient httpClient = new ResolveHttpClient(url);
            HttpPost httpPost = new HttpPost(url);

            MultipartEntity reqEntity = new MultipartEntity();
            reqEntity.addPart("action", getStringBody("fileupload"));

            try
            {
                //reqEntity.addPart("fileName", new StringBody(streamBody.getFilename(), Charset.forName("UTF-8")));
                InputStreamBody streamBody = new InputStreamBody(inputStream, "multipart/form-data", file);
                reqEntity.addPart("filetoupload", streamBody);
                if(isFolder)
                {
                    reqEntity.addPart("isFolder", getStringBody("TRUE"));
                }
                httpPost.setEntity(reqEntity);

                HttpResponse response = httpClient.execute(httpPost);
                HttpEntity resEntity = response.getEntity();
                if (resEntity != null)
                {
                    result = StringUtils.toString(resEntity.getContent(), "UTF-8");
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                httpClient.getConnectionManager().shutdown();
            }
        }

        return result;
    }

    /**
     * Deletes file/directory uploaded through the Resolve REST Web service. It will
     * error out if the caller tries to delete some other file from the file system.
     * The general use case of this method is after a file/directory is uploaded the caller
     * can delete it as the upload method returns the absolute location of the
     * file or directory.
     *
     * <pre>
     * {@code
     *      import com.resolve.util.restclient.RestAPI;
     *      ...
     *      try
     *      {
     *          //String serviceUrl = "http://<RSMGMT_IP>:<HTTP_PORT>/resolve/restservice";
     *          //String serviceUrl = "https://<RSMGMT_IP>:<HTTPS_PORT>/resolve/restservice";
     *          String serviceUrl = "http://127.0.0.1:8082/resolve/restservice";
     *          String fileOrDir = "/opt/resolve/tmp/34377bd0-b53b-4660-ad58-3ff907d85445/myfile.txt";
     *          String result = RestAPI.deleteFile(serviceUrl, fileOrDir);
     *          //result could be a message like 'File deleted successfully." or a string starting with ERROR:
     *          System.out.println(result);
     *      }
     *      catch (Exception e)
     *      {
     *          //do something with the exception
     *      }
     * }
     * </pre>
     *
     * @param url the URL of the Resolve REST web service (e.g., http://127.0.0.1:8082/resolve/restservice)
     * @param fileOrDir is the absolute path of a file or directory.
     * @return a message like 'File deleted successfully." or a string starting with ERROR:
     * @throws Exception
     */
    public static String deleteFile(String url, String fileOrDir) throws Exception
    {
        String result = "";

        if(StringUtils.isNotBlank(url) && StringUtils.isNotBlank(fileOrDir))
        {
            HttpClient httpClient = new ResolveHttpClient(url);
            HttpPost httpPost = new HttpPost(url);
            //String jsonString = "{\"action\": \"filedelete\", \"file\": \"" + file + "\"}";
            String httpParams = "action=filedelete&filetodelete=" + fileOrDir;
            HttpEntity entity = new StringEntity(httpParams);
            httpPost.setEntity(entity);

            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity resEntity = response.getEntity();
            if (resEntity != null)
            {
                result = StringUtils.toString(resEntity.getContent(), "UTF-8");
            }
        }

        return result;
    }

    /**
     * This method lets the user submit any arbitrary information to the server. The main
     * goal is to process the data by a method in the RSMGMT side. The params must have
     * a key ("CLASSMETHOD") that indicates the classname and the method to be executed upon receiving the
     * data.
     *
     * @param url
     * @param params
     * @return
     * @throws Exception
     */
    public static String post(String url, Map<String, String> params) throws Exception
    {
        String result = "";

        if(StringUtils.isNotBlank(url) && params != null && params.size() > 0 && params.containsKey("CLASSMETHOD"))
        {
            HttpClient httpClient = new ResolveHttpClient(url);
            HttpPost httpPost = new HttpPost(url);
            String httpParams = StringUtils.mapToUrl(params);
            HttpEntity entity = new StringEntity(httpParams);
            httpPost.setEntity(entity);

            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity resEntity = response.getEntity();
            if (resEntity != null)
            {
                result = StringUtils.toString(resEntity.getContent(), "UTF-8");
            }
        }

        return result;
    }

    /**
     * This method provides the Resolve tmp folder location. It uses reflection
     * because the project is Util and it'll be unwise to attach rsbase project here.
     *
     * @return
     * @throws Exception
     */
    private static File getTempDir() throws Exception
    {
        File result = null;
        Class<?> c;
        try
        {
            String resolveHome = MainBase.main.getResolveHome();
            /*
            c = Class.forName("com.resolve.rsbase.MainBase");
            Method method = c.getDeclaredMethod("getResolveHome");
            Object resolveHome = method.invoke(null, "dummy");
            */
            if(resolveHome != null)
            {
                String random = UUID.randomUUID().toString();
                result = FileUtils.getFile(resolveHome + "/tmp/" + random);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception("Resolve tmp folder not found, this API must be used in Resolve environment");
        }
        return result;
    }

    private static StringBody getStringBody(String seed) throws UnsupportedEncodingException
    {
        return new StringBody(seed, Charset.forName("UTF-8"));
    }

//    public static void main(String[] args) throws Exception
//    {
//        Log.init();
//        Map<String, String> params = new HashMap<String, String>();
//        //String result = RestAPI.uploadFile("http://localhost:9082/resolve/restservice", "c:/tmp/sample", params);
//        //String result = RestAPI.uploadFile("https://10.20.2.113:8083/resolve/restservice", "c:/tmp/mypdf1.pdf");
//        //System.out.println(result);
//        //String result2 = RestAPI.deleteFile("https://10.20.2.113:8083/resolve/restservice", result);
//        //System.out.println(result2);
//
//        params.put("CLASSMETHOD", "com.resolve.rsmgmt.MMCP.getComponentStatus");
//        params.put("COMPONENT_ID", "3E9C9FECC89705319763CB46C772C39A");
//        String result = RestAPI.post("https://10.20.2.113:8083/resolve/restservice", params);
//        System.out.println(result);
//    }
}
