package com.resolve.util.eventsourcing.incident;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include = Inclusion.NON_NULL)
public class IncidentTypeChangedData extends EventData
{
    private String type;
    
    public IncidentTypeChangedData(String incidentId, String type)
    {
        super(incidentId);
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

}
