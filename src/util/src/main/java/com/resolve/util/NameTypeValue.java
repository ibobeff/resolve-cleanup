/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

public class NameTypeValue
{
    String name = "";
    String type = "";
    Object value = "";

    public NameTypeValue()
    {
    } // NameTypeValue

    public NameTypeValue(String name, String type, Object value)
    {
        this.name = name;
        this.type = type;
        this.value = value;
    } // NameTypeValue

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public Object getValue()
    {
        return value;
    }

    public void setValue(Object value)
    {
        this.value = value;
    }

} // NameTypeValue
