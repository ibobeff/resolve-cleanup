/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util.queue;

import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.resolve.util.Log;
import com.resolve.util.SystemUtil;

/**
 * Abstract class to define common methods for {@link QueueListener}.
 *
 */
abstract public class AbstractQueueListener<T> implements QueueListener<T>
{
    private final String id = UUID.randomUUID().toString();
    protected final ExecutorService executor;

    protected AbstractQueueListener()
    {
        int maxThreads = SystemUtil.getCPUCount();

        executor = new ThreadPoolExecutor(
                        maxThreads - 1, // core thread pool size
                        maxThreads, // maxThreads maximum thread pool size
                        1, // time to wait before resizing pool
                        TimeUnit.MINUTES,
                        new ArrayBlockingQueue<Runnable>(maxThreads, true),
                        new ThreadPoolExecutor.CallerRunsPolicy());
    }
    
    public String getId()
    {
        return id;
    }
    
    public boolean receive(T message)
    {
        boolean result = true;
        try
        {
            submit(message);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            result = false;
        }
        return result;
    }
    
    private void submit(final T message) throws Exception
    {
        Callable<Boolean> task = new Callable<Boolean>()
        {
            public Boolean call()
            {
                Boolean result = Boolean.TRUE;
                try
                {
                    if(Log.log.isTraceEnabled())
                    {
                        Log.log.trace("Listener Id: " + getId() + " processing message: " + message);
                    }
                    process(message);
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage(), e);
                    result = Boolean.FALSE;
                }
                return result;
            }
        };
        
        //submit the message for processing
        executor.submit(task);
    }
}
