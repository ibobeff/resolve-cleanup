/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.IOException;
import java.io.OutputStream;

public class NullOutputStream extends OutputStream
{
    private boolean closed = false;

    public NullOutputStream()
    {
    }

    public void close()
    {
        this.closed = true;
    }

    public void flush() throws IOException
    {
        if (this.closed) _throwClosed();
    }

    private void _throwClosed() throws IOException
    {
        throw new IOException("This OutputStream has been closed");
    }

    public void write(byte[] b) throws IOException
    {
        if (this.closed) _throwClosed();
    }

    public void write(byte[] b, int offset, int len) throws IOException
    {
        if (this.closed) _throwClosed();
    }

    public void write(int b) throws IOException
    {
        if (this.closed) _throwClosed();
    }

} // NullOutputStream
