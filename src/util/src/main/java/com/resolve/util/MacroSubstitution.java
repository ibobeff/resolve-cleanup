package com.resolve.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MacroSubstitution {

	private final static String MACRO_PREFIX = "${";
	private final static String MACRO_SUFFIX = "}";

	private final static String DEFAULT_UNDEFINIED = "";

	private final Map<String, String> mappings;

	public MacroSubstitution() {

		this.mappings = new HashMap<String, String>();
	}

	public void clearMappings() {

		this.mappings.clear();
	}

	public void addMapping(String key, String value) {

		this.mappings.put(key.toLowerCase(), value);
	}

	public void addAllMappings(Map<String, String> paramstoAdd) {
	    for (String key : paramstoAdd.keySet() )
	    {
	        this.mappings.put(key.toLowerCase(), paramstoAdd.get(key));
	    }
		//this.mappings.putAll(paramstoAdd);
	}

	public void addAllMappingsArrayValues(Map<String, String[]> paramstoAdd) {

		for (Map.Entry<String, String[]> parameter : paramstoAdd.entrySet()) {
			String[] value = parameter.getValue();
			this.mappings.put(parameter.getKey().toLowerCase(), value[value.length - 1]);
		}
	}

	public Map<String, String> getMappings() {

		return this.mappings;
	}

	public String evaluate(String stringToEvaluate, boolean caseInsensitive, boolean wipeOutNonMatching, String... defaultUndefinied) {

		String result = stringToEvaluate;
		String defaultUndefinedSubstitution = defaultUndefinied.length > 0 ? defaultUndefinied[0] : DEFAULT_UNDEFINIED;

		for (Map.Entry<String, String> mapping : this.mappings.entrySet()) {
			if (StringUtils.isNotBlank(result) && result.matches(".*\\$\\{.+\\}.*")) {
				String macroToFind = String.format("%s%s%s", MACRO_PREFIX, mapping.getKey(), MACRO_SUFFIX);
				if (caseInsensitive) {
					result = result.replace(macroToFind.toUpperCase(), mapping.getValue());
					result = result.replace(macroToFind.toLowerCase(), mapping.getValue());
				} else {
					result = result.replace(macroToFind, mapping.getValue());
				}
			} else {
				break;
			}
		}
		if (wipeOutNonMatching) {
			Pattern pattern = Pattern.compile("\\$\\{(.*?[\\}]*)\\}[\\}]*");			
			if (StringUtils.isNotBlank(result)) {
    			Matcher matcher = pattern.matcher(result);
    			while(matcher.find()) {
    				result = result.replace(matcher.group(0), "");
    			}
			}
		}
		return result;
	}
	
	public String substitute(String stringToEvaluate, boolean toUpper, boolean wipeOutNonMatching) {
	    
	    if (StringUtils.isBlank(stringToEvaluate) || !stringToEvaluate.matches(".*\\$\\{.+\\}.*"))
	        return stringToEvaluate;
	    
	    String result = stringToEvaluate;
	    
	    List<String> keys = getKeys(result);
	    
	    for(String key:keys) {
	        String value = this.mappings.get(toUpper == true ? key.toUpperCase():key.toLowerCase());
	        if(StringUtils.isNotEmpty(value)) {
	            String macroToFind = String.format("%s%s%s", MACRO_PREFIX, key, MACRO_SUFFIX);
	            result = result.replace(macroToFind, value);
	        }
	    }
	    
       if (wipeOutNonMatching) {
            Pattern pattern = Pattern.compile("\\$\\{(.*?[\\}]*)\\}[\\}]*");            
            if (StringUtils.isNotBlank(result)) {
                Matcher matcher = pattern.matcher(result);
                while(matcher.find()) {
                    result = result.replace(matcher.group(0), "");
                }
            }
        }
	       
	    return result;
	}
	
	private List<String> getKeys(String result) {
	    
	    if(StringUtils.isBlank(result) || result.indexOf("${") == -1)
	        return null;
	    
	    List<String> keys = new ArrayList<String>();
	    
	    StringTokenizer st = new StringTokenizer(result, "${");
	    
	    while(st.hasMoreTokens()) {
	        String key = st.nextToken();
	        int index = key.indexOf("}");
	        if(index > 3)
	            keys.add(key.substring(0, index));
	    }
	    
	    return keys;
	}

}
