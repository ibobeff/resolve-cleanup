package com.resolve.util.eventsourcing;

public enum IncidentResolutionTaskType {
    ACTION_TASK("action task"),
    AUTOMATION("automation"),
    DESICION_TREE("decision tree"),
    WIKI("wiki");

    private final String type;

    IncidentResolutionTaskType(final String type) {
        this.type = type;
    }

    @Override
    public String toString()  {
        return this.type;
    }
}