/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.vo.TNodeVO;

// MAP of unique execution id => List<TNodeVO>
public class TMetricCache extends ConcurrentHashMap<String, List<TNodeVO>>
{
    private static final long serialVersionUID = 6067571076761422807L;

    public void add(String executionId, List<TNodeVO> metric)
    {
        this.put(executionId, metric);
    } // add
    
    @SuppressWarnings("rawtypes")
    public void printDTCache()
    {
        Iterator it = this.keySet().iterator();
        while(it.hasNext()) {
            String key= (String)it.next();
            List<TNodeVO> value= this.get(key);
            //value.equals("anotherstring");//May throw a Null Pointer
            System.out.println("ExecutionId: " + key );
            if(value != null)
            {
                for(TNodeVO tnode : value) 
                {
                    System.out.println("wiki_name: " + tnode.getWikiFullName() + 
                                    " start_time: " + tnode.getStartTimeInMS() +
                                    " stop_time: " + tnode.getStopTimeInMS() + 
                                    " user: " + tnode.getUsername() +
                                    " node_time: " + tnode.getNodeTimeInMS() +
                                    " iscomplete: " + tnode.getIsPathComplete() +
                                    " isleaf: " + tnode.getIsLeaf() +
                                    " currwikiname: " + tnode.getCurrWikiFullName() +
                                    " expirytime: " + tnode.getAbortTimeInSecs());
                }
            }
        }
    } // printDTCache

} // TMetricCache
