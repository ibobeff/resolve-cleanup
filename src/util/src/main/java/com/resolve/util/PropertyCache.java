/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.util.Map;

import EDU.oswego.cs.dl.util.concurrent.ConcurrentHashMap;

public class PropertyCache
{
    // [ [user]:[name:value] ]
    static ConcurrentHashMap propertyCache = new ConcurrentHashMap();
    static ConcurrentHashMap systemCache = new ConcurrentHashMap();

    public static void init()
    {
        propertyCache.put("system", systemCache);
    }

    public static Map getUserCache(String user)
    {
        return (Map) propertyCache.get(user.toLowerCase());
    }

    public static Map getUserCacheOrCreate(String user)
    {
        user = user.toLowerCase();

        Map userCache = (Map) propertyCache.get(user);
        if (userCache == null)
        {
            userCache = new ConcurrentHashMap();
            propertyCache.put(user, userCache);
        }

        return userCache;
    }

    public static String get(String name)
    {
        return (String) systemCache.get(name.toLowerCase());
    }

    public static String get(String name, String user)
    {
        String result = null;

        Map userCache = getUserCache(user);
        if (userCache != null)
        {
            result = (String) userCache.get(name.toLowerCase());
        }

        return result;
    }

    public static void put(String name, String value)
    {
        systemCache.put(name.toLowerCase(), value);
    }

    public static void put(String name, String user, String value)
    {
        name = name.toLowerCase();
        user = user.toLowerCase();

        if (user.equalsIgnoreCase("system"))
        {
            systemCache.put(name, value);
        }
        else
        {
            Map userCache = getUserCacheOrCreate(user);
            userCache.put(name, value);
        }
    }

    public static void remove(String name)
    {
        systemCache.remove(name.toLowerCase());
    }

    public static void remove(String name, String user)
    {
        Map userCache = getUserCache(user);
        if (userCache != null)
        {
            userCache.remove(name.toLowerCase());
        }
    }
}
