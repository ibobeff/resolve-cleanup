package com.resolve.util.eventsourcing;

import com.resolve.util.eventsourcing.incident.EventData;
import com.resolve.util.eventsourcing.incident.resolution.IncidentResolutionCreatedData;
import com.resolve.util.eventsourcing.incident.resolution.IncidentResolutionData;
import com.resolve.util.eventsourcing.incident.resolution.IncidentResolutionUpdatedData;
import com.resolve.util.eventsourcing.incident.resolution.task.IncidentResolutionTaskCreatedData;
import com.resolve.util.eventsourcing.incident.resolution.task.IncidentResolutionTaskData;

public class EventMessageFactory
{
    private static EventMessageFactory instance;
    
    private EventMessageFactory() {}
    
    
    // ================ INCIDENT =============================
    public EventMessage getIncidentCompletedMessage(String incidentId) {
        EventData data = new EventData(incidentId);
        EventMessage em = new EventMessage(EventType.INCIDENT_COMPLETED_EVENT.getName(), data);
        
        return em;
        
    }
    public EventMessage getIncidentFailedMessage(String incidentId) {
        EventData data = new EventData(incidentId);
        EventMessage em = new EventMessage(EventType.INCIDENT_FAILED_EVENT.getName(), data);
        
        return em;
    }
    
    public EventMessage getIncidentAbortedMessage(String incidentId) {
        EventData data = new EventData(incidentId);
        EventMessage em = new EventMessage(EventType.INCIDENT_ABORTED_EVENT.getName(), data);
        
        return em;
    }

    // ================ INCIDENT RESOLUTION ==================
    public EventMessage getIncidentResolutionCreatedMessage(String incidentId, String rootResolutionId, String content, String type) {
        IncidentResolutionCreatedData data = new IncidentResolutionCreatedData(incidentId, rootResolutionId, content, type);
        return createEventMessage(EventType.INCIDENT_RESOLUTION_CREATED_EVENT, data);
    }
    
    public EventMessage getIncidentResolutionUpdatedMessage(String incidentId, String rootResolutionId, String summary, String condition, String severity) {
        IncidentResolutionUpdatedData data = new IncidentResolutionUpdatedData(incidentId, rootResolutionId, summary, condition, severity);
        return createEventMessage(EventType.INCIDENT_RESOLUTION_UPDATED_EVENT, data);
    }
    
    public EventMessage getIncidentResolutionBeganMessage(String incidentId, String rootResolutionId) {
        IncidentResolutionData data =  new IncidentResolutionData(incidentId, rootResolutionId);
        return createEventMessage(EventType.INCIDENT_RESOLUTION_BEGAN_EVENT, data);
    }    
    
    public EventMessage getIncidentResolutionFailedMessage(String incidentId, String rootResolutionId) {
        IncidentResolutionData data =  new IncidentResolutionData(incidentId, rootResolutionId);
        return createEventMessage(EventType.INCIDENT_RESOLUTION_FAILED_EVENT, data);
    }
    
    public EventMessage getIncidentResolutionCompletedMessage(String incidentId, String rootResolutionId) {
        IncidentResolutionData data =  new IncidentResolutionData(incidentId, rootResolutionId);
        return createEventMessage(EventType.INCIDENT_RESOLUTION_COMPLETED_EVENT, data);
    }
    
    public EventMessage getIncidentResolutionAbortedMessage(String incidentId, String rootResolutionId) {
        IncidentResolutionData data =  new IncidentResolutionData(incidentId, rootResolutionId);
        return createEventMessage(EventType.INCIDENT_RESOLUTION_ABORTED_EVENT, data);
    }
    
    // ============= INCIDENT RESOLUTION TASK ===============
    
    public EventMessage getIncidentResolutionTaskCreatedMessage(String incidentId, String taskId, String parentTaskId, String rootResolutionId, 
                    String type, String content, String rootResolution, String parentResolution)
    {
        IncidentResolutionTaskCreatedData data = new IncidentResolutionTaskCreatedData(incidentId, taskId, parentTaskId, rootResolutionId, 
                        type, content, rootResolution, parentResolution);
        return createEventMessage(EventType.INCIDENT_RESOLUTION_TASK_CREATED_EVENT, data);
    }
    
    public EventMessage getIncidentResolutionTaskStartedMessage(String incidentId, String taskId, String rootResolutionId) {
        IncidentResolutionTaskData data = new IncidentResolutionTaskData(incidentId, taskId, rootResolutionId);
        return createEventMessage(EventType.INCIDENT_RESOLUTION_TASK_STARTED_EVENT, data);
    }

    public EventMessage getIncidentReolutionTaskFailedMessage(String incidentId, String taskId, String rootResolutionId) {
        IncidentResolutionTaskData data = new IncidentResolutionTaskData(incidentId, taskId, rootResolutionId);
        return createEventMessage(EventType.INCIDENT_RESOLUTION_TASK_FAILED_EVENT, data);
    }
    
    public EventMessage getIncidentReolutionTaskCompletedMessage(String incidentId, String taskId, String rootResolutionId) {
        IncidentResolutionTaskData data = new IncidentResolutionTaskData(incidentId, taskId, rootResolutionId);
        return createEventMessage(EventType.INCIDENT_RESOLUTION_TASK_COMPLETED_EVENT, data);
    }

    public EventMessage getIncidentResolutionTaskAbortedMessage(String incidentId, String taskId, String rootResolutionId) {
        IncidentResolutionTaskData data = new IncidentResolutionTaskData(incidentId, taskId, rootResolutionId);
        return createEventMessage(EventType.INCIDENT_RESOLUTION_TASK_ABORTED_EVENT, data);
    }
    // =======================================================
    
    private EventMessage createEventMessage(EventType type, EventData data) {
        EventMessage em = new EventMessage(type.getName(), data);
        return em;
        
    }
    public static EventMessageFactory getInstance() {
        if(instance == null) {
            synchronized(EventMessageFactory.class) {
                if(instance == null) {
                    instance = new EventMessageFactory();
                }
            }
        }
        return instance;
    }

}
