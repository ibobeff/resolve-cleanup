/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.math.BigDecimal;

public class NumberUtils extends org.apache.commons.lang3.math.NumberUtils
{
    public static int compare(long first, long second)
    {
        int result = 0;
        if(first < second)
        {
            result = -1;
        }
        else if(first > second)
        {
            result = 1;
        }
        return result;
    }
    
    /**
     * This method extracts all the digits from a string.
     * example: 1.2.3.a.b.c.4 will return 1234
     * 
     * @param seed
     * @return
     */
    public static BigDecimal[] getBigDecimalArray(String seed)
    {
        BigDecimal[] result = null;
        if(StringUtils.isNotBlank(seed))
        {
            if(isNumber(seed))
            {
                result = new BigDecimal[] {new BigDecimal(seed)};
            }
            else
            {
                //seed=12.23% or 12.34.56abc
                //output 12.23 and 12.34.56
                String extract = seed.replaceAll("[^\\.0-9\\s]", "");
                String[] splittedArray = extract.split("\\.");
                result = new BigDecimal[splittedArray.length];
                for(int i = 0; i < splittedArray.length; i++)
                {
                    result[i] = new BigDecimal(splittedArray[i]);
                }
            }
        }
        return result;
    }
    
    public static Long createLong(Integer seed)
    {
        Long result = null;
        if(seed != null)
        {
            result = new Long(seed);
        }
        return result;
    }
}
