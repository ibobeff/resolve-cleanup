/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

public interface DBPoolStatusProvider
{
    int getMaxPoolSize();

    int getPoolSize();
    
    // Returns # of connections available from currently allocated connections
    int getAvailableSize();
    
    // Returns # of currently allocated connections
    int getTotalSize();
}
