package com.resolve.util.eventsourcing.incident.resolution.task;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.resolve.util.eventsourcing.incident.EventData;

@JsonSerialize(include = Inclusion.NON_NULL)
public class IncidentResolutionTaskCreatedData extends IncidentResolutionTaskData
{
    private String parentTaskId;
    private String type;
    private String content;
    private String rootResolution;
    private String parentResolution;
    
    public IncidentResolutionTaskCreatedData(String incidentId, String taskId, String parentTaskId, String rootResolutionId, String type,
                    String content, String rootResolution, String parentResolution)
    {
        super(incidentId, taskId, rootResolutionId);
        this.parentTaskId = parentTaskId;
        this.type = type;
        this.content = content;
        this.rootResolution = rootResolution;
        this.parentResolution = parentResolution;
    }
    
    public String getParentTaskId()
    {
        return parentTaskId;
    }
    public void setParentTaskId(String parentTaskId)
    {
        this.parentTaskId = parentTaskId;
    }
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    public String getContent()
    {
        return content;
    }
    public void setContent(String content)
    {
        this.content = content;
    }
    public String getRootResolution()
    {
        return rootResolution;
    }
    public void setRootResolution(String rootResolution)
    {
        this.rootResolution = rootResolution;
    }
    public String getParentResolution()
    {
        return parentResolution;
    }
    public void setParentResolution(String parentResolution)
    {
        this.parentResolution = parentResolution;
    }
}
