package com.resolve.util.cef;

import java.util.Map;

public class CEFEvent {
	private Map<String, String> headers;
	private Map<String, String> extensions;

	public Map<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	public Map<String, String> getExtensions() {
		return extensions;
	}

	public void setExtensions(Map<String, String> extensions) {
		this.extensions = extensions;
	}
}
