package com.resolve.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.parser.Parser;
import org.jsoup.safety.Whitelist;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.StringUtilities;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;

import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.resolve.util.restclient.ResolveHttpClient;

public class HttpUtil
{
    public static String HTML_ENCODING = "HTML_ENCODING";
    public static String URL_ENCODING = "URL_ENCODING";
    public static String SQL_ENCODING = "SQL_ENCODING";
    
    public static String VALIDATOR_RESOLVETEXT = "ResolveText";
    public static String VALIDATOR_RESOLVENUMBER = "ResolveNumber";
    public static String VALIDATOR_FILENAME = "FileName";
    public static String NUMBERWITHPERCENT = "NumberWithPercent";
    
    private static final int MAX_HEADER_NAME_SIZE = 256;
	private static final int MAX_HEADER_VALUE_SIZE = 2048; // 2K
	
    /**
     * This method lets the user POST to any http endpoint.
     *
     * @param url
     * @param params
     * @return
     * @throws Exception
     */
    public static String post(String url, Map<String, String> params)
    {
        String result = "";

        if(StringUtils.isNotBlank(url) && params != null && params.size() > 0)
        {
            HttpClient httpClient = null;
            
            try
            {
                httpClient = new ResolveHttpClient(url);
                
                HttpPost httpPost = new HttpPost(url);
                httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                List<NameValuePair> postParams = new ArrayList<NameValuePair>();
                for(String key : params.keySet())
                {
                    postParams.add(new BasicNameValuePair(key, params.get(key)));
                }
            
                httpPost.setEntity(new UrlEncodedFormEntity(postParams, "UTF-8"));
                HttpResponse response = httpClient.execute(httpPost);
                HttpEntity respEntity = response.getEntity();

                if (respEntity != null)
                {
                    result = EntityUtils.toString(respEntity);
                }
            }
            catch (UnknownHostException e)
            {
                result = "ERROR: UnknownHostException, " + e.getMessage();  
                Log.log.error(e.getMessage(), e);
            }
            catch (MalformedURLException e)
            {
                result = "ERROR: MalformedURLException, " + e.getMessage();  
                Log.log.error(e.getMessage(), e);
            }
            catch (UnsupportedEncodingException e)
            {
                result = "ERROR: UnsupportedEncodingException, " + e.getMessage();  
                Log.log.error(e.getMessage(), e);
            }
            catch (ParseException e)
            {
                result = "ERROR: ParseException, " + e.getMessage();  
                Log.log.error(e.getMessage(), e);
            }
            catch (IOException e)
            {
                result = "ERROR: IOException, " + e.getMessage();  
                Log.log.error(e.getMessage(), e);
            }
            catch (Exception e)
            {
                result = "ERROR: Unknown exception, " + e.getMessage();  
                Log.log.error(e.getMessage(), e);
            }
            finally
            {
                if (httpClient != null)
                {
                    try
                    {
                        ((CloseableHttpClient)httpClient).close();
                    }
                    catch (IOException e)
                    {
                        Log.log.error(e.getMessage(), e);
                    }
                }
            }
        }
        
        return result;
    }
    
    // Removes all new line characters and carriage returns form a response header
    
    public static HttpServletResponse sanitizeResponse(HttpServletResponse response) 
    {
        Collection<String> headers = response.getHeaderNames();
        
        for (String s : headers)
        {
            Collection<String> headerValues = response.getHeaders(s);
            
            if (headerValues != null && !headerValues.isEmpty())
            {
                for (String headerValue : headerValues)
                {
                    ESAPI.httpUtilities().addHeader(response, s, StringUtils.removeNewLineAndCarriageReturn(headerValue));
                }
            }
        }
        
        return response;
    }
    
    public static Cookie sanitizeCookie (Cookie cookie) {
        
        if(cookie != null) {
            if(cookie.getComment() != null) {
                cookie.setComment(StringUtils.removeNewLineAndCarriageReturn(cookie.getComment()));
            }
            
            if(cookie.getDomain() != null) {
                cookie.setDomain(StringUtils.removeNewLineAndCarriageReturn(cookie.getDomain()));
            }
            
            if(cookie.getPath() != null) {
                cookie.setPath(StringUtils.removeNewLineAndCarriageReturn(cookie.getPath()));
            }
            
            if(cookie.getValue() != null) {
                cookie.setValue(StringUtils.removeNewLineAndCarriageReturn(cookie.getValue()));
            }
        }
        return cookie;
    }

    public static String sanitizeValue(String value)
    {
        return sanitizeEncodedValue(value, false);
    }

    public static String sanitizeEncodedValue(String value, boolean encoded) {
        if (StringUtils.isEmpty(value))
        {
            return "";
        }
        if (!encoded) {
            value = ESAPI.encoder().encodeForHTML(value);
        }
        try
        {
            value = ESAPI.validator().getValidInput("Invalid return value ", value, "ResolveText", 400000, true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(),e);
            throw new RuntimeException("Failed to validate input value"); 
        }
        
        return value;
    }

    public static String sanitizeValue(String value, String validator, int length)
    {
        return sanitizeValue(value, validator, length, HTML_ENCODING);
    }
    
    public static String sanitizeValue(String value, String validator, int length, String encoding)
    {
        if (StringUtils.isEmpty(value))
        {
            return "";
        }

        if(StringUtils.isEmpty(encoding) || HTML_ENCODING.equals(encoding))
        {
            value = ESAPI.encoder().encodeForHTML(value);
        }
        else if (URL_ENCODING.equals(encoding))
        {
            try
            {
                value = ESAPI.encoder().decodeFromURL(value);
                value = ESAPI.encoder().encodeForURL(value);
            }
            catch (Exception ex)
            {
                Log.log.error(ex,ex);
                throw new RuntimeException(ex);
            }
        }
//        else if (SQL_ENCODING.equals(encoding))
//        {
//            try
//            {
//                value = ESAPI.encoder().encodeForSQL(new MySQLCodec(MySQLCodec.Mode.ANSI), value);
//            }
//            catch (Exception ex)
//            {
//                Log.log.error(ex,ex);
//                throw new RuntimeException(ex);
//            }
//        }
        else
        {
            value = ESAPI.encoder().encodeForHTML(value);
        }

        try
        {
            value = ESAPI.validator().getValidInput("Invalid return value ", value, validator, length, true);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(),e);
            throw new RuntimeException("Failed to validate input value"); 
        }
        
        return value;
    }

    public static String validateJsonString(String str)
    {
        try
        {
            JsonParser parser = new JsonParser();
            parser.parse(str);
            return str;
        } 
        catch(JsonSyntaxException jse)
        {
            throw new RuntimeException(jse);
        }
    }

    public static String sanitizeMacroValue(String value)
    {
        if (StringUtils.isEmpty(value))
        {
            return "";
        }
        value = ESAPI.encoder().encodeForHTML(value);

        try
        {
            value = ESAPI.encoder().canonicalize(value, false, false);
            value = ESAPI.validator().getValidInput("Invalid return value ", value, "ResolveText", 400000, true, false);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(),e);
            throw new RuntimeException("Failed to validate input value"); 
        }
        return value;
    }
    
    public static String getSafeHeaderName(String headerName) {
    	String safeHeaderName = headerName;
    	
    	if (StringUtils.isNotBlank(headerName)) {
    		String strippedName = StringUtilities.replaceLinearWhiteSpace(headerName);
    		try {
    			safeHeaderName = ESAPI.validator().getValidInput("getSafeHeadrName", strippedName, "HTTPHeaderName", 
    															 MAX_HEADER_NAME_SIZE, false);
    		} catch (ValidationException | IntrusionException e) {
    			Log.auth.warn("Attempt to use malicious header name " + headerName + " denied.", e);
    			safeHeaderName = null;
    		}
    	}
    	
    	return safeHeaderName;
    }
    
    public static String getSafeHeaderValue(String headerValue) {
    	String safeHeaderValue = headerValue;
    	
    	if (StringUtils.isNotBlank(headerValue)) {
    		String strippedValue = StringUtilities.replaceLinearWhiteSpace(headerValue);
    		try {
    			safeHeaderValue = ESAPI.validator().getValidInput("getSafeHeadrValue", strippedValue, "HTTPHeaderValue", 
    															  MAX_HEADER_VALUE_SIZE, false);
    		} catch (ValidationException | IntrusionException e) {
    			Log.auth.warn("Attempt to use malicious header value " + headerValue + " denied.", e);
    			safeHeaderValue = null;
    		}
    	}
    	
    	return safeHeaderValue;
    }

    public static String validateInput(String input) {
    	// Convert input into its simplest form to prevent attacker from encoding the input.
    	String canonicalizeInput = ESAPI.encoder().canonicalize(input, false, false);

    	// Strip out malicious tags and then compare with original value. If differ then malicious code is detected.
    	String strippedCanonicalizeInputInput = Parser.unescapeEntities(Jsoup.clean(canonicalizeInput, Whitelist.basic()), true);
    	if (!strippedCanonicalizeInputInput.equals(canonicalizeInput)) {
    		Log.log.error("Failed to validate possible malicious input value '" + input + "'. Possible XSS attack has been detected.");
    		throw new RuntimeException("Failed to validate possible malicious input value. Possible XSS attack has been detected."); 
		}
    	return input;
	}}
