package com.resolve.util;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.resolve.rsbase.ConfigKeyService;
import com.resolve.rsbase.MainBase;
import com.resolve.util.CryptUtils.KeyType;

/**
 * REST implementation for {@link CryptKeyProvider}. Provides encryption keys from external key service. 
 */
public class CryptKeyRestProvider implements CryptKeyProvider {
	private static final String KEY_URL = "%s/keys/search/key?keyType=%s";
	
	@Override
	public String getKey(KeyType keyType) {
		try {
	        ConfigKeyService config = MainBase.main.getConfigKeyService();

	        CloseableHttpClient httpclient = getHttpClient(config);

			HttpHost host = new HttpHost(config.getHost(), config.getPort(), config.getProtocol());
			
			HttpGet httpGet = new HttpGet(String.format(KEY_URL, host.toURI(), keyType.name()));
			CloseableHttpResponse response = httpclient.execute(httpGet);
			
			String json = EntityUtils.toString(response.getEntity());
			EntityUtils.consume(response.getEntity());
			return getKeyFromJSON(json);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to get data from key service", e);
		}
	}
	
	private CloseableHttpClient getHttpClient(ConfigKeyService config) {
		try {
			// Apply credentials
			CredentialsProvider credsProvider = new BasicCredentialsProvider();
			credsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(config.getUsername(), config.getPwd()));

			// Allow self-signed certificates
			SSLContextBuilder builder = new SSLContextBuilder();
			builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());

			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());
			CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider)
					.setSSLSocketFactory(sslsf).build();
			
			return httpclient;
		} catch (GeneralSecurityException e) {
			throw new RuntimeException("Unable to create http client", e);
		}
	}
	
	private String getKeyFromJSON(String json) {
		try {
			JSONObject jsonObject = new JSONObject(json);
			return jsonObject.get("keyValue").toString();
		} catch (JSONException e) {
			throw new RuntimeException("Unable to get key from server response", e);
		}
	}
}
