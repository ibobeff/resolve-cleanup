/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class ModelProcessParser
{
    Map result;
    Map task2exec;
    Set traversed;

    /*
     * Generates map of execute_sid1 => set( execute_sid2, ... )
     * 
     * execute_sid1 depends on execute_sid2, ...
     */
    public Map parse(DirectedTree model, Map task2exec)
    {
        this.result = new Hashtable();
        this.task2exec = task2exec;
        this.traversed = new HashSet();

        DirectedTreeNode start = model.getStartNode();
        if (start != null)
        {
            setDependents(start);

        }

        return result;
    } // parse

    void setDependents(DirectedTreeNode node)
    {
        if (node != null)
        {
            // get node execute_sid
            String execute_sid = (String) task2exec.get(node.getValue());
            if (execute_sid != null && !traversed.contains(node))
            {
                // marked as traversed
                traversed.add(node);

                // get dependents
                if (node.parents != null)
                {
                    for (Iterator i = node.parents.values().iterator(); i.hasNext();)
                    {
                        DirectedTreeNode parent = (DirectedTreeNode) i.next();
                        String parent_sid = (String) task2exec.get(parent.getValue());

                        // get dependent set
                        Set dependsOn = (Set) result.get(execute_sid);
                        if (dependsOn == null)
                        {
                            dependsOn = new HashSet();
                            result.put(execute_sid, dependsOn);
                        }

                        // add child_sid
                        dependsOn.add(parent_sid);
                    }
                }

                // recursively setDependents for each child
                if (node.children != null)
                {
                    for (Iterator i = node.children.values().iterator(); i.hasNext();)
                    {
                        DirectedTreeNode child = (DirectedTreeNode) i.next();

                        setDependents(child);
                    }
                }
            }
        }
    } // setDependents

} // ModelProcessParser
