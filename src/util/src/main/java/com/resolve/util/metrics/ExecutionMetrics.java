package com.resolve.util.metrics;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Stopwatch;

public enum ExecutionMetrics
{

    INSTANCE;

    private ExecutionMetricsProcessor metricsProcessor;

    private Map<String, Map<String, Stopwatch>> threadStopwatchMap = new ConcurrentHashMap<String, Map<String, Stopwatch>>();

    private ExecutionMetrics()
    {
        this.metricsProcessor = new ExecutionMetricsProcessor(TimeUnit.SECONDS.toMillis(10));
    }

    public void registerEventManualTiming(String processName, long durationInNanoseconds, int... predefinedExecutionsCount)
    {
        this.metricsProcessor.registerEvent(processName, Thread.currentThread().getName(), durationInNanoseconds, predefinedExecutionsCount);
    }

    public void registerEventStart(String processName)
    {
        Stopwatch stopwatch = Stopwatch.createUnstarted();
        Map<String, Stopwatch> stopwatchMap = this.threadStopwatchMap.get(processName);
        if (stopwatchMap == null)
        {
            stopwatchMap = new ConcurrentHashMap<String, Stopwatch>();
        }
        stopwatchMap.put(Thread.currentThread().getName(), stopwatch);
        this.threadStopwatchMap.put(processName, stopwatchMap);
        stopwatch.start();
    }

    public void registerEventEnd(String processName, int... predefinedExecutionsCount) throws ExecutionMetricsException
    {
        Stopwatch stopwatch = null;
        if (this.threadStopwatchMap.containsKey(processName))
        {
            stopwatch = this.threadStopwatchMap.get(processName).get(Thread.currentThread().getName());
            if (stopwatch != null)
            {
                stopwatch.stop();
                registerEventManualTiming(processName, stopwatch.elapsed(TimeUnit.NANOSECONDS), predefinedExecutionsCount);
            }
            else
            {
                throw new ExecutionMetricsException(String.format("Event start for process [%s], thread [%s] has never been registered.", Thread.currentThread().getName(), processName));
            }
        }
        else
        {
            throw new ExecutionMetricsException(String.format("Event start for process [%s] has never been registered.", processName));
        }
    }

}
