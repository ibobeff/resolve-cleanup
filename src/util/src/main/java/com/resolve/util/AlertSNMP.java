/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.net.InetAddress;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

public class AlertSNMP
{
    // netcool class id
    final static String NETCOOL_RESOLVE_CLASSID = "29243";

    // oid nodes
    final static String OID_ENTERPRISES = "1.3.6.1.4.1";
    final static String OID_RESOLVESYSTEMS = OID_ENTERPRISES + ".29243";
    final static String OID_RESOLVEEVENTS = OID_RESOLVESYSTEMS + ".0";

    // alert notification-types
    final static int TYPE_GENERIC = 1;
    // .1.3.6.1.4.1.1977.47.1.1.4.2

    // genericAlert oids
    final static String OID_GENERIC = OID_RESOLVEEVENTS + "." + TYPE_GENERIC;

    // value object
    final static String OID_ALERT = OID_RESOLVESYSTEMS + ".1";
    final static String OID_ALERTSEVERITY = OID_ALERT + ".1.0";
    final static String OID_ALERTTYPE = OID_ALERT + ".2.0";
    final static String OID_ALERTSOURCE = OID_ALERT + ".3.0";
    final static String OID_ALERTCOMPONENT = OID_ALERT + ".4.0";
    final static String OID_ALERTMESSAGE = OID_ALERT + ".5.0";

    static boolean active = false;
    static String community = "public";
    static InetAddress address;
    static int port = 162;
    static String source;
    static long inittime;

    public static void init(String address, int port, String community) throws Exception
    {
        // check if address is specified
        if (address != null && !address.equals(""))
        {
            Log.log.info("Initialize SNMP Trap Destination - " + address + ":" + port + "/" + community);
            AlertSNMP.address = InetAddress.getByName(address);
            AlertSNMP.port = port;

            // community
            if (StringUtils.isEmpty(community))
            {
                AlertSNMP.community = "public";
            }
            else
            {
                AlertSNMP.community = community;
            }

            AlertSNMP.source = InetAddress.getLocalHost().getHostAddress();
            AlertSNMP.inittime = System.currentTimeMillis();

            active = true;
        }
    } // init

    public static void alert(String severity, String component, String type, String message) throws Exception
    {
        if (active)
        {
            sendGenericAlert(severity, component, type, message);
        }
    } // alert

    public static void sendGenericAlert(String severity, String component, String type, String message) throws Exception
    {
        // add varbindlist fields
        LinkedHashMap<String, String> variables = new LinkedHashMap<String, String>();

        variables.put(OID_ALERTSEVERITY, severity);
        variables.put(OID_ALERTTYPE, type);
        variables.put(OID_ALERTSOURCE, source);
        variables.put(OID_ALERTCOMPONENT, component);
        variables.put(OID_ALERTMESSAGE, message);

        // send snmp trap
        // sendSNMPTrap(TYPE_GENERICALERT, variables);
        sendV2cTrap(variables);

    } // sendGenericAlert

    public static void sendV2cTrap(LinkedHashMap<String, String> variables)
    {
        TransportMapping transport = null;
        Snmp snmp = null;
        try
        {
            transport = new DefaultUdpTransportMapping();
            snmp = new Snmp(transport);
            snmp.listen();

            CommunityTarget target = new CommunityTarget();
            target.setCommunity(new OctetString(community));
            target.setAddress(new UdpAddress(address + "/" + port)); // "127.0.0.1/162"
            target.setVersion(SnmpConstants.version2c);

            PDU request = new PDU();
            request.setType(PDU.TRAP);
            request.add(new VariableBinding(new OID("1.3.6.1.2.1.1.3.0"), new Integer32((int) (System.currentTimeMillis() - inittime) / 10)));
            request.add(new VariableBinding(new OID("1.3.6.1.6.3.1.1.4.1.0"), new OID(OID_GENERIC)));

            for (Iterator i = variables.entrySet().iterator(); i.hasNext();)
            {
                Map.Entry entry = (Map.Entry) i.next();

                OID fieldOid = new OID((String) entry.getKey());
                OctetString fieldValue = new OctetString((String) entry.getValue());

                request.add(new VariableBinding(fieldOid, fieldValue));
            }

            ResponseEvent responseEvent = snmp.send(request, target);
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (snmp != null)
                {
                    snmp.close();
                }
                if (transport != null)
                {
                    transport.close();
                }
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
            }
        }
    } // sendV2cTrap

    /*
     * public static void sendSNMPTrap(int specificTrap,
     * LinkedHashMap<String,String> variables) { try { // set notification type
     * oid SNMPObjectIdentifier enterpriseOid = new
     * SNMPObjectIdentifier(OID_RESOLVESYSTEMS); SNMPIPAddress agentAddress =
     * new SNMPIPAddress(source); int genericTrap = 6; // enterprise traps
     * 
     * // set timestamp SNMPTimeTicks timestamp = new
     * SNMPTimeTicks((System.currentTimeMillis()-inittime)/10);
     * 
     * // add varbindlist fields SNMPVarBindList varBindList = new
     * SNMPVarBindList(); for (Iterator i=variables.entrySet().iterator();
     * i.hasNext(); ) { Map.Entry entry = (Map.Entry)i.next();
     * SNMPObjectIdentifier fieldOid = new
     * SNMPObjectIdentifier((String)entry.getKey()); SNMPOctetString fieldValue
     * = new SNMPOctetString((String)entry.getValue());
     * varBindList.addSNMPObject(new SNMPVariablePair(fieldOid, fieldValue)); }
     * 
     * // create trap pdu SNMPv1TrapPDU pdu = new SNMPv1TrapPDU(enterpriseOid,
     * agentAddress, genericTrap, specificTrap, timestamp, varBindList);
     * 
     * String msg = "  timestamp:          " + timestamp.toString() + "\n"; msg
     * += "  enterprise OID:     " + pdu.getEnterpriseOID().toString() + "\n";
     * msg += "  agent address:      " + pdu.getAgentAddress().toString() +
     * "\n"; msg += "  generic trap:       " + pdu.getGenericTrap() + "\n"; msg
     * += "  specific trap:      " + pdu.getSpecificTrap() + "\n"; msg +=
     * "  varbinds:           " + pdu.getVarBindList().toString() + "\n";
     * Log.log.debug("Sending trap to " + address.getHostAddress() + ":\n"+msg);
     * 
     * // send trap pdu snmptrap.sendTrap(address, community, pdu); } catch
     * (Throwable e) { Log.log.error(e.getMessage(), e); }
     * 
     * } // sendSNMPTrap
     */

    public static boolean isActive()
    {
        return active;
    } // isActive

    public static void setActive(boolean isActive)
    {
        AlertSNMP.active = isActive;
    } // setActive

    public static String getAddress()
    {
        return address.getHostAddress();
    } // getAddress

    public static void setAddress(String address) throws Exception
    {
        AlertSNMP.address = InetAddress.getByName(address);
    } // setAddress

    public static String getCommunity()
    {
        return community;
    } // getCommunity

    public static void setCommunity(String community)
    {
        AlertSNMP.community = community;
    } // setCommunity

    public static int getPort()
    {
        return port;
    } // getPort

    public static void setPort(int port) throws Exception
    {
        AlertSNMP.port = port;
    } // setPort

} // AlertSNMP
