/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.io.File;
import java.io.FilenameFilter;

/**
 * filename filter for Resolve
 * 
 * @author jeet.marwah
 * 
 */
public class RSFilenameFilter implements FilenameFilter
{
    private String fileExtension = "";

    public RSFilenameFilter(String fileExtension)
    {
        this.fileExtension = "." + fileExtension;
    }

    public boolean accept(File dir, String name)
    {
        return name.endsWith(fileExtension);
    }

}
