/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is used for mapping a column name of a class to another
 * name. The mapping could be a Map's key or another classes property name.
 *
 * Refer to com.resolve.services.hibernate.vo.GatewayFilterVO for its usage which
 * maps its properties to key of a Map that gets used in gateway filter management.
 */
@Target(value = ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface MappingAnnotation
{
    String columnName();
}
