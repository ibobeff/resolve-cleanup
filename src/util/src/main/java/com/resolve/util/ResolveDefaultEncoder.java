package com.resolve.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.client.utils.URLEncodedUtils;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.Encoder;
import org.owasp.esapi.codecs.Codec;
import org.owasp.esapi.errors.EncodingException;
import org.owasp.esapi.reference.DefaultEncoder;

/**
 * Reference implementation of the Encoder interface. This implementation is a
 * wrapper around OWASP {@link DefaultEncoder} and most of the methods should
 * simply route the call to {@link DefaultEncoder} instance.   
 */
public class ResolveDefaultEncoder implements Encoder {
    private static volatile Encoder singletonInstance;
    private Encoder defaultEncoder;

	public static Encoder getInstance() {
		if (singletonInstance == null) {
			synchronized (ResolveDefaultEncoder.class) {
				if (singletonInstance == null) {
					singletonInstance = new ResolveDefaultEncoder();
				}
			}
		}
		return singletonInstance;
	}

	private ResolveDefaultEncoder() {
		defaultEncoder = DefaultEncoder.getInstance();  
	}

	@Override
	public String canonicalize(String arg0) {
		return defaultEncoder.canonicalize(arg0);
	}

	@Override
	public String canonicalize(String arg0, boolean arg1) {
		return defaultEncoder.canonicalize(arg0, arg1);
	}

	@Override
	public String canonicalize(String arg0, boolean arg1, boolean arg2) {
		return defaultEncoder.canonicalize(arg0, arg1, arg2);
	}

	@Override
	public String decodeForHTML(String arg0) {
		return defaultEncoder.decodeForHTML(arg0);
	}

	@Override
	public byte[] decodeFromBase64(String arg0) throws IOException {
		return defaultEncoder.decodeFromBase64(arg0);
	}

	/*
	 * Fixes the original feature of the {@link DefaultEncoder} which
	 * transforms incoming url query parameters into HTML symbol entities.
	 * For example, &subcategory is trasanformed into &sub HTML entity.
	 * 
	 * @see org.owasp.esapi.Encoder#decodeFromURL(java.lang.String)
	 */
	@Override
	public String decodeFromURL(String arg0) throws EncodingException {
		try {
			String decoded = URLDecoder.decode(arg0, ESAPI.securityConfiguration().getCharacterEncoding());
			
			// Original canonicalize method performs a security check for incoming arg.
			// If arg has malicious code, this call will throw IntrusionException. Return value
			// is discarded as it contains modified input.
			canonicalize(decoded);
		    
			// try to construct uri from input.
		    URI dirtyURI = new URI(arg0);
			
		    // Canonicalize each part of uri separately, then assemble it. This approach allows
		    // to avoid canonicalization of strings like &subcategory without transformation into
		    // HTML entities.
		    URIBuilder uriData = new URIBuilder();
		    uriData.setScheme(canonicalize(dirtyURI.getScheme()));
		    uriData.setHost(canonicalize(dirtyURI.getHost()));
		    uriData.setPort(dirtyURI.getPort());
	    	uriData.setPath(canonicalize(dirtyURI.getPath()));
		    
		    List<NameValuePair> params = URLEncodedUtils.parse(dirtyURI, ESAPI.securityConfiguration().getCharacterEncoding());
		    Iterator<NameValuePair> it = params.iterator();
			while (it.hasNext()) {
				NameValuePair nValuePair = it.next();
				uriData.setParameter(canonicalize(nValuePair.getName()), canonicalize(nValuePair.getValue()));
			}

			// Construct url from canonicalized url parts.
			return URLDecoder.decode(uriData.toString(), ESAPI.securityConfiguration().getCharacterEncoding());
		} catch (UnsupportedEncodingException ex) {
			throw new EncodingException("Decoding failed", "Character encoding not supported", ex);
		} catch (Exception e) {
			throw new EncodingException("Decoding failed", "Problem URL decoding input", e);
		}
	}

	@Override
	public String encodeForBase64(byte[] arg0, boolean arg1) {
		return defaultEncoder.encodeForBase64(arg0, arg1);
	}

	@Override
	public String encodeForCSS(String arg0) {
		return defaultEncoder.encodeForCSS(arg0);
	}

	@Override
	public String encodeForDN(String arg0) {
		return defaultEncoder.encodeForDN(arg0);
	}

	@Override
	public String encodeForHTML(String arg0) {
		return defaultEncoder.encodeForHTML(arg0);
	}

	@Override
	public String encodeForHTMLAttribute(String arg0) {
		return defaultEncoder.encodeForHTMLAttribute(arg0);
	}

	@Override
	public String encodeForJavaScript(String arg0) {
		return defaultEncoder.encodeForJavaScript(arg0);
	}

	@Override
	public String encodeForLDAP(String arg0) {
		return defaultEncoder.encodeForLDAP(arg0);
	}

	@Override
	public String encodeForOS(Codec arg0, String arg1) {
		return defaultEncoder.encodeForOS(arg0, arg1);
	}

	@Override
	public String encodeForSQL(Codec arg0, String arg1) {
		return defaultEncoder.encodeForSQL(arg0, arg1);
	}

	@Override
	public String encodeForURL(String arg0) throws EncodingException {
		return defaultEncoder.encodeForURL(arg0);
	}

	@Override
	public String encodeForVBScript(String arg0) {
		return defaultEncoder.encodeForVBScript(arg0);
	}

	@Override
	public String encodeForXML(String arg0) {
		return defaultEncoder.encodeForXML(arg0);
	}

	@Override
	public String encodeForXMLAttribute(String arg0) {
		return defaultEncoder.encodeForXMLAttribute(arg0);
	}

	@Override
	public String encodeForXPath(String arg0) {
		return defaultEncoder.encodeForXPath(arg0);
	}
	
}
