package com.resolve.util.metrics;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.google.gson.Gson;

public class ExecutionMetricsDTO
{
    private volatile int eventsCount;
    private volatile Map<String, Integer> threadsCountMap;
    private volatile long cumulativeDurationInNanoseconds;

    protected ExecutionMetricsDTO()
    {
        this.eventsCount = 0;
        this.threadsCountMap = new ConcurrentHashMap<String, Integer>();
        this.cumulativeDurationInNanoseconds = 0L; // nanoseconds
    }

    protected int getEventsCount()
    {
        return eventsCount;
    }

    protected Map<String, Integer> getThreadsCountMap()
    {
        return threadsCountMap;
    }

    protected long getCumulativeDuration()
    {
        return cumulativeDurationInNanoseconds;
    }

    protected synchronized void eventsCountAdd(int eventsCount)
    {
        this.eventsCount += eventsCount;
    }

    protected synchronized void eventsCountIncrement()
    {
        this.eventsCount++;
    }

    protected synchronized void threadsCountIncrement(String threadName)
    {
        if (this.threadsCountMap.containsKey(threadName))
        {
            this.threadsCountMap.put(threadName, this.threadsCountMap.get(threadName));
        }
        else
        {
            this.threadsCountMap.put(threadName, 1);
        }
    }

    protected synchronized void cumulativeDurationAdd(long durationInNanoseconds)
    {
        this.cumulativeDurationInNanoseconds += durationInNanoseconds;
    }

    protected synchronized void eventsCountReset()
    {
        this.eventsCount = 0;
    }

    protected synchronized void threadsCountReset(String threadName)
    {
        this.threadsCountMap.put(threadName, 0);
    }

    protected synchronized void cumulativeDurationReset()
    {
        this.cumulativeDurationInNanoseconds = 0;
    }

    protected synchronized void resetAll()
    {
        eventsCountReset();
        cumulativeDurationReset();
        this.threadsCountMap.clear();
    }

    @Override
    public String toString()
    {
        Map<String, Object> map = new LinkedHashMap<String, Object>(3);
        map.put("eventsCount", this.eventsCount);
        map.put("cumulativeDuration", this.cumulativeDurationInNanoseconds);
        Map<String, Map<String, Object>> mainMap = new HashMap<String, Map<String, Object>>(1);
        mainMap.put(this.getClass().getSimpleName(), map);
        Gson gson = new Gson();
        String json = gson.toJson(mainMap);
        return json;
    }

}
