/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.util.concurrent.ConcurrentHashMap;

// MAP of unique id => MetricMsg
public class MetricCache extends ConcurrentHashMap<String, MetricMsg>
{
    private static final long serialVersionUID = 1402451928845877961L;

    public void add(MetricMsg metric)
    {
        this.put(metric.getKey(), metric);
    } // add

} // MetricCache
