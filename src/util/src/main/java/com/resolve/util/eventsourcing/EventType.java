package com.resolve.util.eventsourcing;

public enum EventType
{
    INCIDENT_CREATED_EVENT("IncidentCreatedEvent"),
    INCIDENT_TYPE_CHANGED_EVENT("IncidentTypeChangedEvent"),
    INCIDENT_COMPLETED_EVENT("IncidentCompletedEvent"),
    INCIDENT_FAILED_EVENT("IncidentFailedEvent"),
    INCIDENT_ABORTED_EVENT("IncidentAbortedEvent"),
    
    INCIDENT_RESOLUTION_CREATED_EVENT("IncidentResolutionCreatedEvent"),
    INCIDENT_RESOLUTION_UPDATED_EVENT("IncidentResolutionUpdatedEvent"),
    INCIDENT_RESOLUTION_BEGAN_EVENT("IncidentResolutionBeganEvent"),
    INCIDENT_RESOLUTION_FAILED_EVENT("IncidentResolutionFailedEvent"),
    INCIDENT_RESOLUTION_COMPLETED_EVENT("IncidentResolutionCompletedEvent"),
    INCIDENT_RESOLUTION_ABORTED_EVENT("IncidentResolutionAbortedEvent"),
    
    INCIDENT_RESOLUTION_TASK_CREATED_EVENT("IncidentResolutionTaskCreatedEvent"),
    INCIDENT_RESOLUTION_TASK_STARTED_EVENT("IncidentResolutionTaskStartedEvent"),
    INCIDENT_RESOLUTION_TASK_FAILED_EVENT("IncidentResolutionTaskFailedEvent"),
    INCIDENT_RESOLUTION_TASK_COMPLETED_EVENT("IncidentResolutionTaskCompletedEvent"),
    INCIDENT_RESOLUTION_TASK_ABORTED_EVENT("IncidentResolutionTaskAbortedEvent");

    EventType(String name) {
        this.name = name;
    }
    
    private final String name;
    
    public String getName()
    {
        return name;
    }

}
