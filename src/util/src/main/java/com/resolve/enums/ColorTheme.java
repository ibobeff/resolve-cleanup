package com.resolve.enums;

public enum ColorTheme {
    LIGHT("light"),
    DARK("dark");
    
    private final String color;
    
    private ColorTheme(String color) {
	this.color = color;
    }
    
    public String getColorTheme() {
	return this.color;
    }
}
