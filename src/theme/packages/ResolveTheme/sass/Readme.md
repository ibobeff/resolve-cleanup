# ResolveTheme1/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    ResolveTheme1/sass/etc
    ResolveTheme1/sass/src
    ResolveTheme1/sass/var
