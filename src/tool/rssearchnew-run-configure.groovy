import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String runFilename = "config/jvm.options";
final String runFilenameWin = "bin/runtmp.bat";

String instanceName = "elasticsearch.";

def help()
{
    println "Usage: rssearch-run-configure <new or old>\n"
    println "This script will configure rssearch's run.sh or run.bat";
}
if ((args instanceof ArrayList && args.size() < 2) || (args.length < 2))
{
    help();
}
else
{
    try
    {
        def instance = args[1];
        instanceName = instanceName + instance;
        def dist = MAIN.getDist();
        def config = MAIN.getConfigRSSearch();

        if (config && dist)
        {
            def os = System.getProperty("os.name");

            File runFile = null;
            if (os.contains("Win"))
            {
                runFile = new File(dist + "/" + instanceName + "/" + runFilenameWin);
            }
            else if (os.contains("Linux") || os.contains("SunOS"))
            {
                runFile = new File(dist + "/" + instanceName + "/" + runFilename);
            }

            def replaceMap = config.rssearchRunReplaceValues();

            //Configure run.sh || run.bat
            if (runFile != null && runFile.exists())
            {
                byte[] runFileBytes = new byte[runFile.length()];
                FileInputStream fis = new FileInputStream(runFile);
                fis.read(runFileBytes);
                fis.close();

                String runFileStr = new String(runFileBytes);

                for (regex in replaceMap.keySet())
                {
                    def value = replaceMap.get(regex);
                    if (regex.contains("Xms"))
                    {
                        println "RSSEARCH Minimum Heap Allocation Set to " + (value - "Xms");
                        LOG.info("RSSEARCH Minimum Heap Allocation Set to " + value);
                    }

                    if (regex.contains("Xmx"))
                    {
                        println "RSSEARCH Maximum Heap Allocation Set to " + (value - "Xmx");
                        LOG.info("RSSEARCH Maximum Heap Allocation Set to " + value);
                    }
                    value = Matcher.quoteReplacement(value);
                    runFileStr = runFileStr.replaceFirst(regex, value);
                }

                runFileBytes = runFileStr.getBytes();
                FileOutputStream fos = new FileOutputStream(runFile);
                fos.write(runFileBytes);
                fos.close();

                println "Configured File: " + runFile.getAbsolutePath();
                LOG.warn("Configured File: " + runFile.getAbsolutePath());
            }
        }
        else
        {
            println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
            LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
        }
    }
    catch (Exception e)
    {
        println "Unexpected Exception while Running Configuration: " + e.getMessage();
        LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
    }
}
return null;
