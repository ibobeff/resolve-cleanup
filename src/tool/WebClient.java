/*******************************************************************************
 * 
 * (C) Copyright 2006
 * 
 * Resolve Systems, Inc. Delaware, USA
 * 
 * This software may not be given to any party other than authorised employees
 * of Resolve Systems, Inc.
 * 
 ******************************************************************************/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.util.URIUtil;

import com.resolve.util.StringUtils;

public class WebClient
{
    final static String MODE_INTERACTIVE = "INTERACTIVE";
    final static String MODE_GET = "GET";
    final static String MODE_POST = "POST";
    
    static boolean debug = false;
    
    HttpClient client;
    String mode;
    String host;
    String uri;
    Hashtable params;
    String cookie;
    
    public static void main(String[] args) throws Exception
    {
	    String mode = null;
	    String url = null;
        Hashtable params = null;
        
        int argsStart = 0;
        if (args.length > argsStart)
        {
            // check for help switch
            if (args[argsStart].equals("-?") || args[argsStart].equals("-h") || args[argsStart].equals("--help"))
            {
	            usage();
            }
            
            // check for debug switch
            if (args[argsStart].equals("-debug"))
            {
	            debug = true;
                argsStart++;
            }
            
            // check for valid switch
            if (args.length > argsStart+1)
            {
                // command
                if (args[argsStart].equalsIgnoreCase("GET"))
                {
                    mode = MODE_GET;
                }
                else if (args[argsStart].equalsIgnoreCase("POST"))
                {
                    mode = MODE_POST;
                }
                
                // url
                url = args[argsStart+1];
                
                // params
                if (args.length > argsStart+2)
                {
                    params = new Hashtable();
                    
                    int i = argsStart+2;
                    while (i < args.length)
                    {
                        String name = args[i];
                        i++;
                        
                        if (i < args.length)
                        {
	                        String value = args[i];
	                        i++;
                            
                            params.put(name, value);
                        }
                        else
                        {
                            params.put(name, "");
                        }
                    }
                }
            }
        }
        
        // init
        WebClient webclient = new WebClient(mode, url, params);
        webclient.start();
        
    } // main
    
    static void usage()
    {
        System.out.println();
        System.out.println("Usage: GET | POST <url> [<paramName> <paramValue> ...]");
        System.out.println();
        System.out.println("    If not arguments are provided, interactive mode is used.");
        System.exit(1);
    } // usage
    
    public WebClient()
    {
	    this.mode = MODE_INTERACTIVE;
	    this.params = new Hashtable();
        this.cookie = null;
        this.host = "http://localhost";
        this.uri = "/";
        
        // init log
        initLog();
        
        // Create an instance of HttpClient.
        client = new HttpClient();
        
    } // WebClient
    
    public WebClient(String mode, String url, Hashtable params) throws Exception
    {
        this.mode = mode;
        this.params = params;
        this.cookie = null;
        this.host = "http://localhost";
        this.uri = "/";
        
        // set mode
        if (mode == null)
        {
            this.mode = MODE_INTERACTIVE;
        }
        
        // set host and uri
        if (!StringUtils.isEmpty(url))
        {
            setURL(url);
        }
        
        // set parameter
        if (params == null)
        {
            this.params = new Hashtable();
        }
        
        // init log
        initLog();
        
        // Create an instance of HttpClient.
        client = new HttpClient();
        
    } // WebClient
    
    public void start() throws Exception
    {
        // get
        if (mode.equals(MODE_GET))
        {
	        getMethod();
        }
        
        // post
        else if (mode.equals(MODE_POST))
        {
	        postMethod();
        }
        
        // interactive
        else
        {
            getInteractive();
        }
    } // start
    
    void initLog()
    {
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
        System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
        
        initLogLevel();
    } // initLog
    
    void initLogLevel()
    {
        // debugging
        if (debug)
        {
	        //System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "debug");
	        System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire.header", "debug");
	        System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "debug"); 
        }
        else
        {
	        System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.commons.httpclient", "error"); 
        }
    } // initLogLevel

    void getMethod() throws Exception
    {
        // create url
        String url = host+URIUtil.encodePathQuery(uri);
        if (debug)
        {
			System.out.println();
			System.out.println("Getting URL: "+url);
        }
        
        // Create a method instance.
        GetMethod method = new GetMethod(url);

        // Provide custom retry handler is necessary
        method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));

        // manually set cookie if defined
        if (!StringUtils.isEmpty(cookie))
        {
			method.setRequestHeader("Cookie", cookie);
        }

        try
        {
            // Execute the method.
            int statusCode = client.executeMethod(method);

            if (statusCode != HttpStatus.SC_OK)
            {
                System.err.println("WARN: " + method.getStatusLine());
            }

            // Read the response body.
            byte[] responseBody = method.getResponseBody();

            // Deal with the response.
            // Use caution: ensure correct character encoding and is not binary
            // data
            System.out.println();
            System.out.println(new String(responseBody));

        }
        catch (HttpException e)
        {
            System.err.println("ERROR: protocol violation. " + e.getMessage());
            e.printStackTrace();
        }
        catch (IOException e)
        {
            System.err.println("ERROR: transport error. " + e.getMessage());
            e.printStackTrace();
        }
        finally
        {
            // Release the connection.
            method.releaseConnection();
        }
    } // getMethod
    
    void postMethod() throws Exception
    {
        // create url
        String url = host+URIUtil.encodePathQuery(uri);
        if (debug)
        {
			System.out.println();
			System.out.println("Posting URL: "+url);
        }
        
        // Create a method instance.
        PostMethod method = new PostMethod(url);
        
        // set form parameters
        NameValuePair[] data = new NameValuePair[params.size()];
        int idx = 0;
        for (Iterator i=params.entrySet().iterator(); i.hasNext(); )
        {
            Map.Entry entry = (Map.Entry)i.next();
            String name = (String)entry.getKey();
            String value = (String)entry.getValue();
            
            data[idx] = new NameValuePair(name, value);
            idx++;
        }
		method.setRequestBody(data);

        // Provide custom retry handler is necessary
        method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));
        method.getParams().setParameter("http.protocol.single-cookie-header", new Boolean(true));
        
        // manually set cookie if defined
        if (!StringUtils.isEmpty(cookie))
        {
			method.setRequestHeader("Cookie", cookie);
        }

        try
        {
            // Execute the method.
            int statusCode = client.executeMethod(method);

            if (statusCode != HttpStatus.SC_OK)
            {
                System.err.println("WARN: " + method.getStatusLine());
            }

            // Read the response body.
            byte[] responseBody = method.getResponseBody();

            // Deal with the response.
            // Use caution: ensure correct character encoding and is not binary
            // data
            System.out.println();
            System.out.println(new String(responseBody));

        }
        catch (HttpException e)
        {
            System.err.println("ERROR: protocol violation. " + e.getMessage());
            e.printStackTrace();
        }
        catch (IOException e)
        {
            System.err.println("ERROR: transport error. " + e.getMessage());
            e.printStackTrace();
        }
        finally
        {
            // Release the connection.
            method.releaseConnection();
        }
    } // postMethod
    
    void getInteractive()
    {
        try
        {
	        String cmdline = "";
	        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
            
            System.out.print("webclient> ");
	        cmdline = stdin.readLine();
	        while (!cmdline.equalsIgnoreCase("QUIT"))
	        {
                try
                {
	                if (cmdline.equals("help") || cmdline.equals("?"))
	                {
	                    cmdHelp();
	                }
	                else if (cmdline.startsWith("get"))
	                {
	                    cmdGet(cmdline);
	                }
	                else if (cmdline.startsWith("post"))
	                {
	                    cmdPost(cmdline);
	                }
	                else if (cmdline.startsWith("host"))
	                {
	                    cmdHost(cmdline);
	                }
	                else if (cmdline.startsWith("uri"))
	                {
	                    cmdURI(cmdline);
	                }
	                else if (cmdline.startsWith("url"))
	                {
	                    cmdURL(cmdline);
	                }
	                else if (cmdline.startsWith("params"))
	                {
	                    cmdParams(cmdline);
	                }
	                else if (cmdline.startsWith("cookie"))
	                {
	                    cmdCookie(cmdline);
	                }
	                else if (cmdline.startsWith("print"))
	                {
	                    cmdPrint(cmdline);
	                }
	                else if (cmdline.startsWith("println"))
	                {
	                    cmdPrintln(cmdline);
	                }
                }
                catch (Exception e)
                {
		            System.err.println(e.getMessage());
                }
	            
                System.out.println("");
                System.out.print("webclient> ");
		        cmdline = stdin.readLine();
	        }
        }
        catch (Exception e)
        {
            System.err.println(e.getMessage());
        }
        
    } // getInteractive
    
    void cmdHelp()
    {
        System.out.println("");
        System.out.println("Usage Help Information");
        System.out.println("======================");
        System.out.println("");
        System.out.println("quit");
        System.out.println("    Exits the program");
        System.out.println("");
        System.out.println("get [url]");
        System.out.println("    Sends a HTTP GET request");
        System.out.println("");
        System.out.println("post [url | paramName paramValue ...]");
        System.out.println("    Sends a HTTP POST request");
        System.out.println("");
        System.out.println("url [url]");
        System.out.println("    Display or set the URL location");
        System.out.println("");
        System.out.println("host [ipaddress[:port]]");
        System.out.println("    Display or set the host location of the url");
        System.out.println("");
        System.out.println("uri [[/]uri");
        System.out.println("    Display or set the uri path of the url");
        System.out.println("");
        System.out.println("params [add <name> <value> | remove <name> | <name1> <value1> ...]");
        System.out.println("    Display or set HTTP POST form parameter values");
        System.out.println("");
        System.out.println("cookie [add <name> <value> | remove <name> | <name1> <value1> ...]");
        System.out.println("    Display or set HTTP cookie parameter values");
        System.out.println("");
        System.out.println("print[ln] [<value>]");
        System.out.println("    Display the string value");
        System.out.println("");
        
    } // cmdHelp
    
    void cmdGet(String cmdline) throws Exception
    {
        String[] args = cmdline.split(" ");
        if (args.length >= 2)
        {
            if (args[1].charAt(0) == '/')
            {
                setURI(StringUtils.join(args, ' ', 1, args.length));
            }
            else if (args[1].startsWith("http"))
            {
                setURL(StringUtils.join(args, ' ', 1, args.length));
            }
        }
        
        getMethod();
    } // cmdGet
    
    void cmdPost(String cmdline) throws Exception
    {
        String[] args = cmdline.split(" ");
        if (args.length > 1)
        {
            // e.g. post URI
            if (args[1].charAt(0) == '/')
            {
                setURI(StringUtils.join(args, ' ', 1, args.length));
            }
            // e.g. post URL
            else if (args[1].startsWith("http"))
            {
                setURL(StringUtils.join(args, ' ', 1, args.length));
            }
            // e.g. post paramName paramValue ...
            else
            {
	            
	            params = new Hashtable();
	                    
	            int i=2;
	            while (i < args.length)
	            {
	                String name = args[i];
	                i++;
	                        
	                if (i < args.length)
	                {
	                    String value = args[i];
	                    i++;
	                            
	                    params.put(name, value);
	                }
	                else
	                {
	                    params.put(name, "");
	                }
	            }
            }
        }
        
        postMethod();
    } // cmdPost
    
    void cmdHost(String cmdline)
    {
        String[] args = cmdline.split(" ");
        if (args.length == 2)
        {
            setHost(args[1]);
        }
        else
        {
            System.out.println("HOST: "+this.host);
        }
    } // cmdHost
    
    void cmdURI(String cmdline) throws Exception
    {
        String[] args = cmdline.split(" ");
        if (args.length >= 2)
        {
            setURI(StringUtils.join(args, ' ', 1, args.length));
        }
        else
        {
            System.out.println("URI: "+this.uri);
        }
    } // cmdURI

    void cmdURL(String cmdline) throws Exception
    {
        String[] args = cmdline.split(" ");
        if (args.length >= 2)
        {
            setURL(StringUtils.join(args, ' ', 1, args.length));
        }
        else
        {
            System.out.println("URL: "+this.host+this.uri);
        }
    } // cmdURL
    
    void cmdParams(String cmdline)
    {
        String[] args = cmdline.split(" ");
        
        // add param
        if (args.length == 4 && args[1].equals("add"))
        {
            String name = args[2];
            String value = args[3];
            params.put(name, value);
        }
        
        // remove param
        else if (args.length == 3 && args[1].equals("remove"))
        {
            String name = args[2];
            params.remove(name);
        }
        
        // set params map
        else if (args.length > 1)
        {
            params = new Hashtable();
                    
            int i=1;
            while (i < args.length)
            {
                String name = args[i];
                i++;
                        
                if (i < args.length)
                {
                    String value = args[i];
                    i++;
                            
                    params.put(name, value);
                }
                else
                {
                    params.put(name, "");
                }
            }
        }
        
        // print params
        else
        {
            System.out.println("PARAMS: "+this.params);
        }
    } // cmdParams
    
    void cmdCookie(String cmdline)
    {
        String[] args = cmdline.split(" ");
        if (args.length == 2)
        {
            cookie =  args[1];
        }
    } // cmdCookie
    
    void setURL(String url) throws Exception
    {
        if (!url.startsWith("http"))
        {
            url = "http://"+url;
        }
        
        // get url host
        int pos = url.indexOf('/', "http://".length());
        if (pos != -1)
        {
            String host = url.substring(0, pos);
            String uri = url.substring(pos, url.length());
            setHost(host);
            setURI(uri);
        }
        else
        {
            setHost(url);
        }
    } // setURL
    
    void setHost(String host)
    {
        if (host.startsWith("http"))
        {
	        this.host = host;
        }
        else
        {
            this.host = "http://"+host;
        }
    } // setHost
    
    void setURI(String uri) throws Exception
    {
        if (uri.charAt(0) != '/')
        {
            uri = "/"+uri;
        }
        
        this.uri = uri;
    } // setURI
    
    void cmdPrint(String cmdline)
    {
        int pos = cmdline.indexOf(' ');
        if (pos != -1)
        {
            System.out.print(cmdline.substring(pos+1, cmdline.length()));
        }
    } // cmdPrint
    
    void cmdPrintln(String cmdline)
    {
        int pos = cmdline.indexOf(' ');
        if (pos != -1)
        {
            System.out.println(cmdline.substring(pos+1, cmdline.length()));
        }
        else
        {
            System.out.println();
        }
    } // cmdPrintln
    
} // WebClient
