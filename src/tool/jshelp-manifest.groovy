import static groovy.io.FileType.FILES
import groovy.json.JsonBuilder
def result = ''
def helpList = []
def pattern = /glu.defView\('RS.(.*?)'/

new File(".").eachFileRecurse(FILES) {
    if(it.name.endsWith('.js')) {
        it.eachLine { line ->
            if ((matcher = line =~ pattern)) {
               def match = matcher[0][1]
               helpList += match
            }
        }
    }
}
helpList = helpList.unique()
helpList.each{
    result += it + '\n'
}

print result

def showSerialized = true; //set this to true to create a file containing a json serialized version of the help point list
                            //this was added to make it easier to write a task that auto-creates the wiki stubs

if(showSerialized){
    builder = new JsonBuilder()
    builder(helpList) 
    
    def deserializeCode = """//To deserialize the list of documents, paste this into your groovy script:
import groovy.json.JsonSlurper
slurper = new JsonSlurper()
m = slurper.parseText('${builder.toString()}')

//For reference, here is the formatted list:
/*
${result}
*/
    """
    def f = new File(new File(".").getCanonicalPath().minus('dist'),'jshelpDeserializeCode.txt')
    
    f.createNewFile()
    def w = f.newWriter() 
    f << deserializeCode
    
    print "Created file containing json serialized list of help points:\n"
    print f.absolutePath
}