@echo off

set DIST=%CD%/../..
set JRE=%DIST%/jdk

set CP=C:/Project/Java/wjc/lib/IcomWJC.jar;
set CP=%CP%;C:/Project/Platform/windows/dist/rsremote/lib/resolve-remote.jar;
set CP=%CP%;C:/Project/Platform/windows/dist/lib/log4j.jar

"%JRE%\bin\java" -cp "%CP%" com.resolve.connect.OhioConnect %1 %2 %3 %4 %5 %6 %7 %8 %9
