@echo off
rem Setup passwords for keystore / truststore

if not "%1" == "" GOTO :START
    echo.
    echo Usage: <Keystore Password>
    echo This script will Create a keystore and truststore to use securing Cassandra communication
    GOTO :END

:START

set STOREPASS=%1

set DIST=%~p0..\..
set JRE=%DIST%\jdk

%JRE%\bin\keytool -genkeypair -alias cassandra_external -keyalg RSA -keysize 1024 -dname "CN=cassandra_external, OU=Resolve, O=Gen-E, C=US" -keystore cassandra\conf\.keystore -storepass %STOREPASS% -keypass %STOREPASS%
%JRE%\bin\keytool -exportcert -alias cassandra_external -file cassandra_external.crt -keystore cassandra\conf\.keystore -storepass %STOREPASS%
%JRE%\bin\keytool -importcert -alias cassandra_external -file cassandra_external.crt -keystore cassandra\conf\.truststore -storepass %STOREPASS% -noprompt

:END
set STOREPASS=
set DIST=
set JRE=
