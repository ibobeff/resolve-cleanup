

import java.security.MessageDigest;

import org.apache.commons.codec.binary.Base64;

public class CreateUser
{
	private static final String HASH_FUNCTION = "SHA-1";
    
    private static String encrypt(String x)
    {
        String answer = x;
        if (x != null)
        {
            try
            {
                MessageDigest d = MessageDigest.getInstance(HASH_FUNCTION);
                d.reset();
                d.update(x.getBytes());
                byte[] bytes = d.digest();
                answer = new String(Base64.encodeBase64(bytes)); //converted to apache encoding
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return answer;
    } // encrypt
    
    static String generate(String username, String password, String name, String roles)
    {
        // default glide.maint roles
        if (roles == null)
        {
            roles = "itil,admin,maint";
        }
        
        String result =
        "<user>\n" +
        "    <user_name>"+username+"</user_name>\n" +
        "    <name>"+name+"</name>\n" +
        "    <user_password>"+encrypt(password)+"</user_password>\n" +
        "    <roles>"+roles+"</roles>\n" +
        "</user>\n";
 
        return result;
    } // generate
    
    public static void main(String[] args)
    {
        if ((args.length == 2) && (args[0].equals("-p")))
        {
            System.out.println(encrypt(args[1]));
        }
        else if (args.length == 3)
        {
            System.out.println(generate(args[0], args[1], args[2], null));
        }
        else if (args.length == 4)
        {
            System.out.println(generate(args[0], args[1], args[2], args[3]));
        }
        else
        {
            usage();
        }
    } // main
        static void usage()
    {
        System.out.println("Usage: <username> <password> <name> [<roles>]");
        System.out.println("\n");
        System.out.println("  <username> <password> <name> [<roles>] - Create user definition XML file");
        System.out.println("\n");
        System.out.println("  -p <password>                          - Create encrypted password");
        System.out.println("\n");
    }
    
//<user>
//  <user_name>glide.maint</user_name>
//  <name>Glide Maintenance</name>
//  <user_password>TnoghD4vZ7ITlGrEOkPE0uo6ZqM=</user_password>
//  <roles>itil,admin,maint</roles>
//</user>

} // CreateUser
