/******************************************************************************

(C) Copyright 2006
 
Resolve Systems, Inc.
 Delaware, USA

This software may not be given to any party other than authorised
employees of Resolve Systems, Inc.

******************************************************************************/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.InputStreamReader;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;

public class SSHClient
{
    Connection conn;    // ssh connection
    Session sess;       // ssh session
    InputStream in;     // stdout from ssh session
    InputStream err;    // stderr from ssh session
    OutputStream out;   // stdin to ssh session
    
    int x;              // terminal x size
    int y;              // terminal y size
    
    public static void main(String[] args) throws Exception
    {
        String username;
        String password;
        String hostname;
        int port = 22;
        
        // get connection information
        if (args.length < 3)
        {
            System.out.println("SSHClient <username> <password> <hostname> [<port>]");
            System.out.println();
            System.exit(0);
        }
        
        // initialize arguments
        username = args[0];
        password = args[1];
        hostname = args[2];
        if (args.length == 4)
        {
            port = Integer.parseInt(args[3]);
        }
        
        try
        {
            // start ssh connection
            SSHClient sshClient = new SSHClient(username, password, hostname, port);
            
            // get user input commands
            BufferedReader inReader = new BufferedReader(new InputStreamReader(System.in));
            boolean finished = false;
            while (!finished)
            {
                // get input string
                String inline = inReader.readLine();
                if (inline != null)
                {
                    sshClient.send(inline);
                    
                    if (inline.startsWith("exit"))
                    {
                        finished = true;
                    }
                }
            }
            
            // close ssh connection
            sshClient.close();
        }
        catch (IOException e)
        {
            e.printStackTrace(System.err);
            System.exit(2);
        }
    } // main

    public SSHClient(String username, String password, String hostname, int port) throws Exception
    {
        /* Create a connection instance */
        conn = new Connection(hostname, port);

        /* Now connect */
        conn.connect();

        /* Authenticate */
        boolean isAuthenticated = conn.authenticateWithPassword(username, password);

        if (isAuthenticated == false)
        {
            throw new IOException("Authentication failed.");
        }

        /* Create a session */
        Session sess = conn.openSession();

        // terminal size
        x = 80;
        y = 24;

        sess.requestPTY("dumb", x, y, 0, 0, null);
        sess.startShell();

        // set streams
        in = sess.getStdout();
        err = sess.getStderr();
        out = sess.getStdin();

        // start session consumer
        new RemoteConsumer().start();

    } // SSHClient

    public void close()
    {
        /* Close this session */
        if (sess != null)
        {
            sess.close();
        }

        /* Close the connection */
        if (conn != null)
        {
            conn.close();
        }
    } // close
    
    public void send(String line) throws Exception
    {
        // include line termination
        line += "\n";
        
        out.write(line.getBytes());
        out.flush();
    } // send

    /**
     * This thread consumes output from the remote server and displays it in the
     * terminal window.
     * 
     */
    class RemoteConsumer extends Thread
    {
        char[][] lines = new char[y][];
        int posy = 0;
        int posx = 0;

        private void addText(byte[] data, int len)
        {
            for (int i = 0; i < len; i++)
            {
                char c = (char) (data[i] & 0xff);

                if (c == 8) // Backspace, VERASE
                {
                    if (posx < 0) 
                    {
                        continue;
                    }
                    posx--;
                    continue;
                }

                if (c == '\r')
                {
                    posx = 0;
                    continue;
                }

                if (c == '\n')
                {
                    posy++;
                    if (posy >= y)
                    {
                        for (int k = 1; k < y; k++)
                        {
                            lines[k - 1] = lines[k];
                        }
                        posy--;
                        lines[y - 1] = new char[x];
                        for (int k = 0; k < x; k++)
                        {
                            lines[y - 1][k] = ' ';
                        }
                    }
                    continue;
                }

                if (c < 32)
                {
                    continue;
                }

                if (posx >= x)
                {
                    posx = 0;
                    posy++;
                    if (posy >= y)
                    {
                        posy--;
                        for (int k = 1; k < y; k++)
                        {
                            lines[k - 1] = lines[k];
                        }
                        lines[y - 1] = new char[x];
                        for (int k = 0; k < x; k++)
                        {
                            lines[y - 1][k] = ' ';
                        }
                    }
                }

                if (lines[posy] == null)
                {
                    lines[posy] = new char[x];
                    for (int k = 0; k < x; k++)
                    {
                        lines[posy][k] = ' ';
                    }
                }

                lines[posy][posx] = c;
                posx++;
            } // for

            StringBuffer sb = new StringBuffer(x * y);

            for (int i = 0; i < lines.length; i++)
            {
                if (i != 0) sb.append('\n');

                if (lines[i] != null)
                {
                    sb.append(lines[i]);
                }
            }
            
            // output to screen
            System.out.print(sb.toString());
            
        } // addText

        public void run()
        {
            byte[] buff = new byte[8192];

            try
            {
                while (true)
                {
                    int len = in.read(buff);
                    if (len == -1) 
                    {
                        return;
                    }
                    System.out.print(new String(buff, 0, len));
                    //addText(buff, len);
                }
            }
            catch (Exception e) { }
        } // run
        
    } // RemoteConsumer

} // SSHClient
