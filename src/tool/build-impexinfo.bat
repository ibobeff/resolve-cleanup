cd C:\project\resolve3\src\tool
rem compile from Eclipse first
del impexinfo.jar

rem impextool classes
rem "C:\Program Files (x86)\Java\jdk1.5.0_10\bin\jar" -cmvf impextool.mf impextool.jar com\resolve\impex\*.class
jar -cmvf impextool.mf impextool.jar com\resolve\impex\*.class

rem util classes
cd ..\util
rem "C:\Program Files (x86)\Java\jdk1.5.0_10\bin\jar" -uvf ..\tool\impextool.jar com\resolve\util\StringUtils.class
jar -uvf ..\tool\impextool.jar com\resolve\util\StringUtils.class

rem return to start dir
cd ..\tool

rem list files
rem "C:\Program Files (x86)\Java\jdk1.5.0_10\bin\jar" -tvf impextool.jar
jar -tvf impextool.jar

