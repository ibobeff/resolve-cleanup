/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.tool;

import java.sql.Connection;
import java.util.TreeMap;

import org.apache.log4j.Level;

import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.sql.SQLDriverInterface;
import com.resolve.sql.SQLRecord;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.SysId;

public class BulkInsert
{
    final static int PROCESS_EXECUTION = 1;
    final static int TASK_PER_PROCESS = 10;
    
//  Oracle settings
//	final static String DBTYPE = "oracle";
//	final static String DBNAME = "orcl";
//	final static String DBHOST = "10.20.2.212";
//	final static String USERNAME = "Resolve1";
//	final static String PASSWORD = "resolve";
    
//  MySQL settings
    final static String version = "3.4.1";
    final static int NUM_RECORDS = 1000;
    final static String DBTYPE = "mysql";
    final static String DBNAME = "resolve";
    final static String DBHOST = "10.20.2.108";
    final static String USERNAME = "resolve";
    final static String PASSWORD = "resolve";

    SQLConnection conn;
    
    public static void main(String[] args) throws Exception
    {
        Log.init();
       
        /* Accept user arguments for inserting data to the wikidoc and accessrights table*/
        if( args.length != 7 )
        {
            System.out.println("Too few arguments. Currently works for inserts to wikidoc table");
            System.out.println("For example: BulkInsert 3.4.1 100 mysql resolve 10.20.2.108 resolve resolve");
            throw new Error("USAGE: BulkInsert <version> <num recs to be inserted> <dbtype> <dbname> <dbhost> <username> <password>");
        }
        
        String version = args[0];
        int NUM_RECORDS = Integer.parseInt(args[1]);
        String DBTYPE = args[2];
        String DBNAME = args[3];
        String DBHOST = args[4];
        String USERNAME = args[5];
        String PASSWORD = args[6];
        
        BulkInsert bulk = new BulkInsert(DBTYPE, DBNAME, DBHOST, USERNAME, PASSWORD);
        Log.log.info("version= " + version + " num records= " + NUM_RECORDS + " dbhost= " + DBHOST + " dbtype=  " + DBTYPE);
        System.out.println(version + " " + NUM_RECORDS + "  " + DBHOST + "  " + USERNAME + " ");
        
        Log.start("Starting inserts", Level.INFO);
        for (int i=0; i < NUM_RECORDS; i++)
        {
            TreeMap wikidoc_data = new TreeMap();
            TreeMap accessrights_data = new TreeMap();
            String wiki_sys_id = SysId.generate(wikidoc_data);
            String accessrights_sys_id = SysId.generate(accessrights_data);
            String resource_name = bulk.insertWikiDoc(version, wikidoc_data, wiki_sys_id, accessrights_sys_id);
            bulk.insertAccessRights(accessrights_data, accessrights_sys_id, wiki_sys_id, resource_name);
        }
        Log.duration("Completed inserts", Level.INFO);
        
        bulk.close();
        
    } // main
    
 

    public BulkInsert() throws Exception
    {
        Log.log.info("Starting SQL");
        
        // get db driver
        SQLDriverInterface driver = SQL.getDriver( DBTYPE, DBNAME, DBHOST, USERNAME, PASSWORD, null, false);
        if (driver != null)
        {
	        GMTDate.initTimezone(null);
	        SQL.init(driver);
	        SQL.start();
        }
        
        Log.log.info("Getting Connection");
        conn = SQL.getConnection();
    } // BulkInsert
    
    public BulkInsert(String dBTYPE, String dBNAME2, String dBHOST2, String uSERNAME2, String pASSWORD2) throws Exception
    {
        Log.log.info("Starting SQL");
        
        // get db driver
        SQLDriverInterface driver = SQL.getDriver( dBTYPE, dBNAME2, dBHOST2, uSERNAME2, pASSWORD2, null, false);
        if (driver != null)
        {
            GMTDate.initTimezone(null);
            SQL.init(driver);
            SQL.start();
        }
        
        Log.log.info("Getting Connection");
        conn = SQL.getConnection();
        
    } //BulkInsert

    public void close()
    {
        if (conn != null)
        {
	        conn.close();
        }
    } // close
    
    void insertProcessRequest() throws Exception
    {
        insertWorksheet();
        insertWorksheetDebug();
        
        for (int i=0; i < TASK_PER_PROCESS; i++)
        {
	        insertExecuteRequest();
	        insertExecuteResult();
	        insertActionResult();
	        insertExecuteDependency();
        }
    } // insertProcessRequest

    private String insertWikiDoc(String version, TreeMap wiki_data, String wiki_sys_id, String accessrights_sys_id) throws Exception
    {
        SQLRecord rec = new SQLRecord(conn.getConnection(), "wikidoc", wiki_data);
        String wiki_resource_name = rec.wikiDocInsert(version, wiki_sys_id, accessrights_sys_id);
        return wiki_resource_name;
    }
    
    //accessrights_data, accessrights_sys_id, wiki_sys_id, resource_name
    private void insertAccessRights(TreeMap accessrights_data, String accessrights_sys_id, String wiki_sys_id, String resource_name) throws Exception
    {
        SQLRecord rec = new SQLRecord(conn.getConnection(), "access_rights", accessrights_data);
        rec.accessRightsInsert(accessrights_sys_id, wiki_sys_id, resource_name);
    }

    private void insertExecuteDependency() throws Exception
    {
        SQLRecord rec = new SQLRecord(conn.getConnection(), "resolve_execute_dependency");
        rec.insert();
    } // insertExecuteDependency

    private void insertActionResult() throws Exception
    {
        SQLRecord rec = new SQLRecord(conn.getConnection(), "resolve_action_result");
        rec.insert();
    } // insertActionResult

    private void insertExecuteResult() throws Exception
    {
        SQLRecord rec = new SQLRecord(conn.getConnection(), "resolve_execute_result");
        rec.insert();
    } // insertExecuteResult

    private void insertExecuteRequest() throws Exception
    {
        SQLRecord rec = new SQLRecord(conn.getConnection(), "resolve_execute_request");
        rec.insert();
    } // insertExecuteRequest

    private void insertWorksheetDebug() throws Exception
    {
        SQLRecord rec = new SQLRecord(conn.getConnection(), "worksheet_debug");
        rec.insert();
    } // insertWorksheetDebug

    private void insertWorksheet() throws Exception
    {
        SQLRecord rec = new SQLRecord(conn.getConnection(), "worksheet");
        rec.insert();
    } // insertWorksheet

} // BulkInsert
