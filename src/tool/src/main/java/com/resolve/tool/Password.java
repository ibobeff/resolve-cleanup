/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.tool;

import java.security.MessageDigest;

import org.apache.commons.codec.binary.Base64;

public class Password
{
	private static final String HASH_FUNCTION = "SHA-1";
    
    private static String encrypt(String x)
    {
        String answer = x;
        if (x != null)
        {
            try
            {
                MessageDigest d = MessageDigest.getInstance(HASH_FUNCTION);
                d.reset();
                d.update(x.getBytes());
                byte[] bytes = d.digest();
                //Base64 enc = new Base64();
                answer = new String(Base64.encodeBase64(bytes)); //converted to apache encoding
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return answer;
    } // encrypt
    
    public static void main(String[] args)
    {
        if (args.length == 0)
        {
            System.out.println("Usage: <password>");
        }
        else
        {
            System.out.println(encrypt(args[0]));
        }
    } // main
      //<user_name>glide.maint</user_name>
  //<name>Glide Maintenance</name>
  //<user_password>TnoghD4vZ7ITlGrEOkPE0uo6ZqM=</user_password>
  //<roles>itil,admin,maint</roles>
  //</user>
} // Password
