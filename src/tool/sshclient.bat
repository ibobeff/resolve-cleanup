@echo off

set DIST=%CD%/../..
set JRE=%DIST%/jdk

set CP=%DIST%/tools/bin
set CP=%CP%;%DIST%/lib/ganymed-ssh2.jar

"%JRE%\bin\java" -cp "%CP%" SSHClient %1 %2 %3 %4 %5 %6 %7 %8 %9
