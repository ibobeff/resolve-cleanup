#!/bin/bash

if [ $# -lt 1 ]; then
  echo ""
  echo "Usage: <Keystore Password>"
  echo "This script will Create a keystore and truststore to use securing Cassandra communication"
  exit 1
fi

STOREPASS=$1

DIST=`echo $0 | awk '{ print substr( $0, 0, length($0)-20 ) }'`/..
JDK=${DIST}/jdk

${JDK}/bin/keytool -genkeypair -alias cassandra_external -keyalg RSA -keysize 1024 -dname "CN=cassandra_external, OU=Resolve, O=Gen-E, C=US" -keystore cassandra/conf/.keystore -storepass $STOREPASS -keypass $STOREPASS
${JDK}/bin/keytool -exportcert -alias cassandra_external -file cassandra_external.crt -keystore cassandra/conf/.keystore -storepass $STOREPASS
${JDK}/bin/keytool -importcert -alias cassandra_external -file cassandra_external.crt -keystore cassandra/conf/.truststore -storepass $STOREPASS -noprompt
