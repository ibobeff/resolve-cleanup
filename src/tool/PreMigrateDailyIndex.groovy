import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequestBuilder;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesResponse;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.bulk.byscroll.BulkByScrollResponse;
import org.elasticsearch.common.bytes.BytesArray;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.ReindexAction;
import org.elasticsearch.index.reindex.ReindexRequestBuilder;
import org.elasticsearch.index.reindex.remote.RemoteInfo;

import com.resolve.util.RESTUtils;
import com.resolve.util.StringUtils;

import groovy.json.JsonSlurper;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.concurrent.ExecutionException;

scrollSize = 50;

def help()
{
    println "Usage: MigrateDailyIndex <ToServerList> <ToServerPort> <ToServerClustername> <FromServer> <FromServerPort> <StartDate> <EndDate> [ScrollSize]";
    println "";
    println "This script will migrate data from one ES cluster to another.  This is meant for migrating data from older ES";
    println "instances to a newer ES cluster";
    println "Inputs:";
    println "    ToServerList: Comma separated list of all servers (IP/Host) in the new cluster";
    println "    ToServerPort: Port to connect to new server on, this should be the TCP port (i.e. 9300 or equivalent)";
    println "    ToServerClustername: Name of the ES Cluster being conected to (e.g. RESOLVE)";
    println "    FromServer: IP/Host of the server to copy data from, only one server in this cluster should be listed";
    println "    FromServerPort: Port to connect to on old server cluster, this should be the HTTP port (i.e. 9200 or equivalent)";
    println "    StartDate: First Day to copy data from, needs to be in YYYYMMDD format";
    println "    EndDate: Last Day to copy data from, needs to be in YYYYMMDD format";
    println "    ScrollSize: Amount of records to copy at a time to ES Cluster being connected to, default 50";
}


if ((!binding.variables.containsKey("args") || args.length < 8))
{
    help();
}
else
{
    LOG.info("Migrating Data Passed Arguments: " + args);

    def i=0;
    if (args[0] == "PreMigrateDailyIndex.groovy")
    {
        i++;
    }
    def servers = args[i].split(",");
    i++;

    def serverPort = args[i];
    i++;

    if (!serverPort.matches("\\d+"))
    {
        LOG.error("Invalid Server Port: " + serverPort);
        System.exit(-1);
    }
    else
    {
        serverPort = serverPort.toInteger();
    }

    def cluster = args[i];
    i++;

    def oldServer = args[i];
    i++;

    def oldServerPort = args[i];
    i++;

    if (!oldServerPort.matches("\\d+"))
    {
        println "Invalid Server Port: " + oldServerPort;
        LOG.error("Invalid Server Port: " + oldServerPort);
        System.exit(-1);
    }
    else
    {
        oldServerPort = oldServerPort.toInteger();
    }

    def startDate = args[i];
    i++;

    if (!startDate.matches("\\d{8}") && !startDate.equals("0"))
    {
        println "Invalid Start Date: " + startDate;
        LOG.error("Invalid Start Date: " + startDate);
        System.exit(-1);
    }
    else
    {
        startDate = startDate.toInteger();
    }

    def endDate = args[i];
    i++;

    if (!endDate.matches("\\d{8}") && !endDate.equals("0"))
    {
        println "Invalid End Date: " + endDate;
        LOG.error("Invalid End Date: " + endDate);
        System.exit(-1);
    }
    else
    {
        endDate = endDate.toInteger();
    }

    if (args.length >= i && args[i].matches("\\d+"))
    {
        scrollSize = args[i].toInteger();
        LOG.info("Setting new Scroll Size: " + scrollSize);
    }

    def sdf = new SimpleDateFormat("yyyyMMdd");

    def shards = BLUEPRINT.get("rssearch.shards");
    def replicas = servers.size() - 1;

    def client = null;

    try
    {
        def settingsBuilder = Settings.builder().put("cluster.name", cluster);
        settingsBuilder.put("client.transport.ping_timeout", TimeValue.timeValueSeconds(20));
        def settings = settingsBuilder.build();

        // a Map with value as serverip:port
        TransportAddress[] transportAddresses = new InetSocketTransportAddress[servers.size()];

        i=0;
        for (def server in servers)
        {
            def transportAddress = new InetSocketTransportAddress(InetAddress.getByName(server), serverPort);
            transportAddresses[i++] = transportAddress;
        }

        client = new PreBuiltTransportClient(settings).addTransportAddresses(transportAddresses);

        LOG.info("Migrating ES data starts...");
        println("Migrating ES data starts...");
        def url = "http://" + oldServer + ":" + oldServerPort + "/_cat/indices";
        def indexList = RESTUtils.get(url, null, null);

        def indexLines = indexList.split("\\r?\\n");
        def oldIndexes = new ArrayList<String>();
        for (def line in indexLines)
        {
            def splited = line.split("\\s+");
            if (StringUtils.isNotEmpty(splited[2]))
            {
                oldIndexes.add(splited[2]);
            }
        }

        def datePattern = Pattern.compile("[a-z_]+_(\\d{8})");
        for (def idxName : oldIndexes)
        {
            LOG.info("Migrating data on index " + idxName + " from ES server: " + oldServer + ", port: " + oldServerPort);
            println("Migrating data on index " + idxName + " from ES server: " + oldServer + ", port: " + oldServerPort);
            try
            {
                def dateMatcher = datePattern.matcher(idxName);
                def copiedRecords = 0;
                if (dateMatcher.matches())
                {
                    def date = dateMatcher.group(1).toInteger();
                    if ((startDate == 0 || date >= startDate) && (endDate == 0 || date <= endDate) && !isIndexExists(client, idxName))
                    {
                        createDynamicIndex(client, idxName, shards, replicas);

                        def dataquery = "{\"match_all\":{}}";
                        copiedRecords = copyData(client, idxName, oldServer, oldServerPort, dataquery)
                    }
                    else
                    {
                        LOG.info("Index " + idxName + " Not in Start/End Date block or already exists, skipping");
                    }
                }
                else if (idxName.equals("worksheetdata"))
                {
                    if (!isIndexExists(client, idxName))
                    {
                        def commonSettingsJson = readJson("common_settings.json");

                        //index alias
                        Map<String, String> mappings = new HashMap<String, String>();
                        mappings.put("worksheetdata", readJson("worksheets_data_mappings.json"));

                        createIndex(client, idxName, commonSettingsJson, mappings, shards, replicas);
                    }
                    def startRange = startDate == 0 ? 0 : sdf.parse(startDate.toString()).getTime();
                    def endRange = endDate == 0 ? 0 : sdf.parse(endDate.toString()).getTime();

                    def dataquery = "{\"match_all\":{}}";
                    if (startRange != 0 || endRange != 0)
                    {
                        dataquery = "{ \"range\":{ \"worksheetdata.sysCreatedOn\":{";
                        if (startRange != 0)
                        {
                            dataquery += "\"from\":\"" + startRange + "\"";
                            if (endRange != 0)
                            {
                                dataquery += ",";
                            }
                        }
                        if (endRange != 0)
                        {
                            dataquery += "\"to\":\"" + endRange + "\"";
                        }
                        dataquery += "}}}";
                    }

                    copiedRecords = copyData(client, idxName, oldServer, oldServerPort, dataquery);
                    //sleep for a few seconds to let system catch up
                    Thread.sleep(5000);
                }
                else
                {
                    LOG.info("Index " + idxName + " Not a Daily Index, Skipping");
                    println("Skipping index " + idxName);
                }
                if (copiedRecords > 100)
                {
                    //sleep for a few seconds to catch up
                    Thread.sleep(4000);
                }
            }
            catch (Exception ex)
            {
                LOG.error("Error migrating data from index " + idxName, ex);
                println("Error migrating data from index " + idxName);
                ex.printStackTrace();
            }
        }

        LOG.info("Migrating ES data is done");
        println("Migrating ES data is done");
    }
    catch (Exception ex)
    {
        LOG.error(ex,ex);
        ex.printStackTrace();
        System.exit(-1);
    }
}

def readJson(def fileName)
{
    def file = new File(MAIN.getDist() + "/file/tmp/" + fileName);
    if (file.exists())
    {
        return FileUtils.readFileToString(file, "UTF-8");
    }
    else
    {
        LOG.error("Missing Index JSON File: " + file.getAbsolutePath())
    }
}

def isIndexExists(def client, def indexName)
{
    boolean result = false;

    try
    {
        IndicesExistsResponse response = client.admin().indices().exists(new IndicesExistsRequest(indexName)).get();
        if (response.isExists())
        {
            result = true;
        }
    }
    catch (InterruptedException ie)
    {
        LOG.error(ie.getMessage(), ie);
        ie.printStackTrace();
    }
    catch (ExecutionException ee)
    {
        LOG.error(ee.getMessage(), ee);
        ee.printStackTrace();
    }
    return result;
}

def getTotalCount(def indexName, def serverHost, def serverPort, def query)
{
    def count = -1;
    def url = "http://" + serverHost + ":" + serverPort + "/" + indexName + "/_count";

    def httpClient = HttpClientBuilder.create().build();
    def request = new HttpPost(url);
    if (query && !query.contains("match_all"))
    {
        def fullquery = "{\"query\": " + query + "}";
        StringEntity params = new StringEntity(fullquery);
        request.addHeader("content-type", "application/json");
        request.setEntity(params);
    }
    def response = httpClient.execute(request);

    def statusCode = response.getStatusLine().getStatusCode();
    def countResult = StringUtils.toString(response.getEntity().getContent() as InputStream, "UTF-8")
    LOG.info("Count Result: " + countResult);
    if (statusCode != 200)
    {
        LOG.error("Failed to Query Total - Returned Status is " + statusCode);
    }
    else
    {
        def json = (new JsonSlurper()).parseText(countResult);
        count = json.count;
    }
    return count;
}


def createDynamicIndex(def client, def idx, def shards, def replicas)
{
    LOG.info("Prepare to create index: " + idx );
    println("Prepare to create index: " + idx );
    def mappingFile = null;
    def aliasName = null;
    def docType = null;
    if (!"worksheetdata".equals(idx) && idx.startsWith("worksheet"))
    {
        mappingFile = "worksheets_mappings.json";
        aliasName = "worksheetalias";
        docType = "worksheet";
    }
    else if (idx.startsWith("processrequest"))
    {
        mappingFile = "process_requests_mappings.json";
        aliasName = "processrequestalias";
        docType = "processrequest";
    }
    else if (idx.startsWith("taskresult"))
    {
        mappingFile = "task_results_mappings.json";
        aliasName = "taskresultalias";
        docType = "taskresult";
    }
    else if (idx.startsWith("executionsummary"))
    {
        mappingFile = "execution_summary_mappings.json";
        aliasName = "executionsummaryalias";
        docType = "executionsummary";
    }

    def indexAliases = new HashMap<String, IndicesAliasesRequestBuilder>();
    // some common clusterwide settings like shards/replica etc.
    def commonSettingsJson = readJson("common_settings.json");

    //index alias
    Map<String, String> mappings = new HashMap<String, String>();
    mappings.put(docType, readJson(mappingFile));

    IndicesAliasesRequestBuilder idxAlias = client.admin().indices().prepareAliases();
    idxAlias.addAlias(idx, aliasName);
    LOG.info("Reindex create new index " + idx);
    println("Reindex create new index " + idx);
    // call the common method to create indices.
    indexAliases.put(aliasName, idxAlias);
    try
    {
        createIndex(client, idx, commonSettingsJson, mappings, shards, replicas);

        IndicesAliasesResponse indicesAliasesResponse = idxAlias.execute().actionGet();
        if(indicesAliasesResponse.isAcknowledged())
        {
            LOG.info("Successfully added index " + idx + " to alias " + aliasName);
            println("Successfully added index " + idx + " to alias " + aliasName);
        }
        else
        {
            LOG.error("Alias Creation for Index " + idx + " to alias " + aliasName + " Failed");
            println("Alias Creation for Index " + idx + " to alias " + aliasName + " Failed");
        }
    }
    catch (Exception ex)
    {
        LOG.error(ex,ex);
        ex.printStackTrace();
        throw new RuntimeException(ex);
    }
}

def createIndex(def client, def indexName, def settingsJson, def mappings, def shards, def replicas)
{
    def createIndexRequestBuilder = client.admin().indices().prepareCreate(indexName);

    def builder = Settings.builder().put("number_of_shards", shards).put("number_of_replicas", replicas).put("max_result_window", 5000000).put("priority", 0);
    Settings indexSettings = null;

    if (StringUtils.isBlank(settingsJson))
    {
        indexSettings = builder.build();
    }
    else
    {
        indexSettings = builder.loadFromSource(settingsJson).build();
    }

    if (mappings != null && mappings.size() > 0)
    {
        for(String documentType : mappings.keySet())
        {
            if (StringUtils.isNotBlank(documentType))
            {
                createIndexRequestBuilder.addMapping(documentType, mappings.get(documentType));
            }
            else
            {
                createIndexRequestBuilder.addMapping(indexName, mappings.get(documentType));
            }
        }
    }

    createIndexRequestBuilder.setSettings(indexSettings);
    CreateIndexResponse createResponse = createIndexRequestBuilder.execute().actionGet();
    if (createResponse.isAcknowledged())
    {
        LOG.info("Successfully created index " + indexName);
        println("Successfully created index " + indexName);
    }

    LOG.info("Index created: " + indexName + ", number_of_shards: " + shards + ", number_of_replicas: " + replicas);
    LOG.debug("Other settings: " + settingsJson);
    LOG.debug("Mappings: " + mappings);
    println("Index created: " + indexName + ", number_of_shards: " + shards + ", number_of_replicas: " + replicas);
    if (mappings != null && mappings.size() > 0)
    {
        for(String documentType : mappings.keySet())
        {
            LOG.info("Document type : " + documentType);
            LOG.debug("Mappings : " + mappings.get(documentType));
            println("Document type : " + documentType);
            //println("Mappings : " + mappings.get(documentType));
        }
    }
}

def copyData(def client, def idxName, def oldServer, def oldServerPort, def dataquery)
{
    def idxAry = new String[1];
    idxAry[0] = idxName;
    ReindexRequestBuilder builder = ReindexAction.INSTANCE.newRequestBuilder(client).source(idxAry).destination(idxName);
    RemoteInfo ri = new RemoteInfo("http", oldServer, oldServerPort, new BytesArray(dataquery), null, null, new HashMap(), RemoteInfo.DEFAULT_SOCKET_TIMEOUT, RemoteInfo.DEFAULT_CONNECT_TIMEOUT );
    builder.setRemoteInfo(ri);
    builder.source().setScroll(new TimeValue(1200*1000)).setSize(scrollSize);

    long totalFrom = getTotalCount(idxName, oldServer, oldServerPort, dataquery);
    LOG.info("Count from Source: " + totalFrom);
    println("Count from Source: " + totalFrom);

    if (totalFrom == 0)
    {
        LOG.info("Nothing to copy, moving on");
    }
    else
    {
        BulkByScrollResponse response = builder.get();
        long totalTo = response.getCreated();
        LOG.info("Count from Reindex Response: " + totalTo);
        println("Count from Reindex Response: " + totalTo);
        if (totalFrom != totalTo)
        {
            LOG.warn("Possible Error with Data Migration of " + idxName + ". Counts from before and after do not match");
        }
    }
    return totalFrom;
}
