

SELECT DISTINCT r.UName
FROM 
	Users AS users 
	JOIN UserRoleRel AS userRoleRel 
	INNER JOIN 


SELECT DISTINCT rolename FROM 
(
	SELECT DISTINCT r.u_name rolename FROM
	users u
	JOIN user_role_rel urr ON u.sys_id = urr.u_user_sys_id
	JOIN roles r ON urr.u_role_sys_id = r.sys_id
	WHERE 1=1 AND u.u_user_name = 'admin'
    UNION ALL
	SELECT DISTINCT r.u_name rolename FROM
	users u
	JOIN user_group_rel ugr ON u.sys_id = ugr.u_user_sys_id
	JOIN groups g ON ugr.u_group_sys_id = g.sys_id
	JOIN group_role_rel grr ON g.sys_id = grr.u_group_sys_id
	JOIN roles r ON grr.u_role_sys_id = r.sys_id
	WHERE 1=1 AND u.u_user_name = 'admin'
) a
ORDER BY rolename


SELECT a.read FROM access_rights a WHERE 1=1  AND a.resource_type = 'wikidoc'  AND a.resource_name = 'Test.test2' 


SELECT * FROM wikiattachment 
WHERE WIKIDOC_SYS_ID = (SELECT sys_id FROM wikidoc WHERE fullname = 'Test.test1')

/*for versioning*/
SELECT patch FROM 
	wikiarchive a
	JOIN wikidoc b ON a.table_sys_id = b.sys_id
WHERE 	
	b.fullname = 'Test.test2'
	AND a.table_name = 'wikidoc'
	AND a.table_column = 'content'
	AND a.version <= 6
ORDER BY a.version	

/*
drop table wikidoc
DROP TABLE wikiarchive
*/
SELECT * FROM xwikidoc WHERE xwd_fullname LIKE '%Test%'

SELECT * FROM wikidoc 
SELECT * FROM wikiarchive 


SELECT * FROM sys_journal_field WHERE NAME = 'resolve_action_result' AND element = 'u_detail' AND element_id IN (
SELECT sys_id FROM resolve_action_result WHERE u_problem = '4cf47e43ac110136018fbd81e47c13d0' AND u_actiontask = '9b725a9ac0a8a21500c8c89231ca8602'
)


rat - 9b725a9ac0a8a21500c8c89231ca8602
lRar - 7 , 
eleIdQry - '4cf480c3ac110136007f7957f37f81db', '4cf48102ac110136017470d0a70b7a18', '4cf48140ac110136000adb3fe405e7f7', '4d1062f7ac11013600da89a787d3c79c', '5069995bac110136014c0761bb314658', '5069a37dac1101360158d74b3edc414a', '5069a3bcac110136006dc8f0ebc027c4'

SELECT * FROM sys_journal_field WHERE NAME = 'resolve_action_result' AND element = 'u_detail' AND 
element_id IN (SELECT sys_id FROM resolve_action_result WHERE u_problem = '4cf47e43ac110136018fbd81e47c13d0' AND u_actiontask = '9b725a9ac0a8a21500c8c89231ca8602')

SELECT sys_id FROM resolve_action_result WHERE u_actiontask = '9b725a9ac0a8a21500c8c89231ca8602'




SELECT * FROM resolve_session WHERE u_active_type = 'admin'

SELECT * FROM problem WHERE sys_id IN (SELECT u_problem FROM resolve_session WHERE u_active_type = 'admin')

SELECT sys_id FROM resolve_action_result WHERE u_problem = '51e8b068ac1101360121bae7c841a73e' AND u_actiontask = ''

SELECT * FROM sys_journal_field 
WHERE NAME = 'resolve_action_result' AND element = 'u_detail' AND 
element_id IN (SELECT sys_id FROM resolve_action_result WHERE u_problem = '51e8b068ac1101360121bae7c841a73e' AND u_actiontask = '')

SELECT u_condition, u_severity 
FROM 	resolve_action_result a
	JOIN resolve_execute_request b ON a.u_execute_request = b.sys_id
	JOIN resolve_action_task c ON a.u_actiontask = c.sys_id
WHERE b.u_wiki = 'Test.test1'	AND u_problem = ''
	
SELECT * FROM resolve_action_result WHERE u_actiontask = '9b725a9ac0a8a21500c8c89231ca8602'

SELECT DISTINCT u_problem FROM resolve_session WHERE u_active_type = 'admin'
SELECT * FROM resolve_execute_request WHERE u_wiki = 'Test.test3'	


SELECT DISTINCT u_condition, u_severity
FROM 	resolve_action_result a	
	JOIN resolve_execute_request b ON a.u_execute_request = b.sys_id 
	JOIN resolve_action_task c ON a.u_actiontask = c.sys_id
WHERE 	a.u_problem = (SELECT DISTINCT u_problem FROM resolve_session WHERE u_active_type = 'admin') AND b.u_wiki = 'Test.test3'		


SELECT a.* 
FROM resolve.resolve_action_result a 
INNER JOIN resolve.resolve_action_task b ON a.`u_actiontask`=b.`sys_id`, 
resolve.resolve_execute_request c 
WHERE a.`u_execute_request`=c.`sys_id` AND c.`u_wiki`='Test.test3' AND a.`u_problem`='509a4dd5ac1101360011a2172d08d582'
resolve_target

SELECT * FROM xwikidoc WHERE xwd_fullname = 'RunBook.WebHome'

SELECT * FROM xwikidoc WHERE xwd_fullname LIKE 'RunBook.%'
SELECT xwd_fullname, xwd_content FROM xwikidoc WHERE xwd_fullname LIKE 'System%' ORDER BY xwd_fullname
SELECT xwd_fullname, xwd_content FROM xwikidoc WHERE xwd_fullname LIKE 'RunBook%' ORDER BY xwd_fullname


COMMIT

-- to get the list of roles that the user belongs to
SELECT DISTINCT r.UName 
FROM Users AS u  
INNER UserRoleRel AS urr   
INNER Roles AS r   
WHERE u.UUserName = 'admin' 
AND u.sysId = urr.UUserSysId 
AND urr.URoleSysId = r.sysId 

--------------------------------------------------------------------------

SELECT wd.u_name, wd.u_namespace, wd.u_fullname, wd.sys_updated_by, wd.sys_updated_on,  ar.u_read_access
FROM access_rights ar
	JOIN wikidoc wd ON ar.u_resource_sys_id = wd.sys_id
WHERE ar.u_resource_type = 'wikidoc'
AND 
(ar.u_resource_name LIKE 'Test%' OR wd.u_fullname LIKE '%abc%' OR wd.u_summary LIKE '%abc%' OR wd.u_content LIKE '%abc%')


SELECT * FROM access_rights WHERE u_resource_type = 'wikidoc' AND u_resource_name LIKE 'Test%'--this is namespace filter


SELECT  wd.UName, wd.UNamespace, wd.UFullname, wd.sysUpdatedBy, wd.sysUpdatedOn,  ar.UReadAccess  FROM AccessRights AS ar,  WikiDocument AS wd  WHERE ar.UResourceSysId = wd.sysId AND ar.UResourceType = :UResourceType AND (wd.UFullname LIKE :UFullname OR wd.USummary LIKE :USummary OR wd.UContent LIKE :UContent) ORDER BY wd.UFullname






SELECT DISTINCT rolename FROM 
(
	SELECT DISTINCT r.u_name rolename FROM
	users u
	JOIN user_role_rel urr ON u.sys_id = urr.u_user_sys_id
	JOIN roles r ON urr.u_role_sys_id = r.sys_id
	WHERE 1=1 AND u.u_user_name = 'admin'
    UNION ALL
	SELECT DISTINCT r.u_name rolename FROM
	users u
	JOIN user_group_rel ugr ON u.sys_id = ugr.u_user_sys_id
	JOIN groups g ON ugr.u_group_sys_id = g.sys_id
	JOIN group_role_rel grr ON g.sys_id = grr.u_group_sys_id
	JOIN roles r ON grr.u_role_sys_id = r.sys_id
	WHERE 1=1 AND u.u_user_name = 'admin'
) a
ORDER BY rolename

SELECT wa.*, --wa.u_table_column, wa.u_version, wa.sys_created_by, wa.sys_created_on
FROM wikiarchive wa
	JOIN wikidoc wd ON wa.u_table_sys_id = wd.sys_id
WHERE wd.u_fullname = 'Test.test12'
ORDER BY wa.u_version DESC


SELECT 
	ref.*
FROM 
	wikidoc AS wd,
	content_wikidoc_wikidoc_rel AS rel,
	wikidoc AS ref
WHERE 
	wd.sys_id = rel.u_content_wikidoc_sys_id
	AND rel.u_wikidoc_sys_id = ref.sys_id
	AND wd.U_FULLNAME = 'Test.test12'


SELECT 
	ref.*
FROM 
	wikidoc AS wd,
	content_wikidoc_actiontask_rel AS rel,
	resolve_action_task AS ref
WHERE 
	wd.sys_id = rel.u_content_wikidoc_sys_id
	AND rel.u_actiontask_sys_id = ref.sys_id
	AND wd.U_FULLNAME = 'Test.test12'

SELECT 
	ref.*
FROM 
	wikidoc AS wd,
	main_wikidoc_wikidoc_rel AS rel,
	wikidoc AS ref
WHERE 
	wd.sys_id = rel.u_main_wikidoc_sys_id
	AND rel.u_wikidoc_sys_id = ref.sys_id
	AND wd.U_FULLNAME = 'Test.test12'



SELECT 
	ref.*
FROM 
	wikidoc AS wd,
	main_wikidoc_actiontask_rel AS rel,
	resolve_action_task AS ref
WHERE 
	wd.sys_id = rel.u_main_wikidoc_sys_id
	AND rel.u_actiontask_sys_id = ref.sys_id
	AND wd.U_FULLNAME = 'Test.test12'



SELECT 
	ref.*
FROM 
	wikidoc AS wd,
	content_wikidoc_actiontask_rel AS rel,
	resolve_action_task AS ref
WHERE 
	wd.sys_id = rel.u_content_wikidoc_sys_id
	AND rel.u_actiontask_sys_id = ref.sys_id
	AND wd.U_FULLNAME = 'Test.test12'

SELECT 
	ref.*
FROM 
	wikidoc AS wd,
	exception_wikidoc_wikidoc_rel AS rel,
	wikidoc AS ref
WHERE 
	wd.sys_id = rel.u_exception_wikidoc_sys_id
	AND rel.u_wikidoc_sys_id = ref.sys_id
	AND wd.U_FULLNAME = 'Test.test12'



SELECT 
	ref.*
FROM 
	wikidoc AS wd,
	exception_wikidoc_actiontask_rel AS rel,
	resolve_action_task AS ref
WHERE 
	wd.sys_id = rel.u_exception_wikidoc_sys_id
	AND rel.u_actiontask_sys_id = ref.sys_id
	AND wd.U_FULLNAME = 'Test.test12'



SELECT 
	ref.*
FROM 
	wikidoc AS wd,
	final_wikidoc_wikidoc_rel AS rel,
	wikidoc AS ref
WHERE 
	wd.sys_id = rel.u_final_wikidoc_sys_id
	AND rel.u_wikidoc_sys_id = ref.sys_id
	AND wd.U_FULLNAME = 'Test.test12'


SELECT 
	wd.*
FROM 
	resolve_action_task AS rat,
	content_wikidoc_actiontask_rel AS rel,
	wikidoc AS wd
WHERE 
	rat.sys_id = rel.u_actiontask_sys_id
	AND rel.u_content_wikidoc_sys_id = wd.sys_id
	AND rat.U_NAME = 'mysql db users'
	AND rat.u_namespace = 'resolve'

SELECT 
	wd.*
FROM 
	resolve_action_task AS rat,
	main_wikidoc_actiontask_rel AS rel,
	wikidoc AS wd
WHERE 
	rat.sys_id = rel.u_actiontask_sys_id
	AND rel.u_main_wikidoc_sys_id = wd.sys_id
	AND rat.U_NAME = 'Set CNS FLOWS and PARAMS'
	AND rat.u_namespace = 'demo'

SELECT 
	wd.*
FROM 
	resolve_action_task AS rat,
	exception_wikidoc_actiontask_rel AS rel,
	wikidoc AS wd
WHERE 
	rat.sys_id = rel.u_actiontask_sys_id
	AND rel.u_exception_wikidoc_sys_id = wd.sys_id
	AND rat.U_NAME = 'Set CNS FLOWS and PARAMS'
	AND rat.u_namespace = 'demo'




SELECT * FROM wikiarchive WHERE u_table_sys_id = '-1633409wikidoc629'

SELECT wd.* FROM wikidoc AS wd, content_wikidoc_wikidoc_rel AS rel
WHERE wd.sys_id = rel.u_content_wikidoc_sys_id AND rel.u_wikidoc_fullname = 'Test.test1'

SELECT * FROM wikidoc 
ORDER BY sys_updated_on DESC

SELECT * FROM wikidoc WHERE u_namespace = 'Duke' AND sys_id NOT IN (SELECT DISTINCT u_wikidoc_sys_id  FROM wikidoc_resolve_action_pkg_rel)
test12 --> 2010-03-15 20:28:05
