INSERT INTO wikiattachmentcontent(SYS_ID, U_WIKIATTCH_SYS_ID, U_CONTENT)
	SELECT sys_id, sys_id, U_CONTENT FROM wikiattachment ;

INSERT INTO users(sys_id, u_user_name, u_user_password)
	SELECT sys_id, user_name, user_password FROM sys_user ;

INSERT INTO roles(sys_id, u_NAME, u_description)
	SELECT sys_id, NAME, description FROM sys_user_role WHERE NAME NOT LIKE '%grou%';

INSERT INTO groups(sys_id, u_NAME, u_description)
	SELECT sys_id, NAME, description FROM sys_user_role WHERE NAME LIKE '%grou%';

INSERT INTO user_role_rel(sys_id, u_user_sys_id, u_role_sys_id) VALUES('1', '718eea88c611227201bb112d7966edf8', '2831a114c611228501d4ea6c309d626d');
INSERT INTO user_role_rel(sys_id, u_user_sys_id, u_role_sys_id) VALUES('2', '718eea88c611227201bb112d7966edf8', '85805422c61122890157c76a4036958b');
INSERT INTO user_role_rel(sys_id, u_user_sys_id, u_role_sys_id) VALUES('3', '718eea88c611227201bb112d7966edf8', '8d1f97f0c611227d0100078c7573be2f');
INSERT INTO user_role_rel(sys_id, u_user_sys_id, u_role_sys_id) VALUES('4', '718eea88c611227201bb112d7966edf8', '282bf1fac6112285017366cb5f867469');
INSERT INTO user_role_rel(sys_id, u_user_sys_id, u_role_sys_id) VALUES('4', '718eea88c611227201bb112d7966edf8', '6c55f04cc61122720149865b3e064a68');

INSERT INTO group_role_rel(sys_id, u_group_sys_id, u_role_sys_id) VALUES('5', '8a562cd0c0a8010701ec463995abf6fb', '2831a114c611228501d4ea6c309d626d');
INSERT INTO group_role_rel(sys_id, u_group_sys_id, u_role_sys_id) VALUES('6', '8a562cd0c0a8010701ec463995abf6fb', '85805422c61122890157c76a4036958b');
INSERT INTO group_role_rel(sys_id, u_group_sys_id, u_role_sys_id) VALUES('8', '8a562cd0c0a8010701ec463995abf6fb', '89504004c0a8010701c24b452b9d156d');
INSERT INTO group_role_rel(sys_id, u_group_sys_id, u_role_sys_id) VALUES('9', '8a562cd0c0a8010701ec463995abf6fb', 'c402a0f20a0a0a6500972b8a6cdee710');
INSERT INTO group_role_rel(sys_id, u_group_sys_id, u_role_sys_id) VALUES('10', '8a562cd0c0a8010701ec463995abf6fb', '9045bb27c61122720108e90a15aaf4d6');
INSERT INTO group_role_rel(sys_id, u_group_sys_id, u_role_sys_id) VALUES('11', '8a562cd0c0a8010701ec463995abf6fb', '72ca818cc0a8a214001d29c1bfeeaccc');
INSERT INTO group_role_rel(sys_id, u_group_sys_id, u_role_sys_id) VALUES('12', '8a562cd0c0a8010701ec463995abf6fb', '1b6403bdc611227200dfda8f0fd82818');

INSERT INTO user_group_rel(sys_id, u_user_sys_id, u_group_sys_id) VALUES('13', '718eea88c611227201bb112d7966edf8', '8a562cd0c0a8010701ec463995abf6fb');

INSERT INTO access_rights(SYS_ID, u_resource_type, u_resource_name, u_resource_SYS_ID, u_READ_ACCESS, u_WRITE_ACCESS, u_ADMIN_ACCESS, u_EXECUTE_ACCESS) 
VALUES('1', 'wikidoc', 'Test.test2', '-1633409628', 'admin, personalize, resolve_user', 'admin, resolve_user', 'admin', 'admin, action_execute');

INSERT INTO access_rights(SYS_ID, u_resource_type, u_resource_name, u_resource_SYS_ID, u_READ_ACCESS, u_WRITE_ACCESS, u_ADMIN_ACCESS, u_EXECUTE_ACCESS) 
VALUES('2', 'wikidoc', 'Test.test1', '-1633409629', 'admin, personalize, resolve_user', 'admin, resolve_user', 'admin', 'admin, action_execute');

INSERT INTO access_rights(SYS_ID, u_resource_type, u_resource_name, u_resource_SYS_ID, u_READ_ACCESS, u_WRITE_ACCESS, u_ADMIN_ACCESS, u_EXECUTE_ACCESS) 
VALUES('3', 'wikidoc', 'Test.test3', '-1633409627', 'admin, personalize, resolve_user', 'admin, resolve_user', 'admin', 'admin, action_execute');



INSERT INTO wikidoc (SYS_ID, u_NAME, u_FULLNAME, u_CONTENT, u_MODEL_PROCESS, u_NAMESPACE, SYS_CREATED_BY, SYS_CREATED_ON)
	SELECT XWD_ID, XWD_NAME, XWD_FULLNAME, XWD_CONTENT, XWD_CLASS_XML, XWD_WEB, XWD_AUTHOR, XWD_CREATION_DATE 
	FROM xwikidoc WHERE xwd_fullname LIKE '%Test%' OR xwd_fullname LIKE 'Runbook.%' OR xwd_fullname LIKE 'System.%';

INSERT INTO access_rights(sys_id, u_resource_type, u_resource_name, u_resource_sys_id, u_read_access, u_write_access, u_admin_access, u_execute_access) 
	SELECT sys_id, 'wikidoc', u_fullname, sys_id, 'admin, personalize, resolve_user', 'admin, personalize, resolve_user', 'admin, personalize, resolve_user', 'admin, personalize, resolve_user'
	FROM wikidoc WHERE sys_id NOT IN (SELECT DISTINCT u_resource_sys_id FROM access_rights WHERE u_resource_type = 'wikidoc');


COMMIT;