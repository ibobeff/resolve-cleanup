CREATE TABLE `wikidoc` (
  `SYS_ID` VARCHAR(255) NOT NULL DEFAULT '0',
  `U_NAME` VARCHAR(255) NULL DEFAULT '',
  `U_FULLNAME` VARCHAR(255) NULL DEFAULT '',
  `U_TITLE` VARCHAR(255) DEFAULT NULL,

  `U_SUMMARY` MEDIUMTEXT  NULL,
  `U_CONTENT` MEDIUMTEXT  NULL,
  `U_MODEL_PROCESS` MEDIUMTEXT  NULL,
  `U_MODEL_EXCEPTION` MEDIUMTEXT  NULL,
  `U_MODEL_FINAL` MEDIUMTEXT NULL,

  `U_LANGUAGE` VARCHAR(5) DEFAULT NULL,
  `U_NAMESPACE` VARCHAR(255) DEFAULT NULL,
  `U_TAG` VARCHAR(255) DEFAULT NULL,
  `U_VERSION` INT(11) NOT NULL DEFAULT 1,
  `U_IS_DELETED` VARCHAR(1) NOT NULL DEFAULT '0',

  `SYS_CREATED_ON` DATETIME NULL,
  `SYS_CREATED_BY` VARCHAR(255) DEFAULT NULL,
  `SYS_UPDATED_ON` DATETIME DEFAULT NULL,
  `SYS_UPDATED_BY` VARCHAR(255) DEFAULT NULL,
  `SYS_MOD_COUNT` INT(11) DEFAULT NULL,
  
  PRIMARY KEY (`SYS_ID`),
  INDEX (U_FULLNAME)
) ENGINE=MYISAM DEFAULT CHARSET=UTF8;

/*--used to store version info for all the different types
*/
CREATE TABLE `wikiarchive` (
  `SYS_ID` VARCHAR(255) NOT NULL DEFAULT '0',
  `U_TABLE_SYS_ID` VARCHAR(255) NOT NULL DEFAULT '0',
  `U_TABLE_NAME` VARCHAR(255) NULL DEFAULT '',
  `U_TABLE_COLUMN` VARCHAR(255) DEFAULT NULL,
  `U_VERSION` INT(11)  NULL,
  `U_PATCH` MEDIUMTEXT  NULL,
  `SYS_CREATED_ON` DATETIME NULL,
  `SYS_CREATED_BY` VARCHAR(255) DEFAULT NULL,
  `SYS_UPDATED_ON` DATETIME DEFAULT NULL,
  `SYS_UPDATED_BY` VARCHAR(255) DEFAULT NULL,
  `SYS_MOD_COUNT` INT(11) DEFAULT NULL,
  PRIMARY KEY (`SYS_ID`)
) ENGINE=MYISAM DEFAULT CHARSET=UTF8;

/**
tinyblob: Only 255 characters 
blob: This one is limited to 65 Kbytes 
mediumblob: Limited to 16 MB 
longblob: Up to 4GB. 

**/
CREATE TABLE `wikiattachment` (
  `SYS_ID` VARCHAR(255) NOT NULL DEFAULT '0',
  `U_WIKIDOC_SYS_ID` VARCHAR(255) NOT NULL DEFAULT '0',
  `U_FILENAME` VARCHAR(255) NOT NULL DEFAULT '0',
  `U_TYPE` VARCHAR(255) NOT NULL DEFAULT '0',
  `U_LOCATION` VARCHAR(255) NULL DEFAULT '',
  `U_SIZE` INT(11) DEFAULT NULL,  
  `SYS_CREATED_ON` DATETIME NULL,
  `SYS_CREATED_BY` VARCHAR(255) DEFAULT NULL,
  `SYS_UPDATED_ON` DATETIME DEFAULT NULL,
  `SYS_UPDATED_BY` VARCHAR(255) DEFAULT NULL,
  `SYS_MOD_COUNT` INT(11) DEFAULT NULL,
  PRIMARY KEY (`SYS_ID`),
  INDEX(`U_WIKIDOC_SYS_ID`),
  FOREIGN KEY (`U_WIKIDOC_SYS_ID`) REFERENCES wikidoc(`SYS_ID`) ON DELETE CASCADE
) ENGINE=MYISAM DEFAULT CHARSET=UTF8;


CREATE TABLE `wikiattachmentcontent` (
  `SYS_ID` VARCHAR(255) NOT NULL DEFAULT '0',
  `U_WIKIATTCH_SYS_ID` VARCHAR(255) NOT NULL DEFAULT '0',
  `U_CONTENT` LONGBLOB  NULL,/*this will have limit of 4GB*/
  `SYS_CREATED_ON` DATETIME NULL,
  `SYS_CREATED_BY` VARCHAR(255) DEFAULT NULL,
  `SYS_UPDATED_ON` DATETIME DEFAULT NULL,
  `SYS_UPDATED_BY` VARCHAR(255) DEFAULT NULL,
  `SYS_MOD_COUNT` INT(11) DEFAULT NULL,
   PRIMARY KEY (`SYS_ID`)
) ENGINE=MYISAM DEFAULT CHARSET=UTF8;


CREATE TABLE `properties` (
  `SYS_ID` VARCHAR(255) NOT NULL DEFAULT '0',
  `U_NAME` VARCHAR(255) DEFAULT NULL,
  `U_VALUE` VARCHAR(255) DEFAULT NULL,
  `U_TYPE` VARCHAR(255) DEFAULT NULL,
  `U_DESCRIPTION` VARCHAR(255) DEFAULT NULL,
  `SYS_CREATED_ON` DATETIME NULL,
  `SYS_CREATED_BY` VARCHAR(255) DEFAULT NULL,
  `SYS_UPDATED_ON` DATETIME DEFAULT NULL,
  `SYS_UPDATED_BY` VARCHAR(255) DEFAULT NULL,
  `SYS_MOD_COUNT` INT(11) DEFAULT NULL,
   PRIMARY KEY (`SYS_ID`)
) ENGINE=MYISAM DEFAULT CHARSET=UTF8;

CREATE TABLE `access_rights` (
  `SYS_ID` VARCHAR(255) NOT NULL DEFAULT '0',
  `U_RESOURCE_TYPE` VARCHAR(255) NULL,
  `U_RESOURCE_NAME` VARCHAR(255) NULL,
  `U_RESOURCE_SYS_ID` VARCHAR(255) NOT NULL DEFAULT '0',
  `U_READ_ACCESS` VARCHAR(1000) NULL,
  `U_WRITE_ACCESS` VARCHAR(1000) NULL,
  `U_ADMIN_ACCESS` VARCHAR(1000) NULL,
  `U_EXECUTE_ACCESS` VARCHAR(1000) NULL,
  `SYS_CREATED_ON` DATETIME NULL,
  `SYS_CREATED_BY` VARCHAR(255) DEFAULT NULL,
  `SYS_UPDATED_ON` DATETIME DEFAULT NULL,
  `SYS_UPDATED_BY` VARCHAR(255) DEFAULT NULL,
  `SYS_MOD_COUNT` INT(11) DEFAULT NULL,
  PRIMARY KEY (`SYS_ID`)
) ENGINE=MYISAM DEFAULT CHARSET=UTF8;


CREATE TABLE `users` (
  `SYS_ID` VARCHAR(255) NOT NULL DEFAULT '0',
  `u_user_name` VARCHAR(40) NOT NULL,
  `u_user_password` VARCHAR(40) DEFAULT NULL,
  `u_password_needs_reset` TINYINT(1) DEFAULT NULL,
  `u_preferred_language` VARCHAR(40) DEFAULT NULL,
  `u_introduction` VARCHAR(40) DEFAULT NULL,
  `u_first_name` VARCHAR(50) DEFAULT NULL,
  `u_middle_name` VARCHAR(50) DEFAULT NULL,
  `u_last_name` VARCHAR(50) DEFAULT NULL,
  `u_name` VARCHAR(151) DEFAULT NULL,
  `u_phone` VARCHAR(40) DEFAULT NULL,
  `u_home_phone` VARCHAR(40) DEFAULT NULL,
  `u_mobile_phone` VARCHAR(40) DEFAULT NULL,
  `u_email` VARCHAR(100) DEFAULT NULL,
  `u_title` VARCHAR(100) DEFAULT NULL,
  `u_gender` VARCHAR(40) DEFAULT NULL,
  `u_time_zone` VARCHAR(40) DEFAULT NULL,
  `u_date_format` VARCHAR(40) DEFAULT NULL,
  `u_date_time_format` VARCHAR(40) DEFAULT NULL,
  `u_time_format` VARCHAR(40) DEFAULT NULL,
  `u_last_password` VARCHAR(40) DEFAULT NULL,
  `u_last_login` DATE DEFAULT NULL,
  `u_locked_out` TINYINT(1) DEFAULT NULL,
  `u_last_login_device` VARCHAR(40) DEFAULT NULL,
  `u_manager` VARCHAR(32) DEFAULT NULL,
  `u_notification` INT(11) DEFAULT NULL,
  `u_calendar_integration` INT(11) DEFAULT NULL,
  `u_photo` VARCHAR(40) DEFAULT NULL,
  `u_default_perspective` VARCHAR(32) DEFAULT NULL,
  `u_location` VARCHAR(32) DEFAULT NULL,
  `u_department` VARCHAR(32) DEFAULT NULL,
  `u_edu_status` VARCHAR(40) DEFAULT NULL,
  `u_company` VARCHAR(32) DEFAULT NULL,
  `u_source` VARCHAR(255) DEFAULT NULL,
  `u_street` VARCHAR(255) DEFAULT NULL,
  `u_city` VARCHAR(40) DEFAULT NULL,
  `u_state` VARCHAR(40) DEFAULT NULL,
  `u_zip` VARCHAR(40) DEFAULT NULL,
  `u_employee_number` VARCHAR(40) DEFAULT NULL,
  `u_vip` TINYINT(1) DEFAULT NULL,
  `u_building` VARCHAR(32) DEFAULT NULL,
  `u_failed_attempts` INT(11) DEFAULT NULL,
  `sys_updated_by` VARCHAR(40) DEFAULT NULL,
  `sys_updated_on` DATETIME DEFAULT NULL,
  `sys_created_by` VARCHAR(40) DEFAULT NULL,
  `sys_created_on` DATETIME DEFAULT NULL,
  `sys_mod_count` INT(11) DEFAULT NULL,
  PRIMARY KEY (`sys_id`),
  INDEX(`u_user_name`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

CREATE TABLE `roles` (
  `sys_id` VARCHAR(32) NOT NULL,
  `u_name` VARCHAR(100) DEFAULT NULL,
  `u_description` MEDIUMTEXT,
  `sys_updated_by` VARCHAR(40) DEFAULT NULL,
  `sys_updated_on` DATETIME DEFAULT NULL,
  `sys_created_by` VARCHAR(40) DEFAULT NULL,
  `sys_created_on` DATETIME DEFAULT NULL,
  `sys_mod_count` INT(11) DEFAULT NULL,
  PRIMARY KEY (`sys_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

CREATE TABLE `groups` (
  `sys_id` VARCHAR(32) NOT NULL,
  `u_name` VARCHAR(100) DEFAULT NULL,
  `u_description` MEDIUMTEXT,

  `sys_updated_by` VARCHAR(40) DEFAULT NULL,
  `sys_updated_on` DATETIME DEFAULT NULL,
  `sys_created_by` VARCHAR(40) DEFAULT NULL,
  `sys_created_on` DATETIME DEFAULT NULL,
  `sys_mod_count` INT(11) DEFAULT NULL,
  PRIMARY KEY (`sys_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

CREATE TABLE `user_group_rel` (
  `sys_id` VARCHAR(32) NOT NULL,
  `u_user_sys_id` VARCHAR(32) NOT NULL,
  `u_group_sys_id` VARCHAR(32) NOT NULL,

  `sys_updated_by` VARCHAR(40) DEFAULT NULL,
  `sys_updated_on` DATETIME DEFAULT NULL,
  `sys_created_by` VARCHAR(40) DEFAULT NULL,
  `sys_created_on` DATETIME DEFAULT NULL,
  `sys_mod_count` INT(11) DEFAULT NULL,
  PRIMARY KEY (`sys_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

CREATE TABLE `group_role_rel` (
  `sys_id` VARCHAR(32) NOT NULL,
  `u_group_sys_id` VARCHAR(32) NOT NULL,
  `u_role_sys_id` VARCHAR(32) NOT NULL,

  `sys_updated_by` VARCHAR(40) DEFAULT NULL,
  `sys_updated_on` DATETIME DEFAULT NULL,
  `sys_created_by` VARCHAR(40) DEFAULT NULL,
  `sys_created_on` DATETIME DEFAULT NULL,
  `sys_mod_count` INT(11) DEFAULT NULL,
  PRIMARY KEY (`sys_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

CREATE TABLE `user_role_rel` (
  `sys_id` VARCHAR(32) NOT NULL,
  `u_user_sys_id` VARCHAR(32) NOT NULL,
  `u_role_sys_id` VARCHAR(32) NOT NULL,

  `sys_updated_by` VARCHAR(40) DEFAULT NULL,
  `sys_updated_on` DATETIME DEFAULT NULL,
  `sys_created_by` VARCHAR(40) DEFAULT NULL,
  `sys_created_on` DATETIME DEFAULT NULL,
  `sys_mod_count` INT(11) DEFAULT NULL,
  PRIMARY KEY (`sys_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

/*****/
DROP TABLE content_wikidoc_wikidoc_rel;
DROP TABLE main_wikidoc_wikidoc_rel;
DROP TABLE exception_wikidoc_wikidoc_rel;
DROP TABLE final_wikidoc_wikidoc_rel;
DROP TABLE content_wikidoc_actiontask_rel;
DROP TABLE main_wikidoc_actiontask_rel;
DROP TABLE exception_wikidoc_actiontask_rel;
DROP TABLE final_wikidoc_actiontask_rel;
DROP TABLE wikidoc_resolve_action_pkg_rel;
DROP TABLE resolve_action_task_resolve_action_pkg_rel;

CREATE TABLE `content_wikidoc_wikidoc_rel` (
  `sys_id` VARCHAR(32) NOT NULL,
  `u_content_wikidoc_sys_id` VARCHAR(32) NOT NULL,
  `u_wikidoc_fullname` VARCHAR(255) NOT NULL,

  `sys_updated_by` VARCHAR(40) DEFAULT NULL,
  `sys_updated_on` DATETIME DEFAULT NULL,
  `sys_created_by` VARCHAR(40) DEFAULT NULL,
  `sys_created_on` DATETIME DEFAULT NULL,
  `sys_mod_count` INT(11) DEFAULT NULL,
  PRIMARY KEY (`sys_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

CREATE TABLE `main_wikidoc_wikidoc_rel` (
  `sys_id` VARCHAR(32) NOT NULL,
  `u_main_wikidoc_sys_id` VARCHAR(32) NOT NULL,
  `u_wikidoc_fullname` VARCHAR(255) NOT NULL,

  `sys_updated_by` VARCHAR(40) DEFAULT NULL,
  `sys_updated_on` DATETIME DEFAULT NULL,
  `sys_created_by` VARCHAR(40) DEFAULT NULL,
  `sys_created_on` DATETIME DEFAULT NULL,
  `sys_mod_count` INT(11) DEFAULT NULL,
  PRIMARY KEY (`sys_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

CREATE TABLE `exception_wikidoc_wikidoc_rel` (
  `sys_id` VARCHAR(32) NOT NULL,
  `u_exception_wikidoc_sys_id` VARCHAR(32) NOT NULL,
  `u_wikidoc_fullname` VARCHAR(255) NOT NULL,

  `sys_updated_by` VARCHAR(40) DEFAULT NULL,
  `sys_updated_on` DATETIME DEFAULT NULL,
  `sys_created_by` VARCHAR(40) DEFAULT NULL,
  `sys_created_on` DATETIME DEFAULT NULL,
  `sys_mod_count` INT(11) DEFAULT NULL,
  PRIMARY KEY (`sys_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

CREATE TABLE `final_wikidoc_wikidoc_rel` (
  `sys_id` VARCHAR(32) NOT NULL,
  `u_final_wikidoc_sys_id` VARCHAR(32) NOT NULL,
  `u_wikidoc_fullname` VARCHAR(255) NOT NULL,

  `sys_updated_by` VARCHAR(40) DEFAULT NULL,
  `sys_updated_on` DATETIME DEFAULT NULL,
  `sys_created_by` VARCHAR(40) DEFAULT NULL,
  `sys_created_on` DATETIME DEFAULT NULL,
  `sys_mod_count` INT(11) DEFAULT NULL,
  PRIMARY KEY (`sys_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;


CREATE TABLE `content_wikidoc_actiontask_rel` (
  `sys_id` VARCHAR(32) NOT NULL,
  `u_content_wikidoc_sys_id` VARCHAR(32) NOT NULL,
  `u_actiontask_fullname` VARCHAR(255) NOT NULL,

  `sys_updated_by` VARCHAR(40) DEFAULT NULL,
  `sys_updated_on` DATETIME DEFAULT NULL,
  `sys_created_by` VARCHAR(40) DEFAULT NULL,
  `sys_created_on` DATETIME DEFAULT NULL,
  `sys_mod_count` INT(11) DEFAULT NULL,
  PRIMARY KEY (`sys_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

CREATE TABLE `main_wikidoc_actiontask_rel` (
  `sys_id` VARCHAR(32) NOT NULL,
  `u_main_wikidoc_sys_id` VARCHAR(32) NOT NULL,
  `u_actiontask_fullname` VARCHAR(32) NOT NULL,

  `sys_updated_by` VARCHAR(40) DEFAULT NULL,
  `sys_updated_on` DATETIME DEFAULT NULL,
  `sys_created_by` VARCHAR(40) DEFAULT NULL,
  `sys_created_on` DATETIME DEFAULT NULL,
  `sys_mod_count` INT(11) DEFAULT NULL,
  PRIMARY KEY (`sys_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;
			

CREATE TABLE `exception_wikidoc_actiontask_rel` (
  `sys_id` VARCHAR(32) NOT NULL,
  `u_exception_wikidoc_sys_id` VARCHAR(32) NOT NULL,
  `u_actiontask_fullname` VARCHAR(32) NOT NULL,

  `sys_updated_by` VARCHAR(40) DEFAULT NULL,
  `sys_updated_on` DATETIME DEFAULT NULL,
  `sys_created_by` VARCHAR(40) DEFAULT NULL,
  `sys_created_on` DATETIME DEFAULT NULL,
  `sys_mod_count` INT(11) DEFAULT NULL,
  PRIMARY KEY (`sys_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

CREATE TABLE `final_wikidoc_actiontask_rel` (
  `sys_id` VARCHAR(32) NOT NULL,
  `u_final_wikidoc_sys_id` VARCHAR(32) NOT NULL,
  `u_actiontask_fullname` VARCHAR(32) NOT NULL,

  `sys_updated_by` VARCHAR(40) DEFAULT NULL,
  `sys_updated_on` DATETIME DEFAULT NULL,
  `sys_created_by` VARCHAR(40) DEFAULT NULL,
  `sys_created_on` DATETIME DEFAULT NULL,
  `sys_mod_count` INT(11) DEFAULT NULL,
  PRIMARY KEY (`sys_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

/*******************/

CREATE TABLE `wikidoc_resolve_action_pkg_rel` 
(
  `sys_id` VARCHAR(32) NOT NULL,
  `u_wikidoc_sys_id` VARCHAR(32) NOT NULL,
  `u_tag_fullname` VARCHAR(255) NOT NULL,

  `sys_updated_by` VARCHAR(40) DEFAULT NULL,
  `sys_updated_on` DATETIME DEFAULT NULL,
  `sys_created_by` VARCHAR(40) DEFAULT NULL,
  `sys_created_on` DATETIME DEFAULT NULL,
  `sys_mod_count` INT(11) DEFAULT NULL,
  PRIMARY KEY (`sys_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

CREATE TABLE `resolve_action_task_resolve_action_pkg_rel` 
(
  `sys_id` VARCHAR(32) NOT NULL,
  `u_resolve_action_task_sys_id` VARCHAR(32) NOT NULL,
  `u_tag_fullname` VARCHAR(255) NOT NULL,

  `sys_updated_by` VARCHAR(40) DEFAULT NULL,
  `sys_updated_on` DATETIME DEFAULT NULL,
  `sys_created_by` VARCHAR(40) DEFAULT NULL,
  `sys_created_on` DATETIME DEFAULT NULL,
  `sys_mod_count` INT(11) DEFAULT NULL,
  PRIMARY KEY (`sys_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

CREATE TABLE `wikidoc_statistics` 
(
  `sys_id` 			VARCHAR(32) NOT NULL,
  `u_wikidoc_sys_id` 		VARCHAR(32) NOT NULL,
  `u_content_checksum` 		VARCHAR(255) DEFAULT NULL,
  `u_model_checksum` 		VARCHAR(255) DEFAULT NULL,
  `u_exception_checksum` 	VARCHAR(255) DEFAULT NULL,
  `u_final_checksum` 		VARCHAR(255) DEFAULT NULL,
  `u_view_count` 		INTEGER,
  `u_edit_count` 		INTEGER,
  `u_execute_count` 		INTEGER,
  `sys_updated_by` 		VARCHAR(40) DEFAULT NULL,
  `sys_updated_on` 		DATETIME DEFAULT NULL,
  `sys_created_by` 		VARCHAR(40) DEFAULT NULL,
  `sys_created_on` 		DATETIME DEFAULT NULL,
  `sys_mod_count` 		INT(11) DEFAULT NULL,
  PRIMARY KEY (`sys_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;



	