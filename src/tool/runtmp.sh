#!/bin/bash

# set os type
linux=false
sunos=false
case "`uname`" in
Linux*) linux=true;;
SunOS*) sunos=true;;
esac

# properties
BIN=`echo $0 | awk '{ print substr( $0, 0, length($0)-9 ) }'`

cd ${BIN}/../..
DIST=`pwd`
if [ -d ${DIST}/jdktmp ]; then
    export JAVA_HOME=${DIST}/jdktmp
else
    export JAVA_HOME=${DIST}/jdk
fi

export ELASTICSEARCH_PID_FILE=${DIST}/elasticsearch.new/bin/lock

if [ -f ${ELASTICSEARCH_PID_FILE} ]; then
    if [ $# -eq 0 ] || [ "$1" != "1" ]; then
        echo "Lock File Detected"
        echo "Cancelling ElasticSearch Startup"
        exit 1
    fi
fi

if $sunos; then
    export JAVA_OPTS="$JAVA_OPTS -d64"
fi

echo "Starting ElasticSearch"
elasticsearch.new/bin/elasticsearch -p $ELASTICSEARCH_PID_FILE -d

for i in 1 2 3 4 5
do
  if [ -f $ELASTICSEARCH_PID_FILE ]; then
    break
  else
    sleep 1
  fi
done

PID=`cat $ELASTICSEARCH_PID_FILE 2>/dev/null`

echo "Started ElasticSearch pid: ${PID}"
