@echo off

set DIST=%CD%/../..
set JRE=%DIST%/jdk

set CP=%DIST%/tools/bin
set CP=%CP%;%DIST%/lib/commons-httpclient.jar
set CP=%CP%;%DIST%/lib/commons-codec.jar
set CP=%CP%;%DIST%/lib/commons-lang.jar
set CP=%CP%;%DIST%/lib/commons-logging.jar

"%JRE%\bin\java" -cp "%CP%" WebClient %1 %2 %3 %4 %5 %6 %7 %8 %9
