/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.query;

public     /**
 * This class hold the tokens collectd from a query. This is required
 * because the Email doesn't have a formal query (like SQL) language. We
 * collect these tokens and compare with incoming message from email server.
 */
class QueryToken
{
    // Supported operators for HPOM. Must be same with HPOMQueryTranslator
    public final static String EQUAL = "|=|";
    public final static String GREATER = "|>|";
    public final static String GREATER_OR_EQUAL = "|>=|";
    public final static String LESS_OR_EQUAL = "|<=|";
    public final static String LESS = "|<|";
    public final static String CONTAINS = "|CONTAINS|";
    public final static String AND = "|&&|";
    public final static String OR = "|OR|";

    private final String key;
    private final String operator;
    private final String value;

    QueryToken(String key, String operator, String value) throws Exception
    {
        if (key == null || operator == null || value == null)
        {
            throw new Exception("Key, Operator, and Value must not be nul");
        }
        else
        {
            this.key = key.trim();
            this.operator = operator.trim();
            this.value = value.trim();
        }
    }

    public String getKey()
    {
        return key;
    }

    public String getOperator()
    {
        return operator;
    }

    public String getValue()
    {
        return value;
    }

    
    // Splits the query item and build a QueryToken object.
    public static QueryToken get(String queryItem) throws Exception
    {
        QueryToken result = null;
        if (queryItem.contains(QueryToken.EQUAL))
        {
            result = splitQueryItem(queryItem, QueryToken.EQUAL);
        }
        else if (queryItem.contains(QueryToken.GREATER))
        {
            result = splitQueryItem(queryItem, QueryToken.GREATER);
        }
        else if (queryItem.contains(QueryToken.GREATER_OR_EQUAL))
        {
            result = splitQueryItem(queryItem, QueryToken.GREATER_OR_EQUAL);
        }
        else if (queryItem.contains(QueryToken.LESS_OR_EQUAL))
        {
            result = splitQueryItem(queryItem, QueryToken.LESS_OR_EQUAL);
        }
        else if (queryItem.contains(QueryToken.LESS))
        {
            result = splitQueryItem(queryItem, QueryToken.LESS);
        }
        else if (queryItem.contains(QueryToken.CONTAINS))
        {
            result = splitQueryItem(queryItem, QueryToken.CONTAINS);
        }

        return result;
    }

    private static QueryToken splitQueryItem(String queryItem, String delimeter) throws Exception
    {
        QueryToken result = null;
        String convertedDelem = java.util.regex.Pattern.quote(delimeter);

        String[] items = queryItem.split(convertedDelem);
        if (items.length == 2)
        {
            result = new QueryToken(items[0], delimeter, items[1]);
        }
        else
        {
            // This should never happen but just an extra protection.
            throw new Exception("Invalid query part: " + queryItem);
        }
        return result;
    }
}


