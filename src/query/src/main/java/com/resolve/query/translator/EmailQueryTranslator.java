/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.query.translator;

import com.resolve.query.QueryParser.Assignment;
import com.resolve.query.QueryParser.BoolOperator;
import com.resolve.query.QueryUtils;

/**
 * Extends the AbstractQueryTranslator class, implementing the QueryTranslator interface.
 * Translates according to Remedy syntax.
 */
public class EmailQueryTranslator extends AbstractQueryTranslator
{
    public String translateBoolOperator(BoolOperator op)
    {
        String print = "";
        switch (op)
        {
            case AND:
                print = " |&&| ";
                break;
            /*case OR:
                print = " |||| ";
                break;
            */
        }
        if (print.equals(""))
        {
            throw new RuntimeException("Email Gateway Error: Unsupported boolean operator.");
        }
        
        return print;
    } //translateBoolOperator

    public String translateAssignment(Assignment obj)
    {
        String print = obj.getName();
        switch (obj.getOperator())
        {
            case EQUAL: //for String and Datetime
                print += " |=| ";
                break;
            case GREATER:  //for Datetime
                print += " |>| ";
                break;
            case GREATER_OR_EQUAL:  //for Datetime
                print += " |>=| ";
                break;
            case LESS_OR_EQUAL: //for Datetime
                print += " |<=| ";
                break;
            case LESS: //for Datetime
                print += " |<| ";
                break;
            case CONTAINS: // for String
            {
                QueryUtils.validateString(obj.getValue(), "CONTAINS", "EMAIL");
                print += " |CONTAINS| ";
                break;
            }
            default:
                throw new RuntimeException("HPOM Error: Unsupported comparison operator.");
        }
        if (QueryUtils.isString(obj.getValue()))        // a String requires reformatting for HPOM
        {
            obj.setValue(QueryUtils.trimAndRemoveEscape(obj.getValue()));
        }
        print += obj.getValue();
        
        return print;
    }

} //RemedyQueryTranslator
