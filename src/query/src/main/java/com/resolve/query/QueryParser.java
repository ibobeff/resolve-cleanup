/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.query;

    /*
     * NOTES
     * -----
     * Use public API to retrieve parsed data and print.
     *
     * - Parser errors throw RecognitionException which must be caught by the program.
     * - BoolOperator, CmpOperator enum's and Assignment class are declared public.
     * - Includes the QueryTranslator interface.
     * - Public API returns ArrayList's of various features:
     *  - public ArrayList<Assignment> getAssignments();        //for the specific query
     *  - public ArrayList<BoolOperator> getBoolOperators();        //for the specific query
     *  - public static ArrayList<String> getCmpOperatorsString();  //all supported CmpOperator's
     *  - public static ArrayList<String> getBoolOperatorsString(); //all supported BoolOperator's
     */


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class QueryParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "CHAR", "CLOSE_PARENS", "CONTAINS", "DIGIT", "ENDS_WITH", "EQUAL", "ESC_SEQ", "FLOAT", "GREATER", "GREATER_OR_EQUAL", "INT", "LESS", "LESS_OR_EQUAL", "NAME", "NOT_EQUAL", "OID", "OPEN_PARENS", "OR", "STARTS_WITH", "STRING", "WS"
    };

    public static final int EOF=-1;
    public static final int AND=4;
    public static final int CHAR=5;
    public static final int CLOSE_PARENS=6;
    public static final int CONTAINS=7;
    public static final int DIGIT=8;
    public static final int ENDS_WITH=9;
    public static final int EQUAL=10;
    public static final int ESC_SEQ=11;
    public static final int FLOAT=12;
    public static final int GREATER=13;
    public static final int GREATER_OR_EQUAL=14;
    public static final int INT=15;
    public static final int LESS=16;
    public static final int LESS_OR_EQUAL=17;
    public static final int NAME=18;
    public static final int NOT_EQUAL=19;
    public static final int OID=20;
    public static final int OPEN_PARENS=21;
    public static final int OR=22;
    public static final int STARTS_WITH=23;
    public static final int STRING=24;
    public static final int WS=25;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public QueryParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public QueryParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

    public String[] getTokenNames() { return QueryParser.tokenNames; }
    public String getGrammarFileName() { return "C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g"; }


        /**
         * The BoolOperator enum defines the various logical boolean operators that can be used in
         * a query statement, such as AND and OR.
         */
        public enum BoolOperator { AND, OR }
        
        /**
         * The CmpOperator enum defines the various comparison operators that can be used in a query
         * statement, particularly in the context "varName CmpOperator varValue". Not all CmpOperators
         * may be used in all output formats.
         */
        public enum CmpOperator { EQUAL, NOT_EQUAL, GREATER, GREATER_OR_EQUAL,
                   LESS, LESS_OR_EQUAL, CONTAINS, STARTS_WITH, ENDS_WITH }
        
        /**
         * The Assignment object encapsulates the information required for a condition in a query statement.
         * It based off the structure "varName cmpOperator value". Various API methods include
         * String getName(), void setOperator(CmpOperator op), CmpOperator getOperator(), void setValue(String value),
         * and String getValue().
         */
        public class Assignment extends Object
        {
            private String name;            //variable name
            private CmpOperator operator;       //(enum) comparison operator
            private String value;           //variable value
            
            /**
             * Constructor initializes an Assignmnet object.
             * @param name The Assignment varName
             * @param operator The CmpOperator used.
             * @param value The Assignment varValue.
             */
            public Assignment(String name, CmpOperator operator, String value)
            {
                this.name = name;
                this.operator = operator;
                this.value = value;
            }
            
            /** Returns the name of the Assignment */
            public String getName() { return name; }
            /** Sets the name of the assignment */
            public void setName(String name) { this.name = name; }
            
            /** Sets the operator for the Assignment */
            public void setOperator(CmpOperator operator) { this.operator = operator; }
            /** Returns the operator for the Assignment */
            public CmpOperator getOperator() { return operator; }
            
            /** Sets the value for the Assignment */
            public void setValue(String value) { this.value = value; }
            /** Returns the value of the Assignment */
            public String getValue() { return value; }
        }
        
        /**
         * The MismatchedParensException class is used when the number of open parentheses does
         * not match the number of closed parentheses in a query.
         */
        public class MismatchedParensException extends Exception
        {
            private static final long serialVersionUID = -8401295032917303924L;

			/**
             * Constructor initializes a MismatchedParensException exception object.
             * @param message The message to be displayed.
             */
            public MismatchedParensException(String message)
            {
                super(message);
            }
        }
        
        
        /*
         * These two ArrayLists keep track of the query statement's data. The assignments List
         * must be of Objects because it can include both Assignments and parentheses. The list
         * elements are ordered in the same sequence as the query.
         */
        private ArrayList<BoolOperator> boolOperators = new ArrayList<BoolOperator>();
        private ArrayList<Object> assignments = new ArrayList<Object>();
        
        /* Private variables to keep track of the number of each parenthesis */
        private int openParens = 0;
        private int closeParens = 0;
        
        /**
         * Returns a copy of the boolOperators ArrayList for the query. Deep copy. Note that this
         * method should be called only after prog() has been run and the Lists have been populated.
         * The Assignments and BoolOperators Lists alternate assign1, bool1, assign2, bool2, ...
         * AFTER one DISREGARDS the parentheses. Make sure the parentheses in the Assignment List
         * are taken care of.
         */
        public ArrayList<BoolOperator> getBoolOperators()
        {
            ArrayList<BoolOperator> newList = new ArrayList<BoolOperator>();
            for(int i = 0; i < boolOperators.size(); i++)
            {
                newList.add(boolOperators.get(i));
            }
            return newList;
        }
        
        /**
         * Returns a copy of the assignments ArrayList for the query. Deep copy. Note that this
         * method should be called only after prog() has been run and the Lists have been populated.
         * The Assignments and BoolOperators Lists alternate assign1, bool1, assign2, bool2, ...
         * AFTER one DISREGARDS the parentheses. Make sure the parentheses in the Assignment List
         * are taken care of.
         */
        public ArrayList<Object> getAssignments()
        {
            ArrayList<Object> newList = new ArrayList<Object>();
            for(int i = 0; i < assignments.size(); i++)
            {
                newList.add(assignments.get(i));
            }
            return newList;
        }
        
        /**
         * Returns a list of Strings of the names of all the supported CmpOperators.
         */
        public static ArrayList<String> getCmpOperatorsString()
        {
            ArrayList<String> list = new ArrayList<String>();
            for(CmpOperator op : CmpOperator.values())
            {
                list.add(op.name());
            }
            return list;
        }
        
        /**
         * Returns a list of Strings of the names of all the supported BoolOperators.
         */
        public static ArrayList<String> getBoolOperatorsString()
        {
            ArrayList<String> list = new ArrayList<String>();
            for(BoolOperator op : BoolOperator.values())
            {
                list.add(op.name());
            }
            return list;
        }
        
        /*
         * Trims a number to remove leading and trailing zeroes.
         */
        private String trimNumber(String number)
        {
            boolean negative = false;
            if(number.startsWith("-"))
            {
                negative = true;
                number = number.substring(1);
            }
            if(number.contains("."))
            {
                while(number.charAt(number.length() - 1) == '0')
                {
                    number = number.substring(0, number.length() - 1);
                }
            }
            while(!number.equals("") && number.charAt(0) == '0')
            {
                number = number.substring(1);
            }
            if(number.equals(".") || number.equals(""))
            {
                number = "0";
            }
            else if(negative)
            {
                number = "-" + number;
            }
            if(number.endsWith("."))
            {
                number = number.substring(0, number.length() - 1);
            }
            return number;
            }
        
        /* These two methods are used to tell the parser to automatically stop upon error. */
        
        @Override
        protected Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException
        {
            throw new MismatchedTokenException(ttype, input);
        }
        
        @Override
        public Object recoverFromMismatchedSet(IntStream input, RecognitionException e, BitSet follow) throws RecognitionException
        {
                throw e;
        }



    // $ANTLR start "prog"
    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:296:1: prog : query EOF ;
    public final void prog() throws RecognitionException, MismatchedParensException {
        try {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:297:2: ( query EOF )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:297:4: query EOF
            {
            pushFollow(FOLLOW_query_in_prog68);
            query();

            state._fsp--;


            match(input,EOF,FOLLOW_EOF_in_prog70); 


                        if(openParens != closeParens)
                        {
                            throw new MismatchedParensException("Mismatched parentheses: " + openParens + " open parens, " + closeParens + " close parens");
                        }
                    

            }

        }
           
            /* Throws RecognitionException to allow us to catch it in our own code in Eclipse. */
            catch (RecognitionException e)
            {
                throw e;
                }

        finally {
            // do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "prog"



    // $ANTLR start "query"
    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:307:1: query : assignment ( moreAssignments )* ;
    public final void query() throws RecognitionException {
        try {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:307:7: ( assignment ( moreAssignments )* )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:307:9: assignment ( moreAssignments )*
            {
            pushFollow(FOLLOW_assignment_in_query85);
            assignment();

            state._fsp--;


            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:307:20: ( moreAssignments )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==AND||LA1_0==FLOAT||LA1_0==INT||LA1_0==NAME||(LA1_0 >= OID && LA1_0 <= OR)) ) {
                    alt1=1;
                }


                switch (alt1) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:307:21: moreAssignments
                    {
                    pushFollow(FOLLOW_moreAssignments_in_query88);
                    moreAssignments();

                    state._fsp--;


                    }
                    break;

                default :
                    break loop1;
                }
            } while (true);


            }

        }
           
            /* Throws RecognitionException to allow us to catch it in our own code in Eclipse. */
            catch (RecognitionException e)
            {
                throw e;
                }

        finally {
            // do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "query"



    // $ANTLR start "moreAssignments"
    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:311:1: moreAssignments : ( boolOp assignment | assignment );
    public final void moreAssignments() throws RecognitionException {
        try {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:312:2: ( boolOp assignment | assignment )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==AND||LA2_0==OR) ) {
                alt2=1;
            }
            else if ( (LA2_0==FLOAT||LA2_0==INT||LA2_0==NAME||(LA2_0 >= OID && LA2_0 <= OPEN_PARENS)) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }
            switch (alt2) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:312:4: boolOp assignment
                    {
                    pushFollow(FOLLOW_boolOp_in_moreAssignments102);
                    boolOp();

                    state._fsp--;


                    pushFollow(FOLLOW_assignment_in_moreAssignments104);
                    assignment();

                    state._fsp--;


                    }
                    break;
                case 2 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:313:4: assignment
                    {
                    pushFollow(FOLLOW_assignment_in_moreAssignments109);
                    assignment();

                    state._fsp--;


                     boolOperators.add(BoolOperator.AND); 

                    }
                    break;

            }
        }
           
            /* Throws RecognitionException to allow us to catch it in our own code in Eclipse. */
            catch (RecognitionException e)
            {
                throw e;
                }

        finally {
            // do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "moreAssignments"



    // $ANTLR start "boolOp"
    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:317:1: boolOp : ( AND | OR );
    public final void boolOp() throws RecognitionException {
        try {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:317:8: ( AND | OR )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==AND) ) {
                alt3=1;
            }
            else if ( (LA3_0==OR) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;

            }
            switch (alt3) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:317:10: AND
                    {
                    match(input,AND,FOLLOW_AND_in_boolOp122); 

                     boolOperators.add(BoolOperator.AND); 

                    }
                    break;
                case 2 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:318:4: OR
                    {
                    match(input,OR,FOLLOW_OR_in_boolOp129); 

                     boolOperators.add(BoolOperator.OR); 

                    }
                    break;

            }
        }
           
            /* Throws RecognitionException to allow us to catch it in our own code in Eclipse. */
            catch (RecognitionException e)
            {
                throw e;
                }

        finally {
            // do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "boolOp"



    // $ANTLR start "assignment"
    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:322:1: assignment : ( varName op value | OPEN_PARENS varName op value | OPEN_PARENS varName op value CLOSE_PARENS | varName op value CLOSE_PARENS );
    public final void assignment() throws RecognitionException {
        Token OPEN_PARENS4=null;
        Token OPEN_PARENS8=null;
        Token CLOSE_PARENS12=null;
        Token CLOSE_PARENS16=null;
        QueryParser.varName_return varName1 =null;

        CmpOperator op2 =null;

        String value3 =null;

        QueryParser.varName_return varName5 =null;

        CmpOperator op6 =null;

        String value7 =null;

        QueryParser.varName_return varName9 =null;

        CmpOperator op10 =null;

        String value11 =null;

        QueryParser.varName_return varName13 =null;

        CmpOperator op14 =null;

        String value15 =null;


        try {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:323:2: ( varName op value | OPEN_PARENS varName op value | OPEN_PARENS varName op value CLOSE_PARENS | varName op value CLOSE_PARENS )
            int alt4=4;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==FLOAT||LA4_0==INT||LA4_0==NAME||LA4_0==OID) ) {
                switch ( input.LA(2) ) {
                case EQUAL:
                    {
                    switch ( input.LA(3) ) {
                    case INT:
                        {
                        int LA4_13 = input.LA(4);

                        if ( (LA4_13==EOF||LA4_13==AND||LA4_13==FLOAT||LA4_13==INT||LA4_13==NAME||(LA4_13 >= OID && LA4_13 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_13==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 13, input);

                            throw nvae;

                        }
                        }
                        break;
                    case FLOAT:
                        {
                        int LA4_14 = input.LA(4);

                        if ( (LA4_14==EOF||LA4_14==AND||LA4_14==FLOAT||LA4_14==INT||LA4_14==NAME||(LA4_14 >= OID && LA4_14 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_14==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 14, input);

                            throw nvae;

                        }
                        }
                        break;
                    case STRING:
                        {
                        int LA4_15 = input.LA(4);

                        if ( (LA4_15==EOF||LA4_15==AND||LA4_15==FLOAT||LA4_15==INT||LA4_15==NAME||(LA4_15 >= OID && LA4_15 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_15==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 15, input);

                            throw nvae;

                        }
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 4, 3, input);

                        throw nvae;

                    }

                    }
                    break;
                case NOT_EQUAL:
                    {
                    switch ( input.LA(3) ) {
                    case INT:
                        {
                        int LA4_13 = input.LA(4);

                        if ( (LA4_13==EOF||LA4_13==AND||LA4_13==FLOAT||LA4_13==INT||LA4_13==NAME||(LA4_13 >= OID && LA4_13 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_13==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 13, input);

                            throw nvae;

                        }
                        }
                        break;
                    case FLOAT:
                        {
                        int LA4_14 = input.LA(4);

                        if ( (LA4_14==EOF||LA4_14==AND||LA4_14==FLOAT||LA4_14==INT||LA4_14==NAME||(LA4_14 >= OID && LA4_14 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_14==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 14, input);

                            throw nvae;

                        }
                        }
                        break;
                    case STRING:
                        {
                        int LA4_15 = input.LA(4);

                        if ( (LA4_15==EOF||LA4_15==AND||LA4_15==FLOAT||LA4_15==INT||LA4_15==NAME||(LA4_15 >= OID && LA4_15 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_15==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 15, input);

                            throw nvae;

                        }
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 4, 4, input);

                        throw nvae;

                    }

                    }
                    break;
                case GREATER:
                    {
                    switch ( input.LA(3) ) {
                    case INT:
                        {
                        int LA4_13 = input.LA(4);

                        if ( (LA4_13==EOF||LA4_13==AND||LA4_13==FLOAT||LA4_13==INT||LA4_13==NAME||(LA4_13 >= OID && LA4_13 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_13==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 13, input);

                            throw nvae;

                        }
                        }
                        break;
                    case FLOAT:
                        {
                        int LA4_14 = input.LA(4);

                        if ( (LA4_14==EOF||LA4_14==AND||LA4_14==FLOAT||LA4_14==INT||LA4_14==NAME||(LA4_14 >= OID && LA4_14 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_14==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 14, input);

                            throw nvae;

                        }
                        }
                        break;
                    case STRING:
                        {
                        int LA4_15 = input.LA(4);

                        if ( (LA4_15==EOF||LA4_15==AND||LA4_15==FLOAT||LA4_15==INT||LA4_15==NAME||(LA4_15 >= OID && LA4_15 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_15==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 15, input);

                            throw nvae;

                        }
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 4, 5, input);

                        throw nvae;

                    }

                    }
                    break;
                case GREATER_OR_EQUAL:
                    {
                    switch ( input.LA(3) ) {
                    case INT:
                        {
                        int LA4_13 = input.LA(4);

                        if ( (LA4_13==EOF||LA4_13==AND||LA4_13==FLOAT||LA4_13==INT||LA4_13==NAME||(LA4_13 >= OID && LA4_13 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_13==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 13, input);

                            throw nvae;

                        }
                        }
                        break;
                    case FLOAT:
                        {
                        int LA4_14 = input.LA(4);

                        if ( (LA4_14==EOF||LA4_14==AND||LA4_14==FLOAT||LA4_14==INT||LA4_14==NAME||(LA4_14 >= OID && LA4_14 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_14==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 14, input);

                            throw nvae;

                        }
                        }
                        break;
                    case STRING:
                        {
                        int LA4_15 = input.LA(4);

                        if ( (LA4_15==EOF||LA4_15==AND||LA4_15==FLOAT||LA4_15==INT||LA4_15==NAME||(LA4_15 >= OID && LA4_15 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_15==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 15, input);

                            throw nvae;

                        }
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 4, 6, input);

                        throw nvae;

                    }

                    }
                    break;
                case LESS:
                    {
                    switch ( input.LA(3) ) {
                    case INT:
                        {
                        int LA4_13 = input.LA(4);

                        if ( (LA4_13==EOF||LA4_13==AND||LA4_13==FLOAT||LA4_13==INT||LA4_13==NAME||(LA4_13 >= OID && LA4_13 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_13==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 13, input);

                            throw nvae;

                        }
                        }
                        break;
                    case FLOAT:
                        {
                        int LA4_14 = input.LA(4);

                        if ( (LA4_14==EOF||LA4_14==AND||LA4_14==FLOAT||LA4_14==INT||LA4_14==NAME||(LA4_14 >= OID && LA4_14 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_14==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 14, input);

                            throw nvae;

                        }
                        }
                        break;
                    case STRING:
                        {
                        int LA4_15 = input.LA(4);

                        if ( (LA4_15==EOF||LA4_15==AND||LA4_15==FLOAT||LA4_15==INT||LA4_15==NAME||(LA4_15 >= OID && LA4_15 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_15==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 15, input);

                            throw nvae;

                        }
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 4, 7, input);

                        throw nvae;

                    }

                    }
                    break;
                case LESS_OR_EQUAL:
                    {
                    switch ( input.LA(3) ) {
                    case INT:
                        {
                        int LA4_13 = input.LA(4);

                        if ( (LA4_13==EOF||LA4_13==AND||LA4_13==FLOAT||LA4_13==INT||LA4_13==NAME||(LA4_13 >= OID && LA4_13 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_13==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 13, input);

                            throw nvae;

                        }
                        }
                        break;
                    case FLOAT:
                        {
                        int LA4_14 = input.LA(4);

                        if ( (LA4_14==EOF||LA4_14==AND||LA4_14==FLOAT||LA4_14==INT||LA4_14==NAME||(LA4_14 >= OID && LA4_14 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_14==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 14, input);

                            throw nvae;

                        }
                        }
                        break;
                    case STRING:
                        {
                        int LA4_15 = input.LA(4);

                        if ( (LA4_15==EOF||LA4_15==AND||LA4_15==FLOAT||LA4_15==INT||LA4_15==NAME||(LA4_15 >= OID && LA4_15 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_15==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 15, input);

                            throw nvae;

                        }
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 4, 8, input);

                        throw nvae;

                    }

                    }
                    break;
                case CONTAINS:
                    {
                    switch ( input.LA(3) ) {
                    case INT:
                        {
                        int LA4_13 = input.LA(4);

                        if ( (LA4_13==EOF||LA4_13==AND||LA4_13==FLOAT||LA4_13==INT||LA4_13==NAME||(LA4_13 >= OID && LA4_13 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_13==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 13, input);

                            throw nvae;

                        }
                        }
                        break;
                    case FLOAT:
                        {
                        int LA4_14 = input.LA(4);

                        if ( (LA4_14==EOF||LA4_14==AND||LA4_14==FLOAT||LA4_14==INT||LA4_14==NAME||(LA4_14 >= OID && LA4_14 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_14==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 14, input);

                            throw nvae;

                        }
                        }
                        break;
                    case STRING:
                        {
                        int LA4_15 = input.LA(4);

                        if ( (LA4_15==EOF||LA4_15==AND||LA4_15==FLOAT||LA4_15==INT||LA4_15==NAME||(LA4_15 >= OID && LA4_15 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_15==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 15, input);

                            throw nvae;

                        }
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 4, 9, input);

                        throw nvae;

                    }

                    }
                    break;
                case STARTS_WITH:
                    {
                    switch ( input.LA(3) ) {
                    case INT:
                        {
                        int LA4_13 = input.LA(4);

                        if ( (LA4_13==EOF||LA4_13==AND||LA4_13==FLOAT||LA4_13==INT||LA4_13==NAME||(LA4_13 >= OID && LA4_13 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_13==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 13, input);

                            throw nvae;

                        }
                        }
                        break;
                    case FLOAT:
                        {
                        int LA4_14 = input.LA(4);

                        if ( (LA4_14==EOF||LA4_14==AND||LA4_14==FLOAT||LA4_14==INT||LA4_14==NAME||(LA4_14 >= OID && LA4_14 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_14==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 14, input);

                            throw nvae;

                        }
                        }
                        break;
                    case STRING:
                        {
                        int LA4_15 = input.LA(4);

                        if ( (LA4_15==EOF||LA4_15==AND||LA4_15==FLOAT||LA4_15==INT||LA4_15==NAME||(LA4_15 >= OID && LA4_15 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_15==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 15, input);

                            throw nvae;

                        }
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 4, 10, input);

                        throw nvae;

                    }

                    }
                    break;
                case ENDS_WITH:
                    {
                    switch ( input.LA(3) ) {
                    case INT:
                        {
                        int LA4_13 = input.LA(4);

                        if ( (LA4_13==EOF||LA4_13==AND||LA4_13==FLOAT||LA4_13==INT||LA4_13==NAME||(LA4_13 >= OID && LA4_13 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_13==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 13, input);

                            throw nvae;

                        }
                        }
                        break;
                    case FLOAT:
                        {
                        int LA4_14 = input.LA(4);

                        if ( (LA4_14==EOF||LA4_14==AND||LA4_14==FLOAT||LA4_14==INT||LA4_14==NAME||(LA4_14 >= OID && LA4_14 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_14==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 14, input);

                            throw nvae;

                        }
                        }
                        break;
                    case STRING:
                        {
                        int LA4_15 = input.LA(4);

                        if ( (LA4_15==EOF||LA4_15==AND||LA4_15==FLOAT||LA4_15==INT||LA4_15==NAME||(LA4_15 >= OID && LA4_15 <= OR)) ) {
                            alt4=1;
                        }
                        else if ( (LA4_15==CLOSE_PARENS) ) {
                            alt4=4;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 15, input);

                            throw nvae;

                        }
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 4, 11, input);

                        throw nvae;

                    }

                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 4, 1, input);

                    throw nvae;

                }

            }
            else if ( (LA4_0==OPEN_PARENS) ) {
                int LA4_2 = input.LA(2);

                if ( (LA4_2==FLOAT||LA4_2==INT||LA4_2==NAME||LA4_2==OID) ) {
                    switch ( input.LA(3) ) {
                    case EQUAL:
                        {
                        switch ( input.LA(4) ) {
                        case INT:
                            {
                            int LA4_27 = input.LA(5);

                            if ( (LA4_27==EOF||LA4_27==AND||LA4_27==FLOAT||LA4_27==INT||LA4_27==NAME||(LA4_27 >= OID && LA4_27 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_27==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 27, input);

                                throw nvae;

                            }
                            }
                            break;
                        case FLOAT:
                            {
                            int LA4_28 = input.LA(5);

                            if ( (LA4_28==EOF||LA4_28==AND||LA4_28==FLOAT||LA4_28==INT||LA4_28==NAME||(LA4_28 >= OID && LA4_28 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_28==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 28, input);

                                throw nvae;

                            }
                            }
                            break;
                        case STRING:
                            {
                            int LA4_29 = input.LA(5);

                            if ( (LA4_29==EOF||LA4_29==AND||LA4_29==FLOAT||LA4_29==INT||LA4_29==NAME||(LA4_29 >= OID && LA4_29 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_29==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 29, input);

                                throw nvae;

                            }
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 16, input);

                            throw nvae;

                        }

                        }
                        break;
                    case NOT_EQUAL:
                        {
                        switch ( input.LA(4) ) {
                        case INT:
                            {
                            int LA4_27 = input.LA(5);

                            if ( (LA4_27==EOF||LA4_27==AND||LA4_27==FLOAT||LA4_27==INT||LA4_27==NAME||(LA4_27 >= OID && LA4_27 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_27==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 27, input);

                                throw nvae;

                            }
                            }
                            break;
                        case FLOAT:
                            {
                            int LA4_28 = input.LA(5);

                            if ( (LA4_28==EOF||LA4_28==AND||LA4_28==FLOAT||LA4_28==INT||LA4_28==NAME||(LA4_28 >= OID && LA4_28 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_28==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 28, input);

                                throw nvae;

                            }
                            }
                            break;
                        case STRING:
                            {
                            int LA4_29 = input.LA(5);

                            if ( (LA4_29==EOF||LA4_29==AND||LA4_29==FLOAT||LA4_29==INT||LA4_29==NAME||(LA4_29 >= OID && LA4_29 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_29==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 29, input);

                                throw nvae;

                            }
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 17, input);

                            throw nvae;

                        }

                        }
                        break;
                    case GREATER:
                        {
                        switch ( input.LA(4) ) {
                        case INT:
                            {
                            int LA4_27 = input.LA(5);

                            if ( (LA4_27==EOF||LA4_27==AND||LA4_27==FLOAT||LA4_27==INT||LA4_27==NAME||(LA4_27 >= OID && LA4_27 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_27==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 27, input);

                                throw nvae;

                            }
                            }
                            break;
                        case FLOAT:
                            {
                            int LA4_28 = input.LA(5);

                            if ( (LA4_28==EOF||LA4_28==AND||LA4_28==FLOAT||LA4_28==INT||LA4_28==NAME||(LA4_28 >= OID && LA4_28 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_28==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 28, input);

                                throw nvae;

                            }
                            }
                            break;
                        case STRING:
                            {
                            int LA4_29 = input.LA(5);

                            if ( (LA4_29==EOF||LA4_29==AND||LA4_29==FLOAT||LA4_29==INT||LA4_29==NAME||(LA4_29 >= OID && LA4_29 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_29==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 29, input);

                                throw nvae;

                            }
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 18, input);

                            throw nvae;

                        }

                        }
                        break;
                    case GREATER_OR_EQUAL:
                        {
                        switch ( input.LA(4) ) {
                        case INT:
                            {
                            int LA4_27 = input.LA(5);

                            if ( (LA4_27==EOF||LA4_27==AND||LA4_27==FLOAT||LA4_27==INT||LA4_27==NAME||(LA4_27 >= OID && LA4_27 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_27==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 27, input);

                                throw nvae;

                            }
                            }
                            break;
                        case FLOAT:
                            {
                            int LA4_28 = input.LA(5);

                            if ( (LA4_28==EOF||LA4_28==AND||LA4_28==FLOAT||LA4_28==INT||LA4_28==NAME||(LA4_28 >= OID && LA4_28 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_28==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 28, input);

                                throw nvae;

                            }
                            }
                            break;
                        case STRING:
                            {
                            int LA4_29 = input.LA(5);

                            if ( (LA4_29==EOF||LA4_29==AND||LA4_29==FLOAT||LA4_29==INT||LA4_29==NAME||(LA4_29 >= OID && LA4_29 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_29==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 29, input);

                                throw nvae;

                            }
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 19, input);

                            throw nvae;

                        }

                        }
                        break;
                    case LESS:
                        {
                        switch ( input.LA(4) ) {
                        case INT:
                            {
                            int LA4_27 = input.LA(5);

                            if ( (LA4_27==EOF||LA4_27==AND||LA4_27==FLOAT||LA4_27==INT||LA4_27==NAME||(LA4_27 >= OID && LA4_27 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_27==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 27, input);

                                throw nvae;

                            }
                            }
                            break;
                        case FLOAT:
                            {
                            int LA4_28 = input.LA(5);

                            if ( (LA4_28==EOF||LA4_28==AND||LA4_28==FLOAT||LA4_28==INT||LA4_28==NAME||(LA4_28 >= OID && LA4_28 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_28==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 28, input);

                                throw nvae;

                            }
                            }
                            break;
                        case STRING:
                            {
                            int LA4_29 = input.LA(5);

                            if ( (LA4_29==EOF||LA4_29==AND||LA4_29==FLOAT||LA4_29==INT||LA4_29==NAME||(LA4_29 >= OID && LA4_29 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_29==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 29, input);

                                throw nvae;

                            }
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 20, input);

                            throw nvae;

                        }

                        }
                        break;
                    case LESS_OR_EQUAL:
                        {
                        switch ( input.LA(4) ) {
                        case INT:
                            {
                            int LA4_27 = input.LA(5);

                            if ( (LA4_27==EOF||LA4_27==AND||LA4_27==FLOAT||LA4_27==INT||LA4_27==NAME||(LA4_27 >= OID && LA4_27 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_27==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 27, input);

                                throw nvae;

                            }
                            }
                            break;
                        case FLOAT:
                            {
                            int LA4_28 = input.LA(5);

                            if ( (LA4_28==EOF||LA4_28==AND||LA4_28==FLOAT||LA4_28==INT||LA4_28==NAME||(LA4_28 >= OID && LA4_28 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_28==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 28, input);

                                throw nvae;

                            }
                            }
                            break;
                        case STRING:
                            {
                            int LA4_29 = input.LA(5);

                            if ( (LA4_29==EOF||LA4_29==AND||LA4_29==FLOAT||LA4_29==INT||LA4_29==NAME||(LA4_29 >= OID && LA4_29 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_29==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 29, input);

                                throw nvae;

                            }
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 21, input);

                            throw nvae;

                        }

                        }
                        break;
                    case CONTAINS:
                        {
                        switch ( input.LA(4) ) {
                        case INT:
                            {
                            int LA4_27 = input.LA(5);

                            if ( (LA4_27==EOF||LA4_27==AND||LA4_27==FLOAT||LA4_27==INT||LA4_27==NAME||(LA4_27 >= OID && LA4_27 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_27==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 27, input);

                                throw nvae;

                            }
                            }
                            break;
                        case FLOAT:
                            {
                            int LA4_28 = input.LA(5);

                            if ( (LA4_28==EOF||LA4_28==AND||LA4_28==FLOAT||LA4_28==INT||LA4_28==NAME||(LA4_28 >= OID && LA4_28 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_28==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 28, input);

                                throw nvae;

                            }
                            }
                            break;
                        case STRING:
                            {
                            int LA4_29 = input.LA(5);

                            if ( (LA4_29==EOF||LA4_29==AND||LA4_29==FLOAT||LA4_29==INT||LA4_29==NAME||(LA4_29 >= OID && LA4_29 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_29==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 29, input);

                                throw nvae;

                            }
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 22, input);

                            throw nvae;

                        }

                        }
                        break;
                    case STARTS_WITH:
                        {
                        switch ( input.LA(4) ) {
                        case INT:
                            {
                            int LA4_27 = input.LA(5);

                            if ( (LA4_27==EOF||LA4_27==AND||LA4_27==FLOAT||LA4_27==INT||LA4_27==NAME||(LA4_27 >= OID && LA4_27 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_27==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 27, input);

                                throw nvae;

                            }
                            }
                            break;
                        case FLOAT:
                            {
                            int LA4_28 = input.LA(5);

                            if ( (LA4_28==EOF||LA4_28==AND||LA4_28==FLOAT||LA4_28==INT||LA4_28==NAME||(LA4_28 >= OID && LA4_28 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_28==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 28, input);

                                throw nvae;

                            }
                            }
                            break;
                        case STRING:
                            {
                            int LA4_29 = input.LA(5);

                            if ( (LA4_29==EOF||LA4_29==AND||LA4_29==FLOAT||LA4_29==INT||LA4_29==NAME||(LA4_29 >= OID && LA4_29 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_29==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 29, input);

                                throw nvae;

                            }
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 23, input);

                            throw nvae;

                        }

                        }
                        break;
                    case ENDS_WITH:
                        {
                        switch ( input.LA(4) ) {
                        case INT:
                            {
                            int LA4_27 = input.LA(5);

                            if ( (LA4_27==EOF||LA4_27==AND||LA4_27==FLOAT||LA4_27==INT||LA4_27==NAME||(LA4_27 >= OID && LA4_27 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_27==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 27, input);

                                throw nvae;

                            }
                            }
                            break;
                        case FLOAT:
                            {
                            int LA4_28 = input.LA(5);

                            if ( (LA4_28==EOF||LA4_28==AND||LA4_28==FLOAT||LA4_28==INT||LA4_28==NAME||(LA4_28 >= OID && LA4_28 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_28==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 28, input);

                                throw nvae;

                            }
                            }
                            break;
                        case STRING:
                            {
                            int LA4_29 = input.LA(5);

                            if ( (LA4_29==EOF||LA4_29==AND||LA4_29==FLOAT||LA4_29==INT||LA4_29==NAME||(LA4_29 >= OID && LA4_29 <= OR)) ) {
                                alt4=2;
                            }
                            else if ( (LA4_29==CLOSE_PARENS) ) {
                                alt4=3;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 4, 29, input);

                                throw nvae;

                            }
                            }
                            break;
                        default:
                            NoViableAltException nvae =
                                new NoViableAltException("", 4, 24, input);

                            throw nvae;

                        }

                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 4, 12, input);

                        throw nvae;

                    }

                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 4, 2, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;

            }
            switch (alt4) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:323:4: varName op value
                    {
                    pushFollow(FOLLOW_varName_in_assignment144);
                    varName1=varName();

                    state._fsp--;


                    pushFollow(FOLLOW_op_in_assignment146);
                    op2=op();

                    state._fsp--;


                    pushFollow(FOLLOW_value_in_assignment148);
                    value3=value();

                    state._fsp--;


                     assignments.add(new Assignment((varName1!=null?input.toString(varName1.start,varName1.stop):null), op2, value3)); 

                    }
                    break;
                case 2 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:324:4: OPEN_PARENS varName op value
                    {
                    OPEN_PARENS4=(Token)match(input,OPEN_PARENS,FOLLOW_OPEN_PARENS_in_assignment155); 

                    pushFollow(FOLLOW_varName_in_assignment157);
                    varName5=varName();

                    state._fsp--;


                    pushFollow(FOLLOW_op_in_assignment159);
                    op6=op();

                    state._fsp--;


                    pushFollow(FOLLOW_value_in_assignment161);
                    value7=value();

                    state._fsp--;



                                for(int i = 0; i < (OPEN_PARENS4!=null?OPEN_PARENS4.getText():null).length(); i++)
                                {
                                    assignments.add('(');
                                    openParens++;
                                }
                                assignments.add(new Assignment((varName5!=null?input.toString(varName5.start,varName5.stop):null), op6, value7));
                            

                    }
                    break;
                case 3 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:333:4: OPEN_PARENS varName op value CLOSE_PARENS
                    {
                    OPEN_PARENS8=(Token)match(input,OPEN_PARENS,FOLLOW_OPEN_PARENS_in_assignment170); 

                    pushFollow(FOLLOW_varName_in_assignment172);
                    varName9=varName();

                    state._fsp--;


                    pushFollow(FOLLOW_op_in_assignment174);
                    op10=op();

                    state._fsp--;


                    pushFollow(FOLLOW_value_in_assignment176);
                    value11=value();

                    state._fsp--;


                    CLOSE_PARENS12=(Token)match(input,CLOSE_PARENS,FOLLOW_CLOSE_PARENS_in_assignment178); 


                                for(int i = 0; i < (OPEN_PARENS8!=null?OPEN_PARENS8.getText():null).length(); i++)
                                {
                                    assignments.add('(');
                                    openParens++;
                                }
                                assignments.add(new Assignment((varName9!=null?input.toString(varName9.start,varName9.stop):null), op10, value11));
                                for(int i = 0; i < (CLOSE_PARENS12!=null?CLOSE_PARENS12.getText():null).length(); i++)
                                {
                                    assignments.add(')');
                                    closeParens++;
                                }
                            

                    }
                    break;
                case 4 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:347:4: varName op value CLOSE_PARENS
                    {
                    pushFollow(FOLLOW_varName_in_assignment187);
                    varName13=varName();

                    state._fsp--;


                    pushFollow(FOLLOW_op_in_assignment189);
                    op14=op();

                    state._fsp--;


                    pushFollow(FOLLOW_value_in_assignment191);
                    value15=value();

                    state._fsp--;


                    CLOSE_PARENS16=(Token)match(input,CLOSE_PARENS,FOLLOW_CLOSE_PARENS_in_assignment193); 


                                assignments.add(new Assignment((varName13!=null?input.toString(varName13.start,varName13.stop):null), op14, value15));
                                for(int i = 0; i < (CLOSE_PARENS16!=null?CLOSE_PARENS16.getText():null).length(); i++)
                                {
                                    assignments.add(')');
                                    closeParens++;
                                }
                            

                    }
                    break;

            }
        }
           
            /* Throws RecognitionException to allow us to catch it in our own code in Eclipse. */
            catch (RecognitionException e)
            {
                throw e;
                }

        finally {
            // do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "assignment"


    public static class varName_return extends ParserRuleReturnScope {
    };


    // $ANTLR start "varName"
    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:359:1: varName : ( NAME | OID | FLOAT | INT );
    public final QueryParser.varName_return varName() throws RecognitionException {
        QueryParser.varName_return retval = new QueryParser.varName_return();
        retval.start = input.LT(1);


        try {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:359:9: ( NAME | OID | FLOAT | INT )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:
            {
            if ( input.LA(1)==FLOAT||input.LA(1)==INT||input.LA(1)==NAME||input.LA(1)==OID ) {
                input.consume();
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


        }
           
            /* Throws RecognitionException to allow us to catch it in our own code in Eclipse. */
            catch (RecognitionException e)
            {
                throw e;
                }

        finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "varName"



    // $ANTLR start "op"
    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:363:1: op returns [CmpOperator op] : ( EQUAL | NOT_EQUAL | GREATER | GREATER_OR_EQUAL | LESS | LESS_OR_EQUAL | CONTAINS | STARTS_WITH | ENDS_WITH );
    public final CmpOperator op() throws RecognitionException {
        CmpOperator op = null;


        try {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:364:2: ( EQUAL | NOT_EQUAL | GREATER | GREATER_OR_EQUAL | LESS | LESS_OR_EQUAL | CONTAINS | STARTS_WITH | ENDS_WITH )
            int alt5=9;
            switch ( input.LA(1) ) {
            case EQUAL:
                {
                alt5=1;
                }
                break;
            case NOT_EQUAL:
                {
                alt5=2;
                }
                break;
            case GREATER:
                {
                alt5=3;
                }
                break;
            case GREATER_OR_EQUAL:
                {
                alt5=4;
                }
                break;
            case LESS:
                {
                alt5=5;
                }
                break;
            case LESS_OR_EQUAL:
                {
                alt5=6;
                }
                break;
            case CONTAINS:
                {
                alt5=7;
                }
                break;
            case STARTS_WITH:
                {
                alt5=8;
                }
                break;
            case ENDS_WITH:
                {
                alt5=9;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;

            }

            switch (alt5) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:364:4: EQUAL
                    {
                    match(input,EQUAL,FOLLOW_EQUAL_in_op236); 

                     op = CmpOperator.EQUAL; 

                    }
                    break;
                case 2 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:365:4: NOT_EQUAL
                    {
                    match(input,NOT_EQUAL,FOLLOW_NOT_EQUAL_in_op243); 

                     op = CmpOperator.NOT_EQUAL; 

                    }
                    break;
                case 3 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:366:4: GREATER
                    {
                    match(input,GREATER,FOLLOW_GREATER_in_op250); 

                     op = CmpOperator.GREATER; 

                    }
                    break;
                case 4 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:367:4: GREATER_OR_EQUAL
                    {
                    match(input,GREATER_OR_EQUAL,FOLLOW_GREATER_OR_EQUAL_in_op257); 

                     op = CmpOperator.GREATER_OR_EQUAL; 

                    }
                    break;
                case 5 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:368:4: LESS
                    {
                    match(input,LESS,FOLLOW_LESS_in_op264); 

                     op = CmpOperator.LESS; 

                    }
                    break;
                case 6 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:369:4: LESS_OR_EQUAL
                    {
                    match(input,LESS_OR_EQUAL,FOLLOW_LESS_OR_EQUAL_in_op271); 

                     op = CmpOperator.LESS_OR_EQUAL; 

                    }
                    break;
                case 7 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:370:4: CONTAINS
                    {
                    match(input,CONTAINS,FOLLOW_CONTAINS_in_op278); 

                     op = CmpOperator.CONTAINS; 

                    }
                    break;
                case 8 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:371:4: STARTS_WITH
                    {
                    match(input,STARTS_WITH,FOLLOW_STARTS_WITH_in_op285); 

                     op = CmpOperator.STARTS_WITH; 

                    }
                    break;
                case 9 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:372:4: ENDS_WITH
                    {
                    match(input,ENDS_WITH,FOLLOW_ENDS_WITH_in_op292); 

                     op = CmpOperator.ENDS_WITH; 

                    }
                    break;

            }
        }
           
            /* Throws RecognitionException to allow us to catch it in our own code in Eclipse. */
            catch (RecognitionException e)
            {
                throw e;
                }

        finally {
            // do for sure before leaving
        }
        return op;
    }
    // $ANTLR end "op"



    // $ANTLR start "value"
    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:377:1: value returns [String text] : ( INT | FLOAT | STRING );
    public final String value() throws RecognitionException {
        String text = null;


        Token INT17=null;
        Token FLOAT18=null;
        Token STRING19=null;

        try {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:378:2: ( INT | FLOAT | STRING )
            int alt6=3;
            switch ( input.LA(1) ) {
            case INT:
                {
                alt6=1;
                }
                break;
            case FLOAT:
                {
                alt6=2;
                }
                break;
            case STRING:
                {
                alt6=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;

            }

            switch (alt6) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:378:4: INT
                    {
                    INT17=(Token)match(input,INT,FOLLOW_INT_in_value311); 

                     text = trimNumber((INT17!=null?INT17.getText():null)); 

                    }
                    break;
                case 2 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:379:4: FLOAT
                    {
                    FLOAT18=(Token)match(input,FLOAT,FOLLOW_FLOAT_in_value318); 

                     text = trimNumber((FLOAT18!=null?FLOAT18.getText():null)); 

                    }
                    break;
                case 3 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:380:4: STRING
                    {
                    STRING19=(Token)match(input,STRING,FOLLOW_STRING_in_value325); 

                     text = (STRING19!=null?STRING19.getText():null); 

                    }
                    break;

            }
        }
           
            /* Throws RecognitionException to allow us to catch it in our own code in Eclipse. */
            catch (RecognitionException e)
            {
                throw e;
                }

        finally {
            // do for sure before leaving
        }
        return text;
    }
    // $ANTLR end "value"

    // Delegated rules


 

    public static final BitSet FOLLOW_query_in_prog68 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_prog70 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_assignment_in_query85 = new BitSet(new long[]{0x0000000000749012L});
    public static final BitSet FOLLOW_moreAssignments_in_query88 = new BitSet(new long[]{0x0000000000749012L});
    public static final BitSet FOLLOW_boolOp_in_moreAssignments102 = new BitSet(new long[]{0x0000000000349000L});
    public static final BitSet FOLLOW_assignment_in_moreAssignments104 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_assignment_in_moreAssignments109 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_AND_in_boolOp122 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OR_in_boolOp129 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_varName_in_assignment144 = new BitSet(new long[]{0x00000000008B6680L});
    public static final BitSet FOLLOW_op_in_assignment146 = new BitSet(new long[]{0x0000000001009000L});
    public static final BitSet FOLLOW_value_in_assignment148 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OPEN_PARENS_in_assignment155 = new BitSet(new long[]{0x0000000000149000L});
    public static final BitSet FOLLOW_varName_in_assignment157 = new BitSet(new long[]{0x00000000008B6680L});
    public static final BitSet FOLLOW_op_in_assignment159 = new BitSet(new long[]{0x0000000001009000L});
    public static final BitSet FOLLOW_value_in_assignment161 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OPEN_PARENS_in_assignment170 = new BitSet(new long[]{0x0000000000149000L});
    public static final BitSet FOLLOW_varName_in_assignment172 = new BitSet(new long[]{0x00000000008B6680L});
    public static final BitSet FOLLOW_op_in_assignment174 = new BitSet(new long[]{0x0000000001009000L});
    public static final BitSet FOLLOW_value_in_assignment176 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_CLOSE_PARENS_in_assignment178 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_varName_in_assignment187 = new BitSet(new long[]{0x00000000008B6680L});
    public static final BitSet FOLLOW_op_in_assignment189 = new BitSet(new long[]{0x0000000001009000L});
    public static final BitSet FOLLOW_value_in_assignment191 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_CLOSE_PARENS_in_assignment193 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_EQUAL_in_op236 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NOT_EQUAL_in_op243 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_GREATER_in_op250 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_GREATER_OR_EQUAL_in_op257 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LESS_in_op264 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LESS_OR_EQUAL_in_op271 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CONTAINS_in_op278 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_STARTS_WITH_in_op285 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ENDS_WITH_in_op292 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INT_in_value311 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FLOAT_in_value318 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_STRING_in_value325 = new BitSet(new long[]{0x0000000000000002L});

}