/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.query.translator;

import com.resolve.query.QueryParser.Assignment;
import com.resolve.query.QueryParser.BoolOperator;
import com.resolve.query.QueryTranslator;
import com.resolve.query.QueryUtils;

/**
 * This class extends the AbstractQueryTranslator class, thus implementing
 * the {@link QueryTranslator} interface for the EWS gateway.
 * 
 * Example: isread = 'false' and subject contains 'test'
 *          isread = 'false' and body contains 'test'
 *          isread = 'false' and from contains 'support' and subject contains 'help'
 */
public class EWSQueryTranslator extends AbstractQueryTranslator
{
    public String translateBoolOperator(BoolOperator op)
    {
        String print = "";
        switch (op)
        {
            case AND:
                print = " |&&| ";
                break;
            case OR:
                print = " |OR| ";
                break;
        }
        if (print.equals(""))
        {
            throw new RuntimeException("EWS Error: Unsupported boolean operator.");
        }
        return print;
    }

    public String translateAssignment(Assignment obj)
    {
        String print = obj.getName();
        switch (obj.getOperator())
        {
            case EQUAL: //for String and Datetime
                print += " |=| ";
                break;
            case GREATER:  //for Datetime
                print += " |>| ";
                break;
            case GREATER_OR_EQUAL:  //for Datetime
                print += " |>=| ";
                break;
            case LESS_OR_EQUAL: //for Datetime
                print += " |<=| ";
                break;
            case LESS: //for Datetime
                print += " |<| ";
                break;
            case CONTAINS: // for String
            {
                QueryUtils.validateString(obj.getValue(), "CONTAINS", "EWS");
                print += " |CONTAINS| ";
                break;
            }
            default:
                throw new RuntimeException("EWS Error: Unsupported comparison operator.");
        }
        if (QueryUtils.isString(obj.getValue()))        // a String requires reformatting for EWS
        {
            obj.setValue(QueryUtils.trimAndRemoveEscape(obj.getValue()));
        }
        print += obj.getValue();
        
        return print;
    }

}
