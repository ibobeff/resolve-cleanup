/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.query.translator;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.resolve.query.QueryParser.Assignment;
import com.resolve.query.QueryParser.BoolOperator;
import com.resolve.query.QueryUtils;
import com.resolve.util.Log;

/**
 * This class extends the AbstractQueryTranslator class, thus implementing
 * the {@link QueryTranslator} interface for the TSRM gateway.
 * 
 *  For detailed information about TSRM operators visit:  
 *  http://pic.dhe.ibm.com/infocenter/tivihelp/v49r1/index.jsp?topic=%2Fcom.ibm.mbs.doc%2Fgp_intfrmwk%2Frest_api%2Fc_resource_attribute_param.html
 *
 */
public class TSRMQueryTranslator extends AbstractQueryTranslator
{
    public String translateBoolOperator(BoolOperator op)
    {
        String print = "";
        switch (op)
        {
            case AND:
                print = "&";
                break;
        }
        if (print.equals(""))
        {
            throw new RuntimeException("TSRM Error: Unsupported boolean operator.");
        }
        return print;
    }

    public String translateAssignment(Assignment obj)
    {
        String print = obj.getName();
        switch (obj.getOperator())
        {
            case EQUAL:
                print += "=~eq~";       //for exact match
                break;
            case NOT_EQUAL:
                print += "=~neq~";
                break;
            case GREATER:
                print += "=~gt~";
                break;
            case GREATER_OR_EQUAL:
                print += "=~gteq~";
                break;
            case LESS:
                print += "=~lt~";
                break;
            case LESS_OR_EQUAL:
                print += "=~lteq~";
                break;
            case CONTAINS:           // CONTAINS only works with Strings
            {
                QueryUtils.validateString(obj.getValue(), "CONTAINS", "TSRM");
                print += "=";
                break;
            }
            case STARTS_WITH:              //STARTS_WITH only works with Strings
            {
                QueryUtils.validateString(obj.getValue(), "STARTS_WITH", "TSRM");
                print += "=~sw~";
                break;
            }
            case ENDS_WITH:             //ENDS_WITH only works with Strings
            {
                QueryUtils.validateString(obj.getValue(), "ENDS_WITIH", "TSRM");
                print += "=~ew~";
                break;
            }
            default:
                throw new RuntimeException("TSRM Error: Unsupported comparison operator.");
        }
        if (QueryUtils.isString(obj.getValue()))        // a String requires reformatting for TSRM
        {
            obj.setValue(QueryUtils.trimAndRemoveEscape(obj.getValue()));
        }
        try
        {
            print += URLEncoder.encode(obj.getValue(), "utf-8");
        }
        catch (UnsupportedEncodingException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        return print;
    }

}
