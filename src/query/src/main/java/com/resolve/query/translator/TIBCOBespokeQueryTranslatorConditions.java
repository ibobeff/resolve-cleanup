package com.resolve.query.translator;

import com.resolve.query.QueryUtils;
import com.resolve.query.QueryParser.Assignment;
import com.resolve.query.QueryParser.BoolOperator;

public class TIBCOBespokeQueryTranslatorConditions extends AbstractQueryTranslator
{
    int assignmentCount = 0;
    
    @Override
    public String translateBoolOperator(BoolOperator op)
    {
        String print = "";
        switch (op)
        {
            case AND:
                print = " && ";
                break;
            case OR:
                print = " || ";
                break;            
        }
        if (print.equals(""))
        {
            throw new RuntimeException("TIBCO Gateway Error: Unsupported boolean operator.");
        }
        return print;
    }

    @Override
    public String translateAssignment(Assignment obj)
    {
        String print = " params[" + assignmentCount + "] ";
        assignmentCount++;
                
        return print;
    }

}
