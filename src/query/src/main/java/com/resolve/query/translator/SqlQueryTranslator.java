/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.query.translator;

import com.resolve.query.QueryParser.Assignment;
import com.resolve.query.QueryParser.BoolOperator;
import com.resolve.query.QueryUtils;

/**
 * Extends the AbstractQueryTranslator class, implementing the QueryTranslator interface.
 * Translates according to SQL syntax.
 */
public class SqlQueryTranslator extends AbstractQueryTranslator
{
    public String translateBoolOperator(BoolOperator op)
    {
        String print = "";
        switch (op)
        {
            case AND:
                print = " AND ";
                break;
            case OR:
                print = " OR ";
                break;
        }
        if (print.equals(""))
        {
            throw new RuntimeException("SQL Error: Unsupported boolean operator.");
        }
        
        return print;
    } //translateBoolOperator

    public String translateAssignment(Assignment obj)
    {
        String print = obj.getName() + " ";
        switch(obj.getOperator())
        {
            case EQUAL: print += "="; break;
            case NOT_EQUAL: print += "<>"; break;
            case GREATER: print += ">"; break;
            case GREATER_OR_EQUAL: print += ">="; break;
            case LESS: print += "<"; break;
            case LESS_OR_EQUAL: print += "<="; break;
            case CONTAINS:              //CONTAINS only works with Strings
            {
                QueryUtils.validateString(obj.getValue(), "CONTAINS", "SQL");
                obj.setValue("'%" + QueryUtils.trimString(obj.getValue()) + "%'");
                print += "LIKE";
                break;
            }
            case STARTS_WITH:           //STARTS_WITH only works with Strings
            {
                QueryUtils.validateString(obj.getValue(), "STARTS_WITH", "SQL");
                obj.setValue("'" + QueryUtils.trimString(obj.getValue()) + "%'");
                print += "LIKE";
                break;
            }
            case ENDS_WITH:             //ENDS_WITH only works with Strings
            {
                QueryUtils.validateString(obj.getValue(), "ENDS_WITH", "SQL");
                obj.setValue("'%" + QueryUtils.trimString(obj.getValue()) + "'");
                print += "LIKE";
                break;
            }
            default: throw new RuntimeException("SQL Error: Unsupported comparison operator.");
        }
        print += " " + obj.getValue();
        
        return print;
    } //translateAssignment

} //SqlQueryTranslator
