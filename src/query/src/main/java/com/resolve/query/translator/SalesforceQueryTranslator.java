/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.query.translator;

import com.resolve.query.QueryParser.Assignment;
import com.resolve.query.QueryParser.BoolOperator;

import com.resolve.query.QueryUtils;
//TODO: Make sure that these translations are still relevant in Salesforce
/**
 * This class implements the {@link QueryTranslator} interface for the Salesforce gateway.
 * 
 *  For detailed information about Salesforce operators visit:  
 *  
 *
 */
public class SalesforceQueryTranslator extends AbstractQueryTranslator
{
    //@Override
    public String translateBoolOperator(BoolOperator op)
    {
        String print = "";
        switch (op)
        {
            case AND:
                print = "^";
                break;
            case OR:
                print = "^OR";
                break;
        }
        if (print.equals(""))
        {
            throw new RuntimeException("Salesforce Error: Unsupported boolean operator.");
        }
        return print;
    }

    //@Override
    public String translateAssignment(Assignment obj)
    {
        String print = obj.getName();
        switch (obj.getOperator())
        {
            case EQUAL:
                print += "=";
                break;
            case NOT_EQUAL:
                print += "!=";
                break;
            case GREATER:
                print += ">";
                break;
            case GREATER_OR_EQUAL:
                print += ">=";
                break;
            case LESS:
                print += "<";
                break;
            case LESS_OR_EQUAL:
                print += "<=";
                break;
            case CONTAINS:               // CONTAINS only works with Strings
            {
                QueryUtils.validateString(obj.getValue(), "CONTAINS", "Salesforce");
                print += "CONTAINS";
                break;
            }
            case STARTS_WITH:            // STARTS_WITH only works with Strings
            {
                QueryUtils.validateString(obj.getValue(), "STARTS_WITH", "Salesforce");
                print += "STARTSWITH";
                break;
            }
            case ENDS_WITH:
            {
                QueryUtils.validateString(obj.getValue(), "ENDS_WITH", "Salesforce");
                print += "ENDSWITH";
                break;
            }
            default:
                throw new RuntimeException("Salesforce Error: Unsupported comparison operator.");
        }
        if (QueryUtils.isString(obj.getValue()))        // a String requires reformatting for Salesforce
        {
            obj.setValue(QueryUtils.trimAndRemoveEscape(obj.getValue()));
        }
        print += obj.getValue();
        
        return print;
    }

}
