/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.query;

@SuppressWarnings("serial")
public class QueryException extends Exception
{
    private static final long serialVersionUID = 2748793488662646515L;

	public QueryException(String message)
    {
        super(message);
    }

    public QueryException(String message, Throwable t)
    {
        super(message, t);
    }
}
