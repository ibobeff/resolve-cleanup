package com.resolve.query;

import java.util.Date;

import org.joda.time.DateTime;
import com.resolve.util.ComparisonUtils;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;

public class AssignmentComparisonUtils extends ComparisonUtils
{    
    public static boolean compareAssignment(QueryParser.Assignment assignment)
    {
        boolean result = compareAssignment(assignment.getName(), assignment.getValue(), assignment.getOperator()); 
        return result;
    }
    
    /**
     *  Compares the values by date first. If either the variable or source are not detected as
     *  dates it will default to using ComparisonUtils.comparAny()
     *  
     *  @param Variable - This would be the left hand side of a comparison
     *  @param Source - This would be the right hand side of a comparison
     *  @param operator - Operator for the assignment: CONTAINS, GTE, LTE, ...
     */
    public static boolean compareAssignment(Object variable, Object source, QueryParser.CmpOperator operator)
    {
        Boolean result = null;        
        Date varDate = null;
        Date srcDate = null;
           
        String translatedOperator = translateOperator(operator);
        if(variable instanceof String)
        {
            String varString = (String) variable;
            varDate = DateUtils.convertStringToDate(varString);
        }
        if(source instanceof String)
        {
            String srcString = (String) source;
            srcDate = DateUtils.convertStringToDate(srcString);
        }
        if(varDate != null && srcDate != null)
        {
            boolean compareResults = compareDate(varDate, srcDate, translatedOperator);
            result =  compareResults;
        }
        else
        {
           result = compareAny(variable, source, translatedOperator);
        }
        
        return result;
    }
    
    //translates the operators to a format the ComparisonUtils class likes
    private static String translateOperator(QueryParser.CmpOperator operator)
    {
        switch(operator)
        {
            case EQUAL:
                return "==";
            case NOT_EQUAL:
                return "!=";
            case LESS:
                return "<";
            case LESS_OR_EQUAL:
                return "<=";
            case GREATER:
                return ">";
            case GREATER_OR_EQUAL:
                return ">=";
            case CONTAINS:
                return "contains";
            case STARTS_WITH:
                return "startsWith";
            case ENDS_WITH:
                return "endsWith";
            default:
                throw new RuntimeException("Unknown operator for assignment comparison: " + operator);
        }
    }
}