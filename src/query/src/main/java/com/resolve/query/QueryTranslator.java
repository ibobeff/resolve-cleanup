/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.query;

import com.resolve.query.QueryParser.Assignment;
import com.resolve.query.QueryParser.BoolOperator;

/**
 * Interface defines methods necessary for the ResolveQuery object to
 * properly translate the query. Calling translate() on the ResolveQuery
 * object will start a process in which these methods are called alternating
 * on a list of Assignment's and a list of BoolOperator's. The lists are
 * loaded to directly correspond to the query.
 * 
 * In each method, use the information provided in either the Assignment object
 * or BoolOperator enum to return a String that represents proper syntax
 * for the output format.
 * 
 * See existing QueryTranslator's for examples.
 */
public interface QueryTranslator
{
    /**
     * Return a String that represents proper output syntax based on which BoolOperator
     * enum is used. For a list of all supported BoolOperator's use the
     * ResolveQuery.getSupportedBoolOperators() method.
     * 
     * (Example: BoolOperator.AND will return "&")
     * 
     * @param op The enum BoolOperator to be translated.
     */
    String translateBoolOperator(BoolOperator op);
    
    /**
     * Return a String that represents proper output syntax based on the information
     * encapsulated in the Assignment object. The Assignment object contains String
     * name, CmpOperator operator, and String value with appropriate getters/setters.
     * For a list of all supported CmpOperator enums, use the ResolveQuery.getSupportedCmpOperators()
     * method.
     * 
     * @param obj The Assignment object to be translated.
     */
    String translateAssignment(Assignment obj);
    
} //QueryTranslator
