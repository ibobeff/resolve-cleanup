package com.resolve.query.translator;

import com.resolve.query.QueryUtils;
import com.resolve.query.QueryParser.Assignment;
import com.resolve.query.QueryParser.BoolOperator;

public class TIBCOBespokeQueryTranslator extends AbstractQueryTranslator
{

    @Override
    public String translateBoolOperator(BoolOperator op)
    {
        String print = "";
        switch (op)
        {
            case AND:
                print = " && ";
                break;
            case OR:
                print = " || ";
                break;
            
        }
        if (print.equals(""))
        {
            throw new RuntimeException("TIBCO Gateway Error: Unsupported boolean operator.");
        }
        return print;
    }

    @Override
    public String translateAssignment(Assignment obj)
    {
        String print = obj.getName();
        switch (obj.getOperator())
        {
            case EQUAL: //for String and Datetime
                print += " = ";
                break;
            case NOT_EQUAL:
                print += " != ";
                break;               
            case GREATER:  //for Datetime
                print += " > ";
                break;
            case GREATER_OR_EQUAL:  //for Datetime
                print += " >= ";
                break;
            case LESS_OR_EQUAL: //for Datetime
                print += " <= ";
                break;
            case LESS: //for Datetime
                print += " < ";
                break;
            case CONTAINS: // for String
            {
                QueryUtils.validateString(obj.getValue(), "CONTAINS", "TIBCO");
                print += " CONTAINS ";
                break;
            }
            default:
                throw new RuntimeException("TIBCO: Error Unsupported comparison operator.");
        }
        if (QueryUtils.isString(obj.getValue()))        // a String requires reformatting for HPOM
        {
            obj.setValue("'"+QueryUtils.trimAndRemoveEscape(obj.getValue())+"'");
        }
        print += obj.getValue();
        
        return print;
    }

}
