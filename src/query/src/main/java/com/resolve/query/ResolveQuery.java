/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.query;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

import com.resolve.query.QueryParser.Assignment;
import com.resolve.query.QueryParser.BoolOperator;
import com.resolve.query.QueryParser.MismatchedParensException;
import com.resolve.query.translator.AbstractQueryTranslator;
import com.resolve.query.translator.EmailQueryTranslator;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ResolveQuery
{
    private final AbstractQueryTranslator queryTranslator;

    /**
     * Constructor initializes a new ResolveQuery translator object.
     * @param queryTranslator Must extend the AbstractQueryTranslator class. Immutable.
     */
    public ResolveQuery(AbstractQueryTranslator queryTranslator)
    {
        this.queryTranslator = queryTranslator;
    } //ResolveQuery

    /**
     * Returns a translated String according to the rules of the AbstractQueryTranslator. Throws a regular
     * Exception upon error (the message may be translated from a subclass of Exception).
     * @param query To be translated.
     */
    public String translate(String query) throws RecognitionException, MismatchedParensException
    {
        String result = null;

        QueryLexer lex = new QueryLexer(new ANTLRStringStream(query));
        CommonTokenStream tokens = new CommonTokenStream(lex);
        QueryParser g = new QueryParser(tokens, null);
        g.prog();
        result = translateQuery(queryTranslator, g);
        return result;
    } //translate

    /**
     * Translates the query using the rules specified by the AbstactQueryTranslator. Throws a RecognitionException
     * which will be caught by the parent method.
     * @param translator The translator extending the AbstractQueryTranslator class.
     * @param g The QueryParser that has run g.prog() to properly load the parsed data.
     */
    private String translateQuery(AbstractQueryTranslator translator, QueryParser g) throws RecognitionException
    {
        ArrayList<Object> assignments = g.getAssignments();
        ArrayList<BoolOperator> boolOperators = g.getBoolOperators();

        StringBuilder result = new StringBuilder();
        while(!assignments.isEmpty())
        {
            result.append(checkOpenParens(assignments));
            result.append(translator.translateAssignment((Assignment)assignments.remove(0)));
            result.append(checkCloseParens(assignments));
            if(!boolOperators.isEmpty())
            {
                result.append(translator.translateBoolOperator(boolOperators.remove(0)));
            }
        }
        return result.toString();
    } //translateQuery

    /**
     * Checks for any open parentheses before an Assignment object.
     * @param assignments The ArrayList of Object data.
     */
    private String checkOpenParens(ArrayList<Object> assignments)
    {
        StringBuilder result = new StringBuilder();
        while(!assignments.isEmpty() && assignments.get(0) instanceof Character && (Character)assignments.get(0) == '(')
        {
            assignments.remove(0);
            result.append(queryTranslator.translateParens(true));
        }
        return result.toString();
    }

    /**
     * Checks for any close parentheses at the end of an Assignment object.
     * @param assignments The ArrayList of Object data.
     */
    private String checkCloseParens(ArrayList<Object> assignments)
    {
        StringBuilder result = new StringBuilder();
        while(!assignments.isEmpty() && assignments.get(0) instanceof Character && (Character)assignments.get(0) == ')')
        {
            assignments.remove(0);
            result.append(queryTranslator.translateParens(false));
        }
        return result.toString();
    }

    /**
     * Returns an ArrayList of Assignment data corresponding to the query. Normally used in conjunction
     * with getBoolOperatorData(). The Assignment and BoolOperator Lists alternate assign1, bool1, assign2,
     * bool2,... AFTER one DISREGARDS the parentheses included with the Assignments. Be sure to account for
     * the fact that getAssignmentData() includes both Assignments and parentheses. Throws a regular
     * Exception upon error (the message may be translated from a subclass of Exception).
     * @param query To get Assignment data from.
     */
    public static ArrayList<Object> getAssignmentData(String query) throws Exception
    {
        QueryLexer lex = new QueryLexer(new ANTLRStringStream(query));
        CommonTokenStream tokens = new CommonTokenStream(lex);
        QueryParser g = new QueryParser(tokens, null);

        try
        {
            g.prog();
            return g.getAssignments();
        }
        catch (RecognitionException ex)         //Construct message and throw more general Exception
        {
            StringBuilder message = new StringBuilder("Parser error around ");
            message.append("line " + ex.line + ", index " + ex.charPositionInLine);
            if(ex.token != null)
            {
                message.append(", token '" + ex.token.getText() + "'");
            }
            message.append(".");
            throw new Exception(message.toString());
        }
        catch (MismatchedParensException ex)
        {
            throw new Exception(ex.getMessage());
        }
    } //getAssignmentData

    /**
     * Returns an ArrayList of BoolOperator data corresponding to the query. Normally used in conjunction
     * with getAssignmentData(). The Assignment and BoolOperator Lists alternate assign1, bool1, assign2,
     * bool2,... AFTER one DISREGARDS the parentheses included with the Assignments. Be sure to account for
     * the fact that getAssignmentData() includes both Assignments and parentheses. Throws a regular
     * Exception upon error (the message may be translated from a subclass of Exception).
     * @param query To get BoolOperator data from.
     */
    public static ArrayList<BoolOperator> getBoolOperatorData(String query) throws Exception
    {
        QueryLexer lex = new QueryLexer(new ANTLRStringStream(query));
        CommonTokenStream tokens = new CommonTokenStream(lex);
        QueryParser g = new QueryParser(tokens, null);

        try
        {
            g.prog();
            return g.getBoolOperators();
        }
        catch (RecognitionException ex)         //Construct message and throw more general Exception
        {
            StringBuilder message = new StringBuilder("Parser error around ");
            message.append("line " + ex.line + ", index " + ex.charPositionInLine);
            if(ex.token != null)
            {
                message.append(", token '" + ex.token.getText() + "'");
            }
            message.append(".");
            throw new Exception(message.toString());
        }
        catch (MismatchedParensException ex)
        {
            throw new Exception(ex.getMessage());
        }
    } //getBoolOperatorData

    /**
     * Returns an ArrayList of all supported enum BoolOperator's with names in String form.
     */
    public static ArrayList<String> getSupportedBoolOperators()
    {
        return QueryParser.getBoolOperatorsString();
    } //getSupportedBoolOperators

    /**
     * Returns an ArrayList of all supported enum CmpOperator's with names in String form.
     */
    public static ArrayList<String> getSupportedCmpOperators()
    {
        return QueryParser.getCmpOperatorsString();
    } //getSupportedCmpOperators

    /**
     * Compare two String values.
     *
     * @param actualValue - String to compare
     * @param valueFromQuery - String to compare "original" with.
     * @param operator
     * @return
     * @throws Exception
     */
    public boolean isStringValueMatch(String actualValue, String valueFromQuery, String operator) throws Exception
    {
        boolean result = false;

        if(StringUtils.isNotBlank(actualValue))
        {
            //for equals comparison the email should be coming as plain text otherwise
            //there are other html characters added and doesn't work.
            if (operator.equals(QueryToken.EQUAL))
            {
                result = valueFromQuery.toLowerCase().equalsIgnoreCase(actualValue.trim().toLowerCase());
            }
            else if (operator.equals(QueryToken.CONTAINS))
            {
                //result = valueFromQuery.toLowerCase().contains(actualValue.toLowerCase());
                result = actualValue.toLowerCase().contains(valueFromQuery.toLowerCase());
            }
        }

        if(!result)
        {
            throw new Exception(valueFromQuery + " " +  operator + " " + actualValue + " did not match.");
        }

        return result;
    }

    public boolean isNumberMatch(String actualValue, String valueFromQuery, String operator)
    {
        boolean result = false;

        if(StringUtils.isNotBlank(actualValue) && StringUtils.isNotBlank(valueFromQuery) && StringUtils.isNotBlank(operator))
        {
            BigDecimal actualNumber = new BigDecimal(actualValue);
            BigDecimal valueFromQueryNumber = new BigDecimal(valueFromQuery);
            int comparedValue = actualNumber.compareTo(valueFromQueryNumber);

            if(QueryToken.GREATER.equals(operator) && comparedValue == 1)
            {
                result = true;
            }
            else if(QueryToken.GREATER_OR_EQUAL.equals(operator) && (comparedValue >= 0))
            {
                result = true;
            }
            else if(QueryToken.EQUAL.equals(operator) && comparedValue == 0)
            {
                result = true;
            }
            else if(QueryToken.LESS_OR_EQUAL.equals(operator) && (comparedValue <= 0))
            {
                result = true;
            }
            else if(QueryToken.LESS.equals(operator) && comparedValue == -1)
            {
                result = true;
            }
        }

        return result;
    }

    public boolean isDateMatch(Date actualDate, String valueFromQuery, String operator, Set<SimpleDateFormat> dateFormats) throws QueryException
    {
        boolean result = false;

        if(actualDate != null && StringUtils.isNotBlank(valueFromQuery) && StringUtils.isNotBlank(operator))
        {
            QueryDate queryDate;
            try
            {
                queryDate = parseDate(actualDate, valueFromQuery, dateFormats);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw new QueryException(e.getMessage(), e);
            }

            Date dateFromQuery = queryDate.dateFromQuery;
            actualDate = queryDate.actualDate;
            int comparedValue = actualDate.compareTo(dateFromQuery);

            if(QueryToken.GREATER.equals(operator) && comparedValue >= 1)
            {
                result = true;
            }
            else if(QueryToken.GREATER_OR_EQUAL.equals(operator) && (comparedValue >= 0))
            {
                result = true;
            }
            else if(QueryToken.EQUAL.equals(operator) && comparedValue == 0)
            {
                result = true;
            }
            else if(QueryToken.LESS_OR_EQUAL.equals(operator) && (comparedValue <= 0))
            {
                result = true;
            }
            else if(QueryToken.LESS.equals(operator) && comparedValue <= -1)
            {
                result = true;
            }
        }

        return result;
    }

    private class QueryDate
    {
        Date actualDate;
        Date dateFromQuery;
    }

    private QueryDate parseDate(Date actualDate, String dateFromQuery, Set<SimpleDateFormat> dateFormats) throws Exception
    {
        QueryDate result = new QueryDate();
        for(SimpleDateFormat dateFormat : dateFormats)
        {
            try
            {
                Date parsedDate = dateFormat.parse(dateFromQuery);
                //if the parse succeeded then set this format as the winner
                Date reformattedActualDate = dateFormat.parse(DateUtils.convertTimeInMillisToString(actualDate.getTime(), dateFormat.toPattern()));
                result.dateFromQuery = parsedDate;
                result.actualDate = reformattedActualDate;
                break;
            }
            catch (ParseException e)
            {
                //ignore and move on
            }
        }

        if(result.dateFromQuery == null)
        {
            throw new Exception("Unsupported date: " + dateFromQuery);
        }
        return result;
    }

    /**
     * Used for testing purposes. Outputs translations of a sample query to the console.
     */
//    public static void main(String args[]) throws Exception
//    {
//        Log.init();
//
//        ResolveQuery resolveQuery = null;
//
//        /*String query = "(age = 30 and joe = 3) and (bob = 3)";
//        System.out.println("QUERY :" + query + '\n');
//
//        resolveQuery = new ResolveQuery(new ServiceNowQueryTranslator());
//        String result = resolveQuery.translate(query);
//        System.out.println("ServiceNow :" + result + '\n');
//
//        resolveQuery = new ResolveQuery(new SqlQueryTranslator());
//        result = resolveQuery.translate(query);
//        System.out.println("SQL :" + result + '\n');
//
//        resolveQuery = new ResolveQuery(new TSRMQueryTranslator());
//        result = resolveQuery.translate(query);
//        System.out.println("TSRM :" + result + '\n');
//        */
//        resolveQuery = new ResolveQuery(new EmailQueryTranslator());
//        Date date1 = new Date();
//        Date date2 = new Date();
//        Set<SimpleDateFormat> dateFormats = new HashSet<SimpleDateFormat>();
//        boolean result1 = resolveQuery.isDateMatch(date1, date2.toString(), "|>|", dateFormats);
//        System.out.println("Email isDateMatch :" + result1 + '\n');
//
//    } //main

} //ResolveQuery