/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.query.mapper;
    
    /* Refer to MapperParser for notes */


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class MapperLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__7=7;
    public static final int T__8=8;
    public static final int KEY=4;
    public static final int VALUE=5;
    public static final int WS=6;

        /* Used to tell lexer to immediately stop upon error. Throws a RecognitionError that goes to the parser. */
        
        @Override
        public void reportError(RecognitionException e)
        {
            Thrower.sneakyThrow(e);
        }
        
        static class Thrower
        {
            private static Throwable t;
            
            private Thrower() throws Throwable
            {
                throw t;
            }
            
            public static synchronized void sneakyThrow(Throwable t)
            {
                Thrower.t = t;
                try
                {
                    Thrower.class.newInstance();
                }
                catch (InstantiationException e)
                {
                    throw new IllegalArgumentException(e);
                }
                catch (IllegalAccessException e)
                {
                    throw new IllegalArgumentException(e);
                }
                finally
                {
                    Thrower.t = null;
                }
            }
        }


    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public MapperLexer() {} 
    public MapperLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public MapperLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g"; }

    // $ANTLR start "T__7"
    public final void mT__7() throws RecognitionException {
        try {
            int _type = T__7;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:48:6: ( ';' )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:48:8: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__7"

    // $ANTLR start "T__8"
    public final void mT__8() throws RecognitionException {
        try {
            int _type = T__8;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:49:6: ( '=' )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:49:8: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "T__8"

    // $ANTLR start "KEY"
    public final void mKEY() throws RecognitionException {
        try {
            int _type = KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:154:5: ( ( 'e' | 'E' ) ( 'v' | 'V' ) ( 'e' | 'E' ) ( 'n' | 'N' ) ( 't' | 'T' ) | ( 'r' | 'R' ) ( 'e' | 'E' ) ( 'f' | 'F' ) ( 'e' | 'E' ) ( 'r' | 'R' ) ( 'e' | 'E' ) ( 'n' | 'N' ) ( 'c' | 'C' ) ( 'e' | 'E' ) | ( 'w' | 'W' ) ( 'i' | 'I' ) ( 'k' | 'K' ) ( 'i' | 'I' ) | ( 'p' | 'P' ) ( 'r' | 'R' ) ( 'o' | 'O' ) ( 'b' | 'B' ) ( 'l' | 'L' ) ( 'e' | 'E' ) ( 'm' | 'M' ) ( 'i' | 'I' ) ( 'd' | 'D' ) | ( 'e' | 'E' ) ( 'v' | 'V' ) ( 'e' | 'E' ) ( 'n' | 'N' ) ( 't' | 'T' ) ( 'r' | 'R' ) ( 'e' | 'E' ) ( 'f' | 'F' ) | ( 'a' | 'A' ) ( 'l' | 'L' ) ( 'e' | 'E' ) ( 'r' | 'R' ) ( 't' | 'T' ) ( 'i' | 'I' ) ( 'd' | 'D' ) | ( 'p' | 'P' ) ( 'r' | 'R' ) ( 'b' | 'B' ) ( 'n' | 'N' ) ( 'o' | 'O' ) | ( 'p' | 'P' ) ( 'e' | 'E' ) ( 'r' | 'R' ) ( 's' | 'S' ) ( 'i' | 'I' ) ( 's' | 'S' ) ( 't' | 'T' ) ( 'e' | 'E' ) ( 'n' | 'N' ) ( 't' | 'T' ) )
            int alt1=8;
            switch ( input.LA(1) ) {
            case 'E':
            case 'e':
                {
                int LA1_1 = input.LA(2);

                if ( (LA1_1=='V'||LA1_1=='v') ) {
                    int LA1_6 = input.LA(3);

                    if ( (LA1_6=='E'||LA1_6=='e') ) {
                        int LA1_9 = input.LA(4);

                        if ( (LA1_9=='N'||LA1_9=='n') ) {
                            int LA1_12 = input.LA(5);

                            if ( (LA1_12=='T'||LA1_12=='t') ) {
                                int LA1_13 = input.LA(6);

                                if ( (LA1_13=='R'||LA1_13=='r') ) {
                                    alt1=5;
                                }
                                else {
                                    alt1=1;
                                }
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 1, 12, input);

                                throw nvae;

                            }
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 1, 9, input);

                            throw nvae;

                        }
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 1, 6, input);

                        throw nvae;

                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 1, input);

                    throw nvae;

                }
                }
                break;
            case 'R':
            case 'r':
                {
                alt1=2;
                }
                break;
            case 'W':
            case 'w':
                {
                alt1=3;
                }
                break;
            case 'P':
            case 'p':
                {
                int LA1_4 = input.LA(2);

                if ( (LA1_4=='R'||LA1_4=='r') ) {
                    int LA1_7 = input.LA(3);

                    if ( (LA1_7=='O'||LA1_7=='o') ) {
                        alt1=4;
                    }
                    else if ( (LA1_7=='B'||LA1_7=='b') ) {
                        alt1=7;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 1, 7, input);

                        throw nvae;

                    }
                }
                else if ( (LA1_4=='E'||LA1_4=='e') ) {
                    alt1=8;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 4, input);

                    throw nvae;

                }
                }
                break;
            case 'A':
            case 'a':
                {
                alt1=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;

            }

            switch (alt1) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:154:7: ( 'e' | 'E' ) ( 'v' | 'V' ) ( 'e' | 'E' ) ( 'n' | 'N' ) ( 't' | 'T' )
                    {
                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 2 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:155:4: ( 'r' | 'R' ) ( 'e' | 'E' ) ( 'f' | 'F' ) ( 'e' | 'E' ) ( 'r' | 'R' ) ( 'e' | 'E' ) ( 'n' | 'N' ) ( 'c' | 'C' ) ( 'e' | 'E' )
                    {
                    if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 3 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:156:4: ( 'w' | 'W' ) ( 'i' | 'I' ) ( 'k' | 'K' ) ( 'i' | 'I' )
                    {
                    if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='K'||input.LA(1)=='k' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 4 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:157:4: ( 'p' | 'P' ) ( 'r' | 'R' ) ( 'o' | 'O' ) ( 'b' | 'B' ) ( 'l' | 'L' ) ( 'e' | 'E' ) ( 'm' | 'M' ) ( 'i' | 'I' ) ( 'd' | 'D' )
                    {
                    if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='M'||input.LA(1)=='m' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 5 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:158:4: ( 'e' | 'E' ) ( 'v' | 'V' ) ( 'e' | 'E' ) ( 'n' | 'N' ) ( 't' | 'T' ) ( 'r' | 'R' ) ( 'e' | 'E' ) ( 'f' | 'F' )
                    {
                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='V'||input.LA(1)=='v' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='F'||input.LA(1)=='f' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 6 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:159:4: ( 'a' | 'A' ) ( 'l' | 'L' ) ( 'e' | 'E' ) ( 'r' | 'R' ) ( 't' | 'T' ) ( 'i' | 'I' ) ( 'd' | 'D' )
                    {
                    if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='L'||input.LA(1)=='l' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 7 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:160:4: ( 'p' | 'P' ) ( 'r' | 'R' ) ( 'b' | 'B' ) ( 'n' | 'N' ) ( 'o' | 'O' )
                    {
                    if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='B'||input.LA(1)=='b' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 8 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:161:4: ( 'p' | 'P' ) ( 'e' | 'E' ) ( 'r' | 'R' ) ( 's' | 'S' ) ( 'i' | 'I' ) ( 's' | 'S' ) ( 't' | 'T' ) ( 'e' | 'E' ) ( 'n' | 'N' ) ( 't' | 'T' )
                    {
                    if ( input.LA(1)=='P'||input.LA(1)=='p' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "KEY"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:165:4: ( ( ' ' | '\\t' | '\\n' | '\\r' ) )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:
            {
            if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "WS"

    // $ANTLR start "VALUE"
    public final void mVALUE() throws RecognitionException {
        try {
            int _type = VALUE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:169:7: ( (~ ( '=' | ';' | WS ) )+ )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:169:9: (~ ( '=' | ';' | WS ) )+
            {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:169:9: (~ ( '=' | ';' | WS ) )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0 >= '\u0000' && LA2_0 <= '\b')||(LA2_0 >= '\u000B' && LA2_0 <= '\f')||(LA2_0 >= '\u000E' && LA2_0 <= '\u001F')||(LA2_0 >= '!' && LA2_0 <= ':')||LA2_0=='<'||(LA2_0 >= '>' && LA2_0 <= '\uFFFF')) ) {
                    alt2=1;
                }


                switch (alt2) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:
                    {
                    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\b')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\u001F')||(input.LA(1) >= '!' && input.LA(1) <= ':')||input.LA(1)=='<'||(input.LA(1) >= '>' && input.LA(1) <= '\uFFFF') ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

                default :
                    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "VALUE"

    public void mTokens() throws RecognitionException {
        // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:1:8: ( T__7 | T__8 | KEY | WS | VALUE )
        int alt3=5;
        int LA3_0 = input.LA(1);

        if ( (LA3_0==';') ) {
            alt3=1;
        }
        else if ( (LA3_0=='=') ) {
            alt3=2;
        }
        else if ( (LA3_0=='E'||LA3_0=='e') ) {
            int LA3_3 = input.LA(2);

            if ( (LA3_3=='V'||LA3_3=='v') ) {
                int LA3_10 = input.LA(3);

                if ( (LA3_10=='E'||LA3_10=='e') ) {
                    int LA3_16 = input.LA(4);

                    if ( (LA3_16=='N'||LA3_16=='n') ) {
                        int LA3_23 = input.LA(5);

                        if ( (LA3_23=='T'||LA3_23=='t') ) {
                            int LA3_30 = input.LA(6);

                            if ( (LA3_30=='R'||LA3_30=='r') ) {
                                int LA3_37 = input.LA(7);

                                if ( (LA3_37=='E'||LA3_37=='e') ) {
                                    int LA3_42 = input.LA(8);

                                    if ( (LA3_42=='F'||LA3_42=='f') ) {
                                        int LA3_47 = input.LA(9);

                                        if ( ((LA3_47 >= '\u0000' && LA3_47 <= '\b')||(LA3_47 >= '\u000B' && LA3_47 <= '\f')||(LA3_47 >= '\u000E' && LA3_47 <= '\u001F')||(LA3_47 >= '!' && LA3_47 <= ':')||LA3_47=='<'||(LA3_47 >= '>' && LA3_47 <= '\uFFFF')) ) {
                                            alt3=5;
                                        }
                                        else {
                                            alt3=3;
                                        }
                                    }
                                    else {
                                        alt3=5;
                                    }
                                }
                                else {
                                    alt3=5;
                                }
                            }
                            else if ( ((LA3_30 >= '\u0000' && LA3_30 <= '\b')||(LA3_30 >= '\u000B' && LA3_30 <= '\f')||(LA3_30 >= '\u000E' && LA3_30 <= '\u001F')||(LA3_30 >= '!' && LA3_30 <= ':')||LA3_30=='<'||(LA3_30 >= '>' && LA3_30 <= 'Q')||(LA3_30 >= 'S' && LA3_30 <= 'q')||(LA3_30 >= 's' && LA3_30 <= '\uFFFF')) ) {
                                alt3=5;
                            }
                            else {
                                alt3=3;
                            }
                        }
                        else {
                            alt3=5;
                        }
                    }
                    else {
                        alt3=5;
                    }
                }
                else {
                    alt3=5;
                }
            }
            else {
                alt3=5;
            }
        }
        else if ( (LA3_0=='R'||LA3_0=='r') ) {
            int LA3_4 = input.LA(2);

            if ( (LA3_4=='E'||LA3_4=='e') ) {
                int LA3_11 = input.LA(3);

                if ( (LA3_11=='F'||LA3_11=='f') ) {
                    int LA3_17 = input.LA(4);

                    if ( (LA3_17=='E'||LA3_17=='e') ) {
                        int LA3_24 = input.LA(5);

                        if ( (LA3_24=='R'||LA3_24=='r') ) {
                            int LA3_31 = input.LA(6);

                            if ( (LA3_31=='E'||LA3_31=='e') ) {
                                int LA3_38 = input.LA(7);

                                if ( (LA3_38=='N'||LA3_38=='n') ) {
                                    int LA3_43 = input.LA(8);

                                    if ( (LA3_43=='C'||LA3_43=='c') ) {
                                        int LA3_48 = input.LA(9);

                                        if ( (LA3_48=='E'||LA3_48=='e') ) {
                                            int LA3_51 = input.LA(10);

                                            if ( ((LA3_51 >= '\u0000' && LA3_51 <= '\b')||(LA3_51 >= '\u000B' && LA3_51 <= '\f')||(LA3_51 >= '\u000E' && LA3_51 <= '\u001F')||(LA3_51 >= '!' && LA3_51 <= ':')||LA3_51=='<'||(LA3_51 >= '>' && LA3_51 <= '\uFFFF')) ) {
                                                alt3=5;
                                            }
                                            else {
                                                alt3=3;
                                            }
                                        }
                                        else {
                                            alt3=5;
                                        }
                                    }
                                    else {
                                        alt3=5;
                                    }
                                }
                                else {
                                    alt3=5;
                                }
                            }
                            else {
                                alt3=5;
                            }
                        }
                        else {
                            alt3=5;
                        }
                    }
                    else {
                        alt3=5;
                    }
                }
                else {
                    alt3=5;
                }
            }
            else {
                alt3=5;
            }
        }
        else if ( (LA3_0=='W'||LA3_0=='w') ) {
            int LA3_5 = input.LA(2);

            if ( (LA3_5=='I'||LA3_5=='i') ) {
                int LA3_12 = input.LA(3);

                if ( (LA3_12=='K'||LA3_12=='k') ) {
                    int LA3_18 = input.LA(4);

                    if ( (LA3_18=='I'||LA3_18=='i') ) {
                        int LA3_25 = input.LA(5);

                        if ( ((LA3_25 >= '\u0000' && LA3_25 <= '\b')||(LA3_25 >= '\u000B' && LA3_25 <= '\f')||(LA3_25 >= '\u000E' && LA3_25 <= '\u001F')||(LA3_25 >= '!' && LA3_25 <= ':')||LA3_25=='<'||(LA3_25 >= '>' && LA3_25 <= '\uFFFF')) ) {
                            alt3=5;
                        }
                        else {
                            alt3=3;
                        }
                    }
                    else {
                        alt3=5;
                    }
                }
                else {
                    alt3=5;
                }
            }
            else {
                alt3=5;
            }
        }
        else if ( (LA3_0=='P'||LA3_0=='p') ) {
            switch ( input.LA(2) ) {
            case 'R':
            case 'r':
                {
                switch ( input.LA(3) ) {
                case 'O':
                case 'o':
                    {
                    int LA3_19 = input.LA(4);

                    if ( (LA3_19=='B'||LA3_19=='b') ) {
                        int LA3_26 = input.LA(5);

                        if ( (LA3_26=='L'||LA3_26=='l') ) {
                            int LA3_33 = input.LA(6);

                            if ( (LA3_33=='E'||LA3_33=='e') ) {
                                int LA3_39 = input.LA(7);

                                if ( (LA3_39=='M'||LA3_39=='m') ) {
                                    int LA3_44 = input.LA(8);

                                    if ( (LA3_44=='I'||LA3_44=='i') ) {
                                        int LA3_49 = input.LA(9);

                                        if ( (LA3_49=='D'||LA3_49=='d') ) {
                                            int LA3_52 = input.LA(10);

                                            if ( ((LA3_52 >= '\u0000' && LA3_52 <= '\b')||(LA3_52 >= '\u000B' && LA3_52 <= '\f')||(LA3_52 >= '\u000E' && LA3_52 <= '\u001F')||(LA3_52 >= '!' && LA3_52 <= ':')||LA3_52=='<'||(LA3_52 >= '>' && LA3_52 <= '\uFFFF')) ) {
                                                alt3=5;
                                            }
                                            else {
                                                alt3=3;
                                            }
                                        }
                                        else {
                                            alt3=5;
                                        }
                                    }
                                    else {
                                        alt3=5;
                                    }
                                }
                                else {
                                    alt3=5;
                                }
                            }
                            else {
                                alt3=5;
                            }
                        }
                        else {
                            alt3=5;
                        }
                    }
                    else {
                        alt3=5;
                    }
                    }
                    break;
                case 'B':
                case 'b':
                    {
                    int LA3_20 = input.LA(4);

                    if ( (LA3_20=='N'||LA3_20=='n') ) {
                        int LA3_27 = input.LA(5);

                        if ( (LA3_27=='O'||LA3_27=='o') ) {
                            int LA3_34 = input.LA(6);

                            if ( ((LA3_34 >= '\u0000' && LA3_34 <= '\b')||(LA3_34 >= '\u000B' && LA3_34 <= '\f')||(LA3_34 >= '\u000E' && LA3_34 <= '\u001F')||(LA3_34 >= '!' && LA3_34 <= ':')||LA3_34=='<'||(LA3_34 >= '>' && LA3_34 <= '\uFFFF')) ) {
                                alt3=5;
                            }
                            else {
                                alt3=3;
                            }
                        }
                        else {
                            alt3=5;
                        }
                    }
                    else {
                        alt3=5;
                    }
                    }
                    break;
                default:
                    alt3=5;
                }

                }
                break;
            case 'E':
            case 'e':
                {
                int LA3_14 = input.LA(3);

                if ( (LA3_14=='R'||LA3_14=='r') ) {
                    int LA3_21 = input.LA(4);

                    if ( (LA3_21=='S'||LA3_21=='s') ) {
                        int LA3_28 = input.LA(5);

                        if ( (LA3_28=='I'||LA3_28=='i') ) {
                            int LA3_35 = input.LA(6);

                            if ( (LA3_35=='S'||LA3_35=='s') ) {
                                int LA3_40 = input.LA(7);

                                if ( (LA3_40=='T'||LA3_40=='t') ) {
                                    int LA3_45 = input.LA(8);

                                    if ( (LA3_45=='E'||LA3_45=='e') ) {
                                        int LA3_50 = input.LA(9);

                                        if ( (LA3_50=='N'||LA3_50=='n') ) {
                                            int LA3_53 = input.LA(10);

                                            if ( (LA3_53=='T'||LA3_53=='t') ) {
                                                int LA3_54 = input.LA(11);

                                                if ( ((LA3_54 >= '\u0000' && LA3_54 <= '\b')||(LA3_54 >= '\u000B' && LA3_54 <= '\f')||(LA3_54 >= '\u000E' && LA3_54 <= '\u001F')||(LA3_54 >= '!' && LA3_54 <= ':')||LA3_54=='<'||(LA3_54 >= '>' && LA3_54 <= '\uFFFF')) ) {
                                                    alt3=5;
                                                }
                                                else {
                                                    alt3=3;
                                                }
                                            }
                                            else {
                                                alt3=5;
                                            }
                                        }
                                        else {
                                            alt3=5;
                                        }
                                    }
                                    else {
                                        alt3=5;
                                    }
                                }
                                else {
                                    alt3=5;
                                }
                            }
                            else {
                                alt3=5;
                            }
                        }
                        else {
                            alt3=5;
                        }
                    }
                    else {
                        alt3=5;
                    }
                }
                else {
                    alt3=5;
                }
                }
                break;
            default:
                alt3=5;
            }

        }
        else if ( (LA3_0=='A'||LA3_0=='a') ) {
            int LA3_7 = input.LA(2);

            if ( (LA3_7=='L'||LA3_7=='l') ) {
                int LA3_15 = input.LA(3);

                if ( (LA3_15=='E'||LA3_15=='e') ) {
                    int LA3_22 = input.LA(4);

                    if ( (LA3_22=='R'||LA3_22=='r') ) {
                        int LA3_29 = input.LA(5);

                        if ( (LA3_29=='T'||LA3_29=='t') ) {
                            int LA3_36 = input.LA(6);

                            if ( (LA3_36=='I'||LA3_36=='i') ) {
                                int LA3_41 = input.LA(7);

                                if ( (LA3_41=='D'||LA3_41=='d') ) {
                                    int LA3_46 = input.LA(8);

                                    if ( ((LA3_46 >= '\u0000' && LA3_46 <= '\b')||(LA3_46 >= '\u000B' && LA3_46 <= '\f')||(LA3_46 >= '\u000E' && LA3_46 <= '\u001F')||(LA3_46 >= '!' && LA3_46 <= ':')||LA3_46=='<'||(LA3_46 >= '>' && LA3_46 <= '\uFFFF')) ) {
                                        alt3=5;
                                    }
                                    else {
                                        alt3=3;
                                    }
                                }
                                else {
                                    alt3=5;
                                }
                            }
                            else {
                                alt3=5;
                            }
                        }
                        else {
                            alt3=5;
                        }
                    }
                    else {
                        alt3=5;
                    }
                }
                else {
                    alt3=5;
                }
            }
            else {
                alt3=5;
            }
        }
        else if ( ((LA3_0 >= '\t' && LA3_0 <= '\n')||LA3_0=='\r'||LA3_0==' ') ) {
            alt3=4;
        }
        else if ( ((LA3_0 >= '\u0000' && LA3_0 <= '\b')||(LA3_0 >= '\u000B' && LA3_0 <= '\f')||(LA3_0 >= '\u000E' && LA3_0 <= '\u001F')||(LA3_0 >= '!' && LA3_0 <= ':')||LA3_0=='<'||(LA3_0 >= '>' && LA3_0 <= '@')||(LA3_0 >= 'B' && LA3_0 <= 'D')||(LA3_0 >= 'F' && LA3_0 <= 'O')||LA3_0=='Q'||(LA3_0 >= 'S' && LA3_0 <= 'V')||(LA3_0 >= 'X' && LA3_0 <= '`')||(LA3_0 >= 'b' && LA3_0 <= 'd')||(LA3_0 >= 'f' && LA3_0 <= 'o')||LA3_0=='q'||(LA3_0 >= 's' && LA3_0 <= 'v')||(LA3_0 >= 'x' && LA3_0 <= '\uFFFF')) ) {
            alt3=5;
        }
        else {
            NoViableAltException nvae =
                new NoViableAltException("", 3, 0, input);

            throw nvae;

        }
        switch (alt3) {
            case 1 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:1:10: T__7
                {
                mT__7(); 


                }
                break;
            case 2 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:1:15: T__8
                {
                mT__8(); 


                }
                break;
            case 3 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:1:20: KEY
                {
                mKEY(); 


                }
                break;
            case 4 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:1:24: WS
                {
                mWS(); 


                }
                break;
            case 5 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:1:27: VALUE
                {
                mVALUE(); 


                }
                break;

        }

    }


 

}