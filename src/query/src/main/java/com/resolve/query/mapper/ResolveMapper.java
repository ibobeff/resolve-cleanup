/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.query.mapper;

import java.util.HashMap;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

import com.resolve.util.Log;

public class ResolveMapper
{
    /**
     * Constructor initializes a new ResolveMapper object. Does nothing else.
     */
    public ResolveMapper() { /* Empty */ } //ResolveMapper
    
    /**
     * Returns a HashMap<String, String> of mapped key/value pairs. Throws a regular
     * Exception upon error (translated from RecognitionException). Only syntax rule
     * involves separating keys from their values with '='.
     * @param query To be translated.
     */
    public HashMap<String, String> mapQuery(String query) throws Exception
    {
        MapperLexer lex = new MapperLexer(new ANTLRStringStream(query));
        CommonTokenStream tokens = new CommonTokenStream(lex);
        MapperParser g = new MapperParser(tokens, null);
        
        try
        {
            HashMap<String, String> map = g.prog();
            return map;
        }
        catch (RecognitionException ex)         //Construct message and throw more general exception.
        {
            StringBuilder message = new StringBuilder("Parser error around ");
            message.append("line " + ex.line + ", index " + ex.charPositionInLine);
            if(ex.token != null)
            {
                message.append(", token '" + ex.token.getText() + "'");
            }
            message.append(".");
            throw new Exception(message.toString());
        }
    } //mapQuery
    
    /**
     * Returns a String array of all supported keys. Keys are case insensitive.
     */
    public static String[] getSupportedKeys()
    {
        return MapperParser.getKeysString();
    } //getSupportedKeys
    
    
    /**
     * Used for testing purposes. Outputs key/value pairs to the console.
     */
//    public static void main(String args[]) throws Exception
//    {
//        Log.init();
//        
//        String query = "Hi everbody EVENT= My Event PROCESSID =process id REFERENCE = Event reference " +
//        		"USER=bob WIKI=ns.wiki name PRBNO=PRB1234 START=true; thank you";
//        System.out.println(query);
//        
//        ResolveMapper resolveMapper = new ResolveMapper();
//        HashMap<String, String> map = resolveMapper.mapQuery(query);
//        
//        for(String key : map.keySet())
//        {
//            System.out.println(key + "::" + map.get(key));
//        }
//    } //main
    
} //ResolveMapper