/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.query;
    
    /* Refer to QueryParser for notes */


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class QueryLexer extends Lexer {
    public static final int EOF=-1;
    public static final int AND=4;
    public static final int CHAR=5;
    public static final int CLOSE_PARENS=6;
    public static final int CONTAINS=7;
    public static final int DIGIT=8;
    public static final int ENDS_WITH=9;
    public static final int EQUAL=10;
    public static final int ESC_SEQ=11;
    public static final int FLOAT=12;
    public static final int GREATER=13;
    public static final int GREATER_OR_EQUAL=14;
    public static final int INT=15;
    public static final int LESS=16;
    public static final int LESS_OR_EQUAL=17;
    public static final int NAME=18;
    public static final int NOT_EQUAL=19;
    public static final int OID=20;
    public static final int OPEN_PARENS=21;
    public static final int OR=22;
    public static final int STARTS_WITH=23;
    public static final int STRING=24;
    public static final int WS=25;

        /* Used to tell lexer to immediately stop upon error. Throws a RecognitionError that goes to the parser. */
        
        @Override
        public void reportError(RecognitionException e)
        {
            Thrower.sneakyThrow(e);
        }
        
        static class Thrower
        {
            private static Throwable t;
            
            private Thrower() throws Throwable
            {
                throw t;
            }
            
            public static synchronized void sneakyThrow(Throwable t)
            {
                Thrower.t = t;
                try
                {
                    Thrower.class.newInstance();
                }
                catch (InstantiationException e)
                {
                    throw new IllegalArgumentException(e);
                }
                catch (IllegalAccessException e)
                {
                    throw new IllegalArgumentException(e);
                }
                finally
                {
                    Thrower.t = null;
                }
            }
        }


    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public QueryLexer() {} 
    public QueryLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public QueryLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g"; }

    // $ANTLR start "AND"
    public final void mAND() throws RecognitionException {
        try {
            int _type = AND;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:390:5: ( ( 'a' | 'A' ) ( 'n' | 'N' ) ( 'd' | 'D' ) | '&' | '&&' | '+' )
            int alt1=4;
            switch ( input.LA(1) ) {
            case 'A':
            case 'a':
                {
                alt1=1;
                }
                break;
            case '&':
                {
                int LA1_2 = input.LA(2);

                if ( (LA1_2=='&') ) {
                    alt1=3;
                }
                else {
                    alt1=2;
                }
                }
                break;
            case '+':
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;

            }

            switch (alt1) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:390:7: ( 'a' | 'A' ) ( 'n' | 'N' ) ( 'd' | 'D' )
                    {
                    if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 2 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:391:4: '&'
                    {
                    match('&'); 

                    }
                    break;
                case 3 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:392:4: '&&'
                    {
                    match("&&"); 



                    }
                    break;
                case 4 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:393:4: '+'
                    {
                    match('+'); 

                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "AND"

    // $ANTLR start "OR"
    public final void mOR() throws RecognitionException {
        try {
            int _type = OR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:397:4: ( ( 'o' | 'O' ) ( 'r' | 'R' ) | '|' | '||' )
            int alt2=3;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='O'||LA2_0=='o') ) {
                alt2=1;
            }
            else if ( (LA2_0=='|') ) {
                int LA2_2 = input.LA(2);

                if ( (LA2_2=='|') ) {
                    alt2=3;
                }
                else {
                    alt2=2;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }
            switch (alt2) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:397:6: ( 'o' | 'O' ) ( 'r' | 'R' )
                    {
                    if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 2 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:398:4: '|'
                    {
                    match('|'); 

                    }
                    break;
                case 3 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:399:4: '||'
                    {
                    match("||"); 



                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "OR"

    // $ANTLR start "EQUAL"
    public final void mEQUAL() throws RecognitionException {
        try {
            int _type = EQUAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:403:8: ( '=' | '==' )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0=='=') ) {
                int LA3_1 = input.LA(2);

                if ( (LA3_1=='=') ) {
                    alt3=2;
                }
                else {
                    alt3=1;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;

            }
            switch (alt3) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:403:10: '='
                    {
                    match('='); 

                    }
                    break;
                case 2 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:404:4: '=='
                    {
                    match("=="); 



                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "EQUAL"

    // $ANTLR start "NOT_EQUAL"
    public final void mNOT_EQUAL() throws RecognitionException {
        try {
            int _type = NOT_EQUAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:409:2: ( '!=' | '<>' )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='!') ) {
                alt4=1;
            }
            else if ( (LA4_0=='<') ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;

            }
            switch (alt4) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:409:4: '!='
                    {
                    match("!="); 



                    }
                    break;
                case 2 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:410:4: '<>'
                    {
                    match("<>"); 



                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "NOT_EQUAL"

    // $ANTLR start "GREATER"
    public final void mGREATER() throws RecognitionException {
        try {
            int _type = GREATER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:414:9: ( '>' )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:414:11: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "GREATER"

    // $ANTLR start "GREATER_OR_EQUAL"
    public final void mGREATER_OR_EQUAL() throws RecognitionException {
        try {
            int _type = GREATER_OR_EQUAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:419:2: ( '>=' )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:419:4: '>='
            {
            match(">="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "GREATER_OR_EQUAL"

    // $ANTLR start "LESS"
    public final void mLESS() throws RecognitionException {
        try {
            int _type = LESS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:423:6: ( '<' )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:423:8: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "LESS"

    // $ANTLR start "LESS_OR_EQUAL"
    public final void mLESS_OR_EQUAL() throws RecognitionException {
        try {
            int _type = LESS_OR_EQUAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:428:2: ( '<=' )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:428:4: '<='
            {
            match("<="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "LESS_OR_EQUAL"

    // $ANTLR start "STARTS_WITH"
    public final void mSTARTS_WITH() throws RecognitionException {
        try {
            int _type = STARTS_WITH;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:433:2: ( ( 's' | 'S' ) ( 't' | 'T' ) ( 'a' | 'A' ) ( 'r' | 'R' ) ( 't' | 'T' ) ( 's' | 'S' ) ( 'w' | 'W' ) ( 'i' | 'I' ) ( 't' | 'T' ) ( 'h' | 'H' ) )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:433:4: ( 's' | 'S' ) ( 't' | 'T' ) ( 'a' | 'A' ) ( 'r' | 'R' ) ( 't' | 'T' ) ( 's' | 'S' ) ( 'w' | 'W' ) ( 'i' | 'I' ) ( 't' | 'T' ) ( 'h' | 'H' )
            {
            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='R'||input.LA(1)=='r' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "STARTS_WITH"

    // $ANTLR start "ENDS_WITH"
    public final void mENDS_WITH() throws RecognitionException {
        try {
            int _type = ENDS_WITH;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:438:2: ( ( 'e' | 'E' ) ( 'n' | 'N' ) ( 'd' | 'D' ) ( 's' | 'S' ) ( 'w' | 'W' ) ( 'i' | 'I' ) ( 't' | 'T' ) ( 'h' | 'H' ) )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:438:4: ( 'e' | 'E' ) ( 'n' | 'N' ) ( 'd' | 'D' ) ( 's' | 'S' ) ( 'w' | 'W' ) ( 'i' | 'I' ) ( 't' | 'T' ) ( 'h' | 'H' )
            {
            if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='D'||input.LA(1)=='d' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='W'||input.LA(1)=='w' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='H'||input.LA(1)=='h' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "ENDS_WITH"

    // $ANTLR start "CONTAINS"
    public final void mCONTAINS() throws RecognitionException {
        try {
            int _type = CONTAINS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:442:9: ( ( 'c' | 'C' ) ( 'o' | 'O' ) ( 'n' | 'N' ) ( 't' | 'T' ) ( 'a' | 'A' ) ( 'i' | 'I' ) ( 'n' | 'N' ) ( 's' | 'S' ) )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:442:11: ( 'c' | 'C' ) ( 'o' | 'O' ) ( 'n' | 'N' ) ( 't' | 'T' ) ( 'a' | 'A' ) ( 'i' | 'I' ) ( 'n' | 'N' ) ( 's' | 'S' )
            {
            if ( input.LA(1)=='C'||input.LA(1)=='c' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='O'||input.LA(1)=='o' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='T'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='A'||input.LA(1)=='a' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='I'||input.LA(1)=='i' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='N'||input.LA(1)=='n' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            if ( input.LA(1)=='S'||input.LA(1)=='s' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "CONTAINS"

    // $ANTLR start "STRING"
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:446:8: ( '\\'' ( ESC_SEQ |~ ( '\\\\' | '\\'' ) )* '\\'' )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:446:10: '\\'' ( ESC_SEQ |~ ( '\\\\' | '\\'' ) )* '\\''
            {
            match('\''); 

            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:446:15: ( ESC_SEQ |~ ( '\\\\' | '\\'' ) )*
            loop5:
            do {
                int alt5=3;
                int LA5_0 = input.LA(1);

                if ( (LA5_0=='\\') ) {
                    alt5=1;
                }
                else if ( ((LA5_0 >= '\u0000' && LA5_0 <= '&')||(LA5_0 >= '(' && LA5_0 <= '[')||(LA5_0 >= ']' && LA5_0 <= '\uFFFF')) ) {
                    alt5=2;
                }


                switch (alt5) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:446:17: ESC_SEQ
                    {
                    mESC_SEQ(); 


                    }
                    break;
                case 2 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:446:27: ~ ( '\\\\' | '\\'' )
                    {
                    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '&')||(input.LA(1) >= '(' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

                default :
                    break loop5;
                }
            } while (true);


            match('\''); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "STRING"

    // $ANTLR start "OPEN_PARENS"
    public final void mOPEN_PARENS() throws RecognitionException {
        try {
            int _type = OPEN_PARENS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:451:2: ( ( '(' )* )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:451:4: ( '(' )*
            {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:451:4: ( '(' )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0=='(') ) {
                    alt6=1;
                }


                switch (alt6) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:451:4: '('
                    {
                    match('('); 

                    }
                    break;

                default :
                    break loop6;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "OPEN_PARENS"

    // $ANTLR start "CLOSE_PARENS"
    public final void mCLOSE_PARENS() throws RecognitionException {
        try {
            int _type = CLOSE_PARENS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:456:2: ( ( ')' )* )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:456:4: ( ')' )*
            {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:456:4: ( ')' )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==')') ) {
                    alt7=1;
                }


                switch (alt7) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:456:4: ')'
                    {
                    match(')'); 

                    }
                    break;

                default :
                    break loop7;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "CLOSE_PARENS"

    // $ANTLR start "FLOAT"
    public final void mFLOAT() throws RecognitionException {
        try {
            int _type = FLOAT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:460:7: ( ( '-' )? ( DIGIT )* '.' ( DIGIT )+ | ( '-' )? ( DIGIT )+ '.' ( DIGIT )* )
            int alt14=2;
            alt14 = dfa14.predict(input);
            switch (alt14) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:460:9: ( '-' )? ( DIGIT )* '.' ( DIGIT )+
                    {
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:460:9: ( '-' )?
                    int alt8=2;
                    int LA8_0 = input.LA(1);

                    if ( (LA8_0=='-') ) {
                        alt8=1;
                    }
                    switch (alt8) {
                        case 1 :
                            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:460:9: '-'
                            {
                            match('-'); 

                            }
                            break;

                    }


                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:460:14: ( DIGIT )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( ((LA9_0 >= '0' && LA9_0 <= '9')) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                        case 1 :
                            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:
                            {
                            if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                                input.consume();
                            }
                            else {
                                MismatchedSetException mse = new MismatchedSetException(null,input);
                                recover(mse);
                                throw mse;
                            }


                            }
                            break;

                        default :
                            break loop9;
                        }
                    } while (true);


                    match('.'); 

                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:460:25: ( DIGIT )+
                    int cnt10=0;
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( ((LA10_0 >= '0' && LA10_0 <= '9')) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                        case 1 :
                            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:
                            {
                            if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                                input.consume();
                            }
                            else {
                                MismatchedSetException mse = new MismatchedSetException(null,input);
                                recover(mse);
                                throw mse;
                            }


                            }
                            break;

                        default :
                            if ( cnt10 >= 1 ) break loop10;
                                EarlyExitException eee =
                                    new EarlyExitException(10, input);
                                throw eee;
                        }
                        cnt10++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:461:4: ( '-' )? ( DIGIT )+ '.' ( DIGIT )*
                    {
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:461:4: ( '-' )?
                    int alt11=2;
                    int LA11_0 = input.LA(1);

                    if ( (LA11_0=='-') ) {
                        alt11=1;
                    }
                    switch (alt11) {
                        case 1 :
                            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:461:4: '-'
                            {
                            match('-'); 

                            }
                            break;

                    }


                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:461:9: ( DIGIT )+
                    int cnt12=0;
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( ((LA12_0 >= '0' && LA12_0 <= '9')) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                        case 1 :
                            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:
                            {
                            if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                                input.consume();
                            }
                            else {
                                MismatchedSetException mse = new MismatchedSetException(null,input);
                                recover(mse);
                                throw mse;
                            }


                            }
                            break;

                        default :
                            if ( cnt12 >= 1 ) break loop12;
                                EarlyExitException eee =
                                    new EarlyExitException(12, input);
                                throw eee;
                        }
                        cnt12++;
                    } while (true);


                    match('.'); 

                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:461:20: ( DIGIT )*
                    loop13:
                    do {
                        int alt13=2;
                        int LA13_0 = input.LA(1);

                        if ( ((LA13_0 >= '0' && LA13_0 <= '9')) ) {
                            alt13=1;
                        }


                        switch (alt13) {
                        case 1 :
                            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:
                            {
                            if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                                input.consume();
                            }
                            else {
                                MismatchedSetException mse = new MismatchedSetException(null,input);
                                recover(mse);
                                throw mse;
                            }


                            }
                            break;

                        default :
                            break loop13;
                        }
                    } while (true);


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "FLOAT"

    // $ANTLR start "INT"
    public final void mINT() throws RecognitionException {
        try {
            int _type = INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:465:5: ( ( '-' )? ( DIGIT )+ )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:465:7: ( '-' )? ( DIGIT )+
            {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:465:7: ( '-' )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0=='-') ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:465:7: '-'
                    {
                    match('-'); 

                    }
                    break;

            }


            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:465:12: ( DIGIT )+
            int cnt16=0;
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( ((LA16_0 >= '0' && LA16_0 <= '9')) ) {
                    alt16=1;
                }


                switch (alt16) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:
                    {
                    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

                default :
                    if ( cnt16 >= 1 ) break loop16;
                        EarlyExitException eee =
                            new EarlyExitException(16, input);
                        throw eee;
                }
                cnt16++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "INT"

    // $ANTLR start "NAME"
    public final void mNAME() throws RecognitionException {
        try {
            int _type = NAME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:469:6: ( ( CHAR | '_' ) ( CHAR | DIGIT | '_' )* )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:469:8: ( CHAR | '_' ) ( CHAR | DIGIT | '_' )*
            {
            if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:469:19: ( CHAR | DIGIT | '_' )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( ((LA17_0 >= '0' && LA17_0 <= '9')||(LA17_0 >= 'A' && LA17_0 <= 'Z')||LA17_0=='_'||(LA17_0 >= 'a' && LA17_0 <= 'z')) ) {
                    alt17=1;
                }


                switch (alt17) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:
                    {
                    if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

                default :
                    break loop17;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "NAME"

    // $ANTLR start "OID"
    public final void mOID() throws RecognitionException {
        try {
            int _type = OID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:473:5: ( ( DIGIT )+ ( '.' ( DIGIT )+ )* )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:473:7: ( DIGIT )+ ( '.' ( DIGIT )+ )*
            {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:473:7: ( DIGIT )+
            int cnt18=0;
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( ((LA18_0 >= '0' && LA18_0 <= '9')) ) {
                    alt18=1;
                }


                switch (alt18) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:
                    {
                    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

                default :
                    if ( cnt18 >= 1 ) break loop18;
                        EarlyExitException eee =
                            new EarlyExitException(18, input);
                        throw eee;
                }
                cnt18++;
            } while (true);


            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:473:14: ( '.' ( DIGIT )+ )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0=='.') ) {
                    alt20=1;
                }


                switch (alt20) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:473:15: '.' ( DIGIT )+
                    {
                    match('.'); 

                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:473:19: ( DIGIT )+
                    int cnt19=0;
                    loop19:
                    do {
                        int alt19=2;
                        int LA19_0 = input.LA(1);

                        if ( ((LA19_0 >= '0' && LA19_0 <= '9')) ) {
                            alt19=1;
                        }


                        switch (alt19) {
                        case 1 :
                            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:
                            {
                            if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                                input.consume();
                            }
                            else {
                                MismatchedSetException mse = new MismatchedSetException(null,input);
                                recover(mse);
                                throw mse;
                            }


                            }
                            break;

                        default :
                            if ( cnt19 >= 1 ) break loop19;
                                EarlyExitException eee =
                                    new EarlyExitException(19, input);
                                throw eee;
                        }
                        cnt19++;
                    } while (true);


                    }
                    break;

                default :
                    break loop20;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "OID"

    // $ANTLR start "CHAR"
    public final void mCHAR() throws RecognitionException {
        try {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:483:6: ( ( 'a' .. 'z' | 'A' .. 'Z' ) )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:
            {
            if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }


        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "CHAR"

    // $ANTLR start "DIGIT"
    public final void mDIGIT() throws RecognitionException {
        try {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:485:7: ( ( '0' .. '9' ) )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:
            {
            if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }


        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "DIGIT"

    // $ANTLR start "ESC_SEQ"
    public final void mESC_SEQ() throws RecognitionException {
        try {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:490:9: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' ) )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:490:11: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' )
            {
            match('\\'); 

            if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||input.LA(1)=='t' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }


        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "ESC_SEQ"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:493:4: ( ( ' ' | '\\t' | '\\n' | '\\r' ) )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:493:6: ( ' ' | '\\t' | '\\n' | '\\r' )
            {
            if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


             _channel = HIDDEN; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
            // do for sure before leaving
        }
    }
    // $ANTLR end "WS"

    public void mTokens() throws RecognitionException {
        // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:8: ( AND | OR | EQUAL | NOT_EQUAL | GREATER | GREATER_OR_EQUAL | LESS | LESS_OR_EQUAL | STARTS_WITH | ENDS_WITH | CONTAINS | STRING | OPEN_PARENS | CLOSE_PARENS | FLOAT | INT | NAME | OID | WS )
        int alt21=19;
        alt21 = dfa21.predict(input);
        switch (alt21) {
            case 1 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:10: AND
                {
                mAND(); 


                }
                break;
            case 2 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:14: OR
                {
                mOR(); 


                }
                break;
            case 3 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:17: EQUAL
                {
                mEQUAL(); 


                }
                break;
            case 4 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:23: NOT_EQUAL
                {
                mNOT_EQUAL(); 


                }
                break;
            case 5 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:33: GREATER
                {
                mGREATER(); 


                }
                break;
            case 6 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:41: GREATER_OR_EQUAL
                {
                mGREATER_OR_EQUAL(); 


                }
                break;
            case 7 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:58: LESS
                {
                mLESS(); 


                }
                break;
            case 8 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:63: LESS_OR_EQUAL
                {
                mLESS_OR_EQUAL(); 


                }
                break;
            case 9 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:77: STARTS_WITH
                {
                mSTARTS_WITH(); 


                }
                break;
            case 10 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:89: ENDS_WITH
                {
                mENDS_WITH(); 


                }
                break;
            case 11 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:99: CONTAINS
                {
                mCONTAINS(); 


                }
                break;
            case 12 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:108: STRING
                {
                mSTRING(); 


                }
                break;
            case 13 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:115: OPEN_PARENS
                {
                mOPEN_PARENS(); 


                }
                break;
            case 14 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:127: CLOSE_PARENS
                {
                mCLOSE_PARENS(); 


                }
                break;
            case 15 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:140: FLOAT
                {
                mFLOAT(); 


                }
                break;
            case 16 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:146: INT
                {
                mINT(); 


                }
                break;
            case 17 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:150: NAME
                {
                mNAME(); 


                }
                break;
            case 18 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:155: OID
                {
                mOID(); 


                }
                break;
            case 19 :
                // C:\\Users\\christopher.wong\\Desktop\\antlr\\Query.g:1:159: WS
                {
                mWS(); 


                }
                break;

        }

    }


    protected DFA14 dfa14 = new DFA14(this);
    protected DFA21 dfa21 = new DFA21(this);
    static final String DFA14_eotS =
        "\4\uffff\1\6\2\uffff";
    static final String DFA14_eofS =
        "\7\uffff";
    static final String DFA14_minS =
        "\1\55\2\56\1\uffff\1\60\2\uffff";
    static final String DFA14_maxS =
        "\3\71\1\uffff\1\71\2\uffff";
    static final String DFA14_acceptS =
        "\3\uffff\1\1\1\uffff\1\1\1\2";
    static final String DFA14_specialS =
        "\7\uffff}>";
    static final String[] DFA14_transitionS = {
            "\1\1\1\3\1\uffff\12\2",
            "\1\3\1\uffff\12\2",
            "\1\4\1\uffff\12\2",
            "",
            "\12\5",
            "",
            ""
    };

    static final short[] DFA14_eot = DFA.unpackEncodedString(DFA14_eotS);
    static final short[] DFA14_eof = DFA.unpackEncodedString(DFA14_eofS);
    static final char[] DFA14_min = DFA.unpackEncodedStringToUnsignedChars(DFA14_minS);
    static final char[] DFA14_max = DFA.unpackEncodedStringToUnsignedChars(DFA14_maxS);
    static final short[] DFA14_accept = DFA.unpackEncodedString(DFA14_acceptS);
    static final short[] DFA14_special = DFA.unpackEncodedString(DFA14_specialS);
    static final short[][] DFA14_transition;

    static {
        int numStates = DFA14_transitionS.length;
        DFA14_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA14_transition[i] = DFA.unpackEncodedString(DFA14_transitionS[i]);
        }
    }

    class DFA14 extends DFA {

        public DFA14(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 14;
            this.eot = DFA14_eot;
            this.eof = DFA14_eof;
            this.min = DFA14_min;
            this.max = DFA14_max;
            this.accept = DFA14_accept;
            this.special = DFA14_special;
            this.transition = DFA14_transition;
        }
        public String getDescription() {
            return "460:1: FLOAT : ( ( '-' )? ( DIGIT )* '.' ( DIGIT )+ | ( '-' )? ( DIGIT )+ '.' ( DIGIT )* );";
        }
    }
    static final String DFA21_eotS =
        "\1\15\1\22\1\uffff\1\22\3\uffff\1\27\1\31\3\22\4\uffff\1\37\3\uffff"+
        "\1\22\1\4\4\uffff\3\22\1\37\1\21\1\uffff\1\2\3\22\1\21\3\22\1\uffff"+
        "\12\22\1\66\1\67\1\22\2\uffff\1\71\1\uffff";
    static final String DFA21_eofS =
        "\72\uffff";
    static final String DFA21_minS =
        "\1\11\1\116\1\uffff\1\122\3\uffff\2\75\1\124\1\116\1\117\3\uffff"+
        "\2\56\3\uffff\1\104\1\60\4\uffff\1\101\1\104\1\116\1\56\1\60\1\uffff"+
        "\1\60\1\122\1\123\1\124\1\56\1\124\1\127\1\101\1\uffff\1\123\2\111"+
        "\1\127\1\124\1\116\1\111\1\110\1\123\1\124\2\60\1\110\2\uffff\1"+
        "\60\1\uffff";
    static final String DFA21_maxS =
        "\1\174\1\156\1\uffff\1\162\3\uffff\1\76\1\75\1\164\1\156\1\157\3"+
        "\uffff\2\71\3\uffff\1\144\1\172\4\uffff\1\141\1\144\1\156\2\71\1"+
        "\uffff\1\172\1\162\1\163\1\164\1\71\1\164\1\167\1\141\1\uffff\1"+
        "\163\2\151\1\167\1\164\1\156\1\151\1\150\1\163\1\164\2\172\1\150"+
        "\2\uffff\1\172\1\uffff";
    static final String DFA21_acceptS =
        "\2\uffff\1\1\1\uffff\1\2\1\3\1\4\5\uffff\1\14\1\15\1\16\2\uffff"+
        "\1\17\1\21\1\23\2\uffff\1\10\1\7\1\6\1\5\5\uffff\1\20\10\uffff\1"+
        "\22\15\uffff\1\12\1\13\1\uffff\1\11";
    static final String DFA21_specialS =
        "\72\uffff}>";
    static final String[] DFA21_transitionS = {
            "\2\23\2\uffff\1\23\22\uffff\1\23\1\6\4\uffff\1\2\1\14\1\uffff"+
            "\1\16\1\uffff\1\2\1\uffff\1\17\1\21\1\uffff\12\20\2\uffff\1"+
            "\7\1\5\1\10\2\uffff\1\1\1\22\1\13\1\22\1\12\11\22\1\3\3\22\1"+
            "\11\7\22\4\uffff\1\22\1\uffff\1\1\1\22\1\13\1\22\1\12\11\22"+
            "\1\3\3\22\1\11\7\22\1\uffff\1\4",
            "\1\24\37\uffff\1\24",
            "",
            "\1\25\37\uffff\1\25",
            "",
            "",
            "",
            "\1\26\1\6",
            "\1\30",
            "\1\32\37\uffff\1\32",
            "\1\33\37\uffff\1\33",
            "\1\34\37\uffff\1\34",
            "",
            "",
            "",
            "\1\21\1\uffff\12\35",
            "\1\36\1\uffff\12\20",
            "",
            "",
            "",
            "\1\40\37\uffff\1\40",
            "\12\22\7\uffff\32\22\4\uffff\1\22\1\uffff\32\22",
            "",
            "",
            "",
            "",
            "\1\41\37\uffff\1\41",
            "\1\42\37\uffff\1\42",
            "\1\43\37\uffff\1\43",
            "\1\21\1\uffff\12\35",
            "\12\44",
            "",
            "\12\22\7\uffff\32\22\4\uffff\1\22\1\uffff\32\22",
            "\1\45\37\uffff\1\45",
            "\1\46\37\uffff\1\46",
            "\1\47\37\uffff\1\47",
            "\1\50\1\uffff\12\44",
            "\1\51\37\uffff\1\51",
            "\1\52\37\uffff\1\52",
            "\1\53\37\uffff\1\53",
            "",
            "\1\54\37\uffff\1\54",
            "\1\55\37\uffff\1\55",
            "\1\56\37\uffff\1\56",
            "\1\57\37\uffff\1\57",
            "\1\60\37\uffff\1\60",
            "\1\61\37\uffff\1\61",
            "\1\62\37\uffff\1\62",
            "\1\63\37\uffff\1\63",
            "\1\64\37\uffff\1\64",
            "\1\65\37\uffff\1\65",
            "\12\22\7\uffff\32\22\4\uffff\1\22\1\uffff\32\22",
            "\12\22\7\uffff\32\22\4\uffff\1\22\1\uffff\32\22",
            "\1\70\37\uffff\1\70",
            "",
            "",
            "\12\22\7\uffff\32\22\4\uffff\1\22\1\uffff\32\22",
            ""
    };

    static final short[] DFA21_eot = DFA.unpackEncodedString(DFA21_eotS);
    static final short[] DFA21_eof = DFA.unpackEncodedString(DFA21_eofS);
    static final char[] DFA21_min = DFA.unpackEncodedStringToUnsignedChars(DFA21_minS);
    static final char[] DFA21_max = DFA.unpackEncodedStringToUnsignedChars(DFA21_maxS);
    static final short[] DFA21_accept = DFA.unpackEncodedString(DFA21_acceptS);
    static final short[] DFA21_special = DFA.unpackEncodedString(DFA21_specialS);
    static final short[][] DFA21_transition;

    static {
        int numStates = DFA21_transitionS.length;
        DFA21_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA21_transition[i] = DFA.unpackEncodedString(DFA21_transitionS[i]);
        }
    }

    class DFA21 extends DFA {

        public DFA21(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 21;
            this.eot = DFA21_eot;
            this.eof = DFA21_eof;
            this.min = DFA21_min;
            this.max = DFA21_max;
            this.accept = DFA21_accept;
            this.special = DFA21_special;
            this.transition = DFA21_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( AND | OR | EQUAL | NOT_EQUAL | GREATER | GREATER_OR_EQUAL | LESS | LESS_OR_EQUAL | STARTS_WITH | ENDS_WITH | CONTAINS | STRING | OPEN_PARENS | CLOSE_PARENS | FLOAT | INT | NAME | OID | WS );";
        }
    }
 

}