/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.query;

/**
 * The QueryUtils class offers static methods that may be helpful for the implementation
 * of classes using the QueryTranslator interface.
 */
public class QueryUtils
{
    /**
     * Returns whether the value is a properly formatted String, meaning that it must be
     * surrounded by single quotations. Example: 'aString'
     * @param value The value to check.
     */
    public static boolean isString(String value)
    {
        return value.charAt(0) == '\'' && value.charAt(value.length() - 1) == '\'';
    } //isString
    
    /**
     * Used to ensure that a value is a properly formatted String before continuing translation. Throws
     * a RuntimeException with an appropriate error message if it is not.
     * Example error message: "SQL Error: CONTAINS must have String value"
     * @param value The value to validate.
     * @param op The String representation of the current operator to translate. Example: "CONTAINS"
     * @param outputFormat The String representation of the current output format. Example: "SQL"
     */
    public static void validateString(String value, String op, String outputFormat)
    {
        if(!isString(value))
        {
            throw new RuntimeException(outputFormat + " Error: " + op + " must have String value.");
        }
    } //validateString
    
    /**
     * Reformats a proper String by removing the outside quotes and internal escape sequences. Use
     * the method isString() to ensure the value is a String before calling this method.
     * @param value The String to reformat.
     */
    public static String trimAndRemoveEscape(String value)
    {
        value = trimString(value);
        value = removeEscape(value);
        return value;
    } //trimAndRemoveEscape

    /**
     * Reformats a proper String by removing the outside quotes. Use the method isString() to ensure
     * the value is a String before calling this method.
     * @param value The String to reformat.
     */
    public static String trimString(String value)
    {
        return value.substring(1, value.length() - 1);
    } //trimString

    /**
     * Reformats a proper String by removing internal escape sequences. Use the method isString() to
     * ensure the value is a String before calling this method.
     * @param value The String to reformat.
     */
    public static String removeEscape(String value)
    {
        for (int i = 0; i < value.length() - 1; i++)
        {
            if (value.charAt(i) == '\\')
            {
                value = value.substring(0, i) + value.substring(i + 1);
            }
        }
        return value;
    } //removeEscape
    
} //QueryUtils
