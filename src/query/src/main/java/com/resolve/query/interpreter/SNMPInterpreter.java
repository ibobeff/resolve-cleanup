/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.query.interpreter;

import java.util.ArrayList;
import java.util.HashMap;

import com.resolve.query.QueryParser.Assignment;
import com.resolve.query.QueryParser.BoolOperator;
import com.resolve.query.QueryUtils;
import com.resolve.query.ResolveQuery;

/**
 * The SNMPInterpreter class extends the functionality of the ResolveQuery translator.
 * In addition to parsing the query, this class allows the user to validate the statement
 * (whether it is true or false) based on a stored Map of SNMP OIDs. The OID values in
 * the Map are substituted in the query, and then the boolean logical expression is evaluated.
 * Use SNMPInterpreter.validateQuery(query, trapIds).
 */
public class SNMPInterpreter
{
    /** Used to reset the precedence to the lowest level */
    private static final int LOWEST_PRECEDENCE = 0;
    /** Precedence for AND BoolOperator */
    private static final int AND_PRECEDENCE = 3;
    /** Precedence for OR BoolOpetaor */
    private static final int OR_PRECEDENCE = 2;
    
    /**
     * Static method validates the stored query using the Map of trapIds. Throws an Exception upon any parser error.
     */
    public static boolean validateQuery(String query, HashMap<String, Object> trapIds) throws Exception
    {
        return validate(ResolveQuery.getAssignmentData(query), ResolveQuery.getBoolOperatorData(query), trapIds, LOWEST_PRECEDENCE);
    } //validateQuery
    
    
    /**
     * Internal method. Recursive structure with validateExpression is needed to evaluate the query
     * while keeping precedence in mind. At each recursive level, reads expressions (which can be
     * Assignments or parentheses) until a higher-precedence BoolOperator is found, at which point
     * it calls itself recursively to read that subexpression.
     * @param assignments ArrayList of Assignment and parentheses data
     * @param boolOps ArrayList of BoolOperator data
     * @param trapIds HashMap of trapIds data
     * @param precedence Precedence of the previous BoolOperator to be compared to
     */
    private static boolean validate(ArrayList<Object> assignments, ArrayList<BoolOperator> boolOps, HashMap<String, Object> trapIds, int precedence)
    {
        boolean leftReturnValue = validateExpression(assignments, boolOps, trapIds);
        while(!boolOps.isEmpty())
        {
            if(assignments.get(0) instanceof Character && (Character)assignments.get(0) == ')')
            {
                return leftReturnValue;                     //Return to allow a higher recursive level to evaluate the expression
            }
            
            BoolOperator op = boolOps.get(0);
            int newPrecedence = getPrecedence(op);
            if(newPrecedence < precedence) break;
            boolOps.remove(0);                              //Can only remove after the precedence check allows us to proceed

            boolean rightReturnValue = validate(assignments, boolOps, trapIds, newPrecedence);
            switch(op)
            {
                case AND: leftReturnValue = leftReturnValue && rightReturnValue; break;
                case OR: leftReturnValue = leftReturnValue || rightReturnValue; break;
            }
        }
        return leftReturnValue;
    } //validate
    
    /**
     * Internal method. Recursive structure with validate is needed to evaluate the query while
     * keeping precedence in mind. Removes an Assignment or parenthesis from the ArrayList. If
     * the Object is an Assignment, returns whether it is true or false based on the trapIds.
     * If the Object is a '(', calls validate to read that parenthesized expression.
     * @param assignments ArrayList of Assignment and parentheses data
     * @param boolOps ArrayList of BoolOperator data
     * @param trapIds HashMap of trapIds data
     */
    private static boolean validateExpression(ArrayList<Object> assignments, ArrayList<BoolOperator> boolOps, HashMap<String, Object> trapIds)
    {
        Object obj = assignments.remove(0);                     //Removes either an Assignment or a '('
        if(obj instanceof Assignment)
        {
            return validateAssignment((Assignment)obj, trapIds);
        }
        else if(obj instanceof Character)
        {
            if((Character)obj == '(')
            {
                boolean returnBool = validate(assignments, boolOps, trapIds, 0);
                assignments.remove(0);                          //This should be a ')', as it is not removed in validate
                return returnBool;
            }
        }
        return false;
    } //validateExpression
    
    /**
     * Internal method. Evaluates the assignment using the stored Map of trapIds data. Calls appropriate
     * method based on which CmpOperator is used.
     * @param assignment Assignment to be evaluated.
     * @param trapIds HashMap of trapIds data.
     */
    private static boolean validateAssignment(Assignment assignment, HashMap<String, Object> trapIds)
    {
        String name = assignment.getName();
        if(!trapIds.containsKey(name)) 
        {
            return false;
        }
        Object trapIdValue = trapIds.get(name);
        String queryValue = assignment.getValue();
        switch(assignment.getOperator())
        {
            case EQUAL: return equal(trapIdValue, queryValue);
            case NOT_EQUAL: return notEqual(trapIdValue, queryValue);
            case GREATER: return greater(trapIdValue, queryValue);
            case GREATER_OR_EQUAL: return greaterOrEqual(trapIdValue, queryValue);
            case LESS: return less(trapIdValue, queryValue);
            case LESS_OR_EQUAL: return lessOrEqual(trapIdValue, queryValue);
            case CONTAINS: return contains(trapIdValue, queryValue);
            case STARTS_WITH: return startsWith(trapIdValue, queryValue);
            case ENDS_WITH: return endsWith(trapIdValue, queryValue);
        }
        return false;
    } //validateAssignment
    
    
    /**
     * Returns true if the mapped trapId value is equal to the query value. Supports String and Integers.
     * @param trapIdValue The subsituted value based off of trapId data.
     * @param queryValue The righthand side of the Assignment expression.
     */
    private static boolean equal(Object trapIdValue, String queryValue)
    {
        if(trapIdValue instanceof String)
        {
            if(!QueryUtils.isString(queryValue))
            {
                return false;
            }
            return ((String)trapIdValue).equalsIgnoreCase(QueryUtils.trimAndRemoveEscape(queryValue));
        }
        else if(trapIdValue instanceof Integer)
        {
            return (Integer)trapIdValue == Integer.parseInt(queryValue);
        }
        return false;
    } //equal
    
    /**
     * Returns true if the mapped trapId value is not equal to the query value. Supports String and Integers.
     * @param trapIdValue The subsituted value based off of trapId data.
     * @param queryValue The righthand side of the Assignment expression.
     */
    private static boolean notEqual(Object trapIdValue, String queryValue)
    {
        if(trapIdValue instanceof String)
        {
            if(!QueryUtils.isString(queryValue))
            {
                return false;
            }
            return !((String)trapIdValue).equalsIgnoreCase(QueryUtils.trimAndRemoveEscape(queryValue));
        }
        else if(trapIdValue instanceof Integer)
        {
            return (Integer)trapIdValue != Integer.parseInt(queryValue);
        }
        return false;
    } //notEqual
    
    /**
     * Returns true if the mapped trapId value is greater than the query value. Supports Integers only.
     * @param trapIdValue The subsituted value based off of trapId data.
     * @param queryValue The righthand side of the Assignment expression.
     */
    private static boolean greater(Object trapIdValue, String queryValue)
    {
        if(trapIdValue instanceof Integer)
        {
            return (Integer)trapIdValue > Integer.parseInt(queryValue);
        }
        return false;
    } //greater
    
    /**
     * Returns true if the mapped trapId value is greater than or equal to the query value. Supports Integers only.
     * @param trapIdValue The subsituted value based off of trapId data.
     * @param queryValue The righthand side of the Assignment expression.
     */
    private static boolean greaterOrEqual(Object trapIdValue, String queryValue)
    {
        if(trapIdValue instanceof Integer)
        {
            return (Integer)trapIdValue >= Integer.parseInt(queryValue);
        }
        return false;
    } //greaterOrEqual
    
    /**
     * Returns true if the mapped trapId value is less than the query value. Supports Integers only.
     * @param trapIdValue The subsituted value based off of trapId data.
     * @param queryValue The righthand side of the Assignment expression.
     */
    private static boolean less(Object trapIdValue, String queryValue)
    {
        if(trapIdValue instanceof Integer)
        {
            return (Integer)trapIdValue < Integer.parseInt(queryValue);
        }
        return false;
    } //less
    
    /**
     * Returns true if the mapped trapId value is less than or equal to the query value. Supports Integers only.
     * @param trapIdValue The subsituted value based off of trapId data.
     * @param queryValue The righthand side of the Assignment expression.
     */
    private static boolean lessOrEqual(Object trapIdValue, String queryValue)
    {
        if(trapIdValue instanceof Integer)
        {
            return (Integer)trapIdValue <= Integer.parseInt(queryValue);
        }
        return false;
    } //lessOrEqual
    
    /**
     * Returns true if the mapped trapId value contains the query value. Supports Strings only.
     * @param trapIdValue The subsituted value based off of trapId data.
     * @param queryValue The righthand side of the Assignment expression.
     */
    private static boolean contains(Object trapIdValue, String queryValue)
    {
        if(QueryUtils.isString(queryValue) && trapIdValue instanceof String)
        {
            return ((String)trapIdValue).contains(QueryUtils.trimAndRemoveEscape(queryValue));
        }
        return false;
    } //contains
    
    /**
     * Returns true if the mapped trapId value starts with the query value. Supports Strings only.
     * @param trapIdValue The subsituted value based off of trapId data.
     * @param queryValue The righthand side of the Assignment expression.
     */
    private static boolean startsWith(Object trapIdValue, String queryValue)
    {
        if(QueryUtils.isString(queryValue) && trapIdValue instanceof String)
        {
            return ((String)trapIdValue).startsWith(QueryUtils.trimAndRemoveEscape(queryValue));
        }
        return false;
    } //startsWith
    
    /**
     * Returns true if the mapped trapId value ends with the query value. Supports Strings only.
     * @param trapIdValue The subsituted value based off of trapId data.
     * @param queryValue The righthand side of the Assignment expression.
     */
    private static boolean endsWith(Object trapIdValue, String queryValue)
    {
        if(QueryUtils.isString(queryValue) && trapIdValue instanceof String)
        {
            return ((String)trapIdValue).endsWith(QueryUtils.trimAndRemoveEscape(queryValue));
        }
        return false;
    } //endsWith
    
    /**
     * Returns the precedence level of the BoolOperator.
     * @param op The BoolOperator to get the precedence for.
     */
    private static int getPrecedence(BoolOperator op)
    {
        switch(op)
        {
            case AND: return AND_PRECEDENCE;
            case OR: return OR_PRECEDENCE;
        }
        return LOWEST_PRECEDENCE;
    } //getPrecedence
    
    
    //For testing purposes. Populate a HashMap of trapIds and validate a sample query. Prints results to the console.
//    public static void main(String args[])
//    {
//        HashMap<String, Object> trapIds = new HashMap<String, Object>();
//        trapIds.put("1.2.3.4", "Computer Science");
//        trapIds.put("3.14.159", "Pi");
//        trapIds.put("10.20.300", new Integer(1020300));
//        trapIds.put("1.1", new Integer(11));
//        
//        String query = "1.2.3.4 contains 'Science' or 1.1 < 10";
//        
//        try
//        {
//            Boolean result = SNMPInterpreter.validateQuery(query, trapIds);
//            System.out.println(query);
//            System.out.println("This query is " + String.valueOf(result).toUpperCase());
//            System.out.println("TRAP IDS:");
//            for(String key : trapIds.keySet())
//            {
//                System.out.println(key + " " + trapIds.get(key));
//            }
//        }
//        catch (Exception e)
//        {
//            System.out.println(e.getMessage());
//        }
//    } //main
    
} //SNMPInterpreter