/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.query.translator;

import com.resolve.query.QueryParser.Assignment;
import com.resolve.query.QueryParser.BoolOperator;
import com.resolve.query.QueryUtils;

/**
 * This class extends the AbstractQueryTranslator class, thus implementing
 * the {@link QueryTranslator} interface for the ServiceNow gateway.
 * 
 *  For detailed information about ServiceNow operators visit:  
 *  http://wiki.servicenow.com/index.php?title=Using_GlideRecord_to_Query_Tables
 *
 */
public class ServiceNowQueryTranslator extends AbstractQueryTranslator
{
    public String translateBoolOperator(BoolOperator op)
    {
        String print = "";
        switch (op)
        {
            case AND:
                print = "^";
                break;
            case OR:
                print = "^OR";
                break;
        }
        if (print.equals(""))
        {
            throw new RuntimeException("ServiceNow Error: Unsupported boolean operator.");
        }
        return print;
    }

    public String translateAssignment(Assignment obj)
    {
        String print = obj.getName();
        switch (obj.getOperator())
        {
            case EQUAL:
                print += "=";
                break;
            case NOT_EQUAL:
                print += "!=";
                break;
            case GREATER:
                print += ">";
                break;
            case GREATER_OR_EQUAL:
                print += ">=";
                break;
            case LESS:
                print += "<";
                break;
            case LESS_OR_EQUAL:
                print += "<=";
                break;
            case CONTAINS:               // CONTAINS only works with Strings
            {
                QueryUtils.validateString(obj.getValue(), "CONTAINS", "ServiceNow");
                print += "CONTAINS";
                break;
            }
            case STARTS_WITH:            // STARTS_WITH only works with Strings
            {
                QueryUtils.validateString(obj.getValue(), "STARTS_WITH", "ServiceNow");
                print += "STARTSWITH";
                break;
            }
            case ENDS_WITH:
            {
                QueryUtils.validateString(obj.getValue(), "ENDS_WITH", "ServiceNow");
                print += "ENDSWITH";
                break;
            }
            default:
                throw new RuntimeException("ServiceNow Error: Unsupported comparison operator.");
        }
        if (QueryUtils.isString(obj.getValue()))        // a String requires reformatting for ServiceNow
        {
            obj.setValue(QueryUtils.trimAndRemoveEscape(obj.getValue()));
        }
        print += obj.getValue();
        
        return print;
    }

}
