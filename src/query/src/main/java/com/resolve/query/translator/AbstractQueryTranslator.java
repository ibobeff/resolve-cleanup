/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.query.translator;

import com.resolve.query.QueryTranslator;


/**
 * The AbstractQueryTranslator class extends the QueryTranslator interface, which contains
 * two methods: translateAssignment() and translateBoolOperator(). Any subclass of this class
 * must implement these two methods (for more information, see the QueryTranslator interface).
 * Also defines translateParens(), which can be overridden.
 */
public abstract class AbstractQueryTranslator implements QueryTranslator
{
    /**
     * Returns a String that represents the equivalent of an open or closed parenthesis. Override
     * this method if the output format syntax differs from "(" and ")".
     * @param isOpen True if the parenthesis required is an open/starting parenthesis "(". False if closed/ending.
     */
    public String translateParens(boolean isOpen)
    {
        if(isOpen)
        {
            return "(";
        }
        else
        {
            return ")";
        }
    }
}
