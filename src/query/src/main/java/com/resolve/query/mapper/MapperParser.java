/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.query.mapper;

    /*
     * NOTES
     * -----
     * There is no main method for the mapper. The mapper returns a HashMap<String, String>
     * that contains the given keys mapped to the inputted values.
     * - Keys must be separated from their values by the equals sign '='
     * - There should be NO other separation identification (ex. ',') unless it is a value
     * - Public API returns an array of all supported keys:
     *  - public static String[] getKeysString();
     */

    import java.util.HashMap;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class MapperParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "KEY", "VALUE", "WS", "';'", "'='"
    };

    public static final int EOF=-1;
    public static final int T__7=7;
    public static final int T__8=8;
    public static final int KEY=4;
    public static final int VALUE=5;
    public static final int WS=6;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public MapperParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public MapperParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

    public String[] getTokenNames() { return MapperParser.tokenNames; }
    public String getGrammarFileName() { return "C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g"; }


        //The HashMap object to be returned
        private HashMap<String, String> map = new HashMap<String, String>();
        
        /**
         * Returns a String array of the names of all keys supported. Case insensitive.
         */
        public static String[] getKeysString()
        {
            String[] list = { "EVENT", "REFERENCE", "PERSISTENT", "WIKI",
                      "PROBLEMID", "EVENTREF", "ALERTID", "PRBNO" };
            return list;
        }
        
        /* These two methods are used to tell the parser to automatically stop upon error. */
        
        @Override
        protected Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException
        {
            throw new MismatchedTokenException(ttype, input);
        }
        
        @Override
        public Object recoverFromMismatchedSet(IntStream input, RecognitionException e, BitSet follow) throws RecognitionException
        {
                throw e;
        }



    // $ANTLR start "prog"
    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:134:1: prog returns [HashMap<String, String> returnMap] : ( WS )* ( value )? ( WS )* ( pair ( WS )* )+ ( end )? EOF ;
    public final HashMap<String, String> prog() throws RecognitionException {
        HashMap<String, String> returnMap = null;


        try {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:135:2: ( ( WS )* ( value )? ( WS )* ( pair ( WS )* )+ ( end )? EOF )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:135:4: ( WS )* ( value )? ( WS )* ( pair ( WS )* )+ ( end )? EOF
            {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:135:4: ( WS )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==WS) ) {
                    alt1=1;
                }


                switch (alt1) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:135:4: WS
                    {
                    match(input,WS,FOLLOW_WS_in_prog68); 

                    }
                    break;

                default :
                    break loop1;
                }
            } while (true);


            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:135:8: ( value )?
            int alt2=2;
            alt2 = dfa2.predict(input);
            switch (alt2) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:135:8: value
                    {
                    pushFollow(FOLLOW_value_in_prog71);
                    value();

                    state._fsp--;


                    }
                    break;

            }


            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:135:15: ( WS )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==WS) ) {
                    alt3=1;
                }


                switch (alt3) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:135:15: WS
                    {
                    match(input,WS,FOLLOW_WS_in_prog74); 

                    }
                    break;

                default :
                    break loop3;
                }
            } while (true);


            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:135:19: ( pair ( WS )* )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==KEY) ) {
                    alt5=1;
                }


                switch (alt5) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:135:20: pair ( WS )*
                    {
                    pushFollow(FOLLOW_pair_in_prog78);
                    pair();

                    state._fsp--;


                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:135:25: ( WS )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==WS) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                        case 1 :
                            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:135:25: WS
                            {
                            match(input,WS,FOLLOW_WS_in_prog80); 

                            }
                            break;

                        default :
                            break loop4;
                        }
                    } while (true);


                    }
                    break;

                default :
                    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);


            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:135:31: ( end )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==7) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:135:31: end
                    {
                    pushFollow(FOLLOW_end_in_prog85);
                    end();

                    state._fsp--;


                    }
                    break;

            }


            match(input,EOF,FOLLOW_EOF_in_prog88); 

             returnMap = map; 

            }

        }
           
            /* Throws RecognitionException to allow us to catch it in our own code in Eclipse. */
            catch (RecognitionException e)
            {
                throw e;
                }

        finally {
            // do for sure before leaving
        }
        return returnMap;
    }
    // $ANTLR end "prog"



    // $ANTLR start "pair"
    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:139:1: pair : KEY ( WS )* '=' ( WS )* value ;
    public final void pair() throws RecognitionException {
        Token KEY1=null;
        MapperParser.value_return value2 =null;


        try {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:139:6: ( KEY ( WS )* '=' ( WS )* value )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:139:8: KEY ( WS )* '=' ( WS )* value
            {
            KEY1=(Token)match(input,KEY,FOLLOW_KEY_in_pair101); 

            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:139:12: ( WS )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==WS) ) {
                    alt7=1;
                }


                switch (alt7) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:139:12: WS
                    {
                    match(input,WS,FOLLOW_WS_in_pair103); 

                    }
                    break;

                default :
                    break loop7;
                }
            } while (true);


            match(input,8,FOLLOW_8_in_pair106); 

            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:139:20: ( WS )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==WS) ) {
                    alt8=1;
                }


                switch (alt8) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:139:20: WS
                    {
                    match(input,WS,FOLLOW_WS_in_pair108); 

                    }
                    break;

                default :
                    break loop8;
                }
            } while (true);


            pushFollow(FOLLOW_value_in_pair111);
            value2=value();

            state._fsp--;



                        map.put((KEY1!=null?KEY1.getText():null).toUpperCase(), (value2!=null?input.toString(value2.start,value2.stop):null));
                    

            }

        }
           
            /* Throws RecognitionException to allow us to catch it in our own code in Eclipse. */
            catch (RecognitionException e)
            {
                throw e;
                }

        finally {
            // do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "pair"


    public static class value_return extends ParserRuleReturnScope {
    };


    // $ANTLR start "value"
    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:146:1: value : ( ( WS )* ( VALUE | KEY ) )+ ;
    public final MapperParser.value_return value() throws RecognitionException {
        MapperParser.value_return retval = new MapperParser.value_return();
        retval.start = input.LT(1);


        try {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:146:7: ( ( ( WS )* ( VALUE | KEY ) )+ )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:146:9: ( ( WS )* ( VALUE | KEY ) )+
            {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:146:9: ( ( WS )* ( VALUE | KEY ) )+
            int cnt10=0;
            loop10:
            do {
                int alt10=2;
                alt10 = dfa10.predict(input);
                switch (alt10) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:146:11: ( WS )* ( VALUE | KEY )
                    {
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:146:11: ( WS )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0==WS) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                        case 1 :
                            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:146:11: WS
                            {
                            match(input,WS,FOLLOW_WS_in_value128); 

                            }
                            break;

                        default :
                            break loop9;
                        }
                    } while (true);


                    if ( (input.LA(1) >= KEY && input.LA(1) <= VALUE) ) {
                        input.consume();
                        state.errorRecovery=false;
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        throw mse;
                    }


                    }
                    break;

                default :
                    if ( cnt10 >= 1 ) break loop10;
                        EarlyExitException eee =
                            new EarlyExitException(10, input);
                        throw eee;
                }
                cnt10++;
            } while (true);


            }

            retval.stop = input.LT(-1);


        }
           
            /* Throws RecognitionException to allow us to catch it in our own code in Eclipse. */
            catch (RecognitionException e)
            {
                throw e;
                }

        finally {
            // do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "value"



    // $ANTLR start "end"
    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:150:1: end : ';' ( WS )* ( value )? ( WS )* ;
    public final void end() throws RecognitionException {
        try {
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:150:5: ( ';' ( WS )* ( value )? ( WS )* )
            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:150:7: ';' ( WS )* ( value )? ( WS )*
            {
            match(input,7,FOLLOW_7_in_end149); 

            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:150:11: ( WS )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==WS) ) {
                    alt11=1;
                }


                switch (alt11) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:150:11: WS
                    {
                    match(input,WS,FOLLOW_WS_in_end151); 

                    }
                    break;

                default :
                    break loop11;
                }
            } while (true);


            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:150:15: ( value )?
            int alt12=2;
            alt12 = dfa12.predict(input);
            switch (alt12) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:150:15: value
                    {
                    pushFollow(FOLLOW_value_in_end154);
                    value();

                    state._fsp--;


                    }
                    break;

            }


            // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:150:22: ( WS )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==WS) ) {
                    alt13=1;
                }


                switch (alt13) {
                case 1 :
                    // C:\\Users\\christopher.wong\\Desktop\\antlr\\Mapper.g:150:22: WS
                    {
                    match(input,WS,FOLLOW_WS_in_end157); 

                    }
                    break;

                default :
                    break loop13;
                }
            } while (true);


            }

        }
           
            /* Throws RecognitionException to allow us to catch it in our own code in Eclipse. */
            catch (RecognitionException e)
            {
                throw e;
                }

        finally {
            // do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "end"

    // Delegated rules


    protected DFA2 dfa2 = new DFA2(this);
    protected DFA10 dfa10 = new DFA10(this);
    protected DFA12 dfa12 = new DFA12(this);
    static final String DFA2_eotS =
        "\6\uffff";
    static final String DFA2_eofS =
        "\6\uffff";
    static final String DFA2_minS =
        "\3\4\1\uffff\1\4\1\uffff";
    static final String DFA2_maxS =
        "\2\6\1\10\1\uffff\1\10\1\uffff";
    static final String DFA2_acceptS =
        "\3\uffff\1\1\1\uffff\1\2";
    static final String DFA2_specialS =
        "\6\uffff}>";
    static final String[] DFA2_transitionS = {
            "\1\2\1\3\1\1",
            "\1\2\1\3\1\1",
            "\2\3\1\4\1\uffff\1\5",
            "",
            "\2\3\1\4\1\uffff\1\5",
            ""
    };

    static final short[] DFA2_eot = DFA.unpackEncodedString(DFA2_eotS);
    static final short[] DFA2_eof = DFA.unpackEncodedString(DFA2_eofS);
    static final char[] DFA2_min = DFA.unpackEncodedStringToUnsignedChars(DFA2_minS);
    static final char[] DFA2_max = DFA.unpackEncodedStringToUnsignedChars(DFA2_maxS);
    static final short[] DFA2_accept = DFA.unpackEncodedString(DFA2_acceptS);
    static final short[] DFA2_special = DFA.unpackEncodedString(DFA2_specialS);
    static final short[][] DFA2_transition;

    static {
        int numStates = DFA2_transitionS.length;
        DFA2_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA2_transition[i] = DFA.unpackEncodedString(DFA2_transitionS[i]);
        }
    }

    class DFA2 extends DFA {

        public DFA2(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 2;
            this.eot = DFA2_eot;
            this.eof = DFA2_eof;
            this.min = DFA2_min;
            this.max = DFA2_max;
            this.accept = DFA2_accept;
            this.special = DFA2_special;
            this.transition = DFA2_transition;
        }
        public String getDescription() {
            return "135:8: ( value )?";
        }
    }
    static final String DFA10_eotS =
        "\6\uffff";
    static final String DFA10_eofS =
        "\2\3\1\4\2\uffff\1\4";
    static final String DFA10_minS =
        "\3\4\2\uffff\1\4";
    static final String DFA10_maxS =
        "\2\7\1\10\2\uffff\1\10";
    static final String DFA10_acceptS =
        "\3\uffff\1\2\1\1\1\uffff";
    static final String DFA10_specialS =
        "\6\uffff}>";
    static final String[] DFA10_transitionS = {
            "\1\2\1\4\1\1\1\3",
            "\1\2\1\4\1\1\1\3",
            "\2\4\1\5\1\4\1\3",
            "",
            "",
            "\2\4\1\5\1\4\1\3"
    };

    static final short[] DFA10_eot = DFA.unpackEncodedString(DFA10_eotS);
    static final short[] DFA10_eof = DFA.unpackEncodedString(DFA10_eofS);
    static final char[] DFA10_min = DFA.unpackEncodedStringToUnsignedChars(DFA10_minS);
    static final char[] DFA10_max = DFA.unpackEncodedStringToUnsignedChars(DFA10_maxS);
    static final short[] DFA10_accept = DFA.unpackEncodedString(DFA10_acceptS);
    static final short[] DFA10_special = DFA.unpackEncodedString(DFA10_specialS);
    static final short[][] DFA10_transition;

    static {
        int numStates = DFA10_transitionS.length;
        DFA10_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA10_transition[i] = DFA.unpackEncodedString(DFA10_transitionS[i]);
        }
    }

    class DFA10 extends DFA {

        public DFA10(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 10;
            this.eot = DFA10_eot;
            this.eof = DFA10_eof;
            this.min = DFA10_min;
            this.max = DFA10_max;
            this.accept = DFA10_accept;
            this.special = DFA10_special;
            this.transition = DFA10_transition;
        }
        public String getDescription() {
            return "()+ loopback of 146:9: ( ( WS )* ( VALUE | KEY ) )+";
        }
    }
    static final String DFA12_eotS =
        "\4\uffff";
    static final String DFA12_eofS =
        "\2\3\2\uffff";
    static final String DFA12_minS =
        "\2\4\2\uffff";
    static final String DFA12_maxS =
        "\2\6\2\uffff";
    static final String DFA12_acceptS =
        "\2\uffff\1\1\1\2";
    static final String DFA12_specialS =
        "\4\uffff}>";
    static final String[] DFA12_transitionS = {
            "\2\2\1\1",
            "\2\2\1\1",
            "",
            ""
    };

    static final short[] DFA12_eot = DFA.unpackEncodedString(DFA12_eotS);
    static final short[] DFA12_eof = DFA.unpackEncodedString(DFA12_eofS);
    static final char[] DFA12_min = DFA.unpackEncodedStringToUnsignedChars(DFA12_minS);
    static final char[] DFA12_max = DFA.unpackEncodedStringToUnsignedChars(DFA12_maxS);
    static final short[] DFA12_accept = DFA.unpackEncodedString(DFA12_acceptS);
    static final short[] DFA12_special = DFA.unpackEncodedString(DFA12_specialS);
    static final short[][] DFA12_transition;

    static {
        int numStates = DFA12_transitionS.length;
        DFA12_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA12_transition[i] = DFA.unpackEncodedString(DFA12_transitionS[i]);
        }
    }

    class DFA12 extends DFA {

        public DFA12(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 12;
            this.eot = DFA12_eot;
            this.eof = DFA12_eof;
            this.min = DFA12_min;
            this.max = DFA12_max;
            this.accept = DFA12_accept;
            this.special = DFA12_special;
            this.transition = DFA12_transition;
        }
        public String getDescription() {
            return "150:15: ( value )?";
        }
    }
 

    public static final BitSet FOLLOW_WS_in_prog68 = new BitSet(new long[]{0x0000000000000070L});
    public static final BitSet FOLLOW_value_in_prog71 = new BitSet(new long[]{0x0000000000000050L});
    public static final BitSet FOLLOW_WS_in_prog74 = new BitSet(new long[]{0x0000000000000050L});
    public static final BitSet FOLLOW_pair_in_prog78 = new BitSet(new long[]{0x00000000000000D0L});
    public static final BitSet FOLLOW_WS_in_prog80 = new BitSet(new long[]{0x00000000000000D0L});
    public static final BitSet FOLLOW_end_in_prog85 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_prog88 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_KEY_in_pair101 = new BitSet(new long[]{0x0000000000000140L});
    public static final BitSet FOLLOW_WS_in_pair103 = new BitSet(new long[]{0x0000000000000140L});
    public static final BitSet FOLLOW_8_in_pair106 = new BitSet(new long[]{0x0000000000000070L});
    public static final BitSet FOLLOW_WS_in_pair108 = new BitSet(new long[]{0x0000000000000070L});
    public static final BitSet FOLLOW_value_in_pair111 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WS_in_value128 = new BitSet(new long[]{0x0000000000000070L});
    public static final BitSet FOLLOW_set_in_value131 = new BitSet(new long[]{0x0000000000000072L});
    public static final BitSet FOLLOW_7_in_end149 = new BitSet(new long[]{0x0000000000000072L});
    public static final BitSet FOLLOW_WS_in_end151 = new BitSet(new long[]{0x0000000000000072L});
    public static final BitSet FOLLOW_value_in_end154 = new BitSet(new long[]{0x0000000000000042L});
    public static final BitSet FOLLOW_WS_in_end157 = new BitSet(new long[]{0x0000000000000042L});

}