/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.rsproxy;

import com.resolve.util.Log;

public class ProxyMetrics extends Thread
{
    Main main;
    boolean metrics = true;
    private long interval = 60 * 1000; // in milliseconds

    private Object lck = new Object();

    public ProxyMetrics(Main main, ConfigGeneral configGeneral)
    {
        this.main = main;
        this.metrics = configGeneral.isMetrics();
        if (configGeneral.getMetricsInterval() > 0)
        {
            this.interval = configGeneral.getMetricsInterval();
        }
    }

    @Override
    public void run()
    {
        if(metrics)
        {
            try
            {
                while (true)
                {
                    synchronized (lck)
                    {
                        lck.wait(interval);
                    }
                    
                    Log.log.info("Proxy statistics...");
                    Log.log.info("Available processors (cores): " + Runtime.getRuntime().availableProcessors());
                    /* Total amount of free memory available to the JVM */
                    Log.log.info("Free memory (bytes): " + Runtime.getRuntime().freeMemory());
                    /* This will return Long.MAX_VALUE if there is no preset limit */
                    long maxMemory = Runtime.getRuntime().maxMemory();
                    /* Maximum amount of memory the JVM will attempt to use */
                    Log.log.info("Maximum memory (bytes): " + (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemory));
                    /* Total memory currently available to the JVM */
                    Log.log.info("Total memory available to JVM (bytes): " + Runtime.getRuntime().totalMemory());
                    Log.log.info("Number of connections: " + main.numberOfConnection());
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        else
        {
            Log.log.info("Metrics disabled.");
        }
    }
}
