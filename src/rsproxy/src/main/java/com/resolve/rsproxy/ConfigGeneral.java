/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsproxy;

import com.resolve.util.XDoc;

public class ConfigGeneral extends com.resolve.rsbase.ConfigGeneral
{
    private static final long serialVersionUID = -3825573733896501978L;
	private long fileCheckInterval = 5000; //in milliseconds
    private boolean metrics = true;
    private long metricsInterval = 60000; //in milliseconds
    public int scheduledPool;

    
    public ConfigGeneral(XDoc config) throws Exception
    {
        super(config);

        define("fileCheckInterval", LONG, "./GENERAL/@FILE_CHECK_INTERVAL");
        define("metrics", BOOLEAN, "./GENERAL/@METRICS");
        define("metricsInterval", LONG, "./GENERAL/@METRICS_INTERVAL");
        define("scheduledPool", INTEGER, "./GENERAL/@SCHEDULEDPOOL");
    } // ConfigGeneral

    public void load()
    {
        super.load();
    } // load

    public void save()
    {
        super.save();
    } // save

    public long getFileCheckInterval()
    {
        return fileCheckInterval;
    }

    public void setFileCheckInterval(long fileCheckInterval)
    {
        this.fileCheckInterval = fileCheckInterval;
    }

    public boolean isMetrics()
    {
        return metrics;
    }

    public void setMetrics(boolean metrics)
    {
        this.metrics = metrics;
    }

    public long getMetricsInterval()
    {
        return metricsInterval;
    }

    public void setMetricsInterval(long metricsInterval)
    {
        this.metricsInterval = metricsInterval;
    }
    
    public int getScheduledPool() {
        return scheduledPool;
    }

    public void setScheduledPool(int scheduledPool) {
        this.scheduledPool = scheduledPool;
    }

} // ConfigGeneral
