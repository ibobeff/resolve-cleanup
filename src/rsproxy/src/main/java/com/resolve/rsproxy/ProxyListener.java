/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/
package com.resolve.rsproxy;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;

public class ProxyListener implements Runnable
{
    private ProxyPortConfig portConfig;

    private ServerSocket server;
    private boolean isRunning = false;
    private List<ProxyConnection> proxyConnections = new ArrayList<ProxyConnection>();
    
    public ProxyListener(final ProxyPortConfig portConfig)
    {
        this.portConfig = portConfig;
    }

    public boolean isRunning()
    {
        return isRunning;
    }

    public void setRunning(boolean isRunning)
    {
        this.isRunning = isRunning;
    }

    public List<ProxyConnection> getProxyConnections()
    {
        return proxyConnections;
    }

    @Override
    public void run()
    {
        try
        {
            this.isRunning = true;
            InetAddress address = InetAddress.getByName(portConfig.getListenerHost());
            server = new ServerSocket(portConfig.getListenerPort(), 10, address);
            // Keep listening for new clients!
            while (true)
            {
                try
                {
                    Socket client = server.accept();
                    String remoteIP = client.getInetAddress().getHostAddress();
                    ConfigProxy configProxy = ((Main) MainBase.main).getConfigProxy();
                    if(configProxy.isAllowedIP(remoteIP))
                    {
                        if(Log.log.isDebugEnabled())
                        {
                            Log.log.debug("Established inbound connection, Remote host/IP : " + remoteIP + ", remote port : " + client.getPort() + ", listener port : " + client.getLocalPort());
                        }
                        
                        ProxyConnection proxyConnection = new ProxyConnection(portConfig, client);
                        
                        /*
                         * Check if tunnel is formed. If yes, go ahead and create streams for read/write
                         */
                        if(proxyConnection.isRunning())
                        {
                            proxyConnections.add(proxyConnection);
                            Thread thread = new Thread(proxyConnection);
                            thread.start();
                        }
                        else
                        {
                            /*
                             * Tunnel could not be formed. Shutdown all the connected sockets and start over.
                             */
                            shutdown();
                            server = new ServerSocket(portConfig.getListenerPort(), 10, address);
                        }
                    }
                    else
                    {
                        Log.log.debug("Remote host/IP : " + remoteIP + " trying to connect but not allowed due to whitelist configuration.");
                        Thread.sleep(2000L); //sleep for 2 seconds
                    }
                }
                catch(Exception e)
                {
                    shutdown();
                    Log.log.debug(e.getMessage(), e);
                }
            }
        }
        catch (Exception e)
        {
            //this.isRunning = false;
            Log.log.info(e);
        }
        
    }

    public void shutdown()
    {
        for(ProxyConnection proxyConnection : proxyConnections)
        {
            if(proxyConnection != null)
            {
                proxyConnection.shutdown();
            }
        }
        //clear the collection
        proxyConnections.clear();
        
        if(server != null && !server.isClosed())
        {
            try
            {
                this.isRunning = false;
                server.close();
            }
            catch (IOException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    @Override
    public String toString()
    {
        return "ProxyListener [listenerHost=" + portConfig.getListenerHost() + ", listenerPort=" + portConfig.getListenerPort() + ", remoteHost=" + portConfig.getRemoteHost() + ", remotePort=" + portConfig.getRemotePort() + ", isRunning=" + isRunning + "]";
    }
}
