/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/
package com.resolve.rsproxy;

import com.resolve.util.SystemUtil;

public class ProxyPortConfig
{
    String listenerHost;
    int listenerPort;
    String remoteHost;
    int remotePort;
    int bufferSize = 1024;
    int workerCount = 1;

    public ProxyPortConfig(String listenerHost, int listenerPort, String remoteHost, int remotePort)
    {
        this.listenerHost = listenerHost;
        this.listenerPort = listenerPort;
        this.remoteHost = remoteHost;
        this.remotePort = remotePort;
        this.workerCount = SystemUtil.getCPUCount();
    }

    public String getListenerHost()
    {
        return listenerHost;
    }

    public void setListenerHost(String host)
    {
        this.listenerHost = host;
    }

    public int getListenerPort()
    {
        return listenerPort;
    }

    public void setListenerPort(int listenerPort)
    {
        this.listenerPort = listenerPort;
    }

    public String getRemoteHost()
    {
        return remoteHost;
    }

    public void setRemoteHost(String remoteHost)
    {
        this.remoteHost = remoteHost;
    }

    public int getRemotePort()
    {
        return remotePort;
    }

    public void setRemotePort(int remotePort)
    {
        this.remotePort = remotePort;
    }

    public int getBufferSize()
    {
        return bufferSize;
    }

    public void setBufferSize(int bufferSize)
    {
        this.bufferSize = bufferSize;
    }

    public int getWorkerCount()
    {
        return workerCount;
    }

    public void setWorkerCount(int workerCount)
    {
        this.workerCount = workerCount;
    }

    @Override
    public String toString()
    {
        return "ProxyPortConfig [listenerHost=" + listenerHost + ", listenerPort=" + listenerPort + ", remoteHost=" + remoteHost + ", remotePort=" + remotePort + "]";
    }
}
