/******************************************************************************
 * (C) Copyright 2015
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.rsproxy;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.Socket;

import com.resolve.util.Log;

public class ProxyConnection implements Runnable
{
    private boolean isRunning = true;
    private String clientIP = null;
    
    private Socket remoteSocket;
    private Socket clientSocket;

    private InputStream serverInputStream;
    private OutputStream serverOutputStream;

    private InputStream clientInputStream;
    private OutputStream clientOutputStream;

    private SocketListener socketListener;

    public ProxyConnection(ProxyPortConfig portConfig, Socket clientSocket)
    {
        try
        {
            this.clientSocket = clientSocket;
            this.remoteSocket = new Socket(portConfig.getRemoteHost(), portConfig.getRemotePort());
            
            Log.log.info("Connected to RSMQ: " + remoteSocket.getInetAddress().getHostAddress() + " at " + remoteSocket.getPort() + " from " + clientSocket.getInetAddress().getHostAddress() + " at " + clientSocket.getPort());
            
            // Initiate the in and out fields.
            this.clientInputStream = this.clientSocket.getInputStream();
            this.clientOutputStream = this.clientSocket.getOutputStream();

            this.serverInputStream = this.remoteSocket.getInputStream();
            this.serverOutputStream = this.remoteSocket.getOutputStream();

        }
        catch (ConnectException ex)
        {
            Log.log.info("Connection error during creating socket to RSMQ: " + portConfig.getRemoteHost() + " at " + portConfig.getRemotePort());
            // kill the process.
            this.shutdown();
            return;
        }
        catch (IOException ex)
        {
            Log.log.info("Connection error during creating socket to RSMQ: " + portConfig.getRemoteHost() + " at " + portConfig.getRemotePort());
            // kill the process.
            this.shutdown();
            return;
        }

        // Start up the SocketListener.
        clientIP = clientSocket.getInetAddress().getHostAddress();
        this.socketListener = new SocketListener(this);
        Thread thread = new Thread(this.socketListener);
        thread.start();
    }


    public InputStream getServerInputStream()
    {
        return serverInputStream;
    }

    public OutputStream getServerOutputStream()
    {
        return serverOutputStream;
    }

    public OutputStream getClientOutputStream()
    {
        return clientOutputStream;
    }

    public InputStream getClientInputStream()
    {
        return clientInputStream;
    }

    public String getClientIP()
    {
        return clientIP;
    }

    public void setClientIP(String clientIP)
    {
        this.clientIP = clientIP;
    }

    public void run()
    {
        try
        {
            final byte[] request = new byte[1024];

            int bytesRead;

            // Try and read lines from the client.
            while (isRunning && this.clientInputStream != null && (bytesRead = this.clientInputStream.read(request)) != -1)
            {
                this.serverOutputStream.write(request, 0, bytesRead);
                this.serverOutputStream.flush();

                if (Log.log.isDebugEnabled())
                {
                    Log.log.trace("Data read: " + new String(request, "UTF-8"));
                }
            }

            // Client disconnected.
            this.shutdown();
        }
        catch (IOException e)
        {
            Log.log.info(e.getMessage() + " " + remoteSocket.getInetAddress() + "/" + remoteSocket.getPort());
            this.shutdown();
        }
    }

    /**
     * Kills the Registry, this happens when either the client or server
     * disconnects.
     */
    public void shutdown()
    {
        if (this.socketListener != null) this.socketListener.shutdown();
        isRunning = false;

        Log.log.debug("Client disconnected: " + clientSocket.getInetAddress().getHostAddress() + ":" + clientSocket.getPort() + " from local port: " + clientSocket.getLocalPort());
        
        try
        {
            if (this.remoteSocket != null)
            {
                this.remoteSocket.close();
            }
            if (this.clientSocket != null)
            {
                this.clientSocket.close();
                
            }
        }
        catch (IOException ex)
        {
            // Do nothing.
        }
        finally
        {
            //catching the exceptions individually is critical, do not change it 
            try
            {
                if (getServerInputStream() != null)
                {
                    getServerInputStream().close();
                }
            }
            catch (IOException e)
            {
                //do nothing
            }
            try
            {
                if (getServerOutputStream() != null)
                {
                    getServerOutputStream().close();
                }
            }
            catch (IOException e)
            {
                //do nothing
            }
            try
            {
                if (getClientInputStream() != null)
                {
                    getClientInputStream().close();
                }
            }
            catch (IOException e)
            {
                //do nothing
            }
            try
            {
                if (getClientOutputStream() != null)
                {
                    getClientOutputStream().close();
                }
            }
            catch (IOException e)
            {
                //do nothing
            }
        }
    }
    
    public boolean isRunning()
    {
        return this.isRunning;
    }
}
