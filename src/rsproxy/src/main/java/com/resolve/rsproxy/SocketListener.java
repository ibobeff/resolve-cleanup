/******************************************************************************
 * (C) Copyright 2015
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.rsproxy;

import java.io.IOException;

import com.resolve.util.Log;

public class SocketListener implements Runnable
{
    private ProxyConnection proxyConnection;
    private boolean isRunning = true;

    public SocketListener(ProxyConnection proxyConnection)
    {
        this.proxyConnection = proxyConnection;
    }

    public void run()
    {
        try
        {
            final byte[] request = new byte[1024];
            int bytesRead;

            while (isRunning && (bytesRead = proxyConnection.getServerInputStream().read(request)) != -1)
            {
                proxyConnection.getClientOutputStream().write(request, 0, bytesRead);
                proxyConnection.getClientOutputStream().flush();
            }
            Log.log.info("Socket listener stopped.");
        }
        catch (IOException e)
        {
            Log.log.info(e.getMessage(), e);
            this.shutdown();
        }
        finally
        {
            /*
             * Flow would come here only if there's a connection problem.
             * So, shutdown entire tunnel. A new tunnel will be built after client (RSRemote, RSControl...) asks for it.
             */
            proxyConnection.shutdown();
        }
    }

    /**
     * Stops the processing.
     */
    public void shutdown()
    {
        isRunning = false;
    }
}
