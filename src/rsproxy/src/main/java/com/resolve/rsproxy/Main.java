/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsproxy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.resolve.rsbase.ConfigId;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;
import com.resolve.util.XDoc;

public class Main extends MainBase
{
    private ConfigProxy configProxy;
    private Map<Integer, ProxyListener> proxyListeners = new HashMap<Integer, ProxyListener>();
    
    public static void main(final String[] args)
    {
        String serviceName = null;

        try
        {
            if (args.length > 0)
            {
                serviceName = args[0];
            }

            main = new Main();
            main.init(serviceName);
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            Log.log.error("Failed to start RSPROXY", t);
            System.exit(0);
        }
    }

    @Override
    public void init(String serviceName) throws Exception
    {
        // init release information
        initRelease(new Release(serviceName));

        // init logging
        initLog();

        // init signal handlers
        initShutdownHandler();

        // init configuration
        initConfigFile();
        initConfig();

        ConfigChecker cc = new ConfigChecker(this, configFile, ((ConfigGeneral) configGeneral).getFileCheckInterval());
        cc.start();

        startListeners();

        ProxyMetrics proxyMetrics = new ProxyMetrics(this, ((ConfigGeneral) configGeneral));
        proxyMetrics.start();

        System.out.println("RSPROXY started...");
    }

    @Override
    protected void loadConfig(XDoc configDoc) throws Exception
    {
        if (configGeneral == null)
        {
            configGeneral = new ConfigGeneral(configDoc);
            configGeneral.load();
        }
        if (configId == null)
        {
            configId = new ConfigId(configDoc);
            configId.load();
        }
        if (configProxy == null)
        {
            configProxy = new ConfigProxy(configDoc);
            configProxy.load();
        }
    } // loadConfig
    
    /**
     * This method run when the config file is changed
     */
    public void reinit()
    {
        Set<String> existingWhiteList = configProxy.getWhiteList();
        
        // init configuration
        configProxy = null;
        try
        {
            initConfig();
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        Set<String> newWhiteList = configProxy.getWhiteList();
        
        Map<Integer, ProxyPortConfig> latestProxyListeners = configProxy.getProxyPortConfigs();
        Map<Integer, ProxyListener> runningProxyListeners = new HashMap<Integer, ProxyListener>();
        
        //check existing listeners
        for(Integer listenerPort : proxyListeners.keySet())
        {
            ProxyListener proxyListener = proxyListeners.get(listenerPort);
            if(latestProxyListeners.containsKey(listenerPort))
            {
                runningProxyListeners.put(listenerPort, proxyListener);
                Log.log.info("Proxy already in running state " + proxyListener.toString());
            }
            else
            {
                Log.log.info("Shutting down proxy " + proxyListener.toString());
                proxyListeners.get(listenerPort).shutdown();
            }
        }
        
        proxyListeners.clear();
        proxyListeners.putAll(runningProxyListeners);
        
        if(latestProxyListeners.size() > 0)
        {
            Log.log.info("Reinitializing RSProxy with " + latestProxyListeners.size() + " listener(s)");
            ExecutorService executorService = Executors.newFixedThreadPool(latestProxyListeners.size());
            
            for (final ProxyPortConfig portConfig : latestProxyListeners.values())
            {
                if(!proxyListeners.containsKey(portConfig.getListenerPort()))
                {
                    ProxyListener proxyListener = new ProxyListener(portConfig);
                    executorService.execute(proxyListener);
                    proxyListeners.put(portConfig.getListenerPort(), proxyListener);
                    Log.log.info("Proxy started " + portConfig.toString());
                }
            }
        }

        //now shotdown connections based on whitelist
        if(existingWhiteList.isEmpty())
        {
            if(!newWhiteList.isEmpty())
            {
                for(ProxyListener proxyListener : proxyListeners.values())
                {
                    List<ProxyConnection> proxyConnections = proxyListener.getProxyConnections();
                    for(ProxyConnection proxyConnection : proxyConnections)
                    {
                        if(proxyConnection != null)
                        {
                            if(!newWhiteList.contains(proxyConnection.getClientIP()))
                            {
                                Log.log.info("Remote IP " + proxyConnection.getClientIP() + " no longer allowed through white list, disconnecting");
                                proxyConnection.shutdown();
                            }
                        }
                    }
                }
            }
        }
        else
        {
            for(String ipAddress : existingWhiteList)
            {
                if(!newWhiteList.contains(ipAddress))
                {
                    for(ProxyListener proxyListener : proxyListeners.values())
                    {
                        List<ProxyConnection> proxyConnections = proxyListener.getProxyConnections();
                        for(ProxyConnection proxyConnection : proxyConnections)
                        {
                            if(proxyConnection != null)
                            {
                                if(ipAddress.equalsIgnoreCase(proxyConnection.getClientIP()))
                                {
                                    Log.log.info("Remote IP " + ipAddress + " no longer allowed through white list, disconnecting");
                                    proxyConnection.shutdown();
                                }
                            }
                        }
                    }
                }
            }
        }
        
        Log.log.info("RSPROXY reinitialized...");
    }

    /**
     * This method runs during the initial start up.
     */
    private void startListeners()
    {
        Map<Integer, ProxyPortConfig> latestProxyListeners = configProxy.getProxyPortConfigs();
        if(latestProxyListeners.size() > 0)
        {
            Log.log.info("Starting RSProxy with " + latestProxyListeners.size() + " ports");
            ExecutorService executorService = Executors.newFixedThreadPool(latestProxyListeners.size());
            for (final ProxyPortConfig portConfig : latestProxyListeners.values())
            {
                Log.log.info("Starting " + portConfig.toString());
                ProxyListener proxyListener = new ProxyListener(portConfig);
                executorService.execute(proxyListener);
                proxyListeners.put(portConfig.getListenerPort(), proxyListener);
            }
        }
    }

    public int numberOfConnection()
    {
        int result = 0;
        for(ProxyListener proxyListener : proxyListeners.values())
        {
            result += proxyListener.getProxyConnections().size();
        }

        return result;
    }
    
    @Override
    protected void exit(String[] argv)
    {
        System.exit(0);
    }
    
    @Override
    protected void terminate()
    {
        // shutdown
        isShutdown = true;

        Log.log.warn("Terminating " + release.name.toUpperCase());

        try
        {
            Log.log.info("Closing proxy listeners");
            for(ProxyListener proxyListener : proxyListeners.values())
            {
                proxyListener.shutdown();
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to terminate RSProxy: " + e.getMessage(), e);
        }
        Log.log.warn("Terminated " + release.name.toUpperCase());
    } // terminate

    public ConfigProxy getConfigProxy()
    {
        return configProxy;
    }
}
