/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsproxy;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.util.ConfigMap;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigProxy extends ConfigMap
{
    private static final long serialVersionUID = 314013075277003479L;
	private Map<Integer, ProxyPortConfig> proxyPortConfigs = new HashMap<Integer, ProxyPortConfig>();
    private Set<String> whiteList = new HashSet<String>();
    
    public ConfigProxy(XDoc config) throws Exception
    {
        super(config);
    } // ConfigESB

    boolean addListener(ProxyPortConfig listener)
    {
        proxyPortConfigs.put(listener.getListenerPort(), listener);
        return true;
    }

    public void load() throws Exception
    {
        String whiteListString = xdoc.getStringValue("./WHITE_LIST");
        if(StringUtils.isNotBlank(whiteListString))
        {
            Log.log.info("RSProxy white list : " + whiteListString);
            this.whiteList = StringUtils.stringToSet(whiteListString, ",");
        }
        List listeners = xdoc.getListMapValue("./PROXY/LISTENER");

        if (listeners.size() > 0)
        {
            for (Iterator i = listeners.iterator(); i.hasNext();)
            {
                Map values = (Map) i.next();
                String host = (String) values.get("LISTENER_HOST");
                int listenerPort = Integer.valueOf((String) values.get("LISTENER_PORT"));
                String remoteHost = (String) values.get("REMOTE_HOST");
                int remotePort = Integer.valueOf((String) values.get("REMOTE_PORT"));
                ProxyPortConfig listener = new ProxyPortConfig(host, listenerPort, remoteHost, remotePort);
                proxyPortConfigs.put(listenerPort, listener);
            }
        }
    } // load

    public void save() throws Exception
    {
        //don't do anything
    } // save

    public Map<Integer, ProxyPortConfig> getProxyPortConfigs()
    {
        return proxyPortConfigs;
    }

    public void setProxyPortConfigs(Map<Integer, ProxyPortConfig> proxyPortConfigs)
    {
        this.proxyPortConfigs = proxyPortConfigs;
    }

    public Set<String> getWhiteList()
    {
        return whiteList;
    }

    public void setWhiteList(Set<String> whiteList)
    {
        this.whiteList = whiteList;
    }
    
    public boolean isAllowedIP(String ipAddress)
    {
        boolean result = true;
        if(StringUtils.isNotBlank(ipAddress) && whiteList.size() > 0 && !whiteList.contains(ipAddress))
        {
            result = false;
        }
        return result;
    }
} // ConfigProxy
