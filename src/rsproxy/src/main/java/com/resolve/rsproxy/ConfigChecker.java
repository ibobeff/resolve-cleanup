/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.rsproxy;

import java.io.File;

import com.resolve.util.Log;

public class ConfigChecker extends Thread
{
    private final Main main;
    private final File file;
    long timeStamp;
    private long interval = 5000; //in milliseconds
    
    private Object lck = new Object();

    public ConfigChecker(Main main, File file, long interval)
    {
        this.main = main;
        if(file == null || !file.exists())
        {
            throw new RuntimeException("File must be provided");
        }
        this.file = file;
        this.timeStamp = this.file.lastModified();
        if(interval > 0)
        {
            this.interval = interval;
        }
    }
    
    @Override
    public void run()
    {
        try
        {
            Log.log.debug("Config file checker started: " + file);
            while (true)
            {
                if(isFileUpdated())
                {
                    Log.log.debug("File modified: " + file);
                    System.out.println("File modified: " + file);
                    main.reinit();
                }
                else
                {
                    synchronized (lck)
                    {
                        lck.wait(interval);
                    }
                }

            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    private boolean isFileUpdated()
    {
        boolean result = false;
        if(file != null)
        {
            long lastModified = file.lastModified();
            if (this.timeStamp < lastModified)
            {
                this.timeStamp = lastModified;
                result = true;
            }
        }
        return result;
    }
}
