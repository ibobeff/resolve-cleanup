import java.text.SimpleDateFormat;

def result = "";

def help()
{
    println "Usage: BackupIndex <index> <graphdb>\n"
    println "This script will will backup the index and/or graphdb folders in the"
    println "local RSView component."
}

try
{
    def dist = MAIN.getDist();

    if (dist)
    {
        def rsview = MAIN.isBlueprintRSView();
        if (rsview)
        {
            def index = false;
            def graphdb = false;
            def extension = new SimpleDateFormat("yyyyMMdd").format(new Date());
            def compress = new Compress();
            if (binding.variables.containsKey("PARAMS"))
            {
                index = PARAMS["BACKUP_INDEX"];
                graphdb = PARAMS["BACKUP_GRAPHDB"];
            }
            else if (args.length >= 3)
            {
                index = args[1].equalsIgnoreCase("true");
                graphdb = args[2].equalsIgnoreCase("true");
            }
            else
            {
                help();
            }
            if (index)
            {
                def indexFolder = new File(dist + "/tomcat/webapps/resolve/WEB-INF/index");
                def indexBackupZip = new File(dist + "/tomcat/webapps/resolve/WEB-INF/index-" + extension + ".zip");
                if (indexFolder.isDirectory())
                {
                    if (indexBackupZip.exists())
                    {
                        result += "Index Backup Zip " + indexBackupZip.getName() + " already exists, skipping index backup\n";
                        LOG.warn("Index Backup Zip " + indexBackupZip.getAbsolutePath() + " already exists, skipping index backup");
                    }
                    else
                    {
                        compress.zip(indexFolder, indexBackupZip);
                        result += "Index Backup Zip " + indexBackupZip.getName() + " has been created\n";
                        LOG.info("Index Backup Zip " + indexBackupZip.getAbsolutePath() + " has been created");
                    }
                }
                else
                {
                    result += "Index Folder Not found, skipping index backup\n";
                    LOG.warn("Index Folder " + indexFolder.getAbsolutePath() + " Not found, skipping index backup");
                }
            }
            if (graphdb)
            {
                def graphdbFolder = new File(dist + "/tomcat/webapps/resolve/WEB-INF/graphdb");
                def graphdbBackupZip = new File(dist + "/tomcat/webapps/resolve/WEB-INF/graphdb-" + extension + ".zip");
                if (graphdbFolder.isDirectory())
                {
                    if (graphdbBackupZip.exists())
                    {
                        result += "GraphDB Backup Zip " + graphdbBackupZip.getName() + " already exists, skipping graphdb backup\n";
                        LOG.warn("GraphDB Backup Zip " + graphdbBackupZip.getAbsolutePath() + " already exists, skipping graphdb backup");
                    }
                    else
                    {
                        compress.zip(graphdbFolder, graphdbBackupZip);
                        result += "GraphDB Backup Zip " + graphdbBackupZip.getName() + " has been created\n";
                        LOG.info("GraphDB Backup Zip " + graphdbBackupZip.getAbsolutePath() + " has been created");
                    }
                }
                else
                {
                    result += "GraphDB Folder Not found, skipping graphdb backup\n";
                    LOG.warn("GraphDB Folder " + graphdbFolder.getAbsolutePath() + " Not found, skipping graphdb backup");
                }
            }
        }
        else
        {
            result += "RSView Not installed on this server, skipping backup";
            LOG.info("RSView Not installed on this server, skipping backup");
        }
    }
    else
    {
        result += "Blueprint Has Not Been Properly Instantiated. Unable to Determine if RSView is Installed on the System";
        LOG.warn("Blueprint Has Not Been Properly Instantiated. Unable to Determine if RSView is Installed on the System");
    }
}
catch (Exception e)
{
    result += "Unexpected Exception while Gathering Logs: " + e.getMessage();
    LOG.warn("Unexpected Exception while Gathering Logs: " + e.getMessage(), e);
}

return result;
