import java.lang.StringBuilder;
import java.lang.management.*;

return (String)jvmInfo();


static String jvmInfo()
{
    StringBuilder sb = new StringBuilder();

    sb.append("\n");
    sb.append("====== JVM INFO ======");
    sb.append("\n");

    sb.append("# memory: ");
    sb.append(" max: ");
    sb.append(toMb(Runtime.getRuntime().maxMemory()));
    sb.append(", total: ");
    sb.append(toMb(Runtime.getRuntime().totalMemory()));
    sb.append(", free: " + toMb(Runtime.getRuntime().freeMemory()));
    sb.append((int) (Runtime.getRuntime().freeMemory() * 100 / Runtime.getRuntime().totalMemory()));
    sb.append("%");
    sb.append("\n");

    MemoryUsage heap = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
    sb.append("# heap memory: ");
    sb.append(" init: ");
    sb.append(toMb(heap.getInit()));
    sb.append(", max: ");
    sb.append(toMb(heap.getMax()));
    sb.append(", comitted: " + toMb(heap.getCommitted()));
    sb.append(", used: " + toMb(heap.getUsed()));
    sb.append("\n");

    MemoryUsage nonheap = ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage();
    sb.append("# non heap memory: ");
    sb.append(" init:" + toMb(nonheap.getInit()));
    sb.append(", max:" + toMb(nonheap.getMax()));
    sb.append(", comitted: " + toMb(nonheap.getCommitted()));
    sb.append(", used: " + toMb(nonheap.getUsed()));
    sb.append("\n");

    ThreadMXBean thread = ManagementFactory.getThreadMXBean();
    sb.append("# thread peak: "+thread.getPeakThreadCount());
    sb.append(" live: " + thread.getThreadCount());
    sb.append(" daemon: " + thread.getDaemonThreadCount());
    sb.append("\n");

    sb.append("# procs: ");
    sb.append(Runtime.getRuntime().availableProcessors());
    sb.append("\n");

    sb.append("# os info: ");
    sb.append(" arch:" + ManagementFactory.getOperatingSystemMXBean().getArch());
    sb.append(", name:" + ManagementFactory.getOperatingSystemMXBean().getName());
    sb.append(", version:" + ManagementFactory.getOperatingSystemMXBean().getVersion());
    sb.append("\n");

    sb.append("# jvm info: ");
    sb.append(" vendor:" + ManagementFactory.getRuntimeMXBean().getVmVendor());
    sb.append(", name:" + ManagementFactory.getRuntimeMXBean().getVmName());
    sb.append(", version:" + ManagementFactory.getRuntimeMXBean().getVmVersion());
    sb.append("\n");
    sb.append("=======================");
    return sb.toString();
} // jvmInfo

static String toMb(long size)
{
    return size / 1024 / 1024 + "M ";
} // toMb
	
