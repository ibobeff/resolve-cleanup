interval = PARAMS["INTERVAL"];
if (interval != null)
{
    sleep(Integer.parseInt(interval) * 1000);
    return (String)"SUCCESS: wait for "+interval+" secs";
}
return (String)"FAIL: invalid interval value: "+interval;
