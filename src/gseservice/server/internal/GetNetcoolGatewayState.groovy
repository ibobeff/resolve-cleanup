import com.resolve.gateway.NetcoolGateway;

def result = ""
result += "state: "+NetcoolGateway.getStateString() +"\n"
result += "primary: "+NetcoolGateway.primary +"\n"
result += "active: "+NetcoolGateway.active +"\n"
result += "hasBackupOS: "+NetcoolGateway.hasBackupOS +"\n"
result += "heartbeat: "+NetcoolGateway.heartbeat +"\n"
result += "lastHeartbeat: "+(System.currentTimeMillis() - NetcoolGateway.lastHeartbeat) +"\n"
result += "failover: "+NetcoolGateway.failover +"\n"
result += "retryDelay: "+NetcoolGateway.retryDelay +"\n"
result += "reconnectDelay: "+NetcoolGateway.reconnectDelay +"\n"

return (String)result
