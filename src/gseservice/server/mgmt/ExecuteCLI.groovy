import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

def result = "";

def help()
{
    println "Usage: ExecuteCLI <CLI file> [Cassandra Host] [Cassandra Port] [Cassandra Password]"
    println ""
    println "This script will execute the cli contained in the <CLI file> ";
    println "against the Specified Cassandra Host.  If no Host is specified";
    println "the local BLUEPRINT Cassandra configuration will be used.";
    println ""
}
if (binding.variables.containsKey("args") && args.length < 2)
{
    help();
}
else if (binding.variables.containsKey("PARAMS") && !PARAMS["FILENAME"])
{
    result = "INVALID PARAMETERS";
}
else
{
    def os = System.getProperty("os.name");
    def filename = null;
    def host = null;
    def port = null;
    def password = null;

    if (binding.variables.containsKey("args"))
    {
        filename = args[1];
        if (args.length > 4)
        {
            password = args[4];
        }
        if (args.length > 3)
        {
            port = args[3];
            host = args[2];
        }
        else if (args.length > 2)
        {
            if (args[2].matches("\\d+"))
            {
                port = args[2];
            }
            else
            {
                host = args[2];
            }
        }
        else
        {
            def cassandraConfig = MAIN.getConfigCassandra();
            host = cassandraConfig.getRpcAddress();
            port = cassandraConfig.getRpcPort();
            password = cassandraConfig.getCassandraPassword();
        }
    }
    else
    {
        filename = PARAMS["FILENAME"];
        def cassandraConfig = MAIN.getConfigCassandra();
        host = PARAMS["HOST"] != null ? PARAMS["HOST"] : cassandraConfig.getRpcAddress();
        port = PARAMS["PORT"] != null ? PARAMS["PORT"] : cassandraConfig.getRpcPort();
        password = PARAMS["PASSWORD"] != null ? PARAMS["PASSWORD"] : cassandraConfig.getCassandraPassword();
    }

    byte[] cliFileBytes;
    if (!filename.matches("^(?:[\\\\/]).*|(?:[A-Za-z](?=:\\\\)).*"))
    {
        tmpFilename = "rsmgmt/file/" + filename;
        def file = new File(tmpFilename);
        if (!file.isFile())
        {
            file = new File(tmpFilename + ".cli");

            if (!file.isFile())
            {
                tmpFilename = "cassandra/conf/" + filename;
            }
        }
        filename = tmpFilename;
    }
    def file = new File(filename);
    if (!file.isFile())
    {
        file = new File(filename + ".cli");
    }
    if (file.isFile())
    {
        LOG.debug("Adding show cluster name; command");
        cliFileBytes = new byte[file.length()];
        FileInputStream fis = new FileInputStream(file);
        fis.read(cliFileBytes);
        fis.close();

        FileOutputStream fos = new FileOutputStream(file, true);
        fos.write("\n\nshow cluster name;".getBytes());
        fos.close();
        try
        {
            if (host)
            {
                def cmd = "cassandra/bin/cassandra-cli"
                if (os.contains("Win"))
                {
                    cmd = "cmd /c " + cmd.replaceAll("/","\\\\") + ".bat";
                }
                cmd += " -host " + host;
                if (port)
                {
                    cmd += " -port " + port;
                }
                if (password)
                {
                    cmd += " -u admin -pw " + password;
                }
                cmd += " -f \"" + file.getAbsolutePath() + "\"";

                LOG.info("Execute Cassandra " + file.getAbsolutePath() + " script");
                LOG.debug("Execute Cassandra Command: " + cmd.replaceFirst("-pw " + password, "-pw ****"));

                print "Attempting to Execute script";
                def numTry = 0;
                def active = false;
                while (!active && numTry < 19)
                {
                    if (numTry > 0)
                    {
                        Thread.sleep(20000);
                    }
                    print ".";
                    def sout = new StringBuffer();
                    def proc = cmd.execute();
                    proc.consumeProcessOutput(sout, sout);
                    proc.waitFor();
                    def info = sout.toString();
                    LOG.debug("Cassandra Info Output: " + info);

                    def successMatcher = info =~ /resolve$/
                    if (info.contains("Connection refused"))
                    {
                        LOG.error("Failed to Connect to Cassandra");
                    }
                    else if (successMatcher.find())
                    {
                        active = true;
                    }
                    numTry++;
                }

                if (active)
                {
                    println "CLI File " + file.getName() + " executed";
                    result = "CLI File " + file.getName() + " executed";
                    LOG.warn("CLI File " + file.getAbsolutePath() + " executed");
                }
                else
                {
                    println "Failed to Execute CLI File " + file.getName();
                    result = "Failed to Execute CLI File " + file.getName();
                    LOG.error("Failed to Execute CLI File " + file.getName());
                }
            }
            else
            {
                println "No Host Specified or Configured";
                result = "No Host Specified or Configured";
                LOG.warn("No Host Specified or Configured");
            }
        }
        catch (Exception e)
        {
            println "Unexpected Exception: " + e.getMessage();
            result = "Unexpected Exception: " + e.getMessage();
            LOG.warn("Unexpected Exception: " + e.getMessage(), e);
        }
        finally
        {
            if (cliFileBytes)
            {
                try
                {
                    fos = new FileOutputStream(file);
                    fos.write(cliFileBytes);
                    fos.close();
                }
                catch (Exception e)
                {
                    LOG.warn("Failed to return CLI file to original state", e);
                }
            }
        }
    }
    else
    {
        println "Cannot Find CLI File: " + filename;
        result = "Cannot Find CLI File: " + filename;
        LOG.warn("Cannot Find CLI File: " + file.getAbsolutePath());
    }
}

return result;
