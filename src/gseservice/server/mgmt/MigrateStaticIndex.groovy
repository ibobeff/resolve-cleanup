import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequestBuilder;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesResponse;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.bulk.byscroll.BulkByScrollResponse;
import org.elasticsearch.common.bytes.BytesArray;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.ReindexAction;
import org.elasticsearch.index.reindex.ReindexRequestBuilder;
import org.elasticsearch.index.reindex.remote.RemoteInfo;

import com.resolve.util.RESTUtils;
import com.resolve.util.StringUtils;

import groovy.json.JsonSlurper;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.concurrent.ExecutionException;

scrollSize=50;

def help()
{
    println "Usage: MigrateStaticIndex <ToServerList> <ToServerPort> <ToServerClustername> <FromServer> <FromServerPort> [ScrollSize]";
    println "";
    println "This script will migrate all static data from one ES cluster to another.  This is meant for migrating data from older ES";
    println "instances to a newer ES cluster";
    println "Inputs:";
    println "    ToServerList: Comma separated list of all servers (IP/Host) in the new cluster";
    println "    ToServerPort: Port to connect to new server on, this should be the TCP port (i.e. 9300 or equivalent)";
    println "    ToServerClustername: Name of the ES Cluster being conected to (e.g. RESOLVE)";
    println "    FromServer: IP/Host of the server to copy data from, only one server in this cluster should be listed";
    println "    FromServerPort: Port to connect to on old server cluster, this should be the HTTP port (i.e. 9200 or equivalent)";
    println "    ScrollSize: Amount of records to copy at a time to ES Cluster being connected to, default 50";
}


if ((!binding.variables.containsKey("args") || args.length < 6))
{
    help();
}
else
{
    LOG.info("Migrating Data Passed Arguments: " + args);

    def i=0;
    if (args[0] == "MigrateStaticIndex.groovy")
    {
        i++;
    }
    def servers = args[i].split(",");
    i++;

    def serverPort = args[i];
    i++;

    if (!serverPort.matches("\\d+"))
    {
        LOG.error("Invalid Server Port: " + serverPort);
        System.exit(-1);
    }
    else
    {
        serverPort = serverPort.toInteger();
    }

    def cluster = args[i];
    i++;

    def oldServer = args[i];
    i++;

    def oldServerPort = args[i];
    i++;

    if (!oldServerPort.matches("\\d+"))
    {
        println "Invalid Server Port: " + oldServerPort;
        LOG.error("Invalid Server Port: " + oldServerPort);
        System.exit(-1);
    }
    else
    {
        oldServerPort = oldServerPort.toInteger();
    }

    if (args.length >= i && args[i].matches("\\d+"))
    {
        scrollSize = args[i].toInteger();
        LOG.info("Setting new Scroll Size: " + scrollSize);
    }

    def shards = BLUEPRINT.get("rssearch.shards");
    def replicas = servers.size() - 1;

    def client = null;

    try 
    {
        def settingsBuilder = Settings.builder().put("cluster.name", cluster);
        settingsBuilder.put("client.transport.ping_timeout", TimeValue.timeValueSeconds(20));
        def settings = settingsBuilder.build();

        // a Map with value as serverip:port
        TransportAddress[] transportAddresses = new InetSocketTransportAddress[servers.size()];

        i=0;
        for (def server in servers)
        {
            def transportAddress = new InetSocketTransportAddress(InetAddress.getByName(server), serverPort);
            transportAddresses[i++] = transportAddress;
        }

        client = new PreBuiltTransportClient(settings).addTransportAddresses(transportAddresses);

        LOG.info("Migrating ES data starts...");
        println("Migrating ES data starts...");
        def url = "http://" + oldServer + ":" + oldServerPort + "/_cat/indices";
        def indexList = RESTUtils.get(url, null, null);
        
        def indexLines = indexList.split("\\r?\\n");
        def oldIndexes = new ArrayList<String>();
        for (def line in indexLines)
        {	
            def splited = line.split("\\s+");
            if (StringUtils.isNotEmpty(splited[2]))
            {
                oldIndexes.add(splited[2]);
            }
        }
        
        for (def idxName : oldIndexes)
        {
            try
            {
                if (idxName.matches(".*\\d{8}") || idxName.equals("worksheetdata"))
                {
                    LOG.info("Index " + idxName + " Not a Static Index, Skipping");
                }
                else
                {
                    LOG.info("Migrating data on index " + idxName + " from ES server: " + oldServer + ", port: " + oldServerPort);
                    if (!isIndexExists(client, idxName))
                    {
                        def commonSettingsJson = readJson("common_settings.json");

                        //index alias
                        Map<String, String> mappings = new HashMap<String, String>();
                        if (idxName.equals("actiontask"))
                        {
                            mappings.put("actiontask", readJson("action_tasks_mappings.json"));
                        }
                        else if (idxName.equals("dictionary"))
                        {
                            mappings.put("dictionary", readJson("dictionary_mappings.json"));
                        }
                        else if (idxName.equals("executestate"))
                        {
                            mappings.put("executestate", readJson("execute_states_mappings.json"));
                        }
                        else if (idxName.equals("globalquery"))
                        {
                            mappings.put("globalquery", readJson("global_query_mappings.json"));
                        }
                        else if (idxName.equals("metriccpu"))
                        {
                            mappings.put("metriccpu", readJson("metric_cpu_mappings.json"));
                        }
                        else if (idxName.equals("metricdb"))
                        {
                            mappings.put("metricdb", readJson("metric_database_mappings.json"));
                        }
                        else if (idxName.equals("metricjmsqueue"))
                        {
                            mappings.put("metricjmsqueue", readJson("metric_jmsqueue_mappings.json"));
                        }
                        else if (idxName.equals("metricjvm"))
                        {
                            mappings.put("metricjvm", readJson("metric_jvm_mappings.json"));
                        }
                        else if (idxName.equals("metriclatency"))
                        {
                            mappings.put("metriclatency", readJson("metric_latency_mappings.json"));
                        }
                        else if (idxName.equals("metricrunbook"))
                        {
                            mappings.put("metricrunbook", readJson("metric_runbook_mappings.json"));
                        }
                        else if (idxName.equals("metrictimer"))
                        {
                            mappings.put("metrictimer", readJson("metric_timer_mappings.json"));
                        }
                        else if (idxName.equals("metrictransaction"))
                        {
                            mappings.put("metrictransaction", readJson("metric_transaction_mappings.json"));
                        }
                        else if (idxName.equals("metricusers"))
                        {
                            mappings.put("metricusers", readJson("metric_users_mappings.json"));
                        }
                        else if (idxName.equals("pbactivity"))
                        {
                            mappings.put("pbactivity", readJson("pb_activity_mappings.json"));
                        }
                        else if (idxName.equals("pbartifacts"))
                        {
                            mappings.put("pbartifacts", readJson("pb_artifacts_mappings.json"));
                        }
                        else if (idxName.equals("pbattachment"))
                        {
                            mappings.put("pbattachment", readJson("pb_attachment_mappings.json"));
                        }
                        else if (idxName.equals("pbauditlog"))
                        {
                            mappings.put("pbauditlog", readJson("pb_auditlog_mappings.json"));
                        }
                        else if (idxName.equals("pbnotes"))
                        {
                            mappings.put("pbnotes", readJson("pb_notes_mappings.json"));
                        }
                        else if(idxName.equals("searchquery"))
                        {
                            mappings.put("documentquery", suggestionMapping("documentquery", "documentSuggest"));
                            mappings.put("socialquery", suggestionMapping("socialquery", "socialSuggest"));
                            mappings.put("automationquery", suggestionMapping("automationquery", "automationSuggest"));
                            mappings.put("wikiquery", suggestionMapping("wikiquery", "wikiSuggest"));
                        }
                        else if (idxName.equals("socialpost"))
                        {
                            mappings.put("socialpost", readJson("social_posts_mappings.json"));
                            mappings.put("socialcomment", readJson("social_comments_mappings.json"));
                        }
                        else if (idxName.equals("systempost"))
                        {
                            mappings.put("systempost", readJson("system_posts_mappings.json"));
                            mappings.put("systemcomment", readJson("system_comments_mappings.json"));
                        }
                        else if (idxName.equals("tag"))
                        {
                            mappings.put("tag", readJson("tags_mappings.json"));
                        }
                        else if (idxName.equals("userquery"))
                        {
                            mappings.put("userquery", readJson("user_query_mappings.json"));
                        }
                        else if (idxName.equals("wikidocument"))
                        {
                            mappings.put("wikidocument", readJson("wiki_documents_mappings.json"));
                            mappings.put("wikiattachment", readJson("wiki_attachments_mappings.json"));
                        }
                        else if (idxName.equals("executionsummaryarchive"))
                        {
                            mappings.put("executionsummaryarchive", readJson("execution_summary_archive_mappings.json"));
                        }
                        else if (idxName.equals("security-incident"))
                        {
                            mappings.put("security-incident", readJson("security-incident_mappings.json"));
                        }
                        
                        else
                        {
                            LOG.error("Unkown Index: " + idxName);
                        }

                        createIndex(client, idxName, commonSettingsJson, mappings, shards, replicas);

                        if (idxName.equals("executionsummaryarchive"))
                        {
                            IndicesAliasesRequestBuilder executionSummaryAlias = client.admin().indices().prepareAliases();
                            executionSummaryAlias.addAlias("executionsummaryarchive", "executionsummaryalias");
                            IndicesAliasesResponse indicesAliasesResponse = executionSummaryAlias.execute().actionGet();
                            if(indicesAliasesResponse.isAcknowledged())
                            {
                                LOG.info("Successfully added index " + idxName + " to alias " + "executionsummaryalias");
                            }
                            else
                            {
                                LOG.error("Alias Creation for Index " + idxName + " to alias executionsummaryalias Failed");
                                println("Alias Creation for Index " + idxName + " to alias executionsummaryalias Failed");
                            }
                        }
        

                    }
                    copyData(client, idxName, oldServer, oldServerPort);
                }
            }
            catch (Exception ex)
            {
                LOG.error("Error migrating data from index " + idxName, ex);
                println("Error migrating data from index " + idxName);
                ex.printStackTrace();
            }
        }
        
        LOG.info("Migrating ES data is done");
        println("Migrating ES data is done");
    } 
    catch (Exception ex)
    {
        LOG.error(ex,ex);
        ex.printStackTrace();
        System.exit(-1);
    }
}

def readJson(def fileName)
{
    def stream = this.getClass().getResourceAsStream(fileName);
    return StringUtils.toString(stream as InputStream, "UTF-8");
}

def isIndexExists(def client, def indexName)
{
    boolean result = false;

    try
    {
        IndicesExistsResponse response = client.admin().indices().exists(new IndicesExistsRequest(indexName)).get();
        if (response.isExists())
        {
            result = true;
        }
    }
    catch (InterruptedException e)
    {
        LOG.error(e.getMessage(), e);
        ex.printStackTrace();
    }
    catch (ExecutionException e)
    {
        LOG.error(e.getMessage(), e);
        ex.printStackTrace();
    }
    return result;
}

def getTotalCount(def indexName, def serverHost, def serverPort)
{
    def count = -1;
    def url = "http://" + serverHost + ":" + serverPort + "/" + indexName + "/_count";

    def httpClient = HttpClientBuilder.create().build();
    def request = new HttpPost(url);
    def response = httpClient.execute(request);

    def statusCode = response.getStatusLine().getStatusCode();
    def countResult = StringUtils.toString(response.getEntity().getContent() as InputStream, "UTF-8")
    LOG.info("Count Result: " + countResult);
    if (statusCode != 200)
    {
        LOG.error("Failed to Query Total - Returned Status is " + statusCode);
    }
    else
    {
        def json = (new JsonSlurper()).parseText(countResult);
        count = json.count;
    }
    return count;
}


def createIndex(def client, def indexName, def settingsJson, def mappings, def shards, def replicas)
{
    def createIndexRequestBuilder = client.admin().indices().prepareCreate(indexName);

    def builder = Settings.builder().put("number_of_shards", shards).put("number_of_replicas", replicas).put("max_result_window", 5000000).put("priority", 0);
    Settings indexSettings = null;

    if (StringUtils.isBlank(settingsJson))
    {
        indexSettings = builder.build();
    }
    else
    {
        indexSettings = builder.loadFromSource(settingsJson).build();
    }

    if (mappings != null && mappings.size() > 0)
    {
        for(String documentType : mappings.keySet())
        {
            if (StringUtils.isNotBlank(documentType))
            {
                createIndexRequestBuilder.addMapping(documentType, mappings.get(documentType));
            }
            else
            {
                createIndexRequestBuilder.addMapping(indexName, mappings.get(documentType));
            }
        }
    }

    createIndexRequestBuilder.setSettings(indexSettings);
    CreateIndexResponse createResponse = createIndexRequestBuilder.execute().actionGet();
    if (createResponse.isAcknowledged())
    {
        LOG.info("Successfully created index " + indexName);
        println("Successfully created index " + indexName);
    }

    LOG.info("Index created: " + indexName + ", number_of_shards: " + shards + ", number_of_replicas: " + replicas);
    println("Index created: " + indexName + ", number_of_shards: " + shards + ", number_of_replicas: " + replicas);
    LOG.debug("Other settings: " + settingsJson);
    LOG.debug("Mappings: " + mappings);
    if (mappings != null && mappings.size() > 0)
    {
        for(String documentType : mappings.keySet())
        {
            LOG.info("Document type : " + documentType);
            LOG.debug("Mappings : " + mappings.get(documentType));
            println("Document type : " + documentType);
            //println("Mappings : " + mappings.get(documentType));
        }
    }
}

def suggestionMapping(def documentType, def suggestFieldName)
    {
        def result = new StringBuilder();

        result.append("{");
        result.append("\"" + documentType + "\" :");
        result.append("{");
        result.append("\"_all\" :");
        result.append("{");
        result.append("\"enabled\" : false");
        result.append("},");
        result.append("\"properties\":");
        result.append("{");
        result.append("\"" + suggestFieldName + "\":");
        result.append("{");
        result.append("\"type\" : \"completion\",");
        result.append("\"analyzer\" : \"simple\",");
        result.append("\"search_analyzer\" : \"simple\",");
        result.append("\"preserve_position_increments\": false,");
        result.append("\"preserve_separators\": false");
        result.append("},");
        result.append("\"weight\":");
        result.append("{");
        result.append("\"type\":\"integer\",");
        result.append("\"null_value\":0");
        result.append("},");
        result.append("\"selectCount\":");
        result.append("{");
        result.append("\"type\":\"integer\",");
        result.append("\"null_value\":0");
        result.append("},");
        result.append("\"sysUpdatedOn\":");
        result.append("{");
        result.append("\"type\":\"long\",");
        result.append("\"null_value\":0");
        result.append("},");
        result.append("\"suggestion\":");
        result.append("{");
        result.append("\"type\":\"string\",");
        result.append("\"analyzer\" : \"keyword_lowercase_analyzer\"");
        result.append("},");
        result.append("\"sourceIds\":");
        result.append("{");
        result.append("\"type\":\"string\",");
        result.append("\"index\" : \"not_analyzed\"");
        result.append("}");
        result.append("}");
        result.append("}");
        result.append("}");

        return result.toString();
    }

def copyData(def client, def idxName, def oldServer, def oldServerPort)
{
    def dataquery = "{\"match_all\":{}}";
    def idxAry = new String[1];
    idxAry[0] = idxName;
    ReindexRequestBuilder builder = ReindexAction.INSTANCE.newRequestBuilder(client).source(idxAry).destination(idxName);
    RemoteInfo ri = new RemoteInfo("http", oldServer, oldServerPort, new BytesArray(dataquery), null, null, new HashMap(), RemoteInfo.DEFAULT_SOCKET_TIMEOUT, RemoteInfo.DEFAULT_CONNECT_TIMEOUT );
    builder.setRemoteInfo(ri);
    builder.source().setScroll(new TimeValue(1200*1000)).setSize(scrollSize);

    long totalFrom = getTotalCount(idxName, oldServer, oldServerPort);
    LOG.info("Count from Source: " + totalFrom);
    println("Count from Source: " + totalFrom);

    if (totalFrom == 0)
    {
        LOG.info("Nothing to copy, moving on");
    }
    else
    {
        BulkByScrollResponse response = builder.get();
        long totalTo = response.getCreated();
        LOG.info("Count from Reindex Response: " + totalTo);
        println("Count from Reindex Response: " + totalTo);
        if (totalFrom != totalTo)
        {
            LOG.warn("Possible Error with Data Migration of " + idxName + ". Counts from before and after do not match");
        }
    }
}
