def help()
{
    println "Usage: status <Resolve Component>";
    println "Valid Components: ALL, RSACTION, RSCONSOLE, RSCONTROL, RSMQ, RSREMOTE, RSVIEW, TOMCAT\n";
    println "This script will query the Process ID of the specified Resolve Component";
    println "ALL can be used to query all components installed on this system";
}
if (args.length < 2)
{
    help();
    def cmd;
    def os = System.getProperty("os.name");
    def sb = new StringBuffer();
    if (os.contains("Win"))
    {
        cmd = "bin/status.bat";
    }
    else
    {
        cmd = "bin/status.sh";
    }
    
    for (i=1; i<args.length; i++)
    {
        cmd += " " + args[i];
    }
    LOG.warn("Running status command: " + cmd);
            
    proc = cmd.execute();
    proc.consumeProcessOutput(sb, sb);
    proc.waitFor();
    MAIN.println(sb.toString());
    LOG.info("Start Output: " + sb.toString());
}
else
{
    println status();
}
return null;
