def levels = ["TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL"];
def help()
{
    println "Usage: Log [LEVEL] <message>\n";
    println "";
    println "This script will log out the supplied message, the default value";
    println "level is INFO";
    println ""
}
if ((!binding.variables.containsKey("args") || args.length < 2) && !binding.variables.containsKey("PARAMS"))
{
    help();
}
else
{
    def level = "INFO";
    def message;
    if (binding.variables.containsKey("PARAMS"))
    {
        if (PARAMS["LEVEL"])
        {
            if (levels.contains(PARAMS["LEVEL"]))
            {
                level = PARAMS["LEVEL"];
            }
            else
            {
                Log.log.warn("Invalid Log Level " + PARAMS["LEVEL"] + ", using INFO");
            }
        }
        if (PARAMS["MESSAGE"])
        {
            message = PARAMS["MESSAGE"];
        }
    }
    else
    {
        def i=0;
        if (args[0] == "Log.groovy")
        {
            i++;
        }
        if (levels.contains(args[i]))
        {
            level = args[i];
            i++;
        }
        message = "";
        for (; i<args.length; i++)
        {
            message += " " + args[i];
        }
    }
    if (message)
    {
        message = message.trim();
        switch (level)
        {
            case "TRACE":
                LOG.trace(message);
                break;
            case "DEBUG":
                LOG.debug(message);
                break;
            case "WARN":
                LOG.warn(message);
                break;
            case "ERROR":
                LOG.error(message);
                break;
            case "FATAL":
                LOG.fatal(message);
                break;
            case "INFO":
            default:
                LOG.info(message);
                break;
        }
    }
}

return null;
