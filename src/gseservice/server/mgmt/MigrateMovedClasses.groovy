import groovy.sql.Sql;
import java.sql.Clob;

def help()
{
    println "\nUsage: MigrateMovedClasses";
    println ""
    println "This Script is used to upgrade action task/gateway scripts for Resolve classes moved from one package to another";
	println "Moved classes are retrieved from rsmgmt/config/moved_classes.csv";
}

def movedClassesFilename = "rsmgmt/config/moved_classes.csv"
def movedClassesFile = new File(movedClassesFilename)
	
movedClassesFile.eachLine { line ->
	def entries = line.split(",")
	def oldFqdn = entries[0]
	def newFqdn = entries[1]
	
	if(!oldFqdn?.trim()) {
		return;
	}
	
	println "Migrating from class: " + oldFqdn + " to class: " + newFqdn
	def conn;
	try
	{
		if (!MAIN.isDBConnected())
	    {
			println "\nConnecting to connection1";
	        MAIN.executeCommand("DBConnect connection1");
	    }
		conn = new Sql(DB);
		
		DatabaseMetaData md = conn.connection.metaData;
		ResultSet rs = md.getTables(null, null, "%", null);
		def tableNames = ["resolve_action_invoc_options","resolve_preprocess","resolve_assess","resolve_parser","resolve_sys_script","wikidoc"]
		while (rs.next()) {
			def tableName = rs.getString(3);
			if(tableNames.contains(tableName) || tableName.contains("_filter")) {
				def colName = "u_script";
				if(tableName.equals("wikidoc"))
					colName = "u_content"
				else if (tableName.equals("resolve_action_invoc_options"))
					colName = "u_value"
				def cols = md.getColumns(null, null, tableName, null)
				def updateTable = false;
				
				//custom tables may contain filter in the name so check for correct column Name
				while (cols.next()) {
					updateTable = colName.equals(cols.getString("COLUMN_NAME"));
					if(updateTable)
						break;
				}
				if(updateTable){
					def updateSql = "UPDATE " + tableName + " SET " + colName + " = REPLACE(" + colName + ", '" + oldFqdn + "', '" + newFqdn + "')";
					conn.executeUpdate(updateSql);
				}
			}
		}
		
		println "Finished migrating from class: " + oldFqdn + " to class: " + newFqdn
			
	} 
	catch (Exception e)
	{
		println "WARNING!!! Unexpected Exception occurred: " + e.getMessage();
		LOG.error("Unexpected exception during migration of classes", e);
	}
}

return null;
