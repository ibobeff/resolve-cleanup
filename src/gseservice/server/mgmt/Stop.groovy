def help()
{
    println "Usage: stop <Resolve Component...>";
    println "Valid Components: ALL, RSCONTROL, RSMQ, RSREMOTE, RSVIEW";
    println "Invalid Components: RSCONSOLE\n";
    println "This script will stop the specified Resolve Components";
    println "ALL can be used to stop all components (except RSCONSOLE) installed on this system"
}

if (args.length < 2)
{
    help();
}
else
{
    def cmd;
    def os = System.getProperty("os.name");
    def sb = new StringBuffer();
    if (os.contains("Win"))
    {
        cmd = "bin/stop.bat";
    }
    else
    {
        cmd = "bin/stop.sh";
    }
    
    for (i=1; i<args.length; i++)
    {
        cmd += " " + args[i];
    }
    LOG.warn("Running stop command: " + cmd);
            
    proc = cmd.execute();
    proc.consumeProcessOutput(sb, sb);
    proc.waitFor();
    MAIN.println(sb.toString());
    LOG.info("Start Output: " + sb.toString());
}

return null;
