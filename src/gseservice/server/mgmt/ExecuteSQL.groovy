import com.resolve.util.Constants;

def help()
{
    println "Usage: ExecuteSQL <SQL file> [DB Type] [DB User Name] [DB Password] [DB URL]\n"
    println ""
    println "This script will execute the sql contained";
    println "in the <SQL file> against the connected DB";
    println "If no DB is connected connection details can be passed in like below:";
    println "    ExecuteSQL file.sql mysql username jdbc:mysql://localhost:3306";
    println "    ExecuteSQL file.sql mysql username password jdbc:mysql://localhost:3306";
    println ""
}

def delimiter = ";";

if (args.length < 2)
{
    help();
}
else
{
    def statement;
    def filename = args[1];
    if (!filename.matches("^(?:[\\\\/]).*|(?:[A-Za-z](?=:\\\\)).*"))
    {
        filename = "rsmgmt/file/" + filename;
    }
    def file = new File(filename);
    if (!file.isFile())
    {
        file = new File(filename + ".sql");
    }
    if (file.isFile())
    {
        try
        {
            def connected = false;
            if (MAIN.isDBConnected())
            {
                connected = true;
            }
            else if (args.length == 5)
            {
                DB = connect(args[2], args[3], "", args[4]);
                if (DB != null)
                {
                    connected = true;
                }
            }
            else if (args.length == 6)
            {
                DB = connect(args[2], args[3], args[4], args[5]);
                if (DB != null)
                {
                    connected = true;
                }
            }
            if (connected)
            {
                statement = DB.createStatement();

                byte[] sqlBytes = new byte[file.length()];
                fis = new FileInputStream(file);
                fis.read(sqlBytes);
                fis.close();
                def statements = new String(sqlBytes);
                statements = statements.replaceAll("/\\*(?!!).*\\*/", "");
                statements = statements.replaceAll("#.*\n", "");
                statements = statements.replaceAll("--.*\n", "");

                def currIdx = 0;

                while (currIdx != statements.length())
                {
                    if (currIdx != 0)
                    {
                        currIdx += delimiter.length();
                    }
                    def nextIdx = statements.indexOf(delimiter, currIdx) != -1 ? statements.indexOf(delimiter, currIdx) : statements.length();
                    def sql = statements.substring(currIdx, nextIdx);

                    sql = sql.trim();
                    if (sql && sql.toLowerCase().startsWith("delimiter"))
                    {
                        def delimMatcher = sql =~ /(?mi)^delimiter (.+)$/;
                        if (delimMatcher.find())
                        {
                            delimiter = delimMatcher.group(1);
                            currIdx = statements.indexOf(delimiter, currIdx) + delimiter.length();
                            nextIdx = statements.indexOf(delimiter, currIdx) != -1 ? statements.indexOf(delimiter, currIdx) : statements.length();
                            sql = statements.substring(currIdx, nextIdx);
                            sql = sql - "delimiter $delimiter";
                    
                            sql = sql.trim();
                        }
                    }

                    if (sql && !sql.toLowerCase().startsWith("connect"))
                    {
                        if (sql.toLowerCase().contains("begin") && !sql.endsWith(";"))
                        {
                            sql = sql + ";";
                        }
                        LOG.warn("Execute SQL: " + sql);
                        try
                        {
                            if (statement.execute(sql))
                            {
                                def rs = statement.getResultSet();
                                def metaData = rs.getMetaData();
                                def numColumns = metaData.getColumnCount();
                                def columnNames = new String[numColumns];
                                for (i in 1..numColumns)
                                {
                                    columnNames[i-1] = metaData.getColumnName(i);
                                }
                                def resultCount = "1";
                                while (rs.next())
                                {
                                    def result = resultCount++ + ": ";
                                    for (i in 1..numColumns)
                                    {
                                        if (i > 1)
                                        {
                                            print ", ";
                                        }
                                        result += columnNames[i-1] + "=";
                                        result += rs.getString(i);
                                    }
                                    println result;
                                    LOG.info(result);
                                }
                            }
                        }
                        catch (SQLException sqle)
                        {
                            println "WARNING!!! Failed to Execute SQL: " + sqle.getMessage();
                            LOG.error("Failed to Execute SQL", sqle);
                        }
                    }
                    currIdx = nextIdx;
                }
                println "SQL File " + file.getAbsolutePath() + " executed";
                LOG.warn("SQL File " + file.getAbsolutePath() + " executed");
            }
            else
            {
                println "No Database Connection Detected";
                LOG.warn("No Database Connection Detected");
            }
            
        }
        catch (Exception e)
        {
            println "Unexpected Exception: " + e.getMessage();
            LOG.warn("Unexpected Exception: " + e.getMessage(), e);
        }
        finally
        {
            statement?.close();
            if (!MAIN.isDBConnected())
            {
                DB?.close();
            }
        }
    }
    else
    {
        println "Cannot Find SQL File: " + filename;
        LOG.warn("Cannot Find SQL File: " + file.getAbsolutePath());
    }
}

return null;

def connect(String dbType, String username, String password, String url)
{
    def connection = null;
    def driverClass = null;
    if (dbType.equalsIgnoreCase("oracle"))
    {
        driverClass = Constants.ORACLEDRIVER;
    }
    else if (dbType.equalsIgnoreCase("mysql"))
    {
        driverClass = Constants.MYSQLDRIVER;
    }
    if (driverClass != null)
    {
        try
        {
            Class.forName(driverClass);
            connection = DriverManager.getConnection(url, username, password);
            MAIN.println("DB Connection Successful");
            LOG.info("DB Connection Successful");
        }
        catch (SQLException sqle)
        {
            Log.log.error("Failed to Connect to " + dbType + " DB at " + url, sqle);
            def message = sqle.getMessage();
            if (message.contains("No suitable driver"))
            {
                MAIN.println("Connection details are incorrectly formatted");
                LOG.error("Connection details are incorrectly formatted");
            }
            else if (message.contains("Access denied for user") || message.contains("invalid username/password"))
            {
                MAIN.println("Username or Password is Incorrect");
                LOG.error("Username or Password is Incorrect");
            }
            else
            {
                MAIN.println("Unable to connect to Database");
                LOG.error("Unable to connect to Database");
            }
        }
    }
    else
    {
        MAIN.println("DB Type " + dbType + " Not Supported");
        LOG.warn("DB Type " + dbType + " Not Supported");
    }
    return connection;
} // connect
