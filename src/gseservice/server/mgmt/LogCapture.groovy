import java.text.SimpleDateFormat;
import java.util.Date;
import java.net.InetAddress;

import com.resolve.util.Compress;

def result = "";
def extensions = new String[5];
extensions[0] = "xml";
extensions[1] = "cfg";
extensions[2] = "properties";
extensions[3] = "conf";
extensions[4] = "log";
def pid = '';

def help()
{
    println "Usage: LogCapture\n"
    println "This script will will gather all log and config files into a zip in rsmgmt/file";
}

try
{
    def dist = MAIN.getDist();

    if (dist)
    {
        def copyLocation = dist + "/rsmgmt/file/logs";
        def copyLocationFile = new File(copyLocation);
        if (copyLocationFile.exists())
        {
            LOG.info("Removing Old Log Capture: " + copyLocationFile.getAbsolutePath());
            removeFile(copyLocationFile);
        }

        def rsremote = MAIN.isBlueprintRSRemote();
        def os = System.getProperty("os.name");
        if (rsremote)
        {
            def instances = MAIN.getRSRemoteInstances();
            if (instances)
            {
                for (instance in instances)
                {
                    LOG.info("Capturing " + instance + " Logs");
                    //rsremote logs
                    def rsremoteDir = new File(dist + "/${instance}/log");
                    def rsremoteCopyDir = new File(copyLocation + "/${instance}/logs");

                    FileUtils.copyDirectory(rsremoteDir, rsremoteCopyDir);

                    //rsremote configs
                    rsremoteDir = new File(dist + "/${instance}/config");
                    rsremoteCopyDir = new File(copyLocation + "/${instance}/config");

                    FileUtils.copyDirectory(rsremoteDir, rsremoteCopyDir);
                    
                    if (!os.contains("Win"))
                    {
                        //get jstack dump
                        pid = new File(dist + "/${instance}/bin/lock");
                        if(pid.exists()){
                            pid = pid.text;
                            def rsremoteJStack = new File(copyLocation + "/${instance}/jstack.txt");
                            rsremoteJStack.write("${dist}/jdk/bin/java -cp ${dist}/jdk/lib/tools.jar sun.tools.jstack.JStack -l ${pid}".execute().text)
                        }
                    }
                }
            }
        }
        def rscontrol = MAIN.isBlueprintRSControl();
        if (rscontrol)
        {
            LOG.info("Capturing RSControl Logs");
            //rscontrol logs
            def rscontrolDir = new File(dist + "/rscontrol/log");
            def rscontrolCopyDir = new File(copyLocation + "/rscontrol/log");

            FileUtils.copyDirectory(rscontrolDir, rscontrolCopyDir);

            //rscontrol configs
            rscontrolDir = new File(dist + "/rscontrol/config");
            rscontrolCopyDir = new File(copyLocation + "/rscontrol/config");

            FileUtils.copyDirectory(rscontrolDir, rscontrolCopyDir);
            
            if (!os.contains("Win"))
            {
                //get jstack dump
                pid = new File(dist + "/rscontrol/bin/lock");
                if(pid.exists()){
                    pid = pid.text;
                    def rscontrolJStack = new File(copyLocation + "/rscontrol/jstack.txt");
                    rscontrolJStack.write("${dist}/jdk/bin/java -cp ${dist}/jdk/lib/tools.jar sun.tools.jstack.JStack -l ${pid}".execute().text)
                }
            }
        }
        def rsview = MAIN.isBlueprintRSView();
        if (rsview)
        {
            LOG.info("Capturing RSView Logs");
            //rsview logs
            def rsviewDir = new File(dist + "/tomcat/logs");
            def rsviewCopyDir = new File(copyLocation + "/rsview/log");

            FileUtils.copyDirectory(rsviewDir, rsviewCopyDir);

            //tomcat conf
            rsviewDir = new File(dist + "/tomcat/conf");
            rsviewCopyDir = new File(copyLocation + "/rsview/conf");

            FileUtils.copyDirectory(rsviewDir, rsviewCopyDir);

            //resolve WEB-INF configs
            rsviewDir = new File(dist + "/tomcat/webapps/resolve/WEB-INF");

            def rsviewFiles = FileUtils.iterateFiles(rsviewDir, extensions, true);
            for (rsviewFile in rsviewFiles)
            {
                rsviewCopyDir = rsviewFile.getParentFile().getAbsolutePath().replaceAll("\\\\", "/");
                rsviewCopyDir = rsviewCopyDir.replaceAll(dist.replaceAll("\\\\", "/")
                    + "/tomcat/webapps/resolve", copyLocation.replaceAll("\\\\", "/") + "/rsview");
                rsviewCopyDir = new File(rsviewCopyDir);
                FileUtils.copyFileToDirectory(rsviewFile, rsviewCopyDir);
            }
            
            if (!os.contains("Win"))
            {
                //get jstack dump
                pid = new File(dist + "/tomcat/bin/lock");
                if(pid.exists()){
                    pid = pid.text;
                    def tomcatJStack = new File(copyLocation + "/rsview/jstack.txt");
                    tomcatJStack.write("${dist}/jdk/bin/java -cp ${dist}/jdk/lib/tools.jar sun.tools.jstack.JStack -l ${pid}".execute().text)
                }
            }
        }
        def rsmq = MAIN.isBlueprintRSMQ();
        if (rsmq)
        {
            LOG.info("Capturing RSMQ Logs");
            //rabbitmq logs
            def rsmqDir = new File(dist + "/rabbitmq/log");
            def rsmqCopyDir = new File(copyLocation + "/rsmq/log");

            FileUtils.copyDirectory(rsmqDir, rsmqCopyDir);
            
            //rabbitmq configs
            rsmqDir = new File(dist + "/rabbitmq/config");
            rsmqCopyDir = new File(copyLocation + "/rsmq/config");

            FileUtils.copyDirectory(rsmqDir, rsmqCopyDir);
        }
        def rsconsole = MAIN.isBlueprintRSConsole();
        if (rsconsole)
        {
            LOG.info("Capturing RSConsole Logs");
            //rsconsole configs
            def rsconsoleDir = new File(dist + "/rsconsole/config");
            def rsconsoleCopyDir = new File(copyLocation + "/rsconsole/config");

            FileUtils.copyDirectory(rsconsoleDir, rsconsoleCopyDir);

            //rsconsole logs
            rsconsoleDir = new File(dist + "/rsconsole/log");
            rsconsoleCopyDir = new File(copyLocation + "/rsconsole/log");

            FileUtils.copyDirectory(rsconsoleDir, rsconsoleCopyDir);
        }

        def rsmgmt = MAIN.isBlueprintRSMgmt();
        if (rsmgmt)
        {
            LOG.info("Capturing RSMgmt Logs");
            //rsmgmt configs
            def rsmgmtDir = new File(dist + "/rsmgmt/config");
            def rsmgmtCopyDir = new File(copyLocation + "/rsmgmt/config");

            FileUtils.copyDirectory(rsmgmtDir, rsmgmtCopyDir);

            //rsmgmt logs
            rsmgmtDir = new File(dist + "/rsmgmt/log");
            rsmgmtCopyDir = new File(copyLocation + "/rsmgmt/log");

            FileUtils.copyDirectory(rsmgmtDir, rsmgmtCopyDir);
            
            if (!os.contains("Win"))
            {
                //get jstack dump
                pid = new File(dist + "/rsmgmt/bin/lock");
                if(pid.exists()){
                    pid = pid.text;
                    def rsmgmtJStack = new File(copyLocation + "/rsmgmt/jstack.txt");
                    rsmgmtJStack.write("${dist}/jdk/bin/java -cp ${dist}/jdk/lib/tools.jar sun.tools.jstack.JStack -l ${pid}".execute().text)
                }
            }
        }
        def rssearch = MAIN.isBlueprintRSSearch();
        if (rssearch)
        {
            LOG.info("Capturing RSSearch Logs");
            //rssearch configs
            def rssearchDir = new File(dist + "/elasticsearch/config");
            def rssearchCopyDir = new File(copyLocation + "/elasticsearch/config");

            FileUtils.copyDirectory(rssearchDir, rssearchCopyDir);

            //rssearch logs
            rssearchDir = new File(dist + "/elasticsearch/logs");
            rssearchCopyDir = new File(copyLocation + "/elasticsearch/logs");

            FileUtils.copyDirectory(rssearchDir, rssearchCopyDir);
            
            if (!os.contains("Win"))
            {
                //get jstack dump
                pid = new File(dist + "/elasticsearch/bin/lock");
                if(pid.exists()){
                    pid = pid.text;
                    def rssearchJStack = new File(copyLocation + "/elasticsearch/jstack.txt");
                    rssearchJStack.write("${dist}/jdk/bin/java -cp ${dist}/jdk/lib/tools.jar sun.tools.jstack.JStack -l ${pid}".execute().text)
                }
            }
        }

        def rssync = MAIN.isBlueprintRSSync();
        if (rssync)
        {
            LOG.info("Capturing RSSync Logs");
            //rssync configs
            def rssyncDir = new File(dist + "/rssync/config");
            def rssyncCopyDir = new File(copyLocation + "/rssync/config");

            FileUtils.copyDirectory(rssyncDir, rssyncCopyDir);

            //rssync logs
            rssyncDir = new File(dist + "/rssync/log");
            rssyncCopyDir = new File(copyLocation + "/rssync/log");

            FileUtils.copyDirectory(rssyncDir, rssyncCopyDir);
            
            if (!os.contains("Win"))
            {
                //get jstack dump
                pid = new File(dist + "/rssync/bin/lock");
                if(pid.exists()){
                    pid = pid.text;
                    def rssyncJStack = new File(copyLocation + "/rssync/jstack.txt");
                    rssyncJStack.write("${dist}/jdk/bin/java -cp ${dist}/jdk/lib/tools.jar sun.tools.jstack.JStack -l ${pid}".execute().text)
                }
            }
        }
        def rsarchive = MAIN.isBlueprintRSArchive();
        if (rsarchive)
        {
            LOG.info("Capturing RSArchive Logs");
            //rsarchive configs
            def rsarchiveDir = new File(dist + "/rsarchive/config");
            def rsarchiveCopyDir = new File(copyLocation + "/rsarchive/config");

            FileUtils.copyDirectory(rsarchiveDir, rsarchiveCopyDir);

            //rsarchive logs
            rsarchiveDir = new File(dist + "/rsarchive/log");
            rsarchiveCopyDir = new File(copyLocation + "/rsarchive/log");

            FileUtils.copyDirectory(rsarchiveDir, rsarchiveCopyDir);
            
            if (!os.contains("Win"))
            {
                //get jstack dump
                pid = new File(dist + "/rsarchive/bin/lock");
                if(pid.exists()){
                    pid = pid.text;
                    def rsarchiveJStack = new File(copyLocation + "/rsarchive/jstack.txt");
                    rsarchiveJStack.write("${dist}/jdk/bin/java -cp ${dist}/jdk/lib/tools.jar sun.tools.jstack.JStack -l ${pid}".execute().text)
                }
            }
        }
        def logstash = MAIN.isBlueprintLogstash();
        if (logstash)
        {
            LOG.info("Capturing Logstash Logs");
            //logstash configs
            def logstashDir = new File(dist + "/logstash/config");
            def logstashCopyDir = new File(copyLocation + "/logstash/config");

            FileUtils.copyDirectory(logstashDir, logstashCopyDir);

            //logstash logs
            logstashDir = new File(dist + "/logstash/logs");
            logstashCopyDir = new File(copyLocation + "/logstash/logs");

            FileUtils.copyDirectory(logstashDir, logstashCopyDir);
            
            if (!os.contains("Win"))
            {
                //get jstack dump
                pid = new File(dist + "/logstash/bin/lock");
                if(pid.exists()){
                    pid = pid.text;
                    def logstashJStack = new File(copyLocation + "/logstash/jstack.txt");
                    logstashJStack.write("${dist}/jdk/bin/java -cp ${dist}/jdk/lib/tools.jar sun.tools.jstack.JStack -l ${pid}".execute().text)
                }
            }
        }
        
        // find the host name to append to the file
        InetAddress localhost = null;
        def hostName = "";
        
        try
        {
            localhost = InetAddress.getLocalHost();
            hostName = "_" + localhost.getHostName();
            // set to ip address if get local host returns '_localhost'
            if(hostName.equalsIgnoreCase("localhost")){
                hostName = "_" + localhost.getHostAddress();
            }
        } catch(UnknownHostException unknownHostException){
            // appends nothing if an exception occurs
            hostName = "";
        }

        //zip up all gathered logs
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        def zipDirLoc = copyLocation + "/../logs." + sdf.format(new Date()) + hostName + ".zip";
        def zipDir = new File(zipDirLoc);
        def srcDir = new File(copyLocation);

        Compress compress = new Compress();
        compress.zip(srcDir, zipDir);

        result += "Logs Captured:\n" + dist + "/rsmgmt/file/logs." + sdf.format(new Date()) + hostName + ".zip";
    }
    else
    {
        result += "Blueprint Has Not Been Properly Instantiated. Unable to Determine Component Installation";
        LOG.warn("Blueprint Has Not Been Properly Instantiated. Unable to Determine Component Installation");
    }
}
catch (Exception e)
{
    result += "Unexpected Exception while Gathering Logs: " + e.getMessage();
    LOG.warn("Unexpected Exception while Gathering Logs: " + e.getMessage(), e);
}
println result;
return result;

def removeFile(File file)
{
    if (file.exists())
    {
        if (file.isDirectory())
        {
            for (child in file.listFiles())
            {
                removeFile(child);
            }
        }
        if (file.delete())
        {
            LOG.debug("Removed File: " + file.getAbsolutePath());
        }
    }

    return null;
}
