import java.io.File;
import org.apache.commons.io.FileUtils;
import com.resolve.util.Log;

filename = MAIN.getProductHome() + "/config/" + PARAMS["FILENAME"];
File file = new File(filename);
if (file.exists())
{
	result = FileUtils.readFileToString(file, "UTF-8");
}
else
{
    result = "Unable to find file: "+file.getCanonicalPath();
}

return (String)result;
