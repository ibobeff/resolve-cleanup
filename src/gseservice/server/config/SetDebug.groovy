import com.resolve.util.Log;

def result = ""
def module = PARAMS["MODULE"]
if (module)
{
    def value = PARAMS["VALUE"]
    if (module == "SQL" || module == "sql")
    {
	    com.resolve.sql.SQL.debug = (value == "true" || value == "TRUE")
		result = "SQL statement debugging set to: "+value
    }
    else if (module == "NETCOOL" || module == "netcool")
    {
	    com.resolve.gateway.NetcoolGateway.debug = (value == "true" || value == "TRUE")
		result = "NetcoolGateway statement debugging set to: "+value
    }
    else
    {
		result = "ERROR unknown module: "+module
    }
}
else
{
	Log.setDebug()
	result = "Log set to level: DEBUG"
}
Log.log.info(result)
return result
