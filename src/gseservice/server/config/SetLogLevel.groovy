import com.resolve.util.Log;

def level = PARAMS["LEVEL"];
if (!level)
{
    level = "DEBUG";
}
Log.setLevel(level);

return "Log set to level: "+level;
