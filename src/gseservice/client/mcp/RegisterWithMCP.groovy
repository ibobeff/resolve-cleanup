import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import com.resolve.services.util.CSRFUtil;
import com.resolve.services.vo.ResponseDTO;
import org.codehaus.jackson.map.ObjectMapper;
import org.apache.http.util.EntityUtils;

String currentID = "";
HashMap inputParams = new HashMap();
boolean isGettingParameterID = true;

def help()
{
    println "Usage: RegisterWithMCP -uri <MCP URI> -u <USER NAME> -p <PASSWORD> -protocol <PROTOCOL> -port <PORT>";
    println "";
    println "\tUsing RSMgmt, registers this host with the MCP given at <MCP URI>";
    println "\n\t-uri , the URI of the MCP.                     - [REQUIRED]";
    println "\t\tExamples:";
    println "\t\t\tIP address   -   127.0.0.1";
    println "\t\t\tDomain name  -   mcp.servername.com"; 
    println "\n\t-u , the MCP login username                    - [REQUIRED]";
    println "\t\tA username with which to log in to the MCP server to reach the MCP register REST endpoint.";
    println "\n\t-p , the MCP login password                    - [REQUIRED]"; 
    println "\t\tA password with which to log in to the MCP server to reach the MCP register REST endpoint.";
    println "\n\t-protocol , the protocol to access the server. - [OPTIONAL]";
    println "\t\tDefault: http";
    println "\n\t-port , the port number of the MCP server.     - [OPTIONAL]";
    println "\t\tDefault: 8080";
    println "\n-------------------------------";
    println "Example commands:"
    println "\n\tRegisterWithMCP -uri 125.11.126.21 -u admin -p myPassword";
    println "\n\tRegisterWithMCP -uri mcp.servername.com -u admin -p myPassword -protocol https -port 8443";
    println "\n-------------------------------";   
    println "NOTE: If using https, be sure that the run.bat/run.sh file for rsconsole includes the following truststore parameter in JAVA_OPTS: ";
    println "";
    println "-Djavax.net.ssl.trustStore=\"path to keystore file here\"";
    println ""; 
}

String setParameterID(String paramID){
    String c;
    
    if(paramID.equalsIgnoreCase("-uri")){
        c = "URI";
    } else if(paramID.equalsIgnoreCase("-u")){
        c = "USERNAME";
    } else if(paramID.equalsIgnoreCase("-p")){
        c = "PASSWORD";
    } else if(paramID.equalsIgnoreCase("-protocol")){
        c = "PROTOCOL";
    } else if(paramID.equalsIgnoreCase("-port")){
        c = "PORT";
    } 
    
    return c;
}

boolean checkParameters(String arg){
    if(
    arg.equalsIgnoreCase("-uri") || 
    arg.equalsIgnoreCase("-u") || 
    arg.equalsIgnoreCase("-p") || 
    arg.equalsIgnoreCase("-protocol") || 
    arg.equalsIgnoreCase("-port")
    ){
      return true;  
    } else {
      return false;
    }
}

def printErrorMessage(String msg){
    println("\n\n***************************");
    println("ERROR: " + msg);
    println("***************************\n");
}

private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
{
    StringBuilder result = new StringBuilder();
    boolean first = true;

    for (NameValuePair pair : params)
    {
        if (first)
            first = false;
        else
            result.append("&");

        result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
        result.append("=");
        result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
    }

    return result.toString();
}

// ------------ START --------------

// Get arguments

inputParams.put("URI", "none");
inputParams.put("USERNAME", "none");
inputParams.put("PASSWORD", "none");
inputParams.put("PROTOCOL", "http");
inputParams.put("PORT", "8080");

for (int i = 1; i < args.length; i++){
    if(isGettingParameterID){
        currentID = setParameterID(args[i]);
        isGettingParameterID = false;
    } else {
        if(checkParameters(args[i])){
            // No value given for a parameter
            println("INPUT ERROR for " + currentID);
            help();
            return "";
        } else {
            inputParams.put(currentID, args[i]);
            isGettingParameterID = true;
        }
    }
}

if(inputParams.get("URI").equalsIgnoreCase("none")){
    printErrorMessage("Missing -uri parameter");
    help();
    return null; 
}

if(inputParams.get("USERNAME").equalsIgnoreCase("none")){
    printErrorMessage("Missing -u parameter");
    help();
    return null; 
}

if(inputParams.get("PASSWORD").equalsIgnoreCase("none")){
    printErrorMessage("Missing -p parameter");
    help();
    return null; 
}

println("\n\n**********");
println("Registering with MCP using the following parameters:");
println("\tURI: " + inputParams.get("URI"));
println("\tUsername: " + inputParams.get("USERNAME"));
println("\tPassword: " + inputParams.get("PASSWORD"));
println("\tProtocol: " + inputParams.get("PROTOCOL"));
println("\tPort: " + inputParams.get("PORT"));
println("**********");

if(!MAIN.connected){
    MAIN.executeCommand("CONNECT RSMGMT");
}
File blueprintFile = FileUtils.getFile(MAIN.getResolveHome() + "/rsmgmt/config/blueprint.properties");
String blueprint = FileUtils.readFileToString(blueprintFile, "utf8");
HashMap<String, String> params = new HashMap<String, String>();
params.put("blueprint", blueprint);

String mcpURI = inputParams.get("URI");
String mcpPort = inputParams.get("PORT");
String mcpProtocol = inputParams.get("PROTOCOL");
String mcpUsername = inputParams.get("USERNAME");
String mcpPassword = inputParams.get("PASSWORD");

params.put("_username_", mcpUsername);
params.put("_password_", mcpPassword);

String protocol = mcpProtocol.equalsIgnoreCase("http") ? "http://" : mcpProtocol+"://";; 
String port = ":" + mcpPort;

String restURL = protocol + mcpURI + port + "/resolve/service/mcp/register";
String serviceUrl = protocol + mcpURI + port;

println("Connecting to the following REST endpoint: " + restURL + "\n");

try {
	HttpClient httpClient = null;

	if(inputParams.get("PROTOCOL").equalsIgnoreCase("http")){
		httpClient = CSRFUtil.initSession(serviceUrl);
	} else if(inputParams.get("PROTOCOL").equalsIgnoreCase("https")) {
   			httpClient = CSRFUtil.initSslSession(serviceUrl);
	}
		
	if (httpClient != null) { 
		String initialToken = CSRFUtil.fetchInitialToken(httpClient, serviceUrl);
		CSRFUtil.login(httpClient, serviceUrl, mcpUsername, mcpPassword);
		
		String token = CSRFUtil.fetchToken(httpClient, serviceUrl, initialToken);

        HttpPost httppost = new HttpPost(restURL);
    
        httppost.setHeader("X-Requested-With", "XMLHttpRequest");
        httppost.setHeader("Referer", serviceUrl);
        
        String[] tokens = CSRFUtil.parseCSRFToken(token);
        httppost.setHeader(tokens[0], tokens[1]);
    
        ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
        postParameters.add(new BasicNameValuePair("_username_", mcpUsername));
        postParameters.add(new BasicNameValuePair("_password_", mcpPassword));
        postParameters.add(new BasicNameValuePair("blueprint", blueprint));
    
        httppost.setEntity(new UrlEncodedFormEntity(postParameters));
    
        HttpResponse response = httpClient.execute(httppost);

        if (response.getEntity() != null) {
            String jsonStr = EntityUtils.toString(response.getEntity());
            
            ResponseDTO result = new ObjectMapper().readValue(jsonStr, ResponseDTO.class);
            
            if (!result.isSuccess()) {
                String msg = "RSMgmt MCP registration - FAILED: " + result.getMessage();
                println(msg);
                Log.log.error(msg);
            } else { 
                String msg = "RSMgmt MCP registration - SUCCESS: " + response.toString();
                Log.log.info(msg);
                println(msg);
            }
        } else {
            String msg = "RSMgmt MCP registration - SUCCESS: " + response.toString();
            Log.log.info(msg);
            println(msg);
        }
	}
} catch (Exception e) {
    println("RSMgmt MCP registration - FAILED:" + e.getMessage());
}


