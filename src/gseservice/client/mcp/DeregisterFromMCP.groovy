import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.net.ssl.HttpsURLConnection;

import com.resolve.services.util.CSRFUtil;

String currentID = "";
HashMap inputParams = new HashMap();
boolean isGettingParameterID = true;
HashMap<String, String> params = new HashMap<String, String>();


def help()
{
    println "Usage: tDeregisterFromMCP -uri <URI> -u <USER NAME> -p <PASSWORD> -protocol <PROTOCOL> -port <PORT> -cluster <CLUSTER NAME> -hostname <HOST NAME>";
    println "";
    println "\tUsing RSMgmt, deregisters a Resolve cluster and hostname with the MCP given at <MCP URI>";
    println "\n\t-uri , the URI of the MCP.                     - [REQUIRED]";
    println "\t\tExamples:";
    println "\t\t\tIP address   -   127.0.0.1";
    println "\t\t\tDomain name  -   mcp.servername.com"; 
    println "\n\t-u , the MCP login username                    - [REQUIRED]";
    println "\t\tA username with which to log in to the MCP server to reach the MCP deregister REST endpoint.";
    println "\n\t-p , the MCP login password                    - [REQUIRED]"; 
    println "\t\tA password with which to log in to the MCP server to reach the MCP deregister REST endpoint.";
    println "\n\t-cluster , the cluster name                    - [REQUIRED]";
    println "\t\tThe name of the resovlve cluster which contains the resolve instance that you wish to deregister from the MCP";
    println "\n\t-hostname , the host name                    - [OPTIONAL]";
    println "\t\tThe host name of the resolve instance that you wish to deregister from the MCP";
    println "\n\t-protocol , the protocol to access the server. - [OPTIONAL]";
    println "\t\tDefault: http";
    println "\n\t-port , the port number of the MCP server.     - [OPTIONAL]";
    println "\t\tDefault: 8080";
    println "\n-------------------------------";
    println "Example commands:"
    println "\n\tDeregisterFromMCP -uri 125.11.126.21 -u admin -p myPassword -cluster myResolveCluster";
    println "\n\tDeregisterFromMCP -uri mcp.servername.com -u admin -p myPassword -protocol https -port 8443 -hostname myResolveHost -cluster myResolveCluster";
    println "\n-------------------------------";   
    println "NOTE: If using https, be sure that the run.bat/run.sh file for rsconsole includes the following truststore parameter in JAVA_OPTS: ";
    println "";
    println "-Djavax.net.ssl.trustStore=\"path to keystore file here\"";
    println ""; 
}

String setParameterID(String paramID){
    String c;
    
    if(paramID.equalsIgnoreCase("-uri")){
        c = "URI";
    } else if(paramID.equalsIgnoreCase("-u")){
        c = "USERNAME";
    } else if(paramID.equalsIgnoreCase("-p")){
        c = "PASSWORD";
    } else if(paramID.equalsIgnoreCase("-protocol")){
        c = "PROTOCOL";
    } else if(paramID.equalsIgnoreCase("-port")){
        c = "PORT";
    } else if(paramID.equalsIgnoreCase("-cluster")){
        c = "CLUSTER";
    } else if(paramID.equalsIgnoreCase("-hostname")){
        c = "HOSTNAME";
    } 
    
    return c;
}

boolean checkParameters(String arg){
    if(
    arg.equalsIgnoreCase("-uri") || 
    arg.equalsIgnoreCase("-u") || 
    arg.equalsIgnoreCase("-p") || 
    arg.equalsIgnoreCase("-protocol") || 
    arg.equalsIgnoreCase("-port") || 
    arg.equalsIgnoreCase("-cluster") ||
    arg.equalsIgnoreCase("-hostname")
    ){
      return true;  
    } else {
      return false;
    }
}

def printErrorMessage(String msg){
    println("\n\n***************************");
    println("ERROR: " + msg);
    println("***************************\n");
}

private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
{
    StringBuilder result = new StringBuilder();
    boolean first = true;

    for (NameValuePair pair : params)
    {
        if (first)
            first = false;
        else
            result.append("&");

        result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
        result.append("=");
        result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
    }

    return result.toString();
}

// ------------ START --------------

// Get arguments

inputParams.put("URI", "none");
inputParams.put("USERNAME", "none");
inputParams.put("PASSWORD", "none");
inputParams.put("PROTOCOL", "http");
inputParams.put("PORT", "8080");
inputParams.put("CLUSTER", "none");
inputParams.put("HOSTNAME", "");

for (int i = 1; i < args.length; i++){
    if(isGettingParameterID){
        currentID = setParameterID(args[i]);
        isGettingParameterID = false;
    } else {
        if(checkParameters(args[i])){
            // No value given for a parameter
            println("INPUT ERROR for " + currentID);
            help();
            return "";
        } else {
            inputParams.put(currentID, args[i]);
            isGettingParameterID = true;
        }
    }
}

// Check required arguments

if(inputParams.get("URI").equalsIgnoreCase("none")){
    printErrorMessage("Missing -uri parameter");
    help();
    return null; 
}

if(inputParams.get("USERNAME").equalsIgnoreCase("none")){
    printErrorMessage("Missing -u parameter");
    help();
    return null; 
}

if(inputParams.get("PASSWORD").equalsIgnoreCase("none")){
    printErrorMessage("Missing -p parameter");
    help();
    return null; 
}

if(inputParams.get("CLUSTER").equalsIgnoreCase("none")){
    printErrorMessage("Missing -cluster parameter");
    help();
    return null; 
}

println("\n\n**********");
println("Deregistering from MCP using the following parameters:");
println("\tURI: " + inputParams.get("URI"));
println("\tCluster: " + inputParams.get("CLUSTER"));
println("\tHostname: " + inputParams.get("HOSTNAME"));
println("\tUsername: " + inputParams.get("USERNAME"));
println("\tPassword: " + inputParams.get("PASSWORD"));
println("\tProtocol: " + inputParams.get("PROTOCOL"));
println("\tPort: " + inputParams.get("PORT"));
println("**********");

if(!MAIN.connected){
    MAIN.executeCommand("CONNECT RSMGMT");
}

String mcpURI = inputParams.get("URI");
String mcpPort = inputParams.get("PORT");
String mcpProtocol = inputParams.get("PROTOCOL");
String mcpUsername = inputParams.get("USERNAME");
String mcpPassword = inputParams.get("PASSWORD");
String cluster = inputParams.get("CLUSTER");
String hostname = inputParams.get("HOSTNAME");


params.put("_username_", mcpUsername);
params.put("_password_", mcpPassword);

String protocol = mcpProtocol.equalsIgnoreCase("http") ? "http://" : mcpProtocol+"://";; 
String port = ":" + mcpPort;

String restURL = protocol + mcpURI + port + "/resolve/service/mcp/deregister";
String serviceUrl = protocol + mcpURI + port;

println("Connecting to the following REST endpoint: " + restURL + "\n");

try
{
    HttpClient httpClient = null;
    
	if(inputParams.get("PROTOCOL").equalsIgnoreCase("http")){
		httpClient = CSRFUtil.initSession(serviceUrl);
	} else if(inputParams.get("PROTOCOL").equalsIgnoreCase("https")) {
   		httpClient = CSRFUtil.initSslSession(serviceUrl);
	}
        
	if (httpClient != null) { 
		String initialToken = CSRFUtil.fetchInitialToken(httpClient, serviceUrl);
		CSRFUtil.login(httpClient, serviceUrl, mcpUsername, mcpPassword);
		
		String token = CSRFUtil.fetchToken(httpClient, serviceUrl, initialToken);

        HttpPost httppost = new HttpPost(restURL);

        httppost.setHeader("X-Requested-With", "XMLHttpRequest");
        httppost.setHeader("Referer", serviceUrl);
        
        String[] tokens = CSRFUtil.parseCSRFToken(token);
        httppost.setHeader(tokens[0], tokens[1]);

        ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
        postParameters.add(new BasicNameValuePair("_username_", mcpUsername));
        postParameters.add(new BasicNameValuePair("_password_", mcpPassword));
        postParameters.add(new BasicNameValuePair("clusterName", cluster));
        postParameters.add(new BasicNameValuePair("hostName", hostname));
    
        httppost.setEntity(new UrlEncodedFormEntity(postParameters));
    
        HttpResponse response = httpClient.execute(httppost);
        
        println("RSMgmt MCP deregistration request sent. Please verify that the MCP has deregistered this cluster."); 
        println("\n\nHTTPPost output: " + response.toString());
	}    
} catch (Exception e) {
    println("RSMgmt MCP registration - FAILED:" + e.getMessage());
}
