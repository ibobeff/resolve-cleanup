import groovy.sql.Sql;
import com.resolve.util.MCPConstants;
def help()
{
	println "Usage: ClearMCPRegistration"
	println "\t Connect to a database using DBCONNECT"
	println "\t Run ClearMCPRegistration to clear any data regarding MCP"
}


def sql = new StringBuilder();
def tables = ["resolve_mcp_blueprint","resolve_mcp_cluster", "resolve_mcp_component", "resolve_mcp_group", "resolve_mcp_host","resolve_mcp_host_status","resolve_mcp_login"];
if(MAIN.isDBConnected())
{
	def statement = DB.createStatement();
	println "starting to clear MCP"
	try
	{	
		tables.each {
			sql = sql.append("TRUNCATE TABLE ");
			sql = sql.append("${it}");
			println "Clearing table ${it}"
			statement.executeQuery(sql.toString());
			sql.setLength(0);
		}
	}
	catch (Exception e)
	{
		println "unable to clear MCP";
		LOG.error("Failed to clear MCP, will not be able to monitor for Completion", e);
		mcpLog.error("Failed to clear MCP, will not be able to monitor for Completion: " + e.getMessage());
	}
	println "Finished clearing MCP Registration information";
	
}
else
{
	println "Not connected to a database"
}
