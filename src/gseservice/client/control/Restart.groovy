MAIN.setMethod("MControl.restart", null);

// usage: restart [guid] [save]
params = new Hashtable();
if (args.size() > 1)
{
	params["GUID"] = args[1];
}

if (args.size() > 2 && args[2].equalsIgnoreCase("FALSE"))
{
	params["SAVE"] = "false";
}
else
{
	params["SAVE"] = "true";
}

return params;

