def help()
{
    println "\nUsage: <Resolve Component...>";
    println "Valid Components: ALL, RSCONTROL, RSMQ, RSREMOTE, RSVIEW\n";
    println "This script will start up the specified Resolve Components";
    println "ALL can be used to start up all components (except RSCONSOLE) installed on this system";
}
if (args.length < 2)
{
    help();
}
else
{
    def cmd;
    def os = System.getProperty("os.name");
    def sb = new StringBuffer();
    if (os.contains("Win"))
    {
        cmd = "bin/run.bat";
    }
    else
    {
        cmd = "bin/run.sh";
    }
    
    for (i=1; i<args.length; i++)
    {
        cmd += " " + args[i];
    }
    LOG.warn("Running start command: " + cmd);
            
    proc = cmd.execute();
    proc.consumeProcessOutput(sb, sb);
    proc.waitFor();
    MAIN.println(sb.toString());
    LOG.info("Start Output: " + sb.toString());
}

return null;
