// terminates without saving configuration, even if saveConfigOnExit is set to TRUE
MAIN.setMethod("MControl.terminate", null);

params = new Hashtable();
if (args.size() > 1))
{
	params["GUID"] = args[1];
}

params["SAVE"] = "false";
return params;


def help()
{
    println "Usage: abort";
    println "";
    println "  Terminates the component without saving confgiuration to the file system";
    println "";
    return null;
} // help