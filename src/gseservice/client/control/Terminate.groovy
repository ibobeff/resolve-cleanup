MAIN.setMethod("MControl.terminate", null);

params = new Hashtable();
return params;


def help()
{
    println "Usage: terminate";
    println "";
    println "  Terminates the component and saves the active configuration to the file system";
    println "";
    return null;
} // help