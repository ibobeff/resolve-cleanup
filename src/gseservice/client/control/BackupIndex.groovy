MAIN.setMethod("MService.control.BackupIndex", "MDefault.print");
def help()
{
    println "\nUsage: BackupIndex <index> <graphdb>"
    println ""
    println "This Script will send a message to the connected RSMgmts to"
    println "back up the index and graphdb folders in the RSView component(s)."
    println "It is recommended to either run 'StopIndex' or to shut down the"
    println "local tomcat before creating the backup."
    println ""
    println "Example Backup Procedure:"
    println " #/>connect RSVIEWS"
    println " RSVIEWS#/>/control/StopIndex"
    println " RSVIEWS#/>connect RSMGMTS"
    println " RSMGMTS#/>/config/BackupIndex true true"
    println " RSMGMTS#/>connect RSVIEWS"
    println " RSVIEWS#/>/control/StartIndex"
}

def result = null;
if (args.length < 3)
{
    help();
}
else
{
    def params = [:];
    params["BACKUP_INDEX"] = args[1].equalsIgnoreCase("true");
    params["BACKUP_GRAPHDB"] = args[2].equalsIgnoreCase("true");
    result = params;
}
return result;
