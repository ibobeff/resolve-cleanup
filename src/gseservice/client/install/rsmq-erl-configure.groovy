import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String erlLinuxConfFilename = "rabbitmq/linux64/erlang/bin/erl";
final String erlErtsLinuxConfFilename = "rabbitmq/linux64/erlang/erts-7.3/bin/erl";
final String erlLinuxStartFilename = "rabbitmq/linux64/erlang/bin/start";
final String erlErtsLinuxStartFilename = "rabbitmq/linux64/erlang/erts-7.3/bin/start";

final String erlSolarisConfFilename = "rabbitmq/solaris/erlang/bin/erl";
final String erlErtsSolarisConfFilename = "rabbitmq/solaris/erlang/erts-7.3/bin/erl";
final String erlSolarisStartFilename = "rabbitmq/solaris/erlang/bin/start";
final String erlErtsSolarisStartFilename = "rabbitmq/solaris/erlang/erts-7.3/bin/start";

final String erlWindowsConfFilename = "rabbitmq/windows/erlang/bin/erl.ini";
final String erlErtsWindowsConfFilename = "rabbitmq/windows/erlang/erts-7.3/bin/erl.ini";

def help()
{
    println "Usage: rsmq-erl-configure\n"
    println "This script will configure the erl(.ini) file for primary and backup rsmq";
}
try
{
    def config = MAIN.getConfigRSMQ();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        def os = System.getProperty("os.name");
        File erlConfFile = null;

        if (os.contains("Win"))
        {
            erlConfFile = new File(erlWindowsConfFilename);
        }
        else if (os.contains("Linux"))
        {
            erlConfFile = new File(erlLinuxConfFilename);
        }
        else if (os.contains("SunOS"))
        {
            erlConfFile = new File(erlSolarisConfFilename);
        }

        configureFile(erlConfFile);

        if (os.contains("Win"))
        {
            erlConfFile = new File(erlErtsWindowsConfFilename);
        }
        else if (os.contains("Linux"))
        {
            erlConfFile = new File(erlErtsLinuxConfFilename);
        }
        else if (os.contains("SunOS"))
        {
            erlConfFile = new File(erlErtsSolarisConfFilename);
        }

        configureFile(erlConfFile);

        if (os.contains("Linux"))
        {
            erlConfFile = new File(erlLinuxStartFilename);
        }
        else if (os.contains("SunOS"))
        {
            erlConfFile = new File(erlSolarisStartFilename);
        }

        configureFile(erlConfFile);

        if (os.contains("Linux"))
        {
            erlConfFile = new File(erlErtsLinuxStartFilename);
        }
        else if (os.contains("SunOS"))
        {
            erlConfFile = new File(erlErtsSolarisStartFilename);
        }

        configureFile(erlConfFile);
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;


def configureFile(File erlConfFile)
{
    def os = System.getProperty("os.name");
    def dist = MAIN.getDist();
    //configure erl or erl.ini

    if (erlConfFile != null && erlConfFile.exists())
    {
        byte[] erlConfFileBytes = new byte[erlConfFile.length()];
        FileInputStream fis = new FileInputStream(erlConfFile);
        fis.read(erlConfFileBytes);
        fis.close();
        def erlConfFileStr = new String(erlConfFileBytes);

        dist = dist.replaceAll("\\\\", "/");
        dist = Matcher.quoteReplacement(dist);
        erlConfFileStr = erlConfFileStr.replaceAll("INSTALLDIR", dist);

        erlConfFileBytes = erlConfFileStr.getBytes();
        FileOutputStream fos = new FileOutputStream(erlConfFile);
        fos.write(erlConfFileBytes);
        fos.close();

        println "Configured File: " + erlConfFile.getAbsolutePath();
        LOG.warn("Configured File: " + erlConfFile.getAbsolutePath());
    }
    else
    {
        if (envConfFile!= null)
        {
            println "Cannot Find erl file: " + erlConfFile.getAbsolutePath();
            LOG.warn("Cannot Find erl file: " + erlConfFile.getAbsolutePath());
        }
        else
        {
            println "Unknown OS Type: " + os;
            LOG.warn("Unknown OS Type: " + os);
        }
    }
}
