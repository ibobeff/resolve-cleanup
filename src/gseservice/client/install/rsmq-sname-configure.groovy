import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

final String fileList = """\
rabbitmq-env
"""

def help()
{
    println "Usage: rsmq-sname-configure\n"
    println "This script will configure the rabbitmq script files to use either -name or -sname";
    println "depending on whether the local node name uses \".\" characters or not";
}
try
{
    def config = MAIN.getConfigRSMQ();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        def os = System.getProperty("os.name");
        def scriptDir = dist + "/rabbitmq/";
        if (os.contains("Win"))
        {
            scriptDir += "windows";
        }
        if (os.contains("Linux"))
        {
            scriptDir += "linux64";
        }
        if (os.contains("SunOS"))
        {
            scriptDir += "solaris";
        }
        scriptDir += "/rabbitmq/sbin/";

        def nodeName = config.getPrimaryNodeName() ? config.getPrimaryNodeName() : config.getBrokerAddr();
        if (!config.isPrimary())
        {
            nodeName = config.getBackupNodeName() ? config.getBackupNodeName() : config.getBrokerAddr2();
        }

        if (!nodeName)
        {
            println "WARNING!!! Unable to determine local rabbitmq host/ip";
            LOG.error("primary/backup Node Name value missing from config, isPrimary: " + config.isPrimary());
        }
        else
        {
            def name = nodeName.contains(".");
            for (fileName in fileList.split("\n"))
            {
                if (os.contains("Win"))
                {
                    fileName = fileName + ".bat";
                }

                def file = new File(scriptDir + fileName);

                if (file.exists())
                {
                    byte[] fileBytes = new byte[file.length()];
                    FileInputStream fis = new FileInputStream(file);
                    fis.read(fileBytes);
                    fis.close();
                    def fileStr = new String(fileBytes);

                    if (name)
                    {
						if(os.contains("Win")){
							fileStr = fileStr.replaceFirst(/set USE_LONGNAME=.*/, "set USE_LONGNAME=true");
							fileStr = fileStr.replaceFirst(/set RABBITMQ_USE_LONGNAME=.*/,"set RABBITMQ_USE_LONGNAME=true");
						} else {
							fileStr = fileStr.replaceFirst(/USE_LONGNAME=.*/,"USE_LONGNAME=true");
							fileStr = fileStr.replaceFirst(/RABBITMQ_USE_LONGNAME=.*/,"RABBITMQ_USE_LONGNAME=true");
						}
                    }
                    else
                    {
						if(os.contains("Win")){
							fileStr = fileStr.replaceFirst(/set USE_LONGNAME=.*/,"set USE_LONGNAME=");
							fileStr = fileStr.replaceFirst(/set RABBITMQ_USE_LONGNAME=.*/,"set RABBITMQ_USE_LONGNAME=");
						} else {
							fileStr = fileStr.replaceFirst(/USE_LONGNAME=.*/,"USE_LONGNAME=");
							fileStr = fileStr.replaceFirst(/RABBITMQ_USE_LONGNAME=.*/,"RABBITMQ_USE_LONGNAME=");
						}
							
                    }

                    fileBytes = fileStr.getBytes();
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(fileBytes);
                    fos.close();

                    if (name)
                    {
                        println "Configured -name in File: " + file.getName();
                        LOG.warn("Configured -name in File: " + file.getAbsolutePath());
                    }
                    else
                    {
                        println "Configured -sname in File: " + file.getName();
                        LOG.warn("Configured -sname in File: " + file.getAbsolutePath());
                    }
                }
                else
                {
                    LOG.warn("Unable to find File for -sname/-name configure: " + file.getAbsolutePath());
                }
            }
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
