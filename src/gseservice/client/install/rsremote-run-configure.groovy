import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String runFilename = "bin/run.sh";
final String runFilenameWin = "bin/run.bat";

def help()
{
    println "Usage: rsremote-run-configure <instance name>\n"
    println "This script will configure the <instance name> rsremote's run.sh or run.bat";
}
if (args.length < 2)
{
    help();
}
else
{
    try
    {
        def instanceName = args[1].toLowerCase();
        def config = MAIN.getConfigRSRemote(instanceName);
        def dist = MAIN.getDist();

        if (config && dist)
        {
            def os = System.getProperty("os.name");
            File runFile = null;
            if (os.contains("Win"))
            {
                runFile = new File(dist + "/" + instanceName + "/" + runFilenameWin);
            }
            else if (os.contains("Linux") || os.contains("SunOS"))
            {
                runFile = new File(dist + "/" + instanceName + "/" + runFilename);
            }

            def replaceMap = config.rsremoteRunReplaceValues();

            //Configure run.sh || run.bat
            if (runFile != null && runFile.exists())
            {
                byte[] runFileBytes = new byte[runFile.length()];
                FileInputStream fis = new FileInputStream(runFile);
                fis.read(runFileBytes);
                fis.close();

                String runFileStr = new String(runFileBytes);


                for (regex in replaceMap.keySet())
                {
                    def value = replaceMap.get(regex);
                    if (regex.contains("Xmx"))
                    {
                        println instanceName.toUpperCase() + " Maximum Heap Allocation Set to " + (value - "Xmx");
                        LOG.info(instanceName.toUpperCase() + " Maximum Heap Allocation Set to " + value);
                    }
                    value = Matcher.quoteReplacement(value);
                    runFileStr = runFileStr.replaceFirst(regex, value);
                }

                runFileStr = runFileStr.replaceFirst("INSTANCE_NAME=.*", "INSTANCE_NAME="+instanceName);

                runFileBytes = runFileStr.getBytes();
                FileOutputStream fos = new FileOutputStream(runFile);
                fos.write(runFileBytes);
                fos.close();

                println "Configured File: " + runFile.getAbsolutePath();
                LOG.warn("Configured File: " + runFile.getAbsolutePath());
            }
            else
            {
                if (runFile != null)
                {
                    println "Cannot Find " + instanceName + " run script: " + runFile.getAbsolutePath();
                    LOG.warn("Cannot Find " + instanceName + " run script: " + runFile.getAbsolutePath());
                }
                else
                {
                    println "Unknown OS Type: " + os;
                    LOG.warn("Unknown OS Type: " + os);
                }
            }
        }
        else
        {
            println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
            LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
        }
    }
    catch (Exception e)
    {
        println "Unexpected Exception while Running Configuration: " + e.getMessage();
        LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
    }
}
return null;
