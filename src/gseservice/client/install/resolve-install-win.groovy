import java.io.File;
import java.io.FileOutputStream;
import com.resolve.util.MCPLog;
import com.resolve.util.MCPConstants;

def help()
{
    println "This Script will create a VBS file to run the setup during installation for all configured components";
}

final String setupFile = "resolve-setup.bat";
MCPLog mcpLog = null;

try
{
    def installStatusFileName = MAIN.getDist() + "/rsmgmt/log/" + MCPConstants.MCP_RESOLVE_INSTALL_WIN_STATUS_LOGFILE;
    mcpLog = new MCPLog(installStatusFileName, true, true, MCPConstants.MCP_LOG_ITEM_INSTALL_WIN);
    mcpLog.write(MCPConstants.MCP_FIELD_KEY_START, "Windows Installation Started");

    def os = System.getProperty("os.name");
    if (os.contains("Win"))
    {
        def dist = MAIN.getDist();
        if (dist)
        {
            def vbsScript = new StringBuilder();
            vbsScript.append("@echo off\r\n");
            def rsmq = MAIN.isBlueprintRSMQ();
            if (rsmq)
            {
                vbsScript.append("cscript.exe %1\\setup-rsmq-win.vbs\r\n");
                vbsScript.append("ping 123.45.67.89 -n 1 -w 5000 > nul\r\n");
				def cmd = [dist + "\\rabbitmq\\windows\\erlang\\vcredist_x64.exe","/passive","/check"];
				def sb = new StringBuffer();
				LOG.warn("running vcredist_x64.exe")
				proc = cmd.execute();
				proc.consumeProcessOutput(sb,sb);
				proc.waitFor();
				MAIN.println(sb);
				LOG.info("vcredist_x64 output: " + sb.toString());
            }
            def rssearch = MAIN.isBlueprintRSSearch();
            if (rssearch)
            {
                vbsScript.append("cscript.exe %1\\setup-rssearch-win.vbs\r\n");
                vbsScript.append("ping 123.45.67.89 -n 1 -w 5000 > nul\r\n");
            }
            def rsview = MAIN.isBlueprintRSView();
            if (rsview)
            {
                vbsScript.append("cscript.exe %1\\setup-rsview-win.vbs\r\n");
                vbsScript.append("ping 123.45.67.89 -n 1 -w 5000 > nul\r\n");
            }
            def rscontrol = MAIN.isBlueprintRSControl();
            if (rscontrol)
            {
                vbsScript.append("cscript.exe %1\\setup-rscontrol-win.vbs\r\n");
                vbsScript.append("ping 123.45.67.89 -n 1 -w 5000 > nul\r\n");
            }
            def rsremote = MAIN.isBlueprintRSRemote();
            if (rsremote)
            {
                vbsScript.append("cscript.exe %1\\setup-rsremote-win.vbs\r\n");
                vbsScript.append("ping 123.45.67.89 -n 1 -w 5000 > nul\r\n");
            }
			
            def vbsFile = new File(dist + "/" + setupFile);
            LOG.debug("Creating file " + vbsFile.getAbsolutePath());

            vbsFileBytes = vbsScript.toString().getBytes();
            FileOutputStream fos = new FileOutputStream(vbsFile);
            fos.write(vbsFileBytes);
            fos.close();
        }
        else
        {
            println "Blueprint Has Not Been Properly Instantiated. Cancelling Install Service Setup";
            LOG.warn("Blueprint Has Not Been Properly Instantiated. Cancelling Install Service Setup");
            mcpLog.warn("Blueprint Has Not Been Properly Instantiated. Cancelling Install Service Setup");
        }
    }
    else
    {
        println "This is not a Windows Installation, skipping VBS script creation";
        mcpLog.warn("This is not a Windows Installation, skipping VBS script creation");
    }
}
catch (Exception e)
{
    println "Unexpected Exception: " + e.getMessage();
    LOG.error("Failed to create VBS script for install setup", e)
    mcpLog.error("Failed to create VBS script for install setup: " + e.getMessage());
}
mcpLog?.write(MCPConstants.MCP_FIELD_KEY_END, "Windows Installation Ended");

return null;
