import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;
import java.util.concurrent.TimeUnit;

final String runFilename = "bin/kafka-acls.sh";
final String instanceName = "dcs/kafka_2.11-2.0.0";

def runAclConfig(cmd, args)
{
	StringBuilder sb = new StringBuilder();
	def process = (cmd + " " + args).execute();
	process.consumeProcessOutput(sb, sb);
	boolean success = process.waitFor(5, TimeUnit.MINUTES);
	if(success) {
		println "Configured File: " + runFile.getAbsolutePath() + " is executed to configure Kafka ACLs";
		LOG.debug("Configured File: " + runFile.getAbsolutePath() + " is executed to configure Kafka ACLs");
		LOG.debug("command coutput: " + sb.toString());
	} else {
		println "Configured File: " + runFile.getAbsolutePath() + " is executed but fail to configure Kafka ACLs.";
		LOG.warn("Configured File: " + runFile.getAbsolutePath() + " is executed but fail to configure Kafka ACLs.");
		LOG.warn("command coutput: " + sb.toString());
	}
}

def help()
{
	println "Usage: kafka-acl-configure\n"
	println "This script will configure Kafka ACLs";
}
try
{
	def dist = MAIN.getDist();
	def config = MAIN.getConfigKafka();

	if (config && dist)
	{
		def os = System.getProperty("os.name");

		File runFile = null;
		if (os.contains("Linux"))
		{
			runFile = new File(dist + "/" + instanceName + "/" + runFilename);
		}

		if (runFile != null && runFile.exists())
		{
			String cmd = runFile.getAbsolutePath();
			
			runAclConfig(cmd, "--authorizer-properties zookeeper.connect=localhost:2181 --add --allow-principal User:datacollector --operation Write --topic 'dcs_topic'");
			runAclConfig(cmd, "--authorizer-properties zookeeper.connect=localhost:2181 --add --allow-principal User:datacollector --operation Write --operation Describe --transactional-id '*'");
			runAclConfig(cmd, "--authorizer-properties zookeeper.connect=localhost:2181 --add --allow-principal User:dataloader --operation Read --topic 'dcs_topic' --group 'LOCAL_GROUP'");
		}
	}
	else
	{
		println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
		LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
	}
}
catch (Exception e)
{
	println "Unexpected Exception while Running Configuration: " + e.getMessage();
	LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;

