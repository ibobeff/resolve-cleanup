import com.resolve.esb.ESB;
import com.resolve.rsmgmt.Main;
import org.codehaus.jackson.map.ObjectMapper;

try {
	if (args.size() < 4)
		{
			return help();
		}
		
		def roleId = args[1];
		def secretId = args[2];
		def vaultHostname = args[3];
		def rootToken = args[4];
		def rsremotesQueue = "RSREMOTES";
		def rsviewsQueue = "RSVIEWS";
		def dataCollectorsQueue = "DATA_COLLECTORS";
		def dcsReportingsQueue = "DCS_REPORTINGS";
		
		params = new HashMap();
		params["ROLEID"] = roleId
		params["SECRETID"] = secretId
		params["VAULTHOSTNAME"] = vaultHostname
		params["ROOTTOKEN"] = rootToken
		
		Main main = new Main();
		// skip sending of metrics data
		main.REMOTE_RSMGMT = true;
		main.init("rsmgmt");
		ESB esb = main.getESB();
		success = esb.sendMessage(rsremotesQueue, "MAction.applyVaultConfiguration", params);
		
		if (success)
		{
			println("Successfully sent vault information to RSRemotes.");
		}
		else
		{
			println("Failed to send vault information to RSRemotes.");
		}
			
		success = esb.sendMessage(rsviewsQueue, "MAction.applyVaultConfiguration", params);
		
		if (success)
		{
			println("Successfully sent vault information to RSViews.");
		}
		else
		{
			println("Failed to send vault information to RSViews.");
		}
		
		def objectMapper = new ObjectMapper();
		def vaultProperties = new HashMap();
		vaultProperties["roleId"] = roleId
		vaultProperties["secretId"] = secretId
		vaultProperties["vaultHostname"] = vaultHostname
		vaultProperties["token"] = rootToken
		def vaultPropertiesJSON = objectMapper.writeValueAsString(vaultProperties);

		success = esb.sendInternalMessage(dataCollectorsQueue, "", vaultPropertiesJSON);
		
		if (success)
		{
			println("Successfully sent vault information to DATA_COLLECTORS.");
		}
		else
		{
			println("Failed to send vault information to DATA_COLLECTORS.");
		}
		
		success = esb.sendInternalMessage(dcsReportingsQueue, "", vaultPropertiesJSON);
		
		if (success)
		{
			println("Successfully sent vault information to DCS_REPORTINGS.");
		}
		else
		{
			println("Failed to send vault information to DCS_REPORTINGS.");
		}
				
} catch (Exception e)
{
	println "Unexpected Exception while Running Configuration: " + e.getMessage();
	LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;

def help()
{
	println "Usage: setup-vault-tokens '-FD<vaultRoleId>;<vaultSecretId>;<vaultHostname>";
	println "";
	println "  Distributes the Vault information to RSRemote and RSView instances bound to the specified RSMQ broker";
	println "";
	return null;
} // help
