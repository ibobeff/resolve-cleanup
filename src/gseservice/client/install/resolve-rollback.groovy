import com.resolve.util.Compress;
import java.io.File;
import java.io.FileInputStream;

def help()
{
    println "Usage: resolve-rollback <Update Date Stamp>";
    println "Rolls Back Update denoted by the <Update Date Stamp>";
}

if (args.length < 2)
{
    help();
}
else
{
    try
    {
        def dist = MAIN.getDist();
        def homeDir = new File(dist);

        if (!homeDir.exists())
        {
            println "Home Directory Misconfigured: " + homeDir.getAbosolutePath();
            LOG.warn("Home Directory Misconfigured: " + homeDir.getAbosolutePath());
        }
        else
        {
            def backupExtension = args[1];
            def fileList = fileList(homeDir, backupExtension);
            LOG.info("Final rollback list: " + fileList);

            def compress = new Compress();
            //Undo Any Files Moved
            for (fileName in fileList)
            {
                if (fileName =~ /update-.+-\d{8}\.zip/)
                {
                    //skip
                    LOG.debug("Skipping Rollback of Update Zip: " + fileName);
                }
                else if (fileName.endsWith(".zip"))
                {
                    def updatedFile = new File(fileName);
                    def matcher = fileName =~ /(.+)-${backupExtension}\.zip/;
                    def componentName = matcher[0][1];

                    if (updatedFile.exists())
                    {
                        println "Attempting To Roll Back " + updatedFile.getName();
                        def zipContent = compress.zipContent(updatedFile);

                        def componentFile = new File(componentName);
                        if (!removeFile(componentFile))
                        {
                            println "WARNING!!! Unable to Remove Updated Component File " + componentFile.getName();
                            println "    Please Verify " + componentFile + " Has Been Rolled Back Propertly";
                            LOG.error(componentFile.getAbsolutePath() + " Could Not Be Removed On Rollback. Exists: " + componentFile.exists());
                        }
                        componentFile = null;

                        LOG.debug("Roll Back Updated Content: " + zipContent);
                        for (content in zipContent)
                        {
                            if (!content.startsWith(componentName))
                            {
                                def updatedGWTFile = new File(dist + "/tomcat/webapps/resolve/" + content);
                                LOG.debug("Possible Removal: " + updatedGWTFile.getAbsolutePath());
                                if (updatedGWTFile.exists())
                                {
                                    LOG.debug("Attempting to remove update: " + updatedGWTFile.getAbsolutePath());
                                    if(!updatedGWTFile.delete())
                                    {
                                        println "WARNING!!! Unable to Remove Updated File " + updatedGWTFile.getName();
                                        println "    Please Verify " + updatedGWTFile.getName() + " Has Been Rolled Back Propertly";
                                        LOG.error(updatedGWTFile.getAbsolutePath() + " Could Not Be Removed On Rollback. " + 
                                            "Exists: " + updatedGWTFile.exists());
                                    }
                                }
                                else
                                {
                                    LOG.debug("No longer exists: " + updatedGWTFile.getAbsolutePath());
                                }
                            }
                        }
                        compress.unzip(updatedFile, new File(dist + "/tomcat/webapps/resolve"));
                        if (!updatedFile.delete())
                        {
                            println "WARNING!!! Backup GWT Zip File " + updatedFile.getName() + " May Not Have Been Removed Properly";
                            println "    Please Verify " + updatedFile.getName() + " Has Been Removed";
                            LOG.error(updatedFile.getAbsolutePath() + " Could Not Be Removed On Rollback. Exists: " + updatedFile.exists());
                            updatedFile.deleteOnExit();
                        }
                        updatedFile = null;
                    }
                }
                else
                {
                    def updatedFile = new File(fileName);
                    if (updatedFile.exists())
                    {
                        def parent = updatedFile.getParentFile();
                        if (parent.getName() == "rsexpert")
                        {
                            //skip
                            LOG.debug("Skipping Rollback of Export Zip: " + fileName);
                        }
                        else
                        {
                            def matcher = fileName =~ /(.+)\.${backupExtension}/;
                            def originalName = matcher[0][1];

                            println "Attempting To Roll Back " + updatedFile.getName();
                            LOG.warn("Attempting To Roll Back " + updatedFile.getAbsolutePath() + " to " + originalName);
                            def file = new File(originalName);
                            LOG.info("Restoring Backup " + updatedFile.getAbsolutePath() + " to " + file.getAbsolutePath());
                            if (!file.delete())
                            {
                                println "WARNING!!! Unable To Remove Updated File " + file.getName() + ".";
                                println "    Please Verify " + file.getName() + " has been rolled back properly";
                                LOG.error(file.getAbsolutePath() + " Could Not Be Removed On Rollback. Exists: " + file.exists());
                            }
                            if (!updatedFile.renameTo(file))
                            {
                                println "WARNING!!! Unable To Roll Back Updated File."
                                println "    Please Verify " + file.getName() + " has been rolled back properly";
                                LOG.error(file.getAbsolutePath() + " Could Not Be Renamed On Rollback");
                            }
                            file = null;
                        }
                    }
                }
            }
            def newFiles = new File(dist + "/file/update-" + backupExtension + "-new.log");
            if (newFiles.exists())
            {
                println "Removing filed added by " + backupExtension + " update";
                LOG.info("Removing files listed in " + newFiles.getAbsolutePath());

                byte[] newFileBytes = new byte[newFiles.length()];
                FileInputStream fis = new FileInputStream(newFiles);
                fis.read(newFileBytes);
                fis.close();

                String newFileStr = new String(newFileBytes);
                for (newFileName in newFileStr.split("\n"))
                {
                    def newFile = new File(dist + "/" + newFileName);
                    if (newFile.exists())
                    {
                        println "Removing " + newFile.getName();
                        LOG.info("Removing " + newFile.getAbsolutePath());
                        if (!newFile.delete())
                        {
                            println "WARNING!!! Failed to remove file " + newFile.getName() + ", please remove it manually";
                            LOG.error("Failed to remove file " + newFile.getAbsolutePath());
                        }
                    }
                    else
                    {
                        println "Cannot find file " + newFile.getName() + " to remove";
                        LOG.warn("Cannot find file " + newFile.getAbsolutePath() + " to remove");
                    }
                }
            }
        }
    }
    catch (Exception e)
    {
        println "WARNING!!! Unexpected Exception While Rolling Back Update: " + e.getMessage();
        println "    Please Verify Rollback has Been Completed";
        LOG.error("Exception on Rollback", e);
    }
}

return null;

def removeFile(File file)
{
    LOG.warn("Attempting to Remove File/Directory: " + file.getAbsolutePath());
    def result = true;
    if (file.exists())
    {
        if (file.isDirectory())
        {
            for (child in file.listFiles())
            {
                removeFile(child);
            }
        }
        if (file.delete())
        {
            LOG.warn("Removed File: " + file.getAbsolutePath());
        }
        else
        {
            LOG.warn("Failed to Remove File: " + file.getAbsolutePath());
            result = false;
        }
    }

    return result;
}

def fileList(File file, extension)
{
    def result;
    if (file.exists())
    {
        if (file.getName() != "file" && file.isDirectory())
        {
            result = new LinkedList();
            for (child in file.listFiles())
            {
                def childResult = fileList(child, extension);
                if (childResult)
                {
                    if (childResult instanceof List)
                    {
                        for (fileName in childResult)
                        {
                            if (fileName.endsWith(".zip"))
                            {
                                result.add(fileName);
                            }
                            else
                            {
                                result.addFirst(fileName);
                            }
                        }
                    }
                    else
                    {
                        if (childResult)
                        {
                            if (childResult.endsWith(".zip"))
                            {
                                result.add(childResult);
                            }
                            else
                            {
                                result.addFirst(childResult);
                            }
                        }
                    }
                }
            }
        }
        else if (file.getName().endsWith("." + extension) || file.getName().endsWith(extension + ".zip"))
        {
            LOG.info("Add rollback file: " + file.getAbsolutePath());
            result = file.getAbsolutePath();
        }
    }
    return result;
}
