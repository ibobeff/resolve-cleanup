import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

final String logFilename = "config/log.cfg";

def help()
{
    println "Usage: rssync-log-configure\n"
    println "This script will configure the rssync's log.cfg";
}
try
{
    def config = MAIN.getConfigRSSync();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        //Configure log.cfg
        File logFile = new File(dist + "/rssync/" + logFilename);
        if (logFile.exists())
        {
            byte[] logFileBytes = new byte[logFile.length()];
            FileInputStream fis = new FileInputStream(logFile);
            fis.read(logFileBytes);
            fis.close();

            String logFileStr = new String(logFileBytes);

            def replaceMap = config.rssyncLogReplaceValues();

            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                logFileStr = logFileStr.replaceAll(regex, value);
            }

            logFileBytes = logFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(logFile);
            fos.write(logFileBytes);
            fos.close();

            println "Configured File: " + logFile.getAbsolutePath();
            LOG.warn("Configured File: " + logFile.getAbsolutePath());
        }
        else
        {
            println "Cannot Find RSSync log file: " + logFile.getAbsolutePath();
            LOG.warn("Cannot Find RSSync log file: " + logFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
