import org.apache.commons.io.FileUtils;
import java.nio.charset.StandardCharsets;

final String templateFileName = "config/logstash-primaryhost.template";
final String configFilename = "config/logstash-resolve.conf";
final String instanceName = "logstash";

def help() {
    println "Usage: logstash-primaryhost-configure\n";
    println "This script will copy config/logstash-primaryhost.txt to config/logstash-resolve.conf if logstash is configured on primary RSVIEW host.";
}

try {
        def rslogEnabled = MAIN.isBlueprinRSLog();
        if (rslogEnabled) {
            def dist = MAIN.getDist();
            def config = MAIN.getConfigLogstash();
            
            if (config && dist) {
                File configFile = new File(dist + "/" + instanceName + "/" + configFilename);
                File templateConfigFile = new File(dist + "/" + instanceName + "/" + templateFileName);
                
                String template = FileUtils.readFileToString(templateConfigFile, StandardCharsets.UTF_8.name());
                FileUtils.writeStringToFile(configFile, template, StandardCharsets.UTF_8.name());
            }
            else {
                println "logstash-primaryhost.txt could not be copied to logstash-resolve.conf on primary host";
                LOG.warn("logstash-primaryhost.txt could not be copied to logstash-resolve.conf on primary host");
            }
    }
}
catch (Exception e) {
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
