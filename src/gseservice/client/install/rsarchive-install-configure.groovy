import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String installFilename = "bin/init.rsarchive";

final String instanceName = "rsarchive";

def help()
{
    println "Usage: rsarchive-install-configure\n";
    println "This script will configure rsarchive's init.rsarchive";
}
try
{
    def dist = MAIN.getDist();
    def config = MAIN.getConfigRSArchive();

    if (config && dist)
    {
        def os = System.getProperty("os.name");

        File installFile = null;
        if (os.contains("Win"))
        {
            LOG.warn("Windows Standalone Archive is not currently supported");
        }
        else if (os.contains("Linux") || os.contains("SunOS"))
        {
            installFile = new File(dist + "/" + instanceName + "/" + installFilename);
            def replaceMap = config.rsarchiveRunReplaceValues();

            //Configure init.rsarchive
            if (installFile != null && installFile.exists())
            {
                byte[] installFileBytes = new byte[installFile.length()];
                FileInputStream fis = new FileInputStream(installFile);
                fis.read(installFileBytes);
                fis.close();

                String installFileStr = new String(installFileBytes);

                for (regex in replaceMap.keySet())
                {
                    def value = replaceMap.get(regex);
                    value = Matcher.quoteReplacement(value);
                    installFileStr = installFileStr.replaceFirst(regex, value);
                }

                dist = Matcher.quoteReplacement(dist);
                installFileStr = installFileStr.replaceAll("INSTALLDIR", dist);

                installFileBytes = installFileStr.getBytes();
                FileOutputStream fos = new FileOutputStream(installFile);
                fos.write(installFileBytes);
                fos.close();

                println "Configured File: " + installFile.getAbsolutePath();
                LOG.warn("Configured File: " + installFile.getAbsolutePath());
            }
            else
            {
                println "Cannot Find " + instanceName + " init script: " + installFile.getAbsolutePath();
                LOG.warn("Cannot Find " + instanceName + " init script: " + installFile.getAbsolutePath());
            }
        }
        else
        {
            println "Unknown OS Type: " + os;
            LOG.warn("Unknown OS Type: " + os);
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
