import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

final String sreeFilename = "tomcat/webapps/sree/WEB-INF/classes/sree.properties";
final String newsreeFilename = "tomcat/webapps/sree/WEB-INF/classes/newsree.properties";

def help()
{
    println "Usage: rsview-sree-configure\n"
    println "This script will configure rsview's sree.properties and newsree.properties";
}
try
{
    def config = MAIN.getConfigRSView();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        def replaceMap = config.sreeReplaceValues();
        def sreeFile = new File(dist + "/" + sreeFilename);
        def newsreeFile = new File(dist + "/" + newsreeFilename);

        if (sreeFile.exists())
        {
            byte[] sreeFileBytes = new byte[sreeFile.length()];
            FileInputStream fis = new FileInputStream(sreeFile);
            fis.read(sreeFileBytes);
            fis.close();

            String sreeFileStr = new String(sreeFileBytes);

            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                sreeFileStr = sreeFileStr.replaceFirst(regex, value);
            }

            sreeFileBytes = sreeFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(sreeFile);
            fos.write(sreeFileBytes);
            fos.close();

            println "Configured File: " + sreeFile.getAbsolutePath();
            LOG.warn("Configured File: " + sreeFile.getAbsolutePath());
        }
        else
        {
            println "Cannot Find sree properties file: " + sreeFile.getAbsolutePath();
            LOG.warn("Cannot Find sree properties file: " + sreeFile.getAbsolutePath());
        }
        if (newsreeFile.exists())
        {
            byte[] newsreeFileBytes = new byte[newsreeFile.length()];
            FileInputStream fis = new FileInputStream(newsreeFile);
            fis.read(newsreeFileBytes);
            fis.close();

            String newsreeFileStr = new String(newsreeFileBytes);

            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                newsreeFileStr = newsreeFileStr.replaceFirst(regex, value);
            }

            newsreeFileBytes = newsreeFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(newsreeFile);
            fos.write(newsreeFileBytes);
            fos.close();

            println "Configured File: " + newsreeFile.getAbsolutePath();
            LOG.warn("Configured File: " + newsreeFile.getAbsolutePath());
        }
        else
        {
            println "Cannot Find newsree properties file: " + newsreeFile.getAbsolutePath();
            LOG.warn("Cannot Find newsree properties file: " + newsreeFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
