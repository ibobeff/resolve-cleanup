import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;
import java.util.concurrent.TimeUnit;

final String runFilename = "bin/logstash-config.sh";
final String runFilenameWin = "bin/logstash-config.bast";

final String instanceName = "logstash";

def help()
{
    println "Usage: logstash-keystore-configure\n"
    println "This script will configure logstash keystore value for database password";
}
try
{
    def dist = MAIN.getDist();
    def config = MAIN.getConfigLogstash();

    if (config && dist)
    {
        def os = System.getProperty("os.name");

        File runFile = null;
        if (os.contains("Win"))
        {
            println "Window is not supported for logstash";
			LOG.warn("Window is not supported for logstash");
			return null;
        }
        if (os.contains("Linux"))
        {
            runFile = new File(dist + "/" + instanceName + "/" + runFilename);
        }

        def replaceMap = config.logstashResolveConfReplaceValues();
        //Configure run.sh || run.bat
        if (runFile != null && runFile.exists())
        {
			String dbPasswordKeystore = replaceMap.get("logstash.resolveconf.input.jdbc.jdbc_pwd", '${RESOLVE_DB_PWD}');
            def keyMatcher = dbPasswordKeystore =~ /^\\?\$\{(.+)\}$/;
            if (keyMatcher.find())
            {
                dbPasswordKeystore = keyMatcher.group(1);
                String dbPassword = replaceMap.get("logstash.resolveconf.input.DB_PASSWORD", "resolve");
                dbPassword = dbPassword.replace('$', '\$'); // adding escape chars if password contains '$'
                String cmd = runFile.getAbsolutePath() + " " + dbPasswordKeystore + " " + dbPassword;
                StringBuilder sb = new StringBuilder();
                def process = cmd.execute();
                process.consumeProcessOutput(sb, sb);
                boolean success = process.waitFor(5, TimeUnit.MINUTES);
                if(success) {
                    println "Configured File: " + runFile.getAbsolutePath() + " is executed to configure logstash keystore for database password";
                    LOG.debug("Configured File: " + runFile.getAbsolutePath() + " is executed to configure logstash keystore for database password");
                    LOG.debug("command coutput: " + sb.toString());
                } else {
                    println "Configured File: " + runFile.getAbsolutePath() + " is executed but fail to save database password to logstash keystore. A manual logstash keystore command may need to be execute to store changes to logstash keystore. FAIL ";
                    LOG.warn("Configured File: " + runFile.getAbsolutePath() + " is executed but fail to save database password to logstash keystore. A manual logstash keystore command may need to be execute to store changes to logstash keystore");
                    LOG.warn("command coutput: " + sb.toString());
                }
            }
            else
            {
                println "Password Appears to be Plain Text instead of Reference, Skipping add to " + runFile.getAbsolutePath();
                LOG.info("Password Appears to be Plain Text instead of Reference, Skipping add to " + runFile.getAbsolutePath());
            }
            
            String rsmqPwdKey = replaceMap.get("logstash.resolveconf.input.rabbitmq.password.key", '${rsmq_password_key}');
            keyMatcher = rsmqPwdKey =~ /^\\?\$\{(.+)\}$/;
            if (keyMatcher.find())
            {
                rsmqPwdKey = keyMatcher.group(1);
                String rabbitPassword = replaceMap.get("logstash.resolveconf.input.rabbitmq.password", "resolve");
                rabbitPassword = rabbitPassword.replace('$', '\$'); // adding escape chars if password contains '$'
                String cmd = runFile.getAbsolutePath() + " " + rsmqPwdKey + " " + rabbitPassword;
                StringBuilder sb = new StringBuilder();
                def process = cmd.execute();
                process.consumeProcessOutput(sb, sb);
                boolean success = process.waitFor(5, TimeUnit.MINUTES);
                if(success) {
                    println "Configured File: " + runFile.getAbsolutePath() + " is executed to configure logstash keystore for rabbitmq password";
                    LOG.debug("Configured File: " + runFile.getAbsolutePath() + " is executed to configure logstash keystore for rabbitmq password");
                    LOG.debug("command coutput: " + sb.toString());
                } else {
                    println "Configured File: " + runFile.getAbsolutePath() + " is executed but fail to save rabbitmq password to logstash keystore. A manual logstash keystore command may need to be execute to store changes to logstash keystore. FAIL ";
                    LOG.warn("Configured File: " + runFile.getAbsolutePath() + " is executed but fail to save rabbitmq password to logstash keystore. A manual logstash keystore command may need to be execute to store changes to logstash keystore");
                    LOG.warn("command coutput: " + sb.toString());
                }
            }
            else
            {
                println "Password Appears to be Plain Text instead of Reference, Skipping add to " + runFile.getAbsolutePath();
                LOG.info("Password Appears to be Plain Text instead of Reference, Skipping add to " + runFile.getAbsolutePath());
            }
        }
        else
        {
            println "Unable to find " + runFile.getName() + ", Skipping Keystore Password Setup";
            LOG.warn("Unable to find " + runFile.getAbsolutePath() + ", Skipping Keystore Password Setup");
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
