import com.resolve.util.MCPLog;
import com.resolve.util.MCPConstants;
import java.io.File;

def help()
{
    println "Usage: resolve-configure\n"
    println "This script will configure resolve's components based on blueprint file.";
}

/**
 * return -1 if disk space < 15%.
 *        1 if disk space < 50% or < 10GB
 *        0 otherwise
 */
def checkdiskSpace(mcpLog) {
    File file = new File("tmp");
    long freeSpace = file.getFreeSpace();
    float diskspacePer = freeSpace / file.getTotalSpace() * 100.0;
    def code = 0;
    if(diskspacePer <= 15) {
        def msg = "current free disk space is " + diskspacePer + "%. At least 15% free disk space need to be allocated for this installation.";
        println msg;
        mcpLog.warn(msg);
        code = -1;
    }
    else if(diskspacePer < 50) {
        def msg = "warn: current free disk space is " + diskspacePer + "%. This space be to allocated at least 50% is recommended.";
        println msg;
        mcpLog.warn(msg);
        code = 1;
    }
    float freeSpaceInGB = freeSpace/1073741824.0
    if(code == 0 && freeSpaceInGB < 10) {
        def msg = "warn: current free disk space is " + freeSpaceInGB + "GB. This space be to allocated at least 10GB is recommended.";
        println msg;
        mcpLog.warn(msg);
        code = 1;
    }
    return code;
}

MCPLog mcpLog = null;
def errorExitCode = null;
try
{
    def dist = MAIN.getDist();
   
    if (dist)
    {
        def configLogFileName = dist + "/rsmgmt/log/" + MCPConstants.MCP_RESOLVE_CONFIG_LOGFILE;
        mcpLog = new MCPLog(configLogFileName, true);
        mcpLog.write("CONFIG","START","Configuration Started");
        def rsmq = MAIN.isBlueprintRSMQ();
        if (rsmq)
        {
            errorExitCode = checkdiskSpace(mcpLog);
            println "\nConfiguring RSMQ";
            mcpLog.write("RSMQ_CONFIG","START","Configuring RSMQ");
            def product = MAIN.getConfigRSMQ().getProduct();
            if (Constants.ESB_RABBITMQ.equalsIgnoreCase(product))
            {
                MAIN.executeCommand("/install/rsmq-rabbitmq-configure");
                MAIN.executeCommand("/install/rsmq-rabbitmqenv-configure");
                MAIN.executeCommand("/install/rsmq-erl-configure");
                MAIN.executeCommand("/install/rsmq-sname-configure");
                MAIN.executeCommand("/install/rsmq-start-configure");
            }

            MAIN.executeCommand("/install/rsmq-user-configure");
            MAIN.executeCommand("/install/rsmq-install-configure");
            MAIN.executeCommand("/install/rsmq-service-configure");
            //MAIN.executeCommand("/install/rsmq-esapi-configure");
            MAIN.executeCommand("/install/rsmq-run-configure");
        }
        
        def rssearch = MAIN.isBlueprintRSSearch();
        if (rssearch)
        {
            if(errorExitCode == null) {
                errorExitCode = checkdiskSpace(mcpLog);
            }
            println "\nConfiguring RSSearch";
            mcpLog.write("RSSEARCH_CONFIG","START","Configuring RSSearch");
            MAIN.executeCommand("/install/rssearch-install-configure.groovy");
            MAIN.executeCommand("/install/rssearch-service-configure.groovy");
            MAIN.executeCommand("/install/rssearch-run-configure.groovy");
            MAIN.executeCommand("/install/rssearch-env-configure.groovy");
            MAIN.executeCommand("/install/rssearch-yml-configure.groovy");
        }
        
        def rsview = MAIN.isBlueprintRSView();
        if (rsview)
        {
            println "\nConfiguring RSView";
            mcpLog.write("RSVIEW_CONFIG","START","Configuring RSView");
            MAIN.executeCommand("/install/rsview-configure");
            if(MAIN.executeCommand("/install/rsview-tomcat-configure") == -1) {
                errorExitCode = -1;
            } else {
                MAIN.executeCommand("/install/rsview-hibernate-configure");
                MAIN.executeCommand("/install/rsview-ehcache-configure");
                MAIN.executeCommand("/install/rsview-log-configure");
                MAIN.executeCommand("/install/rsview-run-configure");
                MAIN.executeCommand("/install/rsview-env-configure");
                MAIN.executeCommand("/install/rsview-install-configure");
                MAIN.executeCommand("/install/rsview-service-configure");
                MAIN.executeCommand("/install/rsview-axis2-configure");
                MAIN.executeCommand("/install/rsview-sree-configure");
                MAIN.executeCommand("/install/rsview-dashboard-configure");
                MAIN.executeCommand("/install/rsview-neo4j-configure");
                MAIN.executeCommand("/install/rsview-krb5-configure");
                MAIN.executeCommand("/install/rsview-web-configure");
                MAIN.executeCommand("/install/rsview-kibana-configure");
                MAIN.executeCommand("/install/rsview-bigdesk-configure");
                MAIN.executeCommand("/install/rsview-esapi-configure");
                MAIN.executeCommand("/install/rsview-csrfguard-configure");
                MAIN.executeCommand("/install/rsview-move-configs");
            }
            
        }
        def rsremote = MAIN.isBlueprintRSRemote();
        if (rsremote)
        {
            println "Configuring RSRemote";
            mcpLog.write("RSREMOTE_CONFIG","START","Configuring RSRemote");
            def instances = MAIN.getRSRemoteInstances();
            if (instances)
            {
                for (instance in instances)
                {
                    System.out.println("Configuring rsremote instance: " + instance);
                    MAIN.executeCommand("/install/rsremote-configure " + instance);
                    MAIN.executeCommand("/install/rsremote-run-configure " + instance);
                    MAIN.executeCommand("/install/rsremote-env-configure " + instance);
                    MAIN.executeCommand("/install/rsremote-install-configure " + instance);
                    MAIN.executeCommand("/install/rsremote-service-configure " + instance);
                    MAIN.executeCommand("/install/rsremote-esapi-configure " + instance);
                    MAIN.executeCommand("/install/rsremote-log-configure " + instance);
                }
            }
        }
        def rscontrol = MAIN.isBlueprintRSControl();
        if (rscontrol)
        {
            println "\nConfiguring RSControl";
            mcpLog.write("RSCONTROL_CONFIG","START","Configuring RSControl");
            MAIN.executeCommand("/install/rscontrol-configure");
            MAIN.executeCommand("/install/rscontrol-hibernate-configure");
            MAIN.executeCommand("/install/rscontrol-ehcache-configure");
            MAIN.executeCommand("/install/rscontrol-run-configure");
            MAIN.executeCommand("/install/rscontrol-env-configure");
            MAIN.executeCommand("/install/rscontrol-log-configure");
            MAIN.executeCommand("/install/rscontrol-esapi-configure");
            MAIN.executeCommand("/install/rscontrol-install-configure");
            MAIN.executeCommand("/install/rscontrol-service-configure");
        }

        def rsconsole = MAIN.isBlueprintRSConsole();
        if (rsconsole)
        {
            println "\nConfiguring RSConsole";
            mcpLog.write("RSCONSOLE_CONFIG","START","Configuring RSConsole");

            MAIN.executeCommand("/install/rsconsole-configure");
            MAIN.executeCommand("/install/rsconsole-connections-configure");
            MAIN.executeCommand("/install/rsconsole-env-configure");
            MAIN.executeCommand("/install/rsconsole-esapi-configure");
            MAIN.executeCommand("/install/rsconsole-log-configure");
        }

        def rsmgmt = MAIN.isBlueprintRSMgmt();
        if (rsmgmt)
        {
            println "\nConfiguring RSMgmt";
            if ("TRUE".equalsIgnoreCase(BLUEPRINT.get("INSTALL")) && !rsview && !rscontrol)
            {
                println "\nNo RSView or RSControl Present, Setting RSMgmt DB Pool to false";
                LOG.info("No RSView or RSControl Configured, Setting RSMgmt DB Pool to false");
                BLUEPRINT.setProperty("rsmgmt.sql.active", "false");
            }
            mcpLog.write("RSMGMT_CONFIG","START","Configuring RSMgmt");

            MAIN.executeCommand("/install/rsmgmt-configure");
            MAIN.executeCommand("/install/rsmgmt-install-configure");
            MAIN.executeCommand("/install/rsmgmt-service-configure");
            MAIN.executeCommand("/install/rsmgmt-run-configure");
            MAIN.executeCommand("/install/rsmgmt-env-configure");
            MAIN.executeCommand("/install/rsmgmt-esapi-configure");
            MAIN.executeCommand("/install/rsmgmt-log-configure");
        }

        def rssync = MAIN.isBlueprintRSSync();
        if (rssync)
        {
            println "\nConfiguring RSSync";
            mcpLog.write("RSSYNC_CONFIG","START","Configuring RSSync");
            MAIN.executeCommand("/install/rssync-configure.groovy");
            MAIN.executeCommand("/install/rssync-install-configure.groovy");
            MAIN.executeCommand("/install/rssync-service-configure.groovy");
            MAIN.executeCommand("/install/rssync-run-configure.groovy");
            MAIN.executeCommand("/install/rssync-env-configure.groovy");
            MAIN.executeCommand("/install/rssync-esapi-configure.groovy");
            MAIN.executeCommand("/install/rssync-log-configure.groovy");
        }

        def rsarchive = MAIN.isBlueprintRSArchive();
        if (rsarchive)
        {
            println "\nConfiguring RSArchive";
            mcpLog.write("RSARCHIVE_CONFIG","START","Configuring RSArchive");
            MAIN.executeCommand("/install/rsarchive-hibernate-configure.groovy");
            MAIN.executeCommand("/install/rsarchive-configure.groovy");
            MAIN.executeCommand("/install/rsarchive-install-configure.groovy");
            MAIN.executeCommand("/install/rsarchive-run-configure.groovy");
            MAIN.executeCommand("/install/rsarchive-env-configure.groovy");
            MAIN.executeCommand("/install/rsarchive-esapi-configure.groovy");
            MAIN.executeCommand("/install/rsarchive-log-configure.groovy");
        }

        def logstash = MAIN.isBlueprintLogstash();
        if (logstash) {
            println "\nConfiguring Logstash";
            mcpLog.write("LOGSTASH_CONFIG","START","Configuring Logstash");
            MAIN.executeCommand("/install/logstash-env-configure.groovy");
            MAIN.executeCommand("/install/logstash-install-configure.groovy");
			MAIN.executeCommand("/install/logstash-service-configure.groovy");
			MAIN.executeCommand("/install/logstash-primaryhost-configure.groovy");
            MAIN.executeCommand("/install/logstash-resolve-configure.groovy");
			MAIN.executeCommand("/install/logstash-keystore-configure.groovy");
            MAIN.executeCommand("/install/logstash-run-configure.groovy");
            MAIN.executeCommand("/install/logstash-startup-configure.groovy");
            MAIN.executeCommand("/install/logstash-yml-configure.groovy");
        }
        
        def rslogEnabled = MAIN.isBlueprinRSLog();
        if (rslogEnabled) {
            println "\nConfiguring Rslog";
            MAIN.executeCommand("/install/rslog-install-configure.groovy");
            MAIN.executeCommand("/install/rslog-configure.groovy");
        }
  
        def dcs = MAIN.isBlueprintDCS();
        if (dcs) {
            println "\nConfiguring DCS";
            mcpLog.write("DCS_CONFIG","START","Configuring DCS");
            MAIN.executeCommand("/install/dcs-rsdataloader-yml-configure.groovy");
            MAIN.executeCommand("/install/dcs-rsreporting-yml-configure.groovy");
        }
		
		def dcs_rsdataloader = MAIN.isBlueprintDCSRSDataLoader();
		if(dcs_rsdataloader) {
			println "\nConfiguring DCS RSDataLoader";
			MAIN.executeCommand("/install/rsdataloader-install-configure");
            MAIN.executeCommand("/install/rsdataloader-service-configure");
		}
		
		def dcs_rsreporting = MAIN.isBlueprintDCSRSReporting()
		if(dcs_rsreporting) {
			println "\nConfiguring DCS RSReporting";
			MAIN.executeCommand("/install/rsreporting-install-configure");
            MAIN.executeCommand("/install/rsreporting-service-configure");
		}
		
        MAIN.executeCommand("/install/resolve-service-configure");

        LOG.info("Setting up Softlinks");
        MAIN.executeCommand("/install/resolve-softlink-configure");

        mcpLog.write("CONFIG","COMPLETE","Configuration Completed");
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated. Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated. Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.warn("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return errorExitCode;
/* removing for now since it is breaking the config.sh blueprint save
if(errorExitCode == null) {
    errorExitCode = 0;
}
System.exit(errorExitCode);
*/
