import groovy.sql.Sql;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

def help()
{
    println "This method has been deprecated";
}
if (args.length < 4)
{
    help();
}
else
{
    //deprecating method
    LOG.warn("This method has been deprecated");
    return null;
    try
    {
        def os = System.getProperty("os.name");
        def arch = System.getProperty("sun.arch.data.model");
        def fileName = args[1];
        def importArgs = "";
        def postConsoleCommands = [];
        def postScriptCommands = [];
        def postCliCommands = [];
        def arguments = [];
        rsconsoleUsername = args[2];
        rsconsolePassword = args[3];
        rsviewGuid = null;

        def type = "";
        def buildVersion = "";
        def buildDate = "";

        def updateFileMatcher = fileName =~ /(.+)-(.+)-(\d{8}).zip/;
        if (updateFileMatcher.find())
        {
            type = updateFileMatcher.group(1);
            buildVersion = updateFileMatcher.group(2);
            buildDate = updateFileMatcher.group(3);
        }

        def argumentsFile = new File(MAIN.getDist() + "/file/update.properties");
        if (argumentsFile.exists())
        {
            def br = new BufferedReader(new FileReader(argumentsFile));
            def arg;
            while ((arg=br.readLine()) != null)
            {
                arguments.add(arg);
            }
            br.close();
        }

        if (args.length > 3)
        {
            for (i=3; i<args.length; i++)
            {
                arguments.add(args[i]);
            }
        }
        LOG.info("Optional Arguments: " + arguments);
        if (os.contains("Win") && arguments.contains("--reset-services") && !arguments.contains("--no-config"))
        {
            println "Resetting Services";
            def resetCmd = "bin/reset-services.bat all";
            def resetProc = resetCmd.execute();
            def resetSout = new StringBuffer();
            resetProc.consumeProcessOutput(resetSout, resetSout);
            resetProc.waitFor();
            LOG.debug("Reset Services Output: " + resetSout.toString());
        }
        else
        {
            LOG.debug("No Windows service reset");
        }

        def rsviewPrimary = false;
        if (MAIN.getConfigRSView())
        {
            LOG.debug("RSView Configured");
            def configFile = new File(MAIN.getDist() + "/tomcat/webapps/resolve/WEB-INF/config.xml");
            if (configFile.exists())
            {
                XDoc configDoc = new XDoc(configFile);
                rsviewGuid = configDoc.getStringValue("./ID/@GUID");
            }
            rsviewPrimary = MAIN.getConfigRSView().isPrimary();

            //Remove Tomcat Cache on Upgrade
            if ("UPGRADE".equalsIgnoreCase(type))
            {
                def tomcatWorkDir = new File(MAIN.getDist() + "/tomcat/work/Catalina/localhost/resolve/org/apache/jsp");
                if (tomcatWorkDir.exists())
                {
                    LOG.info("Attempting to remove Tomcat local cache: " + tomcatWorkDir);
                    if (!removeFile(tomcatWorkDir))
                    {
                        LOG.warn("Failed to remove Tomcat working dir");
                    }
                }
                else
                {
                    LOG.info("Cannot find Tomcat local cache: " + tomcatWorkDir);
                }
            }
        }
        LOG.debug("Local RSView GUID: " + rsviewGuid);

        if (rsviewPrimary)
        {
            MAIN.executeCommand("install/resolve-install-setup.groovy");
        }

        if (arguments.contains("--backup-index"))
        {
            println "Backing up Index and GraphDB folder";
            def backupResult = MAIN.executeCommand("control/BackupIndex true true");
            println backupResult;
        }
        if (arguments.contains("--purge-index"))
        {
            def index = new File(MAIN.getDist() + "/tomcat/webapps/resolve/WEB-INF/index")
            println "Purging Search Index";
            LOG.warn("Attempting to Delete Index Directory: " + index.getAbsolutePath());
            if (index.exists())
            {
                if (!removeFile(index))
                {
                    println "WARNING!!! Failed to Purge Search Index. Please run Purge Index from Wiki Administration.";
                    LOG.error("Failed to Delete Index Directory: " + index.getAbsolutePath());
                }
            }
            else
            {
                println "WARNING!!! Search Index Not Found.";
                LOG.error("Index Directory Doesn't Exist: " + index.getAbsolutePath());
            }

            if (arguments.contains("--no-start") || arguments.contains("--no-restart"))
            {
                println "WARNING!!! Index All Must be run Manually after Start Up";
                LOG.warn("Purge Called without Start Up, Unable to Run Index All");
            }
        }
        if (arguments.contains("--purge-graphdb"))
        {
            def graphdb; 
            if (rsviewPrimary)
            {
                graphdb = new File(MAIN.getDist() + "/tomcat/webapps/resolve/WEB-INF/graphdb/index");
                println "Purging GraphDB Index";
                LOG.warn("Attempting to Delete GraphDB Index Directory: " + graphdb.getAbsolutePath());
            }
            else
            {
                graphdb = new File(MAIN.getDist() + "/tomcat/webapps/resolve/WEB-INF/graphdb");
                println "Purging GraphDB";
                LOG.warn("Attempting to Delete GraphDB Directory: " + graphdb.getAbsolutePath());
            }
            if (graphdb.exists())
            {
                if (!removeFile(graphdb))
                {
                    println "WARNING!!! Failed to Purge GraphDB.";
                    LOG.error("Failed to Delete GraphDB Directory: " + graphdb.getAbsolutePath());
                }
            }
            else
            {
                println "WARNING!!! GraphDB directory Not Found.";
                LOG.error("GraphDB Directory Doesn't Exist: " + graphdb.getAbsolutePath());
            }
        }

        def postCommandsFile = new File("file/post_commands.txt");
        LOG.info("Looking for Post Commands: " + postCommandsFile.getAbsolutePath());
        if (postCommandsFile.exists())
        {
            LOG.debug("Post Commands Found");
            def br = new BufferedReader(new FileReader(postCommandsFile));
            def cmd;
            while ((cmd=br.readLine()) != null)
            {
                LOG.debug("Found Command: " + cmd);
                if (cmd.trim())
                {
                    if (cmd.startsWith("rsconsole"))
                    {
                        postConsoleCommands.add(cmd);
                    }
                    else if (cmd.startsWith("script"))
                    {
                        postScriptCommands.add(cmd);
                    }
                    else if (cmd.contains(".cli"))
                    {
                        postCliCommands.add(cmd);
                    }
                    else if (cmd.startsWith("IMPORT:"))
                    {
                        importArgs = cmd.substring(7);
                    }
                    else
                    {
                        MAIN.executeCommand(cmd);
                    }
                }
            }
            br.close();
        }

        if (rsviewPrimary && postConsoleCommands)
        {
            def tmpPostConsoleCommands = [];
            tmpPostConsoleCommands.addAll(postConsoleCommands);
            for (cmd in tmpPostConsoleCommands)
            {
                if (!cmd.contains("GUID"))
                {
                    postConsoleCommand(cmd);
                    postConsoleCommands.remove(cmd);
                }
            }
        }

        if (!arguments.contains("--no-start") && !arguments.contains("--no-restart"))
        {
            def sql;
            def rsviewStartCount = 0;

            println "Starting Components";
            if (MAIN.isBlueprintRSMQ())
            {
                LOG.debug("Starting Components: mgmt/Start RSMQ");
                MAIN.executeCommand("mgmt/Start RSMQ");

                def config = MAIN.getConfigRSMQ();

                def result = "nodedown";
                def i=0;

                def hostname = config.getBrokerAddr();
                if (!config.isPrimary())
                {
                    hostname = config.getBrokerAddr2();
                }
                def port = config.getManagementPort();
                def username = config.getUsername();
                def password = config.getP_assword();
                def rootUrl = "http://" + hostname + ":" + port + "/api";

                while (result.equals("nodedown") && i < 10)
                {
                    if (i > 0)
                    {
                        Thread.sleep(1000);
                    }
                    try
                    {
                        def statusResult = RESTUtils.isExists(rootUrl, username, password);
                        LOG.info("RSMQ Client Response Code 200? " + statusResult);
                        if (statusResult)
                        {
                            result = "up";
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        LOG.info("RSMQ Test Failed - " + e.getMessage());
                        LOG.trace("RSMQ Test Failed", e);
                    }
                    i++;
                }

                if (result.equals("nodedown"))
                {
                    println "WARNING!!! The RSMQ Instance Does not appear to have started properly";
                    LOG.error("The RSMQ Instance Does not appear to have started properly");
                }
            }

            //rabbitmq commands
            if (postScriptCommands)
            {
                def tmpPostScriptCommands = [];
                tmpPostScriptCommands.addAll(postScriptCommands);
                for (cmd in tmpPostScriptCommands)
                {
                    if (cmd.contains("rabbitmq"))
                    {
                        if (MAIN.isBlueprintRSMQ())
                        {
                            if (cmd.contains("setup_user"))
                            {
                                if (os.contains("Win"))
                                {
                                    try
                                    {
                                        def config = MAIN.getConfigRSMQ();
                                        def hostname = config.getBrokerAddr();
                                        if (!config.isPrimary())
                                        {
                                            hostname = config.getBrokerAddr2();
                                        }
                                        def port = config.getManagementPort();
                                        def username = config.getUsername();
                                        def password = config.getP_assword();
                                        def rootUrl = "http://" + hostname + ":" + port + "/api";

                                        def addUserUrl = rootUrl + "/users/" + username;
                                        def params = [:];
                                        params.put("password", password);
                                        params.put("tags", "administrator");
                                        LOG.info("add user: " + RESTUtils.put(addUserUrl, params, "guest", "guest"));

                                        def setPermissionsUrl = rootUrl + "/permissions/%2f/" + username;
                                        params = [:];
                                        params.put("configure", ".*");
                                        params.put("write", ".*");
                                        params.put("read", ".*");
                                        LOG.info("set permissions: " + RESTUtils.put(setPermissionsUrl, params, "guest", "guest"));

                                        if (RESTUtils.isExists(rootUrl, username, password))
                                        {
                                            System.out.println("RSMQ User created successfully");
                                            LOG.info("RabbitMQ User Created");

                                            def deleteUrl = rootUrl + "/users/guest";
                                            LOG.info("delete guest: " + RESTUtils.delete(deleteUrl, username, password));
                                        }
                                        else
                                        {
                                            System.out.println("WARNING!!! RSMQ User creation failed");
                                            LOG.error("RabbitMQ User Creation Failed");
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        System.out.println("WARNING!!! RSMQ User creation failed");
                                        LOG.error("RabbitMQ User Creation Failed", e);
                                    }
                                }
                            }
                            else
                            {
                                postScriptCommand(cmd);
                            }
                        }
                        postScriptCommands.remove(cmd);
                    }
                }
            }

            if (MAIN.isBlueprintCassandra())
            {
                LOG.debug("Starting Components: mgmt/Start CASSANDRA");
                MAIN.executeCommand("mgmt/Start CASSANDRA");
                if (!arguments.contains("--skip-cli-test"))
                {
                    def testFile = new File(MAIN.getDist() + "/cassandra/conf/test.cli");
                    if (testFile.exists())
                    {
                        println "Checking for Cassandra Startup";
                        LOG.debug("Checking for Cassandra Startup");
                        MAIN.executeCommand("mgmt/ExecuteCLI \"" + testFile.getAbsolutePath() + "\"");

                    }
                    else
                    {
                        println "Unable to check for Cassandra Startup Completion";
                        LOG.warn("Unable to check for Cassandra Startup Completion, Test CLI File Not Found: " + testFile.getAbsolutePath());
                    }
                }
                for (cliCommand in  postCliCommands)
                {
                    MAIN.executeCommand(cliCommand);
                }
            }
            if (MAIN.isBlueprintRSSearch())
            {
                LOG.debug("Starting Components: mgmt/Start RSSEARCH");
                MAIN.executeCommand("mgmt/Start RSSEARCH");
            }
            if (rsviewPrimary)
            {
                LOG.debug("Starting Components: mgmt/Start RSVIEW");
                MAIN.executeCommand("mgmt/Start RSVIEW");
                if (MAIN.isDBConnected() && !arguments.contains("--no-import"))
                {
                    sql = "select count(*) as COUNT from resolve_event where u_value LIKE '%RSVIEW%'";
                    def statement = DB.createStatement();
                    def startRS;
                    try
                    {
                        LOG.debug("Attempting RSView Start Query: " + sql.toString());
                        startRS = statement.executeQuery(sql);
                        if (startRS.next())
                        {
                            rsviewStartCount = startRS.getInt(1);
                            LOG.debug("Found " + rsviewStartCount + " instances of event in database");
                        }
                        else
                        {
                            LOG.error("Unable to determine the number of instances of the event in the database");
                        }
                    }
                    catch (Exception e)
                    {
                        rsviewStartCount = -1;
                        println "Unable to monitor for Installation Completion";
                        LOG.error("Failed to Query for RSView or Installation Finished event, will not be able to monitor for Completion", e);
                    }
                    finally
                    {
                        startRS?.close();
                        statement?.close();
                    }
                }
            }
            else
            {
                if ("UPGRADE".equalsIgnoreCase(type))
                {
                    def cmd;
                    def sb = new StringBuffer();
                    if (os.contains("Win"))
                    {
                        cmd = "tomcat/bin/run.bat";
                    }
                    else
                    {
                        cmd = "tomcat/bin/run.sh";
                    }
                    cmd += " NEO_CLUSTER";
                    
                    LOG.warn("Running start command: " + cmd);
                            
                    def proc = cmd.execute();
                    proc.consumeProcessOutput(sb, sb);
                    proc.waitFor();
                    MAIN.println(sb.toString());
                    LOG.info("Start Output: " + sb.toString());
                }

                if (importArgs)
                {
                    sql = "select count(*) as COUNT from resolve_event where u_value like 'PRIMARY " + type.toUpperCase() + " " + buildVersion + " " + buildDate + "% Finished'";
                }
            }

            def rsviewStarted = false;
            if (sql && MAIN.isDBConnected() && !arguments.contains("--no-import") && rsviewStartCount != -1)
            {
                LOG.debug("Detect RSView Startup or Primary Completion SQL: " + sql);
                def statement = DB.createStatement();

                print "Waiting For Initialization to Finish";
                def time = System.currentTimeMillis();
                while (true)
                {
                    def rs;
                    try
                    {
                        rs = statement.executeQuery(sql);
                        if (rs.next())
                        {
                            def eventCount = rs.getInt(1);
                            if ((rsviewPrimary && eventCount == rsviewStartCount) || (!rsviewPrimary && eventCount == 0))
                            {
                                //still waiting for RSView startup/Primary Imports Completed
                                Thread.sleep(20000);
                                print ".";
                                continue;
                            }
                            if (rsviewPrimary)
                            {
                                LOG.debug("RSView Startup Detected");
                                rsviewStarted = true;
                                break;
                            }
                            else
                            {
                                LOG.debug("Primary Import Completion Detected");
                                break;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        if (rsviewPrimary)
                        {
                            println "\nWARNING: CANNOT MONITOR RSVIEW STARTUP - DEFAULT IMPORT MODULES NOT IMPORTED";
                        }
                        else
                        {
                            println "\nWARNING: CANNOT MONITOR " + type.toUpperCase() + " IMPORTS";
                        }
                        LOG.error("Failed to Query for RSView or Installation Finished event, will not be able to monitor for Completion", e);
                        break;
                    }
                    finally
                    {
                        rs?.close();
                    }
                }
                //if (System.currentTimeMillis() - time >= 1200000)
                //{
                //    println "\nWARNING: TIMED OUT ON RSVIEW STARTUP - DEFAULT IMPORT MODULES NOT IMPORTED";
                //    LOG.error("WARNING: TIMED OUT ON RSVIEW STARTUP - DEFAULT IMPORT MODULES NOT IMPORTED");
                //}
                statement?.close();
                println "";
            }
            else
            {
                rsviewStarted = true;
                LOG.debug("No Monitoring for RSView Startup or Import Completion");
            }

            if (rsviewPrimary)
            {
                if (postConsoleCommands)
                {
                    for (cmd in postConsoleCommands)
                    {
                        postConsoleCommand(cmd);
                    }
                }
                if (!arguments.contains("--no-cleanup"))
                {
                    if ("UPGRADE".equalsIgnoreCase(type))
                    {
                        postConsoleCommand("rsconsole internal/UpgradeCleanup GUID");
                    }
                    else
                    {
                        postConsoleCommand("rsconsole internal/UpdateCleanup GUID");
                    }
                }
            }

            if (rsviewPrimary && rsviewStarted && !arguments.contains("--no-import"))
            {
                LOG.debug("Generating Import Args");
                def lastImportModule = "";
                if (importArgs)
                {
                    lastImportModule = importArgs;
                    def lastImportIdx = importArgs.lastIndexOf(";");
                    if (lastImportIdx != -1)
                    {
                        lastImportModule = importArgs.substring(lastImportIdx+1);
                    }
                    def importSql = "select count(*) as COUNT from resolve_impex_log l join resolve_impex_module m on m.sys_id = l.u_impex_module where UPPER(m.u_name) = UPPER('$lastImportModule')";
                    def importCount = -1;
                    if (MAIN.isDBConnected())
                    {
                        def statement = DB.createStatement();
                        def importRS;
                        try
                        {
                            LOG.debug("Attempting Impex Monitor Query: " + importSql.toString());
                            importRS = statement.executeQuery(importSql);
                            if (importRS.next())
                            {
                                importCount = importRS.getInt(1);
                                LOG.debug("Found " + importCount + " instances of " + lastImportModule + " log in the database");
                            }
                            else
                            {
                                println "Unable to monitor for Impex Completion";
                                LOG.error("No Results Returned for Impex Monitor Query");
                            }
                        }
                        catch (Exception e)
                        {
                            println "Unable to monitor for Impex Completion";
                            LOG.error("Failed to Query for Impex Log Count, will not be able to monitor for Import Completion", e);
                        }
                        finally
                        {
                            statement?.close();
                            importRS?.close();
                        }
                    }


                    importArgs += ";-u;resolve.maint;--block-index";
                    if (rsviewGuid)
                    {
                        importArgs += ";-guid;" + rsviewGuid;
                    }

                    println "Importing Updated Libraries";
                    def importCmd;
                    if (os.contains("Win"))
                    {
                        importCmd = ["rsconsole/bin/run.bat", "-u", rsconsoleUsername, "-p", rsconsolePassword, "-f", "impex/ImportModule", "\"-FD" + importArgs + "\""];
                    }
                    else
                    {
                        importCmd = ["rsconsole/bin/run.sh", "-u", rsconsoleUsername, "-p", rsconsolePassword, "-f", "impex/ImportModule", "-FD" + importArgs];
                    }
                    LOG.info("Import Updated Libraries Command: " + importCmd);
                    def proc = importCmd.execute();
                    def sout = new StringBuffer();
                    proc.consumeProcessOutput(sout, sout);
                    proc.waitFor();
                    LOG.debug("Import Process Output: " + sout.toString());

                    if (importCount >= 0)
                    {
                        print "Waiting for the Imports to Complete";
                        time = System.currentTimeMillis();
                        def statement = DB.createStatement();
                        while (true)
                        {
                            def rs = statement.executeQuery(importSql);
                            if (rs.next())
                            {
                                def newImportCount = rs.getInt(1);
                                if (newImportCount > importCount)
                                {
                                    LOG.debug("Import Log Count Increased " + importCount + " -> " + newImportCount); 
                                    println " Imports Completed";
                                    break;
                                }
                            }
                            rs?.close();
                            Thread.sleep(20000);
                            print ".";
                        }
                        //if (System.currentTimeMillis() - time >= 1200000)
                        //{
                        //    println "\nWARNING: TIMED OUT ON RSVIEW IMPORT MONITORING, UNABLE TO TELL IF IMPORTS FINISHED";
                        //    LOG.error("WARNING: TIMED OUT ON RSVIEW IMPORT MONITORING, UNABLE TO TELL IF IMPORTS FINISHED");
                        //}
                        statement?.close();
                        println "";
                    }
                    else
                    {
                        LOG.debug("No Imports to Monitor");
                    }
                }
                else
                {
                    println "No Updates Modules to Import";
                    LOG.debug("No Updates Modules to Import");
                }
            }
            else
            {
                println "Skipping Import Updated Libraries";
                LOG.debug("Skipping Import Updated Libraries");
            }

            if ("UPGRADE".equalsIgnoreCase(type) && !rsviewPrimary)
            {
                LOG.debug("Restarting Backup RSViews");
                MAIN.executeCommand("mgmt/Stop RSVIEW");
            }
            LOG.debug("Starting Components: mgmt/Start ALL");
            MAIN.executeCommand("mgmt/Start ALL");

            if (MAIN.isDBConnected())
            {
                def currTime = new Timestamp(GMTDate.getTime());
                def sysId = SysId.generate(this);
                def insertSql = "insert into resolve_event (sys_id, u_value, sys_updated_by, sys_updated_on, sys_created_by, sys_created_on) values(?, '";
                if (rsviewPrimary)
                {
                    insertSql += "PRIMARY ";
                }
                insertSql += "" + type.toUpperCase() + " " + buildVersion + " " + buildDate + " " + MAIN.configId.getGuid() + " Finished', 'system', ?, 'system', ?)";

                //insert into resolve_event
                LOG.debug("Inserting Resolve Event: " + insertSql);
                def conn = new Sql(DB);
                def insertRow = conn.executeUpdate(insertSql, [sysId, currTime, currTime]);
                if (insertRow == 0)
                {
                    println "WARNING: UNABLE TO INSERT PRIMARY INSTALLATION EVENT, SECONDARY INSTALLATION WILL NOT DETECT COMPLETION";
                    LOG.error("WARNING: UNABLE TO INSERT PRIMARY INSTALLATION EVENT, SECONDARY INSTALLATIONS WILL NOT DETECT COMPLETION");
                }
            }

            if (rsviewPrimary && (arguments.contains("--purge-index") || arguments.contains("--index-all")))
            {
                println "Indexing All Components";
                if (os.contains("Win"))
                {
                    indexCmd = ["rsconsole/bin/run.bat", "-u", rsconsoleUsername, "-p", rsconsolePassword, "-f", "runbook/Index", "\"-FDtrue;true\""];
                }
                else
                {
                    indexCmd = ["rsconsole/bin/run.sh", "-u", rsconsoleUsername, "-p", rsconsolePassword, "-f", "runbook/Index", "-FDtrue;true"];
                }
                LOG.warn("Running Index All: " + indexCmd);
                def proc = indexCmd.execute();
                def sout = new StringBuffer();
                proc.consumeProcessOutput(sout, sout);
                proc.waitFor();
                LOG.debug("Index All Output: " + sout.toString());
            }
        }
        else
        {
            println "Skipping Start All Components";
            LOG.debug("Skipping Start All Components");
        }

        LOG.info("Post Script Commands: " + postScriptCommands);
        if (postScriptCommands)
        {
            for (cmd in postScriptCommands)
            {
                postScriptCommand(cmd);
            }
        }

        MAIN.executeCommand("/install/resolve-update-cleanup");
    }
    catch (Exception e)
    {
        println "Unexpected Exception While Performing Update: " + e.getMessage();
        LOG.error("Unexpected Exception While Performing Update: " + e.getMessage(), e);
    }
}
return null;

def removeFile(File file)
{
    def result = false;
    if (file.exists())
    {
        if (file.isDirectory())
        {
            for (child in file.listFiles())
            {
                result = removeFile(child);
            }
        }
        if (file.delete())
        {
            result = true;
            LOG.debug("Removed File: " + file.getAbsolutePath());
        }
    }

    return result;
}

def postConsoleCommand(def cmd)
{
    def os = System.getProperty("os.name");
    cmd = cmd - "rsconsole";
    if (cmd.contains("GUID"))
    {
        if (rsviewGuid)
        {
            cmd = cmd.replaceAll("GUID", rsviewGuid);
        }
        else
        {
            println "RSView is not configured, skipping " + cmd;
            LOG.warn("RSView is not configured, skipping " + cmd);
            cmd = null;
        }
    }
    if (cmd)
    {
        def consoleCmd;
        if (os.contains("Win"))
        {
            consoleCmd = ["rsconsole/bin/run.bat", "-u", rsconsoleUsername, "-p", rsconsolePassword, "-w", "0", "-f"];
        }
        else
        {
            consoleCmd = ["rsconsole/bin/run.sh", "-u", rsconsoleUsername, "-p", rsconsolePassword, "-w", "0", "-f"];
        }
        cmd = cmd.tokenize(" ");
        consoleCmd.add(cmd[0]);
        if (cmd.size() > 1)
        {
            cmd.remove(0);
            if (os.contains("Win"))
            {
                consoleCmd.add("\"-FD" + cmd.join(";") + "\"");
            }
            else
            {
                consoleCmd.add("-FD" + cmd.join(";"));
            }
        }

        println "Executing Console command: " + consoleCmd.join(" ");
        LOG.info("Executing Console command: " + consoleCmd);

        def proc = consoleCmd.execute();
        def sout = new StringBuffer();
        proc.consumeProcessOutput(sout, sout);
        proc.waitFor();
        println sout.toString();
        LOG.debug("RSConsole Process Output: " + sout.toString());
    }
}

def postScriptCommand(def cmd)
{
    def os = System.getProperty("os.name");
    cmd = cmd - "script";
    if (rsviewGuid)
    {
        cmd = cmd.replaceAll("GUID", rsviewGuid);
    }

    if (os.contains("Win"))
    {
        cmd = cmd.replaceAll("<os>", "windows");
        cmd += ".bat";
    }
    else if (os.contains("Linux"))
    {
        cmd = cmd.replaceAll("<os>", "linux64");
        cmd += ".sh";
    }
    else if (os.contains("SunOS"))
    {
        cmd = cmd.replaceAll("<os>", "solaris");
        cmd += ".sh";
    }
    
    println "Executing Script: " + cmd;
    LOG.info("Executing Script: " + cmd);
    def proc = cmd.execute();
    def sout = new StringBuffer();
    proc.consumeProcessOutput(sout, sout);
    proc.waitFor();
    LOG.debug("Script Output: " + sout.toString());
}

def importSort()
{
    def list;
    if (MAIN.blueprintProperties)
    {
        list = MAIN.blueprintProperties.getProperty("resolve.import");
    }
    if (list)
    {
        list = list.split(",");
        for (int i=0; i<list.length; i++)
        {
            if (!list[i].endsWith(".zip"))
            {
                list[i] = list[i] + ".zip";
            }
        }
        list = Arrays.asList(list);
    }
    else
    {
        list = [];
    }
    def sort = { File f1, f2 ->
        int result = 0;

        String s1 = f1.getName();
        String s2 = f2.getName();
        
        int p1 = list.indexOf(s1);
        int p2 = list.indexOf(s2);
        
        if (p1 == -1)
        {
            if (p2 > -1)
            {
                result = 1;
            }
        }
        else if (p2 == -1)
        {
            result = -1;
        }
        else if (p1 > p2)
        {
            result = 1;
        }
        else if (p2 > p1)
        {
            result = -1;
        }
        
        return result;
    }
    return sort;
};
