import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String runFilename = "tomcat/bin/run.sh";
final String runFilenameWin = "tomcat/bin/run.bat";

def help()
{
    println "Usage: rsview-run-configure\n"
    println "This script will configure rsview's run.sh or run.bat";
}
try
{
    def config = MAIN.getConfigRSView();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        def os = System.getProperty("os.name");

        File runFile = null;
        if (os.contains("Linux") || os.contains("SunOS"))
        {
            runFile = new File(dist + "/" + runFilename);
        }
        if (os.contains("Win"))
        {
            runFile = new File(dist + "/" + runFilenameWin);
        }

        def replaceMap = config.rsviewRunReplaceValues();

        //Configure run.sh || run.bat
        if (runFile != null && runFile.exists())
        {
            byte[] runFileBytes = new byte[runFile.length()];
            FileInputStream fis = new FileInputStream(runFile);
            fis.read(runFileBytes);
            fis.close();

            String runFileStr = new String(runFileBytes);

            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                if (regex.contains("Xmx"))
                {
                    println "RSVIEW Maximum Heap Allocation Set to " + (value - "Xmx");
                    LOG.info("RSVIEW Maximum Heap Allocation Set to " + value);
                }
                value = Matcher.quoteReplacement(value);
                if (regex.toLowerCase().contains("xmx") || regex.toLowerCase().contains("xms"))
                {
                    runFileStr = runFileStr.replaceAll(regex, value);
                }
                else
                {
                    runFileStr = runFileStr.replaceFirst(regex, value);
                }
            }

            runFileBytes = runFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(runFile);
            fos.write(runFileBytes);
            fos.close();

            println "Configured File: " + runFile.getAbsolutePath();
            LOG.warn("Configured File: " + runFile.getAbsolutePath());
        }
        else
        {
            if (runFile != null)
            {
                println "Cannot Find rsview's install script: " + runFile.getAbsolutePath();
                LOG.warn("Cannot Find rsview's install script: " + runFile.getAbsolutePath());
            }
            else
            {
                println "Unknown OS Type: " + os;
                LOG.warn("Unknown OS Type: " + os);
            }
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
