import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String installFilenameWin = "install-service.bat";
final String installFilename = "init.rsremote";
final String updateServiceFileName = "rsremote-updateService.bat"

def help()
{
    println "Usage: rsremote-install-configure <instance name>\n"
    println "This script will configure the <instance name> rsremote's init.rsremote and install-service.bat";
}
if (args.length < 2)
{
    help();
}
else
{
    try
    {
        def instanceName = args[1].toLowerCase();
        def config = MAIN.getConfigRSRemote(instanceName);
        def dist = MAIN.getDist();

        if (config && dist)
        {
            def os = System.getProperty("os.name");

            File[] installFiles = null;
            if (os.contains("Win"))
            {
                installFiles = [new File(dist + "/" + instanceName + "/bin/" + installFilenameWin), new File(dist + "/" + instanceName + "/bin/" + updateServiceFileName)];
            }
            if (os.contains("Linux") || os.contains("SunOS"))
            {
                installFiles = [new File(dist + "/" + instanceName + "/bin/" + installFilename)];
                if (installFiles[0] == null || !installFiles[0].exists())
                {
                    installFiles = [new File(dist + "/" + instanceName + "/bin/" + "init." + instanceName)];
                }
            }

            def replaceMap = config.rsremoteRunReplaceValues();
			for(installFile in installFiles){
				//Configure install-service.bat || init.rsremote
				if (installFile != null && installFile.exists())
				{
					byte[] installFileBytes = new byte[installFile.length()];
					FileInputStream fis = new FileInputStream(installFile);
					fis.read(installFileBytes);
					fis.close();

					String installFileStr = new String(installFileBytes);

					for (regex in replaceMap.keySet())
					{
						def value = replaceMap.get(regex);
						value = Matcher.quoteReplacement(value);
						installFileStr = installFileStr.replaceFirst(regex, value);
					}

					dist = Matcher.quoteReplacement(dist);
					installFileStr = installFileStr.replaceAll("INSTALLDIR", dist);
					installFileStr = installFileStr.replaceFirst("INSTANCE=.*", "INSTANCE="+instanceName);

					installFileBytes = installFileStr.getBytes();
					FileOutputStream fos = new FileOutputStream(installFile);
					fos.write(installFileBytes);
					fos.close();

					println "Configured File: " + installFile.getAbsolutePath();
					LOG.warn("Configured File: " + installFile.getAbsolutePath());


					//rename init.rsremote if instancename is different
					if (os.contains("Linux") || os.contains("SunOS"))
					{
						if (instanceName != (installFile.getName() - "init."))
						{
							def newInstallFile = new File(dist + "/" + instanceName + "/bin/" + "init." + instanceName);
							if (newInstallFile.exists() && !newInstallFile.delete())
							{
								LOG.warn("Failed to Delete Instance Init File: " + newInstallFile.getAbsolutePath());
							}
							if (installFile.renameTo(newInstallFile))
							{
								println "Renamed Init File to " + newInstallFile.getAbsolutePath();
								LOG.warn("Renamed Init File to " + newInstallFile.getAbsolutePath());
							}
							else
							{
								println "Failed to Rename Init File to " + newInstallFIle.getAbsolutePath();
								LOG.warn("Failed to Rename Init File to " + newInstallFIle.getAbsolutePath());
							}
						}
					}
				}
				else
				{
					if (installFile != null)
					{
						println "Cannot Find " + instanceName + " install script: " + installFile.getAbsolutePath();
						LOG.warn("Cannot Find " + instanceName + " install script: " + installFile.getAbsolutePath());
					}
					else
					{
						println "Unknown OS Type: " + os;
						LOG.warn("Unknown OS Type: " + os);
					}
				}
			}
        }
        else
        {
            println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
            LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
        }
    }
    catch (Exception e)
    {
        println "Unexpected Exception while Running Configuration: " + e.getMessage();
        LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
    }
}
return null;
