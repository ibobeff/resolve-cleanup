import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String serviceFilename = "rsdataloader.service";
final String initLocationLnxRSDataLoader = "dcs/rsdataloader";

def help()
{
    println "Usage: rsdataloader-service-configure\n";
    println "This script will configure rsdataloader.service";
}
try
{
    def dist = MAIN.getDist();
    def config = MAIN.getConfigRSDataLoader();

    if (config && dist)
    {
        def os = System.getProperty("os.name");

        File serviceFile = null;
        if (os.contains("Linux"))
        {
            serviceFile = new File(dist + "/" + initLocationLnxRSDataLoader + "/" + serviceFilename);

            //Configure vault.service
            if (serviceFile != null && serviceFile.exists())
            {
                byte[] serviceFileBytes = new byte[serviceFile.length()];
                FileInputStream fis = new FileInputStream(serviceFile);
                fis.read(serviceFileBytes);
                fis.close();

                String serviceFileStr = new String(serviceFileBytes);

                dist = Matcher.quoteReplacement(dist);
                serviceFileStr = serviceFileStr.replaceAll("INSTALLDIR", dist);

                def user = config.getUser();
                serviceFileStr = serviceFileStr.replaceAll("User=.*", "User=" + user);

                serviceFileBytes = serviceFileStr.getBytes();
                FileOutputStream fos = new FileOutputStream(serviceFile);
                fos.write(serviceFileBytes);
                fos.close();

                println "Configured File: " + serviceFile.getAbsolutePath();
                LOG.warn("Configured File: " + serviceFile.getAbsolutePath());
            }
            else
            {
                println "Cannot Find rsdataloader init script: " + serviceFile.getAbsolutePath();
                LOG.warn("Cannot Find rsdataloader init script: " + serviceFile.getAbsolutePath());
            }
        }
        else
        {
            println "Skipping - systemd Service Setup for Linux Systems Only";
            LOG.info("Skipping - systemd Service Setup for Linux Systems Only: " + os);
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
