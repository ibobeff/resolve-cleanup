import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String userLinuxConfFilename = "rabbitmq/linux64/rabbitmq/sbin/setup_user.sh";
final String userSolarisConfFilename = "rabbitmq/solaris/rabbitmq/sbin/setup_user.sh";
final String userWindowsConfFilename = "rabbitmq/windows/rabbitmq/sbin/setup_user.bat";

def help()
{
    println "Usage: rsmq-user-configure\n"
    println "This script will configure the setup_user script";
}
try
{
    def config = MAIN.getConfigRSMQ();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        def os = System.getProperty("os.name");
        File userConfFile = null;

        if (os.contains("Win"))
        {
            userConfFile = new File(userWindowsConfFilename);
        }
        else if (os.contains("Linux"))
        {
            userConfFile = new File(userLinuxConfFilename);
        }
        else if (os.contains("SunOS"))
        {
            userConfFile = new File(userSolarisConfFilename);
        }

        //configure setup_user.sh

        if (userConfFile != null && userConfFile.exists())
        {
            byte[] userConfFileBytes = new byte[userConfFile.length()];
            FileInputStream fis = new FileInputStream(userConfFile);
            fis.read(userConfFileBytes);
            fis.close();

            String userConfFileStr = new String(userConfFileBytes);

            def username = config.getUsername();
            def password = config.getP_assword();

            userConfFileStr = userConfFileStr.replaceFirst("add_user .*", "add_user $username $password".toString());
            userConfFileStr = userConfFileStr.replaceFirst("set_user_tags \\w+", "set_user_tags $username".toString());
            userConfFileStr = userConfFileStr.replaceFirst("set_permissions -p / \\w+", "set_permissions -p / $username".toString());

            userConfFileBytes = userConfFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(userConfFile);
            fos.write(userConfFileBytes);
            fos.close();

            println "Configured File: " + userConfFile.getAbsolutePath();
            LOG.warn("Configured File: " + userConfFile.getAbsolutePath());
        }
        else
        {
            println "Cannot Find setup_user script: " + userConfFile.getAbsolutePath();
            LOG.warn("Cannot Find setup_user script: " + userConfFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
