import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String neo4jFilename = "tomcat/webapps/resolve/WEB-INF/neo4j.properties";

def help()
{
    println "Usage: rsview-neo4j-configure\n"
    println "This script will configure the rsview's neo4j.properties";
}
try
{
    def config = MAIN.getConfigRSView();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        //Configure neo4j.properties
        File neo4jFile = new File(dist + "/" + neo4jFilename);

        if (neo4jFile.exists())
        {
            byte[] neo4jFileBytes = new byte[neo4jFile.length()];
            FileInputStream fis = new FileInputStream(neo4jFile);
            fis.read(neo4jFileBytes);
            fis.close();

            String neo4jFileStr = new String(neo4jFileBytes);

            def replaceMap = config.rsviewNeo4jReplaceValues();

            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                value = Matcher.quoteReplacement(value);
                neo4jFileStr = neo4jFileStr.replaceAll(regex, value);
            }
            

            neo4jFileBytes = neo4jFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(neo4jFile);
            fos.write(neo4jFileBytes);
            fos.close();

            println "Configured File: " + neo4jFile.getAbsolutePath();
            LOG.warn("Configured File: " + neo4jFile.getAbsolutePath());
        }
        else
        {
            println "Cannot Find RSView neo4j file: " + neo4jFile.getAbsolutePath();
            LOG.warn("Cannot Find RSView neo4j file: " + neo4jFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
