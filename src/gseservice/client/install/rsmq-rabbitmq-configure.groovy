import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String rabbitmqConfFilename = "rabbitmq/config/rabbitmq.config";

def help()
{
    println "Usage: rsmq-rabbitmq-configure\n"
    println "This script will configure the rabbitmq.conf scripts for primary and backup rsmq";
}
try
{
    def config = MAIN.getConfigRSMQ();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        def os = System.getProperty("os.name");
        File rabbitmqConfFile = new File(rabbitmqConfFilename);

        //configure rabbitmq.conf

        if (rabbitmqConfFile != null && rabbitmqConfFile.exists())
        {
            byte[] rabbitmqConfFileBytes = new byte[rabbitmqConfFile.length()];
            FileInputStream fis = new FileInputStream(rabbitmqConfFile);
            fis.read(rabbitmqConfFileBytes);
            fis.close();
            def rabbitmqConfFileStr = new String(rabbitmqConfFileBytes);

            dist = dist.replaceAll("\\\\", "/");
            dist = Matcher.quoteReplacement(dist);
            rabbitmqConfFileStr = rabbitmqConfFileStr.replaceAll("INSTALLDIR", dist);

            def sslPort = config.getBrokerPort();
            if (!config.isPrimary())
            {
                sslPort = config.getBrokerPort2();
            }
            if (sslPort && sslPort.trim().matches("\\d+"))
            {
                sslPort = sslPort.trim().toInteger();
                rabbitmqConfFileStr = rabbitmqConfFileStr.replaceAll("ssl_listeners, \\[\\d+", "ssl_listeners, [" + sslPort);
                rabbitmqConfFileStr = rabbitmqConfFileStr.replaceAll("tcp_listeners, \\[\\d+", "tcp_listeners, [" + (sslPort-1));
            }
            else
            {
                println "Invalid Port Settings for RSMQ: " + sslPort;
                LOG.error("Invalid Port Settings for RSMQ: " + sslPort);
            }

            def managementPort = config.getManagementPort();
            if (managementPort && managementPort.trim().matches("\\d+"))
            {
                managementPort = managementPort.trim().toInteger();
                rabbitmqConfFileStr = rabbitmqConfFileStr.replaceAll("rabbitmq_management, (.+)port, \\d+", "rabbitmq_management, \$1port, " + managementPort);
            }

            def primaryNodeName = config.getPrimaryNodeName() ? config.getPrimaryNodeName() : config.getBrokerAddr();
            if (primaryNodeName)
            {
                def clusterNodesRegex = "cluster_nodes, \\{\\[.+\\]";
                def clusterNodesValue = "cluster_nodes, {['rabbit@" + primaryNodeName;
                def backupNodeName = config.getBackupNodeName() ? config.getBackupNodeName() : config.getBrokerAddr2();
                if (backupNodeName)
                {
                    clusterNodesValue += "','rabbit@" + backupNodeName;
                }
                clusterNodesValue += "']";
                rabbitmqConfFileStr = rabbitmqConfFileStr.replaceFirst(clusterNodesRegex, clusterNodesValue);
            }
            else
            {
                println "WARNING!!! Unable to determine rabbitmq cluster hosts/ips";
                LOG.error("Node Name and BrokerAddr value missing from config");
            }

            rabbitmqConfFileBytes = rabbitmqConfFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(rabbitmqConfFile);
            fos.write(rabbitmqConfFileBytes);
            fos.close();

            println "Configured File: " + rabbitmqConfFile.getAbsolutePath();
            LOG.warn("Configured File: " + rabbitmqConfFile.getAbsolutePath());
        }
        else
        {
            println "Cannot Find rabbitmq.conf file: " + rabbitmqConfFile.getAbsolutePath();
            LOG.warn("Cannot Find rabbitmq.conf file: " + rabbitmqConfFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
