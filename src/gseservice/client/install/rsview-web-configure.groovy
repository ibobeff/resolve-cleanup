import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

final String webFilename = "tomcat/webapps/resolve/WEB-INF/web.xml";

def help()
{
    println "Usage: rsview-web-configure\n"
    println "This script will configure rsview's web.xml";
}
try
{
    def config = MAIN.getConfigRSView();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        File webFile = null;
        webFile = new File(dist + "/" + webFilename);

        def replaceMap = config.rsviewWebReplaceValues();

        //Configure web.conf
        if (webFile != null && webFile.exists())
        {
            byte[] webFileBytes = new byte[webFile.length()];
            FileInputStream fis = new FileInputStream(webFile);
            fis.read(webFileBytes);
            fis.close();

            String webFileStr = new String(webFileBytes);
            def webFileStrBefore = webFileStr;

            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                webFileStr = webFileStr.replaceFirst(regex, value);
            }

            if (webFileStrBefore != webFileStr)
            {
                println "WARNING!!! Changes to the web.xml file will require an RSView restart";
                LOG.warn("WARNING!!! Changes to the web.xml file will require an RSView restart");
                webFileBytes = webFileStr.getBytes();
                FileOutputStream fos = new FileOutputStream(webFile);
                fos.write(webFileBytes);
                fos.close();

                println "Configured File: " + webFile.getAbsolutePath();
                LOG.warn("Configured File: " + webFile.getAbsolutePath());
            }
            else
            {
                println "No Change to: " + webFile.getAbsolutePath();
                LOG.warn("No Change to: " + webFile.getAbsolutePath());
            }
        }
        else
        {
            println "Cannot Find rsview's install script: " + webFile.getAbsolutePath();
            LOG.warn("Cannot Find rsview's install script: " + webFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
