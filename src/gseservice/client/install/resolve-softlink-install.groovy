final String linkFolder = "bin";

def help()
{
    println "Usage: rsremote-softlink-install [<install path>]\n";
    println "This script will install softlinks for all installed components";
    println "in the rc3.d folder (unless an <install path> is specified)";
    println "This script is for Unix installs only";
    println "This script must be run by a user who has permission to install the softlinks";
}
try
{
    def dist = MAIN.getDist();

    if (dist)
    {
        def installPath = "/etc/rc3.d";
        if (args.length > 1)
        {
            installPath = args[1];
        }
        def install = new File(installPath);

        if (install.isDirectory())
        {
            def os = System.getProperty("os.name");
            if (os.contains("Linux") || os.contains("SunOS"))
            {
                def cmdBase = "ln -s " + dist;
                def rsremote = MAIN.isBlueprintRSRemote();
                if (rsremote)
                {
                    def instances = MAIN.getRSRemoteInstances();
                    if (instances)
                    {
                        for (instance in instances)
                        {
                            def output = new StringBuffer();
                            def link = "${installPath}/S94${instance}";
                            cmd = cmdBase + "/${instance}/bin/init.${instance} ${link}";
                            LOG.warn("Executing command: " + cmd);
                            println "Creating Softlink: " + link;

                            def proc = cmd.execute();
                            proc.consumeProcessOutput(output, output);
                            proc.waitFor();
                            Thread.sleep(500);
                            println output.toString();
                            LOG.warn("output: " + output.toString());
                        }
                    }
                }

                def rscontrol = MAIN.isBlueprintRSControl();
                if (rscontrol)
                {
                    def output = new StringBuffer();
                    def link = "${installPath}/S94rscontrol";
                    cmd = cmdBase + "/rscontrol/bin/init.rscontrol ${link}";
                    LOG.warn("Executing command: " + cmd);
                    println "Creating Softlink: " + link;

                    def proc = cmd.execute();
                    proc.consumeProcessOutput(output, output);
                    proc.waitFor();
                    Thread.sleep(500);
                    println output.toString();
                    LOG.warn("output: " + output.toString());
                }

                def rsview = MAIN.isBlueprintRSView();
                if (rsview)
                {
                    def output = new StringBuffer();
                    def link = "${installPath}/S93rsview";
                    cmd = cmdBase + "/tomcat/bin/init.rsview ${link}";
                    LOG.warn("Executing command: " + cmd);
                    println "Creating Softlink: " + link;

                    def proc = cmd.execute();
                    proc.consumeProcessOutput(output, output);
                    proc.waitFor();
                    Thread.sleep(500);
                    println output.toString();
                    LOG.warn("output: " + output.toString());
                }

                def rsmq = MAIN.isBlueprintRSMQ();
                if (rsmq)
                {
                    if (os.contains("Linux"))
                    {
                        os = "linux";
                    }
                    if (os.contains("SunOS"))
                    {
                        os = "solaris";
                    }
                    def output = new StringBuffer();
                    def link = "${installPath}/S92rsmq";
                    cmd = cmdBase + "/rabbitmq/${os}/rabbitmq/sbin/init.rsmq ${link}";
                    LOG.warn("Executing command: " + cmd);
                    println "Creating Softlink: " + link;

                    def proc = cmd.execute();
                    proc.consumeProcessOutput(output, output);
                    proc.waitFor();
                    Thread.sleep(500);
                    println output.toString();
                    LOG.warn("output: " + output.toString());
                }

				def zookeeper = MAIN.isBlueprintDCSZookeeper();
				if (zookeeper)
				{
					def output = new StringBuffer();
					def link = "${installPath}/S98zookeeper";
					cmd = cmdBase + "/dcs/zookeeper-3.4.12/init.zookeeper ${link}";
					LOG.warn("Executing command: " + cmd);
					println "Creating Softlink: " + link;

					def proc = cmd.execute();
					proc.consumeProcessOutput(output, output);
					proc.waitFor();
					Thread.sleep(500);
					println output.toString();
					LOG.warn("output: " + output.toString());
				}
				
				def vault = MAIN.isBlueprintDCSVault();
				if (vault)
				{
					def output = new StringBuffer();
					def link = "${installPath}/S96vault";
					cmd = cmdBase + "/dcs/vault_0.11/init.vault ${link}";
					LOG.warn("Executing command: " + cmd);
					println "Creating Softlink: " + link;

					def proc = cmd.execute();
					proc.consumeProcessOutput(output, output);
					proc.waitFor();
					Thread.sleep(500);
					println output.toString();
					LOG.warn("output: " + output.toString());
				}
				
                def rsmgmt = MAIN.isBlueprintRSMgmt();
                if (rsmgmt)
                {
                    def output = new StringBuffer();
                    def link = "${installPath}/S95rsmgmt";
                    cmd = cmdBase + "/rsmgmt/bin/init.rsmgmt ${link}";
                    LOG.warn("Executing command: " + cmd);
                    println "Creating Softlink: " + link;

                    def proc = cmd.execute();
                    proc.consumeProcessOutput(output, output);
                    proc.waitFor();
                    Thread.sleep(500);
                    println output.toString();
                    LOG.warn("output: " + output.toString());
                }
            }
            else
            {
                LOG.warn("Windows System, Skipping Softlink");
            }
        }
        else
        {
            println install.getAbsolutePath() + " is not a valid Path to Install Softlinks";
            LOG.warn(install.getAbsolutePath() + " is not a valid Path to Install Softlinks");
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Installation";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Installation");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Installation: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Installation: " + e.getMessage(), e);
}
return null;
