import java.io.File;

def help()
{
    println "\nUsage: resolve-update-cleanup";
    println "This Script will Clean Up the file directory of any Update Files";
}

try
{
    println "Cleaning Up Updated Files";
    LOG.info("Cleaning Up file directory of any non-checksum files");
    def fileDir = new File("file");
    for (file in fileDir.listFiles())
    {
        if (!file.getName().endsWith("md5") && !file.getName().endsWith("-new.log") && !file.getName().endsWith("update_onetime.log"))
        {
            removeFile(file);
        }
    }
}
catch (Exception e)
{
    println "Unexpected Exception While Cleaning File Directory: " + e.getMessage();
    LOG.warn("Unexpected Exception While Cleaning File Directory: " + e.getMessage(), e);
}

return null;

def removeFile(File file)
{
    if (file.exists())
    {
        if (file.isDirectory())
        {
            for (child in file.listFiles())
            {
                removeFile(child);
            }
        }
        if (file.delete())
        {
            LOG.debug("Removed File: " + file.getAbsolutePath());
        }
    }

    return null;
}
