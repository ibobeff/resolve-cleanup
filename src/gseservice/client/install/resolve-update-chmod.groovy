import java.io.File;

def help()
{
    println "\nUsage: resolve-update-chmod";
    println "This Script will find all bin directories and set everything in them (except .bat files) to executable";
}

try
{
    def os = System.getProperty("os.name");
    if (os.contains("Win"))
    {
        println "Windows System Detected, Skipping Setting bin files to Executable";
        LOG.info("Windows System Detected, Skipping Setting bin files to Executable");
    }
    else
    {
        println "Setting scripts to executable";
        LOG.info("Setting all bin scripts to executable");
        def rootDir = new File("file");
        def files = findBinFiles(rootDir);
        if (os.contains("Linux"))
        {
            files.add(new File("file/rabbitmq/linux64/rabbitmq/sbin"));
            files.add(new File("file/rabbitmq/linux64/erlang/bin"));
            files.add(new File("file/rabbitmq/linux64/erlang/erts-7.3/bin"));
        }
        if (os.contains("SunOS"))
        {
            files.add(new File("file/rabbitmq/solaris/rabbitmq/sbin"));
            files.add(new File("file/rabbitmq/solaris/erlang/bin"));
            files.add(new File("file/rabbitmq/solaris/erlang/erts-7.3/bin"));
        }

        for (def dir in files)
        {
            if (dir.isDirectory())
            {
                LOG.debug("Running setExecutable for " + dir.getCanonicalPath());
                def executableFiles = dir.listFiles();
                for (def executableFile in executableFiles)
                {
                    if (!executableFile.getName().endsWith(".bat"))
                    {
                        executableFile.setExecutable(true);
                    }
                }
            }
        }
		def cookie = new File("file/rabbitmq/config/.erlang.cookie");
		cookie.setWritable(true,true);
    }
}
catch (Exception e)
{
    println "Unexpected Exception While Setting Scripts to Executable: " + e.getMessage();
    LOG.warn("Unexpected Exception While Setting Scripts to Executable: " + e.getMessage(), e);
}

return true;


def findBinFiles(File rootFile) {
    def result = [];
    def files = rootFile.listFiles(new DirFileFilter());
    for ( def file : files ) {
        if (file.getName().endsWith("bin"))
        {
            result.add(file);
        }
        else
        {
            result.addAll(findBinFiles(file))
        }
    }
    return result;
}

class DirFileFilter implements FileFilter
{
    boolean accept(File file)
    {
        return (file.isDirectory() && !file.getCanonicalPath().contains("rabbitmq"));
    }
}
