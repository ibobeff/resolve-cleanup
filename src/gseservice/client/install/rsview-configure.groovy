import java.io.File;
import com.resolve.util.XDoc;

final String configFilename = "tomcat/webapps/resolve/WEB-INF/config.xml";

def help()
{
    println "Usage: rsview-configure\n"
    println "This script will configure the rsview's config.xml";
}
try
{
    def config = MAIN.getConfigRSView();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        //Configure config.xml
        File configFile = new File(dist + "/" + configFilename);
        if (configFile.exists())
        {
            def configXPath = config.rsviewXPathValues();
            XDoc configDoc = new XDoc(configFile);
            
            configDoc.removeElement("./NOSQL");
            
            for (key in configXPath.keySet())
            {
                def value = configXPath.get(key);
                if (value instanceof String)
                {
                    configDoc.setStringValue(key, value);
                }
                else if (value instanceof List)
                {
                    if (!value.isEmpty() && value.get(0) instanceof Map)
                    {
                        configDoc.setListMapValue(key, value);
                    }
                    else if (!value.isEmpty() && value.get(0) instanceof String)
                    {
                        configDoc.setListValue(key, value);
                    }
                }
            }
            
            configDoc.removeElement("./CUSTOMSDKFIELDS");
            
            def sdkFieldsMap = config.getSdkFieldsMap();
            if (sdkFieldsMap != null && sdkFieldsMap.size() > 0)
            {
                String s = "";
                for (key in sdkFieldsMap.keySet())
                {
                    Map<String, String> sdkAttribMap = new HashMap<String, String>();
                    Map<String, String> localMap = sdkFieldsMap.get(key);
                    
                    sdkAttribMap.put("menuTitle", localMap.get("menuTitle"));
                    sdkAttribMap.put("implPrefixes", localMap.get("implPrefixes"));
                    
                    localMap.remove("menuTitle");
                    localMap.remove("implPrefixes");
                    
                    String partialKey = key.split("\\.")[0];
                    LOG.warn("Partial Key: " + partialKey);
                    if (!s.equals(partialKey))
                    {
                        if (configDoc.getNode("./CUSTOMSDKFIELDS/" + partialKey.toUpperCase()) == null)
                        {
                            configDoc.addMapValue("./CUSTOMSDKFIELDS/" + partialKey.toUpperCase(), sdkAttribMap);
                        }
                        else
                        {
                            LOG.trace ("Node with ./CUSTOMSDKFIELDS/" + partialKey.toUpperCase() + " is already present, not creating it");
                        }
                        
                        s = partialKey;
                    }
                    configDoc.addMapValue("./CUSTOMSDKFIELDS/" + partialKey.toUpperCase() + "/UI", localMap);
                }
            }
            
            configDoc.removeElement("./DEFAULTINTERVAL");
            
            def intervalMap = config.getIntervalMap();
            
            if (intervalMap != null && intervalMap.size() > 0)
            {
                for (key in intervalMap.keySet())
                {
                    configDoc.addStringValue("./DEFAULTINTERVAL/" + key.toUpperCase() + "/@interval", intervalMap.get(key));
                }
            }
            
            
            configDoc.removeElement("./FILTERAUTHTYPE");
            
            def authTypeMap = config.getAuthTypeMap();
            
            if (authTypeMap != null && authTypeMap.size() > 0)
            {
                for (key in authTypeMap.keySet())
                {
                    configDoc.addStringValue("./FILTERAUTHTYPE/" + key.toUpperCase() + "/@type", authTypeMap.get(key));
                }
            }
            
            
            
            configDoc.toPrettyFile(configFile);

            println "Configured File: " + configFile.getAbsolutePath();
            LOG.warn("Configured File: " + configFile.getAbsolutePath());
        }
        else
        {
            println "Cannot Find RSView config file: " + configFile.getAbsolutePath();
            LOG.warn("Cannot Find RSView config file: " + configFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
