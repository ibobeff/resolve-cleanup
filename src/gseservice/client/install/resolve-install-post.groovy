import groovy.sql.Sql;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;
import com.resolve.util.SysId;
import com.resolve.util.MCPLog;
import com.resolve.util.MCPConstants;
import com.resolve.util.RESTUtils;

def help()
{
    println "Usage: resolve-install-post\n"
    println "This script will run post installation imports and statup";
}

MCPLog mcpLog = null;

try
{
    def installStatusFileName = MAIN.getDist() + "/rsmgmt/log/" + MCPConstants.MCP_RESOLVE_INSTALL_POST_STATUS_LOGFILE;
    mcpLog = new MCPLog(installStatusFileName, true, true, MCPConstants.MCP_LOG_ITEM_INSTALL_POST);
    mcpLog.write(MCPConstants.MCP_FIELD_KEY_START, "Post Installation Started");

    //start rsmq
    def os = System.getProperty("os.name");

    def rsconsoleUsername = "admin";
    def rsconsolePassword = "resolve";

    if (MAIN.isBlueprintRSConsole() && BLUEPRINT.getProperty("RSCONSOLE_USERNAME"))
    {
        rsconsoleUsername = BLUEPRINT.getProperty("RSCONSOLE_USERNAME");
    }
    if (MAIN.isBlueprintRSConsole() && BLUEPRINT.getProperty("RSCONSOLE_PASSWORD"))
    {
        rsconsolePassword = BLUEPRINT.getProperty("RSCONSOLE_PASSWORD");
    }

    if (MAIN.isBlueprintRSMQ())
    {
        println "starting RSMQ";
        LOG.info("Starting RSMQ - mgmt/Start RSMQ 1");
        mcpLog.executeStart("mgmt/Start RSMQ 1");
        MAIN.executeCommand("mgmt/Start RSMQ 1");
        mcpLog.executeEnd("mgmt/Start RSMQ 1");

        def config = MAIN.getConfigRSMQ();

        def product = config.getProduct();
        if (Constants.ESB_RABBITMQ.equalsIgnoreCase(product))
        {
            println "Setup RabbitMQ User";
            mcpLog.executeStart("install/rsmq-user-configure");
            MAIN.executeCommand("install/rsmq-user-configure");
            mcpLog.executeEnd("install/rsmq-user-configure");

            def result = "nodedown";
            def i=0;
			def hostname = "localhost"
//          def hostname = config.getBrokerAddr();
//          if (!config.isPrimary())
//          {
//          	hostname ="localhost"
//          }
            def port = config.getManagementPort();
            def username = config.getUsername();
            def password = config.getP_assword();
            def rootUrl = "http://" + hostname + ":" + port + "/api";

            while (result.equals("nodedown") && i < 10)
            {
                if (i > 0)
                {
                    Thread.sleep(1000);
                }
                try
                {
                    def statusResult = RESTUtils.isExists(rootUrl, "guest", "guest");
                    LOG.info("RSMQ Client Response Code 200? " + statusResult);
                    if (statusResult)
                    {
                        result = "up";
                        mcpLog.info("The RSMQ Instans appears to have started properly");
                        break;
                    }
                }
                catch (Exception e)
                {
                    LOG.info("RSMQ Test Failed - " + e.getMessage());
                    LOG.trace("RSMQ Test Failed", e);
                }
                i++;
            }

            if (result.equals("nodedown"))
            {
                println "WARNING!!! The RSMQ Instance Does not appear to have started properly";
                LOG.error("The RSMQ Instance Does not appear to have started properly");
                mcpLog.error("The RSMQ Instance Does not appear to have started properly");
            }

            if (os.contains("Win"))
            {
                try
                {
                    def addUserUrl = rootUrl + "/users/" + username;
                    def params = [:];
                    params.put("password", password);
                    params.put("tags", "administrator");
                    LOG.info("add user: " + RESTUtils.put(addUserUrl, params, "guest", "guest"));

                    def setPermissionsUrl = rootUrl + "/permissions/%2f/" + username;
                    params = [:];
                    params.put("configure", ".*");
                    params.put("write", ".*");
                    params.put("read", ".*");
                    LOG.info("set permissions: " + RESTUtils.put(setPermissionsUrl, params, "guest", "guest"));

                    if (RESTUtils.isExists(rootUrl, username, password))
                    {
                        System.out.println("RSMQ User created successfully");
                        LOG.info("RabbitMQ User Created");
                        mcpLog.info("RabbitMQ User Created");

                        def deleteUrl = rootUrl + "/users/guest";
                        LOG.info("delete guest: " + RESTUtils.delete(deleteUrl, username, password));
                    }
                    else
                    {
                        System.out.println("WARNING!!! RSMQ User creation failed");
                        LOG.error("RabbitMQ User Creation Failed");
                        mcpLog.error("RabbitMQ User Creation Failed");
                    }
                }
                catch (Exception e)
                {
                    System.out.println("WARNING!!! RSMQ User creation failed");
                    LOG.error("RabbitMQ User Creation Failed", e);
                    mcpLog.error("RabbitMQ User Creation Failed - " + e.getMessage());
                }
            }
        }
    }

    if (MAIN.isBlueprintRSSearch())
    {
        println "starting RSSearch";
        LOG.info("Starting RSSEARCH - mgmt/Start RSSEARCH 1");
        mcpLog.executeStart("mgmt/Start RSSEARCH 1");
        MAIN.executeCommand("mgmt/Start RSSEARCH 1");
        mcpLog.executeEnd("mgmt/Start RSSEARCH 1");

        print "Waiting for RSSearch Startup.";
        def result = "red";
        def i=0;

        def hostname = BLUEPRINT.getProperty("LOCALHOST");
        def port = BLUEPRINT.getProperty("rssearch.yml.http.port");
        def rootUrl = "http://" + hostname + ":" + port + "/_cluster/health?wait_for_status=yellow&timeout=50s";

        def startTime = System.currentTimeMillis();
        while (result.equals("red") && startTime > (System.currentTimeMillis() - 600000))
        {
            if (i > 0)
            {
                Thread.sleep(1000);
                print ".";
            }
            try
            {
                def statusResult = RESTUtils.get(rootUrl, null, null);
                LOG.info("RSSearch Client Response Code 200? " + statusResult);
                if (statusResult)
                {
                    def resultMatcher = statusResult =~ /.*"status":"(\w+)"/;
                    if (resultMatcher.find())
                    {
                        result = resultMatcher.group(1);
                    }
                }
            }
            catch (Exception e)
            {
                if (!e.getMessage().matches("Connection to .+ refused"))
                {
                    LOG.warn("RSSearch Startup Test Failed", e);
                }
            }
            i++;
        }
        if (result == "red")
        {
            println "\nWARNING!!! RSSearch Status is unavailable or 'red'";
            LOG.warn("RSSearch Status is unavailable or 'red'");
        }
        else
        {
            println "\nRSSearch started";
            LOG.info("RSSearch started");
        }

        /* removing snapshot since shared fs is required for cluster
        rootUrl = "http://" + hostname + ":" + port + "/_snapshot/resolve_backup";
        def jsonParams = "{ \"type\": \"fs\", \"settings\": { \"location\": \"" + MAIN.getDist() + "/elasticsearch/snapshots\", \"compress\": true } }";

        LOG.info("Shapshot Creation using URL: " + url + " and params: " + jsonParams);
        def backupResult = 500;
        try
        {
            backupResult = RESTUtils.put(rootUrl, jsonParams, null, null);
        }
        catch (Exception e)
        {
            LOG.error("Exception Creating Snapshot Repository", e);
        }

        if (backupResult != 200)
        {
            println "\nWARNING!!! RSSearch Snapshot Repository Failed to Create, Scheduled Backups will Fail";
            LOG.error("RSSearch Snapshot Repository Failed to Create, error code: " + backupResult);
        }
        else
        {
            LOG.info("RSSearch Shapshot Repository Created");
        }
        */
    }
	
	if (MAIN.isBlueprintDCSRSDataLoader()) {
		println "starting RSDataLoader";
		LOG.info("Starting RSDataLoader - mgmt/Start rsdataloader 1");
		mcpLog.executeStart("mgmt/Start rsdataloader 1");
		MAIN.executeCommand("mgmt/Start rsdataloader 1");
		mcpLog.executeEnd("mgmt/Start rsdataloader 1");
	}
	
	if (MAIN.isBlueprintDCSRSReporting()) {
		println "starting RSReporting";
		LOG.info("Starting RSReporting - mgmt/Start rsreporting 1");
		mcpLog.executeStart("mgmt/Start rsreporting 1");
		MAIN.executeCommand("mgmt/Start rsreporting 1");
		mcpLog.executeEnd("mgmt/Start rsreporting 1");
	}
	
    def rsviewStarted = false;

    def sql;
    def importSql;
    def importCount = -1;
    def rsviewStartCount = 0;
    def rsviewPrimary = false;

    if (MAIN.isBlueprintRSView() || MAIN.isBlueprintRSControl())
    {
        rsviewPrimary = MAIN.isBlueprintRSView() && MAIN.getConfigRSView().isPrimary();
        if (rsviewPrimary)
        {
            sql = "select count(*) as COUNT from resolve_event where u_value LIKE '%RSVIEW%'";

            if (MAIN.isDBConnected())
            {
                def statement = DB.createStatement();
                def startRS;
                try
                {
                    LOG.debug("Attempting RSView Start/Install Query: " + sql.toString());
                    startRS = statement.executeQuery(sql);
                    if (startRS.next())
                    {
                        rsviewStartCount = startRS.getInt(1);
                        LOG.debug("Found " + rsviewStartCount + " instances of event in database");
                    }
                    else
                    {
                        LOG.error("Unable to determine the number of instances of the event in the database");
                    }
                }
                catch (Exception e)
                {
                    println "Unable to monitor for Installation Completion";
                    LOG.error("Failed to Query for RSView or Installation Finished event, will not be able to monitor for Completion", e);
                    mcpLog.error("Failed to Query for RSView or Installation Finished event, will not be able to monitor for Completion: " + e.getMessage());
                }
                finally
                {
                    startRS?.close();
                    statement?.close();
                }
            }

            println "starting RSVIEW";
            LOG.info("Starting RSVIEW - mgmt/Start RSVIEW 1");
            mcpLog.executeStart("mgmt/Start RSVIEW 1");
            MAIN.executeCommand("mgmt/Start RSVIEW 1");
            mcpLog.executeEnd("mgmt/Start RSVIEW 1");
        }
        else
        {
            //RSView Not Primary
            if (MAIN.isBlueprintRSView())
            {
                def cmd;
                def sb = new StringBuffer();
                if (os.contains("Win"))
                {
                    cmd = "tomcat/bin/run.bat";
                }
                else
                {
                    cmd = "tomcat/bin/run.sh";
                }
                cmd += " NEO_CLUSTER";
                
                LOG.warn("Running start command: " + cmd);
                        
                def proc = cmd.execute();
                proc.consumeProcessOutput(sb, sb);
                proc.waitFor();
                MAIN.println(sb.toString());
                LOG.info("Start Output: " + sb.toString());
            }

            def buildVersion = MAIN.release.getVersion();
            sql = "select count(*) as COUNT from resolve_event where u_value='INSTALL " + buildVersion + " Finished'";
            sql +=" or u_value like 'PRIMARY UPGRADE " + buildVersion + "% Finished'";
            sql +=" or u_value like 'PRIMARY UPDATE " + buildVersion + "% Finished'";

        }
        if (sql)
        {
            if (MAIN.isDBConnected())
            {
                def statement = DB.createStatement();

                print "Waiting For Initialization to Finish";
                while (true)
                {
                    def rs;
                    try {
                        rs = statement.executeQuery(sql);
                    } catch (SQLException sqle) {
                        if (sqle.getMessage().contains("doesn't exist") || sqle.getMessage().contains("does not exist")) {
                            Log.log.warn("Table resolve_event doesn't exist yet, sleeping then re-trying");
                            Thread.sleep(20000);
                            print ".";
                            continue;
                        } else {
                            throw sqle;
                        }
                    }
                    if (rs.next())
                    {
                        def eventCount = rs.getInt(1);
                        if ((rsviewPrimary && eventCount == rsviewStartCount) || (!rsviewPrimary && eventCount == 0))
                        {
                            //still waiting for RSView startup/Primary Imports Completed
                            Thread.sleep(20000);
                            print ".";
                            continue;
                        }
                        if (rsviewPrimary)
                        {
                            LOG.debug("RSView Startup Detected");
                        }
                        else
                        {
                            LOG.debug("Primary Import Completion Detected");
                        }

                        rsviewStarted = true;
                        if (rsviewPrimary && MAIN.isBlueprintRSConsole())
                        {
                            def modules = BLUEPRINT.getProperty("resolve.import");
                            if (modules)
                            {
                                def importArgs = "";
                                def lastImportModule = "";
                                for (module in modules.split(","))
                                {
                                    if (module.trim())
                                    {
                                        if (importArgs)
                                        {
                                            importArgs += ";" + module;
                                        }
                                        else
                                        {
                                            importArgs = module;
                                        }
                                        lastImportModule = module;
                                    }
                                }
                                if (importArgs)
                                {
                                    importSql = "select count(*) as COUNT from resolve_impex_log l join resolve_impex_module m on m.sys_id = l.u_impex_module where UPPER(m.u_name) = UPPER('$lastImportModule')";
                                    def importRS;
                                    try
                                    {
                                        LOG.debug("Attempting Impex Monitor Query: " + importSql.toString());
                                        importRS = statement.executeQuery(importSql);
                                        if (importRS.next())
                                        {
                                            importCount = importRS.getInt(1);
                                            LOG.debug("Found " + importCount + " instances of " + lastImportModule + " log in the database");
                                        }
                                        else
                                        {
                                            println "Unable to monitor for Impex Completion";
                                            LOG.error("No Results Returned for Impex Monitor Query");
                                            mcpLog.error("No Results Returned for Impex Monitor Query");
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        println "Unable to monitor for Impex Completion";
                                        LOG.error("Failed to Query for Impex Log Count, will not be able to monitor for Import Completion", e);
                                        mcpLog.error("Failed to Query for Impex Log Count, will not be able to monitor for Import Completion: " + e.getMessage());
                                    }
                                    finally
                                    {
                                        importRS?.close();
                                    }


                                    importArgs += ";-u;resolve.maint;--block-index";

                                    println "\nImporting Default Modules";
                                    LOG.debug("Importing Default Modules:" + importArgs);
                                    def importCmd = "";
                                    if (os.contains("Win"))
                                    {
                                        importCmd = ["rsconsole/bin/run.bat", "-u", rsconsoleUsername, "-p", rsconsolePassword, "-f", "impex/ImportModule", "\"-FD" + importArgs + "\""];
                                    }
                                    else
                                    {
                                        importCmd = ["rsconsole/bin/run.sh", "-u", rsconsoleUsername, "-p", rsconsolePassword, "-f", "impex/ImportModule", "-FD" + importArgs];
                                    }
                                    def proc = importCmd.execute();
                                    def sout = new StringBuffer();
                                    proc.consumeProcessOutput(sout, sout);
                                    proc.waitFor();
                                    LOG.debug("Import Process Output: " + sout.toString());
                                }
                            }
                            else
                            {
                                println "\nNo Modules Configured for Import in Blueprint";
                                LOG.warn("No Modules Configured for Import in Blueprint");
                                mcpLog.warn("No Modules Configured for Import in Blueprint");
                            }
                        }
                        break;
                    }
                    else
                    {
                        println "WARNING: UNABLE TO DETERMINE WHEN PRIMARY RSVIEW SETUP HAS COMPLETED";
                        LOG.error("WARNING: UNABLE TO DETERMINE WHEN PRIMARY RSVIEW SETUP HAS COMPLETED");
                        mcpLog.error("WARNING: UNABLE TO DETERMINE WHEN PRIMARY RSVIEW SETUP HAS COMPLETED");
                        break;
                    }
                    rs?.close();
                }
                println "";

                if (importCount >= 0)
                {
                    print "Waiting for the Imports to Complete";
                    while (true)
                    {
                        def rs = statement.executeQuery(importSql);
                        if (rs.next())
                        {
                            def newImportCount = rs.getInt(1);
                            if (newImportCount > importCount)
                            {
                                LOG.debug("Import Log Count Increased " + importCount + " -> " + newImportCount);
                                println " Imports Completed";
                                break;
                            }
                        }
                        rs?.close();
                        Thread.sleep(20000);
                        print ".";
                    }
                    println "";
                }
                else
                {
                    println "No Imports to Monitor";
                    LOG.debug("No Imports to Monitor");
                }
            }
            else
            {
                println "Unable to Determine if Primary RSView has finished creating DB Tables";
                LOG.warn("DB Connection Failed: Unable to Determine if Primary RSView has finished creating DB Tables");
                mcpLog.warn("DB Connection Failed: Unable to Determine if Primary RSView has finished creating DB Tables");
            }
        }
        else
        {
            println "Unable to Determine if Primary RSView has finished creating DB Tables";
            LOG.warn("Unknown DB Type: " + dbType + ": Unable to Determine if Primary RSView has finished creating DB Tables");
            mcpLog.warn("Unknown DB Type: " + dbType + ": Unable to Determine if Primary RSView has finished creating DB Tables");
        }
        if (rsviewPrimary)
        {
            //finish install operations
            def installCmd = "";
            if (os.contains("Win"))
            {
                installCmd = ["rsconsole/bin/run.bat", "-u", rsconsoleUsername, "-p", rsconsolePassword, "-f", "internal/InstallOperations"];
            }
            else
            {
                installCmd = ["rsconsole/bin/run.sh", "-u", rsconsoleUsername, "-p", rsconsolePassword, "-f", "internal/InstallOperations"];
            }
            def proc = installCmd.execute();
            def sout = new StringBuffer();
            proc.consumeProcessOutput(sout, sout);
            proc.waitFor();
            LOG.debug("Install Operations Process Output: " + sout.toString());

            def currTime = new Timestamp(GMTDate.getTime());
            def buildVersion = MAIN.release.getVersion();
            def sysId = SysId.generate(this);
            def insertSql = "insert into resolve_event (sys_id, u_value, sys_updated_by, sys_updated_on, sys_created_by, sys_created_on)";
            insertSql += " values(?, 'INSTALL " + buildVersion + " Finished', 'system', ?, 'system', ?)";

            //insert into resolve_event
            LOG.debug("Inserting Resolve Event: " + insertSql);
            def conn = new Sql(DB);
            def insertRow = conn.executeUpdate(insertSql, [sysId, currTime, currTime]);
            if (insertRow == 0)
            {
                println "WARNING: UNABLE TO INSERT PRIMARY INSTALLATION EVENT, SECONDARY INSTALLATIONS MAY TAKE LONGER THAN EXPECTED";
                LOG.error("WARNING: UNABLE TO INSERT PRIMARY INSTALLATION EVENT, SECONDARY INSTALLATIONS MAY TAKE LONGER THAN EXPECTED");
                mcpLog.error("WARNING: UNABLE TO INSERT PRIMARY INSTALLATION EVENT, SECONDARY INSTALLATIONS MAY TAKE LONGER THAN EXPECTED");
            }
        }
        else
        {
            if (rsviewStarted)
            {
                println "Re-Starting Secondary RSVIEW";
                LOG.info("Stopping Secondary RSVIEW - mgmt/Stop RSVIEW");
                mcpLog.executeStart("mgmt/Stop RSVIEW");
                MAIN.executeCommand("mgmt/Stop RSVIEW");
                mcpLog.executeEnd("mgmt/Stop RSVIEW");

                LOG.info("Starting RSVIEW - mgmt/Start RSVIEW");
                mcpLog.executeStart("mgmt/Start RSVIEW");
                MAIN.executeCommand("mgmt/Start RSVIEW");
                mcpLog.executeEnd("mgmt/Start RSVIEW");
            }
            else
            {
                println "WARNING: UNABLE TO DETERMINE IF PRIMARY RSVIEW HAS FINISHED WITH TABLE CREATION, SKIPPING RSVIEW STARTUP";
                LOG.error("WARNING: UNABLE TO DETERMINE IF PRIMARY RSVIEW HAS FINISHED WITH TABLE CREATION, SKIPPING RSVIEW STARTUP");
                mcpLog.error("WARNING: UNABLE TO DETERMINE IF PRIMARY RSVIEW HAS FINISHED WITH TABLE CREATION, SKIPPING RSVIEW STARTUP");
            }
        }
		
    }

    if (MAIN.isBlueprintRSMgmt())
    {
        println "starting RSMGMT";
        LOG.info("Starting RSMGMT - mgmt/Start RSMGMT");
        mcpLog.executeStart("mgmt/Start RSMGMT 1");
        MAIN.executeCommand("mgmt/Start RSMGMT 1");
        mcpLog.executeEnd("mgmt/Start RSMGMT 1");
    }

    if (MAIN.isBlueprintRSControl())
    {
        println "starting RSCONTROL";
        LOG.info("Starting RSCONTROL - mgmt/Start RSCONTROL 1");
        mcpLog.executeStart("mgmt/Start RSCONTROL 1");
        MAIN.executeCommand("mgmt/Start RSCONTROL 1");
        mcpLog.executeEnd("mgmt/Start RSCONTROL 1");
    }

    if (MAIN.isBlueprintRSRemote())
    {	
        def remotes = MAIN.getRSRemoteInstances();
        for (remote in remotes)
        {
            println "starting " + remote.toUpperCase();
            LOG.info("Starting " + remote.toUpperCase() + " - mgmt/Start " + remote.toUpperCase() + " 1");
            mcpLog.executeStart("mgmt/Start " + remote.toUpperCase() + " 1");
            MAIN.executeCommand("mgmt/Start " + remote.toUpperCase() + " 1");
            mcpLog.executeEnd("mgmt/Start " + remote.toUpperCase() + " 1");
        }
    }
    
    if (MAIN.isBlueprintRSArchive())
    {
        println "starting RSARCHIVE";
        LOG.info("Starting RSARCHIVE - mgmt/Start RSARCHIVE 1");
        mcpLog.executeStart("mgmt/Start RSARCHIVE 1");
        MAIN.executeCommand("mgmt/Start RSARCHIVE 1");
        mcpLog.executeEnd("mgmt/Start RSARCHIVE 1");
    }

    if (MAIN.isBlueprintLogstash())
    {
        println "starting LOGSTASH";
        LOG.info("Starting LOGSTASH - mgmt/Start LOGSTASH 1");
        mcpLog.executeStart("mgmt/Start LOGSTASH 1");
        MAIN.executeCommand("mgmt/Start LOGSTASH 1");
        mcpLog.executeEnd("mgmt/Start LOGSTASH 1");
    }
    
    if (MAIN.isBlueprinRSLog())
    {
        println "starting RSLOG";
        LOG.info("Starting RSLOG - mgmt/Start RSLOG 1");
        mcpLog.executeStart("mgmt/Start RSLOG 1");
        MAIN.executeCommand("mgmt/Start RSLOG 1");
        mcpLog.executeEnd("mgmt/Start RSLOG 1");
    }
}
catch (Exception e)
{
    println "\nUnexpected Exception, Cancelling Post-Installation\n" + e.getMessage();
    LOG.warn("Unexpected Exception, Cancelling Post-Installation", e);
    mcpLog.error("Unexpected Exception, Cancelling Post-Installation: " + e.getMessage());
}

mcpLog?.write(MCPConstants.MCP_FIELD_KEY_START, "Post Installation Ended");

return null;
