import java.util.regex.Matcher;
import com.resolve.util.MCPLog;
import com.resolve.util.MCPConstants;
import com.resolve.util.Constants;
import groovy.sql.Sql;

def help()
{
    println "Usage: resolve-install-db\n"
    println "This script will run installation DB setup";
}

def tableCheck(mcpLog, tableName)
{
    def existed = false;
    def connected = false;
	try
    {
        if (MAIN.isDBConnected())
        {
            connected = true;
        }
        if(connected) {
            def sql = new Sql(DB);
			def resolveSchema = BLUEPRINT.getProperty("DB_SCHEMA");
			def sqlString = "select 1 from " + resolveSchema + "." + tableName;
            def rs = sql.executeQuery(sqlString);
            existed = true;
            
        } else {
			println "can't connect to database";
			LOG.warn("can't connect to database");
			mcpLog.warn("can't connect to database");
		}
    }
    catch (Exception e)
    {
		if(!connected) {
			println "Unexpected Exception: " + e.getMessage();
			LOG.warn("Unexpected Exception: " + e.getMessage(), e);
			mcpLog.warn("Unexpected Exception, Cancelling DB Setup: " + e.getMessage());
		}
    }
	 return existed;
}

def statement;
MCPLog mcpLog = null;

try
{
    def installStatusFileName = MAIN.getDist() + "/rsmgmt/log/" + MCPConstants.MCP_RESOLVE_INSTALL_DB_STATUS_LOGFILE;
    mcpLog = new MCPLog(installStatusFileName, true, true, MCPConstants.MCP_LOG_ITEM_INSTALL_DB);
    mcpLog.write(MCPConstants.MCP_FIELD_KEY_START, "DB Installation Started");

    def primary = MAIN.isBlueprintRSView() && MAIN.getConfigRSView().isPrimary();
    if (primary)
    {
        def os = System.getProperty("os.name");
        def file;
        def dbType = BLUEPRINT.getProperty("DB_TYPE");
        if ("mysql".equalsIgnoreCase(dbType))
        {
            file = new File("mysql/init.sql");
        }
        else if ("oracle".equalsIgnoreCase(dbType) || "oracle12c".equalsIgnoreCase(dbType))
        {
            file = new File("oracle/init.sql");
        }
        else if ("db2".equalsIgnoreCase(dbType))
        {
            file = new File("db2/init.sql");
        }

        if (file && file.exists())
        {
            if ("mysql".equalsIgnoreCase(dbType))
            {
                def resolveSchema = BLUEPRINT.getProperty("DB_NAME");

                if (resolveSchema != "resolve")
                {
                    byte[] fileBytes = new byte[file.length()];
                    FileInputStream fis = new FileInputStream(file);
                    fis.read(fileBytes);
                    fis.close();
                    String fileStr = new String(fileBytes);

                    resolveSchema = Matcher.quoteReplacement(resolveSchema);
                    fileStr = fileStr.replaceAll("resolve;", resolveSchema + ";");

                    fileBytes = fileStr.getBytes();
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(fileBytes);
                    fos.close();
                }
            }

            mcpLog.executeStart("mgmt/ExecuteSQL \"" + file.getAbsolutePath()+"\"");
            MAIN.executeCommand("mgmt/ExecuteSQL \"" + file.getAbsolutePath()+"\"");
            mcpLog.executeEnd("mgmt/ExecuteSQL \"" + file.getAbsolutePath()+"\"");
            if(!tableCheck(mcpLog, "resolve_event")) {
                mcpLog.warn("installation fail. can't find resolve_event database table.");
                System.exit(-1);
            }
        }
        else
        {
            if (!file)
            {
                println "Unknown DB Type: " + dbType;
                LOG.warn("Unknown DB Type: " + dbType);
                mcpLog.warn("Unknown DB Type: " + dbType);
            }
            else
            {
                println "Cannot Find Init SQL File: " + file.getAbsolutePath();
                LOG.warn("Cannot Find Init SQL File: " + file.getAbsolutePath());
                mcpLog.warn("Cannot Find Init SQL File: " + file.getAbsolutePath());
            }
        }
    }
    else
    {
        println "Installation Not Configured As Primary, Skipping DB Setup";
        LOG.warn("Installation Not Configured As Primary, Skipping DB Setup");
        mcpLog.warn("Installation Not Configured As Primary, Skipping DB Setup");
    }
}
catch (Exception e)
{
    println "Unexpected Exception, Cancelling DB Setup\n" + e.getMessage();
    LOG.warn("Unexpected Exception, Cancelling DB Setup", e);
    mcpLog.warn("Unexpected Exception, Cancelling DB Setup: " + e.getMessage());
}
finally
{
    statement?.close();
}
mcpLog?.write(MCPConstants.MCP_FIELD_KEY_END, "DB Installation Ended");

return null;
