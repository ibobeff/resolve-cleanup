import java.io.File;
import com.resolve.util.XDoc;

final String configFilename = "config/config.xml";

String instanceName = "rslog";

def help()
{
    println "Usage: rslog-configure\n"
    println "This script will configure the rslog's config.xml";
}
try
{
    def config = MAIN.getConfigRSLog();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        //Configure config.xml
        File configFile = new File(dist + "/" + instanceName + "/" + configFilename);
        if (configFile.exists())
        {
            def configXPath = config.rslogXPathValues();
            XDoc configDoc = new XDoc(configFile);
            
            for (key in configXPath.keySet())
            {
                def value = configXPath.get(key);
                if (value instanceof String)
                {
                    configDoc.setStringValue(key, value);
                }
                else if (value instanceof List)
                {
                    if (!value.isEmpty() && value.get(0) instanceof Map)
                    {
                        configDoc.setListMapValue(key, value);
                    }
                    else if (!value.isEmpty() && value.get(0) instanceof String)
                    {
                        configDoc.setListValue(key, value);
                    }
                }
            }
            configDoc.toPrettyFile(configFile);

            println "Configured File: " + configFile.getAbsolutePath();
            LOG.warn("Configured File: " + configFile.getAbsolutePath());
        }
        else
        {
            println "Cannot Find " + instanceName + " config file: " + configFile.getAbsolutePath();
            LOG.warn("Cannot Find " + instanceName + " config file: " + configFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
