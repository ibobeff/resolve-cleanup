import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

final String logFilename = "config/log.cfg";
final String appenderTag = "AMQP_APPENDER";

def help()
{
    println "Usage: rsremote-log-configure <instance name>\n"
    println "This script will configure the <instance name> rsremote's log.cfg";
}
if (args.length < 2)
{
    help();
}
else
{
    try
    {
        def instanceName = args[1].toLowerCase();
        def config = MAIN.getConfigRSRemote(instanceName);
        def dist = MAIN.getDist();

        if (config && dist)
        {
            //Configure log.cfg
            File logFile = new File(dist + "/" + instanceName + "/" + logFilename);
            if (logFile.exists())
            {
                byte[] logFileBytes = new byte[logFile.length()];
                FileInputStream fis = new FileInputStream(logFile);
                fis.read(logFileBytes);
                fis.close();

                String logFileStr = new String(logFileBytes);

                def instanceRegex = "(.+\\.File=).+";
                def instanceValue = "\$1" + instanceName + "/log/" + instanceName + ".log";
                logFileStr = logFileStr.replaceAll(instanceRegex, instanceValue);

                def replaceMap = config.rsremoteLogReplaceValues();

                def rslogEnabled = MAIN.isBlueprinRSLog() || config.isAmqpAppenderEnabled();
                for (regex in replaceMap.keySet())
                {
                    def value = replaceMap.get(regex);
                    // check if value has a appender tag to be replaced.
                    if (value.contains(appenderTag)) {
                        if (rslogEnabled) // either rslog component is active or add amqp appender to the RSRemote logs
                            value = value.replace(appenderTag, ", amqp"); // replace tag with proper appender
                        else
                            value = value.replace(appenderTag, ""); // else remove the appender tag
                    }
                    logFileStr = logFileStr.replaceAll(regex, value);
                }

                logFileBytes = logFileStr.getBytes();
                FileOutputStream fos = new FileOutputStream(logFile);
                fos.write(logFileBytes);
                fos.close();

                println "Configured File: " + logFile.getAbsolutePath();
                LOG.warn("Configured File: " + logFile.getAbsolutePath());
            }
            else
            {
                println "Cannot Find RSRemote log file: " + logFile.getAbsolutePath();
                LOG.warn("Cannot Find RSRemote log file: " + logFile.getAbsolutePath());
            }
        }
        else
        {
            println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
            LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
        }
    }
    catch (Exception e)
    {
        println "Unexpected Exception while Running Configuration: " + e.getMessage();
        LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
    }
}
return null;
