final String ymlFilename = "config/logstash.yml";
final String instanceName = "logstash";

def help()
{
    println "Usage: logstash-yml-configure\n"
    println "This script will configure logstash logstash.yml";
}
try {
    def dist = MAIN.getDist();
    def config = MAIN.getConfigLogstash();
    if (config && dist) {
        File ymlFile = new File(dist + "/" + instanceName + "/" + ymlFilename);
        if (ymlFile != null && ymlFile.exists()) { 
                    byte[] ymlFileBytes = new byte[ymlFile.length()];
            FileInputStream fis = new FileInputStream(ymlFile);
            fis.read(ymlFileBytes);
            fis.close();

            String ymlFileStr = new String(ymlFileBytes);
            def replaceMap = config.logstashReplaceValues();
            for (key in replaceMap.keySet()) {
                def value = replaceMap.get(key);
                
                if (key.startsWith("logstash.yml")) {
                 	key = key.split("logstash.yml.")[1];
	                if (ymlFileStr.contains(key)) {
						def regex = key + ":?.*";
		            	value = key + ": " + value;

	                    ymlFileStr = ymlFileStr.replaceAll(regex, value);
	                }
                }
			}        

            ymlFileBytes = ymlFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(ymlFile);
            fos.write(ymlFileBytes);
            fos.close();

            println "Configured File: " + ymlFile.getAbsolutePath();
            LOG.warn("Configured File: " + ymlFile.getAbsolutePath());
        } else {
            println "Cannot Find " + instanceName + ".conf script: " + ymlFile.getAbsolutePath();
            LOG.warn("Cannot Find " + instanceName + ".conf script: " + ymlFile.getAbsolutePath());
        }
    }
    else {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e) {
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
