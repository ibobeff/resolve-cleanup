import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String ymlFilename = "config/elasticsearch.yml";

final String instanceName = "elasticsearch";

def help()
{
    println "Usage: rssearch-yml-configure\n"
    println "This script will configure rssearch's elasticsearch.yml";
}
try
{
    def dist = MAIN.getDist();
    def config = MAIN.getConfigRSSearch();

    if (config && dist)
    {
        File ymlFile = new File(dist + "/" + instanceName + "/" + ymlFilename);
        def replaceMap = config.rssearchYmlReplaceValues();

        //Configure elasticsearch.yml
        if (ymlFile != null && ymlFile.exists())
        {
            byte[] ymlFileBytes = new byte[ymlFile.length()];
            FileInputStream fis = new FileInputStream(ymlFile);
            fis.read(ymlFileBytes);
            fis.close();

            String ymlFileStr = new String(ymlFileBytes);
            
            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                value = Matcher.quoteReplacement(value);
                ymlFileStr = ymlFileStr.replaceFirst(regex, value);
            }

            dist = dist.replaceAll("\\\\", "/");
            ymlFileStr = ymlFileStr.replaceAll("INSTALLDIR", dist);
            
            def sysCallFilter = true;
            def os = System.getProperty("os.name");
            if (os.contains("Linux"))
            {
                // Check if CONFIG_SECCOMP flag is on
                LOG.debug("Checking for CONFIG_SECCOMP flag");
                def proc = ["bash", "-c", "cat /boot/config-\$(uname -r) | grep CONFIG_SECCOMP"].execute();
                def buf = new StringBuffer();
                proc.consumeProcessOutput(buf, buf);
                proc.waitFor();
                Thread.sleep(500);
                    
                LOG.info("CONFIG_SECOMP Check: " + buf.toString());
                if (buf.toString().contains("CONFIG_SECCOMP is not set"))
                {
                    LOG.warn("CONFIG_SECCOMP is not set, bootstrap.system_call_filter should be turned off for elasticsearch");
                    sysCallFilter = false;
                }
            }       
            ymlFileStr = ymlFileStr.replaceAll("SYSTEMCALLFILTER", Boolean.toString(sysCallFilter));

            ymlFileBytes = ymlFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(ymlFile);
            fos.write(ymlFileBytes);
            fos.close();

            println "Configured File: " + ymlFile.getAbsolutePath();
            LOG.warn("Configured File: " + ymlFile.getAbsolutePath());
        }
        else
        {
            println "Cannot Find " + instanceName + ".yml script: " + ymlFile.getAbsolutePath();
            LOG.warn("Cannot Find " + instanceName + ".yml script: " + ymlFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
