import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String envConfLinuxFilename = "rabbitmq/linux64/rabbitmq/sbin/rabbitmq-env";
final String envConfSolarisFilename = "rabbitmq/solaris/rabbitmq/sbin/rabbitmq-env";
final String envConfWindowsFilename = "rabbitmq/windows/rabbitmq/sbin/rabbitmq-env.bat";

def help()
{
    println "Usage: rsmq-rabbitmqenv-configure\n"
    println "This script will configure the rabbitmq-env file for primary and backup rsmq";
}
try
{
    def config = MAIN.getConfigRSMQ();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        File envConfFile = null;
        def os = System.getProperty("os.name");

        if (os.contains("Win"))
        {
            envConfFile = new File(envConfWindowsFilename);
        }
        if (os.contains("Linux"))
        {
            envConfFile = new File(envConfLinuxFilename);
        }
        if (os.contains("SunOS"))
        {
            envConfFile = new File(envConfSolarisFilename);
        }

        //configure rabbitmq-env or erts-7.3 erl.ini

        if (envConfFile != null && envConfFile.exists())
        {
            byte[] envConfFileBytes = new byte[envConfFile.length()];
            FileInputStream fis = new FileInputStream(envConfFile);
            fis.read(envConfFileBytes);
            fis.close();
            def envConfFileStr = new String(envConfFileBytes);

            dist = dist.replaceAll("\\\\", "/");
            dist = Matcher.quoteReplacement(dist);
            envConfFileStr = envConfFileStr.replaceAll("INSTALLDIR", dist);

            def nodeName = config.getPrimaryNodeName() ? config.getPrimaryNodeName() : config.getBrokerAddr();
            if (!config.isPrimary())
            {
                nodeName = config.getBackupNodeName() ? config.getBackupNodeName() : config.getBrokerAddr2();
            }
            if (nodeName)
            {
                def nodenameRegex = "RABBITMQ_NODENAME=.*";
                def nodenameValue;
                if (os.contains("Win"))
                {
                    nodenameValue = "RABBITMQ_NODENAME=rabbit@" + nodeName;
                }
                else
                {
                    nodenameValue = "RABBITMQ_NODENAME=\"rabbit@" + nodeName + "\"";
                }
                envConfFileStr = envConfFileStr.replaceAll(nodenameRegex, nodenameValue);
            }
            else
            {
                println "WARNING!!! Unable to determine local rabbitmq host/ip";
                LOG.error("primary/backup Node Name value missing from config, isPrimary: " + config.isPrimary());
            }

            def epmdPort = config.getEpmdPort();
            if (epmdPort && epmdPort.trim().matches("\\d+"))
            {
                epmdPort = epmdPort.trim().toInteger();
                if (os.contains("Win"))
                {
                    envConfFileStr = envConfFileStr.replaceAll("ERL_EPMD_PORT=.*", "ERL_EPMD_PORT=" + epmdPort);
                }
                else
                {
                    envConfFileStr = envConfFileStr.replaceAll("ERL_EPMD_PORT=.*", "ERL_EPMD_PORT=\"" + epmdPort + "\"");
                }
            }

            def inetPort = config.getInetPort();
            if (inetPort && inetPort.trim().matches("\\d+"))
            {
                inetPort = inetPort.trim().toInteger();
                if (os.contains("Win"))
                {
                    envConfFileStr = envConfFileStr.replaceAll("RABBITMQ_INET_LISTEN_PORT=.*", "RABBITMQ_INET_LISTEN_PORT=" + inetPort);
                }
                else
                {
                    envConfFileStr = envConfFileStr.replaceAll("RABBITMQ_INET_LISTEN_PORT=.*", "RABBITMQ_INET_LISTEN_PORT=\"" + inetPort + "\"");
                }
            }

            envConfFileBytes = envConfFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(envConfFile);
            fos.write(envConfFileBytes);
            fos.close();

            println "Configured File: " + envConfFile.getAbsolutePath();
            LOG.warn("Configured File: " + envConfFile.getAbsolutePath());
        }
        else
        {
            if (envConfFile!= null)
            {
                println "Cannot Find rabbitmq-conf file: " + envConfFile.getAbsolutePath();
                LOG.warn("Cannot Find rabbitmq-conf file: " + envConfFile.getAbsolutePath());
            }
            else
            {
                println "Unknown OS Type: " + os;
                LOG.warn("Unknown OS Type: " + os);
            }
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
