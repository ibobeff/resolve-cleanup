import java.util.regex.Matcher;

final String configFilename = "config/logstash-resolve.conf";
final String instanceName = "logstash";

def help() {
    println "Usage: logstash-resolve-configure\n";
    println "This script will configure logstash-resolve.conf file";
}

try {
    def dist = MAIN.getDist();
    def config = MAIN.getConfigLogstash();

    if (config && dist) {
        File configFile = new File(dist + "/" + instanceName + "/" + configFilename);
    
        if (configFile != null && configFile.exists()) {
            byte[] configFileBytes = new byte[configFile.length()];
            FileInputStream fis = new FileInputStream(configFile);
            fis.read(configFileBytes);
            fis.close();

            String configFileStr = new String(configFileBytes);
            def replaceMap = config.logstashResolveConfReplaceValues();
            
            def resolveUser = BLUEPRINT.getProperty("DB_USERNAME");
			def dbTimeZone = BLUEPRINT.getProperty("DB_TIMEZONE");
            
            for (key in replaceMap.keySet()) {
                def value = replaceMap.get(key);
                
                if (key.startsWith("logstash.resolveconf.input.jdbc")) { 
                	key = key.split("logstash.resolveconf.input.jdbc.")[1];
                	if (key.equalsIgnoreCase("jdbc_pwd")) {
                		key = "jdbc_password"; 
                	}
                }
                else if (key.startsWith("logstash.resolveconf.elasticsearch")) { 
                	key = key.split("logstash.resolveconf.elasticsearch.")[1];
                }
                
                if (key.equalsIgnoreCase("logstash.resolveconf.input.rabbitmq.port")) {
                    configFileStr = configFileStr.replace("RABBIT_PORT", value);
                    continue;
                }
                if (key.equalsIgnoreCase("logstash.resolveconf.input.rabbitmq.host")) {
                    configFileStr = configFileStr.replace("RABBIT_HOST", value);
                    continue;
                }

            	if (key.equalsIgnoreCase("jdbc_user") && value.contains("DB_USERNAME")) {
            		value = "\"" + resolveUser + "\"";
				}
				
				if (key.equalsIgnoreCase("jdbc_default_timezone") && value.contains("DB_TIMEZONE")) {
            		value = "\"" + dbTimeZone + "\"";
				}
				
                if (key == "jdbc_password" && value.startsWith("\\"))
                {
                     value = value.substring(1);
                }

				def regex = key + "\\s+=>\\s+.*";

                if (key.equalsIgnoreCase("logstash.resolveconf.elasticsearch.hosts")) { 
                	key = "hosts";
                	regex = key + "\\s+=>\\s+\\[.*\\]";
                }

                if (key == "hosts")
                {
                    value = Matcher.quoteReplacement(key + " => " + value);
                }
                else
                {
                    value = Matcher.quoteReplacement(key + " => \"" + value + "\"");
                }

                if (configFileStr.contains(key)) {
                    configFileStr = configFileStr.replaceAll(regex, value);
                }
            }
            
            def logLocation = MAIN.getConfigRSLog().getLogLocation();
            println "log location: " + logLocation;
            configFileStr = configFileStr.replace("LOG_LOCATION", logLocation);

            configFileBytes = configFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(configFile);
            fos.write(configFileBytes);
            fos.close();

            println "Configured File: " + configFile.getAbsolutePath();
            LOG.warn("Configured File: " + configFile.getAbsolutePath());
        }
        else {
            println "Cannot Find " + instanceName + ".conf script: " + configFile.getAbsolutePath();
            LOG.warn("Cannot Find " + instanceName + ".conf script: " + configFile.getAbsolutePath());
        }
    }
    else {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e) {
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
