import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String hibernateFilename = "tomcat/webapps/resolve/WEB-INF/hibernate.cfg.xml";

def help()
{
    println "Usage: rsview-hibernate-configure\n"
    println "This script will configure the rsview's hibernate.cfg.xml";
}
try
{
    def config = MAIN.getConfigRSView();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        //Configure hibernate.cfg.xml
        File hibernateFile = new File(dist + "/" + hibernateFilename);
        if (hibernateFile.exists())
        {
            def dbType = config.getHibernateDBType();
            byte[] hibernateFileBytes = new byte[hibernateFile.length()];
            FileInputStream fis = new FileInputStream(hibernateFile);
            fis.read(hibernateFileBytes);
            fis.close();

            String hibernateFileStr = new String(hibernateFileBytes);
            String hibernateFileStrFinal = "";

			// Gateway filter model mappings in hibernate.cfg.xml
			def gtwFilterModelMappings = [];
			
			def skip = false;
			
            for (hibernateLine in hibernateFileStr.split("\n"))
            {
                if (hibernateLine.contains("Oracle10gDialect"))
                {
                    if (dbType.equalsIgnoreCase("ORACLE"))
                    {
                        if (hibernateLine.contains("<!-- "))
                        {
                            hibernateLine -= "<!-- ";
                            hibernateLine -= " -->";
                        }
                    }
                    else if (!hibernateLine.contains("<!-- "))
                    {
                        hibernateLine = "<!-- " + hibernateLine + " -->";
                    }
                }
                else if (hibernateLine.contains("Oracle12cDialect"))
                {
                    if (dbType.equalsIgnoreCase("ORACLE12C"))
                    {
                        if (hibernateLine.contains("<!-- "))
                        {
                            hibernateLine -= "<!-- ";
                            hibernateLine -= " -->";
                        }
                    }
                    else if (!hibernateLine.contains("<!-- "))
                    {
                        hibernateLine = "<!-- " + hibernateLine + " -->";
                    }
                }
                else if (hibernateLine.contains("MySQL5Dialect") || hibernateLine.contains("org.mariadb.jdbc.Driver"))
                {
                    if (dbType.equalsIgnoreCase("MYSQL"))
                    {
                        if (hibernateLine.contains("<!-- "))
                        {
                            hibernateLine -= "<!-- ";
                            hibernateLine -= " -->";
                        }
                    }
                    else if (!hibernateLine.contains("<!-- "))
                    {
                        hibernateLine = "<!-- " + hibernateLine + " -->";
                    }
                }
                else if (hibernateLine.contains("DB2Dialect"))
                {
                    if (dbType.equalsIgnoreCase("DB2"))
                    {
                        if (hibernateLine.contains("<!-- "))
                        {
                            hibernateLine -= "<!-- ";
                            hibernateLine -= " -->";
                        }
                    }
                    else if (!hibernateLine.contains("<!-- "))
                    {
                        hibernateLine = "<!-- " + hibernateLine + " -->";
                    }
                }
				else if (hibernateLine.indexOf("<mapping class=\"com.resolve.persistence.model.") > -1 &&
						 hibernateLine.indexOf("Filter\" />") > -1 && !skip)
				{
					def startIndx = hibernateLine.lastIndexOf(".");
					def endIndx = hibernateLine.indexOf("Filter");
					
					gtwFilterModelMappings.add(hibernateLine.substring(startIndx + 1, endIndx));
					println "Found Gateway Filer Model Mapping For " + hibernateLine.substring(startIndx + 1, endIndx);
					LOG.debug("Found Gateway Filer Model Mapping For " + hibernateLine.substring(startIndx + 1, endIndx));
				}
				else if (hibernateLine.contains( "<!-- SDK Developed Gateway Filter Mappings Start -->"))
				{
					skip = true;
				}
				else if (hibernateLine.contains( "<!-- SDK Developed Gateway Filter Mappings End -->"))
				{
					skip = false;
					
					hibernateFileStrFinal += "\t\t<!-- SDK Developed Gateway Filter Mappings Start -->\n";
					
					def implPrefixes = config.getImplPrefixes();
					def implPackages = config.getImplPackages();
					
					if (!implPrefixes.isEmpty())
					{
						println "Size of implPrefixes is " + implPrefixes.size();
						LOG.debug("Size of implPrefixes is " + implPrefixes.size());
						LOG.debug("Size of implPackages is " + implPackages.size());
					
						for (implPrefix in implPrefixes)
						{
							String packageName = implPackages.get(implPrefix);
							if(packageName.contains(".push.") || packageName.contains(".pull.") || packageName.contains(".jms.") || packageName.contains(".msg."))
								continue;
						
							if (!gtwFilterModelMappings.contains(implPrefix))
							{
								hibernateFileStrFinal += "\n\t\t<!-- " + implPrefix + " Gateway Filter -->\n";
								hibernateFileStrFinal += "\t\t<mapping class=\"com.resolve.persistence.model." + implPrefix + "Filter\" />\n";
								hibernateFileStrFinal += "\t\t<!-- End -->\n\n";
								
								println "Adding Mapping [\t\t<mapping class=\"com.resolve.persistence.model." + implPrefix + "Filter\" />]";
								LOG.debug("Adding Mapping [\t\t<mapping class=\"com.resolve.persistence.model." + implPrefix + "Filter\" />]");
							}
						}
					}
				}
                
				if (!skip)
				{
					hibernateFileStrFinal += hibernateLine + "\n";
				}
            }

            def replaceMap = config.hibernateReplaceValues();

            for (property in replaceMap.keySet())
            {
                def value = replaceMap.get(property);
                def regex = "<property name=\"" + property + "\">.*</property>";
                def replace = "<property name=\"" + property + "\">" + value + "</property>";
                replace = Matcher.quoteReplacement(replace);
                hibernateFileStrFinal = hibernateFileStrFinal.replaceFirst(regex, replace);
            }

            hibernateFileBytes = hibernateFileStrFinal.getBytes();
            FileOutputStream fos = new FileOutputStream(hibernateFile);
            fos.write(hibernateFileBytes);
            fos.close();
        }
        else
        {
            println "Cannot Find RSView hibernate file: " + hibernateFile.getAbsolutePath();
            LOG.warn("Cannot Find RSView hibernate file: " + hibernateFile.getAbsolutePath());
        }

        println "Configured File: " + hibernateFile.getAbsolutePath();
        LOG.warn("Configured File: " + hibernateFile.getAbsolutePath());
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
