import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String csrfguardFilename = "tomcat/webapps/resolve/WEB-INF/csrfguard.properties";

def help()
{
    println "Usage: rsview-csrfguard-configure\n"
    println "This script will configure rsview's csrfguard.properties";
}
try
{
    def config = MAIN.getConfigRSView();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        def os = System.getProperty("os.name");

        File csrfguardFile = new File(dist + "/" + csrfguardFilename);

        def unprotectedList = config.rsviewCsrfguardUnprotectedValues();

        //Configure csrfguard.properties
        if (csrfguardFile != null && csrfguardFile.exists())
        {
            if (unprotectedList)
            {
                byte[] csrfguardFileBytes = new byte[csrfguardFile.length()];
                FileInputStream fis = new FileInputStream(csrfguardFile);
                fis.read(csrfguardFileBytes);
                fis.close();

                String csrfguardFileStr = new String(csrfguardFileBytes);

                def regex = "(?s)######CUSTOM SETTINGS######.*";
                def replaceString = new StringBuilder("######CUSTOM SETTINGS######");
                for (def unprotectedUrl in unprotectedList)
                {
                    if (unprotectedUrl)
                    {
                        replaceString.append("\norg.owasp.csrfguard.unprotected." + Matcher.quoteReplacement(unprotectedUrl));
                    }
                }
                csrfguardFileStr = csrfguardFileStr.replaceAll(regex, replaceString);

                csrfguardFileBytes = csrfguardFileStr.getBytes();
                FileOutputStream fos = new FileOutputStream(csrfguardFile);
                fos.write(csrfguardFileBytes);
                fos.close();

                println "Configured File: " + csrfguardFile.getAbsolutePath();
                LOG.warn("Configured File: " + csrfguardFile.getAbsolutePath());
            }
            else
            {
                println "No Custom CSRFGuard URLs appear to be configured, Skipping Configuration of " + csrfguardFile.getAbsolutePath();
                LOG.warn("No Custom CSRFGuard URLs appear to be configured, Skipping Configuration of " + csrfguardFile.getAbsolutePath());
            }
        }
        else
        {
            println "Cannot Find rsview's install script: " + csrfguardFile.getAbsolutePath();
            LOG.warn("Cannot Find rsview's install script: " + csrfguardFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
