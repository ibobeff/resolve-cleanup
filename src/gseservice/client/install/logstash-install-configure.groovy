import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String installFilename = "bin/init.logstash";

final String instanceName = "logstash";

def help()
{
    println "Usage: logstash-install-configure\n";
    println "This script will configure logstash's init.logstash";
}
try
{
    def dist = MAIN.getDist();
    def config = MAIN.getConfigLogstash();

    if (config && dist)
    {
        def os = System.getProperty("os.name");

        File installFile = new File(dist + "/" + instanceName + "/" + installFilename);

        def replaceMap = config.logstashRunReplaceValues();

        //Configure init.logstash
        if (installFile != null && installFile.exists())
        {
            byte[] installFileBytes = new byte[installFile.length()];
            FileInputStream fis = new FileInputStream(installFile);
            fis.read(installFileBytes);
            fis.close();

            String installFileStr = new String(installFileBytes);

            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                value = Matcher.quoteReplacement(value);
                installFileStr = installFileStr.replaceFirst(regex, value);
            }

            dist = Matcher.quoteReplacement(dist);
            installFileStr = installFileStr.replaceAll("INSTALLDIR", dist);

            installFileBytes = installFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(installFile);
            fos.write(installFileBytes);
            fos.close();

            println "Configured File: " + installFile.getAbsolutePath();
            LOG.warn("Configured File: " + installFile.getAbsolutePath());
        }
        else
        {
            if (installFile != null)
            {
                println "Cannot Find " + instanceName + " init script: " + installFile.getAbsolutePath();
                LOG.warn("Cannot Find " + instanceName + " init script: " + installFile.getAbsolutePath());
            }
            else
            {
                println "Unknown OS Type: " + os;
                LOG.warn("Unknown OS Type: " + os);
            }
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
