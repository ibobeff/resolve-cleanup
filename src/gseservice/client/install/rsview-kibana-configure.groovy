import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String kibanaFilename = "tomcat/webapps/resolve/kibana/config.js";

def help()
{
    println "Usage: rsview-kibana-configure\n"
    println "This script will configure the tomcat kibana connector";
}
try
{
    def dist = MAIN.getDist();
    def config = MAIN.getConfigRSView();

    if (config && dist)
    {
        File kibanaFile = new File(dist + "/" + kibanaFilename);
        def endpoint = config.getRSSearchEndpoint();

        //Configure kibana's config.js
        if (kibanaFile != null && kibanaFile.exists())
        {
            byte[] kibanaFileBytes = new byte[kibanaFile.length()];
            FileInputStream fis = new FileInputStream(kibanaFile);
            fis.read(kibanaFileBytes);
            fis.close();

            String kibanaFileStr = new String(kibanaFileBytes);

            def regex = "(?m)^(\\s*)elasticsearch:.*";
            def value = "\$1elasticsearch: \"" + endpoint + "\",";
            kibanaFileStr = kibanaFileStr.replaceFirst(regex, value);

            kibanaFileBytes = kibanaFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(kibanaFile);
            fos.write(kibanaFileBytes);
            fos.close();

            println "Configured File: " + kibanaFile.getAbsolutePath();
            LOG.warn("Configured File: " + kibanaFile.getAbsolutePath());
        }
        else
        {
            println "Cannot Find kibana's config.js file: " + kibanaFile.getAbsolutePath();
            LOG.warn("Cannot Find kibana's config.js file: " + kibanaFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
