import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String dashboardFilename = "bin/dashboard.properties";

def help()
{
    println "Usage: rsview-dashboard-configure\n"
    println "This script will configure the dashboard.properties";
}
try
{
    def config = MAIN.getConfigRSView();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        //Configure dashboard.properties
        File dashboardFile = new File(dist + "/" + dashboardFilename);
        if (dashboardFile.exists())
        {
            byte[] dashboardFileBytes = new byte[dashboardFile.length()];
            FileInputStream fis = new FileInputStream(dashboardFile);
            fis.read(dashboardFileBytes);
            fis.close();

            String dashboardFileStr = new String(dashboardFileBytes);

            def replaceMap = config.dashboardReplaceValues();

            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                value = Matcher.quoteReplacement(value);
                dashboardFileStr = dashboardFileStr.replaceFirst(regex, value);
            }

            dashboardFileBytes = dashboardFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(dashboardFile);
            fos.write(dashboardFileBytes);
            fos.close();

            println "Configured File: " + dashboardFile.getAbsolutePath();
            LOG.warn("Configured File: " + dashboardFile.getAbsolutePath());
        }
        else
        {
            println "Cannot Find dashboard file: " + dashboardFile.getAbsolutePath();
            LOG.warn("Cannot Find dashboard file: " + dashboardFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
