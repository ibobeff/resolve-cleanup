import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String runFilename = "run.sh";
final String runFilenameWin = "run.bat";

final String runLocationWinRabbitmq = "rabbitmq/windows/rabbitmq/sbin";
final String runLocationLnxRabbitmq = "rabbitmq/linux64/rabbitmq/sbin";
final String runLocationSolRabbitmq = "rabbitmq/solaris/rabbitmq/sbin";

def help()
{
    println "Usage: rsmq-run-configure\n"
    println "This script will configure the run scripts for primary rsmq";
}
try
{
    def config = MAIN.getConfigRSMQ();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        def os = System.getProperty("os.name");
        File runFile = null;

        if ("RABBITMQ".equalsIgnoreCase(config.getProduct()))
        {
            if (os.contains("Win"))
            {
                runFile = new File(dist + "/" + runLocationWinRabbitmq + "/" + runFilenameWin);
            }
            if (os.contains("Linux"))
            {
                runFile = new File(dist + "/" + runLocationLnxRabbitmq + "/" + runFilename);
            }
            if (os.contains("SunOS"))
            {
                runFile = new File(dist + "/" + runLocationSolRabbitmq + "/" + runFilename);
            }
        }

        //Configure run.sh or run.bat
        if (runFile != null && runFile.exists())
        {
            configureFile(runFile);
        }
        else
        {
            if (runFile != null)
            {
                println "Cannot Find RSMQ install script: " + runFile.getAbsolutePath();
                LOG.warn("Cannot Find RSMQ install script: " + runFile.getAbsolutePath());
            }
            else
            {
                println "Unknown OS Type: " + os;
                LOG.warn("Unknown OS Type: " + os);
            }
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;

def configureFile(File file)
{
    def config = MAIN.getConfigRSMQ();

    if (file != null && file.exists())
    {
        byte[] fileBytes = new byte[file.length()];
        FileInputStream fis = new FileInputStream(file);
        fis.read(fileBytes);
        fis.close();

        String fileStr = new String(fileBytes);

        def replaceMap = config.rsmqRunReplaceValues();

        for (regex in replaceMap.keySet())
        {
            def value = replaceMap.get(regex);
            value = Matcher.quoteReplacement(value);
            fileStr = fileStr.replaceFirst(regex, value);
        }

        fileStr = fileStr.replaceFirst("PRIMARY=.*", "PRIMARY=" + config.isPrimary().toString().toUpperCase());

        fileBytes = fileStr.getBytes();
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(fileBytes);
        fos.close();

        println "Configured File: " + file.getAbsolutePath();
        LOG.warn("Configured File: " + file.getAbsolutePath());
    }
}
