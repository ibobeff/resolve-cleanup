import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;

final String esapiFilename = "rssync/config/ESAPI.properties";

def help()
{
    println "Usage: rsmgmt-esapi-configure\n"
    println "This script will configure the rsmgmt's ESAPI properties file";
}

try
{
    def config = MAIN.getConfigRSSync();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        //Configure ESAPI.properties
        File esapiFile = new File(dist + "/" + esapiFilename);

        if (esapiFile.exists())
        {
            byte[] esapiFileBytes = new byte[esapiFile.length()];
            FileInputStream fis = new FileInputStream(esapiFile);
            fis.read(esapiFileBytes);
            fis.close();

            String esapiFileStr = new String(esapiFileBytes);
            def replaceMap = config.rssyncEsapiReplaceValues();
			
            //System.out.println("Replace Map: " + replaceMap);
            
            def newESAPIProps = [];
            
            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                
                def key = regex.split("=")[0];
                
                if (esapiFileStr.contains(key))
                {
                    // Update existing ESAPI properties
                    
                    value = Matcher.quoteReplacement(value);
                    esapiFileStr = esapiFileStr.replaceAll(regex, value);
                }
                else
                {
                    println "Found new ESAPI property: " + value; 
                    LOG.trace("Found new ESAPI property: " + value);
                    
                    newESAPIProps << value;
                }
            }
            
            if (!newESAPIProps.isEmpty())
            {
                esapiFileStr = esapiFileStr + "\n\n# Newly Added Properties on " + Calendar.getInstance().getTime().toString() + "\n\n";
                
                newESAPIProps.each {esapiFileStr = esapiFileStr + "$it" + "\n";}
                
                esapiFileStr = esapiFileStr + "\n";
                
                //println "Updated ESAPI Properties : [" + esapiFileStr + "]";
                LOG.trace("ESAPI Properties With New Properties: [" + esapiFileStr + "]");
            }
            
            esapiFileBytes = esapiFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(esapiFile);
            fos.write(esapiFileBytes);
            fos.close();

            println "Configured File: " + esapiFile.getAbsolutePath();
            LOG.warn("Configured File: " + esapiFile.getAbsolutePath());
        }
        else
        {
            println "Cannot Find RSView ESAPI file: " + esapiFile.getAbsolutePath();
            LOG.warn("Cannot Find RSView ESAPI file: " + esapiFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;