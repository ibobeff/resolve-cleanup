import java.io.File;

def fileNames = ["tomcat/webapps/resolve/WEB-INF/cluster-cache.xml", "tomcat/webapps/resolve/WEB-INF/ehcache.xml", "tomcat/webapps/resolve/WEB-INF/jndi.properties", "tomcat/webapps/resolve/WEB-INF/local-cache.xml"];

def toFileNames = ["tomcat/webapps/resolve/WEB-INF/classes/cluster-cache.xml", "tomcat/webapps/resolve/WEB-INF/classes/ehcache.xml", "tomcat/webapps/resolve/WEB-INF/classes/jndi.properties", "tomcat/webapps/resolve/WEB-INF/classes/local-cache.xml"];

def numFiles = fileNames.size();

def help()
{
    println "Usage: rsview-move-configs\n"
    println "This script will move configuration files from WEB-INF to WEB-INF/classes";
}

for (i in 0..(numFiles-1))
{
    File file = new File(fileNames[i]);
    if (file.exists())
    {
        try
        {
            File toFile = new File(toFileNames[i]);
            if (toFile.exists())
            {
                LOG.warn("Attempting to Delete File: " + toFile.getAbsolutePath());
                if (!toFile.delete())
                {
                    LOG.warn("Unable to Delete File: " + toFile.getAbsolutePath());
                }
            }
            if (!file.renameTo(toFile))
            {
                println "Unable to Move File: " + file.getName();
                LOG.warn("Unable to Move File: " + file.getName());
            }
            else
            {
                println "Moved File " + file.getName() + " to " + toFile.getAbsolutePath();
                LOG.warn("Moved File to " + toFile.getAbsolutePath());
            }
        }
        catch (Exception e)
        {
            println "Unable to Move File: " + file.getName() + " - " + e.getMessage();
            LOG.warn("Unable to Move File: " + file.getName() + " - " + e.getMessage(), e);
        }
    }
    else
    {
        LOG.warn("File " + file.getAbsolutePath() + " Not Found, skipping move");
    }
}
