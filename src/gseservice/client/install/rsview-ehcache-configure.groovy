import java.io.File;
import com.resolve.util.XDoc;

final String ehcacheFilename = "tomcat/webapps/resolve/WEB-INF/ehcache.xml";
final String ehcacheFilename2 = "tomcat/webapps/resolve/WEB-INF/classes/ehcache.xml";

def help()
{
    println "Usage: rsview-ehcache-configure\n"
    println "This script will configure the rsview's ehcache.xml";
}
try
{
    def config = MAIN.getConfigRSView();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        //Configure ehcache.xml
        File ehcacheFile = new File(dist + "/" + ehcacheFilename);
        if (!ehcacheFile.exists())
        {
            ehcacheFile = new File(dist + "/" + ehcacheFilename2);
        }
        if (ehcacheFile.exists())
        {
            def ehcacheXPath = config.ehcacheXPathValues();
            XDoc ehcacheDoc = new XDoc(ehcacheFile);

            for (key in ehcacheXPath.keySet())
            {
                def value = ehcacheXPath.get(key);
                if (value instanceof String)
                {
                    ehcacheDoc.setStringValue(key, value);
                }
                else if (value instanceof List)
                {
                    if (!value.isEmpty() && value.get(0) instanceof Map)
                    {
                        ehcacheDoc.setListMapValue(key, value);
                    }
                    else if (!value.isEmpty() && value.get(0) instanceof String)
                    {
                        ehcacheDoc.setListValue(key, value);
                    }
                }
            }
            ehcacheDoc.toPrettyFile(ehcacheFile);

            println "Configured File: " + ehcacheFile.getAbsolutePath();
            LOG.warn("Configured File: " + ehcacheFile.getAbsolutePath());
        }
        else
        {
            println "Cannot Find RSView ehcache file: " + ehcacheFile.getAbsolutePath();
            LOG.warn("Cannot Find RSView ehcache file: " + ehcacheFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
