import java.io.File;

import com.resolve.service.License;
import com.resolve.service.LicenseService;

def help()
{
    println "Usage: resolve-install-license <license file>\n"
    println "This script will upload the local server's license file";
}

if (args.length < 2)
{
    help();
}
else
{
    try
    {
        def filename = args[1];
        def licenseFile = new File(filename);
        if (!licenseFile.exists())
        {
            println "WARNING!!! License File " + filename + " not found, local license will not be uploaded";
            LOG.warn("License File " + licenseFile.getAbsolutePath() + " not found, local license will not be uploaded");
        }
        else
        {
            def params = [:];
            def fileContents = licenseFile.getText('UTF-8');
            params[Constants.LICENSE_CONTENT] = fileContents;
            def licenseService = new LicenseService(MAIN.configId.getGuid());
            licenseService.uploadLicense(params);
            println "License File Uploaded";
            LOG.info("License File " + licenseFile.getAbsolutePath() + " Uploaded");
        }
    }
    catch (Exception e)
    {
        println "WARNING!!! Failed to set up the local license file: " + e.getMessage();
        LOG.error("Failed to set up the local license file", e);
    }
}

return null;
