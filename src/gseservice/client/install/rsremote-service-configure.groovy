import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String serviceFilename = "rsremote.service";

def help()
{
    println "Usage: rsremote-service-configure <instance name>\n"
    println "This script will configure the <instance name> rsremote's rsremote.service";
}
if (args.length < 2)
{
    help();
}
else
{
    try
    {
        def instanceName = args[1].toLowerCase();
        def config = MAIN.getConfigRSRemote(instanceName);
        def dist = MAIN.getDist();

        if (config && dist)
        {
            def os = System.getProperty("os.name");

            File serviceFile = null;
            if (os.contains("Linux"))
            {
                serviceFile = new File(dist + "/" + instanceName + "/bin/" + serviceFilename);
                if (serviceFile == null || !serviceFile.exists())
                {
                    serviceFile = new File(dist + "/" + instanceName + "/bin/" + instanceName + ".service");
                }

                //Configure rsremote.service
                if (serviceFile != null && serviceFile.exists())
                {
                    byte[] serviceFileBytes = new byte[serviceFile.length()];
                    FileInputStream fis = new FileInputStream(serviceFile);
                    fis.read(serviceFileBytes);
                    fis.close();

                    String serviceFileStr = new String(serviceFileBytes);

                    dist = Matcher.quoteReplacement(dist);
                    serviceFileStr = serviceFileStr.replaceAll("INSTALLDIR", dist);

                    def user = config.getUser();
                    if (!user)
                    {
                        user = BLUEPRINT.getProperty("rsremote.run.R_USER");
                    }
                    serviceFileStr = serviceFileStr.replaceAll("User=.*", "User=" + user);

                    serviceFileBytes = serviceFileStr.getBytes();
                    FileOutputStream fos = new FileOutputStream(serviceFile);
                    fos.write(serviceFileBytes);
                    fos.close();

                    println "Configured File: " + serviceFile.getAbsolutePath();
                    LOG.warn("Configured File: " + serviceFile.getAbsolutePath());


                    if (instanceName != (serviceFile.getName() - ".service"))
                    {
                        def newServiceFile = new File(dist + "/" + instanceName + "/bin/" + instanceName + ".service");
                        if (newServiceFile.exists() && !newServiceFile.delete())
                        {
                            LOG.warn("Failed to Delete Instance Service File: " + newServiceFile.getAbsolutePath());
                        }
                        if (serviceFile.renameTo(newServiceFile))
                        {
                            println "Renamed Service File to " + newServiceFile.getAbsolutePath();
                            LOG.warn("Renamed Service File to " + newServiceFile.getAbsolutePath());
                        }
                        else
                        {
                            println "Failed to Rename Service File to " + newServiceFile.getAbsolutePath();
                            LOG.warn("Failed to Rename Service File to " + newServiceFile.getAbsolutePath());
                        }
                    }
                }
                else
                {
                    println "Cannot Find " + instanceName + " service script: " + serviceFile.getAbsolutePath();
                    LOG.warn("Cannot Find " + instanceName + " service script: " + serviceFile.getAbsolutePath());
                }
            }
            else
            {
                println "Skipping - systemd Service Setup for Linux Systems Only";
                LOG.info("Skipping - systemd Service Setup for Linux Systems Only: " + os);
            }
        }
        else
        {
            println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
            LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
        }
    }
    catch (Exception e)
    {
        println "Unexpected Exception while Running Configuration: " + e.getMessage();
        LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
    }
}
return null;
