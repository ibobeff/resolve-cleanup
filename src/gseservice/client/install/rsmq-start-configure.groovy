import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String startErlLinuxFile = "rabbitmq/linux64/erlang/bin/start";
final String startErtsLinuxFile = "rabbitmq/linux64/erlang/erts-7.3/bin/start";

final String startErlSolarisFile = "rabbitmq/solaris/erlang/bin/start";
final String startErtsSolarisFile = "rabbitmq/solaris/erlang/erts-7.3/bin/start";

def help()
{
    println "Usage: rsmq-start-configure\n"
    println "This script will configure the erlang and erlang/erts-7.3 start file"
}
try
{
    def os = System.getProperty("os.name");
    if (os.contains("Win"))
    {
        LOG.info("Windows system, skipping start configuration");
    }
    else
    {
        def config = MAIN.getConfigRSMQ();
        def dist = MAIN.getDist();

        if (config && dist)
        {
            File startFile;

            if (os.contains("Linux"))
            {
                startFile = new File(dist + "/" + startErlLinuxFile);
            }
            else if (os.contains("SunOS"))
            {
                startFile = new File(dist + "/" + startErlSolarisFile);
            }

            //Configure erlang/bin/start
            if (startFile != null && startFile.exists())
            {
                configureFile(startFile, dist);
            }
            else
            {
                println "Cannot Find " + instanceName + " install script: " + startFile.getAbsolutePath();
                LOG.warn("Cannot Find " + instanceName + " install script: " + startFile.getAbsolutePath());
            }

            if (os.contains("Linux"))
            {
                startFile = new File(dist + "/" + startErtsLinuxFile);
            }
            if (os.contains("SunOS"))
            {
                startFile = new File(dist + "/" + startErtsSolarisFile);
            }

            //Configure erlang/erts-7.3/bin/start
            if (startFile != null && startFile.exists())
            {
                configureFile(startFile, dist);
            }
            else
            {
                println "Cannot Find " + instanceName + " install script: " + startFile.getAbsolutePath();
                LOG.warn("Cannot Find " + instanceName + " install script: " + startFile.getAbsolutePath());
            }
        }
        else
        {
            println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
            LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
        }
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;

def configureFile(File file, String dist)
{
    def config = MAIN.getConfigRSMQ();

    if (file != null && file.exists())
    {
        byte[] fileBytes = new byte[file.length()];
        FileInputStream fis = new FileInputStream(file);
        fis.read(fileBytes);
        fis.close();

        String fileStr = new String(fileBytes);

        dist = dist.replaceAll("\\\\", "/");
        dist = Matcher.quoteReplacement(dist);
        fileStr = fileStr.replaceAll("INSTALLDIR", dist);

        fileBytes = fileStr.getBytes();
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(fileBytes);
        fos.close();

        println "Configured File: " + file.getAbsolutePath();
        LOG.warn("Configured File: " + file.getAbsolutePath());
    }
}
