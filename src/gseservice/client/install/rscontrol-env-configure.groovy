import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String runFilename = "bin/rscontrol-env.sh";
final String runFilenameWin = "bin/rscontrol-env.bat";

final String instanceName = "rscontrol";

def help()
{
    println "Usage: rscontrol-env-configure\n"
    println "This script will configure rscontrol's rscontrol-env.sh or rscontrol-env.bat";
}
try
{
    def dist = MAIN.getDist();
    def config = MAIN.getConfigRSControl();

    if (config && dist)
    {
        def os = System.getProperty("os.name");

        File runFile = null;
        if (os.contains("Win"))
        {
            runFile = new File(dist + "/" + instanceName + "/" + runFilenameWin);
        }
        else if (os.contains("Linux") || os.contains("SunOS"))
        {
            runFile = new File(dist + "/" + instanceName + "/" + runFilename);
        }

        def replaceMap = config.rscontrolEnvReplaceValues();

        //Configure rscontrol-env.sh || rscontrol-env.bat
        if (runFile != null && runFile.exists())
        {
            byte[] runFileBytes = new byte[runFile.length()];
            FileInputStream fis = new FileInputStream(runFile);
            fis.read(runFileBytes);
            fis.close();

            String runFileStr = new String(runFileBytes);

            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                value = Matcher.quoteReplacement(value);
                runFileStr = runFileStr.replaceFirst(regex, value);
            }

            runFileBytes = runFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(runFile);
            fos.write(runFileBytes);
            fos.close();

            println "Configured File: " + runFile.getAbsolutePath();
            LOG.warn("Configured File: " + runFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
