import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String startupFilename = "config/startup.options";

final String instanceName = "logstash";

def help()
{
    println "Usage: logstash-startup-configure\n"
    println "This script will configure logstash's startup.options";
}
try
{
    def dist = MAIN.getDist();
    def config = MAIN.getConfigLogstash();

    if (config && dist)
    {
        def os = System.getProperty("os.name");

        File startupFile = new File(dist + "/" + instanceName + "/" + startupFilename);

        def replaceMap = config.logstashStartupReplaceValues();

        //Configure startup.options
        if (startupFile != null && startupFile.exists())
        {
            byte[] startupFileBytes = new byte[startupFile.length()];
            FileInputStream fis = new FileInputStream(startupFile);
            fis.read(startupFileBytes);
            fis.close();

            String startupFileStr = new String(startupFileBytes);

            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                value = Matcher.quoteReplacement(value);
                startupFileStr = startupFileStr.replaceFirst(regex, value);
            }

            startupFileBytes = startupFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(startupFile);
            fos.write(startupFileBytes);
            fos.close();

            println "Configured File: " + startupFile.getAbsolutePath();
            LOG.warn("Configured File: " + startupFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
