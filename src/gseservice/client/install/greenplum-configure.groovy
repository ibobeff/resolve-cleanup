/*
 Creates sysconfig file with lexically later name to overwite any other
 settings from /usr/lib/sysctl.d/, /run/sysctl.d/, and /etc/sysctl.d/.
 ...*.conf are parsed within sysctl(8) at boot time.
 Same goes for nproc soft/hard limits (/etc/security/limits.d/)
 */
 import java.io.File;
 import java.io.FileInputStream;
 import java.io.FileOutputStream;
 import java.util.regex.Matcher;
 import static groovy.io.FileType.FILES;
 
 final String gphome = "/opt/resolve/dcs/greenplum-db-5.11.1"
 String[] greenplumDataDirs = [ gphome + '/data/master/gpseg-1', gphome + '/data/primary']
 
 
 def hostname = "hostname".execute().text
 sysctlConfDir = "";
 
 def help()
 {
	 println "Usage: rs-greenplum-configure\n"
	 println "This script will configure the greenplum init config file";
 }
 
 // Some static config
 final String sysctlConf = """\
# Written by greenplum setup script
    kernel.shmmni = 4096
    kernel.shmall = 4000000000
    kernel.sem = 250 512000 100 2048
    kernel.sysrq = 1
    kernel.core_uses_pid = 1
    kernel.msgmnb = 65536
    kernel.msgmax = 65536
    kernel.msgmni = 2048
    net.ipv4.tcp_syncookies = 1
    net.ipv4.ip_forward = 0
    net.ipv4.conf.default.accept_source_route = 0
    net.ipv4.tcp_tw_recycle = 1
    net.ipv4.tcp_max_syn_backlog = 4096
    net.ipv4.conf.all.arp_filter = 1
    net.ipv4.ip_local_port_range = 1025 65535
    net.core.netdev_max_backlog = 10000
    net.core.rmem_max = 2097152
    net.core.wmem_max = 2097152
    vm.overcommit_memory = 0
"""
 
 final String nprocConf = """\
# Written by greenplum setup script
    * soft nofile 65536
    * hard nofile 65536
    * soft nproc 131072
    * hard nproc 131072
"""
 
 final String gpInitConf = """\
    ARRAY_NAME="Greenplum"
    MACHINE_LIST_FILE=./hostfile
    SEG_PREFIX=gpseg
    PORT_BASE=50000
    declare -a DATA_DIRECTORY=( """ + gphome + """/data/primary""" + gphome + """/data/primary )
	 MASTER_HOSTNAME=`hostname -f`
	 MASTER_DIRECTORY=""" + gphome + """/data/master
    MASTER_PORT=5432
    TRUSTED_SHELL=ssh
    CHECK_POINT_SEGMENTS=8
    ENCODING=UNICODE
"""
 
 final String gpPath = '''\
    GPHOME=''' + gphome + '''
	 # Replace with symlink path if it is present and correct
	 if [ -h ${GPHOME}/../greenplum-db ]; then
		 GPHOME_BY_SYMLINK=`(cd ${GPHOME}/../greenplum-db/ && pwd -P)`
		 if [ x"${GPHOME_BY_SYMLINK}" = x"${GPHOME}" ]; then
			 GPHOME=`(cd ${GPHOME}/../greenplum-db/ && pwd -L)`/.
		 fi
		 unset GPHOME_BY_SYMLINK
	 fi
	 #setup PYTHONHOME
	 if [ -x $GPHOME/ext/python/bin/python ]; then
		 PYTHONHOME="$GPHOME/ext/python"
	 fi
	 PYTHONPATH=$GPHOME/lib/python
	 PATH=$GPHOME/bin:$PYTHONHOME/bin:$PATH
	 LD_LIBRARY_PATH=$GPHOME/lib:$PYTHONHOME/lib:${LD_LIBRARY_PATH-}
	 export LD_LIBRARY_PATH
	 OPENSSL_CONF=$GPHOME/etc/openssl.cnf
	 export GPHOME
	 export PATH
	 export PYTHONPATH
	 export PYTHONHOME
	 export OPENSSL_CONF
 '''

def runCommand(def command, def output) {
    if (!output) {
        output = new StringBuffer();
    }
        def result = true
        try {
        def proc = command.execute();
                proc.consumeProcessOutput(output, output);
                proc.waitForOrKill(30000)
                result = proc.exitValue() == 0;
        } catch (Exception e) {
                println "error when running command: " + e.getMessage()
                result = false
        }
        return result
}

def void writeConfFile(dir, content, fileExtension) {
def tmp = []
def currentFiles = []

try {

    if (!new File(dir).isDirectory()) {
        println "No such directory $dir"
        System.exit(1)
    }        
        new File(dir).eachFileRecurse(FILES) {
            // Only *.conf per sysctl.d(5)
            if(it.name =~ /\d*-\w*.conf/) {
                // Push all file names to list; strip path 
                currentFiles << it.toString().minus(dir)
            }
        }
        if(!currentFiles) {
            new File(dir + fileExtension).write(content) 
            return
        }
        
        def fileArray = currentFiles.sort() { a, b -> a.tokenize('-').first().toInteger() <=> b.tokenize('-').first().toInteger() }
        for (file in fileArray) {
            tmp << file.split('-').first().toInteger()
        }
        def nextCount = (++tmp.max())

        def sysctlConfFile = new File(dir + nextCount + '-' + fileExtension)

    if (sysctlConfFile.exists()) {
        println "File $sysctlConfFile already exists!"
    }
    else {
        sysctlConfFile.write(content)
    }
}
    catch (IOException e) {
        println "Error creating config file in $dir ${e.message}"
        throw new RuntimeException(e);
    }
}
/*
 Logic
 */
try { 
    runCommand("/usr/sbin/useradd gpadmin -s /bin/bash -d " + gphome , null)

    writeConfFile("/etc/sysctl.d/", sysctlConf, "sysctl.conf")

    writeConfFile("/etc/security/limits.d/", nprocConf, "nproc.conf")

    writeConfFile(gphome + "/", gpPath, "greenplum_path.sh")
    writeConfFile(gphome + "/", gpInitConf, "gp_init_config")

    // Setup greenplum data dirs
    for (d in greenplumDataDirs) {
        new File(d).mkdirs()
    }

    runCommand("/usr/bin/chown -R gpadmin.gpadmin " + gphome, null)
    runCommand("/usr/bin/chmod 755 " + gphome + "/bin/gpinitsystem", null)

	runCommand(gphome + "/bin/gpinitsystem -c " + gphome + " /gp_init_config", null)
	
	File bashrc = new File(gphome + "/.bashrc")
	bashrc.append("\nsource " + gphome + "/greenplum_path.sh\nexport MASTER_DATA_DIRECTORY=" + gphome + "/data/master/gpseg-1")
	
	runCommand(gphome + "/bin/source_bashrc", null)
	
    File hostFile = new File(gphome + "/hostfile")
    hostFile.append("\n" + hostname)

    runCommand("/usr/bin/chmod 755 " + gphome + "/bin/gpssh-exkeys", null)
    runCommand(gphome + "/bin/gpssh-exkeys -f " + gphome + "/hostfile", null)

    writeConfFile(gphome + "/data/master/gpseg-1/", '\nhost all all 127.0.0.1/32 md5', "pg_hba.conf")

    runCommand(gphome + "/bin/gpstop -u", null)
	
	runCommand(gphome + "/bin/psql -c 'create database gpadmin'", null)
	
	runCommand(gphome + "/bin/psql -c 'create user postgresql password \'resolve\''", null)
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    // LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;

