import org.apache.commons.io.FileUtils;
import java.nio.charset.StandardCharsets;

final String installFilename = "bin/init.rslog";

final String instanceName = "rslog";

def help()
{
    println "Usage: rslog-install-configure\n";
    println "This script will configure rslog's init.rslog";
}
try
{
    def dist = MAIN.getDist();

    if (dist)
    {
        def os = System.getProperty("os.name");

        File installFile = new File(dist + "/" + instanceName + "/" + installFilename);

        //Configure init.rslog
        if (installFile != null && installFile.exists())
        {
            String content = FileUtils.readFileToString(installFile);
            content = content.replace("INSTALLDIR", dist);
            FileUtils.writeStringToFile(installFile, content, StandardCharsets.UTF_8.name());

            println "Configured File: " + installFile.getAbsolutePath();
            LOG.warn("Configured File: " + installFile.getAbsolutePath());
        }
        else
        {
            if (installFile != null)
            {
                println "Cannot Find " + instanceName + " init script: " + installFile.getAbsolutePath();
                LOG.warn("Cannot Find " + instanceName + " init script: " + installFile.getAbsolutePath());
            }
            else
            {
                println "Unknown OS Type: " + os;
                LOG.warn("Unknown OS Type: " + os);
            }
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
