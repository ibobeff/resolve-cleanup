import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

final String logFilename = "tomcat/webapps/resolve/WEB-INF/log.cfg";
final String sreeLogFilename = "tomcat/webapps/sree/WEB-INF/classes/logging.properties";

def help()
{
    println "Usage: rsview-log-configure\n"
    println "This script will configure the rsview's log.cfg";
}
try
{
    def config = MAIN.getConfigRSView();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        //Configure log.cfg
        File logFile = new File(dist + "/" + logFilename);
        if (logFile.exists())
        {
            byte[] logFileBytes = new byte[logFile.length()];
            FileInputStream fis = new FileInputStream(logFile);
            fis.read(logFileBytes);
            fis.close();

            String logFileStr = new String(logFileBytes);

            def replaceMap = config.rsviewLogReplaceValues();

            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                logFileStr = logFileStr.replaceAll(regex, value);
            }

            logFileBytes = logFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(logFile);
            fos.write(logFileBytes);
            fos.close();

            println "Configured File: " + logFile.getAbsolutePath();
            LOG.warn("Configured File: " + logFile.getAbsolutePath());
        }
        else
        {
            println "Cannot Find RSView log file: " + logFile.getAbsolutePath();
            LOG.warn("Cannot Find RSView log file: " + logFile.getAbsolutePath());
        }

        //Configure logging.properties
        File sreeLogFile = new File(dist + "/" + sreeLogFilename);
        if (sreeLogFile.exists())
        {
            byte[] sreeLogFileBytes = new byte[sreeLogFile.length()];
            FileInputStream fis = new FileInputStream(sreeLogFile);
            fis.read(sreeLogFileBytes);
            fis.close();

            String sreeLogFileStr = new String(sreeLogFileBytes);

            def replaceMap = config.rsviewLogReplaceValues();

            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                sreeLogFileStr = sreeLogFileStr.replaceAll(regex, value);
            }

            sreeLogFileBytes = sreeLogFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(sreeLogFile);
            fos.write(sreeLogFileBytes);
            fos.close();

            println "Configured File: " + sreeLogFile.getAbsolutePath();
            LOG.warn("Configured File: " + sreeLogFile.getAbsolutePath());
        }
        else
        {
            println "Cannot Find Sree log file: " + sreeLogFile.getAbsolutePath();
            LOG.warn("Cannot Find Sree log file: " + sreeLogFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
