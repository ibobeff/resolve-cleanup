import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String bigdeskFilename = "tomcat/webapps/resolve/bigdesk/js/bigdeskApp.js";

def help()
{
    println "Usage: rsview-bigdesk-configure\n"
    println "This script will configure the tomcat bigdesk connector";
}
try
{
    def dist = MAIN.getDist();
    def config = MAIN.getConfigRSView();

    if (config && dist)
    {
        File bigdeskFile = new File(dist + "/" + bigdeskFilename);
        def endpoint = config.getRSSearchEndpoint();

        //Configure bigdesk's bigdeskApp.js
        if (bigdeskFile != null && bigdeskFile.exists())
        {
            byte[] bigdeskFileBytes = new byte[bigdeskFile.length()];
            FileInputStream fis = new FileInputStream(bigdeskFile);
            fis.read(bigdeskFileBytes);
            fis.close();

            String bigdeskFileStr = new String(bigdeskFileBytes);

            def regex = "(?m)^(\\s*endpoint: .+) \".*?(,?)\\s*\$";
            def value = "\$1 \"" + endpoint + "\"\$2";
            bigdeskFileStr = bigdeskFileStr.replaceFirst(regex, value);

            bigdeskFileBytes = bigdeskFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(bigdeskFile);
            fos.write(bigdeskFileBytes);
            fos.close();

            println "Configured File: " + bigdeskFile.getAbsolutePath();
            LOG.warn("Configured File: " + bigdeskFile.getAbsolutePath());
        }
        else
        {
            println "Cannot Find bigdesk's bigdeskApp.js file: " + bigdeskFile.getAbsolutePath();
            LOG.warn("Cannot Find bigdesk's bigdeskApp.js file: " + bigdeskFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
