import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

String blueprintFileName = "rsmgmt/config/blueprint.properties";

// List of deprecated Blueprint properties
def excludeList = """\
rsconsole.esb.brokerport2
rsconsole.esb.brokerport
rsmq.primary
resolve.import
resolve.rszk
RSZK_NODES
rszk.myid
rszk.coord.cfg.tickTime
rszk.coord.cfg.dataDir
rszk.coord.cfg.clientPort
rszk.coord.cfg.initLimit
rszk.coord.cfg.syncLimit
rszk.coord.cfg.server.port1
rszk.coord.cfg.server.port2
rsmgmt.metric.group.units.2
rsmgmt.metric.group.type.9
rscontrol.neo4j.coordinator.port
rscontrol.neo4j.coordinator_port
rscontrol.neo4j.ha.cluster_name
rscontrol.neo4j.ha.server_id
rscontrol.neo4j.ha.server
rscontrol.neo4j.ha.pull_interval
rscontrol.neo4j.enable_remote_shell
rscontrol.neo4j.allow_store_upgrade
rscontrol.neo4j.neostore.nodestore.db.mapped_memory
rscontrol.neo4j.neostore.relationshipstore.db.mapped_memory
rscontrol.neo4j.neostore.propertystore.db.mapped_memory
rscontrol.neo4j.neostore.propertystore.db.strings.mapped_memory
rscontrol.neo4j.neostore.propertystore.db.arrays.mapped_memory
rscontrol.neo4j.enable_online_backup
rscontrol.hazelcast.
rscontrol.jbc.ClusterName
rscontrol.jgroups.stack.config.TCPPING.initial_hosts
rscontrol.jgroups.stack.name
rscontrol.jgroups.stack.config.TCP.bind_port
rscontrol.jgroups.stack.config.TCP.start_port
rscontrol.jgroups.stack.config.pbcast\\.STREAMING_STATE_TRANSFER.use_default_transport
rscontrol.jgroups.stack.config.FD_SOCK.start_port
rsview.hazelcast.
rsview.neo4j.coordinator.port
rsview.neo4j.coordinator_port
rsview.neo4j.ha.cluster_name
rsview.jbc.ClusterName
rsview.jgroups.stack.config.TCPPING.initial_hosts
rsview.jgroups.stack.name
rsview.jgroups.stack.config.TCP.bind_port
rsview.jgroups.stack.config.TCP.start_port
rsview.jgroups.stack.config.pbcast\\.STREAMING_STATE_TRANSFER.use_default_transport
rsview.jgroups.stack.config.FD_SOCK.start_port
.esb.product
.esb.messagettl
CASSANDRA_NODES
resolve.cassandra
cassandra.env.HEAP_NEWSIZE
cassandra.env.MAX_HEAP_SIZE
cassandra.env.SET_HEAP
cassandra.passwd.admin
cassandra.primary
cassandra.run.R_USER
cassandra.run.terminatetimeout
cassandra.ttl
cassandra.yaml.cipher_suites
cassandra.yaml.enabled
cassandra.yaml.internode_encryption
cassandra.yaml.keystore
cassandra.yaml.keystore_password
cassandra.yaml.listen_address
cassandra.yaml.memtable_flush_queue_size
cassandra.yaml.memtable_flush_writers
cassandra.yaml.phi_convict_threshold
cassandra.yaml.require_client_auth
cassandra.yaml.rpc_address
cassandra.yaml.rpc_port
cassandra.yaml.seeds
cassandra.yaml.ssl_storage_port
cassandra.yaml.storage_port
cassandra.yaml.truststore
cassandra.yaml.truststore_password
rscontrol.hazelcast.group.name
rscontrol.hazelcast.group.password
rscontrol.hazelcast.network.port
.nosql.cluster
.nosql.compaction.active
.nosql.compaction.schedule
.nosql.keyspace
.nosql.maxblockedthreads
.nosql.maxhostconns
.nosql.maxtimeout
.nosql.password
.nosql.perfdebug
.nosql.readconsistency
.nosql.retrymaxattempts
.nosql.retrysleeptimems
.nosql.secure.active
.nosql.secure.truststore
.nosql.secure.truststore_password
.nosql.ttl
.nosql.writeconsistency
rsview.sree.StyleReport.locale.resource
rsview.sree.adhoc.wizard.path
rsview.sree.adm.home
rsview.sree.dashboard.configpath
rsview.sree.dashboard.context
rsview.sree.dashboard.enabled
rsview.sree.dashboard.uri
rsview.sree.datasource.registry.file
rsview.sree.db.caseSensitive
rsview.sree.deployment.portlet
rsview.sree.deployment.server
rsview.sree.deployment.soap
rsview.sree.deployment.war.jar
rsview.sree.help.dev.url
rsview.sree.help.url
rsview.sree.html.archive.button.portlet
rsview.sree.html.close.button.portlet
rsview.sree.html.customize.button.portlet
rsview.sree.html.find.button.portlet
rsview.sree.html.findNext.button.portlet
rsview.sree.html.pdf.button.portlet
rsview.sree.html.print.button.portlet
rsview.sree.html.replet.name.portlet
rsview.sree.html.serverPrint.button.portlet
rsview.sree.init.home
rsview.sree.license.key
rsview.sree.query.registry.file
rsview.sree.replet.adm.servlet
rsview.sree.replet.live.deploy
rsview.sree.replet.repository.servlet
rsview.sree.report.em.title
rsview.sree.schedule.auto.down
rsview.sree.schedule.auto.start
rsview.sree.schedule.concurrency
rsview.sree.schedule.memory.max
rsview.sree.schedule.memory.min
rsview.sree.scheduler.classpath
rsview.sree.scheduler.rmi.port
rsview.sree.sree.bundle
rsview.dashboard.dbname
rsview.dashboard.dbtype
rsview.dashboard.defaultDB
rsview.dashboard.host
rsview.dashboard.password
rsview.dashboard.url
rsview.dashboard.user
rsview.hazelcast.group.name
rsview.hazelcast.group.password
rsview.hazelcast.network.port
rsview.neo4j.allow_store_upgrade
rsview.neo4j.enable_online_backup
rsview.neo4j.enable_remote_shell
rsview.neo4j.ha.lock_read_timeout
rsview.neo4j.ha.pull_interval
rsview.neo4j.ha.read_timeout
rsview.neo4j.ha.server
rsview.neo4j.ha.server_id
rsview.neo4j.ha.state_switch_timeout
rsview.neo4j.neostore.nodestore.db.mapped_memory
rsview.neo4j.neostore.propertystore.db.arrays.mapped_memory
rsview.neo4j.neostore.propertystore.db.mapped_memory
rsview.neo4j.neostore.propertystore.db.strings.mapped_memory
rsview.neo4j.neostore.relationshipstore.db.mapped_memory
rsview.neo4j.online_backup_server
rsview.neo4j.remote_shell_enabled
.general.timezone
.sql.usexadatasource
rsview.selfcheck.es..retry
rsview.tomcat.ssl.config
RSVIEW.TOMCAT.SSL.IMPORT
RSVIEW.TOMCAT.SSL.IMPORT.CERTFILE
rsview.ESAPI.Encoder
rsconsole.ESAPI.Validator
rsview.ESAPI.Encoder
rscontrol.ESAPI.Validator
rsview.ESAPI.Encoder
rsmgmt.ESAPI.Validator
rsview.ESAPI.Encoder
rsproxy.ESAPI.Validator
rsview.ESAPI.Encoder
rsremote.ESAPI.Validator
rsview.ESAPI.Encoder
rssync.ESAPI.Validator
rsview.ESAPI.Encoder
rsview.ESAPI.Validator
rsview.ESAPI.Encoder
rsarchive.ESAPI.Validator
rscontrol.archive.sleeptime
rscontrol.archive.archiveexecutedata
rscontrol.archive.blocksleeptime
rscontrol.archive.smalltableblocksleeptime
rscontrol.archive.active
rscontrol.archive.schedule
rscontrol.archive.expiry
rscontrol.archive.cleanup
rscontrol.archive.sleeptime
rscontrol.archive.archiveexecutedata
rscontrol.archive.blocksize
rscontrol.archive.blocksleeptime
rscontrol.archive.smalltableblocksize
rscontrol.archive.smalltableblocksleeptime
rsarchive.archive.sleeptime
rsarchive.archive.archiveexecutedata
rsarchive.archive.blocksleeptime
rsarchive.archive.smalltableblocksleeptime
"""

def help()
{
    println "Usage: copy-blueprint [file to copy] <file to overwrite>\n";
    println "This script will copy the blueprint properties file at";
    println "rsmgmt/config/blueprint.properties (or the specified";
    println "file to copy) to the specified blueprint properties file";
}
if (args.length < 2)
{
    help();
}
else
{
    try
    {
        def copyBlueprintFileName = args[1];
        if (args.length > 2)
        {
            copyBlueprintFileName = args[2];
            blueprintFileName = args[1];
        }
        def dist = MAIN.getDist();

        copyBlueprintFileName = copyBlueprintFileName.replaceAll("\\\\","/");
        blueprintFileName = blueprintFileName.replaceAll("\\\\","/");

        if (!copyBlueprintFileName.matches("^(?:[a-zA-Z]:)?/.*"))
        {
            copyBlueprintFileName = dist + "/" + copyBlueprintFileName;
        }
        if (!blueprintFileName.matches("^(?:[a-zA-Z]:)?/.*")) //windows?
        {
            blueprintFileName = dist + "/" + blueprintFileName;
        }

        def blueprintFile = new File(blueprintFileName);
        def copyFile = new File(copyBlueprintFileName);

        LOG.warn("Attempting to copy " + blueprintFile.getAbsolutePath() + " values to " + copyFile.getAbsolutePath());

        if (!blueprintFile.isFile())
        {
            println "Blueprint File To Copy Does Not Exist";
            LOG.debug("Blueprint File To Copy:" + blueprintFile.getAbsolutePath() + " Does Not Exist");
        }
        else if (!copyFile.isFile())
        {
            println "Blueprint File To Copy To Does Not Exist";
            LOG.debug("Blueprint File To Copy To: " + copyFile.getAbsolutePath() + " Does Not Exist");
        }
        else
        {
            byte[] blueprintFileBytes = new byte[blueprintFile.length()];
            FileInputStream fis = new FileInputStream(blueprintFile);
            fis.read(blueprintFileBytes);
            fis.close();

            String blueprintFileStr = new String(blueprintFileBytes);

            byte[] copyFileBytes = new byte[copyFile.length()];
            fis = new FileInputStream(copyFile);
            fis.read(copyFileBytes);
            fis.close();

            String copyFileStr = new String(copyFileBytes);
            /////////////////////3.1 to 3.2
            def nodeList = "";
            def primary = "true";
            def localhost = "";
            /////////////////////3.1 to 3.2

            /////////////////////3.2 to 3.3
            def nodeList32 = "";
            def localhost32 = "";
            def serverid = "";
            def cassandraNodes = "";
            /////////////////////3.2 to 3.3

            def additionalItems = new ArrayList<String>();

            for (line in blueprintFileStr.split("\n"))
            {
                line = line.trim();
                if (line && !line.startsWith("#"))
                {
                    def index = line.indexOf("=");
                    if (index != -1)
                    {
                        def key = line.substring(0,index);
                        def value = line.substring(index+1);

                        /////////////////////3.1 to 3.2
                        if (key.equals("PRIMARY"))
                        {
                            primary = value;
                        }
                        if (key.equals("PRIMARY_HOST"))
                        {
                            if (nodeList)
                            {
                                nodeList = nodeList + "," + value;
                            }
                            else
                            {
                                nodeList = value;
                            }
                            if (primary.equalsIgnoreCase("true"))
                            {
                                localhost = value;
                            }
                            key = "RSMQ_PRIMARY_HOST";
                        }
                        if (key.equals("PRIMARY_PORT"))
                        {
                            key = "RSMQ_PRIMARY_PORT";
                        }
                        if (key.equals("BACKUP_HOST"))
                        {
                            if (value)
                            {
                                if (nodeList)
                                {
                                    nodeList = nodeList + "," + value;
                                }
                                else
                                {
                                    nodeList = value;
                                }
                                if (primary.equalsIgnoreCase("false"))
                                {
                                    localhost = value;
                                }
                            }
                            key = "RSMQ_BACKUP_HOST";
                        }
                        if (key.equals("BACKUP_PORT"))
                        {
                            key = "RSMQ_BACKUP_PORT";
                        }
                        if (key.equals("rsconsole.esb.brokeraddr"))
                        {
                            value = "\${RSMQ_PRIMARY_HOST}:\${RSMQ_PRIMARY_PORT}";
                        }
                        if (key.equals("rsconsole.esb.brokeraddr2"))
                        {
                            value = "\${RSMQ_BACKUP_HOST}:\${RSMQ_BACKUP_PORT}";
                        }

                        /////////////////////3.2 to 3.3
                        if (key.equals("NODE_LIST"))
                        {
                            nodeList32 = value;
                            /////////////////////3.4.1 to 3.4.2
                            continue;
                        }
                        if (key.equals("LOCALHOST"))
                        {
                            localhost32 = value;
                        }
                        if (key.equals("SERVER_ID"))
                        {
                            serverid = value;
                        }
                        /////////////////////3.2 to 3.3

                        /////////////////////3.3 to 3.4
                        if (key.equals("CASSANDRA_NODES"))
                        {
                            cassandraNodes = value;
                        }
                        /////////////////////3.3 to 3.4

                        /////////////////////3.4.1 to 3.4.2
                        if (key.equals("rsview.neo4j.ha.pull_interval") && value.equals("5"))
                        {
                            value = "30s";
                        }
                        /////////////////////3.4.1 to 3.4.2

                        if (key.contains("mmx"))
                        {
                            key = key.replaceAll("mmx", "Xmx");
                        }
                        if (key.contains("mms"))
                        {
                            key = key.replaceAll("mms", "Xms");
                        }
                        value = value.replaceAll("[{]PRIMARY_HOST", "{RSMQ_PRIMARY_HOST");
                        value = value.replaceAll("[{]PRIMARY_PORT", "{RSMQ_PRIMARY_PORT");
                        value = value.replaceAll("[{]BACKUP_HOST", "{RSMQ_BACKUP_HOST");
                        value = value.replaceAll("[{]BACKUP_PORT", "{RSMQ_BACKUP_PORT");
                        /////////////////////3.1 to 3.2
                        /////////////////////3.2.2 to 3.2.5
                        if (key.endsWith("ldap.base_dn"))
                        {
                            key = key.replace("ldap.base_dn", "ldap.basedn.dn.1");
                        }
                        if (key.endsWith("active_directory.base_dn"))
                        {
                            key = key.replace("active_directory.base_dn", "active_directory.basedn.dn.1");
                        }

                        if (excludeList.find("(?m)^" + key + "\$") != null)
                        {
                            LOG.debug("Skipping Excluded Attribute: " + key);
                            continue;
                        }
                        else if (key.indexOf(".") != -1 && excludeList.find("(?m)^" + key.substring(key.indexOf(".")) + "\$") != null)
                        {
                            LOG.debug("Skipping Excluded Attribute: " + key);
                            continue;
                        }

                        def regex = "(?m)^" + key.replaceAll("\\\\", "\\\\\\\\") + "=.*";
                        def replaceStr = key + "=" + value;
                        if (copyFileStr =~ regex)
                        {
                            replaceStr = Matcher.quoteReplacement(replaceStr);
                            LOG.trace("Setting " + key + " to " + value);
                            copyFileStr = copyFileStr.replaceFirst(regex, replaceStr);
                        }
                        else
                        {
                            additionalItems.add(line);
                        }
                    }
                }
            }

            if (additionalItems.size() > 0)
            {

                copyFileStr = copyFileStr + "\n\n" +
                "##################################################################################################################################\n" +
                "#Custom Parameters                                                                                                               #\n" + 
                "##################################################################################################################################";
                Collections.sort(additionalItems);
                def section = "";
                for (item in additionalItems)
                {
                    //if item not in exclude list
                    def keyMatcher = item =~ /(.+?)=.+/;
                    def key = "";
                    if (keyMatcher.find())
                    {
                        key = keyMatcher.group(1);
                    }
                    if (!key)
                    {
                        LOG.info("Key-Value could not be extracted, or value is blank: " + item);
                    }
                    else if (excludeList.find("(?m)^" + key + "\$") == null)
                    {
                        def sectionMatcher = item =~ /(.+)\.\w+=.*/;
                        if (sectionMatcher.find())
                        {
                            def tmpSection = sectionMatcher.group(1);
                            if (section == "")
                            {
                                section = tmpSection;
                            }
                            if (!section.contains(tmpSection) && !tmpSection.contains(section))
                            {
                                copyFileStr += "\n";
                                section = tmpSection;
                            }
                        }
                        copyFileStr += "\n" + item;
                        LOG.debug("Adding Custom configuration: " + item);
                    }
                    else
                    {
                        LOG.info("Configuration Value Skipped, Found in Exclude List: " + item);
                    }
                }
            }

            /////////////////////3.3 to 3.4
            if (!cassandraNodes && nodeList32)
            {
                def regex = "(?m)^CASSANDRA_NODES=.*";
                def replaceStr = "CASSANDRA_NODES=" + nodeList32;
                replaceStr = Matcher.quoteReplacement(replaceStr);
                LOG.debug("Setting CASSANDRA_NODES to " + nodeList32);
                copyFileStr = copyFileStr.replaceFirst(regex, replaceStr);
            }
            /////////////////////3.3 to 3.4

            /////////////////////3.4.1 to 3.4.2
            if (nodeList32)
            {
                def regex = "(?m)^RSVIEW_NODES=.*";
                def replaceStr = "RSVIEW_NODES=" + nodeList32;
                replaceStr = Matcher.quoteReplacement(replaceStr);
                LOG.debug("Setting RSVIEW_NODES to " + nodeList32);
                copyFileStr = copyFileStr.replaceFirst(regex, replaceStr);

                regex = "(?m)^RSCONTROL_NODES=.*";
                replaceStr = "RSCONTROL_NODES=" + nodeList32;
                replaceStr = Matcher.quoteReplacement(replaceStr);
                LOG.debug("Setting RSCONTROL_NODES to " + nodeList32);
                copyFileStr = copyFileStr.replaceFirst(regex, replaceStr);
            }
            /////////////////////3.4.1 to 3.4.2

            /////////////////////3.2 to 3.3
            if (!serverid)
            {
                if (nodeList32 && localhost32)
                {
                    def nodeAry = nodeList32.split(",");
                    for (int i=0; i<nodeAry.length; i++)
                    {
                        if (localhost32 == nodeAry[i])
                        {
                            serverid = i + 1;
                            break;
                        }
                    }
                }
                if (serverid)
                {
                    def regex = "(?m)^SERVER_ID=.*";
                    def replaceStr = "SERVER_ID=" + serverid;
                    replaceStr = Matcher.quoteReplacement(replaceStr);
                    LOG.debug("Setting SERVERID to " + serverid);
                    copyFileStr = copyFileStr.replaceFirst(regex, replaceStr);
                }
            }
            /////////////////////3.2 to 3.3

            /////////////////////3.1 to 3.2
            if (nodeList)
            {
                def regex = "(?m)^NODE_LIST=.*";
                def replaceStr = "NODE_LIST=" + nodeList;
                replaceStr = Matcher.quoteReplacement(replaceStr);
                LOG.debug("Setting NODE_LIST to " + nodeList);
                copyFileStr = copyFileStr.replaceFirst(regex, replaceStr);
            }

            if (localhost)
            {
                regex = "(?m)^LOCALHOST=.*";
                replaceStr = "LOCALHOST=" + localhost;
                replaceStr = Matcher.quoteReplacement(replaceStr);
                LOG.debug("Setting LOCALHOST to " + localhost);
                copyFileStr = copyFileStr.replaceFirst(regex, replaceStr);
            }
            /////////////////////3.1 to 3.2

            copyFileBytes = copyFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(copyFile);
            fos.write(copyFileBytes);
            fos.close();

            println "Blueprint Copy Complete";
            LOG.warn("Copied " + blueprintFile.getAbsolutePath() + " values to " + copyFile.getAbsolutePath());
        }
    }
    catch (Exception e)
    {
        println "Unexpected Exception while Copying Blueprint: " + e.getMessage();
        LOG.error("Unexpected Exception while Copying Blueprint: " + e.getMessage(), e);
    }
}
return null;
