import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String connectionsFilename = "config/connections.cfg";

String instanceName = "rsconsole";

def help()
{
    println "Usage: rsconsole-connections-configure\n"
    println "This script will configure the rsconsole's connections.cfg";
}
try
{
    def config = MAIN.getConfigRSConsole();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        //Configure connections.cfg
        File connectionsFile = new File(dist + "/" + instanceName + "/" + connectionsFilename);
        if (connectionsFile.exists())
        {
            byte[] connectionsFileBytes = new byte[connectionsFile.length()];
            FileInputStream fis = new FileInputStream(connectionsFile);
            fis.read(connectionsFileBytes);
            fis.close();

            String connectionsFileStr = new String(connectionsFileBytes);

            def replaceMap = config.rsconsoleConnectionsReplaceValues();

            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                value = Matcher.quoteReplacement(value);
                connectionsFileStr = connectionsFileStr.replaceFirst(regex, value);
            }

            connectionsFileBytes = connectionsFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(connectionsFile);
            fos.write(connectionsFileBytes);
            fos.close();

            println "Configured File: " + connectionsFile.getAbsolutePath();
            LOG.warn("Configured File: " + connectionsFile.getAbsolutePath());
        }
        else
        {
            println "Cannot Find " + instanceName + " connections file: " + connectionsFile.getAbsolutePath();
            LOG.warn("Cannot Find " + instanceName + " connections file: " + connectionsFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
