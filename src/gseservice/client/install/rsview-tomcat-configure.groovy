import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

final String tomcatFilename = "tomcat/conf/server.xml";

jdkHome = "jdk";

def help()
{
    println "Usage: rsview-tomcat-configure\n"
    println "This script will configure Tomcat's server.xml file";
}
try
{
    def config = MAIN.getConfigRSView();
    def dist = MAIN.getDist();
	LOG.trace("config = " + config + ", dist " + dist);
    if (config && dist) {
        //getReplacement will make sure any ${} syntax will be replaced with the correct value
        jdkHome = BLUEPRINT.getReplacement("resolve.java_home");
        //Configure server.xml
        File tomcatFile = new File(dist + "/" + tomcatFilename);
        if (tomcatFile.exists()) {
            def success = true;
            def tomcatConfig = config.getTomcatProperties();
            def keyStore= new File(tomcatConfig.get("rsview.tomcat.connector.https.keystoreFile"));
            def keyPass= tomcatConfig.get("rsview.tomcat.connector.https.keystorePass");
            def isSSLConfig = "true".equalsIgnoreCase(tomcatConfig.get("rsview.tomcat.ssl.config"));
            def isHTTPS = "true".equalsIgnoreCase(tomcatConfig.get("rsview.tomcat.https"));
            if ((isHTTPS || isSSLConfig) && !keyStore.exists()) {
                success = installAndConfigSelfSignSSL(keyStore, keyPass);
            }
            else if ((isHTTPS || isSSLConfig) && keyStore.exists()) {
                success = validateTrustedCert(keyStore, keyPass);
            }
            if(!success) {
                if (isSSLConfig)
                {
                    LOG.error("Unable to Generate and Import Self Signed SSL Keystore File for RSView Installation");
                    println "WARNING!!! Unable to generate or imported Self Signed SSL keystore file. Your system will now revert back to non-secure (http not https)";
                    println "Do you want to continue with non-secure (http not https) installation?"
                    print "Yes to continue or No to cancel Installation. (Y/N) "
                    BufferedReader br = new BufferedReader(new InputStreamReader(System.in))
                    input = br.readLine();
                    if(input.equalsIgnoreCase("y")) {
                        LOG.warn("Can't generate or import SSL keystore file. Your system will revert to non-secure (http not https)");
                        println "WARNING!!! Can't generate or import SSL keystore file. Your system will revert to non-secure (http not https)";
                        BLUEPRINT.setProperty("rsview.tomcat.http", "true");
                        BLUEPRINT.setProperty("rsview.tomcat.https", "false");
                        config.getTomcatProperties().set("rsview.tomcat.http","true");
                        config.getTomcatProperties().set("rsview.tomcat.https","false");
                        config.getEsapiProperties().set("rsview.ESAPI.HttpUtilities.ForceSecureCookies","false")
                        config.getAxis2Properties().set("rsview.axis2.https","false")

                    } else {
                        return -1;
                    }
                }
                else {
                    LOG.error("Unable to Validate SSL Keystore File for RSView");
                    println "WARNING!!! Unable to validate Tomcat's HTTPS Configuration.  The RSView may not Start the HTTPS Port correctly.";
                    println "Do you want to continue?"
                    print "Yes to continue or No to cancel Configuration. (Y/N) "
                    BufferedReader br = new BufferedReader(new InputStreamReader(System.in))
                    input = br.readLine();
                    if(input.equalsIgnoreCase("y")) {
                        println "WARNING!!! Unable to Validate RSView SSL Keystore";
                        LOG.warn("Unable to Validate RSView SSL Keystore");
                    } else {
                        return -1;
                    }
                }

            }
            byte[] tomcatFileBytes = new byte[tomcatFile.length()];
            FileInputStream fis = new FileInputStream(tomcatFile);
            fis.read(tomcatFileBytes);
            fis.close();

            String tomcatFileStr = new String(tomcatFileBytes);

            def replaceMap = config.tomcatPropertiesReplaceValues();

            for (regex in replaceMap.keySet()) {
                def value = replaceMap.get(regex);
                if (regex.contains("maxThreads") || regex.contains("compression") || regex.contains("compressionMinSize")
                    || regex.contains("noCompressionUserAgents") || regex.contains("compressableMimeType")) {
                    tomcatFileStr = tomcatFileStr.replaceAll(regex, value);
                }
                else {
                    tomcatFileStr = tomcatFileStr.replaceFirst(regex, value);
                }
            }

            tomcatFileBytes = tomcatFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(tomcatFile);
            fos.write(tomcatFileBytes);
            fos.close();

            println "Configured File: " + tomcatFile.getAbsolutePath();
            LOG.warn("Configured File: " + tomcatFile.getAbsolutePath());
        }
        else {
            println "Cannot Find Tomcat server.xml file: " + tomcatFile.getAbsolutePath();
            LOG.warn("Cannot Find Tomcat server.xml file: " + tomcatFile.getAbsolutePath());
        }
    }
    else {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e) {
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;

def runCommand(def command, def output) {
    if (!output) {
        output = new StringBuffer();
    }
	def result = true
	try {
        def proc = command.execute(); 
		proc.consumeProcessOutput(output, output);
		proc.waitForOrKill(30000)
		result = proc.exitValue() == 0;
        LOG.info("Executed Command: Results:\n" + output.toString());
	} catch (Exception e) {
		println "error when running command: " + e.getMessage()
		LOG.error("error when running command: " + e.getMessage(),e)
		result = false
	}
	return result
}

def installAndConfigSelfSignSSL(def keyStore, def keyPass) {
    def keysize = "2048";
    def issuer = "CN=Resolve,OU=Resolve,O=Resolve,L=Irvine,C=US";
    def validity = "360";
    def result = true;
    def cacertDefPass = "changeit";
    println "Creating Self Signed Certificate and Keystore";
    def genkeyCommand = "$jdkHome/bin/keytool -genkeypair -keyalg RSA -keysize " + keysize + " -storepass " + keyPass + " -keypass " + keyPass + " -keystore " +  keyStore + " -dname " + issuer + " -validity " + validity + " -alias resolve";
    LOG.info("Generate Key Command: " + genkeyCommand.replaceAll(" " + keyPass + " ", " **** "));
    result = runCommand(genkeyCommand, null)
    if(keyStore.exists()) {
        println "Converting certificate into a trusted certificate";
        def exportCommand = "$jdkHome/bin/keytool -export -file mytrust.cert -storepass " + keyPass + " -keystore " + keyStore + " -alias resolve";
        LOG.info("Export Cert Command: " + exportCommand.replaceAll(" " + keyPass + " ", " **** ")); 
        result = runCommand(exportCommand, null);
        if(result) {
           if(!importCert("mytrust.cert", keyStore, keyPass, "resolve")){
               LOG.error("Failed to Import resolve certificate in " + keyStore);
               result = false;
           }
           // Import resolve cert in jdk/security/cacerts.
           keyStore = "$jdkHome/lib/security/cacerts";
           if(!importCert("mytrust.cert", keyStore, cacertDefPass, "_resolve")){
               LOG.error("Failed to Import resolve certificate in " + keyStore);
           }
        }
        else {
            LOG.error("Failed to Export Private Key to Include as Trusted");
        }
        FileUtils.deleteQuietly(new File("mytrust.cert"));
    }
    return result;
}

def validateTrustedCert(def keyStore, def keyPass) {
    def success = false;
    if (keyStore.exists()) {
        def exportCaCertsCommand = "$jdkHome/bin/keytool -export -file mytrust.cert -storepass " + keyPass + " -keystore " + keyStore + " -alias resolve";
        LOG.debug("Export Cert Command: " + exportCaCertsCommand.replaceAll(" " + keyPass + " ", " **** "));
        runCommand(exportCaCertsCommand, null);
        
        def caCerts = "$jdkHome/lib/security/cacerts";
        if(!importCert("mytrust.cert", caCerts, "changeit", "_resolve")){
            LOG.warn("Failed to Import resolve certificate in " + caCerts + ". Probably it is already imported.");
        }
        FileUtils.deleteQuietly(new File("mytrust.cert"));

        def listCommand = "$jdkHome/bin/keytool -list -keystore " + keyStore + " -storepass " + keyPass;
        def certs = new StringBuffer();
        certs.append(" ");
        LOG.info("List Certs Command: " + listCommand.replaceAll(" " + keyPass + " ", " **** "));
        def result = runCommand(listCommand.toString(), certs);
        certs = certs.toString();
        if (!result || certs.contains("IOException")) {
            LOG.error("WARNING!!! Unable to Read Keystore, Keystore Validation Failed:\n" + certs);
            println "WARNING!!! Unable to Read Keystore, Keystore Validation Failed";
        }
        else if (!certs.contains("PrivateKeyEntry")) {
            LOG.error("WARNING!!! Keystore missing Private Key, Keystore Validation Failed:\n" + certs);
            println "WARNING!!! Keystore missing Private Key, Keystore Validation Failed";
        }
        else if (!certs.contains("trustedCertEntry")) {
            LOG.warn("No Trusted Cert Entry Found, Attempting to add Private Key as Trusted to avoid Tomcat 8 incompatibility:\n" + certs);
            def privateKeyAliasMatcher = certs =~ /([^,\n]+),.*PrivateKeyEntry/;
            if (privateKeyAliasMatcher.find()) {
                def privateKeyAlias = privateKeyAliasMatcher.group(1);
                def exportCommand = "$jdkHome/bin/keytool -export -file mytrust.cert -storepass " + keyPass + " -keystore " + keyStore + " -alias " + privateKeyAlias;
                LOG.debug("Export Cert Command: " + exportCommand.replaceAll(" " + keyPass + " ", " **** "));
                runCommand(exportCommand, null);
                def exportCert = new File("mytrust.cert");
                if (exportCert.exists()) {
                    success = importCert("mytrust.cert", keyStore, keyPass, privateKeyAlias);
                    FileUtils.deleteQuietly(exportCert);
                }
                else {
                    LOG.error("Failed to Export Private Cert to Add as Trusted");
                }
            }
            else {
                LOG.warn("Matcher Failed to find private key alias to export.  Trusted Cert will not be set up");
            }
        }
        else {
            LOG.info("Keystore Validation Passed");
            success = true;
        }
    }
}

def importCert(def importCertFile, def keyStore, def keyPass, def alias) {
    def importCommand = "$jdkHome/bin/keytool -import -trustcacerts -noprompt -file " + importCertFile + " -keystore " + keyStore + " -storepass " + keyPass + " -alias trusted" + alias;
    LOG.info("Import Cert Command: " + importCommand.replaceAll(" " + keyPass + " ", " **** "));
    result = runCommand(importCommand, null);
    return result;
}
