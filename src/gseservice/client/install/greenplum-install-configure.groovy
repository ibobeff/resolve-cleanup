import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String installFilename = "init.greenplum";
final String instanceName = "dcs/greenplum-db-5.11.1";

def help()
{
    println "Usage: greenplum-install-configure\n";
    println "This script will configure greenplum's init.greenplum file";
}
try
{
    def dist = MAIN.getDist();
    def config = MAIN.getConfigGreenplum();

    if (config && dist)
    {
        def os = System.getProperty("os.name");

        File installFile = null;
        if (os.contains("Linux") || os.contains("SunOS"))
        {
            installFile = new File(dist + "/" + instanceName + "/" + installFilename);
        }

        //Configure init.greenplum
        if (installFile != null && installFile.exists())
        {
            byte[] installFileBytes = new byte[installFile.length()];
            FileInputStream fis = new FileInputStream(installFile);
            fis.read(installFileBytes);
            fis.close();

            String installFileStr = new String(installFileBytes);
            dist = Matcher.quoteReplacement(dist);
            installFileStr = installFileStr.replaceAll("INSTALLDIR", dist);

            installFileBytes = installFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(installFile);
            fos.write(installFileBytes);
            fos.close();

            println "Configured File: " + installFile.getAbsolutePath();
            LOG.warn("Configured File: " + installFile.getAbsolutePath());
        }
        else
        {
            if (installFile != null)
            {
                println "Cannot Find " + instanceName + " init script: " + installFile.getAbsolutePath();
                LOG.warn("Cannot Find " + instanceName + " init script: " + installFile.getAbsolutePath());
            }
            else
            {
                println "Unknown OS Type: " + os;
                LOG.warn("Unknown OS Type: " + os);
            }
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated. Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated. Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
