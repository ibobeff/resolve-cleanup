import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

final String axis2Filename = "tomcat/webapps/resolve/WEB-INF/conf/axis2.xml";

def help()
{
    println "Usage: rsview-axis3-configure\n"
    println "This script will configure RSView's axis2.xml file";
}
try
{
    def config = MAIN.getConfigRSView();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        //Configure server.xml
        File axis2File = new File(dist + "/" + axis2Filename);
        if (axis2File.exists())
        {
            byte[] axis2FileBytes = new byte[axis2File.length()];
            FileInputStream fis = new FileInputStream(axis2File);
            fis.read(axis2FileBytes);
            fis.close();

            String axis2FileStr = new String(axis2FileBytes);

            def replaceMap = config.axis2PropertiesReplaceValues();

            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                axis2FileStr = axis2FileStr.replaceFirst(regex, value);
            }

            axis2FileBytes = axis2FileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(axis2File);
            fos.write(axis2FileBytes);
            fos.close();

            println "Configured File: " + axis2File.getAbsolutePath();
            LOG.warn("Configured File: " + axis2File.getAbsolutePath());
        }
        else
        {
            println "Cannot Find axis2.xml file: " + axis2File.getAbsolutePath();
            LOG.warn("Cannot Find axis2.xml file: " + axis2File.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
