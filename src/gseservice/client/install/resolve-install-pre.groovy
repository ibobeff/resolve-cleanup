import java.util.regex.Matcher;
import com.resolve.util.MCPLog;
import com.resolve.util.MCPConstants;

def help()
{
    println "Usage: resolve-install-pre\n"
    println "This script will run DB Creation and User Setup for MySQL databases";
}

def statement;
MCPLog mcpLog = null;

//run create.sql
try
{
    def installStatusFileName = MAIN.getDist() + "/rsmgmt/log/" + MCPConstants.MCP_RESOLVE_INSTALL_PRE_STATUS_LOGFILE;
    mcpLog = new MCPLog(installStatusFileName, true, true, MCPConstants.MCP_LOG_ITEM_INSTALL_PRE);
    mcpLog.write(MCPConstants.MCP_FIELD_KEY_START,"Installation Pre Script Started");

    def primary = BLUEPRINT.get("PRIMARY");
    if (primary && primary.equalsIgnoreCase("TRUE"))
    {
        def dbType = BLUEPRINT.getProperty("DB_TYPE");
        if ("mysql".equalsIgnoreCase(dbType))
        {
            def url = "jdbc:mysql://localhost:3306";
            if (BLUEPRINT.get("DB_URL"))
            {
                url = BLUEPRINT.get("DB_URL");
            }
            else if (BLUEPRINT.get("DB_HOST"))
            {
                url = "jdbc:mysql://" + BLUEPRINT.get("DB_HOST") + ":3306";
            }
            def username = BLUEPRINT.get("MYSQL_ROOT");
            def password = BLUEPRINT.get("MYSQL_ROOT_PASSWORD");
            def createFile = new File("mysql/create.sql");

            def resolveUser = BLUEPRINT.getProperty("DB_USERNAME");
            def resolvePassword = BLUEPRINT.getProperty("DB_PASSWORD");
            def resolveSchema = BLUEPRINT.getProperty("DB_NAME");

            if ((username || password) && createFile.isFile())
            {
                if (resolveUser != "resolve" || resolveSchema != "resolve" || resolvePassword != "resolve")
                {
                    byte[] createFileBytes = new byte[createFile.length()];
                    FileInputStream fis = new FileInputStream(createFile);
                    fis.read(createFileBytes);
                    fis.close();
                    String createFileStr = new String(createFileBytes);

                    resolveSchema = Matcher.quoteReplacement(resolveSchema);
                    resolveUser = Matcher.quoteReplacement(resolveUser);
                    resolvePassword = Matcher.quoteReplacement(resolvePassword);

                    createFileStr = createFileStr.replaceAll("resolve;", resolveSchema + ";");
                    createFileStr = createFileStr.replaceAll("resolve'@", resolveUser + "'@");
                    createFileStr = createFileStr.replaceAll("resolve';", resolvePassword + "';");

                    createFileBytes = createFileStr.getBytes();
                    FileOutputStream fos = new FileOutputStream(createFile);
                    fos.write(createFileBytes);
                    fos.close();
                }

                if (!username)
                {
                    username = "root";
                }
                if (!password)
                {
                    mcpLog.executeStart("mgmt/ExecuteSQL \"" + createFile.getAbsolutePath()  + "\" mysql ${username} ${url}");
                    MAIN.executeCommand("mgmt/ExecuteSQL \"" + createFile.getAbsolutePath()  + "\" mysql ${username} ${url}");
                    mcpLog.executeEnd("mgmt/ExecuteSQL \"" + createFile.getAbsolutePath()  + "\" mysql ${username} ${url}");
                }
                else
                {
                    mcpLog.executeStart("mgmt/ExecuteSQL \"" + createFile.getAbsolutePath()  + "\" mysql ${username} ${password} ${url}");
                    MAIN.executeCommand("mgmt/ExecuteSQL \"" + createFile.getAbsolutePath()  + "\" mysql ${username} ${password} ${url}");
                    mcpLog.executeEnd("mgmt/ExecuteSQL \"" + createFile.getAbsolutePath()  + "\" mysql ${username} ${password} ${url}");
                }
            }
            else
            {
                if (username || password)
                {
                    println "Could Not Find Database Creation and User Setup File: " + createFile.getAbsolutePath();
                    LOG.warn("Could Not Find Database Creation and User Setup File: " + createFile.getAbsolutePath());
                    mcpLog.warn("Could Not Find Database Creation and User Setup File: " + createFile.getAbsolutePath());
                }
                else
                {
                    println "Root Username And Password Not Provided, Skipping Database Creation and Resolve User Setup";
                    LOG.info("Root Username And Password Not Provided, Skipping Database Creation and Resolve User Setup");
                    mcpLog.info("Could Not Find Database Creation and User Setup File: " + createFile.getAbsolutePath());
                }
            }
        }
        else
        {
            println "Not a MySQL database, skipping Database Creation and User Setup";
            LOG.warn("Not a MySQL database, skipping Database Creation and User Setup");
            mcpLog.warn("Not a MySQL database, skipping Database Creation and User Setup");
        }
    }
    else
    {
        println "Installation Not Configured As Primary, Skipping Database Creation and User Setup";
        LOG.warn("Installation Not Configured As Primary, Skipping Database Creation and User Setup");
        mcpLog.warn("Not a MySQL database, skipping Database Creation and User Setup");
    }
}
catch (Exception e)
{
    println "Unexpected Exception, Cancelling Database Creation and User Setup\n" + e.getMessage();
    LOG.warn("Unexpected Exception, Cancelling Database Creation and User Setup", e);
    mcpLog.warn("Unexpected Exception, Cancelling Database Creation and User Setup: " + e.getMessage());
}
finally
{
    statement?.close();
}
mcpLog?.write(MCPConstants.MCP_FIELD_KEY_END,"Installation Pre Script Ended");

return null;
