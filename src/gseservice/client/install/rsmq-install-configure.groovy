import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String initFilename = "init.rsmq";
final String serviceFilename = "install-service.bat";

final String initLocationWinRabbitmq = "rabbitmq/windows/rabbitmq/sbin";
final String initLocationLnxRabbitmq = "rabbitmq/linux64/rabbitmq/sbin";
final String initLocationSolRabbitmq = "rabbitmq/solaris/rabbitmq/sbin";

def help()
{
    println "Usage: rsmq-install-configure\n"
    println "This script will configure the install scripts for primary and backup rsmq";
}
try
{
    def config = MAIN.getConfigRSMQ();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        def os = System.getProperty("os.name");
        File installFile = null;

        if ("RABBITMQ".equalsIgnoreCase(config.getProduct()))
        {
            if (os.contains("Win"))
            {
                installFile = new File(dist + "/" + initLocationWinRabbitmq + "/" + serviceFilename);
            }
            if (os.contains("Linux"))
            {
                installFile = new File(dist + "/" + initLocationLnxRabbitmq + "/" + initFilename);
            }
            if (os.contains("SunOS"))
            {
                installFile = new File(dist + "/" + initLocationSolRabbitmq + "/" + initFilename);
            }
        }

        //configure init.rsmq or install-service.bat

        if (installFile != null && installFile.exists())
        {
            byte[] installFileBytes = new byte[installFile.length()];
            FileInputStream fis = new FileInputStream(installFile);
            fis.read(installFileBytes);
            fis.close();

            String installFileStr = new String(installFileBytes);

            def replaceMap = config.rsmqRunReplaceValues();

            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                value = Matcher.quoteReplacement(value);
                installFileStr = installFileStr.replaceFirst(regex, value);
            }
            if (os.contains("Linux") || os.contains("SunOS"))
            {
                dist = Matcher.quoteReplacement(dist);
                installFileStr = installFileStr.replaceAll("INSTALLDIR", dist);
            }

            installFileBytes = installFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(installFile);
            fos.write(installFileBytes);
            fos.close();

            println "Configured File: " + installFile.getAbsolutePath();
            LOG.warn("Configured File: " + installFile.getAbsolutePath());
        }
        else
        {
            if (installFile!= null)
            {
                println "Cannot Find RSMQ install script: " + installFile.getAbsolutePath();
                LOG.warn("Cannot Find RSMQ install script: " + installFile.getAbsolutePath());
            }
            else
            {
                println "Unknown OS Type: " + os;
                LOG.warn("Unknown OS Type: " + os);
            }
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
