import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.regex.Matcher;

final String runFilename = "bin/run.sh";
final String runFilenameWin = "bin/run.bat";

final String instanceName = "logstash";

def help()
{
    println "Usage: logstash-run-configure\n"
    println "This script will configure logstash's run.sh or run.bat";
}
try
{
    def dist = MAIN.getDist();
    def config = MAIN.getConfigLogstash();

    if (config && dist)
    {
        def os = System.getProperty("os.name");

        File runFile = null;
        if (os.contains("Win"))
        {
            runFile = new File(dist + "/" + instanceName + "/" + runFilenameWin);
        }
        else if (os.contains("Linux"))
        {
            runFile = new File(dist + "/" + instanceName + "/" + runFilename);
        }

        def replaceMap = config.logstashRunReplaceValues();

        //Configure run.sh || run.bat
        if (runFile != null && runFile.exists())
        {
            byte[] runFileBytes = new byte[runFile.length()];
            FileInputStream fis = new FileInputStream(runFile);
            fis.read(runFileBytes);
            fis.close();

            String runFileStr = new String(runFileBytes);

            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                if (regex.contains("Xmx"))
                {
                    println "Logstash Maximum Heap Allocation Set to " + (value - "Xmx");
                    LOG.info("Logstash Maximum Heap Allocation Set to " + value);
                }
                value = Matcher.quoteReplacement(value);
                runFileStr = runFileStr.replaceFirst(regex, value);
            }

            runFileBytes = runFileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(runFile);
            fos.write(runFileBytes);
            fos.close();

            println "Configured File: " + runFile.getAbsolutePath();
            LOG.warn("Configured File: " + runFile.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
