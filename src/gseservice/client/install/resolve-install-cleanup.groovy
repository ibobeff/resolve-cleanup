import com.resolve.util.MCPLog;
import com.resolve.util.MCPConstants;

def help()
{
    println "Usage: resolve-install-cleanup\n"
    println "This script will run cleanup installation files";
}
MCPLog mcpLog = null;

try
{
    def installStatusFileName = MAIN.getDist() + "/rsmgmt/log/" + MCPConstants.MCP_RESOLVE_INSTALL_CLEANUP_STATUS_LOGFILE;
    mcpLog = new MCPLog(installStatusFileName, true, true, MCPConstants.MCP_LOG_ITEM_INSTALL_CLEANUP);
    mcpLog.write(MCPConstants.MCP_FIELD_KEY_START,"Installation Cleanup Started");

    // list of exact files to be removed
    def removeFiles = [
       "blueprint.properties",
       "blueprint-rsremote.properties",
       "install-resolve.sh",
       "install-rsremote.sh",
       "install-rsproxy.sh",
       "install-rsarchive.sh",
       "patch-resolve.sh",
       "mysqlconf.tar.gz",
       "oracleconf.tar.gz",
       "db2conf.tar.gz",
       "rs-common.tar.gz",
       "rs-thirdparty.tar.gz",
       "rsmq.tar.gz",
	   "dcs.tar.gz",
       "rszk.tar.gz",
       "rsconsole-shared.tar.gz",
       "rscontrol-shared.tar.gz",
       "rsremote-shared.tar.gz",
       "rsview-shared.tar.gz",
       "rsmgmt-shared.tar.gz",
       "cassandra.tar.gz",
       "rszk.tar.gz",
       "license.txt",
       "elasticsearch.tar.gz",
       "rssync-shared.tar.gz",
       "rsproxy-shared.tar.gz",
       "rsarchive-shared.tar.gz",
       "rslog-shared.tar.gz",
       "logstash.tar.gz"
    ]

    // list of match patterns to be removed
    def matchFiles = [
       /.*resolve-.*\.tar(\.gz)?/,
       /.*rsremote-.*\.tar(\.gz)?/,
       /.*rsproxy-.*\.tar(\.gz)?/
    ]

    // list of files to move
    def moveFiles = [
        "gtar":"bin/gtar",
    ]

    // init install dir
    def dist = BLUEPRINT.getProperty("DIST")

    // remove exact files
    removeFiles.each { filename ->
        File file = new File(dist+"/"+filename)
        if (file.exists())
        {
            println "Removing file: "+file.getName()
            file.delete();
        }
    }

    // remove match files
    matchFiles.each { filename ->
        def pattern = ~filename
        new File(dist).eachFileMatch(pattern) { file ->
            if (file.exists())
            {
                println "Removing file: "+file.getName()
                file.delete();
            }
        }
    }

    // move files
    for (entry in moveFiles)
    {
        File file = new File(dist+"/"+entry.key)
        if (file.exists())
        {
            File dest = new File(dist+"/"+entry.value)

            println "moving file: "+file.getName()
            file.renameTo(dest)
        }
    }
}
catch (Exception e)
{
    println "\nUnexpected Exception, Cancelling Cleanup-Installation\n" + e.getMessage();
    LOG.warn("Unexpected Exception, Cancelling Cleanup-Installation", e);
    mcpLog.warn("Unexpected Exception, Cancelling Cleanup-Installation: " + e.getMessage());
}

mcpLog?.write(MCPConstants.MCP_FIELD_KEY_END,"Installation Cleanup Ended");

return null;
