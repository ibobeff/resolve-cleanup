import java.io.File;
import com.resolve.util.XDoc;

final String configFilename = "config/config.xml";

def help()
{
    println "Usage: rsremote-configure <instance name>\n"
    println "This script will configure the <instance name> rsremote's config.xml";
}
if (args.length < 2)
{
    help();
}
else
{
    try
    {
        def instanceName = args[1].toLowerCase();
        def config = MAIN.getConfigRSRemote(instanceName);
        def dist = MAIN.getDist();

        if (config && dist)
        {
            //Configure config.xml
            File configFile = new File(dist + "/" + instanceName + "/" + configFilename);
            if (configFile.exists())
            {
                def configXPath = config.rsremoteXPathValues();
                XDoc configDoc = new XDoc(configFile);

                configDoc.removeElements("./ESB/QUEUE");

				// SDK gateways configured in blueprint.properties
				def configuredSDKGtws = [];
				
                for (key in configXPath.keySet())
                {
                    def value = configXPath.get(key);
					
					if (key.endsWith("/@IMPLPREFIX"))
					{
						def endIndx = key.indexOf("/@IMPLPREFIX");
						def keyPrefix = key.substring(0,endIndx);
						def beginIndx = keyPrefix.lastIndexOf("/");
						
						if (beginIndx != -1)
						{
							def sdkGtwName = keyPrefix.substring(beginIndx + 1);
							
							if (!configuredSDKGtws.contains(sdkGtwName))
							{
								configuredSDKGtws.add(sdkGtwName);
							}
						}
					}
					
                    if (value instanceof String)
                    {
                        configDoc.setStringValue(key, value);
                    }
                    else if (value instanceof List)
                    {
                        if (!value.isEmpty() && value.get(0) instanceof Map)
                        {
                            configDoc.setListMapValue(key, value);
                        }
                        else if (!value.isEmpty() && value.get(0) instanceof String)
                        {
                            configDoc.setListValue(key, value);
                        }
                    }
                }
				
				// Remove Gateway configurations for gateways not found in blueprint.properties
				
				def sdkGtwElements = configDoc.getNodes("/CONFIGURATION/RECEIVE/*[@IMPLPREFIX]");
				def sdkGtwElmNamesToPurge = [];
				
				for (sdkGtwElement in sdkGtwElements)
				{
					if (!configuredSDKGtws.contains(sdkGtwElement.getName()))
					{
						sdkGtwElmNamesToPurge.add(sdkGtwElement.getName());
					}
				}
				
				if (!sdkGtwElmNamesToPurge.isEmpty())
				{
					for (sdkGtwElmNameToPurge in sdkGtwElmNamesToPurge)
					{
						println "Removing ./RECEIVE/" + sdkGtwElmNameToPurge + " from config.xml...";
						configDoc.removeElement("./RECEIVE/" + sdkGtwElmNameToPurge);
					}
				}
				
                configDoc.toPrettyFile(configFile);

                println "Configured File: " + configFile.getAbsolutePath();
                LOG.warn("Configured File: " + configFile.getAbsolutePath());
            }
            else
            {
                println "Cannot Find " + instanceName + " config file: " + configFile.getAbsolutePath();
                LOG.warn("Cannot Find " + instanceName + " config file: " + configFile.getAbsolutePath());
            }
        }
        else
        {
            println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
            LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
        }
    }
    catch (Exception e)
    {
        println "Unexpected Exception while Running Configuration: " + e.getMessage();
        LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
    }
}
return null;
