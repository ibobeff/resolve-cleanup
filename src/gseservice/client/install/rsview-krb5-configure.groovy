import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

final String krb5Filename = "tomcat/webapps/resolve/WEB-INF/krb5.conf";

def help()
{
    println "Usage: rsview-krb5-configure\n"
    println "This script will configure rsview's krb5.conf";
}
try
{
    def config = MAIN.getConfigRSView();
    def dist = MAIN.getDist();

    if (config && dist)
    {
        File krb5File = null;
        krb5File = new File(dist + "/" + krb5Filename);

        def replaceMap = config.rsviewKrb5ReplaceValues();

        //Configure krb5.conf
        if (krb5File != null && krb5File.exists())
        {
            byte[] krb5FileBytes = new byte[krb5File.length()];
            FileInputStream fis = new FileInputStream(krb5File);
            fis.read(krb5FileBytes);
            fis.close();

            String krb5FileStr = new String(krb5FileBytes);

            for (regex in replaceMap.keySet())
            {
                def value = replaceMap.get(regex);
                krb5FileStr = krb5FileStr.replaceFirst(regex, value);
            }

            krb5FileBytes = krb5FileStr.getBytes();
            FileOutputStream fos = new FileOutputStream(krb5File);
            fos.write(krb5FileBytes);
            fos.close();

            println "Configured File: " + krb5File.getAbsolutePath();
            LOG.warn("Configured File: " + krb5File.getAbsolutePath());
        }
        else
        {
            println "Cannot Find rsview's install script: " + krb5File.getAbsolutePath();
            LOG.warn("Cannot Find rsview's install script: " + krb5File.getAbsolutePath());
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
