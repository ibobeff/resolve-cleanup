import java.util.regex.Matcher;
import com.resolve.util.MCPLog;
import com.resolve.util.MCPConstants;

def help()
{
    println "Usage: resolve-install-setup\n"
    println "This script will set up the rsview system.resolve.url property";
}

MCPLog mcpLog = null;

//Setup system.resolve.url rsview property
try
{
    def installStatusFileName = MAIN.getDist() + "/rsmgmt/log/" + MCPConstants.MCP_RESOLVE_INSTALL_SETUP_STATUS_LOGFILE;
    mcpLog = new MCPLog(installStatusFileName, true, true, MCPConstants.MCP_LOG_ITEM_INSTALL_SETUP);
    mcpLog.write(MCPConstants.MCP_FIELD_KEY_START, "Setup Started");

    if (MAIN.isBlueprintRSView() && MAIN.getConfigRSView().isPrimary())
    {
        def dnsName = BLUEPRINT.getProperty("DNS_HOST");
        if (dnsName)
        {
            def rsviewPropertiesFile = new File("tomcat/webapps/resolve/WEB-INF/rsview.properties");
            if (rsviewPropertiesFile.exists())
            {
                byte[] rsviewFileBytes = new byte[rsviewPropertiesFile.length()];
                FileInputStream fis = new FileInputStream(rsviewPropertiesFile);
                fis.read(rsviewFileBytes);
                fis.close();

                String rsviewFileStr = new String(rsviewFileBytes);

                def url = "";
                def http = true;
                if ("true".equalsIgnoreCase(BLUEPRINT.getProperty("rsview.tomcat.https")))
                {
                    http = false;
                    url += "https://"
                }
                else
                {
                    url += "http://"
                }
                url += dnsName;

                def port = BLUEPRINT.getProperty("DNS_PORT");
                if (!port)
                {
                    if (http && BLUEPRINT.getProperty("rsview.tomcat.connector.http.port"))
                    {
                        port = BLUEPRINT.getProperty("rsview.tomcat.connector.http.port");
                    }
                    else if (BLUEPRINT.getProperty("rsview.tomcat.connector.https.port"))
                    {
                        port = BLUEPRINT.getProperty("rsview.tomcat.connector.https.port");
                    }
                }

                if (port && !(port == "80" || port == "443"))
                {
                    url += ":" + port;
                }
                url += "/resolve";

                rsviewFileStr = rsviewFileStr.replaceAll("system.resolve.url=.+?(\\s+)(##.*)", "system.resolve.url=$url\$1\$2".toString());

                rsviewFileBytes = rsviewFileStr.getBytes();
                FileOutputStream fos = new FileOutputStream(rsviewPropertiesFile);
                fos.write(rsviewFileBytes);
                fos.close();

                println "Configured Resolve URL Property: " + dnsName;
                LOG.warn("Configured File: " + rsviewPropertiesFile.getAbsolutePath());
                LOG.debug("Set system.resolve.url=" + url);
                mcpLog.write(MCPConstants.MCP_INSTALL_SETUP_RESOLVE_URL_CONFIGURED, "Set system.resolve.url=" + url);
            }
        }
        else
        {
            println "No DNS Host name given, leaving as default";
            LOG.warn("No DNS Host name given, leaving as default")
            mcpLog.warn("No DNS Host name given, leaving as default");
        }
    }
    else
    {
        println "Installation Not Configured As Primary, Skipping Resolve System URL Setup";
        LOG.warn("Installation Not Configured As Primary, Skipping Resolve System URL Setup");
        mcpLog.warn("Installation Not Configured As Primary, Skipping Resolve System URL Setup");
    }
}
catch (Exception e)
{
    println "Unexpected Exception, Cancelling Resolve System URL Setup\n" + e.getMessage();
    LOG.warn("Unexpected Exception, Cancelling Resolve System URL Setup", e);
    mcpLog.warn("Unexpected Exception, Cancelling Resolve System URL Setup: " + e.getMessage());
}
mcpLog?.write(MCPConstants.MCP_FIELD_KEY_END, "Setup Installation Ended");

return null;
