import java.util.stream.Collectors
import com.resolve.dcs.properties2yaml.ConversionStatus
import com.resolve.dcs.properties2yaml.Properties2YamlConverter
import com.resolve.dcs.properties2yaml.Properties2YamlConverter.YamlConversionResult

final String ymlFilename = "application.yml";
final String instanceName = "rsreporting";

def help() {
  println "Usage: dcs-rsreporting-yml-configure\n";
  println "This script will configure RSReporting application.yml";
}

try {
  def dist = MAIN.getDist();
  def config = MAIN.getConfigDCS();
  if (config && dist) {
    def dcsProperties = config.getDCSProperties();
    StringBuffer sb = new StringBuffer();
    for (key in dcsProperties.keySet()) {
      def value = dcsProperties.get(key);
      if (key.startsWith("dcs.rsreporting.")) {
        key = key.split("dcs.rsreporting.")[1];
		if(key.startsWith("logging.level")) {
			key = key.split("logging.level.")[1];
			key = key.replace(".", "_");
			key = "logging.level." + key;
		}
        sb.append(key + "=" + value + "\n");
      }
    }
    Properties2YamlConverter converter = new Properties2YamlConverter();
    YamlConversionResult result = converter.convert(sb.toString());
    if (result.getSeverity() != ConversionStatus.OK) {
      throw new IllegalStateException(
      "Problem during conversion: \n" + result.getStatus().getEntries());
    }
    String yamlConfig = result.getYaml();
    String[] lines = yamlConfig.split("\n");
    yamlConfig = Arrays.stream(lines)
		.map{line -> line.trim().startsWith("org_") || line.trim().startsWith("com_") ? line.replace("_", ".") : line}
		.collect(Collectors.joining("\n"));
    File ymlFile = new File(dist + "/dcs/" + instanceName + "/" + ymlFilename);
    ymlFile.write yamlConfig;
    println "Configured File: " + ymlFile.getAbsolutePath();
    LOG.info("Configured File: " + ymlFile.getAbsolutePath());
  }
}
catch (Exception e) {
  println "Unexpected Exception while Running Configuration: " + e.getMessage();
  LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}

return null;
