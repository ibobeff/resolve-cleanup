def help()
{
    println "Usage: rsremote-softlink-configure\n";
    println "This script will create softlinks in the bin folder to all installed components";
    println "This script is for Unix installs only";
}
try
{
    def dist = MAIN.getDist();

    if (dist)
    {
        def os = System.getProperty("os.name");
        if (os.contains("Linux") || os.contains("SunOS"))
        {
            def binFolder = new File(dist + "/bin");

            if (binFolder.isDirectory())
            {
                def binFiles = binFolder.listFiles();
                for (file in binFiles)
                {
                    if (file.getName().startsWith("init.rs") || file.getName().equals("init.logstash"))
                    {
                        LOG.info("Removing File " + file.getAbsolutePath());
                        if (!file.delete())
                        {
                            println "Failed to remove existing Softlink " + file.getName();
                            LOG.warn("Failed to remove existing Softlink " + file.getAbsolutePath());
                        }
                    }
                }
            }

            def cmdBase = "ln -s " + dist;
            def rsremote = MAIN.isBlueprintRSRemote();
            if (rsremote)
            {
                def instances = MAIN.getRSRemoteInstances();
                if (instances)
                {
                    for (instance in instances)
                    {
                        def link = "bin/init.${instance}";
                        def file =  new File(link);
                        if (!file.exists())
                        {
                            def output = new StringBuffer();
                            def cmd = cmdBase + "/${instance}/bin/init.${instance} ${link}";
                            LOG.warn("Executing command: " + cmd);
                            println "Creating Softlink: " + link;

                            def proc = cmd.execute();
                            proc.consumeProcessOutput(output, output);
                            proc.waitFor();
                            Thread.sleep(500);
                            println output.toString();
                            LOG.warn("output: " + output.toString());
                        }
                        else
                        {
                            println "Softlink " + link + " Already Created, Skipping";
                            LOG.warn("Softlink File" + file.getAbsolutePath() + " Already Created");
                        }
                    }
                }
            }

            def rscontrol = MAIN.isBlueprintRSControl();
            if (rscontrol)
            {
                def link = "bin/init.rscontrol";
                def file =  new File(link);
                if (!file.exists())
                {
                    def output = new StringBuffer();
                    def cmd = cmdBase + "/rscontrol/bin/init.rscontrol ${link}";
                    LOG.warn("Executing command: " + cmd);
                    println "Creating Softlink: " + link;

                    def proc = cmd.execute();
                    proc.consumeProcessOutput(output, output);
                    proc.waitFor();
                    Thread.sleep(500);
                    println output.toString();
                    LOG.warn("output: " + output.toString());
                }
                else
                {
                    println "Softlink " + link + " Already Created, Skipping";
                    LOG.warn("Softlink File" + file.getAbsolutePath() + " Already Created");
                }
            }

            def rsview = MAIN.isBlueprintRSView();
            if (rsview)
            {
                def link = "bin/init.rsview";
                def file =  new File(link);
                if (!file.exists())
                {
                    def output = new StringBuffer();
                    def cmd = cmdBase + "/tomcat/bin/init.rsview ${link}";
                    LOG.warn("Executing command: " + cmd);
                    println "Creating Softlink: " + link;

                    def proc = cmd.execute();
                    proc.consumeProcessOutput(output, output);
                    proc.waitFor();
                    Thread.sleep(500);
                    println output.toString();
                    LOG.warn("output: " + output.toString());
                }
                else
                {
                    println "Softlink " + link + " Already Created, Skipping";
                    LOG.warn("Softlink File" + file.getAbsolutePath() + " Already Created");
                }
            }

            def rsmq = MAIN.isBlueprintRSMQ();
            if (rsmq)
            {
                def link = "bin/init.rsmq";
                def file =  new File(link);
                if (!file.exists())
                {
                    def product = MAIN.getConfigRSMQ().getProduct();
                    def cmd;
                    if ("RABBITMQ".equalsIgnoreCase(product))
                    {
                        if (os.contains("Linux"))
                        {
                            os = "linux64";
                        }
                        if (os.contains("SunOS"))
                        {
                            os = "solaris";
                        }
                        cmd = cmdBase + "/rabbitmq/${os}/rabbitmq/sbin/init.rsmq ${link}";
                    }

                    if (cmd)
                    {
                        LOG.warn("Executing command: " + cmd);
                        println "Creating Softlink: " + link;

                        def output = new StringBuffer();
                        def proc = cmd.execute();
                        proc.consumeProcessOutput(output, output);
                        proc.waitFor();
                        Thread.sleep(500);
                        println output.toString();
                        LOG.warn("output: " + output.toString());
                    }
                    else
                    {
                        println "WARNING!!! Unknown RSMQ Product, skipping softlink creation";
                        LOG.error("Unknown RSMQ Product: " + product + ", skipping softlink creation");
                    }
                }
                else
                {
                    println "Softlink " + link + " Already Created, Skipping";
                    LOG.warn("Softlink File" + file.getAbsolutePath() + " Already Created");
                }
            }
			
			def rsdataloader = MAIN.isBlueprintDCSRSDataLoader();
			if (rsdataloader)
			{
				def link = "bin/init.rsdataloader";
				def file =  new File(link);
				if (!file.exists())
				{
					
					def	cmd = cmdBase + "/dcs/rsdataloader/init.rsdataloader ${link}";
					LOG.warn("Executing command: " + cmd);
					println "Creating Softlink: " + link;

					def output = new StringBuffer();
					def proc = cmd.execute();
					proc.consumeProcessOutput(output, output);
					proc.waitFor();
					Thread.sleep(500);
					println output.toString();
					LOG.warn("output: " + output.toString());
				}
				else
				{
					println "Softlink " + link + " Already Created, Skipping";
					LOG.warn("Softlink File" + file.getAbsolutePath() + " Already Created");
				}
			}
			
			def rsreporting = MAIN.isBlueprintDCSRSReporting();
			if (rsreporting)
			{
				def link = "bin/init.rsreporting";
				def file =  new File(link);
				if (!file.exists())
				{
					
					def	cmd = cmdBase + "/dcs/rsreporting/init.rsreporting ${link}";
					LOG.warn("Executing command: " + cmd);
					println "Creating Softlink: " + link;

					def output = new StringBuffer();
					def proc = cmd.execute();
					proc.consumeProcessOutput(output, output);
					proc.waitFor();
					Thread.sleep(500);
					println output.toString();
					LOG.warn("output: " + output.toString());
				}
				else
				{
					println "Softlink " + link + " Already Created, Skipping";
					LOG.warn("Softlink File" + file.getAbsolutePath() + " Already Created");
				}
			}
			
            def rsmgmt = MAIN.isBlueprintRSMgmt();
            if (rsmgmt)
            {
                def link = "bin/init.rsmgmt";
                def file =  new File(link);
                if (!file.exists())
                {
                    def output = new StringBuffer();
                    def cmd = cmdBase + "/rsmgmt/bin/init.rsmgmt ${link}";
                    LOG.warn("Executing command: " + cmd);
                    println "Creating Softlink: " + link;

                    def proc = cmd.execute();
                    proc.consumeProcessOutput(output, output);
                    proc.waitFor();
                    Thread.sleep(500);
                    println output.toString();
                    LOG.warn("output: " + output.toString());
                }
                else
                {
                    println "Softlink " + link + " Already Created, Skipping";
                    LOG.warn("Softlink File" + file.getAbsolutePath() + " Already Created");
                }
            }
            def rssearch = MAIN.isBlueprintRSSearch();
            if (rssearch)
            {
                def link = "bin/init.rssearch";
                def file =  new File(link);
                if (!file.exists())
                {
                    def output = new StringBuffer();
                    def cmd = cmdBase + "/elasticsearch/bin/init.rssearch ${link}";
                    LOG.warn("Executing command: " + cmd);
                    println "Creating Softlink: " + link;

                    def proc = cmd.execute();
                    proc.consumeProcessOutput(output, output);
                    proc.waitFor();
                    Thread.sleep(500);
                    println output.toString();
                    LOG.warn("output: " + output.toString());
                }
                else
                {
                    println "Softlink " + link + " Already Created, Skipping";
                    LOG.warn("Softlink File" + file.getAbsolutePath() + " Already Created");
                }
            }
            def rssync = MAIN.isBlueprintRSSync();
            if (rssync)
            {
                def link = "bin/init.rssync";
                def file =  new File(link);
                if (!file.exists())
                {
                    def output = new StringBuffer();
                    def cmd = cmdBase + "/rssync/bin/init.rssync ${link}";
                    LOG.warn("Executing command: " + cmd);
                    println "Creating Softlink: " + link;

                    def proc = cmd.execute();
                    proc.consumeProcessOutput(output, output);
                    proc.waitFor();
                    Thread.sleep(500);
                    println output.toString();
                    LOG.warn("output: " + output.toString());
                }
                else
                {
                    println "Softlink " + link + " Already Created, Skipping";
                    LOG.warn("Softlink File" + file.getAbsolutePath() + " Already Created");
                }
            }
            def rsarchive = MAIN.isBlueprintRSArchive();
            if (rsarchive)
            {
                def link = "bin/init.rsarchive";
                def file =  new File(link);
                if (!file.exists())
                {
                    def output = new StringBuffer();
                    def cmd = cmdBase + "/rsarchive/bin/init.rsarchive ${link}";
                    LOG.warn("Executing command: " + cmd);
                    println "Creating Softlink: " + link;

                    def proc = cmd.execute();
                    proc.consumeProcessOutput(output, output);
                    proc.waitFor();
                    Thread.sleep(500);
                    println output.toString();
                    LOG.warn("output: " + output.toString());
                }
                else
                {
                    println "Softlink " + link + " Already Created, Skipping";
                    LOG.warn("Softlink File" + file.getAbsolutePath() + " Already Created");
                }
            }
            def logstash = MAIN.isBlueprintLogstash();
            if (logstash)
            {
                def link = "bin/init.logstash";
                def file =  new File(link);
                if (!file.exists())
                {
                    def output = new StringBuffer();
                    def cmd = cmdBase + "/logstash/bin/init.logstash ${link}";
                    LOG.warn("Executing command: " + cmd);
                    println "Creating Softlink: " + link;

                    def proc = cmd.execute();
                    proc.consumeProcessOutput(output, output);
                    proc.waitFor();
                    Thread.sleep(500);
                    println output.toString();
                    LOG.warn("output: " + output.toString());
                }
                else
                {
                    println "Softlink " + link + " Already Created, Skipping";
                    LOG.warn("Softlink File" + file.getAbsolutePath() + " Already Created");
                }
            }
            def rslogEnabled = MAIN.isBlueprinRSLog();
            if (rslogEnabled) {
                def link = "bin/init.rslog";
                def file =  new File(link);
                if (!file.exists())
                {
                    def output = new StringBuffer();
                    def cmd = cmdBase + "/rslog/bin/init.rslog ${link}";
                    LOG.warn("Executing command: " + cmd);
                    println "Creating Softlink: " + link;

                    def proc = cmd.execute();
                    proc.consumeProcessOutput(output, output);
                    proc.waitFor();
                    Thread.sleep(500);
                    println output.toString();
                    LOG.warn("output: " + output.toString());
                }
                else
                {
                    println "Softlink " + link + " Already Created, Skipping";
                    LOG.warn("Softlink File" + file.getAbsolutePath() + " Already Created");
                }
            }
            def link = "bin/blueprint.properties";
            def file =  new File(link);
            if (!file.exists())
            {
                def output = new StringBuffer();
                def cmd = cmdBase + "/rsmgmt/config/blueprint.properties ${link}";
                LOG.warn("Executing command: " + cmd);
                println "Creating Softlink: " + link;

                def proc = cmd.execute();
                proc.consumeProcessOutput(output, output);
                proc.waitFor();
                Thread.sleep(500);
                println output.toString();
                LOG.warn("output: " + output.toString());
            }
            else
            {
                println "Softlink " + link + " Already Created, Skipping";
                LOG.warn("Softlink File" + file.getAbsolutePath() + " Already Created");
            }
        }
        else
        {
            LOG.warn("Windows System, Skipping Softlink");
        }
    }
    else
    {
        println "Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration";
        LOG.warn("Blueprint Has Not Been Properly Instantiated.  Cancelling Configuration");
    }
}
catch (Exception e)
{
    println "Unexpected Exception while Running Configuration: " + e.getMessage();
    LOG.error("Unexpected Exception while Running Configuration: " + e.getMessage(), e);
}
return null;
