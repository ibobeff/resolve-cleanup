MAIN.setMethod("MMetric.process1Hr", "MDefault.print");

params = new HashMap();
return params;

def help()
{
    println "Usage: Process1Hr";
    println "";
    println "  Instructs the RSMGMT to perform the 1hr processing";
    println "";
    return null;
} // help