import com.resolve.util.MetricThresholdType;

MAIN.setMethod("MMetric.updateMetricThresholdProperties", "MDefault.print");

def help()
{
    println " Usage: UpdateMetricThreshold UpdateMetricThreshold sendmsgto=<GUID component> <metricname>=<metricthresholdtype>";
    println "";
    println " Note: All default <metricname> are available in the threshold.properties file under dist\tomcat\webapps\resolve"; 
    println " Updates the metric thresholds used for reporting alerts";
    println "";
    println " Parameters:";
    println"     sendmsgto=<GUID component> - The address of the GUID component to which message has to be sent.";
    println"     <metricname>=jvm.memory - The metric group followed by the metric name to be updated.";
    println"     <metricthresholdtype>=100,10,warning - The new metric high value, metric low value, and the default alert status of the metric threshold.";
    println "";
    println "  e.g. UpdateMetricThreshold sendmsgto=RSREMOTE jvm.memory=90,10,warning";
    println "  e.g. UpdateMetricThreshold sendmsgto=0F3496DDF165309FDB6DDF4C8C7AC5CB runbook.aborted=10000,100,fatal";
    println "  e.g. UpdateMetricThreshold sendmsgto=RSMGMT database.free_space=1000,25,info";
    
    return null;
}

def params = [:]
if (args.length >= 3)
{
    for(param in args)
    {
        if(param.contains("sendmsgto="))
        {
            params["GUID"] = param.split("=")[1];
        }
        else
        {
            String metricname = param;
            String metricvalue = param.split("=")[1];
            MetricThresholdType mtype = new MetricThresholdType();
            mtype.setHigh(metricvalue.split(",")[0]);
            mytpe.setLow(metricvalue.split(",")[1]);
            mtype.setAlertstatus(metricvalue.split(",")[2]);
            params.put(metricname, mtype);
        }
    }
}
else
{
    help();
}

return params;