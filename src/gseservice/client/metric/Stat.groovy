import groovy.sql.Sql;
import java.lang.Runtime;
import java.text.DecimalFormat;
import groovy.sql.GroovyRowResult;
import java.io.FileOutputStream;
import java.io.File;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import com.resolve.util.RESTUtils;
import java.net.URLEncoder;
import net.sf.json.JSONObject;
import net.sf.json.JSONException;
import org.apache.commons.beanutils.DynaBean;


String outputFilePath = ""; 
int interval = 365; 
Map dbInfo;

// Users

int usersTotalRegistered = -1;
int usersActive = -1;
int usersActiveHighestDaily = -1;
int usersActiveAverageDaily = -1;
int usersNonLocked = -1;

// Wiki

int wikiTotal = -1;
int wikiNotDeleted = -1;

// Decision Trees

int decisionTreesTotal = -1;
int decisionTreesNotDeleted = -1;

// Runbooks

int runbooksTotal = -1;
int runbooksNotDeleted = -1;
int totalDailyRunbookExecutions = -1;
int averageDailyRunbookExecutions = -1;

// Worksheets

String worksheetsTotal = "-1";
String worksheetsTotalYear = -1;
String worksheetsArchived = -1;
String processRequestsTotal = "-1";
String taskResultsTotal = "-1";

// Actiontasks

int actionTasksHighestPerMinute = -1;

// System

String ram = "";
int cpuCount = -1;
int startupLatencyPeak = -1;
int startupLatencyAverage = -1;

// Stat

StringBuilder fileOutputString = new StringBuilder(); // this is what is used to write the CSV file.
Boolean isOracle = false;
int paddingValue = 50;
int paddingTabOffset = 7;
StringBuilder queryString = new StringBuilder();
Statement statement;
GroovyRowResult rows;
String titleName = "Resolve Customer Stat Collection and Analysis";
String ipAddress = "127.0.0.1";


    

// BEGIN

println "********************";
println titleName;
println "********************";

fileOutputString.append(titleName + "\n\n");

// Check to ensure that the DB has been properly connected

if(!binding.variables.containsKey("DB")){
    println "** ERROR **: DB CONNECTION NOT FOUND.";
    println "Before executing this script, please run 'DBConnect connection1' in the RSConsole to connect to the Resolve database.\n";
} else {

    if(args.size() > 3){
        println "ERROR: Too many arguments.";
        return help();
    } else if(args.size() > 2){
        ipAddress = args[2];
    }
    
    
    
    // Check to see if we're using MySQL or Oracle
    
    String dbtype = DB.toString().substring(0, 6);
    if(dbtype.equalsIgnoreCase("oracle")){
        isOracle = true;
        println "DBType: Oracle";
    } else {
        println "DBType: MySQL";
    }
    
    
    
    
    // Get the interval in days from the command line
    
    println "Getting interval...";
    if(args.size() == 2){
        interval = Integer.parseInt(args[1]);
    }  
    
    
    
    println "INTERVAL (Days): " + interval;
    println "Connecting to database...";
    println "Database: " + DB;
    
    
    
    
    // Connect to the database
    def sql = new Sql(DB);
    
    
    
    
    println "\n---!! BEGIN STAT COLLECTION !!---";
    
    
    
    
    println "*********************************";
    println "-------- SYSTEM --------";
    fileOutputString.append("SYSTEM\n\n");
    
    DecimalFormat df = new DecimalFormat("##.##");
    Runtime runtime = Runtime.getRuntime();
    
    println "RAM: ";
    fileOutputString.append("RAM\n");
    
    String totalRam = df.format(runtime.totalMemory()/1024);
    fileOutputString.append("\"Total ram\"," + totalRam + ",MB\n");
    println "\ttotal:".padRight(paddingValue-paddingTabOffset) + totalRam + " MB"; 
    
    String usedMemory = df.format((runtime.totalMemory() - runtime.freeMemory())/1024);
    fileOutputString.append("\"Used memory\"," + usedMemory + ",MB\n");
    println "\tUsed:".padRight(paddingValue-paddingTabOffset) + usedMemory + " MB";
    
    String freeMemory = df.format(runtime.freeMemory()/1024);
    fileOutputString.append("\"Available memory\"," + freeMemory + ",MB\n");
    println "\tAvailable:".padRight(paddingValue-paddingTabOffset) + freeMemory + " MB";
    
    cpuCount = Runtime.getRuntime().availableProcessors();
    fileOutputString.append("\"CPU count\"," + cpuCount + "\n");    
    println "Number of CPUs".padRight(paddingValue) + cpuCount;
    
    println "____________________________\n";
    
    
    
    
    println "*********************************";
    println "-------- USERS --------";
    
    rows = sql.rows( "SELECT COUNT(*) FROM sys_user" );
    usersTotalRegistered = rows.getAt(0);
    
    rows = sql.rows( "SELECT COUNT(*) FROM sys_user WHERE locked_out = 0 OR locked_out = NULL" );
    usersNonLocked = rows.getAt(0);
    
    
    if(isOracle){
        rows = sql.rows( "SELECT COUNT(*) FROM sys_user WHERE last_login > sysdate - " + interval );
    } else {
        rows = sql.rows( "SELECT COUNT(*) FROM sys_user WHERE last_login BETWEEN DATE_SUB(last_login, INTERVAL " + interval + " DAY) AND NOW()" );
    } 
    usersActive = rows.getAt(0);
    
    
    if(isOracle){
        rows = sql.rows( "SELECT MAX(T) FROM (SELECT COUNT(ts_datetime) AS T FROM metric_day_users WHERE ts_datetime > sysdate - " + interval + "GROUP BY ts_datetime)");
    } else {
        rows = sql.rows( "SELECT MAX(T) from (SELECT COUNT(ts_datetime) as T from metric_day_users WHERE (ts_datetime BETWEEN DATE_SUB(NOW(), INTERVAL " + interval + " DAY) AND NOW()) GROUP BY ts_datetime) as alias");
        //rows = sql.rows( "SELECT MAX(*) FROM metric_day_users WHERE ts_datetime BETWEEN DATE_SUB(NOW(), INTERVAL " + interval + " DAY) AND NOW()" );
    }
    
    if(rows.getAt(0) != null){
        usersActiveHighestDaily = rows.getAt(0);
    } 
    
    if(isOracle){
        rows = sql.rows( "SELECT AVG(T) FROM (SELECT COUNT(ts_datetime) AS T FROM metric_day_users WHERE ts_datetime > sysdate - " + interval + "GROUP BY ts_datetime)");
    } else {
        rows = sql.rows( "SELECT AVG(T) from (SELECT COUNT(ts_datetime) as T from metric_day_users WHERE (ts_datetime BETWEEN DATE_SUB(NOW(), INTERVAL " + interval + " DAY) AND NOW()) GROUP BY ts_datetime) as alias");
    }
    if(rows.getAt(0) != null){
        usersActiveAverageDaily = rows.getAt(0);
    } 
    
    fileOutputString.append("\"Registered users (total)\"," + usersTotalRegistered + "\n");    
    println "Registered users (total): ".padRight(paddingValue) + usersTotalRegistered;
    fileOutputString.append("\"Non-locked users\"," + usersNonLocked + "\n"); 
    println "Non-locked users: ".padRight(paddingValue) + usersNonLocked;
    String activeLabel = "Active users (over " + interval + " days)";
    fileOutputString.append("\"" + activeLabel + "\"," + usersActive + "\n"); 
    println activeLabel.padRight(paddingValue) + usersActive;
    
    def usersActiveHighestDailyValue = "";
    if(usersActiveHighestDaily == -1){
       usersActiveHighestDailyValue = "No records found."
    } else {
        usersActiveHighestDailyValue = usersActiveHighestDaily;
    }
    
    String highestDailyActiveUsersTag = "Highest # of daily active users (" + interval + " days)";
    fileOutputString.append("\"" + highestDailyActiveUsersTag + "\"," + usersActiveHighestDailyValue + "\n"); 
    println highestDailyActiveUsersTag.padRight(paddingValue) + usersActiveHighestDailyValue;
    
    def usersActiveAverageDailyValue = "";
    if(usersActiveHighestDaily == -1){
       usersActiveAverageDailyValue = "No records found."
    } else {
        usersActiveAverageDailyValue = usersActiveAverageDaily;
    }
    
    String averageDailyActiveUsersTag ="Average # of daily active users (" + interval + " days)" 
    fileOutputString.append("\"" + averageDailyActiveUsersTag + "\"," + usersActiveAverageDailyValue + "\n\n"); 
    println averageDailyActiveUsersTag.padRight(paddingValue) + usersActiveAverageDailyValue;
    
    println "____________________________\n";
    
    
    
    
    println "********************************";
    println "----------- WIKI ---------------";
    fileOutputString.append("WIKI\n"); 
    
    rows = sql.rows( "SELECT count(*) FROM wikidoc" );
    wikiTotal = rows.getAt(0);
    fileOutputString.append("\"Total wikis\"," + wikiTotal + "\n"); 
    println "Total wikis:".padRight(paddingValue) + wikiTotal;
    
    rows = sql.rows( "SELECT count(*) FROM wikidoc WHERE sys_is_deleted = 'N' OR sys_is_deleted IS NULL OR sys_is_deleted = ''" );
    wikiNotDeleted = rows.getAt(0);
    fileOutputString.append("\"Total wikis that aren't flagged for deletion\"," + wikiNotDeleted + "\n"); 
    println "Total wikis that aren't flagged for deletion:".padRight(paddingValue) + wikiNotDeleted;
    
    println "Total wikis by namespace: ";  
    fileOutputString.append("\"Total wikis by namespace\"\n");   
    sql.eachRow("SELECT COUNT(*) AS \"count\", u_namespace, sys_is_deleted FROM wikidoc GROUP BY u_namespace, sys_is_deleted"){ row ->
        fileOutputString.append(" ,\"$row.u_namespace\",$row.count\n");
        println("\t${row.u_namespace.padRight(paddingValue - paddingTabOffset)} $row.count");
    }
    fileOutputString.append("\n");
    
    println "____________________________\n";
    
    fileOutputString.append("\n");
    
    
    
    
    println "********************************";
    println "-------- DECISION TREES --------";
    fileOutputString.append("\"DECISION TREES\"\n"); 
    
    rows = sql.rows( "SELECT count(*) FROM wikidoc WHERE u_is_root = 'Y'" );
    decisionTreesTotal = rows.getAt(0);
    
    rows = sql.rows( "SELECT count(*) FROM wikidoc WHERE u_is_root = 'Y' AND (sys_is_deleted = 'N' OR sys_is_deleted IS NULL OR sys_is_deleted = '')" );
    decisionTreesNotDeleted = rows.getAt(0);
    
    fileOutputString.append("\"Total decision trees\"," + decisionTreesTotal + "\n"); 
    println "Total decision trees: ".padRight(paddingValue) + decisionTreesTotal;
    fileOutputString.append("\"Total decision trees (not deleted)\"," + decisionTreesNotDeleted + "\n"); 
    println "Total decision trees (not deleted): ".padRight(paddingValue) + decisionTreesNotDeleted;
    
    println "Total decision trees by namespace: ";
    fileOutputString.append("\"Total decision trees by namespace\"\n");    
    if(decisionTreesTotal == 0){
        fileOutputString.append("\"Total decision trees by namespace\",\"No records found.\"\n"); 
        println "\tNo records found.";
    } else {
        sql.eachRow("SELECT COUNT(*) AS \"count\", u_namespace, sys_is_deleted FROM wikidoc WHERE u_is_root = 'Y' AND (sys_is_deleted IS NULL OR sys_is_deleted = '' OR sys_is_deleted = 'N') GROUP BY u_namespace, sys_is_deleted"){ row ->
            fileOutputString.append(" ,\"$row.u_namespace\",$row.count\n"); 
            println("\t${row.u_namespace.padRight(paddingValue - paddingTabOffset)} $row.count");
        }
    }
    
    println "____________________________\n";
    fileOutputString.append("\n");
   
    
    
    
    println "********************************";
    println "----------- RUNBOOKS -----------";
    fileOutputString.append("RUNBOOKS\n");
    
    // prepare ES query. It's easier to break it into separate lines.
    StringBuilder params = new StringBuilder("");
    params.append("{");
    params.append("\"size\": 0,"); 
    params.append("\"query\": {");
    params.append("\"filtered\": {");
    params.append("\"query\": {");
    params.append("\"match\": {");
    params.append("\"taskName\": \"end\"");
    params.append("}");
    params.append("},");
    params.append("\"filter\": {");
    params.append("\"range\": {");
    params.append("\"timestamp\": {");
    params.append("\"gt\" :  " + ( interval * 86400000 ) ); // day interval * milliseconds per day
    params.append("}");
    params.append("}");
    params.append("}");
    params.append("}");
    params.append("}");
    params.append("}");
                        
    double totalRunbooksExecutions = 0;
    
    println (params.toString());
    
    try{
        // Query elastic search to get total runbook executions over <INTERVAL>
        
        String urlParametersSanitized = URLEncoder.encode(params.toString(), "ISO-8859-1");
        
        //def results1 = RESTUtils.post("http://" + ipAddress + ":9200/taskresultalias/taskresult/_search?" + urlParametersSanitized, "text/plain", null, null);
        def results1 = RESTUtils.get("http://" + ipAddress + ":9200/taskresultalias/taskresult/_search?source=" + urlParametersSanitized, null, null);
        
        
        JSONObject j = JSONObject.fromObject(results1);
        totalRunbooksExecutions = j.getJSONObject("hits").getInt("total");
        runbooksExecutedAverageDaily = totalRunbooksExecutions / (double)interval;
        
    } catch (JSONException je){
        println("An error occurred while retrieving Elastic Search JSON data");
        je.printStackTrace();
    } catch (Exception e){
        println("An error occurred while loading the runbook data.\n");
        e.printStackTrace();
    }
    
    rows = sql.rows( "SELECT count(*) FROM wikidoc WHERE u_has_active_model = 'Y'" );
    runbooksTotal = rows.getAt(0);
    
    rows = sql.rows( "SELECT count(*) FROM wikidoc WHERE u_has_active_model = 'Y' AND (sys_is_deleted = 'N' OR sys_is_deleted IS NULL OR sys_is_deleted = '')" );
    runbooksNotDeleted = rows.getAt(0);
    
    println("Querying Elastic Search the following IP: " + ipAddress);
    
    fileOutputString.append("\"Total runbooks\"," + runbooksTotal + "\n"); 
    println "Total runbooks: ".padRight(paddingValue) + runbooksTotal;
    fileOutputString.append("\"Total runbooks (not deleted)\"," + runbooksNotDeleted + "\n"); 
    println "Total runbooks (not deleted): ".padRight(paddingValue) + runbooksNotDeleted;
    
    String totalDailyRunbookExecTag = "Total runbook executions (" + interval + " days)";
    fileOutputString.append("\"" + totalDailyRunbookExecTag + "\"," + totalRunbooksExecutions + "\n"); 
    println totalDailyRunbookExecTag.padRight(paddingValue) + totalRunbooksExecutions;
    
    String dailyRunbookExecTag = "Average daily runbook executions (" + interval + " days)";
    fileOutputString.append("\"" + dailyRunbookExecTag + "\"," + runbooksExecutedAverageDaily + "\n"); 
    println dailyRunbookExecTag.padRight(paddingValue) + String.format("%.3f",runbooksExecutedAverageDaily);
    
    fileOutputString.append("\"Total runbooks by namespace\"\n");
    println "Total runbooks trees by namespace: ";    
    if(runbooksTotal == 0){
        println "\tNo records found.";
        fileOutputString.append("\"Total runbooks by namespace\",\"No records found.\"\n");
    } else {
        sql.eachRow("SELECT COUNT(*) AS \"count\", u_namespace, sys_is_deleted FROM wikidoc WHERE u_has_active_model = 'Y' AND (sys_is_deleted IS NULL OR sys_is_deleted = '' OR sys_is_deleted = 'N') GROUP BY u_namespace, sys_is_deleted"){ row ->
            println("\t${row.u_namespace.padRight(paddingValue - paddingTabOffset)} $row.count");
            fileOutputString.append(" ,\"$row.u_namespace\",$row.count\n");
        }
    }
    fileOutputString.append("\n");
    
   /*
    fileOutputString.append("Daily runbook executions\n");
    println "Daily runbook executions: ";    
    if(isOracle){
        queryString.append("SELECT COUNT(*) AS \"count\", EXTRACT(DAY FROM ts_datetime) AS \"day\", EXTRACT (MONTH FROM ts_datetime) AS \"month\", EXTRACT (YEAR FROM ts_datetime) AS \"year\" FROM metric_day_runbook WHERE (ts_datetime > sysdate - 365) GROUP BY EXTRACT(MONTH FROM ts_datetime), EXTRACT(DAY FROM ts_datetime), EXTRACT(YEAR FROM ts_datetime) ORDER BY EXTRACT(YEAR FROM ts_datetime), EXTRACT(MONTH FROM ts_datetime), EXTRACT(DAY FROM ts_datetime)");
    } else {
        queryString.append("SELECT ");
        queryString.append("COUNT(*) AS \"count\", ");
        queryString.append("DAYOFMONTH(ts_datetime) AS \"day\", ");
        queryString.append("MONTHNAME(ts_datetime) AS \"month\", ");
        queryString.append("YEAR(ts_datetime) AS \"year\" ");
        queryString.append("FROM ");
        queryString.append("metric_day_runbook ");
        queryString.append("WHERE "); 
        queryString.append("ts_datetime BETWEEN DATE_SUB(NOW(), INTERVAL " + interval + " DAY) AND NOW() ");
        queryString.append("GROUP BY ");
        queryString.append("MONTH(ts_datetime), DAYOFMONTH(ts_datetime), YEAR(ts_datetime) ");
        queryString.append("ORDER BY ");
        queryString.append("YEAR(ts_datetime), ");
        queryString.append("MONTH(ts_datetime), ")
        queryString.append("DAYOFMONTH(ts_datetime)")
    }
    
    String dailyRunbookTab = "";
    sql.eachRow(queryString.toString()){ row ->
        dailyRunbookTab = "[$row.year, $row.month $row.day]";
        fileOutputString.append(" ,\"" + dailyRunbookTab + "\",$row.count\n");
        println("\t" + dailyRunbookTab.padRight(paddingValue - paddingTabOffset) + "$row.count");
    }
    */
    
    println "____________________________\n";
    
    fileOutputString.append("\n");
    
    
    // connect to RSVIEW
    
    if (!MAIN.connected)
    {
        MAIN.executeCommand("CONNECT RSVIEWS");        
    }
    else {
        println "RSVIEW already connected...";
    }
    
    println "****************************";
    println "-------- WORKSHEETS --------";
    fileOutputString.append("WORKSHEET\n");
   
    
    rows = sql.rows( "SELECT COUNT(*) FROM archive_worksheet WHERE sys_is_deleted IS NULL OR sys_is_deleted = '' OR sys_is_deleted = 'N'" );
    if(rows != null){
       worksheetsArchived = String.valueOf(rows.getAt(0));
    } 
    
    if(isOracle){
        rows = sql.rows( "SELECT COUNT(*) FROM archive_worksheet WHERE (sys_is_deleted IS NULL OR sys_is_deleted = '' OR sys_is_deleted = 'N') AND (sys_created_on > sysdate - 365)" );
    } else { 
        rows = sql.rows( "SELECT COUNT(*) FROM archive_worksheet WHERE (sys_is_deleted IS NULL OR sys_is_deleted = '' OR sys_is_deleted = 'N') AND (sys_created_on BETWEEN DATE_SUB(NOW(), INTERVAL 365 DAY) AND NOW() )" );
    }
    if(rows != null){
       worksheetsTotalYear = String.valueOf(rows.getAt(0));
    } 
    
    def paramList = [:];
    def results = MAIN.esb.call("RSVIEW","MIndex.getIndexRecordSize", paramList, 10000);
    
    if(results == null){
        println "Could not connect to RSVIEW and query index record size";
    } else {
        worksheetsTotal = String.valueOf(results["worksheet"]);
        processRequestsTotal = String.valueOf(results["processrequest"]);
        taskResultsTotal = String.valueOf(results["taskresult"]);
        
        fileOutputString.append("\"Total worksheets\"," + worksheetsTotal + "\n"); 
        println("Total worksheets: ".padRight(paddingValue) + worksheetsTotal);
        fileOutputString.append("\"Total archived worksheets\"," + worksheetsArchived + "\n"); 
        println("Total archived worksheets: ".padRight(paddingValue) + worksheetsArchived);
        fileOutputString.append("\"Total archived worksheets (year)\"," + worksheetsTotalYear + "\n"); 
        println("Total archived worksheets (year): ".padRight(paddingValue) + worksheetsTotalYear);
        println "";
        fileOutputString.append("\"Total process requests\"," + processRequestsTotal + "\n"); 
        println("Total process requests:".padRight(paddingValue) + processRequestsTotal); 
        fileOutputString.append("\"Total task results\"," + taskResultsTotal + "\n"); 
        println("Total task results:".padRight(paddingValue) + taskResultsTotal); 
    }
    
    println "____________________________\n";
    
   
   
   
    println "****************************";
    println "--------- COMPLETE ---------";
    
    sql.close();
    
    
    
    
    // Write the CSV file
    
    //println(fileOutputString.toString());
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date now = new Date();
    File saveDestination = new File("rsconsole/file/Stats_" + dateFormat.format(now) + ".csv");
    FileOutputStream fos = new FileOutputStream(saveDestination);
    fos.write(fileOutputString.toString().getBytes());
    fos.close();
    println "File saved to " + saveDestination.getCanonicalPath();
    
    
}


def help()
{
    println "Customer Stat Collection and Analysis ";
    println "Usage: Stat <INTERVAL> <IP ADDRESS>";
    println "";
    println "<INTERVAL> - The number of days to check statistics. Default is 365.";
    println "<IP ADDRESS> - The IP address of the elastic search server you wish to query. Default is 127.0.0.1 .";
    println "";
    return null;
} // help


