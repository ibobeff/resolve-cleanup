MAIN.setMethod("MMetric.process1Day", "MDefault.print");

params = new HashMap();
return params;

def help()
{
    println "Usage: Process1Day";
    println "";
    println "  Instructs the RSMGMT to perform the 1day processing";
    println "";
    return null;
} // help