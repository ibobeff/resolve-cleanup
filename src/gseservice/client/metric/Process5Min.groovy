MAIN.setMethod("MMetric.process5Min", "MDefault.print");

params = new HashMap();
return params;

def help()
{
    println "Usage: Process5Min";
    println "";
    println "  Instructs the RSMGMT to perform the 5min processing";
    println "";
    return null;
} // help