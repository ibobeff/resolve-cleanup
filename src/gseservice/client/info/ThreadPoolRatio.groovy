MAIN.setMethod("MInfo.threadpoolRatio", "MService.info.ThreadPoolRatioCallback");
params = new Hashtable();

return params;


def help()
{
    println "Usage: threadpoolRatio";
    println "";
    println "  Query the specified component(s) for threadpool executor ratio."
    println "";
    return null;
} // help