MAIN.setMethod("MInfo.version", null);
params = new Hashtable();

return params;


def help()
{
    println "Usage: version";
    println "";
    println "  Query the specified component to return the version and build information";
    println "";
    return null;
} // help