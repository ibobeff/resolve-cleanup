MAIN.setMethod("MInfo.info", "MService.info.InfoCallback");
params = new Hashtable();

return params;


def help()
{
    println "Usage: info";
    println "";
    println "  Query the specified component(s) for summary configuration information."
    println "";
    return null;
} // help