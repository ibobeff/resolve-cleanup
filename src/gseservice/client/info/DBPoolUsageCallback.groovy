MAIN.println("");
MAIN.println("DB Available Pool Size and Usage Information");
MAIN.println("");
MAIN.println("  DB Current Pool Size:          "+PARAMS["DB_POOL_SIZE"]);
MAIN.println("  DB Max Pool Size:              "+PARAMS["DB_MAX_POOL_SIZE"]);
MAIN.println("  DB Available Connections :     "+PARAMS["DB_AVAILABLE_POOL_SIZE"]);
MAIN.println("  DB Used Connections:           "+PARAMS["DB_USED_POOL_SIZE"]);
MAIN.println("  DB Pool Busy Ratio (Used/Max): "+PARAMS["DB_POOL_BUSY_RATIO"]);
MAIN.println("  guid:                          "+PARAMS["GUID"]);