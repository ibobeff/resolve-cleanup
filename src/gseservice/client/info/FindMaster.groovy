MAIN.setMethod("MConfig.isMaster", "MService.info.FindMasterCallback");
params = new Hashtable();

return params;

def help()
{
    println "Usage: ismaster";
    println "";
    println "  First connecto to RSCONTROLS before issuing this command to determine which RSControl is the master";
    println "";
    return null;
} // help