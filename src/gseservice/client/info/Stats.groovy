def help()
{
    println "Usage: Stats";
    println "";
    println "  Get various statistics (e.g., no. of wikis, worksheets etc.).";
    println "";
    println "  e.g. stats";
    return null;
}

def params=[:];

MAIN.variables.put(MAIN.VAR_MSG_ROUTE, Constants.ESB_NAME_RSVIEW);
MAIN.variables.set(MAIN.VAR_MSG_CALLBACK, "MService.info.StatsCallback");
MAIN.sendMessage(Constants.ESB_NAME_RSVIEW,"MIndex.getIndexRecordSize",params, "MService.info.StatsCallback");
