MAIN.setMethod("MInfo.dbpoolUsage", "MService.info.DBPoolUsageCallback");
params = new Hashtable();

return params;


def help()
{
    println "Usage: DB Pool Size and Usage";
    println "";
    println "  Query the specified component(s) for DB Pool size and usage."
    println "";
    return null;
} // help