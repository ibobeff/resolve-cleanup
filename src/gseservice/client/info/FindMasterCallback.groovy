MAIN.println("");
MAIN.println("Configuration Information");
MAIN.println("");
if (PARAMS["CONFIGVERSION"] != null)
{
    MAIN.println("  ConfigFile Version: "+PARAMS["CONFIGVERSION"]);
}
MAIN.println("  type:               "+PARAMS["TYPE"]);
MAIN.println("  name:               "+PARAMS["NAME"]);
MAIN.println("  guid:               "+PARAMS["GUID"]);
MAIN.println("  nodeid:             "+PARAMS["NODEID"]);
MAIN.println("  ipaddress:          "+PARAMS["IPADDRESS"]);
MAIN.println("  esbuser:            "+PARAMS["ESBUSER"]);
MAIN.println("  version:            "+PARAMS["VERSION"]);
MAIN.println("  description:        "+PARAMS["DESCRIPTION"]);
