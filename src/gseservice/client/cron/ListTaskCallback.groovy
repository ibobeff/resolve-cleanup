MAIN.println();
MAIN.println("Cron Tasks:");
MAIN.println();
for (i in PARAMS)
{
    def taskname = i.key;
    def taskdef = i.value.split(",");
    def wiki = taskdef[0];
    def expression = taskdef[1];
    def startTime = taskdef[2];
    def endTime = taskdef[3];
    
	MAIN.println("task name:    "+taskname);
    MAIN.println("  runbook:    "+wiki);
    MAIN.println("  expression: "+expression);
    MAIN.println("  starttime:  "+startTime);
    MAIN.println("  endtime:    "+endTime);
	MAIN.println();
}
