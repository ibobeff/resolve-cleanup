MAIN.setMethod("MCron.listTask", "MService.cron.ListTaskCallback");

params = new Hashtable();

return params;


def help()
{
    println "Usage: listTask";
    println "";
    println "  Displays the current scheduled cron tasks";
    println "";
    return null;
} // help