if (args.size() == 1)
{
    return help();
}

MAIN.setMethod("MCron.removeTask", "MDefault.printMessage");
params = new Hashtable();
params["NAME"] = args[1];
return params;


def help()
{
    println "Usage: removetask <taskname>";
    println "";
    println "NOTE: Removes the scheduled task only from memory, not from the database"
    return null;
} // help