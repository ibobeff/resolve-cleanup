import com.resolve.util.StringUtils;

MAIN.println();
MAIN.println("Cron Jobs:");
MAIN.println();
for (i in PARAMS)
{
    def jobdef = i.key.split(",");
    def jobname = jobdef[0];
    def classname = jobdef[1];
    
	MAIN.println("job name:      "+jobname);
    MAIN.println("  class name:  "+classname);
    MAIN.println("  data:");
    if (PARAMS.size() == 1)
    {
        Map params = StringUtils.stringToMap(i.value);
        for (param in params)
        {
        	MAIN.println("    "+param.key+": "+param.value);
    	}
	}
	MAIN.println();
}
