if (args.size() == 1)
{
    help()
    return null;
}

MAIN.setMethod("MCron.loadTask", "MDefault.printMessage");
params = new Hashtable();
params["CRONID"] = args[1];
return params;

def help()
{
    println "Usage: loadtask ALL | <resolve_cron.sys_id>";
    println "";
    println "Schedule task definition from the database"
    println "";
    return null;
} // help


