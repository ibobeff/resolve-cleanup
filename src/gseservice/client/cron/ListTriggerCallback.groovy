MAIN.println();
MAIN.println("Cron Triggers:");
MAIN.println();
for (i in PARAMS)
{
    def trigname = i.key;
    def trigdef = i.value.split(",");
    def type = trigdef[0];
    def repeat = trigdef[1];
    def interval = trigdef[2];
    def expression = "";
    if (trigdef.size() > 3)
    {
	    expression = trigdef[3];
    }
    
	MAIN.println("trigger name:  "+trigname);
    MAIN.println("  type:        "+type);
    MAIN.println("  repeat:      "+repeat);
    MAIN.println("  interval:    "+interval);
    MAIN.println("  expression:  "+expression);
	MAIN.println();
}
