if (args.size() < 4)
{
    return help();
}

MAIN.setMethod("MCron.addTask", "MDefault.printMessage");

params = new Hashtable();
params["NAME"] = args[1]
params["WIKI"] = args[2];
params["EXPRESSION"] = args[3];
if (args.size() >= 5)
{
	params["STARTTIME"] = args[4];
}
if (args.size() >= 6)
{
	params["ENDTIME"] = args[5];
}
return params;


def help()
{
    println "Usage: addtask <name> <wiki> <expression> [<starttime> <endtime>]";
    println "";
    println "NOTE: Add the scheduled task only to memory, not to the database"
    println "";
    println "  <name>       - cronjob task name";
    println "  <wiki>       - wiki runbook full name (e.g. Test.test1)";
    println "  <expression> - Use quotes \"<sec> <min> <hr> <dayofmonth> <month> <dayofweek>\"";
    println "  <starttime>  - start system time in milliseconds";
    println "  <endtime>    - end system time in milliseconds";
    println "";
    println "";
    println "                 *     - wildcard";
    println "                 ?     - dont care";
    println "                 x/y   - x offset / y interval";
    println "";
    println "    Expression             Meaning";
    println "    ==========             =======";
    println "    0 0 12 * * ?           Fire at 12pm (noon) every day";
    println "    0 15 10 ? * *          Fire at 10:15am every day";
    println "    0 15 10 * * ?          Fire at 10:15am every day";
    println "    0 * 14 * * ?           Fire every minute starting at 2pm and ending at";
    println "                           2:59pm, every day";
    println "    0 0/5 14 * * ?         Fire every 5 minutes starting at 2pm and ending";
    println "                           at 2:55pm, every day";
    println "    0 0/5 14,18 * * ?      Fire every 5 minutes starting at 2pm and ending";
    println "                           at 2:55pm, AND fire every 5 minutes starting at";
    println "                           6pm and ending at 6:55pm, every day";
    println "    0 0-5 14 * * ?         Fire every minute starting at 2pm and ending at";
    println "                           2:05pm, every day";
    println "    0 10,44 14 ? 3 WED     Fire at 2:10pm and at 2:44pm every Wednesday in";
    println "                           the month of March";
    println "    0 15 10 ? * MON-FRI    Fire at 10:15am every Monday, Tuesday, Wednesday,";
    println "                           Thursday and Friday";
    println "    0 15 10 15 * ?         Fire at 10:15am on the 15th day of every month";
    println "    0 15 10 L * ?          Fire at 10:15am on the last day of every month";
    println "    0 15 10 ? * 6L         Fire at 10:15am on the last Friday of every month";
    println "    0 15 10 ? * 6#3        Fire at 10:15am on the third Friday of every month";
    println "";
    return null;
} // help
