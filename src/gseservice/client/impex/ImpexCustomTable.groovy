import com.resolve.util.ConversionUtils;

MAIN.setMethod("MImpex.impexCustomTable", "MDefault.print");

def help()
{
    println "Usage: ImpexCustomTable import|export table=<table name> file=<full path to CSV file> [datatype=csv] [delim=<custom delimeter>] [override=true|false]";
    println "";
    println " Performs custom table data import from a file or export to a file";
    println "";
    println " Parameters:"
    println "   import or export - indicates the operation you are performing.";
    println "   table=<table name> - the name of the database table.";
    println "   file=<full path to CSV file> - The absolute path to the CSV file to read data during \"import\", or write data during \"export\".";
    println "   datatype=csv|xls - optional parameter that indicates data type of the file, default is CSV.";
    println "   delim=<custom delimeter> - optional parameter that indicates the delimeter, if not supplied uses comma(,).";
    println "   override=true|false - optional parameter that indicates during import if you want to override the data if same sys_id is found, default is true";
    println "   stoponfailure=true|false - optional parameter that indicates during import if there is data error should it stop processing further? default is false";
    println "";
    println "  e.g. ImpexCustomTable import table=my_table file=c:/tmp/my_data_imp.csv override=false";
    println "  e.g. ImpexCustomTable export table=my_table file=c:/tmp/my_data_exp.csv";
    println "  e.g. ImpexCustomTable export table=my_table file=c:/tmp/my_data_exp.xls datatype=xls";
    
    return null;
}

def params = [:]
if (args.length >= 4)
{
    params["USERID"] = MAIN.getUsername();
    for(param in args)
    {
        if(param.contains("import"))
        {
            params["OP"] = "import";
        }
        if(param.contains("export"))
        {
            params["OP"] = "export";
        }
        if(param.contains("table="))
        {
            params["TABLE"] = param.split("=")[1];
        }
        if(param.contains("file="))
        {
            params["FILE"] = param.split("=")[1];;
        }
        if(param.contains("datatype="))
        {
            params["DATATYPE"] = param.split("=")[1];
        }
        if(param.contains("delim="))
        {
            params["DELIMETER"] = param.split("=")[1];
        }
        if(param.contains("override="))
        {
            params["OVERRIDE"] = param.split("=")[1];
        }
        if(param.contains("stoponfailure="))
        {
            params["STOP_ON_FAILURE"] = param.split("=")[1];
        }
    }
    
    if(params.containsKey("OP") && params.get("OP").equalsIgnoreCase("import"))
    {
        String fileContents = null;
        
        if(params.containsKey("DATATYPE") && params.get("DATATYPE").equalsIgnoreCase("xls"))
        {
           fileContents = ConversionUtils.convertExcelToCsv(params["FILE"]);
           //since we convert it making it csv again
           params["DATATYPE"] = "csv";
        }
        else
        {
            fileContents = new File(params["FILE"]).getText("UTF-8");
        }
        params["DATA"] = fileContents;
    }
}
else
{
    help();
}

return params;


