def help()
{
    println "\nUsage: ImportUpdates <module name>";
    println ""
    println "  This Script will import the update XMLs located at:";
    println "";
    println "    <INSTALLDIR>/tomcat/webapps/resolve/WEB-INF/updates";
    println "";
}

def params = [:]

MAIN.sendMessage("RSVIEW","MAction.importUpdates",params);
println("Importing update XMLs");
LOG.warn("Importing update XMLs");

return null;