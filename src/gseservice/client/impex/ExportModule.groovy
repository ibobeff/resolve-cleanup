def help()
{
    println "\nUsage: ExportModule <module name> ...";
    println ""
    println "This Script will import the module specified by <module name>";
    println "The exported module will be written to the rsexpert folder";
    println ""
}
if (args.length < 2)
{
    help();
}
else
{
    def modules = args[1];
    if (args.length > 2)
    {
	    for (i in 2..(args.length - 1))
	    {
            modules += ","+args[i]
	    }
    }

    def params = [(Constants.IMPEX_PARAM_MODULENAME):modules];
    MAIN.sendMessage("RSVIEW","MAction.exportModule",params);
    println "Exporting modules: " + modules
    LOG.warn("Exporting modules: " + modules)

    return null;
}
