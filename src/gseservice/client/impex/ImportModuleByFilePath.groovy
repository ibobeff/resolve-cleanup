import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.lang.StringBuffer;
import com.resolve.services.util.JsonUtils;
import 



if (args.length < 2) {
    help();
}
else {
    // ImportModuleByFilePath -user=admin -password=resolve -host=localhost -port=8080 -ssh=false -filepath=C:/Users/James.Dalby.RESOLVESYS/Desktop/QuirksModeTest.zip
    // ImportModuleByFilePath -user=admin -password=resolve -host=localhost -port=8443 -ssh=true -filepath=C:/Users/James.Dalby.RESOLVESYS/Desktop/QuirksModeTest.zip
    // http://localhost:8080/resolve/service/impex/uploadAndInstallModule?path=C:/Users/James.Dalby.RESOLVESYS/Desktop/QuirksModeTest.zip&_username_=admin&_password_=resolve
    String username = "";
    String password = "";
    String httpHost = "";
    String httpPort = "";
    String ssh = "";
    String filePath = "";

    // get all arguments
    String arg = "";
    String value = "";

    for(int i = 1; i<args.length; i++) {
        String[] a = args[i].split("=");
        if(a[0].equalsIgnoreCase("-user")) {
            username    =   a[1];
        } else if(a[0].equalsIgnoreCase("-password")) {
            password    =   a[1];
        } else if(a[0].equalsIgnoreCase("-host")) {
            httpHost    =   a[1];
        } else if(a[0].equalsIgnoreCase("-port")) {
            httpPort    =   a[1];
        } else if(a[0].equalsIgnoreCase("-ssh")) {
            ssh         =   a[1];
        } else if(a[0].equalsIgnoreCase("-filepath")) {
            filePath    =   a[1];
        }
    }


    // Check all required arguments
    if(username.isEmpty()) {
        println "A username is required.";
        help();
        return "";
    } else if(password.isEmpty()) {
        println "A password is required.";
        help();
        return "";
    } else if(httpHost.isEmpty()) {
        println "A httphost is required.";
        help();
        return "";
    } else if(httpPort.isEmpty()) {
        println "A httpport is required.";
        help();
        return "";
    } else if(filePath.isEmpty()) {
        println "A filePath is required.";
        help();
        return "";
    }

    if(!ssh.isEmpty() && !(ssh.equalsIgnoreCase("true") || ssh.equalsIgnoreCase("false"))){
        println "ssh can only accept the following values: { true, false }.";
        help();
        return "";
    }

    println "\n***** IMPORT MODULE BY FILE PATH *****";
    println "\nImporting module with the following arguments: \n";
    println "Username: " + username;
    println "Password: " + password;
    println "Http host: " + httpHost;
    println "Http port: " + httpPort;
    println "File path: " + filePath;
    println "SSH: " + ssh;

    String protocol = "http://";

    if(ssh.equalsIgnoreCase("true")) {
        protocol = "https://";
    }

    // Assemble URL

    String url = protocol + httpHost + ":" + httpPort + "/" + "resolve/service/impex/uploadAndInstallModule?path=" + filePath + "&_username_=" + username + "&_password_=" + password;
    //println "\nUrl: " + url;

    System.setProperty("javax.net.ssl.trustStoreType","JCEKS");
    
    try {

        URL targetUrl = new URL(url);

        HttpURLConnection httpConnection = (HttpURLConnection) targetUrl.openConnection();
        httpConnection.setDoOutput(true);
        httpConnection.setRequestMethod("POST");
        httpConnection.setRequestProperty("Content-Type", "application/json");

        String input = "{}";

        OutputStream outputStream = httpConnection.getOutputStream();
        outputStream.write(input.getBytes());
        outputStream.flush();

        if (httpConnection.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
            + httpConnection.getResponseCode());
        }

        BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(
                (httpConnection.getInputStream())));

        StringBuffer jsonOutput = new StringBuffer();
        String output;
        println("\nSERVER OUTPUT:\n");
        while ((output = responseBuffer.readLine()) != null) {
            jsonOutput.append(output);
        }

        Map json = JsonUtils.convertJsonStringToMap(jsonOutput.toString());
        println "Import success: " + (String)(json.get("success"));
        println "Message: " + (String)(json.get("message"));
        println "";

        httpConnection.disconnect();

    } catch (MalformedURLException e) {

        e.printStackTrace();

    } catch (IOException e) {

        e.printStackTrace();

    }



    return null;
}

def help()
{
    println "\nUsage: ImportModuleByFilePath <-user> <-password> <-host> <-port> <-ssh> <-filepath>";
    println "";
    println "This script will load and install a module from a file path on a remote server.";
    println "\nArguments:";
    println "<-user>: Resolve username, REQUIRED";
    println "<-password>: Resolve password, REQUIRED";
    println "<-host>: server host, REQUIRED";
    println "<-port>: server port. default: 8080";
    println "<-ssh>: using ssh? true/false, default: false";
    println "<-filepath>: file path of the IMPEX module, REQUIRED";
    println "";
    println "Example: ";
    println "";
    println "ImportModuleByFilePath -user=myUser -password=myPassword -host=localhost -port=8080 -ssh=false -filepath=C:/MyIMPEXModule.zip";
    return null;
}