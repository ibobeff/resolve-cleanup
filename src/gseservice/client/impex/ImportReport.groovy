def help()
{
    println "\nUsage: ImportReport <report name>|<module name> ...";
    println ""
    println "This Script will import the module specified by <report name>";
    println "The imported report will be read from the rsexpert folder";
    println "The arguments can either be .jar names or module names"
    println ""
    println "Example Import .jar Reports:"
    println "Example: ImportReport resolve.system.jar resolve.business.jar"
    println ""
    println "Example Import module Reports:"
    println "Example: ImportReport dashboard"
    println ""
}
if (args.length < 2)
{
    help();
}
else
{
    def modules = "";
    def username;
    def guid = "RSVIEW";
    def isModule = true;
    def method = "MAction.importReports";
    if (args.length > 1)
    {
        for (def i=1; i<args.length; i++)
        {
            if (args[i] == "-u")
            {
                if (i < (args.length-1))
                {
                    i++;
                    username = args[i];
                }
            }
            else if (args[i] == "-guid")
            {
                if (i < (args.length-1))
                {
                    i++;
                    guid = args[i];
                }
            }
            else
            {
                if (args[i].toLowerCase().endsWith(".jar"))
                {
                    isModule = false;
                }
                if (modules)
                {
                    modules += ","+args[i]
                }
                else
                {
                    modules = args[i];
                }
            }
        }
    }

    def params = [:];
    if (isModule)
    {
        params[Constants.IMPEX_PARAM_MODULENAMES] = modules;
        println "Importing module reports: " + modules
        LOG.warn("Importing module reports: " + modules)
    }
    else
    {
        params[Constants.ESB_PARAM_REPORTNAME] = modules;
        method = "MAction.importReport";
        println "Importing reports: " + modules;
        LOG.warn("Importing reports: " + modules)
    }
    if (username)
    {
        params[Constants.HTTP_REQUEST_USERNAME] = username;
    }
    MAIN.sendMessage(guid,method,params);

    return null;
}
