import groovy.sql.Sql;

def help()
{
    println "\nUsage: ActionTaskValidation";
    println ""
    println "This Script will validate that there are no Action Tasks with";
    println "missing Invocations, or Invocations that are missing Action Task";
    println "entries in the database.  If the RSConsole is not connected to a";
    println "database this script will connect to the default database defined";
    println "in connections.cfg. To manually connect to a database use the";
    println "\"DBConnect\" command.";
    println ""
}

if (!MAIN.isDBConnected())
{
    println "\nConnecting to connection1";
    MAIN.executeCommand("DBConnect connection1");
}

def conn = new Sql(DB);

def sql = """\
select
  sys_id,
  u_description
from
  resolve_action_invoc
where
  sys_id NOT IN
  (select
    u_invocation
  from
    resolve_action_task
  where
    u_invocation IS NOT NULL)
""";
LOG.debug("Get Dangling Invocations SQL: " + sql);

def sys_id = [];
def description = [];
conn.eachRow(sql) { row ->
    sys_id.add(row["SYS_ID"]);
    description.add(row["U_DESCRIPTION"]);
}

if (sys_id.size() > 0)
{
    def descriptions = "The Following Action Task Invocations are missing their base";
    descriptions += " definition and need to be removed: ";
    for (descriptionStr in description)
    {
        descriptions += "'" + descriptionStr + "' ";
    }
    descriptions += "\n\nYou can run the following sql to remove these invocations:\n";
    descriptions += "DELETE FROM resolve_action_invoc WHERE sys_id in ("
    def comma = false;
    for (sysId in sys_id)
    {
        if (comma)
        {
            descriptions += ", ";
        }
        else
        {
            comma = true;
        }
        descriptions += "'" + sysId + "'";
    }
    descriptions += ");";
    println descriptions;
    LOG.info(descriptions);
}

sql = """\
select
  sys_id,
  u_fullname
from
  resolve_action_task
where
  u_invocation IS NULL or
  u_invocation NOT IN
  (select
    sys_id
  from
    resolve_action_invoc)
""";
LOG.debug("Get Dangling Tasks SQL: " + sql);

sys_id = [];
def fullname = [];
conn.eachRow(sql) { row ->
    sys_id.add(row["SYS_ID"]);
    fullname.add(row["U_fullname"]);
}

if (sys_id.size() > 0)
{
    def fullnames = "The Following Action Tasks are missing their invocation";
    fullnames += " definition and need to be removed: ";
    for (fullnameStr in fullname)
    {
        fullnames += "'" + fullnameStr + "' ";
    }
    fullnames += "\n\nYou can run the following sql to remove these invocations:\n";
    fullnames += "DELETE FROM resolve_action_task WHERE sys_id in ("
    def comma = false;
    for (sysId in sys_id)
    {
        if (comma)
        {
            fullnames += ", ";
        }
        else
        {
            comma = true;
        }
        fullnames += "'" + sysId + "'";
    }
    fullnames += ");";
    println fullnames;
    LOG.info(fullnames);
}

if (description.size() == 0 && fullname.size() == 0)
{
    println "Action Task Integrity Test Passed";
}

return null;
