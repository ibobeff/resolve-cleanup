def help()
{
    println "\nUsage: ImportModule <module name> ...";
    println ""
    println "This Script will import the module specified by <module name>";
    println "The imported module will be read from the rsexpert folder";
    println ""
}
if (args.length < 2)
{
    help();
}
else
{
    def modules = "";
    def username;
    def blockIndex = false;
    def guid = "RSVIEW";
    if (args.length > 1)
    {
        for (def i=1; i<args.length; i++)
        {
            if (args[i] == "-u")
            {
                if (i < (args.length-1))
                {
                    i++;
                    username = args[i];
                }
            }
            else if (args[i] == "--block-index")
            {
                blockIndex = true;
            }
            else if (args[i] == "-guid")
            {
                if (i < (args.length-1))
                {
                    i++;
                    guid = args[i];
                }
            }
            else
            {
                if (modules)
                {
                    modules += ","+args[i]
                }
                else
                {
                    modules = args[i];
                }
            }
        }
    }

    def params = [(Constants.IMPEX_PARAM_MODULENAME):modules];
    if (username)
    {
        params[Constants.HTTP_REQUEST_USERNAME] = username;
    }
    if (blockIndex)
    {
        params[Constants.IMPEX_PARAM_BLOCK_INDEX] = "true";
    }
    MAIN.sendMessage(guid,"MAction.importModule",params);
    println "Importing modules: " + modules
    LOG.warn("Importing modules: " + modules)

    return null;
}
