MAIN.setMethod("MIndex.removeAllWorkSheetsRelatedIndex", "MDefault.print");

def help()
{
    println "Usage: RemoveWorksheetIndex";
    println "";
    println "  Connect to RSVIEW or RSCONTROL before running this command.";
    println "  Remove all data of indexed worksheets, process requests, task results.";
    println "  It is recommended to run ExecuteCLI drop or truncate first.";
    println "";
    return null;
}

def params = null;

println "This will remove all data of indexed worksheets, process requests, task results.";
println "Do you wish to continue? (Y/N)";
br = new BufferedReader(new InputStreamReader(System.in));
def answer = br.readLine();
LOG.info("Answer: " + answer);
if (answer.equalsIgnoreCase("Y"))
{
    params = [:]
    println "Sending Clear Index Message";
}
else
{
    println "No Clear Index Message Sent";
}

return params;
