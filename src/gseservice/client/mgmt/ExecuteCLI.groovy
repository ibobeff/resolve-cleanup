MAIN.setMethod("MService.mgmt.ExecuteCLI", "MDefault.print");

def params = null;
def help()
{
    println "Usage: ExecuteCLI <CLI file> [Cassandra Host] [Cassandra Port] [Cassandra Password]"
    println ""
    println "This script will execute the cli contained in the <CLI file> ";
    println "located on the connected RSMgmt server";
    println ""
}

if (binding.variables.containsKey("args") && args.length < 2)
{
    help();
}
else
{
    def filename = null;
    def host = null;
    def port = null;
    def passwd = null;

    filename = args[1];
    if (args.length > 4)
    {
        passwd = args[4];
    }
    if (args.length > 3)
    {
        port = args[3];
        host = args[2];
    }
    else if (args.length > 2)
    {
        if (args[2].matches("\\d+"))
        {
            port = args[2];
        }
        else
        {
            host = args[2];
        }
    }
    params = [:];
    params["FILENAME"] = filename;
    if (host)
    {
        params["HOST"] = host;
    }
    if (port)
    {
        params["PORT"] = port;
    }
    if (passwd)
    {
        params["PASSWORD"] = passwd;
    }
    println "Sending CLI Execute Request";
    LOG.debug("Sending Execute CLI to Remote machine: " + params);
}

return params;
