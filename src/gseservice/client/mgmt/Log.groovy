MAIN.setMethod("MService.mgmt.Log");

def levels = ["TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL"];
def params = null;
def help()
{
    println "Usage: Log [LEVEL] <message>\n";
    println "";
    println "This script will log out the supplied message to the connected"
    println "component's log, the default log level is INFO";
    println ""
}
if (args.length < 2)
{
    help();
}
else
{
    params = [:];
    def level = "INFO";
    def i=1;
    if (levels.contains(args[i]))
    {
        level = args[i];
        i++;
    }
    params["LEVEL"] = level;
    message = "";
    for (; i<args.length; i++)
    {
        message += " " + args[i];
    }
    message = message.trim();
    if (message)
    {
        params["MESSAGE"] = message;
        println "Log Message Sent";
        LOG.debug("Sending message " + message + " at log level " + level);
    }
    else
    {
        help();
        params = null;
    }
}

return params;
