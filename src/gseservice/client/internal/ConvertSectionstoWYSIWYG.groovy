import groovy.sql.Sql;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final String fromType = "SOURCE";
final String toType = "WYSIWYG";

def help()
{
    println "Usage: ConvertSectionsToWYSIWYG <Namespace> [<Docname>]";
    println "";
    println "This script will Edit the Sections of all documents in the";
    println "specified namespace, and change the first " + fromType + " type to " + toType + ".";
    println "Optionally a specific Docname can be provided to change a";
    println "single document instead of a full namespace.";
    println "";
    println "By defualt, this script will connect to the configured Resolve";
    println "Database. If you want to connect to a different Resolve database";
    println "use the \"DBConnect\" command to connect to a specific database.";
}

if (args.length < 2)
{
    help();
}
else
{
    def namespace = args[1];
    def docname;

    if (args.length >2)
    {
        docname = args[2];
    }

    if (!MAIN.isDBConnected())
    {
        println "\nConnecting to connection1";
        MAIN.executeCommand("DBConnect connection1");
    }

    def editedDocs = [];
    def noneditedDocs = [];

    try
    {
        def conn = new Sql(DB);

        def dbType = DB.getMetaData().getDatabaseProductName();

        def sql = "select sys_id, u_fullname, u_content from wikidoc where u_namespace='" + namespace + "'";
        if (docname)
        {
            sql += " and u_name='" + docname + "'";
        }
        LOG.trace("Search for Documents to Convert: " + sql);
        conn.eachRow(sql) { row ->
            def id = row.sys_id;
            def docName = row.u_fullname;
            def content;

            if (dbType.equalsIgnoreCase("Oracle"))
            {
                def buffer = new byte[10240];
                def num = 0;
                def inStream = row.u_content.getAsciiStream();
                out = new ByteArrayOutputStream();
                while ((num = inStream.read(buffer)) > 0) {
                    out.write(buffer,0,num);
                }
                content = new String(out.toByteArray());
            }
            else
            {
                content = row.u_content;
            }

            def edited = false;
            if (content.matches("(?ms).*\\{section:type=" + fromType + ".*"))
            {
                LOG.debug("Converting " + docName + " section from " + fromType + " to " + toType);
                Pattern pattern = Pattern.compile("(?ms)(.*?)\\{section:type=" + fromType + "(.*)"); 
                Matcher matcher = pattern.matcher(content);
                if(matcher.matches())
                {
                    content = matcher.group(1) + "{section:type=" + toType + matcher.group(2);
                    edited = true;
                }
            }
            else if (!content.matches("(?ms).*\\{section:type=.*"))
            {
                LOG.debug(docName + " missing any section text, adding " + toType + " section");
                content = "{section:type=" + toType + "|title=main|id=rssection_main|height=300}\n" + content + "\n{section}";
                edited = true;
            }
            def updateSql = "update wikidoc set u_content=? where sys_id=?";
            conn.execute(updateSql, content, id);
            if (edited)
            {
                editedDocs.add(docName);
            }
            else
            {
                noneditedDocs.add(docName);
            }
        }
    }
    catch (Exception e)
    {
        println "WARNING!!! UNEXPECTED EXCEPTION: " + e.getMessage() + "\n\n";
        LOG.error("Failure while changing " + fromType + " to " + toType, e);
    }

    def edited = editedDocs.size();
    def nonedited = noneditedDocs.size();

    println "Found: " + (edited + nonedited) + " Edited: " + edited;
    LOG.info("Found: " + (edited + nonedited) + " Edited: " + edited);

    def detail = "";

    detail += "\nDocuments Changed to " + toType + ":\n";
    for (docName in editedDocs)
    {
        detail += docName + "\n";
    }

    detail += "\nDocuments Found but with no " + fromType + " to change:\n";
    for (docName in noneditedDocs)
    {
        detail += docName + "\n";
    }

    println detail;
    LOG.debug("Detailed Results:\n" + detail);
}

return null;
