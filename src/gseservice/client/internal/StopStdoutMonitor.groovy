MAIN.setMethod("MStatus.stopStdoutMonitor", "MDefault.print");

def help()
{
    println "\nUsage: StopStdoutMonitor <Resolve Component...>";
    println "Valid Components: ALL, RSCONTROL, RSREMOTE, RSVIEW, RSMGMT\n";
    println "This script will stop the stdout monitoring of the specified resolve component(s)";
    println "in the local system of the connected RSMgmt";
    println "ALL can be used to stop the stdout monitoring all the configured resolve component(s)";
    println "in the local system of the connected RSMgmt";
}

def params = null;

if (args.length < 2)
{
    help();
}
else
{
    def components = "";
    for (i=1; i<args.length; i++)
    {
        components += "" + args[i];
    }
    LOG.warn("Stopping Monitoring of " + components);
    params = ["COMPONENTS":components];
}

return params;
