import java.util.concurrent.TimeUnit

def help()
{
    println "\nUsage: SyncWorksheetIndex <N> [GUID]";
    println ""
    println "This Script will execute the worksheet index re-sync starting";
    println "from <N> days ago. This will target the current connected";
    println "queue/topic unless a [GUID] value is specified";
    println ""
}
if (args.length < 2)
{
    help();
}
else
{
    def params = [:]
    def daysAgo = args[1];
    if (daysAgo.toInteger() == 0)
    {
        params["STARTTIME"] = 0L;
    }
    else
    {
        long daysAgoMillis = TimeUnit.MILLISECONDS.convert(daysAgo.toInteger(), TimeUnit.DAYS)
        long startTime = System.currentTimeMillis() - daysAgoMillis

        params["STARTTIME"] = startTime;
    }

    if (args.length >= 3)
    {
        def guid = args[2];
        MAIN.sendMessage(guid,"MIndex.resyncWorkSheetIndex",params);
        params = null;
    }
    else
    {
        MAIN.setMethod("MIndex.resyncWorkSheetIndex")
    }
    
    println "Running Worksheet Index Resync";
    return params;
}
