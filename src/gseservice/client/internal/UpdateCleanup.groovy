def help()
{
    println "\nUsage: UpdateCleanup [GUID]"
    println ""
    println "This Script will run some internal cleanup";
    println ""
}
def guid = "RSVIEW";

if (args.length >= 2)
{
    guid = args[1];
}

if (!MAIN.connected)
{
    MAIN.initESB();
}

def params = [:];

println "Triggering Update Cleanup";
LOG.warn("Triggering Update Cleanup: " + guid);
MAIN.esb.call(guid,"MAction.updateCleanup",params, 600000);


return null;
