import com.resolve.esb.MMsgHeader
import com.resolve.rsbase.MainBase

def help()
{
    println "Usage: Event <name=value> ...";
    println "";
    println "  Sends test event";
    println "";
    println "  e.g. Event \"WIKI=Condition.High CPU Load\"";
    println "       Event EVENTID=id PROCESSID=id ...";
    println "       Event EVENTID=id EVENT_REFERENCE=id ...";
    println "";
    return null;
}

if (args.size() < 2)
{
    return help();
}

def params = [:]
for (i in 1..args.size()-1)
{
    String[] nameval = args[i].split("=")
    params[nameval[0]] = nameval[1]
}
MAIN.sendMessage("RSSERVER","MEvent.eventResult",params);

return null
