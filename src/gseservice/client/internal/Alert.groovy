if (args.size() < 4)
{
    return help();
}

MAIN.setMethod("MAlert.alert", "MDefault.print");

params = new Hashtable();
params["SEVERITY"] = args[1].toUpperCase();
params["COMPONENT"] = args[2];
params["TYPE"] = args[3];
params["MESSAGE"] = args[4];

return params;

def help()
{
    println "Usage: alert <severity> <component> <type> <message>";
    println "";
    println "  Send alert message (SNMP trap) to management console";
    println "  e.g. alert CRITICAL LOG \"RSCONTROL START\" \"RSControl started\"";
    println "";
    return null;
}

