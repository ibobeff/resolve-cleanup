def help()
{
    println "Usage: ExecuteSQL <SQL file>\n"
    println ""
    println "This script will execute the sql contained";
    println "in the <SQL file> against the connected DB";
    println ""
}

def delimiter = ";";

if (args.length < 2)
{
    help();
}
else
{
    def statement;
    def filename = args[1];
    if (!filename.matches("^(?:[\\\\/]).*|(?:[A-Za-z](?=:\\\\)).*"))
    {
        filename = "rsconsole/file/" + filename;
    }
    def file = new File(filename);
    if (!file.isFile())
    {
        file = new File(filename + ".sql");
    }
    if (file.isFile())
    {
        try
        {
            if (MAIN.isDBConnected())
            {
                statement = DB.createStatement();

                byte[] sqlBytes = new byte[file.length()];
                fis = new FileInputStream(file);
                fis.read(sqlBytes);
                fis.close();
                def statements = new String(sqlBytes);
                statements = statements.replaceAll("/\\*(?!!).*\\*/", "");
                statements = statements.replaceAll("#.*\n", "");
                statements = statements.replaceAll("--.*\n", "");

                def currIdx = 0;

                while (currIdx != statements.length())
                {
                    if (currIdx != 0)
                    {
                        currIdx += delimiter.length();
                    }
                    def nextIdx = statements.indexOf(delimiter, currIdx) != -1 ? statements.indexOf(delimiter, currIdx) : statements.length();
                    def sql = statements.substring(currIdx, nextIdx);

                    sql = sql.trim();
                    if (sql && sql.toLowerCase().startsWith("delimiter"))
                    {
                        def delimMatcher = sql =~ /(?mi)^delimiter (.+)$/;
                        if (delimMatcher.find())
                        {
                            delimiter = delimMatcher.group(1);
                            currIdx = statements.indexOf(delimiter, currIdx) + delimiter.length();
                            nextIdx = statements.indexOf(delimiter, currIdx) != -1 ? statements.indexOf(delimiter, currIdx) : statements.length();
                            sql = statements.substring(currIdx, nextIdx);
                            sql = sql - "delimiter $delimiter";
                    
                            sql = sql.trim();
                        }
                    }

                    if (sql && !sql.toLowerCase().startsWith("connect"))
                    {
                        if (sql.toLowerCase().contains("begin") && !sql.endsWith(";"))
                        {
                            sql = sql + ";";
                        }
                        LOG.warn("Execute SQL: " + sql);
                        try
                        {
                            if (statement.execute(sql))
                            {
                                def rs = statement.getResultSet();
                                def metaData = rs.getMetaData();
                                def numColumns = metaData.getColumnCount();
                                def columnNames = new String[numColumns];
                                for (i in 1..numColumns)
                                {
                                    columnNames[i-1] = metaData.getColumnName(i);
                                }
                                def resultCount = "1";
                                while (rs.next())
                                {
                                    def result = resultCount++ + ": ";
                                    for (i in 1..numColumns)
                                    {
                                        if (i > 1)
                                        {
                                            print ", ";
                                        }
                                        result += columnNames[i-1] + "=";
                                        result += rs.getString(i);
                                    }
                                    println result;
                                    LOG.info(result);
                                }
                            }
                        }
                        catch (SQLException sqle)
                        {
                            println "WARNING!!! Failed to Execute SQL: " + sqle.getMessage();
                            LOG.error("Failed to Execute SQL", sqle);
                        }
                    }
                    currIdx = nextIdx;
                }
                println "SQL File " + file.getAbsolutePath() + " executed";
                LOG.warn("SQL File " + file.getAbsolutePath() + " executed");
            }
            else
            {
                println "No Database Connection Detected";
                LOG.warn("No Database Connection Detected");
            }
            
        }
        catch (Exception e)
        {
            println "Unexpected Exception: " + e.getMessage();
            LOG.warn("Unexpected Exception: " + e.getMessage(), e);
        }
        finally
        {
            statement?.close();
        }
    }
    else
    {
        println "Cannot Find SQL File: " + filename;
        LOG.warn("Cannot Find SQL File: " + file.getAbsolutePath());
    }
}

return null;
