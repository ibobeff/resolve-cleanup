import java.util.concurrent.TimeUnit

def help()
{
    println "\nUsage: SyncWorksheetIndex [GUID]";
    println ""
    println "This Script will execute the index configure script";
    println "This will target the RSVIEW queue unless a [GUID]";
    println "value is specified";
    println ""
}

def params = [:]

if (!MAIN.connected)
{
    MAIN.initESB();
}

if (args.length >= 2)
{
    def guid = args[1];

    println "Running Worksheet Index Configure: " + guid;
    result = MAIN.esb.call(guid,"MIndex.configureIndex",params, 300000);
}
else
{
    println "Running Worksheet Index Configure: RSVIEW";
    result = MAIN.esb.call("RSVIEW","MIndex.configureIndex",params, 300000);
}

return null;
