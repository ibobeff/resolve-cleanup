println "Starting updating index mapping and reindexing for";
LOG.warn("Starting updating index mapping and reindexing guid: RSVIEW");

	if (!MAIN.connected) {
        MAIN.initESB();
    }
    def params = [:];
    params.put("ACTIONTASK", true);
    params.put("PROPERTY", true);
    params.put("CUSTOMFORM", true);
    params.put("DOCUMENT", true);
    params.put("AUTOMATION", true);
    params.put("PLAYBOOK", true);
    params.put("DECISIONTREE", true);
    
    MAIN.esb.sendMessage("RSVIEW","MIndex.updateIndexMappingAndReindex",params);
    return true;
return true;