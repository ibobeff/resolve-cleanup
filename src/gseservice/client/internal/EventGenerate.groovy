import com.resolve.esb.MMsgHeader
import com.resolve.rsbase.MainBase

if (args.size() < 3)
{
    return help();
}

for (i in 1..args[2].toInteger())
{
	def params = new Hashtable()
	params["EVENT_TYPE"] = "NETCOOL"
	params["WIKI"] = args[1] 
	params["Identifier"] = "EventGenerate: "+args[1]+" idx: "+i
	params["Summary"] = "EventGenerate: "+args[1]
	params["PROCESS_TIMEOUT"] = "10"
	
	// manually send messages
	MMsgHeader header = new MMsgHeader();
	header.setRouteDest("RSSERVER", "MEvent.eventResult")
	MainBase.main.mServer.sendMessage(header, params, null)
}

return null

def help()
{
    println "Usage: EventGenerate <runbook> <number of events>";
    println "";
    println "  Sends test events.";
    println "";
    println "  e.g. EventGenerate \"Condition.High CPU Load\" 10";
    println "";
    return null;
}

