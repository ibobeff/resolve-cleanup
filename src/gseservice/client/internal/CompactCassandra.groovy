MAIN.setMethod("MAction.compactCassandra", "MDefault.print");

params = new Hashtable();
return params;

def help()
{
    println "Usage: CompactCassandra";
    println "";
    println "  Perform compaction of Cassandra data";
    println "";
    return null;
} // help