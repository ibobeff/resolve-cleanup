def help()
{
    println "\nUsage: UpgradeCleanup [GUID]"
    println ""
    println "This Script will run some internal cleanup";
    println ""
}
def guid = "RSVIEW";

if (args.length >= 2)
{
    guid = args[1];
}

if (!MAIN.connected)
{
    MAIN.initESB();
}

def params = [:];

println "Triggering Upgrade Cleanup";
LOG.warn("Triggering Upgrade Cleanup: " + guid);
MAIN.esb.call(guid,"MAction.upgradeCleanup",params, 1800000);


return null;
