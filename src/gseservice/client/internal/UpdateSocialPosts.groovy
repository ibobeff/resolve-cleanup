def help()
{
    println "\nUsage: UpdateSocialPosts <GUID>"
    println ""
    println "This Script will Update the Social Posts for the Resolve 3.5 Upgrade";
    println ""
}
if (args.length < 2)
{
    help();
}
else
{
    def guid = args[1];

    if (!MAIN.connected)
    {
        MAIN.initESB();
    }

    def params = [:];
//    println "Migrating Graph";
//    LOG.warn("Migrate Graph guid: " + guid);
//    def result = MAIN.esb.call(guid,"MIndex.migrateGraph",params, 300000);
//    if (result)
//    {
//        println result["RESULT"];
//        LOG.warn("Migrate Graph result: " + result["RESULT"]);
//    }

    println "Migrating Posts";
    LOG.warn("Migrate Posts guid: " + guid);
    result = MAIN.esb.call(guid,"MIndex.migratePosts",params, 300000);
    if (result)
    {
        println result["RESULT"];
        LOG.warn("Migrate Posts result: " + result["RESULT"]);
    }

    return null;
}
