import java.io.File;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;

import com.resolve.util.DateUtils;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

if (args.size() != 3)
{
    return help();
}

def help()
{
    println "This Script will create relationship XML based on user.xml and tole.xml and import it:";
    println "Usage : UserRoleRelation <user.xml> <role.xml>";
}

LOG.warn("arg[1] -->" + args[1]);
LOG.warn("arg[2] -->" + args[2]);

def params = new HashMap();
params["FILE"] = "user_role_rel.xml";
params["CONTENT"] = mergeAndCreateUserRoleRelations(args[1], args[2]);

MAIN.sendMessage("RSVIEW","MAction.importUpdates",params);
println("Importing update XMLs");
LOG.warn("Importing update XMLs");

return null;


/**
 * Prereqsite - 2 XML files - role.xml - user.xml
 * 
 * Goal - to create user_role_rel.xml
 */
private String mergeAndCreateUserRoleRelations(String USER_XML_FILE, String ROLE_XML_FILE) throws Exception
{
	String roleXML = FileUtils.readFileToString(new File(ROLE_XML_FILE));
	String userXML = FileUtils.readFileToString(new File(USER_XML_FILE));

	LOG.warn("roleXML -->" + roleXML);
	LOG.warn("userXML -->" + userXML);
	
	XDoc xdocRole = new XDoc(roleXML);
	XDoc xdocUser = new XDoc(userXML);
	
	//key-value ==> role-sys_id
	Map<String, String> roleMap = prepareRoleMap(xdocRole);
	
	//create the xml
	return createRelationshipXML(xdocUser, roleMap);

}//mergeAndCreateUserRoleRelations

private String createRelationshipXML(XDoc xdocUser, Map<String, String> roleMap) throws Exception
{
	//create a template for the final output
	XDoc docUserRoleRel = new XDoc();
	Element docUserRoleRelRoot = docUserRoleRel.addRoot("unload");
	docUserRoleRel.removeResolveNamespaces();
	docUserRoleRel.setStringValue(docUserRoleRelRoot, "./@unload_date", DateUtils.getGMTDateIn_YYYYMMDDmmssFormat());
	
	//read the user doc
	Document dom = xdocUser.getDocument();
	Element root = dom.getRootElement();
	
	// iterate through child elements of root
	for (Iterator<Element> i = root.elementIterator("sys_user"); i.hasNext();)
	{
		Element sys_user = (Element) i.next();
		
		if(sys_user != null)
		{
			String userSysId = getElement(sys_user, "sys_id");
			String rolesCSV = getElement(sys_user, "roles"); 
			String userName = getElement(sys_user, "user_name");
			
			String[] roles = rolesCSV.split(",");
			for(String role : roles)
			{
				role = role.trim();
				String roleSysId = roleMap.get(role);
				
				if(!StringUtils.isEmpty(roleSysId))
				{
					try
					{
						createUserRoleRelXML(docUserRoleRelRoot, userSysId, roleSysId);
					}
					catch(Exception e)
					{
						Log.log.error("Error creating file for user --> " + userName + " -- role -->" + role, e);
					}
				}
			}//end of for loop
		}//end of if
	}//end of for loop
	
	//output the file
	//docUserRoleRel.toFile(new File(OUTPUT_XML_FILE));
	return docUserRoleRel.toString();
	
}//createRelationshipXML

private void createUserRoleRelXML(Element docUserRoleRelRoot, String userSysId, String roleSysId) throws Exception 
{
	XDoc doc = new XDoc();
	Element root = doc.addRoot("user_role_rel");
	doc.removeResolveNamespaces();
	doc.setStringValue(root, "./@action", "INSERT_OR_UPDATE");

	//add columns to the XML
	doc.setStringValue(root, "./sys_id", "");
	doc.setStringValue(root, "./u_user_sys_id", userSysId);
	doc.setStringValue(root, "./u_role_sys_id", roleSysId);
	doc.setStringValue(root, "./sys_created_by", "resolve.maint");
	
	//add that to the main doc root
	docUserRoleRelRoot.add(doc.getRoot());
	
}//createUserRoleRelXML


/**
 * prepare the map of role-sys_id 
 * 
 * @param xdocRole
 * @return
 */
private Map<String, String> prepareRoleMap(XDoc xdocRole)
{
	Map<String, String> hm = new HashMap<String, String>();

	Document dom = xdocRole.getDocument();
	Element root = dom.getRootElement();
	
	// iterate through child elements of root
	for (Iterator<Element> i = root.elementIterator("sys_user_role"); i.hasNext();)
	{
		Element sys_user_role = (Element) i.next();
		
		if(sys_user_role != null)
		{
			String sys_id =  getElement(sys_user_role, "sys_id");
			String roleName = getElement(sys_user_role, "name");
			
			hm.put(roleName, sys_id);
		}
	}//end of for loop
	return hm;
}//prepareRoleMap


private String getElement(Element docel, String name)
{
	Element el = docel.element(name);
	if (el == null)
		return "";
	else
		return el.getText().trim();
}//getElement








