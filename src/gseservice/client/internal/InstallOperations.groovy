def help()
{
    println "\nUsage: InstallOperations [GUID]"
    println ""
    println "This Script will run some installation setup on the Primary RSView";
    println ""
}
def guid = "RSVIEW";

if (args.length >= 2)
{
    guid = args[1];
}

if (!MAIN.connected)
{
    MAIN.initESB();
}

def params = [:];

println "Triggering Install Operations";
LOG.warn("Triggering Install Operations: " + guid);
MAIN.esb.call(guid,"MAction.postInstalledOperations",params, 600000);


return null;
