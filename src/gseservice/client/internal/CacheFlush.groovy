if (args.size() < 2)
{
    return help();
}

MAIN.setMethod("MAction.cacheFlush", "MDefault.print");

params = new HashMap();
params["WIKI"] = args[1]
return params;


def help()
{
    println "Usage: CacheFlush <wiki>";
    println "";
    println "  Flush the Resolve execution cache for the specified runbook.";
    println "";
    println "  e.g. cacheflush \"Condition.High CPU Load\"";
    println "";
    return null;
}

