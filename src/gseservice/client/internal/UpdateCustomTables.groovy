import groovy.sql.Sql;
import java.sql.Clob;

def help()
{
    println "\nUsage: UpdateCustomTables <table name> <column name> <varchar size>";
    println ""
    println "This Script is used to update the size of one of the custom table varchar";
    println "columns in the custom_table xml.  It will automatically connect to the";
    println "default Resolve connection stored in connections.cfg. To manually connect";
    println "to a different database use the \"DBConnect\" command before executing this script";
    println ""
}
if (args.length < 4)
{
    help();
}
else
{
    def conn;
    try
    {
        if (!MAIN.isDBConnected())
        {
            println "\nConnecting to connection1";
            MAIN.executeCommand("DBConnect connection1");
        }

        def tableName = args[1];
        def columnName = args[2];
        def varcharSize = args[3];

        if (!varcharSize.matches("\\d+"))
        {
            println "Varchar Size must be a number";
            LOG.error("Invalid Varchar Size " + varcharSize);
        }
        else if (!columnName.toLowerCase().startsWith("u_"))
        {
            println "Only the u_ columns may be edited with this script";
            LOG.error("Invalid Column Name " + columnName);
        }
        else
        {
            sql = "select u_schema_definition from custom_table where u_name='" + tableName + "'";
            conn = new Sql(DB);

            LOG.warn("Getting Custom Table Definition: " + sql);
            def updatedTable = false;
            conn.eachRow(sql) { row ->
                //schemaDefinition = row.u_schema_definition;
                schemaDefinition = row.getString("u_schema_definition");
                if (schemaDefinition.toUpperCase().contains(columnName.toUpperCase()))
                {
                    def newSchema = schemaDefinition.replaceFirst("(?i)(" + columnName + ".+?length=\")\\d+", "\$1" + varcharSize);

                    def updateSql = "update custom_table set u_schema_definition = ? where u_name=?";

                    LOG.warn("Executing Update on table " + tableName + ": " + updateSql);
                    LOG.warn("New Schema\n" + newSchema);
                    if (conn.executeUpdate(updateSql, newSchema, tableName))
                    {
                        updatedTable = true;
                    }
                    else
                    {
                        println "WARNING!!! Custom Table Update SQL Failed";
                        LOG.error("Custom Table Update SQL Failed");
                    }
                }
            }

            if (updatedTable)
            {
                println "" + tableName + "-" + columnName + " has been updated to VARCHAR(" + varcharSize + ")";
                LOG.info("" + tableName + "-" + columnName + " has been updated to VARCHAR(" + varcharSize + ")");
            }
            else
            {
                println "Failed to update " + tableName + "-" + columnName + " to VARCHAR(" + varcharSize + ")";
                LOG.info("Failed to update " + tableName + "-" + columnName + " to VARCHAR(" + varcharSize + ")");
            }
        }
    }
    catch (Exception e)
    {
        println "WARNING!!! Unexpected Exception occurred: " + e.getMessage();
        LOG.error("Unexpected Exception execute Update Custom Table Script", e);
    }
}

return null;
