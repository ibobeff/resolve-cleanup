def statement;
def failed = "";
def repeat = 1;
def interval = 1;

try
{
    if (MAIN.isDBConnected())
    {
        def include = "";
        if (args.length > 1)
        {
            def j = 1;
            if (args[j].matches("\\d"))
            {
                repeat = args[j].toInteger();
                j++;
                if (args.length > 2 && args[j].matches("\\d"))
                {
                    interval = args[j].toInteger();
                    j++;
                }
            }
            for (int i=j; i<args.length; i++)
            {
                if (!include)
                {
                    include = "'" + args[i] + "'";
                }
                else
                {
                    include += ",'" + args[i] + "'";
                }
            }
        }
        def sql = "select u_fullname, u_content, u_summary from wikidoc where u_namespace = 'RSQA' and u_tag='RSQA_Automated'";
        if (include)
        {
            sql += " and u_fullname in (" + include + ")";
        }
        statement = DB.createStatement();
        LOG.debug("Get RSQA Tests: " + sql);

        def paramList = [];

        def rs = statement.executeQuery(sql);
        while (rs.next())
        {
            def test = rs.getString("u_fullname");

            def content = rs.getString("u_content");
            def description = rs.getString("u_summary");
            def wiki = "";
            LOG.info("Test Page: " + test + ": " + description);

            def params = [:];
            for(line in content.split("\n"))
            {
                if(line.contains("RSQA_TEST_RUNBOOK"))
                {
                    def matcher = line =~ /.*value=\"(.*?)\".*/;
                    wiki = matcher[0][1];
                }
                if(line.contains("RSQA_TEST_PARAMS"))
                {
                    def matcher = line =~ /.*name=\"(.*?)\".*/;
                    def key = matcher[0][1];
                    matcher = line =~ /.*value=\"(.*?)\".*/;
                    def value = matcher[0][1];
                    params[key] = value;
                }
            }
            if(wiki == "" || test == null || test =="")
            {
                failed += test + "\n";
            }
            else
            {
                params["WIKI"] = wiki;
                params["SUMMARY"] = test;
                params["PROBLEMID"] = "NEW";
                params["DESCRIPTION"] = description;
                params["PROCESS_DEBUG"] = "true";
                params["USERID"] = "admin";

                paramList.add(params);

                println "Adding Test: " + test;
                LOG.info("Adding Test: " + test + " with params: " + params);
            }
        }
        rs?.close();

        for (int i=0; i<repeat; i++)
        {
            println "Executing set: " + i;
            for (params in paramList)
            {
                ESB.sendMessage("EXECUTEQUEUE","MAction.executeProcess",params);
                LOG.debug("Executing Test: " + params);
            }
            if ((i+1) < repeat)
            {
                Thread.sleep(interval * 1000);
            }
        }
    }
    else
    {
        println "Must be Connected to DB to Execute RSQA Tests";
        LOG.warn("DB Not Connected, Skipping Tests");
    }
}
catch(Exception e)
{
    println "Test Run Failed: " + e.getMessage();
    LOG.warn("RSQA Test Failed: " + e.getMessage(), e);
}
finally
{
    statement?.close();
}

if(failed != "")
{
    println "The following tests failed to run:" + failed;
    LOG.warn("The following tests failed to run:" + failed);
}

return null;

def help()
{
    println "Usage: ExecuteRSQA [<repeat>] [<interval>] [<runbook>]...";
    println "";
    println "  Executes RSQA Runbooks, or Specified RSQA runbooks if supplied";
    println "  as a batch execution.  Default is all RSQA runbook executed as";
    println "  a single batch";
    println "";
    println "  Options:";
    println "      repeat       Number of RSQA batches to submit";
    println "                     Default 1";
    println "      interval     Number of seconds between each RSQA batch";
    println "                     Default 1";
    println "      runbook      RSQA Runbook to Execute";
    println "";
    println "  e.g. EventGenerate \"RSQA.Test Simple\"";
    println "";
    return null;
}
