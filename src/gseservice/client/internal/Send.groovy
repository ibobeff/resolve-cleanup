
def help()
{
    println "Usage: Send <MClass.method> [<name1>=<val1> <name2>=<val2> ...]";
    println "";
    println "  Sends the parameters to the specified MClass.method.";
    println "  Use quotes around the entire key=value if the value has spaces e.g. Send MClass.method \"test=hello world\"";
    println "";
    return null;
}


if (args.length < 2)
{
    help();
}
else
{
    def classMethod = args[1]
    
    def params = [:]
    if (args.length > 2)
    {
        for (i in 2 .. args.length-1)
        {
            def param = args[i]
             
            int pos = param.indexOf('=')
            if (pos > 0)
            {
                def key = param.substring(0,pos)
                def val = param.substring(pos+1,param.length())
                
                params[key] = val
                
            }
        }
        
    }

    MAIN.setMethod(classMethod, "MDefault.print");
    return params;
}


