if (args.size() < 5)
{
    return help();
}

def wiki = args[1];
def size = args[2].toLong();
def interval = args[3].toInteger();
def repeat = args[4].toInteger();
def queue = "EXECUTEQUEUE";

if (args.size() >= 6)
{
	queue = args[5];
}

if(repeat==0)
{
    repeat = 1
}

if (!MAIN.connected)
{
   MAIN.initESB();
}

MAIN.setMethod("MAction.executeProcess", "MDefault.print");

params = new HashMap();
params["PROBLEMID"] = "NEW"
params["USERID"] = MAIN.getUsername();
params["WIKI"] = wiki
def i = 6;
while (i < args.size())
{
    def nameval = args[i].split("=");
    params[nameval[0]] = nameval[1]
    
    i++;
}

// send messages
//MMsgHeader header = new MMsgHeader();
//header.setRouteDest("EXECUTEQUEUE", "MAction.executeProcess")

def boolean success = false;

for (repeatIdx in 1..repeat)
{
	println("repeatIdx: $repeatIdx");
	
	def long startTime = System.currentTimeMillis();
	
	success = ESB.sendMessageRepeatedly(queue, "MAction.executeProcess", params, size);
	
	//println("repeatIdx: $repeatIdx Successfully sent " + successSendCount + " messages");
	if (success)
	{
		println("repeatIdx: $repeatIdx Successfully sent " + size + " messages, took " + (float)((System.currentTimeMillis() - startTime) / 1000) + " secs");
	}
	else
	{
		println("repeatIdx: $repeatIdx Failed to send " + size + " messages, took " + (float)((System.currentTimeMillis() - startTime) / 1000) + " secs");
	}
	
	if (repeatIdx != repeat)
	{
		Thread.sleep(interval*1000);
	}
}
	
return null;

def help()
{
    println "Usage: ExecuteLoadTest <wiki> <size> <interval> <repeat> <queue> <param=value> ...";
    println "";
    println "  Executes the Wiki Runbook with the provided load test parameters";
    println "    wiki       - Runbook to be executed";
    println "    size       - Number of jobs to be requested in each batch";
    println "    interval   - Number of secs to wait between batch";
    println "    repeat     - Number of batches to be submitted";
	println "    queue      - Queue name, if not provided defaults to EXECUTEQUEUE";
    println "";
    println "  e.g. ExecuteLoadTest \"LoadTest.5stepssh\" 1500 1 10 DEXECUTEQUEUE";
    println "";
    println "    This will submit 1500 request repeatedly for 10 times with delay of 1 sec in between to durable queue DEXECUTEQUEUE, i.e. a total of 15,000 requests";
    println "";
    return null;
} // help
