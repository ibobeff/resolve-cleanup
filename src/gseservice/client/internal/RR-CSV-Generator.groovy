import java.lang.NumberFormatException;
import java.io.File;
import java.io.IOException;
import java.lang.Exception;
import java.io.FileNotFoundException;
import java.lang.StringBuilder;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;

if (args.size() > 4 || args.size() < 4)
{
    return help();
} else {
    
    try{
        File configFile = new File(args[1]);
        BufferedReader br = new BufferedReader(new FileReader(configFile));
        
        String names = br.readLine();
        String values = br.readLine();
        String savePathInput = args[2];
        int countInput = Integer.parseInt(args[3]);
        
        br.close();
        
        
        Generator g = new Generator(names, values, countInput, savePathInput);
        g.start();
    } catch(IOException ioe){
        ioe.printStackTrace();
    } catch(FileNotFoundException fnfe){
        fnfe.printStackTrace();
    } catch(Exception e){
        e.printStackTrace();
    }
}

def help()
{
    println "----- HELP -----";
    println "RR CSV Generator Tool";
    println "Generates a csv file of RID data.";
    println "";
    println "USAGE: Requires 3 arguments ";
    println "Config path - Absolute path to a config file which describes the rules to output a CSV block of field data.";
    println "Save path - Absolute path to a destination save file."
    println "Row count - The number of rows to generate in the CSV file."
    println "";
    println "Example: rr-csv-generator C:/config.txt C:/output.csv 1000";
    println "";
    println "The config file requires 2 lines:"
    println "LINE 1: A line of field names.";
    println "LINE 2: A line of field value rules.";
    println "";
    println "Example: ";
    println "";
    println "module,schema,wiki,runbook,automation,eventId,active,newWorksheetOnly,sir,source,title,type,playbook,severity,owner";
    println "test#,,[true;false],true,network(1-100),dns,device[100;200;300;400],10.20.2.(100-400)";
    println "";
    println "FIELD VALUE RULES: ";
    println "Each comma-separated field can be given a particular format which will increment ";
    println "with specific behavior for each new row.";
    println "";
    println "1) EMPTY VALUE - enter nothing. The field value will be blank for each row."
    println "2) TEXT - a constant text value. The field value does not change with each row. e.g. myTextValue";
    println "3) LIST OF CHOICES [] - randomly chooses a value in a given set for each new row. Can be nested into a string value. - e.g. isEnabled=[true;false]";
    println "4) INCREMENTING # - increments a number #. Can be nested into a string value. - e.g. device#";
    println "5) RANGE () - randomly nests a unique value within a string using a integer range (start-end). NOTE: values will only be unique if the range is less than or equal to the row count. Can be nested into a string value. - e.g. 10.20.1.(0-999)";
   
} // help

class Generator {
    //[MODULE],[SCHEMA],[WIKI],[RUNBOOK],[AUTOMATION],[EVENTID],[ACTIVE]
    
    ArrayList<Modifier> valueModifiers;
    
    // File
    StringBuilder fileOutput;
    BufferedReader br;
    
    
    int count = 0;
    String namesText = "";
    String valuesText = "";
    String saveFilePath = "";
    
    public Generator(String names, String values, int count, String saveFilePath){
       
        this.namesText = names;
        this.valuesText = values;
        this.count = count;
        this.saveFilePath = saveFilePath;
        
    }
    
    public void start(){
        // Append CSV header
        fileOutput = new StringBuilder();
        fileOutput.append(namesText + "\n");
        
        // Parse values into modifier rules
        parseValues(valuesText);
        
        // Write file output
        for (int i=0; i<count; i++){
            for (int j=0; j<valueModifiers.size(); j++){
                fileOutput.append(valueModifiers.getAt(j).getNext() + ",");
            }
            fileOutput.append("\n");
        }
        
        // saveFile
        saveFile();
        
        println "Goodbye"
    }
    
    private void parseValues(String valuesText){
        String[] values = valuesText.split(",");
        
        valueModifiers = new ArrayList<Modifier>();
        for(int i=0; i<values.length; i++){
            
            if(values[i].indexOf("#") != -1){
                // check #
                valueModifiers.add(new IncrementModifier(values[i]));
            } else if(values[i].indexOf("[") != -1){
                // check []
                valueModifiers.add(new RandomListModifier(values[i], values[i].substring(values[i].indexOf("[")+1, values[i].indexOf("]"))));
            } else if(values[i].indexOf("(") != -1){
                // check ()
                
                String rangeValues = values[i].substring(values[i].indexOf("(")+1, values[i].indexOf(")"));
                String[] rangeInputs = rangeValues.split("-");
                int start = Integer.parseInt(rangeInputs[0]);
                int stop = Integer.parseInt(rangeInputs[1]);
                valueModifiers.add(new RangeModifier(values[i], start, stop));
            } else {
                // else String constant
                valueModifiers.add(new StringConstant(values[i]));
            }
            
        }
    }
    
    private void saveFile(){
            
        try {
            //println(fileOutputString.toString());
            File saveDestination = new File(saveFilePath);
            FileOutputStream fos = new FileOutputStream(saveDestination);
            fos.write(fileOutput.toString().getBytes());
            fos.close();
            println "\nFile saved to " + saveDestination.getCanonicalPath();
        } catch(IOException ioe){
            println "An error occured while trying to save your file...";
            ioe.printStackTrace();
        } catch(Exception e){
            println "An error occured while trying to save your file...";
            e.printStackTrace();
        }
    }
}

abstract class Modifier{

    public String getNext(){
        return "";
    }

}

class StringConstant extends Modifier {
    
    String word;
    
    public StringConstant(String word) {
        this.word = word;
    }
    
    public String getNext(){
        // get random item from list
        return word;
    } 
}

class RandomListModifier extends Modifier{

    String[] listItems;
    String text;
    
    public RandomListModifier(String text, String params){
        this.text = text;
        listItems = params.split(";");
        
        for (int i = 0; i < listItems.length; i++){
            listItems[i] = listItems[i].trim();
        }
    }
    
    public String getNext(){
        // get random item from list
        return text.replaceAll("\\[.*?\\]", listItems[(int) Math.floor(Math.random()*listItems.length)]);
    }
}

class IncrementModifier extends Modifier{
    
    int counter = 0;
    String text = "";
    String textModified = "";
    
    
    public IncrementModifier(String text){
        if(text != null || !text.isEmpty()){
            this.text = text;
        }
    }
    
    public String getNext(){
        counter++;
        String counterStr = counter + "";
        if(text.isEmpty()){
            return counter+""; 
        } else {
            textModified = text.replaceAll("#", counterStr);
            return textModified;
        }
    }
}

class RangeModifier extends Modifier{

    int range = 0;
    int startValue = 0;
    String text = "";
	ArrayList<Integer> nums;
	int counter = 0;

    public RangeModifier(String text, int startValue, int endValue){
        //println "Start value, end value: " + startValue + ", " + endValue;
        this.startValue = startValue;
        this.text = text;
		nums = new ArrayList<Integer>();
		int length = endValue - startValue;
		
		// create a list of possible ints
		nums = new ArrayList<Integer>();
		for(int i = 0; i < length; i++){
			nums.add(startValue + i);
		}
		
		// shuffle ints
		Collections.shuffle(nums);
		
    }
    
    public String getNext(){
        int val = nums.get(counter);
		counter++;
		if(counter >= nums.size()){
			counter = 0;
		}
        return text.replaceAll("\\(.*?\\)", val+"");
    }
    
}

class EmptyModifier extends Modifier {
    public String getNext(){
        return "";
    }
}

