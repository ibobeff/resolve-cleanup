MAIN.setMethod("MInfo.threadpool", "MService.internal.ThreadpoolCallback");
params = new Hashtable();

return params;


def help()
{
    println "Usage: threadpool";
    println "";
    println "  Query the specified component(s) for threadpool summary information."
    println "";
    return null;
} // help