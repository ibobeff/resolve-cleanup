def help()
{
    println "\nUsage: SetPassword "
	println "User: [username]" 
	println "Old Password: [old password]" 
	println "New Password: [new password]";
    println ""
    println "This Script will Change the specified password on connected RSView systems";
    println ""
    return null;
}
import com.resolve.util.PasswordField
MAIN.setMethod("MAction.updatePassword", "MDefault.print");

def username;
def oldPassword;
def newPassword
def params = null;

def count = 0;
try
{
    def br;
    if (!oldPassword || !newPassword || !username)
    {
        br = new BufferedReader(new InputStreamReader(System.in));
    }
	while (!username && count < 4){
		LOG.debug("Getting username");
		print "username: ";
		username = br.readLine();
		count++;
	}
	if(!username)
	{
		println "Failed to enter username";
		LOG.warn("Failed to enter username");
	}else
	{
		while (!oldPassword && count < 4)
		{
			LOG.debug("Getting Old password");
			oldPassword = PasswordField.getPassword("old Password: ");
			count++;
		}
		if (!oldPassword)
		{
			println "Failed to enter Old Password";
			LOG.warn("Failed to enter Old Password");
		}
		else
		{
			count = 0;
			while (!newPassword && count < 4)
			{
				LOG.debug("Getting New password");
				newPassword = PasswordField.getPassword("new Password: ");
				count++;
			}
			if (!newPassword)
			{
				println "Failed to enter New Password";
				LOG.warn("Failed to enter New Password");
			}
			else
			{
				params = [:];
				params[Constants.USER_ADMIN] = username;
				params[Constants.OLD_PASSWORD] = oldPassword;
				params[Constants.NEW_PASSWORD] = newPassword;

				if (!MAIN.connected)
				{
					MAIN.executeCommand("CONNECT RSVIEWS");
					MAIN.setMethod("MAction.updatePassword", "MDefault.print");
				}
			}
		}
	}
}
catch (Exception e)
{
    println "Error changing password: " + e.getMessage();
    LOG.error("Error changing password", e);
}
return params;
