if (args.size() < 2)
{
    return help();
}

MAIN.setMethod("MService.config.View", "MDefault.print");

params = new Hashtable();
params["FILENAME"] = args[1];
return params;

def help()
{
    println "Usage: View <filename>";
    println "";
    println "<filename>  - name of file in the config directory (e.g. config.xml)";
    println "";
    return null;
} // help