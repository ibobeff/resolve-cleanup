import com.resolve.util.CryptUtils;

if (args.size() < 2)
{
    return help();
}

MAIN.setMethod("MService.config.setPropertyNoEncrypt", "MDefault.print");

name = args[1];
value = args[2];

// check for encrypt
if (value != null && !value.equals(""))
{
    if (name.startsWith("ENC_") || name.startsWith("enc_"))
    {
    	value = CryptUtils.encrypt(value);
    }
}

params = new Hashtable();
params["NAME"] = name.toUpperCase();
params["VALUE"] = value;
return params;

def help()
{
    println "Usage: SetProperty <name> <value>";
    println "";
    println "  Prefix the property name with ENC_ if the value is to be encrypted";
    return null;
} // help