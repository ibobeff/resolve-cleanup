if (args.size() < 2)
{
    return help();
}

MAIN.setMethod("MService.config.SetLogLevel", "MDefault.print");

params = new Hashtable();
params["LEVEL"] = args[1];
return params;


def help()
{
    println "Usage: SetLogLevel <level>";
    println "";
    println "<level>  - Log level: DEBUG, INFO, WARN, ERROR";
    println "";
    return null;
} // help