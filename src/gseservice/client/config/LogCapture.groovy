def help()
{
    println "\nUsage: LogCapture [<queue name>]";
    println ""
    println "This Script will send a message to the RSMGMTS topic to run the log capture";
    println "unless a <queue name> is specified as an argument";
    println ""
}

def queue = "RSMGMTS";
if (args.length > 1)
{
    queue = args[1];
}

def params = [:];
MAIN.sendMessage(queue,"MService.mgmt.LogCapture",params,"MDefault.print");
println "Capturing Logs ";
LOG.warn("Sending Log Capture Message to : " + queue);

return null;
