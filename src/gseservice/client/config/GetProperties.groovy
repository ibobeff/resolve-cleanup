MAIN.setMethod("MService.config.getProperties", "MDefault.print");
params = new Hashtable();
return params;

def help()
{
    println "Usage: GetProperties";
    println "";
    println "  Returns the local system properties and environment variables on the component";
    println "";
    return null;
} // help