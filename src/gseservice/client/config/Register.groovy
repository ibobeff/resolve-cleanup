MAIN.setMethod("MRegister.register", "MDefault.print");
params = new Hashtable();

return params;

def help()
{
    println "Usage: register";
    println "";
    println "  Initiates the registration process for the component.";
    println "";
    return null;
} // help