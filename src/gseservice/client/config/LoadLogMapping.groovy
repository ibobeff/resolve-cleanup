MAIN.setMethod("MService.config.loadLogMapping", "MDefault.print");

params = new Hashtable();
return params;

def help()
{
    println "Usage: loadLogMapping";
    println "";
    println "  Displays the current log re-mapping configuration";
    println "";
    return null;
} // help