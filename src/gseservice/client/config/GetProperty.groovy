if (args.size() < 2)
{
    return help();
}

MAIN.setMethod("MService.config.getProperty", "MDefault.print");

params = new Hashtable();
params["NAME"] = args[1];
return params;

def help()
{
    println "Usage: GetProperty";
    println "";
    println "  Returns the value of the specified property on the component";
    println "";
    return null;
} // help