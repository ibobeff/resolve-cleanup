if (args.size() < 2)
{
    return help();
}

MAIN.setMethod("MService.config.removeProperty", "MDefault.print");

params = new Hashtable();
params["NAME"] = args[1];
return params;


def help()
{
    println "Usage: RemoveProperty <name>";
    println "";
    println "  Remove the specified property from the component";
    println "";
    return null;
} // help