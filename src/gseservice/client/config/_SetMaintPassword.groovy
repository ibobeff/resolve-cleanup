def help()
{
    println "\nUsage: SetMaintPassword [old password] [new password]";
    println ""
    println "This Script will Change the resolve.maint password on connected RSView systems";
    println ""
    return null;
}

MAIN.setMethod("MAction.updateMaintPassword", "MDefault.print");

def oldPassword;
def newPassword
if (args.length > 1)
{
    oldPassword = args[1];
    if (args.length > 2)
    {
        newPassword = args[2];
    }
}

def params = null;

def count = 0;
try
{
    def br;
    if (!oldPassword || !newPassword)
    {
        br = new BufferedReader(new InputStreamReader(System.in));
    }
    while (!oldPassword && count < 3)
    {
        LOG.debug("Getting Old resolve.maint password");
        print "Old Password: ";
        oldPassword = br.readLine();
        count++;
    }
    if (!oldPassword)
    {
        println "Failed to enter Old Password";
        LOG.warn("Failed to enter Old Password");
    }
    else
    {
        count = 0;
        while (!newPassword && count < 3)
        {
            LOG.debug("Getting New resolve.maint password");
            print "New Password: ";
            newPassword = br.readLine();
            count++;
        }
        if (!newPassword)
        {
            println "Failed to enter New Password";
            LOG.warn("Failed to enter New Password");
        }
        else
        {
            params = [:];
            params[Constants.OLD_PASSWORD] = oldPassword;
            params[Constants.NEW_PASSWORD] = newPassword;

            if (!MAIN.connected)
            {
                MAIN.executeCommand("CONNECT RSVIEWS");
                MAIN.setMethod("MAction.updateMaintPassword", "MDefault.print");
            }
        }
    }
}
catch (Exception e)
{
    println "Error changing resolve.maint password: " + e.getMessage();
    LOG.error("Error changing resolve.maint password", e);
}
return params;
