MAIN.setMethod("MService.config.SetDebug", "MDefault.print");

params = new Hashtable();
if (args.size() > 1)
{
	params["MODULE"] = args[1];
	if (args.size() > 2)
	{
		params["VALUE"] = args[2];
	}
	else
	{
	    params["VALUE"] = "true";
	}
}
return params;

def help()
{
    println "Usage: SetDebug";
    println "";
    println "  Change the logging level to debug. This is not persistent, you need to edit the log.cfg file to make a permanent change.";
    println "";
    return null;
} // help