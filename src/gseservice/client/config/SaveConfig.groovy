MAIN.setMethod("MService.config.SaveConfig", "MDefault.print");
params = new Hashtable();

return params;

def help()
{
    println "Usage: saveConfig";
    println "";
    println "  Save the current active configuration of the component to the file system.";
    println "";
    return null;
} // help