if (args.size() < 2)
{
    return help();
}

MAIN.setMethod("MAction.executeProcess", "MDefault.print");

params = new HashMap();
params["USERID"] = MAIN.getUsername();
params["WIKI"] = args[1];
def i = 2;
while (i < args.size())
{
    def nameval = args[i].split("=");
    params[nameval[0]] = nameval[1]
    
    i++;
}

if (!params.containsKey("PROBLEMID") && !params.containsKey("problemid"))
{
    params["PROBLEMID"] = "NEW"
}
if (!MAIN.connected) {
   MAIN.initESB();
   ESB.sendMessage("EXECUTEQUEUE", "MAction.executeProcess", params);
   println "Executing ${params["WIKI"]} with params: ${params}";
}

return params;

def help()
{
    println "Usage: ExecuteRunbook <wiki> [<param1>=<value1> <param2=value2> ...]";
    println "";
    println "  Executes the Wiki Runbook with the provided parameters";
    println "";
    println "  e.g. ExecuteRunbook \"Condition.High CPU Load\" NAME=VALUE";
    println "";
    return null;
} // help