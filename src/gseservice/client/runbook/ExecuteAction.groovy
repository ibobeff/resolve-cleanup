if (args.size() < 2)
{
    return help();
}

MAIN.executeCommand("connect RSCONTROL");
MAIN.setMethod("MAction.executeTask", "MDefault.print");

params = new Hashtable();
params["ACTIONNAME"] = args[1];

// check if missing actionname
if (args[1].indexOf('&') != -1)
{
    println "ERROR: Missing actiontask name"
    println "";
    
    params = null;
}
else
{
	int i = 2
	while (i < args.size())
	{
	    def nameval = args[i].split("=");
	    params[nameval[0]] = nameval[1]
	    
	    i++;
	}
}

return params;

def help()
{
    println "Usage: ExecuteAction <taskname> [<PROBLEMID=NEW param1=value1>]";
    println "";
    println "  Executes the ActionTask. Set the worksheet ID to NEW for"
    println "  a new worksheet or ACTIVE for the currently active worksheet"
    println "";
    println "  e.g. executeaction \"Condition.High CPU Load\" NEW";
    println "";
    return null;
} // help
