MAIN.setMethod("MAction.archive", "MDefault.print");

def help()
{
    println "Usage: Archive [force=true|false] [sleeptime=60]";
    println "";
    println "  Perform the runbook runtime table to archive table";
    println "";
    println "  e.g. archive force=true sleeptime=60";
    return null;
}

def params = [:]
if (args.length > 1)
{
    for(param in args)
    {
        if(param.contains("force"))
        {
            params[Constants.ESB_PARAM_FORCE] = param.split("=")[1];
        }
        if(param.contains("sleeptime"))
        {
            params[Constants.ARCHIVE_SLEEPTIME] = param.split("=")[1];
        }
    }
}
else
{
    help();
}

return params;


