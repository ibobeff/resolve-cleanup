def help()
{
    println "\nUsage: Index <action task> <wikidocuments>";
    println ""
    println "This Script will run the search index, which components will be indexed";
    println "and whether to run the is based on the booleans passed in the arguments";
    println ""
}
if (args.length != 3)
{
    help();
}
else
{
    def actionTask = "true".equalsIgnoreCase(args[1]);
    def wikiDoc = "true".equalsIgnoreCase(args[2]);

    def params = [:];
    params["INDEX_ALL_ACTIONTASK"] = actionTask;
    params["INDEX_ALL_WIKIDOCUMENTS"] = wikiDoc;
    
    LOG.warn("Sending MIndex.indexAll : " + params);
    MAIN.sendMessage("RSVIEW","MIndex.indexAll",params);
    println "Indexing:";
    println "\tAction Tasks: " + actionTask;
    println "\tWiki Documents: " + wikiDoc;
}

return null;
