MAIN.setMethod("MAction.compactCassandra", "MDefault.print");

def help()
{
    println "Usage: CompactCassandra <start>";
    println "";
    println "  Run full compaction on the Cassandra cluster.";
    println "  This action can take a while to complete and will increase";
    println "  load on the server.  It is recommended to only run during";
    println "  off hours when server load is not as high";
    println ""
    println "e.g. CompactCassandra true";
    return null;
}

def params = null;
if (args.length > 1)
{
    if (args[1].equalsIgnoreCase("true"))
    {
        params = [:];
    }
    else
    {
        help();
    }
}
else
{
    help();
}

return params;


