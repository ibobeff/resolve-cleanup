if (args.size() < 2)
{
    return help();
}

MAIN.setMethod("MAction.abortWorksheet", "MDefault.print");

params = new HashMap();
params["USERID"] = MAIN.getUsername();
params["PROBLEMID"] = args[1];
return params;

def help()
{
    println "Usage: AbortWorksheet <problemid | number>";
    println "";
    println "  Abort Runbook with the specified PROBLEMID";
    println "";
    return null;
} // help
