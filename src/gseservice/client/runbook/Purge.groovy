MAIN.setMethod("MAction.purge", "MDefault.print");

def help()
{
    println "Usage: Purge date=yyyymmdd [lobonly=true|false]";
    println "";
    println "  Perform the archive table to be purged for the given date";
    println "  Data beyond the given date will be purged from archive tables.";
    println "  lobonly=true purges only from the LOB tables.";
    println "";
    println "  e.g. purge date=20121231 lobonly=true";
    return null;
}

def params = [:]
if (args.length > 1)
{
    for(param in args)
    {
        if(param.contains("date"))
        {
            params[Constants.PURGE_DATE] = param.split("=")[1];
        }
        if(param.contains("lobonly"))
        {
            params[Constants.PURGE_LOB_ONLY] = param.split("=")[1];
        }
    }
    if(params.size() == 0)
    {
        help();
    }
}
else
{
    help();
}

return params;