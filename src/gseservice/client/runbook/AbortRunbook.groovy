if (args.size() < 2)
{
    return help();
}

MAIN.setMethod("MAction.abortProcess", "MDefault.print");

params = new HashMap();
params["USERID"] = MAIN.getUsername();
params["PROCESSID"] = args[1];
return params;

def help()
{
    println "Usage: AbortRunbook <processid>";
    println "";
    println "  Abort Runbook with the specified PROCESSID";
    println "";
    return null;
} // help