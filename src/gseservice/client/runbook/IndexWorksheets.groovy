import java.text.SimpleDateFormat;

def help()
{
    println "\nUsage: IndexWorksheets <start time> <end time>";
    println ""
    println "This script will run the index on all worksheets based on the time period";
    println "specified.  The start and end times format can either be in milliseconds since";
    println "January 1, 1070, 00:00:00 GMT, as MM/dd/yyyy, MM/dd/yyyy hh:mm:ss, or 0 if";
    println "the start time should be since the last Index was run, or the end should be now";
    println ""
}
if (args.length != 3)
{
    help();
}
else
{
    def startTime = args[1].trim();
    def endTime = args[2].trim();

    startTime = parseTime(startTime);
    endTime = parseTime(endTime);

    if (startTime >=0 && endTime >= 0)
    {
        def params = [:];
        params["STARTTIME"] = startTime;
        params["ENDTIME"] = endTime;
        
        LOG.warn("Sending MIndex.indexAll : " + params);
        MAIN.sendMessage("RSVIEW","MIndex.indexAllWorksheets",params);

        def sdf = new SimpleDateFormat("MMM/dd/yyyy hh:mm:ss");
        print "Indexing Worksheets from ";
        if (startTime == 0)
        {
            print "Last Index Start Time";
        }
        else
        {
            print sdf.format(startTime);
        }
        print " to ";
        if (endTime == 0)
        {
            println "Now";
        }
        else
        {
            println sdf.format(endTime);
        }
    }
}

return null;

def parseTime(def time)
{
    def result = -1;
    if (time.matches("\\d+"))
    {
        result = Long.parseLong(time);
    }
    else if (time.matches("\\d{1,2}/\\d{1,2}/\\d{4}"))
    {
        def sdf = new SimpleDateFormat("MM/dd/yyyy");
        result = sdf.parse(time).getTime();
    }
    else if (time.matches("\\d{1,2}/\\d{1,2}/\\d{4} \\d{2}:\\d{2}:\\d{2}"))
    {
        def sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
        result = sdf.parse(time).getTime();
    }
    else
    {
        println "WARNING!!! Invalid Time : " + time + "\n";
    }

    return result;
}
