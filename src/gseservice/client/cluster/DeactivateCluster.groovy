def help()
{
    println "\nUsage: DeactivateCluster"
    println ""
    println "This script will broadcast a message to deactivate the Resolve cluster."
    println "Before running this script, connect to rsremote using the following command in RSConsole."
    println "";
    println "connect BROADCAST"
}

println "Deactivate the cluster when primary cluster is ready";

def params = [:]
//MAIN.variables.put(MAIN.VAR_MSG_ROUTE, Constants.ESB_NAME_BROADCAST);
//MAIN.sendMessage(Constants.ESB_NAME_BROADCAST,"MAction.deactivateCluster",params);
MAIN.setMethod("MAction.deactivateCluster", "MDefault.print");

return params;