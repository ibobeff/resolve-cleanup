def help()
{
    println "\nUsage: ActivateCluster"
    println ""
    println "This script will broadcast a message to activate the Resolve cluster."
    println "Before running this script, connect to rsremote using the following command in RSConsole."
    println "";
    println "connect BROADCAST"
}

println "Activate the cluster when secondrary cluster needs to be ready";

def params = [:]
//MAIN.variables.put(MAIN.VAR_MSG_ROUTE, Constants.ESB_NAME_BROADCAST);
//MAIN.sendMessage(Constants.ESB_NAME_BROADCAST,"MAction.activateCluster",params);
MAIN.setMethod("MAction.activateCluster", "MDefault.print");

return params;