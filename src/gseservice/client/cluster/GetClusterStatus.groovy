def help()
{
    println "\nUsage: GetClusterStatus"
    println ""
    println "This script will broadcast return the status of the Resolve cluster."
    println "Before running this script, connect to rsremote using the following command in RSConsole."
    println "";
    println "connect BROADCAST"
}

println "Getting Resolve cluster status... ";

def params = [:]
//MAIN.variables.put(MAIN.VAR_MSG_ROUTE, Constants.ESB_NAME_BROADCAST);
//MAIN.sendMessage(Constants.ESB_NAME_BROADCAST,"MAction.activateCluster",params);
MAIN.setMethod("MAction.getClusterStatus", "MDefault.print");

return params;