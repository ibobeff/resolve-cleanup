def help()
{
    println "\nUsage: IndexAllActionTasks"
    println ""
    println "This script will broadcast a message to index all action tasks in Resolve."
    println "Before running this script, connect to rsremote using the following command in RSConsole."
    println "";
    println "connect BROADCAST"
}

println "Indexing all action tasks...";

def params = [:]
MAIN.setMethod("MAction.indexAllActionTasks", "MDefault.print");

return params;