def help()
{
    println "\nUsage: IndexAllWikis"
    println ""
    println "This script will broadcast a message to index all wikis in Resolve."
    println "Before running this script, connect to rsremote using the following command in RSConsole."
    println "";
    println "connect BROADCAST"
}

println "Indexing all wiki documents...";

def params = [:]
MAIN.setMethod("MAction.indexAllWikiDocuments", "MDefault.print");

return params;