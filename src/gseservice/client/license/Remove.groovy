import com.resolve.rsbase.MainBase;

def help()
{
    println "Usage: Remove <space separated list of license keys>";
    println "";
    println "  Remove a list of licenses";
    println "";
    println "  e.g. remove 9D799F78E42C2F6C60946909B321BC59 E86799F78E42C2F6C61246909B321BD25";
    return null;
}

def params = [:]
if (args.length > 1)
{
    for(int i = 1; i < args.length; i++)
    {
        def param = args[i];
        params[param.trim()] = "";
    }
    params[Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID] = MainBase.main.configId.getGuid();
    MAIN.variables.put(MAIN.VAR_MSG_ROUTE, Constants.ESB_NAME_RSLICENSES);
    MAIN.sendMessage(Constants.ESB_NAME_RSLICENSES,"MLicense.remove",params);
}
else
{
    help();
}

return params;


