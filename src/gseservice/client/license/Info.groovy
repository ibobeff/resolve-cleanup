def help()
{
    println "Usage: Info [key=License Key]";
    println "";
    println "  Get license information, if "key" param not provided then cummulative license info is displayed";
    println "";
    println "  e.g. info key=9D799F78E42C2F6C60946909B321BC59";
    return null;
}

def params=[:];
if (args.length > 1)
{
    for(param in args)
    {
        if(param.contains("key"))
        {
            String licenseKey = param.split("=")[1];
            params[LicenseUtil.LICENSE_KEY] = licenseKey;
        }
    }
}

MAIN.variables.put(MAIN.VAR_MSG_ROUTE, Constants.ESB_NAME_RSLICENSE);
MAIN.variables.set(MAIN.VAR_MSG_CALLBACK, "MService.license.InfoCallback");
MAIN.sendMessage(Constants.ESB_NAME_RSLICENSE,"MLicense.getLicenseInfo",params, "MService.license.InfoCallback");
