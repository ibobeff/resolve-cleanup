import com.resolve.rsbase.MainBase;

def help()
{
    println "Usage: Upload file=<absolute location of license file>";
    println "";
    println "  Upload the license";
    println "";
    println "  e.g. upload file=/usr/home/resolve/license.lic";
    return null;
}

def params = [:]
if (args.length > 1)
{
    for(param in args)
    {
        if(param.contains("file"))
        {
            String fileContents = new File(param.split("=")[1]).getText('UTF-8');
            params[Constants.LICENSE_CONTENT] = fileContents;
            params[Constants.HTTP_ATTRIBUTE_RSVIEW_ID_GUID] = MainBase.main.configId.getGuid();
            MAIN.variables.put(MAIN.VAR_MSG_ROUTE, Constants.ESB_NAME_RSLICENSES);
            MAIN.sendMessage(Constants.ESB_NAME_RSLICENSES,"MLicense.upload",params);
        }
    }
}
else
{
    help();
}

return params;


