def help()
{
    println "\nUsage: UpdateTaskResultIndex [GUID]"
    println ""
    println "This Script will update the task results index"
    println ""
}
def guid = Constants.ESB_NAME_RSVIEW;

if (args.length >= 2)
{
    guid = args[1];
}
if (!MAIN.connected)
{
    MAIN.initESB();
}

println "Update the taskresult indexes and reindex data with new configuration";

def params = [:]
MAIN.variables.put(MAIN.VAR_MSG_ROUTE, Constants.ESB_NAME_RSVIEW);
MAIN.sendMessage(guid,"MIndex.updateTaskResultIndex",params);

return params;
