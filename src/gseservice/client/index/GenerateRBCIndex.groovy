if (args.size() < 3)
{
    return help();
}

def help()
{
    println "\nUsage: GenerateRBCIndex [01/02/2016 Ticket]"
    println ""
    println "This Script will generate RBC index for old worksheets starting from the given date and the given code. Only COMPLETED worksheets will be considered."
    println ""
}

def guid = Constants.ESB_NAME_RSVIEW;
def startDate = "";
def rbc = "";
def params = new HashMap();

if (args.length >= 3)
{
	params["START_DATE"] =  args[1].trim();
	params["RBC"] =  args[2].trim();
}

if (!MAIN.connected)
{
    MAIN.initESB();
}

println "Creating RBC index for old worksheets starting from the given date and the given code. Progress will be logged in debug rsview.log.";

MAIN.variables.put(MAIN.VAR_MSG_ROUTE, Constants.ESB_NAME_RSVIEW);
MAIN.sendMessage(Constants.ESB_NAME_RSVIEW,"MIndex.generateRBCForOldWorksheets",params);

return params;
