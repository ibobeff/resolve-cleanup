println "Deletes day based indices from RSSearch";
println "";

if (args.size() < 4)
{
    return help();
}
println "args size " + args.size();

params = new HashMap();
def param1 = args[1].split("=");
params[param1[0].trim()] = param1[1].trim()

def param2 = args[2].split("=");
params[param2[0].trim()] = param2[1].trim()

def param3 = args[3].split("=");
params[param3[0].trim()] = param3[1].trim()

if(args.size() == 5)
{
    def param4 = args[4].split("=");
    params[param4[0].trim()] = param4[1].trim()
}
MAIN.variables.put(MAIN.VAR_MSG_ROUTE, Constants.ESB_NAME_RSVIEW);
MAIN.sendMessage(Constants.ESB_NAME_RSVIEW,"MIndex.deleteRunbookIndex",params);

return params;

def help()
{
    println "Usage: DeleteRunbookIndex index=taskresult,processrequest,worksheet pwd=<xxx> [start=20150101] end=20150131";
    println "";
    println "  Deletes the index from RSSearch";
    println "    index - Name of one or more runbook related index to be deleted (e.g., taskresult, processrequest, worksheet)";
    println "    pwd   - Must be resolve.maint user password";
    println "    start - Optional date in yyyyMMdd format like 20150101 for January 1st, 2015";
    println "    end   - Date in yyyyMMdd format like 20150131 for January 31st, 2015";
    println "";
    return null;
} // help
