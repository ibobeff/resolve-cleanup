println "Truncates data from the requested index from RSSearch";
println "";

if (args.size() < 3)
{
    return help();
}

params = new HashMap();
def param1 = args[1].split("=");
params[param1[0].trim()] = param1[1].trim()

def param2 = args[2].split("=");
params[param2[0].trim()] = param2[1].trim()

if(args.size() == 4)
{
    def param3 = args[3].split("=");
    params[param3[0].trim()] = param3[1].trim()
}
MAIN.variables.put(MAIN.VAR_MSG_ROUTE, Constants.ESB_NAME_RSVIEW);
MAIN.sendMessage(Constants.ESB_NAME_RSVIEW,"MIndex.truncateRunbookIndex",params);

return params;

def help()
{
    println "Usage: TruncateRunbookIndex index=taskresult pwd=<xxx> [month=12]";
    println "";
    println "  Truncates the index from RSSearch";
    println "    index  - Name of the index to be truncated (e.g., worksheet, processrequest, taskresult)";
    println "    pwd    - Must be resolve.maint user password";
    println "    month  - Month number jan=1...dec=12";
    println "";
    return null;
} // help
