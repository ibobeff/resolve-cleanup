if (args.size() < 3)
{
    return help();
}

println "Removing requested index from RSSearch";
println "";

MAIN.executeCommand("connect RSVIEW");
MAIN.setMethod("MIndex.removeIndex", "MDefault.print");

params = new HashMap();
def param1 = args[1].split("=");
params[param1[0].trim()] = param1[1].trim()

def param2 = args[2].split("=");
params[param2[0].trim()] = param2[1].trim()

return params;

def help()
{
    println "Usage: RemoveIndex index=taskresult pwd=<xxx>";
    println "";
    println "  Removes the index from RSSearch";
    println "    index  - Name of the index to be removed (e.g., worksheet, processrequest, taskresult)";
    println "    pwd    - Must be resolve.maint user password";
    println "";
    return null;
} // help
