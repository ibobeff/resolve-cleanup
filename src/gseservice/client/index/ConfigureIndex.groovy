println "Creating indexes in ElasticSearch if not available";

def params = [:]
MAIN.variables.put(MAIN.VAR_MSG_ROUTE, Constants.ESB_NAME_RSVIEW);
MAIN.sendMessage(Constants.ESB_NAME_RSVIEW,"MIndex.configureIndex",params);

return params;
