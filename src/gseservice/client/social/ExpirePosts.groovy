import java.text.SimpleDateFormat;
MAIN.setMethod("MSocial.archiveBefore", "MDefault.print");

def help()
{
    println "Usage: ExpirePosts <Expire Date> [<Format>]";
    println "";
    println "Options:";
    println "  <Expire Date> - Date to use when Expiring Posts";
    println "                  Default Format: MM-dd-yyyy";
    println "  <Format>      - Date Format to use when parsing Expire Date";
    println "  Perform the social cleanup of expired posts using the specified date";
    println "";
    return null;
}

def params = null;
if (args.length < 2)
{
    help();
}
else
{
    try
    {
        def date = args[1];
        def format = "MM-dd-yyyy";
        if (args.length >= 3)
        {
            format = args[2];
        }
        def sdf = new SimpleDateFormat(format);
        def expireDate = sdf.parse(date);
        sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss.SSS");

        println "This will expire all posts older than " + sdf.format(expireDate);
        println "Do you wish to continue? (Y/N)";
        LOG.info("Checking Expire Post Date " + sdf.format(expireDate));
        br = new BufferedReader(new InputStreamReader(System.in));
        def answer = br.readLine();
        LOG.info("Answer: " + answer);
        if (answer.equalsIgnoreCase("Y"))
        {
            println "Sending Expire Message";
            params = ["EXPIRE_TIMESTAMP":expireDate.getTime()];
        }
        else
        {
            println "No Expire Message Send";
        }
    }
    catch (Exception e)
    {
        println "Unable to Send Expire Posts Message: " + e.getMessage();
        LOG.error("Unable to Send Expire Posts Message", e);
        params = null;
    }
}

return params;
