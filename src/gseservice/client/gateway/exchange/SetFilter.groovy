def help()
{
    println "\nUsage: <name> <active> <order> <interval> <runbook> <criteria>"
    println ""
    println "This will set the filter on the connected Exchange Server Gateway."
    println "Ensure the correct gateway connection is established before"
    println "issuing the command."
    println ""
}
if (args.length != 10)
{
    help();
}
else
{
    def id = args[1]            // name
    def active = args[2]        // active
    def order = args[3]         // order
    def interval = args[4]      // interval
    def runbook = args[5]       // runbook
    def subjectKey = args[6]           // subjectKey
    def contentKey = args[7]           // contentKey
    def sendernamae = args[8]           // sendername
    def senderaddress = args[9]           // senderaddress

    def criteria = new HashMap();
    
    if(id != null && !id.equals(""))
    {
        if(subjectKey != null && !subjectKey.equals(""))
        {
            criteria.put(id+".subject", subjectKey);
        }
        else if(contentKey != null && !contentKey.equals(""))
        {
            criteria.put(id+".content", contentKey);
        }
        else if(sendernamae != null && !sendernamae.equals(""))
        {
            criteria.put(id+".sendername", sendernamae);
        }
        else if(senderaddress != null && !senderaddress.equals(""))
        {
            criteria.put(id+".senderaddress", senderaddress);
        }
    }

    def params = ["ID":id, "ACTIVE":active, "ORDER":order, "INTERVAL":interval, "RUNBOOK":runbook, "CRITERIA":criteria];
    
    MAIN.setMethod("MExchange.setFilter");
    println "Setting Exchange Server filter at the Exchange Server Gateway"

    return params;
}
