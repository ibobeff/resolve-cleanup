def help()
{
    println "\nUsage: removeFilter <name>"
    println ""
    println "This will remove the filter on the connected Exchange Server Gateway."
    println "Ensure the correct gateway connection is established before"
    println "issuing the command."
    println ""
}
if (args.length != 2)
{
    help();
}
else
{
    def id = args[1]            // name

	MAIN.setMethod("MExchange.removeFilter");
    println "Removing the Exchange Server filter at the Exchange Server Gateway"

    return id;
}
