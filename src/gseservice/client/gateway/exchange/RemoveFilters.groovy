def help()
{
    println "\nUsage: removeFilters <name1> <name2> ..."
    println ""
    println "This will remove the filter on the connected Exchange Server Gateway."
    println "Ensure the correct gateway connection is established before"
    println "issuing the command."
    println ""
}
if (args.length < 2)
{
    help();
}
else
{
    def params = args.toList()
    params.remove(0);                     // remove command

	MAIN.setMethod("MExchange.removeFilters");
    println "Removing the Exchange Server filters at the Exchange Server Gateway"

    return params;
}
