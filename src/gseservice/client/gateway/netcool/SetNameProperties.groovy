def help()
{
    println "\nUsage: <name> <name1=value1> <name2=value2> ..."
    println ""
    println "This will set the name properties with the specified map values."
    println ""
}
if (args.length < 3)
{
    help();
}
else
{
    def name = args[1]            // name
    
    def params = ["NAME_PROPERTIES":name]
    
    for (i in 2 .. args.length-1)
    {
        def nameval = args[i].split("=");
        params[nameval[0].trim()] = nameval[1].trim()
    }

    
    MAIN.setMethod("MNetcool.setNameProperties");
    println "Setting Netcool Gateway Name Properties"

    return params;
}
