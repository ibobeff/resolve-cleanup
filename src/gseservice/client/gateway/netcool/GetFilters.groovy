def help()
{
    println "\nUsage: getFilters";
    println ""
    println "This will retrieve the list of gateways from the connected"
    println "Netcool Gateway. The results will be populated into the DB"
    println "";
}

def params = [:]

    
MAIN.setMethod("MNetcool.getFilters");
println "Loading Netcool filters into database"

return params;