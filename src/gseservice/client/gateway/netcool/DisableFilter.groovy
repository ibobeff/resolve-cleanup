def help()
{
    println "\nUsage: disableFilter <name>"
    println ""
    println "This will enable the filter on the connected Netcool Gateway."
    println "Ensure the correct gateway connection is established before"
    println "issuing the command."
    println ""
}
if (args.length != 2)
{
    help();
}
else
{
    def id = args[1]            // name

    def params = ["ID":id, "ACTIVE":"false"];
    
    MAIN.setMethod("MNetcool.setFilterActive");
    println "Disabling Netcool filter at the Netcool Gateway"

    return params;
}
