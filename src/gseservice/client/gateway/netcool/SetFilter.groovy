def help()
{
    println "\nUsage: <name> <active> <order> <interval> <runbook> <sql>"
    println ""
    println "This will set the filter on the connected Netcool Gateway."
    println "Ensure the correct gateway connection is established before"
    println "issuing the command."
    println ""
}
if (args.length != 7)
{
    help();
}
else
{
    def id = args[1]            // name
    def active = args[2]        // active
    def order = args[3]         // order
    def interval = args[4]      // interval
    def runbook = args[5]       // runbook
    def sql = args[6]           // sql

    def params = ["ID":id, "ACTIVE":active, "ORDER":order, "INTERVAL":interval, "RUNBOOK":runbook, "SQL":sql];
    
    MAIN.setMethod("MNetcool.setFilter");
    println "Setting Netcool filter at the Netcool Gateway"

    return params;
}
