import com.resolve.rscontrol.MAction;
import com.resolve.sql.SQLRecord;
import com.resolve.util.Constants;
import org.apache.commons.lang.StringUtils;

// get target_sid
String target_sid = (String)HEADER.get("TARGETID");

// delay END processing
String wait = PARAMS.get("END_WAIT")
if (wait != null && !wait.equals("") && !wait.equals("0"))
{
    sleep(1000 * Long.parseLong(wait));
}
else
{
    sleep(1000);
}

// set process status
String status = "COMPLETED";
MAction.updateProcessRequestStatus(DB, PROCESS.processid, status);

// set summary and description
String summary = "Process completed";
String description = "Process completed";

// set condition and severity
String condition = "good";
String severity = "good";

String type = PARAMS.get("END_TYPE");
if (!StringUtils.isEmpty(type))
{
    if (type.equalsIgnoreCase("BEST"))
    {
        SQLRecord row = new SQLRecord(DB, Constants.GLIDE_RESOLVE_ACTION_RESULT);
        row.query("u_process", PROCESS.processid);
        while (row.next())
        {
            String u_condition = row.getString("u_condition");
            String u_severity = row.getString("u_severity");
            
            if (condition == null || severity == null)
            {
                condition = u_condition;
                severity = u_severity;
            }
            else
            {
	            if (MAction.conditionIsGT(u_condition, condition))
	            {
	                condition = u_condition;
	            }
	            
	            if (MAction.severityIsGT(u_severity, severity))
	            {
	                severity = u_severity;
	            }
            }
        }
    }
    else if (type.equalsIgnoreCase("WORST"))
    {
        SQLRecord row = new SQLRecord(DB, Constants.GLIDE_RESOLVE_ACTION_RESULT);
        row.query("u_process", PROCESS.processid);
        while (row.next())
        {
            String u_condition = row.getString("u_condition");
            String u_severity = row.getString("u_severity");
            
            if (condition == null || severity == null)
            {
                condition = u_condition;
                severity = u_severity;
            }
            else
            {
	            if (MAction.conditionIsLT(u_condition, condition))
	            {
	                condition = u_condition;
	            }
	            
	            if (MAction.severityIsLT(u_severity, severity))
	            {
	                severity = u_severity;
	            }
            }
        }
    }
}

String content = "";
content += "COMPLETED=true\n";
content += "CONDITION="+condition+"\n";
content += "SEVERITY="+severity+"\n";
content += "SUMMARY="+summary+"\n";
content += "DETAIL="+description+"\n";
content += "DURATION="+STATE.getTotalDuration()+"\n";

// remove process
def quit = PARAMS.get("END_QUIT");
if (quit == null || quit == "" || quit == "true" || quit == "TRUE")
{
    LOG.debug("End Process - process removed: "+PROCESS.processid);
    MAction.removeProcess(PROCESS.processid);
                            
	// Set RSCONTROL as TARGET for final end process task
	HEADER.put("TARGET", "RSCONTROL");
}
else
{
    LOG.debug("End Subprocess - process NOT removed: "+PROCESS.processid);
}

return content;
