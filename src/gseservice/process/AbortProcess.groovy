import com.resolve.rscontrol.MAction;

String summary = PARAMS.get("SUMMARY");
String description = PARAMS.get("DESCRIPTION");
String condition = PARAMS.get("CONDITION");
String severity = PARAMS.get("SEVERITY");

String content = "";
content += "COMPLETED=false\n";
content += "CONDITION=bad\n";
content += "SEVERITY=critical\n";
content += "SUMMARY=Process Aborted\n";
content += "DETAIL=Process Aborted\n";

return content;
