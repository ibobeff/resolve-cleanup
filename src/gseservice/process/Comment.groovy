import com.resolve.rscontrol.MAction;

String summary = PARAMS.get("SUMMARY");
String description = PARAMS.get("DESCRIPTION");
String condition = PARAMS.get("CONDITION");
String severity = PARAMS.get("SEVERITY");

String content = "";
content += "COMPLETED=true\n";
content += "CONDITION="+condition+"\n";
content += "SEVERITY="+severity+"\n";
content += "SUMMARY="+summary+"\n";
content += "DETAIL="+description+"\n";

return content;
