import com.resolve.rscontrol.MAction;
import com.resolve.util.StringUtils;
import com.resolve.util.Constants;
import com.resolve.html.Table;

String condition = PARAMS.get("CONDITION");
String severity = PARAMS.get("SEVERITY");
String summary = PARAMS.get("SUMMARY");

if (!condition)
{
    condition = "good";
}
if (!severity)
{
    severity = "good";
}
if (!summary)
{
    summary = "ActionForm";
}

String content = "";
content += "COMPLETED:=true\n";
content += "CONDITION:="+condition+"\n";
content += "SEVERITY:="+severity+"\n";
content += "SUMMARY:="+summary+"\n";
content += "PARAMSTR:="+PARAMS.get("PARAMSTR")+"\n";

return content;
