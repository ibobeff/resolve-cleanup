@echo off
::The argument is the customer's name whom the license file is issued to.
if "%1"=="" GOTO END
"java" -cp "lib\commons-io.jar;lib\commons-lang.jar;lib\commons-lang3.jar;lib\json-lib.jar;lib\resolve-license.jar;" com.resolve.license.Keygen %1 %2 %3 %4 %5 %6 %7 %8 %9 %10
exit /B 0
:END
echo Usage: keygen.bat -customer "customer name"
echo Optional parameters:
echo      [-ip comma separated IP address list, no in between space]
echo      [-exp expiration date in YYYYMMDD format]
echo      [-serial serial no]
echo      [-type evaluation or commercial]
echo
echo Example: keygen.bat -customer "Acme, Inc." -ip "10.10.10.100,10.20.3.23" -exp 20121231 -type evaluation
exit /B 1

