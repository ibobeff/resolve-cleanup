/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.license;

import com.resolve.util.Log;

public class KeygenTest
{
    public static void main(String args[]) throws Exception
    {
        Log.init();
/*
        boolean result = false;

        File resolveHome = new File("c:/temp/resolveLicTest");
        if(!resolveHome.exists())
        {
            FileUtils.forceMkdir(resolveHome);
        }

        File licenseFile = new File(resolveHome + "/license.lic");
        File startdateFile = new File(resolveHome + "/tm");

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String datestring = formatter.format(date);

        String customer = "cust_no_ip_no_exp_date";
        System.out.printf("Use case: " + customer + " >>>>>>>>>>>>>>>>\n");
        FileUtils.deleteDirectory(new File(customer));
        FileUtils.deleteQuietly(licenseFile);
        Keygen.generateLicenseFile(new String[]{"customer="+customer}, "C:/project/resolve3/src/license/keygen.cfg");
        FileUtils.moveFile(new File(customer + "/license" + datestring + ".lic"), licenseFile);
        boolean isEvaluation = false;
        result = LicenseUtil.checkLicense(resolveHome.getAbsolutePath(), startdateFile);
        isEvaluation = LicenseUtil.isEvaluation();
        System.out.printf("Use case: " + customer + ", is validity passed? %b, is evaluation passed %b\n\n", result, !isEvaluation);
        result = false;
        isEvaluation = Boolean.FALSE;

        customer = "cust_with_ip_no_exp_date";
        System.out.printf("Use case: " + customer + " >>>>>>>>>>>>>>>>\n");
        FileUtils.deleteDirectory(new File(customer));
        FileUtils.deleteQuietly(licenseFile);
        Keygen.generateLicenseFile(new String[]{"customer="+customer,"ip=10.20.2.113"}, "C:/project/resolve3/src/license/keygen.cfg");
        //FileUtils.copyFile(new File(customer + "/license" + datestring + ".lic"), licenseFile);
        FileUtils.moveFile(new File(customer + "/license" + datestring + ".lic"), licenseFile);
        result = LicenseUtil.checkLicense(resolveHome.getAbsolutePath(), startdateFile);
        isEvaluation = LicenseUtil.isEvaluation();
        System.out.printf("Use case: " + customer + ", is validity passed? %b, is evaluation passed %b\n\n", result, !isEvaluation);
        result = false;
        isEvaluation = Boolean.FALSE;

        customer = "cust_no_ip_with_exp_date_not_expired";
        System.out.printf("Use case: " + customer + " >>>>>>>>>>>>>>>>\n");
        FileUtils.deleteDirectory(new File(customer));
        FileUtils.deleteQuietly(licenseFile);
        Keygen.generateLicenseFile(new String[]{"customer="+customer,"exp=20121231"}, "C:/project/resolve3/src/license/keygen.cfg");
        FileUtils.moveFile(new File(customer + "/license" + datestring + ".lic"), licenseFile);
        result = LicenseUtil.checkLicense(resolveHome.getAbsolutePath(), startdateFile);
        isEvaluation = LicenseUtil.isEvaluation();
        System.out.printf("Use case: " + customer + ", is validity passed? %b, is evaluation passed? %b\n\n", result, isEvaluation);
        result = false;
        isEvaluation = Boolean.FALSE;

        customer = "cust_with_ip_with_exp_date_not_expired";
        System.out.printf("Use case: " + customer + " >>>>>>>>>>>>>>>>\n");
        FileUtils.deleteDirectory(new File(customer));
        FileUtils.deleteQuietly(licenseFile);
        Keygen.generateLicenseFile(new String[]{"customer="+customer,"ip=10.20.2.113", "exp=20121231"}, "C:/project/resolve3/src/license/keygen.cfg");
        FileUtils.moveFile(new File(customer + "/license" + datestring + ".lic"), licenseFile);
        result = LicenseUtil.checkLicense(resolveHome.getAbsolutePath(), startdateFile);
        isEvaluation = LicenseUtil.isEvaluation();
        System.out.printf("Use case: " + customer + ", is validity passed? %b, is evaluation passed? %b\n\n", result, isEvaluation);
        result = false;
        isEvaluation = Boolean.FALSE;

        customer = "cust_no_ip_with_exp_date_expired";
        System.out.printf("Use case: " + customer + " >>>>>>>>>>>>>>>>\n");
        try
        {
            FileUtils.deleteDirectory(new File(customer));
            FileUtils.deleteQuietly(licenseFile);
            Keygen.generateLicenseFile(new String[]{"customer="+customer, "exp=20120101"}, "C:/project/resolve3/src/license/keygen.cfg");
            FileUtils.moveFile(new File(customer + "/license" + datestring + ".lic"), licenseFile);
            result = LicenseUtil.checkLicense(resolveHome.getAbsolutePath(), startdateFile);
            isEvaluation = LicenseUtil.isEvaluation();
            System.out.printf("Use case: " + customer + ", is validity passed? %b, is evaluation passed? %b\n\n", result, !isEvaluation);
            result = false;
            isEvaluation = Boolean.FALSE;
        }
        catch (Exception e)
        {
            System.out.println("Use case: " + customer + ", expired license!");
        }

        customer = "cust_with_ip_with_exp_date_expired";
        System.out.printf("Use case: " + customer + " >>>>>>>>>>>>>>>>\n");
        try
        {
            FileUtils.deleteDirectory(new File(customer));
            FileUtils.deleteQuietly(licenseFile);
            Keygen.generateLicenseFile(new String[]{"customer="+customer, "ip=10.20.2.113", "exp=20120101"}, "C:/project/resolve3/src/license/keygen.cfg");
            FileUtils.moveFile(new File(customer + "/license" + datestring + ".lic"), licenseFile);
            //result = LicenseUtil.checkLicense(resolveHome.getAbsolutePath(), startdateFile);
            isEvaluation = LicenseUtil.isEvaluation();
            System.out.printf("Use case: " + customer + ", is validity passed? %b, is evaluation passed? %b\n\n", result, !isEvaluation);
            result = false;
            isEvaluation = Boolean.FALSE;
        }
        catch (Exception e)
        {
            System.out.println("Use case: " + customer + ", expired license!");
        }

        customer = "cust_with_bad_ip_no_exp_date";
        System.out.printf("Use case: " + customer + " >>>>>>>>>>>>>>>>\n");
        try
        {
            FileUtils.deleteDirectory(new File(customer));
            FileUtils.deleteQuietly(licenseFile);
            Keygen.generateLicenseFile(new String[]{"customer="+customer, "ip=10.10.2.10"}, "C:/project/resolve3/src/license/keygen.cfg");
            FileUtils.moveFile(new File(customer + "/license" + datestring + ".lic"), licenseFile);
            result = LicenseUtil.checkLicense(resolveHome.getAbsolutePath(), startdateFile);
            isEvaluation = LicenseUtil.isEvaluation();
            System.out.printf("Use case: " + customer + ", is validity passed? %b, is evaluation passed? %b\n\n", result, !isEvaluation);
            result = false;
            isEvaluation = Boolean.FALSE;
        }
        catch (Exception e)
        {
            System.out.println("Use case: " + customer + ", expired license!");
        }
*/
    } // main

} // KeygenTest
