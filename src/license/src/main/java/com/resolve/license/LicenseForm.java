/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.license;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;

import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;

import org.apache.commons.io.FileUtils;

import com.resolve.service.License;
import com.resolve.util.DateUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.LicenseEnum;
import com.resolve.util.LicenseInterval;
import com.resolve.util.StringUtils;

/**
 * This is the user data entry form to generate Resolve license.
 *
 */
public class LicenseForm extends javax.swing.JFrame
{
    private static final long serialVersionUID = -3706456122546263167L;

    //private final static String KEYGENCFGFILE_CE = "keygen-ce.cfg";
    private final static String KEYGENCFGFILE_EVAL = "keygen-eval.cfg";
    //private final static String KEYGENCFGFILE_COM = "keygen-com.cfg";
    private final static String KEYGENCFGFILE_STD = "keygen-std.cfg";
    private final static String KEYGENCFGFILE_ENT = "keygen-ent.cfg";
    private final static String KEYGENCFGFILE_V2 = "keygen-v2.cfg";
    private static final String HYPHEN = "-";
    private static final String BLANK = "";

    Properties config = new Properties();

    // license types
    //private final static String[] SUPPORTED_LICENSE_TYPE = new String[] { Constants.LICENSE_TYPE_STANDARD, Constants.LICENSE_TYPE_COMMUNITY, Constants.LICENSE_TYPE_EVALUATION, Constants.LICENSE_TYPE_COM , Constants.LICENSE_TYPE_ENTERPRISE };
    private final static String[] SUPPORTED_LICENSE_TYPE = new String[] {LicenseEnum.LICENSE_TYPE_STANDARD.getKeygenName(), 
    																	 LicenseEnum.LICENSE_TYPE_EVALUATION.getKeygenName(), 
    																	 LicenseEnum.LICENSE_TYPE_ENTERPRISE.getKeygenName(),
    																	 LicenseEnum.LICENSE_TYPE_V2.getKeygenName()};

    private final static String[] SUPPORTED_ENV_TYPE = new String[] {LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName(), 
			 														 LicenseEnum.LICENSE_ENV_NON_PROD.getKeygenName()};
    
    // Variables declaration - do not modify
    private JLabel logo;

    private JSeparator titleSeparator;

    //private javax.swing.JLabel lblTitle;

    private JLabel lblMessage;

    private JLabel lblCustomerName;
    private JTextField txtCustomerName;

    private JLabel lblLicenseType;
    @SuppressWarnings("rawtypes")
	private JComboBox cbLicenseType;
    
    //private javax.swing.JLabel lblSerialNo;
    //private javax.swing.JTextField txtSerialNo;

    private JLabel lblNoOfCore;
    private JTextField txtNoOfCore;

    private JLabel lblMaxMemoryRSCONTROL;
    private JTextField txtMaxMemoryRSCONTROL;

    private JLabel lblMaxMemoryRSVIEW;
    private JTextField txtMaxMemoryRSVIEW;

    private JLabel lblIPAddress;
    private JTextField txtIPAddress;

    private JLabel lblHA;
    private JCheckBox cbHA;

    private JLabel lblDuration;
    private javax.swing.JTextField txtDuration;

    private JLabel lblExpirationDate;
    private javax.swing.JTextField txtExpirationDate;

    private JLabel lblEndUserCount;
    private JTextField txtEndUserCount;

    private JLabel lblGateway;
    private JTextField txtGateway;

    private JLabel lblLicenseText;
    private JScrollPane spLicenseText;
    private JTextArea taLicenseText;

    private JLabel lblLicenseFile;
    private JTextField txtLicenseFile;

    //private javax.swing.JFileChooser fcLicenseFile;

    private JButton btnGenerate;
    private JButton btnClose;
    private JButton btnDownloadLicense;

    private JLabel lblLicenseEnv;
    @SuppressWarnings("rawtypes")
	private JComboBox cbLicenseEnv;
    
    // Usage Interval Components
    
    private JLabel lblIntervalFlds;    
    private JLabel lblIntervalStartDate;
    private JTextField txtIntervalStartDate;    
    private JLabel lblIntervalEndDate;
    private JTextField txtIntervalEndDate;    
    private JLabel lblIntervalIncidentCount;
    private JTextField txtIntervalIncidentCount;    
    private JLabel lblIntervalEventCount;
    private JTextField txtIntervalEventCount;    
    private JButton btnAddUsageInterval;
    private JLabel lblIntervals;
    private DefaultComboBoxModel<String> cbIntervalModel;
    private JComboBox<String> cbIntervals;
    private JScrollPane intervalsScrollPane;
    private JButton btnDeleteUsageInterval;
    
    // Event Sources Components
    
    private JLabel lblEventSourceFlds;
    private JLabel lblEventSourceName;
    private JTextField txtEventSourceName;
    private JButton btnAddEventSource;
    private JLabel lblEventSources;
    private DefaultComboBoxModel<String> cbEventSourceModel;
    private JComboBox<String> cbEventSources;
    private JScrollPane eventsourcesScrollPane;
    private JButton btnDeleteEventSource;
    
    // End of variables declaration

    /**
     * Creates new form License
     */
    public LicenseForm()
    {
        createComponents();
        loadConfig();
    }

    private void loadConfig()
    {
        loadConfig(KEYGENCFGFILE_STD);
    }

    private void loadConfig(String configFile)
    {
        // get keygen.cfg file
        File cfgfile = new File(configFile);
        if (cfgfile.exists())
        {
            try
            {
                FileInputStream fis = new FileInputStream(cfgfile);
                config.load(fis);
                fis.close();
                //initialize the components based on this file
                initComponents();
            }
            catch (Exception e)
            {
                //Could not load config file but it's not really necessary.
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     * @throws IOException
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	private void createComponents()
    {
        logo = new JLabel();
        logo.setText("Resolve License");
        logo.setFont(new java.awt.Font("Tahoma", 0, 45)); // NOI18N
        logo.setText("   Resolve License Generator v6.3");
        logo.setBackground(new java.awt.Color(255, 255, 255));
        logo.setForeground(Color.DARK_GRAY);
        try
        {
            URL rsrcUrl = Thread.currentThread().getContextClassLoader().getResource("logo.png");
            BufferedImage wp = ImageIO.read(rsrcUrl);
            logo.setIcon(new ImageIcon(wp));
        }
        catch (Exception e)
        {
            //Didn't find the icon, ignore.
        }
        titleSeparator = new JSeparator();

        //lblTitle = new javax.swing.JLabel();
        lblMessage = new JLabel();

        lblLicenseType = new JLabel();
        cbLicenseType = new JComboBox();

        lblLicenseEnv = new JLabel();
        cbLicenseEnv = new JComboBox();
        cbLicenseEnv.setEditable(true);
        
        lblCustomerName = new JLabel();
        txtCustomerName = new JTextField();

        lblNoOfCore = new JLabel();
        txtNoOfCore = new JTextField();

        lblMaxMemoryRSCONTROL = new JLabel();
        txtMaxMemoryRSCONTROL = new JTextField();

        lblMaxMemoryRSVIEW = new JLabel();
        txtMaxMemoryRSVIEW = new JTextField();

        lblIPAddress = new JLabel();
        txtIPAddress = new JTextField();

        lblHA = new JLabel();
        cbHA = new JCheckBox();
        cbHA.setSelected(Boolean.FALSE);

        lblDuration = new JLabel();
        txtDuration = new JTextField();

        lblExpirationDate = new JLabel();
        txtExpirationDate = new JTextField();

        lblEndUserCount = new JLabel();
        txtEndUserCount = new JTextField();

        lblGateway = new JLabel();
        txtGateway = new JTextField();

        lblLicenseText = new JLabel();
        taLicenseText = new JTextArea();
        spLicenseText = new JScrollPane(taLicenseText);
        lblLicenseFile = new JLabel();
        txtLicenseFile = new JTextField();
        btnGenerate = new JButton();
        btnClose = new JButton();
        btnDownloadLicense = new JButton();
        
        // Create Usage Interval Components
        
        lblIntervalFlds = new JLabel();        
        lblIntervalStartDate = new JLabel();
        txtIntervalStartDate = new JTextField();        
        lblIntervalEndDate = new JLabel();
        txtIntervalEndDate = new JTextField();        
        lblIntervalIncidentCount = new JLabel();
        txtIntervalIncidentCount = new JTextField();        
        lblIntervalEventCount = new JLabel();
        txtIntervalEventCount = new JTextField();
        btnAddUsageInterval = new JButton();
        lblIntervals = new JLabel();
        cbIntervalModel = new DefaultComboBoxModel();
        cbIntervals = new JComboBox(cbIntervalModel);        
        intervalsScrollPane = new JScrollPane(cbIntervals, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
        									  JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        btnDeleteUsageInterval = new JButton();
        
        // Create Event Source Components
        
        lblEventSourceFlds = new JLabel();
        lblEventSourceName = new JLabel();
        txtEventSourceName =  new JTextField();
        btnAddEventSource = new JButton();
        lblEventSources = new JLabel();
        cbEventSourceModel = new DefaultComboBoxModel();
        cbEventSources = new JComboBox(cbEventSourceModel);
        eventsourcesScrollPane = new JScrollPane(cbEventSources, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				  								 JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        btnDeleteEventSource = new JButton();
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lblMessage.setForeground(Color.RED);
        lblMessage.setText("Enter data and click the \"Generate\" button to create license.");

        lblLicenseType.setText("License Type:");
        cbLicenseType.setModel(new DefaultComboBoxModel(SUPPORTED_LICENSE_TYPE));
//        Object cbLicenseTypePopUp = cbLicenseType.getUI().getAccessibleChild(cbLicenseType, 0);
//        if (cbLicenseTypePopUp instanceof ComboPopup) {
//        	JList LicenseTypePopUpList = ((ComboPopup)cbLicenseTypePopUp).getList();
//        	LicenseTypePopUpList.setVisibleRowCount(SUPPORTED_LICENSE_TYPE.length);
//        }
        cbLicenseType.addActionListener(new ComboListener());
        if (StringUtils.isNotBlank(config.getProperty(LicenseEnum.LICENSE_TYPE.getKeygenName())))
        {
            cbLicenseType.setSelectedItem(config.getProperty(LicenseEnum.LICENSE_TYPE.getKeygenName()).toUpperCase());
        }
        
        lblLicenseEnv.setText("License Environment*:");
        cbLicenseEnv.setModel(new javax.swing.DefaultComboBoxModel(SUPPORTED_ENV_TYPE));
        cbLicenseEnv.addActionListener(new LicenseEnvComboListener());
        if (StringUtils.isNotBlank(config.getProperty(LicenseEnum.LICENSE_ENV.getKeygenName())))
        {
            cbLicenseEnv.setSelectedItem(config.getProperty(LicenseEnum.LICENSE_ENV.getKeygenName()).toUpperCase());
        }

        lblCustomerName.setText("Customer Name*:");
        txtCustomerName.setText(config.getProperty("CUSTOMER")); //initially load from config

        lblNoOfCore.setText("Number of Core(s):");

        lblMaxMemoryRSCONTROL.setText("Maximum Memory (mb) RSCONTROL:");

        lblMaxMemoryRSVIEW.setText("Maximum Memory (mb) RSVIEW:");

        lblIPAddress.setText("IP Address(comma separated) :");

        lblHA.setText("Is Highly Available (HA):");
        
        lblDuration.setText("Duration (days):");

        lblExpirationDate.setText("Expiration Date (yyyymmdd):");

        lblEndUserCount.setText("Maximum No. of End Users:");

        //lblAdminUserCount.setText("Maximum No. of Admin Users:");

        lblGateway.setText("Gateway/Connector (format: CODE#NO_OF_INSTANCE example EMAIL#1,REMEDYX#2) :");

        lblLicenseText.setText("License Key:");

        //taLicenseText.setColumns(5);
        //taLicenseText.setRows(20);
        taLicenseText.setEditable(false);
        taLicenseText.setLineWrap(true);
        taLicenseText.setPreferredSize(new Dimension(100, 75));
        spLicenseText.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        spLicenseText.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        spLicenseText.setPreferredSize(new Dimension(100, 75));
        spLicenseText.setViewportView(taLicenseText);

        lblLicenseFile.setText("Generated License File:");
        txtLicenseFile.setEditable(false);

        btnGenerate.setText("Generate");
        btnGenerate.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                generateButtonClicked(evt);
            }
        });

        btnClose.setText("Close");
        btnClose.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                 closeButtonClicked(evt);
            }
        });

        btnDownloadLicense.setText("Download License File");
        btnDownloadLicense.setEnabled(false);

        // Setup Usage Interval Components
        
        lblIntervalFlds.setText("Usage Interval Fields:");        
        lblIntervalStartDate.setText("Interval Start Date (yyyymmdd):");
        lblIntervalEndDate.setText("Interval End Date (yyyymmdd):");
        lblIntervalIncidentCount.setText("Max No. of Incidents (Default is unlimited):");
        txtIntervalIncidentCount.setText(LicenseInterval.UNLIMITED_INCIDENT_COUNT.toString());
        lblIntervalEventCount.setText("Max No. of Events (Default is unlimited):");
        txtIntervalEventCount.setText(LicenseInterval.UNLIMITED_EVENT_COUNT.toString());
        btnAddUsageInterval.setText("Add");
        btnAddUsageInterval.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                addUsageIntervalButtonClicked(evt);
            }
        });
        lblIntervals.setText("Usage Intervals:");
        // Experimental code to always display Intervals ComboBox list
//        Object cbIntervalsPopUp = cbIntervals.getUI().getAccessibleChild(cbIntervals, 0);
//        if (cbIntervalsPopUp instanceof ComboPopup) {
//        	JList cbIntervalsPopUpList = ((ComboPopup)cbIntervalsPopUp).getList();
//        	cbIntervalsPopUpList.setVisibleRowCount(2);
//        }
        btnDeleteUsageInterval.setText("Delete");
        btnDeleteUsageInterval.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                deleteUsageIntervalButtonClicked(evt);
            }
        });
        
        // Setup Event Source Component
        
        lblEventSourceFlds.setText("Event Source Fields:");
        lblEventSourceName.setText("Event Source Name:");
        btnAddEventSource.setText("Add");
        btnAddEventSource.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                addEventSourceButtonClicked(evt);
            }
        });
        lblEventSources.setText("Event Sources:");
//        Object cbEventSourcesPopUp = cbEventSources.getUI().getAccessibleChild(cbEventSources, 0);
//      if (cbEventSourcesPopUp instanceof ComboPopup) {
//      	JList cbEventSourcesPopUpList = ((ComboPopup)cbEventSourcesPopUp).getList();
//      	cbEventSourcesPopUpList.setVisibleRowCount(2);
//      }
        btnDeleteEventSource.setText("Delete");
        btnDeleteEventSource.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                deleteEventSourceButtonClicked(evt);
            }
        });
        
        // Layout all components
        
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
        	layout.createParallelGroup(
        		GroupLayout.Alignment.LEADING)
        		.addGroup(
        		    layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(
                        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(logo)
                        .addComponent(titleSeparator)
                        .addComponent(lblMessage)
                        .addGroup(
                            layout.createSequentialGroup()
                            .addGroup(
                                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(lblCustomerName).addComponent(lblIPAddress).addComponent(lblLicenseType)
                                .addComponent(lblLicenseEnv).addComponent(lblNoOfCore).addComponent(lblMaxMemoryRSCONTROL)
                                .addComponent(lblMaxMemoryRSVIEW).addComponent(lblEndUserCount).addComponent(lblGateway)
                                .addComponent(lblHA).addComponent(lblDuration).addComponent(lblExpirationDate)
                                .addComponent(lblLicenseText).addComponent(lblLicenseFile).addComponent(lblIntervalFlds)
                                .addComponent(lblIntervalStartDate).addComponent(lblIntervalEndDate)
                                .addComponent(lblIntervalIncidentCount).addComponent(lblIntervalEventCount)
                                .addComponent(lblIntervals).addComponent(lblEventSourceFlds)
                                .addComponent(lblEventSourceName).addComponent(lblEventSources))
                            .addGap(37, 37, 37)
                            .addGroup(
                                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(txtCustomerName).addComponent(txtIPAddress).addComponent(spLicenseText)
                                .addComponent(txtNoOfCore).addComponent(txtMaxMemoryRSCONTROL)
                                .addComponent(txtMaxMemoryRSVIEW).addComponent(txtEndUserCount).addComponent(txtGateway)
                                .addGroup(
                                    layout.createSequentialGroup()
                                    .addGroup(
                                        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(cbHA).addComponent(txtDuration).addComponent(txtExpirationDate)
                                        .addComponent(cbLicenseType).addComponent(cbLicenseEnv).addComponent(txtLicenseFile)
                                        .addComponent(txtIntervalStartDate).addComponent(txtIntervalEndDate)
                                        .addComponent(txtIntervalIncidentCount).addComponent(txtIntervalEventCount)
                                        .addGroup(layout.createSequentialGroup().addComponent(btnAddUsageInterval))
                                        .addComponent(intervalsScrollPane)
                                        .addGroup(layout.createSequentialGroup().addComponent(btnDeleteUsageInterval))
                                        .addComponent(txtEventSourceName)
                                        .addGroup(layout.createSequentialGroup().addComponent(btnAddEventSource))
                                        .addComponent(eventsourcesScrollPane)
                                        .addGroup(layout.createSequentialGroup().addComponent(btnDeleteEventSource))
                                        .addGroup(
                                            layout.createSequentialGroup()
                                            .addComponent(btnGenerate).addGap(18, 18, 18)
                                            .addComponent(btnClose)))
                                    .addGap(55, 441, Short.MAX_VALUE)))))
                    .addContainerGap()));
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(
                layout.createSequentialGroup()
                .addContainerGap().addComponent(logo).addGap(15, 15, 15).addComponent(titleSeparator).addGap(15, 15, 15)
                .addComponent(lblMessage, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGap(15, 15, 15)
                .addGroup(
                    layout.createParallelGroup(
                        GroupLayout.Alignment.LEADING).addComponent(cbLicenseType).addComponent(lblLicenseType))
                .addGap(18, 18, 18)
                .addGroup(
                    layout.createParallelGroup(
                        GroupLayout.Alignment.LEADING).addComponent(cbLicenseEnv).addComponent(lblLicenseEnv))
                .addGap(18, 18, 18)
                .addGroup(
                    layout.createParallelGroup(
                        GroupLayout.Alignment.LEADING).addComponent(lblCustomerName).addComponent(txtCustomerName))
                .addGap(18, 18, 18)
                .addGroup(
                    layout.createParallelGroup(
                        GroupLayout.Alignment.LEADING).addComponent(lblNoOfCore).addComponent(txtNoOfCore))
                .addGap(18, 18, 18)
                .addGroup(
                    layout.createParallelGroup(
                        GroupLayout.Alignment.LEADING).addComponent(lblMaxMemoryRSCONTROL)
                        .addComponent(txtMaxMemoryRSCONTROL))
                .addGap(18, 18, 18)
                .addGroup(
                    layout.createParallelGroup(
                        GroupLayout.Alignment.LEADING).addComponent(lblMaxMemoryRSVIEW).addComponent(txtMaxMemoryRSVIEW))
                .addGap(18, 18, 18)
                .addGroup(
                    layout.createParallelGroup(
                        GroupLayout.Alignment.LEADING).addComponent(lblEndUserCount).addComponent(txtEndUserCount))
                .addGap(18, 18, 18)
                .addGroup(
                    layout.createParallelGroup(
                        GroupLayout.Alignment.LEADING).addComponent(lblGateway).addComponent(txtGateway))
                .addGap(18, 18, 18)
                .addGroup(
                    layout.createParallelGroup(
                        GroupLayout.Alignment.LEADING).addComponent(lblIPAddress).addComponent(txtIPAddress))
                .addGap(18, 18, 18)
                .addGroup(
                    layout.createParallelGroup(
                        GroupLayout.Alignment.LEADING).addComponent(lblHA).addComponent(cbHA))
                .addGap(18, 18, 18)
                .addGroup(
                    layout.createParallelGroup(
                        GroupLayout.Alignment.LEADING).addComponent(lblDuration).addComponent(txtDuration))
                .addGap(18, 18, 18)
                .addGroup(
                    layout.createParallelGroup(
                        GroupLayout.Alignment.LEADING).addComponent(lblExpirationDate).addComponent(txtExpirationDate))
                .addGap(10, 10, 10)
                .addGroup(
                    layout.createParallelGroup(
                        GroupLayout.Alignment.LEADING).addComponent(lblIntervalFlds))
                .addGap(10, 10, 10)
                .addGroup(
                    layout.createParallelGroup(
                        GroupLayout.Alignment.LEADING).addComponent(lblIntervalStartDate).addComponent(txtIntervalStartDate))
                .addGap(5, 5, 5)
                .addGroup(
                        layout.createParallelGroup(
                            GroupLayout.Alignment.LEADING).addComponent(lblIntervalEndDate).addComponent(txtIntervalEndDate))
                .addGap(5, 5, 5)
                .addGroup(
                        layout.createParallelGroup(
                            GroupLayout.Alignment.LEADING).addComponent(lblIntervalIncidentCount)
                            .addComponent(txtIntervalIncidentCount))
                .addGap(5, 5, 5)
                .addGroup(
                        layout.createParallelGroup(
                            GroupLayout.Alignment.LEADING).addComponent(lblIntervalEventCount)
                            .addComponent(txtIntervalEventCount))
                .addGap(5, 5, 5)
                .addGroup(
                	layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(btnAddUsageInterval))
                .addGap(5, 5, 5)
                .addGroup(
                        layout.createParallelGroup(
                            GroupLayout.Alignment.LEADING).addComponent(lblIntervals).addComponent(intervalsScrollPane))
                .addGap(5, 5, 5)
                .addGroup(
                	layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(btnDeleteUsageInterval))
                .addGap(10, 10, 10)
                .addGroup(
                    layout.createParallelGroup(
                        GroupLayout.Alignment.LEADING).addComponent(lblEventSourceFlds))
                .addGap(10, 10, 10)
                .addGroup(
                        layout.createParallelGroup(
                            GroupLayout.Alignment.LEADING).addComponent(lblEventSourceName).addComponent(txtEventSourceName))
                .addGap(5, 5, 5)
                .addGroup(
                    	layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(btnAddEventSource))
                .addGap(5, 5, 5)
                .addGroup(
                        layout.createParallelGroup(
                            GroupLayout.Alignment.LEADING).addComponent(lblEventSources)
                        	.addComponent(eventsourcesScrollPane))
                .addGap(5, 5, 5)
                .addGroup(
                    	layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(btnDeleteEventSource))
                .addGap(18, 18, 18)
                .addGroup(
                    layout.createParallelGroup(
                        GroupLayout.Alignment.LEADING).addComponent(lblLicenseText).addComponent(spLicenseText))
                .addGap(18, 18, 18)
                .addGroup(
                    layout.createParallelGroup(
                        GroupLayout.Alignment.LEADING).addComponent(lblLicenseFile).addComponent(txtLicenseFile))
                .addGap(18, 18, 18)
                .addGroup(
                    layout.createParallelGroup(
                        GroupLayout.Alignment.BASELINE).addComponent(btnGenerate).addComponent(btnClose))
                .addContainerGap(39, Short.MAX_VALUE)));
        pack();
    }

    /**
     * Reads a config file and initialize the values.
     */
    private void initComponents()
    {
        txtCustomerName.setText(config.getProperty(LicenseEnum.CUSTOMER.getKeygenName())); //initially load from config
        if (StringUtils.isNotBlank(config.getProperty(LicenseEnum.LICENSE_TYPE.getKeygenName())))
        {
            cbLicenseType.setSelectedItem(config.getProperty(LicenseEnum.LICENSE_TYPE.getKeygenName()).toUpperCase());
        }
        if (StringUtils.isNotBlank(config.getProperty(LicenseEnum.LICENSE_ENV.getKeygenName())))
        {
            cbLicenseEnv.setSelectedItem(config.getProperty(LicenseEnum.LICENSE_ENV.getKeygenName()).toUpperCase());
        }
        txtNoOfCore.setText(config.getProperty(LicenseEnum.NO_OF_CORE.getKeygenName())); //initially load from config
        txtMaxMemoryRSCONTROL.setText(config.getProperty(LicenseEnum.MAX_MEMORY_RSCONTROL.getKeygenName())); //initially load from config
        txtMaxMemoryRSVIEW.setText(config.getProperty(LicenseEnum.MAX_MEMORY_RSVIEW.getKeygenName())); //initially load from config
        txtIPAddress.setText(config.getProperty(LicenseEnum.IP_ADDRESS.getKeygenName())); //initially load from config
        cbHA.setSelected(Boolean.valueOf(config.getProperty(LicenseEnum.HA.getKeygenName()))); //initially load from config
        txtDuration.setText(config.getProperty(LicenseEnum.DURATION.getKeygenName())); //initially load from config
        txtExpirationDate.setText(config.getProperty(LicenseEnum.EXPIRATION_DATE.getKeygenName())); //initially load from config
        txtEndUserCount.setText(config.getProperty(LicenseEnum.END_USER_COUNT.getKeygenName())); //initially load from config
        txtGateway.setText(config.getProperty(LicenseEnum.GATEWAY.getKeygenName())); //initially load from config
    }

    private void generateButtonClicked(java.awt.event.ActionEvent evt)
    {
        //initialize to the original foreground color.
    	lblLicenseEnv.setForeground(Color.BLACK);
        lblCustomerName.setForeground(Color.BLACK);
        lblIPAddress.setForeground(Color.BLACK);
        lblExpirationDate.setForeground(Color.BLACK);

        lblNoOfCore.setForeground(Color.BLACK);
        lblMaxMemoryRSCONTROL.setForeground(Color.BLACK);
        lblMaxMemoryRSVIEW.setForeground(Color.BLACK);
        lblEndUserCount.setForeground(Color.BLACK);
        lblHA.setForeground(Color.BLACK);
        lblDuration.setForeground(Color.BLACK);

        taLicenseText.setText("");
        txtLicenseFile.setText("");

        String customerName = txtCustomerName.getText();
        String licenseType = cbLicenseType.getSelectedItem().toString();
        String licenseEnv = cbLicenseEnv.getSelectedItem().toString();
        String noOfCore = txtNoOfCore.getText();
        String maxMemoryRSCONTROL = txtMaxMemoryRSCONTROL.getText();
        String maxMemoryRSVIEW = txtMaxMemoryRSVIEW.getText();
        String endUserCount = txtEndUserCount.getText();
        String ha = Boolean.toString(cbHA.isSelected());
        String duration = txtDuration.getText();
        String gateway = txtGateway.getText();
        String intervals = null;
        String expirationDate = null;
        String eventSources = null;

        boolean isInputValid = true;
        
        if (StringUtils.isNotBlank(licenseType) && StringUtils.isNotBlank(licenseEnv) &&
        	licenseType.equals(LicenseEnum.LICENSE_TYPE_EVALUATION.getKeygenName()) && 
        	licenseEnv.equals(LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName())) {
        	lblLicenseEnv.setForeground(Color.RED);
        	isInputValid = false;
        } else {
        	lblLicenseEnv.setForeground(Color.BLACK);
        }
        
        if (StringUtils.isEmpty(customerName))
        {
            isInputValid = false;
            lblCustomerName.setForeground(Color.RED);
        } else {
        	lblCustomerName.setForeground(Color.BLACK);
        }

        String ipAddresses = txtIPAddress.getText();
        if (!Util.validateIP(ipAddresses))
        {
            isInputValid = false;
            lblIPAddress.setForeground(Color.RED);
        } else {
        	lblIPAddress.setForeground(Color.BLACK);
        }

        if (licenseType.equals(LicenseEnum.LICENSE_TYPE_V2.getKeygenName()) && 
        	cbIntervalModel.getSize() > 0) {
        	StringBuilder intervalsSb = new StringBuilder();
        	
        	for (int i = 0; i < cbIntervalModel.getSize(); i++) {
        		if (intervalsSb.length() > 0) {
        			intervalsSb.append(License.VALUES_SEPARATOR);
        		}
        		intervalsSb.append(cbIntervalModel.getElementAt(i));
        	}
        	
        	intervals = intervalsSb.toString();
        	
        	Properties liIntrvlsProp = new Properties();
        	liIntrvlsProp.put(LicenseEnum.INTERVALS.getKeygenName(), intervals);
        	SortedSet<LicenseInterval> liIntrvls = License.processLicenseIntervals(liIntrvlsProp);
        	
        	if (!Util.validateLicenseIntervals(liIntrvls)) {
        		isInputValid = false;
        		lblIntervals.setForeground(Color.RED);
        	} else {
        		lblIntervals.setForeground(Color.BLACK);
        		Long liBasedExpirationDate = License.getLicenseIntervalBasedExpirationDate(liIntrvls);
        		
        		if (liBasedExpirationDate != null && liBasedExpirationDate.longValue() > 0) {
        			expirationDate = DateUtils.convertDateToStringGMT(GMTDate.getDate(liBasedExpirationDate), 
        							 					   		      LicenseEnum.DATE_FORMAT.getKeygenName());
        		}
        	}
        }
        
        if (StringUtils.isBlank(expirationDate)) {
        	expirationDate = txtExpirationDate.getText();
        }
        
        if (!Util.validateDate(expirationDate, licenseType))
        {
            isInputValid = false;
            lblExpirationDate.setForeground(Color.RED);
        } else {
        	lblExpirationDate.setForeground(Color.BLACK);
        }

        if (!Util.validateNumber(noOfCore))
        {
            isInputValid = false;
            lblNoOfCore.setForeground(Color.RED);
        } else {
        	lblNoOfCore.setForeground(Color.BLACK);
        }

        if (!Util.validateNumber(maxMemoryRSCONTROL))
        {
            isInputValid = false;
            lblMaxMemoryRSCONTROL.setForeground(Color.RED);
        } else {
        	lblMaxMemoryRSCONTROL.setForeground(Color.BLACK);
        }
        
        if (!Util.validateNumber(maxMemoryRSVIEW))
        {
            isInputValid = false;
            lblMaxMemoryRSVIEW.setForeground(Color.RED);
        } else {
        	lblMaxMemoryRSVIEW.setForeground(Color.BLACK);
        }

        if (!Util.validateNumber(endUserCount))
        {
            isInputValid = false;
            lblEndUserCount.setForeground(Color.RED);
        } else {
        	lblEndUserCount.setForeground(Color.BLACK);
        }

        if (!Util.validateNumber(duration))
        {
            isInputValid = false;
            lblDuration.setForeground(Color.RED);
        } else {
        	lblDuration.setForeground(Color.BLACK);
        }

        boolean isGatewayInputValid = true;
        if (StringUtils.isNotBlank(gateway))
        {
            //make sure the string is well formatted e.g., EMAIL#1, NETCOOL#1
            String[] gateways = gateway.split(",");
            for(String license : gateways)
            {
                String[] license1 = license.trim().split("#");
                //2 newest, 3 old disregard duration
                if(license1.length == 2 || license1.length == 3)
                {
                    String gatewayCode = license1[0];
                    String instanceCount = license1[1];
                    if(!StringUtils.isBlank(gatewayCode) && !StringUtils.isBlank(instanceCount))
                    {
                        try
                        {
                            Integer.parseInt(instanceCount);
                        }
                        catch(NumberFormatException e)
                        {
                            isInputValid = false;
                            isGatewayInputValid = false;
                            break;
                        }
                    }
                    else
                    {
                        isInputValid = false;
                        isGatewayInputValid = false;
                        break;
                    }
                }
                else
                {
                    isInputValid = false;
                    isGatewayInputValid = false;
                    break;
                }
            }

            if(!isGatewayInputValid)
            {
                lblGateway.setForeground(Color.RED);
            } else {
            	lblGateway.setForeground(Color.BLACK);
            }
        }
        
        if (licenseType.equals(LicenseEnum.LICENSE_TYPE_V2.getKeygenName()) && 
            cbEventSourceModel.getSize() > 0) {
        	StringBuilder eventsourcesSb = new StringBuilder();
        	
        	for (int i = 0; i < cbEventSourceModel.getSize(); i++) {
        		if (eventsourcesSb.length() > 0) {
        			eventsourcesSb.append(License.VALUES_SEPARATOR);
        		}
        		eventsourcesSb.append(cbEventSourceModel.getElementAt(i));
        	}
        	
        	eventSources = eventsourcesSb.toString();
        	
        	Properties liEventSourcesProp = new Properties();
        	liEventSourcesProp.put(LicenseEnum.EVENTSOURCES.getKeygenName(), eventSources);
        	Set<String> liEventSources = License.processEventSources(liEventSourcesProp);
        	
        	if (!Util.validateEventSources(liEventSources)) {
        		isInputValid = false;
        		lblEventSources.setForeground(Color.RED);
        	} else {
        		lblEventSources.setForeground(Color.BLACK);
        	}
        }
        
        if (isInputValid)
        {
        	System.out.printf("Customer=%s, Type=%s, ipAddress=%s, expirationDate=%s, Core=%s, Max Memory RSCONTROL=%s, " +
  				  			  "Max Memory RSVIEW=%s, Named user count=%s, HA=%s, Duration=%s, Gateway=%s, Envirnment=%s, "+
  				  			  "Usage Intervals=%s, Event Sources=%s\n", 
  				  			  customerName, licenseType, ipAddresses, expirationDate, noOfCore, maxMemoryRSCONTROL, 
  				  			  maxMemoryRSVIEW, endUserCount, ha, duration, gateway, licenseEnv, 
  				  			  (StringUtils.isNotBlank(intervals) ? intervals : ""),
  				  			  (StringUtils.isNotBlank(eventSources) ? eventSources : ""));
        	
            //call generate license
            String[] params = new String[30];
            int i = 0;
            params[i++] = HYPHEN + LicenseEnum.CUSTOMER.getParamName();
            params[i++] = customerName;
            params[i++] = HYPHEN + LicenseEnum.LICENSE_TYPE.getParamName();
            params[i++] = licenseType;
            params[i++] = HYPHEN + LicenseEnum.LICENSE_ENV.getParamName();
            params[i++] = licenseEnv;

            if (StringUtils.isNotBlank(ipAddresses))
            {
                params[i++] = HYPHEN + LicenseEnum.IP_ADDRESS.getParamName();
                params[i++] = ipAddresses;
            }
            if (StringUtils.isNotBlank(expirationDate))
            {
                params[i++] = HYPHEN + LicenseEnum.EXPIRATION_DATE.getParamName();
                params[i++] = expirationDate;
            }
            if (StringUtils.isNotBlank(noOfCore))
            {
                params[i++] = HYPHEN + LicenseEnum.NO_OF_CORE.getParamName();
                params[i++] = noOfCore;
            }
            if (StringUtils.isNotBlank(maxMemoryRSCONTROL))
            {
                params[i++] = HYPHEN + LicenseEnum.MAX_MEMORY_RSCONTROL.getParamName();
                params[i++] = maxMemoryRSCONTROL;
            }
            if (StringUtils.isNotBlank(maxMemoryRSVIEW))
            {
                params[i++] = HYPHEN + LicenseEnum.MAX_MEMORY_RSVIEW.getParamName();
                params[i++] = maxMemoryRSVIEW;
            }
            if (StringUtils.isNotBlank(endUserCount))
            {
                params[i++] = HYPHEN + LicenseEnum.END_USER_COUNT.getParamName();
                params[i++] = endUserCount;
            }
            if (StringUtils.isNotBlank(gateway))
            {
                params[i++] = HYPHEN + LicenseEnum.GATEWAY.getParamName();
                params[i++] = gateway;
            }
            if (StringUtils.isNotBlank(ha))
            {
                params[i++] = HYPHEN + LicenseEnum.HA.getParamName();
                params[i++] = ha;
            }
            if (StringUtils.isNotBlank(duration))
            {
                params[i++] = HYPHEN + LicenseEnum.DURATION.getParamName();
                params[i++] = duration;
            }

            // Set Usage Intervals option
            if (StringUtils.isNotBlank(intervals)) {
            	params[i++] = HYPHEN + LicenseEnum.INTERVALS.getParamName();
            	params[i++] = intervals;
            }
            
            // Set Event Sources option
            if (StringUtils.isNotBlank(eventSources)) {
            	params[i++] = HYPHEN + LicenseEnum.EVENTSOURCES.getParamName();
            	params[i++] = eventSources;
            }
            
            try
            {
                String licenseFile = Keygen.generateLicenseFile(params);
                if (StringUtils.isNotBlank(licenseFile))
                {
                    lblMessage.setForeground(Color.GREEN);
                    lblMessage.setText("License created succesfully");

                    File licFile = new File(licenseFile);
                    taLicenseText.setText(FileUtils.readFileToString(licFile));
                    txtLicenseFile.setText(licFile.getAbsolutePath());
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            lblMessage.setText("ERROR: Invalid input, please check.");
        }

    }

    private void closeButtonClicked(java.awt.event.ActionEvent evt)
    {
        System.exit(0);
    }

    private void addUsageIntervalButtonClicked(ActionEvent evt) {
    	
    	String licenseType = cbLicenseType.getSelectedItem().toString();
    	
    	if (!licenseType.equals(LicenseEnum.LICENSE_TYPE_V2.getKeygenName())) {
    		return;
    	}
    	
    	//initialize to the original foreground color.
    	lblIntervalStartDate.setForeground(Color.BLACK);
    	lblIntervalEndDate.setForeground(Color.BLACK);
    	lblIntervalIncidentCount.setForeground(Color.BLACK);
    	lblIntervalEventCount.setForeground(Color.BLACK);
    	
    	boolean isInputValid = true;
    	
    	String intrvlStartDateStr = txtIntervalStartDate.getText();
    	
        if (!Util.validateDate(intrvlStartDateStr, licenseType)) {
            isInputValid = false;
            lblIntervalStartDate.setForeground(Color.RED);
        } else {
        	lblIntervalStartDate.setForeground(Color.BLACK);
        }
        
        String intrvlEndDateStr = txtIntervalEndDate.getText();
        
        if (!Util.validateDate(intrvlEndDateStr, licenseType)) {
            isInputValid = false;
            lblIntervalEndDate.setForeground(Color.RED);
        } else {
        	lblIntervalEndDate.setForeground(Color.BLACK);
        }
        
        String intrvlIncidentCountStr = txtIntervalIncidentCount.getText();
        
        if (!Util.validateNumber(intrvlIncidentCountStr)) {
        	isInputValid = false;
        	lblIntervalIncidentCount.setForeground(Color.BLACK);
        } else {
        	lblIntervalIncidentCount.setForeground(Color.BLACK);
        }
        
        String intrvlEventCountStr = txtIntervalEventCount.getText();
        
        if (!Util.validateNumber(intrvlEventCountStr)) {
        	isInputValid = false;
        	lblIntervalEventCount.setForeground(Color.BLACK);
        } else {
        	lblIntervalEventCount.setForeground(Color.BLACK);
        }
        
        if (isInputValid) {
        	String licenseIntrvlStr = intrvlStartDateStr.trim() + License.FIELDS_SEPARATOR + intrvlEndDateStr.trim() + 
        							  License.FIELDS_SEPARATOR + intrvlIncidentCountStr.trim() + License.FIELDS_SEPARATOR +
        							  intrvlEventCountStr.trim();
        	
        	// Validate single license interval - start date < end date, start date not perpetual
        	
        	Properties liIntrvlProp = new Properties();
        	liIntrvlProp.put(LicenseEnum.INTERVALS.getKeygenName(), licenseIntrvlStr);
        	SortedSet<LicenseInterval> liIntrvls = License.processLicenseIntervals(liIntrvlProp);
        	
        	if (!Util.validateLicenseIntervals(liIntrvls)) {
        		isInputValid = false;
        		lblIntervalStartDate.setForeground(Color.RED);
        		lblIntervalEndDate.setForeground(Color.RED);
        	} else {
        		// Check if license interval is not already defined
        		boolean addUsageInterval = true;
        		if (cbIntervalModel.getSize() > 0) {
    	        	StringBuilder intervalsSb = new StringBuilder();
    	        	
    	        	for (int i = 0; i < cbIntervalModel.getSize(); i++) {
    	        		if (intervalsSb.length() > 0) {
    	        			intervalsSb.append(License.VALUES_SEPARATOR);
    	        		}
    	        		intervalsSb.append(cbIntervalModel.getElementAt(i));
    	        	}
    	        	
    	        	String currentIntervals = intervalsSb.toString();
    	        	
    	        	Properties liIntrvlsProp = new Properties();
    	        	liIntrvlsProp.put(LicenseEnum.INTERVALS.getKeygenName(), currentIntervals);
    	        	SortedSet<LicenseInterval> currIntrvls = License.processLicenseIntervals(liIntrvlsProp);
    	        	
    	        	if (currIntrvls.contains(liIntrvls.first())) {
    	        		isInputValid = false;
    	        		lblIntervalStartDate.setForeground(Color.RED);
    	        		lblIntervalEndDate.setForeground(Color.RED);
    	        		addUsageInterval = false;
    	        	}
        		}
        		
        		if (addUsageInterval) {
	        		lblIntervalStartDate.setForeground(Color.BLACK);
	        		lblIntervalEndDate.setForeground(Color.BLACK);
	        		lblIntervals.setForeground(Color.BLACK);
	        		System.out.printf("Adding License Usage Interval [%s].\n", licenseIntrvlStr);
	        		cbIntervalModel.addElement(licenseIntrvlStr);
	//        		cbIntervals.showPopup();
	        		setVisible(true);
        		}
        	}
        }
    }
    
    private void addEventSourceButtonClicked(ActionEvent evt) {
    	String licenseType = cbLicenseType.getSelectedItem().toString();
    	
    	if (!licenseType.equals(LicenseEnum.LICENSE_TYPE_V2.getKeygenName())) {
    		return;
    	}
    	
    	//initialize to the original foreground color.
    	lblEventSourceName.setForeground(Color.BLACK);
    	
    	boolean isInputValid = true;
    	
    	String eventSourceNameStr = txtEventSourceName.getText();
    	
        if (!Util.validateEventSource(eventSourceNameStr)) {
            isInputValid = false;
            lblEventSourceName.setForeground(Color.RED);
        } else {
        	lblEventSourceName.setForeground(Color.BLACK);
        }
        
        if (isInputValid) {
	        boolean addEventSource = true;
			if (cbEventSourceModel.getSize() > 0) {
	        	
	        	for (int i = 0; i < cbEventSourceModel.getSize(); i++) {
	        		if (eventSourceNameStr.equalsIgnoreCase(cbEventSourceModel.getElementAt(i))) {
	        			isInputValid = false;
	        			lblEventSourceName.setForeground(Color.RED);
	        			addEventSource = false;
	        			break;
	        		}
	        	}
	        }
			
			if (addEventSource) {
				lblEventSourceName.setForeground(Color.BLACK);
	    		lblEventSources.setForeground(Color.BLACK);
	    		System.out.printf("Adding License Event Source [%s].\n", eventSourceNameStr);
	    		cbEventSourceModel.addElement(eventSourceNameStr);
	    		// Experimental to keep list of event sources ComboBox list always displayed
//	        		cbEventSources.showPopup();
	    		setVisible(true);
			}
        }
    }
    
    private void deleteUsageIntervalButtonClicked(ActionEvent evt) {
    	
    	int selectedIndex = cbIntervals.getSelectedIndex();
    	
    	if (selectedIndex < 0) {
    		return;
    	}
    	
    	String licenseType = cbLicenseType.getSelectedItem().toString();
    	
    	if (!licenseType.equals(LicenseEnum.LICENSE_TYPE_V2.getKeygenName())) {
    		return;
    	}
    	
    	cbIntervalModel.removeElementAt(selectedIndex);
    	
    	setVisible(true);
    }
    
    private void deleteEventSourceButtonClicked(ActionEvent evt) {
    	
    	int selectedIndex = cbEventSources.getSelectedIndex();
    	
    	if (selectedIndex < 0) {
    		return;
    	}
    	
    	String licenseType = cbLicenseType.getSelectedItem().toString();
    	
    	if (!licenseType.equals(LicenseEnum.LICENSE_TYPE_V2.getKeygenName())) {
    		return;
    	}
    	
    	cbEventSourceModel.removeElementAt(selectedIndex);
    	
    	setVisible(true);
    }
    
    /**
     * @param args
     *            the command line arguments
     */
    public static void main(String args[])
    {
        /*
         * Set the Nimbus look and feel
         */
        // <editor-fold defaultstate="collapsed"
        // desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase
         * /tutorial/uiswing/lookandfeel/plaf.html
         */
    	// Initialize license time zone to UTC/GMT/UT
    	GMTDate.initTimezoneSOP("UTC");
    				
        try
        {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch (Exception ex)
        {
            System.out.println("Error running the license manager. Actual error: " + ex.getMessage());
        }

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable()
        {

            public void run()
            {
            	LicenseForm lf = new LicenseForm();
            	
            	JScrollPane contentPane = new JScrollPane(lf.getContentPane(), JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
				   		  								  JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            	
            	lf.setContentPane(contentPane);
                lf.setVisible(true);
            }
        });
    }

    private class ComboListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            @SuppressWarnings("rawtypes")
			String selectedItem = (String) ((JComboBox) e.getSource()).getSelectedItem();
            // Experimental to always display license type ComboBox list
//            cbLicenseType.showPopup();
            if (LicenseEnum.LICENSE_TYPE_EVALUATION.getKeygenName().equalsIgnoreCase(selectedItem))
            {
                loadConfig(KEYGENCFGFILE_EVAL);
            }
            else if (LicenseEnum.LICENSE_TYPE_STANDARD.getKeygenName().equalsIgnoreCase(selectedItem))
            {
                loadConfig(KEYGENCFGFILE_STD);
            }
            else if (LicenseEnum.LICENSE_TYPE_ENTERPRISE.getKeygenName().equalsIgnoreCase(selectedItem))
            {
                loadConfig(KEYGENCFGFILE_ENT);
            } else if (LicenseEnum.LICENSE_TYPE_V2.getKeygenName().equalsIgnoreCase(selectedItem)) {
            	loadConfig(KEYGENCFGFILE_V2);
            }
            
            /*
             * Enable/disable usage interval and event source fields based on the selected license type and 
             * existing state of fields
             */
            
            boolean usgIntrvlAndEvntSrcFldsEnabled = 
            			txtIntervalStartDate.isEnabled() || txtIntervalEndDate.isEnabled() || 
            			txtIntervalIncidentCount.isEnabled() || txtIntervalEventCount.isEnabled() ||
            			btnAddUsageInterval.isEnabled() || btnDeleteUsageInterval.isEnabled() ||
            			txtEventSourceName.isEnabled() || btnAddEventSource.isEnabled() ||
            			btnDeleteEventSource.isEnabled();
            
            if ((LicenseEnum.LICENSE_TYPE_V2.getKeygenName().equalsIgnoreCase(selectedItem) && 
            	 !usgIntrvlAndEvntSrcFldsEnabled) ||
            	(!LicenseEnum.LICENSE_TYPE_V2.getKeygenName().equalsIgnoreCase(selectedItem) && 
            	 usgIntrvlAndEvntSrcFldsEnabled)) {
            	
            	boolean enableUsageIntrvlAndEventSrcFlds = false;
            	
            	if (LicenseEnum.LICENSE_TYPE_V2.getKeygenName().equalsIgnoreCase(selectedItem) && 
            		!usgIntrvlAndEvntSrcFldsEnabled) {
            		enableUsageIntrvlAndEventSrcFlds = true;
            	}
            	
            	lblIntervalStartDate.setForeground(Color.BLACK);
            	txtIntervalStartDate.setText(BLANK);
            	txtIntervalStartDate.setEnabled(enableUsageIntrvlAndEventSrcFlds);
            	
            	lblIntervalEndDate.setForeground(Color.BLACK);
            	txtIntervalEndDate.setText(BLANK);
            	txtIntervalEndDate.setEnabled(enableUsageIntrvlAndEventSrcFlds);
            	
            	lblIntervalIncidentCount.setForeground(Color.BLACK);
            	txtIntervalIncidentCount.setText(LicenseInterval.UNLIMITED_INCIDENT_COUNT.toString());
            	txtIntervalIncidentCount.setEnabled(enableUsageIntrvlAndEventSrcFlds);
            	
            	lblIntervalEventCount.setForeground(Color.BLACK);
            	txtIntervalEventCount.setText(LicenseInterval.UNLIMITED_EVENT_COUNT.toString());
            	txtIntervalEventCount.setEnabled(enableUsageIntrvlAndEventSrcFlds);
            	
            	btnAddUsageInterval.setEnabled(enableUsageIntrvlAndEventSrcFlds);
            	btnDeleteUsageInterval.setEnabled(enableUsageIntrvlAndEventSrcFlds);
            	
            	lblIntervals.setForeground(Color.BLACK);
            	cbIntervalModel.removeAllElements();
            	cbIntervals.setEnabled(enableUsageIntrvlAndEventSrcFlds);
            	// Experimental to always display usage intervals ComboBox list
//            	cbIntervals.setPopupVisible(enableUsageIntrvlAndEventSrcFlds);
            	
            	lblEventSourceName.setForeground(Color.BLACK);
            	txtEventSourceName.setText(BLANK);
            	txtEventSourceName.setEnabled(enableUsageIntrvlAndEventSrcFlds);
            	
            	btnAddEventSource.setEnabled(enableUsageIntrvlAndEventSrcFlds);
            	btnDeleteEventSource.setEnabled(enableUsageIntrvlAndEventSrcFlds);
            	
            	lblEventSources.setForeground(Color.BLACK);
            	cbEventSourceModel.removeAllElements();
            	cbEventSources.setEnabled(enableUsageIntrvlAndEventSrcFlds);
            	// Experimental to always display event sources ComboBox list
//            	cbEventSources.setPopupVisible(enableUsageIntrvlAndEventSrcFlds);
            	
            	setVisible(true);
            }
        }
    }
    
    @SuppressWarnings("unused")
	private class LicenseEnvComboListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent ae) {
			// TODO Auto-generated method stub
			@SuppressWarnings("rawtypes")
			String selectdItem = (String) ((JComboBox) ae.getSource()).getSelectedItem();
		}
    }
}
