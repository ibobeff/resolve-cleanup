/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.license;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.SortedSet;

import org.apache.commons.collections.CollectionUtils;

import com.resolve.util.LicenseEnum;
import com.resolve.util.LicenseInterval;
import com.resolve.util.StringUtils;

public class Util
{
	public static final String SPACE = " ";
	
    public static boolean validateIP(String ipAddress)
    {
        boolean result = true;

        if (!StringUtils.isEmpty(ipAddress))
        {
            String[] ips = ipAddress.split(",");
            for (String ip : ips)
            {
                String[] parts = ip.trim().split("\\.");
                if (parts.length != 4)
                {
                    result = false;
                    break;
                }

                for (String s : parts)
                {
                    try
                    {
                        int i = Integer.parseInt(s);
                        if (i < 0 || i > 255)
                        {
                            result = false;
                            break;
                        }
                    }
                    catch (NumberFormatException e)
                    {
                        result = false;
                        break;
                    }
                }
            }
        }
        return result;
    }

    public static boolean validateDate(String date, String licenseType)
    {
        boolean result = true;

        if (StringUtils.isNotBlank(date))
        {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            Date testDate = null;
            try
            {
                testDate = sdf.parse(date);
            }
            catch (ParseException e)
            {
            	result = false;
            }
            
            if (result && !sdf.format(testDate).equals(date))
            {
                result = false;
            }
        } else {
        	if (licenseType.equals(LicenseEnum.LICENSE_TYPE_V2.getKeygenName())) {
        		result = false;
        	}
        }
        return result;
    }

    public static boolean validateNumber(String number)
    {
        boolean result = true;

        if (!StringUtils.isEmpty(number))
        {
            try
            {
                Integer.parseInt(number);
            }
            catch (NumberFormatException e)
            {
                result = false;
            }
        }
        
        return result;
    }
    
    public static boolean validateNumberForLicenseType(String number, String licenseType)
    {
        boolean result = true;

        if (!StringUtils.isEmpty(number))
        {
            try
            {
                int duration = Integer.parseInt(number);
                
                if (licenseType.equals(LicenseEnum.LICENSE_TYPE_EVALUATION.getKeygenName()) && duration <= 0) {
                	result = false;
                }
            }
            catch (NumberFormatException e)
            {
                result = false;
            }
        } else {
        	if (licenseType.equals(LicenseEnum.LICENSE_TYPE_EVALUATION.getKeygenName())) {
            	result = false;
            }
        }
        
        return result;
    }
    
    public static boolean validateBoolean(String bool)
    {
        boolean result = false;

        if (StringUtils.isNotBlank(bool))
        {
            result = Boolean.parseBoolean(bool);
        }
        return result;
    }
    
    public static boolean validateLicenseIntervals(SortedSet<LicenseInterval> sortedLicenseIntervals) {
    	if (sortedLicenseIntervals == null || sortedLicenseIntervals.isEmpty()) {
    		return false;
    	}
    	
    	Long prevIntrvlEndDate = null;
    	
    	for (LicenseInterval licIntrvl : sortedLicenseIntervals) {
    		Long currStartDate = licIntrvl.getStartDate();
    		if (prevIntrvlEndDate != null) {
    			if ( currStartDate != (prevIntrvlEndDate + 1L)) {
    				return false;
    			}
    		}
    		
    		prevIntrvlEndDate = licIntrvl.getEndDate();
    		
    		if (currStartDate < 0 || currStartDate >= LicenseInterval.INTERVAL_END_PERPETUAL_MILLISECONDS ||
    			currStartDate >= prevIntrvlEndDate) {
    			return false;
    		}
    	}
    	
    	return true;
    }
    
    public static boolean validateEventSource(String eventSource) {
    	return StringUtils.isNotBlank(eventSource) && (!eventSource.contains(SPACE));
    }
    
    public static boolean validateEventSources(Set<String> eventSources) {
    	if (CollectionUtils.isEmpty(eventSources)) {
    		return false;
    	}
    	
    	for (String eventSource : eventSources) {
    		if (!validateEventSource(eventSource)) {
    			return false;
    		}
    	}
    	
    	return true;
    }
}
