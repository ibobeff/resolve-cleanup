/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.license;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.security.Signature;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;

import com.resolve.service.License;
import com.resolve.util.CryptUtils;
import com.resolve.util.DateUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.Guid;
import com.resolve.util.LicenseEnum;
import com.resolve.util.LicenseInterval;
import com.resolve.util.StringUtils;

/**
 *  Generates the new license key to stdout and license.lic file. The properties of the license
 *  is defined by the keygen.cfg file or the passed in parameters.
 *
 */
public class Keygen
{
	private static final String LICENSEDIR = "licenses/";
	private static final String LICENSEFILE = "license.lic";
	private static final String KEYGENCFGFILE = "keygen.cfg";
	private static final String TMP = "tmp";
	private static final String ZERO = "0";
	private static final String BLANK = "";
	private static final String VERBOSE_INDICATOR = "v";
	private static final String HYPHEN = "-";
	private static final String TMP_TEXT_FILE_NAME = "tmp.txt";
	private static final String DATE_FORMAT_YYYYMMDD = "yyyyMMdd";
	private static final String PERIOD = ".";
	private static final String FORWARD_SLASH = "/";
	private static final String LICENSE_TYPE_PREFIX = "LICENSE_TYPE_";
	private static final String SEPERATOR = ",";
	
	private static boolean isVerbose = false;

	// Resolve's java.security.PrivateKey serialized object bytes (hex) string used by V2 licenses to sign the license
	private static final String rprivksobs = 
									"aced0005737200146a6176612e73656375726974792e4b6579526570bdf94fb3889aa5430200044c000" +
									"9616c676f726974686d7400124c6a6176612f6c616e672f537472696e673b5b0007656e636f64656474" +
									"00025b424c0006666f726d617471007e00014c00047479706574001b4c6a6176612f736563757269747" +
									"92f4b657952657024547970653b7870740003445341757200025b42acf317f8060854e0020000787000" +
									"00014f3082014b0201003082012c06072a8648ce3804013082011f02818100fd7f53811d75122952df4" +
									"a9c2eece4e7f611b7523cef4400c31e3f80b6512669455d402251fb593d8d58fabfc5f5ba30f6cb9b55" +
									"6cd7813b801d346ff26660b76b9950a5a49f9fe8047b1022c24fbba9d7feb7c61bf83b57e7c6a8a6150" +
									"f04fb83f6d3c51ec3023554135a169132f675f3ae2b61d72aeff22203199dd14801c70215009760508f" +
									"15230bccb292b982a2eb840bf0581cf502818100f7e1a085d69b3ddecbbcab5c36b857b97994afbbfa3" +
									"aea82f9574c0b3d0782675159578ebad4594fe67107108180b449167123e84c281613b7cf09328cc8a6" +
									"e13c167a8b547c8d28e0a3ae1e2bb3a675916ea37f0bfa213562f1fb627a01243bcca4f1bea8519089a" +
									"883dfe15ae59f06928b665e807b552564014c3bfecf492a041602144a791f586428a4717f8049aad858" +
									"e7820aef1cb5740006504b435323387e7200196a6176612e73656375726974792e4b657952657024547" +
									"9706500000000000000001200007872000e6a6176612e6c616e672e456e756d00000000000000001200" +
									"00787074000750524956415445";
	
	/**
	 *
	 * @param args
	 * @return the absolute file location.
	 *
	 * @throws Exception
	 */
	public static String generateLicenseFile(String args[]) throws Exception
	{
		return generateLicenseFile(args, KEYGENCFGFILE);
	}

	public static String generateLicenseFile(String args[], String configFile) throws Exception
	{
		File licenseDir = new File(LICENSEDIR);
		if (!licenseDir.exists() || !licenseDir.isDirectory())
		{
			licenseDir.mkdir();
		}

		// get keygen.cfg file
		File cfgfile = new File(configFile);
		if (!cfgfile.exists())
		{
			throw new Exception(String.format("Missing %s file.", configFile));
		}

		Properties cfg = new Properties();
		FileInputStream fis = new FileInputStream(cfgfile);
		cfg.load(fis);
		fis.close();

		//initially read all the values from the configuration file.
		String licenseKey = new Guid().toString();
		String createDate = "" + GMTDate.getTime();
		String licenseType = cfg.getProperty(LicenseEnum.LICENSE_TYPE.getKeygenName());
		String customerName = cfg.getProperty(LicenseEnum.CUSTOMER.getKeygenName());
		String ipAdresses = cfg.getProperty(LicenseEnum.IP_ADDRESS.getKeygenName());
		String expirationDate = cfg.getProperty(LicenseEnum.EXPIRATION_DATE.getKeygenName());

		String noOfCore = cfg.getProperty(LicenseEnum.NO_OF_CORE.getKeygenName());
		String maxMemoryRSCONTROL = cfg.getProperty(LicenseEnum.MAX_MEMORY_RSCONTROL.getKeygenName());
		String maxMemoryRSVIEW = cfg.getProperty(LicenseEnum.MAX_MEMORY_RSVIEW.getKeygenName());
		String endUserCount = cfg.getProperty(LicenseEnum.END_USER_COUNT.getKeygenName());
		String ha = cfg.getProperty(LicenseEnum.HA.getKeygenName());
		String duration = cfg.getProperty(LicenseEnum.DURATION.getKeygenName());
		String gateway = cfg.getProperty(LicenseEnum.GATEWAY.getKeygenName());		
		String licenseEnv = cfg.getProperty(LicenseEnum.LICENSE_ENV.getKeygenName());
		String intervals = cfg.getProperty(LicenseEnum.INTERVALS.getKeygenName());
		String eventSources = cfg.getProperty(LicenseEnum.EVENTSOURCES.getKeygenName());

		File newdir = new File(LICENSEDIR + TMP);
		if (args.length != 0)
		{
			Properties prop = new Properties();
			String propertyName = BLANK;
			for (String arg : args)
			{
				if (StringUtils.isNotBlank(arg))
				{
					if (arg.startsWith(HYPHEN))
					{
						if (StringUtils.isNotBlank(propertyName)) {
							prop.put(propertyName, BLANK);
						}
						propertyName = arg.substring(1);
					}
					else
					{
						prop.put(propertyName, arg);
						propertyName = BLANK;
					}
				}
			}
			
			if (StringUtils.isNotBlank(propertyName)) {
				prop.put(propertyName, BLANK);
			}

			//create a directory after given name from input
			customerName = prop.getProperty(LicenseEnum.CUSTOMER.getParamName());
			String dirstring = customerName;
			newdir = new File(LICENSEDIR + dirstring);

			ipAdresses = prop.getProperty(LicenseEnum.IP_ADDRESS.getParamName(), ipAdresses);
			expirationDate = prop.getProperty(LicenseEnum.EXPIRATION_DATE.getParamName(), expirationDate);
			licenseType = prop.getProperty(LicenseEnum.LICENSE_TYPE.getParamName());

			noOfCore = prop.getProperty(LicenseEnum.NO_OF_CORE.getParamName(), ZERO);
			maxMemoryRSCONTROL = prop.getProperty(LicenseEnum.MAX_MEMORY_RSCONTROL.getParamName(), ZERO);
			maxMemoryRSVIEW = prop.getProperty(LicenseEnum.MAX_MEMORY_RSVIEW.getParamName(), ZERO);
			endUserCount = prop.getProperty(LicenseEnum.END_USER_COUNT.getParamName(), ZERO);
			ha = prop.getProperty(LicenseEnum.HA.getParamName(), Boolean.toString(false).toUpperCase());
			duration = prop.getProperty(LicenseEnum.DURATION.getParamName(), ZERO);
			gateway = prop.getProperty(LicenseEnum.GATEWAY.getParamName(), BLANK);			
			licenseEnv = prop.getProperty(LicenseEnum.LICENSE_ENV.getParamName(), 
										  LicenseEnum.LICENSE_ENV_NON_PROD.getKeygenName());
			intervals = prop.getProperty(LicenseEnum.INTERVALS.getParamName(), BLANK);
			eventSources = prop.getProperty(LicenseEnum.EVENTSOURCES.getParamName(), BLANK);
			
			isVerbose = prop.containsKey(VERBOSE_INDICATOR);
			if (isVerbose)
			{
				System.out.printf("Verbose: Customer=%s, Type=%s, ipAddress=%s, expirationDate=%s, Core=%s, " +
								  "Max Memory RSCONTROL=%s, Max Memory RSVIEW=%s, End user count=%s, HA=%s, " +
								  "Duration=%s, Gateway=%s, Env=%s, Intervals=%s, eventSources=%s \n", customerName, licenseType, ipAdresses, expirationDate, 
								  noOfCore, maxMemoryRSCONTROL, maxMemoryRSVIEW, endUserCount, ha, duration, gateway, 
								  licenseEnv, intervals, eventSources);
			}

			validateData(customerName, ipAdresses, expirationDate, noOfCore, maxMemoryRSCONTROL, maxMemoryRSVIEW, 
						 endUserCount, ha, duration, licenseType);
			
		}
		
		// Set license type to 
		if (StringUtils.isBlank(licenseType)) {
			if (StringUtils.isBlank(customerName)) {
				licenseType = LicenseEnum.LICENSE_TYPE_STANDARD.getKeygenName();
			} else {
				licenseType = LicenseEnum.LICENSE_TYPE_EVALUATION.getKeygenName();
			}
		} else {
			licenseType = licenseType.toUpperCase();
		}
		
		// save keygen.cfg
		cfg.put(LicenseEnum.LICENSE_KEY.getKeygenName(), licenseKey);
		cfg.put(LicenseEnum.LICENSE_TYPE.getKeygenName(), licenseType);
		cfg.put(LicenseEnum.CREATE_DATE.getKeygenName(), createDate);
		cfg.put(LicenseEnum.CUSTOMER.getKeygenName(), customerName);
		cfg.put(LicenseEnum.IP_ADDRESS.getKeygenName(), ipAdresses);
		cfg.put(LicenseEnum.EXPIRATION_DATE.getKeygenName(), expirationDate);

		cfg.put(LicenseEnum.NO_OF_CORE.getKeygenName(), noOfCore);
		cfg.put(LicenseEnum.MAX_MEMORY_RSCONTROL.getKeygenName(), maxMemoryRSCONTROL);
		cfg.put(LicenseEnum.MAX_MEMORY_RSVIEW.getKeygenName(), maxMemoryRSVIEW);
		cfg.put(LicenseEnum.END_USER_COUNT.getKeygenName(), endUserCount);
		cfg.put(LicenseEnum.HA.getKeygenName(), ha);
		cfg.put(LicenseEnum.DURATION.getKeygenName(), duration);
		cfg.put(LicenseEnum.GATEWAY.getKeygenName(), gateway);		
		cfg.put(LicenseEnum.LICENSE_ENV.getKeygenName(), licenseEnv);
		cfg.put(LicenseEnum.INTERVALS.getKeygenName(), intervals);
		cfg.put(LicenseEnum.EVENTSOURCES.getKeygenName(), eventSources);
		
		validateLicenseType(cfg);
		
		if (!newdir.exists() || !newdir.isDirectory())
		{
			newdir.mkdir();
		}

		File envDir = new File(LICENSEDIR + customerName + FORWARD_SLASH + licenseEnv);

		if (!envDir.exists() || !envDir.isDirectory())
		{
			envDir.mkdir();
		}
		
		newdir = envDir;
		
		//save to the tmp file
		File tmpfile = new File(newdir, TMP_TEXT_FILE_NAME);
		if (!tmpfile.exists())
		{
			tmpfile.createNewFile();
		}
		cfg.store(new FileOutputStream(tmpfile), BLANK);

		// encrypt keygen.cfg to license.lic
		String keyvalue = FileUtils.readFileToString(tmpfile, StandardCharsets.UTF_8.name());
		
		if (isVerbose)
		{
			System.out.println("\nVerbose: License (Plain):\n\n");
			System.out.println(keyvalue);
		}
		
		String license = CryptUtils.encryptLicense(keyvalue);

		//append the time stamp to the license file
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_YYYYMMDD);
		Date date = GMTDate.getDate();
		String datestring = formatter.format(date);
		String newfilename = LICENSEFILE.replace(PERIOD, datestring + PERIOD);
		File licensefile = new File(newdir, newfilename);
		
		// For V2 license type create and append signature prefixed with SIGNATURE to generated license
		if (LicenseEnum.LICENSE_TYPE_V2 == LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + licenseType)) {
			license = signLicense(license);
		}
		
		FileUtils.writeStringToFile(licensefile, license, StandardCharsets.UTF_8.name());
		
		//Finally persist the latest serial number into the configuration file.
		cfg.put(LicenseEnum.LICENSE_KEY.getKeygenName(), BLANK);
		cfg.put(LicenseEnum.LICENSE_TYPE.getKeygenName(), BLANK);
		cfg.put(LicenseEnum.CUSTOMER.getKeygenName(), BLANK);
		cfg.put(LicenseEnum.IP_ADDRESS.getKeygenName(), BLANK);
		cfg.put(LicenseEnum.CREATE_DATE.getKeygenName(), BLANK);
		cfg.put(LicenseEnum.EXPIRATION_DATE.getKeygenName(), BLANK);
		cfg.put(LicenseEnum.NO_OF_CORE.getKeygenName(), BLANK);
		cfg.put(LicenseEnum.MAX_MEMORY_RSCONTROL.getKeygenName(), BLANK);
		cfg.put(LicenseEnum.MAX_MEMORY_RSVIEW.getKeygenName(), BLANK);
		cfg.put(LicenseEnum.END_USER_COUNT.getKeygenName(), BLANK);
		cfg.put(LicenseEnum.HA.getKeygenName(), BLANK);
		cfg.put(LicenseEnum.DURATION.getKeygenName(), BLANK);
		cfg.put(LicenseEnum.GATEWAY.getKeygenName(), BLANK);
		cfg.put(LicenseEnum.LICENSE_ENV.getKeygenName(), BLANK);
		cfg.put(LicenseEnum.INTERVALS.getKeygenName(), BLANK);
		cfg.put(LicenseEnum.EVENTSOURCES.getKeygenName(), BLANK);

		cfg.store(new FileOutputStream(cfgfile), BLANK);

		// print license.lic
		System.out.println("\nLicense (Encrypted):\n\n");
		System.out.println(license);

		System.out.println(String.format("\n\nLicense key generated above also stored in %s\\%s", newdir.getAbsolutePath(), 
										 newfilename));

		return newdir.getAbsolutePath() + FORWARD_SLASH + newfilename;
	} // generateLicenseFile

	private static void validateData(String customerName, String ipAddresses, String expirationDate, String noOfCore, 
									 String maxMemoryRSCONTROL, String maxMemoryRSVIEW, String endUserCount, String ha, 
									 String duration, String licenseType) throws Exception
	{
		Collection<String> propsInError = new ArrayList<String>();

		if (isVerbose)
		{
			System.out.println("Verbose: inside validateData");
		}
		boolean isInputValid = true;
		if (StringUtils.isEmpty(customerName))
		{
			isInputValid = false;
			propsInError.add(LicenseEnum.CUSTOMER.getParamName());
		}

		if (!Util.validateIP(ipAddresses))
		{
			isInputValid = false;
			propsInError.add(LicenseEnum.IP_ADDRESS.getParamName());
		}

		if (!Util.validateDate(expirationDate, licenseType))
		{
			isInputValid = false;
			propsInError.add(LicenseEnum.EXPIRATION_DATE.getParamName());
		}
		
		if (!Util.validateNumber(noOfCore))
		{
			isInputValid = false;
			propsInError.add(LicenseEnum.NO_OF_CORE.getParamName());
		}

		if (!Util.validateNumber(maxMemoryRSCONTROL))
		{
			isInputValid = false;
			propsInError.add(LicenseEnum.MAX_MEMORY_RSCONTROL.getParamName());
		}

		if (!Util.validateNumber(maxMemoryRSVIEW))
		{
			isInputValid = false;
			propsInError.add(LicenseEnum.MAX_MEMORY_RSVIEW.getParamName());
		}

		if (!Util.validateNumber(endUserCount))
		{
			isInputValid = false;
			propsInError.add(LicenseEnum.END_USER_COUNT.getParamName());
		}

		if (!Util.validateNumber(duration))
		{
			isInputValid = false;
			propsInError.add(LicenseEnum.DURATION.getParamName());
		}

		if (!isInputValid)
		{
			throw new Exception(String.format("Invalid values for parameters: [%s], please check!", 
											  StringUtils.collectionToString(propsInError, SEPERATOR)));
		}
	}

	private static void validateExpirationDate(Properties prop) throws Exception {
		if (!prop.containsKey(LicenseEnum.EXPIRATION_DATE.getKeygenName()) ||
			StringUtils.isBlank((String)prop.get(LicenseEnum.EXPIRATION_DATE.getKeygenName()))) {
			throw new Exception(String.format("Missing %s which is required for license type %s!",
											  LicenseEnum.EXPIRATION_DATE.getKeygenName(), 
											  (String)prop.get(LicenseEnum.LICENSE_TYPE.getKeygenName())));
		}
		
		if (!Util.validateDate((String)prop.get(LicenseEnum.EXPIRATION_DATE.getKeygenName()), 
							   (String)prop.get(LicenseEnum.LICENSE_TYPE.getKeygenName()))) {
			throw new Exception(String.format("Invalid %s which is required for license type %s!",
					  						  LicenseEnum.EXPIRATION_DATE.getKeygenName(), 
					  						  (String)prop.get(LicenseEnum.LICENSE_TYPE.getKeygenName())));
		}
	}
	
	private static void validateIntervals(Properties prop) throws Exception {		
		SortedSet<LicenseInterval> sortedParsedLicenseIntervals = License.processLicenseIntervals(prop);
		
		if (sortedParsedLicenseIntervals == null || sortedParsedLicenseIntervals.isEmpty()) {
			throw new Exception(String.format("Failed to parse %s specified for license type %s!",
					  						  LicenseEnum.INTERVALS.getKeygenName(), 
					  						  (String)prop.get(LicenseEnum.LICENSE_TYPE.getKeygenName())));
		}
		
		if (!Util.validateLicenseIntervals(sortedParsedLicenseIntervals)) {
			throw new Exception(String.format("Invalid %s specified for license type %s!",
					  						  LicenseEnum.INTERVALS.getKeygenName(), 
					  						  (String)prop.get(LicenseEnum.LICENSE_TYPE.getKeygenName())));
		}
		
		Long liBasedExpirationDate = License.getLicenseIntervalBasedExpirationDate(sortedParsedLicenseIntervals);
		
		if (liBasedExpirationDate != null && liBasedExpirationDate.longValue() > 0) {
			prop.put(LicenseEnum.EXPIRATION_DATE.getKeygenName(), 
					 DateUtils.convertDateToStringGMT(GMTDate.getDate(liBasedExpirationDate), 
							 					      LicenseEnum.DATE_FORMAT.getKeygenName()));
		}
	}
	
	private static void validateEventSources(Properties prop) throws Exception {
		Set<String> parsedEventSources = License.processEventSources(prop);
		
		if (CollectionUtils.isEmpty(parsedEventSources)) {
			throw new Exception(String.format("Failed to parse %s specified for license type %s!",
					  						  LicenseEnum.EVENTSOURCES.getKeygenName(), 
					  						  (String)prop.get(LicenseEnum.LICENSE_TYPE.getKeygenName())));
		}
		
		if (!Util.validateEventSources(parsedEventSources)) {
			throw new Exception(String.format("Invalid %s specified for license type %s!",
					  						  LicenseEnum.EVENTSOURCES.getKeygenName(), 
					  						  (String)prop.get(LicenseEnum.LICENSE_TYPE.getKeygenName())));
		}
	}
	
	private static void validateDuration(Properties prop) throws Exception {
		if (!prop.containsKey(LicenseEnum.DURATION.getKeygenName()) ||
			StringUtils.isBlank((String)prop.get(LicenseEnum.DURATION.getKeygenName()))) {
			throw new Exception(String.format("Missing %s which is required for license type %s!",
											  LicenseEnum.DURATION.getKeygenName(), 
											  (String)prop.get(LicenseEnum.LICENSE_TYPE.getKeygenName())));
		}
		
		if (!Util.validateNumberForLicenseType((String)prop.get(LicenseEnum.DURATION.getKeygenName()), 
							   (String)prop.get(LicenseEnum.LICENSE_TYPE.getKeygenName()))) {
			throw new Exception(String.format("Missing or invalid %s which is required for license type %s!",
					  						  LicenseEnum.DURATION.getKeygenName(), 
					  						  (String)prop.get(LicenseEnum.LICENSE_TYPE.getKeygenName())));
		}
	}
	
	private static void validateEndUserCount(Properties prop) throws Exception {
		if (!prop.containsKey(LicenseEnum.END_USER_COUNT.getKeygenName())) {
			throw new Exception(String.format("%s is missing which is invalid for license type %s!",
											  LicenseEnum.END_USER_COUNT.getKeygenName(),
											  (String)prop.get(LicenseEnum.LICENSE_TYPE.getKeygenName())));
		}
	}
	
	private static void validateLicenseEnv(Properties prop) throws Exception {
		if (!prop.containsKey(LicenseEnum.LICENSE_ENV.getKeygenName())) {
			throw new Exception(String.format(
					"Missing %s which is required for license type %s!",
					LicenseEnum.LICENSE_ENV.getKeygenName(), (String)prop.get(LicenseEnum.LICENSE_TYPE.getKeygenName())));
		}
	}
	
	private static void validateV2TypeLicense(Properties prop) throws Exception {
		if (prop.containsKey(LicenseEnum.INTERVALS.getKeygenName()) && 
			StringUtils.isNotBlank((String)prop.get(LicenseEnum.INTERVALS.getKeygenName()))) {
			validateIntervals(prop);
		}
		
		if (prop.containsKey(LicenseEnum.EVENTSOURCES.getKeygenName()) && 
		    StringUtils.isNotBlank((String)prop.get(LicenseEnum.EVENTSOURCES.getKeygenName()))) {
			validateEventSources(prop);
		}
		
		validateExpirationDate(prop);		
		validateEndUserCount(prop);
		validateLicenseEnv(prop);
	}
	
	private static void validateEvaluationTypeLicense(Properties prop) throws Exception {
		validateDuration(prop);
		validateLicenseEnv(prop);
		
		if (LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(
				(String)prop.get(LicenseEnum.LICENSE_ENV.getKeygenName()))) {
			throw new Exception(String.format("Invalid %s %s for license type %s!",
											  LicenseEnum.LICENSE_ENV.getKeygenName(), 
											  (String)prop.get(LicenseEnum.LICENSE_ENV.getKeygenName()),
											  (String)prop.get(LicenseEnum.LICENSE_TYPE.getKeygenName())));
		}
	}
	
	private static void validateLicenseType(Properties prop) throws Exception {
		if (prop != null && prop.containsKey(LicenseEnum.LICENSE_TYPE.getKeygenName())) {
			if (LicenseEnum.LICENSE_TYPE_V2 == 
				LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + 
								    (String)prop.get(LicenseEnum.LICENSE_TYPE.getKeygenName()))) {
				validateV2TypeLicense(prop);
			} else if (LicenseEnum.LICENSE_TYPE_EVALUATION == 
					   LicenseEnum.valueOf(LICENSE_TYPE_PREFIX + 
						    (String)prop.get(LicenseEnum.LICENSE_TYPE.getKeygenName()))) {
				validateEvaluationTypeLicense(prop);
			}
		}
	}
	
	private static String signLicense(String license) throws Exception {
		if (StringUtils.isNotBlank(license)) {
			// De-serialize Resolve's java.security.PrivateKey		
			ByteArrayInputStream bipriv;
			bipriv = new ByteArrayInputStream(Hex.decodeHex(rprivksobs.toCharArray()));
			ObjectInputStream oipriv = new ObjectInputStream(bipriv);
		    Object obj = oipriv.readObject();
		    
		    PrivateKey dspriv = (PrivateKey)obj;
		    System.out.println("Deserialized Private Key Algorithm:" + dspriv.getAlgorithm() + 
					   		   ", Encoding Format:" + dspriv.getFormat() + 
					   		   ",# of Bytes:" + (dspriv.getEncoded() != null ? dspriv.getEncoded().length : "0"));
		    
			Signature dsa = Signature.getInstance("SHA1withDSA", "SUN"); 
		    dsa.initSign(dspriv);
		    dsa.update(license.getBytes(StandardCharsets.UTF_8));
		    return license + LicenseEnum.SIGNATURE.getKeygenName() + Hex.encodeHexString(dsa.sign());
		} else {
			throw new Exception("License data passed to sign is blank or null.");
		}
	}
	
	public static void main(String args[]) throws Exception
	{
		if (args.length < 1)
		{
			System.out.println("Usage: com.resolve.license.Keygen -customer Gen-E [-ip 10.10.10.100] " +
							   "[-exp " + Calendar.getInstance().get(Calendar.YEAR) + "1231] " +
							   "[-type evaluation/standard/enterprise/v2] [-noOfCore 2] [-maxMemoryRSCONTROL 2048] " +
							   "[-maxMemoryRSREMOTE 1024] [-maxMemoryRSVIEW 2048] [-endUserCount 100] [-ha true] " +
							   "[-duration 90] [-gateway EMAIL#1#30,REMEDYX#1#30] [-env PROD/<user defined>]" + 
							   "[-intervals " + Calendar.getInstance().get(Calendar.YEAR) + "0101#" +
							   Calendar.getInstance().get(Calendar.YEAR) + "1231#1000#2000," + 
							   (Calendar.getInstance().get(Calendar.YEAR) + 1) + "0101#" + 
							   (Calendar.getInstance().get(Calendar.YEAR) + 1) + "1231#1500#3000 " +
							   "[-eventsources splunk,remedyx]");
			System.out.println("\nNotes:");
			System.out.println("\n\nip: Default is blank if not specified and indicates license is not restricted to specific IP address");
			System.out.println("\n    License can be restricted to set of IP addresses by specifying multiple -ip <IP Address> options");
			System.out.println("\n\nexp: Format is YYYYMMDD, Default is blank if not specified indicating perpetual license");
			System.out.println("\n\ntype: Default is standard if not specified, seurity license type provides access to Resolve's Security OP product line in addtion to Network & IT OP product lines");
			System.out.println("\n      seurity and evaluation license type must have valid expiry date");
			System.out.println("\n\nendUserCount: # of users must be >= 0, Default is 0 which indicates no restriction on # of users for all product lines");
			System.out.println("\n              For seurity type # > 0 indicates per named user based licensing and 0 indicates usage based licensing");
			System.out.println("\n\ngateway: The gateway parameter format is GATEWAYCODE#NOOFINSTANCES, wild card for GATEWAYCODE is *");
			System.out.println("\n\nenv: The environment this license is for, Only PROD environment specified in blueprint will be matched for loading license");
			System.out.println("\n     All other user defined environments will be treated as other than PROD and no matching check will be performed for license loading");
			System.out.println("\n     Resolve 6.3 and above will use the license environment for Network & IT OP as well as Security OP product line");
			System.out.println("\n     Default is \"NON PROD\" if not specified");
			System.out.println("\n\nintervals: V2 type license usage interval(s) in format Start Date(YYYYMMDD)#End Date(YYYYMMDD#Max Incidents#Max Events");
			System.out.println("\n           Both start and end dates are inclusive, intervals should be contiguous");
			System.out.println("\n           End date value of \"99991231\" is considered as perpetual");
			System.out.println("\n\neventsources: List of sources data from which should be treated as events instead of default incidents");
		}
		else
		{
			// Initialize license time zone to UTC/GMT/UT
			GMTDate.initTimezoneSOP("UTC");
			generateLicenseFile(args);
		}
	} // main

} // Keygen
