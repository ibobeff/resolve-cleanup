@echo off
::The argument is the customer's name whom the license file is issued to.
"java" -cp "lib\commons-io.jar;lib\commons-lang.jar;lib\commons-lang3.jar;lib\commons-codec.jar;lib\json-lib.jar;lib\resolve-license.jar;lib\joda-time.jar;lib\commons-collections4.jar" com.resolve.license.LicenseForm
exit /B 0

