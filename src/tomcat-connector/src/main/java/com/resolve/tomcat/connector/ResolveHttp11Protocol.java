package com.resolve.tomcat.connector;

import org.apache.coyote.http11.Http11Protocol;

public class ResolveHttp11Protocol extends Http11Protocol
{
    @Override
    public void init() throws Exception
    {
        getLog().info("Initializing Resolve customized protocol");
        String encData = CryptUtils.decrypt(Crypt.getEncDataLocal(), CryptUtils.prefixLocal);
        CryptUtils.setENCData(encData);
        
        //setKeyPass(getKeyPass());
		 setKeystorePass(getKeystorePass());
        super.init();
    }

   /* @Override
	//Reads the key password of the certificate
    public String getKeyPass()
    {
        try
        {
            String keystorePass = super.getKeyPass();
            return CryptUtils.decrypt(keystorePass);
        }
        catch (Exception e)
        {
            getLog().error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }
	*/
	
	@Override
    //Reads keystore password of certificate
    public String getKeystorePass()
    {
        try
        {

            String keystorePass = super.getKeystorePass();
            return CryptUtils.decrypt(keystorePass);
        }
        catch (Exception e)
        {
            getLog().error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }
	
}
