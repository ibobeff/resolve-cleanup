package com.resolve.gateway.radius;

import com.resolve.gateway.BaseFilter;
import com.resolve.util.Log;

public class RADIUSFilter extends BaseFilter {

    public RADIUSFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script) {
        super(id, active, order, interval, eventEventId, runbook, script);
    }
}

