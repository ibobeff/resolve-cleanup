package com.resolve.gateway.radius.client;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.resolve.util.Log;

import net.sourceforge.jradiusclient.RadiusAttribute;
import net.sourceforge.jradiusclient.RadiusAttributeValues;
import net.sourceforge.jradiusclient.RadiusClient;
import net.sourceforge.jradiusclient.RadiusPacket;
import net.sourceforge.jradiusclient.exception.InvalidParameterException;
import net.sourceforge.jradiusclient.exception.RadiusException;
import net.sourceforge.jradiusclient.packets.ChapAccessRequest;
import net.sourceforge.jradiusclient.util.ChapUtil;

public class AuthClient
{
   // public RadiusClient radiusClient;
    public Map<String,String> authResponseAttribute = new HashMap();
    public boolean isClientInitialised = false;
    private static byte[] chapChallenge = null;

    public  RadiusClient initRadiusClient(String host, int authport, int acctport, String sharedSecret)
    {
        RadiusClient radiusClient=null;
  
        try
        {
        
         
            Log.log.info("Instantiating JRadius client to host: "+host);
            
             radiusClient = new RadiusClient(host, authport, acctport, sharedSecret);
            
            this.isClientInitialised = true;
            Log.log.info("Initialised Radius Client to host: " + host);
          
        }
        catch (RadiusException rex)
        {
            Log.log.error(rex.getMessage());
           
        }
        catch (InvalidParameterException ivpex)
        {
            Log.log.error("Unable to initialise Radius Client to host " + host);
            Log.log.error(ivpex.getMessage());
          
        }
        return radiusClient;
    }

    public  RadiusPacket authenticateUser( RadiusClient radiusClient,String authMethod, String userName, String userPass, String sharedScecret)
    {
       // authMethod ="chap";
     //   userName = "resolve";
     //   userPass = "Resolv1234";
        RadiusPacket accessResponse = null;
        try
        {
            ChapUtil chapUtil = new ChapUtil();
             
            
            
            RadiusPacket accessRequest = new RadiusPacket(RadiusPacket.ACCESS_REQUEST);
            RadiusAttribute userNameAttribute = new RadiusAttribute(RadiusAttributeValues.USER_NAME, userName.getBytes());
            accessRequest.setAttribute(userNameAttribute);

            if (authMethod.equalsIgnoreCase("chap"))
            {
                // byte[] chapChallenge = chapUtil.getNextChapChallenge(16);
//   accessRequest = new ChapAccessRequest(userName, userPass);
                byte[] chapChallenge = chapUtil.getNextChapChallenge(16);
                accessRequest.setAttribute(new RadiusAttribute(RadiusAttributeValues.CHAP_PASSWORD, chapEncrypt(userPass, chapChallenge)));

                accessRequest.setAttribute(new RadiusAttribute(RadiusAttributeValues.CHAP_CHALLENGE, chapChallenge));
            }
            else if(authMethod.equalsIgnoreCase("PAP"))
            {
                accessRequest.setAttribute(new RadiusAttribute(RadiusAttributeValues.USER_PASSWORD, userPass.getBytes()));
            }
            else {
                Log.log.error("Authetication method "+ authMethod +" is not supported. Should be PAP/CHAP");
              //  authResponseAttribute.put("Status",authMethod+" authentication protocol is not supported. Please specify PAP/CHAP.");
                return null;
            }

            // TESTING to see how to get attribute
            // String userGroup = "doesnt matter";
            // RadiusAttribute groupAttribute = new
            // RadiusAttribute(RadiusAttributeValues.LOGIN_LAT_GROUP,userGroup.getBytes());
            // accessRequest.setAttribute(groupAttribute);

            // RadiusAttributeValues.LOGIN_LAT_GROUP
            Log.log.info("Performing basic authentication...");
          //  Log.log.info("userName: " + userName);
           // Log.log.info("Password: " + userPass);
            accessResponse = radiusClient.authenticate(accessRequest);
            //radiusClient=null;

          }
        catch (InvalidParameterException ivpex)
        {
          
            Log.log.error( ivpex.getMessage());
            ivpex.printStackTrace();
           // System.out.println(ivpex.getLocalizedMessage());
           // authResponseAttribute.put("Status", ivpex.getMessage());
           
        }
        catch (RadiusException rex)
        {
          
            Log.log.error(rex.getMessage());
           // authResponseAttribute.put("status", rex.getMessage());
      
        
        }
        return accessResponse;
    }

   

    private static byte[] chapEncrypt(final String plainText, final byte[] chapChallenge)
    {
        // see RFC 2865 section 2.2
        byte chapIdentifier = '0'; // chapUtil.getNextChapIdentifier();//tim not
                                   // tested this
        byte[] chapPassword = new byte[17];
        chapPassword[0] = chapIdentifier;
        System.arraycopy(ChapUtil.chapEncrypt(chapIdentifier, plainText.getBytes(), chapChallenge), 0, chapPassword, 1, 16);
        return chapPassword;
    }

//    private static void printAttributes(RadiusPacket rp)
//    {
//        Iterator attributes = rp.getAttributes().iterator();
//        RadiusAttribute tempRa;
//        System.out.println("Response Packet Attributes");
//        System.out.println("\tType\tValue");
//        while (attributes.hasNext())
//        {
//            tempRa = (RadiusAttribute) attributes.next();
//            Object o = tempRa.getValue();
//           Log.log.info("\t" + tempRa.getType() + "\t" + new String(tempRa.getValue()));
//        }
//    }

    public  Map<String, String> getAuthAttributes(RadiusPacket rp, String userName)
    {

       
        Map<String, String> responseAttributes = new HashMap();
     
        try
        {
            switch (rp.getPacketType())
            {
                case RadiusPacket.ACCESS_ACCEPT:
                    responseAttributes.put("status", "Autheticated");
                    Log.log.info("User " + userName + " Authenticated");
                    break;
                case RadiusPacket.ACCESS_REJECT:
                   Log.log.warn("User " + userName + " NOT authenticated");
                 //   responseAttributes.put("status", "NOT Autheticated");
                    break;
                case RadiusPacket.ACCESS_CHALLENGE:
                    String reply;

                    reply = new String(rp.getAttribute(RadiusAttributeValues.REPLY_MESSAGE).getValue());
                    responseAttributes.put("status", " Challenged with " + reply);

                    Log.log.warn("User " + userName + " Challenged with " + reply);
                    break;
                default:
                    responseAttributes.put("status", "Packet type received in response is invalid");
                    Log.log.warn("Packet type received in response is invalid" + rp.getPacketType());
                    break;

            }
           
            Iterator attributes = rp.getAttributes().iterator();
            RadiusAttribute tempRa;

            while (attributes.hasNext())
            {
                tempRa = (RadiusAttribute) attributes.next();
              //  Object o = tempRa.getValue();
                int a = tempRa.getType();
                
                Log.log.info("\t" + ""+tempRa.getType() + "\t" + new String(tempRa.getValue()));
                responseAttributes.put(""+tempRa.getType(), new String(tempRa.getValue()));
                //  25: (Accounting) Arbitrary value that the network access server includes in all accounting packets for this user if supplied by the RADIUS server. 
            }
        }
        catch (InvalidParameterException e)
        {
            responseAttributes.put("status", e.getMessage());
            Log.log.error(e.getMessage(), e);
        }
        catch (RadiusException e)
        {
            responseAttributes.put("status", e.getMessage());
            Log.log.error(e.getMessage(), e);
        }
        return responseAttributes;

    }

}
