package com.resolve.gateway;

import java.util.Map;

import com.resolve.gateway.radius.RADIUSFilter;
import com.resolve.gateway.radius.RADIUSGateway;
import com.resolve.util.Constants;
import com.resolve.util.StringUtils;

public class MRADIUS extends MGateway {

    

    private static final String RSCONTROL_SET_FILTERS = "MRADIUS.setFilters";

    private static final RADIUSGateway instance = RADIUSGateway.getInstance();

    public MRADIUS() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link RADIUSFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        if (instance.isActive()) {
            RADIUSFilter radiusFilter = (RADIUSFilter) filter;
        }
    }
    
    public Map<String, String> authenticateUser(Map params)
    {
        
        String userName = StringUtils.getString(Constants.AUTH_USERNAME, params);
        String userPass = StringUtils.getString(Constants.AUTH_P_ASSWORD, params);
        String authMethod = StringUtils.getString(Constants.AUTH_RADIUS_AUTHPROTOCOL,params);
        String sharedScecret = StringUtils.getString(Constants.AUTH_RADIUS_SHAREDSECRET, params);
        String primaryHost = StringUtils.getString(Constants.AUTH_RADIUS_PRIMARYHOST, params);
        String secondaryHost = StringUtils.getString(Constants.AUTH_RADIUS_SECONDARYHOST, params);
        int authPort = StringUtils.getInt(Constants.AUTH_RADIUS_AUTHPORT, params);
        int acctPort = StringUtils.getInt(Constants.AUTH_RADIUS_ACCTPORT, params);
        return instance.authenticateUser(primaryHost, secondaryHost, authPort, acctPort, userName, userPass, authMethod, sharedScecret);
     
    }
}

