package com.resolve.gateway.radius.client;

import java.util.Map;

import com.resolve.gateway.radius.RADIUSGateway;
import com.resolve.util.Log;

public class ThreadedAuth implements Runnable
{   
    private Thread t;
    private static final RADIUSGateway instance=RADIUSGateway.getInstance();
    private String threadName;
    
    public ThreadedAuth( String name){
        threadName = name;
     //   System.out.println("Creating " +  threadName );
    }
    
    public static void main(String[] args)
    {
        // TODO Auto-generated method stub
    for(int i =0;i<2;i++) {
        ThreadedAuth R1 = new ThreadedAuth( "Thread-"+i);
        R1.start();
    }
       
        
       
    }

    @Override
    public void run()
    {
        System.out.println("Running " +  threadName );
         
           Map<String,String> responseAttributes = instance.authenticateUser("10.50.1.156","10.50.1.156",1812,1813, "resolve", "Resolve1234", "PAP","resolve");
            for (Map.Entry<String, String> entry : responseAttributes.entrySet()) {
                Log.log.info(entry.getKey()+": " + entry.getValue());
                System.out.println(entry.getKey()+": " + entry.getValue());
            }
            try
            {
                
                Thread.sleep(5000);
            }
            catch (InterruptedException e)
            {
                Log.log.error(e.getMessage(), e);
            }
            responseAttributes.clear();
        System.out.println("exiting " +  threadName );
        
    }
    
    
    public void start ()
    {
   //    System.out.println("Starting " +  threadName );
       if (t == null)
       {
          t = new Thread (this, threadName);
          t.start ();
       }
    }
       
}
