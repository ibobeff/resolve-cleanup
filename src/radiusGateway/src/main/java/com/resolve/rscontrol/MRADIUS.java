package com.resolve.rscontrol;

import com.resolve.persistence.model.RADIUSFilter;

public class MRADIUS extends MGateway {

    private static final String MODEL_NAME = RADIUSFilter.class.getSimpleName();

    protected String getModelName() {
        return MODEL_NAME;
    }
}

