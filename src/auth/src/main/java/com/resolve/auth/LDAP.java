/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.auth;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPAttributeSet;
import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPJSSESecureSocketFactory;
import com.novell.ldap.LDAPMessage;
import com.novell.ldap.LDAPMessageQueue;
import com.novell.ldap.LDAPSearchResults;
import com.novell.ldap.LDAPUnbindRequest;
import com.resolve.util.Base64;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.DigestUtils;
import com.resolve.util.ERR;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class LDAP 
{
    LDAPConnection conn;
        
    int version = LDAPConnection.LDAP_V3;
    String cryptType = CRYPT_TYPE_CLEAR;
    String cryptPrefix = null;
    String mode;
    List<String> baseDNList;
    String defaultDomain = "";
    String uidAttribute = "uid";
    String p_asswordAttribute = "userPassword";
    String host;
    int port;
    String bindDN;
    String bindP_assword;
    boolean ssl;
    
    boolean active = false;
    boolean sync = false;
    boolean fallback = false;
    String domain = null;
    
    Properties syncProperties;
    
    final static String AUTH_MODE_BIND = "BIND";                // bind using user account, use CN=username,... 
    final static String AUTH_MODE_BIND_CN = "BIND_CN";          // bind using user account, use CN=username,...
    final static String AUTH_MODE_BIND_UID = "BIND_UID";        // bind using user account, use <uidAttribute>=username,...
    final static String AUTH_MODE_COMPARE = "COMPARE";          // bind using bindDN account, use <uidAttribute>=username,..., compare password locally
    final static String AUTH_MODE_COMPARE_LDAP = "COMPARE_LDAP";// bind using bindDN account, use <uidAttribute>=username,..., compare password through ldap compare call
    final static String AUTH_MODE_USERNAME = "USERNAME";        // bind using bindDN account, use <uidAttribute>=username with no password validation
    
    final static String CRYPT_TYPE_CLEAR = "CLEAR";
    final static String CRYPT_TYPE_CRYPT = "CRYPT";
    final static String CRYPT_TYPE_MD5 = "MD5";
    
    final static String CRYPT_PREFIX_DEFAULT = "DEFAULT";
    final static String CRYPT_PREFIX_CRYPT = "{CRYPT}";
    final static String CRYPT_PREFIX_MD5 = "{MD5}";

    private String memberofLookupKey = Constants.MEMBEROF_DEFAULT;
    
    /**
     * @param args
     */
    
    public LDAP(String mode, String host, int port, String bindDN, String bindP_assword, List<String> baseDNList, boolean ssl, String cryptType, String cryptPrefix, File syncFile) throws Exception
    {
        // init ldap
        if (ssl)
        {
            Log.log.info("Using Secure LDAPS connection");
        }
        else
        {
            Log.log.info("Using standard LDAP connection");
        }
        
        this.mode = mode.toUpperCase();
        this.host = host;
        this.port = port;
        this.bindDN = bindDN;
        this.bindP_assword = bindP_assword;
        this.baseDNList = baseDNList;
        this.ssl = ssl;
        this.cryptType = cryptType;
        this.cryptPrefix = CRYPT_PREFIX_DEFAULT.equalsIgnoreCase(cryptPrefix) ? getDefaultCryptPrefix(cryptType) : cryptPrefix;
                    
        // sync mapping file
        if (syncFile != null)
        {
	        this.syncProperties = new Properties();
            FileInputStream fis = new FileInputStream(syncFile);
	        this.syncProperties.load(fis);
	        fis.close();
        }
        else
        {
            this.syncProperties = null;
        }
    } // LDAP
    
    public LDAP(String mode, String host, int port, String bindDN, String bindPassword, List<String> baseDNList, boolean ssl, String cryptType, String cryptPrefix, Properties syncProp) throws Exception
    {
        if (ssl)
        {
            Log.log.info("Using Secure LDAPS connection");
        }
        else
        {
            Log.log.info("Using standard LDAP connection");
        }
        
        this.mode = mode.toUpperCase();
        this.host = host;
        this.port = port;
        this.bindDN = bindDN;
        this.bindP_assword = bindPassword;
        this.baseDNList = baseDNList;
        this.ssl = ssl;
        this.cryptType = cryptType;
        this.cryptPrefix = CRYPT_PREFIX_DEFAULT.equalsIgnoreCase(cryptPrefix) ? getDefaultCryptPrefix(cryptType) : cryptPrefix;
        
        // sync mapping file
        this.syncProperties = syncProp;
        
        //the key is coming from the definition that is set 
        memberofLookupKey = syncProperties.getProperty(Constants.MEMBEROF);
        if(StringUtils.isEmpty(memberofLookupKey))
        {
            memberofLookupKey = Constants.MEMBEROF_DEFAULT;
        }
    } // LDAP
    
    public int getVersion()
    {
        return version;
    } // getVersion

    public void setVersion(int version)
    {
        this.version = version;
    } // setVersion

    public String getUIDAttribute()
    {
        return uidAttribute;
    } // getUidAttribute

    public void setUIDAttribute(String uidAttribute)
    {
        this.uidAttribute = uidAttribute;
    } // setUIDAttribute

    public String getPasswordAttribute()
    {
        return p_asswordAttribute;
    }

    public void setPasswordAttribute(String passwordAttribute)
    {
        this.p_asswordAttribute = passwordAttribute;
    }
    
    public String getDefaultDomain()
    {
        return defaultDomain;
    }

    public void setDefaultDomain(String defaultDomain)
    {
        this.defaultDomain = defaultDomain;
    }
    
    public boolean connect()
    {
        boolean result = false;
        
        try
        {
            if (ssl)
            {
                Log.log.debug("Creating Secure LDAP Connection Object");
                LDAPJSSESecureSocketFactory socketFactory = new LDAPJSSESecureSocketFactory();
                this.conn = new LDAPConnection(socketFactory);
            }
            else
            {
                Log.log.debug("Creating LDAP Connection Object");
                this.conn = new LDAPConnection();
            }
            
            if (conn != null)
            {
                // connect to ldap server
                Log.log.debug("Connecting to LDAP");
                conn.connect(host, port);
    
                Log.log.info("LDAP Connect successfully to host " + host + " and port " + port);
                result = true;
            }
        } 
        catch (Throwable e)
        {
            Log.alert(ERR.E60002);
        }
            
        return result;
    } // connect
    
    public void disconnect()
    {
        try
        {
            if (conn != null && conn.isConnected())
            {
                // explicitly send unbind message request - disconnect should automatically unbind, 
                // but novell eDirectory need an explicit unbind on auth fails
                try
                {
                    if (!conn.isBound())
                    {
                        Log.log.debug("Explicitly sending LDAPUnbindRequest");
                        LDAPMessage unbindMsg = new LDAPUnbindRequest(null);
                        int msgId = unbindMsg.getMessageID();
                        LDAPMessageQueue responseQueue = conn.sendRequest(unbindMsg, null);
                        responseQueue.getResponse(msgId);
                        //conn.sendRequest(new LDAPUnbindRequest(null), null);
                    }   
                }
                catch (LDAPException e)
                {
                    Log.log.error("Failed to unbind: "+e.getMessage(), e);
                }
                // disconnect
                Log.log.debug("Disconnecting LDAP connection");
                conn.disconnect();
            }
        }
        catch (LDAPException e)
        {
            Log.log.error("Failed to finilize connection: "+e.getMessage(), e);
        }
        finally
        {
            conn = null;
        }
    } // disconnect
    
    public synchronized Map authenticate(String username, String p_assword)
    {
		Map result = null;
		 int pos=-1;
		 
        
        try
        {
           
            if(username.contains("\\")) {  // remove domain\ from username
                 pos = username.indexOf('\\');
                 username = username.substring(pos+1,username.length());
            }
            else if (username.contains("@")) { // remove @domain from username
             pos = username.indexOf('@');
             username = username.substring(0,pos);
            }
            
            
            if (StringUtils.isEmpty(p_assword) && !AUTH_MODE_USERNAME.equals(mode))
            {
                //password required
                Log.log.warn("No Password Sent, Failing Authentication");
            }
            else if (connect())
            {
                String authBindDN = null;
                String authPassword = null;
                String authUidAttribute = null;
                
                boolean bound = false;
                
                if (mode.equals(AUTH_MODE_BIND) || mode.equals(AUTH_MODE_BIND_CN))
                {
                    authUidAttribute = "cn";
                    authPassword = p_assword;
                    //authBindDN = "cn="+username+","+baseDN;
                } 
                else if (mode.equals(AUTH_MODE_BIND_UID))
                {
                    authUidAttribute = uidAttribute;
                    authPassword = p_assword;
                }
                else // AUTH_MODE_COMPARE || AUTH_MODE_USERNAME
                {
                    authUidAttribute = uidAttribute;
                    authPassword = bindP_assword;
                    authBindDN = bindDN;
                    
                    // bind bindDN account
                    bound = bind(authBindDN, authPassword);
                }
                
                // bind to ldap server
                String searchUidBaseDN = null;
                for (String baseDN : baseDNList)
                {
                    // if bind mode, set bindDN to "cn=username,baseDN"
                    if (mode.equals(AUTH_MODE_BIND) || mode.equals(AUTH_MODE_BIND_CN))
                    {
                        authBindDN = authUidAttribute+"="+username+","+baseDN;
                        bound = bind(authBindDN, authPassword);
                        //authBindDN = "cn="+username+","+baseDN;
                    } 
                    else if (mode.equals(AUTH_MODE_BIND_UID))
                    {
                        authBindDN = authUidAttribute+"="+username+","+baseDN;
                        bound = bind(authBindDN, authPassword);
                    }
                    else // AUTH_MODE_COMPARE || AUTH_MODE_USERNAME
                    {
                        // binding perfomed above outside loop
                    }
                    
                    if (bound)
                    {
                        int a=11;
                        String query = "("+ authUidAttribute+"="+username+")";
                        Log.log.info("LDAP searching user: "+username+" query: "+query+" under baseDN: "+baseDN);
                        LDAPSearchResults searchResults = conn.search(baseDN, LDAPConnection.SCOPE_SUB, query, null, false);
                        if (searchResults.hasMore())
                        {
                            LDAPEntry ldapEntry = searchResults.next();
                            
                            if (mode.equals(AUTH_MODE_BIND) || mode.equals(AUTH_MODE_BIND_CN) || mode.equals(AUTH_MODE_BIND_UID) || mode.equals(AUTH_MODE_USERNAME))
                            {
                                result = getUserAttributes(ldapEntry);
                            }
                            else if (mode.equals(AUTH_MODE_COMPARE_LDAP))
                            {
                                String userDN = ldapEntry.getDN();
                                LDAPAttribute comparePasswordAttr = new LDAPAttribute(p_asswordAttribute, p_assword);
                                Log.log.debug("Comparing Password for " + userDN);
                                if (conn.compare(userDN, comparePasswordAttr))
                                {
                                    result = getUserAttributes(ldapEntry);
                                }
                                else
                                {
                                    Log.log.warn("Password Compare for " + userDN + " failed");
                                }
                            }
                            else // AUTH_MODE_COMPARE
                            {
                                // encrypt compare value
                                String ldapPassword = ((LDAPAttribute)ldapEntry.getAttribute(p_asswordAttribute)).getStringValue();
                                //if the cryptPrefix is default then it's not necessary to get the substring
                                if(!"DEFAULT".equalsIgnoreCase(cryptPrefix))
                                {
                                    ldapPassword = ldapPassword.substring(cryptPrefix.length(),ldapPassword.length());
                                }
                                if (comparePassword(p_assword, ldapPassword))
                                {
                                    result = getUserAttributes(ldapEntry);
                                }
                            }
                            
                            if (result != null)
                            {
                                //if (!result.containsKey("memberof"))
                                if (!result.containsKey(memberofLookupKey))
                                {
                                    //LDAPSearchResults memberOfResults = conn.search(baseDN, LDAPConnection.SCOPE_SUB, "("+ authUidAttribute+"="+username+")", new String[] {"memberof"}, false);
                                    LDAPSearchResults memberOfResults = conn.search(baseDN, LDAPConnection.SCOPE_SUB, "("+ authUidAttribute+"="+username+")", new String[] {memberofLookupKey}, false);
                                    if (memberOfResults.hasMore())
                                    {
                                        LDAPEntry memberOfEntry = memberOfResults.next();
                                        Map memberOfResult = getUserAttributes(memberOfEntry);
                                        result.putAll(memberOfResult);
                                    }
                                }
                                
                                // set uid basedn
                                searchUidBaseDN = baseDN;
                                
                                break;
                            }
                        }
                        else
                        {
                            Log.log.warn("LDAP search user failed for baseDN: " + baseDN);
                        }
                    }
                    else
                    {
                        Log.log.warn("Failed to bind to LDAP for bindDN: "+authBindDN);
                    }
                }
                
                // try to get group membership using groupOfNames objectClass
                //if (result != null && !result.containsKey("memberof"))
                if (result != null && !result.containsKey(memberofLookupKey))
                {                        
                    // using the initial BIND
                    String memberOf = findGroupOfNamesMembership(conn, username, authUidAttribute, searchUidBaseDN);
                    
                    // also use the bindDN / bindPassword if provided
                    if (mode.equals(AUTH_MODE_BIND) || mode.equals(AUTH_MODE_BIND_CN) || mode.equals(AUTH_MODE_BIND_UID) )
                    {
                        if (StringUtils.isNotEmpty(bindDN) && StringUtils.isNotEmpty(bindP_assword) && bind(bindDN, bindP_assword))
                        {
                            memberOf = StringUtils.append(memberOf, findGroupOfNamesMembership(conn, username, authUidAttribute, searchUidBaseDN));
                        }
                    }
                    
                    // insert memberOf
                    if (StringUtils.isNotBlank(memberOf))
                    {
                        //result.put(Constants.MEMBEROF, memberOf);
                        result.put(memberofLookupKey, memberOf);
                    }
                }
                
                if (result != null)
                {
                    Log.log.info("LDAP authentication successful: " + username);
                    Log.log.debug("result: " + result);
                }
                else
                {
                    Log.log.warn("LDAP authentication failed: " + username);
                    Log.log.warn("result: " + result);
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.warn("LDAP Authentication failed: "+e.getMessage(), e);
        }
        finally
        {
            disconnect();
        }
        
        return result;
    } // authenticate

    String findGroupOfNamesMembership(LDAPConnection conn, String username, String authUidAttribute, String searchUidBaseDN) throws LDAPException
    {
        String memberOf = "";
        
        for (String baseDN : baseDNList)
        { 
            try
            {
                String query = "( member="+ authUidAttribute+"="+username+","+searchUidBaseDN+")";
                Log.log.debug("LDAP searching group - filter: "+query+" under baseDN: "+baseDN);
                LDAPSearchResults searchResults = conn.search(baseDN, LDAPConnection.SCOPE_SUB, query, null, false);
                while (searchResults.hasMore())
                {
                    LDAPEntry ldapEntry = searchResults.next();
                    String groupName = ldapEntry.getAttribute("cn").getStringValue();
                    
                    Log.log.debug("adding memberOf group: "+groupName);
                    memberOf = StringUtils.append(memberOf, groupName);
                }
            }
            catch (Exception e)
            {
                Log.log.debug(e.getMessage(), e);
            }
        }
        
        // try using cn
        if (StringUtils.isBlank(memberOf))
        {
            for (String baseDN : baseDNList)
            { 
                try
                {
                    String query = "( member=cn="+username+","+searchUidBaseDN+")";
                    Log.log.debug("LDAP searching group - filter: "+query+" under baseDN: "+baseDN);
                    LDAPSearchResults searchResults = conn.search(baseDN, LDAPConnection.SCOPE_SUB, query, null, false);
                    while (searchResults.hasMore())
                    {
                        LDAPEntry ldapEntry = searchResults.next();
                        String groupName = ldapEntry.getAttribute("cn").getStringValue();
                        
                        Log.log.debug("adding memberOf group: "+groupName);
                        memberOf = StringUtils.append(memberOf, groupName);
                    }
                }
                catch (Exception e)
                {
                    Log.log.debug(e.getMessage(), e);
                }
            }
        }
        return memberOf;
    } // findGroupOfNamesMembership
    
    Map getUserAttributes(LDAPEntry ldapEntry)
    {
        Map result = null;
        if (ldapEntry != null)
        {
            result = new HashMap();
        
            String foundDN = ldapEntry.getDN();
            
            LDAPAttributeSet attributeSet = ldapEntry.getAttributeSet();
            Iterator allAttributes = attributeSet.iterator();
    
            while (allAttributes.hasNext())
            {
                LDAPAttribute attribute = (LDAPAttribute) allAttributes.next();
                String attributeName = attribute.getName().toLowerCase();
    
                Object value = "";
                Log.log.trace("attributeName: "+attribute.getName());
                Log.log.trace("  attributeSize: "+attribute.size());
                Log.log.trace("  stringValue: "+attribute.getStringValue());
                Log.log.trace("  stringValues: "+attribute.getStringValues());
                //if (attributeName.equals("memberof"))
                if (attributeName.equals(memberofLookupKey))
                {
                    if (attribute.size() > 1)
                    {
                        value = Collections.list(attribute.getStringValues()); 
                    }
                    else
                    {
                        value = new ArrayList<String>();
                        ((List<String>)value).add(attribute.getStringValue());
                    }      
                }
                else if (attribute.size() > 1)
                {
	                value = Collections.list(attribute.getStringValues()); 
                }
                else
                {
                    value = attribute.getStringValue();
                }

                Log.log.trace("  value: "+value); 
                result.put(attributeName, value);
            }
            result.put("dn", foundDN);
        }
        
        return result;
    } // getUserAttributes
    
    boolean bind(String bindDN, String bindP_assword)
    {
        boolean bound = false;
        
        Log.log.debug("LDAP Bind starting: "+bindDN);
        if (bindDN != null && bindDN.length() > 0 && bindP_assword != null)
        {
            try
            {
                conn.bind(version, bindDN, bindP_assword.getBytes("UTF8"));
                bound = true;
                Log.log.debug("LDAP Bind successful");
            }
            catch (Exception e)
            {
                Log.log.error("LDAP Bind failed: "+e.getMessage(), e);
            }
        }
        else
        {
            Log.log.debug("LDAP Bind does not have necessary info");
        }
        return bound;
    } // bind
    
    boolean comparePassword(String password, String ldapPassword)
    {
        boolean result = false;
        
        try
        {
            if (!StringUtils.isEmpty(ldapPassword))
            {
                // unix crypt
                if (cryptType.equals(CRYPT_TYPE_CRYPT))
                {
                    String salt = ldapPassword.substring(0, 2);
                    String encryptPassword = CryptUtils.encryptUnix(password, salt, null);
                    
                    if (encryptPassword.equals(ldapPassword))
                    {
                        result = true;
                    }
                    
                }
                
                // md5
                else if (cryptType.equals(CRYPT_TYPE_MD5))
                {
                    // base64 encoded md5
                    if (ldapPassword.endsWith("=="))
                    {
                        byte[] ldapBytes = Base64.decode(ldapPassword);
                        
                        byte[] pwdBytes = DigestUtils.md5(password);
                        
//                        MD5 md5 = new MD5(password);
//                        byte[] pwdBytes = md5.asBytes();
                        
                        result = DigestUtils.hashesEqual(ldapBytes, pwdBytes);
                        
                    }
                    else
                    {
                        String encryptPassword = CryptUtils.encryptMD5(password);
                        if (encryptPassword.equals(ldapPassword))
                        {
                            result = true;
                        }
                    }
                }
                
                // plain text
                else
                {
                    if (password.equals(ldapPassword))
                    {
                        result = true;
                    }
                }
            }
            else
            {
                Log.log.error("Missing ldapPassword");
            }
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        
        return result;
    } // comparePassword

    public Properties getSyncProperties()
    {
        return syncProperties;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public void setSyncProperties(Properties syncProperties)
    {
        this.syncProperties = syncProperties;
    }

    public boolean isSync()
    {
        return sync;
    }

    public void setSync(boolean sync)
    {
        this.sync = sync;
    }

    public boolean isFallback()
    {
        return fallback;
    }

    public void setFallback(boolean fallback)
    {
        this.fallback = fallback;
    }

    public String getDomain()
    {
        return domain;
    }

    public void setDomain(String domain)
    {
        this.domain = domain;
    }
    
    String getDefaultCryptPrefix(String cryptType)
    {
        String result;
        
        if (CRYPT_TYPE_CRYPT.equalsIgnoreCase(cryptType))
        {
            result = CRYPT_PREFIX_CRYPT;
        }
        else if (CRYPT_TYPE_MD5.equalsIgnoreCase(cryptType))
        {
            result = CRYPT_PREFIX_MD5;
        }
        else if (CRYPT_TYPE_CLEAR.equalsIgnoreCase(cryptType))
        {
            result = CRYPT_PREFIX_DEFAULT;
        }
        else
        {
            Log.log.error("Invalid crypt type: "+cryptType, new Exception());
            result = CRYPT_PREFIX_DEFAULT;
        }
        
        return result;
    } // getDefaultCryptPrefix
    
 
} // LDAP
