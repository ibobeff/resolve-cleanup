//C:\project\resolve\src\rsview\war\WEB-INF\lib
package com.resolve.auth;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sourceforge.jradiusclient.RadiusAttribute;
import net.sourceforge.jradiusclient.RadiusAttributeValues;
import net.sourceforge.jradiusclient.RadiusClient;
import net.sourceforge.jradiusclient.RadiusPacket;
import net.sourceforge.jradiusclient.exception.InvalidParameterException;
import net.sourceforge.jradiusclient.exception.RadiusException;
import net.sourceforge.jradiusclient.util.ChapUtil;

public class RADIUS
{
    boolean active                  = false;
    String primaryRadiusServerIpAddress    ="";
    String secondaryRadiusServerIpAddress    ="";
    String authProtocol;
    String authPort;
    String accntPort;
    String sharedSecret;
    boolean grouprequired;
    public Map<String,Object> authResponseAttribute = new HashMap<String, Object>();
    public boolean isClientInitialised = false;

    public RADIUS(String primaryRadiusServer, String secondaryRadiusServer, String authProtocol, String authPort, String accntPort, String sharedSecret) throws Exception
    {
        this.primaryRadiusServerIpAddress=primaryRadiusServer;
        this.secondaryRadiusServerIpAddress=secondaryRadiusServer;
        this.authProtocol=authProtocol;
        this.authPort=authPort;
        this.accntPort=accntPort;
        this.sharedSecret=sharedSecret;
    }
    
    public Map<String, Object> authenticateUser(String userName, String userPass)
    {
        RadiusClient radiusClient = null;
        int authPort;
        int accntPort;
        try {
         authPort= Integer.parseInt(this.authPort);
         accntPort=Integer.parseInt(this.accntPort);
        }
        catch(Exception e) {
            Log.log.error("Please provide valid port number for authPort/accntPort");
            throw e;
        }
   

        Log.log.info("Initialising radius client for primary host...");
        if ((!(this.primaryRadiusServerIpAddress == null)) || StringUtils.isNotEmpty(this.primaryRadiusServerIpAddress))
        {
            radiusClient = initRadiusClient(this.primaryRadiusServerIpAddress, authPort, accntPort, this.sharedSecret);
        }
        else
        {
            Log.log.error("Please provide primary Radius Server IpAddress.");
            return this.authResponseAttribute;

        }

        if (radiusClient == null && (!(this.secondaryRadiusServerIpAddress == null)) || StringUtils.isNotEmpty(this.secondaryRadiusServerIpAddress))
        {

            Log.log.error(" Failed to connect to primary RADIUS server. Initialising connection to secondary RADIUS server...");
            // init secondary client
            radiusClient = initRadiusClient(this.secondaryRadiusServerIpAddress, authPort, accntPort, this.sharedSecret);
            
        }
        if (radiusClient != null)
        {
          
          
            RadiusPacket authResponsePacket = authenticateUser(radiusClient, this.authProtocol, userName, userPass, this.sharedSecret);
            // get authetication response attributes
            if (authResponsePacket != null)
            {
                this.authResponseAttribute = getAuthAttributes(authResponsePacket, userName);
            }
            else if (this.authResponseAttribute.size() == 0)
            {
                Log.log.error("Response packet is not received from authentication request..");
         
            }
        }

        else
        {

            Log.log.error(" Failed to connect to primary RADIUS server as well as secondary RADIUS server");
            return this.authResponseAttribute;
        }
        return this.authResponseAttribute;
    }
    
    public RadiusClient initRadiusClient(String host, int authport, int acctport, String sharedSecret)
    {
        RadiusClient radiusClient = null;

        try
        {
            Log.log.info("Instantiating JRadius client to host: " + host);

            radiusClient = new RadiusClient(host, authport, acctport, sharedSecret);

            this.isClientInitialised = true;
            Log.log.debug("Initialised Radius Client successfully to host: " + host);

        }
        catch (RadiusException rex)
        {
            Log.log.error("Unable to initialise Radius Client to host " + host);
            Log.log.error(rex.getMessage());

        }
        catch (InvalidParameterException ivpex)
        {
            Log.log.error("Unable to initialise Radius Client to host " + host);
            Log.log.error(ivpex.getMessage());

        }
        return radiusClient;
    }

    public RadiusPacket authenticateUser(RadiusClient radiusClient, String authMethod, String userName, String userPass, String sharedScecret)
    {
        RadiusPacket accessResponse = null;
        try
        {
      
            RadiusPacket accessRequest = new RadiusPacket(RadiusPacket.ACCESS_REQUEST);
            RadiusAttribute userNameAttribute = new RadiusAttribute(RadiusAttributeValues.USER_NAME, userName.getBytes());
            accessRequest.setAttribute(userNameAttribute);
 
            if (authMethod.equalsIgnoreCase("PAP"))
            {
                accessRequest.setAttribute(new RadiusAttribute(RadiusAttributeValues.USER_PASSWORD, userPass.getBytes()));
            }
            else
            {
                Log.log.error("Authetication method " + authMethod + " is not supported. Should be PAP");
                return null;
            }

            Log.log.info("Performing basic authentication...");

            accessResponse = radiusClient.authenticate(accessRequest);
            

        }
        catch (InvalidParameterException ivpex)
        {
            Log.log.error(ivpex.getMessage());
            ivpex.printStackTrace();
        }
        catch (RadiusException rex)
        {
            Log.log.error(rex.getMessage());
        }
        return accessResponse;
    }

    private static byte[] chapEncrypt(final String plainText, final byte[] chapChallenge)
    {
        // see RFC 2865 section 2.2
        byte chapIdentifier = '0'; // chapUtil.getNextChapIdentifier();//tim not
                                   // tested this
        byte[] chapPassword = new byte[17];
        chapPassword[0] = chapIdentifier;
        System.arraycopy(ChapUtil.chapEncrypt(chapIdentifier, plainText.getBytes(), chapChallenge), 0, chapPassword, 1, 16);
        return chapPassword;
    }

    @SuppressWarnings("rawtypes")
    public Map<String, Object> getAuthAttributes(RadiusPacket rp, String userName)
    {

        Map<String, Object> responseAttributes = new HashMap<String, Object>();

        try
        {
            switch (rp.getPacketType())
            {
                case RadiusPacket.ACCESS_ACCEPT:
                    responseAttributes.put("status", "Autheticated");
                    Log.log.info("User " + userName + " Authenticated");
                    break;
                case RadiusPacket.ACCESS_REJECT:
                    Log.log.warn("User " + userName + " NOT authenticated");
                    break;
                case RadiusPacket.ACCESS_CHALLENGE:
                    String reply;

                    reply = new String(rp.getAttribute(RadiusAttributeValues.REPLY_MESSAGE).getValue());
                    responseAttributes.put("status", " Challenged with " + reply);

                    Log.log.warn("User " + userName + " Challenged with " + reply);
                    break;
                default:
                    responseAttributes.put("status", "Packet type received in response is invalid");
                    Log.log.warn("Packet type received in response is invalid" + rp.getPacketType());
                    break;

            }
           
            Iterator attributes = rp.getAttributes().iterator();
            RadiusAttribute tempRa;
            String userGroup = new String();
            String userDomain = new String();
            while (attributes.hasNext())
            {
                tempRa = (RadiusAttribute) attributes.next();
                System.out.println("Type: "+tempRa.getType());
                System.out.println("Value: "+new String(tempRa.getValue()));
                if (tempRa.getType() == 1) // username attribute is 1
                {
                    responseAttributes.put("username", new String(tempRa.getValue()));

                }
                if (tempRa.getType() == 26) // cisco av pair attribute is 26
                                            // that return group
                {
                    String group = new String(tempRa.getValue());
                    System.out.println("Returned value of group is: "+group);

                    // below code removes nonprintable ascii characters from the
                    // byte array
                    char[] oldChars = new char[group.length()];
                    group.getChars(0, group.length(), oldChars, 0);
                    char[] newChars = new char[group.length()];
                    int newLen = 0;
                    for (int j = 0; j < group.length(); j++)
                    {
                        char ch = oldChars[j];
                        if (ch >= ' ')
                        {
                            newChars[newLen] = ch;
                            newLen++;
                        }
                    }
                    //group value after removing nonprintable ascii chars from the byte array
                    group = new String(newChars, 0, newLen);

                    String[] groupTokens = group.split(",");
                    for (String groupName : groupTokens)
                    {
                        //
                        if (groupTokens.length == 1) //internal users in ACS just returns the group name and no domain name. e.g Test
                        {
                          
                            responseAttributes.put(Constants.MEMBEROF, group);
                        }
                        else if (groupName.contains("CN=")  )// AD users return group value as e.g 1CN=RBA-13759,OU=OrgUnit,DC=resolvetest-3,DC=com
                        {
                            if (groupName.contains("CN="))
                            {

                                String[] groupList = groupName.split("=");

                                if (!userGroup.isEmpty())
                                {
                                    userGroup = userGroup.concat("," + groupList[1]);

                                }
                                else
                                {
                                    userGroup = userGroup.concat(new String(groupList[1]));

                                }
                                responseAttributes.put(Constants.MEMBEROF, userGroup);

                            }

                            if (groupName.contains("DC="))
                            {

                                String[] domainControllerList = groupName.split("=");

                                if (!userDomain.isEmpty())
                                {
                                    if (!domainControllerList[1].equalsIgnoreCase("com"))
                                    {
                                        userDomain = userDomain.concat("," + domainControllerList[1]);

                                    }
                                    responseAttributes.put("domain", userDomain);
                                }
                                else
                                {
                                    if (!domainControllerList[1].equalsIgnoreCase("com"))
                                    {
                                        userDomain = userDomain.concat(new String(domainControllerList[1]));
                                        responseAttributes.put("domain", userDomain);

                                    }
                                }

                            }

                        }
                        //below else is for the internal users group. It is assumed that for ACS internal user group returned will be just comma seperated list of groups
                        //as of now only one group can be assigned to the user in ACS5.8/ACS5.3(Telus using 5.3)
                        else  if(!group.contains("=")){
                            responseAttributes.put(Constants.MEMBEROF, group);

                          
                        }

                    }

                }
                // 25: (Accounting) Arbitrary value that the network access
                // server includes in all accounting packets for this user if
                // supplied by the RADIUS server.
            }
        }
        catch (InvalidParameterException e)
        {
            responseAttributes.put("status", e.getMessage());
            Log.log.error(e.getMessage(), e);
        }
        catch (RadiusException e)
        {
            responseAttributes.put("status", e.getMessage());
            Log.log.error(e.getMessage(), e);
        }

        return responseAttributes;

    }

}
