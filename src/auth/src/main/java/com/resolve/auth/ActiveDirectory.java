/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.auth;

import java.io.File;
import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPAttributeSet;
import com.novell.ldap.LDAPBindHandler;
import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPJSSESecureSocketFactory;
import com.novell.ldap.LDAPReferralException;
import com.novell.ldap.LDAPSearchConstraints;
import com.novell.ldap.LDAPSearchResults;
import com.novell.ldap.LDAPUrl;
import com.resolve.util.Constants;
import com.resolve.util.ERR;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ActiveDirectory
{
    LDAPConnection conn;
    
    public final static String AUTH_MODE_BIND = "BIND";            // uses CN=username,...
    public final static String AUTH_MODE_USERNAME = "USERNAME";    // uses <uidAttribute>=username, no password validation

    int version = LDAPConnection.LDAP_V3;
    List<String> baseDNList;
    String defaultDomain = "";
    String uidAttribute = "sAMAccountName";
    String host;
    int port;
    String bindDN;
    String bindPas_sword;
    String mode;
    boolean ssl = false;
    
    boolean active = false;
    boolean sync = false;
    boolean fallback = false;
    String domain = null;
    
    Properties syncProperties;
    
    private String memberofLookupKey = Constants.MEMBEROF_DEFAULT;

    /**
     * @param args
     */
    /*
    public static void main(String[] args) throws Exception
    {
        String username = args[0];
        String password = args[1];

        // init ldap auth
        List<String> baseDNList = new ArrayList<String>();
        baseDNList.add("dc=resolve,dc=com");

        // establish connection
        String host = "172.17.1.253";
        int port = LDAPConnection.DEFAULT_PORT;
        
        File syncFile = new File("ldap.properties");

        ActiveDirectory ldap = new ActiveDirectory(host, port, baseDNList, false, "BIND", "", "", syncFile);
        Map user = ldap.authenticate(username, password, null);
        System.out.println("user: " + user);
    } // main
    */

    public ActiveDirectory(String host, int port, List<String> baseDNList, boolean ssl, String mode, String bindDN, String bindPas_sword, File syncFile) throws Exception
    {
        this.ssl = ssl;
        resetConnection();

        this.host = host;
        this.port = port;
        this.baseDNList = baseDNList;
        
        // sync mapping file
        if (syncFile != null)
        {
	        this.syncProperties = new Properties();
            FileInputStream fis = new FileInputStream(syncFile);
	        this.syncProperties.load(fis);
	        fis.close();
        }
        else
        {
            this.syncProperties = null;
        }
        
        this.mode = mode.toUpperCase();
        this.bindDN = bindDN;
        this.bindPas_sword = bindPas_sword;
    } // ActiveDirectory
    
    public ActiveDirectory(String host, int port, List<String> baseDNList, boolean ssl, String mode, String bindDN, String bindPas_sword, Properties syncProp) throws Exception
    {
        this.ssl = ssl;
        resetConnection();

        this.host = host;
        this.port = port;
        this.baseDNList = baseDNList;
        this.mode = mode.toUpperCase();
        this.bindDN = bindDN;
        this.bindPas_sword = bindPas_sword;
        
        // sync mapping file
        this.syncProperties = syncProp; 
        
        //the key is coming from the definition that is set 
        memberofLookupKey = syncProperties.getProperty(Constants.MEMBEROF);
        if(StringUtils.isEmpty(memberofLookupKey))
        {
            memberofLookupKey = Constants.MEMBEROF_DEFAULT;
        }
    } // ActiveDirectory
    
    public int getVersion()
    {
        return version;
    } // getVersion

    public void setVersion(int version)
    {
        this.version = version;
    } // setVersion

    public String getDefaultDomain()
    {
        return defaultDomain;
    }

    public void setDefaultDomain(String defaultDomain)
    {
        this.defaultDomain = defaultDomain;
    }

    public String getUIDAttribute()
    {
        return uidAttribute;
    } // getUidAttribute

    public void setUIDAttribute(String uidAttribute)
    {
        this.uidAttribute = uidAttribute;
    } // setUIDAttribute

    public boolean connect()
    {
        boolean result = false;

        try
        {
            if (conn != null)
            {
                // connect to active directory server
                Log.log.info("Connecting to ActiveDirectory");
                conn.connect(host, port);

                Log.log.info("ActiveDirectory Connect successfully to host " + host + " and port " + port);
                result = true;
            }
        }
        catch (Throwable e)
        {
            Log.alert(ERR.E60001);
            resetConnection();
        }

        return result;
    } // connect

    public void disconnect()
    {
        try
        {
            if (conn != null)
            {
                conn.disconnect();
            }
        }
        catch (LDAPException e)
        {
            Log.log.error("Failed to disconnect: "+e.getMessage());
            resetConnection();
        }
    } // disconnect

    public boolean isConnected()
    {
        boolean result = false;
        if (conn != null)
        {
            result = conn.isConnected();
        }
        return result;
    } // isConnected
    
    public void resetConnection()
    {
        this.conn = null;
        if (ssl)
        {
            Log.log.info("Using Secure AD connection");
            LDAPJSSESecureSocketFactory socketFactory = new LDAPJSSESecureSocketFactory();
            this.conn = new LDAPConnection(socketFactory);
        }
        else
        {
            Log.log.info("Using standard AD connection");
            this.conn = new LDAPConnection();
        }
    } //resetConnection()
    
    public synchronized Map authenticateUsingBindDN(String username)
    {
        return authenticate(username, null, AUTH_MODE_USERNAME);
    } // authenticateUsingBindDN

    public synchronized Map authenticate(String username, String p_assword, String mode)
    {
        Map result = null;

        String foundDN = null;
        SimpleBindHandler sbh = null;

        try
        {
            if (StringUtils.isEmpty(mode))
            {
                mode = this.mode;
            }
            if (StringUtils.isEmpty(p_assword) && !AUTH_MODE_USERNAME.equals(mode))
            {
                //password required
                Log.log.warn("No Password Sent, Failing Authentication");
            }
            else if (connect())
            {
                // init uid and domain
                String uid = null;
                String domain = null;
                String bindUsername;
                String bindUID = null;;
                String userPas_sword;
                
                int pos = username.indexOf('\\');
                if (pos > 0)
                {
                    uid = username.substring(pos + 1, username.length());
                }
                else
                {
                    pos = username.indexOf('@');
                    if (pos > 0)
                    {
                        uid = username.substring(0, pos);
                    }
                    else
                    {
                        uid = username;
                    }
                } 
                
                if (AUTH_MODE_USERNAME.equals(mode))
                {
                    bindUsername = bindDN;
                    userPas_sword = bindPas_sword;
                }
                else
                {
                    bindUsername = username;
                    userPas_sword = p_assword;
                }

                pos = bindUsername.indexOf('\\');
                if (pos > 0)
                {
                    domain = bindUsername.substring(0, pos);
                    bindUID = bindUsername.substring(pos + 1, bindUsername.length());
                }
                else
                {
                    pos = bindUsername.indexOf('@');
                    if (pos > 0)
                    {
                        bindUID = bindUsername.substring(0, pos);
                        domain = bindUsername.substring(pos + 1, bindUsername.length());
                    }
                    else
                    {
                        if (!StringUtils.isEmpty(defaultDomain))
                        {
                            Log.log.info("Using default domain: " + defaultDomain);
                            bindUID = bindUsername;
                            domain = defaultDomain;
                        }
                        else
                        {
                            Log.log.info("Only the following username formats are supported: name@domain.com or domain.com\name");
                        }
                    }
                }

                Log.log.info("Bind Login User: " + bindUID + "@" + domain);
                sbh = new SimpleBindHandler(bindUID + "@" + domain, userPas_sword);
                LDAPSearchConstraints constraints = conn.getSearchConstraints();
                constraints.setReferralFollowing(true);
                constraints.setReferralHandler(sbh); 
                conn.setConstraints(constraints); 
                boolean bind = bind(bindUID + "@" + domain, userPas_sword, conn, version);
                
                if (uid != null && domain != null && bind)
                {
                    // search using uidAttribute (sAMAccountName)
                    String uidAttr = this.uidAttribute;
                    for (String baseDN : baseDNList)
                    {
                        Log.log.info("ActiveDirectory searching uidAttr: "+uidAttr+" user: " + uid + " in dn: " + baseDN);
                        LDAPSearchResults searchResults = conn.search(baseDN, LDAPConnection.SCOPE_SUB, "(" + uidAttr + "=" + uid + ")", null, false);
                        if (searchResults.hasMore())
                        {
                            LDAPEntry ldapEntry = searchResults.next();
    
                            Log.log.debug("ActiveDirectory getting user attributes");
                            result = getUserAttributes(ldapEntry);
                            break;
                        }
                    }
                    
                    // try again using userPrincipalName and uid@domain
                    if (result == null)
                    {
                        uidAttr = "userPrincipalName";                        
                        uid = uid+"@"+domain;
                        
                        for (String baseDN : baseDNList)
                        {
                            Log.log.info("ActiveDirectory searching uidAttr: "+uidAttr+" user: " + uid + " in dn: " + baseDN);
                            LDAPSearchResults searchResults = conn.search(baseDN, LDAPConnection.SCOPE_SUB, "(" + uidAttr + "=" + uid + ")", null, false);
                            if (searchResults.hasMore())
                            {
                                LDAPEntry ldapEntry = searchResults.next();
        
                                Log.log.debug("ActiveDirectory getting user attributes");
                                result = getUserAttributes(ldapEntry);
                                break;
                            }
                        }

                    }
                    
                    if (result == null)
                    {
                        Log.log.warn("ActiveDirectory search user failed");
                    }
                }
                else
                {
                    Log.log.warn("Failed to bind to ActiveDirectory for uid: " + uid + " domain: " + domain);
                }

                if (result != null)
                {
                    Log.log.info("ActiveDirectory authentication successful: " + username);
                }
                else
                {
                    Log.log.warn("ActiveDirectory authentication failed: " + username);
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.warn("ActiveDirectory Authentication failed: " + e.getMessage(), e);
        }
        finally
        {
            if (sbh != null)
            {
                sbh.disconnect();
            }
            disconnect();
        }

        return result;
    } // authenticate

    Map getUserAttributes(LDAPEntry ldapEntry)
    {
        Map result = null;
        if (ldapEntry != null)
        {
            result = new HashMap();

            String foundDN = ldapEntry.getDN();

            LDAPAttributeSet attributeSet = ldapEntry.getAttributeSet();
            Iterator allAttributes = attributeSet.iterator();

            while (allAttributes.hasNext())
            {
                LDAPAttribute attribute = (LDAPAttribute) allAttributes.next();
                String attributeName = attribute.getName().toLowerCase();
                
                Object value = "";
                Log.log.trace("attributeName: "+attribute.getName());
                Log.log.trace("  attributeSize: "+attribute.size());
                Log.log.trace("  stringValue: "+attribute.getStringValue());
                Log.log.trace("  stringValues: "+attribute.getStringValues());
                //if (attributeName.equals("memberof"))
                if (attributeName.equals(memberofLookupKey))
                {
                    if (attribute.size() > 1)
                    {
                        value = Collections.list(attribute.getStringValues()); 
                    }
                    else
                    {
                        value = new ArrayList<String>();
                        ((List<String>)value).add(attribute.getStringValue());
                    }
                }
                else if (attribute.size() > 1)
                {
	                value = Collections.list(attribute.getStringValues()); 
                }
                else
                {
                    value = attribute.getStringValue();
                }
                
                Log.log.trace("  value: "+value); 
                result.put(attributeName, value);
            }
            result.put("dn", foundDN);
        }

        return result;
    } // getUserAttributes

    boolean bind(String bindDN, String bindPassword, LDAPConnection lc, int ldapVersion)
    {
        boolean bound = false;

        if (bindDN != null && bindDN.length() > 0 && bindPassword != null)
        {
            try
            {
                Log.log.debug("ActiveDirectory Bind user: " + bindDN);
                lc.bind(ldapVersion, bindDN, bindPassword.getBytes("UTF8"));
                bound = true;
                Log.log.debug("ActiveDirectory Bind successful");
            }
            catch (Exception e)
            {
                Log.log.warn("ActiveDirectory Bind failed: " + e.getMessage(), e);
            }
        }
        else
        {
            Log.log.debug("ActiveDirectory Bind does not have necessary info");
        }
        return bound;
    } // bind

    public Properties getSyncProperties()
    {
        return syncProperties;
    }
    
    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }
    
    public void setSyncProperties(Properties syncProperties)
    {
        this.syncProperties = syncProperties;
    }

    public boolean isSync()
    {
        return sync;
    }

    public void setSync(boolean sync)
    {
        this.sync = sync;
    }

    public boolean isFallback()
    {
        return fallback;
    }

    public void setFallback(boolean fallback)
    {
        this.fallback = fallback;
    }

    public String getDomain()
    {
        return domain;
    }

    public void setDomain(String domain)
    {
        this.domain = domain;
    }
    private class SimpleBindHandler implements LDAPBindHandler
    {
        // A list of the connections being used for following referrals
        // It is hashed on host:port
        private HashMap<String, LDAPConnection> connections;

        private String referralDN;
        private String referralPassword;
        
        private SimpleBindHandler(String referralDN, String referralPassword)
        {
            connections = new HashMap<String, LDAPConnection>();
            this.referralDN = referralDN;
            this.referralPassword = referralPassword;
        }
        
        public void disconnect()
        {
            if (connections != null)
            {
                for (String host : connections.keySet())
                {
                    try
                    {
                        LDAPConnection conn = connections.get(host);
                        conn.disconnect();
                    }
                    catch (LDAPException e)
                    {
                        Log.log.error("Failed to disconnect referral connection: "+e.getMessage(), e);
                    }
                }
            }
            connections = null;
        } // disconnect
        
        /**
         * the bind method is called by the API.  This method is
         * responsible to create an authenticated connection that
         * the API can use to follow the referral.
         */
        public LDAPConnection bind(String[] urls, LDAPConnection conn) throws LDAPReferralException
        {
            LDAPConnection newconn = null;
            LDAPUrl url;
            String host;
            
            // Check if original connection is in the hash table, if not add it
            host = conn.getHost() + ":" + conn.getPort();
            Log.log.info("Bind: Referral from " + host);
            if (!connections.containsKey(host))
            {
                connections.put(host, conn);
            }
            
            // Report what URLs were received on referral/reference
            // and what Connection the referrals originated from.
            Log.log.debug("Bind: Referral contains " + urls.length + " URLs");
            if (Log.log.isTraceEnabled())
            {
                for (int i = 0; i < urls.length; i++)
                {
                    Log.log.trace("Referral URL: " + urls[i]);
                }
            }
            
            // Check if any URL matches a known authenticated connection
            for (int i = 0; i < urls.length; i++)
            {
                try
                {
                    url = new LDAPUrl(urls[i]);
                    host = url.getHost() + ":" + conn.getPort();
                    if (connections.containsKey(host))
                    {
                        newconn = (LDAPConnection) connections.get(host);
                        Log.log.debug("Bind: Using existing connection to host " + host);
                        break;
                    }
                }
                catch (Exception e)
                {
                    newconn = null;// just try the next one
                    Log.log.warn("Failed connecting to host : " + urls[i], e);
                }
            }
            // We found no existing connections, so make a new one
            if (newconn == null)
            {
                newconn = new LDAPConnection(conn.getSocketFactory());
                LDAPSearchConstraints cons = conn.getSearchConstraints();
                newconn.setConstraints(cons);
                
                // Disallow referral following, and set timelimit on bind
                cons.setReferralFollowing(false);
                
                for (int i = 0; i < urls.length; i++)
                {
                    try
                    {
                        url = new LDAPUrl(urls[i]);
                    }
                    catch( MalformedURLException murle)
                    {
                        Log.log.warn("Bad URL in references: " + urls[i], murle);
                        continue; // bad url, move to next
                    }

                    host = url.getHost();
                    try
                    {
                        // Connect to server.
                        // Note: if host is format host:port, then the port
                        // parameter is ignored.
                        newconn.connect(host, conn.getPort());
                        if (conn.isTLS())
                        {
                            newconn.startTLS();
                        }
                        // Skip bind, if anonymous
                        newconn.bind(version, referralDN, referralPassword.getBytes("UTF8"));

                        connections.put(host, newconn);
                        break;
                    }
                    catch (Throwable t)
                    {
                        // Bind operation did not succeed.
                        //Note: connect also disconnects any previous connection
                        Log.log.error("Exception connecting and binding to referral at " + host, t);
                        try
                        {
                            newconn.disconnect();
                        }
                        catch (LDAPException ldape)
                        {
                            Log.log.error("Failed to disconnect referral connection", ldape);
                        }
                        newconn = null; // Let GC clean up connection object
                        if (t instanceof LDAPReferralException)
                        {
                            // Already an LDAPReferralException
                            throw (LDAPReferralException) t;
                        }
                        else
                        {
                            // Set up an appropriate LDAPReferralException
                            // When thrown to the API, the API sets referral list
                            LDAPReferralException ldapre = new LDAPReferralException("Referral Exception: Could not follow referrals", t);
                            ldapre.setFailedReferral(urls[i]);
                            throw ldapre;
                        }
                    }
                }
            }
            return newconn;
        }
    }

} // ActiveDirectory
