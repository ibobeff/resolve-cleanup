/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.hpom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.HPOMConstants;
import com.resolve.gateway.MHPOM;
import com.resolve.query.ResolveQuery;
import com.resolve.query.translator.HPOMQueryTranslator;
import com.resolve.rsremote.ConfigReceiveHPOM;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;

public class HPOMGateway extends BaseClusteredGateway
{
    // Singleton
    private static volatile HPOMGateway instance = null;

    // This will be set in initialize() with the proper queue name from the
    // configuration.
    private String queue = "HPOM";

    // Following values come from the blueprint.properties.
    // Every subclass who needs the functionality must initialize
    // the values in its initialize method.
    private String resolveStatusField = "RESOLVE_STATUS";
    private boolean updateResolveStatus = false;
    private String resolveStatusValue = "1";
    private String runbookIdField = "RESOLVE_RUNBOOKID";
    private boolean updateRunbookId = false;

    public static String RECEIVED_TIME = "ReceivedTime";

    public static volatile ObjectService eventService;

    private final ResolveQuery resolveQuery;

    private final ConfigReceiveHPOM hpomConfiguration;

    public static HPOMGateway getInstance(ConfigReceiveHPOM config)
    {
        if (instance == null)
        {
            instance = new HPOMGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static HPOMGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("HPOM Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    /**
     * Private constructor.
     *
     * @param config
     */
    private HPOMGateway(ConfigReceiveHPOM config)
    {
        // Important, call super here.
        super(config);
        hpomConfiguration = config;
        queue = config.getQueue();
        resolveQuery = new ResolveQuery(new HPOMQueryTranslator());
    }

    @Override
    public String getLicenseCode()
    {
        return "HPOM";
    }

    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_HPOM;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MHPOM.class.getSimpleName();
    }

    @Override
    protected Class<MHPOM> getMessageHandlerClass()
    {
        return MHPOM.class;
    }

    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    /**
     * Initializes the specific gateway resources.
     *
     * @param config
     */
    @Override
    protected void initialize()
    {
        ConfigReceiveHPOM hpomConfig = (ConfigReceiveHPOM) configurations;

        queue = hpomConfig.getQueue().toUpperCase();
        interval = hpomConfig.getInterval() * 1000;
        this.gatewayConfigDir = "/config/hpom/";

        resolveStatusField = hpomConfig.getStatus_fieldname();
        updateResolveStatus = hpomConfig.isStatus_active();
        resolveStatusValue = "" + hpomConfig.getStatus_process();

        runbookIdField = hpomConfig.getRunbookid_fieldname();
        updateRunbookId = hpomConfig.isRunbookid_active();

        eventService = ObjectServiceFactory.getObjectService(hpomConfig, EventService.OBJECT_IDENTITY);
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new HPOMFilter((String) params.get(HPOMFilter.ID), (String) params.get(HPOMFilter.ACTIVE), (String) params.get(HPOMFilter.ORDER), (String) params.get(HPOMFilter.INTERVAL), (String) params.get(HPOMFilter.EVENT_EVENTID)
        // , (String) params.get(HPOMFilter.PERSISTENT_EVENT)
                        , (String) params.get(HPOMFilter.RUNBOOK), (String) params.get(HPOMFilter.SCRIPT), (String) params.get(HPOMFilter.OBJECT), (String) params.get(HPOMFilter.QUERY));
    } // getFilter

    @Override
    public void start()
    {
        Log.log.warn("Starting HPOMListener");
        super.start();
    } // start

    @Override
    public void run()
    {
        //This call is must for all the ClusteredGateways.
        super.sendSyncRequest();

        ConfigReceiveHPOM hpomConfig = (ConfigReceiveHPOM) configurations;

        String resolveStatusField = hpomConfig.getStatus_fieldname();
        String resolveStatusValue = "" + hpomConfig.getStatus_process();

        String currentServerDateTime = DateUtils.getCurrentDateTimeAsXmlString();

        while (running)
        {
            try
            {
                long startTime = System.currentTimeMillis();

                /* if any remaining events are in primary data queue or 
                 * primary data executor has waiting messages still to be processed, 
                 * do not get messages for more new events.
                 */
                
                if (primaryDataQueue.isEmpty() && !primaryDataQueueExecutorBusy())
                {
                    Log.log.trace("HPOM Gateway Local Event Queue is empty and Primary Data Queue Executor is free.....");
                    for (Filter filter : orderedFilters)
                    {
                        if (shouldFilterExecute(filter, startTime))
                        {
                            HPOMFilter hpomFilter = (HPOMFilter) filter;

                            // First time set it with the server's date time.
                            if (StringUtils.isEmpty(hpomFilter.getLastReceiveDate()))
                            {
                                hpomFilter.setLastReceiveDate(currentServerDateTime);
                            }

                            List<Map<String, String>> results = invokeHPOMService(hpomFilter);
                            if (results.size() > 0)
                            {
                                // Let's execute runboook for each incident
                                for (Map<String, String> hpomEvent : results)
                                {
                                    boolean processFilter = true;
                                    if (updateResolveStatus)
                                    {
                                        if (hpomEvent.containsKey(hpomConfig.getStatus_fieldname()))
                                        {
                                            // if the resolve status value equal
                                            // to the expected value don't
                                            // process the filter it.
                                            // this is happening because HPOM
                                            // don't allow a way to query based
                                            // on custom field's
                                            // value null (e.g.,
                                            // RESOLVE_STATUS=null is not
                                            // allowed).
                                            if (hpomEvent.get(hpomConfig.getStatus_fieldname()).equalsIgnoreCase(resolveStatusValue))
                                            {
                                                processFilter = false;
                                            }
                                        }
                                    }

                                    if (processFilter)
                                    {
                                        //processFilter(hpomFilter, null, hpomEvent);
                                        addToPrimaryDataQueue(hpomFilter, hpomEvent);
                                    }
                                }
                                // Update the lastReceivedDate property. Add a
                                // second to it, milliseconds would have
                                // been better but HPOM only deals with seconds.
                                String lastReceivedTime = (String) results.get(results.size() - 1).get(RECEIVED_TIME);
                                XMLGregorianCalendar cal = XMLGregorianCalendarImpl.parse(lastReceivedTime);
                                cal.setSecond(cal.getSecond() + 1);
                                hpomFilter.setLastReceiveDate(cal.toXMLFormat());
                            }
                        }
                    }
                }

                // process events collected by processFilter.
                //processFilterEvents(startTime);

                // If there is no filter yet no need to keep spinning, wait for
                // a while
                if (orderedFilters.size() == 0)
                {
                    Thread.sleep(interval);
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Error: " + e.getMessage(), e);
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", 
                          "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + 
                          "due to " + e.getMessage());
                
                try
                {
                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    } // run

    /**
     * This method invokes the HPOM web service.
     *
     * @param filter
     * @return - Service response or null based on filter condition.
     * @throws Exception
     */
    private List<Map<String, String>> invokeHPOMService(HPOMFilter filter) throws Exception
    {
        if (filter.getNativeQuery() == null)
        {
            try
            {
                String nativeQuery = resolveQuery.translate(filter.getQuery());
                filter.setNativeQuery(nativeQuery); // set it so we don't
                                                    // attempt to translate
                                                    // again.
            }
            catch (Exception e)
            {
                Log.log.debug("Wrong RSQL syntaxt for HPOM gateway, please check. Query provided \"" + filter.getQuery() + "\".");
            }
        }

        String nativeQuery = filter.getNativeQuery();

        boolean hasVariable = VAR_REGEX_GATEWAY_VARIABLE.matcher(filter.getNativeQuery()).find();
        if (hasVariable)
        {
            if (filter.getNativeQuery().contains(HPOMFilter.LAST_RECEIVE_DATE))
            {
                nativeQuery = filter.getNativeQuery().replaceFirst(Constants.VAR_REGEX_GATEWAY_VARIABLE, filter.getLastReceiveDate());
            }
        }

        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        if (EventService.OBJECT_IDENTITY.equalsIgnoreCase(filter.getObject()))
        {
            // try to get all by passing -1
            result = eventService.search(nativeQuery, 1000, null, null);
        }

        return result;
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
        if (result.containsKey(HPOMConstants.FIELD_TITLE))
        {
            event.put(Constants.EXECUTE_REFERENCE, result.get(HPOMConstants.FIELD_TITLE));
        }
        if (result.containsKey(HPOMConstants.FIELD_EVENT_ID))
        {
            event.put(Constants.EXECUTE_ALERTID, result.get(HPOMConstants.FIELD_EVENT_ID));
        }
    }

    /*
     * Public API method implementation goes here
     */
    public String createEvent(String nodeIPAddress, Map<String, String> params, String username, String password) throws Exception
    {
        return eventService.create(nodeIPAddress, params, username, password);
    }

    public void updateEvent(String sysId, Map<String, String> params, String username, String password) throws Exception
    {
        eventService.update(sysId, params, username, password);
    }

    public void closeEvent(String incidentId, String username, String password) throws Exception
    {
        eventService.close(incidentId, username, password);
    }

    public Map<String, String> selectEventById(String eventId, String username, String password) throws Exception
    {
        return eventService.selectByEventId(eventId, username, password);
    }

    public List<Map<String, String>> searchEvent(String query, int maxResult, String username, String password) throws Exception
    {
        if (StringUtils.isNotEmpty(query))
        {
            Log.log.debug("Incoming HPOM Query: " + query);

            try
            {
                query = resolveQuery.translate(query);
                Log.log.debug("Translated HPOM Query: " + query);
            }
            catch (Exception e)
            {
                Log.log.debug("Did not translate, the query \"" + query + "\" is treated as native.");
            }
        }
        return eventService.search(query, maxResult, username, password);
    }

    @Override
    public void updateServerData(Filter filter, Map content)
    {
        if (content.containsKey(HPOMConstants.FIELD_EVENT_ID))
        {
            String eventId = (String) content.get(HPOMConstants.FIELD_EVENT_ID);
            Log.log.debug("HPOM event ID is:" + eventId);
            Map<String, String> params = new HashMap<String, String>();

            // update custom status field
            if (updateResolveStatus)
            {
                String statusFieldValue = String.valueOf(hpomConfiguration.getStatus_process());
                if (content.containsKey(resolveStatusField))
                {
                    statusFieldValue = (String) content.get(resolveStatusField);
                }
                params.put(resolveStatusField, statusFieldValue);
            }
            if (updateRunbookId)
            {
                String value = filter.getRunbook();

                if (content.containsKey(runbookIdField))
                {
                    value = (String) content.get(runbookIdField);
                }
                params.put(runbookIdField, value);
            }
            if (params.size() > 0)
            {
                try
                {
                    updateEvent(eventId, params, null, null);
                }
                catch (Exception e)
                {
                    Log.log.error("Error updating the HPOM event: " + eventId, e);
                }
            }
        }
    }
}
