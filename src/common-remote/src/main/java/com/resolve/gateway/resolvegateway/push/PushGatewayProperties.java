package com.resolve.gateway.resolvegateway.push;

import java.util.HashMap;
import java.util.Map;

import com.resolve.gateway.resolvegateway.GatewayProperties;

public class PushGatewayProperties extends GatewayProperties
{
    protected Integer httpPort;
    protected String httpUser;
    protected String httpPass;
    
    protected Boolean httpSsl;
    protected String sslCertificate ;
    protected String sslPassword;
    
    protected Integer executeTimeout = 120;
    protected Integer scriptTimeout = 120;
    protected Integer waitTimeout = 120;
    
    protected Map<String, Object> properties = new HashMap<String, Object>();

    public Integer getHttpPort()
    {
        return httpPort;
    }

    public void setHttpPort(Integer httpPort)
    {
        this.httpPort = httpPort;
    }

    public String getHttpUser()
    {
        return httpUser;
    }

    public void setHttpUser(String httpUser)
    {
        this.httpUser = httpUser;
    }

    public String getHttpPass()
    {
        return httpPass;
    }

    public void setHttpPass(String httpPass)
    {
        this.httpPass = httpPass;
    }

    public boolean getHttpSsl()
    {
        return httpSsl;
    }

    public void setHttpSsl(Boolean httpSsl)
    {
        this.httpSsl = httpSsl;
    }

    public String getSslCertificate()
    {
        return sslCertificate;
    }

    public void setSslCertificate(String sslCertificate)
    {
        this.sslCertificate = sslCertificate;
    }

    public String getSslPassword()
    {
        return sslPassword;
    }

    public void setSslPassword(String sslPassword)
    {
        this.sslPassword = sslPassword;
    }

    public Integer getExecuteTimeout()
    {
        return executeTimeout;
    }

    public void setExecuteTimeout(Integer executeTimeout)
    {
        this.executeTimeout = executeTimeout;
    }

    public Integer getScriptTimeout()
    {
        return scriptTimeout;
    }

    public void setScriptTimeout(Integer scriptTimeout)
    {
        this.scriptTimeout = scriptTimeout;
    }

    public Integer getWaitTimeout()
    {
        return waitTimeout;
    }

    public void setWaitTimeout(Integer waitTimeout)
    {
        this.waitTimeout = waitTimeout;
    }
    
    public Map<String, Object> getProperties()
    {
        return properties;
    }

    public void setProperties(Map<String, Object> properties)
    {
        this.properties = properties;
    }

} // class PushGatewayProperties
