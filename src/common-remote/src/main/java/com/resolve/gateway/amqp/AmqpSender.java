/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.amqp;

import java.io.Serializable;

import com.rabbitmq.client.Channel;
import com.resolve.esb.ESBException;
import com.resolve.rsremote.ConfigReceiveAmqp;
import com.resolve.util.Guid;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;

/**
 * This class is used for sending messages regardless of Topic or Queue. This
 * implements mechanism to recover from connection failure.
 */
public class AmqpSender
{
    private AmqpServer amqpServer;
    private Channel sendChannel;

    public AmqpSender(final AmqpServer mAmqpServer, final ConfigReceiveAmqp config) throws ESBException
    {
        this.amqpServer = mAmqpServer;
    } // AmqpSender

    public Channel getSendChannel()
    {
        return sendChannel;
    }

    public void setSendChannel(final Channel sendChannel)
    {
        this.sendChannel = sendChannel;
    }

    public void init(String address) throws ESBException
    {
        amqpServer.createPublisher(address, Guid.getGuid(), this);
    } // init

    public void close()
    {
        try
        {
            if (sendChannel != null && sendChannel.isOpen())
            {
                sendChannel.close();
                sendChannel = null;
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    } // close

    public synchronized boolean send(final String queue, final Serializable content) throws ESBException
    {
        boolean result = false;

        if (StringUtils.isNotBlank(queue) && content != null)
        {
            try
            {
                int retries = 0;
                if (sendChannel == null || !sendChannel.isOpen())
                {
                    // this loop makes sure if the channel is closed, we retry
                    // to open the channel. AmqpServer's CheckConnection thread keeps trying to open
                    // the connection in every 5 seconds for 3 times in case it was broken.
                    while (retries < 3)
                    {
                        try
                        {
                            this.sendChannel = amqpServer.createChannel();
                            break;
                        }
                        catch (ESBException e)
                        {
                            Log.log.warn("Connection to the AMQP service broke, retrying...");
                            this.wait(5000L);
                            retries++;
                        }
                    }
                }
                if(retries < 3)
                {
                    if(Log.log.isDebugEnabled())
                    {
                        Log.log.debug("Sending to queue: " + queue + ", message: " + content);
                    }
                    result = amqpServer.getDriver().send(this.sendChannel, queue, ObjectProperties.serialize(content));
                }
                else
                {
                    throw new ESBException("Could not open channel to the AMQP server for " + queue + " queue, check if it exists on the server or not.");
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw new ESBException(e.getMessage(), e);
            }
        }

        return result;
    } // send
    
    public synchronized boolean publish(final String exchange, final Serializable content) throws ESBException
    {
        boolean result = false;

        if (StringUtils.isNotBlank(exchange) && content != null)
        {
            try
            {
                int retries = 0;
                if (sendChannel == null || !sendChannel.isOpen())
                {
                    // this loop makes sure if the channel is closed, we retry
                    // to open the channel. AmqpServer's CheckConnection thread keeps trying to open
                    // the connection in every 5 seconds for 3 times in case it was broken.
                    while (retries < 3)
                    {
                        try
                        {
                            this.sendChannel = amqpServer.createChannel();
                            break;
                        }
                        catch (ESBException e)
                        {
                            Log.log.warn("Connection to the AMQP service broke, retrying...");
                            this.wait(5000L);
                            retries++;
                        }
                    }
                }
                if(retries < 3)
                {
                    if(Log.log.isDebugEnabled())
                    {
                        Log.log.debug("Publishing to exchange: " + exchange + ", message: " + content);
                    }
                    result = amqpServer.getDriver().publish(this.sendChannel, exchange, ObjectProperties.serialize(content));
                }
                else
                {
                    throw new ESBException("Could not open channel to the AMQP server for " + exchange + " exchange, check if it exists on the server or not.");
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw new ESBException(e.getMessage(), e);
            }
        }

        return result;
    } // publish
} // AmqpSender
