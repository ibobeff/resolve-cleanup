/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.snmp;

import java.util.List;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.Counter32;
import org.snmp4j.smi.Counter64;
import org.snmp4j.smi.Gauge32;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.IpAddress;
import org.snmp4j.smi.Null;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.Opaque;
import org.snmp4j.smi.TimeTicks;
import org.snmp4j.smi.UnsignedInteger32;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import com.resolve.gateway.SNMPData;
import com.resolve.util.Log;

public class SNMPV2cUtils
{
    private static PDU get(int pduType, Address targetAddress, String readCommunity, int retries, long timeout, String oid)
    {
        PDU responsePDU = null;

        try
        {
            // Create TransportMapping and Listen
            TransportMapping transport;
            transport = new DefaultUdpTransportMapping();
            transport.listen();

            // Create Target Address object
            CommunityTarget comtarget = new CommunityTarget();
            comtarget.setCommunity(new OctetString(readCommunity));
            comtarget.setVersion(SnmpConstants.version2c);
            comtarget.setAddress(targetAddress);
            comtarget.setRetries(retries);
            comtarget.setTimeout(timeout);

            // Create the PDU object
            PDU pdu = new PDU();
            pdu.add(new VariableBinding(new OID(oid)));
            pdu.setRequestID(new Integer32(1));
            pdu.setType(pduType);

            // Create Snmp object for sending data to Agent
            Snmp snmp = new Snmp(transport);

            ResponseEvent response;
            if (PDU.GET == pduType)
            {
                response = snmp.get(pdu, comtarget);
            }
            else
            {
                response = snmp.getNext(pdu, comtarget);
            }
            // Process Agent Response
            if (response != null)
            {
                Log.log.debug("\nGot following response from agent " + targetAddress + "\nResponse:" + response);
                responsePDU = response.getResponse();

                if (responsePDU != null)
                {
                    int errorStatus = responsePDU.getErrorStatus();
                    int errorIndex = responsePDU.getErrorIndex();
                    String errorStatusText = responsePDU.getErrorStatusText();

                    if (errorStatus != PDU.noError)
                    {
                        responsePDU = null;
                        Log.log.debug("Error: Request Failed");
                        Log.log.debug("Error Status = " + errorStatus);
                        Log.log.debug("Error Index = " + errorIndex);
                        Log.log.debug("Error Status Text = " + errorStatusText);
                    }
                }
                else
                {
                    Log.log.debug("Error: GetNextResponse PDU is null");
                }
            }
            else
            {
                Log.log.debug("Error: Agent Timeout... ");
            }
            snmp.close();
        }
        catch (Exception e)
        {
            Log.log.error("Error during getting data, " + e.getMessage(), e);
        }
        return responsePDU;
    }

    public static PDU get(Address targetAddress, String readCommunity, int retries, long timeout, String oid)
    {
        return get(PDU.GET, targetAddress, readCommunity, retries, timeout, oid);
    }

    public static PDU getNext(Address targetAddress, String readCommunity, int retries, long timeout, String oid)
    {
        return get(PDU.GETNEXT, targetAddress, readCommunity, retries, timeout, oid);
    }

    public static void set(Address targetAddress, String writeCommunity, int retries, long timeout, String oid, String value, String valueType) throws Exception
    {
        try
        {
            TransportMapping transport = new DefaultUdpTransportMapping();
            Snmp snmp = new Snmp(transport);
            transport.listen();
            CommunityTarget target = new CommunityTarget();
            target.setCommunity(new OctetString(writeCommunity));
            target.setAddress(targetAddress);
            target.setRetries(retries);
            target.setTimeout(timeout);
            target.setVersion(SnmpConstants.version2c);
            PDU pdu = new PDU();
            // Depending on the MIB attribute type, appropriate casting can be done here.
            pdu.add(SNMPUtils.getVariableBinding(oid, value, valueType));

            pdu.setType(PDU.SET);

            ResponseEvent response = snmp.set(pdu, target);
            if(response == null)
            {
                throw new Exception("No response from the device");
            }
            else
            {
                Exception ex = response.getError();
                if(ex == null)
                {
                    PDU responsePDU = response.getResponse();
                    if(responsePDU == null)
                    {
                        throw new Exception("No PDU response from the device");
                    }
                    else
                    {
                        int errorStatus = responsePDU.getErrorStatus();
                        int errorIndex = responsePDU.getErrorIndex();
                        String errorStatusText = responsePDU.getErrorStatusText();
                        if(errorStatus == PDU.noError)
                        {
                            Log.log.debug("Set Status is: " + errorStatusText);   
                        }
                        else
                        {
                            throw new Exception("Error status: " + errorStatus + ", Error index: " + errorIndex + ", Error status text: " + errorStatusText);
                        }
                    }
                }
                else
                {
                    throw ex;
                }
            }
            
            snmp.close();
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage());
            throw e;
        }
    }
    
    /**
     * Sends Snmp trap to a device.
     * 
     * @param version
     * @param deviceIp
     * @param port
     * @param retries
     * @param timeout
     * @param writeCommunity
     * @param snmpTrapOid
     * @param oids
     *            is a Map with OID and their values. Currently we attempt to
     *            check the data type of the value and only support Integer32
     *            and OctetString
     * @return
     * @throws Exception
     */
    public static boolean sendTrap(String deviceIp, int port, int retries, long timeout, String writeCommunity, String snmpTrapOid, List<SNMPData> traps) throws Exception
    {
        boolean result = true;

        if (traps != null && traps.size() > 0)
        {
            // Create PDU
            PDU trap = new PDU();
            trap.setType(PDU.TRAP);

            // Mandatory variable bindings
            OID trapOid = new OID(snmpTrapOid);
            trap.add(new VariableBinding(SnmpConstants.sysUpTime, new Integer32(0))); // this
                                                                                      // must
                                                                                      // be
                                                                                      // the
                                                                                      // first.
            trap.add(new VariableBinding(SnmpConstants.snmpTrapOID, trapOid)); // this
                                                                               // must
                                                                               // be
                                                                               // the
                                                                               // second.

            for (SNMPData snmpData : traps)
            {
                SNMPUtils.addValue(trap, snmpData.getOid(), snmpData.getValue(), snmpData.getDataType());
            }

            // Specify receiver
            Address targetaddress = SNMPUtils.getAddress(deviceIp, port);
            CommunityTarget target = new CommunityTarget();
            target.setCommunity(new OctetString(writeCommunity));
            target.setVersion(SnmpConstants.version2c);
            target.setAddress(targetaddress);
            target.setRetries(retries);
            target.setTimeout(timeout);

            // Send
            Snmp snmp;
            try
            {
                snmp = new Snmp(new DefaultUdpTransportMapping());
                snmp.send(trap, target);
                snmp.close();
            }
            catch (Exception e)
            {
                Log.log.error("Could not send SNMP Trap to " + deviceIp + " on port " + port);
                Log.log.error("Actual error is " + e.getMessage(), e);
                throw new Exception("Could not send SNMP Trap to " + deviceIp + " on port " + port + "\nActual error is :" + e.getMessage(), e);
            }
        }
        return result;
    }

    /**
     * This methods sends the V2 trap to the Localhost in port 163
     */
    /*
     * public void sendSnmpV2Trap() { try { // Create Transport Mapping
     * TransportMapping transport = new DefaultUdpTransportMapping();
     * transport.listen();
     * 
     * // Create Target CommunityTarget comtarget = new CommunityTarget();
     * comtarget.setCommunity(new OctetString("public"));
     * comtarget.setVersion(SnmpConstants.version2c); comtarget.setAddress(new
     * UdpAddress("10.20.2.128/162")); comtarget.setRetries(2);
     * comtarget.setTimeout(5000);
     * 
     * // Create PDU for V2 PDU pdu = new PDU();
     * 
     * // need to specify the system up time long sysUpTime = 111111;
     * pdu.add(new VariableBinding(SnmpConstants.sysUpTime, new
     * TimeTicks(sysUpTime))); pdu.add(new
     * VariableBinding(SnmpConstants.snmpTrapOID, new OID(trapOid)));
     * pdu.add(new VariableBinding(SnmpConstants.snmpTrapAddress, new
     * IpAddress(ipAddress)));
     * 
     * // variable binding for Enterprise Specific objects, Severity (should be
     * defined in MIB file) pdu.add(new VariableBinding(new OID(trapOid), new
     * OctetString("Major"))); pdu.setType(PDU.NOTIFICATION);
     * 
     * // Send the PDU Snmp snmp = new Snmp(transport);
     * System.out.println("Sending V2 Trap to " + ipAddress + " on Port " +
     * port); snmp.send(pdu, comtarget); snmp.close(); } catch (Exception e) {
     * System.err.println("Error in Sending V2 Trap to " + ipAddress +
     * " on Port " + port); System.err.println("Exception Message = " +
     * e.getMessage()); } }
     */
}
