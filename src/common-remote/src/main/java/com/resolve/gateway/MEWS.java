/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.ews.EWSAddress;
import com.resolve.gateway.ews.EWSFilter;
import com.resolve.gateway.ews.EWSGateway;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class is used by the Message Listener and invokes it methods. Messages
 * destined for the gateways set this class name in the message header.
 *
 */
public class MEWS extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MEWS.setFilters";
    private static final String RSCONTROL_SET_EWSADDRESSES = "MEWS.setEWSAddresses";

    private static final EWSGateway localInstance = EWSGateway.getInstance();

    public MEWS()
    {
        super.instance = localInstance;
    }

    @Override
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * This method is invoked once filter synchronization message from RSCONTROL
     * is received.
     * @param paramList
     */
    public void synchronizeGateway(Map<String, List<Map<String, Object>>> paramList)
    {
    	//LI-33 Improve logging for gateways
    	Log.log.info("EWSGateway: synchronize all the common data like filter, name properties, routing schemas ");
        //synchronize all the common data like filter, name properties, routing schemas
        super.synchronizeGateway(paramList);
        
        //super class method sets them to be true, however the last step may fail
        isUpdateRSControl = false;
        instance.setSyncDone(false);
        
        List<Map<String, Object>> ewsAddresses = (List<Map<String, Object>>) paramList.get("EWSADDRESSES");
        int number = 0;
        if (ewsAddresses != null)
        {
            number = ewsAddresses.size();
        }
        Log.log.debug("EWSGateway: NUMBER of EWS addresses -->" + number);
        
        clearAndSetEWSAddresses(ewsAddresses);

        isUpdateRSControl = true;
        //tell the gateway that we have received RSControl sync message.
        instance.setSyncDone(true);
    }

    /**
     * This method is called by the social UI when for example user add/deletes/update
     * a process email/im.
     */
    public void sendSyncRequest(Map<String, Object> paramList)
    {
        //we do not want to ask for synchronization if there is
        if (paramList != null && paramList.containsKey("UPDATE_SOCIAL_POSTER"))
        {
            Boolean updateSocialPoster = Boolean.parseBoolean((String) paramList.get("UPDATE_SOCIAL_POSTER"));
            if (updateSocialPoster && localInstance.isSocialPoster())
            {
                localInstance.sendSyncRequest();
            }
        }
        else
        {
            //some other consumer could ask for everybody to sync.
            localInstance.sendSyncRequest();
        }
    }

    /**
     * Clears the current instance of the Exchange Web Services gateway. If paramList isn't null, 
     * it uses it to initialize the new EWSGateway instance 
     * 
     * @param paramList a List of Map parameters for EWSGateway initialization
     * 
     */
    @FilterCompanionModel(modelName = "EWSAddress")
    public void clearAndSetEWSAddresses(List<Map<String, Object>> paramList)
    {
        // clear and set filters
        localInstance.clearAndSetEWSAddresses(paramList);

        // send new filters
        Map<String, Object> params = null;
        if (paramList != null)
        {
            //extract the first Map so we get the username who is deploying the filter
            if (paramList != null && paramList.size() > 0)
            {
                params = paramList.get(0);
            }
        }
        getEWSAddresses(params);
        // save config
        MainBase.main.exitSaveConfig();

    } // clearAndSetFilters

    /**
     * Sends an message to RSControl with all email addresses in the Exchange Web Service gateway.
     * 
     * @param params a Map of Constants.SYS_UPDATED_BY parameters for EWSGateway initialization
     * 
     */
    public void getEWSAddresses(Map<String, Object> params)
    {
        if (localInstance.isActive())
        {
            Map<String, String> content = new HashMap<String, String>();

            for (EWSAddress ewsAddress : localInstance.getEWSAddresses().values())
            {
                Map<String, Object> filterMap = new HashMap<String, Object>();
                filterMap.put(EWSAddress.EWSADDRESS, ewsAddress.getEWSAddress());
                filterMap.put(EWSAddress.EWSP_ASSWORD, ewsAddress.getEWSPassword());
                filterMap.put("QUEUE", localInstance.getQueueName());

                // This value comes all the way from the gateway filter
                // deployment UI.
                // This is important so RSConrtol can set the user name who
                // deployed the filter.
                if (params != null)
                {
                    filterMap.put(Constants.SYS_UPDATED_BY, params.get(Constants.SYS_UPDATED_BY));
                }

                content.put(ewsAddress.getEWSAddress(), StringUtils.remove(StringUtils.mapToString(filterMap), "null"));
            }

            // send content to rscontrol
            if (isUpdateRSControl)
            {
                //this is important as RSControl need this
                content.put("QUEUE", localInstance.getQueueName());

                if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROL, RSCONTROL_SET_EWSADDRESSES, content) == false)
                {
                    Log.log.warn("Failed to send email addresses to RSCONTROL");
                }
            }
        }
    } // getFilters

    @Override
    protected void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        EWSFilter ewsFilter = (EWSFilter) filter;
        filterMap.put(EWSFilter.QUERY, ewsFilter.getQuery());
        filterMap.put(EWSFilter.INCLUDE_ATTACHMENT, String.valueOf(ewsFilter.getIncludeAttachment()));
        filterMap.put(EWSFilter.FOLDER_NAMES, StringUtils.join(ewsFilter.getFolderNames(),","));
        filterMap.put(EWSFilter.MARK_AS_READ, String.valueOf(ewsFilter.getMarkAsRead()));
		filterMap.put(EWSFilter.ONLY_NEW, String.valueOf(ewsFilter.getOnlyNew()));
    }
    
    /**
     * Enqueues a map of parameters into the EWSGateway. 
     * 
     * @param params
     *            a {@link Map} message to be sent to the EWSGateway singleton.
     */
    public void sendMessage(Map<String, Object> params)
    { 
        if (instance.isActive())
        {
            String username = (String) params.get("USERNAME");
            String password = (String) params.get("PASSWORD");
            params.remove("USERNAME");
            params.remove("PASSWORD");
            Log.log.debug("Sending Email with Parameters: " + params);
            try
            {
                localInstance.sendMessage(params, username, password);
            }
            catch (Exception e)
            {
                Log.log.error("Failed to send Message", e);
            }
        }
        else
        {
            Log.log.error("Gateway is not currently active, unable to send Email");
        }
    }
}
