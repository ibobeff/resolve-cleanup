package com.resolve.gateway.salesforce;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.StringTokenizer;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;

import org.owasp.esapi.ESAPI;

import com.resolve.rsremote.ConfigReceiveSalesforce;
import com.resolve.util.Log;
import com.resolve.util.SSLUtils;
import com.resolve.util.StringUtils;

/**
 * The is a convenience class for consuming Salesforce SOAP web services. This
 * is most generic and lowest level class used by more specific Salesforce
 * object based (e.g., incident, problem) classes.
 */
public class SoapCaller
{
    private ConfigReceiveSalesforce configurations;
    private String url;
    private String httpbasicauthusername;
    private String httpbasicauthpassword;
    private String sessionId;

    private String urn = "urn:enterprise.soap.sforce.com";
    // private String serverUrl;

    /**
     * Constructor
     * 
     * @param url
     *            in the form of
     *            https://login.salesforce.com/services/Soap/u/25.0
     */
    public SoapCaller(ConfigReceiveSalesforce configurations, String url, String httpbasicauthusername, String httpbasicauthpassword)
    {
        this.configurations = configurations;
        this.url = url;
        this.httpbasicauthusername = httpbasicauthusername;
        this.httpbasicauthpassword = httpbasicauthpassword;
        if("partner".equalsIgnoreCase(configurations.getAccounttype()))
        {
            urn = "urn:partner.soap.sforce.com";
        }

        try
        {
            SSLUtils.FakeX509TrustManager tm = new SSLUtils.FakeX509TrustManager();
            TrustManager[] trustAllCerts = new TrustManager[] { tm };
            
            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
    
            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier()
            {
                @Override
                public boolean verify(String hostname, SSLSession session)
                {
                    return true;
                }
            };
    
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        }
        catch (NoSuchAlgorithmException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (KeyManagementException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        // sessionId = loginToSalesforce(httpbasicauthusername,
        // httpbasicauthpassword);
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getSessionId()
    {
        return sessionId;
    }

    public String getServerUrl()
    {
        return url;
    }

    public String getHttpbasicauthusername()
    {
        return httpbasicauthusername;
    }

    public void setHttpbasicauthusername(String httpbasicauthusername)
    {
        this.httpbasicauthusername = httpbasicauthusername;
    }

    public String getHttpbasicauthpassword()
    {
        return httpbasicauthpassword;
    }

    public void setHttpbasicauthpassword(String httpbasicauthpassword)
    {
        this.httpbasicauthpassword = httpbasicauthpassword;
    }

    // ////////////////////////////////////////////////////////////////////
    // Method: sendSoapRequest //
    // //
    // Description: This method sends the input soap envelope and //
    // returns the soap envelope reply sent by Salesforce //
    // //
    // Inputs: The soapEnvelope to be sent, all ready to go //
    // //
    // Outputs: The soap envelope with which that Salesforce replies. //
    // //
    // ////////////////////////////////////////////////////////////////////
    public String sendSoapRequest(String soapEnvelope) throws Exception
    {
        String response = null;
        if(StringUtils.isNotBlank(soapEnvelope))
        {
                
            try
            {
                URL url = new URL(this.url);
                if(Log.log.isTraceEnabled())
                {
                    Log.log.trace("Sending soap request to : " + this.url);
                    Log.log.trace("Soap envelope : " + soapEnvelope);
                }
                
                HttpsURLConnection connection = null;
                int len = soapEnvelope.length();
                try
                {
                    connection = (HttpsURLConnection) url.openConnection();
                    connection.setRequestMethod("POST");
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
                    connection.setInstanceFollowRedirects(true);
        
                    connection.setRequestProperty("Content-Length", Integer.toString(len));
                    connection.setRequestProperty("SOAPAction", "login");
                    connection.connect();
                }
                catch(IOException e)
                {
                    Log.log.error("Connection failed. " + e.getMessage());
                    throw new Exception("Connection failed. " + e.getMessage());
                }
    
                OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream(), "UTF8");
    
                try
                {
                    out.write(ESAPI.encoder().encodeForXML(soapEnvelope), 0, len);
                    out.flush();
                    out.close();
                }
                catch(IOException e)
                {
                    Log.log.error("Error writing content to the OutputStreamWriter. " + e.getMessage());
                    throw new Exception("Error writing content to the OutputStreamWriter. " + e.getMessage());
                }
                
                try
                {
                    InputStreamReader read = new InputStreamReader(connection.getInputStream());
                    StringBuilder sb = new StringBuilder();
                    int ch = read.read();
                    while (ch != -1)
                    {
                        sb.append((char) ch);
                        ch = read.read();
                    }
                    response = sb.toString();
                    read.close();
                }
                catch(IOException e)
                {
                    Log.log.error("Error reading from InputStreamReader. " + e.getMessage());
                    throw new Exception("Error reading from InputStreamReader. " + e.getMessage());
                }
                connection.disconnect();
            }
            catch (Exception e)
            {
                Log.log.error("Error sending SOAP request to the Salesforce Server, check blueprint configuration and restart." + e.getMessage());
                throw new Exception("Error sending SOAP request to the Salesforce Server, check blueprint configuration and restart." + e.getMessage());
            }
        }
        else
        {
            Log.log.info("There is no SOAP enelop provided.");
        }
        return response;
    }

    public String getServerTime()
    {
        String time = null;

        String response = null;
        try
        {
            response = this.sendSoapRequest(
                            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"" + urn + "\">\n" 
                            + "   <soapenv:Header>\n" 
                            + "       <urn:CallOptions>\n" 
                            + "           <urn:client></urn:client>\n" 
                            + "           <urn:defaultNamespace></urn:defaultNamespace>\n" 
                            + "       </urn:CallOptions>\n" 
                            + "       <urn:SessionHeader>\n" 
                            + "           <urn:sessionId>" + sessionId + "</urn:sessionId>\n"
                            + "       </urn:SessionHeader>\n" 
                            + "   </soapenv:Header>\n" 
                            + "   <soapenv:Body>\n" 
                            + "       <urn:getServerTimestamp/>\n" 
                            + "   </soapenv:Body>\n" 
                            + "</soapenv:Envelope>\n");
        }
        catch (Exception e)
        {
            Log.log.error("Error calling getServerTimestamp(), ignoring and relogin. " + e.getMessage());
        }
        
        if(StringUtils.isNotBlank(response))
        {
            StringTokenizer tokenize = new StringTokenizer(response, "<>");
    
            boolean timeNext = false;
    
            // now we will parse out the sessionid, which must be passed with each
            // subsequent soap packet and
            // the server url, which is the url specific to this login, and will be
            // used as the url from now on.
            while (tokenize.hasMoreTokens())
            {
                String current = tokenize.nextToken();
                //System.out.println("TOKEN:" + current);
                if ("timestamp".equals(current))
                {
                    timeNext = true;
                }
                else if ("/timestamp".equals(current))
                {
                    timeNext = false;
                }
                else if (timeNext)
                {
                    if (time == null)
                    {
                        time = current;
                    }
                    else
                    {
                        time += current;
                    }
                }
            }
        }
        return time;
    }

    // ////////////////////////////////////////////////////////////////////
    // Method: testConnectionAndLoginIfNecessary //
    // //
    // Description: This method checks if the connection to salesforce //
    // is still active, by sending a server time check to the //
    // current connection, if it succeeds it returns, if it fails //
    // then it calls the login method with the original provided //
    // values //
    // //
    // Inputs: none //
    // Outputs: Id: returns the sessionId in the form of a String //
    // //
    // ////////////////////////////////////////////////////////////////////
    public void testConnectionAndLoginIfNecessary() throws Exception
    {
        if (sessionId == null)
        {
            loginToSalesforce(httpbasicauthusername, httpbasicauthpassword);
        }
        else
        {
            String serverTime = this.getServerTime();
            if (serverTime == null)
            {
                loginToSalesforce(httpbasicauthusername, httpbasicauthpassword);
            }
        }
    }

    // ////////////////////////////////////////////////////////////////////
    // Method: loginToSalesforce //
    // //
    // Description: This method logs into Salesforce using the username //
    // and password provided. It also parses the response soap //
    // envelope and retrieves the url and sessionId, which are //
    // needed in future soap envelopes. //
    // //
    // Inputs: username: your username for salesforce login //
    // password: your password including security token, //
    // appended to the standard password //
    // Outputs: Id: returns the sessionId in the form of a String //
    // //
    // ////////////////////////////////////////////////////////////////////
    public String loginToSalesforce(String username, String password) throws Exception
    {
        /*
        StringBuilder soapEnvelope = new StringBuilder("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:partner.soap.sforce.com\">\n" + "   <soapenv:Header>\n" + "         <urn:organizationId>00DE0000000bunbMAA</urn:organizationId>\n" + "   </soapenv:Header>\n" + "   <soapenv:Body>\n" + "      <urn:login>\n" + "         <urn:username>" + username + "</urn:username>\n" + "         <urn:password>" + password + "</urn:password>\n"
                        + "      </urn:login>\n" + "   </soapenv:Body>\n" + "</soapenv:Envelope>");
        */
        
        StringBuilder soapEnvelope = new StringBuilder();
        soapEnvelope.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"" + urn + "\">\n");
        soapEnvelope.append("   <soapenv:Header>\n");
        soapEnvelope.append("       <urn:CallOptions>");
        soapEnvelope.append("           <urn:client></urn:client>");
        soapEnvelope.append("           <urn:defaultNamespace></urn:defaultNamespace>");
        soapEnvelope.append(        "</urn:CallOptions>");
        soapEnvelope.append("       <urn:LoginScopeHeader>");
        soapEnvelope.append("           <urn:organizationId></urn:organizationId>");
        soapEnvelope.append("           <urn:portalId></urn:portalId>");
        soapEnvelope.append("       </urn:LoginScopeHeader>");
        soapEnvelope.append("   </soapenv:Header>\n");
        soapEnvelope.append("   <soapenv:Body>\n");
        soapEnvelope.append("       <urn:login>\n");
        soapEnvelope.append("           <urn:username>" + username + "</urn:username>\n");
        soapEnvelope.append("           <urn:password>" + password + "</urn:password>\n");
        soapEnvelope.append("      </urn:login>\n");
        soapEnvelope.append("   </soapenv:Body>\n");
        soapEnvelope.append("</soapenv:Envelope>");

        if(Log.log.isTraceEnabled())
        {
            Log.log.trace("Salesforce login SOAP Envelope:");
            String tmpEnvelope = soapEnvelope.toString().replaceAll(password, "*************");
            Log.log.trace(tmpEnvelope);
        }
        
        SoapCaller soapCaller = new SoapCaller(configurations, url, username, password);
        String response = soapCaller.sendSoapRequest(soapEnvelope.toString());
        String id = null;
        if(StringUtils.isBlank(response))
        {
            Log.log.info("Error login to salesforce for user : " + username);
            throw new Exception("Error login to salesforce for user : " + username);
        }
        
        if(Log.log.isTraceEnabled())
        {
            Log.log.trace("Salesforce login response:" + response);
        }
        
        StringTokenizer tokenize = new StringTokenizer(response, "<>");

        boolean idNext = false;
        boolean urlNext = false;
        String serverUrl = null;

        // now we will parse out the sessionid, which must be passed with each
        // subsequent soap packet and
        // the server url, which is the url specific to this login, and will be
        // used as the url from now on.
        while (tokenize.hasMoreTokens())
        {
            String current = tokenize.nextToken();
            if (current.equals("sessionId"))
            {
                idNext = true;
            }
            else if (current.equals("/sessionId"))
            {
                idNext = false;
            }
            else if (current.equals("serverUrl"))
            {
                urlNext = true;
            }
            else if (current.equals("/serverUrl"))
            {
                urlNext = false;
            }
            else if (idNext)
            {
                if (id == null)
                {
                    id = current;
                }
                else
                {
                    id += current;
                }
            }
            else if (urlNext)
            {
                if (serverUrl == null)
                {
                    serverUrl = current;
                }
                else
                {
                    serverUrl += current;
                }
            }
        }
        sessionId = id;
        this.url = serverUrl;
        if(Log.log.isTraceEnabled())
        {
            Log.log.trace("Salesforce Session ID:" + sessionId);
            Log.log.trace("Salesforce URL after login:" + this.url);
        }

        return id;
    }

	/*
    public static void main(String[] args) throws Exception
    {
        Log.init();
        String username = "art.silverstein@gen-e.com.parkertest";
        String password = "Test123GeneQrZg9uYeeFhCD3osBEUaXmBo4";
        
        SoapCaller sc = new SoapCaller("https://test.salesforce.com/services/Soap/u/29.0", username, password);
        String sessionId = sc.loginToSalesforce(username, password);
        System.out.println("sessionId:" + sessionId);
        String serverTime = sc.getServerTime();
        System.out.println("serverTime:" + serverTime);
        if(serverTime == null)
        {
            sessionId = sc.loginToSalesforce(username, password);
            System.out.println("relogin sessionId:" + sessionId);
        }
    }*/
}
