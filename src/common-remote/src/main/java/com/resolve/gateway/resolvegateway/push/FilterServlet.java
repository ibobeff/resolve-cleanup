package com.resolve.gateway.resolvegateway.push;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.owasp.esapi.ESAPI;

import com.resolve.gateway.resolvegateway.GatewayProperties;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SystemUtil;
import com.resolve.util.ResolveHttpServlet;

import net.sf.json.JSONObject;

@SuppressWarnings("serial")
public class FilterServlet extends ResolveHttpServlet
{
    private static final long serialVersionUID = 7831538166676706839L;

	private final String filterName;
    private static long waitTime = 2000;
    
    private static String UTF8 = "utf-8";
    private static String TYPE_JSON = "application/json";
    static String HTTP_REQUEST_BODY = "HTTP_REQUEST_BODY";

    protected ExecutorService executor = Executors.newFixedThreadPool(SystemUtil.getCPUCount());

    private PushGateway instance = null;

    public FilterServlet(String filterName)
    {
        this.filterName = filterName;
        PushGatewayFilter filter = PushGateway.getPushFilters().get(filterName);
        String gatewayName = filter.getGatewayName();
        instance = ConfigReceivePushGateway.getGateways().get(gatewayName);
        
        Integer wait = instance.getWaitTimeout(filterName);
        if(wait != null)
            waitTime = wait;
    }

    @Override
    protected void doOptions(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setHeader("Access-Control-Allow-Origin", instance.getServerUrl());
        response.setHeader("Access-Control-Allow-Methods", "OPTIONS");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization, Content-Length, X-Requested-With");

        if(request.getMethod().equalsIgnoreCase("OPTIONS"))
            return;
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Log.log.debug(request.getRequestURI());
//        response.setContentType("text/plain");
        response.setContentType(TYPE_JSON);
        response.setStatus(HttpServletResponse.SC_OK);
       
//        int port = request.getServerPort();
        GatewayProperties properties = instance.getGatewayProperties(filterName);
        if(properties == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            response.getWriter().println("Filter not found");
            return;
        }
        
        response.setHeader("Access-Control-Allow-Origin", properties.getServerUrl());
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
        
        String inputString = StringUtils.toString(request.getInputStream(), UTF8);

        Map<String, String> params = new HashMap<String, String>();

        //this loop will read from the query string parameters. e.g. ?name1=value&name2=value2
        Map<String, String[]> requestParameters = request.getParameterMap();
        for (String key : requestParameters.keySet())
        {
            if (requestParameters.get(key).length > 0)
            {
                try {
                    String value = StringUtils.arrayToString(requestParameters.get(key), ",");
                    value = ESAPI.validator().getValidInput("Invalid input " + value, value, "HTTPURL", 4000, true);
                    params.put(key, value);
                    
                    String id = getIdValue(params);
                    if(StringUtils.isNotEmpty(id))
                        params.put(Constants.WORKSHEET_ALERTID, id);
                } catch(Exception e) {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }

        Map<String, PushGatewayFilter> filters = PushGateway.getPushFilters();
        if(filters == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            response.getWriter().println("Filter not found");
            return;
        }
        
        PushGatewayFilter filter = filters.get(filterName);
        
        int blocking = 1;
        
        if(filter.getBlocking() != null)
            blocking = filter.getBlockingCode();
        
        if(blocking > 3) {
            String result = process(instance, filterName, request, response, params);
            response.getWriter().println(result);
        }
        
        else if(blocking > 1) {
            String result = processBlocking(instance, filterName, request, response, params);
            response.getWriter().println(result);
        }

        else {
            executor.execute(new RequestProcessor(instance, filterName, request, response, params));
            
            JSONObject message = new JSONObject();
            message.accumulate("Message", "Success.");
            response.getWriter().println(message.toString());
        }
    } // doGet()

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if(Log.log.isTraceEnabled())
        {
            Log.log.trace(request.getRequestURI());
            Log.log.trace("Client IP: " + request.getRemoteAddr());
            Log.log.trace("Client Host: " + request.getRemoteHost());
        }
        
        response.setContentType(TYPE_JSON);
        response.setStatus(HttpServletResponse.SC_OK);
        response.setHeader("Access-Control-Allow-Origin", instance.getServerUrl());
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");

        String inputString = StringUtils.toString(request.getInputStream(), UTF8);
        Map<String, String> params = new HashMap<String, String>();
        
        String contentType = request.getContentType();
        if (StringUtils.isNotBlank(contentType)) {
            if(TYPE_JSON.equalsIgnoreCase(contentType)) {
                try {
                    JSONObject json = JSONObject.fromObject(inputString);
                    params.put(HTTP_REQUEST_BODY, inputString);
                    
                    String id = parseIdValue(inputString);
                    if(StringUtils.isNotEmpty(id))
                        params.put(Constants.WORKSHEET_ALERTID, id);
                } catch(Exception e) {
                    Log.log.error(e.getMessage(), e);
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    JSONObject message = new JSONObject();
                    message.accumulate("Error", e.getMessage());
                    response.getWriter().println(message.toString());
                    
                    return;
                }
            }
            
            else { // not JSON
                params = StringUtils.urlToMap(inputString);
            }
        }
        
        else { // no content-type
            params.put(HTTP_REQUEST_BODY, inputString);
        }

        //this loop will read from the query string parameters. e.g. ?name1=value&name2=value2
        Map<String, String[]> requestParameters = request.getParameterMap();
        
        for (String key : requestParameters.keySet())
        {
            if (requestParameters.get(key).length > 0)
            {
                params.put(key, StringUtils.arrayToString(requestParameters.get(key), ","));
            }
        }
        
        Map<String, PushGatewayFilter> filters = PushGateway.getPushFilters();
        if(filters == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            response.getWriter().println("Filter not found");
            return;
        }
        
        PushGatewayFilter filter = filters.get(filterName);
        
        int blocking = 1;
        
        if(filter.getBlocking() != null)
            blocking = filter.getBlockingCode();

        if(blocking > 3) {
            
            String result = process(instance, filterName, request, response, params);
            response.getWriter().println(result);
        }
        
        else if(blocking > 1) {
            String result = processBlocking(instance, filterName, request, response, params);
            response.getWriter().println(result);
        }

        else {
            executor.execute(new RequestProcessor(instance, filterName, request, response, params));
            
            JSONObject message = new JSONObject();
            message.accumulate("Message", "HTTP request submitted successfully.");
            response.getWriter().println(message.toString());
        }
    } // doPost()
    
    private String process(final PushGateway instance, final String filterName, final HttpServletRequest request, final HttpServletResponse response, final  Map<String, String> params) {
        
        String resultStr = processBlocking(instance, filterName, request, response, params);
        Log.log.info("Synchronous HTTPGateway processing result: " + resultStr);
        
        JSONObject message = new JSONObject();

        if(StringUtils.isBlank(resultStr)) {
            message.accumulate("Message", "No information is available at this time.");
            return message.toString();
        }

        JSONObject json = null;
        
        try {
            json = JSONObject.fromObject(resultStr);
            String result = json.getString("Message");
            
            if(StringUtils.isBlank(result)) {
                message.accumulate("Message", "No information is available at this time.");
                return message.toString();
            }
            
            if(result.toLowerCase().indexOf("timed out") != -1 || result.indexOf("Failed to ") != -1)
                return resultStr;
  
            json = JSONObject.fromObject(result);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
            
        String problemId = json.getString("Worksheet Id");
        String processId = json.getString("Process Id");
        String wiki = json.getString("Wiki Runbook");
        
        Log.log.info("problemId = " + problemId + "!");
        Log.log.info("processId = " + processId + "!");
        
        if(StringUtils.isBlank(problemId) || StringUtils.isBlank(processId)) {
            message.accumulate("Message", "No information is available at this time!");
            return message.toString();
        }
    
        String status = instance.getRunbookResult(problemId, processId, wiki);
        
        if(StringUtils.isBlank(status))
            message.accumulate("Message", "Timed out.");
        
        else 
            message.accumulate("Message", status);

        return message.toString();
    } // process()
    
    private String processBlocking(final PushGateway instance, final String filterName, final HttpServletRequest request, final HttpServletResponse response, final  Map<String, String> params) {
        
        String result = "Response is: ";
        
        FilterCallable callable = new FilterCallable(instance, filterName, params);
        FutureTask<String> futureTask = new FutureTask<String>(callable);
        
        try {
            executor.execute(futureTask);
            
            while (!futureTask.isDone()) {
                try {
                    Log.log.debug("Waiting for FutureTask to complete");
                    result = futureTask.get(waitTime, TimeUnit.SECONDS);
                } catch (InterruptedException | ExecutionException e) {
                    Log.log.error(e.toString());
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "Failed to process.");
                    result = message.toString();
                    break;
                } catch(TimeoutException e) {
                    futureTask.cancel(true);
                    Log.log.error(e.toString());
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "Task is timed out.");
                    result = message.toString();
                    break;
                }
            }
        } catch(Exception e) {
            Log.log.error(e.toString());
            JSONObject message = new JSONObject();
            message.accumulate("Message", "Failed to execute task.");
            result = message.toString();
        }
        
        return result;
    } // processBlocking()
    
    private String getIdValue(Map<String, String> params) {
        
        String id = null;
        String inputKey = instance.getInputKey();
        
        if(params != null && params.size() != 0) {
            for(String key:params.keySet()) {
                if(key != null && key.contains(inputKey)) {
                    if(key.equalsIgnoreCase(inputKey)) {
                        id = params.get(key);
                        break;
                    }
                    
                    try {
                        JSONObject json = JSONObject.fromObject(key);
                        id = json.getString(inputKey);
                        break;
                    } catch(Exception e) {
                    }
                }
            }
        }
        
        return id;
    }
    
    private String parseIdValue(String input) {
        
        if(StringUtils.isBlank(input))
            return "";
        
        String id = "";
        String inputKey = instance.getInputKey();
        
        try {
            JSONObject json = JSONObject.fromObject(input);
            id = json.getString(inputKey);
            Log.log.debug("alert id = " + id);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        
        return id;
    }

    static class RequestProcessor implements Runnable
    {
        final PushGateway instance;
        final String filterName;
        final HttpServletRequest request;
        final HttpServletResponse response;
        final  Map<String, String> params;
        
        public RequestProcessor(final PushGateway instance, final String filterName, final HttpServletRequest request, final HttpServletResponse response, final  Map<String, String> params)
        {
            this.instance = instance;
            this.filterName = filterName;
            this.request = request;
            this.response = response;
            this.params = params;
        }

        @Override
        public void run()
        {
            try
            {
                // process the data through a filter.
                boolean isProcessed = instance.processData(filterName, params);

                if (isProcessed)
                {
                    response.setStatus(HttpServletResponse.SC_OK);
//                    response.setContentType("text/plain");
//                    response.getWriter().println("processed");
                }
                else
                {
                    // The requested resource is no longer available at the server
                    // this happens when a filter is no longer active, using http status code is 410
                    response.setStatus(HttpServletResponse.SC_GONE);
                    Log.log.info("Gateway filter " + filterName + " not available.");
                }
            }
            catch (Exception e)
            {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                Log.log.error(e.getMessage());
            }
        }
        
    } // end of class RequestProcessor
    
    static class FilterCallable implements Callable<String> {
        
        private PushGateway instance;
        private String filterName;
        private Map<String, String> params;

        
        public FilterCallable(final PushGateway instance, final String filterName, final Map<String, String> params) {
            this.instance = instance;
            this.filterName = filterName;
            this.params = params;
        }
        
        @Override
        public String call() throws Exception {
            
            return instance.processBlockingData(filterName, params);
        }
        
    } // end of class FilterCallable
    
} // end of class FilterServlet
