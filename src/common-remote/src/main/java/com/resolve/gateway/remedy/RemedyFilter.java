/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.remedy;

public class RemedyFilter
{
    public String fieldName;
    public String fieldID;
    public String operator;
    public String lastValue;
    public boolean quote = false;
    public int sort = 1;

}
