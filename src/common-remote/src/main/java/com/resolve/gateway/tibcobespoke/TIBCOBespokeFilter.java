/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.gateway.tibcobespoke;

import groovy.lang.Script;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.RetainValue;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.query.AssignmentComparisonUtils;
import com.resolve.query.QueryParser;
import com.resolve.query.ResolveQuery;
import com.resolve.query.translator.TIBCOBespokeQueryTranslatorConditions;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class TIBCOBespokeFilter extends BaseFilter
{
    // Additional field names for this filter.
    public static final String REGEX = "REGEX";
    public static final String BUS_URI = "BUS_URI";
    public static final String TOPIC = "TOPIC";
    public static final String CERTIFIED_MESSAGING = "CERTIFIED_MESSAGING";
    public static final String XML_QUERY = "XML_QUERY";
    public static final String PROCESS_AS_XML = "PROCESS_AS_XML";
    public static final String UNESCAPE_XML = "UNESCAPE_XML";
    public static final String REQUIRE_REPLY = "REQUIRE_REPLY";
    public static final String REPLY_TIMEOUT = "REPLY_TIMEOUT";
    
    public static final Integer DEFAULT_REPLY_TIMEOUT =  5*60*1000; // 5 minutes

    private String regex;
    private String busUri;
    private String topic;
    private boolean certifiedMessaging;    
    private String xmlQuery;
    private Script groovyScript;
    private boolean processAsXml;
    private boolean unescapeXml;
    private boolean requireReply;
    private Integer replyTimeout;
    
    private String workerId;
    private String policyName;
    private ArrayList<Object> assignmentData;
    private ArrayList<QueryParser.Assignment> assignmentOnlyData;
    private ArrayList<QueryParser.BoolOperator> boolOperatorData;    

    public TIBCOBespokeFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String regex, String busUri, String topic, String certifiedMessaging, String xmlQuery, String unescapeXml, String processAsXml, String requireReply, String replyTimeout)
    {
        super(id, active, order, interval, eventEventId, runbook, script);
        setRegex(regex);
        setBusUri(busUri);
        setTopic(topic);
        setCertifiedMessaging(certifiedMessaging);
        setXmlQuery(xmlQuery);       
        Log.log.debug("Created TIBCO filter:" + this.toString());
        setUnescapeXml(unescapeXml);
        setProcessAsXml(processAsXml);
        setRequireReply(requireReply);
        setReplyTimeout(replyTimeout);
        Log.log.debug("Created TIBCO filter:" + this.toString());
    }
  
    public String getBusUri()
    {
        return busUri;
    }

    public String getTopic()
    {
        return topic;
    }

    public boolean getCertifiedMessaging()
    {
        return certifiedMessaging;
    }

    public String getRegex()
    {
        return regex;
    }

    public String getXmlQuery()
    {
        return xmlQuery;
    }

    // Annotation used to retain value across redeployments
    @RetainValue
    public String getWorkerId()
    {
        return workerId;
    }
    
    @RetainValue
    public String getPolicyName()
    {
        return policyName;
    }
    
    public Boolean getUnescapeXml()
    {
        return unescapeXml;
    }
    
    public Boolean getProcessAsXml()
    {
        return processAsXml;
    }
    
    public Boolean getRequireReply()
    {
        return requireReply;
    }
    
    public Integer getReplyTimeout()
    {
        return replyTimeout;
    }

    public void setPolicyName(String cmName)
    {
        this.policyName = cmName;
    }
    
    public void setBusUri(String busUri)
    {
        this.busUri = busUri != null ? busUri.trim(): busUri;
    }

    public void setTopic(String topic)
    {
        this.topic = topic != null ? topic.trim() : topic;
    }

    public void setCertifiedMessaging(String certifiedMessaging)
    {
        if (StringUtils.isNotBlank(certifiedMessaging) && StringUtils.equals(StringUtils.capitalize(certifiedMessaging), StringUtils.capitalize("true")))
        {
            this.certifiedMessaging = true;
        }
        else
        {
            this.certifiedMessaging = false;
        }
    }

    public void setCertfiedMessaging(boolean certifiedMessaging)
    {
        this.certifiedMessaging = certifiedMessaging;
    }

    public void setRegex(String regex)
    {
        this.regex = regex != null ? regex.trim() : regex;
    }

    public void setWorkerId(String workerId)
    {
        this.workerId = workerId != null ? workerId.trim() : workerId;
    }

    public void setXmlQuery(String xmlQuery)
    {
        this.xmlQuery = xmlQuery != null ? xmlQuery.trim() : xmlQuery;
    }
    
    public void setUnescapeXml( String unescapeXml)
    {
        if (StringUtils.isNotBlank(unescapeXml) && StringUtils.equals(StringUtils.capitalize(unescapeXml), StringUtils.capitalize("true")))
        {
            this.unescapeXml = true;
        }
        else
        {
            this.unescapeXml = false;
        }
    }
    
    public void setUnescapeXml(boolean unescapeXml)
    {
        this.unescapeXml = unescapeXml;
    }

    public void setProcessAsXml(String processAsXml)
    {
        if (StringUtils.isNotBlank(processAsXml) && StringUtils.equals(StringUtils.capitalize(processAsXml), StringUtils.capitalize("true")))
        {
            this.processAsXml = true;
        }
        else
        {
            this.processAsXml = false;
        }
    }
    
    public void setProcessAsXml(boolean processAsXml)
    {
        this.processAsXml = processAsXml;
    }
    
    public void setRequireReply(String requireReply)
    {
        if(StringUtils.isNotEmpty(requireReply) && StringUtils.equals(StringUtils.capitalize(requireReply), StringUtils.capitalize("true")))
        {
            this.requireReply = true;
        }
        else
        {
            this.requireReply = false;
        }
    }
    
    public void setRequireReply(Boolean requireReply)
    {
        this.requireReply = requireReply;
    }
    
    public void setReplyTimeout(String replyTimeout)
    {
        if(StringUtils.isInteger(replyTimeout))
        {
            this.replyTimeout = Integer.parseInt(replyTimeout);
        }
        else
        {
            this.replyTimeout = DEFAULT_REPLY_TIMEOUT;
        }
    }
    
    public void setReplyTimeout(Integer replyTimeout)
    {
        this.replyTimeout = replyTimeout;
    }
    

    private void getQueryData() throws Exception
    {
        String nativeQuery = getNativeQuery();        
        if(nativeQuery != null)
        {
            assignmentData = ResolveQuery.getAssignmentData(nativeQuery);
            //need to remote quotes fro assignment data for comparison
            for(Object asgnObj: assignmentData)
            {
                if(asgnObj instanceof QueryParser.Assignment)
                {
                    QueryParser.Assignment assignment = (QueryParser.Assignment) asgnObj;
                    
                    //replace _DOT_ for key values, _DOTS_ were needed to run the query through the parser
                    String assignmentName = assignment.getName();
                    assignmentName = assignmentName.replaceAll("_DOT_", "\\.");
                    assignment.setName(assignmentName);
                    
                    //remove quotes from value
                    String asgnValue = assignment.getValue();
                    String replacedDouble = StringUtils.removeStartAndEnd(asgnValue, "\"");
                    String replacedSingle = StringUtils.removeStartAndEnd(asgnValue, "\'");
                    
                    if(!StringUtils.equals(replacedSingle, asgnValue))
                    {
                        assignment.setValue(replacedSingle);
                    }
                    else if (!StringUtils.equals(replacedDouble, asgnValue))
                    {
                        assignment.setValue(replacedDouble);
                    }                   
                }
            }
            
            //Map of assignments with the parenthese removed
            assignmentOnlyData = new ArrayList<QueryParser.Assignment>();
            for(Object asgnObj: assignmentData)
            {
                if(asgnObj instanceof QueryParser.Assignment)
                {
                    assignmentOnlyData.add(((QueryParser.Assignment) asgnObj));
                }
            }
            
            boolOperatorData = ResolveQuery.getBoolOperatorData(nativeQuery);
        }
    }

    /**
     * Determines if the "UI defined query" matches a message
     * recieved through the gateway.
     * 
     * @param normalizedXml
     *            - Map of xml using XmlUtils.normalizeXml()
     * @return Indicates if the xml map matches the UI defined filter query
     * @throws Exception - Issue with compiling conditional statement
     */    
    public Boolean isXmlMatch(Map<String, String> normalizedXml) throws Exception
    {        
        Boolean result = null;
        
        if(groovyScript == null)
        {
            throw new RuntimeException("TIBCO: groovy conditional script was not created");
        }
        
        //Lets create a result map of the condition results from filter query and normalized messaged xml
        ArrayList<Boolean> listOfResults = new ArrayList<Boolean>();
        for(Object assignmentObj: assignmentData)
        {
            if(assignmentObj instanceof QueryParser.Assignment)
            {
                QueryParser.Assignment assignment = (QueryParser.Assignment) assignmentObj;
                
                String normalizedKey = assignment.getName();
                String normalizedEntry = normalizedXml.get(normalizedKey);
                
                String value = assignment.getValue();
                if(value instanceof String)
                {
                    value = value.replaceAll("_DOT_", "\\.");
                }
                
                Boolean compareResult = AssignmentComparisonUtils.compareAssignment(normalizedEntry, value, assignment.getOperator());
                Log.log.debug("TIBCO: Comparing: AssignmentComparisonUtils.compareAssignment(" + normalizedEntry + ", "+ value + ", " + assignment.getOperator().toString() + ") = " + compareResult.toString()); 
                listOfResults.add(compareResult);
            }               
        }
        
        //A groovy script indicating the AND/OR and ()'s was made to based on the UI defined query.
        //The results are fed into the script and outputs a Boolean indicating a match/non-match.
        Log.log.debug("TIBCO: GroovyScript(checkCondition) solving for: " + listOfResults.toString());
        result = (Boolean) groovyScript.invokeMethod("checkCondition", (List) listOfResults);                
        return result;
    }
    
    /**
     * Builds a groovy script based on UI defined query. Script processes and/or
     * conditions in relation to () grouping. Statement comparison is done in java
     * where the groovy script only performs logical comparison i.e (true || false) && true.
     * The script is only compiled once for the filter; then it is referenced for comparison.
     *  
     * @throws Exception
     */
    private void compileConditionGroovyScript() throws Exception
    {
        String className = "TIBCOFilterRuntimeCompiledClass" + getId().toString();
        
        //gen condition string
        ResolveQuery resolveQuery = new ResolveQuery(new TIBCOBespokeQueryTranslatorConditions());
        String booleanString = resolveQuery.translate(getNativeQuery());
        
        //This is groovy script which is used to test the query against it's
        String groovyScriptString = "";
        groovyScriptString += "def static checkCondition(List params)" + "\n";
        groovyScriptString += "{" + "\n";
        groovyScriptString += "   return "+ booleanString +";"+ "\n";
        groovyScriptString += "}" + "\n";
        
        Log.log.debug("TIBCO: Filter: " + getId()+ ", Created groovyScript:\n" + groovyScriptString);
        groovyScript = GroovyScript.getGroovyScript(groovyScriptString, className, true);
    }
    
    /**
     * Get the boolean object attached to the assignment index 
     * 
     * @param assignmentIndex
     * @return
     */
    private QueryParser.BoolOperator getBool(Integer assignmentIndex)
    {
        //get assignment boolean
        // i have assignment index, i have order of operations
        
        //Backtrack to assignment, parenthese might be in the way
        int realLastAssignmentIndex = -1;
        for(int i = assignmentIndex; i >= 0; i--)
        {
            Object asgnObj = assignmentData.get(i);
            if(asgnObj instanceof QueryParser.Assignment)
            {               
                realLastAssignmentIndex = i;
                break;
            }
        }
        
        //Find the index of the boolean operator by comparing to index of list without parenthesis
        if(realLastAssignmentIndex != -1)
        {
            for(int i = 0; i < assignmentOnlyData.size(); i++)
            {
                QueryParser.Assignment curAssign = assignmentOnlyData.get(i);
                
                if(curAssign.equals(assignmentData.get(realLastAssignmentIndex)))
                {
                    //current index should be the index of the boolean operator
                    QueryParser.BoolOperator lastBoolOp = boolOperatorData.get(i);
                    return lastBoolOp;
                }                            
            }
        }
        
        return null;        
    }

    /**
     * Used to store translated query in filter. When query is stored its
     * elements are parse and stored for future query comparison against
     * messages.
     * 
     * @param nativeQuery
     *            - Query to store
     */
    @Override
    public void setNativeQuery(String nativeQuery)
    {
        // Overwrote method to add query parsing functionality and 
        //compilation of condition statements when nativeQuery is set
        try
        {
            super.setNativeQuery(nativeQuery != null ? nativeQuery.trim() : nativeQuery);
            getQueryData();
            Log.log.debug("TIBCO: Filter: " + getId() + ", Native Query set to: " + nativeQuery);
            compileConditionGroovyScript();            
        }
        catch (Exception e)
        {
            Log.log.error("TIBCO: Error translating UI query to Native Query:\n " + e.getMessage(), e);
        }
    }

    @Override
    public Matcher getMatcher(Map<String, String> params)
    {
        Matcher matcher = null;
        String content = params.get("DATA");
        if (regex != null && content != null)
        {
            Pattern pattern = Pattern.compile(regex);
            matcher = pattern.matcher(content.toLowerCase());
        }
        return matcher;
    }

    @Override
    public String toString()
    {
        String objectToString = super.toString();
        objectToString += ", regex=" + regex + ", busUri=" + busUri + ", certifiedMessaging=" + certifiedMessaging + ", topic=" + topic + ", workerId=" + getWorkerId() + 
                        ", policyName =" + getPolicyName() + ", xmlQuery=" + xmlQuery + ", nativeQuery=" + getNativeQuery() + ", processAsXml=" + getProcessAsXml() + 
                        ", unescapeXml=" + getUnescapeXml() + ", requireReply=" + requireReply + ", replyTimeout=" + replyTimeout;
        return objectToString;
    }
    
    public boolean equals(TIBCOBespokeFilter filter)
    {   
        if(busUri.equals(filter.getBusUri()) && topic.equals(filter.getTopic()) && xmlQuery.equals(filter.getXmlQuery()) && active == filter.isActive() && unescapeXml == filter.getUnescapeXml() && 
           processAsXml == filter.getProcessAsXml() && replyTimeout.equals(filter.getReplyTimeout()) && certifiedMessaging == filter.getCertifiedMessaging())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
