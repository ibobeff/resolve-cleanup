/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.email;

import com.resolve.util.CryptUtils;
import com.resolve.util.Log;

public class EmailAddress
{
    // Field names for this pool used by DBGateway related classes.
    public static final String EMAILADDRESS = "EMAILADDRESS";
    public static final String EMAILP_ASSWORD = "EMAILPASSWORD";
    public static final String QUEUE = "QUEUE";

    private String emailAddress;
    private String emailPassword;

    public EmailAddress(String emailAddress, String emailPassword)
    {
        super();
        this.emailAddress = emailAddress;
        try
        {
            this.emailPassword = CryptUtils.decrypt(emailPassword);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
        }
    }

    public void setAddress(String emailAddress, String emailPassword)
    {
        this.emailAddress = emailAddress;
        try
        {
            this.emailPassword = CryptUtils.decrypt(emailPassword);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
        }
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }
    
    public String getEmailPassword()
    {
        return emailPassword;
    }

    public void setEmailPassword(String emailPassword)
    {
        this.emailPassword = emailPassword;
    }
}
