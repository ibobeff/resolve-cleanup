/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.gateway;

import java.util.Map;

import com.resolve.gateway.tibcobespoke.TIBCOBespokeFilter;
import com.resolve.gateway.tibcobespoke.TIBCOBespokeGateway;

/**
 * This class is used by the Message Listener and invokes it methods. Messages
 * destined for the gateways set this class name in the message header.
 *
 */
public class MTIBCOBespoke extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MTIBCOBespoke.setFilters";

    private static final TIBCOBespokeGateway localInstance = TIBCOBespokeGateway.getInstance();

    public MTIBCOBespoke()
    {
        super.instance = localInstance;
    }

    @Override
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    @Override
    protected void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        if (localInstance.isActive())
        {
            TIBCOBespokeFilter tibcoBespokeFilter = (TIBCOBespokeFilter) filter;
            filterMap.put(TIBCOBespokeFilter.REGEX, tibcoBespokeFilter.getRegex());
            filterMap.put(TIBCOBespokeFilter.BUS_URI, tibcoBespokeFilter.getBusUri());
            filterMap.put(TIBCOBespokeFilter.TOPIC, tibcoBespokeFilter.getTopic());
            
            if(tibcoBespokeFilter.getCertifiedMessaging())
            {
                filterMap.put(TIBCOBespokeFilter.CERTIFIED_MESSAGING, "true");
            }
            else
            {
                filterMap.put(TIBCOBespokeFilter.CERTIFIED_MESSAGING, "false");
            }
            
            filterMap.put(TIBCOBespokeFilter.XML_QUERY, tibcoBespokeFilter.getXmlQuery());
            
            if(tibcoBespokeFilter.getProcessAsXml())
            {
                filterMap.put(TIBCOBespokeFilter.PROCESS_AS_XML, "true");
            }
            else
            {
                filterMap.put(TIBCOBespokeFilter.PROCESS_AS_XML, "false");
            }
            
            if(tibcoBespokeFilter.getUnescapeXml())
            {
                filterMap.put(TIBCOBespokeFilter.UNESCAPE_XML, "true");
            }
            else
            {
                filterMap.put(TIBCOBespokeFilter.UNESCAPE_XML, "false");
            }
            
            if(tibcoBespokeFilter.getRequireReply())
            {
                filterMap.put(TIBCOBespokeFilter.REQUIRE_REPLY, "true");
            }
            else
            {
                filterMap.put(TIBCOBespokeFilter.REQUIRE_REPLY, "false");
            }
            
            filterMap.put(TIBCOBespokeFilter.REPLY_TIMEOUT, tibcoBespokeFilter.getReplyTimeout().toString());
        }
    }
}
