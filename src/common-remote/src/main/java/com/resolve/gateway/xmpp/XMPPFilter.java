/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.xmpp;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.gateway.BaseFilter;

public class XMPPFilter extends BaseFilter
{
    // Additional field names for this filter.
    public static final String REGEX = "REGEX";

    private String regex;

    public XMPPFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String regex)
    {
        super(id, active, order, interval, eventEventId, runbook, script);
        setRegex(regex);
    } // XMPPFilter

    public String getRegex()
    {
        return regex;
    }

    public void setRegex(String regex)
    {
        this.regex = regex != null ? regex.trim() : regex;
    }
    
    @Override
    public Matcher getMatcher(Map<String, String> params)
    {
        Matcher matcher = null;
        //check XMPPGateway how the key "BODY" is set
        String content = params.get("BODY");
        if(regex != null && content != null)
        {
            Pattern pattern = Pattern.compile(regex);
            matcher = pattern.matcher(content.toLowerCase());
        }
        return matcher;
    }
}
