package com.resolve.gateway;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class RoutingSchema implements Comparable<RoutingSchema>
{
    private String id;
    private String name;
    private Integer order;
    private boolean hasRegex = false;
    private TreeSet<RoutingSchemaField> fields;
    private Map<String, RoutingRule> routingRules = new HashMap<String, RoutingRule>();
    
    public RoutingSchema(String id, String name, Integer order)
    {
        super();
        this.id = id;
        this.name = name;
        this.order = order;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Integer getOrder()
    {
        return order;
    }

    public void setOrder(Integer order)
    {
        this.order = order;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public boolean hasRegex()
    {
        return hasRegex;
    }

    public TreeSet<RoutingSchemaField> getFields()
    {
        return fields;
    }

    public void setFields(String json, boolean isUppercase)
    {
        //this is actually comes all the way from database, UI saves them as json
        //format of the json string is:
        //[{"name":"MYFIELD1","source":"","regex":"","order":0},{"name":"MYFIELD","source":"","regex":"","order":1}]
        ObjectMapper mapper = new ObjectMapper();
        try
        {
            this.fields = mapper.readValue(json, new TypeReference<TreeSet<RoutingSchemaField>>() {});
            
            if(isUppercase)
            {
                TreeSet<RoutingSchemaField> convertedFields = new TreeSet<RoutingSchemaField>();
                for(RoutingSchemaField field : fields)
                {
                    field.setName(field.getName().toUpperCase());
                    field.setSource(StringUtils.isNotBlank(field.getSource()) ? field.getSource().toUpperCase() : "");                    
                    convertedFields.add(field);
                }
                this.fields = convertedFields;
            }
            
            for(RoutingSchemaField field : fields)
            {
                if(StringUtils.isNotBlank(field.regex))
                {
                    hasRegex = true;
                    break;
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    public Map<String, RoutingRule> getRoutingRules()
    {
        return routingRules;
    }

    public void setRoutingRules(Map<String, RoutingRule> routingRules)
    {
        this.routingRules = routingRules;
    }

    public void addRoutingRule(RoutingRule routingRule)
    {
        routingRules.put(routingRule.getRid(), routingRule);
    }

    /**
     * This method generates an RID baed on the info set in the schema.
     * It first uses the field name/source as key if specified to generate RID.
     * If field/source based RID can not be generated then it uses optional regex.
     * 
     * @param params
     * @return rid
     */
    public String synthesizeRid(Map<String, String> params)
    {
        return synthesizeFieldRid(params);
    }
    
    private String synthesizeFieldRid(Map<String, String> params)
    {
        String result = null;
        String fieldToCheck = null;
        
        Set<RoutingSchemaField> fields = getFields();
        if(!fields.isEmpty())
        {
            StringBuilder rid = new StringBuilder();
            for(RoutingSchemaField field : fields)
            {
                fieldToCheck = StringUtils.isNotBlank(field.source) ? field.source : field.name;
                if(params.containsKey(fieldToCheck))
                {
                    if(StringUtils.isNotBlank(field.regex))
                    {
                        Pattern pattern = Pattern.compile(field.regex);
                        
                        if (params.containsKey(fieldToCheck) && StringUtils.isNotEmpty(params.get(fieldToCheck)))
                        {
                            Matcher matcher = pattern.matcher(params.get(fieldToCheck));
                            if (matcher.find())
                            {
                                //add the special delimeter in rid
                                rid.append(matcher.group(1)).append(StringUtils.DELIMITER);
                            }
                        }
                    }
                    else {
                        //add the special delimeter in rid
                        rid.append(params.get(fieldToCheck)).append(StringUtils.DELIMITER);
                    }
                }
                else
                {
                    //this schema does not need to be checked because the field doesn't exists in params
                    rid.setLength(0);
                    break;
                }
            }
            if(rid.length() > 0)
            {
                result = rid.substring(0, rid.length() - 1);
            }
        }
        
        if(Log.log.isDebugEnabled() && result != null)
        {
            Log.log.debug("Found key (" + fieldToCheck + ") based RID : " + result);
        }
        
        return result;
    }

    /**
     * This method generates an RID from the regex that was set in the schema.
     * It goes over all the fields that came through the params and try to match
     * the regex. 
     * 
     * @param params
     * @return
     */
    private String synthesizeRegexRid(Map<String, String> params)
    {
        String result = null;
        String fieldToCheck = null;
        
        Set<RoutingSchemaField> fields = getFields();
        if(!fields.isEmpty())
        {
            StringBuilder rid = new StringBuilder();
            for(RoutingSchemaField field : fields)
            {
                fieldToCheck = StringUtils.isNotBlank(field.source) ? field.source : field.name;
                if(params.containsKey(fieldToCheck) && StringUtils.isNotBlank(field.regex))
                {
                    Pattern pattern = Pattern.compile(field.regex);
                    Matcher matcher = pattern.matcher(params.get(fieldToCheck));
                    if (matcher.find())
                    {
                        //add the special delimeter in rid
                        rid.append(matcher.group(0)).append(StringUtils.DELIMITER);
                    }
                }
                else
                {
                    //this schema does not need to be checked because the field doesn't exists in params
                    rid.setLength(0);
                    break;
                }
            }
            if(rid.length() > 0)
            {
                result = rid.substring(0, rid.length() - 1);
            }
        }
        
        if(Log.log.isDebugEnabled() && result != null)
        {
            Log.log.debug("Found key (" + fieldToCheck + ") regex based RID : " + result);
        }

        return result;
    }
    
    @Override
    public int compareTo(RoutingSchema schema)
    {
        return this.order.compareTo(schema.getOrder());
    }

    @Override
    public String toString()
    {
        return "RoutingSchema [id=" + id + ", name=" + name + ", order=" + order + ", hasRegex=" + hasRegex + ", fields=" + fields + "]";
    }
}
