/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.servicenow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpStatus;

import com.resolve.rsremote.ConfigReceiveServiceNow;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.RestCallException;
import com.resolve.util.restclient.RestCaller;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;


/**
 * Generic ServiceNow object service provider class.
 * Uses REST API
 * 
 * @author hemant.phanasgaonkar
 */
public class GenericObjectService implements ObjectService
{
    protected final ConfigReceiveServiceNow configurations;
    //protected final RestCaller restCaller;

    //protected static final String OBJECT_CLASS = "class";

    public GenericObjectService(ConfigReceiveServiceNow configurations)
    {
        
        this.configurations = configurations;
        //restCaller = new RestCallerEx(configurations);         // need to override configproxy if proxy is specified in servicenow docs
        
       
    }
    
    protected RestCaller setupRestCaller(String objectType, String username, String p_assword)
    {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(p_assword))
        {
            username = configurations.getHttpbasicauthusername();
            p_assword = configurations.getHttpbasicauthpassword();
        }
        RestCaller restCaller = new RestCallerEx(configurations);
        restCaller.setHttpbasicauthusername(username);
        restCaller.setHttpbasicauthpassword(p_assword);

        restCaller.setUrlSuffix(objectType);
        return restCaller;
    }
    
    @Override
    public final Map<String, String> create(String objectType, Map<String, String> objectProperties, 
    		String username, String p_assword) throws Exception
    {
        RestCaller restCaller = setupRestCaller(objectType, username, p_assword);
        
        String response = null;
        
        try
        {
            response = restCaller.postMethod(null, null, objectProperties);
            Log.log.debug("ServiceNowGateway: Create Rest call succssful with response --> " + response);

        }
        catch (RestCallException rce)
        {
        	Log.log.error("ServiceNowGateway: Create Rest call failed due to --> "  + rce.getMessage());            
            if ((rce.statusCode == HttpStatus.SC_OK || (rce.statusCode == HttpStatus.SC_NO_CONTENT)) &&
                StringUtils.isNotBlank(rce.responseEntity) && rce.responseEntity.contains("sys_id"))
            {
                response = rce.responseEntity;
            }
            else
            {
                throw rce;
            }
        }

        return StringUtils.jsonObjectStringToMap("result", response);
    }

    @Override
    public boolean createWorknote(String objectType, String parentObjectId, String note, String username, String p_assword) throws Exception
    {
        boolean result = true;

        Map<String, String> objectProperties = new HashMap<String, String>();
        objectProperties.put("work_notes", note);
        update(objectType, parentObjectId, objectProperties, username, p_assword);

        return result;
    }

    @Override
    public void update(String objectType, String objectId, Map<String, String> objectProperties, String username, String p_assword) throws Exception
    {
        RestCaller restCaller = setupRestCaller(objectType, username, p_assword);

        restCaller.setUrlSuffix(objectType);
        
        Map<String, String> reqHeaders = new HashMap<String, String>();
        reqHeaders.put("X-no-response-body", "true");
        try {
	        restCaller.putMethod(objectId, null, reqHeaders, objectProperties);
	        Log.log.debug("ServiceNowGateway:Update Rest call successful for object id -->" + objectId);
        }catch(RestCallException rce) {
        	 Log.log.error("ServiceNowGateway: ERROR Update Rest call failed due to --> "  + rce.getMessage());
        }

    }

    @Override
    public void delete(String objectType, String objectId, String username, String p_assword) throws Exception
    {
        RestCaller restCaller = setupRestCaller(objectType, username, p_assword);
        
        try {
	        	restCaller.deleteMethod(objectId);
	            Log.log.debug("ServiceNowGateway: Delete Rest call successful for object id -->" + objectId);
	        }catch(RestCallException rce) {
	       	 Log.log.error("ServiceNowGateway: Delete Rest call failed due to --> "  + rce.getMessage());
	       }
    }

    @Override
    public Map<String, String> selectByID(String objectType, String objectId, String username, String p_assword) throws Exception
    {
    	Map<String, String> selectResult = null;
        RestCaller restCaller = setupRestCaller(objectType, username, p_assword);


        try {
    			String response = restCaller.getMethod(objectId, new HashMap<String, String>(), null);
		        selectResult = StringUtils.jsonObjectStringToMap("result", response);
		        
		        if(selectResult == null || selectResult.isEmpty())
		        {
		            throw new Exception("ServiceNowGateway: Select " + objectType + " by " + ServiceNowGateway.SYS_ID + " = " + objectId + " did not return any results.");
		        }
	            Log.log.debug("ServiceNowGateway: selectByID Rest call successful with results -->" 
	            				+ selectResult.toString());
	        }catch(RestCallException rce) {
	          	 Log.log.error("ServiceNowGateway: selectByID Rest call failed due to --> "  + rce.getMessage());

        }
        
        return selectResult;
    }

    @Override
    public Map<String, String> selectByAlternateID(String objectType, String altIDName, String altIDValue, String username, String p_assword) throws Exception
    {
    	List< Map<String, String>> searchResults = null;
    	try {
    		RestCaller restCaller = setupRestCaller(objectType, username, p_assword);
	
	        Map<String, String> reqParams = new HashMap<String, String>();
	        reqParams.put(altIDName, altIDValue);
	
	        String response = restCaller.getMethod(null, reqParams, null);
	
	        searchResults = StringUtils.jsonArrayStringToList("result",response);
	        
	        if(searchResults == null || searchResults.isEmpty() || searchResults.size() > 1)
	        {
	            throw new Exception("Search " + objectType + " by alternate id " + altIDName + " = " + altIDValue + " did not return any results or returned multiple results.");
	        }
	        Log.log.debug("*****************************************************************"); 
            Log.log.debug("ServiceNowGateway: selectByAlternateID Rest call successful with results -->" 
            				+ searchResults.toString());
            Log.log.debug("*****************************************************************");
    	}catch(RestCallException rce) {
         	 Log.log.error("ServiceNowGateway: selectByAlternateID Rest call failed due to --> "  + rce.getMessage());
       }
        
        return searchResults.get(0);
    }

    @Override
    public List<Map<String, String>> search(String objectType, String query, String orderBy,
    		boolean isAscending, String username, String p_assword) throws Exception
    {
        return searchWithPageLimit(objectType, query, orderBy, isAscending, 
        		configurations.getPagelimit(), username, p_assword);
    }
    
    @Override
    public List<Map<String, String>> searchWithPageLimit(String objectType, String query, 
		    		String orderBy, boolean isAscending, int pageLimit, String username, 
		    		String p_assword) throws Exception
    {
        RestCaller restCaller = setupRestCaller(objectType, username, p_assword);

        Map<String, String> reqParams = new HashMap<String, String>();
        StringBuilder qryBldr = new StringBuilder(query != null ? query : "");
        
        if (StringUtils.isNotEmpty(orderBy))
        {
            if(StringUtils.isNotEmpty(qryBldr.toString()))
            {
                qryBldr.append("^");
            }
                            
            if (isAscending)
            {
                qryBldr.append("ORDERBY" + orderBy);
            }
            else
            {
                qryBldr.append("ORDERBYDESC" + orderBy);
            }
        }
        
        if(StringUtils.isNotEmpty(qryBldr.toString()))
        {
            reqParams.put("sysparm_query", qryBldr.toString());
        }
        
        int offset = 0;
        boolean isAvailable = true;
        
        reqParams.put("sysparm_limit", Integer.toString(pageLimit > configurations.getPagelimit() 
        		? configurations.getPagelimit() : pageLimit));
        
        List<Map<String, String>> searchResults = new ArrayList<Map<String, String>>();
        
        while(isAvailable)
        {
            if(offset > 0)
            {
                reqParams.put("sysparm_offset", Integer.toString(offset));
            }
            
            String response = null;
            Log.log.debug("ServiceNowGateway: Preparing to make the Rest call with request "
                    	+ "parameters--> " + reqParams != null ? reqParams.toString():"No Params being sent");
            try
            {
                response = restCaller.getMethod(null, reqParams, null);
            }
            catch (RestCallException rce)
            {
            	Log.log.error("ServiceNowGateway: GenericObjectService.search failed with errors --> "  
            			            		   + rce.getMessage());
             
                if (rce.statusCode != HttpStatus.SC_OK && rce.statusCode != HttpStatus.SC_NOT_FOUND && 
                    StringUtils.isNotBlank(rce.responseEntity) && (rce.responseEntity.contains("sys_id") || rce.responseEntity.contains("error")))
                {
                    response = rce.responseEntity;
                }
                else
                {
                    throw rce;
                }
            }
            
            isAvailable = false;
            
            if(response != null && !response.isEmpty())
            {
                searchResults.addAll(StringUtils.jsonArrayStringToList("result",response));
                Log.log.debug("ServiceNowGateway: search fetched --> " + searchResults.size() + " items");
                if(restCaller.getResponseHeaders().containsKey("Link") &&
                   restCaller.getResponseHeaders().get("Link").indexOf("rel=\"next\"") > 0 )
                {
                    isAvailable = true;
                    offset += configurations.getPagelimit();
                }
            }
        }

        return searchResults;
    }
    
    public String getLatestIncidentNumber() throws Exception
    {
        String response = "";
        String latestIncidentNumber = "";
        List<Map<String, String>> incidentMap=null;
        RestCaller restCaller = setupRestCaller("incident", "", "");
       
        Map<String, String> reqParams = new HashMap<String, String>();
        reqParams.put("sysparm_query", "ORDERBYDESC sys_created_on");
        reqParams.put("sysparm_limit", "1");
        reqParams.put("sysparm_offset", "0");

        try
        {
            response = restCaller.getMethod(null, reqParams, null);
            JSONObject jsonObj = (JSONObject) JSONSerializer.toJSON(response);
            if (jsonObj.containsKey("result"))
            {
                JSONArray incidentArray = (JSONArray) jsonObj.get("result");
                if (incidentArray.size() == 1)
                {
                    jsonObj = (JSONObject) incidentArray.get(0);
                    if (jsonObj.containsKey("number"))
                    {
                        latestIncidentNumber = jsonObj.getString("number");
                    }
                    else
                    {
                        throw new Exception("Error getting latest Incident Number from ServiceNow");
                    }
                }
                else
                {
                    throw new Exception("Error getting latest Incident Number from ServiceNow");
                }
            }
            else
            {
                throw new Exception("Error getting latest Incident Number from ServiceNow");
            }
        }
        catch (RestCallException rce)
        {
            Log.log.error("ServiceNowGateway: getLatestIncidentNumber Rest call Exception --> "
            					+ rce.getMessage());
            
            if (rce.statusCode != HttpStatus.SC_OK && rce.statusCode != HttpStatus.SC_NOT_FOUND && 
                StringUtils.isNotBlank(rce.responseEntity) && rce.responseEntity.contains("sys_id"))
            {
                response = rce.responseEntity;
                Log.log.debug("ServiceNowGateway: Results after Restcall exception  ::::"
                          		+ "Response status code -->" + rce.statusCode + " and Response -->" +
                         		response);
            }
            else
            {
            	Log.log.error("ServiceNowGateway: getLatestIncidentNumber Rest call Exception --> "
            					+ rce.getMessage());
                throw rce;
            }
        }
       
        return latestIncidentNumber;

    }

    @Override
    public String importSetApi(String stagingTableName, HashMap<String, String> params, String username, String p_assword) throws Exception
    {
        // TODO Auto-generated method stub
                
      if(StringUtils.isEmpty(stagingTableName)) {
          throw new Exception("Staging Table Name is required.");
          
      }
      if((params == null) || (params.size()== 0)) {
          throw new Exception("Please provide the fields name and values in the params map.");
          
      }
        RestCaller restCaller = new RestCaller(configurations.getImportsetapiurl(), username, p_assword);
       
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(p_assword))
        {
            username = configurations.getHttpbasicauthusername();
            p_assword = configurations.getHttpbasicauthpassword();
        }

        restCaller.setHttpbasicauthusername(username);
        restCaller.setHttpbasicauthpassword(p_assword);
        restCaller.setUrlSuffix(stagingTableName);
        
        String response = null;
        
        try
        {
            response = restCaller.postMethod(null, null, params);
            Log.log.debug("*****************************************************************"); 
            Log.log.debug("ServiceNowGateway: importSetApi Rest call successful with results -->" + response);
            Log.log.debug("*****************************************************************");
            
        }
        catch (RestCallException rce)
        {
            Log.log.debug("ServiceNowGateway: importSetApi call failed due to : "  + rce.getMessage());
            
            if (rce.statusCode == HttpStatus.SC_CREATED  &&
                StringUtils.isNotBlank(rce.responseEntity) && rce.responseEntity.contains("sys_id"))
            {
                response = rce.responseEntity;
               
            }
            else
            {
                throw rce;
            }
        }
        String formattedResponse = StringUtils.jsonObjectToString(StringUtils.stringToJSONObject(response));
        return formattedResponse;
        
    }
}
