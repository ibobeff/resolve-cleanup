/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.ews;

import com.resolve.util.CryptUtils;
import com.resolve.util.Log;

public class EWSAddress
{
    public static final String EWSADDRESS = "EWSADDRESS";
    public static final String EWSP_ASSWORD = "EWSPASSWORD";
    public static final String QUEUE = "QUEUE";

    private String ewsAddress;
    private String ewsPassword;

    public EWSAddress(String ewsAddress, String ewsPassword)
    {
        super();
        this.ewsAddress = ewsAddress;
        try
        {
            this.ewsPassword = CryptUtils.decrypt(ewsPassword);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
        }
    }

    public void setAddress(String ewsAddress, String ewsPassword)
    {
        this.ewsAddress = ewsAddress;
        try
        {
            this.ewsPassword = CryptUtils.decrypt(ewsPassword);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
        }
    }

    public String getEWSAddress()
    {
        return ewsAddress;
    }

    public void setEWSAddress(String ewsAddress)
    {
        this.ewsAddress = ewsAddress;
    }
    
    public String getEWSPassword()
    {
        return ewsPassword;
    }

    public void setEWSPassword(String ewsPassword)
    {
        this.ewsPassword = ewsPassword;
    }
}
