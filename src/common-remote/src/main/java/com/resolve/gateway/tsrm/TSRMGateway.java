/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.tsrm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MTSRM;
import com.resolve.query.ResolveQuery;
import com.resolve.query.translator.TSRMQueryTranslator;
import com.resolve.rsremote.ConfigReceiveTSRM;
import com.resolve.util.Constants;
import com.resolve.util.ERR;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class TSRMGateway extends BaseClusteredGateway
{
    // Singleton
    private static volatile TSRMGateway instance = null;

    // This will be set in initialize() with the proper queue name from the
    // configuration.
    private String queue = "TSRM";

    private final static int STATE_PASSIVE_CONNECT = 0;
    private final static int STATE_ACTIVE_PRIMARY = 1;
    private final static int STATE_ACTIVE_BACKUP = 2;

    private static final String TICKETUID = "TICKETUID";
    private static final String TICKETID = "TICKETID";
    public static String CHANGE_DATE = "CHANGEDATE";

    public static volatile ObjectService serviceRequestObjectService;
    public static volatile ObjectService incidentObjectService;
    private final ResolveQuery resolveQuery;
    private RestCaller restCaller;

    private static final int MAX_RETRY = 3;
    private int state = STATE_PASSIVE_CONNECT;
    private long reconnectDelay = 30 * 1000;
    private long retryDelay = 5 * 1000;
    private boolean isBackupServerAvailable = false;

    private final ConfigReceiveTSRM tsrmConfig;

    public static TSRMGateway getInstance(ConfigReceiveTSRM config)
    {
        if (instance == null)
        {
            instance = new TSRMGateway(config);
        }
        return (TSRMGateway) instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static TSRMGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("TSRM Gateway is not initialized correctly..");
        }
        else
        {
            return (TSRMGateway) instance;
        }
    }

    /**
     * Private constructor.
     *
     * @param config
     */
    private TSRMGateway(ConfigReceiveTSRM config)
    {
        // Important, call super here.
        super(config);
        queue = config.getQueue();
        tsrmConfig = config;
        resolveQuery = new ResolveQuery(new TSRMQueryTranslator());

        restCaller = new RestCaller(config.getUrl(), config.getUsername(), config.getPassword(), config.getHttpbasicauthusername(), config.getHttpbasicauthpassword());
    } // TSRMGateway

    @Override
    public String getLicenseCode()
    {
        return "TSRM";
    }

    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_TSRM;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MTSRM.class.getSimpleName();
    }

    @Override
    protected Class<MTSRM> getMessageHandlerClass()
    {
        return MTSRM.class;
    }

    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    /**
     * Initializes the specific gateway resources.
     *
     * @param config
     */
    @Override
    protected void initialize()
    {
        queue = tsrmConfig.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/tsrm/";

        reconnectDelay = tsrmConfig.getReconnectdelay() * 1000;
        retryDelay = tsrmConfig.getRetrydelay() * 1000;
        if(StringUtils.isNotBlank(tsrmConfig.getUrl2()))
        {
            isBackupServerAvailable = true;
        }
        serviceRequestObjectService = ObjectServiceFactory.getObjectService(restCaller, ServiceRequestObjectService.OBJECT_IDENTITY);
        incidentObjectService = ObjectServiceFactory.getObjectService(restCaller, IncidentObjectService.OBJECT_IDENTITY);
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new TSRMFilter((String) params.get(TSRMFilter.ID), (String) params.get(TSRMFilter.ACTIVE), (String) params.get(TSRMFilter.ORDER), (String) params.get(TSRMFilter.INTERVAL), (String) params.get(TSRMFilter.EVENT_EVENTID)
                        , (String) params.get(TSRMFilter.RUNBOOK), (String) params.get(TSRMFilter.SCRIPT), (String) params.get(TSRMFilter.OBJECT), (String) params.get(TSRMFilter.QUERY));
    } // getFilter

    @Override
    public void start()
    {
        Log.log.warn("Starting TSRMListener");
        super.start();
    } // start

    @Override
    public void run()
    {
        //This call is must for all the ClusteredGateways.
        super.sendSyncRequest();

        long lastPrimaryRetry = 0;

        while (running)
        {
            try
            {
                long startTime = System.currentTimeMillis();

                /* if any remaining events are in primary data queue or 
                 * primary data executor has waiting messages still to be processed, 
                 * do not get messages for more new events.
                 */
                
                if (primaryDataQueue.isEmpty() && !primaryDataQueueExecutorBusy())
                {
                    Log.log.trace("Local Event Queue is empty and Primary Data Queue Executor is free.....");

                    for (Filter filter : orderedFilters)
                    {
                        if (shouldFilterExecute(filter, startTime))
                        {
                            TSRMFilter tsrmFilter = (TSRMFilter) filter;
                            if (StringUtils.isEmpty(tsrmFilter.getLastChangeDate()))
                            {
                                tsrmFilter.setLastChangeDate(DatatypeConverter.printDateTime(Calendar.getInstance()));
                            }
                            if (StringUtils.isEmpty(tsrmFilter.getLastTicketId()))
                            {
                                tsrmFilter.setLastTicketId("0");
                            }

                            List<Map<String, String>> results = invokeTSRMService(tsrmFilter);
                            if (results.size() > 0)
                            {
                                Map<String, String> lastResult = results.get(results.size() - 1);

                                // Update the lastChangeDate property.
                                // since we intentionally get the objects
                                // ordered by changed date in ascending order,
                                // go to the bottom of the list and get change
                                // date from the last object.
                                //Map<String, String> lastResult = results.get(results.size() - 1);
                                if(lastResult.size() > 0)
                                {
                                    String lastChangeDate = lastResult.get(CHANGE_DATE);
                                    String lastTicketId = lastResult.get(TICKETID);
                                    Log.log.debug("The retrieved last ticket id is " + lastTicketId);
                                    java.util.Calendar cal = DatatypeConverter.parseDateTime(lastChangeDate);
                                    // due to an issue with the TSRM REST API the ~gt~ which means greater than include the
                                    // value for the datetime field. strange but for integral it seems to behave perfectly.
                                    // So adding a second to the circumvent the problem.
                                    cal.set(Calendar.SECOND, cal.get(Calendar.SECOND) + 1);
                                    lastChangeDate = DatatypeConverter.printDateTime(cal);
                                    tsrmFilter.setLastChangeDate(lastChangeDate);
                                    tsrmFilter.setLastTicketId(lastTicketId);
                                }

                                // Let's execute runboook for each incident
                                for (Map<String, String> tsrmObject : results)
                                {
                                    //processFilter(tsrmFilter, null, tsrmObject);
                                    addToPrimaryDataQueue(tsrmFilter, tsrmObject);
                                }
                            }

                            // attempt to reconnect to primary if currently using backup
                            if (state == STATE_ACTIVE_BACKUP)
                            {
                                if (System.currentTimeMillis() - lastPrimaryRetry > reconnectDelay)
                                {
                                    connect();
                                    lastPrimaryRetry = System.currentTimeMillis();
                                }
                            }
                        }
                        else
                        {
                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                        }
                    }
                }

                // process events collected by processFilter.
                //processFilterEvents(startTime);

                // If there is no filter yet no need to keep spinning, wait for
                // a while
                if (orderedFilters.size() == 0)
                {
                    Thread.sleep(interval);
                }
            }
            catch (Exception e)
            {
                //this is server connction failure comes back
                //all the way from RestCaller.getMethod().
                if(e instanceof IOException)
                {
                    try
                    {
                        Thread.sleep(reconnectDelay);
                    }
                    catch (InterruptedException e1)
                    {
                        //ignore
                    }
                    Log.log.info("Reconnecting...");
                    state = STATE_PASSIVE_CONNECT;
                    connect();
                }

                Log.log.warn("Error: " + e.getMessage(), e);
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", 
                          "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + 
                          "due to " + e.getMessage());
                
                try
                {
                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    } // run

    private boolean connect()
    {
        boolean result = false;

        Log.log.info("Current state: " + state);
        if (running && isActive())
        {
            // try connecting to primary
            Log.log.info("Attempting to connect with PRIMARY TSRM Server");
            RestCaller primaryRestCaller = new RestCaller(tsrmConfig.getUrl(), tsrmConfig.getUsername(), tsrmConfig.getPassword(), tsrmConfig.getHttpbasicauthusername(), tsrmConfig.getHttpbasicauthpassword());

            for(int i = 0; i < MAX_RETRY; i++)
            {
                if (primaryRestCaller.testConnection())
                {
                    //assign the primary rest caller
                    restCaller = primaryRestCaller;
                    incidentObjectService.setRestCaller(restCaller);
                    serviceRequestObjectService.setRestCaller(restCaller);
                    state = STATE_ACTIVE_PRIMARY;
                    Log.log.info("New state: " + state);
                    result = true;
                    break;
                }
                else
                {
                    try
                    {
                        Thread.sleep(retryDelay);
                    }
                    catch (InterruptedException e)
                    {
                        //ignore
                    }
                }
            }
            if (state != STATE_ACTIVE_PRIMARY)
            {
                if (isBackupServerAvailable)
                {
                    // already connected (just attempting primary above)
                    if (state == STATE_ACTIVE_BACKUP)
                    {
                        result = true;
                    }
                    else
                    {
                        Log.log.info("Attempting to connect with BACKUP TSRM Server");
                        RestCaller secondaryRestCaller = new RestCaller(tsrmConfig.getUrl2(), tsrmConfig.getUsername2(), tsrmConfig.getPassword2(), tsrmConfig.getHttpbasicauthusername2(), tsrmConfig.getHttpbasicauthpassword2());
                        for(int i = 0; i < MAX_RETRY; i++)
                        {
                            if (secondaryRestCaller.testConnection())
                            {
                                //assign the secondary rest caller
                                restCaller = secondaryRestCaller;
                                incidentObjectService.setRestCaller(restCaller);
                                serviceRequestObjectService.setRestCaller(restCaller);

                                state = STATE_ACTIVE_BACKUP;
                                Log.log.info("New state: " + state);
                                result = true;
                                break;
                            }
                            else
                            {
                                try
                                {
                                    Thread.sleep(retryDelay);
                                }
                                catch (InterruptedException e)
                                {
                                    //ignore
                                }
                                state = STATE_PASSIVE_CONNECT;
                            }
                        }
                    }

                    //all attempts failed, need to alert.
                    if(state  == STATE_PASSIVE_CONNECT)
                    {
                        Log.log.info("New state: " + state);
                        Log.alert(ERR.E50003);

                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
        }

        return result;
    } // connect

    /**
     * This method invokes the TSRM web service.
     *
     * @param filter
     * @return - Service response or null based on filter condition.
     * @throws Exception
     */
    private List<Map<String, String>> invokeTSRMService(TSRMFilter filter) throws Exception
    {
        Log.log.info("Current state: " + state);

        String nativeQuery = filter.getQuery();
        if (filter.getNativeQuery() == null)
        {
            try
            {
                nativeQuery = resolveQuery.translate(filter.getQuery());
            }
            catch (Exception e)
            {
                Log.log.debug("Did not translate, the query \"" + nativeQuery + "\" is treated as native.");
            }
        }
        filter.setNativeQuery(nativeQuery); // set it so we never attempt to
                                            // translate again.

        boolean hasVariable = VAR_REGEX_GATEWAY_VARIABLE.matcher(nativeQuery).find();
        if (hasVariable)
        {
            if (nativeQuery.contains(TSRMFilter.LAST_CHANGE_DATE))
            {
                nativeQuery = nativeQuery.replaceFirst(Constants.VAR_REGEX_GATEWAY_VARIABLE, filter.getLastChangeDate());
                nativeQuery += "&_orderbyasc=changedate";
            }
            if (nativeQuery.contains(TSRMFilter.LAST_TICKET_ID))
            {
                nativeQuery = nativeQuery.replaceFirst(Constants.VAR_REGEX_GATEWAY_VARIABLE, filter.getLastTicketId());
                nativeQuery += "&_orderbyasc=ticketid";
            }
        }

        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        String modifiedQuery = nativeQuery;
        Log.log.debug("TSRM Query to execute:::" + modifiedQuery);

        if (ServiceRequestObjectService.OBJECT_IDENTITY.equalsIgnoreCase(filter.getObject()))
        {
            result = searchServiceRequest(modifiedQuery, restCaller.getUsername(), restCaller.getPassword());
        }
        else if (IncidentObjectService.OBJECT_IDENTITY.equalsIgnoreCase(filter.getObject()))
        {
            result = searchIncident(modifiedQuery, restCaller.getUsername(), restCaller.getPassword());
        }

        return result;
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
        // TODO In future we'll refactor if any object have some other keys to
        // be referenced here.
        if (result.containsKey(TICKETID))
        {
            event.put(Constants.EXECUTE_REFERENCE, result.get(TICKETID));
        }
        if (result.containsKey(TICKETUID))
        {
            event.put(Constants.EXECUTE_ALERTID, result.get(TICKETUID));
        }
    }

    /*
     * Public API method implementation goes here
     */

    /*
     * TODO, need to see if there is heavy load, then some of these methods will
     * have to be asynchronous(?). Just not doing any preemptive optimization
     * yet.
     */
    public Map<String, String> createServiceRequest(Map<String, String> params, String username, String password) throws Exception
    {
        return serviceRequestObjectService.create(params, username, password);
    }

    public void updateServiceRequestStatus(String ticketUID, String status, String username, String password) throws Exception
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("status", status);
        // now call the common update method.
        updateServiceRequest(ticketUID, params, username, password);
    }

    public void deleteServiceRequest(String objectId, String username, String password) throws Exception
    {
        serviceRequestObjectService.delete(objectId, username, password);
    }

    public Map<String, String> selectServiceRequestByUID(String ticketId, String username, String password) throws Exception
    {
        return serviceRequestObjectService.selectByID(ticketId, username, password);
    }

    public List<Map<String, String>> searchServiceRequest(String query, String username, String password) throws Exception
    {
        return serviceRequestObjectService.search(query, username, password);
    }

    public Map<String, String> createIncident(Map<String, String> params, String username, String password) throws Exception
    {
        return incidentObjectService.create(params, username, password);
    }

    public boolean createIncidentWorklog(String ticketUID, Map<String, String> params, String username, String password) throws Exception
    {
        return incidentObjectService.createWorklog(ticketUID, params, username, password);
    }

    public void updateIncident(String ticketUID, Map<String, String> params, String username, String password) throws Exception
    {
        incidentObjectService.update(ticketUID, params, username, password);
    }

    public void updateIncidentStatus(String ticketUID, String status, String username, String password) throws Exception
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("status", status);
        // now call the common update method.
        updateIncident(ticketUID, params, username, password);
    }

    public void deleteIncident(String objectId, String username, String password) throws Exception
    {
        incidentObjectService.delete(objectId, username, password);
    }

    public Map<String, String> selectIncidentByUID(String ticketUID, String username, String password) throws Exception
    {
        return incidentObjectService.selectByID(ticketUID, username, password);
    }

    public List<Map<String, String>> searchIncident(String query, String username, String password) throws Exception
    {
        return incidentObjectService.search(query, username, password);
    }

    public String search(String objectURI, String query, String username, String password) throws Exception
    {
        return serviceRequestObjectService.search(objectURI, query, username, password);
    }

    public boolean createServiceRequestWorklog(String ticketUID, Map<String, String> params, String username, String password) throws Exception
    {
        return serviceRequestObjectService.createWorklog(ticketUID, params, username, password);
    }

    public void updateServiceRequest(String ticketUID, Map<String, String> params, String username, String password) throws Exception
    {
        serviceRequestObjectService.update(ticketUID, params, username, password);
    }
} // TSRMGateway
