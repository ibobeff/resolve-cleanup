package com.resolve.gateway.http;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.HttpHeaders;
import org.eclipse.jetty.http.HttpStatus;

import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.Gateway;
import com.resolve.gateway.MHttp;
import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveHTTP;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SystemUtil;
import com.resolve.util.metrics.ExecutionMetrics;
import com.resolve.util.metrics.ExecutionMetricsException;
import com.resolve.util.restclient.RestCallException;
import com.resolve.util.restclient.RestCaller;

import net.sf.json.JSONObject;

public class HttpGateway extends BaseClusteredGateway
{
	private static final String ROOT_URL_ACCESSIBLE_MESSAGE = "Root URL %s is accessible%s. ";
	
    // Singleton
    private static volatile HttpGateway instance = null;

    private Map<Integer, HttpServer> httpServers = new HashMap<Integer, HttpServer>();
    private Map<String, HttpServer> deployedServlets = new HashMap<String, HttpServer>();
    
    // This will be set in initialize() with the proper queue name from the
    // configuration.
    private String queue = "HTTP";

    private final HttpServer defaultHttpServer;
    
    public static HttpGateway getInstance(ConfigReceiveHTTP config)
    {
        if (instance == null)
        {
            instance = new HttpGateway(config);
            
        }
        return (HttpGateway) instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static HttpGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("HTTP Gateway is not initialized correctly..");
        }
        else
        {
            return (HttpGateway) instance;
        }
    }

    protected HttpGateway(ConfigReceiveHTTP config)
    {
        // Important, call super here.
        super(config);
        Log.log.info("HTTPGateway: Creating and starting the HTTP gateway");
        defaultHttpServer = new HttpServer(config);
        queue = config.getQueue();
    }

    @Override
    protected void initialize()
    {
        ConfigReceiveHTTP config = (ConfigReceiveHTTP) configurations;
        queue = config.getQueue().toUpperCase();

        try
        {
            Log.log.info("HTTPGateway: Initializing HTTP Listener");

            this.gatewayConfigDir = "/config/http/";
            
            if (Log.log.isDebugEnabled())
            {
                Log.log.debug("HTTPGateway: " + config.toString());
            }
        }
        catch (Exception e)
        {
        	Log.log.error("HTTPGateway: Failed to config HTTP Gateway: " + e.getMessage(), e);
        }
    }
    
    @Override
    public void deactivate() {
        
    	Log.log.info("*************************************************");
    	Log.log.info("HTTPGateway: Deactivating the HTTP servers");
    	Log.log.info("*************************************************");
        super.deactivate();
        
        for (HttpServer httpServer : httpServers.values()) {
            try {
                httpServer.stop();
            } catch (Exception e) {
                Log.log.error("HTTPGateway: " + e.getMessage(), e);
            }
        }

        try {
            defaultHttpServer.stop();
        } catch (Exception e)  {
            Log.log.error("HTTPGateway: " + e.getMessage(), e);
        }

        httpServers.clear();
    }

    @Override
    public void run()
    {
    	Log.log.info("*************************************************");
    	Log.log.info("HTTPGateway: sending sync request");
    	Log.log.info("*************************************************");
        //This call is must for all the ClusteredGateways.
        super.sendSyncRequest();
        
        //this is the point to instantiate servlet end point
        if(isPrimary() && isActive())
        {
            initHttpServers();
        }
    }

    @Override
    public void start()
    {
    	Log.log.info("*************************************************");
    	Log.log.info("HTTPGateway: Starting HTTP gateway");
    	Log.log.info("*************************************************");
        super.start();
    }

    @Override
    public void stop()
    {
    	Log.log.info("*************************************************");
    	Log.log.info("HTTPGateway: Stopping HTTP gateway");
    	Log.log.info("*************************************************");
        super.stop();
        for(HttpServer httpServer : httpServers.values())
        {
            try
            {
                httpServer.stop();
            }
            catch (Exception e)
            {
            	Log.log.error("HTTPGateway: Error stopping HTTPGateway");
                Log.log.error("HTTPGateway: " + e.getMessage(), e);
            }
        }
        try
        {
            defaultHttpServer.stop();
        }
        catch (Exception e)
        {
        	Log.log.error("HTTPGateway: Error stopping Default HTTPGateway");
            Log.log.error("HTTPGateway: " + e.getMessage(), e);
        }
        httpServers.clear();
    }

    @Override
    public String getLicenseCode()
    {
        return "HTTP";
    }

    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_HTTP;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MHttp.class.getSimpleName();
    }

    @Override
    protected Class<MHttp> getMessageHandlerClass()
    {
        return MHttp.class;
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
    	return new HttpFilter((String) params.get(HttpFilter.ID), (String) params.get(HttpFilter.ACTIVE), (String) params.
        	get(HttpFilter.ORDER), (String) params.get(HttpFilter.INTERVAL), (String) params.get(HttpFilter.EVENT_EVENTID),
            (String) params.get(HttpFilter.RUNBOOK), (String) params.get(HttpFilter.SCRIPT), (String) params. 
            get(HttpFilter.URI),(String) params.get(HttpFilter.PORT), (String) params.get(HttpFilter.SSL), (String) params.
            get(HttpFilter.ALLOWED_IP), (String) params.get(HttpFilter.BLOCKING),(String) params.get(HttpFilter.BASIC_AUTH_UN),
            (String) params.get(HttpFilter.BASIC_AUTH_PW), (String) params.get(HttpFilter.CONTENT_TYPE), (String) params.
            get(HttpFilter.RESPONSE_FILTER));
    }

    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
    }

    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList)
    {
    	Log.log.debug("HTTPGateway: Calling Gateway specific clearAndSetFilter" + (filterList != null
    						? " " + filterList.size() + ", [" + filterList + "]": ""));
        super.clearAndSetFilters(filterList);

        //this is the point to instantiate servlet end point
        if(isPrimary() && isActive())
        {
            try
            {
                //initHttpServers();
                initHttpServers();
            }
            catch (Exception e)
            {
                Log.log.error("HTTPGateway: " + e.getMessage(), e);
                //setActive(false);
            } 
        }
    }

    private void initHttpServers()
    {
        try
        {
            if(!defaultHttpServer.isStarted())
            {
                defaultHttpServer.init();
                defaultHttpServer.start();
                
                if (defaultHttpServer.isStarted())
                {
                    defaultHttpServer.addDefaultResolveServlet();
                }
            }
            
            for(Filter filter : orderedFilters)
            {
            	ConfigReceiveHTTP configReceiveHttp = (ConfigReceiveHTTP) configurations;
                HttpFilter httpFilter = (HttpFilter) filter;
                //add the URI based servlets to the default server
                if(httpFilter.getPort() == null || httpFilter.getPort() <= 0 ||
                	configReceiveHttp.getPort() == httpFilter.getPort())
                {
                    if(defaultHttpServer.isStarted())
                    {
                        defaultHttpServer.addServlet(httpFilter);
                        deployedServlets.put(filter.getId(), defaultHttpServer);
                    }
                }
                else
                {
                    HttpServer httpServer = httpServers.get(httpFilter.getPort());
                    //Clean deployment --> Remove the servlet and add them back
                    HttpServer httpServer1 = deployedServlets.get(filter.getId());
                    if(httpServer1 != null)
                    {
                        //port changed
                        httpServer1.removeServlet(filter.getId());
                        deployedServlets.remove(filter.getId());
                    }
                    if(httpServer == null)
                    {
//                        HttpServer httpServer1 = deployedServlets.get(filter.getId());
//                        if(httpServer1 != null)
//                        {
//                            //port changed
//                            httpServer1.removeServlet(filter.getId());
//                            deployedServlets.remove(filter.getId());
//                        }
                        
                        httpServer = new HttpServer(configReceiveHttp, httpFilter.getPort(), httpFilter.isSsl());
                        httpServer.init();
                        httpServer.addServlet(httpFilter);
                        httpServer.start();
                    }
                    else
                    {
                    	
                        httpServer.addServlet(httpFilter);
                    }
                    httpServers.put(httpFilter.getPort(), httpServer);
                    deployedServlets.put(filter.getId(), httpServer);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("HTTPGateway: " + e.getMessage(), e);
        }
    }

    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> undeployedFilters)
    {
        for(Filter filter : undeployedFilters.values())
        {
            HttpFilter httpFilter = (HttpFilter) filter;
            try
            {
            	ConfigReceiveHTTP configReceiveHttp = (ConfigReceiveHTTP) configurations;
                if(httpFilter.getPort() > 0 && httpFilter.getPort() != configReceiveHttp.getPort())
                {
                    HttpServer httpServer = httpServers.get(httpFilter.getPort());
                    httpServer.removeServlet(httpFilter.getId());
                    //if there is no filter/servlet remaining then stop the server on the port
                    if(httpServer.getServlets().isEmpty())
                    {
                    	Log.log.debug("HTTPGateway: Removing the filter -->" + httpFilter.getId());
                        httpServer.stop();
                        httpServers.remove(httpFilter.getPort());
                    }
                }
                else
                {
                    //undeploy the servlet from the default server.
                	Log.log.debug("HTTPGateway: Removing the Default filter --> " + httpFilter.getId());                    defaultHttpServer.removeServlet(httpFilter.getId());
                }
            }
            catch (Exception e)
            {
                Log.log.error("HTTPGateway: " + e.getMessage(), e);
            }
        }
    }

    /**
     * This method processes the message received from the HTTP server/endpoint.
     *
     * @param message
     */
    public boolean processData(String filterName, Map<String, String> params)
    {
        ExecutionMetrics.INSTANCE.registerEventStart("Http Non Blocking GW - put pushed data into the worker queue");
        boolean result = true;
        try
        {
            if(StringUtils.isNotBlank(filterName) && params != null)
            {
                HttpFilter httpFilter = (HttpFilter) filters.get(filterName);
                if (httpFilter != null && httpFilter.isActive())
                {
                    if(Log.log.isDebugEnabled())
                    {
                    	 Log.log.debug("HTTPGateway: Processing filter --> " + httpFilter.getId());
                    	 Log.log.debug("HTTPGateway: Data received after processing Runbook --> " 
                    	       		+ params != null ? params.toString() : "No Params found");
                    }
                    Map<String, String> runbookParams = params;
                    if(!params.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(params.get(Constants.EVENT_EVENTID)))
                    {
                        runbookParams.put(Constants.EVENT_EVENTID, httpFilter.getEventEventId());
                    }

                    addToPrimaryDataQueue(httpFilter, runbookParams);
                }
                else
                {
                    result = false;
                }
            }
        }
        catch (Exception e)
        {
        	Log.log.error("HTTPGateway: Error --> " + e.getMessage(), e);
            try
            {
                Thread.sleep(interval);
            }
            catch (InterruptedException ie)
            {
            	Log.log.error("HTTPGateway: sleep interrupted --> " + ie.getMessage(), ie);
            }
        }
        finally
        {
            try
            {
                ExecutionMetrics.INSTANCE.registerEventEnd("Http Non Blocking GW - put pushed data into the worker queue", 1);
            }
            catch (ExecutionMetricsException eme)
            {
                Log.log.error("HTTPGateway: " + eme.getMessage(), eme);
            }
        }
        
        return result;
    }
    
    public String processBlockingData(String filterName, Map<String, String> params)
    {
        String result = "";
        
        ExecutionMetrics.INSTANCE.registerEventStart("Http Blocking GW - execute runbook and get result or submit runbook for execution");
        
        try
        {
            if(StringUtils.isNotBlank(filterName) && params != null)
            {
                HttpFilter httpFilter = (HttpFilter) filters.get(filterName);
                if (httpFilter != null && httpFilter.isActive())
                {
                    if(Log.log.isDebugEnabled())
                    {
                    	Log.log.debug("HTTPGateway: Processing filter: " + httpFilter.getId());
                    	Log.log.debug("HTTPGateway: Data received after processing Runbook --> " 
                    			+ params != null ? params.toString() : "No Params found");
                    }
                    
                    Map<String, String> runbookParams = params;
                    
                    if(!params.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(params.get(Constants.EVENT_EVENTID)))
                    {
                        runbookParams.put(Constants.EVENT_EVENTID, httpFilter.getEventEventId());
                    }

                    runbookParams.put("FILTER_ID", filterName);
                    
                    return instance.receiveData(runbookParams);
                }
                else
                {
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "The filter is inactive.");
                    result = message.toString();
                }
            }
        }
        catch (Exception e)
        {
        	Log.log.error("HTTPGateway: Error --> " + e.getMessage(), e);
            try
            {
                Thread.sleep(interval);
            }
            catch (InterruptedException ie)
            {
            	Log.log.error("HTTPGateway: sleep interrupted --> " + ie.getMessage(), ie);
            }
        }
        finally
        {
            try
            {
                ExecutionMetrics.INSTANCE.registerEventEnd("Http Blocking GW - execute runbook and get result or submit runbook for execution", 1);
            }
            catch (ExecutionMetricsException eme)
            {
                Log.log.error("HTTPGateway: " + eme.getMessage(), eme);
            }
        }
        
        return result;
    }
    
    public int getWaitTimeout() {
        
        ConfigReceiveHTTP configReceiveHttp = (ConfigReceiveHTTP) configurations;
        
        return configReceiveHttp.getTimeout();
    }
    
    public String getRunbookResult(String problemId, String processId, String wiki) {
        
        String str = "";
        long timeout = getWaitTimeout()*1000;
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.EXECUTE_PROBLEMID, problemId);
        params.put(Constants.EXECUTE_PROCESSID, processId);
        
        try {
            Map<String, Object> result = MainBase.getESB().call(Constants.ESB_NAME_RSCONTROL, 
            		"MAction.getRunbookResult", params, timeout);
            
            if(result != null)
                str = parseRunbookResult(result, wiki);
        } catch(Exception e) {
            Log.log.error("HTTPGateway: " + e.getMessage(), e);
        }
        
        return str;
    }
    
    private Pair<HealthStatusCode, String> checkRootURLAccess() {
    	ConfigReceiveHTTP config = (ConfigReceiveHTTP) configurations;
    	
    	try {
    		String ipAddressesCS = SystemUtil.getHostIPAddress();
    	
    		String[] ipAddresses = ipAddressesCS.split(",");
    		
    		if (ipAddresses == null || ipAddresses.length == 0) {
    			throw new Exception(String.format("Failed to get IP address of server gateway-queue %s-%s is running on", 
    											  getGatewayEventType(), getQueueName()));
    		}
    		
    		String rootURL = String.format("http%s://%s:%d/", (config.getSsl() ? "s" : ""), ipAddresses[0], config.getPort());
    		
    		if (Log.log.isDebugEnabled()) {
    			Log.log.debug(String.format("rootURL = [%s]", rootURL));
    		}
    		
    		RestCaller restCaller = new RestCaller(rootURL, config.getHttpAuthBasicUsername(), 
    											   config.getHttpAuthBasicPassword());
    		
    		try {
    			Map<String, String> reqHeaders = new HashMap<String, String>();
    			reqHeaders.put(HttpHeaders.ACCEPT, "text/html");
    			
    			String respBody = restCaller.getMethod(null, null, null, null, reqHeaders);
    		} catch (RestCallException rce) {
    			if (rce.statusCode != HttpStatus.METHOD_NOT_ALLOWED_405) {
    				throw rce;
    			}
    		}
    		
    		return Pair.of(HealthStatusCode.GREEN, 
    					   String.format(ROOT_URL_ACCESSIBLE_MESSAGE, rootURL, 
    									 ((StringUtils.isNotBlank(config.getHttpAuthBasicUsername()) &&
    									   StringUtils.isNotBlank(config.getHttpAuthBasicPassword()) ? 
    									   " using basic authentication" : ""))));    		
    	} catch (Throwable t) {
    		String errMsg = String.format("Error %sin checking root URL access", 
					  					  (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""));
    		return Pair.of(Gateway.HealthStatusCode.FAILED_TO_RUN, 
					   	   String.format("Error Message = [%s], Stack Trace = [%s]", errMsg, 
					   		 		 	 ExceptionUtils.getStackTrace(t)));
    	}
    }
    
    /**
     * 
     * This method gets the health status specific to HTTP gateway.
     * A healthy HTTP gateway must have valid license and root URL "/" should be accessible.
     */
    @Override
    public Pair<HealthStatusCode, String> checkGatewayHealth(Map<String, Object> healthCheckOptions) {
    	Pair<HealthStatusCode, String> healthStatusReport = null;
    	
    	if (Log.log.isDebugEnabled()) {
    		Log.log.debug(String.format("Invoked health status check for gateway-queue %s-%s.", getGatewayEventType(),
    									getQueueName()));
    	}
    	
        // Check license status
        healthStatusReport = super.checkGatewayHealth(healthCheckOptions);
        
        if (healthStatusReport.getLeft() == Gateway.HealthStatusCode.GREEN) {
        	StringBuilder strBldr = new StringBuilder(healthStatusReport.getRight());
        	// Check access to Root URL "/"
        	healthStatusReport = checkRootURLAccess();
        	healthStatusReport = Pair.of(healthStatusReport.getLeft(), 
        								 strBldr.append(healthStatusReport.getRight()).toString());
        }
        
        if (Log.log.isDebugEnabled()) {
    		Log.log.debug(String.format("Health status check report for gateway-queue %s-%s: [%s-%s]",
    									getGatewayEventType(), getQueueName(), healthStatusReport.getLeft(),
    									healthStatusReport.getRight()));
    	}
        
        return healthStatusReport;
    }
}