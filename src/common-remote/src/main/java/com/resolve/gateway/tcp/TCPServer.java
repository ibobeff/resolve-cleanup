/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.tcp;

import java.util.HashMap;
import java.util.Map;

import com.resolve.rsremote.ConfigReceiveTCP;
import com.resolve.util.Log;

class TCPServer
{
    private Map<String, TCPListener> tcpListeners = new HashMap<String, TCPListener>();
    private Map<Integer, TCPListener> startedPorts = new HashMap<Integer, TCPListener>();

    private final ConfigReceiveTCP config;
    
    TCPServer(ConfigReceiveTCP config)
    {
        this.config = config;
    }
    
    void shutdown()
    {
        for (TCPListener tcpListener : tcpListeners.values())
        {
            tcpListener.stop();
        }
        tcpListeners.clear();
        startedPorts.clear();
    }

    void startListener(TCPFilter tcpFilter)
    {
        if(Log.log.isDebugEnabled())
        {
            Log.log.debug("Listener being added for " + tcpFilter.toString());
        }
        if (tcpFilter.isActive() && tcpFilter.getPort() > 0)
        {
            // this condition is important because at the start
            // there may be some ports started through synchronization
            // message from rscontrol
            if (hasStarted(tcpFilter.getPort()))
            {
                TCPListener tcpListener = startedPorts.get(tcpFilter.getPort());
                tcpListener.addFilter(tcpFilter);
            }
            else
            {
                TCPListener tcpListener = new TCPListener();
                tcpListener.addFilter(tcpFilter);
                
                tcpListeners.put(tcpFilter.getId(), tcpListener);
                startedPorts.put(tcpFilter.getPort(), tcpListener);

                Thread listenerThread = new Thread(tcpListener);
                listenerThread.start();
            }
        }
    }

    void stopListener(TCPFilter tcpFilter)
    {
        if(tcpFilter != null)
        {
            TCPListener tcpListener = startedPorts.get(tcpFilter.getPort());
            if(tcpListener != null)
            {
                tcpListener.removeFilter(tcpFilter);
                //if the last of the filter is removed, stop the listener
                if(tcpListener.getFilters().isEmpty())
                {
                    tcpListener.stop();
                    tcpListeners.remove(tcpFilter.getId());
                    startedPorts.remove(tcpFilter.getPort());
                }
            }
        }
    }
    
    TCPListener getListener(Integer port)
    {
        return startedPorts.get(port);
    }

    boolean hasStarted(Integer port)
    {
        return startedPorts.containsKey(port);
    }
}
