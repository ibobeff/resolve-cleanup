package com.resolve.gateway.http;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlet.ServletMapping;
import org.eclipse.jetty.util.ssl.SslContextFactory;

import com.resolve.gateway.pushauth.InvalidAuthConfigurationException;
import com.resolve.gateway.pushauth.PushAuthHandlerFactory;
import com.resolve.gateway.pushauth.UnknownAuthenticationTypeException;
import com.resolve.rsremote.ConfigReceiveHTTP;
import com.resolve.util.Log;
import com.resolve.util.ResolveDefaultHttpServlet;
import com.resolve.util.StringUtils;

public class HttpServer
{
    private final Server server;
    private final Integer port;
    private final Boolean isSsl;
    private final Map<String, HttpFilter> servlets;
    private ConfigReceiveHTTP config;
    
    
    public HttpServer(ConfigReceiveHTTP config)
    {
        this(config, config.getPort(), config.getSsl());
    }

    /**
     * This constructor is needed so that the port based filters could provide
     * their individual port and ssl setting.
     * 
     * @param config
     * @param port
     * @param isSsl
     */
    public HttpServer(ConfigReceiveHTTP config, Integer port, Boolean isSsl)
    {
        this.config = config;
        
        server = new Server();
        servlets = new HashMap<String, HttpFilter>();
        HttpConfiguration http_config = new HttpConfiguration();
        http_config.setSecureScheme("https");
        http_config.setSendXPoweredBy(false);
        http_config.setSendServerVersion(false);
                
        int httpPort = config.getPort();
        if (port != null && port > 0)
        {
            httpPort = port;
        }
        this.port = httpPort;
        this.isSsl = isSsl;
        if (httpPort > 0)
        {
        	Log.log.info("HTTPGateway: creating a new http filter with port " + httpPort + " "
        					+ "and SSL is " + isSsl.toString());
            if (isSsl)
            {
                SslContextFactory sslContextFactory = new SslContextFactory(config.getSslCertificate());
                sslContextFactory.setKeyStorePassword(config.getSslP_assword());
                sslContextFactory.setKeyManagerPassword(config.getSslP_assword());
                sslContextFactory.setIncludeProtocols("TLSv1.2");
                ServerConnector serverConnector = new ServerConnector(server, sslContextFactory,
                                                                      new HttpConnectionFactory(http_config));
                
                serverConnector.setPort(httpPort);
                server.addConnector(serverConnector);
            }
            else
            {
                ServerConnector connector = new ServerConnector(server, new HttpConnectionFactory(http_config));
                
                connector.setPort(httpPort);
                connector.setIdleTimeout(30000);
//                connector.setMaxIdleTime(30000);
                server.addConnector(connector);
            }
            ServletContextHandler context = new ServletContextHandler(server, "/");
            try
            {
                server.setHandler(PushAuthHandlerFactory.wrapHandler(config, server, context));
            }
            catch (UnknownAuthenticationTypeException | InvalidAuthConfigurationException e)
            {
            	Log.log.error("HTTPGateway: Invalid or no port provided to start the HTTP server");
            }
        }
        else
        {
            Log.log.warn("HTTPGateway: " + "Invalid port provided to start server.");
        }
    }

    public void init() throws Exception
    {
        // new servlet end point will be added to this context
        // ServletContextHandler context = new ServletContextHandler(server,
        // "/");
        // server.setHandler(context);
    }

    public void start() throws Exception
    {
    	Log.log.info("HTTPGateway: Starting HTTP Server on port: " + port + " with ssl: " + isSsl);
        try
        {
            server.start();
            Log.log.info("HTTPGateway: HTTP Server started on port: " + port + " with ssl: " + isSsl);
        }
        catch (Throwable e)
        {
        	Log.log.info("HTTPGateway: [ERROR] Failed to start HTTP Server on port: " + port + " with ssl: " + isSsl);
        	Log.log.error("HTTPGateway: ERROR -->", e);
        }
        // server.join();
    }

    public void stop() throws Exception
    {
        Log.log.info("HTTPGateway: Stopping HTTP Server on port: " + port + " with ssl: " + isSsl);
        server.stop();
    }

    public boolean isStarted()
    {
        return server.isStarted();
    }

    public boolean isStopped()
    {
        return server.isStopped();
    }

    public Map<String, HttpFilter> getServlets()
    {
        return servlets;
    }

    public void addServlet(final HttpFilter filter) throws Exception
    {
        String filterName = filter.getId();
        String uri = StringUtils.isBlank(filter.getUri()) ? filter.getId() : filter.getUri();
        boolean deploy = true;
        if(servlets.containsKey(filterName))
        {
        	Log.log.debug("HTTPGateway: Servlet already exists: " + filterName);
            HttpFilter existingFilter = servlets.get(filterName);
            //if uri changed we need to adjust
            if(StringUtils.equals(existingFilter.getUri(), filter.getUri()))
            {
                deploy = false;
            }
            else
            {
                removeServlet(filterName);
            }
        }

        if(deploy)
        {
            String finalUri = uri;
            if (!uri.startsWith("/"))
            {
                finalUri = "/" + uri;
            }
            Log.log.debug("HTTPGateway: Adding a new filter (Servlet) at URI: " + finalUri);
            ServletHandler context = getContext();
            
            ServletHolder servletHolder = new ServletHolder(filterName, new FilterServlet(filterName));
            context.addServletWithMapping(servletHolder, finalUri);
            
            PushAuthHandlerFactory.registerFilter(config, server.getHandler(), filter);
            
            
            servlets.put(filterName, filter);
        }
    }
    
    public void removeServlet(String filterName)
    {
    	Log.log.debug("HTTPGateway: Stopping filter end point for : " + filterName);
        try
        {
            ServletHandler handler = getContext();
            
            ServletHolder[] holders = handler.getServlets();
    
            List<ServletHolder> remainingServlets = new ArrayList<ServletHolder>();
            Set<String> names = new HashSet<String>();
            for (ServletHolder holder : holders)
            {
                if (!filterName.equals(holder.getName()))
                {
                    remainingServlets.add(holder);
                    names.add(holder.getName());
                }
            }
            
            List<ServletMapping> mappings = new ArrayList<ServletMapping>();
            for(ServletMapping mapping : handler.getServletMappings())
            {
               if(names.contains(mapping.getServletName()))
               {
                   mappings.add(mapping);
               }
            }
            
            /* Set the new configuration for the mappings and the servlets */
            handler.setServletMappings(mappings.toArray(new ServletMapping[0]));
            handler.setServlets(remainingServlets.toArray(new ServletHolder[0]));
            
            if(this.servlets.containsKey(filterName))
            {
                
                PushAuthHandlerFactory.unregisterFilter(config, server.getHandler(), this.servlets.get(filterName));
                
            }
            
                      
            this.servlets.remove(filterName);
            this.server.setHandler(handler);
            Log.log.debug("HTTPGateway: Successfully stopped filter end point for : " + filterName);
        }
        catch(Exception e)
        {
            if(e.getMessage() != null && e.getMessage().contains("STARTED")) //when started is new stopped? :)
            {
            	Log.log.debug("HTTPGateway: Successfully stopped filter end point for : " + filterName);
            }
            else
            {
            	Log.log.error("HTTPGateway: Could not stopped servlet for filter: " + filterName + ". " + e.getMessage());            }
        }
    }
    
    public void addDefaultResolveServlet() throws Exception
    {
    	Log.log.debug("HTTPGateway: Adding Default Resolve Servlet at URI --> " + "/");        ServletHandler context = getContext();
        ServletHolder servletHolder = new ServletHolder("/", new ResolveDefaultHttpServlet());
        context.addServletWithMapping(servletHolder, "/");
    }

    private ServletHandler getContext()
    {
        ServletHandler context;
        
        Handler subject = server.getHandler();
        
        if(subject instanceof ConstraintSecurityHandler)
        {
            
            subject = ((ConstraintSecurityHandler) subject).getHandler();

        }
        
        if(subject instanceof ServletContextHandler)
        {
            context = ((ServletContextHandler) subject).getServletHandler();
        }
        else
        {
            context = (ServletHandler) subject;
        }
        return context;
    }
}
