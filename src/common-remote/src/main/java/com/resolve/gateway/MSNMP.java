/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.Map;

import com.resolve.gateway.snmp.SNMPFilter;
import com.resolve.gateway.snmp.SNMPGateway;

/**
 * This class is used by the Message Listener and invokes it methods. Messages
 * destined for the gateways set this class name in the message header.
 *
 */
public class MSNMP extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MSNMP.setFilters";

    private static final SNMPGateway localInstance = SNMPGateway.getInstance();

    public MSNMP()
    {
        super.instance = localInstance;
    }

    @Override
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data. The following filter data is populated:
     *      - SNMPFilter.SNMP_VERSION
     *      - SNMPFilter.IP_ADDRESSES
     *      - SNMPFilter.READ_COMMUNITY
     *      - SNMPFilter.TIMEOUT
     *      - SNMPFilter.RETRIES
     *      - SNMPFilter.TRAP_RECEIVER
     *      - SNMPFilter.SNMP_TRAP_OID
     *      - SNMPFilter.OID
     *      - SNMPFilter.REGEX
     *      - SNMPFilter.COMPARATOR
     *      - SNMPFilter.VALUE
     *      
     *      
     * @param filter
     *      a {@link Filter} instance
     *            
     */
    @Override
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        if (localInstance.isActive())
        {
            SNMPFilter snmpFilter = (SNMPFilter) filter;
            filterMap.put(SNMPFilter.SNMP_VERSION, String.valueOf(snmpFilter.getSnmpVersion()));
            filterMap.put(SNMPFilter.IP_ADDRESSES, snmpFilter.getIpAddresses());
            filterMap.put(SNMPFilter.READ_COMMUNITY, snmpFilter.getReadCommunity());
            filterMap.put(SNMPFilter.TIMEOUT, String.valueOf(snmpFilter.getTimeout()));
            filterMap.put(SNMPFilter.RETRIES, String.valueOf(snmpFilter.getRetries()));
            filterMap.put(SNMPFilter.TRAP_RECEIVER, String.valueOf(snmpFilter.isTrapReceiver()));
            filterMap.put(SNMPFilter.SNMP_TRAP_OID, snmpFilter.getSnmpTrapOid());
            filterMap.put(SNMPFilter.OID, snmpFilter.getOid());
            filterMap.put(SNMPFilter.REGEX, snmpFilter.getRegex());
            filterMap.put(SNMPFilter.COMPARATOR, snmpFilter.getComparator());
            filterMap.put(SNMPFilter.VALUE, String.valueOf(snmpFilter.getValue()));
        }
    } // populateGatewaySpecificProperties
}
