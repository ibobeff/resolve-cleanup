package com.resolve.gateway.resolutionrouting;

public class UIRRSettings {

	private final String page;
	private final String automation;
	private final boolean executeOnNewWorksheetOnly;

	public UIRRSettings(String page, String automation, boolean executeOnNewWorksheetOnly) {
		this.page = page;
		this.automation = automation;
		this.executeOnNewWorksheetOnly = executeOnNewWorksheetOnly;
	}

	public String getPage() {
		return page;
	}

	public String getAutomation() {
		return automation;
	}

	public boolean isExecuteOnNewWorksheetOnly() {
		return executeOnNewWorksheetOnly;
	}

}