package com.resolve.gateway;

import java.util.Iterator;
import java.util.Map;

import com.resolve.gateway.Filter;
import com.resolve.gateway.MGateway;
import com.resolve.gateway.resolvegateway.push.PushGateway;
import com.resolve.gateway.resolvegateway.push.PushGatewayFilter;

public class MPushGateway extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MPushGateway.setFilters";

    private static final PushGateway instance = PushGateway.getInstance();

    public MPushGateway() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }
    
    public PushGateway getInstance(String gatewayName) {
        
        return PushGateway.getInstance(gatewayName);
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link QRadarPullFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        
        if (instance.isActive()) {
            PushGatewayFilter pushFilter = (PushGatewayFilter) filter;

            filterMap.put(PushGatewayFilter.PORT, "" + pushFilter.getPort());
            filterMap.put(PushGatewayFilter.BLOCKING, pushFilter.getBlocking());
            filterMap.put(PushGatewayFilter.URI, pushFilter.getUri());
            filterMap.put(PushGatewayFilter.SSL, "" + pushFilter.isSsl());
            filterMap.put(PushGatewayFilter.DEPLOYED, "" + pushFilter.isDeployed());
            
            Map<String, Object> map = pushFilter.getAttributes();
            for(Iterator<String> it=map.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                filterMap.put(key, (String)map.get(key));
            }
        }
    }

} // class MPushGateway
