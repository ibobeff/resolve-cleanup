/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.hpsm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.HPSMIncident;
import com.resolve.gateway.MHPSM;
import com.resolve.rsremote.ConfigReceiveHPSM;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class HPSMGateway extends BaseClusteredGateway
{
    // Singleton
    private static volatile HPSMGateway instance = null;

    // This will be set in initialize() with the proper queue name from the
    // configuration.
    private String queue = "HPSM";

    public static String UPDATED_TIME = "UpdatedTime";
    private static String DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";
    private static SimpleDateFormat HPSM_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    public static volatile IncidentService incidentService;

    //private final ResolveQuery resolveQuery;

    private final ConfigReceiveHPSM hpsmConfiguration;

    public static HPSMGateway getInstance(ConfigReceiveHPSM config)
    {
        if (instance == null)
        {
            instance = new HPSMGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static HPSMGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("HPSM Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    /**
     * Private constructor.
     *
     * @param config
     */
    private HPSMGateway(ConfigReceiveHPSM config)
    {
        // Important, call super here.
        super(config);
        hpsmConfiguration = config;
        queue = config.getQueue();
        //resolveQuery = new ResolveQuery(new HPSMQueryTranslator());
    }

    @Override
    public String getLicenseCode()
    {
        return "HPSM";
    }

    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_HPSM;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MHPSM.class.getSimpleName();
    }

    @Override
    protected Class<MHPSM> getMessageHandlerClass()
    {
        return MHPSM.class;
    }

    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    /**
     * Initializes the specific gateway resources.
     *
     * @param config
     */
    @Override
    protected void initialize()
    {
        ConfigReceiveHPSM hpsmConfig = (ConfigReceiveHPSM) configurations;

        queue = hpsmConfig.getQueue().toUpperCase();
        interval = hpsmConfig.getInterval() * 1000;
        this.gatewayConfigDir = "/config/hpsm/";

        incidentService = new IncidentService(hpsmConfig);
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new HPSMFilter((String) params.get(HPSMFilter.ID), (String) params.get(HPSMFilter.ACTIVE), (String) params.get(HPSMFilter.ORDER), (String) params.get(HPSMFilter.INTERVAL), (String) params.get(HPSMFilter.EVENT_EVENTID), (String) params.get(HPSMFilter.RUNBOOK), (String) params.get(HPSMFilter.SCRIPT), (String) params.get(HPSMFilter.OBJECT), (String) params.get(HPSMFilter.QUERY));
    } // getFilter

    @Override
    public void start()
    {
        Log.log.info("Starting HPSMListener");
        super.start();
    } // start

    @Override
    public void run()
    {
        //This call is must for all the ClusteredGateways.
        super.sendSyncRequest();

        //this time will be the time we start polling data from the HPSM server based on the UpdatedDate field.
        String currentServerDateTime = DateUtils.convertTimeInMillisToString(System.currentTimeMillis(), DATE_FORMAT);

        while (running)
        {
            try
            {
                long startTime = System.currentTimeMillis();

                // if any remaining events are in local event queue, doesn't get
                // messages for more new events.
                if (primaryDataQueue.isEmpty() && !primaryDataQueueExecutorBusy())
                {
                    Log.log.trace("HPSM Gateway Local Event Queue is empty and Primary Data Queue Executor is free.....");
                    for (Filter filter : orderedFilters)
                    {
                        if (shouldFilterExecute(filter, startTime))
                        {
                            HPSMFilter hpsmFilter = (HPSMFilter) filter;

                            // First time set it with the server's date time.
                            if (StringUtils.isBlank(hpsmFilter.getLastReceiveDate()))
                            {
                                hpsmFilter.setLastReceiveDate(currentServerDateTime);
                            }

                            List<Map<String, String>> results = invokeHPSMService(hpsmFilter);
                            if (results.size() > 0)
                            {
                                // Let's execute runboook for each incident
                                for (Map<String, String> hpsmEvent : results)
                                {
                                    boolean processFilter = true;

                                    if (processFilter)
                                    {
                                        //processFilter(hpsmFilter, null, hpsmEvent);
                                        addToPrimaryDataQueue(hpsmFilter, hpsmEvent);
                                    }
                                }
                                // Update the lastReceivedDate property. Add a second to it, milliseconds would have
                                // been better but HPSM only deals with seconds.
                                String lastReceivedTime = (String) results.get(results.size() - 1).get(UPDATED_TIME);
                                if (hpsmConfiguration.isUppercase())
                                {
                                    lastReceivedTime = (String) results.get(results.size() - 1).get(UPDATED_TIME.toUpperCase());
                                }
                                //HPSM always send date in GMT
                                Date dt = DateUtils.convertStringToDate(lastReceivedTime, HPSM_DATE_FORMAT, "GMT");
                                dt.setSeconds(dt.getSeconds() + 1);
                                String convertedDate = DateUtils.convertDateToString(dt, DATE_FORMAT);
                                Log.log.debug("Converted Updated Date: " + convertedDate);
                                hpsmFilter.setLastReceiveDate(convertedDate);
                            }
                        }
                    }
                }

                // process events collected by processFilter.
                //processFilterEvents(startTime);

                // If there is no filter yet no need to keep spinning, wait for
                // a while
                if (orderedFilters.size() == 0)
                {
                    Thread.sleep(interval);
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Error: " + e.getMessage(), e);
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", 
                          "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + 
                          "due to " + e.getMessage());
                
                try
                {
                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    } // run

    /**
     * This method invokes the HPSM web service.
     *
     * @param filter
     * @return - Service response or null based on filter condition.
     * @throws Exception
     */
    private List<Map<String, String>> invokeHPSMService(HPSMFilter filter) throws Exception
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        String nativeQuery = filter.getQuery();

        boolean hasVariable = VAR_REGEX_GATEWAY_VARIABLE.matcher(nativeQuery).find();
        if (hasVariable)
        {
            if (nativeQuery.contains(HPSMFilter.LAST_RECEIVE_DATE))
            {
                nativeQuery = nativeQuery.replaceFirst(Constants.VAR_REGEX_GATEWAY_VARIABLE, filter.getLastReceiveDate());
            }
        }

        if (IncidentService.OBJECT_IDENTITY.equalsIgnoreCase(filter.getObject()))
        {
            try
            {
                List<HPSMIncident> incidents = searchIncident(nativeQuery, 0, null, null);
                for (HPSMIncident incident : incidents)
                {
                    result.add(incident.toMap(hpsmConfiguration.isUppercase()));
                }
            }
            catch (Exception e)
            {
                //couldn't retrieve any data
                Log.log.trace("Couldn't retrieve any data. It could be genuine or the Query is wrong: " + nativeQuery);
                Log.log.trace("Exception: " + e.getMessage());
            }
        }

        return result;
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
        String incidentId = (hpsmConfiguration.isUppercase() ? HPSMConstants.INCIDENT_ID.toUpperCase() : HPSMConstants.INCIDENT_ID);
        String incidentTitle = (hpsmConfiguration.isUppercase() ? HPSMConstants.INCIDENT_TITLE.toUpperCase() : HPSMConstants.INCIDENT_TITLE);
        if (result.containsKey(incidentId))
        {
            event.put(Constants.EXECUTE_ALERTID, result.get(incidentId));
        }
        if (result.containsKey(incidentTitle))
        {
            event.put(Constants.EXECUTE_REFERENCE, result.get(incidentTitle));
        }
    }

    /*
     * Public API method implementation goes here
     */
    public HPSMIncident createIncident(HPSMIncident incident, String username, String password) throws Exception
    {
        return incidentService.create(incident, username, password);
    }

    public HPSMIncident createIncident(Map<String, Object> params, String username, String password) throws Exception
    {
        return incidentService.create(params, username, password);
    }

    public void updateIncident(HPSMIncident incident, String username, String password) throws Exception
    {
        incidentService.update(incident, username, password);
    }

    public void updateIncident(Map<String, Object> params, String username, String password) throws Exception
    {
        incidentService.update(params, username, password);
    }

    public void resolveIncident(HPSMIncident incident, String username, String password) throws Exception
    {
        incidentService.resolve(incident, username, password);
    }

    public void closeIncident(String incidentId, String closingCode, String username, String password) throws Exception
    {
        incidentService.close(incidentId, closingCode, username, password);
    }
    
    public void closeIncident(HPSMIncident incident, String username, String password) throws Exception
    {
        incidentService.close(incident, username, password);
    }

    public HPSMIncident selectIncidentById(String incidentId, String username, String password) throws Exception
    {
        return incidentService.selectIncidentById(incidentId, username, password);
    }

    public List<HPSMIncident> searchIncident(String query, int maxResult, String username, String password) throws Exception
    {
        return incidentService.search(query, maxResult, username, password);
    }
}
