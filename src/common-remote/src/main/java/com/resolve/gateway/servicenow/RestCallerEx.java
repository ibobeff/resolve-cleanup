package com.resolve.gateway.servicenow;

import java.io.IOException;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.ProxyAuthenticationStrategy;

import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveServiceNow;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.ConfigProxy;
import com.resolve.util.restclient.RestCaller;

public class RestCallerEx extends RestCaller
{
    ConfigReceiveServiceNow configuration;

    public RestCallerEx(ConfigReceiveServiceNow config)
    {   
        
        super(config.getUrl(), config.getHttpbasicauthusername(), config.getHttpbasicauthpassword(), 
        		MainBase.main != null ? MainBase.main.configProxy : null);
        this.configuration = config;

    }

    public RestCallerEx(String baseUrl, String httpbasicauthusername, String httpbasicauthpassword, ConfigProxy configProxy)
    {
        super(baseUrl, httpbasicauthusername, httpbasicauthpassword, MainBase.main.configProxy);

    }

    /*
     * 
     * For existing proxy setup
     */
    protected void setupProxy(HttpClientBuilder builder, CredentialsProvider cp)
    {
        if (proxyCredentials != null && proxyAuthScope != null)
        {
            cp.setCredentials(proxyAuthScope, proxyCredentials);
        }

        if (proxy != null)
        {
            builder.setProxy(super.proxy);
        }
    }

    @Override
    protected String executeMethod(HttpRequestBase method) throws Exception
    {
        String result = null;

        // Reset response status and headers
        statusCode = -1;
        reasonPhrase = "";
        responseHeaders = new HashMap<String, String>();
        CredentialsProvider credsProvider = new BasicCredentialsProvider();

        // Create an instance of HttpClient.
        HttpClientBuilder builder = HttpClientBuilder.create();
        builder.setSSLHostnameVerifier(new HostnameVerifier()
        {

            @Override
            public boolean verify(String arg0, SSLSession arg1)
            {
                return true;
            }
        });

        handleMutualTls(builder);

        handleProxyOverride(credsProvider, builder);

        handleHttpBasicAuthentication(method, credsProvider);

        handleHeaderRedir(method, credsProvider);

        builder.setDefaultCredentialsProvider(credsProvider);

        try (CloseableHttpClient client = builder.build())
        {

            // Execute the method.
            HttpResponse response = client.execute(method);
            if (response.getStatusLine() != null)
            {
                statusCode = response.getStatusLine().getStatusCode();
                reasonPhrase = response.getStatusLine().getReasonPhrase();
            }

            Header[] httpRespHeaders = response.getAllHeaders();

            if (httpRespHeaders != null && httpRespHeaders.length > 0)
            {
                for (int i = 0; i < httpRespHeaders.length; i++)
                    responseHeaders.put(httpRespHeaders[i].getName(), httpRespHeaders[i].getValue());
            }

            HttpEntity entity = response.getEntity();

            if (entity != null)
            {
                // Read the response body.
                try(java.io.InputStream content = entity.getContent())
                {
                    result = StringUtils.toString(content, "utf-8");
            
                
                }
            }
        }
        catch (IOException e)
        { 
            if(e.getMessage() != null)
                Log.log.error("Fatal transport error:: " + e.getMessage(), e);
            else
                Log.log.error("Fatal transport error", e);

            throw e;
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }

        return result;
    }

    protected void handleHttpBasicAuthentication(HttpRequestBase method, CredentialsProvider credsProvider)
    {
        if (StringUtils.isNotBlank(getHttpbasicauthusername()) && StringUtils.isNotBlank(getHttpbasicauthpassword()))
        {
            credsProvider.setCredentials(new AuthScope(method.getURI().getHost(), method.getURI().getPort()), new UsernamePasswordCredentials(getHttpbasicauthusername(), getHttpbasicauthpassword()));
        }
    }

    protected void handleProxyOverride(CredentialsProvider credsProvider, HttpClientBuilder builder)
    {
        if (configuration.getProxyhost() != null && StringUtils.isNotEmpty(configuration.getProxyhost())) // allows
                                                                                                          // overriding
                                                                                                          // proxy
                                                                                                          // setup
        {

            builder.setProxy(new HttpHost(configuration.getProxyhost(), configuration.getProxyport()));
            
            if (configuration.getProxyuser() != null && StringUtils.isNotEmpty(configuration.getProxyuser()))
            {

                credsProvider.setCredentials(new AuthScope(configuration.getProxyhost(), configuration.getProxyport()), new UsernamePasswordCredentials(configuration.getProxyuser(), configuration.getProxypass()));
                
                builder.setProxyAuthenticationStrategy(new ProxyAuthenticationStrategy());
               
            }

        }
        else
            setupProxy(builder, credsProvider);
    }

    protected void handleMutualTls(HttpClientBuilder builder) throws NoSuchAlgorithmException, KeyManagementException, Exception
    {
        if (configuration.isMutualtlsenabled())
        {
            builder.setSSLContext(new SSLContextHelper(configuration).getSSLContext());

        }
    }

    private void handleHeaderRedir(HttpRequestBase method, CredentialsProvider cp) throws Exception
    {
        if (configuration.isHeaderredirenabled())
        {

            URI uri = method.getURI();
            if (StringUtils.isNotBlank(getHttpbasicauthusername()) && StringUtils.isNotBlank(getHttpbasicauthpassword()))
            {
                if(configuration.isHeaderredirencauth())
                {
                    URIBuilder builder = new URIBuilder(uri.toString());
                    builder.setUserInfo(getHttpbasicauthusername(), getHttpbasicauthpassword());
    
                    uri =  builder.build( );
                }else
                {
                    
                    URI headerUri = URI.create(configuration.getHeaderredirurl());
                    
                    cp.setCredentials(new AuthScope(headerUri.getHost(), headerUri.getPort()), new UsernamePasswordCredentials(getHttpbasicauthusername(), getHttpbasicauthpassword()));

                
                } 
            }
            method.setHeader(configuration.getHeaderredirheader(), uri.toString());
            method.setURI(URI.create(configuration.getHeaderredirurl()));
        }
    }

}
