/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.gateway.tcp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This is a Java NIO based listener which scales well.
 * 
 */
class TCPListener implements Runnable
{

    public static final Charset charset = Charset.forName("utf-8");

    private final Map<String, TCPFilter> filters;

    private volatile RequestProcessor processor;
    private volatile NioServer nioServer;
    private volatile boolean running = false;

    private Integer port = null;
    private Integer timeout = 0; // not used for now

    private TCPGateway instance = TCPGateway.getInstance();

    TCPListener()
    {
        timeout = instance.getConfig().getTimeout();
        filters = new HashMap<String, TCPFilter>();
        processor = new RequestProcessor(instance, filters);
    }

    public Map<String, TCPFilter> getFilters()
    {
        return filters;
    }

    void addFilter(TCPFilter tcpFilter)
    {
        if (tcpFilter != null)
        {
            filters.put(tcpFilter.getId(), tcpFilter);
            processor.addFilter(tcpFilter);
            if (port == null)
            {
                port = tcpFilter.getPort();
            }
        }
    }

    void removeFilter(TCPFilter tcpFilter)
    {
        if (tcpFilter != null && filters.containsKey(tcpFilter.getId()))
        {
            filters.remove(tcpFilter.getId());
            processor.removeFilter(tcpFilter);
        }
    }

    Integer getPort()
    {
        return port;
    }

    void stop()
    {
        Log.log.info("Stopping TCP listener port: " + getPort());
        running = false;
        if (nioServer != null)
        {
            nioServer.stop();
            Log.log.debug("Closed TCP listener port: " + getPort());
        }
    }

    @Override
    public void run()
    {
        Log.log.debug("Initializing TCP listener on port: " + getPort());
        running = true;

        try
        {
            nioServer = new NioServer();
            SocketAddress addr = new InetSocketAddress(port);
            nioServer.addTcpBinding(addr); // Bind to TCP
            nioServer.addNioServerListener(processor);
            nioServer.start();
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    private static class RequestProcessor extends NioServer.Adapter
    {
        private final TCPGateway instance;
        private Map<String, TCPFilter> separators = new HashMap<String, TCPFilter>();

        public RequestProcessor(final TCPGateway instance, final Map<String, TCPFilter> filters)
        {
            this.instance = instance;

            if (filters != null)
            {
                for (TCPFilter tcpFilter : filters.values())
                {
                    TCPFilter existingFilter = separators.get(tcpFilter.getBeginSeparator());
                    if (existingFilter != null && !existingFilter.getId().equals(tcpFilter.getId()) && existingFilter.getPort().equals(tcpFilter.getPort()))
                    {
                        Log.log.warn("Duplicate begin separator for filter in the same port. Following filter will be ignored: \n" + tcpFilter.toString());
                    }
                    else
                    {
                        separators.put(tcpFilter.getBeginSeparator(), tcpFilter);
                    }
                }
            }
        }

        public void addFilter(TCPFilter tcpFilter)
        {
            if (tcpFilter != null)
            {
                separators.put(tcpFilter.getBeginSeparator(), tcpFilter);
                if (Log.log.isDebugEnabled())
                {
                    Log.log.debug("Filter added: " + tcpFilter.toString());
                    Log.log.debug("Total filter added: " + separators.size());
                }
            }
        }

        public void removeFilter(TCPFilter tcpFilter)
        {
            if (tcpFilter != null)
            {
                separators.remove(tcpFilter.getBeginSeparator());
                if (Log.log.isDebugEnabled())
                {
                    Log.log.debug("Filter removed: " + tcpFilter.toString());
                    Log.log.debug("Total filter after remove: " + separators.size());
                }
            }
        }

        StringBuilder content = new StringBuilder();
        boolean hasStarted = false;
        TCPFilter tcpFilter = null;
        
        @Override
        public void tcpDataReceived(NioServer.Event evt)
        {
            if (evt != null && evt.getInputBuffer() != null)
            {
                ByteBuffer inBuff = evt.getInputBuffer();

                //append the incoming message
                String input = charset.decode(inBuff).toString();
                Log.log.debug("Incoming data: " + input);
                content.append(input);
                
                if(!hasStarted)
                {
                    tcpFilter = getMatchedFilter(content.toString());
                    if (tcpFilter != null)
                    {
                        int beginIndex = content.indexOf(tcpFilter.getBeginSeparator());
                        int beginSeparatorLength = tcpFilter.getBeginSeparator().length();
                        int length = 0;
                        if(Log.log.isDebugEnabled())
                        {
                            Log.log.debug("Found matched filter: " + tcpFilter.toString());
                        }
                        
                        if (!tcpFilter.getIncludeBeginSeparator())
                        {
                            length = beginSeparatorLength;
                        }
                        content.delete(0, length);
                        hasStarted = true;
                    }
                    else
                    {
                        content.setLength(0);
                    }
                }
                
                if(hasStarted)
                {
                    int lastIndex = content.lastIndexOf(tcpFilter.getEndSeparator());
                    int separatorLength = tcpFilter.getEndSeparator().length();
                    if (lastIndex >= 0)
                    {
                        Log.log.debug("End found: " + tcpFilter.getEndSeparator());
                        String inputRemainder = content.substring(lastIndex + separatorLength);
                        int length = lastIndex + separatorLength;
                        if (!tcpFilter.getIncludeEndSeparator())
                        {
                            length = lastIndex;
                        }
                        content.setLength(length);
                        instance.processData(tcpFilter.getId(), content.toString());
                        hasStarted = false;
                        content.setLength(0);
                        content.append(inputRemainder);
                    }
                }
                
                //Log.log.debug("Content gathered so far: " + content);
            }
        }

        /**
         * This method matches the input with one of the filters' begin
         * separator and returns it. The input could be just one line for
         * example: <TestData attr="1"><Data1>Hello</Data1></TestData> where
         * begin separator is <TestData and end separator is /TestData>
         * 
         * @param input
         * @return
         */
        private TCPFilter getMatchedFilter(String input)
        {
            TCPFilter result = null;
            if (StringUtils.isNotBlank(input))
            {
                /*
                if (Log.log.isDebugEnabled())
                {
                    for(TCPFilter filter : separators.values())
                    {
                        Log.log.debug("Currently deployed filter: " + filter.toString());
                    }
                }
                */
                
                String trimmedInput = input.trim();
                //text separator
                Map<Integer, TCPFilter> foundFilters = new TreeMap<Integer, TCPFilter>(); 
                for (String beginSeparator : separators.keySet())
                {
                    if (trimmedInput.equals(beginSeparator) || trimmedInput.startsWith(beginSeparator) || trimmedInput.contains(beginSeparator))
                    {
                        foundFilters.put(trimmedInput.indexOf(beginSeparator), separators.get(beginSeparator));
                    }
                }
                if(!foundFilters.isEmpty())
                {
                    result = foundFilters.get(0); //get the first item
                }
            }
            return result;
        }
    }
}
