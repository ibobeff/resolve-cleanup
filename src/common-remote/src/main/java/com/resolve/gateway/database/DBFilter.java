/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.database;

import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.RetainValue;
import com.resolve.util.StringUtils;

public class DBFilter extends BaseFilter
{
    // Additional field names for this filter.
    public static final String POOL = "POOL";
    public static final String SQL = "SQL";
    public static final String LASTVALUECOLUMN = "LASTVALUECOLUMN";
    public static final String LASTVALUE = "LASTVALUE";
    public static final String PREFIX = "PREFIX";
    public static final String LASTVALUEQUOTE = "LASTVALUEQUOTE";
    public static final String UPDATESQL = "UPDATESQL";

    private String pool = "resolve";
    private String sql = "";
    private String lastValue;
    private String lastValueColumn;
    private String prefix = "";
    private boolean lastValueQuote = true;
    private long lastPollingTime = 0; // local timestamp (millis)
    private String updateQuery = ""; // e.g., update my_table set
                                     // resolve_status='1' where
                                     // my_id='${MY_ID}';

    public DBFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String poolname, String sql, String lastValue, String lastValueColumn, String lastValueQuote, String prefix, String updateQuery)
    {
        super(id, active, order, interval, eventEventId, runbook, script);
        setSql(sql);
        setPool(poolname);
        //this.sql = sql;
        setLastValue(lastValue);
        setLastValueColumn(lastValueColumn);
        setPrefix(prefix);
        setLastValueQuote(lastValueQuote);
        setUpdateQuery(updateQuery);
    } // DBFilter

    public long getLastPollingTime()
    {
        return lastPollingTime;
    }

    public void setLastPollingTime(long lastPollingTime)
    {
        this.lastPollingTime = lastPollingTime;
    }

    public String getPool()
    {
        return pool;
    }

    public void setPool(String pool)
    {
        this.pool = pool != null ? pool.trim() : pool;
    }

    public String getSql()
    {
        return sql;
    }

    public void setSql(String sql)
    {
        this.sql = sql != null ? sql.trim() : sql;
    }
    @RetainValue
    public String getLastValue()
    {
        return lastValue;
    }

    public void setLastValue(String lastValue)
    {
        this.lastValue = lastValue != null ? lastValue.trim() : lastValue;
    }

    public boolean isLastValueEmpty()
    {
        boolean result = false;
        
        if(StringUtils.isBlank(lastValue) || lastValue.equalsIgnoreCase("undefined") || lastValue.equalsIgnoreCase("null"))
        {
            result = true;
        }
        return result;
    }

    public String getLastValueColumn()
    {
        return lastValueColumn;
    }

    public void setLastValueColumn(String lastValueColumn)
    {
        this.lastValueColumn = lastValueColumn != null ? lastValueColumn.trim() : lastValueColumn;
    }

    public String getPrefix()
    {
        return prefix;
    }

    public void setPrefix(String prefix)
    {
        this.prefix = prefix != null ? prefix.trim() : prefix;
    }

    public boolean isLastValueQuote()
    {
        return lastValueQuote;
    }

    public void setLastValueQuote(boolean lastValueQuote)
    {
        this.lastValueQuote = lastValueQuote;
    }
    
    public void setLastValueQuote(String lastValueQuote)
    {
        if (lastValueQuote != null && lastValueQuote.trim().equalsIgnoreCase("FALSE"))
        {
            this.lastValueQuote = false;
        }
        else
        {
            this.lastValueQuote = true;
        }
    }
    
    public String getUpdateQuery()
    {
        return updateQuery;
    }

    public void setUpdateQuery(String updateQuery)
    {
        this.updateQuery = updateQuery != null ? updateQuery.trim() : updateQuery;
    }
}
