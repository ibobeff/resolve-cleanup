/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.gateway;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.ssh.SubnetMask;
import com.resolve.gateway.xmpp.XMPPAddress;
import com.resolve.gateway.xmpp.XMPPFilter;
import com.resolve.gateway.xmpp.XMPPGateway;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class is used by the Message Listener and invokes it methods. Messages
 * destined for the gateways set this class name in the message header.
 *
 */
public class MXMPP extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MXMPP.setFilters";
    private static final String RSCONTROL_SET_XMPPADDRESSES = "MXMPP.setXMPPAddresses";

    private static final XMPPGateway localInstance = XMPPGateway.getInstance();

    public MXMPP()
    {
        super.instance = localInstance;
    }

    @Override
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    @Override
    protected void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        if (localInstance.isActive())
        {
            XMPPFilter xmppFilter = (XMPPFilter) filter;
            filterMap.put(XMPPFilter.REGEX, xmppFilter.getRegex());
        }
    }

    /**
     * This method is invoked once filter synchronization message from RSCONTROL
     * is received.
     * 
     * @param paramList
     */
    @Override
    public void synchronizeGateway(Map<String, List<Map<String, Object>>> paramList)
    {
        if (paramList != null)
        {
            isUpdateRSControl = false;
            Log.log.debug("Processing synchronization message received for " + localInstance.getQueueName());
            List<Map<String, Object>> xmppAddresses = (List<Map<String, Object>>) paramList.get("XMPPADDRESSES");
            int number = 0;
            if (xmppAddresses != null)
            {
                number = xmppAddresses.size();
            }
            Log.log.debug("NUMBER of XMPP addresses " + number);

            List<Map<String, Object>> filters = (List<Map<String, Object>>) paramList.get("FILTERS");
            number = 0;
            if (filters != null)
            {
                number = filters.size();
            }
            Log.log.debug("NUMBER of filters " + number);

            List<Map<String, Object>> nameProperties = (List<Map<String, Object>>) paramList.get("NAMEPROPERTIES");
            number = 0;
            if (nameProperties != null)
            {
                number = nameProperties.size();
            }
            Log.log.info("NUMBER of nameProperties " + number);

            clearAndSetXMPPAddresses(xmppAddresses);
            clearAndSetFilters(filters);
            clearAndSetNameProperties(nameProperties);

            isUpdateRSControl = true;
            // tell the gateway that we have received RSControl sync message.
            localInstance.setSyncDone(true);
        }
    }

    /**
     * This method is called by the social UI when for example user
     * add/deletes/update a process email/im.
     */
    public static void sendSyncRequest(Map<String, Object> paramList)
    {
        // we do not want to ask for synchronization if there is
        if (paramList != null && paramList.containsKey("UPDATE_SOCIAL_POSTER"))
        {
            Boolean updateSocialPoster = Boolean.parseBoolean((String) paramList.get("UPDATE_SOCIAL_POSTER"));
            if (updateSocialPoster && localInstance.isSocialPoster())
            {
                localInstance.sendSyncRequest();
            }
        }
        else
        {
            // some other consumer could ask for everybody to sync.
            localInstance.sendSyncRequest();
        }
    }

    /**
     * Clears all XMPP addresses out of the current {@link XMPPGateway}
     * instance. If the paramList isn't null, it will propagate all of its mapped addresses
     * into the XMPPGateway instance. paramList keys include: { XMPPADDRESS :
     * String }
     * 
     * @param paramList  A {@link List} of {@link Map}s of {@link SubnetMask} initialization parameters.
     *            paramList keys include: { SubnetMask.SUBNETMASK : String,
     *            SubnetMask.MAXCONNECTION : String, SubnetMask.TIMEOUT : String
     *            }
     */
    @FilterCompanionModel(modelName = "XMPPAddress")
    public void clearAndSetXMPPAddresses(List<Map<String, Object>> paramList)
    {
        // clear and set filters
        localInstance.clearAndSetXMPPAddresses(paramList);

        // send new filters
        Map<String, Object> params = null;
        if (paramList != null)
        {
            // extract the first Map so we get the username who is deploying the
            // filter
            if (paramList != null && paramList.size() > 0)
            {
                params = paramList.get(0);
            }
        }
        getXMPPAddresses(params);
        // save config
        MainBase.main.exitSaveConfig();

    } // clearAndSetFilters

    public void getXMPPAddresses(Map<String, Object> params)
    {
        if (localInstance.isActive())
        {
            Map<String, String> content = new HashMap<String, String>();

            for (XMPPAddress xmppAddress : localInstance.getXMPPAddresses().values())
            {
                Map<String, Object> filterMap = new HashMap<String, Object>();
                filterMap.put(XMPPAddress.XMPPADDRESS, xmppAddress.getXmppAddress());
                filterMap.put(XMPPAddress.XMPPP_ASSWORD, xmppAddress.getXmppPassword());
                filterMap.put("QUEUE", localInstance.getQueueName());

                // This value comes all the way from the gateway filter
                // deployment UI.
                // This is important so RSConrtol can set the user name who
                // deployed the filter.
                if (params != null)
                {
                    filterMap.put(Constants.SYS_UPDATED_BY, params.get(Constants.SYS_UPDATED_BY));
                }
                content.put(xmppAddress.getXmppAddress(),  StringUtils.remove(StringUtils.mapToString(filterMap), "null"));
            }

            // send content to rscontrol
            if (isUpdateRSControl)
            {
                // this is important as RSControl need this
                content.put("QUEUE", localInstance.getQueueName());

                if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROL, RSCONTROL_SET_XMPPADDRESSES, content) == false)
                {
                    Log.log.warn("Failed to send email addresses to RSCONTROL");
                }
            }
        }
    } // getFilters

    /**
     * Sends a chat message to the chat user.
     *
     * @param to
     * @param message
     * @param username
     * @param password
     * @throws Exception
     */
    public void sendMessage(String to, String message, String username, String password) throws Exception
    {
        localInstance.sendMessage(to, message, username, password);
    }

    public void sendMessage(Map<String, Object> params)
    {
        if (localInstance.isActive() && localInstance.isPrimary())
        {
            if (params != null && params.size() > 0)
            {
                String to = (String) params.get(Constants.NOTIFICATION_MESSAGE_TO);
                String content = (String) params.get(Constants.NOTIFICATION_MESSAGE_CONTENT);
                String username = (String) params.get(Constants.NOTIFICATION_CARRIER_USERNAME);
                String password = (String) params.get(Constants.NOTIFICATION_CARRIER_PASSWORD);
                try
                {
                    sendMessage(to, content, username, password);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
    } // sendMessage
}
