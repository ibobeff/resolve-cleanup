/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.servicenow;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;

import org.apache.commons.lang3.tuple.Pair;

import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.Gateway;
import com.resolve.gateway.MServiceNow;
import com.resolve.query.ResolveQuery;
import com.resolve.query.translator.ServiceNowQueryTranslator;
import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveServiceNow;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.RestCallException;
import com.resolve.util.restclient.RestCaller;

public class ServiceNowGateway extends BaseClusteredGateway
{
    // Singleton
    private static volatile ServiceNowGateway instance = null;

    // This will be set in initialize() with the proper queue name from the
    // configuration.
    private String queue = "SERVICENOW";

    private static final String NUMBER = "number";
    public static final String SYS_ID = "sys_id";
    public static String CHANGE_DATE = "sys_updated_on";

    public static volatile ObjectService genericObjectService;

    private final ResolveQuery resolveQuery;

    public static ServiceNowGateway getInstance(ConfigReceiveServiceNow config)
    {
        if (instance == null)
        {
            instance = new ServiceNowGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static ServiceNowGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("ServiceNow Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    /**
     * Private constructor.
     *
     * @param config
     */
    private ServiceNowGateway(ConfigReceiveServiceNow config)
    {
        // Important, call super here.
        super(config);
        queue = config.getQueue();
        resolveQuery = new ResolveQuery(new ServiceNowQueryTranslator());
    }

    /**
     * This method gets the health status specific to SNOW gateway.
     * A healthy ServiceNOW gateway must have valid license and the ServiceNOW server is reachable
     */
    
    @Override
    public Pair<HealthStatusCode, String> checkGatewayHealth(Map<String, Object> healthCheckOptions) {
        String gatewayId = getQueueName() + " ";
        // Check license status
        Pair<HealthStatusCode, String> licenseStatus = super.checkGatewayHealth(healthCheckOptions);
        if (licenseStatus.getLeft() != Gateway.HealthStatusCode.GREEN) {
            return Pair.of(licenseStatus.getLeft(), gatewayId + licenseStatus.getRight());
        }
        try {
            // Check health status specific to SNOW gateway.
            // A healthy SNOW gateway must be able to communicate with the client.
            // We can test the server by try getting the last incident and the returning status RED if
            // 1. Trouble with authentication (401 ) or authorization (403)
            // 2. Can communicate with the server (not reachable)
            genericObjectService.getLatestIncidentNumber();
        } catch (RestCallException e) {
            // 1. Status RED dues to authentication (401 ) or authorization (403)
            if (e.statusCode == 401 || e.statusCode == 403) {
                // Status RED dues to Authentication AND/OR Authorization
                Log.log.warn(SERVER_NOT_AUTH_MESSAGE, e);
                return Pair.of(Gateway.HealthStatusCode.RED, gatewayId + SERVER_NOT_AUTH_MESSAGE);
            }
        } catch (Exception e) {
            // Status RED dues to server not reachable.
            Log.log.warn(SERVER_UNREACHABLE_MESSAGE, e);
            return Pair.of(Gateway.HealthStatusCode.RED, gatewayId + SERVER_UNREACHABLE_MESSAGE);
        }
        // All is good. Status GREEN
        return Pair.of(Gateway.HealthStatusCode.GREEN, gatewayId + HEALTHY_GATEWAY_MESSAGE);
    }

    @Override
    public String getLicenseCode()
    {
        return "SERVICENOW";
    }

    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_SERVICENOW;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MServiceNow.class.getSimpleName();
    }

    @Override
    protected Class<MServiceNow> getMessageHandlerClass()
    {
        return MServiceNow.class;
    }

    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    /**
     * Initializes the specific gateway resources.
     *
     * @param config
     */
    @Override
    protected void initialize()
    {
        ConfigReceiveServiceNow serviceNowConfig = (ConfigReceiveServiceNow) configurations;

        queue = serviceNowConfig.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/servicenow/";

        //genericObjectService = ObjectServiceFactory.getObjectService(serviceNowConfig, IncidentObjectService.OBJECT_IDENTITY);
        genericObjectService = new GenericObjectService(serviceNowConfig) ;
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new ServiceNowFilter((String) params.get(ServiceNowFilter.ID), (String) params.get(ServiceNowFilter.ACTIVE), (String) params.get(ServiceNowFilter.ORDER), (String) params.get(ServiceNowFilter.INTERVAL), (String) params.get(ServiceNowFilter.EVENT_EVENTID)
        // , (String) params.get(ServiceNowFilter.PERSISTENT_EVENT)
                        , (String) params.get(ServiceNowFilter.RUNBOOK), (String) params.get(ServiceNowFilter.SCRIPT), (String) params.get(ServiceNowFilter.OBJECT), (String) params.get(ServiceNowFilter.QUERY));
    } // getFilter

    @Override
    public void start()
    {
    	Log.log.info("********************************************************");
    	Log.log.info("ServiceNowGateway: Starting ServiceNowListener");
    	Log.log.info("********************************************************");
        super.start();
    } // start

    @Override
    public void run()
    {
    	Log.log.info("ServiceNowGateway: Sending Sync request to rscontrol");
        //This call is must for all the ClusteredGateways.
        super.sendSyncRequest();

        String serviceNowServerDateTime = getServiceNowServerDateTime();
        String incidentNumber="";
        try
        {
            incidentNumber = getLatestIncidentNumber();
        }
        catch (Exception e)
        {
        	Log.log.error("ServiceNowGateway: Error--> " + e.getMessage(), e);
            Log.alert("ServiceNowGateway: " + e.getMessage() + ". ", "Plese note ${LAST_NUMBER} in filter queries will not be replaced with the Incident Number. Filter queries that uses ${LAST_NUMBER} macro will fail.");

        }

        while (running)
        {
            try
            {
                long startTime = System.currentTimeMillis();

                // if any remaining events are in local event queue, doesn't get
                // messages for more new events.
                if (primaryDataQueue.isEmpty())
                {
                	Log.log.trace("ServiceNowGateway: Local Event Queue is empty.....");
                	for (Filter filter : orderedFilters)
                    {
                        Log.putCurrentNamedContext("filter", filter.getId());
                        if (shouldFilterExecute(filter, startTime))
                        {
                            ServiceNowFilter serviceNowFilter = (ServiceNowFilter) filter;

                            // First time set it with the server's date time.
                            if (StringUtils.isEmpty(serviceNowFilter.getLastChangeDate()))
                            {
                                serviceNowFilter.setLastChangeDate(serviceNowServerDateTime);
                            }
                            if (StringUtils.isEmpty(serviceNowFilter.getLastNumber()) && (!incidentNumber.equals("")))
                            {
                                serviceNowFilter.setLastNumber(incidentNumber);
                            }

                            List<Map<String, String>> results = invokeServiceNowService(serviceNowFilter);
                            if (results.size() > 0)
                            {

                                // Let's execute runboook for each incident
                                for (Map<String, String> serviceNowObject : results)
                                {
                                    //processFilter(serviceNowFilter, null, serviceNowObject);
                                    addToPrimaryDataQueue(serviceNowFilter, serviceNowObject);
                                }

                                // Update the lastChangeDate property.
                                // since we intentionally get the objects
                                // ordered by changed date in ascending order,
                                // go to the bottom of the list and get change
                                // date from the last object.
                                String lastChangeDate = (String) results.get(results.size() - 1).get(CHANGE_DATE);
                                String lastNumber = (String) results.get(results.size() - 1).get(NUMBER);
                                Log.log.debug("ServiceNowGateway: The retrieved last change date is " + lastChangeDate);
                                Log.log.debug("ServiceNowGateway: The retrieved last number is " + lastNumber);

                                serviceNowFilter.setLastChangeDate(lastChangeDate);
                                serviceNowFilter.setLastNumber(lastNumber);
                            }
                        }
                        else
                        {
                        	Log.log.trace("ServiceNowGateway: Filter:" + filter.getId() + " is inactive or ");
                        }
                    }
                }

                // process events collected by processFilter.
                //processFilterEvents(startTime);

                // If there is no filter yet no need to keep spinning, wait for
                // a while
                if (orderedFilters.size() == 0)
                {
                    Thread.sleep(interval);
                }
            }
            catch (Exception e)
            {
            	Log.log.warn("ServiceNowGateway: Error --> " + 
                          "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + 
                          "due to " + e.getMessage());
                
                try
                {
                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                	Log.log.warn("ServiceNowGateway: sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    } // run

    /**
     * This method invokes the ServiceNow web service.
     *
     * @param filter
     * @return - Service response or null based on filter condition.
     * @throws Exception
     */
    private List<Map<String, String>> invokeServiceNowService(ServiceNowFilter filter) throws Exception
    {
        String orderBy = CHANGE_DATE;

        String nativeQuery = filter.getQuery();

        if (nativeQuery != null)
        {
            if (filter.getNativeQuery() == null)
            {
                try
                {
                    nativeQuery = resolveQuery.translate(filter.getQuery());
                }
                catch (Exception e)
                {
                	Log.log.debug("ServiceNowGateway: Did not translate, the query \"" + nativeQuery +
                			"\" is treated as native. : " + e.getMessage());
                }
            }
            filter.setNativeQuery(nativeQuery); // set it so we never attempt to
                                                // translate again.
            Matcher matcher = VAR_REGEX_CONNECTOR_VARIABLE.matcher(nativeQuery);
            StringBuffer stringBuffer = new StringBuffer();

            while (matcher.find())
            {
                if (matcher.group(0).contains(ServiceNowFilter.LAST_CHANGE_DATE))
                {
                    matcher.appendReplacement(stringBuffer, filter.getLastChangeDate());
                    /*
                     * characters from the input text being appended to the
                     * StringBuffer, and the matched text being replaced. Only
                     * the characters starting from then end of the last match,
                     * and until just before the matched characters are copied.
                     * The appendReplacement() method keeps track of what has
                     * been copied into the StringBuffer, so you can continue
                     * searching for matches using find() until no more matches
                     * are found in the input text.
                     */
                }
                else if (matcher.group(0).contains(ServiceNowFilter.LAST_NUMBER))
                {
                    matcher.appendReplacement(stringBuffer, filter.getLastNumber());
                }
            }
            // characters from the end of the last match and until the end of
            // the input text
            matcher.appendTail(stringBuffer);
            nativeQuery = stringBuffer.toString();
            Log.log.debug("ServiceNowgateway: Updated Filter Query after replacing all instances of "
            		            		+ "" + ServiceNowFilter.LAST_CHANGE_DATE + " and or " + ServiceNowFilter.LAST_NUMBER + 
            		            		" is: " + nativeQuery);
        }
        // ConfigReceiveServiceNow serviceNowConfig = (ConfigReceiveServiceNow)
        // configurations;
        /*
         * List<Map<String, String>> result = new ArrayList<Map<String,
         * String>>(); if
         * (IncidentObjectService.OBJECT_IDENTITY.equalsIgnoreCase(filter.
         * getObject())) {
         */
        Log.log.debug("ServiceNowGateway: ServiceNow Incident Query to execute for " + 
        		         filter.getObject() + ":::" + filter.getQuery() + ":::" + nativeQuery);
        /*
         * No need of this as we expect the query to include everything.
         * if(StringUtils.isNotEmpty(nativeQuery) &&
         * StringUtils.isNotEmpty(filter.getLastChangeDate())) { nativeQuery +=
         * "^sys_created_on>" + filter.getLastChangeDate(); nativeQuery +=
         * "^incident_state=" + 1; //We're interested in only new incidents. }
         */
        return genericObjectService.search(filter.getObject(), nativeQuery, orderBy, true, null, null);
        /*
         * }
         * 
         * return result;
         */
    }

    /**
     * This method attempts to acces the ServiceNow server for its current date
     * and time in GMT. There is a "Scripted Web Service" we need to create in
     * ServiceNow server for this. If we do not create the web service then we
     * will use whatever current Date and Time in the Resolve server which may
     * be out of sync with ServiceNow server.
     */
    private String getServiceNowServerDateTime()
    {
        String result = null;

        ConfigReceiveServiceNow serviceNowConfig = (ConfigReceiveServiceNow) configurations;
        
        try
        {
            RestCaller restCaller = new RestCallerEx(serviceNowConfig );
        
            restCaller.setUrlSuffix("");
        
            String response = restCaller.getMethod(serviceNowConfig.getDatetimerestserviceurl(), (Map<String, String>)null);
            Map<String, String> serverDateTime = StringUtils.jsonObjectStringToMap("result", response);
            
            if (serverDateTime != null && !serverDateTime.isEmpty())
            {
                Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("Etc/GMT+0"));
                cal.setTimeInMillis(Long.parseLong(serverDateTime.get("serverdatetime")));
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                df.setCalendar(cal);
                result = df.format(cal.getTime());
                Log.log.info("ServiceNowGateway: Rsremote Serverdatetime : " + serverDateTime.
                		get("serverdatetime") + ", ServiceNow Server Date Time : " + result);
            }
        }
        catch(Exception e)
        {
            Log.log.error("ServiceNowGateway: Failed to get ServiceNow server date time using url " + 
                         serviceNowConfig.getDatetimewebservicename() + 
                         " with user id " + serviceNowConfig.getHttpbasicauthusername() + 
                         ", Using this rsremote's local time as ServiceNow server date time!!!");
        }
        
        if (StringUtils.isEmpty(result))
        {
            Calendar now = Calendar.getInstance();
            TimeZone tz = now.getTimeZone();
            String tzStr = tz.getID();
            result = DateUtils.convertDateToGMTString(now.toString(), tzStr);
            Log.log.warn("ServiceNowGateway: Local Server Date Time set as ServiceNow server "
          				+ "date time --> " + result);
        }

        return result;
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
        if(result.containsKey(SYS_ID))
        {
            event.put(Constants.EXECUTE_REFERENCE, result.get("sys_id"));
            event.remove(SYS_ID);
        }
        else if(result.containsKey("SYS_ID"))
        {
            event.put(Constants.EXECUTE_REFERENCE, result.get("SYS_ID"));
            event.remove("SYS_ID");
        }
        
        if (result.containsKey(NUMBER))
        {
            event.put(Constants.EXECUTE_ALERTID, result.get(NUMBER));
            event.remove(NUMBER);
        }
        else if(result.containsKey("NUMBER"))
        {
            event.put(Constants.EXECUTE_ALERTID, result.get("NUMBER"));
            event.remove("NUMBER");
        }
    }

    public boolean checkGatewayConnection() throws Exception
    {
        List<Map<String, String>> searchResult = Collections.emptyList();
        
        try
        {
            searchResult = searchObjectWithPageLimit("alm_license", "", "", true, 1, "", "");
            
            //don't care about what the response is, as long as there is a response.  Timeouts will go to the catch statement.
//            if (searchResult == null || searchResult.isEmpty())
//            {
//                throw new Exception ("Failed to get single software license from ServiceNow server. Connection to ServiceNow server possibly broken.");
//            }
//            else
//            {
            Log.log.debug("ServiceNowGateway: Instance " + MainBase.main.configId.getGuid() + " : " + 
               		getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + 
            		" connected to configured ServiceNow server");
        }
        catch(Exception e)
        {
        	 Log.alert("ServiceNowGateway: Instance " + MainBase.main.configId.getGuid() + " breached " 
        			 + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-ConnectionSelfCheck", 
                       "Instance " + MainBase.main.configId.getGuid() + "Breached " + getLicenseCode() + "-" 
        	     + getQueueName() + "-" + getInstanceType() + "-ConnectionSelfCheck" + " due to " + e.getMessage()); 
        }
        return true;
    }
    
    /*
     * Public API method implementation goes here
     */
    @Deprecated
    public Map<String, String> createIncident(Map<String, String> params, String username, String p_assword) throws Exception
    {
        return genericObjectService.create("incident", params, username, p_assword);
    }

    @Deprecated
    public boolean createIncidentWorknote(String parentSysId, String note, String username, String p_assword) throws Exception
    {
        return genericObjectService.createWorknote("incident", parentSysId, note, username, p_assword);
    }

    @Deprecated
    public void updateIncident(String sysId, Map<String, String> params, String username, String password) throws Exception
    {
        genericObjectService.update("incident", sysId, params, username, password);
    }

    @Deprecated
    public void updateIncidentStatus(String sysId, String status, String username, String password) throws Exception
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("incident_state", status);
        genericObjectService.update("incident", sysId, params, username, password);
    }

    @Deprecated
    public void deleteIncident(String sysId, String username, String password) throws Exception
    {
        genericObjectService.delete("incident", sysId, username, password);
    }

    @Deprecated
    public Map<String, String> selectIncidentBySysId(String sysId, String username, String password) throws Exception
    {
        return genericObjectService.selectByID("incident", sysId, username, password);
    }

    @Deprecated
    public Map<String, String> selectIncidentByNumber(String number, String username, String password) throws Exception
    {
        return genericObjectService.selectByAlternateID("incident", "number", number, username, password);
    }

    @Deprecated
    public List<Map<String, String>> searchIncident(String query, String username, String password) throws Exception
    {
        return genericObjectService.search("incident", query, null, false, username, password);
    }
    
    //Generic ServiceNow Object CRUD methods
    
    public Map<String, String> createObject(String objectType, Map<String, String> params, String username, String password) throws Exception
    {
        return genericObjectService.create(objectType, params, username, password);
    }
    
    public Map<String, String> selectObjectBySysId(String objectType, String sysId, String username, String password) throws Exception
    {
        return genericObjectService.selectByID(objectType, sysId, username, password);
    }
    
    public Map<String, String> selectObjectByAlternateID(String objectType, String altIDName, String altIDValue, String username, String password) throws Exception
    {
        return genericObjectService.selectByAlternateID(objectType, altIDName, altIDValue, username, password);
    }
    
    public List<Map<String, String>> searchObject(String objectType, String query, String orderBy, boolean isAscending, String username, String password) throws Exception
    {
        return genericObjectService.search(objectType, query, orderBy, isAscending, username, password);
    }
    
    public void updateObject(String objectType, String sysId, Map<String, String> params, String username, String password) throws Exception
    {
        genericObjectService.update(objectType, sysId, params, username, password);
    }
    
    public void deleteObject(String objectType, String sysId, String username, String password) throws Exception
    {
        genericObjectService.delete(objectType, sysId, username, password);
    }
    
    public List<Map<String, String>> searchObjectWithPageLimit(String objectType, String query, String orderBy, boolean isAscending, int pageLimit, String username, String password) throws Exception
    {
        return genericObjectService.searchWithPageLimit(objectType, query, orderBy, isAscending, pageLimit, username, password);
    }
    
    public String getLatestIncidentNumber() throws Exception {
        
        return genericObjectService.getLatestIncidentNumber();
        
    }
	
	 public String importSetApi(String stagingTableName, HashMap<String, String> params, String username, String p_assword) throws Exception
    {
        return genericObjectService.importSetApi(stagingTableName, params, username, p_assword);
    }
}
