/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.hpom;

import java.math.BigInteger;
import java.net.InetAddress;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.validation.Schema;

import org.apache.axis2.AxisFault;
import org.apache.axis2.client.Options;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.apache.xerces.dom.ElementNSImpl;
import org.dmtf.schemas.wbem.wsman._1.wsman.AttributableDuration;
import org.dmtf.schemas.wbem.wsman._1.wsman.AttributableURI;
import org.dmtf.schemas.wbem.wsman._1.wsman.SelectorSetType;
import org.dmtf.schemas.wbem.wsman._1.wsman.SelectorType;
import org.xmlsoap.schemas.ws._2004._08.addressing.EndpointReferenceType;
import org.xmlsoap.schemas.ws._2004._09.enumeration.Enumerate;
import org.xmlsoap.schemas.ws._2004._09.enumeration.EnumerateResponse;
import org.xmlsoap.schemas.ws._2004._09.enumeration.EnumerationContextType;
import org.xmlsoap.schemas.ws._2004._09.enumeration.ItemListType;
import org.xmlsoap.schemas.ws._2004._09.enumeration.Pull;
import org.xmlsoap.schemas.ws._2004._09.enumeration.PullResponse;

import com.hp.ov.opr.ws.client.axis.IncidentServiceStub;
import com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incident.Incident;
import com.hp.schemas.ism.serviceoperation.incidentmanagement._1.incidentextensions.IncidentExtensionsType;
import com.hp.schemas.ism.servicetransition.configurationmanagement._1.configurationitem.ConfigurationItemPropertiesType;
import com.hp.schemas.ism.servicetransition.configurationmanagement._1.node.NodePropertiesType;
import com.hp.schemas.ism.servicetransition.configurationmanagement._1.node.NodeReferenceType;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.OperationsExtension;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.CustomAttribute;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.CustomAttributes;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.DateOperators;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.IncidentEnumerationFilter;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.KeywordFilter;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.KeywordOperators;
import com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.TimeFilter;
import com.resolve.gateway.HPOMConstants;
import com.resolve.rsremote.ConfigReceiveHPOM;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;

/**
 * This class acts as a service to manage event.
 */
public class EventService implements ObjectService
{
    // An incident can be uniquely identified via its ID
    private static final String INCIDENT_ID_SELECTOR_NAME = "IncidentID";
    private static final String FILTER_DIALECT_URI = "http://schemas.hp.com/opr/ws/ServiceOperation/IncidentManagement/1/IncidentFilter";
    private static final String INCIDENT_RESOURCE_URI = "http://schemas.hp.com/ism/ServiceOperation/IncidentManagement/1/Incident";

    static String OBJECT_IDENTITY = "event";

    private static String IDENTITY_PROPERTY = "IncidentID";

    private final ConfigReceiveHPOM configurations;

    // private final EventUtil eventUtil;

    public static final Map<String, String> defaultEventAttributes = new HashMap<String, String>();

    // the instance of the Service Stub that does the actual SOAP requests
    private IncidentServiceStub m_proxy = null;

    // the target URI of the WebService. For the OM Incident WS,
    // this is <hostname>:<port>/opr-webservice/Incident.svc
    private String m_targetURI = null;

    static
    {
        defaultEventAttributes.put(HPOMConstants.FIELD_EVENT_ID.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_AFFECTED_CI.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_CATEGORY.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_COLLABORATION_MODE.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_DESCRIPTION.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_LIFECYCLE_STATE.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_PROBLEM_TYPE.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_PRODUCT_TYPE.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_SEVERITY.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_SOLUTION.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_SUB_CATEGORY.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_TITLE.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_TYPE.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_ASSIGNED_OPERATOR.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_CHANGE_TYPE.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_ACKNOWLEDGE_CORRELATION_KEY.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_APPLICATION.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_CORRELATION_KEY.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_OBJECT.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_SOURCE.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_ORIGINAL_EVENT.toLowerCase(), "string");
        defaultEventAttributes.put(HPOMConstants.FIELD_RECEIVED_TIME.toLowerCase(), "xmlgregoriancalendar");
        defaultEventAttributes.put(HPOMConstants.FIELD_STATE_CHANGE_TIME.toLowerCase(), "xmlgregoriancalendar");
    }

    public EventService(ConfigReceiveHPOM configurations)
    {
        this.configurations = configurations;
        try
        {
            m_targetURI = configurations.getUrl() + "/Incident.svc/";
            m_proxy = new IncidentServiceStub(m_targetURI, configurations.getUsername(), configurations.getPassword());
            m_proxy._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
        }
        catch (AxisFault e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    public String getIdPropertyName()
    {
        return IDENTITY_PROPERTY;
    }

    public String getIdentity()
    {
        return OBJECT_IDENTITY;
    }

    /**
     * This class hold the tokens collectd from a query. This is required
     * because the HPOM doesn't have a formal query (like SQL) language. We
     * collect these tokens and build the an object that HPOM understands.
     */
    class QueryToken
    {
        // Supported operators for HPOM. Must be same with HPOMQueryTranslator
        final static String EQUAL = "|=|";
        final static String GREATER = "|>|";
        final static String GREATER_OR_EQUAL = "|>=|";
        final static String LESS_OR_EQUAL = "|<=|";
        final static String LESS = "|<|";
        final static String CONTAINS = "|CONTAINS|";
        final static String AND = "|&&|";

        private final String key;
        private final String operator;
        private final String value;

        QueryToken(String key, String operator, String value) throws Exception
        {
            if (key == null || operator == null || value == null)
            {
                throw new Exception("Key, Operator, and Value must not be nul");
            }
            else
            {
                this.key = key.trim();
                this.operator = operator.trim();
                this.value = value.trim();
            }
        }

        public String getKey()
        {
            return key;
        }

        public String getOperator()
        {
            return operator;
        }

        public String getValue()
        {
            return value;
        }
    }

    // Splits the query item and build a QueryToken object.
    private QueryToken get(String queryItem) throws Exception
    {
        QueryToken result = null;
        if (queryItem.contains(QueryToken.EQUAL))
        {
            result = splitQueryItem(queryItem, QueryToken.EQUAL);
        }
        else if (queryItem.contains(QueryToken.GREATER))
        {
            result = splitQueryItem(queryItem, QueryToken.GREATER);
        }
        else if (queryItem.contains(QueryToken.GREATER_OR_EQUAL))
        {
            result = splitQueryItem(queryItem, QueryToken.GREATER_OR_EQUAL);
        }
        else if (queryItem.contains(QueryToken.LESS_OR_EQUAL))
        {
            result = splitQueryItem(queryItem, QueryToken.LESS_OR_EQUAL);
        }
        else if (queryItem.contains(QueryToken.LESS))
        {
            result = splitQueryItem(queryItem, QueryToken.LESS);
        }
        else if (queryItem.contains(QueryToken.CONTAINS))
        {
            result = splitQueryItem(queryItem, QueryToken.CONTAINS);
        }

        return result;
    }

    private QueryToken splitQueryItem(String queryItem, String delimeter) throws Exception
    {
        QueryToken result = null;
        String convertedDelem = java.util.regex.Pattern.quote(delimeter);

        String[] items = queryItem.split(convertedDelem);
        if (items.length == 2)
        {
            result = new QueryToken(items[0], delimeter, items[1]);
        }
        else
        {
            // This should never happen but just an extra protection.
            throw new Exception("Invalid query part: " + queryItem);
        }
        return result;
    }

    private IncidentEnumerationFilter prepareIncidentFilter(String query) throws Exception
    {
        String andDelimeter = java.util.regex.Pattern.quote(QueryToken.AND);

        String[] queryItems = query.split(andDelimeter);
        List<EventService.QueryToken> queryTokens = new ArrayList<EventService.QueryToken>();
        for (String queryItem : queryItems)
        {
            queryTokens.add(get(queryItem));
        }

        IncidentEnumerationFilter enumFilter = new IncidentEnumerationFilter();
        CustomAttributes customAttributes = new CustomAttributes();

        for (EventService.QueryToken queryToken : queryTokens)
        {
            String originalKey = queryToken.getKey(); // May need for custom
                                                      // field
            String key = queryToken.getKey().toLowerCase();
            String operator = queryToken.getOperator();
            String value = queryToken.getValue();
            if (defaultEventAttributes.containsKey(key))
            {
                if (key.equalsIgnoreCase(HPOMConstants.FIELD_TITLE))
                {
                    KeywordFilter keywordFilter = new KeywordFilter();
                    keywordFilter.setValue(value);
                    if (operator.equals(QueryToken.EQUAL))
                    {
                        keywordFilter.setComparator(KeywordOperators.EXACTLY);
                    }
                    else if (operator.equals(QueryToken.CONTAINS))
                    {
                        keywordFilter.setComparator(KeywordOperators.CONTAINS);
                    }
                    enumFilter.setTitle(keywordFilter);
                }
                if (key.equalsIgnoreCase(HPOMConstants.FIELD_APPLICATION))
                {
                    enumFilter.setApplication(value);
                }
                if (key.equalsIgnoreCase(HPOMConstants.FIELD_CATEGORY))
                {
                    enumFilter.setCategory(value);
                }
                if (key.equalsIgnoreCase(HPOMConstants.FIELD_CORRELATION_KEY))
                {
                    enumFilter.setCorrelationKey(value);
                }
                if (key.equalsIgnoreCase(HPOMConstants.FIELD_ESCALATION_STATUS))
                {
                    enumFilter.setEscalationStatus(value);
                }
                if (key.equalsIgnoreCase(HPOMConstants.FIELD_OBJECT))
                {
                    enumFilter.setObject(value);
                }
                if (key.equalsIgnoreCase(HPOMConstants.FIELD_RECEIVED_TIME))
                {
                    try
                    {
                        XMLGregorianCalendar cal = XMLGregorianCalendarImpl.parse(value);
                        TimeFilter timeFilter = new TimeFilter();
                        timeFilter.setValue(cal);
                        // Just the limitation on HPOM that both of this
                        // operators has to behave similar
                        if (operator.equals(QueryToken.LESS) || operator.equals(QueryToken.LESS_OR_EQUAL))
                        {
                            timeFilter.setComparator(DateOperators.TO);
                        }
                        else if (operator.equals(QueryToken.EQUAL))
                        {
                            timeFilter.setComparator(DateOperators.AT);
                        }
                        // Just the limitation on HPOM that both of this
                        // operators has to behave similar
                        else if (operator.equals(QueryToken.GREATER) || operator.equals(QueryToken.GREATER_OR_EQUAL))
                        {
                            timeFilter.setComparator(DateOperators.FROM);
                        }
                        enumFilter.setReceivedTime(timeFilter);
                    }
                    catch (Exception e)
                    {
                        Log.log.warn("Invalid value " + value + ", for the received time, ignoring it.", e);
                    }
                }
                if (key.equalsIgnoreCase(HPOMConstants.FIELD_SEVERITY))
                {
                    enumFilter.getSeverity().add(value);
                }
            }
            else
            // not in the fixed list, hence treat as custom.
            {
                CustomAttribute customAttribute = new CustomAttribute();
                customAttribute.setKey(originalKey);
                customAttribute.setText(value);
                // HPOM don't like null or empty string as field value, so we
                // cannot have a query
                // like severity='Warning' and ResolveStatus='null';
                /*
                 * if("null".equalsIgnoreCase(value)) {
                 * customAttribute.setText(""); } else {
                 * customAttribute.setText(value); }
                 */
                customAttributes.getCustomAttribute().add(customAttribute);
            }
        }

        if (customAttributes.getCustomAttribute().size() > 0)
        {
            enumFilter.setCustomAttributes(customAttributes);
        }

        return enumFilter;
    }

    private static String extractIncidentIdFromEPR(EndpointReferenceType incidentEPR)
    {
        String incidentID = null;
        List<Object> anyList = incidentEPR.getReferenceParameters().getAny();
        for (Object obj : anyList)
        {
            if (obj instanceof ElementNSImpl)
            {
                ElementNSImpl objElem = (ElementNSImpl) obj;
                if (objElem.getLocalName().equals("SelectorSet"))
                {
                    incidentID = objElem.getFirstChild().getTextContent();
                }
            }
            else if (obj instanceof org.apache.xerces.dom.ElementNSImpl)
            {
                org.apache.xerces.dom.ElementNSImpl objElem = (org.apache.xerces.dom.ElementNSImpl) obj;
                if (objElem.getLocalName().equals("SelectorSet"))
                {
                    incidentID = objElem.getFirstChild().getTextContent();
                }
            }
        }
        return incidentID;
    }

    /**
     * create an OperationTimeout of 10 minutes (used by each operation method)
     * 
     * @return an OperationTimeout
     */
    private AttributableDuration setOperationTimeout()
    {
        AttributableDuration operationTimeout = new AttributableDuration();
        Duration timeoutDuration;
        try
        {
            timeoutDuration = DatatypeFactory.newInstance().newDurationDayTime(true, 0, 0, 10, 0);
            operationTimeout.setValue(timeoutDuration);
        }
        catch (DatatypeConfigurationException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return operationTimeout;
    }

    /**
     * create a ResourceURI (used by each operation method)
     * 
     * @return a ResourceURI
     */
    private AttributableURI setResourceUri()
    {
        AttributableURI resourceUri = new AttributableURI();
        resourceUri.setValue(INCIDENT_RESOURCE_URI);
        return resourceUri;
    }

    /**
     * Method to call the get operation on a WebService server
     * 
     * @param incidentID
     *            ID of an incident, that should be retrieved
     * @return the Incident that was sent back from the server
     * @throws RemoteException
     */
    public Incident getEvent(String incidentId, IncidentServiceStub service) throws RemoteException
    {
        AttributableURI resourceUri = setResourceUri();
        AttributableDuration operationTimeout = setOperationTimeout();

        // SelectorSetType
        SelectorSetType selectorSet = new SelectorSetType();
        SelectorType incidentIdSelector = new SelectorType();
        incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
        incidentIdSelector.getContent().add(incidentId);
        selectorSet.getSelector().add(incidentIdSelector);

        setBasicAuthentication(service);

        Incident incident = service.Get(resourceUri, selectorSet, operationTimeout);
        return incident;
    }

    @Override
    public String create(String nodeIPAddress, Map<String, String> params, String username, String password) throws Exception
    {
        IncidentServiceStub service = m_proxy;
        if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
        {
            service = new IncidentServiceStub(m_targetURI, configurations.getUsername(), configurations.getPassword());
            service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
        }

        Incident incident = marshall(null, params);

        // The node reference is important otherwise HPOM won't create event.
        if (StringUtils.isEmpty(nodeIPAddress))
        {
            nodeIPAddress = InetAddress.getLocalHost().getHostAddress();
        }
        NodeReferenceType nodeRef = new NodeReferenceType();
        NodePropertiesType nodeProperties = new NodePropertiesType();
        nodeProperties.setDnsName(nodeIPAddress);
        nodeRef.setNodeProperties(nodeProperties);
        incident.setEmittingNode(nodeRef);

        AttributableURI resourceUri = setResourceUri();
        AttributableDuration operationTimeout = setOperationTimeout();

        setBasicAuthentication(service);

        EndpointReferenceType epr = service.Create(incident, resourceUri, operationTimeout);
        String incidentID = extractIncidentIdFromEPR(epr);
        return incidentID;
    }

    private void setBasicAuthentication(IncidentServiceStub service)
    {
        // Setting basic auth
        Options options = service._getServiceClient().getOptions();
        HttpTransportProperties.Authenticator auth = new HttpTransportProperties.Authenticator();
        // auth.setPreemptiveAuthentication(true);
        auth.setUsername(configurations.getUsername());
        auth.setPassword(configurations.getPassword());
        // auth.setHost("10.30.10.19");
        // auth.setPort(8081);
        options.setProperty(HTTPConstants.AUTHENTICATE, auth);
        service._getServiceClient().setOptions(options);
    }

    @Override
    public void update(String incidentId, Map<String, String> params, String username, String password) throws Exception
    {
        try
        {
            IncidentServiceStub service = m_proxy;
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, configurations.getUsername(), configurations.getPassword());
	            service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
            }

            Incident incident = getEvent(incidentId, service);
            if (incident == null)
            {
                throw new Exception("Invalid event id: " + incidentId);
            }
            else
            {
                incident = marshall(incident, params);
                AttributableURI resourceUri = setResourceUri();
                AttributableDuration operationTimeout = setOperationTimeout();

                // SelectorSetType
                SelectorSetType selectorSet = new SelectorSetType();
                SelectorType incidentIdSelector = new SelectorType();
                incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
                incidentIdSelector.getContent().add(incident.getIncidentID());
                selectorSet.getSelector().add(incidentIdSelector);

                setBasicAuthentication(service);

                service.Put(incident, resourceUri, selectorSet, operationTimeout);

                com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttributes attributes = marshallCustomAttribute(incident, params);
                if (attributes.getCustomAttribute() != null && attributes.getCustomAttribute().size() > 0)
                {
                    for (com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute attribute : attributes.getCustomAttribute())
                    {
                        service.SetCustomAttribute(attribute, resourceUri, selectorSet, operationTimeout);
                        Log.log.debug("Custom attribute is updated(Key:" + attribute.getKey() + ", Value:" + attribute.getText());
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error updating the event with id: " + incidentId + ", actual error: " + e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public void close(String incidentId, String username, String password) throws Exception
    {
        try
        {
            IncidentServiceStub service = m_proxy;
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, configurations.getUsername(), configurations.getPassword());
                service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
            }

            AttributableURI resourceUri = setResourceUri();
            AttributableDuration operationTimeout = setOperationTimeout();

            // SelectorSetType
            SelectorSetType selectorSet = new SelectorSetType();
            SelectorType incidentIdSelector = new SelectorType();
            incidentIdSelector.setName(INCIDENT_ID_SELECTOR_NAME);
            incidentIdSelector.getContent().add(incidentId);
            selectorSet.getSelector().add(incidentIdSelector);

            setBasicAuthentication(service);

            service.Close(resourceUri, selectorSet, operationTimeout);
        }
        catch (Exception e)
        {
            Log.log.error("Error closing event with IncidentID: " + incidentId + ", actual error: " + e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public Map<String, String> selectByEventId(String eventId, String username, String password) throws Exception
    {
        try
        {
            IncidentServiceStub service = m_proxy;
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, configurations.getUsername(), configurations.getPassword());
	            service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
            }

            Incident incident = getEvent(eventId, service);
            return unmarshall(incident);
        }
        catch (Exception e)
        {
            Log.log.error("Error getting event data with event id: " + eventId + ", actual error: " + e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public List<Map<String, String>> search(String query, int maxResult, String username, String password) throws Exception
    {
        try
        {
            IncidentServiceStub service = m_proxy;
            if (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password))
            {
                service = new IncidentServiceStub(m_targetURI, configurations.getUsername(), configurations.getPassword());
	            service._getServiceClient().getOptions().setTimeOutInMilliSeconds(configurations.getSocket_timeout() * 1000);
            }

            Log.log.debug("HPOM Query: " + query);
            List<Map<String, String>> result = new ArrayList<Map<String, String>>();

            IncidentEnumerationFilter enumFilter = prepareIncidentFilter(query);
            String enumerationContext = enumerateWithFilterOp(enumFilter);

            List<Incident> pulledIncidents = searchIncident(enumerationContext, BigInteger.valueOf(maxResult), service);
            for (Incident incident : pulledIncidents)
            {
                Map<String, String> incidentMap = unmarshall(incident);
                result.add(incidentMap);
            }

            return result;
        }
        catch (Exception e)
        {
            Log.log.error("Error searching event data with query: " + query + ", actual error: " + e.getMessage(), e);
            throw e;
        }
    }

    /**
     * Method to call the pull operation on a WebService server (used by
     * enumeration and eventing)
     * 
     * @param enumerationContext
     *            Context of the enumeration iterator
     * @param maxElements
     *            maximum number of elements that should be sent back
     * @return List of incidents
     * @throws RemoteException
     */
    private List<Incident> searchIncident(String enumerationContext, BigInteger maxElements, IncidentServiceStub service) throws RemoteException
    {
        List<Incident> incidents = new ArrayList<Incident>();

        // Pull
        Pull pull = new Pull();
        EnumerationContextType enumerationContextType = new EnumerationContextType();
        enumerationContextType.getContent().add(enumerationContext);

        Duration maxTimeDuration = null;
        try
        {
            maxTimeDuration = DatatypeFactory.newInstance().newDurationDayTime(true, 0, 0, 50, 0);
        }
        catch (DatatypeConfigurationException e)
        {
            e.printStackTrace();
        }

        pull.setEnumerationContext(enumerationContextType);
        pull.setMaxTime(maxTimeDuration);

        if (maxElements != null)
        {
            pull.setMaxElements(maxElements);
        }
        else
        {
            pull.setMaxElements(BigInteger.valueOf(100));
        }

        AttributableURI resourceUri = setResourceUri();
        AttributableDuration operationTimeout = setOperationTimeout();

        setBasicAuthentication(service);

        PullResponse pullResponse = service.PullOp(pull, resourceUri, operationTimeout);
        ItemListType itemListType = pullResponse.getItems();
        List<Object> itemList = itemListType.getAny();

        // after the pull, convert the returned items to Incidents
        for (Object itemObject : itemList)
        {
            try
            {
                javax.xml.bind.JAXBContext context = service.getContextMap().get(Incident.class);
                javax.xml.bind.Unmarshaller unmarshaller = context.createUnmarshaller();
                Schema sm = unmarshaller.getSchema();
                
                javax.xml.validation.Validator vl = sm != null ?  sm.newValidator() : null;  
                if (vl!=null)
                {
                    try
                    {
                        vl.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
                        vl.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
                        vl.setProperty(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
                        vl.setProperty("http://apache.org/xml/features/disallow-doctype-decl", true);
                        vl.setProperty("http://xml.org/sax/features/external-general-entities", false);
                        vl.setProperty("http://xml.org/sax/features/external-parameter-entities", false);
                        vl.setProperty("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
                    }
                    catch (Exception ex)
                    {
                        Log.log.info(ex,ex);
                    }
                }
                
                incidents.add(unmarshaller.unmarshal((ElementNSImpl) itemObject, Incident.class).getValue());
            }
            catch (javax.xml.bind.JAXBException bex)
            {
                throw org.apache.axis2.AxisFault.makeFault(bex);
            }
        }

        return incidents;
    }

    /**
     * Method to call the enumerate operation on a WebService server with a
     * filter
     * 
     * @param filter
     *            IncidentEnumerationFilter to filter for specific incidents
     * @return enumeration context ID, that can be used in subsequent pull
     *         operations
     * @throws RemoteException
     */
    private String enumerateWithFilterOp(IncidentEnumerationFilter filter) throws RemoteException
    {
        if (m_proxy == null || m_targetURI == null)
        {
            return null;
        }

        AttributableURI resourceUri = setResourceUri();
        AttributableDuration operationTimeout = setOperationTimeout();

        // Enumerate
        Enumerate enumerate = new Enumerate();
        org.xmlsoap.schemas.ws._2004._09.enumeration.FilterType enumerationFilter = new org.xmlsoap.schemas.ws._2004._09.enumeration.FilterType();
        enumerationFilter.setDialect(FILTER_DIALECT_URI);
        // enumerationFilter.getContent().add(filter);
        enumerationFilter.getContent().add(new com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incidentfilter.ObjectFactory().createIncidentEnumerationFilter(filter));
        enumerate.setFilter(enumerationFilter);

        // SelectorSetType
        SelectorSetType selectorSet = null;

        EnumerateResponse enumerateResponse = m_proxy.EnumerateOp(enumerate, resourceUri, selectorSet, operationTimeout);
        String enumerationContext = (String) enumerateResponse.getEnumerationContext().getContent().get(0);
        return enumerationContext;
    }

    private Map<String, String> unmarshall(Incident inc)
    {
        Map<String, String> result = new HashMap<String, String>();

        result.put(HPOMConstants.FIELD_EVENT_ID, inc.getIncidentID());
        if (inc.getAffectedCI() != null)
        {
            result.put("AffectedCI", inc.getAffectedCI().toString());
        }
        result.put(HPOMConstants.FIELD_CATEGORY, inc.getCategory());
        result.put(HPOMConstants.FIELD_COLLABORATION_MODE, inc.getCollaborationMode());
        result.put(HPOMConstants.FIELD_DESCRIPTION, inc.getDescription());

        if (inc.getEmittingCI() != null)
        {
            ConfigurationItemPropertiesType configProperties = inc.getEmittingCI().getConfigurationItemProperties();

            // Map<String, String> configPropertiesMap = new HashMap<String,
            // String>();
            // configPropertiesMap.put("ID", configProperties.getID());
            // configPropertiesMap.put("Name", configProperties.getName());
            // result.put("EmittingCI_ID",
            // StringUtils.convertMapToString(configPropertiesMap));
            result.put(HPOMConstants.FIELD_EMITTING_CI_ID, configProperties.getID());
            result.put(HPOMConstants.FIELD_EMITTING_CI_NAME, configProperties.getName());
        }

        if (inc.getEmittingNode() != null)
        {
            NodePropertiesType nodeProperties = inc.getEmittingNode().getNodeProperties();

            /*
             * Map<String, String> nodePropertiesMap = new HashMap<String,
             * String>(); nodePropertiesMap.put("ShortHostname",
             * nodeProperties.getShortHostname());
             * nodePropertiesMap.put("PrimaryDnsDomainName",
             * nodeProperties.getPrimaryDnsDomainName());
             * nodePropertiesMap.put("NetworkAddress",
             * nodeProperties.getNetworkAddress());
             * nodePropertiesMap.put("DnsName", nodeProperties.getDnsName());
             * nodePropertiesMap.put("CoreId", nodeProperties.getCoreId());
             * 
             * result.put("EmittingNode",
             * StringUtils.convertMapToString(nodePropertiesMap));
             */
            result.put(HPOMConstants.FIELD_EMITTING_NODE_SHORTHOSTNAME, nodeProperties.getShortHostname());
            result.put(HPOMConstants.FIELD_EMITTING_NODE_PRIMARYDNSDOMAINNAME, nodeProperties.getPrimaryDnsDomainName());
            result.put(HPOMConstants.FIELD_EMITTING_NODE_NETWORKADDRESS, nodeProperties.getNetworkAddress());
            result.put(HPOMConstants.FIELD_EMITTING_NODE_DNSNAME, nodeProperties.getDnsName());
            result.put(HPOMConstants.FIELD_EMITTING_NODE_COREID, nodeProperties.getCoreId());
        }

        result.put(HPOMConstants.FIELD_LIFECYCLE_STATE, inc.getLifecycleState());
        result.put(HPOMConstants.FIELD_PROBLEM_TYPE, inc.getProblemType());
        result.put(HPOMConstants.FIELD_PRODUCT_TYPE, inc.getProductType());
        result.put(HPOMConstants.FIELD_SEVERITY, inc.getSeverity());
        result.put(HPOMConstants.FIELD_SOLUTION, inc.getSolution());
        result.put(HPOMConstants.FIELD_SUB_CATEGORY, inc.getSubCategory());
        result.put(HPOMConstants.FIELD_TITLE, inc.getTitle());
        result.put(HPOMConstants.FIELD_TYPE, inc.getType());
        /*
         * if(inc.getAssignedOperator() != null) { AssignedOperator
         * assignedOperator = inc.getAssignedOperator(); Map<String, String>
         * assignedOperatorMap = new HashMap<String, String>();
         * assignedOperatorMap.put("",
         * assignedOperator.getEndpointReference().getServiceName().) }
         */
        /*
         * if(inc.getPrimaryAssignmentGroup() != null) { PrimaryAssignmentGroup
         * primaryAssignedGroup = inc.getPrimaryAssignmentGroup(); Map<String,
         * String> primaryAssignedGroupMap = new HashMap<String, String>(); }
         */

        if (inc.getExtensions() != null)
        {
            result.put(HPOMConstants.FIELD_CHANGE_TYPE, inc.getExtensions().getChangeType());
            if (inc.getExtensions().getOperationsExtension() != null)
            {
                result.put(HPOMConstants.FIELD_ACKNOWLEDGE_CORRELATION_KEY, inc.getExtensions().getOperationsExtension().getAcknowledgeCorrelationKey());
                result.put(HPOMConstants.FIELD_APPLICATION, inc.getExtensions().getOperationsExtension().getApplication());
                result.put(HPOMConstants.FIELD_CORRELATION_KEY, inc.getExtensions().getOperationsExtension().getCorrelationKey());
                result.put(HPOMConstants.FIELD_OBJECT, inc.getExtensions().getOperationsExtension().getObject());
                result.put(HPOMConstants.FIELD_ORIGINAL_EVENT, inc.getExtensions().getOperationsExtension().getOriginalEvent());
                result.put(HPOMConstants.FIELD_SOURCE, inc.getExtensions().getOperationsExtension().getSource());
                if (inc.getExtensions().getOperationsExtension().getCreationTime() != null)
                {
                    result.put(HPOMConstants.FIELD_CREATION_TYPE, inc.getExtensions().getOperationsExtension().getCreationTime().toXMLFormat());
                }
                if (inc.getExtensions().getOperationsExtension().getReceivedTime() != null)
                {
                    result.put(HPOMConstants.FIELD_RECEIVED_TIME, inc.getExtensions().getOperationsExtension().getReceivedTime().toXMLFormat());
                }
                if (inc.getExtensions().getOperationsExtension().getStateChangeTime() != null)
                {
                    result.put(HPOMConstants.FIELD_STATE_CHANGE_TIME, inc.getExtensions().getOperationsExtension().getStateChangeTime().toXMLFormat());
                }

                if (inc.getExtensions().getOperationsExtension().getCustomAttributes() != null)
                {
                    if (inc.getExtensions().getOperationsExtension().getCustomAttributes().getCustomAttribute() != null && inc.getExtensions().getOperationsExtension().getCustomAttributes().getCustomAttribute().size() > 0)
                    {
                        List<com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute> attributes = inc.getExtensions().getOperationsExtension().getCustomAttributes().getCustomAttribute();
                        for (com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute attribute : attributes)
                        {
                            result.put(attribute.getKey(), attribute.getText());
                        }
                    }
                }
            }
        }
        return result;
    }

    private Incident marshall(Incident incident, Map<String, String> incidentMap)
    {
        if (incident == null)
        {
            incident = new Incident();
        }

        // incident.setAffectedCI(value);
        // incident.setAssignedOperator(value);
        if (incidentMap.containsKey(HPOMConstants.FIELD_EVENT_ID))
        {
            incident.setIncidentID(incidentMap.get(HPOMConstants.FIELD_EVENT_ID));
        }
        if (incidentMap.containsKey(HPOMConstants.FIELD_CATEGORY))
        {
            incident.setCategory(incidentMap.get(HPOMConstants.FIELD_CATEGORY));
        }
        if (incidentMap.containsKey(HPOMConstants.FIELD_COLLABORATION_MODE))
        {
            incident.setCollaborationMode(incidentMap.get(HPOMConstants.FIELD_COLLABORATION_MODE));
        }
        if (incidentMap.containsKey(HPOMConstants.FIELD_DESCRIPTION))
        {
            incident.setDescription(incidentMap.get(HPOMConstants.FIELD_DESCRIPTION));
        }

        if (incidentMap.containsKey(HPOMConstants.FIELD_LIFECYCLE_STATE))
        {
            incident.setLifecycleState(incidentMap.get(HPOMConstants.FIELD_LIFECYCLE_STATE));
        }
        if (incidentMap.containsKey(HPOMConstants.FIELD_PROBLEM_TYPE))
        {
            incident.setProblemType(incidentMap.get(HPOMConstants.FIELD_PROBLEM_TYPE));
        }
        if (incidentMap.containsKey(HPOMConstants.FIELD_PRODUCT_TYPE))
        {
            incident.setProductType(incidentMap.get(HPOMConstants.FIELD_PRODUCT_TYPE));
        }
        if (incidentMap.containsKey(HPOMConstants.FIELD_SEVERITY))
        {
            incident.setSeverity(incidentMap.get(HPOMConstants.FIELD_SEVERITY));
        }
        if (incidentMap.containsKey(HPOMConstants.FIELD_SOLUTION))
        {
            incident.setSolution(incidentMap.get(HPOMConstants.FIELD_SOLUTION));
        }
        if (incidentMap.containsKey(HPOMConstants.FIELD_SUB_CATEGORY))
        {
            incident.setSubCategory(incidentMap.get(HPOMConstants.FIELD_SUB_CATEGORY));
        }
        if (incidentMap.containsKey(HPOMConstants.FIELD_TITLE))
        {
            incident.setTitle(incidentMap.get(HPOMConstants.FIELD_TITLE));
        }
        if (incidentMap.containsKey(HPOMConstants.FIELD_TYPE))
        {
            incident.setType(incidentMap.get(HPOMConstants.FIELD_TYPE));
        }

        IncidentExtensionsType extensions = new IncidentExtensionsType();
        OperationsExtension extension = new OperationsExtension();
        if (incidentMap.containsKey(HPOMConstants.FIELD_ACKNOWLEDGE_CORRELATION_KEY))
        {
            extension.setAcknowledgeCorrelationKey(incidentMap.get(HPOMConstants.FIELD_ACKNOWLEDGE_CORRELATION_KEY));
        }
        if (incidentMap.containsKey(HPOMConstants.FIELD_APPLICATION))
        {
            extension.setApplication(incidentMap.get(HPOMConstants.FIELD_APPLICATION));
        }
        if (incidentMap.containsKey(HPOMConstants.FIELD_CORRELATION_KEY))
        {
            extension.setCorrelationKey(incidentMap.get(HPOMConstants.FIELD_CORRELATION_KEY));
        }
        if (incidentMap.containsKey(HPOMConstants.FIELD_OBJECT))
        {
            extension.setObject(incidentMap.get(HPOMConstants.FIELD_OBJECT));
        }
        if (incidentMap.containsKey(HPOMConstants.FIELD_SOURCE))
        {
            extension.setSource(incidentMap.get(HPOMConstants.FIELD_SOURCE));
        }
        if (incidentMap.containsKey(HPOMConstants.FIELD_ORIGINAL_EVENT))
        {
            extension.setOriginalEvent(incidentMap.get(HPOMConstants.FIELD_ORIGINAL_EVENT));
        }
        if (incidentMap.containsKey(HPOMConstants.FIELD_RECEIVED_TIME))
        {
            try
            {
                extension.setReceivedTime(XMLGregorianCalendarImpl.parse(incidentMap.get(HPOMConstants.FIELD_RECEIVED_TIME)));
            }
            catch (Exception e)
            {
                Log.log.warn("Ignoring the invalid value for XMLGregorianCalendar : " + incidentMap.get(HPOMConstants.FIELD_RECEIVED_TIME));
            }

        }
        if (incidentMap.containsKey(HPOMConstants.FIELD_STATE_CHANGE_TIME))
        {
            try
            {
                extension.setStateChangeTime(XMLGregorianCalendarImpl.parse(incidentMap.get(HPOMConstants.FIELD_STATE_CHANGE_TIME)));
            }
            catch (Exception e)
            {
                Log.log.warn("Ignoring the invalid value for XMLGregorianCalendar : " + incidentMap.get(HPOMConstants.FIELD_STATE_CHANGE_TIME));
            }

        }

        if (incidentMap.containsKey(HPOMConstants.FIELD_APPLICATION))
        {
            extension.setAcknowledgeCorrelationKey(incidentMap.get(HPOMConstants.FIELD_APPLICATION));
        }

        com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttributes customAttributes = new com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttributes();

        for (String key : incidentMap.keySet())
        {
            if (!defaultEventAttributes.containsKey(key.toLowerCase()))
            {
                com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute myCA = new com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute();
                myCA.setKey(key);
                myCA.setText(incidentMap.get(key));
                customAttributes.getCustomAttribute().add(myCA);
            }
        }
        extension.setCustomAttributes(customAttributes);
        extensions.setOperationsExtension(extension);

        incident.setExtensions(extensions);

        return incident;
    }

    private com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttributes marshallCustomAttribute(Incident incident, Map<String, String> incidentMap)
    {
        com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttributes customAttributes = new com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttributes();

        for (String key : incidentMap.keySet())
        {
            if (!defaultEventAttributes.containsKey(key.toLowerCase()))
            {
                com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute myCA = new com.hp.schemas.opr.ws.serviceoperation.incidentmanagement._1.incident.CustomAttribute();
                myCA.setKey(key);
                myCA.setText(incidentMap.get(key));
                customAttributes.getCustomAttribute().add(myCA);
            }
        }

        return customAttributes;
    }

}
