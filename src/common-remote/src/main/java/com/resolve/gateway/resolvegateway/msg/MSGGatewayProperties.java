package com.resolve.gateway.resolvegateway.msg;

import com.resolve.gateway.resolvegateway.GatewayProperties;

public class MSGGatewayProperties extends GatewayProperties
{
    protected Integer msgPort;
    protected String msgUser;
    protected String msgPass;
    
    protected String msgSsl;
    protected String sslCertificate ;
    protected String sslPassword;
    protected String consumerProps;
    protected String producerProps;
    
    protected Integer alive = 120;

    public Integer getMSGPort()
    {
        return msgPort;
    }

    public void setMSGPort(Integer msgPort)
    {
        this.msgPort = msgPort;
    }

    public String getMSGUser()
    {
        return msgUser;
    }

    public void setMSGUser(String msgUser)
    {
        this.msgUser = msgUser;
    }

    public String getMSGPass()
    {
        return msgPass;
    }

    public void setMSGPass(String msgPass)
    {
        this.msgPass = msgPass;
    }

    public String getMSGSsl()
    {
        return msgSsl;
    }

    public void setMSGSsl(String msgSsl)
    {
        this.msgSsl = msgSsl;
    }

    public String getSslCertificate()
    {
        return sslCertificate;
    }

    public void setSslCertificate(String sslCertificate)
    {
        this.sslCertificate = sslCertificate;
    }

    public String getSslPassword()
    {
        return sslPassword;
    }

    public void setSslPassword(String sslPassword)
    {
        this.sslPassword = sslPassword;
    }

    public Integer getAlive()
    {
        return alive;
    }

    public void setAlive(Integer alive)
    {
        this.alive = alive;
    }

	public String getConsumerProps() {
		return consumerProps;
	}

	public void setConsumerProps(String consumerProps) {
		this.consumerProps = consumerProps;
	}

	public String getProducerProps() {
		return producerProps;
	}

	public void setProducerProps(String producerProps) {
		this.producerProps = producerProps;
	}
    
}