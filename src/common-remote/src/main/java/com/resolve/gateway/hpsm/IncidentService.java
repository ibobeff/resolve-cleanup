/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.hpsm;

import java.io.IOException;
import java.net.Authenticator;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import com.hp.schemas.sm._7.CloseIncidentRequest;
import com.hp.schemas.sm._7.CloseIncidentResponse;
import com.hp.schemas.sm._7.CreateIncidentRequest;
import com.hp.schemas.sm._7.CreateIncidentResponse;
import com.hp.schemas.sm._7.IncidentInstanceType;
import com.hp.schemas.sm._7.IncidentInstanceType.Description;
import com.hp.schemas.sm._7.IncidentInstanceType.Explanation;
import com.hp.schemas.sm._7.IncidentInstanceType.JournalUpdates;
import com.hp.schemas.sm._7.IncidentInstanceType.Solution;
import com.hp.schemas.sm._7.IncidentKeysType;
import com.hp.schemas.sm._7.IncidentManagement;
import com.hp.schemas.sm._7.IncidentManagement_Service;
import com.hp.schemas.sm._7.IncidentModelType;
import com.hp.schemas.sm._7.ObjectFactory;
import com.hp.schemas.sm._7.ResolveIncidentRequest;
import com.hp.schemas.sm._7.ResolveIncidentResponse;
import com.hp.schemas.sm._7.RetrieveIncidentListRequest;
import com.hp.schemas.sm._7.RetrieveIncidentListResponse;
import com.hp.schemas.sm._7.RetrieveIncidentRequest;
import com.hp.schemas.sm._7.RetrieveIncidentResponse;
import com.hp.schemas.sm._7.UpdateIncidentRequest;
import com.hp.schemas.sm._7.UpdateIncidentResponse;
import com.hp.schemas.sm._7.common.StatusType;
import com.hp.schemas.sm._7.common.StringType;
import com.resolve.gateway.HPSMIncident;
import com.resolve.rsremote.ConfigReceiveHPSM;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class acts as a service to manage event.
 */
public class IncidentService
{
    static String OBJECT_IDENTITY = "incident";

    private final ConfigReceiveHPSM configurations;

    private IncidentManagement incidentManagement;

    public IncidentService(ConfigReceiveHPSM configurations)
    {
        this.configurations = configurations;

        String wsdl = configurations.getUrl() + "/IncidentManagement.wsdl";
        IncidentManagement_Service service;
        try
        {
            service = new IncidentManagement_Service(new URL(wsdl), new QName("http://schemas.hp.com/SM/7", "IncidentManagement"));
            incidentManagement = service.getIncidentManagement();

            Authenticator auth = new HPSMAuthenticator(configurations.getUsername(), configurations.getPassword());
            Authenticator.setDefault(auth);
        }
        catch (MalformedURLException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    public String getIdentity()
    {
        return OBJECT_IDENTITY;
    }

    private IncidentInstanceType unmarshal(ObjectFactory factory, HPSMIncident incident) throws IOException
    {
        IncidentInstanceType incidentInstance = factory.createIncidentInstanceType();

        incidentInstance.setIncidentID(factory.createIncidentInstanceTypeIncidentID(HPSMUtil.getStringType(incident.getIncidentID())));
        incidentInstance.setCategory(factory.createIncidentInstanceTypeCategory(HPSMUtil.getStringType(incident.getCategory())));

        incidentInstance.setOpenTime(factory.createIncidentInstanceTypeOpenTime(HPSMUtil.getDateTimeTypeValue(incidentInstance.getOpenTime())));
        incidentInstance.setOpenedBy(factory.createIncidentInstanceTypeOpenedBy(HPSMUtil.getStringType(incident.getOpenedBy())));
        incidentInstance.setUrgency(factory.createIncidentInstanceTypeUrgency(HPSMUtil.getStringType(incident.getUrgency())));
        incidentInstance.setUpdatedTime(factory.createIncidentInstanceTypeUpdatedTime(HPSMUtil.getDateTimeTypeValue(incidentInstance.getUpdatedTime())));
        incidentInstance.setAssignmentGroup(factory.createIncidentInstanceTypeAssignmentGroup(HPSMUtil.getStringType(incident.getAssignmentGroup())));

        incidentInstance.setClosedTime(factory.createIncidentInstanceTypeClosedTime(HPSMUtil.getDateTimeTypeValue(incidentInstance.getClosedTime())));
        incidentInstance.setClosedBy(factory.createIncidentInstanceTypeClosedBy(HPSMUtil.getStringType(incident.getClosedBy())));
        incidentInstance.setClosureCode(factory.createIncidentInstanceTypeClosureCode(HPSMUtil.getStringType(incident.getClosureCode())));
        incidentInstance.setAffectedCI(factory.createIncidentInstanceTypeAffectedCI(HPSMUtil.getStringType(incident.getAffectedCI())));

        incidentInstance.setAssignee(factory.createIncidentInstanceTypeAssignee(HPSMUtil.getStringType(incident.getAssignee())));
        incidentInstance.setContact(factory.createIncidentInstanceTypeContact(HPSMUtil.getStringType(incident.getContact())));
        incidentInstance.setAlertStatus(factory.createIncidentInstanceTypeAlertStatus(HPSMUtil.getStringType(incident.getAlertStatus())));
        incidentInstance.setContactLastName(factory.createIncidentInstanceTypeContactLastName(HPSMUtil.getStringType(incident.getContactLastName())));
        incidentInstance.setContactFirstName(factory.createIncidentInstanceTypeContactFirstName(HPSMUtil.getStringType(incident.getContactFirstName())));
        incidentInstance.setCompany(factory.createIncidentInstanceTypeCompany(HPSMUtil.getStringType(incident.getCompany())));
        incidentInstance.setTitle(factory.createIncidentInstanceTypeTitle(HPSMUtil.getStringType(incident.getTitle())));
        incidentInstance.setTicketOwner(factory.createIncidentInstanceTypeTicketOwner(HPSMUtil.getStringType(incident.getTicketOwner())));
        incidentInstance.setUpdatedBy(factory.createIncidentInstanceTypeUpdatedBy(HPSMUtil.getStringType(incident.getUpdatedBy())));
        incidentInstance.setStatus(factory.createIncidentInstanceTypeStatus(HPSMUtil.getStringType(incident.getStatus())));
        incidentInstance.setArea(factory.createIncidentInstanceTypeArea(HPSMUtil.getStringType(incident.getArea())));
        incidentInstance.setSubarea(factory.createIncidentInstanceTypeSubarea(HPSMUtil.getStringType(incident.getSubarea())));

        incidentInstance.setSLAAgreementID(factory.createIncidentInstanceTypeSLAAgreementID(HPSMUtil.getDecimalType(incident.getSLAAgreementID())));
        incidentInstance.setSiteCategory(factory.createIncidentInstanceTypeSiteCategory(HPSMUtil.getStringType(incident.getSiteCategory())));
        incidentInstance.setImpact(factory.createIncidentInstanceTypeImpact(HPSMUtil.getStringType(incident.getImpact())));
        incidentInstance.setService(factory.createIncidentInstanceTypeService(HPSMUtil.getStringType(incident.getService())));
        incidentInstance.setProblemType(factory.createIncidentInstanceTypeProblemType(HPSMUtil.getStringType(incident.getProblemType())));
        incidentInstance.setResolutionFixType(factory.createIncidentInstanceTypeResolutionFixType(HPSMUtil.getStringType(incident.getResolutionFixType())));
        incidentInstance.setUserPriority(factory.createIncidentInstanceTypeUserPriority(HPSMUtil.getStringType(incident.getUserPriority())));
        incidentInstance.setLocation(factory.createIncidentInstanceTypeLocation(HPSMUtil.getStringType(incident.getLocation())));
        incidentInstance.setFolder(factory.createIncidentInstanceTypeFolder(HPSMUtil.getStringType(incident.getFolder())));

        Description descriptions = factory.createIncidentInstanceTypeDescription();
        if (incident.getDescription() != null)
        {
            for (String description : incident.getDescription())
            {
                descriptions.getDescription().add(factory.createIncidentInstanceTypeDescriptionDescription(HPSMUtil.getStringType(description)));
            }
            incidentInstance.setDescription(factory.createIncidentInstanceTypeDescription(descriptions));
        }

        Solution solutions = factory.createIncidentInstanceTypeSolution();
        if (incident.getSolution() != null)
        {
            for (String solution : incident.getSolution())
            {
                solutions.getSolution().add(factory.createIncidentInstanceTypeSolutionSolution(HPSMUtil.getStringType(solution)));
            }
            incidentInstance.setSolution(factory.createIncidentInstanceTypeSolution(solutions));
        }

        JournalUpdates journalUpdates = factory.createIncidentInstanceTypeJournalUpdates();
        if (incident.getJournalUpdates() != null)
        {
            for (String journalUpdate : incident.getJournalUpdates())
            {
                journalUpdates.getJournalUpdates().add(factory.createIncidentInstanceTypeJournalUpdatesJournalUpdates(HPSMUtil.getStringType(journalUpdate)));
            }
            incidentInstance.setJournalUpdates(factory.createIncidentInstanceTypeJournalUpdates(journalUpdates));
        }

        Explanation explanations = factory.createIncidentInstanceTypeExplanation();
        if (incident.getExplanation() != null)
        {
            for (String explanation : incident.getExplanation())
            {
                explanations.getExplanation().add(factory.createIncidentInstanceTypeExplanationExplanation(HPSMUtil.getStringType(explanation)));
            }
            incidentInstance.setExplanation(factory.createIncidentInstanceTypeExplanation(explanations));
        }
        //file attachments

        return incidentInstance;
    }

    private HPSMIncident marshal(IncidentInstanceType incidentInstance) throws IOException
    {
        HPSMIncident incident = new HPSMIncident();

        incident.setIncidentID(HPSMUtil.getStringValue(incidentInstance.getIncidentID()));
        incident.setCategory(HPSMUtil.getStringValue(incidentInstance.getCategory()));
        incident.setOpenTime(HPSMUtil.getDateTimeValue(incidentInstance.getOpenTime()));
        incident.setOpenedBy(HPSMUtil.getStringValue(incidentInstance.getOpenedBy()));
        incident.setUrgency(HPSMUtil.getStringValue(incidentInstance.getUrgency()));
        incident.setUpdatedTime(HPSMUtil.getDateTimeValue(incidentInstance.getUpdatedTime()));
        incident.setAssignmentGroup(HPSMUtil.getStringValue(incidentInstance.getAssignmentGroup()));
        incident.setClosedTime(HPSMUtil.getDateTimeValue(incidentInstance.getClosedTime()));
        incident.setClosedBy(HPSMUtil.getStringValue(incidentInstance.getClosedBy()));
        incident.setClosureCode(HPSMUtil.getStringValue(incidentInstance.getClosureCode()));
        incident.setAffectedCI(HPSMUtil.getStringValue(incidentInstance.getAffectedCI()));
        incident.setAssignee(HPSMUtil.getStringValue(incidentInstance.getAssignee()));
        incident.setContact(HPSMUtil.getStringValue(incidentInstance.getContact()));

        incident.setAlertStatus(HPSMUtil.getStringValue(incidentInstance.getAlertStatus()));
        incident.setContactLastName(HPSMUtil.getStringValue(incidentInstance.getContactLastName()));
        incident.setContactFirstName(HPSMUtil.getStringValue(incidentInstance.getContactFirstName()));
        incident.setCompany(HPSMUtil.getStringValue(incidentInstance.getCompany()));
        incident.setTitle(HPSMUtil.getStringValue(incidentInstance.getTitle()));
        incident.setTicketOwner(HPSMUtil.getStringValue(incidentInstance.getTicketOwner()));
        incident.setUpdatedBy(HPSMUtil.getStringValue(incidentInstance.getUpdatedBy()));
        incident.setStatus(HPSMUtil.getStringValue(incidentInstance.getStatus()));
        incident.setArea(HPSMUtil.getStringValue(incidentInstance.getArea()));
        if (incidentInstance.getSLAAgreementID() != null && incidentInstance.getSLAAgreementID().getValue() != null && incidentInstance.getSLAAgreementID().getValue().getValue() != null)
        {
            incident.setSLAAgreementID(incidentInstance.getSLAAgreementID().getValue().getValue());
        }
        incident.setSiteCategory(HPSMUtil.getStringValue(incidentInstance.getSiteCategory()));
        incident.setSubarea(HPSMUtil.getStringValue(incidentInstance.getSubarea()));
        incident.setProblemType(HPSMUtil.getStringValue(incidentInstance.getProblemType()));
        incident.setResolutionFixType(HPSMUtil.getStringValue(incidentInstance.getResolutionFixType()));
        incident.setUserPriority(HPSMUtil.getStringValue(incidentInstance.getUserPriority()));
        incident.setLocation(HPSMUtil.getStringValue(incidentInstance.getLocation()));

        List<String> descriptions = new ArrayList<String>();
        if (incidentInstance.getDescription() != null && incidentInstance.getDescription().getValue() != null && incidentInstance.getDescription().getValue().getDescription() != null)
        {
            for (JAXBElement<StringType> description : incidentInstance.getDescription().getValue().getDescription())
            {
                descriptions.add(HPSMUtil.getStringValue(description));
            }
        }
        incident.setDescription(descriptions);
        List<String> solutions = new ArrayList<String>();
        if (incidentInstance.getSolution() != null && incidentInstance.getSolution().getValue() != null && incidentInstance.getSolution().getValue().getSolution() != null)
        {
            for (JAXBElement<StringType> solution : incidentInstance.getSolution().getValue().getSolution())
            {
                solutions.add(HPSMUtil.getStringValue(solution));
            }
        }
        incident.setSolution(solutions);
        List<String> jurnalUpdates = new ArrayList<String>();
        if (incidentInstance.getJournalUpdates() != null && incidentInstance.getJournalUpdates().getValue() != null && incidentInstance.getJournalUpdates().getValue().getJournalUpdates() != null)
        {
            for (JAXBElement<StringType> jurnalUpdate : incidentInstance.getJournalUpdates().getValue().getJournalUpdates())
            {
                jurnalUpdates.add(HPSMUtil.getStringValue(jurnalUpdate));
            }
        }
        incident.setJournalUpdates(jurnalUpdates);

        List<String> explanations = new ArrayList<String>();
        if (incidentInstance.getExplanation() != null && incidentInstance.getExplanation().getValue() != null && incidentInstance.getExplanation().getValue().getExplanation() != null)
        {
            for (JAXBElement<StringType> explanation : incidentInstance.getExplanation().getValue().getExplanation())
            {
                explanations.add(HPSMUtil.getStringValue(explanation));
            }
        }
        incident.setExplanation(explanations);
        incident.setImpact(HPSMUtil.getStringValue(incidentInstance.getImpact()));
        incident.setFolder(HPSMUtil.getStringValue(incidentInstance.getFolder()));
        incident.setService(HPSMUtil.getStringValue(incidentInstance.getService()));

        //file attachments

        return incident;
    }

    // Public API


    public HPSMIncident create(Map<String, Object> params, String username, String password) throws Exception
    {
        HPSMIncident incident = new HPSMIncident(params);
        return create(incident, username, password);
    }

    public HPSMIncident create(HPSMIncident incident, String username, String password) throws Exception
    {
        HPSMIncident result = null;

        try
        {
            // This is where the caller provided username overriding the default configuration values.
            if (StringUtils.isBlank(username))
                username = configurations.getUsername();
            
            if (StringUtils.isBlank(password))
                password = configurations.getPassword();
            
            Authenticator auth = new HPSMAuthenticator(username, password);
            Authenticator.setDefault(auth);

            ObjectFactory factory = new ObjectFactory();
            CreateIncidentRequest request = factory.createCreateIncidentRequest();
            IncidentModelType type = factory.createIncidentModelType();
            IncidentKeysType keys = factory.createIncidentKeysType();
            type.setKeys(factory.createIncidentModelTypeKeys(keys));

            IncidentInstanceType instanceType;
            instanceType = unmarshal(factory, incident);

            type.setInstance(factory.createIncidentModelTypeInstance(instanceType));
            request.setModel(factory.createCreateIncidentRequestModel(type));

            CreateIncidentResponse response = incidentManagement.createIncident(request);
            if (StatusType.FAILURE.equals(response.getStatus()))
            {
                String errorMessages = HPSMUtil.extractError(response.getMessages());
                throw new Exception("Error creating incident: Return Code:" + response.getReturnCode() + ", Message: " + errorMessages);
            }
            else
            {
                result = marshal(response.getModel().getValue().getInstance().getValue());
                Log.log.debug("Successfully created incident with ID: " + result.getIncidentID());
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }

        return result;
    }

    public void update(Map<String, Object> params, String username, String password) throws Exception
    {
        HPSMIncident incident = new HPSMIncident(params);
        update(incident, username, password);
    }

    public HPSMIncident update(HPSMIncident incident, String username, String password) throws Exception
    {
        HPSMIncident result = null;

        try
        {
            if (incident == null || StringUtils.isBlank(incident.getIncidentID()))
            {
                throw new Exception("Incident object must have IncidentId.");
            }

            // This is where the caller provided username overriding the default configuration values.
            if (StringUtils.isBlank(username))
                username = configurations.getUsername();
            
            if (StringUtils.isBlank(password))
                password = configurations.getPassword();
            
            Authenticator auth = new HPSMAuthenticator(username, password);
            Authenticator.setDefault(auth);

            ObjectFactory factory = new ObjectFactory();
            UpdateIncidentRequest request = factory.createUpdateIncidentRequest();
            IncidentModelType type = factory.createIncidentModelType();

            //add th incoming key, this is important
            IncidentKeysType keys = factory.createIncidentKeysType();
            keys.setIncidentID(factory.createIncidentInstanceTypeIncidentID(HPSMUtil.getStringType(incident.getIncidentID())));
            type.setKeys(factory.createIncidentModelTypeKeys(keys));

            IncidentInstanceType instanceType = unmarshal(factory, incident);

            type.setInstance(factory.createIncidentModelTypeInstance(instanceType));
            request.setModel(factory.createUpdateIncidentRequestModel(type));

            UpdateIncidentResponse response = incidentManagement.updateIncident(request);
            if (StatusType.FAILURE.equals(response.getStatus()))
            {
                String errorMessages = HPSMUtil.extractError(response.getMessages());
                throw new Exception("Error updating incident: Return Code:" + response.getReturnCode() + ", Message: " + errorMessages);
            }
            else
            {
                result = marshal(response.getModel().getValue().getInstance().getValue());
                Log.log.debug("Successfully updated incident with ID: " + result.getIncidentID());
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }

        return result;
    }

    public void resolve(HPSMIncident incident, String username, String password) throws Exception
    {
        try
        {
            // This is where the caller provided username overriding the default configuration values.
            if (StringUtils.isBlank(username))
                username = configurations.getUsername();
            
            if (StringUtils.isBlank(password))
                password = configurations.getPassword();
            
            Authenticator auth = new HPSMAuthenticator(username, password);
            Authenticator.setDefault(auth);

            String incidentId = incident.getIncidentID();

            ObjectFactory factory = new ObjectFactory();
            ResolveIncidentRequest request = factory.createResolveIncidentRequest();
            IncidentModelType type = factory.createIncidentModelType();

            // Add the incoming key, this is important inside <keys></keys>
            IncidentKeysType keys = factory.createIncidentKeysType();
            keys.setIncidentID(factory.createIncidentInstanceTypeIncidentID(HPSMUtil.getStringType(incidentId)));
            type.setKeys(factory.createIncidentModelTypeKeys(keys));

            IncidentInstanceType instanceType = unmarshal(factory, incident);
            type.setInstance(factory.createIncidentModelTypeInstance(instanceType));
            request.setModel(factory.createCreateIncidentRequestModel(type));

            ResolveIncidentResponse response = incidentManagement.resolveIncident(request);
            if (StatusType.FAILURE.equals(response.getStatus()))
            {
                String errorMessages = HPSMUtil.extractError(response.getMessages());
                throw new Exception("Error resolving incident: Return Code:" + response.getReturnCode() + ", Message: " + errorMessages);
            }
            else
            {
                Log.log.debug("Successfully resolved incident: " + incidentId);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }

    public void close(String incidentId, String closureCode, String username, String password) throws Exception
    {
        try
        {
            // This is where the caller provided username overriding the default configuration values.
            if (StringUtils.isBlank(username))
                username = configurations.getUsername();
            
            if (StringUtils.isBlank(password))
                password = configurations.getPassword();
            
            Authenticator auth = new HPSMAuthenticator(username, password);
            Authenticator.setDefault(auth);

            ObjectFactory factory = new ObjectFactory();
            CloseIncidentRequest request = factory.createCloseIncidentRequest();
            IncidentModelType type = factory.createIncidentModelType();
            // type.setKeys(factory.createIncidentModelTypeKeys(factory.createIncidentKeysType()));
            IncidentKeysType incidentKeysType = factory.createIncidentKeysType();
            incidentKeysType.setIncidentID(factory.createIncidentKeysTypeIncidentID(HPSMUtil.getStringType(incidentId)));
            type.setKeys(factory.createIncidentModelTypeKeys(incidentKeysType));

            IncidentInstanceType instanceType = factory.createIncidentInstanceType();
            instanceType.setIncidentID(factory.createIncidentKeysTypeIncidentID(HPSMUtil.getStringType(incidentId)));
            instanceType.setClosureCode(factory.createIncidentInstanceTypeClosureCode(HPSMUtil.getStringType(closureCode)));
            type.setInstance(factory.createIncidentModelTypeInstance(instanceType));
            // request.setModel(factory.createUpdateIncidentRequestModel(type));
            request.setModel(factory.createCloseIncidentRequestModel(type));
            

            CloseIncidentResponse response = incidentManagement.closeIncident(request);
            if (StatusType.FAILURE.equals(response.getStatus()))
            {
                String errorMessages = HPSMUtil.extractError(response.getMessages());
                throw new Exception("Error closing incident: Return Code:" + response.getReturnCode() + ", Message: " + errorMessages);
            }
            else
            {
                Log.log.debug("Successfully closed incident: " + incidentId);
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }
    
    public void close(HPSMIncident incident, String username, String password) throws Exception
    {
        try
        {
            // This is where the caller provided username overriding the default configuration values.
            if (StringUtils.isBlank(username))
                username = configurations.getUsername();
            
            if (StringUtils.isBlank(password))
                password = configurations.getPassword();
            
            Authenticator auth = new HPSMAuthenticator(username, password);
            Authenticator.setDefault(auth);

            ObjectFactory factory = new ObjectFactory();
            CloseIncidentRequest request = factory.createCloseIncidentRequest();
            IncidentModelType type = factory.createIncidentModelType();
            // type.setKeys(factory.createIncidentModelTypeKeys(factory.createIncidentKeysType()));
            IncidentKeysType incidentKeysType = factory.createIncidentKeysType();
            incidentKeysType.setIncidentID(factory.createIncidentKeysTypeIncidentID(HPSMUtil.getStringType(incident.getIncidentID())));
            type.setKeys(factory.createIncidentModelTypeKeys(incidentKeysType));

            IncidentInstanceType instanceType = factory.createIncidentInstanceType();
            instanceType.setIncidentID(factory.createIncidentKeysTypeIncidentID(HPSMUtil.getStringType(incident.getIncidentID())));
            instanceType.setClosureCode(factory.createIncidentInstanceTypeClosureCode(HPSMUtil.getStringType(incident.getClosureCode())));
            type.setInstance(factory.createIncidentModelTypeInstance(instanceType));
            // request.setModel(factory.createUpdateIncidentRequestModel(type));
            request.setModel(factory.createCloseIncidentRequestModel(type));
            

            CloseIncidentResponse response = incidentManagement.closeIncident(request);
            if (StatusType.FAILURE.equals(response.getStatus()))
            {
                String errorMessages = HPSMUtil.extractError(response.getMessages());
                throw new Exception("Error closing incident: Return Code:" + response.getReturnCode() + ", Message: " + errorMessages);
            }
            else
            {
                Log.log.debug("Successfully closed incident: " + incident.getIncidentID());
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }

    public HPSMIncident selectIncidentById(String incidentId, String username, String password) throws Exception
    {
        HPSMIncident result = null;

        try
        {
            // This is where the caller provided username overriding the default configuration values.
            if (StringUtils.isBlank(username))
                username = configurations.getUsername();
            
            if (StringUtils.isBlank(password))
                password = configurations.getPassword();
            
            Authenticator auth = new HPSMAuthenticator(username, password);
            Authenticator.setDefault(auth);

            ObjectFactory factory = new ObjectFactory();
            RetrieveIncidentRequest request = factory.createRetrieveIncidentRequest();
            IncidentModelType type = factory.createIncidentModelType();
            type.setKeys(factory.createIncidentModelTypeKeys(factory.createIncidentKeysType()));

            IncidentInstanceType instanceType = factory.createIncidentInstanceType();
            instanceType.setIncidentID(factory.createIncidentKeysTypeIncidentID(HPSMUtil.getStringType(incidentId)));
            type.setInstance(factory.createIncidentModelTypeInstance(instanceType));
            request.setModel(factory.createRetrieveIncidentRequestModel(type));

            RetrieveIncidentResponse response = incidentManagement.retrieveIncident(request);
            if (StatusType.FAILURE.equals(response.getStatus()))
            {
                String errorMessages = HPSMUtil.extractError(response.getMessages());
                throw new Exception("Error retrieving incident: Return Code:" + response.getReturnCode() + ", Message: " + errorMessages);
            }
            else
            {
                result = marshal(response.getModel().getValue().getInstance().getValue());
                Log.log.debug("Successfully retrieved incident with ID: " + result.getIncidentID());
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }

        return result;
    }

    public List<HPSMIncident> search(String query, int maxResult, String username, String password) throws Exception
    {
        List<HPSMIncident> result = new ArrayList<HPSMIncident>();

        try
        {
            // This is where the caller provided username overriding the default configuration values.
            if (StringUtils.isBlank(username))
                username = configurations.getUsername();
            
            if (StringUtils.isBlank(password))
                password = configurations.getPassword();
            
            Authenticator auth = new HPSMAuthenticator(username, password);
            Authenticator.setDefault(auth);

            ObjectFactory factory = new ObjectFactory();
            RetrieveIncidentListRequest request = factory.createRetrieveIncidentListRequest();
            IncidentKeysType keys = factory.createIncidentKeysType();
            keys.setQuery(query);
            request.setCount(Long.valueOf(maxResult));
            request.getKeys().add(factory.createIncidentModelTypeKeys(keys));

            RetrieveIncidentListResponse response = incidentManagement.retrieveIncidentList(request);
            if (StatusType.FAILURE.equals(response.getStatus()))
            {
                //return code 9 is fine, it seems like either the wuery is malformed or there
                //is genuinely no data to return.
                if (response.getReturnCode() != null && 9L != response.getReturnCode().longValue())
                {
                    String errorMessages = HPSMUtil.extractError(response.getMessages());
                    throw new Exception("Error retrieving incidents: Return Code:" + response.getReturnCode() + ", Message: " + errorMessages);
                }
            }
            else
            {
                for (JAXBElement<IncidentInstanceType> instance : response.getInstance())
                {
                    result.add(marshal(instance.getValue()));
                }
                Log.log.debug("Successfully retrieved incidents, total count: " + result.size());
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }

        return result;
    }
}
