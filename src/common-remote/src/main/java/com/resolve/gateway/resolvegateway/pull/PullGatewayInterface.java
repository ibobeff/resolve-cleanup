package com.resolve.gateway.resolvegateway.pull;

import java.util.List;
import java.util.Map;

public interface PullGatewayInterface
{
    public List<Map<String, String>> retrieveData(PullGatewayFilter filter) throws Exception;
    
    public void updateLastValues(PullGatewayFilter filter, List<Map<String, String>> results) throws Exception;
    
    public void updateAck(List<Map<String, String>> results) throws Exception;
    
    public void setRegexPattern(String pattern);
}
