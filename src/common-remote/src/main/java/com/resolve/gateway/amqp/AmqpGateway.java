/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.gateway.amqp;

import groovy.lang.Binding;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.esb.ESBException;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MAmqp;
import com.resolve.rsremote.ConfigReceiveAmqp;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * The AMQP Gateway gives Resolve node access to all network services associated
 * with an Advanced Message Queuing Protocol (AMQP) application layer.
 * 
 * This class extends from {@link BaseClusteredGateway}.
 * 
 * @author Bipul Dutta
 * @see {@link BaseClusteredGateway}
 */
public class AmqpGateway extends BaseClusteredGateway
{

    // Singleton
    private static volatile AmqpGateway instance = null;

    private AmqpServer amqpServer = null;
    private AmqpSender amqpSender = null;
    // This will be set in initialize() with the proper queue name from the
    // configuration.
    private String queue = "AMQP";

    /**
     * Returns a singleton instance of the AmqpGateway. If the AmqpGateway
     * instance is null, a new instance is created and initialized with the
     * ConfigReceiveAmqp config parameter.
     * 
     * @param config
     *            an instance of a ConfigReceiveAmqp class with all of the
     *            necessary config parameters to initialize an AmqpGateway
     * 
     * @return AmqpGateway object
     */
    public static synchronized AmqpGateway getInstance(ConfigReceiveAmqp config)
    {
        if (instance == null)
        {
            instance = new AmqpGateway(config);
        }
        return (AmqpGateway) instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return AmqpGateway object
     */
    public static AmqpGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("AMQP Gateway is not initialized correctly..");
        }
        else
        {
            return (AmqpGateway) instance;
        }
    }

    /**
     * Private constructor.
     *
     * @param config
     */
    private AmqpGateway(ConfigReceiveAmqp config)
    {
        // call super constructor for common initialization.
        super(config);
        queue = config.getQueue();
    } // AmqpGateway

    /**
     * Returns the Amqp license code.
     * 
     * @return Amqp code String
     */
    @Override
    public String getLicenseCode()
    {
        return "AMQP";
    }

    /**
     * Returns the Amqp event type.
     * 
     * @return Amqp event type String
     */
    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_AMQP;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MAmqp.class.getSimpleName();
    }

    @Override
    protected Class<MAmqp> getMessageHandlerClass()
    {
        return MAmqp.class;
    }

    /**
     * Returns the queue name of the current AmqpGateway instance
     * 
     * @return Queue name
     */
    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    /**
     * Initializes the specific gateway resources.
     *
     * @param configurations
     */
    @Override
    protected void initialize()
    {
        ConfigReceiveAmqp amqpConfigurations = (ConfigReceiveAmqp) configurations;

        this.queue = amqpConfigurations.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/amqp/";

        if (amqpConfigurations.isActive() && (amqpConfigurations.isPrimary() || amqpConfigurations.isWorker()))
        {
            try
            {
                amqpServer = AmqpServer.getInstance(amqpConfigurations);
                amqpServer.start();
                amqpSender = amqpServer.createSender();
            }
            catch (ESBException e)
            {
                Log.log.error(e.getMessage(), e);
                setActive(false);
            }
        }
    }

    /**
     * Re-initializes the specific gateway resources. This is called if the
     * gateway changes its state from secondary to primary.
     *
     * 
     */
    @Override
    public void reinitialize()
    {
        super.reinitialize();
        ConfigReceiveAmqp amqpConfigurations = (ConfigReceiveAmqp) configurations;

        if (amqpConfigurations.isActive() && isPrimary())
        {
            try
            {
                amqpServer = AmqpServer.getInstance(amqpConfigurations);
                amqpServer.start();
            }
            catch (ESBException e)
            {
                Log.log.error(e.getMessage(), e);
                setActive(false);
            }
        }
    }

    /**
     * Deactivates the Amqp gateway instance
     * 
     */
    public void deactivate()
    {
        Log.log.warn("Deactivating AMQP Gateway.");
        super.deactivate();
    }

    /**
     * Returns an AmqpFilter instance instantiated with the params argument
     * 
     * @param params
     *            a Map<String, Object>
     * 
     */
    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new AmqpFilter((String) params.get(AmqpFilter.ID), (String) params.get(AmqpFilter.ACTIVE), (String) params.get(AmqpFilter.ORDER), (String) params.get(AmqpFilter.INTERVAL), (String) params.get(AmqpFilter.EVENT_EVENTID), (String) params.get(AmqpFilter.RUNBOOK), (String) params.get(AmqpFilter.SCRIPT), (String) params.get(AmqpFilter.TARGET_QUEUE), (String) params.get(AmqpFilter.IS_EXCHANGE));
    } // getFilter

    @Override
    public void start()
    {
        Log.log.warn("Starting AMQP Gateway Filter Listener");
        super.start();
    } // start

    @Override
    public void stop()
    {
        Log.log.warn("Stopping AMQP Gateway.");
        super.stop();

        deactivate();
    } // start

    @Override
    public void run()
    {
        // This call is must for all the ClusteredGateways.
        super.sendSyncRequest();

        // initialize queue/exchange listener
        initListener();
    } // run

    private void initListener()
    {
        ConfigReceiveAmqp amqpConfigurations = (ConfigReceiveAmqp) configurations;
        // only start listeneing if it is primary or worker
        if (amqpConfigurations.isActive() && (isPrimary() || isWorker()))
        {
            // initialize all the Queue listeners for all the filters
            Map<String, Filter> filters = getFilters();
            for (Filter filter : filters.values())
            {
                AmqpFilter amqpFilter = (AmqpFilter) filter;
                try
                {
                    if (!amqpServer.getListeners().containsKey(amqpFilter.getTargetQueue()))
                    {
                        if (amqpFilter.isExchange())
                        {
                            if (isPrimary())
                            {
                                // if this is an exchange on the primary can
                                // listen to it, otherwise there
                                // will be duplicate message processing by
                                // workers.
                                AmqpListener listener = amqpServer.createSubscriber(amqpFilter.getTargetQueue());
                                amqpServer.subscribePublication(amqpFilter.getTargetQueue(), listener);
                            }
                            else
                            {
                                Log.log.debug("Since this is not primary ignoring connection to exchange : " + amqpFilter.getTargetQueue());
                            }
                        }
                        else
                        {
                            // we need only one listener per Queue because there
                            // may be multiple filter
                            // interested in messages from the same queue. no
                            // need to create redundent
                            // listener because gateway has to handle all
                            // filters attached to the queue
                            // by itself anyways.
                            AmqpListener listener = amqpServer.createListener(amqpFilter.getTargetQueue());
                            amqpServer.createQueueConsumer(amqpFilter.getTargetQueue(), listener);
                            Log.log.debug("Listener created for " + amqpFilter.getTargetQueue() + ", listener id: " + listener.getListenerId());
                        }
                    }
                }
                catch (ESBException e)
                {
                    Log.log.error("Listener could not be created for " + amqpFilter.getTargetQueue());
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
    }

    /**
     * This method processes the message received from the Amqp server. It
     * checks the message with any interested filter and process the filter.
     *
     * @param message
     */
    public void processData(String targetQueue, String message)
    {
        try
        {
            if (StringUtils.isNotBlank(targetQueue) && message != null)
            {
                for (Filter filter : orderedFilters)
                {
                    AmqpFilter amqpFilter = (AmqpFilter) filter;
                    if (filter.isActive() && targetQueue.equalsIgnoreCase(amqpFilter.getTargetQueue()))
                    {
                        Log.log.trace("Running filter: " + filter.getId());
                        Map<String, String> runbookParams = new HashMap<String, String>();
                        runbookParams.put("AMQP_MESSAGE", message);
                        // runbookParams.put("MESSAGE", message);
                        runbookParams.put(Constants.EVENT_EVENTID, amqpFilter.getEventEventId());

                        // The message may have contents that might
                        // indicate that the sender intends to execute an event
                        // as well.
                        addEvent(message, runbookParams);

                        // if the target was a RabbitMQ exchange then only
                        // primary listens to it.
                        // in that case we want to distribute/process the data
                        // through workers.
                        if (isPrimary() && amqpFilter.isExchange())
                        {
                            addToPrimaryDataQueue(amqpFilter, runbookParams);
                        }
                        else
                        {
                            // since this is worker simply process the data.
                            receiveData(runbookParams);
                        }
                    }
                }
            }
            // Thread.sleep(interval);
        }
        catch (Exception e)
        {
            Log.log.warn("Error: " + e.getMessage(), e);
            try
            {
                Thread.sleep(interval);
            }
            catch (InterruptedException ie)
            {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
    }

    @Override
    protected void addAdditionalBindings(String script, Binding binding)
    {
        // let's use the amqpSender from this class. if we see any
        // bottleneck then we may instantiate it for each message.
        // AmqpSender amqpSender = amqpServer.createSender();
        // this is just a simple hack, there could be variety of
        // situation it can fail. but for now as long as AMQP word
        // is the start of the script or at preceded by a space.
        if (script.startsWith(Constants.GROOVY_BINDING_AMQP) || script.contains(" " + Constants.GROOVY_BINDING_AMQP))
        {
            if (amqpSender != null)
            {
                binding.setVariable(Constants.GROOVY_BINDING_AMQP, amqpSender);
            }
            else
            {
                Log.log.warn("The AMQP Sender object is null, something is terribly wrong!");
            }
        }
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
        // TODO Auto-generated method stub
    }

    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList)
    {
        super.clearAndSetFilters(filterList);
        // now see if we need new listeners to some new queue or not
        initListener();

        // close listener that are no longer needed
        for (String targetQueueName : amqpServer.getListeners().keySet())
        {
            boolean isFound = false;
            for (Filter filter : getFilters().values())
            {
                if (targetQueueName.equalsIgnoreCase(((AmqpFilter) filter).getTargetQueue()))
                {
                    isFound = true;
                    break;
                }
            }
            if (!isFound)
            {
                AmqpListener listener = amqpServer.getListeners().get(targetQueueName);
                listener.close();
                Log.log.debug("Listener closed for unused queue " + targetQueueName);
                amqpServer.getListeners().remove(targetQueueName);
            }
        }
    }

    public synchronized boolean send(final String queue, final Serializable content) throws ESBException
    {
        return amqpSender.send(queue, content);
    } // send

    public synchronized boolean publish(final String exchange, final Serializable content) throws ESBException
    {
        return amqpSender.publish(exchange, content);
    }
} // AMQPGateway
