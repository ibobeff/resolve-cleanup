/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.ssh;

public class SubnetMask
{
    public static final String SUBNETMASK = "SUBNETMASK";
    public static final String MAXCONNECTION = "MAXCONNECTION";
    public static final String TIMEOUT = "TIMEOUT";
    public static final String QUEUE = "QUEUE";

    private String subnetMask;
    private int maxConnection = 10;
    private int timeout = 120; // in seconds

    public SubnetMask(String subnetMask, String maxConnection, String timeout)
    {
        super();
        this.subnetMask = subnetMask;
        this.maxConnection = Integer.parseInt(maxConnection);
        this.timeout = Integer.parseInt(timeout);
    }

    public void setPool(String name, String subnetMask, String maxConnection, String timeout)
    {
        this.subnetMask = subnetMask;
        this.maxConnection = Integer.parseInt(maxConnection);
        this.timeout = Integer.parseInt(timeout);
    }

    public String getSubnetMask()
    {
        return subnetMask;
    }

    public void setSubnetMask(String subnetMask)
    {
        this.subnetMask = subnetMask;
    }

    public int getMaxConnection()
    {
        return maxConnection;
    }

    public void setMaxConnection(int maxConnection)
    {
        this.maxConnection = maxConnection;
    }

    public int getTimeout()
    {
        return timeout;
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }
}
