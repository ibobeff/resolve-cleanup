package com.resolve.gateway.resolvegateway.push;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.gateway.Filter;
import com.resolve.gateway.MPushGateway;
import com.resolve.gateway.resolvegateway.GatewayProperties;
import com.resolve.gateway.resolvegateway.ResolveGateway;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class PushGateway extends ResolveGateway {

    private static volatile PushGateway instance = null;

    protected String inputKey = "alert_id";
    protected static String serverUrl = null;

    private static volatile Map<String, PushGatewayFilter> pushFilters = new ConcurrentHashMap<String, PushGatewayFilter>();

    public static PushGateway getInstance(ConfigReceivePushGateway config) {
        
        if (instance == null) {
            instance = new PushGateway(config);
        }
        
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static PushGateway getInstance() {
        
        if (instance == null) {
            throw new RuntimeException("Push Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }
    
    public static PushGateway getInstance(String gatewayName) {
        
        Map<String, PushGateway> gateways = ConfigReceivePushGateway.getGateways();
        
        for(String gateway:gateways.keySet()) {
            if(gateway.toLowerCase().equals(gatewayName))
                return gateways.get(gateway);
        }
        
        return null;
    }

    protected PushGateway(ConfigReceivePushGateway config) {
        
        super(config);
    }
    
    public GatewayProperties getGatewayProperties(String filterName)
    {
        try {
            Filter filter = getPushFilters().get(filterName);
            String gatewayName = ((PushGatewayFilter)filter).getGatewayName();
            if(StringUtils.isNotBlank(gatewayName))
                return ConfigReceivePushGateway.getGatewayProperties(gatewayName);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        
        return null;
    }
    
    protected HttpServer getDefaultHttpServer()
    {
        return null;
    }

    protected Map<Integer, HttpServer> getHttpServers()
    {
        return null;
    }

    protected Map<String, HttpServer> getDeployedServlets()
    {
        return null;
    }
    
    public static Map<String, PushGatewayFilter> getPushFilters()
    {
        return pushFilters;
    }
    
    public static void addPushFilter(String filterName, PushGatewayFilter filter) {
        
        pushFilters.put(filterName, filter);
    }
    
    public static void removePushFilter(String filterName) {
        
        pushFilters.remove(filterName);
    }

    @Override
    public String getLicenseCode() {
        return queue;
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
    }

    @Override
    protected String getGatewayEventType() {
        return queue;
    }

    @Override
    protected String getMessageHandlerName() {
        return MPushGateway.class.getSimpleName();
    }

    @Override
    protected Class<MPushGateway> getMessageHandlerClass() {
        return MPushGateway.class;
    }
    
    public String getInputKey()
    {
        return inputKey;
    }

    public void setInputKey(String inputKey)
    {
        this.inputKey = inputKey;
    }

    @Override
    protected void initialize() {

    }

    protected void initServers() {

    }

    @Override
    public void start() {
        
        super.start();
    }

    @Override
    public void stop() {
/*        
        // Save the filters to local config.xml when RSRemote is gracefully shutdown
        try {
            ((ConfigReceivePushGateway)config).saveFilters(name, filters);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
*/
        super.stop();
    }
    
    @Override
    public void run() {
    	super.sendSyncRequest();
        if(isPrimary() && isActive())
            initServers();
    }

    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList) {
        
        // Place holder, don't need to do anything
    }
    
    public List<Filter> deployFilters(List<Map<String, Object>> filterList, String username) throws Exception {
        
        Log.log.debug("Calling Gateway specific deployFilters" + (filterList != null ? " " + filterList.size() + ", [" + filterList + "]" : ""));
        
        List<Filter> deployedFilters = new ArrayList<Filter>();
        
        PushGateway gateway = null;
        String gatewayName = "";
        
        for(Map<String, Object> params : filterList) {
            if(!params.containsKey("RESOLVE_USERNAME")) {
                gatewayName = params.get("GATEWAY_NAME") != null ? params.get("GATEWAY_NAME").toString().trim() : null;
                if(gatewayName ==null)
                    continue;
                
                gateway = ConfigReceivePushGateway.getGateways().get(params.get("GATEWAY_NAME"));
                
                if(gateway == null) {
                    Log.log.warn("Gateway " + gatewayName + " does not exist");
                    continue;
                }
                
                int filterValidationErrCode = gateway.validateFilterFields(params);

                if (filterValidationErrCode != 0)
                    throw new Exception("Filter Validation Returned Error Code " + filterValidationErrCode + " : " + gateway.getValidationError(filterValidationErrCode));
                
                String name = (String) params.get("ID");
                String queue = (String) params.get("QUEUE");

                PushGatewayFilter originalFilter = pushFilters.get(name);
                
                if(originalFilter != null) {
                    List<Filter> filterToBeRemoved = new ArrayList<Filter>();
                    filterToBeRemoved.add(originalFilter);
                    gateway.removeFilters(filterToBeRemoved);
                }

                PushGatewayFilter filter = getFilter(params);

                filter.setDeployed(true);
                filter.setUpdated(false);
                filter.setQueue(queue);
                filter.setUsername(username);

                Map<String, Filter> gatewayFilters = gateway.getFilters();
                gatewayFilters.put(name, filter);
                
                filters.put(name, filter);
                pushFilters.put(name, filter);
                deployedFilters.add(filter);
            }
        }
            
        if (isPrimary() && isActive()) {
            try {
                gateway.initServers();
                
                List<Filter> orderedFilters = gateway.getOrderedFilters();
                Map<String, Filter> orderedFiltersMapById = gateway.getOrderedFiltersMapById();
                
                synchronized(orderedFilters)
                {
                    for(Iterator<Filter> it=deployedFilters.iterator(); it.hasNext();)
                        orderedFilters.add(it.next());

                    Collections.sort(orderedFilters);
                    
                    orderedFiltersMapById.clear();
                    
                    for (Filter filter : orderedFilters)
                        orderedFiltersMapById.put(filter.getId(), filter);
                }
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        return deployedFilters;
    } // deployFilters()

    public List<Filter> undeployFilters(List<Map<String, Object>> filterList, String username) throws Exception {
        
        Log.log.debug("Calling Gateway specific undeployFilters" + (filterList != null ? " " + filterList.size() + ", [" + filterList + "]" : ""));
 
        List<Filter> undeployedFilters = new ArrayList<Filter>();
        
        String gatewayName = config.getGatewayName();
        PushGateway gateway = ConfigReceivePushGateway.getGateways().get(gatewayName);
        if(gateway == null)
            throw new Exception("Gateway " + gatewayName + " does not exist");
        
        for (Map<String, Object> params : filterList) {
            if (!params.containsKey("RESOLVE_USERNAME"))
            {
                String name = (String)params.get("ID");

                filters.remove(name);
                pushFilters.remove(name);
                
                PushGatewayFilter filter = getFilter(params);

                filter.setDeployed(false);
                filter.setUpdated(false);
                filter.setUsername(username);
                
                undeployedFilters.add(filter);
            }
        }
            
        if (isPrimary() && isActive()) {
            try {
                List<Filter> orderedFilters = gateway.getOrderedFilters();
                Map<String, Filter> orderedFiltersMapById = gateway.getOrderedFiltersMapById();
                
                gateway.removeFilters(undeployedFilters);
                
                synchronized(orderedFilters)
                {
                    for(Iterator<Filter> it=undeployedFilters.iterator(); it.hasNext();) {
                        Filter undeployedFilter = it.next();
                        
                        for(Filter orderedFilter:orderedFilters) {
                            if(orderedFilter.getId().equals(undeployedFilter.getId()))
                                orderedFilters.remove(orderedFilter);
                        }
                    }

                    Collections.sort(orderedFilters);
                    
                    orderedFiltersMapById.clear();
                    
                    for (Filter filter : orderedFilters)
                        orderedFiltersMapById.put(filter.getId(), filter);
                }
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        return undeployedFilters;
    } //undeployFilters()

    protected void removeFilters(List<Filter> undeployedFilters) {
        ;
    }

    public List<Filter> getOrderedFilters() {
        return orderedFilters;
    }
    
    public void setOrderedFilters(List<Filter> orderedFilters) {
        this.orderedFilters = orderedFilters;
    }
    
    public Map<String, Filter> getOrderedFiltersMapById() {
        return orderedFiltersMapById;
    }
    
    public void setOrderedFiltersMapById(Map<String, Filter> orderedFiltersMapById) {
        this.orderedFiltersMapById = orderedFiltersMapById;
    }

    @Override
    public PushGatewayFilter getFilter(Map<String, Object> params) {
        
        return configurePushGatewayFilter(params);
    }

    protected String getServerUrl() {
        
        if(serverUrl != null)
            return serverUrl;
        
        Map<String, Object> properties = instance.loadSystemProperties(name);
        
        String host = (String)properties.get("HOST");
        String port = (String)properties.get("PORT");
        
        if(StringUtils.isBlank(host) || StringUtils.isBlank(port)) {
            Log.log.error("Host name or port is not available.");
            return null;
        }
        
        StringBuffer sb = new StringBuffer();
        sb.append("https://").append(host);
        if(StringUtils.isNotBlank(port))
            sb.append(":").append(port);
        
        serverUrl = sb.toString();
        
        return serverUrl;
    }

    public Map<String, Object> loadSystemProperties(String gatewayName) {

        Map<String, Object> systemProperties = new HashMap<String, Object>();
        
        PushGatewayProperties properties = (PushGatewayProperties)ConfigReceivePushGateway.getGatewayProperties(gatewayName);

        systemProperties.put("HOST", ((PushGatewayProperties)properties).getHost());
        systemProperties.put("PORT", ((PushGatewayProperties)properties).getPort());
        systemProperties.put("SSL", ((PushGatewayProperties)properties).isSsl());
        systemProperties.put("SSLTYPE", ((PushGatewayProperties)properties).getSslType());
        systemProperties.put("USER", ((PushGatewayProperties)properties).getUser());
        systemProperties.put("PASS", ((PushGatewayProperties)properties).getPass());

        return systemProperties;
    }
    
    private PushGatewayFilter configurePushGatewayFilter(Map<String, Object> filterAttrs) {
        
        String id = null;
        String active = null;
        String order = null;
        String interval = null;
        String eventEventId = null;
        String runbook = null;
        String script = null;
        
        for(Iterator<String> it=filterAttrs.keySet().iterator(); it.hasNext();) {
            String key = (String)it.next();
            String value = (String)(filterAttrs.get(key));
            if("ID".equals(key))
                id = value;
            else if("ACTIVE".equals(key))
                active = value;
            else if("ORDER".equals(key))
                order = value;
            else if("INTERVAL".equals(key))
                interval = value;
            else if("EVENT_EVENTID".equals(key))
                eventEventId = value;
            else if("RUNBOOK".equals(key))
                runbook = value;
            else if("SCRIPT".equals(key))
                script = value;
        }
        
        PushGatewayFilter filter = new PushGatewayFilter(id, active, order, interval, eventEventId, runbook, script);
        
        try {
            filter.setGatewayName((String)filterAttrs.get("GATEWAY_NAME"));
            filter.setQueue((String)filterAttrs.get("QUEUE"));
            filter.setUri((String)filterAttrs.get("URI"));
            filter.setBlocking((String)filterAttrs.get("BLOCKING"));
            
            Integer port = new Integer((String)filterAttrs.get("PORT"));
            filter.setPort(port);
            
            Boolean ssl = new Boolean((String)filterAttrs.get("SSL"));
            filter.setSsl(ssl);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        
        filter.addAttributes(filterAttrs);
        
        return filter;
    }

    protected Integer getWaitTimeout(String filterName) {
        
        int timeout = ConfigReceivePushGateway.DEFAULT_WAIT_TIMEOUT;
        
        try {
            Filter filter = getPushFilters().get(filterName);
            if(filter == null)
                return null;
            
            String gatewayName = ((PushGatewayFilter)filter).getGatewayName();
            PushGatewayProperties properties = (PushGatewayProperties)ConfigReceivePushGateway.getGatewayProperties(gatewayName);
            timeout = ((PushGatewayProperties)properties).getWaitTimeout();
        } catch(Exception e) {
            Log.log.error(e.getMessage(),e);
        }
        
        return timeout;
    }

    protected void addDefaultServlet(PushGatewayFilter filter) throws Exception {
        
        String gatewayName = filter.getGatewayName();
        PushGateway gateway = ConfigReceivePushGateway.getGateways().get(gatewayName);
        if(gateway == null)
            throw new Exception("Gateway not found:" + gatewayName);
        
        HttpServer defaultHttpServer = gateway.getDefaultHttpServer();
        
        if (defaultHttpServer.isStarted()) {
            defaultHttpServer.addServlet(filter);
            getDeployedServlets().put(filter.getId(), defaultHttpServer);
        }
    }
    
    protected void removeDefaultServlet(String id) throws Exception {
        
        PushGatewayFilter filter = getPushFilters().get(id);
        if(filter == null)
            throw new Exception("Filter not found:" + id);
        
        String gatewayName = filter.getGatewayName();
        PushGateway gateway = ConfigReceivePushGateway.getGateways().get(gatewayName);
        if(gateway == null)
            throw new Exception("Gateway not found:" + gatewayName);
        
        HttpServer defaultHttpServer = gateway.getDefaultHttpServer();
        
        defaultHttpServer.removeServlet(id);
    }
    
    protected Integer getExecuteTimeout(String filterName) {
        
        int timeout = ConfigReceivePushGateway.DEFAULT_WAIT_TIMEOUT;
        ConfigReceivePushGateway config = (ConfigReceivePushGateway)configurations;
        
        try {
            Filter filter = getPushFilters().get(filterName);
            if(filter == null)
                return null;
            
            String gatewayName = ((PushGatewayFilter)filter).getGatewayName();
            PushGatewayProperties properties = (PushGatewayProperties)config.getGatewayProperties(gatewayName);
            timeout = ((PushGatewayProperties)properties).getExecuteTimeout();
        } catch(Exception e) {
            Log.log.error(e.getMessage(),e);
        }
        
        return timeout;
    }

    protected String getRunbookResult(String problemId, String processId, String wiki) {

        String str = "";

        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.EXECUTE_PROBLEMID, problemId);
        params.put(Constants.EXECUTE_PROCESSID, processId);

        try {
            Map<String, Object> result = MainBase.getESB().call(Constants.ESB_NAME_RSCONTROL, "MAction.getRunbookResult", 
            		params, GLOBAL_SEND_TIMEOUT);

            if(result != null)
                str = parseRunbookResult(result, wiki);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }

        return str;
    }

    public String parseRunbookResult(Map<String, Object> result, String wiki)
    {
        String problemId = (String)result.get(Constants.EXECUTE_PROBLEMID);
        String processId = (String)result.get(Constants.EXECUTE_PROCESSID);
        String number = (String)result.get(Constants.WORKSHEET_NUMBER);
        String status = (String)result.get(Constants.STATUS);
        String condition = (String)result.get(Constants.EXECUTE_CONDITION);
        String severity = (String)result.get(Constants.EXECUTE_SEVERITY);
        List<Map<String, String>> taskStatus = (List<Map<String, String>>)result.get(Constants.TASK_STATUS);

        JSONObject json = new JSONObject();

        try
        {
            json.accumulate("Wiki Runbook", wiki);
            json.accumulate("Worksheet Id", problemId);
            json.accumulate("Worksheet Number", number);
            json.accumulate("Process Id", processId);
            json.accumulate("Condition", condition);
            json.accumulate("Severity", severity);
            json.accumulate("Status", status);

            if (StringUtils.isNotEmpty(status) && !status.equalsIgnoreCase(Constants.EXECUTE_STATUS_UNKNOWN))
            {
                JSONArray tasks = new JSONArray();

                for (Map<String, String> task:taskStatus)
                {
                    JSONObject taskItem = new JSONObject();
                    taskItem.accumulate("name", task.get(Constants.EXECUTE_TASKNAME));
                    taskItem.accumulate("id", task.get(Constants.EXECUTE_ACTIONID));
                    taskItem.accumulate("completion", task.get(Constants.EXECUTE_COMPLETION));
                    taskItem.accumulate("condition", task.get(Constants.EXECUTE_CONDITION));
                    taskItem.accumulate("severity", task.get(Constants.EXECUTE_SEVERITY));
                    taskItem.accumulate("summary", task.get(Constants.EXECUTE_SUMMARY));
                    taskItem.accumulate("detail", task.get(Constants.EXECUTE_DETAIL));

                    tasks.add(taskItem);
                }

                json.accumulate("Action Tasks", tasks);
            }
        }
        catch(Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return json.toString();

    } // parseRunbookResult()
    
    /**
     * This method processes the message received from the Push system.
     *
     * @param message
     */
    protected boolean processData(String filterName, Map<String, String> params) {
        
        boolean result = true;
        
        try {
            if (StringUtils.isNotBlank(filterName) && params != null) {
                PushGatewayFilter filter = pushFilters.get(filterName);
                
                if (filter != null && filter.isActive()) {
                    if (Log.log.isDebugEnabled()) {
                        Log.log.debug("Processing filter: " + filter.getId());
                        Log.log.debug("Data received through Push gateway: " + params);
                    }
                    
                    Map<String, String> runbookParams = params;
                    
                    if (!params.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(params.get(Constants.EVENT_EVENTID))) {
                        runbookParams.put(Constants.EVENT_EVENTID, filter.getEventEventId());
                    }
                    
                    runbookParams.put(FILTER_ID_NAME, filter.getId());
                    runbookParams.put(GATEWAY_NAME, filter.getGatewayName());
                    runbookParams.put(GATEWAY_TYPE, "Push");
                    
                    addToPrimaryDataQueue(filter, runbookParams);
                } else {
                    result = false;
                }
            }
        } catch (Exception e) {
            Log.log.warn("Error: " + e.getMessage(), e);
            try {
                Thread.sleep(interval);
            } catch (InterruptedException ie) {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
        return result;
    }
    
    protected String processBlockingData(String filterName, Map<String, String> params)
    {
        String result = "";

//        ExecutionMetrics.INSTANCE.registerEventStart("MoogSoft GW - execute runbook and get result or submit runbook for execution");

        try
        {
            if(StringUtils.isNotBlank(filterName) && params != null)
            {
                PushGatewayFilter httpFilter = pushFilters.get(filterName);
                if (httpFilter != null && httpFilter.isActive())
                {
                    if(Log.log.isDebugEnabled())
                    {
                        Log.log.debug("Processing filter: " + httpFilter.getId());
                        Log.log.debug("Data received through HTTP gateway: " + params);
                    }

                    Map<String, String> runbookParams = params;

                    if(!params.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(params.get(Constants.EVENT_EVENTID)))
                    {
                        runbookParams.put(Constants.EVENT_EVENTID, httpFilter.getEventEventId());
                    }

                    runbookParams.put(FILTER_ID_NAME, httpFilter.getId());
                    runbookParams.put(GATEWAY_NAME, httpFilter.getGatewayName());
                    runbookParams.put(GATEWAY_TYPE, "Push");

                    return instance.receiveData(runbookParams);
                }
                else
                {
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "The filter is inactive.");
                    result = message.toString();
                }
            }
        }
        catch (Exception e)
        {
            Log.log.warn("Error: " + e.getMessage(), e);
            try
            {
                Thread.sleep(interval);
            }
            catch (InterruptedException ie)
            {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
/*        finally
        {
            try
            {
                ExecutionMetrics.INSTANCE.registerEventEnd("MoogSoft GW - execute runbook and get result or submit runbook for execution", 1);
            }
            catch (ExecutionMetricsException eme)
            {
                Log.log.error(eme.getMessage(), eme);
            }
        }
*/
        return result;
    }
    
    protected int getServerCheckInterval() {
        
        GatewayProperties properties = ConfigReceivePushGateway.getGatewayProperties(name);
        if(properties != null)
            return properties.getServerCheckInterval();

        return 0;
    }

    protected int getServerCheckRetry() {

        GatewayProperties properties = ConfigReceivePushGateway.getGatewayProperties(name);
        if(properties != null)
            return properties.getServerCheckRetry();

        return 0;
    }
    
} // class PushGateway
