/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.itm;

import java.io.File;
import java.io.IOException;

import com.resolve.util.FileUtils;

/**
 * This class is available as a convenience class for handling error code
 * returned by the ITM.
 * 
 * The following table lists the return codes for the tacmd commands.
 * 
 * Table 6. Return Codes for tacmd CLI commands Code Category Description 0
 * Success Indicates that the command was successful. 1 Syntax Error or Help
 * Indicates either that the help command was given or that the syntax used was
 * incorrect. 2 No Permission Indicates that the user does not have permission
 * to issue the command. 3 Version Mismatch Indicates that the version of the
 * server is not what was expected. 4 Communication Error Indicates that an
 * error occurred in the communications with the server. 5 Timeout Indicates
 * that an operation waiting for data did not receive it within the time it was
 * expected. 6 Input Error Indicates that the input to the command was not what
 * was expected. 7 Server Exception Indicates that an error occurred on the
 * server that caused the command to fail. 8 Command Error Indicates that an
 * internal error occurred while executing the command. 9 Invalid Object
 * Indicates that a specified object does not exist. 10 Duplicate Object
 * Indicates that the object is a duplicate of an already existing object. 11
 * Partial Success Indicates that the command was partially successful. 15
 * Password too long Indicates that the password is longer than 16 characters,
 * which is the maximum password length 19 Situation not present Indicates that
 * there is no historical situation in the server. 100 Deploy Queued Indicates
 * that the deploy operation is queued. 101 Deploy in Progress Indicates that
 * the deploy operation is in progress. 102 Deploy Retryable Indicates that the
 * deploy operation is retryable. 103 Deploy failed Indicates that the deploy
 * operation failed. 127 Invalid command.
 */
public class ErrorHandler
{
    private static final String COMMAND_RESULT = "Command Result";

    public static boolean verifyStatus(File responseFile) throws Exception
    {

        int statusCode = getStatusCode(responseFile);

        boolean result = true;
        String message = null;
        switch (statusCode)
        {
            case 0: // not an error
                break;
            case 1:
                message = "Error 1: Either the help command was given or the syntax used is incorrect.";
                result = false;
                break;
            case 2:
                message = "Error 2: The user does not have permission to issue the command.";
                result = false;
                break;
            case 3:
                message = "Error 3: The version of the server is not what was expected.";
                result = false;
                break;
            case 4:
                message = "Error 4: An error occurred in the communications with the server.";
                result = false;
                break;
            case 5:
                message = "Error 5: An operation waiting for data did not receive it within the time it was expected.";
                result = false;
                break;
            case 6:
                message = "Error 6: The input to the command was not what was expected.";
                result = false;
                break;
            case 7:
                message = "Error 7: An error occurred on the server that caused the command to fail.";
                result = false;
                break;
            case 8:
                message = "Error 8: An internal error occurred while executing the command.";
                result = false;
                break;
            case 9:
                message = "Error 9: Invalid object, a specified object does not exist.";
                result = false;
                break;
            case 10:
                message = "Error 10: Duplicate object, the object is a duplicate of an already existing object.";
                result = false;
                break;
            case 11:
                message = "Error 11: The command was partially successful.";
                result = false;
                break;
            case 15:
                message = "Error 15: The password is longer than 16 characters, which is the maximum password length.";
                result = false;
                break;
            case 19:
                message = "Error 19: There is no historical situation in the server.";
                result = false;
                break;
            case 100:
                message = "Error 100: The deploy operation is queued.";
                result = false;
                break;
            case 101:
                message = "Error 101: The deploy operation is in progress.";
                result = false;
                break;
            case 102:
                message = "Error 102: The deploy operation is retryable.";
                result = false;
                break;
            case 103:
                message = "Error 103: The deploy operation failed.";
                result = false;
                break;
            case 126:
                message = "Error 126: The command cannot be run for unknown reason.";
                result = false;
                break;
            case 127:
                message = "Error 127: Invalid command.";
                result = false;
                break;
            default:
                // although unknown code, cannot assume it's an error.
                result = true;
                break;
        }
        if (!result)
        {
            throw new Exception(message);
        }
        return result;
    }

    private static int getStatusCode(File responseFile) throws IOException
    {
        boolean resultFound = false;

        // if command result is not found by default assume it's executed
        // successfully.
        int resultCode = 0;
        for (String line : FileUtils.readLines(responseFile))
        {
            if (resultFound)
            {
                try
                {
                    resultCode = Integer.valueOf(line.trim());
                    break;
                }
                catch (NumberFormatException e)
                {
                    // Totally unknown code, but cannot assume to be an error,
                    // hence breaking away.
                    break;
                }
            }
            if (line.contains(COMMAND_RESULT))
            {
                resultFound = true;
            }
        }
        return resultCode;
    }
}
