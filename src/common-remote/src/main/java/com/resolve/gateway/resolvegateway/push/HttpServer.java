package com.resolve.gateway.resolvegateway.push;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlet.ServletMapping;
import org.eclipse.jetty.util.ssl.SslContextFactory;

import com.resolve.gateway.pushauth.PushAuthHandlerFactory;
import com.resolve.gateway.resolvegateway.push.PushGatewayFilter;
import com.resolve.util.Log;
import com.resolve.util.ResolveDefaultHttpServlet;
import com.resolve.util.StringUtils;

public class HttpServer
{
    private Server server;
    private Integer port;
    private Boolean isSsl;
    private Map<String, PushGatewayFilter> servlets;
    private ConfigReceivePushGateway config;
    
    /**
     * This constructor is needed so that the port based filters could provide
     * their individual port and ssl setting.
     * 
     * @param config
     * @param port
     * @param isSsl
     */
    public HttpServer(ConfigReceivePushGateway config, Integer port, Boolean isSsl, String name) 
    {
        this.config = config;
        
        int index = name.indexOf("Gateway");
        if(index != -1)
            name = name.substring(0,  index);
        
        PushGatewayProperties properties = (PushGatewayProperties)ConfigReceivePushGateway.getGatewayProperties(name);
        if(properties == null) {
            Log.log.warn("Failed to find gateway properties for gateway with name of " + name);
            return;
        }
        
        server = new Server();
        servlets = new HashMap<String, PushGatewayFilter>();
        
        HttpConfiguration http_config = new HttpConfiguration();
        
        http_config.setSecureScheme("https");
        http_config.setSendXPoweredBy(false);
        http_config.setSendServerVersion(false);
                
        int httpPort = properties.getHttpPort() != null ? properties.getHttpPort() : 0 ;
        if (port != null && port > 0)
            httpPort = port;
        
        this.port = httpPort;
        this.isSsl = properties.getHttpSsl();
        
        if (httpPort > 0)
        {
            if (this.isSsl)
            {
                SslContextFactory sslContextFactory = new SslContextFactory(properties.getSslCertificate());
                sslContextFactory.setKeyStorePassword(properties.getSslPassword());
                sslContextFactory.setKeyManagerPassword(properties.getSslPassword());
//                sslContextFactory.setProtocol("TLSv1.2");
//                sslContextFactory.setIncludeCipherSuites("AES_128_GCM");
                sslContextFactory.setIncludeProtocols("TLSv1.2");
                // sslContextFactory.setNeedClientAuth(true);
                ServerConnector serverConnector = new ServerConnector(server, sslContextFactory,
                                                                      new HttpConnectionFactory(http_config));
                serverConnector.setPort(httpPort);
//                SslSocketConnector sslConnector = new SslSocketConnector(sslContextFactory);
//                sslConnector.setPort(httpPort);
//                server.addConnector(sslConnector);
                server.addConnector(serverConnector);
            }
            else
            {
//                Connector connector = new ServerConnector(server);
                ServerConnector connector = new ServerConnector(server, new HttpConnectionFactory(http_config));
                connector.setPort(httpPort);
                connector.setIdleTimeout(30000);
//                connector.setMaxIdleTime(30000);
                server.addConnector(connector);
            }
            //ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
            //context.setContextPath("/");
            ServletContextHandler context = new ServletContextHandler(server, "/");
            server.setHandler(context);
/*            try
            {
                server.setHandler(PushAuthHandlerFactory.wrapHandler(config, server, context));
            }
            catch (UnknownAuthenticationTypeException | InvalidAuthConfigurationException e)
            {
                Log.log.error(e.getMessage(), e);
            }*/
            
            
            //context.addServlet(org.eclipse.jetty.servlet.DefaultServlet.class, "/");
        }
        else
        {
            Log.log.warn("Invalid port provided to start server.");
        }
    }

    public void init() throws Exception
    {
        // new servlet end point will be added to this context
        // ServletContextHandler context = new ServletContextHandler(server,
        // "/");
        // server.setHandler(context);
    }

    public void start() throws Exception
    {
        Log.log.info("Starting HTTP Server on port: " + port + " with ssl: " + isSsl);
        try
        {
            server.start();
            Log.log.info("HTTP Server started on port: " + port + " with ssl: " + isSsl);
        }
        catch (Throwable e)
        {
            Log.log.info("HTTP Server already started on port: " + port + " with ssl: " + isSsl);
        }
        // server.join();
    }

    public void stop() throws Exception
    {
        Log.log.info("Stopping HTTP Server on port: " + port + " with ssl: " + isSsl);
        server.stop();
    }

    public boolean isStarted()
    {
        return server.isStarted();
    }

    public boolean isStopped()
    {
        return server.isStopped();
    }

    public Map<String, PushGatewayFilter> getServlets()
    {
        return servlets;
    }

    public void addServlet(final PushGatewayFilter filter) throws Exception
    {
        String filterName = filter.getId();
        String uri = StringUtils.isBlank(filter.getUri()) ? filter.getId() : filter.getUri();
        boolean deploy = true;
        if(servlets.containsKey(filterName))
        {
            Log.log.debug("Servlet already exists: " + filterName);
            PushGatewayFilter existingFilter = servlets.get(filterName);
            //if uri changed we need to adjust
            if(StringUtils.equals(existingFilter.getUri(), filter.getUri()))
            {
                deploy = false;
            }
            else
            {
                removeServlet(filterName);
            }
        }

        if(deploy)
        {
            String finalUri = uri;
            if (!uri.startsWith("/"))
            {
                finalUri = "/" + uri;
            }
            Log.log.debug("Starting filter at URI: " + finalUri);
            ServletHandler context = getContext();
            
            ServletHolder servletHolder = new ServletHolder(filterName, new FilterServlet(filterName));
            context.addServletWithMapping(servletHolder, finalUri);
            
            PushAuthHandlerFactory.registerFilter(config, server.getHandler(), filter);

            servlets.put(filterName, filter);
        }
    }
    
    public void removeServlet(String filterName)
    {
        Log.log.debug("Stopping filter end point for : " + filterName);
        try
        {
            ServletHandler handler = getContext();
            
            ServletHolder[] holders = handler.getServlets();
    
            List<ServletHolder> remainingServlets = new ArrayList<ServletHolder>();
            Set<String> names = new HashSet<String>();
            for (ServletHolder holder : holders)
            {
                if (!filterName.equals(holder.getName()))
                {
                    remainingServlets.add(holder);
                    names.add(holder.getName());
                }
            }
            
            List<ServletMapping> mappings = new ArrayList<ServletMapping>();
            for(ServletMapping mapping : handler.getServletMappings())
            {
               if(names.contains(mapping.getServletName()))
               {
                   mappings.add(mapping);
               }
            }
            
            /* Set the new configuration for the mappings and the servlets */
            handler.setServletMappings(mappings.toArray(new ServletMapping[0]));
            handler.setServlets(remainingServlets.toArray(new ServletHolder[0]));
            
            if(this.servlets.containsKey(filterName))
            {
                
                PushAuthHandlerFactory.unregisterFilter(config, server.getHandler(), this.servlets.get(filterName));
                
            }
            
                      
            this.servlets.remove(filterName);
            this.server.setHandler(handler);
            Log.log.debug("Successfully stopped filter end point for : " + filterName);
        }
        catch(Exception e)
        {
            if(e.getMessage() != null && e.getMessage().contains("STARTED")) //when started is new stopped? :)
            {
                Log.log.debug("Successfully stopped filter end point for : " + filterName);
            }
            else
            {
                Log.log.error("Could not stopped servlet for filter: " + filterName + ". " + e.getMessage());
            }
        }
    }
    
    public void addDefaultResolveServlet() throws Exception
    {
        Log.log.debug("Adding Default Resolve Servlet at URI: " + "/");
        ServletHandler context = getContext();
        ServletHolder servletHolder = new ServletHolder("/", new ResolveDefaultHttpServlet());
        context.addServletWithMapping(servletHolder, "/");
    }

    private ServletHandler getContext()
    {
        ServletHandler context;
        
        Handler subject = server.getHandler();
        
        if(subject instanceof ConstraintSecurityHandler)
        {
            
            subject = ((ConstraintSecurityHandler) subject).getHandler();

        }
        
        if(subject instanceof ServletContextHandler)
        {
            context = ((ServletContextHandler) subject).getServletHandler();
        }
        else
        {
            context = (ServletHandler) subject;
        }
        return context;
    }
}
