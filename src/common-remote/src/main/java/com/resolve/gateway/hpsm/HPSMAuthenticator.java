/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.hpsm;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

public class HPSMAuthenticator extends Authenticator
{

    private String username;
    private char[] password;

    public HPSMAuthenticator(final String username, final String password)
    {
        super();
        this.username = new String(username);
        if(password != null)
        {
            this.password = password.toCharArray();
        }
    }

    @Override
    public PasswordAuthentication getPasswordAuthentication()
    {
        return (new PasswordAuthentication(username, password));
    }
}
