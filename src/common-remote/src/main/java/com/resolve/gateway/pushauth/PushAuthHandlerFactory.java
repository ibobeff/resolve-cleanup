package com.resolve.gateway.pushauth;

import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.util.component.ContainerLifeCycle;

import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.ConfigMap;
import com.resolve.util.Log;

public class PushAuthHandlerFactory
{
    
    
    
    /**
     * 
     *  The set of push auth schemes.  To add one just create a new enum record.
     *  This could be more extensible with some IOC. #futurechange
     *  
     *  The enum name is matched with the config value which returns a factory with the enum.
     */
    protected static enum PushAuthScheme
    {
        
        /**
         * This is the basic authentication scheme, which allows configuration of a username and password
         */
        BASIC ( new BasicPushAuthHandlerFactory());
        
       
         
        /**
         * The element constructor
         * @param fact  The factory to associate with this enum constant
         */
        private PushAuthScheme(  ServletAuthHandlerFactory fact)
        {
             this.factory = fact;
        }
        /**
         * The factory to use. 
         */
         public ServletAuthHandlerFactory factory;
        
    }
    
    
    protected PushAuthHandlerFactory()
    {
        
        
    }
    
    
    private static volatile PushAuthHandlerFactory instance;

    /**
     * Obtains the default PushAuthHandlerFactory instance
     * @return
     */
    protected static synchronized PushAuthHandlerFactory getDefaultInstance()
    {   
        if(instance == null)
            instance = new PushAuthHandlerFactory();
         
       
        return instance;
    }
    
    /**
     * Wraps a servlet Handler for the given ConfigMap.  
     * @param map   The config map for this push gateway
     * @param clc   The container lifecycle object (usually the server)
     * @parma h  The handler to wrap
     * @param context  A human readable context for logging and error reporting (the name of the gateway should be used)
     * 
     * @return h   A Handler that will enforce authentication based on the configuration.  The provided handler is returned if no Handler is to be added.
     * @throws UnknownAuthenticationTypeException If the provided authentication type is unknown
     */
    public static synchronized Handler wrapHandler(ConfigReceiveGateway map, ContainerLifeCycle clc, Handler h) throws UnknownAuthenticationTypeException, InvalidAuthConfigurationException
    {
        
        String context = map.getQueue();
        if(context == null)
            context = "Unknown";
        //use queue name instead of context. 
        ConstraintSecurityHandler sh = getDefaultInstance().getHandlerFor(map, clc, context);
        
        if(sh == null )
        {   
            Log.log.warn("No security configured for push gateway " + context);
            return h;
        }
        
        sh.setHandler(h);
        
        return sh;
        
    }
    
    /**
     * Registers the specified filter to have authentication
     * @param map  The gateway config map
     * @param h    The handler for the gateway
     * @param filter  The filter to be registered
     *
     * @throws UnknownAuthenticationTypeException
     * @throws InvalidAuthConfigurationException
     */
    public static synchronized void registerFilter(ConfigReceiveGateway map, Handler h, Object filter) throws UnknownAuthenticationTypeException, InvalidAuthConfigurationException
    {
        
        if(h instanceof ConstraintSecurityHandler)
        {
        
            String context = map.getQueue();
            if(context == null)
                context = "Unknown";
            
            PushAuthScheme scheme = getDefaultInstance().getAuthSchemeFromConfig(map, context);
          
            
            scheme.factory.registerFilter(map, ((ConstraintSecurityHandler) h) , filter);
            
        
        }
        
    }
    
    
    /**
     * Unregisters the specified filter to have authentication
     * @param map  The gateway config map
     * @param h    The handler for the gateway
     * @param filter  The filter to be registered
     *
     * @throws UnknownAuthenticationTypeException
     * @throws InvalidAuthConfigurationException
     */
    public static synchronized void unregisterFilter(ConfigReceiveGateway map, Handler h, Object filter) throws UnknownAuthenticationTypeException, InvalidAuthConfigurationException
    {
        
        if(h instanceof ConstraintSecurityHandler)
        {
        
            String context = map.getQueue();
            
            
            PushAuthScheme scheme = getDefaultInstance().getAuthSchemeFromConfig(map, context);
            
            
            scheme.factory.unregisterFilter(map, ((ConstraintSecurityHandler) h) , filter);
            
        
        }
        
    }
    
    
    
    /**
     * Obtains a servlet Handler for the given ConfigMap.  
     * @param map   The config map for this push gateway
     * @param context  The name of the gateway for logging and error handling purposes
     * @param clc   The container lifecycle object (usually the server)
     * @return A Handler that will enforce authentication based on the configuration.  Null is returned if no Handler is to be added.
     * @throws UnknownAuthenticationTypeException If the provided authentication type is unknown
     */
    protected ConstraintSecurityHandler getHandlerFor(ConfigMap map, ContainerLifeCycle clc, String context) throws UnknownAuthenticationTypeException, InvalidAuthConfigurationException
    {
      
        PushAuthScheme scheme = getAuthSchemeFromConfig(map, context);
        
        if(scheme == null)
            return null;
        
        return scheme.factory.create(map, clc, context);
        
    }
    
    
    /**
     * Obtains a push auth scheme from the configuration
     * @return The push auth scheme for the specified configuration
     */
    protected PushAuthScheme getAuthSchemeFromConfig(ConfigMap map, String context) throws UnknownAuthenticationTypeException
    {
        String type = null;
        type = PushAuthConfigUtil.getValueFromConfig(map, PushAuthConstants.PROPERTY_NAME_HTTP_AUTH_TYPE, true);
        
         
        
        if(type  == null || org.apache.commons.lang3.StringUtils.isEmpty(type))
            return null;
         

        try {
            Log.log.debug("Trying authentication type " + type);
                            
            PushAuthScheme result =  PushAuthScheme.valueOf(type); 
            
            
            return result;
            
            
        }catch(IllegalArgumentException e)  //Thrown per spec if there is no match  https://docs.oracle.com/javase/7/docs/api/java/lang/Enum.html#valueOf(java.lang.Class,%20java.lang.String)
        {
            
            throw new UnknownAuthenticationTypeException("Unknown authentication type '" + type + "' given for push gateway " + context, e);
        }
        
    }


    
    
    
}
