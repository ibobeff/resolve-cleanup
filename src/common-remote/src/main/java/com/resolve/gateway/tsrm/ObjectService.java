/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.tsrm;

import java.util.List;
import java.util.Map;

public interface ObjectService
{

    /**
     * The implementing object provides this so the the unmarshaller gets to the
     * main node. e.g., for Ticket the main node is <MXSRSet/>.
     * 
     * @return
     */
    public String getMainNodeName();

    /**
     * The implementing object provides this so the the unmarshaller gets to the
     * object node. e.g., for Ticket the object node is "<SR/>".
     * 
     * @return
     */
    public String getObjectNodeName();

    /**
     * Every object has its ID property, the subclass requires to implement this
     * method. Example: Service request could be "TICKETUID".
     * 
     * @return
     */
    String getIdPropertyName();

    /**
     * Identity of the object. Implementing class provides this. Example:
     * Service request could be "sr" or Asset could be "asset"
     * 
     * @return
     */
    String getIdentity();

    RestCaller getRestCaller();

    void setRestCaller(RestCaller restCaller);

    /**
     * Creates an object without username, password. This method will use the
     * username, password from the configuration.
     * 
     * @param params
     * @return
     * @throws Exception
     */
    // Map<String, String> create(Map<String, String> params) throws Exception;

    /**
     * Creates an object by supplying username, password.
     * 
     * @param params
     * @param username
     * @param password
     * @return a Map with all the properties of the new object.
     * @throws Exception
     */
    Map<String, String> create(Map<String, String> params, String username, String password) throws Exception;

    /**
     * Creates a Worklog for an object. This method will use the username,
     * password from the configuration.
     * 
     * @param parentObjectId
     *            such as TICKETUID for an SR. It's mandatory.
     * @param params
     *            refer to the Worklog object in your environment for possible
     *            entries.
     * @return
     * @throws Exception
     */
    // boolean createWorklog(String parentObjectId, Map<String, String> params)
    // throws Exception;

    /**
     * Creates an object by supplying username, password.
     * 
     * @param parentObjectId
     *            such as TICKETUID for an SR. It's mandatory.
     * @param params
     *            refer to the Worklog object in your environment for possible
     *            entries.
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    boolean createWorklog(String parentObjectId, Map<String, String> params, String username, String password) throws Exception;

    /**
     * Updates an object without username, password. This method will use the
     * username, password from the configuration.
     * 
     * @param objectId
     *            is mandatory.
     * @param params
     * @return
     * @throws Exception
     */
    // void update(String objectId, Map<String, String> params) throws
    // Exception;

    /**
     * Updates an object by supplying username, password.
     * 
     * @param objectId
     *            is mandatory.
     * @param params
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    void update(String objectId, Map<String, String> params, String username, String password) throws Exception;

    /**
     * Deletes an object by its id without username, password. This method will
     * use the username, password from the configuration.
     * 
     * @param objectId
     *            is mandatory.
     * @return
     * @throws Exception
     */
    // void delete(String objectId) throws Exception;

    /**
     * Deletes an object by its id by supplying username, password.
     * 
     * @param objectId
     *            is mandatory.
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    void delete(String objectId, String username, String password) throws Exception;

    /**
     * Read details of an object by its id. This method will use the username,
     * password from the configuration.
     * 
     * @param objectId
     *            is mandatory
     * @return
     * @throws Exception
     */
    // Map<String, String> selectByID(String objectId) throws Exception;

    /**
     * Retrieves an object's data by its id.
     * 
     * @param objectId
     *            is mandatory
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    Map<String, String> selectByID(String objectId, String username, String password) throws Exception;

    /**
     * Reads data from the object service without username, password. This
     * method will use the username, password from the configuration.
     * 
     * @param query
     * @return
     * @throws Exception
     */
    // List<Map<String, String>> select(String query) throws Exception;

    /**
     * Read data from the object service by supplying username, password.
     * 
     * @param query
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    List<Map<String, String>> search(String query, String username, String password) throws Exception;

    String search(String objectURI, String query, String username, String password) throws Exception;

    /**
     * This method converts an XML string into a {@link List} of {@link Map}.
     * The implementing class must know the Xml structure.
     * 
     * @param xmlContent
     * @return
     * @throws Exception
     */
    List<Map<String, String>> unmarshal(String xmlContent) throws Exception;

}
