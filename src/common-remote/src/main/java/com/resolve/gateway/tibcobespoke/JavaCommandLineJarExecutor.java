package com.resolve.gateway.tibcobespoke;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;

import com.resolve.rsbase.MainBase;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class JavaCommandLineJarExecutor
{
	private static final String INVALID_COMMANDLINE_CONTEXT = "Invalid command line ";
	private static final String RESOLVE_TEXT_NO_AMPERSAND_VALIDATOR = "ResolveTextNoSemiColon";
	private static final String RESOLVE_TEXT_NO_SEMICOLON_VALIDATOR = "ResolveTextNoAmpersand";
	private static final int MAX_COMMAND_LINE_SIZE = 256;
	
	public static Collection<String> blakcListedCommands = new ArrayList<String>();
	public static String validatorName = null;
	
    private File execDir;
    private String javaHome;
    private String classPath;
    private String execEnv[];
    
    static {
    	if (System.getProperty("os.name").toLowerCase().contains("win")) {
    		validatorName = RESOLVE_TEXT_NO_AMPERSAND_VALIDATOR;
    		blakcListedCommands.add(String.format("rmdir %s /s /q", MainBase.main.configGeneral.home));
    		blakcListedCommands.add(String.format("rm %s -r -f", MainBase.main.configGeneral.home));
    		blakcListedCommands.add("rmdir C:\\ /s /q");
    		blakcListedCommands.add("rm C:\\ -r -f");
    	} else {
    		validatorName = RESOLVE_TEXT_NO_SEMICOLON_VALIDATOR;
    		blakcListedCommands.add(String.format("rm -r -f %s", MainBase.main.configGeneral.home));
    		blakcListedCommands.add(String.format("/bin/rm -r -f %s", MainBase.main.configGeneral.home));
    		blakcListedCommands.add("rm -r -f /");
    		blakcListedCommands.add("/bin/rm -r -f /");
    	}
    }
    /***
     * Initilizer for executing jars.
     * @param execDir - Directory to execute program in
     * @param javaHome - location of java, defaults to resolves java reference if not specified
     * @param classPath - class path of all required jars, Where are they located? Seperated by ;(windows) :(linux)
     * @param javaOptions - Java options such as -Dproperties or memory allocation
     * @param execEnv - heck out java.lang.ProcessBuilder to determine what can be specified here
     */
    public JavaCommandLineJarExecutor(String execDir, String javaHome, String classPath, String[] execEnv)
    {   
        if(StringUtils.isEmpty(execDir))
        {
            execDir = System.getProperty("user.dir");
        }
        if(StringUtils.isEmpty(javaHome))
        {
            javaHome = System.getProperty("java.home");
        }
        if(StringUtils.isEmpty(classPath))
        {
            throw new RuntimeException("Must define classpath containing executable jar");
        }
            
        this.execDir = FileUtils.getFile(execDir);
        this.javaHome = javaHome;
        this.classPath = classPath;
        this.execEnv = execEnv;
        
    }
    
    public void setEnv(String[] execEnv)
    {
        this.execEnv = execEnv;
    }
    
    /***
     * Builds the command in the format of: "$javahome/bin/java" -cp "classpath" $javaOptions "$className" $args
     * @param className - main to execute within the defined jar files of the classPath
     * @param args - arguments needed for the main class
     */
    public void executeIndefinitely(String className, String args)
    {
        Log.log.debug("Inside JavaCommandLineJarExecutor.executeIndefinitely(" + className + ", " + args + ")");
        
        //Builds "$javahome/bin/java" -cp "classpath" $javaOptions "$className" $args
        String commandLine = "\""+this.javaHome+"/bin/java"+"\" "+"-cp "+"\""+this.classPath+"\" "+"\""+className+"\" "+args;
        Log.log.debug("Executing jar with system command: [" + commandLine + "]");
        
        Process process = null;
        
        try
        {             
                
            // execute command
            List<String> commandList = prepareCommand(commandLine);
            ProcessBuilder pb = new ProcessBuilder(commandList);          
            execDir.mkdirs(); //make the directories recurisively if they execdir does not exist
            pb.directory(execDir);
            pb.redirectErrorStream(true);        
            // init enviroments
            Log.log.debug("Process Builder CommandList: " + commandList.toString());
            Log.log.debug("Before Process Builder env set: " + pb.environment().toString());
            if (execEnv != null)
            {
                Map<String, String> env = pb.environment();
                int i=0;
                while (i < execEnv.length)
                {
                    env.put(execEnv[i], execEnv[i+1]);
                    i += 2;
                }
            }
            
            Log.log.debug("After Process Builder env set: " + pb.environment().toString());
            validateProcessBuilderCommandLine(pb);
            process = pb.start();   
            
            InputHandler inHandler = new InputHandler(process.getInputStream(), process);
            inHandler.start();
            InputHandler errHandler = new InputHandler(process.getErrorStream(), process);
            errHandler.start();
       
        }
        catch (Throwable e)
        {
            Log.log.error("Issue executing Jar: " + e.getMessage(), e);
        }
    }
        
    private List<String> prepareCommand(String command)
    {
        ArrayList<String> result = new ArrayList<String>();
        
        if(Log.log.isTraceEnabled())
        {
            Log.log.trace("Execute command: "+command);
        }
        
        StringBuffer currPart = new StringBuffer();
        boolean isEscaped = false;
        boolean insideQuote = false;
        char quoteChar = '\0';
        
        for (int x = 0; x < command.length(); x++) 
        { 
            char ch = command.charAt(x);
            char ch2 = '\0';
            if (x+1 < command.length())
            {
                ch2 = command.charAt(x+1);
            }
        
            // handle escaped \ - for the following characters " '
            if ((ch == '\\') && ((ch2 == '"')  || ch2 == '\''))
            {
                isEscaped = true;
            }
            else if (isEscaped)
            {
                isEscaped = false;
                currPart.append(ch); 
            }
            
            // match first quote
            else if (insideQuote == false && (ch == '\'' || ch == '"'))
            { 
                quoteChar = ch;
                insideQuote = true;
            } 
            
            // match end quote
            else if (insideQuote == true && ch == quoteChar)
            {
                quoteChar = '\0';
                insideQuote = false;
                
                // only add if not empty "" or " "
                if (!currPart.toString().trim().equals(""))
                {
                    result.add(sanitizeCommandline(currPart.toString())); 
                }
                currPart = new StringBuffer(); 
            }
            else if (insideQuote == false && Character.isWhitespace(ch)) 
            { 
                // only add if not empty "" or " "
                if (!currPart.toString().trim().equals(""))
                {
                    result.add(sanitizeCommandline(currPart.toString())); 
                }
                currPart = new StringBuffer(); 
            } 
            else 
            { 
                isEscaped = false;
                currPart.append(ch); 
            }
        
            // get the last part of the command 
            if (x == command.length() - 1) 
            {
                // only add if not empty "" or " "
                if (!currPart.toString().trim().equals(""))
                {
                    result.add(sanitizeCommandline(currPart.toString())); 
                }
            } 
        }

        Log.log.trace("preparedCommand:");
        int idx = 0;
        for (Iterator<String> i=result.iterator(); i.hasNext(); )
        {
            Log.log.trace(idx+": "+i.next());
            idx++;
        }
        
        return result; 
    } // prepareCommand  
    
    public static ArrayList<String> getActiveTibcoProcesses()
    {
        ArrayList<String> processList = new ArrayList<String>();
        try 
        {
            String curLine;
            Process p = Runtime.getRuntime().exec("ps aux");
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((curLine = input.readLine()) != null) 
            {
                if(curLine.contains("rstib.jar"))
                {
                    String[] processLineSplit = curLine.replaceAll("\\s+", " ").split(" ");
                    String processId = "";
                    if(processLineSplit.length >= 2)
                    {
                        processId = processLineSplit[1];
                        processId.trim();
                        processList.add(processId);
                    }                   
                }
            }
            input.close();
        } 
        catch (Exception e) 
        {
            Log.log.error(e);
        }
        
        return processList;
    }
    
    private static class InputHandler extends Thread {

        private final InputStream is;

        private final ByteArrayOutputStream os;
        private Process process;

        public InputHandler(InputStream input, Process process) {
            this.is = input;
            this.os = new ByteArrayOutputStream();
            this.process = process;
        }

        public void run() {
            try 
            {
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line = null;
                while ((line = br.readLine()) != null)
                {
                    Log.log.debug(process.hashCode() + " | " + line);
                    if(line.contains("BusResponder terminating after processing"))
                    {
                        process.destroy();
                    }
                }
                br.close();
            } 
            catch (Throwable t) 
            {
                throw new IllegalStateException(t);
            }
        }

        public String getOutput() {
            try {
            os.flush();
            } catch (Throwable t) {
                throw new IllegalStateException(t);
            }
            return os.toString();
        }

    }
    
    private String sanitizeCommandline(String commandline) {
    	String sanitizedCommandline = "";
    	
    	if (StringUtils.isNotBlank(commandline)) {
	    	try {
	    		sanitizedCommandline = ESAPI.validator().getValidInput(INVALID_COMMANDLINE_CONTEXT, commandline, 
	    														   	   validatorName, MAX_COMMAND_LINE_SIZE, false, false);
	    	} catch (ValidationException | IntrusionException e) {
	    		Log.log.warn(String.format("Error %sin sanitizing passed command line [%s]", 
	    								   (StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
	    								   commandline));
	    		sanitizedCommandline = "";
	    	}
	    	
	    	// Check against black listed commands
	    	/*
	    	 * Ideally list of black listed command should be specified and retrieved from blueprint
	    	 * so that Resolve administrator can configure and update list of black listed commands.
	    	 */
	    	
	    	if (blakcListedCommands.contains(sanitizedCommandline)) {
	    		Log.log.warn(String.format("Sanitized command line [%s] is one of black listed commands", sanitizedCommandline));
	    		sanitizedCommandline = "";
	    		
	    	}
    	}
    	
    	return sanitizedCommandline.trim();
    }
    
    private void validateProcessBuilderCommandLine(ProcessBuilder pb) throws Exception {
    	if (pb == null) {
    		return;
    	}
    	
    	List<String> commandList = pb.command();
    	
    	if (CollectionUtils.isEmpty(commandList)) {
    		return;
    	}
    	
    	final String commandLine = StringUtils.listToString(commandList, " ");
    	
    	ESAPI.validator().getValidInput(INVALID_COMMANDLINE_CONTEXT, commandLine, 
    									validatorName, MAX_COMMAND_LINE_SIZE * commandList.size(), false, false);
    	// Check against black listed commands
    	/*
    	 * Ideally list of black listed command should be specified and retrieved from blueprint
    	 * so that Resolve administrator can configure and update list of black listed commands.
    	 */
    	
    	boolean containsBlackListedCommand = blakcListedCommands.stream().filter(bc -> commandLine.contains(bc))
    										 .findAny().isPresent();

    	if (containsBlackListedCommand) {
    		throw new Exception(String.format("Process builder commadn line [%s] contains black listed command", 
    										  commandLine));
    	}
    }
}