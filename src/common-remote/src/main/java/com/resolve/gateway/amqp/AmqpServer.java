/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.amqp;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Consumer;
import com.resolve.esb.ESBException;
import com.resolve.rsremote.ConfigReceiveAmqp;
import com.resolve.util.Log;

/**
 * This class manages entire interaction with the RabbitMQ server. This has mechanism to re-establish
 * connection to the server, intializing Topics.
 *
 */
public class AmqpServer
{
    private static volatile AmqpServer instance = null;

    private boolean isActive = false;
    private Connection connection;
    private Channel channel;
    private AmqpDriver driver;
    private final ConfigReceiveAmqp config;

    protected Map<String, String> publications = new HashMap<String, String>();
    protected Map<String, String> subscriptions = new HashMap<String, String>();

    //topic or queue listeners
    protected Map<String, AmqpListener> listeners = new HashMap<String, AmqpListener>();
    //topic or queue publishers
    protected Map<String, AmqpSender> publishers = new HashMap<String, AmqpSender>();

    private AmqpServer(final ConfigReceiveAmqp config) throws ESBException
    {
        this.config = config;
        init();
        // create connection
        boolean created = false;
        while (!created)
        {
            try
            {
                connection = driver.getConnection();
                channel = connection.createChannel();
                created = true;
            }
            catch (Exception e)
            {
                Log.log.warn("Retry createConnection. " + e.getMessage());
                try
                {
                    Thread.sleep(5000L);
                }
                catch (InterruptedException e1)
                {
                    // ignore
                }
            }
        }
    } // MAmqpServer

    public static AmqpServer getInstance(final ConfigReceiveAmqp config) throws ESBException
    {
        if (instance == null)
        {
            instance = new AmqpServer(config);
        }
        return instance;
    }

    public boolean isActive()
    {
        return isActive;
    }

    public void setActive(boolean isActive)
    {
        this.isActive = isActive;
    }

    /*
     * RabbitMQ Initialization
     */
    public void init() throws ESBException
    {
        // initialize AMQP Driver
        Log.log.info("Initializing AMQP service");
        driver = new AmqpDriver(config);
    } // init

    public void start() throws ESBException
    {
        if (connection.isOpen())
        {
            this.isActive = true;
            //initialize the connection checker thread.
            ConnectionChecker connectionChecker = new ConnectionChecker(5);
            connectionChecker.start();
        }
        else
        {
            Log.log.error("Could not open connection to AMQP server");
            throw new ESBException("Could not open connection to AMQP server");
        }
    } // start

    public void start(boolean isActive) throws ESBException
    {
        this.isActive = isActive;
        if (isActive && !connection.isOpen())
        {
            Log.log.error("Could not open connection to AMQP server");
            throw new ESBException("Could not open connection to AMQP server");
        }
    } // start

    public void stop() throws ESBException
    {
        try
        {
            channel.close();
            connection.close();
        }
        catch (IOException e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        catch (/*Timeout*/Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    } // stop

    /*
     * This method is called by the ConnectionChecker thread if the connection
     * is broken for some reason.
     */
    public void restart() throws ESBException
    {
        try
        {
            // reconnect
            connection = driver.getConnection();
            // start connection
            if (connection == null || !connection.isOpen())
            {
                Log.log.warn("Connection to the AMQP service is down, waiting and then retrying.");
                Thread.sleep(5000L);
            }
            else
            {
                channel = createChannel();

                //it's every individual publisher's duty to get the broken channel itself.
                //you may check MAmqpSender.sendMessage() method in MAmqpSender class for an idea.
                publishers.clear();

                //it's every individual listener's duty to reconnect itself.
                //at this point we'll simply remove the listeners.
                //check the MAmqpListener.handleShutdownSignal() method.
                listeners.clear();
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            // throw new ESBException(e.getMessage(), e);
            try
            {
                Thread.sleep(5000L);
            }
            catch (InterruptedException e1)
            {
                // ignore
            }
        }
    } // restart

    public void close()
    {
        try
        {
            if (connection != null)
            {
                connection.close();
                connection = null;
            }
            driver.close();
        }
        catch (Exception e)
        {
            Log.log.warn("Close connection. " + e.getMessage(), e);
        }
    } // close

    public AmqpDriver getDriver()
    {
        return driver;
    }

    public void setDriver(final AmqpDriver driver)
    {
        this.driver = driver;
    }

    public Channel createChannel() throws ESBException
    {
        try
        {
            return getConnection().createChannel();
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
    }

    public Connection getConnection() throws ESBException
    {
        if (connection == null || !connection.isOpen())
        {
            connection = driver.getConnection();
        }
        return connection;
    }

    public void setConnection(final Connection connection)
    {
        this.connection = connection;
    }

    public Map<String, String> getPublications()
    {
        return publications;
    }

    public boolean subscribePublication(final String topicName, final AmqpListener mAmqpListener)
    {
        boolean result = false;

        // subscribe
        Log.log.info("Registering subscriber " + mAmqpListener.getListenerId() + " to the Exchange " + topicName);

        try
        {
            Channel receiveChannel = mAmqpListener.getReceiveChannel();
            getDriver().subscribe(receiveChannel, topicName, mAmqpListener.getListenerId());

            createQueueConsumer(mAmqpListener.getListenerId(), mAmqpListener, true);
            
            addListener(topicName, mAmqpListener);
        }
        catch (ESBException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    } // subscribePublication

    public boolean unsubscribePublication(final String subscriberId)
    {
        boolean result = false;

        try
        {
            AmqpListener subscriber = listeners.get(subscriberId);
            if (subscriber != null)
            {
                subscriber.close();
                listeners.remove(subscriberId);
            }
            result = true;
        }
        catch (Exception e)
        {
            Log.log.warn("Failed to unsubscribe publication: " + e.getMessage(), e);
        }

        return result;
    } // unsubscribePublication

    protected void addListener(String queueName, AmqpListener listener)
    {
        listeners.put(queueName, listener);
    } // addListener

    public Map<String, AmqpListener> getListeners()
    {
        return listeners;
    }

    public synchronized void createQueueConsumer(final String queueName, final AmqpListener listener)
    {
        createQueueConsumer(queueName, listener, false);
    }
        
    public synchronized void createQueueConsumer(final String queueName, final AmqpListener mAmqpListener, boolean isPublication)
    {
        try
        {
            {
                Log.log.info("Creating AMQP consumer " + mAmqpListener.getListenerId() + " to the Queue " + mAmqpListener.getReceiveQueueName());
                //getDriver().createQueue(mAmqpListener.getReceiveChannel(), queueName);
                boolean autoAck = false;
                mAmqpListener.getReceiveChannel().basicConsume(queueName, autoAck, mAmqpListener.getListenerId(), (Consumer) mAmqpListener);
                
                if (!isPublication)
                {
                    addListener(queueName, mAmqpListener);
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Failed to initialize consumers: " + e.getMessage(), e);
        }
    } // createConsumers

    public synchronized void closeQueueConsumer(final String consumerId)
    {
        try
        {
            // close queue consumer
            if (listeners.containsKey(consumerId))
            {
                AmqpListener mAmqpListener = listeners.get(consumerId);
                Log.log.info("Closing AMQP consumers: " + mAmqpListener.getReceiveQueueName() + " hash: " + mAmqpListener.hashCode());
                mAmqpListener.close();
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    } // closeConsumers

    protected void addPublisher(String name, AmqpSender sender)
    {
        publishers.put(name, sender);
    } // addPublisher

    public boolean createPublisher(final String topicName, final String publisherId, final AmqpSender publisher)
    {
        boolean result = false;

        try
        {
            Log.log.debug("Creating publisher " + publisherId);
            AmqpSender mAmqpSender = publisher;
            mAmqpSender.setSendChannel(createChannel());
            addPublisher(publisherId, publisher);
            result = true;
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
        }

        return result;
    } // createPublication

    public boolean removePublisher(final String publisherId)
    {
        boolean result = true;

        try
        {
            if (publishers.containsKey(publisherId))
            {
                AmqpSender mAmqpSender = publishers.get(publisherId);
                if (mAmqpSender != null)
                {
                    mAmqpSender.close();
                    publishers.remove(publisherId);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
            result = false;
        }

        return result;
    } // removePublication

    public AmqpSender createSender() throws ESBException
    {
        return new AmqpSender(this, config);
    }

    /**
     * Creates a listenr to a Queue.
     * 
     * @param queueName
     * @return
     * @throws ESBException
     */
    public AmqpListener createListener(String queueName) throws ESBException
    {
        return new AmqpListener(this, queueName);
    }

    /**
     * Creates a lister to an Exchange.
     * 
     * @param exchangeName
     * @return
     * @throws ESBException
     */
    public AmqpListener createSubscriber(String exchangeName) throws ESBException
    {
        return new AmqpListener(this, exchangeName, true);
    }

    /**
     * A tiny little Thread class that checks the connectivity and in case it
     * is not open it retries.
     */
    private class ConnectionChecker extends Thread
    {
        private final int waitInSeconds;

        public ConnectionChecker(final int waitInSeconds)
        {
            this.waitInSeconds = waitInSeconds;
        }

        @Override
        public void run()
        {
            while (true)
            {
                try
                {
                    Thread.sleep(this.waitInSeconds * 1000);
                    if (!getConnection().isOpen())
                    {
                        restart();
                    }
                }
                catch (ESBException e)
                {
                    Log.log.warn(e.getMessage());
                }
                catch (InterruptedException e)
                {
                    // ignore is safe
                }
            }
        }
    }
} // AmqpServer
