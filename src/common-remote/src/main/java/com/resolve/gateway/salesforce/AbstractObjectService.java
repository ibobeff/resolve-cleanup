/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.salesforce;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsremote.ConfigReceiveSalesforce;
import com.resolve.util.SoapUtil;
import com.resolve.util.StringUtils;

/**
 * Abstract class for all the Salesforce object service provider classes.
 * 
 */
public abstract class AbstractObjectService implements ObjectService
{
    protected final ConfigReceiveSalesforce configurations;
    protected final SoapCaller soapCaller;

    protected static final String OBJECT_CLASS = "class";

    public AbstractObjectService(ConfigReceiveSalesforce configurations)
    {
        String finalUrl = configurations.getUrl() + "/" + getIdentity() + ".do?SOAP";
        soapCaller = new SoapCaller(configurations, finalUrl, configurations.getHttpbasicauthusername(), configurations.getHttpbasicauthpassword());
        this.configurations = configurations;
    }

    abstract protected String getSoapEnvelope(String sessionId, String operation, Map<String, String> params);

    protected boolean validateObjectId(String objectId) throws Exception
    {
        if (StringUtils.isEmpty(objectId))
        {
            throw new Exception("ID must be provided");
        }
        else
        {
            return true;
        }
    }

    private Map<String, String> unmarshalObject(String xmlContent, String elementTagName) throws Exception
    {
        Map<String, String> result = new HashMap<String, String>();
        if (!StringUtils.isEmpty(xmlContent))
        {
            result = SoapUtil.getData(xmlContent, elementTagName);
        }
        return result;
    }

    // ////////////////////////////////////////////////////////////////////
    // Method: create //
    // //
    // Description: This method gets a create case envelope and sends //
    // it using the soapCaller object. It returns a map of values //
    // and keys converted from the soap envelope that is returned //
    // by salesforce. //
    // //
    // //
    // Inputs: objectProperties: The qualities of the case you //
    // are creating //
    // username: your username for salesforce login, if none //
    // is provided then the config will be used //
    // password: your password including security token, //
    // appended to the standard password, if none //
    // is provided then the config will be used //
    // Outputs: response: The response received from Salesforce broken//
    // into keys and values. //
    // //
    // ////////////////////////////////////////////////////////////////////
    @Override
    public Map<String, String> create(Map<String, String> objectProperties, String username, String password) throws Exception
    {
        String response = null;

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = configurations.getHttpbasicauthusername();
            password = configurations.getHttpbasicauthpassword();
        }
        if (!username.equals(soapCaller.getHttpbasicauthusername()) || !password.equals(soapCaller.getHttpbasicauthpassword()))
        {
            soapCaller.setHttpbasicauthusername(username);
            soapCaller.setHttpbasicauthpassword(password);
            soapCaller.loginToSalesforce(username, password);
        }
        else
        {
            soapCaller.testConnectionAndLoginIfNecessary();
        }

        String sessionId = soapCaller.getSessionId();

        String soapEnvelope = getSoapEnvelope(sessionId, "create", objectProperties);

        response = soapCaller.sendSoapRequest(soapEnvelope);

        return unmarshalObject(response, "result");
    }

    // ////////////////////////////////////////////////////////////////////
    // Method: createWorknote //
    // //
    // Description: This method gets a create Worknote (case comment) //
    // envelope and sends it using the soapCaller object. It //
    // returns a map of values and keys converted from the soap //
    // envelope that is returned by salesforce. //
    // //
    // //
    // Inputs: objectProperties: The qualities of the comment you //
    // are creating //
    // username: your username for salesforce login, if none //
    // is provided then the config will be used //
    // password: your password including security token, //
    // appended to the standard password, if none //
    // is provided then the config will be used //
    // Outputs: response: The response received from Salesforce broken//
    // into keys and values. //
    // //
    // ////////////////////////////////////////////////////////////////////
    @Override
    public boolean createWorknote(String parentId, String note, String username, String password) throws Exception
    {
        boolean result = true;

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = configurations.getHttpbasicauthusername();
            password = configurations.getHttpbasicauthpassword();
        }
        if (!username.equals(soapCaller.getHttpbasicauthusername()) || !password.equals(soapCaller.getHttpbasicauthpassword()))
        {
            soapCaller.setHttpbasicauthusername(username);
            soapCaller.setHttpbasicauthpassword(password);
            soapCaller.loginToSalesforce(username, password);
        }
        else
        {
            soapCaller.testConnectionAndLoginIfNecessary();
        }

        Map<String, String> objectProperties = new HashMap<String, String>();
        objectProperties.put("ParentId", parentId);
        objectProperties.put("CommentBody", note);

        String sessionId = soapCaller.getSessionId();
        // update(parentId, objectProperties, username, password);

        String soapEnvelope = getSoapEnvelope(sessionId, "casecomment", objectProperties);
        soapCaller.sendSoapRequest(soapEnvelope);

        return result;
    }

    // ////////////////////////////////////////////////////////////////////
    // Method: update //
    // //
    // Description: This method gets a case update envelope and sends it//
    // using the soapCaller object. It returns a map of values //
    // and keys converted from the soap envelope that is returned //
    // by salesforce. //
    // //
    // //
    // Inputs: objectId: The id of the object you are updating. //
    // objectProperties: The qualities of the update you are //
    // making to the case //
    // username: your username for salesforce login, if none //
    // is provided then the config will be used //
    // password: your password including security token, //
    // appended to the standard password, if none //
    // is provided then the config will be used //
    // Outputs: response: The response received from Salesforce broken//
    // into keys and values. //
    // //
    // ////////////////////////////////////////////////////////////////////
    @Override
    public void update(String objectId, Map<String, String> objectProperties, String username, String password) throws Exception
    {

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = configurations.getHttpbasicauthusername();
            password = configurations.getHttpbasicauthpassword();
        }
        if (!username.equals(soapCaller.getHttpbasicauthusername()) || !password.equals(soapCaller.getHttpbasicauthpassword()))
        {
            soapCaller.setHttpbasicauthusername(username);
            soapCaller.setHttpbasicauthpassword(password);
            soapCaller.loginToSalesforce(username, password);
        }
        else
        {
            soapCaller.testConnectionAndLoginIfNecessary();
        }

        String sessionId = soapCaller.getSessionId();
        objectProperties.put("Id", objectId);
        String soapEnvelope = getSoapEnvelope(sessionId, "update", objectProperties);

        soapCaller.sendSoapRequest(soapEnvelope);
    }

    // ////////////////////////////////////////////////////////////////////
    // Method: delete //
    // //
    // Description: This method gets a case delete envelope and sends it//
    // using the soapCaller object. It returns a map of values //
    // and keys converted from the soap envelope that is returned //
    // by salesforce. //
    // //
    // Inputs: objectId: The id of the case you are deleting //
    // username: your username for salesforce login, if none //
    // is provided then the config will be used //
    // password: your password including security token, //
    // appended to the standard password, if none //
    // is provided then the config will be used //
    // Outputs: response: The response received from Salesforce broken//
    // into keys and values. //
    // //
    // ////////////////////////////////////////////////////////////////////
    @Override
    public void delete(String objectId, String username, String password) throws Exception
    {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = configurations.getHttpbasicauthusername();
            password = configurations.getHttpbasicauthpassword();
        }
        if (!username.equals(soapCaller.getHttpbasicauthusername()) || !password.equals(soapCaller.getHttpbasicauthpassword()))
        {
            soapCaller.setHttpbasicauthusername(username);
            soapCaller.setHttpbasicauthpassword(password);
            soapCaller.loginToSalesforce(username, password);
        }
        else
        {
            soapCaller.testConnectionAndLoginIfNecessary();
        }

        String sessionId = soapCaller.getSessionId();

        Map<String, String> objectProperties = new HashMap<String, String>();
        objectProperties.put("ids", objectId);

        String soapEnvelope = getSoapEnvelope(sessionId, "delete", objectProperties);

        soapCaller.sendSoapRequest(soapEnvelope);
    }

    // ////////////////////////////////////////////////////////////////////
    // Method: selectByID //
    // //
    // Description: This method gets a case query envelope and sends it //
    // using the soapCaller object. It returns a map of values //
    // and keys converted from the soap envelope that is returned //
    // by salesforce. //
    // Qualities selected: Id CaseNumber, Subject, //
    // Description, Status, and Priority //
    // //
    // Inputs: objectId: The Id of the item you are selecting //
    // username: your username for salesforce login, if none //
    // is provided then the config will be used //
    // password: your password including security token, //
    // appended to the standard password, if none //
    // is provided then the config will be used //
    // Outputs: response: The response received from Salesforce broken//
    // into keys and values. //
    // //
    // ////////////////////////////////////////////////////////////////////
    @Override
    public Map<String, String> selectByID(String objectId, String username, String password) throws Exception
    {
        String response = null;

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = configurations.getHttpbasicauthusername();
            password = configurations.getHttpbasicauthpassword();
        }
        if (!username.equals(soapCaller.getHttpbasicauthusername()) || !password.equals(soapCaller.getHttpbasicauthpassword()))
        {
            soapCaller.setHttpbasicauthusername(username);
            soapCaller.setHttpbasicauthpassword(password);
            soapCaller.loginToSalesforce(username, password);
        }
        else
        {
            soapCaller.testConnectionAndLoginIfNecessary();
        }

        String sessionId = soapCaller.getSessionId();

        Map<String, String> objectProperties = new HashMap<String, String>();
        objectProperties.put("queryString", " SELECT Id, CaseNumber, Subject, Description, Priority, Status from Case where Id = \'" + objectId + "\'");
        String soapEnvelope = getSoapEnvelope(sessionId, "query", objectProperties);

        response = soapCaller.sendSoapRequest(soapEnvelope);

        return unmarshalObject(response, "records");
    }

    // ////////////////////////////////////////////////////////////////////
    // Method: selectByNumber //
    // //
    // Description: This method gets a case query envelope and sends it //
    // using the soapCaller object. It returns a map of values //
    // and keys converted from the soap envelope that is returned //
    // by salesforce. //
    // Qualities selected: Id CaseNumber, Subject, //
    // Description, Status, and Priority //
    // //
    // Inputs: number: The number of the item you are selecting //
    // username: your username for salesforce login, if none //
    // is provided then the config will be used //
    // password: your password including security token, //
    // appended to the standard password, if none //
    // is provided then the config will be used //
    // Outputs: response: The response received from Salesforce broken//
    // into keys and values. //
    // //
    // ////////////////////////////////////////////////////////////////////
    @Override
    public Map<String, String> selectByNumber(String number, String username, String password) throws Exception
    {
        String response = null;

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = configurations.getHttpbasicauthusername();
            password = configurations.getHttpbasicauthpassword();
        }
        if (!username.equals(soapCaller.getHttpbasicauthusername()) || !password.equals(soapCaller.getHttpbasicauthpassword()))
        {
            soapCaller.setHttpbasicauthusername(username);
            soapCaller.setHttpbasicauthpassword(password);
            soapCaller.loginToSalesforce(username, password);
        }
        else
        {
            soapCaller.testConnectionAndLoginIfNecessary();
        }

        String sessionId = soapCaller.getSessionId();

        Map<String, String> objectProperties = new HashMap<String, String>();
        objectProperties.put("queryString", " SELECT Id, CaseNumber, Subject, Description, Priority, Status from Case where CaseNumber = \'" + number + "\'");
        String soapEnvelope = getSoapEnvelope(sessionId, "query", objectProperties);

        response = soapCaller.sendSoapRequest(soapEnvelope);

        return unmarshalObject(response, "records");
    }

    // ////////////////////////////////////////////////////////////////////
    // Method: search //
    // //
    // Description: This method gets a case query envelope and sends it //
    // using the soapCaller object. It returns a map of values //
    // and keys converted from the soap envelope that is returned //
    // by salesforce. //
    // Qualities selected: Id CaseNumber, Subject, //
    // Description, Status, and Priority //
    // //
    // Inputs: query: a properly formatted salesforce query provided //
    // by the user //
    // orderBy: What trait you want your results ranked by //
    // isAscending: boolean representing rank up or down //
    // username: your username for salesforce login, if none //
    // is provided then the config will be used //
    // password: your password including security token, //
    // appended to the standard password, if none //
    // is provided then the config will be used //
    // Outputs: response: The response received from Salesforce broken//
    // into keys and values. //
    // //
    // ////////////////////////////////////////////////////////////////////
    @Override
    public List<Map<String, String>> search(String query, String orderBy, boolean isAscending, String username, String password) throws Exception
    {
        String response = null;

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = configurations.getHttpbasicauthusername();
            password = configurations.getHttpbasicauthpassword();
        }
        if (!username.equals(soapCaller.getHttpbasicauthusername()) || !password.equals(soapCaller.getHttpbasicauthpassword()))
        {
            soapCaller.setHttpbasicauthusername(username);
            soapCaller.setHttpbasicauthpassword(password);
            soapCaller.loginToSalesforce(username, password);
        }
        else
        {
            soapCaller.testConnectionAndLoginIfNecessary();
        }
        Map<String, String> objectProperties = new HashMap<String, String>();

        if (StringUtils.isNotEmpty(orderBy))
        {
            if (isAscending)
            {
                query += " ORDER BY " + orderBy + " ASC";
            }
            else
            {
                query += " ORDER BY " + orderBy + " DESC";
            }
        }
        objectProperties.put("queryString", query);
        String sessionId = soapCaller.getSessionId();
        String soapEnvelope = getSoapEnvelope(sessionId, "query", objectProperties);

        response = soapCaller.sendSoapRequest(soapEnvelope);
        return SoapUtil.getList(response, "records");
    }
}
