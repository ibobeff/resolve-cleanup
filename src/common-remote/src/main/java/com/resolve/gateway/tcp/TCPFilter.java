/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.tcp;

import com.resolve.gateway.BaseFilter;
import com.resolve.util.StringUtils;

public class TCPFilter extends BaseFilter
{
    // Additional field names for this filter.
    public static final String PORT = "PORT";
    public static final String BEGIN_SEPARATOR = "BEGIN_SEPARATOR";
    public static final String END_SEPARATOR = "END_SEPARATOR";
    public static final String INCLUDE_BEGIN_SEPARATOR = "INCLUDE_BEGIN_SEPARATOR";
    public static final String INCLUDE_END_SEPARATOR = "INCLUDE_END_SEPARATOR";

    private Integer port;
    //indicates the end of stream so tha socket listener process the content. 
    private String beginSeparator;
    private String endSeparator;
    private Boolean includeBeginSeparator;
    private Boolean includeEndSeparator;

    public TCPFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String port, String beginSeparator, String endSeparator, String includeBeginSeparator, String includeEndSeparator)
    {
        super(id, active, order, interval, eventEventId, runbook, script);
        
        if(StringUtils.isNumeric(port))
        {
            this.port = Integer.parseInt(port);
        }
        setBeginSeparator(beginSeparator);
        setEndSeparator(endSeparator);
        this.includeBeginSeparator = StringUtils.isTrue(includeBeginSeparator);
        this.includeEndSeparator = StringUtils.isTrue(includeEndSeparator);
    } // TCPFilter

    public Integer getPort()
    {
        return port;
    }

    public void setPort(Integer port)
    {
        this.port = port;
    }

    public String getBeginSeparator()
    {
        return beginSeparator;
    }

    public void setBeginSeparator(String beginSeparator)
    {
        this.beginSeparator = beginSeparator != null ? beginSeparator.trim() : beginSeparator;
    }

    public String getEndSeparator()
    {
        return endSeparator;
    }

    public void setEndSeparator(String endSeparator)
    {
        this.endSeparator = endSeparator != null ? endSeparator.trim() : endSeparator;
    }

    public Boolean getIncludeBeginSeparator()
    {
        return includeBeginSeparator;
    }

    public void setIncludeBeginSeparator(Boolean includeBeginSeparator)
    {
        this.includeBeginSeparator = includeBeginSeparator;
    }

    public Boolean getIncludeEndSeparator()
    {
        return includeEndSeparator;
    }

    public void setIncludeEndSeparator(Boolean includeEndSeparator)
    {
        this.includeEndSeparator = includeEndSeparator;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((beginSeparator == null) ? 0 : beginSeparator.hashCode());
        result = prime * result + ((endSeparator == null) ? 0 : endSeparator.hashCode());
        result = prime * result + ((includeBeginSeparator == null) ? 0 : includeBeginSeparator.hashCode());
        result = prime * result + ((includeEndSeparator == null) ? 0 : includeEndSeparator.hashCode());
        result = prime * result + ((port == null) ? 0 : port.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        TCPFilter other = (TCPFilter) obj;
        if (beginSeparator == null)
        {
            if (other.beginSeparator != null) return false;
        }
        else if (!beginSeparator.equals(other.beginSeparator)) return false;
        if (endSeparator == null)
        {
            if (other.endSeparator != null) return false;
        }
        else if (!endSeparator.equals(other.endSeparator)) return false;
        if (includeBeginSeparator == null)
        {
            if (other.includeBeginSeparator != null) return false;
        }
        else if (!includeBeginSeparator.equals(other.includeBeginSeparator)) return false;
        if (includeEndSeparator == null)
        {
            if (other.includeEndSeparator != null) return false;
        }
        else if (!includeEndSeparator.equals(other.includeEndSeparator)) return false;
        if (port == null)
        {
            if (other.port != null) return false;
        }
        else if (!port.equals(other.port)) return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "TCPFilter [port=" + port + ", beginSeparator=" + beginSeparator + ", endSeparator=" + endSeparator + ", includeBeginSeparator=" + includeBeginSeparator + ", includeEndSeparator=" + includeEndSeparator + ", id=" + id + ", active=" + active + ", order=" + order + ", interval=" + interval + ", eventEventId=" + eventEventId + ", runbook=" + runbook + ", script=" + script + "]";
    }
}
