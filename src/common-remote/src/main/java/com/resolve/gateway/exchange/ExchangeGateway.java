/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.exchange;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.moonrug.exchange.AttachmentSource;
import com.moonrug.exchange.AttachmentType;
import com.moonrug.exchange.DefaultFolder;
import com.moonrug.exchange.EmailInfo;
import com.moonrug.exchange.IAttachment;
import com.moonrug.exchange.IContentItem;
import com.moonrug.exchange.IEwsSession;
import com.moonrug.exchange.IFolder;
import com.moonrug.exchange.IMessage;
import com.moonrug.exchange.NamedProperty;
import com.moonrug.exchange.Property;
import com.moonrug.exchange.Recipient;
import com.moonrug.exchange.Session;
import com.moonrug.exchange.internal.Message;
import com.moonrug.exchange.search.AndFilter;
import com.moonrug.exchange.search.Filter;
import com.moonrug.exchange.search.StringFilter;
import com.moonrug.exchange.search.ValueFilter;
import com.moonrug.exchange.search.ValueFilter.Operation;
import com.resolve.esb.MListener;
import com.resolve.esb.MMsgHeader;
import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.MExchange;
import com.resolve.gateway.NameProperties;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.util.FileUtils;
import com.resolve.rsremote.ConfigReceiveExchange;
import com.resolve.util.Constants;
import com.resolve.util.GatewayEvent;
import com.resolve.util.Log;
import com.resolve.util.MessageDispatcher;
import com.resolve.util.StringUtils;

import groovy.lang.Binding;

public class ExchangeGateway extends BaseClusteredGateway
{
    // Singleton
    private static volatile ExchangeGateway instance = null;
    
    final static int STATE_PASSIVE_CONNECT = 0;
    final static int STATE_ACTIVE_PRIMARY = 1;
    final static int STATE_ACTIVE_BACKUP = 2;

    final static int MAX_RETRY = 3;
    final static int IMPACT_CLASS = 10500;
    final static String EXCHANGE = "EXCHANGE";
    final static MMsgHeader headerHeader = new MMsgHeader();
    // final static MMsgHeader heartbeatHeader = new MMsgHeader();

    public static NameProperties nameProperties = new NameProperties();

    public static int UID = 0;
    public static String queue = "EXCHANGE";
    public static String topic = "EXCHANGE_TOPIC";
    public static boolean debug = false;
    public static ConcurrentLinkedQueue writeQueue = new ConcurrentLinkedQueue();
    public static BlockingQueue<Map<String, Object>> updateQueue = new LinkedBlockingQueue();
    public static boolean active = true;
    public static boolean primary = true;
    public static boolean ewsmode = true;
    public static boolean certs = true;
    public static String ewsurl = "";
    public static String version;

    public static int state = STATE_PASSIVE_CONNECT;
    public static int maxAttachmentSize = 0;
    public static int interval = 10000;
    public static int retryDelay = 5000;
    public static int reconnectDelay = 60000;
    public static boolean hasBackupOS = false;
    public static long GLOBAL_SEND_TIMEOUT = 100; // milliseconds

    MListener mListener = null;

    boolean running = false;
    boolean updateThreadRunning = false;
    boolean connChanged = true;
    boolean hostChanged = true;
    Thread thread;

    private static Connection conn;
    private static Session session;
    private static Session sessionUpdate;
    // private static IFolder folder;
    String url;
    String url2;
    String lastUrl;
    String host;
    String lastHost;
    String username;
    String password;
    String domain;
    String mailbox;
    String mapiClientVersion;
    long lastSerial;
    boolean reinitTS;
    long initTS;
    long diffTS;
    Queue<GatewayEvent<String, Object>> localEventQueue = new LinkedList<GatewayEvent<String, Object>>();
    static long eventCounter;
    long startTimeForUpdate = 0l;
    long startTimeForPoll = 0l;

    private final static Map<String, Property> FILTER_MAP = new HashMap<>();

    private ConfigReceiveExchange config;
    
    static
    {
        FILTER_MAP.put("subject", Message.SUBJECT);
        FILTER_MAP.put("content", Message.BODY);
        FILTER_MAP.put("senderaddress", Message.SENT_REPRESENTING_ADDRESS);
        FILTER_MAP.put("sendername", Message.SENT_REPRESENTING_NAME);
    }

    private final static Property[] props = new Property[6];

    static
    {
        props[0] = IContentItem.SUBJECT;
        props[1] = IContentItem.MESSAGE_CLASS;
        props[2] = IContentItem.SENT_REPRESENTING_NAME;
        props[3] = new NamedProperty("internet-from", NamedProperty.INTERNET_HEADERS, "from", Property.Type.STRING);
        props[4] = IContentItem.HAS_ATTACH;
        // props[5] = IContentItem.BODY;
        props[5] = IContentItem.SENT_REPRESENTING_ADDRESS;
    }

    public static ExchangeGateway getInstance(ConfigReceiveExchange config)
    {
        if (instance == null)
        {
            instance = new ExchangeGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static ExchangeGateway getInstance()
    {
        if (instance == null)
        {
            throw new IllegalStateException("Exchange Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }
    
    private ExchangeGateway(ConfigReceiveExchange config)
    {
        super(config);
        this.config = config;
        queue = config.getQueue();
    }
    
    @Override
    protected void initialize()
    {
        try
        {
            Log.log.info("Initializing ExchangeListener");

            // init settings
            queue = config.getQueue().toUpperCase();
            topic = queue + "_TOPIC";
            // debug = config.isDebug();
            retryDelay = config.getRetryDelay() * 1000;
            reconnectDelay = config.getReconnectDelay() * 1000;
            maxAttachmentSize = config.getMaxAttachmentSize();
            ewsmode = config.getEwsmode();
            certs = config.getAcceptAllCerts();
            ewsurl = config.getEwsurl();
            this.gatewayConfigDir = "/config/exchange/";

            // init message header
            // headerHeader.put(Constants.ESB_PARAM_EVENTTYPE, EXCHANGE);
            headerHeader.setEventType(EXCHANGE);
            headerHeader.setRouteDest(Constants.ESB_NAME_EXECUTEQUEUE, "MEvent.eventResult");

            this.username = config.getUsername();
            this.password = config.getP_assword();
            this.host = config.getHost();
            this.domain = config.getDomain();
            this.mailbox = config.getMailbox();
            this.mapiClientVersion = config.getMapiClientVersion();
            this.lastSerial = 0;
            this.connChanged = true;

            this.thread = new Thread(this);

            // add listener (EXCHANGE)
            Log.log.info("  Initializing Exchange ESB Listener");
            //mListener = MListenerFactory.getMListener(MainBase.main.mServer, queue, "com.resolve.gateway");
            mListener = MainBase.getESB().getMServer().createListener(queue, "com.resolve.gateway");
            mListener.init(false);
            if (!active)
            {
                Log.log.info("Closing queue consumer for EXCHANGE (backup)");
                MainBase.getESB().getMServer().closeQueueConsumer(mListener.getListenerId());
            }

            // init topic
            MainBase.main.mServer.createPublication(topic);
            MainBase.main.mServer.subscribePublication(topic);

            // init handlers
            MainBase.main.mServer.addHandler("MExchange", MExchange.class);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    }


    @Override
    public com.resolve.gateway.Filter getFilter(Map<String, Object> params)
    {
        Map<String, String> criteria = StringUtils.stringToMap((String) params.get(ExchangeFilter.CRITERIA));
        return new ExchangeFilter((String) params.get(ExchangeFilter.ID), 
                        (String) params.get(ExchangeFilter.ACTIVE), 
                        (String) params.get(ExchangeFilter.ORDER), 
                        (String) params.get(ExchangeFilter.INTERVAL), 
                        (String) params.get(ExchangeFilter.EVENT_EVENTID), 
                        (String) params.get(ExchangeFilter.RUNBOOK), 
                        (String) params.get(ExchangeFilter.SCRIPT), 
                        (String) params.get(ExchangeFilter.SUBJECT), 
                        (String) params.get(ExchangeFilter.CONTENT), 
                        (String) params.get(ExchangeFilter.SENDERNAME), 
                        (String) params.get(ExchangeFilter.SENDERADDRESS), 
                        criteria);
    }

    @Override
    public String getLicenseCode()
    {
        return "EXCHANGE";
    }

    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
    }

    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_EXCHANGE;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MExchange.class.getSimpleName();
    }

    @Override
    protected Class getMessageHandlerClass()
    {
        return MExchange.class;
    }

    public Map getFilters()
    {
        return filters;
    } // getFilters

    public boolean connect()
    {
        boolean result = false;

        Log.log.info("Current state: " + getStateString());
        if (running && active)
        {
            // try connecting to Exchange Server
            Log.log.info("Attempting to connect with Exchange Server");
            if (connectExchangeServer())
            {
                state = STATE_ACTIVE_PRIMARY;
                Log.log.info("New state: " + getStateString());

                result = true;
            }
            else
            {
                result = false;
            }
        }

        return result;
    }

    public boolean reconnectForUpdate()
    {
        boolean result = false;

        Log.log.info("Current state: " + getStateString());
        if (running && active)
        {
            // try connecting to Exchange Server
            Log.log.info("Attempting to connect with Exchange Server");
            if (reconnectExchangeServerForUpdate())
            {
                state = STATE_ACTIVE_PRIMARY;
                Log.log.info("New state: " + getStateString());

                result = true;
            }
            else
            {
                result = false;
            }
        }

        return result;
    }

    public boolean reconnectForPoll()
    {
        boolean result = false;

        Log.log.info("Current state: " + getStateString());
        if (running && active)
        {
            // try connecting to Exchange Server
            Log.log.info("Attempting to connect with Exchange Server");
            if (reconnectExchangeServerForPoll())
            {
                state = STATE_ACTIVE_PRIMARY;
                Log.log.info("New state: " + getStateString());

                result = true;
            }
            else
            {
                result = false;
            }
        }

        return result;
    }

    public String getStateString()
    {
        String result = "";

        switch (state)
        {
            case STATE_PASSIVE_CONNECT:
                result = "PASSIVE_CONNECT";
                break;
            case STATE_ACTIVE_PRIMARY:
                result = "ACTIVE_PRIMARY";
                break;
            case STATE_ACTIVE_BACKUP:
                result = "ACTIVE_BACKUP";
                break;
        }

        return result;
    } // getStateString

    private void closeSession(Session session)
    {
        if (session != null)
        {
            try
            {
                session.close();
            }
            catch (Throwable t)
            {
                Log.log.info("=== closeSession cacthed exception : " + t.getMessage());
            }
        }
    }

    private void closeSessionUpdate(Session session)
    {
        if (session != null)
        {
            try
            {
                session.close();
            }
            catch (Throwable t)
            {
                Log.log.info("=== closeSessionUpdate cacthed exception : " + t.getMessage());
            }
        }
    }

    private void closeConnection(Connection con)
    {
        if (con != null)
        {
            try
            {
                con.close();
            }
            catch (Throwable ex)
            {
            }
        }
    }

    public boolean connectExchangeServer()
    {
        boolean result = false;
        int retry = MAX_RETRY;
        Session newsession = null;
        Session newsessionupdate = null;

        Log.log.info("Connecting Exchange Server host: " + host);
        if (state == STATE_PASSIVE_CONNECT)
        {
            this.closeSession(session);
            this.closeSessionUpdate(sessionUpdate);
            session = null;
            sessionUpdate = null;
        }

        while (!result && retry > 0)
        {
            try
            {
                // get session
                Map<String, String> map = new HashMap<String, String>();
                // username = "CMtest";
                // password = "ENC1:Tw0lopleAJH9gatGr4vuOw==";

                // domain = "workgroup";
                // ewsurl = "https://wbexch01.workgroup.neoninc.com/ews/";

                map.put(Session.USERNAME, username);
                map.put(Session.PASSWORD, password);
                map.put(Session.DOMAIN, domain);

                // map.put(IEwsSession.ACCEPT_ALL_CERTS,
                // Boolean.valueOf(certs).toString());
                map.put(IEwsSession.ACCEPT_ALL_CERTS, "true");

                // ewsmode = true;
                if (ewsmode)
                {
                    map.put(Session.SERVER, ewsurl);
                    newsession = IEwsSession.Factory.create(map);
                    Object id = newsession.getId();
                }
                else
                {
                    map.put(Session.DOMAIN, domain);
                    map.put(Session.MAILBOX, mailbox);
                    map.put(Session.SERVER, host);
                    map.put("mapi.client_version", mapiClientVersion);
                    newsession = Session.create(map);
                }

                closeSession(session);
                Log.log.info("Connected to Exchange Server at host: " + host);
                this.session = newsession;

                if (ewsmode)
                {
                    newsessionupdate = IEwsSession.Factory.create(map);
                }
                else
                {
                    newsessionupdate = Session.create(map);
                }

                closeSessionUpdate(sessionUpdate);
                this.sessionUpdate = newsessionupdate;

                // set lastUrl and connChanged
                if (StringUtils.isEmpty(lastHost) || !lastHost.equals(host))
                {
                    this.hostChanged = true;
                }
                this.lastHost = host;

                result = true;

            }
            catch (Exception e)
            {
                try
                {
                    Log.log.warn("Unable to connect to Exchange Server: " + e.getMessage());
                    Thread.sleep(retryDelay);
                }
                catch (Exception e2)
                {
                }
                retry--;
            }
        }

        if (retry == 0)
        {
            Log.log.error("ABORTING: Failed to connect to Exchange Server at host: " + host);
        }

        return result;
    }

    public boolean reconnectExchangeServerForUpdate()
    {
        boolean result = false;
        int retry = MAX_RETRY;
        // Session newsession = null;
        Session newsessionupdate = null;

        Log.log.info("Connecting Exchange Server host: " + host);
        if (state == STATE_PASSIVE_CONNECT)
        {
            // this.closeSession(session);
            this.closeSessionUpdate(sessionUpdate);
            // session = null;
            sessionUpdate = null;
        }

        while (!result && retry > 0)
        {
            try
            {
                // get session
                Map<String, String> map = new HashMap<String, String>();
                map.put(Session.USERNAME, username);
                map.put(Session.PASSWORD, password);
                map.put(Session.DOMAIN, domain);
                // map.put(IEwsSession.ACCEPT_ALL_CERTS,
                // Boolean.valueOf(certs).toString());
                map.put(IEwsSession.ACCEPT_ALL_CERTS, "true");

                if (ewsmode)
                {
                    map.put(Session.SERVER, ewsurl);
                    // newsession = IEwsSession.Factory.create(map);
                }
                else
                {
                    map.put(Session.DOMAIN, domain);
                    map.put(Session.MAILBOX, mailbox);
                    map.put(Session.SERVER, host);
                    map.put("mapi.client_version", mapiClientVersion);
                    // newsession = Session.create(map);

                }

                // closeSession(session);
                // Log.log.info("Connected to Exchange Server at host: " +
                // host);
                // this.session = newsession;
                //
                if (ewsmode)
                {
                    newsessionupdate = IEwsSession.Factory.create(map);
                }
                else
                {
                    newsessionupdate = Session.create(map);
                }

                closeSessionUpdate(sessionUpdate);
                this.sessionUpdate = newsessionupdate;

                // set lastUrl and connChanged
                if (StringUtils.isEmpty(lastHost) || !lastHost.equals(host))
                {
                    this.hostChanged = true;
                }
                this.lastHost = host;

                result = true;
            }
            catch (Exception e)
            {
                try
                {
                    Log.log.warn("Unable to connect to Exchange Server: " + e.getMessage());
                    Thread.sleep(retryDelay);
                }
                catch (Exception e2)
                {
                }
                retry--;
            }
        }

        if (retry == 0)
        {
            Log.log.error("ABORTING: Failed to connect to Exchange Server at host: " + host);
        }

        return result;
    }

    public boolean reconnectExchangeServerForPoll()
    {
        boolean result = false;
        int retry = MAX_RETRY;
        Session newsession = null;
        // Session newsessionupdate = null;

        Log.log.info("Connecting Exchange Server host: " + host);
        if (state == STATE_PASSIVE_CONNECT)
        {
            this.closeSession(session);
            // this.closeSessionUpdate(sessionUpdate);
            session = null;
            // sessionUpdate = null;
        }

        while (!result && retry > 0)
        {
            try
            {
                // get session
                Map<String, String> map = new HashMap<String, String>();
                map.put(Session.USERNAME, username);
                map.put(Session.PASSWORD, password);
                map.put(Session.DOMAIN, domain);
                // map.put(IEwsSession.ACCEPT_ALL_CERTS,
                // Boolean.valueOf(certs).toString());
                map.put(IEwsSession.ACCEPT_ALL_CERTS, "true");

                if (ewsmode)
                {
                    map.put(Session.SERVER, ewsurl);
                    newsession = IEwsSession.Factory.create(map);
                }
                else
                {
                    map.put(Session.DOMAIN, domain);
                    map.put(Session.MAILBOX, mailbox);
                    map.put(Session.SERVER, host);
                    map.put("mapi.client_version", mapiClientVersion);
                    newsession = Session.create(map);

                }

                closeSession(session);
                Log.log.info("Connected to Exchange Server at host: " + host);
                this.session = newsession;

                // set lastUrl and connChanged
                if (StringUtils.isEmpty(lastHost) || !lastHost.equals(host))
                {
                    this.hostChanged = true;
                }
                this.lastHost = host;

                result = true;
            }
            catch (Exception e)
            {
                try
                {
                    Log.log.warn("Unable to connect to Exchange Server: " + e.getMessage());
                    Thread.sleep(retryDelay);
                }
                catch (Exception e2)
                {
                }
                retry--;
            }
        }

        if (retry == 0)
        {
            Log.log.error("ABORTING: Failed to connect to Exchange Server at host: " + host);
        }

        return result;
    }

    public void stop()
    {
        Log.log.warn("Stopping ExchangeListener");
        running = false;
        updateThreadRunning = false;

        try
        {
            disconnectSession();
            disconnectSessionUpdate();

            thread.stop();
        }
        catch (Exception e)
        {
            Log.log.warn("Unable to close session. " + e.getMessage());
        }
    } // stop

    public void disconnectSession()
    {
        try
        {
            if (session != null)
            {
                session.close();
            }
        }
        catch (Exception e)
        {
            Log.log.warn("Unable to close session. " + e.getMessage());
        }
    }

    public void disconnectSessionUpdate()
    {
        try
        {
            if (sessionUpdate != null)
            {
                sessionUpdate.close();
            }
        }
        catch (Exception e)
        {
            Log.log.warn("Unable to close sessionUpdate. " + e.getMessage());
        }
    }

    public void disconnect()
    {
        try
        {
            if (conn != null)
            {
                conn.close();
            }
        }
        catch (Exception e)
        {
            Log.log.warn("Unable to close connection. " + e.getMessage());
        }
    } // disconnect

    public static String getQueue()
    {
        return queue;
    } // getQueue

    public void run()
    {
        // This call is must for all the ClusteredGateways.
        super.sendSyncRequest();
        
        while (running)
        {
            Log.log.trace("state: " + getStateString() + " active: " + active);

            startTimeForPoll = System.currentTimeMillis();

            if (active)
            {
                try
                {
                    // session established
                    if (session != null)
                    {
                        try
                        {
                            synchronized (orderedFilters)
                            {
                                // check for new alerts
                                for (Iterator i = orderedFilters.iterator(); i.hasNext();)
                                {
                                    ExchangeFilter filter = (ExchangeFilter) i.next();

                                    if (filter != null && filter.isActive())
                                    {
                                        // init lastQuery
                                        if (filter.getLastQuery() == 0)
                                        {
                                            filter.setLastQuery(System.currentTimeMillis());
                                        }

                                        if (System.currentTimeMillis() - filter.getLastQuery() > filter.getInterval() * 1000)
                                        {
                                            // If localEventQueue still have new
                                            // events, try to send them to
                                            // global event queue.
                                            long now = System.currentTimeMillis();
                                            while (!localEventQueue.isEmpty())
                                            {
                                                GatewayEvent<String, Object> event = localEventQueue.peek();
                                                // check if events are timed out
                                                // before try to send it to
                                                // global queue.
                                                if ((MessageDispatcher.MAXIMUM_EVENT_WAITING_TIME == 0) || (now - event.timeStamp) < MessageDispatcher.MAXIMUM_EVENT_WAITING_TIME)
                                                {
                                                    boolean b = MessageDispatcher.sendMessage(headerHeader, event, GLOBAL_SEND_TIMEOUT);
                                                    if (b)
                                                    {
                                                        localEventQueue.poll();
                                                    }
                                                    // If failed to insert event
                                                    // into global event queue,
                                                    // bail out to avoid
                                                    // congestion and do
                                                    // something else.
                                                    else
                                                    {
                                                        break;
                                                    }
                                                }
                                                // Check if event is timed out
                                                // and should be dropped from
                                                // queue
                                                else if (MessageDispatcher.MAXIMUM_EVENT_WAITING_TIME != 0 && ((now - event.timeStamp) > MessageDispatcher.MAXIMUM_EVENT_WAITING_TIME))
                                                {
                                                    GatewayEvent<String, Object> evt = localEventQueue.poll();
                                                    Log.log.info("Event timed out at local event queue. Drop event: timestamp=" + evt.timeStamp + ", content=" + evt.content);

                                                }
                                            }

                                            Log.log.trace("Before poll: event counter = " + eventCounter + ", Message counter=" + MessageDispatcher.messageCounter);

                                            // if any remaining events are in
                                            // local event
                                            // queue, doesn't poll
                                            // ExchangeServer for more new
                                            // events.
                                            if (localEventQueue.isEmpty())
                                            {
                                                // poll and update
                                                // filter.lastQuery with
                                                // ExchangeServer timestamp
                                                // Synchronization is needed in
                                                // case conn is ued by
                                                // select(sql)
                                                synchronized (session)
                                                {
                                                    poll(filter);
                                                }

                                                filter.setLastQuery(System.currentTimeMillis());
                                            }

                                            Log.log.trace("After poll: event counter = " + eventCounter + ", Message counter=" + MessageDispatcher.messageCounter);

                                        }
                                    }
                                }

                            } // synchronized (filters)

                            // sleep if no update jobs
                            // if (writeQueue.size() == 0)
                            // {
                            Thread.sleep(interval);
                            // }
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage());
                            reconnectForPoll();
                        }
                    }

                    // reconnect
                    else
                    {
                        Thread.sleep(reconnectDelay);
                        Log.log.info("Reconnecting");
                        state = STATE_PASSIVE_CONNECT;
                        reconnectForPoll();
                    }
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage(), e);
                    Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", 
                              "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + 
                              "due to " + e.getMessage());
                    reconnectForPoll();
                }

                long duration = System.currentTimeMillis() - startTimeForPoll;

                if (duration > reconnectDelay)
                {
                    reconnectForPoll();
                }

                startTimeForPoll = System.currentTimeMillis();
            }
        }

        // disconnect
        disconnectSession();
        Log.log.warn("Stopped ExchangeListener");
    } // run

    private int poll(ExchangeFilter filter)
    {
        int result = 0;
        List<String> filterStringList = new ArrayList();
        List<Filter> filterList = new ArrayList();
        ByteArrayOutputStream baOutStream = new ByteArrayOutputStream();
        Filter filterLocal = null;
        Property property = null;
        IFolder folder = null;
        boolean haveResolveFilter = false;

        try
        {
            String runbook = filter.getRunbook();

            ValueFilter readFlagFilter = new ValueFilter(Message.READ_FLAG, Operation.EQUAL, false);
            filterList.add(readFlagFilter);

            // init criteria
            Map<String, String> criteriaMap = filter.getCriteria();
            Log.log.info("start poll the email from exchange server with criteriaMap: " + criteriaMap);

            for (Iterator itr = criteriaMap.entrySet().iterator(); itr.hasNext();)
            {
                Map.Entry entry = (Map.Entry) itr.next();
                String key = (String) entry.getKey();
                String value = (String) entry.getValue();

                if (value != null && !value.equals("") && key != null && !key.equals(""))
                {
                    filterStringList = getFilterString(value);
                    property = getProperty(key);

                    for (String filterString : filterStringList)
                    {
                        filterLocal = new StringFilter(property, filterString);
                        filterList.add(filterLocal);
                        haveResolveFilter = true;
                    }
                }
            }

            // if(!criteriaMap.isEmpty())
            // {
            Filter[] filters = (Filter[]) filterList.toArray(new Filter[0]);
            AndFilter andMultiFilter = new AndFilter(filters);

            folder = session.getStore().getInbox();

            Log.log.info("haveResolveFilter ===" + haveResolveFilter);

            if (folder != null)
            {
                if (haveResolveFilter)
                {
                    folder.setFilter(andMultiFilter);
                }
                else
                {
                    folder.setFilter(readFlagFilter);
                }

                Property internetFrom = new NamedProperty("internet-from", NamedProperty.INTERNET_HEADERS, "from", Property.Type.STRING);

                IContentItem[] items = folder.getItems(props);

                String senderName = "";
                String senderAddress = "";
                String from = "";

                for (IContentItem item : items)
                {
                    String itemID = item.getId();
                    try
                    {
                        IContentItem itemLocal = folder.getItem(itemID, props);

                        Message emailMsg = (Message) itemLocal;
                        Map<String, Object> content = new HashMap<String, Object>();

                        EmailInfo emailInfo = emailMsg.getSender();
                        senderName = (emailInfo != null) ? emailInfo.getName() : "";
                        senderAddress = (emailInfo != null) ? emailInfo.getAddress() : "";
                        // senderAddress = (String) item.get(internetFrom);

                        content.put("EMAILID", itemID);
                        content.put("SENDERNAME", senderName);
                        content.put("SENDERADDRESS", senderAddress);
                        content.put("FROM", senderAddress);
                        content.put("SUBJECT", (emailMsg.getSubject() != null) ? emailMsg.getSubject() : "");
                        content.put("CONTENT", (emailMsg.getBody() != null) ? emailMsg.getBody() : "");

                        IAttachment[] atts = emailMsg.getAttachments();
                        boolean hasAttachments = emailMsg.hasAttachments();

                        if (hasAttachments)
                        {
                            content.put("FROM", from);
                            content.put("ATTACHMENT_COUNT", Integer.toString(atts.length));
                            int iAttachment = 1;
                            int iAttachmentFile = 1;
                            long size = 0l;
                            boolean noAttachment = false;

                            for (IAttachment att : atts)
                            {
                                String name = att.getLongName();
                                AttachmentType attType = att.getType();

                                if (attType.equals(AttachmentType.BINARY) || attType.equals(AttachmentType.EMBEDDED))
                                {
                                    if (maxAttachmentSize == 0)
                                    {
                                        noAttachment = true;
                                        break;
                                    }
                                    else
                                    {
                                        baOutStream = new ByteArrayOutputStream();
                                        att.writeTo(baOutStream);
                                        byte[] byteArrayLocal = baOutStream.toByteArray();
                                        size += byteArrayLocal.length;
                                        // String fileContent =
                                        // convertByteArrayToString(byteArrayLocal);

                                        if (size > maxAttachmentSize)
                                        {
                                            noAttachment = true;
                                            break;
                                        }
                                        else
                                        {
                                            content.put("ATTACHMENT_CONTENT_" + iAttachment, (byteArrayLocal != null) ? byteArrayLocal : (new byte[0]));
                                            content.put("ATTACHMENT_NAME_" + iAttachment, (name != null) ? name : "");
                                            content.put("ATTACHMENT_MIMETAG_" + iAttachment, (att.getMimeTag() != null) ? att.getMimeTag() : "");

                                            iAttachment++;
                                        }
                                    }
                                }
                            }

                            if (noAttachment)
                            {
                                content = new Hashtable<String, Object>();

                                content.put("EMAILID", itemID);
                                content.put("SENDERNAME", (senderName != null) ? senderName : "");
                                content.put("SENDERADDRESS", (senderAddress != null) ? senderAddress : "");
                                content.put("FROM", from);
                                content.put("SUBJECT", (emailMsg.getSubject() != null) ? emailMsg.getSubject() : "");
                                content.put("CONTENT", (emailMsg.getBody() != null) ? emailMsg.getBody() : "");
                                content.put("ATTACHMENT_COUNT", Integer.toString(0));

                                iAttachmentFile = 1;

                                for (IAttachment att : atts)
                                {
                                    String name = att.getLongName();
                                    AttachmentType attType = att.getType();

                                    if (attType.equals(AttachmentType.BINARY) || attType.equals(AttachmentType.EMBEDDED))
                                    {
                                        baOutStream = new ByteArrayOutputStream();
                                        att.writeTo(baOutStream);
                                        byte[] byteArrayLocal = baOutStream.toByteArray();
                                        // String fileContent =
                                        // convertByteArrayToString(byteArrayLocal);

                                        String path = save(emailMsg.getSubject(), name, byteArrayLocal);

                                        if (path != null && !path.equals(""))
                                        {
                                            content.put("ATTACHMENT_FILE_" + iAttachmentFile, (path != null) ? path : "");
                                            content.put("ATTACHMENT_MIMETAG_" + iAttachmentFile, (att.getMimeTag() != null) ? att.getMimeTag() : "");
                                            iAttachmentFile++;
                                        }
                                    }
                                }

                                content.put("ATTACHMENT_FILE_COUNT", Integer.toString(iAttachmentFile - 1));
                            }
                        }

                        content.put("REFERENCE", (emailMsg.getSubject() != null) ? emailMsg.getSubject() : "");
                        content.put("ALERTID", (emailMsg.getId() != null) ? emailMsg.getId() : "");

                        // This is for executing groovy script that is entered
                        // for the filter
                        if (filter.hasGroovyScript())
                        {
                            String script = filter.getScript();
                            String scriptName = filter.getId();

                            Binding binding = new Binding();
                            binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
                            binding.setVariable(Constants.GROOVY_BINDING_DEBUG, debug);
                            binding.setVariable(Constants.GROOVY_BINDING_NAME_PROPERTIES, nameProperties);
                            binding.setVariable(Constants.GROOVY_BINDING_ESB, MainBase.esb);

                            // Filter specific java.util.regex.Matcher object
                            // may come from the subclasses
                            // for example, XMPP and Email gateway provide a
                            // matcher.
                            binding.setVariable(Constants.GROOVY_BINDING_REGEX_MATCHER, null);

                            // parameters
                            binding.setVariable(Constants.GROOVY_BINDING_INPUTS, content);

                            // assess output
                            Map<String, Object> outputs = new HashMap<String, Object>();
                            binding.setVariable(Constants.GROOVY_BINDING_OUTPUTS, outputs);

                            // execute script
                            try
                            {
                                GroovyScript.executeAsync(script, scriptName, true, binding, 60 * 1000);
                            }
                            catch (Exception ex)
                            {
                                Log.log.error("Failed to execute script for the Filter: " + filter.getId(), ex);
                            }

                            Log.log.debug("Filter Script OUTPUTS: " + outputs);

                            // insert runbook
                            if (outputs.containsKey(Constants.EXECUTE_WIKI))
                            {
                                runbook = (String) content.get(Constants.EXECUTE_WIKI);
                            }

                            // set content to outputs
                            content = outputs;
                        }

                        // This filter has Event so it takes precedence over
                        // Runbook.
                        if (content.containsKey(Constants.EVENT_EVENTID) && StringUtils.isNotEmpty((String) content.get(Constants.EVENT_EVENTID)))
                        {
                            MainBase.esb.sendEvent(content);
                        }
                        else if (runbook != null && !runbook.equals(""))
                        {
                            content.put(Constants.EXECUTE_WIKI, runbook);
                            GatewayEvent<String, Object> gatewayEvent = new GatewayEvent<String, Object>(System.currentTimeMillis(), content);
                            localEventQueue.offer(gatewayEvent);
                        }

                        // if (debug)
                        // {
                        Log.log.info("Alert.status result\n  alertid: " + content.get("ALERTID") + "\n  reference: " + content.get("REFERENCE") + "\n  subject: " + content.get("SUBJECT") + content.get("CONTENT") + "\n  runbook: " + runbook);
                        // }

                        if (!emailMsg.isRead())
                        {
                            folder.markRead(new String[] { itemID }, true);
                            Log.log.debug("Regular block: Message is updated mark as read, message id:" + itemID);
                            // emailMsg.markRead(true);
                            // emailMsg.set(Message.READ_FLAG, true);
                        }
                    }
                    catch (Exception exp)
                    {
                        Log.log.error("Failed to process email from Exchange Gateway: " + exp.getMessage(), exp);
                        reconnectForPoll();

                        folder = session.getStore().getInbox();

                        if (folder != null)
                        {
                            if (haveResolveFilter)
                            {
                                folder.setFilter(andMultiFilter);
                            }
                            else
                            {
                                folder.setFilter(readFlagFilter);
                            }
                        }

                        Log.log.info("After failed the poll item, reinit the folder again");

                        try
                        {
                            Message itemOriginal = (Message) item;

                            if (itemOriginal != null)
                            {
                                if (!itemOriginal.isRead())
                                {
                                    folder.markRead(new String[] { itemID }, true);
                                    Log.log.debug("Exception block: Message is updated mark as read, message id:" + itemID);
                                    // itemOriginal.markRead(true);
                                    // itemOriginal.set(Message.READ_FLAG,
                                    // true);
                                }
                            }
                        }
                        catch (Exception exp1)
                        {
                            Log.log.info("Try to mark the original bad item as read failed : " + exp, exp);
                        }
                    }
                }
            }
            // }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to poll for events: " + e.getMessage(), e);
            reconnectForPoll();
        }
        finally
        {
            if (baOutStream != null)
            {
                try
                {
                    baOutStream.close();
                }
                catch (Throwable t)
                {
                }
            }
        }

        return result;
    } // poll

    public Property getProperty(String filterKey)
    {
        Property property = Message.SUBJECT;

        if (filterKey != null && !filterKey.equals(""))
        {
            filterKey = filterKey.toLowerCase();
            int tagPosition = filterKey.indexOf(".");

            if (tagPosition > 0)
            {
                filterKey = filterKey.substring(tagPosition + 1, filterKey.length());

                property = FILTER_MAP.get(filterKey);

                if (property == null)
                {
                    property = Message.SUBJECT;
                }
            }
        }

        return property;
    }

    public String save(String subject, String attachmentName, byte[] attBytes) throws Exception
    {
        String fileAbsolutePath = "";
        File dir = null;
        File fileLocal = null;
        OutputStream os = null;

        try
        {
            if (subject == null || subject.equals(""))
            {
                subject = "default";
            }

            subject = replaceSpecialChars(subject);
            String path = MainBase.main.getProductHome() + "/attachment/" + subject;

            dir = new File(path);

            if (dir.isFile())
            {
                Log.log.warn("Directory " + path + " exists as file, Attempting to Delete");
                if (!dir.delete())
                {
                    Log.log.warn("Unable to Delete Existing File");
                }
            }

            if (dir.isDirectory())
            {
                Log.log.warn("Directory " + path + " already exists, writing into it");
            }
            else
            {
                Log.log.warn("Creating Directory: " + path);
                FileUtils.forceMkdir(dir);
            }

            attachmentName = this.replaceSpecialChars(attachmentName);
            String attachmentFilePath = path + "/" + attachmentName; // + "." +
                                                                     // System.currentTimeMillis();
            fileLocal = new File(attachmentFilePath);

            Log.log.debug("Attachment File Will Be Saved To: " + fileLocal.getAbsolutePath());
            if (fileLocal.exists())
            {
                Log.log.warn("File Already Exists, Attempting to Delete");
                if (!fileLocal.delete())
                {
                    Log.log.warn("Unable to Delete Existing File");
                }
            }

            os = new FileOutputStream(fileLocal, false);
            os.write(attBytes);

            // FileUtils.writeStringToFile(fileLocal, attachmentContent,
            // "UTF-8");

            fileAbsolutePath = fileLocal.getAbsolutePath() + "\n";

        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage() + ". Skipping file: " + fileLocal.getAbsolutePath());
        }
        finally
        {
            if (os != null)
            {
                try
                {
                    os.close();
                }
                catch (Throwable t)
                {
                }
            }
        }

        return fileAbsolutePath;
    } // save

    /**
     * need remove all special characters
     *
     * @param path
     * @return
     */
    public String replaceSpecialChars(String path)
    {
        if (path != null && !path.equals(""))
        {
            path = path.replace("\\", "");
            path = path.replace("/", "");
            path = path.replace(":", "");
            path = path.replace("*", "");
            path = path.replace("?", "");
            path = path.replace("\"", "");
            path = path.replace("<", "");

        }

        return path;
    }

    public List<String> getFilterString(String sql)
    {
        BufferedReader br = null;
        List<String> filterList = new ArrayList();

        if (sql != null && !sql.equals(""))
        {
            byte[] sqlByte = sql.getBytes();
            filterList = convertByteArrayToListOfString(sqlByte);
        }

        return filterList;
    }

    public String convertByteArrayToString(byte[] byteArray)
    {
        StringBuffer stringContent = new StringBuffer();
        BufferedReader br = null;

        try
        {
            if (byteArray != null && byteArray.length > 0)
            {
                br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(byteArray)));

                boolean blankLine = false;
                String lineString = null;

                while ((lineString = br.readLine()) != null)
                {
                    if (lineString.length() > 0)
                    {
                        stringContent.append(lineString + "\n");
                        blankLine = false;
                    }
                    else
                    {
                        if (blankLine)
                        {
                            stringContent.append("\n");
                            blankLine = false;
                        }
                        else
                        {
                            blankLine = true;
                        }
                    }
                }
            }
        }
        catch (Throwable t)
        {
            Log.log.error("Failed to convertByteArrayToString: " + t.getMessage(), t);
        }
        finally
        {
            if (br != null)
            {
                try
                {
                    br.close();
                }
                catch (Throwable t)
                {
                }
            }
        }

        return stringContent.toString();
    }

    public List<String> convertByteArrayToListOfString(byte[] byteArray)
    {
        List<String> stringList = new ArrayList();
        BufferedReader br = null;

        try
        {
            if (byteArray != null && byteArray.length > 0)
            {
                br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(byteArray)));

                String lineString = null;

                while ((lineString = br.readLine()) != null)
                {
                    if (lineString.length() > 0)
                    {
                        stringList.add(lineString);
                    }
                }
            }
        }
        catch (Throwable t)
        {
            Log.log.error("Failed to convertByteArrayToString: " + t.getMessage(), t);
        }
        finally
        {
            if (br != null)
            {
                try
                {
                    br.close();
                }
                catch (Throwable t)
                {
                }
            }
        }

        return stringList;
    }

    public void startUpdateThread()
    {
        Thread updateThread = new Thread(new Runnable()
        {
            public void run()
            {
                startTimeForUpdate = System.currentTimeMillis();

                while (updateThreadRunning)
                {
                    try
                    {
                        synchronized (updateQueue)
                        {
                            Map<String, Object> message = updateQueue.take();

                            Log.log.info("updateQueue get email need send to exchange server: ");

                            long duration = System.currentTimeMillis() - startTimeForUpdate;

                            if (duration > reconnectDelay)
                            {
                                reconnectForUpdate();
                            }

                            updateExchangeServer(message);

                            startTimeForUpdate = System.currentTimeMillis();
                        }
                    }
                    catch (Throwable t)
                    {
                        Log.log.info("updateQueue take catched exception: " + t.getMessage(), t);
                    }
                }
            }
        });

        updateThread.start();
    }

    public void updateExchangeServer(Map<String, Object> messageDetail)
    {
        IMessage messageLocal = null;
        IFolder folderLocal = null;

        try
        {
            // folderLocal =
            // sessionUpdate.getStore().getFolder(DefaultFolder.DRAFTS);
            // connect();
            folderLocal = sessionUpdate.getStore().getFolder(DefaultFolder.DRAFTS);// sessionUpdate.getStore().getFolder(DefaultFolder.DRAFTS);
            IFolder sent = sessionUpdate.getStore().getFolder(DefaultFolder.SENT_ITEMS);

            messageLocal = (IMessage) folderLocal.add();

            // String subjectLocal = ((String) messageDetail.get("SUBJECT")) !=
            // null?(String) messageDetail.get("SUBJECT"):"";
            // String subjectLocalafter =
            // StringUtils.getUrlString(subjectLocal);
            // subjectLocalafter = new String(subjectLocal.getBytes(),
            // "UTF-8");//java.net.URLDecoder.decode(subjectLocal, "UTF-16LE");

            // messageLocal.setSubject(subjectLocalafter);
            // messageLocal.setBody(subjectLocalafter);

            // messageLocal.set(Message.SENT_REPRESENTING_ADDRESS,
            // "noreply-att-reports@syniverse.com");
            messageLocal.setSubject((messageDetail.get("SUBJECT")) != null ? (String) messageDetail.get("SUBJECT") : "");
            messageLocal.setBody((messageDetail.get("CONTENT")) != null ? (String) messageDetail.get("CONTENT") : "");

            String toName = (messageDetail.get("TONAME") != null) ? (String) messageDetail.get("TONAME") : "";
            String to = (messageDetail.get("TO") != null) ? (String) messageDetail.get("TO") : "";

            String ccName = (messageDetail.get("CCNAME") != null) ? (String) messageDetail.get("CCNAME") : "";
            String cc = (messageDetail.get("CC") != null) ? (String) messageDetail.get("CC") : "";

            String bccName = (messageDetail.get("BCCNAME") != null) ? (String) messageDetail.get("BCCNAME") : "";
            String bcc = (messageDetail.get("BCC") != null) ? (String) messageDetail.get("BCC") : "";

            List<EmailInfo> emailList = new ArrayList();
            List<String> emailaddressList = new ArrayList();

            if ((to != null && !to.equals("")) || messageDetail.get("TOLIST") != null)
            {
                emailList = buildEmailList(toName, to);

                for (EmailInfo email : emailList)
                {
                    messageLocal.addRecipient(new Recipient(Recipient.Type.TO, email));
                }

                emailaddressList = (List) messageDetail.get("TOLIST");
                for (String emailaddress : emailaddressList)
                {
                    messageLocal.addRecipient(new Recipient(Recipient.Type.TO, new EmailInfo("", emailaddress)));
                }

                emailList = buildEmailList(ccName, cc);

                for (EmailInfo email : emailList)
                {
                    messageLocal.addRecipient(new Recipient(Recipient.Type.CC, email));
                }

                emailaddressList = (List) messageDetail.get("CCLIST");
                for (String emailaddress : emailaddressList)
                {
                    messageLocal.addRecipient(new Recipient(Recipient.Type.CC, new EmailInfo("", emailaddress)));
                }

                emailList = buildEmailList(bccName, bcc);

                for (EmailInfo email : emailList)
                {
                    messageLocal.addRecipient(new Recipient(Recipient.Type.BCC, email));
                }

                emailaddressList = (List) messageDetail.get("BCCLIST");
                for (String emailaddress : emailaddressList)
                {
                    messageLocal.addRecipient(new Recipient(Recipient.Type.BCC, new EmailInfo("", emailaddress)));
                }

                String attchmentCount = (String) messageDetail.get("ATTACHMENT_COUNT");
                int attachmentCountInt = 0;

                String attachmentFileCount = (String) messageDetail.get("ATTACHMENT_FILE_COUNT");
                int attachmentFileCountInt = 0;

                if (attchmentCount != null && !attchmentCount.equals(""))
                {
                    attachmentCountInt = Integer.valueOf(attchmentCount);

                    if (attachmentCountInt != 0)
                    {
                        for (int i = 1; i <= attachmentCountInt; i++)
                        {
                            String attName = (String) messageDetail.get("ATTACHMENT_NAME_" + i);
                            byte[] attachmentLocal = (byte[]) messageDetail.get("ATTACHMENT_CONTENT_" + i);
                            ByteArrayInputStream baIn = new ByteArrayInputStream(attachmentLocal);

                            messageLocal.addAttachment(new AttachmentSource(attName, baIn));
                        }
                    }
                }

                Log.log.info("==== the attachmentFileCount is " + attachmentFileCount);

                if (attachmentFileCount != null && !attachmentFileCount.equals(""))
                {
                    attachmentFileCountInt = Integer.valueOf(attachmentFileCount);

                    Log.log.info("==== the attachmentFileCountInt is " + attachmentFileCountInt);

                    if (attachmentFileCountInt != 0)
                    {
                        for (int k = 1; k <= attachmentFileCountInt; k++)
                        {
                            String attFileNamelocal = (String) messageDetail.get("ATTACHMENT_FILE_" + k);
                            attFileNamelocal = attFileNamelocal.trim();

                            Log.log.info("==== the attFileNamelocal is " + attFileNamelocal);

                            // attFileNamelocal =
                            // "C:\\opt\\attachment\\NSTAR Communications Splice Notification for Oct 11\\qry_notification_enter_dateA.rtf";

                            FileInputStream fileIn = getAttachmentFileInputStream(attFileNamelocal);

                            if (fileIn != null)
                            {
                                Log.log.info("==== fileIn != null ");
                                messageLocal.addAttachment(new AttachmentSource(attFileNamelocal, fileIn));
                                fileIn.close();

                                Log.log.info("==== after add addAttachment. ");
                            }
                        }
                    }
                }
            }

            Log.log.info("before send email of : " + messageLocal.getSubject());
            Log.log.info("the email send TO : " + to + " TOLIST : " + messageDetail.get("TOLIST") + " toName: " + toName);
            Log.log.info("the email send ccName : " + ccName + " cc : " + cc);
            Log.log.info("the email send bccName : " + bccName + " bcc : " + bcc);
            messageLocal.send(sent);

            Log.log.info("==== after messageLocal.send. ");

            // todo need make sure don't clean the message from moonrug
            messageDetail.clear();

        }
        catch (Throwable t)
        {
            Log.log.info("updateExchangeServer catched exception: " + t.getMessage(), t);
            reconnectForUpdate();
        }
    }

    public List<EmailInfo> buildEmailList(String name, String emailaddress)
    {
        List<EmailInfo> emailToList = new ArrayList();

        if (emailaddress != null && !emailaddress.equals(""))
        {
            String[] emailaddressList = emailaddress.split(";");
            EmailInfo emailInfo = null;

            for (String emailAddressLocal : emailaddressList)
            {
                emailInfo = new EmailInfo("", emailAddressLocal);
                emailToList.add(emailInfo);
            }
        }

        return emailToList;
    }

    public FileInputStream getAttachmentFileInputStream(String attFileNamelocal)
    {
        File attFileLocal = null;
        FileInputStream fileIn = null;

        // attFileNamelocal = attFileNamelocal.replace("service\\..", "");
        // attFileNamelocal = attFileNamelocal.replace("rsremote\\",
        // "rsremote");
        //
        // attFileNamelocal =
        // "C:\\project\\resolve3\\dist\\rsremote\\attachment\\Test11 Test66 Test99\\targets.xml.1297120680856";

        try
        {
            if (attFileNamelocal != null && !attFileNamelocal.equals(""))
            {
                Log.log.info("==== the attFileNamelocal is " + attFileNamelocal);

                attFileNamelocal = attFileNamelocal.trim();
                attFileLocal = new File(attFileNamelocal);

                Log.log.info("==== the attFileLocal is " + attFileLocal);

                if (attFileLocal.exists())
                {
                    Log.log.info("==== the attFileLocal exists ");

                    fileIn = new FileInputStream(attFileLocal);

                    Log.log.info("==== the fileIn " + fileIn);
                }
            }
        }
        catch (Throwable t)
        {
            Log.log.info("getAttachmentFileInputStream catched exception: " + t.getMessage(), t);
        }

        return fileIn;
    }

    public void start()
    {
        // init
        Log.log.warn("Starting ExchangeListener");
        super.start();
        state = STATE_PASSIVE_CONNECT;

        // connect to exchange
        connect();

        // start the thread for update.
        startUpdateThread();
    }

    public static synchronized boolean moveEmailToFolder(String[] emailIDs, String foldername, String subfoldername)
    {

        boolean sent = false;
        IFolder folderInbox = null;
        IFolder customFolder = null;
        IFolder subFolder = null;

        try
        {

            Log.log.info("moveEmailToFolder session: " + session);

            if (session != null)
            {

                if (emailIDs != null && emailIDs.length > 0 && foldername != null && !foldername.equals("") && (subfoldername == null || subfoldername.equals("")))
                {
                    folderInbox = session.getStore().getInbox();
                    Log.log.info("moveEmailToFolder get folderInbox  " + folderInbox);

                    IFolder[] subFoldersFromRoot = session.getStore().getRootFolder().getSubfolders(false);
                    Log.log.info("moveEmailToFolder get subFoldersFromRoot  " + subFoldersFromRoot);

                    if (subFoldersFromRoot != null)
                    {
                        for (IFolder folder : subFoldersFromRoot)
                        {
                            String name = folder.getName();

                            if (name != null && !name.equals("") && name.equals(foldername))
                            {
                                customFolder = folder;
                                break;
                            }
                        }

                        if (customFolder != null)
                        {
                            folderInbox.moveItems(emailIDs, customFolder);
                            sent = true;
                            Log.log.info("moveEmailToFolder after move items   " + emailIDs);
                        }
                        else
                        {
                            Log.log.info("Didn't find the folder of " + foldername);
                        }
                    }
                }
                else if (emailIDs != null && emailIDs.length > 0 && foldername != null && !foldername.equals("") && subfoldername != null && !subfoldername.equals(""))
                {
                    folderInbox = session.getStore().getInbox();

                    IFolder[] subFoldersFromRoot = session.getStore().getRootFolder().getSubfolders(false);

                    Log.log.info("moveEmailToFolder get folderInbox  " + folderInbox);

                    if (subFoldersFromRoot != null)
                    {
                        for (IFolder folder : subFoldersFromRoot)
                        {
                            String name = folder.getName();

                            if (name != null && !name.equals("") && name.equals(foldername))
                            {
                                customFolder = folder;
                                break;
                            }
                        }

                        if (customFolder != null)
                        {
                            IFolder[] subFolders = customFolder.getSubfolders(false);

                            if (subFolders != null)
                            {
                                for (IFolder subFolderLocal : subFolders)
                                {
                                    String subFName = subFolderLocal.getName();

                                    if (subFName != null && subFName.equals(subfoldername))
                                    {
                                        subFolder = subFolderLocal;
                                        break;
                                    }
                                }
                            }
                        }

                        if (subFolder != null)
                        {
                            folderInbox.moveItems(emailIDs, subFolder);
                            sent = true;

                            Log.log.info("moveEmailToFolder after move items to subfolder  " + emailIDs);
                        }
                        else
                        {
                            Log.log.info("Didn't find the folder of " + foldername + " or subfolder of " + subfoldername);
                        }
                    }
                }
                else
                {
                    Log.log.info("Missing some parameters -- emailIDs: " + emailIDs + " foldername: " + foldername + " subfoldername: " + subfoldername);
                }
            }
        }
        catch (Exception exp)
        {
            Log.log.error("moveEmailToFolder catched exception " + exp.getMessage(), exp);
            // reconnectForPoll();
        }

        return sent;
    }
}
