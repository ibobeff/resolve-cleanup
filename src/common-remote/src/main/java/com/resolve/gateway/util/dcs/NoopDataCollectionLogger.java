package com.resolve.gateway.util.dcs;

import java.util.Map;

public class NoopDataCollectionLogger extends BaseDataCollectionLogger<Object> {

    @Override
    protected Map<String, String> getPropertiesMap(String... properties)
    {
        // TODO NOOP
        return null;
    }

    @Override
    protected String getMessageType()
    {
        // TODO NOOP
        return null;
    }


}