package com.resolve.gateway.resolvegateway.msg;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.gateway.resolvegateway.ConfigReceiveResolveGateway;
import com.resolve.gateway.resolvegateway.ResolveGatewayUtil;
import com.resolve.gateway.resolvegateway.msg.MSGGateway;
import com.resolve.gateway.resolvegateway.msg.MSGGatewayFilter;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveMSGGateway extends ConfigReceiveResolveGateway {

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_NODE = "./RECEIVE/";
    private static volatile Map<String, MSGGateway> gateways = new ConcurrentHashMap<String, MSGGateway>();
    private static volatile Map<String, MSGGatewayProperties> gatewayProperties = new ConcurrentHashMap<String, MSGGatewayProperties>();

    public ConfigReceiveMSGGateway(XDoc configDoc, String gatewayName) throws Exception {
        super(configDoc, gatewayName);
    }

    public static Map<String, MSGGateway> getGateways()
    {
        return gateways;
    }

    public static void setGateways(Map<String, MSGGateway> gateways)
    {
        ConfigReceiveMSGGateway.gateways = gateways;
    }

    @Override
    public String getRootNode() {
        return RECEIVE_NODE + gatewayName.toUpperCase() + "/";
    }

    public void loadMSGGatewayProperties(String gatewayName, String node) {

        Map<String, String> properties = ResolveGatewayUtil.loadGatewayProperties(node);
      
        MSGGatewayProperties msgGatewayProperties = new MSGGatewayProperties();
        Map<String, Object> additional = msgGatewayProperties.getAdditionalProperties();
        
        for(Iterator<String> it=properties.keySet().iterator(); it.hasNext();) {
            String key = it.next();
            String value = properties.get(key);
            switch(key) {
                case "ACTIVE":
                    try {
                        Boolean active = new Boolean(value);
                        msgGatewayProperties.setActive(active);
                        this.setActive(active);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "MENUTITLE":
                    msgGatewayProperties.setDisplayName(value);
                    break;
                case "QUEUE":
                    String queue = value;
                    msgGatewayProperties.setQueue(queue);
                    msgGatewayProperties.setLicenseCode(queue); // Currently it's hardcoded the same as the queue name
                    msgGatewayProperties.setEventType(queue); // Currently it's hardcoded the same as the queue name
                    this.setQueue(queue);
                    break;
                case "IMPLPREFIX":
                    msgGatewayProperties.setClassName(value);
                    break;
                case "PACKAGE":
                    msgGatewayProperties.setPackageName(value);
                    break;
                case "LICENSE":
                    msgGatewayProperties.setLicenseCode(value);
                    break;
                case "EVENTTYPE":
                    msgGatewayProperties.setEventType(value);
                    break;
                case "PRIMARY":
                    try {
                        Boolean primary = new Boolean(value);
                        msgGatewayProperties.setPrimary(primary);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "SECONDARY":
                    try {
                        Boolean secondary = new Boolean(value);
                        msgGatewayProperties.setSecondary(secondary);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "WORKER":
                    try {
                        Boolean worker = new Boolean(value);
                        msgGatewayProperties.setWorker(worker);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "MOCK":
                    try {
                        Boolean mock = new Boolean(value);
                        msgGatewayProperties.setMock(mock);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "UPPERCASE":
                    try {
                        Boolean uppercase = new Boolean(value);
                        msgGatewayProperties.setUppercase(uppercase);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "INTERVAL":
                    try {
                        Integer interval = new Integer(value);
                        msgGatewayProperties.setInterval(interval);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "FAILOVER":
                    try {
                        Integer failover = new Integer(value);
                        msgGatewayProperties.setFailover(failover);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "HEARTBEAT":
                    try {
                        Integer heartbeat = new Integer(value);
                        msgGatewayProperties.setHeartbeat(heartbeat);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "SSLTYPE":
                    msgGatewayProperties.setSslType(value);
                    break;
                case "MSG_SSL":
                    if(value == null)
                        break;
                    try {
                        Boolean ssl = new Boolean(value);
                        msgGatewayProperties.setWorker(ssl);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "MSG_PORT":
                    try {
                        if(StringUtils.isBlank(value))
                            continue;
                        Integer httpPort = new Integer(value);
                        msgGatewayProperties.setMSGPort(httpPort);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "MSG_USERNAME":
                    msgGatewayProperties.setMSGUser(value);
                    break;
                case "MSG_PASSWORD":
                    if (StringUtils.isNotBlank(value) && value.startsWith("ENC1:")) {
                        try {
                            value = CryptUtils.decrypt(value);
                        } catch(Exception e) {
                            Log.log.error(e.getMessage(), e);
                        }
                    }
                    
                    msgGatewayProperties.setMSGPass(value);
                    break;
                case "MSG_SSLCERTIFICATE":
                    msgGatewayProperties.setSslCertificate(value);
                    break;
                
                case "ALIVE":
                    try {
                        Integer alive = new Integer(value == "" ? "0" :value);
                        msgGatewayProperties.setAlive(alive);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "PRODUCERPROPS":
                    msgGatewayProperties.setProducerProps(value);
                    break;
                case "CONSUMERPROPS":
                    msgGatewayProperties.setConsumerProps(value);
                    break;
                default:
                    additional.put(key, value);
                    break;
            }
        }
        
        gatewayProperties.put(gatewayName, msgGatewayProperties);
    }
    
    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for IBMmq gateway, check blueprint. " + e.getMessage(), e);
        }
    }
    
    public void loadMSGGatewayFilters(String gatewayName, String queueName) {

        if(!isActive())
            return;
        
        Map<String, Map<String, Object>> gatewayFilters = ResolveGatewayUtil.loadGatewayFilters(gatewayName, "msg", queueName);
        if(gatewayFilters == null)
            return;
        
        for(Iterator<String> it=gatewayFilters.keySet().iterator(); it.hasNext();) {
        
            String filterName = it.next();
            Map<String, Object> filter = gatewayFilters.get(filterName);
            
            String sysId = (String)filter.get("SYS_ID");
            String active = ((Boolean)filter.get("ACTIVE")).toString();
            String order = ((Integer)filter.get("ORDER")).toString();
            String eventId = (String)filter.get("EVENT_EVENTID");
            String runbook = (String)filter.get("RUNBOOK");
            String script = (String)filter.get("SCRIPT");
            
            MSGGatewayFilter msgGatewayFilterFilter = new MSGGatewayFilter(filterName, active, order, eventId, runbook, script);
            
            msgGatewayFilterFilter.setDeployed(((Boolean)filter.get("DEPLOYED")));
            msgGatewayFilterFilter.setUpdated(((Boolean)filter.get("UPDATED")));
            msgGatewayFilterFilter.setGatewayName(gatewayName);
            msgGatewayFilterFilter.setQueue((String)filter.get("QUEUE"));
            msgGatewayFilterFilter.setUser ((String)filter.get("USERNAME"));
            msgGatewayFilterFilter.setUsername((String)filter.get("USERNAME"));
            msgGatewayFilterFilter.setPass((String)filter.get("PASSWORD"));
            msgGatewayFilterFilter.setSsl((String)filter.get("SSL"));
            
            Map<String, Object> gatewayFilterAttributes = ResolveGatewayUtil.loadGatewayFilterAttributes(sysId, "msg");
            if(gatewayFilterAttributes != null)
                msgGatewayFilterFilter.setAttributes(gatewayFilterAttributes);

            MSGGateway.addMSGFilter(filterName, msgGatewayFilterFilter);
        }
    }

    public static MSGGatewayProperties getGatewayProperties(String name)
    {
        return gatewayProperties.get(name);
    }
    
}
