/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.amqp;

import java.io.IOException;
import java.net.InetAddress;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;
import com.resolve.esb.ESBException;
import com.resolve.util.Guid;
import com.resolve.util.Log;

/**
 * RabbitMQ Queue/Exchange listener. This has mechanism to reestablish broken channel connection.
 *
 */
public class AmqpListener implements Consumer
{
    //this is the Queue
    private String receiveQueueName;

    private String listenerId;

    private final AmqpServer amqpServer;
    protected Channel receiveChannel;
    
    private boolean isOpen = false;

    private AmqpGateway instance = AmqpGateway.getInstance();

    public AmqpListener(final AmqpServer amqpServer, final String queueName) throws ESBException
    {
        this(amqpServer, queueName, false);
    }
    
    public AmqpListener(final AmqpServer amqpServer, final String queueName, final boolean isExchange) throws ESBException
    {
        try
        {
        // init references
            receiveChannel = amqpServer.createChannel();
            if(isExchange)
            {
                String tmpQueueName = receiveChannel.queueDeclare().getQueue();
                this.listenerId = tmpQueueName;
            }
            else
            {
                this.listenerId = queueName + "#" + Guid.getGuid();
            }
            this.receiveQueueName = queueName;
            this.amqpServer = amqpServer;
            isOpen = true;
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        } 
    } // AmqpListener

    public Channel getReceiveChannel()
    {
        return receiveChannel;
    }

    public String getListenerId()
    {
        return listenerId;
    }

    public void setListenerId(String listenerId)
    {
        this.listenerId = listenerId;
    }

    public String getReceiveQueueName()
    {
        return receiveQueueName;
    } // getReceiveQueueName

    public void init(final boolean flushQueue) throws ESBException
    {
        if (receiveChannel == null || !receiveChannel.isOpen())
        {
            receiveChannel = amqpServer.createChannel();
            isOpen = true;
        }
        String address = receiveQueueName;
        if (amqpServer.getPublications().containsKey(address) || address.endsWith("_TOPIC"))
        {
            amqpServer.subscribePublication(receiveQueueName, this);
        }
        else
        {
            amqpServer.createQueueConsumer(receiveQueueName, this);
        }

    } // init

    public void close()
    {
        try
        {
            Log.log.info("Closing listener: " + receiveQueueName);
            isOpen = false;
            if (receiveChannel != null && receiveChannel.isOpen())
            {
                receiveChannel.close();
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    } // close

    /**
     * This method gets activated by AMQP server when a message arrives.
     *
     */
    public void handleDelivery(final String consumerTag, final Envelope envelope, final AMQP.BasicProperties properties, final byte[] body) throws IOException
    {
        String oldThreadName = "PoolExecutor";

        try
        {
            if (body != null)
            {
                String messageId = "" + envelope.getDeliveryTag();
                if (amqpServer.isActive())
                {
                    //Object message = ObjectProperties.deserialize(body);
                    String message = new String(body);
                    if (message != null)
                    {
                        if (Log.log.isDebugEnabled())
                        {
                            Log.log.debug("Received delivery tag: " + envelope.getDeliveryTag());
                            Log.log.debug("Message received: " + message);
                        }
                        // Call the method to process this message.
                        if(message instanceof String)
                        {
                            //process the message for each filter that are interested
                            instance.processData(getReceiveQueueName(), (String) message);
                        }
                    }
                }
                else
                {
                    Log.log.warn("Message discarded. AMQP Server is inactive and Listener not currently active for Id: " + messageId);
                }
            }
        }
        catch (Throwable e)
        {
            Log.log.warn("Failed to execute message handler: " + e.getMessage(), e);
        }
        finally
        {
            // make sure msgs are acknowledged
            try
            {
                receiveChannel.basicAck(envelope.getDeliveryTag(), false);
                // restore thread name
                Thread.currentThread().setName(oldThreadName);
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
    }

    public void handleCancel(String consumerTag) throws IOException
    {
        Log.log.trace("Handling CANCEL AMQP:" + consumerTag);
    }

    public void handleCancelOk(String consumerTag)
    {
        Log.log.trace("Handling CANCEL OK AMQP:" + consumerTag);
    }

    public void handleConsumeOk(String consumerTag)
    {
        Log.log.trace("Handling CONSUME OK AMQP:" + consumerTag);
    }

    public void handleRecoverOk(String consumerTag)
    {
        Log.log.trace("Handling RECOVERY OK AMQP:" + consumerTag);
    }

    public void handleShutdownSignal(String consumerTag, ShutdownSignalException exception)
    {
        Log.log.warn("AMQP service unreachable for consumer: " + consumerTag + ", the reason provided is " + exception.getReason());
        while (isOpen)
        {
            try
            {
                Log.log.debug("Listener: " + getListenerId() + " is reconnecting to " + getReceiveQueueName());
                if (amqpServer.getConnection() != null && amqpServer.getConnection().isOpen())
                {
                    //give it a new listener id, since the listener id is used as "consumerTag", RabbitMQ doesn't
                    //like to reuse a tag that was failed at sometime.
                    setListenerId(getReceiveQueueName() + "#" + Guid.getGuid());
                    init(false);

                    InetAddress host = getReceiveChannel().getConnection().getAddress();
                    int port = getReceiveChannel().getConnection().getPort();

                    Log.log.debug("Connection established for listener: " + getListenerId() + " to " + getReceiveQueueName());
                    Log.log.debug("Connected to: " + host.getHostAddress() + ":" + port);
                    break;
                }
            }
            catch (ESBException e)
            {
                Log.log.warn(e.getMessage());
                Log.log.trace(e.getMessage(),e);
            }

            try
            {
                Thread.sleep(5000L);
            }
            catch (InterruptedException e1) { }
        }
    }
} // AmqpListener

