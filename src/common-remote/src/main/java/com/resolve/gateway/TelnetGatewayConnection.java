/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import com.resolve.connect.TelnetConnect;
import com.resolve.util.Log;

public class TelnetGatewayConnection extends TelnetConnect
{
    public TelnetGatewayConnection(String hostname) throws Exception
    {
        this(hostname, 23, 0, null, true, true);
    } // TelnetGatewayConnection

    public TelnetGatewayConnection(String hostname, int port) throws Exception
    {
        this(hostname, port, 0, null, true, true);
    } // TelnetGatewayConnection

    public TelnetGatewayConnection(String hostname, int port, int timeout) throws Exception
    {
        this(hostname, port, timeout, null, true, true);
    } // TelnetGatewayConnection

    public TelnetGatewayConnection(String hostname, int port, int timeout, String terminalType) throws Exception
    {
        this(hostname, port, timeout, terminalType, true, true);
    }

    public TelnetGatewayConnection(String hostname, int port, int timeout, String terminalType, boolean suppressGAOption, boolean echoOption) throws Exception
    {
        this(hostname, port, timeout, terminalType, suppressGAOption, echoOption, true);
    } // TelnetGatewayConnection

    public TelnetGatewayConnection(String hostname, int port, int timeout, String terminalType, boolean suppressGAOption, boolean echoOption, boolean isConnect) throws Exception
    {
        super(hostname, port, timeout, terminalType, suppressGAOption, echoOption, isConnect);
    } // TelnetGatewayConnection

    @Override
    public void close()
    {
        try
        {
            /* Close this session */
            if (conn != null)
            {
                conn.disconnect();
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to close TelnetGatewayConnection: " + e.getMessage());
        }

        isClosed = true;
    } // close

    @Override
    public String toString()
    {
        return "TelnetGatewayConnection [hostname=" + hostname + ", port=" + port + ", timeout=" + timeout + "]";
    }
}
