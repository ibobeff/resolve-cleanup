/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.salesforce;

import java.util.Map;

import com.resolve.rsremote.ConfigReceiveSalesforce;

/**
 * This class acts as a service to manage Cases.
 * 
 * @author Chelsea Guthrie
 * 
 */
public class CaseObjectService extends AbstractObjectService
{
    static String OBJECT_IDENTITY = "case";

    private static String IDENTITY_PROPERTY = "NUMBER";
    
    private String urn = "urn:enterprise.soap.sforce.com";
    
    public CaseObjectService(ConfigReceiveSalesforce configurations)
    {
        super(configurations);
        if("partner".equalsIgnoreCase(configurations.getAccounttype()))
        {
            urn = "urn:partner.soap.sforce.com";
        }
    }

    @Override
    public String getIdPropertyName()
    {
        return IDENTITY_PROPERTY;
    }

    @Override
    public String getIdentity()
    {
        return OBJECT_IDENTITY;
    }

    // ////////////////////////////////////////////////////////////////////
    // Method: getSoapEnvelope //
    // //
    // Description: This method constructs an envelope to send to //
    // Salesforce based on the input. The operation could be //
    // "create", "update", "casecomment", "delete" and "query" //
    // //
    // Inputs: sessionId: This is needed in each Salesforce envelope //
    // operation: the operation to be performed, this is //
    // necessary because there are slight differences //
    // depending on the operation //
    // params: This is a map containing the keys and values //
    // for input into the soap envelope. //
    // //
    // Outputs: soapEnvelope: This returns a fully constructed, ready //
    // to send soap envelope. //
    // //
    // ////////////////////////////////////////////////////////////////////
    @Override
    protected String getSoapEnvelope(String sessionId, String operation, Map<String, String> params)
    {
        StringBuilder soapEnvelope = new StringBuilder("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"" + urn + "\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" 
        + "    <soapenv:Header>\n" 
        + "       <urn:PackageVersionHeader>\n" 
        + "       </urn:PackageVersionHeader>\n" 
        + "       <urn:MruHeader>\n" 
        + "           <urn:updateMru>0</urn:updateMru>\n" 
        + "       </urn:MruHeader>\n" 
        + "           <urn:QueryOptions>\n"
        + "               <urn:batchSize>10</urn:batchSize>\n" 
        + "           </urn:QueryOptions>\n" 
        + "       <urn:SessionHeader>\n" 
        + "           <urn:sessionId>" + sessionId + "</urn:sessionId>\n" 
        + "       </urn:SessionHeader>\n" 
        + "   </soapenv:Header>\n" 
        + "   <soapenv:Body>\n");
        if (operation.equals("casecomment")) // because you "create" a case
                                             // comment, but there is already a
                                             // create, we need to seperate this
                                             // one out.
        {
            soapEnvelope.append("     <urn:create xmlns=\"" + urn + "\">\n");
        }
        else
        {
            soapEnvelope.append("       <urn:" + operation + ">\n");
        }

        // in the case of both update and create it is necessary that a line of
        // the soap envelope specifically reference the type "Case", this is
        // because Salesforce allows you to call create and update for items
        // other than case.
        if (operation.equals("update") || operation.equals("create"))
        {
            soapEnvelope.append("           <sObjects xsi:type=\"ns3:Case\" xmlns:ns3=\"urn:sobject." + configurations.getAccounttype().toLowerCase() + ".soap.sforce.com\">\n"); // Note,
                                                                                                                                       // this
                                                                                                                                       // does
                                                                                                                                       // mean
                                                                                                                                       // it
                                                                                                                                       // is
                                                                                                                                       // _only_
                                                                                                                                       // relevant
                                                                                                                                       // for
                                                                                                                                       // cases.
        }
        else if (operation.equals("casecomment")) // similarly in caseComment,
                                                  // the type of object needs to
                                                  // be specified
        {
            soapEnvelope.append("           <sObjects xsi:type=\"ns3:CaseComment\" xmlns:ns3=\"urn:sobject." + configurations.getAccounttype().toLowerCase() + ".soap.sforce.com\">\n");
        }

        for (String key : params.keySet())
        {
            // due to the way the soap envelopes are setup there is a different
            // namespace for update, create, and case comment than the other
            // types of operations
            if (operation.equals("update") || operation.equals("create") || operation.equals("casecomment"))
            {
                soapEnvelope.append("           <ns3:" + key + ">" + params.get(key) + "</ns3:" + key + ">\n");
            }
            else
            {
                soapEnvelope.append("           <urn:" + key + ">" + params.get(key) + "</urn:" + key + ">\n");
            }
        }

        // If there was a special object line in the envelope, we need a close
        // tag for that
        if (operation.equals("update") || operation.equals("create") || operation.equals("casecomment"))
        {
            soapEnvelope.append("           </sObjects>\n");
        }
        if (operation.equals("casecomment")) // if we are dealing with a
                                             // casecomment add a close tag for
                                             // the create
        {
            soapEnvelope.append("     </urn:create>\n");
        }
        else
        // otherwise add a close tag for whatever operation we _are_ working
        // with
        {
            soapEnvelope.append("       </urn:" + operation + ">\n");
        }

        soapEnvelope.append("   </soapenv:Body>\n");
        soapEnvelope.append("</soapenv:Envelope>");
        return soapEnvelope.toString();
    }
}
