/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.remedyx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;

import com.bmc.arsys.api.ARServerUser;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.OutputInteger;
import com.bmc.arsys.api.QualifierInfo;
import com.bmc.arsys.api.SortInfo;
import com.bmc.arsys.api.Timestamp;
import com.bmc.arsys.api.Value;
import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MRemedyx;
import com.resolve.query.ResolveQuery;
import com.resolve.query.translator.RemedyQueryTranslator;
import com.resolve.rsremote.ConfigReceiveRemedyx;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class RemedyxGateway extends BaseClusteredGateway
{
    private static final String INCIDENT_NUMBER_FIELD_ID = "1000000161";

    // Singleton
    private static volatile RemedyxGateway instance = null;
    
    private static String STATUS_IN_PROCESS = "2";

    // This will be set in initialize() with the proper queue name from the
    // configuration.
    private String queue = "REMEDYX";

    protected ConcurrentHashMap<String, RemedyxForm> forms = new ConcurrentHashMap<String, RemedyxForm>();

    private final ResolveQuery resolveQuery;

    private final ConfigReceiveRemedyx remedyxConfig;

    public static RemedyxGateway getInstance(ConfigReceiveRemedyx config)
    {
        if (instance == null)
        {
            instance = new RemedyxGateway(config);
        }
        return (RemedyxGateway) instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static RemedyxGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("Remedy2 Gateway is not initialized correctly..");
        }
        else
        {
            return (RemedyxGateway) instance;
        }
    }

    /**
     * Private constructor.
     *
     * @param config
     */
    private RemedyxGateway(ConfigReceiveRemedyx config)
    {
        // Important, call super here.
        super(config);
        remedyxConfig = config;
        queue = config.getQueue();
        resolveQuery = new ResolveQuery(new RemedyQueryTranslator());
    } // RemedyGateway

    @Override
    public String getLicenseCode()
    {
        return "REMEDYX";
    }

    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_REMEDYX;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MRemedyx.class.getSimpleName();
    }

    @Override
    protected Class<MRemedyx> getMessageHandlerClass()
    {
        return MRemedyx.class;
    }

    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    /**
     * Initializes the specific gateway resources.
     *
     * @param config
     */
    @Override
    public void initialize()
    {
        ConfigReceiveRemedyx remedyConfig = (ConfigReceiveRemedyx) configurations;

        queue = remedyConfig.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/remedyx/";
    }

    // Connect the current user to the server.
    private ARServerUser connect(String username, String password) throws Exception
    {
        ConfigReceiveRemedyx remedyConfig = (ConfigReceiveRemedyx) configurations;

        ARServerUser server = new ARServerUser();
        server.setServer(remedyConfig.getHost());
        if (remedyConfig.getPort() > 0)
        {
            server.setPort(remedyConfig.getPort());
        }
        Log.log.debug("Connecting to AR Server...");
        try
        {
            if (StringUtils.isNotEmpty(username))
            {
                server.setUser(username);
                server.setPassword(password);
            }
            else
            {
                server.setUser(remedyConfig.getUsername());
                server.setPassword(remedyConfig.getPassword());
            }
            
            //at this point verify if the server is still available
            //the getServerVersion call is simply to see if a connection is
            //available to the server.
            String serverVersion = server.getServerVersion();
            if(StringUtils.isBlank(serverVersion))
            {
                throw new Exception("Cannot establish a network connection to the AR System server; Connection refused: " + server.getServer() + ":" + server.getPort());
            }
            else
            {
                Log.log.debug("AR System server version: " + serverVersion);
            }
            //now that the server is available verify the user
            server.verifyUser();
        }
        catch (Exception e)
        {
            // This exception is triggered by a bad server, password or,
            // if guest access is turned off, by an unknown username.
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
        Log.log.debug("Connected to AR Server " + server.getServer());
        return server;
    }

    private void disconnect(ARServerUser server)
    {
        if (server != null)
        {
            // Logout the user from the server. This releases there source
            // allocated on the server for the user.
            server.logout();
            Log.log.debug("User logged out.");
        }
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        RemedyxFilter remedyxFilter = new RemedyxFilter((String) params.get(RemedyxFilter.ID), (String) params.get(RemedyxFilter.ACTIVE), (String) params.get(RemedyxFilter.ORDER), (String) params.get(RemedyxFilter.INTERVAL), (String) params.get(RemedyxFilter.EVENT_EVENTID)
        // , (String) params.get(RemedyxFilter.PERSISTENT_EVENT)
                        , (String) params.get(RemedyxFilter.RUNBOOK), (String) params.get(RemedyxFilter.SCRIPT), (String) params.get(RemedyxFilter.FORM_NAME), (String) params.get(RemedyxFilter.QUERY), (String) params.get(RemedyxFilter.LAST_VALUE_FIELD), Integer.valueOf((String) params.get(RemedyxFilter.LIMIT)));
        
        if ((params.containsKey(RemedyxFilter.LAST_VALUE)) && StringUtils.isNotBlank((String)params.get(RemedyxFilter.LAST_VALUE)))
        {
            remedyxFilter.setLastValue((String)params.get(RemedyxFilter.LAST_VALUE));
        }
        
        if ((params.containsKey(RemedyxFilter.LAST_ENTRY_ID)) && StringUtils.isNotBlank((String)params.get(RemedyxFilter.LAST_ENTRY_ID)))
        {
            remedyxFilter.setLastEntryId((String)params.get(RemedyxFilter.LAST_ENTRY_ID));
        }
        
        return remedyxFilter;
    } // getFilter

    public Map<String, RemedyxForm> getForms()
    {
        return forms;
    } // getForms

    /**
     * Clears and set new form.
     *
     * @param formList
     */
    public void clearAndSetForms(List<Map<String, Object>> formList)
    {
        // remove all forms
        forms.clear();

        // set filters
        if(formList != null)
        {
            for (Map<String, Object> params : formList)
            {
                //ignore the username Map
                if(!params.containsKey("RESOLVE_USERNAME"))
                {
                    String name = (String) params.get("NAME");
                    // add filter
                    forms.put(name, getForm(params));
                    Log.log.info("Adding Form: " + params);
                }
            }
        }
    } // clearAndSetForms

    public void removeForm(String id)
    {
        if (StringUtils.isNotEmpty(id))
        {
            forms.remove(id);
        }
    } // removeFilter

    public void setForm(Map<String, Object> params)
    {
        RemedyxForm form = getForm(params);

        // remove existing form
        removeForm(form.getName());

        // update forms
        forms.put(form.getName(), form);
    }

    public RemedyxForm getForm(Map<String, Object> params)
    {
        return new RemedyxForm((String) params.get(RemedyxForm.NAME), (String) params.get(RemedyxForm.FIELD_LIST));
    } // getForm

    @Override
    public void start()
    {
        Log.log.warn("Starting RemedyListener");
        super.start();
    } // start

    @Override
    public void run()
    {
        //This call is must for all the ClusteredGateways.
        super.sendSyncRequest();

        ConfigReceiveRemedyx remedyConfig = (ConfigReceiveRemedyx) configurations;

        ARServerUser server = new ARServerUser();
        server.setServer(remedyConfig.getHost());
        if(remedyConfig.getPort() > 0)
        {
            server.setPort(remedyConfig.getPort());
        }
        server.setUser(remedyConfig.getUsername());
        server.setPassword(remedyConfig.getPassword());

        while (running)
        {
            try
            {
                // verify user.
                server.verifyUser();

                long startTime = System.currentTimeMillis();

                /* if any remaining events are in primary data queue or 
                 * primary data executor has waiting messages still to be processed, 
                 * do not get messages for more new events.
                 */
                
                if (primaryDataQueue.isEmpty() && remedyConfig.isPoll() && !primaryDataQueueExecutorBusy())
                {
                    Log.log.trace("Local Event Queue is empty and Primary Data Queue Executor is free.....");

                    for (Filter filter : orderedFilters)
                    {
                        if (shouldFilterExecute(filter, startTime))
                        {
                            RemedyxFilter remedyFilter = (RemedyxFilter) filter;
                            List<Map<String, String>> results = invokeRemedyService(server, remedyFilter);
                            if (results.size() > 0)
                            {
                                Map<String, String> entries = new HashMap<String, String>();
                                // Let's execute runboook for each object
                                for (Map<String, String> remedyObject : results)
                                {
                                    //processFilter(remedyFilter, null, remedyObject);
                                    addToPrimaryDataQueue(remedyFilter, remedyObject);
                                    String entryId = remedyObject.get("" + com.bmc.arsys.api.Constants.AR_CORE_ENTRY_ID);
//                                  String entryId = new String(remedyObject.get(com.bmc.arsys.api.Constants.AR_CORE_ENTRY_ID));
                                    
                                    // Update the status on the server for the incidents being already retrieved, "2" means "In Progress" from "1" that means "Assigned"
//                                    updateStatus(remedyFilter.getFormName(), entryId, STATUS_IN_PROCESS, remedyConfig.getUsername(), remedyConfig.getPassword());
                                }

                                // Update the lastValue property.
                                // since we intentionally get the objects
                                // ordered by changed date in ascending order,
                                // go to the bottom of the list and get change
                                // date from the last object.
                                Map<String, String> lastResult = results.get(results.size() - 1);
                                String lastValue = lastResult.get(remedyFilter.getLastValueField());
                                // Timestamp x =
                                String lastEntryId = lastResult.get("" + com.bmc.arsys.api.Constants.AR_CORE_ENTRY_ID);
                                Log.log.debug("The retrieved last value is " + lastValue);
                                Log.log.debug("The retrieved last entry id is " + lastEntryId);
                                
                                if (StringUtils.isNotBlank(lastValue))
                                {
                                    remedyFilter.setLastValue(lastValue);
                                }
                                else
                                {
                                    Log.log.error("Last object received from server either does not contain selected last value field [" + 
                                                  remedyFilter.getLastValueField() + "] or value set for last value field is empty. " +
                                                  "Indicates possible configuration error in form fields for filter [" + remedyFilter.getId() + "]");
                                }
                                
                                remedyFilter.setLastEntryId(lastEntryId);
                            }
                        }
                        else
                        {
                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute.....");
                        }
                    }
                }

                // process events collected by processFilter.
                //processFilterEvents(startTime);

                // If there is no filter yet no need to keep spinning, wait for
                // a while
                if (orderedFilters.size() == 0)
                {
                    Thread.sleep(interval);
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Error: " + e.getMessage(), e);
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", 
                          "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + 
                          "due to " + e.getMessage());
                
                try
                {
                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    } // run

    /**
     * This method invokes the Remedy web service.
     *
     * @param filter
     * @return - Service response or null based on filter condition.
     * @throws Exception
     */
    private List<Map<String, String>> invokeRemedyService(ARServerUser server, RemedyxFilter filter) throws Exception
    {
        String nativeQuery = filter.getQuery();
        if (filter.getNativeQuery() == null)
        {
            try
            {
                nativeQuery = resolveQuery.translate(filter.getQuery());
            }
            catch (Exception e)
            {
                Log.log.debug("Did not translate, the query \"" + nativeQuery + "\" is treated as native.");
            }
        }
        filter.setNativeQuery(nativeQuery); // set it so we never attempt to
                                            // translate again.

        Matcher matcher = VAR_REGEX_GATEWAY_VARIABLE.matcher(nativeQuery);

        while(matcher.find())
        {
            String field = matcher.group(1);
            if (RemedyxFilter.LAST_VALUE.equals(field))
            {
                String tmpField = "\\$\\{" + RemedyxFilter.LAST_VALUE + "\\}";
                nativeQuery = nativeQuery.replaceFirst(tmpField, filter.getLastValue());
            }
            if (RemedyxFilter.GATEWAY_USER.equals(field))
            {
                String tmpField = "\\$\\{" + RemedyxFilter.GATEWAY_USER + "\\}";
                nativeQuery = nativeQuery.replaceFirst(tmpField, remedyxConfig.getUsername());
            }
        }

        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        Log.log.debug("Remedy Query to execute:::" + nativeQuery);

        Integer lastValueField = com.bmc.arsys.api.Constants.AR_CORE_MODIFIED_DATE;
        if (filter.getLastValueField() != null && filter.getLastValueField().matches("\\d+"))
        {
	        lastValueField = new Integer(filter.getLastValueField());
        }
        result = search(filter.getFormName(), nativeQuery, forms.get(filter.getFormName()).getFieldIds(), filter.getLimit(), lastValueField, Boolean.TRUE, server);

        return result;
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
        if (result.containsKey("" + com.bmc.arsys.api.Constants.AR_CORE_ENTRY_ID))
        {
            event.put(Constants.EXECUTE_ALERTID, result.get("" + com.bmc.arsys.api.Constants.AR_CORE_ENTRY_ID));
        }
        if (result.containsKey(INCIDENT_NUMBER_FIELD_ID))
        {
            event.put(Constants.EXECUTE_REFERENCE, result.get(INCIDENT_NUMBER_FIELD_ID));
        }
    }

    /*
     * Public API method implementation goes here
     */

    public String create(String form, String fieldValueStr, String username, String password) throws Exception
    {
        Map<String, String> fieldValues = StringUtils.stringToMap(fieldValueStr, "=", "&");
        return create(form, fieldValues, username, password);
    } // create

    public String create(String formName, Map<String, String> data, String username, String password) throws Exception
    {
        String entryId = "";

        ARServerUser server = null;

        try
        {
            server = connect(username, password);
            Entry newEntry = new Entry();
            for (String key : data.keySet())
            {
                int intKey = Integer.valueOf(key);
                newEntry.put(intKey, new Value(data.get(key)));
            }

            entryId = server.createEntry(formName, newEntry);
            Log.log.debug("Entry create in the form " + formName + " with EntryId " + entryId);
        }
        catch (Exception e)
        {
            Log.log.error("Could not create the form " + formName);
            throw e;
        }
        finally
        {
            disconnect(server);
        }

        return entryId;
    }

    public void update(String formName, String entryId, String fieldValueStr, String username, String password) throws Exception
    {
        Map<String, String> fieldValues = StringUtils.stringToMap(fieldValueStr, "=", "&");
        update(formName, entryId, fieldValues, username, password);
    } // update

    public void update(String formName, String entryId, Map<String, String> data, String username, String password) throws Exception
    {
        ARServerUser server = null;

        try
        {
            server = connect(username, password);
            
            Entry entry = new Entry();
            for (String key : data.keySet())
            {
                int intKey = Integer.valueOf(key);
                entry.put(intKey, new Value(data.get(key)));
            }

            server.setEntry(formName, entryId, entry, null, 0);
            Log.log.debug("Entry updated successfully");
        }
        catch (Exception e)
        {
            Log.log.error("Entry could not be updated with for the form " + formName + " with EntryId " + entryId);
            throw e;
        }
        finally
        {
            disconnect(server);
        }
    }

    public void updateStatus(String formName, String entryId, String status, String username, String password) throws Exception
    {
        Map<String, String> data = new HashMap<String, String>();
        data.put("" + com.bmc.arsys.api.Constants.AR_CORE_STATUS, status);
        update(formName, entryId, data, username, password);
    }

    public void delete(String formName, String entryId, String username, String password) throws Exception
    {
        ARServerUser server = null;

        try
        {
            server = connect(username, password);

            server.deleteEntry(formName, entryId, 0);
        }
        catch (Exception e)
        {
            Log.log.error("Could not delete entry for the form " + formName + " with EntryId " + entryId);
            throw e;
        }
        finally
        {
            disconnect(server);
        }
    }

    public Map<String, String> selectByEntryID(String formName, String entryId, String username, String password) throws Exception
    {
        Map<String, String> result = new HashMap<String, String>();

        ARServerUser server = null;

        try
        {
            server = connect(username, password);

            Entry entry = server.getEntry(formName, entryId, null);
            if (entry == null)
            {
                Log.log.debug("No data found for ID#" + entryId);
                return result;
            }
            else
            {
                // Retrieve all properties of fields in the entry.
                Set<Integer> fieldIds = entry.keySet();
                for (Integer fieldId : fieldIds)
                {
                    // Field field = server.getField(formName,
                    // fieldId.intValue());
                    Value value = entry.get(fieldId);
                    // RemedyFormField remedyFormField = new
                    // RemedyFormField(fieldId, field.getName().toString(),
                    // field.getFieldType(), field.getDataType(),
                    // value.toString());
                    // result.add(remedyFormField);
                    result.put("" + fieldId, value.toString());
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Problem while querying for " + formName + " by entry ID " + entryId + "\n" + e.getMessage(), e);
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            disconnect(server);
        }
        return result;
    }

    private List<Map<String, String>> search(String formName, String criteria, int[] fieldIds, int maxResults, Integer sortFieldId, Boolean isAscending, ARServerUser server) throws Exception
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        try
        {
            List<Field> fields = server.getListFieldObjects(formName);

            // Create the search qualifier.
            QualifierInfo qual = server.parseQualification(criteria, fields, null, com.bmc.arsys.api.Constants.AR_QUALCONTEXT_DEFAULT);

            OutputInteger nMatches = new OutputInteger();
            List<SortInfo> sortOrder = new ArrayList<SortInfo>();
            if(sortFieldId != null)
            {
                if(isAscending != null && isAscending)
                {
                    sortOrder.add(new SortInfo(sortFieldId, com.bmc.arsys.api.Constants.AR_SORT_ASCENDING));
                }
                else
                {
                    sortOrder.add(new SortInfo(sortFieldId, com.bmc.arsys.api.Constants.AR_SORT_DESCENDING));
                }
            }

            if (fieldIds == null || fieldIds.length == 0)
            {
                RemedyxForm form = forms.get(formName);
                if(form == null)
                {
                    throw new Exception("Form: " + formName + ", not found have you deployed it to the RSRemote?");
                }
                else
                {
                    fieldIds = forms.get(formName).getFieldIds();
                }
            }

            // Retrieve entries from the form using the given qualification.
            if(maxResults <= 0)
            {
                Log.log.debug("Consumer asking to get all the records, maxResult is passed: " + maxResults);
                maxResults = com.bmc.arsys.api.Constants.AR_NO_MAX_LIST_RETRIEVE;
            }
            else
            {
                Log.log.debug("Consumer asking to get " + maxResults + " records");
            }
            //List<Entry> entryList = server.getListEntryObjects(formName, qual, 0, com.bmc.arsys.api.Constants.AR_NO_MAX_LIST_RETRIEVE, sortOrder, fieldIds, true, nMatches);
            List<Entry> entryList = server.getListEntryObjects(formName, qual, 0, maxResults, sortOrder, fieldIds, true, nMatches);
            Log.log.debug("Query returned " + nMatches + " matches.");
            if (nMatches.intValue() > 0)
            {
                Log.log.debug("Returned list size: " + entryList.size());
                for (Entry entry : entryList)
                {
                    Map<String, String> data = new HashMap<String, String>();
                    Set<Integer> keys = entry.keySet();
                    for (Integer key : keys)
                    {
                        // Log.log.debug("KEY: %d, VALUE: %s\n", key,
                        // entry.get(key).toString());
                        Value value = entry.get(key);
                        if (value.getDataType().equals(com.bmc.arsys.api.Constants.AR_DATA_TYPE_TIME))
                        {
                            Timestamp val = (Timestamp) value.getValue();
                            data.put(String.valueOf(key), "" + val.getValue());
                        }
                        else
                        {
                            data.put(String.valueOf(key), entry.get(key).toString());
                        }
                    }
                    result.add(data);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Problem while querying form: " + formName + " using query: " + criteria + "\n" + e.getMessage(), e);
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }

    public List<Map<String, String>> search(String formName, String criteria, String fieldIds, int maxResults, Integer sortFieldId, Boolean isAscending, String username, String password) throws Exception
    {
        int[] fields = {};

        if (StringUtils.isNotEmpty(fieldIds))
        {
            if (forms.containsKey(formName))
            {
                fields = forms.get(formName).getFieldIds();
            }
            else
            {
                throw new Exception("Make sure that the form :" + formName + " is created and deployed to the RSRemote using the Remedy Gateway UI.");
            }
        }
        return search(formName, criteria, fields, maxResults, sortFieldId, isAscending, username, password);
    }

    public List<Map<String, String>> search(String formName, String criteria, int[] fieldIds, int maxResults, Integer sortFieldId, Boolean isAscending, String username, String password) throws Exception
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        ARServerUser server = null;
        try
        {
            // Retrieve the detail info of all fields from theform.
            server = connect(username, password);
            result = search(formName, criteria, fieldIds, maxResults, sortFieldId, isAscending, server);
        }
        catch (Exception e)
        {
            Log.log.error("Problem while querying form: " + formName + " using query: " + criteria + "\n" + e.getMessage(), e);
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            disconnect(server);
        }

        return result;
    }

    public List<String> find(String formName, String query, String username, String password) throws Exception
    {
        List<String> result = new ArrayList<String>();

        ARServerUser server = null;
        try
        {
            // Retrieve the detail info of all fields from theform.
            server = connect(username, password);

            List<Field> fields = server.getListFieldObjects(formName);

            // Create the search qualifier.
            QualifierInfo qual = server.parseQualification(query, fields, null, com.bmc.arsys.api.Constants.AR_QUALCONTEXT_DEFAULT);

            OutputInteger nMatches = new OutputInteger();
            List<SortInfo> sortOrder = new ArrayList<SortInfo>();
            sortOrder.add(new SortInfo(com.bmc.arsys.api.Constants.AR_CORE_LAST_MODIFIED_BY, com.bmc.arsys.api.Constants.AR_SORT_ASCENDING));

            int[] fieldIds = { com.bmc.arsys.api.Constants.AR_CORE_ENTRY_ID };

            // Retrieve entries from the form using the given qualification.
            List<Entry> entryList = server.getListEntryObjects(formName, qual, 0, com.bmc.arsys.api.Constants.AR_NO_MAX_LIST_RETRIEVE, sortOrder, fieldIds, true, nMatches);
            Log.log.debug("Query returned " + nMatches + " matches.");
            if (nMatches.intValue() > 0)
            {
                for (Entry entry : entryList)
                {
                    result.add(entry.getEntryId());
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Problem while querying form: " + formName + " using query: " + query + "\n" + e.getMessage(), e);
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            disconnect(server);
        }

        return result;
    }
} // RemedyxGateway
