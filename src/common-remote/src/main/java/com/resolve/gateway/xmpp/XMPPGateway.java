/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.xmpp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;

import com.resolve.gateway.BaseSocialGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MXMPP;
import com.resolve.rsremote.ConfigReceiveXMPP;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class XMPPGateway extends BaseSocialGateway
{
    // Singleton
    private static volatile XMPPGateway instance = null;

    // This will be set in initialize() with the proper queue name from the
    // configuration.
    private String queue = "XMPP";
    //private Connection connection = null;
    private boolean isSocialPost;

    protected ConcurrentHashMap<String, XMPPAddress> xmppAddresses = new ConcurrentHashMap<String, XMPPAddress>();
    private ConcurrentHashMap<String, Connection> connections = new ConcurrentHashMap<String, Connection>();
    private ConcurrentHashMap<String, XMPPPacketListener> packetListeners = new ConcurrentHashMap<String, XMPPPacketListener>();
    private final XMPPMessageListener xmppMessageListener = new XMPPMessageListener();

    public static synchronized XMPPGateway getInstance(ConfigReceiveXMPP config)
    {
        if (instance == null)
        {
            instance = new XMPPGateway(config);
        }
        return (XMPPGateway) instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static XMPPGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("XMPP Gateway is not initialized correctly..");
        }
        else
        {
            return (XMPPGateway) instance;
        }
    }

    /**
     * Private constructor.
     *
     * @param config
     */
    private XMPPGateway(ConfigReceiveXMPP config)
    {
        // call super constructor for common initialization.
        super(config);
        queue = config.getQueue();
    } // XMPPGateway

    @Override
    public String getLicenseCode()
    {
        return "XMPP";
    }

    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_XMPP;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MXMPP.class.getSimpleName();
    }

    @Override
    protected Class<MXMPP> getMessageHandlerClass()
    {
        return MXMPP.class;
    }

    public boolean isSocialPoster()
    {
        return isSocialPost;
    }

    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    /**
     * Initializes the specific gateway resources.
     *
     * @param configurations
     */
    @Override
    protected void initialize()
    {
        ConfigReceiveXMPP config = (ConfigReceiveXMPP) configurations;

        this.queue = config.getQueue().toUpperCase();
        this.isSocialPost = config.isSocialpost();
        this.gatewayConfigDir = "/config/xmpp/";

        if (config.isActive() && config.isPrimary())
        {
            registerDefaultListener(config);
        }
    }

    /**
     * Re-initializes the specific gateway resources. This
     * is called if the gateway changes its state from secondary 
     * to primary.
     *
     * @param configurations
     */
    @Override
    public void reinitialize()
    {
        super.reinitialize();
        ConfigReceiveXMPP xmppConfigurations = (ConfigReceiveXMPP) configurations;

        if (xmppConfigurations.isActive() && isPrimary())
        {
            registerDefaultListener(xmppConfigurations);
        }
    }

    public void deactivate()
    {
        super.deactivate();

        Log.log.warn("Deactivating XMPP Gateway.");
        if (connections != null)
        {
            for (Connection connection : connections.values())
            {
                // disconnect from the XMPP service.
                connection.disconnect();
            }
        }
    }

    /**
     * This method registers a listener to the XMPP service.
     *
     * @param xmppConfigurations
     */
    private void registerDefaultListener(ConfigReceiveXMPP xmppConfigurations)
    {
        ConnectionConfiguration connectionConfig = new ConnectionConfiguration(xmppConfigurations.getServer(), xmppConfigurations.getPort(), xmppConfigurations.getService());
        connectionConfig.setCompressionEnabled(true);
        XMPPConnection connection = new XMPPConnection(connectionConfig);
        connectionConfig.setSASLAuthenticationEnabled(xmppConfigurations.isSasl());
        if (xmppConfigurations.isSasl() && !StringUtils.isEmpty(xmppConfigurations.getSaslmechanism()))
        {
            SASLAuthentication.supportSASLMechanism(xmppConfigurations.getSaslmechanism(), 0);
        }

        try
        {
            connection.connect();
            Log.log.debug("Connected to " + connection.getHost());

            connection.login(xmppConfigurations.getUsername(), xmppConfigurations.getP_assword());
            Log.log.debug("Default XMPP gateway login as " + xmppConfigurations.getUsername());

            XMPPPacketListener defaultPacketListener = new XMPPPacketListener();
            connection.addPacketListener(defaultPacketListener, null);
            connections.put(xmppConfigurations.getUsername(), connection);
            packetListeners.put(xmppConfigurations.getUsername(), defaultPacketListener);
        }
        catch (XMPPException ex)
        {
            Log.log.error("Failed to connect to : " + connection.getHost(), ex);
        }
    }

    /**
     * This method registers a listener to the XMPP service.
     *
     * @param xmppConfigurations
     */
    private void registerAdditionalListener(ConfigReceiveXMPP xmppConfigurations, ConcurrentHashMap<String, XMPPAddress> newXmppAddresses)
    {
        ConnectionConfiguration connectionConfig = new ConnectionConfiguration(xmppConfigurations.getServer(), xmppConfigurations.getPort(), xmppConfigurations.getService());
        connectionConfig.setCompressionEnabled(true);
        connectionConfig.setSASLAuthenticationEnabled(xmppConfigurations.isSasl());
        if (xmppConfigurations.isSasl() && !StringUtils.isEmpty(xmppConfigurations.getSaslmechanism()))
        {
            SASLAuthentication.supportSASLMechanism(xmppConfigurations.getSaslmechanism(), 0);
        }

        try
        {
            //now add the other accounts
            for (String address : newXmppAddresses.keySet())
            {
                if(!connections.containsKey(address))
                {
                    XMPPAddress indieAddress = newXmppAddresses.get(address);
                    XMPPConnection indieConnection = new XMPPConnection(connectionConfig);
                    indieConnection.connect();
                    indieConnection.login(indieAddress.getXmppAddress(), indieAddress.getXmppPassword());
                    Log.log.debug("Additional XMPP gateway login as " + address);
    
                    XMPPPacketListener indiePacketListener = new XMPPPacketListener();
                    indieConnection.addPacketListener(indiePacketListener, null);
                    connections.put(address, indieConnection);
                    packetListeners.put(address, indiePacketListener);
                }
            }
        }
        catch (XMPPException ex)
        {
            Log.log.error("Failed to connect to : " + xmppConfigurations.getServer(), ex);
        }
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new XMPPFilter((String) params.get(XMPPFilter.ID), (String) params.get(XMPPFilter.ACTIVE), (String) params.get(XMPPFilter.ORDER), (String) params.get(XMPPFilter.INTERVAL), (String) params.get(XMPPFilter.EVENT_EVENTID)
        // , (String) params.get(XMPPFilter.PERSISTENT_EVENT)
                        , (String) params.get(XMPPFilter.RUNBOOK), (String) params.get(XMPPFilter.SCRIPT), (String) params.get(XMPPFilter.REGEX));
    } // getFilter

    @Override
    public void start()
    {
        Log.log.warn("Starting XMPP Gateway Filter Listener");
        super.start();
    } // start

    @Override
    public void stop()
    {
        Log.log.warn("Stopping XMPP Gateway.");
        super.stop();
        
        deactivate();
    } // start

    @Override
    public void run()
    {
        //This call is must for all the ClusteredGateways.
        super.sendSyncRequest();
    } // run

    /**
     * This class acts as a listener to the XMPP service to receive message.
     */
    private class XMPPPacketListener implements PacketListener
    {
        @Override
        public void processPacket(Packet packet)
        {
            //do not process message if this is not active or primary
            //this check is required because some time due to network
            //latency etc. the packet listener may not get turned off.
            if (isActive() && isPrimary())
            {
                if (packet != null && packet instanceof Message)
                {
                    Message message = (Message) packet;
                    if (message.getType().equals(Message.Type.chat) && message.getBody() != null)
                    {
                        Map<String, String> map = new HashMap<String, String>();
                        map.put("FROM", message.getFrom());
                        map.put("TO", message.getTo());
                        map.put("SUBJECT", message.getSubject());
                        map.put("BODY", message.getBody());
                        map.put("CHAT_THREAD", message.getThread());
                        Log.log.debug("Received message from the server : " + map);
                        processData(map);
                    }
                }
            }
        }
    }

    /**
     * This class acts as a listener to the XMPP service to receive message.
     */
    private class XMPPMessageListener implements MessageListener
    {
        @Override
        public void processMessage(Chat arg0, Message arg1)
        {
            //nothing to do
        }
    }

    /**
     * This method processes the message received from the XMPP server. It
     * checks the message with any interested filter and process the filter.
     *
     * @param message
     */
    protected void processData(Object data)
    {
        long startTime = System.currentTimeMillis();
        try
        {
            Map<String, String> map = (Map<String, String>) data;

            String message = map.get("BODY");

            if (isSocialPost && StringUtils.isNotBlank(message))
            {
                String to = map.get("TO");
                String from = map.get("FROM");
                postToSocial(to, from, null, message);
            }

            for (Filter filter : orderedFilters)
            {
                if (filter.isActive())
                {
                    Log.log.trace("Running filter: " + filter.getId());
                    if (!StringUtils.isEmpty(message))
                    {
                        XMPPFilter xmppFilter = (XMPPFilter) filter;
                        String regex = xmppFilter.getRegex().toLowerCase();
                        Log.log.trace("Regex: " + regex);
                        Pattern pattern = Pattern.compile(regex);
                        Matcher matcher = pattern.matcher(message.toLowerCase());
                        if (!StringUtils.isEmpty(regex))
                        {
                            if (regex.equals("*") || matcher.find())
                            {
                                Map<String, String> runbookParams = new HashMap<String, String>();
                                runbookParams.putAll(map);
                                //runbookParams.put("MESSAGE", message);
                                runbookParams.put(Constants.EVENT_EVENTID, xmppFilter.getEventEventId());

                                // The message may have contents that might
                                // indicate that the
                                // sender intends to execute an event as well.
                                addEvent(message, runbookParams);

                                //processFilter(filter, matcher, runbookParams);
                                addToPrimaryDataQueue(xmppFilter, runbookParams);
                            }
                        }
                    }
                }
            }

            // process events collected by processFilter.
            //processFilterEvents(startTime);

            Thread.sleep(interval);
        }
        catch (Exception e)
        {
            Log.log.warn("Error: " + e.getMessage(), e);
            try
            {
                Thread.sleep(interval);
            }
            catch (InterruptedException ie)
            {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
    }

    /*
    @Override
    public void processMessage(Chat chat, Message message)
    {
        if (message != null)
        {
            Log.log.debug("Message received:" + message.toString());
            Map<String, String> map = new HashMap<String, String>();
            map.put("FROM", message.getFrom());
            map.put("TO", message.getTo());
            map.put("SUBJECT", message.getSubject());
            map.put("BODY", message.getBody());
            map.put("CHAT_THREAD", message.getThread());
            Log.log.debug("Received message from the server : " + map);
            processData(map);
        }
    }
    */

    public Map<String, XMPPAddress> getXMPPAddresses()
    {
        return xmppAddresses;
    } // getXMPPAddresses

    public void setXMPPAddresses(Map<String, Object> params)
    {
        XMPPAddress xmppAddress = getXMPPAddress(params);

        // remove existing emailAddress
        removeXMPPAddress(xmppAddress.getXmppAddress());

        // update emailAddress
        xmppAddresses.put(xmppAddress.getXmppAddress(), xmppAddress);
    }

    public void removeXMPPAddress(String id)
    {
        XMPPAddress emailAddress = xmppAddresses.get(id);
        if (emailAddress != null)
        {
            xmppAddresses.remove(id);
        }
    }

    public XMPPAddress getXMPPAddress(Map<String, Object> params)
    {
        return new XMPPAddress((String) params.get(XMPPAddress.XMPPADDRESS), (String) params.get(XMPPAddress.XMPPP_ASSWORD));
    }

    public void clearAndSetXMPPAddresses(List<Map<String, Object>> emailAddressesList)
    {
        ConcurrentHashMap<String, XMPPAddress> newXmppAddresses = new ConcurrentHashMap<String, XMPPAddress>();

        for (Map<String, Object> params : emailAddressesList)
        {
            //ignore the username Map
            if(!params.containsKey("RESOLVE_USERNAME"))
            {
                String name = (String) params.get("XMPPADDRESS");
                if(!connections.containsKey(name))
                {
                    newXmppAddresses.put(name, getXMPPAddress(params));
                    xmppAddresses.put(name, getXMPPAddress(params));
                    params.remove(XMPPAddress.XMPPP_ASSWORD);
                    Log.log.info("Adding XMPP Address: " + params);
                }
            }
        }

        registerAdditionalListener((ConfigReceiveXMPP) configurations, newXmppAddresses);
    } // clearAndSetXMPPAddresses

    /*
     * Public API method implementation goes here
     */
    public void sendMessage(String to, String message, String username, String password) throws Exception
    {
        if (isActive() && isPrimary())
        {
            ConfigReceiveXMPP xmppConfigurations = (ConfigReceiveXMPP) configurations;
            if (xmppConfigurations.isSasl() && !StringUtils.isEmpty(xmppConfigurations.getSaslmechanism()))
            {
                SASLAuthentication.supportSASLMechanism(xmppConfigurations.getSaslmechanism(), 0);
            }
            if (StringUtils.isBlank(username) && StringUtils.isBlank(username))
            {
                username = xmppConfigurations.getUsername();
                password = xmppConfigurations.getP_assword();
            }

            ConnectionConfiguration connectionConfig = new ConnectionConfiguration(xmppConfigurations.getServer(), xmppConfigurations.getPort(), xmppConfigurations.getService());
            connectionConfig.setCompressionEnabled(true);
            connectionConfig.setSASLAuthenticationEnabled(xmppConfigurations.isSasl());

            Connection xmppConnection = connections.get(username);

            boolean newConnection = false;

            // if the username and password is provided then create a brand new
            // connection, otherwise reuse the original connection.
            if (xmppConnection == null && StringUtils.isNotBlank(username))
            {
                xmppConnection = new XMPPConnection(connectionConfig);
                newConnection = true;

                //password may be empty, then try to find from our XMPP addresses
                //that were deployed.
                if (StringUtils.isBlank(password))
                {
                    if (xmppAddresses.containsKey(username))
                    {
                        password = xmppAddresses.get(username).getXmppPassword();
                    }
                }
                if (StringUtils.isNotBlank(password))
                {
                    try
                    {
                        xmppConnection.connect();
                        Log.log.debug("Connected to " + xmppConnection.getHost());

                        xmppConnection.login(username, password);
                        Log.log.debug("Logged in as " + xmppConnection.getUser());
                        newConnection = true;
                    }
                    catch (XMPPException ex)
                    {
                        Log.log.error("Failed to connect to : " + xmppConnection.getHost(), ex);
                    }
                }
            }

            //Chat chat = xmppConnection.getChatManager().createChat(to, this);
            if (xmppConnection != null)
            {
                Chat chat = xmppConnection.getChatManager().createChat(to, xmppMessageListener);
                Message msg = new Message();
                msg.setBody(message);
                chat.sendMessage(msg);

                if (newConnection && xmppConnection.isConnected())
                {
                    xmppConnection.disconnect();
                }
            }
        }
        else
        {
            Log.log.info("XMPP Gateway is not active. Cannot send message to chat user.");
        }
    }
} // XMPPGateway
