package com.resolve.gateway.tibcobespoke;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

public class TIBCOBespokeProcessMonitor implements Runnable
{
    TIBCOBespokeGateway gatewayInstance;
    
    public TIBCOBespokeProcessMonitor()
    {
        gatewayInstance = TIBCOBespokeGateway.getInstance();
    }

    @Override
    public void run()
    {
        ConcurrentHashMap<String, TIBCOBespokeProcess> heartbeatList = gatewayInstance.getHeartbeatList();        
        Iterator<Entry<String, TIBCOBespokeProcess>> heartIter = heartbeatList.entrySet().iterator();
        boolean restartNeeded = false;
        
        while(heartIter.hasNext())
        {
            Entry<String, TIBCOBespokeProcess> curHeart = heartIter.next();
            String curProcessId = curHeart.getKey();
            TIBCOBespokeProcess curProcess = curHeart.getValue();
            
            if(!curProcess.getDeletionFlag())
            {
                ArrayList<String> existingProcessIdList = JavaCommandLineJarExecutor.getActiveTibcoProcesses();
                if(!existingProcessIdList.contains(curProcessId))
                {
                    restartNeeded = true;
                    break;
                }
            }            
        }
        
        if(restartNeeded)
        {
            gatewayInstance.restartSubscriptions();
        }
    }
    
    
}
