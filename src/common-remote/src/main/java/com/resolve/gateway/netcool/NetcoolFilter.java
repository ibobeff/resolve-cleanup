/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.netcool;

import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.RetainValue;

public class NetcoolFilter extends BaseFilter
{
    public static final String SQL = "SQL";
    public static final String SQLFILE = "SQLFILE";
    public static final String SCRIPTFILE = "SCRIPTFILE";

    String sql;
    long lastQuery; // local timestamp (millis)
    long lastSuccessfulQuery; // remote OMNIbus timestamp (secs)
    long lastSerial; // serial of last successful query
    long lastServerSerial; // serverserial of last successful query

    public NetcoolFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String sql)
    {
        super(id, active, order, interval, eventEventId, runbook, script);
        setSql(sql);
        this.lastQuery = 0;
        this.lastSerial = 0;
        this.lastServerSerial = 0;
    } // NetcoolFilter

    public String getSql()
    {
        return sql;
    }

    public void setSql(String sql)
    {
        this.sql = sql != null ? sql.trim() : sql;
    }

    public long getLastQuery()
    {
        return lastQuery;
    }

    public void setLastQuery(long lastQuery)
    {
        this.lastQuery = lastQuery;
    }

    @RetainValue
    public long getLastSuccessfulQuery()
    {
        return lastSuccessfulQuery;
    }

    public void setLastSuccessfulQuery(long lastSuccessfulQuery)
    {
        this.lastSuccessfulQuery = lastSuccessfulQuery;
    }

    @RetainValue
    public long getLastSerial()
    {
        return lastSerial;
    }

    public void setLastSerial(long lastSerial)
    {
        this.lastSerial = lastSerial;
    }

    @RetainValue
    public long getLastServerSerial()
    {
        return lastServerSerial;
    }

    public void setLastServerSerial(long lastServerSerial)
    {
        this.lastServerSerial = lastServerSerial;
    }
} // NetcoolFilter
