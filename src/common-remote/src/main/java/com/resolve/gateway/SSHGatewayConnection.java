/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.io.File;
import java.util.List;

import com.resolve.connect.SSHConnectSSHJ;
import com.resolve.gateway.ssh.SSHGateway;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;

public class SSHGatewayConnection extends SSHConnectSSHJ
{
    private int timeoutCounter;
    private long unusedPoolStartTime;
    private long startTime;

    public long getUnusedPoolStartTime()
    {
        return unusedPoolStartTime;
    }

    public void setUnusedPoolStartTime(long expiry)
    {
        this.unusedPoolStartTime= expiry;
    }

    public long getStartTime()
    {
        return startTime;
    }

    public void setStartTime(long startTime)
    {
        this.startTime = startTime;
    }

    public int getTimeoutCounter()
    {
        return timeoutCounter;
    }

    public void increaseTimeoutCounter()
    {
        timeoutCounter++;
    }

    /*
     * USERNAME/PASSWORD
     */
    public SSHGatewayConnection(String username, String password, String hostname) throws Exception
    {
        this(username, password, hostname, 22, 0);
    }

    public SSHGatewayConnection(String username, String password, String hostname, int port) throws Exception
    {
        this(username, password, hostname, port, 0);
    }

    public SSHGatewayConnection(String username, String password, String hostname, int port, int timeout) throws Exception
    {
        this(username, password, hostname, port, timeout, -1, true);
    }

    public SSHGatewayConnection(String username, String password, String hostname, int port, int timeout, int keepAliveInterval) throws Exception
    {
        this(username, password, hostname, port, timeout, keepAliveInterval, true);
    }
    
    public SSHGatewayConnection(String username, String password, String hostname, int port, int timeout, int keepAliveInterval, boolean isConnect) throws Exception
    {
        super();
        startTime = DateUtils.getCurrentTimeInMillis();
        authMethod = P_ASSWORD;

        this.username = username;
        this.password = password;
        this.hostname = hostname;
        this.port = port;
        this.timeout = timeout * 1000;
        this.keepAliveInterval = keepAliveInterval;

        if (isConnect)
        {
            connect();
        }
    }

    /*
     * PUBLIC KEY
     */
    public SSHGatewayConnection(String username, String filename, String passphrase, String hostname) throws Exception
    {
        this(username, filename, passphrase, hostname, 22, 0);
    }

    public SSHGatewayConnection(String username, String filename, String passphrase, String hostname, int port) throws Exception
    {
        this(username, filename, passphrase, hostname, port, 0);
    }

    public SSHGatewayConnection(String username, String filename, String passphrase, String hostname, int port, int timeout) throws Exception
    {
        this(username, filename, passphrase, hostname, port, timeout, -1, true);
    }

    public SSHGatewayConnection(String username, String filename, String passphrase, String hostname, int port, int timeout, int keepAliveInterval) throws Exception
    {
        this(username, filename, passphrase, hostname, port, timeout, keepAliveInterval, true);
    }
    
    public SSHGatewayConnection(String username, String filename, String passphrase, String hostname, int port, int timeout, int keepAliveInterval, boolean isConnect) throws Exception
    {
        super();
        startTime = DateUtils.getCurrentTimeInMillis();
        authMethod = PUBLICKEY;

        File file = new File(filename);
        if (!file.exists())
        {
            throw new Exception("Unable to find private key file: " + file.getAbsolutePath());
        }

        this.username = username;
        this.privateKeyFile = file;
        this.privateKeyPassphrase = passphrase;
        this.hostname = hostname;
        this.port = port;
        this.timeout = timeout * 1000;
        this.keepAliveInterval = keepAliveInterval;

        if (isConnect)
        {
            connect();
        }
    }

    /*
     * INTERACTIVE
     */
    public SSHGatewayConnection(String username, List responses, String hostname) throws Exception
    {
        this(username, responses, hostname, 22);
    }

    public SSHGatewayConnection(String username, List responses, String hostname, int port) throws Exception
    {
        this(username, responses, hostname, port, 0);
    }

    public SSHGatewayConnection(String username, List responses, String hostname, int port, int timeout) throws Exception
    {
        this(username, responses, hostname, port, timeout, -1, true);
    }

    public SSHGatewayConnection(String username, List responses, String hostname, int port, int timeout, int keepAliveInterval) throws Exception
    {
        this(username, responses, hostname, port, timeout, keepAliveInterval, true);
    }
    
    public SSHGatewayConnection(String username, List responses, String hostname, int port, int timeout, int keepAliveInterval, boolean isConnect) throws Exception
    {
        super();
        startTime = DateUtils.getCurrentTimeInMillis();
        authMethod = INTERACTIVE;

        this.username = username;
        this.responses = responses;
        this.hostname = hostname;
        this.port = port;
        this.timeout = timeout * 1000;
        this.keepAliveInterval = keepAliveInterval;

        if (isConnect)
        {
            connect();
        }
    }

    /*
     * NONE - no SSH authentication
     */
    public SSHGatewayConnection(String username, String hostname) throws Exception
    {
        this(username, hostname, 22, 0);
    }

    public SSHGatewayConnection(String username, String hostname, int port) throws Exception
    {
        this(username, hostname, port, 0);
    }

    public SSHGatewayConnection(String username, String hostname, int port, int timeout) throws Exception
    {
        this(username, hostname, port, timeout, -1 ,true);
    }

    public SSHGatewayConnection(String username, String hostname, int port, int timeout, int keepAliveInterval) throws Exception
    {
        this(username, hostname, port, timeout, keepAliveInterval, true);
    }
    
    public SSHGatewayConnection(String username, String hostname, int port, int timeout, int keepAliveInterval, boolean isConnect) throws Exception
    {
        super();

        authMethod = NONE;

        this.username = username;
        this.password = "";
        this.hostname = hostname;
        this.port = port;
        this.timeout = timeout * 1000;

        if (isConnect)
        {
            connect();
        }
    }

    @Override
    public void close()
    {
        try
        {
            SSHGateway.getInstance().checkInConnection(this);
        }
        catch (Exception e)
        {
            Log.log.error("Could not check in the SSHGatewayConnection object.", e);
        }
    }
    public void closePoolConnection()
    {
    		super.close();
    }
    @Override
    public String toString()
    {
        return "SSHGatewayConnection [id=" + id + ", username=" + username + ", hostname=" + hostname + ", port=" + port + ", isClosed=" + isClosed + "]";
    }
}
