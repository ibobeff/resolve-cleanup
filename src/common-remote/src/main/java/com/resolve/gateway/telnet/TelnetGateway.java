/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.telnet;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.net.util.SubnetUtils;

import com.resolve.esb.ESBException;
import com.resolve.esb.MListener;
import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseGateway;
import com.resolve.gateway.MTelnet;
import com.resolve.gateway.TelnetGatewayConnection;
import com.resolve.gateway.ssh.SubnetMask;
import com.resolve.rsremote.ConfigReceiveTelnet;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.Constants;
import com.resolve.util.LockUtils;
import com.resolve.util.Log;

public class TelnetGateway extends BaseGateway
{
    // Singleton
    private static volatile TelnetGateway instance = null;

    // These maps keep track of all connections to a particular IP address.
    // IP address is the key in these Maps. The Map with TelnetGatewayConnection
    // stores
    // the TelnetGatewayConnection object's ID as the key. This is required
    // because there are different ways
    // to instantiate an TelnetGatewayConnection object (e.g., username/pwd,
    // passphrase etc.). Without the ID
    // we'll not no what to remove from the used Map.
    public static ConcurrentHashMap<String, ConcurrentHashMap<String, TelnetGatewayConnection>> used = new ConcurrentHashMap<String, ConcurrentHashMap<String, TelnetGatewayConnection>>();
    public static ConcurrentHashMap<String, ConcurrentHashMap<String, TelnetGatewayConnection>> unused = new ConcurrentHashMap<String, ConcurrentHashMap<String, TelnetGatewayConnection>>();

    // This will be set in initialize() with the proper queue name from the
    // configuration.
    private String queue = "TELNET";

    private int defaultPort = ((ConfigReceiveTelnet) configurations).getPort();
    private int defaultTimeout = ((ConfigReceiveTelnet) configurations).getTimeout();
    // milliseconds it wait in case a pool is full.
    private int interval = ((ConfigReceiveTelnet) configurations).getInterval() * 1000;
    // private int timeoutCounter = ((ConfigReceiveTelnet)
    // configurations).getTimeoutcounter();

    protected ConcurrentHashMap<String, SubnetMask> pools = new ConcurrentHashMap<String, SubnetMask>();

    public static TelnetGateway getInstance(ConfigReceiveTelnet config)
    {
        if (instance == null)
        {
            instance = new TelnetGateway(config);
        }
        return (TelnetGateway) instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static TelnetGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("Telnet Pool Gateway is not initialized correctly..");
        }
        else
        {
            return (TelnetGateway) instance;
        }
    }

    /**
     * Find the pool size based on Subnet defined in the "POOL" element in
     * config.xml
     *
     * @param ipAddress
     *            of the host
     * @param defaultPoolSize
     * @return
     */
    private int getPoolSize(String ipAddress, boolean isMax, int defaultPoolSize)
    {
        int result = defaultPoolSize;

        SubnetUtils subnetUtils = null;
        for (String mask : pools.keySet())
        {
            if (mask.contains("/")) // is a CIDR type subnet mask (like
                                    // 255.255.0.0/16)
            {
                subnetUtils = new SubnetUtils(mask);
            }
            else
            {
                subnetUtils = new SubnetUtils(ipAddress, mask);
            }

            if (subnetUtils.getInfo().isInRange(ipAddress))
            {
                SubnetMask subnetMask = pools.get(mask);
                if (isMax)
                {
                    result = subnetMask.getMaxConnection();
                }
                else
                {
                    result = 1;
                }
                Log.log.debug("The IP Address fall in the Subnet Mask : " + mask);
                break; // since we found it get out.
            }
        }
        return result;
    }

    /**
     * Find the timeout based on Subnet defined in the "POOL" element in
     * config.xml
     *
     * @param ipAddress
     *            of the host
     * @param defaultTimeout
     * @return
     */
    private int getTimeout(String ipAddress, int defaultTimeout)
    {
        int result = defaultTimeout;

        SubnetUtils subnetUtils = null;
        for (String mask : pools.keySet())
        {
            if (mask.contains("/")) // is a CIDR type subnet mask (like
                                    // 255.255.0.0/16)
            {
                subnetUtils = new SubnetUtils(mask);
            }
            else
            {
                subnetUtils = new SubnetUtils(ipAddress, mask);
            }

            if (subnetUtils.getInfo().isInRange(ipAddress))
            {
                SubnetMask subnetMask = pools.get(mask);
                result = subnetMask.getTimeout();
            }
        }
        return result;
    }

    private TelnetGatewayConnection getConnection(Map<String, TelnetGatewayConnection> unusedConnections, Map<String, TelnetGatewayConnection> usedConnections, TelnetGatewayConnection telnetConnect)
    {
        TelnetGatewayConnection result = null;

        if (unusedConnections != null && unusedConnections.size() > 0)
        {
            for (String key : unusedConnections.keySet())
            {
                result = unusedConnections.get(key);
                if (result.equals(telnetConnect))
                {
                    if (result.isClosed())
                    {
                        unusedConnections.remove(key);
                        usedConnections.remove(key); // it may be there in used
                                                     // as well, remove it.
                    }
                    break; // got the object, exit.
                }
            }
        }
        return result;
    }

    private ConcurrentHashMap<String, TelnetGatewayConnection> getConnectionMap(ConcurrentHashMap<String, ConcurrentHashMap<String, TelnetGatewayConnection>> pool, String ipAddress)
    {
        ConcurrentHashMap<String, TelnetGatewayConnection> result = pool.get(ipAddress);

        if (result == null) // just initialize
        {
            result = new ConcurrentHashMap<String, TelnetGatewayConnection>();
        }

        return result;
    }

    private TelnetGatewayConnection checkOutConnection(String ipAddress, int port, String terminalType, int timeout, boolean suppressGAOption, boolean echoOption) throws Exception
    {
        Log.log.debug("Checking out connection for: " + ipAddress);

        TelnetGatewayConnection telnetConnect = null;

        String lockName = Constants.LOCK_POOLED_GATEWAY_CHECKOUT + ipAddress;
        ReentrantLock lock = LockUtils.getLock(lockName);

        try
        {
            lock.lock();

            ConfigReceiveTelnet telnetConfigurations = (ConfigReceiveTelnet) configurations;

            int maxPoolSize = getPoolSize(ipAddress, true, telnetConfigurations.getMaxconnection());

            // In the beginning don't connect the SSHCOnnect object.
            // This instantiation is just for comparison.
            telnetConnect = new TelnetGatewayConnection(ipAddress, port, timeout, terminalType, suppressGAOption, echoOption, false);

            ConcurrentHashMap<String, TelnetGatewayConnection> unusedConnections = getConnectionMap(unused, ipAddress);
            ConcurrentHashMap<String, TelnetGatewayConnection> usedConnections = getConnectionMap(used, ipAddress);

            // Get the connection from the unused.
            telnetConnect = getConnection(unusedConnections, usedConnections, telnetConnect);
            while (telnetConnect == null) // do not give up until a connection
                                          // is received
            {
                if (usedConnections.size() < maxPoolSize)
                {
                    telnetConnect = new TelnetGatewayConnection(ipAddress, port, timeout, terminalType, suppressGAOption, echoOption, true);
                }
                else
                {
                    Log.log.debug("Pool size limit encountered, waiting to get some back.");
                    Thread.sleep(interval);
                    telnetConnect = getConnection(unusedConnections, usedConnections, telnetConnect);
                }
            }

            // add to the used connections.
            usedConnections.putIfAbsent(telnetConnect.getId(), telnetConnect);
            unusedConnections.remove(telnetConnect.getId());

            // registering the object is important because ActionTask
            // execution mechanism need to return them in case of
            // exception/timeout etc.
            registerSessionObject(telnetConnect);

            if (Log.log.isTraceEnabled())
            {
                Log.log.trace("Checking out : " + telnetConnect.toString());
                Log.log.trace("Unused pool size: " + unusedConnections.size() + " for the host: " + ipAddress);
                Log.log.trace("Used pool size: " + usedConnections.size() + " for the host: " + ipAddress);
            }
        }
        catch (Throwable e)
        {
            Log.log.debug(e.getMessage(), e);
        }
        finally
        {
            lock.unlock();
            LockUtils.removeLock(lockName);
        }
        return telnetConnect;
    }

    public void checkInConnection(TelnetGatewayConnection telnetConnect) throws Exception
    {
        if (telnetConnect != null)
        {
            Log.log.debug("Checkin in connection: " + telnetConnect.toString());

            String lockName = Constants.LOCK_POOLED_GATEWAY_CHECKIN + telnetConnect.getHostname();
            ReentrantLock lock = LockUtils.getLock(lockName);

            try
            {
                lock.lock();
                String ipAddress = telnetConnect.getHostname();

                ConcurrentHashMap<String, TelnetGatewayConnection> unusedConnections = getConnectionMap(unused, ipAddress);
                ConcurrentHashMap<String, TelnetGatewayConnection> usedConnections = getConnectionMap(used, ipAddress);

                usedConnections.remove(telnetConnect.getId());

                if (!telnetConnect.isClosed())
                {
                    unusedConnections.putIfAbsent(telnetConnect.getId(), telnetConnect);
                }

                if (Log.log.isTraceEnabled())
                {
                    Log.log.trace("Checked in, unused pool size: " + unusedConnections.size() + " for the host: " + ipAddress);
                    Log.log.trace("Checked in, used pool size: " + usedConnections.size() + " for the host: " + ipAddress);
                }
            }
            catch (Throwable e)
            {
                Log.log.debug(e.getMessage(), e);
            }
            finally
            {
                lock.unlock();
                LockUtils.removeLock(lockName);
            }
        }

    }

    /**
     * Private constructor.
     *
     * @param config
     */
    private TelnetGateway(ConfigReceiveTelnet config)
    {
        // call super constructor for common initialization.
        super(config);
        queue = config.getQueue();
    }

    @Override
    public String getLicenseCode()
    {
        return "TELNET";
    }

    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_TELNET;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MTelnet.class.getSimpleName();
    }

    @Override
    protected Class<MTelnet> getMessageHandlerClass()
    {
        return MTelnet.class;
    }

    @Override
    public String getQueueName()
    {
        return queue;
    }

    /**
     * Initializes the specific gateway resources.
     *
     * @param configurations
     */
    @Override
    public void initialize()
    {
        ConfigReceiveTelnet telnetConfigurations = (ConfigReceiveTelnet) configurations;

        queue = telnetConfigurations.getQueue().toUpperCase();

        if (telnetConfigurations.isActive())
        {
            try
            {
                String topicName = getQueueName() + "_TOPIC";
                MainBase.main.mServer.createPublication(topicName);
                MainBase.main.mServer.subscribePublication(topicName, "com.resolve.gateway");
            }
            catch (ESBException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    @Override
    public void start()
    {
        Log.setCurrentContext(getClass().getSimpleName());
        Log.log.warn("Starting SSH Gateway with pools");

        // initialize resources
        try
        {
            init();
            
            //check the license for this gateway
            checkGatewayLicense();
            // schedule next license check every 30 minutes
            ScheduledExecutor.getInstance().executeRepeat(getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType(), this, "checkGatewayLicense", LICENSE_CHECK_INTERVAL, TimeUnit.MINUTES);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        } finally {
            Log.clearContext();
        }
    } // start

    @Override
    public void stop()
    {
        Log.log.warn("Stopping SSH Gateway.");
        // TODO destroy all the connection but logout before destroying
    } // start

    /**
     * Clears and set new pool.
     *
     * @param poolList
     */
    public void clearAndSetPools(List<Map<String, String>> poolList)
    {
        // remove all pools
        pools.clear();

        // set pools
        for (Map<String, String> params : poolList)
        {
            //ignore the username Map
            if(!params.containsKey("RESOLVE_USERNAME"))
            {
                String subnetMask = (String) params.get(SubnetMask.SUBNETMASK);
    
                // add pool
                pools.put(subnetMask, getSubnetMask(params));
                Log.log.info("Adding Telnet subnet pool: " + params);
            }
        }

        // create new pools
        /*synchronized (TelnetGateway.class)
        {
            // startupPools.clear();
            // startupPools.addAll(pools.values());
        }*/

    } // clearAndSetPools

    public void setPool(Map<String, String> params)
    {
        SubnetMask subnetMask = getSubnetMask(params);

        // remove existing pool
        removePool(subnetMask.getSubnetMask());

        // update pools
        pools.put(subnetMask.getSubnetMask(), subnetMask);
    } // setPool

    private SubnetMask getSubnetMask(Map<String, String> params)
    {
        return new SubnetMask((String) params.get(SubnetMask.SUBNETMASK), String.valueOf(params.get(SubnetMask.MAXCONNECTION)), String.valueOf(params.get(SubnetMask.TIMEOUT)));
    } // getPool

    public void removePool(String name)
    {
        SubnetMask pool = pools.get(name);

        if (pool != null)
        {
            // remove from pools
            pools.remove(name);
        }
    } // removePool

    public Map<String, SubnetMask> getPools()
    {
        return pools;
    } // getPools

    public TelnetGatewayConnection checkOutConnection(String ipAddress) throws Exception
    {
        int timeout = getTimeout(ipAddress, defaultTimeout);
        return checkOutConnection(ipAddress, defaultPort, null, timeout, true, true);
    }

    public TelnetGatewayConnection checkOutConnection(String ipAddress, int port) throws Exception
    {
        int timeout = getTimeout(ipAddress, defaultTimeout);
        return checkOutConnection(ipAddress, port, null, timeout, true, true);
    }

    public TelnetGatewayConnection checkOutConnection(String ipAddress, int port, String terminalType) throws Exception
    {
        int timeout = getTimeout(ipAddress, defaultTimeout);
        return checkOutConnection(ipAddress, port, terminalType, timeout, true, true);
    }

    public TelnetGatewayConnection checkOutConnection(String ipAddress, int port, String terminalType, boolean suppressGAOption, boolean echoOption) throws Exception
    {
        int timeout = getTimeout(ipAddress, defaultTimeout);
        return checkOutConnection(ipAddress, port, terminalType, timeout, suppressGAOption, echoOption);
    }
} // SSHGateway
