package com.resolve.gateway.util.dcs;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;

public class RawDataCollectionLogger extends BaseDataCollectionLogger<Object>
{

    private static final String SOURCE = "source";

    private ObjectMapper objectMapper;

    public RawDataCollectionLogger()
    {
        objectMapper = new ObjectMapper();
    }

    @Override
    protected Map<String, String> getPropertiesMap(String... properties)
    {
        Map<String, String> propertiesMap = new HashMap<>();
        propertiesMap.put(SOURCE, properties[0]);
        return propertiesMap;
    }

    @Override
    protected String getMessageType()
    {
        return "raw";
    }

    @Override
    protected Object transformData(Object data) throws Exception
    {
        String transformedData = objectMapper.writeValueAsString(data);

        return transformedData;
    }

}
