/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.List;
import java.util.Map;

import com.resolve.gateway.remedyx.RemedyxGateway;
import com.resolve.gateway.servicenow.ServiceNowFilter;
import com.resolve.gateway.servicenow.ServiceNowGateway;

/**
 * This class is used by the Message Listener and invokes it methods. Messages
 * destined for the gateways set this class name in the message header.
 *
 */
public class MServiceNow extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MServiceNow.setFilters";

    private static final ServiceNowGateway instance = ServiceNowGateway.getInstance();

    public MServiceNow()
    {
        super.instance = instance;
    }

    @Override
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link ServiceNowFilter} instance
     *            
     */
    @Override
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        if (instance.isActive())
        {
            ServiceNowFilter serviceNowFilter = (ServiceNowFilter) filter;
            filterMap.put(ServiceNowFilter.QUERY, serviceNowFilter.getQuery());
            filterMap.put(ServiceNowFilter.OBJECT, serviceNowFilter.getObject());
        }
    } // populateGatewaySpecificProperties
}
