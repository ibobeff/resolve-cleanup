/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.itm;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.util.FileUtils;
import com.resolve.rsbase.MainBase;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.rsremote.ConfigReceiveITM;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import groovy.lang.Binding;

public class ITMGateway
{
    private static final String ITM = "ITM";
    private static final String QUAD_B_SLASH = "\\\\";
    private static final char EMPTY_CHAR = ' ';
    private static final String TACMD_LISTSYSTEMS = "listsystems";
    private static final String NT = "NT";
    private static final String SINGLE_F_SLASH = "/";
    private static final String DBL_B_SLASH = "\\";
    private static final String EMPTY_STRING = "";
    private static final String COLON = ":";

    public static boolean active = true;

    public static volatile ConfigReceiveITM configurations;

    private String username;
    private String password;
    private String remoteUsername;
    private String agentName;
    private String commandLine;
    private String scriptContent;
    private String scriptFileExt;
    private String remoteDirectory;

    private boolean isLoggedIn = false;

    public ITMGateway()
    {
        if(!active)
        {
            Log.log.error("ITM Gateway is not active");
            throw new RuntimeException("ITM Gateway is not active");
        }
    }

    public ITMGateway(String username, String password)
    {
        if(active)
        {
            this.username = username;
            this.password = password;
        }
        else
        {
            Log.log.error("ITM Gateway is not active");
            throw new RuntimeException("ITM Gateway is not active");
        }
    }

    public ITMGateway(String username, String password, String remoteUsername, String agentName, String commandLine, String scriptContent, String scriptFileExt, String remoteDirectory)
    {
        if(active)
        {
            this.username = username;
            this.password = password;
            this.remoteUsername = remoteUsername;
            this.agentName = agentName;
            this.commandLine = commandLine;
            this.scriptContent = scriptContent;
            this.scriptFileExt = scriptFileExt;
            this.remoteDirectory = remoteDirectory;
            if (Log.log.isDebugEnabled())
            {
                Log.log.debug("ITMGateway Object Created with following:");
                Log.log.debug(" Agent name: " + agentName);
                Log.log.debug(" Username: " + username);
                Log.log.debug(" Password supplied?: " + StringUtils.isNotEmpty(password));
                Log.log.debug(" Remote Username: " + remoteUsername);
                Log.log.debug(" Command Line: " + commandLine);
                Log.log.debug(" Script Content: " + scriptContent);
                Log.log.debug(" Script File Ext: " + scriptFileExt);
                Log.log.debug(" Remote Directory: " + remoteDirectory);
            }
        }
        else
        {
            Log.log.error("ITM Gateway is not active");
            throw new RuntimeException("ITM Gateway is not active");
        }
    }

    /**
     * Initializes the gateway properties. Called by the rsremote suring
     * startup.
     * 
     * @param config
     */
    public static void init(ConfigReceiveITM config)
    {
        if (configurations == null)
        {
            configurations = config;
        }
        try
        {
            active = config.isActive();
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    }

    public static void setActive(boolean active)
    {
        ITMGateway.active = active;
    }

    public String getRemoteUsername()
    {
        return remoteUsername;
    }

    public void setRemoteUsername(String remoteUsername)
    {
        this.remoteUsername = remoteUsername;
    }

    public String getAgentName()
    {
        return agentName;
    }

    public void setAgentName(String agentName)
    {
        this.agentName = agentName;
    }

    public String getCommandLine()
    {
        return commandLine;
    }

    public void setCommandLine(String commandLine)
    {
        this.commandLine = commandLine;
    }

    public String getScriptContent()
    {
        return scriptContent;
    }

    public void setScriptContent(String scriptContent)
    {
        this.scriptContent = scriptContent;
    }

    public String getScriptFileExt()
    {
        return scriptFileExt;
    }

    public void setScriptFileExt(String scriptFileExt)
    {
        this.scriptFileExt = scriptFileExt;
    }

    public String getRemoteDirectory()
    {
        return remoteDirectory;
    }

    public void setRemoteDirectory(String remoteDirectory)
    {
        this.remoteDirectory = remoteDirectory;
    }

    public String getUsername()
    {
        if (StringUtils.isBlank(this.username))
        {
            return configurations.getUsername();
        }
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        // this is intentional, password dependent on username only.
        if (StringUtils.isBlank(this.username))
        {
            return configurations.getPassword();
        }
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * Simply verifies if the Agent in a Windows machine.
     * 
     * @param fullname
     * @return
     */
    private static boolean isNTAgent(String fullname)
    {
        return fullname.endsWith(NT);
    }

    private String getScriptFileName(String extension) throws Exception
    {
        // init extension, we may need to try to synthesize with
        // some smart assumptions by agent name. E.g., .bat for a Windows agent
        // or .ksh for a *nix agent?
        if (StringUtils.isBlank(extension))
        {
            extension = ".in";
        }
        return getRandomFileName(extension);
    } // createInputFile

    /**
     * Validate if the parameters are empty or null.
     * 
     * @param scriptName
     * @param script
     * @throws Exception
     */
    private static void validateRemoteScript(String agent, String scriptName, String script) throws Exception
    {
        StringBuilder error = new StringBuilder();
        if (StringUtils.isBlank(agent))
        {
            error.append("Agent name must be provided.\n");
        }
        if (StringUtils.isBlank(scriptName))
        {
            error.append("Script name must be provided.\n");
        }
        if (StringUtils.isBlank(script))
        {
            error.append("There is nothing to execute, please provide script content.\n");
        }
        if (error.length() > 0)
        {
            throw new Exception(error.toString());
        }
    }

    /**
     * Validate if the parameters are empty or null.
     * 
     * @param agent
     * @param command
     * @throws Exception
     */
    private static void validateRemoteCommand(String agent, String command) throws Exception
    {
        StringBuilder error = new StringBuilder();
        if (StringUtils.isBlank(agent))
        {
            error.append("Agent name must be provided.\n");
        }
        if (StringUtils.isBlank(command))
        {
            error.append("There is nothing to execute, please provide a command.\n");
        }
        if (error.length() > 0)
        {
            throw new Exception(error.toString());
        }
    }

    /**
     * This method simply appends OS specific suffix to the dir.
     * 
     * @param isWindows
     * @param dir
     * @return
     */
    private static String refurbishRemoteDir(boolean isWindows, String dir)
    {
        if (StringUtils.isBlank(dir))
        {
            dir = (isWindows ? configurations.getRemoteWindowsDir() : configurations.getRemoteUnixDir());
        }

        if (isWindows)
        {
            dir = StringUtils.replace(dir, DBL_B_SLASH, QUAD_B_SLASH);
            dir = StringUtils.replace(dir, SINGLE_F_SLASH, QUAD_B_SLASH);
            dir += (dir.endsWith(DBL_B_SLASH) ? EMPTY_STRING : DBL_B_SLASH);
        }
        else
        {
            dir = StringUtils.replace(dir, DBL_B_SLASH, SINGLE_F_SLASH);
            dir += (dir.endsWith(SINGLE_F_SLASH) ? EMPTY_STRING : SINGLE_F_SLASH);
        }
        return dir;
    }

    private static String cleanFile(boolean isWindows, String dir)
    {
        if (isWindows)
        {
            dir = StringUtils.replace(dir, DBL_B_SLASH, QUAD_B_SLASH);
            dir = StringUtils.replace(dir, SINGLE_F_SLASH, QUAD_B_SLASH);
        }
        else
        {
            dir = StringUtils.replace(dir, DBL_B_SLASH, SINGLE_F_SLASH);
        }
        return dir;
    }

    private void login() throws Exception
    {
        if (!isLoggedIn)
        {
            String command = "\"" + configurations.getCmd() + " login -s " + configurations.getServer() + " -u " + getUsername() + " -p " + getPassword() + " -t " + configurations.getLoginduration() + "\";\n";
            executeGroovyScript(command);
            isLoggedIn = true;
        }
    }

    private void logout(String username) throws Exception
    {
        String command = "\"" + configurations.getCmd() + " logout\";\n";
        executeGroovyScript(command);
        isLoggedIn = false;
    }

    /**
     * Synthesizes full agent name.
     * 
     * @param agent
     *            (e.g., resolve:LZ or resolve)
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    private String getFullAgentName(String agent) throws Exception
    {
        if (!agent.contains(COLON)) // user sending the full agent name
        {
            agent += COLON + getProductCode(agent);
        }
        return agent;
    }

    /**
     * This method extracts the product code (e.g., LZ, NT) from the output of
     * tacmd listsystems command.
     * 
     * @param agent
     *            caller could provide the fullname already (e.g. hostname:LZ)
     * @param listSystemOutput
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    private String getProductCode(String agent) throws Exception
    {
        String productCode = null;

        String listSystemsOutput = executeLocalCommand(TACMD_LISTSYSTEMS);
        Pattern patt = Pattern.compile(agent + ":[LZKUXNT][LZKUXNT].*");
        for (String line : listSystemsOutput.split("\n"))
        {
            Log.log.trace(line);
            Matcher m = patt.matcher(line);
            if (line != null && m.matches())
            {
                productCode = line.substring(0, line.indexOf(EMPTY_CHAR)).split(COLON)[1];
            }
        }
        if (StringUtils.isBlank(productCode))
        {
            throw new Exception("Product code not found for the Agent, please provide valid agent name!");
        }
        return productCode;
    }

    /**
     * Executes groovy script.
     * 
     * Note: due to an unknown issue with Java Runtime.exec(), we're using
     * groovy execute(). Using Java Runtime loses login knowelege and subsequent
     * command processing gives error such as user not logged in. With groovy
     * the issue is not there.
     * 
     * @param command
     * @return
     * @throws Exception
     */
    private static String executeGroovyScript(String command) throws Exception
    {
        String result = null;

        try
        {
            StringBuilder groovyScript = new StringBuilder();
            groovyScript.append("def command = " + command);
            groovyScript.append("def proc = command.execute();\n");
            groovyScript.append("def sout = new StringBuffer();\n");
            groovyScript.append("def serr = new StringBuffer();\n");
            groovyScript.append("proc.consumeProcessOutput(sout, serr);\n");
            groovyScript.append("proc.waitFor();\n");
            groovyScript.append("if (serr) return serr;\n");
            groovyScript.append("if (sout) return sout;\n");
            // groovyScript.append("println(\"sout:\" + sout);\n\n");
            // groovyScript.append("println(\"serr:\" + serr);\n\n");

            if (Log.log.isDebugEnabled())
            {
                Log.log.debug(groovyScript.toString());
            }

            Object response = GroovyScript.execute(groovyScript.toString(), "ITMGatewayGroovyScript", false, new Binding());
            if (response != null)
            {
                result = response.toString();
                Log.log.trace(result);
            }
            else
            {
                Log.log.info("Nothing returned!");
            }
        }
        catch (IOException e)
        {
            throw new Exception(e);
        }

        return result;
    }

    /**
     * Executes a command in the host server.
     * 
     * @param localCommand
     * @return
     * @throws Exception
     */
    private static String executeHostCommand(String... localCommand) throws Exception
    {
        StringBuilder command = new StringBuilder("[\"" + configurations.getCmd() + "\",");
        for (String cmd : localCommand)
        {
            if (cmd.contains(" "))
            {
                String[] splitted = cmd.trim().split(" ");
                for (String split : splitted)
                {
                    command.append("\"" + split + "\",");
                }
            }
            else
            {
                command.append("\"" + cmd + "\",");
            }
        }
        command.append("\"-d\"];");

        return executeGroovyScript(command.toString());
    }

    /**
     * Recreate the original command by wrapping it with "su".
     * 
     * @param remoteUsername
     * @param command
     * @return
     */
    private static String sudoer(String remoteUsername, String command)
    {
        return "su - " + remoteUsername + " -c \\\"" + command + "\\\"";
    }

    /**
     * This prepares a tacmd command.
     * 
     * @param fullname
     * @param absoluteFile
     * @param command
     * @param timeout
     * @return
     */
    private String getCommand(String fullname, String absoluteFile, String command)
    {
        StringBuilder commandString = new StringBuilder();
        commandString.append(" [\"" + configurations.getCmd() + "\",");
        commandString.append(" \"executecommand\", ");
        commandString.append(" \"-m\", ");
        commandString.append(" \"" + fullname + "\", ");
        commandString.append(" \"-d\", ");
        commandString.append(" \"" + absoluteFile + "\", ");
        commandString.append(" \"-c\", ");
        commandString.append(" \"" + command + "\", ");
        commandString.append(" \"-o\", ");
        commandString.append(" \"-e\", ");
        commandString.append(" \"-r\", ");
        commandString.append(" \"-l\", ");
        commandString.append(" \"-t\", ");
        commandString.append("\"" + configurations.getTimeout() + "\"];\n");
        return commandString.toString();
    }

    private String getRandomFileName(String ext)
    {
        return UUID.randomUUID().toString() + ext;
    }

    /**
     * Gets a list of available agent names from the server.
     * 
     * @param agentTypes
     *            is the list of agent types (e.g., LZ, NT etc.), do not set it
     *            to get the entire list.
     * @return
     * @throws Exception
     */
    public List<String> getAgentList(String... agentTypes) throws Exception
    {
        List<String> results = new ArrayList<String>();

        // Let's login first.
        login();

        String listSystemsOutput = null;
        List<String> commands = new ArrayList<String>();
        commands.add(TACMD_LISTSYSTEMS);

        if (agentTypes != null && agentTypes.length > 0)
        {
            commands.add("-t");
            for (String agentType : agentTypes)
            {
                commands.add(agentType.toUpperCase());
            }
        }

        listSystemsOutput = executeHostCommand(commands.toArray(new String[0]));

        if (!StringUtils.isBlank(listSystemsOutput))
        {
            for (String line : listSystemsOutput.split("\n"))
            {
                Log.log.trace(line);
                if (line.contains(COLON))
                {
                    String agentName = line.substring(0, line.indexOf(EMPTY_CHAR));
                    results.add(agentName);
                }
            }
        }
        return results;
    }

    /**
     * Executes a command in the host server that is a TEMS server.
     * 
     * @param localCommand
     * @return the output of the command.
     * @throws Exception
     */
    public String executeLocalCommand(String localCommand) throws Exception
    {
        // Let's login first.
        login();
        return executeHostCommand(localCommand);
    }

    /**
     * Executes a command in an agent machine.
     * 
     * @param agent
     *            is the agent machine's name, caller may provide full name
     *            (e.g., agent:LZ) or the host name.
     * @param commandLine
     *            to be executed, must be compliant with the agent environment
     *            (e.g., "ls -l /root/test").
     * @param remoteUsername
     *            , this is a user we want to do "su" on a *nix agent. If this
     *            is provided then the gateway will execute the command with by
     *            changing the user (e.g., "su - <remoteUsername> -c <command>"
     *            on the agent.
     * @return the output of the command.
     * @throws Exception
     */
    public String executeRemoteCommand(String agent, String commandLine, String remoteUsername) throws Exception
    {
        String result = null;
        try
        {

            // throws Exception is validation fails.
            validateRemoteCommand(agent, commandLine);

            // Let's login first.
            login();

            String fullname = getFullAgentName(agent);

            String responseFileName = configurations.getOutputDir() + getRandomFileName(".out");

            StringBuilder groovyScript = new StringBuilder();

            if (!isNTAgent(fullname) && !StringUtils.isBlank(remoteUsername))
            {
                // We're going to recreate the original command by wrapping it
                // with "su"
                commandLine = sudoer(remoteUsername, commandLine);
            }
            groovyScript.append(getCommand(fullname, responseFileName, commandLine));

            executeGroovyScript(groovyScript.toString());

            File responseFile = new File(responseFileName);
            ErrorHandler.verifyStatus(responseFile);

            result = FileUtils.readFileToString(responseFile);

            // at the end delete the file.
            if (configurations.isDeleteOutputs())
            {
                FileUtils.deleteQuietly(responseFile);
            }
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        finally
        {
            logout(username);
        }

        return result;
    }

    /**
     * Executes a script in a remote agent machine. Additionally this may wrap
     * the script with the commandline.
     * 
     * @param agent
     *            machine's name, caller may give full (e.g., resolve:LZ) or
     *            just the host name.
     * @param commandLine
     *            if not empty then wraps the scriptFile (e.g., perl
     *            scriptFileName.pl).
     * @param scriptFileExtension
     *            (e.g., .bat or .ksh). Script content will be saved in a file
     *            name with this extension and executed. This is dependent on
     *            the target agent OS or expectation of the command defined in
     *            "commandLine" above.
     * @param scriptContent
     *            contents to be executed, must be compliant with the agent
     *            environment.
     * @param remoteDirectory
     *            where to put the script and execute.
     * @param remoteUsername
     *            , this is a user you want to do "su" on a *nix agent. If
     *            provided then the gateway will execute the command with
     *            "su remoteUsername command" on the agent.
     * @return the output of the script.
     * @throws Exception
     */
    public String executeRemoteScript(String agent, String commandLine, String scriptFileExtension, String scriptContent, String remoteDirectory, String remoteUsername) throws Exception
    {
        String result = null;

        try
        {

            // Generate a random file name with the extension.
            String scriptFileName = getScriptFileName(scriptFileExtension);

            // This will throw exception if one of these are empty.
            validateRemoteScript(agent, scriptFileName, scriptContent);

            // Let's login first.
            login();

            String fullname = getFullAgentName(agent);
            boolean isNTAgent = isNTAgent(fullname);

            remoteDirectory = refurbishRemoteDir(isNTAgent, remoteDirectory);

            String fullRemoteScriptFile = cleanFile(isNTAgent, remoteDirectory + scriptFileName);
            String fullRemoteScriptCommand = fullRemoteScriptFile;

            // First transfer the script into the remote machine.
            File scriptFile = new File(configurations.getScriptDir() + scriptFileName);
            // Save the script as a file in the local file system
            if (isNTAgent)
            {
                FileUtils.writeStringToFile(scriptFile, scriptContent);
            }
            else
            {
                Log.log.debug("Cleaning up linux script file for ^M character");
                FileUtils.writeStringToFile(scriptFile, scriptContent.replaceAll("\r", ""));
            }

            // put the file in the remote machine
            StringBuilder commandString = new StringBuilder();

            // command to transfer the script, overwrites it forcefully.
            String convertedPath = StringUtils.replace(scriptFile.getAbsolutePath(), DBL_B_SLASH, QUAD_B_SLASH);
            commandString.append(" [\"" + configurations.getCmd() + "\", \"putfile\", \"-m\", \"" + fullname + "\"," + "\"-s\", \"" + convertedPath + "\",\"-d\",\"" + fullRemoteScriptCommand + "\", \"-f\"];");
            // Execute the script using groovy.
            executeGroovyScript(commandString.toString());

            String responseFileName = configurations.getOutputDir() + getRandomFileName(EMPTY_STRING);
            File responseOut = new File(responseFileName + ".out");
            File responseChmod = new File(responseFileName + ".chmod");

            // if non Windows, run chmod
            if (!isNTAgent)
            {
                // Agent server must have chmod in the path. If not we'll either
                // have to append a parameter to this
                // asking for the actual location (e.g., /usr/bin/chmod) or do
                // gateway configuration
                String command = "chmod 755 " + fullRemoteScriptCommand;
                commandString.setLength(0);
                commandString.append(getCommand(fullname, responseChmod.getAbsoluteFile().toString(), command));
                executeGroovyScript(commandString.toString());
            }

            commandString.setLength(0);
            boolean isWindows = FileUtils.getFileSeparator().equals(DBL_B_SLASH);
            String responseOutFile = cleanFile(isWindows, responseOut.getAbsolutePath());

            if (!isNTAgent(fullname) && !StringUtils.isBlank(remoteUsername))
            {
                // recreate the original command by wrapping it with "su"
                fullRemoteScriptCommand = sudoer(remoteUsername, fullRemoteScriptCommand);
            }

            // Now wrap the file name with the commandLine.
            String commandLineWrapper = fullRemoteScriptCommand;
            if (!StringUtils.isBlank(commandLine))
            {
                commandLineWrapper = commandLine + " " + fullRemoteScriptCommand;
            }

            commandString.append(getCommand(fullname, responseOutFile, commandLineWrapper));
            executeGroovyScript(commandString.toString());

            result = FileUtils.readFileToString(responseOut);

            // At the end delete the files.
            if (configurations.isDeleteOutputs())
            {
                FileUtils.deleteQuietly(responseOut);
                if (responseChmod.exists())
                {
                    FileUtils.deleteQuietly(responseChmod);
                }
            }

            // Finally remove the script file from the remote agent machine.
            String deleteCommandLine = "";
            if (isNTAgent)
            {
                deleteCommandLine = "del /F " + fullRemoteScriptFile;
            }
            else
            {
                deleteCommandLine = "rm -f " + fullRemoteScriptFile;
            }
            String deleteResponseFileName = configurations.getOutputDir() + getRandomFileName(".out");
            StringBuilder deleteScript = new StringBuilder(getCommand(fullname, deleteResponseFileName, deleteCommandLine));
            executeGroovyScript(deleteScript.toString());
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        finally
        {
            logout(username);
        }

        return result;
    }

    /**
     * Executes commands or script on a remote agent. This is the common entry
     * point to the ITM Gateway. In order for this to work, you may have to
     * either provide the actual comand/script and any other information through
     * either the constructor or various setter methods.
     * 
     * Currently this gateway supports the following use cases:
     * 
     * a) Execute a single command (e.g., ls -l). b) Execute a script (e.g., a
     * bash script). c) Execute a script with a special command (e.g., perl
     * myperlfile.pl).
     * 
     * @return
     * @throws Exception
     */
    public String execute() throws Exception
    {
        String result = null;

        if (StringUtils.isBlank(getScriptContent()))
        {
            if (!StringUtils.isBlank(getCommandLine()))
            {
                result = executeRemoteCommand(getAgentName(), getCommandLine(), getRemoteUsername());
            }
        }
        else
        {
            result = executeRemoteScript(getAgentName(), getCommandLine(), getScriptFileExt(), getScriptContent(), getRemoteDirectory(), getRemoteUsername());
        }
        return result;
    }

    /**
     * This is a one off kind of check as ITM gateway is different from all others.
     * 
     * @throws Exception
     */
    public void checkGatewayLicense() throws Exception
    {
        boolean isValid = MainBase.main.getLicenseService().checkGatewayLicense(ITM, MainBase.main.configId.getGuid(), "", ITM, System.currentTimeMillis(), false, false);

        if(!isValid && active)
        {
            Log.log.info("Invalid gateway license, stopping " + ITM + " gateway.");
            setActive(false);
        }

        //it is possible that a license got uploaded and we can start the gateway now
        if(isValid && !active)
        {
            Log.log.info("Gateway license found, restarting " + ITM + " gateway.");
            setActive(true);
        }

        // schedule next license check
        ScheduledExecutor.getInstance().executeDelayed(this, "checkGatewayLicense", 30, TimeUnit.MINUTES);
    }

//    public static void main(String... args)
//    {
//        try
//        {
//            String username = args[0];
//            String password = args[1];
//            String remoteUser = args[2];
//            String agentName = "netcool:LZ";
//            if (args[3] != null)
//            {
//                agentName = args[3];
//            }
//
//            System.out.printf("Starting ITM test: %s, username: %s \n", username, agentName);
//
//            Log.init();
//            ConfigReceiveITM itmConfig = new ConfigReceiveITM(null);
//            itmConfig.setActive(true);
//
//            // itmConfig.setCmd("C:\\tools\\ITMSimulator\\bin\\tacmd.bat");
//            itmConfig.setServer("localhost");
//            itmConfig.setCmd("/opt/IBM/ITM/bin/tacmd");
//            itmConfig.setUsername(username);
//            itmConfig.setPassword(password);
//            itmConfig.setLoginduration(1440);
//            itmConfig.setTimeout(1200);
//            itmConfig.setScriptDir("/home/bdutta/itm/file/");
//            itmConfig.setOutputDir("/home/bdutta/itm/output/");
//            itmConfig.setRemoteUnixDir("/tmp");
//            itmConfig.setRemoteWindowsDir("c:\\temp");
//            itmConfig.setOutputDir("/home/bdutta/itm/output/");
//            itmConfig.setDeleteOutputs(false);
//
//            // initialize before accessing the static method.
//            ITMGateway.init(itmConfig);
//
//            try
//            {
//                ITMGateway itmGateway = new ITMGateway(username, password);
//
//                String result = "";
//
//                /*
//                 * System.out.println("Testing listsystems");
//                 * System.out.println("~~~~~~~~~~~~~~~~~~~"); String result =
//                 * itmGateway.executeLocalCommand("listsystems");
//                 * System.out.println
//                 * ("Result is:\n\n=====================================\n" +
//                 * result);
//                 */
//
//                System.out.println("Testing command ls -l w/o remote user");
//                System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//                result = itmGateway.executeRemoteCommand("netcool", "ls -l", null);
//                System.out.println("Result of executeCommand:\n\n=====================================\n" + result);
//
//                /*
//                 * System.out.println("Testing command ls -l with remote user");
//                 * System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//                 * result = itmGateway.executeRemoteCommand("netcool", "ls -l",
//                 * remoteUser); System.out.println(
//                 * "Result of executeCommand with remoteUser :\n\n=====================================\n"
//                 * + result);
//                 */
//
//                /*
//                 * System.out.println("Testing script w/o remote user");
//                 * System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//                 * StringBuilder script = new StringBuilder();
//                 * script.append("#!/bin/ksh\n");
//                 * script.append("mkdir mytmp\n");
//                 * //script.append("echo \"going to sleep for 5 minutes\"\n");
//                 * //script.append("sleep 130\n"); script.append("ls -l\n");
//                 * script.append("echo \"Test ITM 13\"\n");
//                 * 
//                 * result = itmGateway.executeRemoteScript(agentName, null,
//                 * "mytest1.ksh", script.toString(), "/home/bdutta", null);
//                 * System.out.println(
//                 * "Result of executeScript:\n\n=====================================\n"
//                 * + result);
//                 * 
//                 * System.out.println("Testing script with remote user");
//                 * System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"); result
//                 * = itmGateway.executeRemoteScript(agentName, null,
//                 * "mytest2.ksh", script.toString(), "/home/bdutta",
//                 * remoteUser); System.out.println(
//                 * "Result of executeScript with remoteUser:\n\n=====================================\n"
//                 * + result);
//                 * 
//                 * System.out.println("Testing agent list, no type");
//                 * System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//                 * List<String> agents1 = itmGateway.getAgentList(username,
//                 * username); System.out.println(
//                 * "Result of Agents1\n=====================================\n"
//                 * ); for(String agt : agents1) { System.out.println("Name: " +
//                 * agt); }
//                 * 
//                 * System.out.println("Testing agent list, type=LZ");
//                 * System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//                 * List<String> agents2 = itmGateway.getAgentList(username,
//                 * username, "LZ"); System.out.println(
//                 * "Result of Agents2\n=====================================\n"
//                 * ); for(String agt : agents2) { System.out.println("Name: " +
//                 * agt); }
//                 * 
//                 * System.out.println("Testing agent list, type=LZ, NT");
//                 * System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//                 * List<String> agents3 = itmGateway.getAgentList(username,
//                 * username, "LZ", "NT"); System.out.println(
//                 * "Result of Agents3\n=====================================\n"
//                 * ); for(String agt : agents3) { System.out.println("Name: " +
//                 * agt); }
//                 */
//            }
//            catch (Exception e)
//            {
//                System.out.println(e.getMessage());
//            }
//        }
//        catch (Exception e)
//        {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//    }
} // ITMGateway
