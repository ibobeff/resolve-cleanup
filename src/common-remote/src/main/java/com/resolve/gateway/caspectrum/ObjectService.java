/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.caspectrum;

import java.util.List;
import java.util.Map;

public interface ObjectService
{
    /**
     * Every object has its ID property, the subclass requires to implement this
     * method. Example: Service request could be "NUMBER".
     * 
     * @return
     */
    //String getIdPropertyName();

    /**
     * Identity of the object. Implementing class provides this. Example:
     * Incident is "incident" or Problem is "problem"
     * 
     * @return
     */
    //String getIdentity();

    /**
     * Creates an object by supplying username, password.
     *
     * @param object type
     * @param params
     * @param username
     * @param password
     * @return a Map with all the properties of the new object.
     * @throws Exception

    Map<String, String> create(String objectType, Map<String, String> params, String username, String password) throws Exception;
     */
    /**
     * Creates an object by supplying username, password.
     *
     * @param object type
     * @param parentObjectId
     *            such as SysId for an Incident. It's mandatory.
     * @param note
     *            to be added.
     * @param username
     * @param password
     * @return
     * @throws Exception

    boolean createWorknote(String objectType, String parentObjectId, String note, String username, String password) throws Exception;
     */
    /**
     * Updates an object by supplying username, password.
     *
     * @param object type
     * @param objectId
     *            is mandatory.
     * @param params
     * @param username
     * @param password
     * @return
     * @throws Exception

    void update(String objectType, String objectId, Map<String, String> params, String username, String password) throws Exception;
     */
    /**
     * Deletes an object by its id by supplying username, password.
     *
     * @param object type
     * @param objectId
     *            is mandatory.
     * @param username
     * @param password
     * @return
     * @throws Exception

    void delete(String objectType, String objectId, String username, String password) throws Exception;
     */
    /**
     * Retrieves object ids and specified attributes associated with retrieved objects. (Alarms/Devices)
     *
     * @param object type
     * @param attributes List of attributes
     *            is optional
     * @param landscapeHandles List of landscape handles
     *            is optional
     * @param throttlesize Throttle size (Defaults to 1000)
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    List<Map<String, String>> getObjects(String objectType, 
                                         List<String> attributes, 
                                         List<String> landscapeHandles, 
                                         int throttlesize, 
                                         String username, 
                                         String password) throws Exception;

    /**
     * Retrieves object ids and specified attributes associated with retrieved objects
     * based on URL/XML query. (Alarms/Devices)
     * 
     * @param object type
     * @param urlQuery
     * @param xmlQuery
     * @param username
     * @param password
     * @param filterId
     * @param subscriptionId
     * @return
     * @throws Exception
     */
    List<Map<String, String>> getObjects(String objectType, 
                                         String urlQuery, 
                                         String xmlQuery, 
                                         String username, 
                                         String password, 
                                         String filterId,
                                         String subscriptionId) throws Exception;

    /**
     * Issues an action request specified by action code on SpectroSERVER.
     * 
     * @param actionCode
     * @param modelHandle 
     * @param attribValMap (optional) to pass data to action
     * @param throttleSize (optional)
     * @return a {@link Map} containing acion response attributes. 
     *                       Contents differ depending on reuested action.
     * @throws Exception
     */
    Map<String, String> issueAnAction(String actionCode, 
                                      String modelHandle, 
                                      Map<String, String> attribValMap, 
                                      int throttlesize,
                                      String username, 
                                      String password) throws Exception;
    
    /**
     * Updates object.
     * 
     * @param object type (alarms/model)
     * @param objectId
     * @param attribVal
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    Map<String, String> updateObject(String objectType, 
                                     String objectId, 
                                     Map<String, String> attribVal, 
                                     String username, 
                                     String password) throws Exception;
    
    /**
     * Deletes object.
     * 
     * @param object type (alarms/model)
     * @param objectId
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    Map<String, String> deleteObject(String objectType, 
                                     String objectId, 
                                     String username, 
                                     String password) throws Exception;
    
    /**
     * Creates an association between two models.
     * 
     * @param relationHandle
     * @param leftModelHandle
     * @param rightModelHandle
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    Map<String, String> createAssociation(String relationHandle,
                                          String leftModelHandle,
                                          String rightModelHandle,
                                          String username,
                                          String password) throws Exception;
    
    /**
     * Get associations for specific relation and model.
     * 
     * @param relationHandle
     * @param modelHandle
     * @param side
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    List<Map<String, String>> getAssociations(String relationHandle,
                                              String modelHandle,
                                              String side,
                                              String username,
                                              String password) throws Exception;
    
    /**
     * Deletes an association between two models.
     * 
     * @param relationHandle
     * @param leftModelHandle
     * @param rightModelHandle
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    Map<String, String> deleteAssociation(String relationHandle,
                                          String leftModelHandle,
                                          String rightModelHandle,
                                          String username,
                                          String password) throws Exception;
    
    /**
     * Get attribute enumeration for enumerated attribute.
     * 
     * @param attrId
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    Map<String, String> getAttributeEnum(String attrId, 
                                         String username, 
                                         String password) throws Exception;
    
    /**
     * Get connectivity information for specified device models IP address.
     * 
     * @param ipAddress
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    Map<String, Map<String, List<Map<String, String>>>> getConnectivity(String ipAdress,
                                                                        String username,
                                                                        String password) throws Exception;
    
    /**
     * Get landscape information for all landscapes.
     * 
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    List<Map<String, String>> getLandscapes(String username,
                                            String password) throws Exception;
    
    /**
     * Create a new model.
     * 
     * @param landscapeHandle
     * @param modelTypeHandle
     * @param snmpPort
     * @param comunityString
     * @param retryCount
     * @param timeout
     * @param ipAddress
     * @param parentModelHandle
     * @param parentToModelRelationHandle
     * @param attribValMap Model attribute value map
     * @param username
     * @param password
     * @return 
     * @return Exception
     */
    Map<String, String> createModel(String landscapeHandle,
                                    String modelTypeHandle,
                                    int snmpAgentPort,
                                    String communityString,
                                    int retryCount,
                                    int timeout,
                                    String ipAddress,
                                    String parentModelHandle,                                                
                                    String parentToModelRelationHandle,
                                    Map<String, String> attribValMap,
                                    String username,
                                    String password) throws Exception;
    
    /**
     * Get specific model and its requested attributes.
     * 
     * @param modelHandle
     * @param attributes (optional) List of model attributes to retrieve for specified model
     *              Model handle is returned by default
     * @param username
     *              if not provided uses gateway configured username
     * @param password
     *              tied with username
     * @return
     *                        
     * @throws Exception
     */
    Map<String, String> getModel(String modelHandle,
                                 List<String> attributes,
                                 String username,
                                 String password) throws Exception;
    
    /**
     * Update objects attribute values based on URL/XML query. (Models)
     * 
     * @param object type
     * @param xmlQuery
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    List<Map<String, String>> updateObjects(String objectType, 
                                            String xmlQuery, 
                                            String username, 
                                            String password) throws Exception;
    
    /**
     * Create an event.
     * 
     * @param eventType
     * @param modelHandle
     * @param varBindIdValue
     * @param username
     * @param password
     * @return 
     * @throws Exception
     */
    Map<String, String> createEvent(String eventType,
                                    String modelHandle,
                                    Map<String, String> varBindIdValue,
                                    String username,
                                    String password) throws Exception;
    
    /**
     * Create events.
     * 
     * @param createEventsRequestXml
     * @param username
     * @param password
     * @return 
     * @throws Exception
     */
    List<Map<String, String>> createEvents(String createEventsRequestXml,
                                           String username,
                                           String password) throws Exception;
    
    /**
     * Create subscription.
     * 
     * @param pullSubscriptionRequestXml
     * @param username
     * @param password
     * @return 
     * @throws Exception
     */
    String createSubscription(String pullSubscriptionRequestXml,
                              String username,
                              String password) throws Exception;
}
