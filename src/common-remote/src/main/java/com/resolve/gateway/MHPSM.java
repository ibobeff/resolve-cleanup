/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.Map;

import com.resolve.gateway.hpsm.HPSMFilter;
import com.resolve.gateway.hpsm.HPSMGateway;

/**
 * This class is used by the Message Listener and invokes it methods. Messages
 * destined for the gateways set this class name in the message header.
 *
 */
public class MHPSM extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MHPSM.setFilters";

    private static HPSMGateway instance = HPSMGateway.getInstance();

    public MHPSM()
    {
        super.instance = instance;
    }

    @Override
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    @Override
    protected void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        HPSMFilter hpsmFilter = (HPSMFilter) filter;
        filterMap.put(HPSMFilter.QUERY, hpsmFilter.getQuery());
        filterMap.put(HPSMFilter.OBJECT, hpsmFilter.getObject());
    }
}
