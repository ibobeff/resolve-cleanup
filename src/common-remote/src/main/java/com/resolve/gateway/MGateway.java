/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.gateway;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.resolve.gateway.Gateway.HealthStatusCode;
import com.resolve.gateway.resolvegateway.ResolveGatewayUtil;
import com.resolve.gateway.resolvegateway.msg.MSGGateway;
import com.resolve.gateway.resolvegateway.msg.MSGGatewayFilter;
import com.resolve.gateway.resolvegateway.pull.PullGateway;
import com.resolve.gateway.resolvegateway.pull.PullGatewayFilter;
import com.resolve.gateway.resolvegateway.push.PushGateway;
import com.resolve.gateway.resolvegateway.push.PushGatewayFilter;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.Properties;
import com.resolve.util.StringUtils;

/**
 * This is an abstract class that's extended by various subclasses for Resolve
 * Gateways (Netcool, Remedyx etc.)
 *
 * While looking at the Gateway code think Abstraction and OO concepts like
 * inheritance etc. There are hardly any copy/paste in these codes. It helps in
 * reducing maintenance overhead and keep the codebase more meaningful. These
 * codes go through "FindBug" which makes sure that there is no obvious problem
 * with the code.
 *
 * Check {@link MNetcool} or {@link MRemedyx} for its usage.
 */
public abstract class MGateway
{
	public static final String HEALTH_CHECK_RESULT_MAP_DETAIL_KEY = "HEALTH_CHECK_RESULT_DETAIL";
    private static final String TRUE = "TRUE";
    

    protected ClusteredGateway instance;

    protected abstract void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter);

    protected abstract String getSetFilterMethodName();

    // flag to set if we want to RSControl to be update
    // used during synchronization with RSControl when we
    // do not need to send update message back to RSCotnrol unnecessarily.
    protected boolean isUpdateRSControl = true;

    /**
     * Initiates a heartbeat from the current Gateway instance
     * 
     * @param params
     *            a map of parameters used in exectuting a heartbeat, mapping
     *            "PRIMARY" to a boolean value, intended to ascertain if you're
     *            looking for a heartbeat from a primary or secondary gateway
     * 
     */
    public void heartbeat(Map<String, String> params)
    {
        String gatewayName = params.get("GATEWAY");
        
        if(gatewayName != null) {
            String className = instance.getClass().getSimpleName();
            
            if(className.indexOf("PushGateway") != -1) {
                PushGateway pushateway = PushGateway.getInstance(gatewayName.toLowerCase());
                if(pushateway != null)
                    pushateway.getHeartbeatThread().processHeartbeatMessage(params);
            }
            
            else if(className.indexOf("PullGateway") != -1) {
                PullGateway pullGateway = PullGateway.getInstance(gatewayName.toLowerCase());
                if(pullGateway != null)
                    pullGateway.getHeartbeatThread().processHeartbeatMessage(params);
            }
            
            else if(className.indexOf("MSGGateway") != -1) {
                MSGGateway msgGateway = MSGGateway.getInstance(gatewayName.toLowerCase());
                if(msgGateway != null)
                    msgGateway.getHeartbeatThread().processHeartbeatMessage(params);
            }
        }
     
        else {
            if (instance.getHeartbeatThread() != null)
            {
                instance.getHeartbeatThread().processHeartbeatMessage(params);
            }
        }
    } // heartbeat

    /**
     * Sets the filters for the current Gateway singleton from a {@link List} of
     * parameter {@link Map}s
     * 
     * @param paramList
     *            a {@link List} of {@link Map}s of filters
     * 
     */
    public void setFilters(List<Map<String, Object>> paramList)
    {
        if (paramList != null)
        {
            for (Map<String, Object> params : paramList)
            {
                instance.setFilter(params);
            }
        }

        // save config
        MainBase.main.exitSaveConfig();
    } // setFilters

    /**
     * Sets the filters for the current Gateway singleton
     * 
     * @param params
     *            a {@link Map} of filters
     * 
     */
    public void setFilter(Map<String, Object> params)
    {
        instance.setFilter(params);
        // save config
        MainBase.main.exitSaveConfig();
    } // setFilter

    /**
     * Activates a filter based on a Map of parameters.
     * 
     * @param params
     *            a {@link Map} of parameters to set a filter active. The Map
     *            should have the following key:value format: BaseFilter.ID :
     *            String BaseFilter.ACTIVE : String
     * 
     */
    public void setFilterActive(Map<String, String> params)
    {
        String name = (String) params.get(BaseFilter.ID);
        String activeString = (String) params.get(BaseFilter.ACTIVE);

        // boolean active = !(activeString != null &&
        // activeString.equalsIgnoreCase("FALSE"));
        boolean active = (activeString == null || activeString.equalsIgnoreCase(TRUE));

        instance.setFilterActive(name, active);

        // save config
        MainBase.main.exitSaveConfig();
    } // setFilterActive

    /**
     * This method is invoked once filter synchronization message from RSCONTROL
     * is received.
     *
     * @param paramList
     */
    public void synchronizeGateway(Map<String, List<Map<String, Object>>> paramList)
    {
        if (paramList != null)
        {
            isUpdateRSControl = false;
            Log.log.info("Processing Gateway synchronization message received for " + instance.getQueueName());
            List<Map<String, Object>> filters = (List<Map<String, Object>>) paramList.get("FILTERS");
            int number = 0;
            if (filters != null)
            {
                number = filters.size();
            }
            Log.log.info("NUMBER of filters " + number);
            
            String gatewayName = null;
            String gwClass = null;
            List<Map<String, Object>> gateway = paramList.get("GATEWAY");
            if(gateway != null) {
                gatewayName = (String)gateway.get(0).get("GATEWAYNAME");
                gwClass = (String)gateway.get(0).get("GATEWAYCLASS");
            }
            else
                clearAndSetFilters(filters);
            Log.log.info(gatewayName + "Gateway: Processing Gateway synchronization message "
            		+ "received for queue -->" + instance.getQueueName());
            List<Map<String, Object>> nameProperties = (List<Map<String, Object>>) paramList.get("NAMEPROPERTIES");
            number = 0;
            if (nameProperties != null)
            {
                number = nameProperties.size();
            }

            Log.log.info("NUMBER of nameProperties " + number);
            clearAndSetNameProperties(nameProperties);
            
            //List<Map<String, Object>> routingSchemas = (List<Map<String, Object>>) paramList.get("ROUTINGSCHEMAS");
            Map<String, Object> routingSchemas = (Map<String, Object>) paramList.get("ROUTINGSCHEMAS");
//            number = 0;
//            if (routingSchemas != null)
//            {
//                number = routingSchemas.size();
//            }
//            
//            Log.log.info("NUMBER of Resolution Routing Schemas " + number);
            // TODO Is this number needs to be displayed?
            
            clearAndSetRoutingSchemas(routingSchemas);

            isUpdateRSControl = true;
            // tell the gateway that we have received RSControl sync message.
            
            if(gatewayName != null) {
                String className = instance.getClass().getSimpleName();
                
                if(className.indexOf("PushGateway") != -1) {
                    PushGateway pushateway = PushGateway.getInstance(gwClass.toLowerCase());
                    if(pushateway != null)
                        pushateway.setSyncDone(true);
                }
                
                else if(className.indexOf("PullGateway") != -1) {
                    PullGateway pullGateway = PullGateway.getInstance(gwClass.toLowerCase());
                    if(pullGateway != null)
                        pullGateway.setSyncDone(true);
                }
                
                else if(className.indexOf("MSGGateway") != -1) {
                    MSGGateway msgsGateway = MSGGateway.getInstance(gwClass.toLowerCase());
                    if(msgsGateway != null)
                        msgsGateway.setSyncDone(true);
                }
            }
            
            else
                instance.setSyncDone(true);
        }
    }

    /**
     * This method requests rscontrol to provide routing schemas.
     *
     * @param params
     */
    public void synchronizeRoutingSchemas(Map<String, String> params)
    {
        if (params != null && params.containsKey("RESET_ROUTING_SCHEMAS"))
        {
            Log.log.info("Processing Gateway Routing Schema synchronization message received for " + instance.getQueueName());
            instance.sendRoutingSchemaSyncRequest();
        }
    }

    /**
     * This method receives routing schemas from RSCONTROL.
     *
     * @param paramList
     */
    public void receiveRoutingSchemas(Map<String, List<Map<String, Object>>> paramList)
    {
        if (paramList != null)
        {
            isUpdateRSControl = false;
            Log.log.info("Processing Gateway Routing Schema synchronization message received for " + instance.getQueueName());
            
            Map<String, Object> routingSchemas = (Map<String, Object>) paramList.get("ROUTINGSCHEMAS");
            int number = 0;
            if (routingSchemas != null)
            {
                number = routingSchemas.size();
            }

            Log.log.info("NUMBER of Resolution Routing Schemas " + number);
            clearAndSetRoutingSchemas(routingSchemas);
        }
    }

    /**
     * Clears the filters associated with the current Gateway instance. Resets
     * the new filter list with those in the paramList, if paramLIst is not
     * null.
     *
     * @param paramList
     *            a {@link List} of filter {@link Map}s.
     */

    public void clearAndSetFilters(List<Map<String, Object>> paramList)
    {
        Log.log.debug("Received clearAndSetFilter" + (paramList != null ? " " + paramList.size() : ""));
        // clear and set filters
        instance.clearAndSetFilters(paramList);

        // Send mesage to RSControl to clear deployed filters for Queue from DB
        if (instance.isPrimary())
        {
            // send new filters
            Map<String, Object> params = null;
            // extract the first Map so we get the username who is deploying the
            // filter
            if (paramList != null && paramList.size() > 0)
                params = paramList.get(0);
                
            getFilters(params);
        }

        // save config
        MainBase.main.exitSaveConfig();

    } // clearAndSetFilters
    
    public void deployFilters(List<Map<String, Object>> paramList)
    {
        Log.log.debug("Received deployFilters" + (paramList != null ? " " + paramList.size() : ""));
        
        String username = "";
        String gatewayName = "";

        try {
            if(paramList != null) {
                for (Map<String, Object> params : paramList)
                {
                    if (params.containsKey("RESOLVE_USERNAME")) {
                        username = (String)params.get("RESOLVE_USERNAME");
                        continue;
                    }
                    
                    gatewayName = (String)params.get("GATEWAY_NAME");
                    
                    if(gatewayName == null) {
                        Log.log.debug("Gateway name is null.");
                        continue;
                    }
                }    
                String className = instance.getClass().getSimpleName();
                
                if(className.indexOf("PushGateway") != -1) {
                    List<Filter> deployedFilters = ((PushGateway)instance).deployFilters(paramList, username);
                    for(Filter filter:deployedFilters) {
                        if(filter instanceof PushGatewayFilter) {
                            ResolveGatewayUtil.updatePushGatewayFilter((PushGatewayFilter)filter);
                            ((PushGateway)instance).sendAlert("INFO", false, "Filter " + gatewayName +  
                            	" deployed by " + username, StringUtils.objectToLogString(filter, "getMatcher"));
                        }
                    }
                    //ResolveGatewayUtil.updatePushGatewayFilter(params);

                   // break;
                }
                
                else if(className.indexOf("PullGateway") != -1) {
                    List<Filter> deployedFilters = ((PullGateway)instance).deployFilters(paramList, username);
                    
                    for(Filter filter:deployedFilters) {
                        if(filter instanceof PullGatewayFilter) {
                            ResolveGatewayUtil.updatePullGatewayFilter((PullGatewayFilter)filter,true);
                            ((PullGateway)instance).sendAlert("INFO", false, "Filter " + gatewayName + 
                             " deployed by " + username, StringUtils.objectToLogString(filter, "getMatcher"));
                        }
                        	
                    }
                }
                
                else if(className.indexOf("MSGGateway") != -1) {
                    List<Filter> deployedFilters = ((MSGGateway)instance).deployFilters(paramList, username);
                    
                    for(Filter filter:deployedFilters) {
                        if(filter instanceof MSGGatewayFilter) {
                            ResolveGatewayUtil.updateMSGGatewayFilter((MSGGatewayFilter)filter);
                            ((MSGGateway)instance).sendAlert("INFO", false, "Filter " + gatewayName + 
                             " deployed by " + username, StringUtils.objectToLogString(filter, "getMatcher"));
                        }
                    }
                }
                
                else
                    Log.log.debug("Not supporting deployment of gateway with class name: " + className);
            }
            
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    } // deployFilters
    
    public void undeployFilters(List<Map<String, Object>> paramList)
    {
        Log.log.debug("Received undeployFilters" + (paramList != null ? " " + paramList.size() : ""));
        
        String username = null;
        String gatewayName = null;
        String className = null;
        
        
        for (Map<String, Object> params : paramList)
        {
            if(params.containsKey("GATEWAY_NAME")) {
                gatewayName = (String)params.get("GATEWAY_NAME");
                
                if(gatewayName == null) {
                    Log.log.debug("Gateway name is null.");
                    continue;
                }
                
                className = instance.getClass().getSimpleName();
                    
                if(username != null)
                    break;
                else
                    continue;
            }
            
            if(params.containsKey("RESOLVE_USERNAME")) {
                username = (String)params.get("RESOLVE_USERNAME");
                
                if(gatewayName != null)
                    break;
                else
                    continue;
            }
        }

        try {
            if(className.indexOf("PushGateway") != -1) {
                List<Filter> undeployedFilters = ((PushGateway)instance).undeployFilters(paramList, username);
                
                for(Filter filter:undeployedFilters) {
                    if(filter instanceof PushGatewayFilter)
                        ResolveGatewayUtil.undeployPushGatewayFilter((PushGatewayFilter)filter);
                }
            }

            else if(className.indexOf("PullGateway") != -1) {
                List<Filter> undeployedFilters = ((PullGateway)instance).undeployFilters(paramList, username);
                
                for(Filter filter:undeployedFilters) {
                    if(filter instanceof PullGatewayFilter)
                        ResolveGatewayUtil.undeployPullGatewayFilter((PullGatewayFilter)filter);
                }
            }
            
            else if(className.indexOf("MSGGateway") != -1) {
                List<Filter> undeployedFilters = ((MSGGateway)instance).undeployFilters(paramList, username);
                
                for(Filter filter:undeployedFilters) {
                    if(filter instanceof MSGGatewayFilter)
                        ResolveGatewayUtil.undeployMSGGatewayFilter((MSGGatewayFilter)filter);
                }
            }
            
            else
                Log.log.debug("Not supporting undeployment of gateway with class name: " + className);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    } // undeployFilters

    /**
     * Clears the name properties associated with the current {@link Gateway}
     * instance and resets the new name properties link List with those in the
     * paramList, if paramList is not null.
     *
     * @param paramList
     *            a {@link List} of name property {@link Map}s
     */

    public void clearAndSetNameProperties(List<Map<String, Object>> paramList)
    {
        instance.clearAndSetNameProperties(paramList);
    } // clearAndSetNameProperties


    /**
     * Clears the resolution routing schemas with the current {@link Gateway}
     * instance and resets the new schemas link List with those in the
     * paramList, if paramList is not null.
     *
     * @param paramList
     *            a {@link List} of routing schemas {@link Map}s
     */

    public void clearAndSetRoutingSchemas(Map<String, Object> orgRRSchemaMa)
    {
        instance.clearAndSetRoutingSchemas(orgRRSchemaMa);
    } // clearAndSetRoutingSchemas

    /**
     * Sends a {@link Map} of gateway {@link Filter}s from the current
     * {@link Gateway} instance as an internal message. If the params
     * {@link Map} isn't valid, it will append its value to the Gateway
     * instance's filterMap. The params map should only feature a
     * Constants.SYS_UPDATED_BY key and value.
     *
     * @param paramList
     *            a parameter {@link Map}, which has the following format:
     *            Constants.SYS_UPDATED_BY : String
     */

    public void getFilters(Map<String, Object> params)
    {
        Map<String, String> content = new HashMap<String, String>();

        for (Filter filter : instance.getFilters().values())
        {
            BaseFilter baseFilter = (BaseFilter) filter;
            Map<String, String> filterMap = new HashMap<String, String>();
            filterMap.put(BaseFilter.ID, baseFilter.getId());
            filterMap.put(BaseFilter.ACTIVE, String.valueOf(baseFilter.isActive()));
            filterMap.put(BaseFilter.ORDER, String.valueOf(baseFilter.getOrder()));
            filterMap.put(BaseFilter.INTERVAL, String.valueOf(baseFilter.getInterval()));
            filterMap.put(BaseFilter.EVENT_EVENTID, baseFilter.getEventEventId());
            filterMap.put(BaseFilter.RUNBOOK, baseFilter.getRunbook());
            filterMap.put(BaseFilter.SCRIPT, baseFilter.getScript());
            filterMap.put(BaseFilter.QUEUE, instance.getQueueName());

            // subclasses populates their data
            populateGatewaySpecificFilterProperties(filterMap, filter);

            // This value comes all the way from the gateway filter
            // deployment UI.
            // This is important so RSConrtol can set the user name who
            // deployed the filter.
            if (params != null)
            {
                filterMap.put(Constants.SYS_UPDATED_BY, (String) params.get(Constants.RESOLVE_USERNAME));
            }

            // Remove any value with just "null" (in lower case so users can still use mixed or upper case as valid field data) from the map
            
            List<String> fldsWithLCNullValue = new ArrayList<String>();
            
            for (String key : filterMap.keySet())
            {
                if (filterMap.get(key) != null && filterMap.get(key).equals("null"))
                    fldsWithLCNullValue.add(key);
                    
            }
            
            if (!fldsWithLCNullValue.isEmpty())
            {
                for (String fldWithLCNullValue : fldsWithLCNullValue)
                {
                    filterMap.put(fldWithLCNullValue, null);
                }
            }
            
            content.put(filter.getId(), StringUtils.mapToString(filterMap));
        }

        // send content to rscontrol
        // this is important as RSControl need this
        content.put("QUEUE", instance.getQueueName());

        if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_SYSTEM_RSCONTROL, getSetFilterMethodName(), content) == false)
        {
            Log.log.warn(String.format("Failed to send filters to %s", Constants.ESB_NAME_SYSTEM_RSCONTROL));
        }
        
        Log.log.debug("Sent " + getSetFilterMethodName() + " message with content [" + content + "] to " + Constants.ESB_NAME_SYSTEM_RSCONTROL);
    } // getFilters

    /**
     * Removes a set of {@link Filter}s from the current {@link Gateway}
     * instance using the paramList {@link List}.
     *
     * @param paramList
     *            a {@link List} of {@link Filter} id {@link String}s.
     */

    public void removeFilters(List<String> paramList)
    {
        if (paramList != null)
        {
            for (String id : paramList)
            {
                instance.removeFilter(id);
            }
        }

        // save config
        MainBase.main.exitSaveConfig();
    } // removeFilters

    /**
     * Removes a single {@link Filter} from the current {@link Gateway} instance
     * using the given {@link String} id.
     *
     * @param id
     *            a {@link String} id.
     */
    public void removeFilter(String id)
    {
        instance.removeFilter(id);

        // save config
        MainBase.main.exitSaveConfig();
    } // removeFilter

    /**
     * Returns a {@link HashMap} of name properties from the current
     * {@link Gateway} instance.
     *
     * @param name
     *            the id of the name property.
     */
    public Map<String, Map<String, Object>> getNameProperties(String name)
    {
        Map<String, Map<String, Object>> result = new HashMap<String, Map<String, Object>>();
        if (StringUtils.isBlank(name))
        {
            for (String key : instance.getNameProperties().keySet())
            {
                result.put(key, instance.getNameProperties().getNameProperties(key));
            }
        }
        else
        {
            result.put(name, instance.getNameProperties().getNameProperties(name));
        }
        return result;
    } // getNameProperties

    /**
     * Sets the name properties of the current {@link Gateway} instance.
     *
     * @param params
     *            a {@link Map} of name properties.
     */
    public void setNameProperties(Map params)
    {
        String name = (String) params.get(Constants.GROOVY_BINDING_NAME_PROPERTIES);
        Map prop = new Properties();
        prop.putAll(params);
        prop.remove(Constants.GROOVY_BINDING_NAME_PROPERTIES);

        instance.setNameProperties(name, prop, true);
    } // setNameProperties

    /**
     * Sets a name property in the current {@link Gateway} instance.
     *
     * @param params
     *            a {@link Map} of name properties.
     */
    public void setNameProperty(Map params)
    {
        String name = (String) params.get(Constants.GROOVY_BINDING_NAME_PROPERTIES);
        String key = (String) params.get("KEY");
        String value = (String) params.get("VALUE");

        instance.setNameProperty(name, key, value);
    } // setNameProperty

    /**
     * Removes name properties in the current {@link Gateway} instance.
     *
     * @param params
     *            a {@link Map} of name properties to remove.
     */
    public void removeNameProperties(Map params)
    {
        String name = (String) params.get(Constants.GROOVY_BINDING_NAME_PROPERTIES);

        instance.getNameProperties().removeNameProperties(name);
    } // removeNameProperties

    /**
     * Removes a name property in the current {@link Gateway} instance.
     *
     * @param params
     *            a {@link Map} of a name property to remove.
     */
    public void removeNameProperty(Map params)
    {
        String name = (String) params.get(Constants.GROOVY_BINDING_NAME_PROPERTIES);
        String key = (String) params.get("KEY");

        instance.removeNameProperty(name, key);
    } // removeNameProperty

    /**
     * This method is called from the primary gateway once some data is
     * available to be processed by workers.
     * 
     * @param params
     */
    public void receiveData(Map<String, String> params)
    {
        Log.log.debug("Message received from primary and need to process data, My GUID: " + MainBase.main.configId.getGuid());
        instance.receiveData(params);
    } // processData

    /**
     * Processes a filter in the current {@link Gateway} instance, so long that
     * the current {@link Gateway} instance is the primary Gateway
     *
     * @param params
     *            a {@link Map} of a name property to remove.
     */
    public void processFilter(Map<String, String> params)
    {
        if (instance.isPrimary())
        {
            Log.log.debug("Filter processing message received, My GUID: " + MainBase.main.configId.getGuid());
            instance.processFilter(params);
        }
    } // processData
    
    /**
     * Process self-check request.
     */
    public Map<String, String> selfCheck(Map<String, String> params)
    {
        if (Log.log.isDebugEnabled())
        {
            String msgContent = "";
            
            if (params != null && !params.isEmpty())
            {
                msgContent = StringUtils.mapToLogString(params);
            }
            
            Log.log.debug("Gateway Message Handler on instance " + MainBase.main.configId.getGuid() + " received Self-Check message [" + msgContent + "]");
        }
        
        Map<String, String> response = new HashMap<String, String>();
        response.put("selfCheck", "Response");
        return response;
    }
    
    /**
     * Process health check request.
     */
    public Map<String, String> healthCheck(Map<String, Object> healthCheckOptions) {
    	Map<String, String> healtCheckResultMap = new HashMap<String, String>();
    	
    	if (Log.log.isDebugEnabled()) {
    		Log.log.debug(String.format("%s received healthCheck() request%s", instance.getClass().getSimpleName(),
    									(MapUtils.isNotEmpty(healthCheckOptions) ? 
    									 " with health check parameter keys [" + 
    									 StringUtils.setToString(healthCheckOptions.keySet(), ", ") + "]" : "")));
    	}
    	
    	Pair<HealthStatusCode, String> healtCheckResult = Pair.of(Gateway.HealthStatusCode.FAILED_TO_RUN, "");
    	
    	try {
    		healtCheckResult = instance.checkGatewayHealth(healthCheckOptions);
    	} catch (Throwable t) {
    		String errMsg = String.format("Error %sin running gateway health check on %s", 
										  (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
										  instance.getClass().getSimpleName());    		
    		Log.log.error(errMsg, t);
    		healtCheckResult = Pair.of(Gateway.HealthStatusCode.FAILED_TO_RUN, 
    								   String.format("Error Message = [%s], Stack Trace = [%s]", errMsg, 
    										   		 ExceptionUtils.getStackTrace(t)));
    	}
    	
    	healtCheckResultMap.put(Gateway.HealthStatusCode.class.getSimpleName(), healtCheckResult.getLeft().toString());
    	healtCheckResultMap.put(HEALTH_CHECK_RESULT_MAP_DETAIL_KEY, healtCheckResult.getRight());
    	
    	return healtCheckResultMap;
    }
}