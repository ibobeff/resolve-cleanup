/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.hpsm;

import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.RetainValue;

public class HPSMFilter extends BaseFilter
{
    // Additional field names for this filter.
    public static final String QUERY = "QUERY";
    public static final String OBJECT = "OBJECT";
    public static final String LAST_RECEIVE_DATE = "LASTRECEIVEDATE";

    private String object;
    private String query;
    private String lastReceiveDate;

    public HPSMFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String object, String query)
    {
        super(id, active, order, interval, eventEventId, runbook, script);
        setObject(object);
        setQuery(query);
    } // ServiceNowFilter

    public String getObject()
    {
        return object;
    }

    public void setObject(String object)
    {
        this.object = object != null ? object.trim() : object;
    }

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query != null ? query.trim() : query;
    }

    @RetainValue
    public String getLastReceiveDate()
    {
        return lastReceiveDate;
    }

    public void setLastReceiveDate(String lastReceiveDate)
    {
        this.lastReceiveDate = lastReceiveDate != null ? lastReceiveDate.trim() : lastReceiveDate;
    }
}
