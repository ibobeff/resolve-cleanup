/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.gateway;

import java.util.List;
import java.util.Map;

import com.resolve.gateway.hpom.HPOMFilter;
import com.resolve.gateway.hpom.HPOMGateway;

/**
 * This class is used by the Message Listener and invokes it methods. Messages
 * destined for the gateways set this class name in the message header.
 *
 */
public class MHPOM extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MHPOM.setFilters";

    private static HPOMGateway instance = HPOMGateway.getInstance();

    public MHPOM()
    {
        super.instance = instance;
    }

    @Override
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    @Override
    protected void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        HPOMFilter hpomFilter = (HPOMFilter) filter;
        filterMap.put(HPOMFilter.QUERY, hpomFilter.getQuery());
        filterMap.put(HPOMFilter.OBJECT, hpomFilter.getObject());
    }

    /**
     * Creates and HPOM Event
     * 
     * @param nodeIPAddress
     *            IP address or DNS host name to be used as the node reference
     *            in the event. If not provided uses the IP address of the
     *            server where RSRemote is running.
     * @param params
     *            is a {@link Map} with various keys (properties of the event)
     *            with their values. For the supported keys, refer to
     *            {@link HPOMConstants}. Anything other than the listed ones
     *            will be treated as custom fields.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return the newly created event's Id.
     * @throws Exception
     * 
     */
    public String createEvent(String nodeIPAddress, Map<String, String> params, String username, String password) throws Exception
    {
        return instance.createEvent(nodeIPAddress, params, username, password);
    }

    /**
     * Updates an existing event. HPOM may place some restrictions on changing
     * the value for any arbitrary elements, for more information refer to the
     * HPOM documentation. However you can change Title and Severity using this
     * API. For severity the value must be one of these: - Normal - Warning -
     * nMinor - Major - Critical
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMAPI;
     *         import com.resolve.gateway.HPOMConstants;
     *         ....
     *         ....
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             String eventId = "2438be98-01c1-71e2-1fa2-0a1e0a130000";
     *
     *             Map<String, String> params = new HashMap<String, String>();
     *
     *             params.put(HPOMConstants.FIELD_TITLE, "Changed title");
     *             params.put(HPOMConstants.FIELD_SEVERITY, "Minor");
     *
     *             HPOMAPI.updateEvent(eventId, params, username, password);
     *             System.out.println("Event updated sucessfully.");
     *         }
     *         catch (Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param eventId
     *            of the event to be updated.
     * @param params
     *            is a {@link Map} with various keys (properties of the event)
     *            with their values. For the supported keys, refer to
     *            {@link HPOMConstants}. Anything other than the listed ones
     *            will be treated as custom fields.
     *
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public void updateEvent(String sysId, Map<String, String> params, String username, String password) throws Exception
    {
        instance.updateEvent(sysId, params, username, password);
    }

    /**
     * Closes an existing event. HPOM may place some restrictions on closing an
     * event based on its business logic, for more information refer to the HPOM
     * documentation.
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMAPI;
     *         import com.resolve.gateway.HPOMConstants;
     *         ....
     *         ....
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             String eventId = "2438be98-01c1-71e2-1fa2-0a1e0a130000";
     *
     *             HPOMAPI.closeEvent(eventId, username, password);
     *             System.out.println("Event closed sucessfully.");
     *         }
     *         catch (Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param incidentId
     *            of the event to be closed.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public void closeEvent(String incidentId, String username, String password) throws Exception
    {
        instance.closeEvent(incidentId, username, password);
    }

    /**
     * Gets the data for an existing event.
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMAPI;
     *         import com.resolve.gateway.HPOMConstants;
     *         ....
     *         ....
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             String eventId = "2438be98-01c1-71e2-1fa2-0a1e0a130000";
     *
     *             Map<String, String> result = HPOMAPI.selectEventById(eventId, username, password);
     *
     *             System.out.println("Event details:");
     *             for(String key : result.keySet())
     *             {
     *                 System.out.printf("%s = %s\n", key, result.get(key));
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param incidentID
     *            to get data.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @throws Exception
     */
    public Map<String, String> selectEventById(String incidentID, String username, String password) throws Exception
    {
        return instance.selectEventById(incidentID, username, password);
    }

    /**
     * Queries the HPOM events based on the provided query.
     *
     * <pre>
     * {@code
     *         import com.resolve.gateway.HPOMAPI;
     *         import com.resolve.gateway.HPOMConstants;
     *         ....
     *         ....
     *         try
     *         {
     *             String username = "hpomuser"; //if null then uses gateway configured value.
     *             String password = "hpompassword"; //if null then uses gateway configured value.
     *
     *             String query = "severity='Major' and title contains 'network'"; //HPOM is case-sensitive on severity
     *             int maxResult = 100;
     *
     *             List<Map<String, String>> result = HPOMAPI.searchEvent(query, maxResult, username, password);
     *
     *             System.out.println("Total number of events returned: " + result.size());
     *             int i = 1;
     *             for(Map<String, String> map : result)
     *             {
     *                 System.out.printf("Event# %d\n", i++);
     *                 for(String key : map.keySet())
     *                 {
     *                     System.out.printf("%s = %s\n", key, map.get(key));
     *                 }
     *             }
     *         }
     *         catch (Exception e)
     *         {
     *             e.printStackTrace();
     *             //Do something usefull with the execption.
     *         }
     * }
     * </pre>
     *
     * @param query
     *            in the form severity = 'major' and title contains 'network'
     * @param maxResult
     *            to be retuened. -1 returns everything.
     * @param username
     *            if not provided uses the gateway configuration.
     * @param password
     *            ties with the username. Gateway configuration is used if not
     *            provided.
     * @return {@link List} of {@link Map} which contain properties for all
     *          events found.
     * @throws Exception
     */
    public List<Map<String, String>> searchEvent(String query, int maxResult, String username, String password) throws Exception
    {
        return instance.searchEvent(query, maxResult, username, password);
    }
}
