/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.hpom;

import java.util.List;
import java.util.Map;

public interface ObjectService
{
    /**
     * Every object has its ID property, the subclass requires to implement this
     * method. Example: Service request could be "NUMBER".
     * 
     * @return
     */
    String getIdPropertyName();

    /**
     * Identity of the object. Implementing class provides this. Example:
     * Incident is "incident" or Problem is "problem"
     * 
     * @return
     */
    String getIdentity();

    /**
     * Creates an object by supplying username, password.
     * 
     * @param nodeIPAddress
     * @param params
     * @param username
     * @param password
     * @return the newly created incident's Id.
     * @throws Exception
     */
    String create(String nodeIPAddress, Map<String, String> params, String username, String password) throws Exception;

    /**
     * Updates an object by supplying username, password.
     * 
     * @param objectId
     *            is mandatory.
     * @param params
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    void update(String objectId, Map<String, String> params, String username, String password) throws Exception;

    /**
     * Closes an object by its id by supplying username, password.
     * 
     * @param objectId
     *            is mandatory.
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    void close(String objectId, String username, String password) throws Exception;

    /**
     * Retrieves an object's data by its id (for example, IncidentID).
     * 
     * @param eventId
     * @param username
     * @param password
     * @return
     */
    Map<String, String> selectByEventId(String eventId, String username, String password) throws Exception;

    /**
     * Read data from the object service by supplying username, password.
     * 
     * @param query
     * @param maxResult
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    List<Map<String, String>> search(String query, int maxResult, String username, String password) throws Exception;

}
