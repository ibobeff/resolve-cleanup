/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.ssh;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.net.util.SubnetUtils;

import com.resolve.esb.ESBException;
import com.resolve.esb.MListener;
import com.resolve.rsbase.MainBase;
import com.resolve.connect.SSHConnectSSHJ;
import com.resolve.gateway.BaseGateway;
import com.resolve.gateway.MSSH;
import com.resolve.gateway.SSHGatewayConnection;
import com.resolve.rsremote.ConfigReceiveSSH;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.LockUtils;
import com.resolve.util.Log;

public class SSHGateway extends BaseGateway
{
    // Singleton
    private static volatile SSHGateway instance = null;

    // These maps keep track of all connections to a particular IP address.
    // IP address is the key in these Maps. The Map with SSHGatewayConnection
    // stores
    // the SSHGatewayConnection object's ID as the key. This is required because
    // there are different ways
    // to instantiate an SSHGatewayConnection object (e.g., username/pwd,
    // passphrase etc.). Without the ID
    // we'll not no what to remove from the used Map.
    public static ConcurrentHashMap<String, ConcurrentHashMap<String, SSHGatewayConnection>> used = new ConcurrentHashMap<String, ConcurrentHashMap<String, SSHGatewayConnection>>();
    public static ConcurrentHashMap<String, ConcurrentHashMap<String, SSHGatewayConnection>> unused = new ConcurrentHashMap<String, ConcurrentHashMap<String, SSHGatewayConnection>>();

    // This will be set in initialize() with the proper queue name from the
    // configuration.
    private String queue = "SSH";
    
    private int cMaxPoolSize = 0;

    private int defaultPort = ((ConfigReceiveSSH) configurations).getPort();
    private int defaultTimeout = ((ConfigReceiveSSH) configurations).getTimeout();
    // milliseconds it wait in case a pool is full.
    private int interval = ((ConfigReceiveSSH) configurations).getInterval() * 1000;
    private int timeoutCounter = ((ConfigReceiveSSH) configurations).getTimeoutcounter();
    private long expiry = ((ConfigReceiveSSH) configurations).getExpiry() * 1000 * 60;
    private long cleanup = ((ConfigReceiveSSH) configurations).getCleanup() * 1000 * 60;
    private int keepAliveInterval = ((ConfigReceiveSSH) configurations).getKeepAliveInterval();
    protected ConcurrentHashMap<String, SubnetMask> pools = new ConcurrentHashMap<String, SubnetMask>();

    public static SSHGateway getInstance(ConfigReceiveSSH config)
    {
        if (instance == null)
        {
            instance = new SSHGateway(config);
        }
        return (SSHGateway) instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static SSHGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("SSH Gateway is not initialized correctly..");
        }
        else
        {
            return (SSHGateway) instance;
        }
    }

    /**
     * Find the pool size based on Subnet defined in the "POOL" element in
     * config.xml
     *
     * @param ipAddress
     *            of the host
     * @param defaultPoolSize
     * @return
     */
    private int getPoolSize(String ipAddress, boolean isMax, int defaultPoolSize)
    {
        int result = defaultPoolSize;

        SubnetUtils subnetUtils = null;
        for (String mask : pools.keySet())
        {
            if (mask.contains("/")) // is a CIDR type subnet mask (like
                                    // 255.255.0.0/16)
            {
                subnetUtils = new SubnetUtils(mask);
            }
            else
            {
                subnetUtils = new SubnetUtils(ipAddress, mask);
            }
            subnetUtils.setInclusiveHostCount(true);
            if (subnetUtils.getInfo().isInRange(ipAddress))
            {
                SubnetMask subnetMask = pools.get(mask);
                if (isMax)
                {
                    result = subnetMask.getMaxConnection();
                }
                else
                {
                    result = 1;
                }
                Log.log.debug("The IP Address fall in the Subnet Mask : " + mask);
                break; // since we found it get out.
            }
        }
        return result;
    }

    /**
     * Find the timeout based on Subnet defined in the "POOL" element in
     * config.xml
     *
     * @param ipAddress
     *            of the host
     * @param defaultTimeout
     * @return
     */
    private int getTimeout(String ipAddress, int defaultTimeout)
    {
        int result = defaultTimeout;

        SubnetUtils subnetUtils = null;
        for (String mask : pools.keySet())
        {
            if (mask.contains("/")) // is a CIDR type subnet mask (like
                                    // 255.255.0.0/16)
            {
                subnetUtils = new SubnetUtils(mask);
            }
            else
            {
                subnetUtils = new SubnetUtils(ipAddress, mask);
            }
            subnetUtils.setInclusiveHostCount(true);
            if (subnetUtils.getInfo().isInRange(ipAddress))
            {
                SubnetMask subnetMask = pools.get(mask);
                result = subnetMask.getTimeout();
            }
        }
        return result;
    }

    /**
     * Instantiates an SSHGatewayConnection object.
     */
    private SSHGatewayConnection createNewInstance(String ipAddress, int port, int timeout, String username, String password, String filename, String passphrase, List<String> responses, boolean isConnect) throws Exception
    {
        SSHGatewayConnection sshConnect = null;

        try
        {
            if (password != null)
            {
                sshConnect = new SSHGatewayConnection(username, password, ipAddress, port, timeout, keepAliveInterval, isConnect);
            }
            else if (passphrase != null)
            {
                sshConnect = new SSHGatewayConnection(username, filename, passphrase, ipAddress, port, timeout, keepAliveInterval, isConnect);
            }
            else if (responses != null)
            {
                sshConnect = new SSHGatewayConnection(username, responses, ipAddress, port, timeout, keepAliveInterval, isConnect);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Could not create SSH connection to " + ipAddress + " using username " + username + " on port " + port);
            throw e;
        }
        return sshConnect;
    }

    private SSHGatewayConnection createNewConnection(String ipAddress, int port, int timeout, String username, String password, String filename, String passphrase, List responses, boolean isConnect) throws Exception
    {
        return createNewInstance(ipAddress, port, timeout, username, password, filename, passphrase, responses, isConnect);
    }

    private SSHGatewayConnection getConnection(Map<String, SSHGatewayConnection> unusedConnections, Map<String, SSHGatewayConnection> usedConnections)
    {
        SSHGatewayConnection result = null;
        long currentTime = DateUtils.getCurrentTimeInMillis();
        boolean isExpired = false;
        boolean isCleanedup = false;
        if (unusedConnections != null && unusedConnections.size() > 0)
        {
            for (String key : unusedConnections.keySet())
            {
                result = unusedConnections.get(key);
                isExpired = expiry > 0 && ((currentTime - result.getUnusedPoolStartTime()) > expiry); 
                isCleanedup = cleanup > 0 && ((currentTime - result.getStartTime()) > cleanup); 
                if(isExpired || isCleanedup){
                	result.closePoolConnection();
                }
                if (result.isLastExpectTimeout())
                {
                    result.increaseTimeoutCounter();
                }
                if (result.getTimeoutCounter() > timeoutCounter)
                {
                    Log.log.warn("There have been more than " + timeoutCounter + " timeouts for the connection: " + result.toString());
                    Log.log.warn("Check if there is need for increased timeout value.");
                }
                if (result.isClosed())
                {
                    unusedConnections.remove(key);
                    usedConnections.remove(key); // it may be there in used
                                                 // as well, remove it.
                    Log.log.debug("Connection was closed removed from the pool");
                    result = null;
                }
                if(result != null){
                	break; // got the object, exit.
                }

            }
        }
        return result;
    }

    private ConcurrentHashMap<String, SSHGatewayConnection> getConnectionMap(ConcurrentHashMap<String, ConcurrentHashMap<String, SSHGatewayConnection>> pool, String ipAddress)
    {
        ConcurrentHashMap<String, SSHGatewayConnection> result = pool.get(ipAddress);

        if (result == null) // just initialize
        {
            result = new ConcurrentHashMap<String, SSHGatewayConnection>();
            //Add the connection back to the pool
           // pool.put(ipAddress, result);
        }

        return result;
    }

    private SSHGatewayConnection checkOutConnection(String ipAddress, int port, int timeout, String username, String password, String filename, String passphrase, List responses) throws Exception
    {
        Log.log.debug("Checking out connection for: " + ipAddress);
        SSHGatewayConnection sshConnect = null;

        String lockName = Constants.LOCK_POOLED_GATEWAY_CHECKOUT + ipAddress;
        ReentrantLock lock = LockUtils.getLock(lockName);

        try
        {

            lock.lock();
            
            ConfigReceiveSSH sshConfigurations = (ConfigReceiveSSH) configurations;

            int maxPoolSize = getPoolSize(ipAddress, true, sshConfigurations.getMaxconnection());
            cMaxPoolSize = maxPoolSize;
            // In the beginning don't connect the SSHCOnnect object.
            // This instantiation is just for comparison.
            ConcurrentHashMap<String, SSHGatewayConnection> unusedConnections = getConnectionMap(unused, ipAddress);
            ConcurrentHashMap<String, SSHGatewayConnection> usedConnections = getConnectionMap(used, ipAddress);

            // Get the connection from the unused.
            sshConnect = getConnection(unusedConnections, usedConnections);
            while (sshConnect == null) // do not give up until a connection is
                                       // received
            {
            	int usedSize = usedConnections.size();
            	int unusedSize = unusedConnections.size();
                if ((usedSize + unusedSize) < maxPoolSize)
                {
                  sshConnect = createNewConnection(ipAddress, port, timeout, username, password, filename, passphrase, responses, true);
                }
                else
                {
                    Log.log.debug("Pool size limit encountered, waiting to get some back.");
                    Thread.sleep(interval);
                    sshConnect = getConnection(unusedConnections, usedConnections);
                }
            }

            // add new or existing connection to the used connections.
            SSHGatewayConnection oldSshConnect = usedConnections.putIfAbsent(sshConnect.getId(), sshConnect);
            
            if (oldSshConnect != null) // If its new connection then old connection will be null
            {
                unusedConnections.remove(oldSshConnect.getId());
            }
            
            // Just for safety, following should be noop either old or new connection 
            unusedConnections.remove(sshConnect.getId());
            
            //Add the connections back to the pool
            unused.putIfAbsent(ipAddress, unusedConnections);
            used.putIfAbsent(ipAddress, usedConnections);
            // registering the object is important because ActionTask
            // execution mechanism need to return them in case of
            // exception/timeout etc.
            registerSessionObject(sshConnect);
            
            if (Log.log.isTraceEnabled())
            {
                Log.log.info("Checking out : " + sshConnect.toString());
                Log.log.info("Unused pool size: " + unusedConnections.size() + " for the host: " + ipAddress);
                Log.log.info("Used pool size: " + usedConnections.size() + " for the host: " + ipAddress);
            }
        }
        catch (Throwable e)
        {
            Log.log.debug(e.getMessage(), e);
        }
        finally
        {
            lock.unlock();
            //LockUtils.removeLock(lockName);
        }
        return sshConnect;
    }

    public void checkInConnection(SSHGatewayConnection sshConnect) throws Exception
    {
        if (sshConnect != null)
        {
            long currentTime = DateUtils.getCurrentTimeInMillis();
            boolean isCleanedup = cleanup > 0 && ((currentTime - sshConnect.getStartTime()) > cleanup);
            sshConnect.setUnusedPoolStartTime(currentTime);
            Log.log.debug("Checkin in connection: " + sshConnect.toString());

            String lockName = Constants.LOCK_POOLED_GATEWAY_CHECKIN + sshConnect.getHostname();
            ReentrantLock lock = LockUtils.getLock(lockName);

            try
            {
                lock.lock();
                String ipAddress = sshConnect.getHostname();

                ConcurrentHashMap<String, SSHGatewayConnection> unusedConnections = getConnectionMap(unused, ipAddress);
                ConcurrentHashMap<String, SSHGatewayConnection> usedConnections = getConnectionMap(used, ipAddress);

                usedConnections.remove(sshConnect.getId());
                boolean checkedIn = unusedConnections.containsKey(sshConnect.getId());
                //sshConnect.
                if (!sshConnect.isClosed() && !checkedIn)
                {
                    //Add only if the sizes don't exceed pool size
                	ConfigReceiveSSH sshConfigurations = ((ConfigReceiveSSH) configurations);
                	int maxPoolSize = getPoolSize(ipAddress, true, sshConfigurations.getMaxconnection());
                    if ((unusedConnections.size() + usedConnections.size()) < maxPoolSize && !isCleanedup)
                    {
                      unusedConnections.putIfAbsent(sshConnect.getId(), sshConnect);
                    }else {
                        //close the connection
                    	Log.log.debug("Closing connection: "+ sshConnect.getId() );
                        sshConnect.closePoolConnection();
                    }
                }
//                if(getPoolSize(sshConnect.getId(), true, defaultPoolSize)
                unused.putIfAbsent(ipAddress, unusedConnections);
                used.putIfAbsent(ipAddress, usedConnections);
                //Check if used + unused has Max pool connection remove the unused connection from the pool
                
                
                if (Log.log.isTraceEnabled())
                {
                    Log.log.trace("Checked in, unused pool size: " + unusedConnections.size() + " for the host: " + ipAddress);
                    Log.log.trace("Checked in, used pool size: " + usedConnections.size() + " for the host: " + ipAddress);
                }
            }
            catch (Throwable e)
            {
                Log.log.debug(e.getMessage(), e);
            }
            finally
            {
                lock.unlock();
                //LockUtils.removeLock(lockName);
            }
        }

    }
    /**
     * Private constructor.
     *
     * @param config
     */
    private SSHGateway(ConfigReceiveSSH config)
    {
        // call super constructor for common initialization.
        super(config);
        queue = config.getQueue();
        if(config.getKeepAliveInterval() > 0) {
        	SSHConnectSSHJ.initKeepAlive();
        }
    }

    @Override
    public String getLicenseCode()
    {
        return "SSH";
    }

    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_SSH;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MSSH.class.getSimpleName();
    }

    @Override
    protected Class<MSSH> getMessageHandlerClass()
    {
        return MSSH.class;
    }

    @Override
    public String getQueueName()
    {
        return queue;
    }

    /**
     * Initializes the specific gateway resources.
     *
     * @param configurations
     */
    @Override
    protected void initialize()
    {
        ConfigReceiveSSH sshConfigurations = (ConfigReceiveSSH) configurations;

        queue = sshConfigurations.getQueue().toUpperCase();

        if (sshConfigurations.isActive())
        {
            try
            {
            	MainBase.main.mServer.createQueue(getQueueName());
            	MListener listener = MainBase.main.mServer.createListener(getQueueName(), "com.resolve.gateway");
            	listener.init(false);
            	
                String topicName = getQueueName() + "_TOPIC";
                MainBase.main.mServer.createPublication(topicName);
                MainBase.main.mServer.subscribePublication(topicName, "com.resolve.gateway");
            }
            catch (ESBException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

    @Override
    public void start()
    {
        Log.setCurrentContext(getClass().getSimpleName());
    	Log.log.info("Starting SSH Gateway with pools");
        // initialize resources
        try
        {
            init();
            running = true;
            //check the license for this gateway
            checkGatewayLicense();
            // schedule next license check every 30 minutes
            ScheduledExecutor.getInstance().executeRepeat(getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType(), this, "checkGatewayLicense", LICENSE_CHECK_INTERVAL, TimeUnit.MINUTES);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        } finally {
            Log.clearContext();
        }
    } // start

    @Override
    public void stop()
    {
        Log.log.warn("Stopping SSH Gateway.");
        // TODO destroy all the connection but logout before destroying
    } // start

    /**
     * Clears and set new pool.
     *
     * @param poolList
     */
    public void clearAndSetPools(List<Map<String, String>> poolList)
    {
        // remove all pools
        pools.clear();
        
        // set pools
        for (Map<String, String> params : poolList)
        {
            //ignore the username Map
            if(!params.containsKey("RESOLVE_USERNAME"))
            {
                String subnetMask = (String) params.get(SubnetMask.SUBNETMASK);
    
                // add pool
                pools.put(subnetMask, getSubnetMask(params));
                Log.log.info("Adding SSH subnet pool: " + params);
            }
        }

        // create new pools
        /*synchronized (SSHGateway.class)
        {
            // startupPools.clear();
            // startupPools.addAll(pools.values());
        }
        */
    } // clearAndSetPools

    public void setPool(Map<String, String> params)
    {
        SubnetMask subnetMask = getSubnetMask(params);

        // remove existing pool
        removePool(subnetMask.getSubnetMask());

        // update pools
        pools.put(subnetMask.getSubnetMask(), subnetMask);
    } // setPool

    private SubnetMask getSubnetMask(Map<String, String> params)
    {
        return new SubnetMask((String) params.get(SubnetMask.SUBNETMASK), String.valueOf(params.get(SubnetMask.MAXCONNECTION)), String.valueOf(params.get(SubnetMask.TIMEOUT)));
    } // getPool

    public void removePool(String name)
    {
        SubnetMask pool = pools.get(name);

        if (pool != null)
        {
            // remove from pools
            pools.remove(name);
        }
    } // removePool

    public Map<String, SubnetMask> getPools()
    {
        return pools;
    } // getPools

    /*
     * USERNAME/PASSWORD
     */
    public SSHGatewayConnection checkOutConnection(String username, String password, String ipAddress) throws Exception
    {
        int timeout = getTimeout(ipAddress, defaultTimeout);
        return checkOutConnection(ipAddress, defaultPort, timeout, username, password, null, null, null);
    }

    public SSHGatewayConnection checkOutConnection(String username, String password, String ipAddress, int port) throws Exception
    {
        int timeout = getTimeout(ipAddress, defaultTimeout);
        return checkOutConnection(ipAddress, port, timeout, username, password, null, null, null);
    }

    public SSHGatewayConnection checkOutConnection(String username, String password, String ipAddress, int port, int timeout) throws Exception
    {
        return checkOutConnection(ipAddress, port, timeout, username, password, null, null, null);
    }

    /*
     * PUBLIC KEY
     */
    public SSHGatewayConnection checkOutConnection(String username, String filename, String passphrase, String ipAddress) throws Exception
    {
        int timeout = getTimeout(ipAddress, defaultTimeout);
        return checkOutConnection(ipAddress, defaultPort, timeout, username, null, filename, passphrase, null);
    }

    public SSHGatewayConnection checkOutConnection(String username, String filename, String passphrase, String ipAddress, int port) throws Exception
    {
        int timeout = getTimeout(ipAddress, defaultTimeout);
        return checkOutConnection(ipAddress, port, timeout, username, null, filename, passphrase, null);
    }

    public SSHGatewayConnection checkOutConnection(String username, String filename, String passphrase, String ipAddress, int port, int timeout) throws Exception
    {
        return checkOutConnection(ipAddress, port, timeout, username, null, filename, passphrase, null);
    }

    /*
     * INTERACTIVE
     */
    public SSHGatewayConnection checkOutConnection(String username, List responses, String ipAddress) throws Exception
    {
        int timeout = getTimeout(ipAddress, defaultTimeout);
        return checkOutConnection(ipAddress, defaultPort, timeout, username, null, null, null, responses);
    }

    public SSHGatewayConnection checkOutConnection(String username, List responses, String ipAddress, int port) throws Exception
    {
        int timeout = getTimeout(ipAddress, defaultTimeout);
        return checkOutConnection(ipAddress, port, timeout, username, null, null, null, responses);
    }

    public SSHGatewayConnection checkOutConnection(String username, List responses, String ipAddress, int port, int timeout) throws Exception
    {
        return checkOutConnection(ipAddress, port, timeout, username, null, null, null, responses);
    }

    /*
     * NONE - no SSH authentication
     */
    public SSHGatewayConnection checkOutConnection(String username, String ipAddress) throws Exception
    {
        int timeout = getTimeout(ipAddress, defaultTimeout);
        return checkOutConnection(ipAddress, defaultPort, timeout, username, null, null, null, null);
    }

    public SSHGatewayConnection checkOutConnection(String username, String ipAddress, int port) throws Exception
    {
        int timeout = getTimeout(ipAddress, defaultTimeout);
        return checkOutConnection(ipAddress, port, timeout, username, null, null, null, null);
    }

    public SSHGatewayConnection checkOutConnection(String username, String ipAddress, int port, int timeout) throws Exception
    {
        return checkOutConnection(ipAddress, port, timeout, username, null, null, null, null);
    }

} // SSHGateway
