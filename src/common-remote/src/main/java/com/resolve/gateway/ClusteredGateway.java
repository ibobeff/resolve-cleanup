/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

import com.resolve.esb.MMsgHeader;

/**
 * This interface is implemented by a gateway that's part of a cluster.
 */
public interface ClusteredGateway extends Runnable, Gateway
{
    Filter getFilter(Map<String, Object> params);

    void setFilter(Map<String, Object> params);

    void removeFilter(String id);

    Map<String, Filter> getFilters();

    void clearAndSetFilters(List<Map<String, Object>> filterList);

    void setFilterActive(String id, boolean active);

    NameProperties getNameProperties();

    void setNameProperties(String name, Map prop, boolean doSave);

    void removeNameProperties(String name);

    void clearAndSetNameProperties(List<Map<String, Object>> paramList);

    void setNameProperty(String name, String key, String value);

    void removeNameProperty(String name, String key);
    
    void setSyncDone(boolean b);

    Map<String, String> getSyncRequestParameters();

    Map<String, String> getMessage();

    void setMessage(Map<String, String> message);

    String receiveData(Map<String, String> params);

    boolean isPrimary();

    void setPrimary(boolean primary);

    boolean isOriginalPrimary();

    boolean isSecondary();

    boolean isWorker();

    void checkSystemClockReset();

    GatewayHeartbeat getHeartbeatThread();

    long getHeartbeatInterval();

    long getFailoverInterval();

    void setHeartbeatThread(GatewayHeartbeat heartbeatThread);
    
    MMsgHeader getHeartbeatHeader();
    
    long getLastHeartbeat();
    
    void setLastHeartbeat(long lastHeartbeat);
    void setLastHeartbeat();
    
    void processFilter(Map<String, String> params);
    
    void addToPrimaryDataQueue(Filter filter, Map<String, String> params);
    
    Queue<Map<String, String>> getPrimaryDataQueue();

    void updateServerData(Filter filter, Map<String, String> runbookParams);    
    
    List<TreeSet<RoutingSchema>> getRoutingSchemas();
    
    void clearAndSetRoutingSchemas(Map<String, Object> orgRRSchemaMap);
    
    void sendRoutingSchemaSyncRequest();
    
    String getOrgSuffix();
}
