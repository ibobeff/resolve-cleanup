package com.resolve.gateway.tibcobespoke;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Address;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class TIBCOBespokeRabbitCommunicator
{
    private static final boolean USE_SSL = true;
    private static final String USERNAME = "admin";
    private static final String P_VALUE = "resolve";
    private static final String EXCHANGE_NAME = "TIBCOSTANDALONE";
    private static final String EXCHANGE_TYPE = "direct";  
    private static final String RESPONSE_QUEUE_PREFIX = "TIBCO_RESPONSE#";
    private static final int RESPONSE_CACHE_HEAP_MAX_SIZE = 100000;
    
    private String rabbitHostname;
    private String sslProtocol = "TLSv1.2";
    private Connection connection;
    public Channel publishingChannel;
    
    private ThreadPoolExecutor threadPool;
    
    public Cache<String, String> responseMessages;
    public String processId;
    
    ConcurrentHashMap<String, String> consumerTags;
    
    private static TIBCOBespokeRabbitCommunicator instance;
    
    public TIBCOBespokeRabbitCommunicator(String rabbitHostname, int corePoolSize, int maxPoolSize, int keepAliveTime, int threadBlockingQueueSize, String sslProtocol) throws Exception
    {        
        //Startup threadspool 
        BlockingQueue<Runnable> threadBlockingQueue = new ArrayBlockingQueue<Runnable>(threadBlockingQueueSize);
        threadPool = new ThreadPoolExecutor(corePoolSize, maxPoolSize, keepAliveTime, TimeUnit.SECONDS, threadBlockingQueue);
        threadPool.prestartAllCoreThreads();
        
        this.rabbitHostname = rabbitHostname;
        if (StringUtils.isNotBlank(sslProtocol))
        {
	        this.sslProtocol = sslProtocol;
        }
        processId = ManagementFactory.getRuntimeMXBean().getName().split("@")[0];
        consumerTags = new ConcurrentHashMap<String, String>();
        initConnection();
        initExchange();
        
        instance = this;
    }
    
    public static TIBCOBespokeRabbitCommunicator getInstance()
    {
        return instance;
    }
    
    private void initExchange() throws Exception
    {
        if(connection != null && connection.isOpen())
        {
            Channel tempChannel = getNewChannel();
            tempChannel.exchangeDeclare(EXCHANGE_NAME, EXCHANGE_TYPE);
            tempChannel.close();
        }
        else
        {
            throw new RuntimeException("Connection was not established to RSMQ at point of initializing the exchange");
        }
    }
    
    private void initConnection() throws Exception
    {
            ConnectionFactory factory = new ConnectionFactory();
            if(USE_SSL)
            {
                factory.useSslProtocol(sslProtocol);            
            }
            factory.setUsername(USERNAME);
            factory.setPassword(P_VALUE);
            
            connection = factory.newConnection(Address.parseAddresses(rabbitHostname));
            publishingChannel = connection.createChannel();
    }
    
    public Channel getNewChannel() throws Exception
    {
        Log.log.trace("TIBCOBespokeRabbitCommunicator: A new Channel was created with thread " + Thread.currentThread().getId());
        if(connection == null || !connection.isOpen())
        {
            initConnection();
        }
        
        return connection.createChannel();
        
    }
    
    /**
     * Gets the publishing channel, but first checks if its open
     * @return channel
     * @throws Exception
     */
    public Channel getPublishingChannel() throws Exception
    {        
        if(publishingChannel == null || !publishingChannel.isOpen())
        {
            if(connection == null || !connection.isOpen())
            {
                initConnection();
            }
            else
            {
                Log.log.warn("TIBCOBespokeRabbitCommunicaotr needed to create a publishing channel. Thread: " + Thread.currentThread().getId());
                publishingChannel = connection.createChannel();
            }
        }
        return publishingChannel;
    }
    
    /**
     * Assumes the queue already exists, someone must have subscribed to it already. The subscribers are the owners of the queues, if the subscribing process dies then the queues die
     * @param queueName 
     * @param message
     * @throws Exception 
     */ 
    public void publish(String queueName, String message) throws Exception
    {
        Log.log.trace("RabbitCom: -> Thread: " + Thread.currentThread().getId() + ", Publishing to: " + queueName);
        Channel publishingChannel = getPublishingChannel();
        boolean durable = false; //true if we are declaring a durable queue (the queue will survive a rsmq restart)
        boolean exclusive = false; //true if we are declaring an exclusive queue (restricted to this connection)
        boolean autoDelete = true; //true if we are declaring an autodelete queue (server will delete it when no longer in use)
        publishingChannel.queueDeclare(queueName, durable, exclusive, autoDelete, null);
        publishingChannel.queueBind(queueName, EXCHANGE_NAME, queueName);
        publishingChannel.basicPublish(EXCHANGE_NAME, queueName, null, message.getBytes());
        
    }
    
    public void subscribe(String queueName, DefaultConsumer consumer) throws IOException
    {
        Log.log.trace("RabbitCom: -> Thread: " + Thread.currentThread().getId() + ", Subscribing to: " + queueName);
        Channel consumerChannel = consumer.getChannel();
        boolean durable = false; //true if we are declaring a durable queue (the queue will survive a rsmq restart)
        boolean exclusive = false; //true if we are declaring an exclusive queue (restricted to this connection)
        boolean autoDelete = true; //true if we are declaring an autodelete queue (server will delete it when no longer in use)
        consumerChannel.queueDeclare(queueName, durable, exclusive, autoDelete, null);
        consumerChannel.queueBind(queueName, EXCHANGE_NAME, queueName);
        consumerChannel.queuePurge(queueName); //remove any messages that might have been missed when no subscriber. Messages are typically usless at this time.
        boolean autoAck = true; //will acknowledge message as soon as its delivered
        String consumerTag = consumerChannel.basicConsume(queueName, autoAck, consumer);
        consumerTags.put(queueName, consumerTag);
    }
    
    /***
     * Call me over subscriber(String, DefaultConsumer) easier to setup without having to care about messaging)
     * @param queueName
     * @param worker
     * @throws Exception 
     */
    public void subscribe(String queueName, final TIBCOBespokeRabbitWorker worker) throws Exception
    {
        subscribe(queueName, new DefaultConsumer(getNewChannel()) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
              String message = new String(body, "UTF-8");
              worker.doWork(message);
            }
        });
    }
    
    /**
     * 
     * @param queueName
     * @param server - setup StringRpcServer and override its handleStringCall method to do your work
     * @throws Exception 
     */
    public void subscribeAndRespond(String queueName, final TIBCOBespokeRabbitWorker worker) throws Exception
    {        
        Log.log.trace("RabbitCom: -> Thread: " + Thread.currentThread().getId() + ", SubscribeAndRespond to: " + queueName);
        subscribe(queueName, new DefaultConsumer(getNewChannel()) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException 
            {
              threadPool.execute(new Runnable() {                  
                      byte[] body;
                      @Override
                      public void run()
                      {
                          try
                          {                              
                              String messageString = new String(body, "UTF-8");
                              MessageEnvelope msgEnv = new MessageEnvelope(messageString);
                              String results = worker.doWork(msgEnv.getRabbitContainer());
                              MessageEnvelope responseEnv = new MessageEnvelope();
                              responseEnv.setRabbitContainer(results);
                              responseEnv.setRabbitId(msgEnv.getRabbitId());
                              publish(msgEnv.getResponseQueue(),responseEnv.getEvelopeString()); 
                          }
                          catch(Exception e)
                          {
                              throw new RuntimeException(e.getMessage() + e.getStackTrace().toString());
                          }
                      }
                      public Runnable init(byte[] body)
                      {
                          this.body = body;
                          return(this);
                      }
                  
              }.init(body));                          
            }
        });
        
    }
    
    public void unsubscribe(String queueName)
    {
        Log.log.trace("RabbitCom: -> Thread: " + Thread.currentThread().getId() + ", UnSubscribing to: " + queueName);
        int channelNumber = -1;
        String consumerTag = null;
        try
        {
            consumerTag = consumerTags.get(queueName);
            if(consumerTag != null)
            {
                Channel tmpChannel = getNewChannel();
                channelNumber = tmpChannel.getChannelNumber();
                tmpChannel.basicCancel(consumerTag);
                tmpChannel.close();
            }
            else
            {
                throw new RuntimeException("ConsumerTag did not exist for queueName: "+queueName);
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException("Issue unsubscribing from queueName: " + queueName + ", ChannelNumber: " + channelNumber + ", ConsumerTag: " + consumerTag + "\n"+ e.getMessage() + ": " + e.getStackTrace());
        }
    }
    
    public String request(String queueName, String message, int timeout) throws Exception
    {
        Log.log.trace("RabbitCom: -> Thread: " + Thread.currentThread().getId() + ", Request to: " + queueName);
        initResponseQueue();
        String rabbitId = UUID.randomUUID().toString();
        
        MessageEnvelope messageEnv = new MessageEnvelope();
        messageEnv.setRabbitContainer(message);
        messageEnv.setRabbitId(rabbitId);
        messageEnv.setResponseQueue(RESPONSE_QUEUE_PREFIX + processId);
        
        publish(queueName,messageEnv.getEvelopeString());
        
        long timeoutTime = System.currentTimeMillis() + timeout;
        
        String response = null;
        while(System.currentTimeMillis() < timeoutTime && response == null)
        {
            response = responseMessages.getIfPresent(rabbitId);
            Thread.sleep(25);
        }
        
        if(response != null)
        {
            responseMessages.invalidate(rabbitId);
        }
        if(System.currentTimeMillis() > timeoutTime)
        {
            throw new RuntimeException("TIBCO: Timed Out waiting for reply. Timeout: "+timeout);
        }
        
        return response;
    }
    
    private synchronized void initResponseQueue() throws Exception
    {
        if(responseMessages == null)
        {
            responseMessages = CacheBuilder.newBuilder()
            .concurrencyLevel(4)
            .maximumSize(RESPONSE_CACHE_HEAP_MAX_SIZE)
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .build();
            
            subscribe(RESPONSE_QUEUE_PREFIX + processId, new DefaultConsumer(getNewChannel()) {            
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                  MessageEnvelope messageEnv = new MessageEnvelope(new String(body, "UTF-8"));
                  
                  String rabbitId = messageEnv.getRabbitId();
                  String rabbitContainerContent = messageEnv.getRabbitContainer();
                  responseMessages.put(rabbitId, rabbitContainerContent);
                }
            });
        }
    }
    
    public void disconnect()
    {
        try
        {            
            publishingChannel.close();
            connection.close();
        }
        catch(Exception e)
        {
            System.err.println(e.getStackTrace().toString());
        }
    }
}
