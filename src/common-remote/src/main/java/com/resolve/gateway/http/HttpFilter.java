package com.resolve.gateway.http;

import com.resolve.gateway.BaseFilter;
import com.resolve.util.StringUtils;

public class HttpFilter extends BaseFilter
{
    // Additional field names for this filter. 
    public static final String URI = "URI";
    public static final String PORT = "PORT";
    public static final String SSL = "SSL";
    public static final String BLOCKING = "BLOCKING";
    public static final String ALLOWED_IP = "ALLOWED_IP";
    public static final String BASIC_AUTH_PW = "BASIC_AUTH_PW";
    public static final String BASIC_AUTH_UN = "BASIC_AUTH_UN";
   
    //GAT-26-27 changes
    public static final String CONTENT_TYPE = "CONTENT_TYPE";
    public static final String RESPONSE_FILTER = "RESPONSE_FILTER";
    
    private String uri;
    private Integer port;
    private boolean ssl;
    private Integer blocking;
    private String allowedIP; //comma separated, could be a range of IP, Subnet etc.
    private String basicAuthPassword;
    private String basicAuthUsername;
    //GAT-27 changes to accomodate content type and response filter
    private String contentType;
    private String responsefilter;
    public HttpFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, 
                    String uri, String port, String ssl, String allowedIP, String blocking, String username, String password,
                    String contentType, String responseFilter)
    {
        super(id, active, order, interval, eventEventId, runbook, script);
        
        setUri(uri);
        if(port != null && StringUtils.isNumeric(port.trim()))
        {
            this.port = Integer.parseInt(port.trim());
        }
        if (ssl != null && StringUtils.isTrue(ssl.trim()))
        {
            this.ssl = true;
        }
        if (blocking != null && StringUtils.isNumeric(blocking.trim()))
        {
            this.blocking = Integer.parseInt(blocking.trim());
        }
        //by default the URI is the /filterid, but if the port is provided then
        //there is port opens to listen for data for the filter.
        if(this.port == null && StringUtils.isBlank(this.uri))
        {
            this.uri = "/" + id;
        }
        setAllowedIP(allowedIP);
        
        this.basicAuthPassword = password;
        this.basicAuthUsername = username;
        
        this.contentType = contentType;
        this.responsefilter = responseFilter;
        
    } // HttpFilter

    public String getUri()
    {
        return uri;
    }

    public void setUri(String uri)
    {
    	this.uri = uri;
    	if (StringUtils.isNotEmpty(this.uri))
    	{
    		this.uri = this.uri.trim();
    	}
//        this.uri = StringUtils.isNotEmpty(uri) ? (uri.trim().startsWith("/") ? uri.trim() : "/" + uri.trim()):uri;
    }
    
    public Integer getPort()
    {
        return port;
    }

    public void setPort(Integer port)
    {
        this.port = port;
    }

    public boolean isSsl()
    {
        return ssl;
    }

    public void setSsl(boolean ssl)
    {
        this.ssl = ssl;
    }
    
    public Integer getBlocking()
    {
        return blocking;
    }

    public void setBlocking(Integer blocking)
    {
        this.blocking = blocking;
    }

    public String getAllowedIP()
    {
        return allowedIP;
    }

    public void setAllowedIP(String allowedIP)
    {
        this.allowedIP = allowedIP != null ? allowedIP.trim() : allowedIP;
    }
    
    public int getBlockingCode()
    {
        return getBlocking().intValue();
    }
    
    public String getContentType() {
   		return contentType;
    }
   
   	public void setContentType(String contentType) {
    	this.contentType = contentType;
   	}
   	public String getResponsefilter() {
   		return responsefilter;
   	}
    
   	public void setResponsefilter(String responsefilter) {
   		this.responsefilter = responsefilter;
   	}
    
    public String getHttpAuthBasicUsername()
    {
        return basicAuthUsername;
    }

    public void setHttpAuthBasicUsername(String httpAuthBasicUsername)
    {
        this.basicAuthUsername = httpAuthBasicUsername;
    }

    public String getHttpAuthBasicPassword()
    {
        return basicAuthPassword;
    }

    public void setHttpAuthBasicPassword(String httpAuthBasicPassword)
    {
        this.basicAuthPassword = httpAuthBasicPassword;
    }
}
