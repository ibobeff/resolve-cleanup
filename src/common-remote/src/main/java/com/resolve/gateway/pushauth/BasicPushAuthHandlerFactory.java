package com.resolve.gateway.pushauth;
 
import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.security.auth.Subject;

import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.MappedLoginService;
import org.eclipse.jetty.server.UserIdentity;
import org.eclipse.jetty.util.component.ContainerLifeCycle;
import org.eclipse.jetty.util.security.Constraint;
import org.eclipse.jetty.util.security.Credential;
import org.eclipse.jetty.util.security.Password;

import org.apache.commons.lang.StringUtils;
import com.resolve.util.ConfigMap;
import com.resolve.util.Log;

class BasicPushAuthHandlerFactory implements ServletAuthHandlerFactory
{

    @Override
    public ConstraintSecurityHandler create(ConfigMap map, ContainerLifeCycle server, String context) throws InvalidAuthConfigurationException
    {
        
        String username =  PushAuthConfigUtil.getValueFromConfig(map, PushAuthConstants.PROPERTY_NAME_HTTP_AUTH_BASIC_USERNAME, true);
        String password =  PushAuthConfigUtil.getValueFromConfig(map, PushAuthConstants.PROPERTY_NAME_HTTP_AUTH_BASIC_PASSWORD, true);
        //If a username has been provided but no password, or vice versa we throw an exception here
        
        if((username == null || username.isEmpty()) != (password == null || password.isEmpty()))
        {
           throw new InvalidAuthConfigurationException("Gateway-wide basic authentication requires configuration of both username and password but only one was provided for gateway " + context);     
        }
        
        SingleAuthLoginService loginService = new SingleAuthLoginService();
        server.addBean(loginService);
        
        
        ConstraintSecurityHandler out = new ConstraintSecurityHandler();
         
        out.addRole("user");
        
        out.setAuthenticator(new BasicAuthenticatorEx());
        out.setLoginService(loginService);
 
        return out;
    }  
     
    
    
    /**
     * This is called when a filter is registered.  
     * 
     * This does the following:
     * 1.  Resolves any global username and password that may be set at the gateway level
     * 2.  Finds if there is a username and password set at the filter level
     * 3.  Adds a user based on the filter level password if it exists, otherwise it tries adding the global username and password.  
     * 4.  If no user and password has been configured, then we throw an InvalidAuthConfigurationException
     * 5.  Then we create a constraint mapping that links the uri path of the filter to the authenticator
     */
    @Override
    public void registerFilter(ConfigMap config, ConstraintSecurityHandler csh, Object filter) throws InvalidAuthConfigurationException
    {
         

         String gusername =  PushAuthConfigUtil.getValueFromConfig(config, PushAuthConstants.PROPERTY_NAME_HTTP_AUTH_BASIC_USERNAME, true);
         String gpassword =  PushAuthConfigUtil.getValueFromConfig(config, PushAuthConstants.PROPERTY_NAME_HTTP_AUTH_BASIC_PASSWORD, true);
        
        
         String username =  PushAuthConfigUtil.getValueFromConfig(filter, PushAuthConstants.PROPERTY_NAME_HTTP_AUTH_BASIC_USERNAME, true);
         String password =  PushAuthConfigUtil.getValueFromConfig(filter, PushAuthConstants.PROPERTY_NAME_HTTP_AUTH_BASIC_PASSWORD, true);
         
         
         String uri =  PushAuthConfigUtil.getValueFromConfig(filter, PushAuthConstants.PROPERTY_NAME_HTTP_URI, true);
 
         
         SingleAuthLoginService  sals = (SingleAuthLoginService) csh.getLoginService();
       
          if(notNullAndNotEmpty(username) && nullOrEmpty(password))
          {
              throw new InvalidAuthConfigurationException("Filter at path '" + uri + "' of gateway has filter specified with username and no password");     

          }
          
          if(notNullAndNotEmpty(password) && nullOrEmpty(username))
          {
              throw new InvalidAuthConfigurationException("Filter at path '" + uri + "' of gateway has filter specified with password and no username");     

          }
          
          
          
         
         if(notNullAndNotEmpty(username) && notNullAndNotEmpty(password))
             sals.addUser(uri, username, password);
         else
             if(notNullAndNotEmpty(gusername) && notNullAndNotEmpty(gpassword))
                 sals.addUser(uri, gusername, gpassword);
             else
             { 
                 throw new InvalidAuthConfigurationException("Filter at path '" + uri + "' of gateway has no user configured for authentication.");     

             }
          
         
         Constraint cs = new Constraint();
         cs.setName(extractFilterIdentityName(filter));
         cs.setAuthenticate(true);
         cs.setRoles(new String[] {"user"}); 
 
         ConstraintMapping cm = new ConstraintMapping();
         cm.setConstraint(cs);
         cm.setPathSpec(uri);
         csh.addRole("user");
         replaceConstraintMapping(cm, csh);
        // csh.setConstraintMappings(csh.getConstraintMappings());
      
         Log.log.info("Filter with path '" + uri + "' has been registered for basic authentication");
         
    }



    private static boolean nullOrEmpty(String s)
    {
        return s == null || StringUtils.isEmpty(s);
    }



    private static boolean notNullAndNotEmpty(String s)
    {
        return s != null && !StringUtils.isEmpty(s);
    }


    private String extractFilterIdentityName(Object filter)
    {
        return Integer.toHexString(System.identityHashCode(filter));
    }
    
    private void replaceConstraintMapping(ConstraintMapping cs, ConstraintSecurityHandler sc)
    {       
        removeConstraintMapping(cs, sc.getConstraintMappings());
        
        sc.addConstraintMapping(cs);
        
    }


    private void removeConstraintMapping(ConstraintMapping cs, List<ConstraintMapping> mappings)
    {
        String nm =  cs.getConstraint().getName();
        
        removeConstraintByName(mappings, nm);
    }


    private void removeConstraintByName(List<ConstraintMapping> mappings, String nm)
    {   
        if(mappings != null)
        for(int i = mappings.size() -1 ; i > -1; --i)
        {
            
            if(nm.equals(mappings.get(i).getConstraint().getName()))
                         mappings.remove(i);
            
            
        }
    }
    /**
     * This is a login service that handles per-filter logins
     *  
     *
     */
    public static class SingleAuthLoginService extends MappedLoginService
    {
        
        protected Map<String, UserIdentity> identities = new HashMap<String, UserIdentity>();
        
        
        public SingleAuthLoginService( )
        {
            super();
        
        }
 
        
        
        @Override
        protected UserIdentity loadUser(String arg0)
        {
            
            
            return identities.get(arg0);
           
        }
        @Override
        protected void loadUsers() throws IOException
        {
            //nothing here for now
        }
        /**
         * Adds a user with the specified uri prefix.  
         * 
         * The prefix is added to the user name to make users independent of each other per filter.  
         * @param uriPrefix The prefix
         * @param un        The username
         * @param password  The password
         */
        public void addUser(String uriPrefix, String un, String password)
        {
            
            String username = PushAuthConfigUtil.flattenURI(uriPrefix) + "\\" + un;
            
            
           
           
            Credential credential = new Password(password);
            Principal userPrincipal = new MappedLoginService.KnownUser(username ,credential);
            
            Subject subject = new Subject();
            subject.getPrincipals().add(userPrincipal);
            subject.getPrivateCredentials().add(credential);
            subject.setReadOnly();
            
            final UserIdentity identity= getIdentityService().newUserIdentity(subject,userPrincipal,new String[]{ "user" });
            UserIdentity ui = putUser(username, identity);
             
            identities.put(username, ui);
            
        }
        
        /**
         * Removes users with the specified uri prefix
         * @param prefix
         */
        public void removeUsersWithUriPrefix(String prefix)
        {
            if(!prefix.endsWith("\\"))
                prefix = prefix + "\\";
            Iterator<Entry<String, UserIdentity>> ii = identities.entrySet().iterator();
          
            while(ii.hasNext())
            {                
               Entry<String, UserIdentity> ent =  ii.next();
                
               if(ent.getKey().startsWith(prefix))
                   ii.remove();
                    
            }
        
        }
        
        
    }

    @Override
    public void unregisterFilter(ConfigMap map, ConstraintSecurityHandler csh, Object filter)
    {
         
        
        removeConstraintByName(csh.getConstraintMappings(), extractFilterIdentityName(filter));
        String uri =  PushAuthConfigUtil.getValueFromConfig(filter, PushAuthConstants.PROPERTY_NAME_HTTP_URI, false);
           
        if((uri != null) && csh.getLoginService() instanceof SingleAuthLoginService)
        {
           
            
            ((SingleAuthLoginService) csh.getLoginService()).removeUsersWithUriPrefix(uri);
             
            
        }
        

    }




    
}
