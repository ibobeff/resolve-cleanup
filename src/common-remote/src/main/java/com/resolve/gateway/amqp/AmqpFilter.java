/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.amqp;

import com.resolve.gateway.BaseFilter;

public class AmqpFilter extends BaseFilter
{
    // Additional field names for this filter.
    public static final String TARGET_QUEUE = "TARGET_QUEUE";
    public static final String IS_EXCHANGE = "IS_EXCHANGE";

    private String targetQueue;
    private boolean isExchange;

    public AmqpFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String targetQueue, String isExchange)
    {
        super(id, active, order, interval, eventEventId, runbook, script);
        
        setTargetQueue(targetQueue);
        setExchange(isExchange);
    } // AmqpFilter

    public String getTargetQueue()
    {
        return targetQueue;
    }

    public void setTargetQueue(String targetQueue)
    {
        this.targetQueue = targetQueue != null ? targetQueue.trim() : targetQueue;
    }

    public boolean isExchange()
    {
        return isExchange;
    }

    public void setExchange(boolean isExchange)
    {
        this.isExchange = isExchange;
    }
    
    public void setExchange(String isExchange)
    {
        if (isExchange != null && isExchange.trim().equalsIgnoreCase("TRUE"))
        {
            this.isExchange = true;
        }
        else
        {
            this.isExchange = false;
        }
    }
}
