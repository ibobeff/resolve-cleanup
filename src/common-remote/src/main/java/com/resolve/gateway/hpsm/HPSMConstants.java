/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.hpsm;

/**
 * This interface defines constants for all the Incident's field name
 *
 */
public interface HPSMConstants
{
    String INCIDENT_ID = "IncidentID";
    String INCIDENT_CATEGORY = "Category";
    String INCIDENT_OPENTIME = "OpenTime";
    String INCIDENT_OPENEDBY = "OpenedBy";
    String INCIDENT_URGENCY = "Urgency";
    String INCIDENT_UPDATEDTIME = "UpdatedTime";
    String INCIDENT_ASSIGNMENTGROUP = "AssignmentGroup";
    String INCIDENT_CLOSEDTIME = "ClosedTime";
    String INCIDENT_CLOSEDBY = "ClosedBy";
    String INCIDENT_CLOSURECODE = "ClosureCode";
    String INCIDENT_AFFECTEDCI = "AffectedCI";
    String INCIDENT_DESCRIPTION = "Description";
    String INCIDENT_SOLUTION = "Solution";
    String INCIDENT_ASSIGNEE = "Assignee";
    String INCIDENT_CONTACT = "Contact";
    String INCIDENT_JOURNALUPDATES = "JournalUpdates";
    String INCIDENT_ALERTSTATUS = "AlertStatus";
    String INCIDENT_CONTACTLASTNAME = "ContactLastName";
    String INCIDENT_CONTACTFIRSTNAME = "ContactFirstName";
    String INCIDENT_COMPANY = "Company";
    String INCIDENT_TITLE = "Title";
    String INCIDENT_TICKETOWNER = "TicketOwner";
    String INCIDENT_UPDATEDBY = "UpdatedBy";
    String INCIDENT_STATUS = "Status";
    String INCIDENT_AREA = "Area";
    String INCIDENT_SLAAGREEMENTID = "SLAAgreementID";
    String INCIDENT_SITECATEGORY = "SiteCategory";
    String INCIDENT_SUBAREA = "Subarea";
    String INCIDENT_PROBLEMTYPE = "ProblemType";
    String INCIDENT_RESOLUTIONFIXTYPE = "ResolutionFixType";
    String INCIDENT_USERPRIORITY = "UserPriority";
    String INCIDENT_LOCATION = "Location";
    String INCIDENT_EXPLANATION = "explanation"; //yes, it is lowercase
    String INCIDENT_IMPACT = "Impact";
    String INCIDENT_FOLDER = "folder";
    String INCIDENT_SERVICE = "Service";
    //String INCIDENT_ATTACHMENTS = "attachments";
}
