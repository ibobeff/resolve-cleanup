/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.amqp;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Address;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.resolve.esb.ESBException;
import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveAmqp;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class is the lowest level member which directly talks to RabbitMQ using RabbitMQ APIs.
 *
 */
public class AmqpDriver
{
    private static String hosts = "";
    private boolean isSSL = true;
    private static AMQP.BasicProperties properties;

    private final ConfigReceiveAmqp config;
    
    Map<String, String> queues = new HashMap<String, String>();

    public AmqpDriver(final ConfigReceiveAmqp config) throws ESBException
    {
        this.config = config;

        //generate the RabbitMQ host string
        //e.g., 192.168.1.1:5674,192.168.1.2:5674
        hosts = config.getServerwithport();
        isSSL = config.isSsl();
        //Check Javadoc http://www.rabbitmq.com/releases/rabbitmq-java-client/v3.1.3/rabbitmq-java-client-javadoc-3.1.3/com/rabbitmq/client/AMQP.BasicProperties.html
        properties = new AMQP.BasicProperties(null, null, null, null, null, null, null, null, null, null, null, null, null, null);

        // create connection factory
        boolean connected = false;
        Connection connection = getConnection();
        if (connection != null && connection.isOpen())
        {
            Channel channel;
            try
            {
                channel = connection.createChannel();
                if (channel != null && channel.isOpen())
                {
                    connected = true;
                    channel.close();
                    connection.close();
                }
            }
            catch (IOException e)
            {
                Log.log.error(e.getMessage(), e);
                throw new ESBException(e.getMessage(), e);
            }
            catch (/*Timeout*/Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw new ESBException(e.getMessage(), e);
            }
        }

        if (connected == false)
        {
            Log.log.fatal("Failed to connect to AMQP messaging service");
        }
    } // MDriverRabbitMQ

    public Connection getConnection() throws ESBException
    {
        Connection connection = null;
        boolean connected = false;
        String sslProtocol = MainBase.main.configESB.config.getSslProtocol();
        int connectionTimeout = MainBase.main.configESB.config.getConnectionTimeout();
        for (int connectRetry = 0; !connected && connectRetry < config.getConnectionretry(); connectRetry++)
        {
            try
            {
                ConnectionFactory factory = new ConnectionFactory();
                if (isSSL)
                {
                    factory.useSslProtocol(sslProtocol);
                }
                String virtualHost = config.getVirtualhost();
                if(StringUtils.isBlank(virtualHost))
                {
                    virtualHost = "/";
                }
                factory.setConnectionTimeout(connectionTimeout);
                factory.setVirtualHost(virtualHost);
                factory.setUsername(config.getUsername());
                factory.setPassword(config.getPassword());
                Address[] addresses = Address.parseAddresses(hosts);
                connection = factory.newConnection(addresses);
                if (connection != null && connection.isOpen())
                {
                    connected = true;
                }
            }
            catch (IOException e)
            {
                Log.log.error(e.getMessage() + ". Use Loglevel TRACE to see stack");
                Log.log.trace(e.getMessage(), e);
                throw new ESBException(e.getMessage(), e);
            }
            catch (KeyManagementException e)
            {
                Log.log.error(e.getMessage(), e);
                throw new ESBException(e.getMessage(), e);
            }
            catch (NoSuchAlgorithmException e)
            {
                Log.log.error(e.getMessage(), e);
                throw new ESBException(e.getMessage(), e);
            }
            catch (/*Timeout*/Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw new ESBException(e.getMessage(), e);
            }
        }
        return connection;
    } // getConnection

    /**
     * Removes Queues from the server
     */
    public void close() throws ESBException
    {
    }

    public boolean subscribe(final Channel channel, final String exchangeName, final String subscriberId) throws ESBException
    {
        boolean result = true;
        try
        {
            // actually in RabbitMQ exchange every subscribers listens
            // to a unique queue which is bounded to an exchange.
            String queueName = subscriberId;
            String routingKey = subscriberId;

            // now bind the subscriber queue to the exchange.
            channel.queueBind(queueName, exchangeName, routingKey);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
        return result;
    }

    public boolean unsubscribe(final Channel channel, final String exchangeName, final String subscriberId) throws ESBException
    {
        boolean result = true;
        try
        {
            if (channel != null && channel.isOpen())
            {
                String queueName = subscriberId;
                String routingKey = subscriberId;

                // now unbind the subscriber queue from the exchange.
                channel.queueUnbind(queueName, exchangeName, routingKey);
            }
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
        return result;
    }

    /**
     * This sends to a queue.
     *
     * @param channel
     * @param queueName
     * @param message
     * @return
     * @throws ESBException
     */
    public boolean send(final Channel channel, final String queueName, final byte[] message) throws ESBException
    {
        boolean result = true;
        try
        {
            channel.queueDeclare(queueName, false, false, false, null);
            channel.basicPublish("", queueName, properties, message);
            // System.out.println(" [x] Sent '" + message + "'");
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
        return result;
    }

    /**
     * Publishes a message to an exchange (similar to JMS Topic)
     *
     * @param channel
     * @param topicName
     * @param message
     * @return
     * @throws ESBException
     */
    public boolean publish(final Channel channel, final String topicName, final byte[] message) throws ESBException
    {
        boolean result = true;
        try
        {
            String exchangeName = topicName;
            String routingKey = topicName;

            channel.basicPublish(exchangeName, routingKey, properties, message);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new ESBException(e.getMessage(), e);
        }
        return result;
    }
} // AmqpDriver
