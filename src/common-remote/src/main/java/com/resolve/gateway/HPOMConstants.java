/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

/**
 * This interface defines various fixed attributes supported for the HP
 * Operations Manager event object. In HPOM terms event is generally called
 * incident.
 * 
 */
public interface HPOMConstants
{
    public static final String FIELD_EVENT_ID = "EventId";
    public static final String FIELD_AFFECTED_CI = "AffectedCI";
    public static final String FIELD_CATEGORY = "Category";
    public static final String FIELD_COLLABORATION_MODE = "CollaborationMode";
    public static final String FIELD_DESCRIPTION = "Description";
    public static final String FIELD_ESCALATION_STATUS = "EscalationStatus";
    public static final String FIELD_LIFECYCLE_STATE = "LifecycleState";
    public static final String FIELD_PROBLEM_TYPE = "ProblemType";
    public static final String FIELD_PRODUCT_TYPE = "ProductType";
    public static final String FIELD_SEVERITY = "Severity";
    public static final String FIELD_SOLUTION = "Solution";
    public static final String FIELD_SUB_CATEGORY = "SubCategory";
    public static final String FIELD_TITLE = "Title";
    public static final String FIELD_TYPE = "Type";
    public static final String FIELD_ASSIGNED_OPERATOR = "AssignedOperator";
    public static final String FIELD_CHANGE_TYPE = "ChangeType";
    public static final String FIELD_ACKNOWLEDGE_CORRELATION_KEY = "AcknowledgeCorrelationKey";
    public static final String FIELD_APPLICATION = "Application";
    public static final String FIELD_CORRELATION_KEY = "CorrelationKey";
    public static final String FIELD_OBJECT = "Object";
    public static final String FIELD_SOURCE = "Source";
    public static final String FIELD_ORIGINAL_EVENT = "OriginalEvent";
    public static final String FIELD_RECEIVED_TIME = "ReceivedTime";
    public static final String FIELD_CREATION_TYPE = "CreationTime";
    public static final String FIELD_STATE_CHANGE_TIME = "StateChangeTime";

    public static final String FIELD_EMITTING_CI_ID = "EmittingCI_ID";
    public static final String FIELD_EMITTING_CI_NAME = "EmittingCI_Name";
    public static final String FIELD_EMITTING_NODE_SHORTHOSTNAME = "EmittingNode_ShortHostname";
    public static final String FIELD_EMITTING_NODE_PRIMARYDNSDOMAINNAME = "EmittingNode_PrimaryDnsDomainName";
    public static final String FIELD_EMITTING_NODE_NETWORKADDRESS = "EmittingNode_NetworkAddress";
    public static final String FIELD_EMITTING_NODE_DNSNAME = "EmittingNode_DnsName";
    public static final String FIELD_EMITTING_NODE_COREID = "EmittingNode_CoreId";
}
