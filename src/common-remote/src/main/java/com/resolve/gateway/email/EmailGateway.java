/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.email;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;

import com.resolve.connect.EmailConnect2;
import com.resolve.gateway.BaseSocialGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MEmail;
import com.resolve.query.QueryToken;
import com.resolve.query.ResolveQuery;
import com.resolve.query.translator.EmailQueryTranslator;
import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveEmail;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;
import com.resolve.util.queue.ResolveConcurrentLinkedQueue;

/**
 * @modified alokika.dash
 *
 */
public class EmailGateway extends BaseSocialGateway
{
    // Singleton
    private static volatile EmailGateway instance = null;

    final static int MAX_ATTACHMENT_READ_SIZE = 5242880;
    // number of seconds to timeout if a message processing is blocked
    private final static int MESSAGE_PROCESS_TIMEOUT = 30;

    private static final Map<String, String> defaultEmailAttributes = new HashMap<String, String>();
    private static final Map<String, String> defaultEmailHeaderAttributes = new HashMap<String, String>();
    private static final Set<SimpleDateFormat> dateFormats = new HashSet<SimpleDateFormat>();

    static
    {
        defaultEmailAttributes.put("subject", "string");
        defaultEmailAttributes.put("body", "string");
        defaultEmailAttributes.put("sentdate", "date");
        defaultEmailAttributes.put("size", "number");
        defaultEmailAttributes.put("replyto", "string");
        defaultEmailAttributes.put("recipient", "string");
        defaultEmailAttributes.put("description", "string");

        defaultEmailHeaderAttributes.put("returnpath", "string"); // Return-Path
        defaultEmailHeaderAttributes.put("received", "string"); // Received
        defaultEmailHeaderAttributes.put("clientaddress", "string"); // X-ClientAddr
        defaultEmailHeaderAttributes.put("messageid", "string"); // Message-ID
        defaultEmailHeaderAttributes.put("contenttype", "string"); // Content-Type
        defaultEmailHeaderAttributes.put("receivedspf", "string"); // Received-SPF
        defaultEmailHeaderAttributes.put("authresults", "string"); // Authentication-Results
        defaultEmailHeaderAttributes.put("mimeversion", "string"); // MIME-Version

        dateFormats.add(new SimpleDateFormat("MM/dd/yyyy hh:mm:ss.S"));
        dateFormats.add(new SimpleDateFormat("MM/dd/yyyy hh:mm:ss"));
        dateFormats.add(new SimpleDateFormat("MM/dd/yyyy"));
        dateFormats.add(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S"));
        dateFormats.add(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"));
        dateFormats.add(new SimpleDateFormat("yyyy-MM-dd"));
    }

    private Session session;
    // private Folder folder;
    // private Store store;
    // private Transport transport;
    private String queue = "EMAIL";

    private Properties props;
    private String ipaddr;
    private int port;
    private boolean secure;
    private String protocol;
    private boolean isSocialPost;
    private String username;
    private String password;
    private int interval;
    private String emailfolder;
  //Flag for confirming if the emails should be deleted after read (Set in blueprint.properties)
    private boolean deleteAfterRead = false;
    private String smtphost = "localhost";
    private int smtpport = 25;
    private String smtpusername = "";
    private String smtppassword = "";
    private boolean isSmtpSSL;
    private Map<String, String> smtpProperties = null;

    // public static ConcurrentLinkedQueue<Map<String, Object>> sendQueue = new
    // ConcurrentLinkedQueue<Map<String, Object>>();
    private static ResolveConcurrentLinkedQueue<Map<String, Object>> sendQueue = null;
    protected ConcurrentHashMap<String, EmailAddress> emailAddresses = new ConcurrentHashMap<String, EmailAddress>();

    protected ConcurrentHashMap<String, Store> emailStores = new ConcurrentHashMap<String, Store>();
    protected ConcurrentHashMap<String, Folder> emailFolders = new ConcurrentHashMap<String, Folder>();

    private final ResolveQuery resolveQuery;

    public static EmailGateway getInstance(ConfigReceiveEmail config)
    {
        if (instance == null)
        {
            instance = new EmailGateway(config);
        }
        return (EmailGateway) instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static EmailGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("Email Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    /**
     * Private constructor.
     *
     * @param config
     */
    private EmailGateway(ConfigReceiveEmail config)
    {
        // Important, call super here.
        super(config);
        queue = config.getQueue();
        resolveQuery = new ResolveQuery(new EmailQueryTranslator());
    } // EmailGateway

    @Override
    protected void initialize()
    {
        ConfigReceiveEmail config = (ConfigReceiveEmail) configurations;

        queue = config.getQueue().toUpperCase();

        try
        {
            Log.log.info("Initializing EmailListener");

            this.ipaddr = config.getIpaddr();
            this.port = config.getPort();
            this.secure = config.isSsl();
            this.protocol = config.getProtocol();
            if (this.protocol == null)
            {
                this.protocol = "pop3";
            }
            this.isSocialPost = config.isSocialpost();
            this.username = config.getUsername();
            this.password = config.getPassword();
            this.interval = config.getInterval() * 1000;

            this.emailfolder = config.getFolder();
            if (emailfolder == null)
            {
                this.emailfolder = "INBOX";
            }
            
            deleteAfterRead = config.isMessagedelete();
            
            this.smtphost = config.getSmtphost();
            this.smtpport = config.getSmtpport();
            this.smtpusername = config.getSmtpusername();
            this.smtppassword = config.getSmtppassword();
            this.isSmtpSSL = config.isSmtpssl();
            if (StringUtils.isNotBlank(config.getSmtpproperties()))
            {
                this.smtpProperties = StringUtils.stringToMap(config.getSmtpproperties(), "=", ",");
                Log.log.debug("smtpProperties: " + smtpProperties); 
            }

            if (Log.log.isDebugEnabled())
            {
                Log.log.debug(config.toString());
            }

            this.running = true;
            this.gatewayConfigDir = "/config/email/";

            this.props = new Properties();
            String properties = config.getProperties();
            if (!StringUtils.isEmpty(properties))
            {
            	if(properties.equalsIgnoreCase("imap.properties")) {
            		props.setProperty("mail.store.protocol", "imaps");
            		
            	}else {
	                File propertiesFile = config.getPropertiesFile();
	                if (propertiesFile != null && propertiesFile.exists())
	                {
	                    FileInputStream fis = new FileInputStream(propertiesFile);
	                    props.load(fis);
	                    fis.close();
	                }
	                else
	                {
	                    Log.log.warn("Failed to load properties file: " + properties);
	                }
            	}
            }
            Log.log.debug("pop3Properties: " + props);

            // connect to the email server so it can be resused.
            connectEmailServer();

            // Add listener;
            QueueListener<Map<String, Object>> emailListener = new SendEmailListener();
            sendQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(emailListener);
        }
        catch (Exception e)
        {
            Log.log.error("Failed to config Email Gateway: " + e.getMessage(), e);
        }
    }

    @Override
    public String getLicenseCode()
    {
        return "EMAIL";
    }

    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_EMAIL;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MEmail.class.getSimpleName();
    }

    @Override
    protected Class<MEmail> getMessageHandlerClass()
    {
        return MEmail.class;
    }

    public boolean isSocialPoster()
    {
        return isSocialPost;
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new EmailFilter((String) params.get(EmailFilter.ID), (String) params.get(EmailFilter.ACTIVE), (String) params.get(EmailFilter.ORDER), (String) params.get(EmailFilter.INTERVAL), (String) params.get(EmailFilter.EVENT_EVENTID), (String) params.get(EmailFilter.RUNBOOK), (String) params.get(EmailFilter.SCRIPT), (String) params.get(EmailFilter.QUERY));
    }

    public Map<String, EmailAddress> getEmailAddresses()
    {
        return emailAddresses;
    } // getEmailAddresses

    public void setEmailAddresses(Map<String, Object> params)
    {
        EmailAddress emailAddress = getEmailAddress(params);

        // remove existing emailAddress
        removeEmailAddress(emailAddress.getEmailAddress());

        // update emailAddress
        emailAddresses.put(emailAddress.getEmailAddress(), emailAddress);
    }

    public EmailAddress getEmailAddress(Map<String, Object> params)
    {
        return new EmailAddress((String) params.get(EmailAddress.EMAILADDRESS), (String) params.get(EmailAddress.EMAILP_ASSWORD));
    }

    public void removeEmailAddress(String id)
    {
        EmailAddress emailAddress = emailAddresses.get(id);
        if (emailAddress != null)
        {
            // remove from filters
            emailAddresses.remove(id);
        }
    } // removeFilter

    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    public void clearAndSetEmailAddresses(List<Map<String, Object>> emailAddressesList)
    {
        emailAddresses.clear();
        disconnect();
        emailFolders.clear();
        emailStores.clear();

        if (emailAddressesList != null)
        {
            for (Map<String, Object> params : emailAddressesList)
            {
                //ignore the username Map
                if(!params.containsKey("RESOLVE_USERNAME"))
                {
                    String name = (String) params.get("EMAILADDRESS");
                    emailAddresses.put(name, getEmailAddress(params));
                    params.remove(EmailAddress.EMAILP_ASSWORD);
                    Log.log.info("Adding Email Address: " + params);
                }
            }
        }
        connectEmailServer();
    } // clearAndSetEmailAddresses

    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
        // nothing to do at this point.
    }

    @Override
    public void run()
    {
        // This call is must for all the ClusteredGateways.
        super.sendSyncRequest();

        final long initTime = System.currentTimeMillis();

        boolean isReceivedData = false;

        while (running)
        {
            try
            {
                long startTime = System.currentTimeMillis();
                if (emailFolders == null)
                {
                    Thread.sleep(interval);
                    Log.log.warn("Email Reconnecting");
                    connectEmailServer();
                }
                else
                {
                    for (Folder folder : emailFolders.values())
                    {
                        if (!folder.isOpen())
                        {
                            folder.open(Folder.READ_WRITE);
                        }
                    }

                    /* if any remaining events are in primary data queue or 
                     * primary data executor has waiting messages still to be processed, 
                     * do not get messages for more new events.
                     */
                    
                    if (primaryDataQueue.isEmpty())
                    {
                        Log.log.trace(getQueueName() + " Primary Data Queue is empty.....");
                        
                        // Only pull unread, since the query provides the key
                        // "sentdate" we
                        // cannot hardcode to read only new email like older
                        // versions (e.g., 3.4.1)
                        FlagTerm ft = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
                        for (String username : emailFolders.keySet())
                        {
                            Folder folder = emailFolders.get(username);
                            // get messages
                            folder.getMessages();
                            Message messages[] = folder.search(ft);
                            isReceivedData = false;
                            if (messages != null && messages.length > 0)
                            {
                                Log.log.debug("Getting " + messages.length + " messages from " + username);
                                isReceivedData = true;
                                for (Message message : messages)
                                {
                                    // we've seen that this could be a blocking
                                    // call for some
                                    // email content. need to protect with
                                    // timeout so that
                                    // processing continues for other messages.
                                    processData(username, message);
                                    message.setFlag(Flags.Flag.DELETED, deleteAfterRead);
                                    Log.log.debug("Marking message as delete = " + deleteAfterRead +
                                    		" With Subject: " + message.getSubject());
                                   // message.setFlag(Flags.Flag.DELETED, true);
                                }
                            }
                            folder.close(true);
                        }
                    }
                }

                // process events collected by processFilter.
                //processFilterEvents(startTime);

                // if we didn't received any data last time then wait
                // otherwise don't sleep
                if (!isReceivedData)
                {
                    Thread.sleep(interval);
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Error: " + e.getMessage(), e);
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", 
                          "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + 
                          "due to " + e.getMessage());
                
                try
                {
                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    }

    /**
     * This method wraps processData (a blocking method) so it can time out in
     * case processDate taking longers than expected processing certain message.
     *
     * @param receiver
     * @param message
     * @throws Exception
     */
    private void processData(final String receiver, final Message message) throws Exception
    {
        ExecutorService executor = Executors.newCachedThreadPool();
        Callable<Boolean> task = new Callable<Boolean>()
        {
            public Boolean call()
            {
                Boolean result = Boolean.TRUE;
                try
                {
                    processMessage(receiver, message);
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage(), e);
                    result = Boolean.FALSE;
                }
                return result;
            }
        };
        Future<Boolean> future = executor.submit(task);
        try
        {
            // 10 seconds timeout.
            Boolean result = future.get(MESSAGE_PROCESS_TIMEOUT, TimeUnit.SECONDS);
            if (result)
            {
                Log.log.trace("Message processed successfully");
            }
        }
        catch (TimeoutException e)
        {
            Log.log.warn("Timeout processing message to: " + receiver + " with the subject: " + message.getSubject(), e);
        }
        catch (InterruptedException e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        catch (ExecutionException e)
        {
            Log.log.warn(e.getMessage(), e);
        }
        finally
        {
            future.cancel(true); // may or may not desire this
        }
    }

    /**
     * This method processes the message received from the Email server. It
     * checks the message with any interested filter and process the filter.
     *
     * @param message
     * @throws Exception
     */
    private void processMessage(String receiver, Message message) throws Exception
    {
        Map<String, String> runbookParams = new HashMap<String, String>();

        // init parameters
        String received = "";
        if (message.getReceivedDate() != null)
        {
            received = "" + message.getReceivedDate().getTime();
        }

        String sent = "";
        if (message.getSentDate() != null)
        {
            sent = "" + message.getSentDate().getTime();
        }

        String from = parseAddresses(message.getFrom());
        String replyTo = parseAddresses(message.getReplyTo());
        String recipients = parseAddresses(message.getAllRecipients());

        String headerValues = "";

        Enumeration headers = message.getAllHeaders();

        while (headers.hasMoreElements())
        {
            Header header = (Header) headers.nextElement();
            if (StringUtils.isEmpty(headerValues))
            {
                headerValues = header.getName() + "=" + header.getValue();
            }
            else
            {
                headerValues += "&" + header.getName() + "=" + header.getValue();
            }
        }

        String subject = "";
        if (!StringUtils.isEmpty(message.getSubject()))
        {
            subject = message.getSubject();
        }
        String description = "";
        if (!StringUtils.isEmpty(message.getDescription()))
        {
            description = message.getDescription();
        }

        runbookParams.put("DATE", received);
        runbookParams.put("SENT", sent);
        runbookParams.put("SUBJECT", subject);
        runbookParams.put("SIZE", "" + message.getSize());
        runbookParams.put("FROM", from);
        runbookParams.put("REPLY_TO", replyTo);
        runbookParams.put("RECIPIENTS", recipients);
        runbookParams.put("HEADERS", headerValues);
        runbookParams.put("DESCRIPTION", description);

        Log.log.trace("DATE: " + received);
        Log.log.trace("SENT: " + sent);
        Log.log.trace("SUBJECT: " + subject);
        Log.log.trace("SIZE: " + message.getSize());
        Log.log.trace("FROM: " + from);
        Log.log.trace("REPLY_TO: " + replyTo);
        Log.log.trace("RECIPIENTS: " + recipients);
        Log.log.trace("HEADERS: " + headerValues);
        Log.log.trace("DESCRIPTION: " + description);

        Object content = message.getContent();
        if (content instanceof Multipart)
        {
            int contentParts = ((Multipart) content).getCount();
            Log.log.trace("Adding multipart - size: " + contentParts);
            for (int i = 0; i < contentParts; i++)
            {
                addPartContent(runbookParams, ((Multipart) content).getBodyPart(i));
            }
        }
        else
        {
            Log.log.trace("Adding part");
            addPartContent(runbookParams, message);
        }

        // The email body may contain data that might indicate that the
        // sender intends to execute an event.
        String body = runbookParams.get("CONTENT");
        addEvent(body, runbookParams);

        // send to social post
        if (isSocialPost && StringUtils.isNotBlank(body))
        {
            Map<String, byte[]> attachments = getAttachments(message);
            postToSocial(receiver, from, subject, body, attachments);
        }

        Map<String, String> messageHeaders = getMessageHeaders(message);
        for (Filter filter : orderedFilters)
        {
            EmailFilter emailFilter = (EmailFilter) filter;
            if (emailFilter.isActive() && isFilterMatch(emailFilter, message, messageHeaders))
            {
                //Log.log.trace("Running filter: " + emailFilter.getId());
                //processFilter(filter, null, runbookParams);
                addToPrimaryDataQueue(filter, runbookParams);
            }
        }
    }

    private Map<String, byte[]> getAttachments(Message message)
    {
        Map<String, byte[]> result = new HashMap<String, byte[]>();

        try
        {
            if (message != null && message.getContent() instanceof Multipart)
            {
                Multipart multipart = (Multipart) message.getContent();

                for (int i = 0; i < multipart.getCount(); i++)
                {
                    BodyPart bodyPart = multipart.getBodyPart(i);
                    if (Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition()) && StringUtils.isNotBlank(bodyPart.getFileName()))
                    {
                        String contentType = bodyPart.getContentType();
                        InputStream is = bodyPart.getInputStream();
                        byte[] bytes = StringUtils.readlInputStream(is);
                        //send the type alongwith the key like file name|&|text/plain
                        result.put(bodyPart.getFileName() + "|&|" + contentType, bytes);
                    }
                }
            }
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (MessagingException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }

    private Map<String, String> getMessageHeaders(Message message)
    {
        Map<String, String> result = new HashMap<String, String>();

        try
        {
            Enumeration<Header> headers = message.getAllHeaders();
            while (headers.hasMoreElements())
            {
                Header header = headers.nextElement();
                // if there is duplicate header name (e.g., Received) then we
                // will add all their values
                String key = header.getName().toLowerCase();
                StringBuilder value = new StringBuilder(header.getValue());
                // "Received" header may be more than once we want to gather
                // them all
                if (result.containsKey(key))
                {
                    value.append("|&|");
                    value.append(header.getValue());
                }
                result.put(key, value.toString());
            }
            Log.log.trace("Message headers: " + result);
        }
        catch (MessagingException e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }

    private boolean isFilterMatch(EmailFilter emailFilter, Message message, Map<String, String> headers) throws Exception
    {
        boolean result = false;

        String andDelimeter = java.util.regex.Pattern.quote(QueryToken.AND);

        if (emailFilter.getNativeQuery() == null)
        {
            try
            {
                String nativeQuery = resolveQuery.translate(emailFilter.getQuery());
                // set it so we don't attempt to translate again.
                emailFilter.setNativeQuery(nativeQuery);
            }
            catch (Exception e)
            {
                // set it so we don't attempt to translate again.
                emailFilter.setNativeQuery(emailFilter.getQuery());
                Log.log.debug("Wrong RSQL syntaxt for Email gateway, please check. Query provided \"" + emailFilter.getQuery() + "\".");
            }
        }

        String[] queryItems = emailFilter.getNativeQuery().split(andDelimeter);
        List<QueryToken> queryTokens = new ArrayList<QueryToken>();
        for (String queryItem : queryItems)
        {
            queryTokens.add(QueryToken.get(queryItem));
        }

        for (QueryToken queryToken : queryTokens)
        {
            try
            {
                // making it lowercase is important
                // that's how it's stored in the map.
                String keyFromQuery = queryToken.getKey().toLowerCase();
                String queryOperator = queryToken.getOperator();
                String valueFromQuery = queryToken.getValue();
                if (defaultEmailAttributes.containsKey(keyFromQuery))
                {
                    if ("subject".equalsIgnoreCase(keyFromQuery))
                    {
                        result = resolveQuery.isStringValueMatch(message.getSubject(), valueFromQuery, queryOperator);
                    }
                    else if ("body".equalsIgnoreCase(keyFromQuery))
                    {
                        Object content = message.getContent();
                        StringBuilder body = new StringBuilder();
                        if (content instanceof String)
                        {
                            body.append(content.toString());
                        }
                        else if (content instanceof Multipart)
                        {
                            MimeMultipart mmp = (MimeMultipart) content;
                            for (int i = 0; i < mmp.getCount(); i++)
                            {
                                BodyPart mbp = mmp.getBodyPart(i);
                                if (mbp.getContent() instanceof String)
                                {
                                    body.append(mbp.getContent().toString());
                                }
                            }
                        }

                        result = resolveQuery.isStringValueMatch(body.toString(), valueFromQuery, queryOperator);
                    }
                    else if ("replyto".equalsIgnoreCase(keyFromQuery))
                    {
                        String replyToAddresses = getEmailAddresses(message, message.getReplyTo());
                        result = resolveQuery.isStringValueMatch(replyToAddresses, valueFromQuery, queryOperator);
                    }
                    // recipients
                    else if ("recipient".equalsIgnoreCase(keyFromQuery))
                    {
                        String recipientAddresses = getEmailAddresses(message, message.getRecipients(RecipientType.TO));
                        recipientAddresses += getEmailAddresses(message, message.getRecipients(RecipientType.CC));
                        recipientAddresses += getEmailAddresses(message, message.getRecipients(RecipientType.BCC));
                        result = resolveQuery.isStringValueMatch(recipientAddresses, valueFromQuery, queryOperator);
                    }
                    else if ("description".equalsIgnoreCase(keyFromQuery))
                    {
                        result = resolveQuery.isStringValueMatch(message.getDescription(), valueFromQuery, queryOperator);
                    }
                    else if ("sentdate".equalsIgnoreCase(keyFromQuery))
                    {
                        // The date coming from the query must be in 'yyyy-MM-dd
                        // HH:mm:ss.S' format
                        // for example a typical date could look like:
                        // 2013-04-23 08:41:23.0 or 2013-04-23 like that
                        result = resolveQuery.isDateMatch(message.getSentDate(), valueFromQuery, queryOperator, dateFormats);
                    }
                    else if ("size".equalsIgnoreCase(keyFromQuery))
                    {
                        result = resolveQuery.isNumberMatch("" + message.getSize(), valueFromQuery, queryOperator);
                    }
                }
                else if (defaultEmailHeaderAttributes.containsKey(keyFromQuery))
                {
                    if ("returnpath".equalsIgnoreCase(keyFromQuery))
                    {
                        result = resolveQuery.isStringValueMatch(headers.get("return-path"), valueFromQuery, queryOperator);
                    }
                    else if ("received".equalsIgnoreCase(keyFromQuery))
                    {
                        result = resolveQuery.isStringValueMatch(headers.get("received"), valueFromQuery, queryOperator);
                    }
                    else if ("clientaddress".equalsIgnoreCase(keyFromQuery))
                    {
                        result = resolveQuery.isStringValueMatch(headers.get("x-xlientaddr"), valueFromQuery, queryOperator);
                    }
                    else if ("messageid".equalsIgnoreCase(keyFromQuery))
                    {
                        result = resolveQuery.isStringValueMatch(headers.get("message-id"), valueFromQuery, queryOperator);
                    }
                    else if ("contenttype".equalsIgnoreCase(keyFromQuery))
                    {
                        result = resolveQuery.isStringValueMatch(headers.get("content-type"), valueFromQuery, queryOperator);
                    }
                    else if ("receivedspf".equalsIgnoreCase(keyFromQuery))
                    {
                        result = resolveQuery.isStringValueMatch(headers.get("received-spf"), valueFromQuery, queryOperator);
                    }
                    else if ("authresults".equalsIgnoreCase(keyFromQuery))
                    {
                        result = resolveQuery.isStringValueMatch(headers.get("authentication-results"), valueFromQuery, queryOperator);
                    }
                    else if ("mimeversion".equalsIgnoreCase(keyFromQuery))
                    {
                        result = resolveQuery.isStringValueMatch(headers.get("mime-version"), valueFromQuery, queryOperator);
                    }
                }
            }
            catch (Exception e)
            {
                result = false;
                Log.log.trace("Not matched:" + e.getMessage());
                break;
            }
        }

        return result;
    }

    private String getEmailAddresses(Message message, Address[] emailAddresses)
    {
        StringBuilder addresses = new StringBuilder();
        for (Address address : emailAddresses)
        {
            addresses.append(address.toString());
            addresses.append("|&&|");
        }
        if (addresses.length() > 0)
        {
            addresses.setLength(addresses.length() - 4); // reduce length of
                                                         // |&&|
        }
        return addresses.toString();
    }

    private String parseAddresses(Address[] addresses)
    {
        StringBuilder result = new StringBuilder();

        if (addresses != null)
        {
            for (Address address : addresses)
            {
                InternetAddress addr = (InternetAddress) address;
                // Sometime the address comes as the syntax defined by RFC822
                // Typical address syntax is of the form "user@host.domain" or
                // "Personal Name <user@host.domain>".
                // but we need only perfect email address all the time.
                result.append(addr.getAddress());
                result.append(",");
            }
            result.setLength(result.length() - 1);
        }

        return result.toString();
    }

    @Override
    public void start()
    {
        Log.log.warn("Starting EmailListener");
        super.start();
    } // start

    @Override
    public void stop()
    {
        Log.log.warn("Stopping EmailListener");
        super.stop();
        try
        {
            disconnect();
        }
        catch (Exception e)
        {
            Log.log.warn("ERROR: " + e.getMessage());
        }
    } // stop

    private void addPartContent(Map messageMap, Part part) throws Exception
    {
        if (part.getDisposition() == null)
        {
            String contentType = part.getContentType();
            Log.log.trace("content type: " + contentType);
            String mimeType = (String) messageMap.get("MIME_TYPE");
            if (mimeType == null)
            {
                messageMap.put("MIME_TYPE", contentType);
            }
            else if (!mimeType.contains(contentType))
            {
                messageMap.put("MIME_TYPE", mimeType + "&" + contentType);
            }
            contentType = contentType.toUpperCase();
            if (contentType.contains("TEXT/PLAIN"))
            {
                Log.log.trace("Adding plain text");
                String content = (String) messageMap.get("CONTENT");
                if (content == null)
                {
                    messageMap.put("CONTENT", part.getContent().toString());
                }
                else
                {
                    messageMap.put("CONTENT", content + part.getContent().toString());
                }
            }
            else if (contentType.contains("MULTIPART/"))
            {
                Multipart multiPart = (Multipart) part.getContent();
                int contentParts = ((Multipart) multiPart).getCount();
                Log.log.trace("Nested multipart - size: " + contentParts);
                for (int i = 0; i < contentParts; i++)
                {
                    addPartContent(messageMap, ((Multipart) multiPart).getBodyPart(i));
                }
            }
            else
            {
                Log.log.trace("Adding other");
                int end = contentType.indexOf(";");
                if (end != -1)
                {
                    contentType = contentType.substring(0, end);
                }
                contentType = contentType.replaceAll("TEXT/", "");

                String content = (String) messageMap.get("CONTENT-" + contentType);
                Log.log.trace("adding content type: " + contentType);
                if (content == null)
                {
                    messageMap.put("CONTENT-" + contentType, part.getContent().toString());
                }
                else
                {
                    messageMap.put("CONTENT-" + contentType, content + part.getContent().toString());
                }
            }
        }
        else if (part.getDisposition().equalsIgnoreCase(Part.ATTACHMENT) || part.getDisposition().equalsIgnoreCase(Part.INLINE))
        {
            boolean attachment = true;
            if (StringUtils.isEmpty(part.getFileName()) && part.getDisposition().equalsIgnoreCase(Part.INLINE))
            {
                Log.log.debug("INLINE Disposition, Checking for Content or Attachment");
                String contentType = part.getContentType();
                Log.log.debug("Content Type of INLINE: " + contentType);
                contentType = contentType.toUpperCase();
                if (contentType.contains("TEXT"))
                {
                    attachment = false;
                    if (contentType.contains("TEXT/PLAIN"))
                    {
                        Log.log.trace("Adding plain text");
                        String content = (String) messageMap.get("CONTENT");
                        if (content == null)
                        {
                            messageMap.put("CONTENT", part.getContent().toString());
                        }
                        else
                        {
                            messageMap.put("CONTENT", content + part.getContent().toString());
                        }
                    }
                    else
                    {
                        Log.log.trace("Adding other");
                        int end = contentType.indexOf(";");
                        if (end != -1)
                        {
                            contentType = contentType.substring(0, end);
                        }
                        contentType = contentType.replaceAll("TEXT/", "");

                        String content = (String) messageMap.get("CONTENT-" + contentType);
                        Log.log.trace("adding content type: " + contentType);
                        if (content == null)
                        {
                            messageMap.put("CONTENT-" + contentType, part.getContent().toString());
                        }
                        else
                        {
                            messageMap.put("CONTENT-" + contentType, content + part.getContent().toString());
                        }
                    }
                }
            }
            if (attachment)
            {
                Log.log.debug("Disposition is: " + part.getDisposition() + ", saving to file folder");
                String fileName = part.getFileName();
                if (!StringUtils.isEmpty(fileName))
                {
                    String subject = (String) messageMap.get("SUBJECT");
                    subject = subject.replaceAll("[\\\\/]", "_");
                    subject = subject.replaceAll(",", "");
                    String path = MainBase.main.getProductHome() + "/file/" + subject;
                    File directory = FileUtils.getFile(path);
                    if (directory.isFile())
                    {
                        Log.log.warn("Directory " + path + " exists as file, Attempting to Delete");
                        if (!directory.delete())
                        {
                            Log.log.warn("Unable to Delete Existing File");
                        }
                    }
                    if (directory.isDirectory())
                    {
                        Log.log.warn("Directory " + path + " already exists, writing into it");
                    }
                    else
                    {
                        Log.log.warn("Creating Directory: " + path);
                        if (!directory.mkdir())
                        {
                            Log.log.warn("Failed to Create Directory");
                        }
                    }
                    fileName = path + "/" + fileName;
                    File file = FileUtils.getFile(fileName);
                    Log.log.debug("Attachment File Will Be Saved To: " + file.getAbsolutePath());
                    if (file.exists())
                    {
                        Log.log.warn("File Already Exists, Attempting to Delete");
                        if (!file.delete())
                        {
                            Log.log.warn("Unable to Delete Existing File");
                        }
                    }
                    try
                    {
                        InputStream is = part.getInputStream();
                        OutputStream os = new FileOutputStream(file, false);

                        byte[] fileContent = new byte[MAX_ATTACHMENT_READ_SIZE];
                        int writeSize;
                        while ((writeSize = is.read(fileContent)) > 0)
                        {
                            os.write(fileContent, 0, writeSize);
                        }
                        is.close();
                        os.close();

                        String attachments = (String) messageMap.get("ATTACHMENTS");
                        if (attachments == null)
                        {
                            messageMap.put("ATTACHMENTS", file.getAbsolutePath());
                        }
                        else
                        {
                            messageMap.put("ATTACHMENTS", attachments + "," + file.getAbsolutePath());
                        }
                    }
                    catch (IOException ioe)
                    {
                        Log.log.error("Failed to Save File Attachment: " + file.getName(), ioe);
                    }
                }
                else
                {
                    Log.log.error("Filename is Empty for Attachment, skipping");
                }
            }
        }
        else
        {
            Log.log.error("Unknown Mime Message disposition: " + part.getDisposition() + ", doing nothing");
        }
    } // addPartContent

    private String getProtocol()
    {
        String result;

        // IMAP
        if (protocol.equalsIgnoreCase("IMAP"))
        {
            result = secure ? "imaps" : "imap";
        }
        // default POP3
        else
        {
            result = secure ? "pop3s" : "pop3";
        }

        return result;
    } // getProtocol

    private boolean connectEmailServer()
    {
        boolean result = false;
        try
        {
            Log.log.debug("Connecting to email server: " + ipaddr + ":" + port);
            // get session
            session = Session.getDefaultInstance(props, null);
            Log.log.debug("session properties: " + session.getProperties());

            // get store
            Store store = session.getStore(getProtocol());
            if (StringUtils.isNotBlank(username))
            {
                store.connect(ipaddr, port, username.trim(), password.trim());
                emailStores.put(username, store);

                // get folder
                Folder folder = store.getFolder(emailfolder);
                Log.log.trace("FOLDER NAME: " + folder.getFullName());
                emailFolders.put(username, folder);
            }
            else
            {
                Log.log.info("POP3 username is empty, so not polling is required!");
            }

            for (String indieUsername : emailAddresses.keySet())
            {
                // get store
                Store userStore = session.getStore(getProtocol());
                
                try
                {
                    userStore.connect(ipaddr, port, indieUsername, emailAddresses.get(indieUsername).getEmailPassword());
                    
                    // get folder
                    Folder userFolder = userStore.getFolder(emailfolder);
                    Log.log.debug("Connected to folder for " + indieUsername);
                    emailStores.put(indieUsername, userStore);
                    emailFolders.put(indieUsername, userFolder);
                }catch (Exception e)
                {
                    Log.log.warn("Failed to connect to Email Server: mailbox " + indieUsername + " due to " + e.getMessage());
                }
            }

            result = true;
        }
        catch (Exception e)
        {
            Log.log.error("Failed to connect to Email Server: " + e.getMessage(), e);
        }
        return result;
    } // connectEmailServer

    private void disconnect()
    {
        try
        {
            if (emailFolders != null && emailFolders.size() > 0)
            {
                for (Folder folder : emailFolders.values())
                {
                    if (folder.isOpen())
                    {
                        try
                        {
                            folder.close(true);
                        }
                        catch (Exception e)
                        {
                            // ignore it!
                        }
                    }
                }
            }
            if (emailStores != null && emailStores.size() > 0)
            {
                for (Store store : emailStores.values())
                {
                    if (store.isConnected())
                    {
                        try
                        {
                            store.close();
                        }
                        catch (Exception e)
                        {
                            // ignore it!
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Unable to close Email connection. " + e.getMessage(), e);
        }
    } // disconnect

    public boolean enqueue(Map<String, Object> parameters)
    {
        return sendQueue.offer(parameters);
    }

    public boolean send(Map<String, Object> parameters)
    {
        boolean result = true;

        try
        {
            // decide the sender information
            String username = this.smtpusername;
            String password = this.smtppassword;
            
            if (parameters.containsKey(Constants.NOTIFICATION_MESSAGE_FROM) && StringUtils.isNotBlank((String) parameters.get(Constants.NOTIFICATION_MESSAGE_FROM)))
            {
                username = (String) parameters.get(Constants.NOTIFICATION_MESSAGE_FROM);
                if (StringUtils.isNotBlank(username))
                {
                    if (emailAddresses.containsKey(username))
                    {
                        password = emailAddresses.get(username).getEmailPassword();
                    }
                }
            }
            
            Map<String, byte[]> attachments = (Map<String, byte[]>) parameters.get(Constants.NOTIFICATION_MESSAGE_ATTACHMENTS);
            boolean hasAttachment = false;
            if (attachments != null && attachments.size() > 0)
            {
                hasAttachment = true;
            }

            EmailConnect2 emailConnect2 = new EmailConnect2(this.smtphost, this.smtpport, username, password, this.isSmtpSSL, hasAttachment, smtpProperties);

            // if from is null get from gateway configuration.
            String smtpFrom = (String) parameters.get(Constants.NOTIFICATION_MESSAGE_FROM);
            if (StringUtils.isBlank(smtpFrom)) 
            {
                if(StringUtils.isBlank(((ConfigReceiveEmail) configurations).getSmtpfrom()))
                {
                    smtpFrom = ((ConfigReceiveEmail) configurations).getSmtpusername();
                }
                else
                {
                    smtpFrom = ((ConfigReceiveEmail) configurations).getSmtpfrom();
                }
            }
            if(Log.log.isDebugEnabled())
            {
                Log.log.debug("Sending email with from address: " + smtpFrom);
            }
            emailConnect2.setFrom(smtpFrom, (String)parameters.get(Constants.NOTIFICATION_MESSAGE_FROM_DISPLAY_NAME));

            List<String> recipients = StringUtils.stringToList((String) parameters.get(Constants.NOTIFICATION_MESSAGE_TO), ",");
            emailConnect2.addTo(recipients.toArray(new String[1]));

            List<String> ccList = StringUtils.stringToList((String) parameters.get(Constants.NOTIFICATION_MESSAGE_CC), ",");
            for (String cc : ccList)
            {
                emailConnect2.addCC(cc.trim());
            }
            List<String> bccList = StringUtils.stringToList((String) parameters.get(Constants.NOTIFICATION_MESSAGE_BCC), ",");
            for (String bcc : bccList)
            {
                emailConnect2.addBCC(bcc.trim());
            }
            emailConnect2.setSubject((String) parameters.get(Constants.NOTIFICATION_MESSAGE_SUBJECT));
            emailConnect2.setText((String) parameters.get(Constants.NOTIFICATION_MESSAGE_CONTENT));
            emailConnect2.setReplyTo((String) parameters.get(Constants.NOTIFICATION_MESSAGE_REPLY_TO));

            String resolveTempDir = MainBase.main.configGeneral.home + "/tmp";

            // collect it so once the job is done we can remove them from the "tmp" directory
            List<File> files = new ArrayList<File>();
            if (hasAttachment)
            {
                for (String attachment : attachments.keySet())
                {
                    byte[] fileContent = attachments.get(attachment);

                    String ext = FileUtils.getFileExtension(attachment);

                    // generate a random file name to be stored in the "tmp"
                    // directory
                    // to avoid conflict when other threads uses the same
                    // original file name.
                    // for example, two action task may send an attachment
                    // name "MyAttachment.txt"
                    // but if we store that then first one will be replaced
                    // with the second one.
                    String randomFileName = FileUtils.createRandomFileName(ext, null);
                    File tempFileToSave = new File(resolveTempDir + "/" + randomFileName);

                    FileUtils.writeByteArrayToFile(tempFileToSave, fileContent);

                    emailConnect2.addAttachment(tempFileToSave);
                    files.add(tempFileToSave);
                }
            }
            emailConnect2.send();

            // remove the attached files
            for (File file : files)
            {
                FileUtils.deleteQuietly(file);
            }
        }
        catch (Exception e)
        {
            result = false;
            Log.log.error("Failed to send message: " + e.getMessage(), e);
        }

        return result;
    } // send
} // EmailGateway
