package com.resolve.gateway.resolvegateway.pull;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.gateway.resolvegateway.ConfigReceiveResolveGateway;
import com.resolve.gateway.resolvegateway.ResolveGatewayUtil;
import com.resolve.gateway.resolvegateway.pull.PullGatewayFilter;
import com.resolve.gateway.resolvegateway.pull.PullGatewayProperties;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceivePullGateway extends ConfigReceiveResolveGateway
{
    private static final String RECEIVE_NODE = "./RECEIVE/";
    private static volatile Map<String, PullGateway> gateways = new ConcurrentHashMap<String, PullGateway>();
    private static volatile Map<String, PullGatewayProperties> gatewayProperties = new ConcurrentHashMap<String, PullGatewayProperties>();

    public ConfigReceivePullGateway(XDoc configDoc, String gatewayName) throws Exception {
        super(configDoc, gatewayName);
    }
    
    public ConfigReceivePullGateway(XDoc configDoc) throws Exception {
        super(configDoc);
    }

    public static Map<String, PullGateway> getGateways()
    {
        return gateways;
    }

    public static void setGateways(Map<String, PullGateway> gateways)
    {
        ConfigReceivePullGateway.gateways = gateways;
    }

    @Override
    public String getRootNode() {
        return RECEIVE_NODE + gatewayName.toUpperCase() + "/";
    }
    
    public void loadPullGatewayProperties(String gatewayName, String node) {
        
        Map<String, String> properties = ResolveGatewayUtil.loadGatewayProperties(node);
        
        PullGatewayProperties pull= new PullGatewayProperties();
        Map<String, Object> additional = pull.getAdditionalProperties();
        
        for(Iterator<String> it=properties.keySet().iterator(); it.hasNext();) {
            String key = it.next();
            String value = properties.get(key);
            switch(key) {
                case "ACTIVE":
                    try {
                        Boolean active = new Boolean(value);
                        pull.setActive(active);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "MENUTITLE":
                    pull.setDisplayName(value);
                    break;
                case "QUEUE":
                    pull.setQueue(value);
                    pull.setLicenseCode(value); // Currently it's hardcoded the same as the queue name
                    pull.setEventType(value); // Currently it's hardcoded the same as the queue name
                    break;
                case "IMPLPREFIX":
                    pull.setClassName(value);
                    break;
                case "PACKAGE":
                    pull.setPackageName(value);
                    break;
                case "LICENSE":
                    pull.setLicenseCode(value);
                    break;
                case "EVENTTYPE":
                    pull.setEventType(value);
                    break;
                case "PRIMARY":
                    try {
                        Boolean primary = new Boolean(value);
                        pull.setPrimary(primary);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "SECONDARY":
                    try {
                        Boolean secondary = new Boolean(value);
                        pull.setSecondary(secondary);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "WORKER":
                    try {
                        Boolean worker = new Boolean(value);
                        pull.setWorker(worker);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "MOCK":
                    try {
                        Boolean mock = new Boolean(value);
                        pull.setMock(mock);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "UPPERCASE":
                    try {
                        Boolean uppercase = new Boolean(value);
                        pull.setUppercase(uppercase);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "INTERVAL":
                    try {
                        Integer interval = new Integer(value);
                        pull.setInterval(interval);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "FAILOVER":
                    try {
                        Integer failover = new Integer(value);
                        pull.setFailover(failover);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "HEARTBEAT":
                    try {
                        Integer heartbeat = new Integer(value);
                        pull.setHeartbeat(heartbeat);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "SSL":
                    try {
                        Boolean ssl = new Boolean(value);
                        pull.setSsl(ssl);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "SSLTYPE":
                    pull.setSslType(value);
                    break;
                case "HOST":
                    pull.setHost(value);
                    break;
                case "PORT":
                    if(StringUtils.isBlank(value))
                        continue;
                    try {
                        Integer port = new Integer(value);
                        pull.setPort(port);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "USERNAME":
                    pull.setUser(value);
                    break;
                case "PASSWORD":
                    if (StringUtils.isNotBlank(value) && value.startsWith("ENC1:")) {
                        try {
                            value = CryptUtils.decrypt(value);
                        } catch(Exception e) {
                            Log.log.error(e.getMessage(), e);
                        }
                    }
                    
                    pull.setPass(value);
                    break;
                case "SEC":
                    pull.setSec(value);
                    break;
                default:
                    additional.put(key, value);
                    break;
            }
        }
        
//        Integer id = ResolveGatewayUtil.savePullGatewayProperties(pull, additional);
        
        gatewayProperties.put(gatewayName, pull);

    }
    
    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for pull gateway, check blueprint. " + e.getMessage(), e);
        }
    }
    
    public void loadPullGatewayFilters(String gatewayName, String queueName) {
        
        if(!isActive())
            return;
        
        Map<String, Map<String, Object>> gatewayFilters = ResolveGatewayUtil.loadGatewayFilters(gatewayName, "pull", queueName);
        if(gatewayFilters == null)
            return;
        
//        Integer filterId = GatewayUtil.savePullGatewayFilters(gatewayFilters, gatewayId);
        
//        filterMap.put(gatewayName, filterId);
        
        for(Iterator<String> it=gatewayFilters.keySet().iterator(); it.hasNext();) {
            
            String filterName = it.next();
            if(!filterName.equalsIgnoreCase("RESOLVE.JOB_ID")) {
	            Map<String, Object> filter = gatewayFilters.get(filterName);
	            
	            String sysId = (String)filter.get("SYS_ID");
	            String active = ((Boolean)filter.get("ACTIVE")).toString();
	            String order = ((Integer)filter.get("ORDER")).toString();
	            String interval = ((Integer)filter.get("INTERVAL")).toString();
	//            String filterName = (String)filter.get("ID");
	            String eventId = (String)filter.get("EVENT_EVENTID");
	            String runbook = (String)filter.get("RUNBOOK");
	            String script = (String)filter.get("SCRIPT");
	            
	            PullGatewayFilter pullFilter = new PullGatewayFilter(filterName, active, order, interval, eventId, runbook, script);
	            
	            pullFilter.setDeployed(((Boolean)filter.get("DEPLOYED")));
	            pullFilter.setUpdated(((Boolean)filter.get("UPDATED")));
	            pullFilter.setGatewayName(gatewayName);
	            pullFilter.setQueue((String)filter.get("QUEUE"));
	            pullFilter.setQuery((String)filter.get("QUERY"));
	            pullFilter.setTimeRange((String)filter.get("TIME_RANGE"));
	            pullFilter.setLastId((String)filter.get("LAST_ID"));
	            pullFilter.setLastTimestamp((String)filter.get("LAST_TIMESTAMP"));
	            pullFilter.setLastValue((String)filter.get("LAST_VALUE"));
	            Map<String, Object> gatewayFilterAttributes = ResolveGatewayUtil.loadGatewayFilterAttributes(sysId, "pull");
	            if(gatewayFilterAttributes != null)
	                pullFilter.setAttributes(gatewayFilterAttributes);
	
	            PullGateway.addPullFilter(filterName, pullFilter);
	            
	            /* Don't need to load from file system because it should be saved in and loaded from the database
	            StringBuilder sb = new StringBuilder();
	            sb.append(MainBase.main.release.serviceName).append("/config/").append(gatewayName).append("/").append(filterName.toLowerCase()).append(".groovy");
	            File scriptFile = getFile(sb.toString());
	            
	            try {
	                if (scriptFile.exists()) {
	                    String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
	                    if (StringUtils.isNotBlank(groovy)) {
	                        pullFilter.setScript(groovy);
	                        Log.log.debug("Loaded groovy script: " + scriptFile);
	                    }
	                }
	            } catch (Exception e) {
	                Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
	            }
	
	            ResolveGatewayUtil.savePullGatewayFilter(pullFilter, filterName, gatewayName); */
            }
        }
    }

    public static PullGatewayProperties getGatewayProperties(String name)
    {
        return gatewayProperties.get(name);
    }
    
} // class ConfigReceivePullGateway
