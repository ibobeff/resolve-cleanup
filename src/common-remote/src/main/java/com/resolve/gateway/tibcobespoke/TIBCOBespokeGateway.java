/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.gateway.tibcobespoke;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.dom4j.tree.DefaultElement;
import org.xml.sax.SAXException;

import com.resolve.gateway.BaseSocialGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MTIBCOBespoke;
import com.resolve.query.ResolveQuery;
import com.resolve.query.translator.TIBCOBespokeQueryTranslator;
import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveTIBCOBespoke;
import com.resolve.util.Constants;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XmlUtils;

public class TIBCOBespokeGateway extends BaseSocialGateway
{    
    // Singleton
    private static volatile TIBCOBespokeGateway instance = null;
    private static TIBCOBespokeRabbitCommunicator processComm;  
    private static ResolveQuery resolveQueryTranslator;
    private static final String HEARTBEAT_QUEUE = "TIBCO_HEARTBEAT";
    private static final String DEFAULT_WORKER_MIN = "5";
    private static final String DEFAULT_WORKER_MAX = "10";
    private static final String DEFAULT_PROCESSOR_CLASS = "com.resolve.tibcostandalone.TIBCOBespokeBusProcessor";
    private static final String BUSRESPONDER_SUBSCRIPTION_TOPIC = "BUSRESPONDER_SUBSCRIPTION_TOPIC#";

    private ConcurrentHashMap<String, TIBCOBespokeProcess> heartbeatList;
    private ConfigReceiveTIBCOBespoke config;
    private String queue = "TIBCOBESPOKE";
    private long initTimestamp = System.currentTimeMillis();
    private ScheduledExecutorService threadPool;
    
    
    private TIBCOBespokeGateway(ConfigReceiveTIBCOBespoke config)
    {
        super(config);    
        queue = config.getQueue();
    }
    
    public static TIBCOBespokeGateway getInstance(ConfigReceiveTIBCOBespoke config)
    {
        if (instance == null)
        {
            instance = new TIBCOBespokeGateway(config);
        }
        
        return instance;
    }

    public static TIBCOBespokeGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("TIBCO: Gateway is not initialized.");
        }
        else
        {
            return (TIBCOBespokeGateway) instance;
        }
    }

    public String request(String topic, String dataToSend, String busUri, Integer timeoutMillis) throws Exception
    {
        Log.log.trace(" Sending request to topic: " + topic + ", busUri: " + busUri);
        TIBCOBespokeProcess process = startBusProcess(busUri, TIBCOBespokeProcess.TYPE_PUBLISHER_REQUESTER);
        
        MessageEnvelope msgEnv = new MessageEnvelope(dataToSend);
        msgEnv.setBusUri(busUri);
        msgEnv.setTimeout(timeoutMillis.toString());
        msgEnv.setMessageTopic(topic);
        
        return processComm.request("TIBCO_REQUESTREPLY#" + process.getProcessId(), msgEnv.getEvelopeString(), timeoutMillis);        
    }
    
    public void publish(String topic, String dataToSend, Boolean certifiedMessaging, String busUri) throws Exception
    {
        Log.log.trace("Publishing to topic: " + topic + ", CertifiedMessaging: " + certifiedMessaging + ", BusUri: " + busUri);
        TIBCOBespokeProcess process = startBusProcess(busUri, TIBCOBespokeProcess.TYPE_PUBLISHER_REQUESTER);
        
        MessageEnvelope msgEnv = new MessageEnvelope(dataToSend);
        msgEnv.setMessageTopic(topic);
        msgEnv.setBusUri(busUri);        
        if(certifiedMessaging)
        {
            msgEnv.setMessageType("CM_PUBLISHER");            
        }
        else
        {
            msgEnv.setMessageType("STANDARD_PUBLISHER");
        }
        
        processComm.publish("TIBCO_PUBLISHER#"+process.getProcessId(), msgEnv.getEvelopeString());
    }

    /**
     * Responds to message previously passed into a fitler. Filter must have
     * save messsage for response set to true. After the response is made the
     * item is removed from the store.
     * 
     * @param replyMessageStoreId
     *            - Available in PARAMS as TIB_MESSAGE_ID after/during filter
     *            execution
     * @param respondPayload
     *            - The message to send
     * @throws Exception 
     * @throws BusException
     */
    public void respond(String replyMessageStoreId, String respondPayload) throws Exception
    {
        Log.log.trace("Replying to message: " + replyMessageStoreId );
        if (StringUtils.isEmpty(replyMessageStoreId))
        {
            throw new IllegalArgumentException("Could not repond to message. Message ID was empty or null");
        }
        if (StringUtils.isEmpty(respondPayload))
        {
            throw new IllegalArgumentException("Could not respond to message. Message to send was empty or null");
        }       
        
        //format expireTime-storeid-processid
        String[] idSplit = replyMessageStoreId.split("#");
        if(idSplit.length != 3)
        {
            throw new RuntimeException("Invalid TIB_MESSAGE_ID, expect format of 'expiretime#messageid#processid' given: "+replyMessageStoreId);
        }
        else
        {
            Long expireTime = Long.parseLong(idSplit[0]);
            if(System.currentTimeMillis() > expireTime)
            {
                throw new RuntimeException("The message has expired, increase filters ReplyTimeout to prevent this. Maximum 10 minutes on the resolve side. Most systems will not store greater than 2.");   
            }
            else
            {
                MessageEnvelope msgEnv = new MessageEnvelope(respondPayload);
                //The id Expected on the rstib side is "expireTimestamp#storeid" hence idSplit[0]#idSplit[1]
                msgEnv.setStoreId(idSplit[0]+"#"+idSplit[1]);        
                processComm.publish("TIBCO_RESPONDSUB#"+idSplit[2], msgEnv.getEvelopeString());
            }
        }
    }
    
    private TIBCOBespokeProcess getProcessForBusAndType(String busUri, String type)
    {
        Iterator<TIBCOBespokeProcess> processIter = heartbeatList.values().iterator();
        while(processIter.hasNext())
        {
            TIBCOBespokeProcess curProcess = processIter.next();
            if(curProcess.getBusUri().equals(busUri) && curProcess.getProcessType().equals(type) && curProcess.getDeletionFlag() == false)
            {
                return curProcess;
            }
        }
        
        return null;
    }

    /***
     * Returns the list of heartbeat objects
     * @return ConcurrentHashMap< processId, TIBCOBespokeProcess object > 
     */
    public ConcurrentHashMap<String, TIBCOBespokeProcess> getHeartbeatList()
    {
        return heartbeatList;
    }
    
    public void updateHeartbeat(TIBCOBespokeProcess process)
    {        
        TIBCOBespokeProcess existingProcess = heartbeatList.get(process.getProcessId());
        if(existingProcess != null && existingProcess.getDeletionFlag() == false)
        {
            existingProcess.updateLastHeartbeat();
        }
        else
        {
            Log.log.debug("New heartbeat process: " + process.toString());
            heartbeatList.put(process.getProcessId(), process);
        }
    }
       
    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_TIBCOBESPOKE;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MTIBCOBespoke.class.getSimpleName();
    }

    @Override
    protected Class<MTIBCOBespoke> getMessageHandlerClass()
    {
        return MTIBCOBespoke.class;
    }

    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }
    /**
     * Initializes the specific gateway resources.
     */
    @Override
    protected void initialize()
    {   
        try
        {   
            Log.log.debug("Initializing TIBCOBespokeGateway: " + this.hashCode());
            threadPool = Executors.newScheduledThreadPool(25); 
            initTimestamp = System.currentTimeMillis();
            config = (ConfigReceiveTIBCOBespoke) configurations;
            queue = config.getQueue().toUpperCase();
            gatewayConfigDir = "/config/tibcobespoke/";
            resolveQueryTranslator = new ResolveQuery(new TIBCOBespokeQueryTranslator());
                    
            initRabbit();
            initHeartbeat();
            initShutdownHook();
            
            Thread.sleep(3000);            
            
            if(isActive() && isPrimary())
            {
                buildSubscriptions();
                threadPool.scheduleWithFixedDelay(new TIBCOBespokeProcessMonitor(), 120, 30, TimeUnit.SECONDS); //wait 2 minutes check every 30
            }
        }
        catch(Exception e)
        {
            Log.log.error("TIBCO: Exception initializing TIBCO Gateway", e);
        }
    }
    
    /**
     * Creates the communication mechanism between Resolve and the TIBCO Processes. RabbitMQ Exchanges and Queues are setup later, per process.
     */
    private void initRabbit()
    {
        try
        {
            int rabbitCorePoolSize = 100;
            int rabbitMaxPoolSize = 500;
            int rabbitKeepAliveTime = 120;
            int rabbitBlockingQueueSize = 50;
            String rabbitSslProtocol = "TLSv1.2";
            try
            {
                rabbitCorePoolSize = Integer.parseInt(config.getRabbitCorePoolSize());
                rabbitMaxPoolSize = Integer.parseInt(config.getRabbitMaxPoolSize());
                rabbitKeepAliveTime = Integer.parseInt(config.getRabbitKeepAliveTime());
                rabbitBlockingQueueSize = Integer.parseInt(config.getRabbitBlockingQueueSize());                
                rabbitSslProtocol = config.getRabbitSslProtocol();
            }
            catch(NumberFormatException ne)
            {
                Log.log.error("TIBCOBespoke: Blueprints has invalid configurations for tibco-rabbit fields", ne);
            }
            processComm = new TIBCOBespokeRabbitCommunicator(MainBase.getESB().getMServer().getConfig().getBrokerAddr(), rabbitCorePoolSize, rabbitMaxPoolSize, rabbitKeepAliveTime, rabbitBlockingQueueSize, rabbitSslProtocol);
        }
        catch(Exception e)
        {
            Log.log.error("TIBCO: issue starting rabbit communications for processes", e);
        }
    }
    
    /**
     * The Heartbeat listener is a "Check Alive" mechanism for each tibco process started.
     * The messages are sent via RabbitMQ Queue: 'TIBCO_HEARTBEAT#TIBCOBESPOKE'
     */
    private void initHeartbeat()
    {
        try
        {
            heartbeatList = new ConcurrentHashMap<String, TIBCOBespokeProcess>();
            processComm.subscribeAndRespond(HEARTBEAT_QUEUE+"#"+config.getQueue(), new TIBCOBespokeRabbitWorker() {
                @Override
                public String doWork(String message)
                {
                    Log.log.trace(message);
                    try
                    {                       
                        TIBCOBespokeProcess curProcess = new TIBCOBespokeProcess(message);
                        updateHeartbeat(curProcess);
                    }
                    catch(Exception e)
                    {
                        Log.log.error("TIBCO: Issue in heartbeat thread", e);
                    }
                    return "Resolve is awake";
                }
            });
            
            Thread.sleep(3000);
            sendShutdownRequestsAllProcesses(); //No processes should exist on init, so lets shut them down
        }
        catch(Exception e)
        {
            Log.log.error("TIBCO: Issue in heartbeat thread", e);
        }
    }

    /**
     * Re-initializes the specific gateway resources. This is called if the
     * gateway changes its state from primary to secondary.
     * 
     * @param configurations
     */
    @Override
    public void reinitialize()
    {
        Log.log.warn("TIBCO: Gateway reinitialize() called, switching to secondary.");
        super.reinitialize();
        ConfigReceiveTIBCOBespoke tibcoConfigurations = (ConfigReceiveTIBCOBespoke) configurations;

        if (tibcoConfigurations.isActive() && isPrimary())
        {
            initialize();
        }
    }

    /**
     * Attempt are deactiving the gateway without shutting down RSREMOTE.
     * Never tested against a real system
     */
    public void deactivate()
    {
        // when the gateway is deactivated all active filters are cleaned
        cleanFilter(filters);
        sendShutdownRequestsAllProcesses();
        super.deactivate();
        Log.log.warn("TIBCO: Deactivating TIBCO Gateway.");
    }
    
    public void restartSubscriptions()
    {
        buildSubscriptions();
    }
    
    /**
     * For each active and deployed filter, this method will create and
     * establish a subscriber for each of their unique bus destinations.
     */
    private synchronized void buildSubscriptions()
    {
        try
        {
            final Collection<String> busList = getDistinctBusList(filters.values());
            Log.log.trace("Building subscriptions: " + busList);
            buildXmlFiles(busList);
            
            sendShutdownRequestsAllProcesses();
            startBusProcesses(busList.toArray(new String[busList.size()]), TIBCOBespokeProcess.TYPE_BUSRESPONDER);
            
            Runnable initRabbitRunnable = new Runnable() {
                @Override
                public void run()
                {
                    try
                    {
                        initRabbitTibcoSubscriptionListeners(busList);
                    }
                    catch (InterruptedException e)
                    {
                        Log.log.error("TIBCO: " + e.getMessage(), e);
                    }
                }};
            //Executing in delayed thread to allow call back to UI sooner
            threadPool.schedule(initRabbitRunnable, 20, TimeUnit.SECONDS);            
        }
        catch (Exception e)
        {
            Log.log.error("TIBCO: "+ e.getMessage(), e);
        }
    }
    
    /**
     * Builds XML files for a list of buses. The xml files are needed
     * by the TIBCO Processes for configurations. These files can be found at
     * rsremote/config/tibcobespoke/busresponder*. The format of the xml is define by
     * the tibco API. See CenturyLink Bus team for more information.
     *  
     * @param busList - Bus URL's 
     */
    private void buildXmlFiles(Collection<String> busList)
    {
        Log.log.trace("Building XML Files for busList: " + busList);
        Iterator<String> busIter = busList.iterator();
        while(busIter.hasNext())
        {
            Document xmlDoc = DocumentHelper.createDocument();
            Element root = xmlDoc.addElement("BusConnector");

            ArrayList<Element> processElements = new ArrayList<Element>();
            ArrayList<Element> topicElements = new ArrayList<Element>();
            
            String curBus = busIter.next();
            Collection<Filter> distinctFilters = getDistinctFiltersPerTopicByBus(curBus, filters.values());
                            
            Iterator<Filter> filterIter = distinctFilters.iterator();
            while(filterIter.hasNext())
            {
                TIBCOBespokeFilter curFilter = (TIBCOBespokeFilter) filterIter.next();
                
                DefaultElement processorElement = new DefaultElement("BusProcessor");
                processorElement.addAttribute("name", curFilter.getId());
                processorElement.addAttribute("class", DEFAULT_PROCESSOR_CLASS);
                if(curFilter.getRequireReply())
                {
                    processorElement.addElement("Attribute").addAttribute("name", "subscriptionType").addAttribute("type","String").addText("REQUEST_REPLY_SUBSCRIBER");
                }
                else if(curFilter.getCertifiedMessaging())
                {
                    processorElement.addElement("Attribute").addAttribute("name", "subscriptionType").addAttribute("type","String").addText("CM_SUBSCRIBER");
                }
                else
                {
                    processorElement.addElement("Attribute").addAttribute("name", "subscriptionType").addAttribute("type","String").addText("STANDARD_SUBSCRIBER");
                }
                processorElement.addElement("Attribute").addAttribute("name", "topic").addAttribute("type","String").addText(curFilter.getTopic().trim());
                
                if(StringUtils.isNotEmpty(config.getProcessMaxThread()))
                {
                    processorElement.addElement("Attribute").addAttribute("name", "worker.max").addAttribute("type","Integer").addText(config.getProcessMaxThread());
                }
                else
                {
                    processorElement.addElement("Attribute").addAttribute("name", "worker.max").addAttribute("type","Integer").addText(DEFAULT_WORKER_MAX);
                }
                if(StringUtils.isNotEmpty(config.getProcessMinThread()))
                {
                    processorElement.addElement("Attribute").addAttribute("name", "worker.min").addAttribute("type","Integer").addText(config.getProcessMinThread());
                }
                else
                {
                    processorElement.addElement("Attribute").addAttribute("name", "worker.min").addAttribute("type","Integer").addText(DEFAULT_WORKER_MIN);
                }
                Element policy = processorElement.addElement("BusPolicy").addAttribute("name", "Policy-"+curFilter.getTopic().replaceAll("\\.", "_"));
                if(curFilter.getCertifiedMessaging())
                {
                    policy.addElement("Attribute").addAttribute("name", "guaranteed").addAttribute("type","Boolean").addText("true");
                    policy.addElement("Attribute").addAttribute("name", "CMName").addAttribute("type","String").addText(curFilter.getTopic().replaceAll("\\.", "_"));
                    
                }
                else
                {
                    policy.addElement("Attribute").addAttribute("name", "guaranteed").addAttribute("type","Boolean").addText("false");
                }
                
                processElements.add(processorElement);
                
                //Alright now lets create the bus topic element
                DefaultElement topicElement = new DefaultElement("BusTopic");
                topicElement.addAttribute("name", curFilter.getTopic().trim()).addAttribute("class", config.getHandlerClassname());
                Element attributeServices = topicElement.addElement("Attribute").addAttribute("name", "service").addAttribute("type","Array");
                attributeServices.addElement("entry").addAttribute("type", "URI").addText(curFilter.getBusUri().trim());
                topicElements.add(topicElement);
                
            }
            
            for(Element curProcessElement: processElements)
            {
                root.add(curProcessElement);
            }
            for(Element curTopicElement: topicElements)
            {
                root.add(curTopicElement);
            }
            //add account
            root.addElement("Attribute").addAttribute("name", "Account").addAttribute("type","String").addText(config.getTomid());
                           
            //lets write the xml to a file of format busresponder-rvd23985248524omahft28524.xml
            String fileName = "rsremote/config/tibcobespoke/busresponder-"+minimizeBusName(curBus)+".xml";
            OutputFormat format = OutputFormat.createPrettyPrint();
            try
            {
                Log.log.trace("Writting xml to file: " + fileName);
                FileWriter fwriter = new FileWriter(FileUtils.getFile(fileName));
                XMLWriter writer = new XMLWriter(fwriter, format);
                writer.write( xmlDoc );
                writer.close();
            }
            catch(Exception e)
            {
                Log.log.error("TIBCO: Issue creating Xml BusResponder files. ", e);
            }
        }
    }
    
    private void initRabbitTibcoSubscriptionListeners(Collection<String> busList) throws InterruptedException
    {
        if(busList != null && busList.size() > 0)
        {
            String[] busListArray = busList.toArray(new String[busList.size()]);
            for(String bus: busListArray)
            {
                TIBCOBespokeProcess busProcess = getProcessForBusAndType(bus, "BUSRESPONDER");
                long startTime = System.currentTimeMillis();
                while( busProcess == null && System.currentTimeMillis() - startTime < 1000*20 ) //20 second timeout
                {
                    busProcess = getProcessForBusAndType(bus, "BUSRESPONDER");
                    Thread.sleep(50);
                }
                if(busProcess != null)
                {
                    try
                    {
                        String busResponderTopicForProcess = BUSRESPONDER_SUBSCRIPTION_TOPIC + busProcess.getProcessId();
                        processComm.subscribe(busResponderTopicForProcess, new TIBCOBespokeRabbitWorker() {
                            @Override
                            public String doWork(String message)
                            {
                                try
                                {
                                    processData(message);
                                }
                                catch(Exception e)
                                {
                                    Log.log.error("TIBCORabbit: Issue processing data in queue", e);                            
                                }
                                
                                return "";
                            }
                        });
                    }
                    catch(Exception e)
                    {
                        Log.log.error("TIBCORabbit: Exception subscribing to queue", e);
                    }
                }
            }
        }        
    }

    /***
     * This will start a process but wait for the heartbeat until returning.
     * If a process already exists for a type and bus then another process will not be started.
     * The method is synchronized to prevent duplicate startup.
     * @param bus
     * @param type
     * @return
     * @throws InterruptedException
     */
     
    public synchronized TIBCOBespokeProcess startBusProcess(String bus, String type) throws InterruptedException
    {
        TIBCOBespokeProcess process = getProcessForBusAndType(bus, TIBCOBespokeProcess.TYPE_PUBLISHER_REQUESTER);
        if(process == null)
        {
            int processWaitCount = 0;
            startBusProcesses(new String[] {bus}, type);
            while((process = getProcessForBusAndType(bus, TIBCOBespokeProcess.TYPE_PUBLISHER_REQUESTER)) == null)
            {
                processWaitCount++;
                if(processWaitCount > 20)
                {
                    throw new RuntimeException("TIBCO: Issue communicating with TIBCO process. Heartbeat not recieved");
                }
                Thread.sleep(1000);
            }
        }        
        return process;
    }
    
    public synchronized void startBusProcesses(String[] busList, String type)
    {
        try
        {
            String resolveHomeDir = System.getProperty("user.dir");
            String busClassPathJars = config.getBusJarFiles();
            String ldLibraryPath = config.getLdLibraryPath();
            String rstibJar = resolveHomeDir + "/lib/rstib.jar";
            String logTypes = config.getLogTypes();
            String javaHome = System.getProperty("java.home");
            String classPath = "";
            classPath += busClassPathJars + File.pathSeparator;
            classPath += rstibJar + File.pathSeparator;
            classPath += resolveHomeDir + "/lib/rabbitmq-client.jar" + File.pathSeparator;
            classPath += resolveHomeDir + "/lib/guava.jar" + File.pathSeparator;
            classPath += resolveHomeDir + "/lib/commons-lang3.jar";
                        
            if(StringUtils.isBlank(ldLibraryPath))
            {
                throw new RuntimeException("TIBCO: Blueprints missing field: rsremote.receive.tibcobespoke.ldlibrarypath. Set this to the tibco library, usually at /opt/tib/<id>/tibrv/lib");            
            }
            if(StringUtils.isBlank(busClassPathJars))
            {
                throw new RuntimeException("TIBCO: Blueprints missing field: rsremote.receive.tibcobespoke.busjarfiles. Set this to point to: argos.jar, bus.jar, rvbus.jar, tibrvj.jar");
            }
            
            //Wait until all processes are down before starting, if two files try to reference the same .ldg files,
            //then they will cancel each other out.
            if(type.equals(TIBCOBespokeProcess.TYPE_BUSRESPONDER))
            {                
                long startTime = System.currentTimeMillis();
                while(JavaCommandLineJarExecutor.getActiveTibcoProcesses().size() > 0)
                {
                    if(System.currentTimeMillis() - startTime > 20000)
                    {
                        Log.log.error("TIBCO: TIBCOBus processes existed for more than 20 seconds, processes were expected to terminate\n");
                        break;
                    }
                    Thread.sleep(500);
                }
                
                sendShutdownRequestsAllProcesses(); //Just to make sure we set those deleted flags to true
            }
            
            for(int i = 0; i < busList.length; i++)
            {            
                String curBus = busList[i];
                String xmlFileName = "busresponder-" + minimizeBusName(curBus) + ".xml";
                
                ArrayList<String> argsList = new ArrayList<String>();
                if(StringUtils.isEmpty(type))
                {
                    throw new RuntimeException("TIBCO: type is null or empty cannot deploy subscriber process");
                }
                if(StringUtils.isEmpty(curBus))
                {
                    throw new RuntimeException("TIBCO: Bus Uri is null or empty cannot deploy subscriber process");
                }
                if(StringUtils.isEmpty(config.getTomid()))
                {
                    throw new RuntimeException("TIBCO: Missing configured TOMID in blueprints");
                }
                if(StringUtils.isEmpty(config.getHandlerClassname()))
                {
                    throw new RuntimeException("TIBCO: Missing configured HandlerClassname in blueprints");
                }
                if(StringUtils.isEmpty(logTypes))
                {
                    logTypes = "INFO,ERROR,WARN,FATAL";
                }
                
                if(type.equals(TIBCOBespokeProcess.TYPE_BUSRESPONDER))
                {
                    //Check if file exists before continuing
                    String xmlFilePath = resolveHomeDir + "/rsremote/config/tibcobespoke/" + xmlFileName;
                    File xmlFile = FileUtils.getFile(xmlFilePath);
                    long checkStartTime = System.currentTimeMillis();
                    //Some time is needed for to allow the concurrent creation of the xml file.
                    //this loop will wait for 20 seconds until the file exists.
                    while(!xmlFile.exists())
                    {
                        if(System.currentTimeMillis() - checkStartTime > 20000)
                        {
                            Log.log.error("TIBCO: Waited 20 seconds for .xml file creation, but file never came into exististence: " + xmlFilePath );
                            break;
                        }
                        Thread.sleep(100);
                    }
                    
                    
                    //Add xml location to params
                    argsList.add("-xml " + xmlFilePath);
                }
                argsList.add("-type "+ type);
                argsList.add("-busuri " + curBus);
                argsList.add("-account " + config.getTomid());
                argsList.add("-bushandler " + config.getHandlerClassname());
                argsList.add("-service " + type + "-" + minimizeBusName(curBus));
                argsList.add("-rabbithost " + MainBase.getESB().getMServer().getConfig().getBrokerAddr());
                argsList.add("-logtypes " + logTypes);
                argsList.add("-rabbitcorepoolsize " + config.getRabbitCorePoolSize());
                argsList.add("-rabbitmaxpoolsize " + config.getRabbitMaxPoolSize());
                argsList.add("-rabbitkeepalivetime " + config.getRabbitKeepAliveTime());
                argsList.add("-rabbitblockingqueuesize " + config.getRabbitBlockingQueueSize());
                argsList.add("-queuename " + config.getQueue());
                
                
                final String args = StringUtils.join(argsList.toArray(), " ");
                String executeFolder = minimizeBusName(curBus) + "-" + type;
                String executeDirectory = resolveHomeDir + "/tibcobespoke/" + executeFolder;                
                ArrayList<String> execEnv = new ArrayList<String>();
                execEnv.add("LD_LIBRARY_PATH");
                execEnv.add(ldLibraryPath);
                
                final JavaCommandLineJarExecutor javaExecutor = new JavaCommandLineJarExecutor(executeDirectory, javaHome, classPath, execEnv.toArray(new String[execEnv.size()]));
                new Thread( new Runnable() {
                    @Override
                    public void run()
                    {
                        javaExecutor.executeIndefinitely("com.resolve.tibcostandalone.TIBCOBespokeMain", args);
                    }
                }).start();
            }
        }
        catch(Exception e)
        {
            Log.log.error("TIBCO: Issue creating processes", e);
        }
    }
    
    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new TIBCOBespokeFilter((String) params.get(TIBCOBespokeFilter.ID), (String) params.get(TIBCOBespokeFilter.ACTIVE), (String) params.get(TIBCOBespokeFilter.ORDER), (String) params.get(TIBCOBespokeFilter.INTERVAL), (String) params.get(TIBCOBespokeFilter.EVENT_EVENTID), (String) params.get(TIBCOBespokeFilter.RUNBOOK), (String) params.get(TIBCOBespokeFilter.SCRIPT), (String) params.get(TIBCOBespokeFilter.REGEX), (String) params.get(TIBCOBespokeFilter.BUS_URI),
                        (String) params.get(TIBCOBespokeFilter.TOPIC), (String) params.get(TIBCOBespokeFilter.CERTIFIED_MESSAGING), (String) params.get(TIBCOBespokeFilter.XML_QUERY), (String) params.get(TIBCOBespokeFilter.UNESCAPE_XML), (String) params.get(TIBCOBespokeFilter.PROCESS_AS_XML), (String) params.get(TIBCOBespokeFilter.REQUIRE_REPLY), (String) params.get(TIBCOBespokeFilter.REPLY_TIMEOUT));
    }

    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList)
    {   
        long curTimeStamp = System.currentTimeMillis();
        //There is an issue with clearandset being called directly after initilization.
        if(curTimeStamp - initTimestamp > 30000) 
        {
            Log.log.debug("TIBCOBespoke: Clearing and setting filters");
            //build a map of the current filters before calling super and updating deployed filters
            ConcurrentHashMap<String, Filter> originalFilters = new ConcurrentHashMap<String, Filter>();
            for (String key : filters.keySet())
            {
                originalFilters.put(key, filters.get(key));
            }
            
            super.clearAndSetFilters(filterList);
            
            if (filtersChanged(originalFilters) && isPrimary() && isActive())
            {            
                buildSubscriptions();
                try
                {
                    Thread.sleep(1000);
                }
                catch (InterruptedException e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
    }
    
    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> removedFilters)
    {
        //Put stuff here if want to do something specific when a list of filters are removed.
        //ClearAndSetFilters will auto redeploy when any change is made to the filter list or its containing filters.
    }

    /**
     * This method processes the messages received from the TIBCO server. It
     * checks the message with any interested filter and process the filter.
     * Depending on filter settings the message could be parsed out and queried via
     * the standard resolve query language
     * 
     * @param msgEnvString - Message from tibco in a custom rabbitmq envelope generated by TIBCOBespokeRabbitCommunicator.
     * @throws SAXException 
     */
    protected void processData(String msgEnvString) throws SAXException
    {
        MessageEnvelope msgEnv = new MessageEnvelope(msgEnvString);
        String messageString = msgEnv.getMessage();
        String messageTopic = msgEnv.getMessageTopic();
        String messageProcessId = msgEnv.getProcessId();
        String messageBusUri = heartbeatList.get(messageProcessId).getBusUri();
        String messageStoreId = msgEnv.getStoreId();
        
        Map<String, String> normalizedXml = null;
        try
        {
            normalizedXml = XmlUtils.normalizeXml(messageString);
        }
        catch (DocumentException e1)
        {
            Log.log.trace("Could not normalized as XML: " + messageString);
            normalizedXml = null;
        }
        
        try
        {            
            for (Filter filter : orderedFilters)
            {                
                TIBCOBespokeFilter tibcoFilter = (TIBCOBespokeFilter) filter;                
                if(tibcoFilter.isActive() && tibcoFilter.getTopic().equals(messageTopic) && tibcoFilter.getBusUri().equals(messageBusUri))                
                {                    
                    if(!messageString.isEmpty()) // check if message is empty
                    {                        
                        Boolean filterMatched = false;                      
                        
                        if (StringUtils.isNotEmpty(tibcoFilter.getXmlQuery()))
                        {
                            if(normalizedXml != null)
                            {
                                filterMatched = isFilterMatch(tibcoFilter, normalizedXml);                                
                            }
                        }
                        else
                        {
                            // Set matched to true if no xml or user does not want to process xml
                            filterMatched = true;
                        }

                        if (filterMatched)
                        {   
                            Map<String, String> runbookParams = new HashMap<String, String>();
                            if (normalizedXml != null && normalizedXml instanceof Map)
                            {
                                runbookParams.putAll(normalizedXml);
                            }

                            runbookParams.put("TIB_MESSAGE", messageString);
                            runbookParams.put("TIB_TOPIC", tibcoFilter.getTopic());
                            runbookParams.put("TIB_BUS_URI", tibcoFilter.getBusUri());
                            runbookParams.put("TIB_MESSAGE_ID", messageStoreId+"#"+messageProcessId);
                            runbookParams.put(Constants.EVENT_EVENTID, tibcoFilter.getEventEventId());

                            addToPrimaryDataQueue(tibcoFilter, runbookParams);
                            break;
                        }
                        else
                        {
                            Log.log.debug("TIBCO message did not match filter: " + tibcoFilter.getId() + ", MessageString: " + messageString);
                        }
                    }
                    else
                    {
                        Log.log.debug("TIBCO message was blank");
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("TIBCO: Error: " + e.getMessage(), e);
        }
    }

    private boolean isFilterMatch(TIBCOBespokeFilter tibFilter, Map<String, String> normalizedXml)
    {
        Boolean result = null;
        try
        {
            synchronized(this)
            {
                if (tibFilter.getNativeQuery() == null)
                {
                        try
                        {
                            // The translaters parser doesn't like "." inside of
                            // assignment names.
                            // Replaced "." with "_DOT_"
                            String xmlQuery = tibFilter.getXmlQuery().replaceAll("\\.", "_DOT_");
                            String translatedQuery = resolveQueryTranslator.translate(xmlQuery);
                            tibFilter.setNativeQuery(translatedQuery);
                        }
                        catch (Exception e)
                        {
                            tibFilter.setNativeQuery("ERROR_IN_TRANSLATING_UI_QUERY");
                            Log.log.error("TIBCO: Bad RSQL syntax for filter. Query provided \"" + tibFilter.getXmlQuery() + "\".", e);
                        }
                }
            }

            result = tibFilter.isXmlMatch(normalizedXml);
        }
        catch (Exception e)
        {
            Log.log.error("TIBCO: Error checking filter against message:\n", e);
        }

        return result;
    }

    protected void sendShutdownRequestsAllProcesses()
    {
        Log.log.trace("Sending shutdown request to all tibco processes");
        Log.log.trace("Heartbeat list of processes to shutdown: " + heartbeatList.toString());
        Iterator<TIBCOBespokeProcess> heartbeatIterator = heartbeatList.values().iterator();
        while(heartbeatIterator.hasNext())
        {
            TIBCOBespokeProcess process = heartbeatIterator.next();
            try
            {
                process.setDeleted();
                processComm.publish(process.getShutdownQueue(), "Please shutdown");
            }
            catch(Exception e)
            {
                Log.log.error("Issue sending shutdown signal to: " + process.getShutdownQueue(), e);
            }
        }
        
        try
        {
            Thread.sleep(500);
        }
        catch (InterruptedException e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    /**
     * This should be a shutdown hook for when the user stops the RSREMOTE
     */
    private void initShutdownHook()
    {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run()            
            {
                Log.log.trace("TIBCOBespokeGateway: SHUTDOWN HOOK CALLED");
                sendShutdownRequestsAllProcesses();
            }
        });
    }
    
    @Override
    protected boolean requireLicense()
    {
        return false;
    }
    
    @Override
    public void start()
    {
        Log.log.warn("TIBCO: Starting TIBCO Gateway Filter Listener");
        super.start();
    }

    @Override
    public void stop()
    {
        Log.log.warn("TIBCO: Stopping TIBCO Gateway.");
        super.stop();
        deactivate();
    } // start

    @Override
    public void run()
    {
        // This call is must for all the ClusteredGateways.
        super.sendSyncRequest();
    }
    
    @Override
    public String getLicenseCode()
    {
        return "TIBCOBESPOKE";
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
        
    }
    
    private static Collection<String> getDistinctBusList(Collection<Filter> filters)
    {
        Collection<String> busList = new ArrayList<String>();
        
        Iterator<Filter> filterIter = filters.iterator();
        while(filterIter.hasNext())
        {
            TIBCOBespokeFilter curFilter = (TIBCOBespokeFilter) filterIter.next();
            if(!busList.contains(curFilter.getBusUri()))
            {
                busList.add(curFilter.getBusUri());
            }                            
        }
        
        return busList;
    }
    
    /***
     * Iterates through the Filter list and return records that have distinct topics per bus.
     * Removes any fitlers that are not active do not have a topic or do not have a busuri.
     * @param busUri - Filters you want for a particular bus
     * @param filters - Filter list to iterator through
     * @return Collection of distinct filters by topic and bus
     */
    private static Collection<Filter> getDistinctFiltersPerTopicByBus(String busUri, Collection<Filter> filters)
    {
        Collection<Filter> filtersForBus = new ArrayList<Filter>(); 
        
        Iterator<Filter> filterIter = filters.iterator();
        while(filterIter.hasNext())
        {
            TIBCOBespokeFilter curFilter = (TIBCOBespokeFilter) filterIter.next();
            if(curFilter.getBusUri().equals(busUri))
            {
                filtersForBus.add(curFilter);
            }
        }
        
        //alright need to remove filters which have identical topics
        Collection<Filter> distinctFilterPerTopicByBus = new ArrayList<Filter>();
        Iterator<Filter> filtersForBusIter = filtersForBus.iterator();
        while(filtersForBusIter.hasNext())
        {
            TIBCOBespokeFilter curFilter = (TIBCOBespokeFilter) filtersForBusIter.next();
            if(curFilter.isActive() && !StringUtils.isBlank(curFilter.getBusUri()) && !StringUtils.isBlank(curFilter.getTopic()))
            {
                //if list does not already contain topic add
                boolean alreadyContainsTopicAndBus = false;
                Iterator<Filter> distinctIter = distinctFilterPerTopicByBus.iterator();
                while(distinctIter.hasNext())
                {
                    TIBCOBespokeFilter curDistinct = (TIBCOBespokeFilter) distinctIter.next();
                    if(curDistinct.getBusUri().equals(curFilter.getBusUri()) && curDistinct.getTopic().equals(curFilter.getTopic()))
                    {
                        alreadyContainsTopicAndBus = true;
                    }
                }
                
                if(!alreadyContainsTopicAndBus)
                {
                    distinctFilterPerTopicByBus.add(curFilter);
                }
            }
        }
               
        return distinctFilterPerTopicByBus;
    }

    private static String minimizeBusName(String bus)
    {
        return bus.replaceAll("[:\\.\\/\\\\]", "");
    }
    
    /**
     * Checks if the filters have changed since the user last clicked deployed.
     * The goal of the function is to determine whether the filters have changed enough that 
     * the tibco processes need to be reinitialized and reconfigured.
     * @param originalFilters
     * @return
     */
    private boolean filtersChanged(ConcurrentHashMap<String, Filter> originalFilters)
    {
        if(originalFilters.size() != filters.size())
        {
            return true;
        }
        
        //sizes are the same must do deep comparison
        Iterator<String> newFilterKeyIter = filters.keySet().iterator();
        while(newFilterKeyIter.hasNext())
        {
            String key = newFilterKeyIter.next();
            if(!originalFilters.containsKey(key))
            {
                return true;
            }
            else
            {
                TIBCOBespokeFilter originalFilter = (TIBCOBespokeFilter) originalFilters.get(key);
                TIBCOBespokeFilter newFilter = (TIBCOBespokeFilter) filters.get(key);
                
                if(!originalFilter.equals(newFilter))
                {
                    return true;
                }
            }
        }
        
        return false;
        
    }
}
