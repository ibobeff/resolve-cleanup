package com.resolve.gateway.resolvegateway;

import java.io.StringReader;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import com.resolve.gateway.resolvegateway.msg.MSGGatewayFilter;
import com.resolve.gateway.resolvegateway.pull.PullGatewayFilter;
import com.resolve.gateway.resolvegateway.push.PushGatewayFilter;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;

public class ResolveGatewayUtil
{
    private static final long SEND_TIMEOUT = 30 * 1000; // milliseconds
                    
    public static Map<String, String> loadGatewayProperties(String config) {
        
        Map<String, String> properties = new HashMap<String, String>();

        try {
            Document doc = stringToDom(config);
            NamedNodeMap attrs = doc.getDocumentElement().getAttributes();
            
            for(int i=0; i<attrs.getLength(); i++) {
                Node attr = attrs.item(i);
                String key = attr.getNodeName();
                String value = attr.getNodeValue();
                properties.put(key, value);
                Log.log.trace(key + ":" + value);
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
            
        return properties;
    }
    
    public static Map<String, Map<String, String>> loadGatewayPropertiesListFromDB(String orgName, String gatewayType) {
        
        Map<String, Map<String, String>> map = null;
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("orgName", orgName);
        params.put("gatewayType", gatewayType);

        try {
            map = MainBase.getESB().call(Constants.ESB_NAME_SYSTEM_RSCONTROL, "MAction.loadGatewayProperties", params, SEND_TIMEOUT);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        
        return map;
    }
    
    public static void updateGatewayPropertiesToDB(String orgName, String gatewayType, GatewayProperties properties) {
        
        Map<String, Map<String, String>> map = null;
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("ORGNAME", orgName);
        params.put("gatewayType", gatewayType);
        
        try {       
            Method[] methods = GatewayProperties.class.getMethods();
            if(methods != null && methods.length != 0) {
                for(Method method:methods) {
                    String methodName = method.getName();
                    
                    if(StringUtils.isNotBlank(methodName) && methodName.length() > 3) {
                        if(methodName.equals("getAdditionalProperties"))
                            continue;
                        
                        if(!methodName.startsWith("get"))
                            continue;
                        
                        String key = methodName.substring(3).toLowerCase();
                        Object obj = method.invoke(properties, (Object[])null);
                        
                        if(obj != null)
                            params.put(key.toUpperCase(), obj);
                    }
                }
            }
            
            Map<String, Object> attrs = properties.getAdditionalProperties();
            if(attrs != null && attrs.size() != 0) {
                for(Iterator<String> it=attrs.keySet().iterator(); it.hasNext();) {
                    String key = it.next();
                    Object obj = attrs.get(key);
                    
                    String value = null;
                    if(value instanceof String)
                        value = (String)obj;
                    else
                        value = obj.toString();
                    
                    params.put(key, value);
                }
            }
                
            MainBase.getESB().call(Constants.ESB_NAME_SYSTEM_RSCONTROL, "MAction.updateGatewayProperties", params, SEND_TIMEOUT);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static Map<String, Map<String, Object>> loadGatewayFilters(String gatewayName, String gatewayType, String queueName) {
        
        Map<String, Map<String, Object>> map = null;
                        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("gatewayName", gatewayName);
        params.put("gatewayType", gatewayType);
        params.put("queueName", queueName);

        try {
            map = MainBase.getESB().call(Constants.ESB_NAME_SYSTEM_RSCONTROL, "MAction.loadGatewayFilters", params, SEND_TIMEOUT);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        
        return map;
    }
    
    public static Map<String, Object> loadGatewayFilterAttributes(String sysId, String gatewaytype) {
        
        Map<String, Object> map = null;
                        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("sysId", sysId);
        params.put("gatewayType", gatewaytype);

        try {
            map = MainBase.getESB().call(Constants.ESB_NAME_SYSTEM_RSCONTROL, "MAction.loadGatewayFilterAttributes", params, SEND_TIMEOUT);
            
            if(map != null && map.size() != 0)
                return map;
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        
        return map;
    }
/*    
    public static Map<String, Map<String, String>> loadGatewayFilters(String gatewayName, String packageName, String config) {
        
        Map<String, Map<String, String>> map = new HashMap<String, Map<String, String>>();
        
        // Don't need to load from file system because it should be saved in and loaded from the database
        try {
            Document doc = stringToDom(config);
            NodeList nodeList = doc.getDocumentElement().getElementsByTagName("FILTER");
            
            for(int i=0; i<nodeList.getLength(); i++) {
                Node filterNode = nodeList.item(i);
                Map<String, String> filterMap = new HashMap<String, String>();
                String filterName = filterNode.getNodeName();
                NamedNodeMap attrs = ((Element)filterNode).getAttributes();
                
                for(int j=0; j<attrs.getLength(); j++) {
                    Node attr = attrs.item(j);
                    String key = attr.getNodeName();
                    String value = attr.getNodeValue();
                    filterMap.put(key, value);
                    Log.log.trace(key + ":" + value);
                }
            
                map.put(filterName, filterMap);
            }
        } catch(Exception e) {
            Log.log.debug(e.getMessage(), e);
        }
        
        return map;
    }
*/
    public static void updatePushGatewayFilter(PushGatewayFilter push) {
        
        Map<String, Object> params = new HashMap<String, Object>();
        
        params.put("uactive", (((new Boolean(push.isActive())).toString())));
        params.put("udeployed", (((new Boolean(push.isDeployed())).toString())));
        params.put("uupdated", (((new Boolean(push.isUpdated())).toString())));
        params.put("uorder", (((new Integer(push.getOrder())).toString())));
        params.put("uinterval", (((new Integer(push.getInterval())).toString())));
        params.put("uname", push.getId());
        params.put("uqueue", push.getQueue());
        params.put("ugatewayName", push.getGatewayName());
        params.put("ueventEventId", push.getEventEventId());
        params.put("urunbook", push.getRunbook());
        params.put("uscript", push.getScript());
        params.put("ublocking", push.getBlocking());
        
        params.put("uuri", push.getUri());
        params.put("uport", (((new Integer(push.getPort())).toString())));
        params.put("username", push.getUsername());
        
        params.put("type", "PUSH");
        
        Map<String, Object> attrs = push.getAttributes();
        
        for(Iterator<String> attr=attrs.keySet().iterator();attr.hasNext();) {
            String key = attr.next();
            Object obj = attrs.get(key);
            if(obj == null)
                continue;
            String value = obj.toString();
            
            if(key != null)
                params.put(key, value);
        }
        
        try {
            MainBase.getESB().sendInternalMessage(Constants.ESB_NAME_SYSTEM_RSCONTROL, "MAction.updateGatewayFilter", params);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static void updatePullGatewayFilter(PullGatewayFilter pull, boolean isDeploying) {
        
        Map<String, Object> params = new HashMap<String, Object>();
        
        params.put("uactive", (((new Boolean(pull.isActive())).toString())));
        params.put("udeployed", (((new Boolean(pull.isDeployed())).toString())));
        params.put("uupdated", (((new Boolean(pull.isUpdated())).toString())));
        params.put("uorder", (((new Integer(pull.getOrder())).toString())));
        params.put("uinterval", (((new Integer(pull.getInterval())).toString())));
        params.put("uname", pull.getId());
        params.put("uqueue", pull.getQueue());
        params.put("ugatewayName", pull.getGatewayName());
        params.put("ueventEventId", pull.getEventEventId());
		params.put("urunbook", pull.getRunbook());
        params.put("uscript", pull.getScript());
        params.put("uquery", pull.getQuery());
        
        String lastId = pull.getLastId();
        if(StringUtils.isNotBlank(lastId)) 
            params.put("ulastId", lastId);
        else
            params.put("ulastId", "");
        
        params.put("utimeRange", pull.getTimeRange());
        params.put("ulastValue", pull.getLastValue());
        params.put("ulastTimestamp", pull.getLastTimestamp());
        params.put("username", pull.getUsername());
        
        params.put("type", "PULL");
        
        Map<String, Object> attrs = pull.getAttributes();
        
        for(Iterator<String> attr=attrs.keySet().iterator();attr.hasNext();) {
            String key = attr.next();
            Object obj = attrs.get(key);
            if(obj == null)
                continue;
            String value = obj.toString();
            
            if(key != null)
                params.put(key, value);
        }

        //key for weather deploying or not this is done to avoid updating the deployed filters with the data
        //from the main filter record. This overwrites the existing deployed filter data 
        //
        if(isDeploying) {
            params.put("DACTION", "true");
        }else {
            params.put("DACTION", "false");
        }
  
        try {
            MainBase.getESB().sendInternalMessage(Constants.ESB_NAME_SYSTEM_RSCONTROL, "MAction.updateGatewayFilter", params);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static void updateMSGGatewayFilter(MSGGatewayFilter msgGatewayFilter) {
        
        Map<String, Object> params = new HashMap<String, Object>();
        
        params.put("uactive", (((new Boolean(msgGatewayFilter.isActive())).toString())));
        params.put("udeployed", (((new Boolean(msgGatewayFilter.isDeployed())).toString())));
        params.put("uupdated", (((new Boolean(msgGatewayFilter.isUpdated())).toString())));
        params.put("uorder", (((new Integer(msgGatewayFilter.getOrder())).toString())));
        params.put("uinterval", (((new Integer(msgGatewayFilter.getInterval())).toString())));
        params.put("uname", msgGatewayFilter.getId());
        params.put("ugatewayName", msgGatewayFilter.getGatewayName());
        params.put("uqueue", msgGatewayFilter.getQueue());
        params.put("ueventEventId", msgGatewayFilter.getEventEventId());
        params.put("urunbook", msgGatewayFilter.getRunbook());
        params.put("uscript", msgGatewayFilter.getScript());
        params.put("uusername", msgGatewayFilter.getUser());
        params.put("upassword", msgGatewayFilter.getPass());
        params.put("ussl", msgGatewayFilter.getSsl());
        params.put("username", msgGatewayFilter.getUsername());
        
        params.put("type", "MSG");
        
        Map<String, Object> attrs = msgGatewayFilter.getAttributes();
        
        for(Iterator<String> attr=attrs.keySet().iterator();attr.hasNext();) {
            String key = attr.next();
            String value = attrs.get(key).toString();
            
            if(key != null)
                params.put(key, value);
        }
        
        try {
            MainBase.getESB().sendInternalMessage(Constants.ESB_NAME_SYSTEM_RSCONTROL, "MAction.updateGatewayFilter", params);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static void undeployPushGatewayFilter(PushGatewayFilter filter) {
        
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("uname", filter.getId());
        params.put("ugatewayName", filter.getGatewayName());
        params.put("uqueue", filter.getQueue());
        params.put("username", filter.getUsername());
        
        params.put("type", "PUSH");
        
        try {
            MainBase.getESB().sendInternalMessage(Constants.ESB_NAME_SYSTEM_RSCONTROL, "MAction.undeployGatewayFilter", params);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static void undeployPullGatewayFilter(PullGatewayFilter filter) {
        
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("uname", filter.getId());
        params.put("ugatewayName", filter.getGatewayName());
        params.put("uqueue", filter.getQueue());
        params.put("username", filter.getUsername());
        params.put("type", "PULL");
        
        try {
            MainBase.getESB().sendInternalMessage(Constants.ESB_NAME_SYSTEM_RSCONTROL, "MAction.undeployGatewayFilter", params);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static void undeployMSGGatewayFilter(MSGGatewayFilter filter) {
        
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("uname", filter.getId());
        params.put("ugatewayName", filter.getGatewayName());
        params.put("uqueue", filter.getQueue());
        params.put("username", filter.getUsername());
        params.put("type", "MSG");
        
        try {
            MainBase.getESB().sendInternalMessage(Constants.ESB_NAME_SYSTEM_RSCONTROL, "MAction.undeployGatewayFilter", params);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static Document stringToDom(String xmlSource) throws Exception {
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        
        return builder.parse(new InputSource(new StringReader(xmlSource)));
    }
    
} // class ResolveGatewayUtil