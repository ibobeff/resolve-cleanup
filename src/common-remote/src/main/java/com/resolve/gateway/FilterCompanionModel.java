/******************************************************************************
* (C) Copyright 2019
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This is a marker annotation to specify filter companion model
 * and associated methods to deploy filter companion model.
 * 
 * Refer to MDatabase.clearAndSetPools method for its usage.
 *
 */
@Target(value = ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface FilterCompanionModel {
	/*
	 * ideally type should should have been model Class instead of model name but that would 
	 * add dependency of rsremote project on persistence project.
	 */
	public String modelName();  
	public boolean isClearAndSetMethod() default true;
	public boolean isdeployMethod() default true;
	public boolean isUndeployMethod() default true;
}
