package com.resolve.gateway.pushauth;

public class UnknownAuthenticationTypeException extends Exception
{

    /**
     * 
     */
    private static final long serialVersionUID = 754083750175498402L;

    public UnknownAuthenticationTypeException()
    {
        super();
        // TODO Auto-generated constructor stub
    }

    public UnknownAuthenticationTypeException(String arg0, Throwable arg1, boolean arg2, boolean arg3)
    {
        super(arg0, arg1, arg2, arg3);
        // TODO Auto-generated constructor stub
    }

    public UnknownAuthenticationTypeException(String arg0, Throwable arg1)
    {
        super(arg0, arg1);
        // TODO Auto-generated constructor stub
    }

    public UnknownAuthenticationTypeException(String arg0)
    {
        super(arg0);
        // TODO Auto-generated constructor stub
    }

    public UnknownAuthenticationTypeException(Throwable arg0)
    {
        super(arg0);
        // TODO Auto-generated constructor stub
    }
    
}
