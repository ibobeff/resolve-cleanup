/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.hpsm;

import java.math.BigDecimal;
import java.util.List;

import javax.xml.bind.JAXBElement;

import com.hp.schemas.sm._7.common.DateTimeType;
import com.hp.schemas.sm._7.common.DecimalType;
import com.hp.schemas.sm._7.common.MessageType;
import com.hp.schemas.sm._7.common.MessagesType;
import com.hp.schemas.sm._7.common.ObjectFactory;
import com.hp.schemas.sm._7.common.StringType;

public class HPSMUtil
{
    public static StringType getStringType(String value)
    {
        ObjectFactory fact = new ObjectFactory();
        StringType stringType = fact.createStringType();
        stringType.setValue(value);
        return stringType;
    }

    public static DecimalType getDecimalType(BigDecimal value)
    {
        ObjectFactory fact = new ObjectFactory();
        DecimalType decimalType = fact.createDecimalType();
        decimalType.setValue(value);
        return decimalType;
    }

    public static String getStringValue(JAXBElement<StringType> item)
    {
        String result = null;
        if(item != null && !item.isNil())
        {
            result = item.getValue().getValue();
        }
        return result;
    }

    public static String getDateTimeValue(JAXBElement<DateTimeType> item)
    {
        String result = null;
        if(item != null && !item.isNil())
        {
            result = item.getValue().getValue().toXMLFormat();
        }
        return result;
    }
    
    public static DateTimeType getDateTimeTypeValue(JAXBElement<DateTimeType> item)
    {
        DateTimeType result = null;
        if(item != null && !item.isNil())
        {
            result = item.getValue();
        }
        return result;
    }

    public static String extractError(JAXBElement<MessagesType> messageTypes)
    {
        StringBuilder result = new StringBuilder();

        if(messageTypes != null && messageTypes.getValue() != null)
        {
            MessagesType messageType = messageTypes.getValue();
            List<JAXBElement<MessageType>> messages = messageType.getMessage();
            if(messages != null)
            {
                for(JAXBElement<MessageType> message : messages)
                {
                    MessageType messageType1 = message.getValue();
                    if(messageType1 != null)
                    {
                        result.append(messageType1.getValue() + "\n"); //just add a newline so it looks pretty
                    }
                }
            }
        }
        return result.toString();
    }

}
