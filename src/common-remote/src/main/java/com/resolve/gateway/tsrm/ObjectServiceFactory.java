/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.tsrm;


public class ObjectServiceFactory
{
    public static ObjectService getObjectService(RestCaller restCaller, String object)
    {
        if (ServiceRequestObjectService.OBJECT_IDENTITY.equals(object))
        {
            return new ServiceRequestObjectService(restCaller);
        }
        if (IncidentObjectService.OBJECT_IDENTITY.equals(object))
        {
            return new IncidentObjectService(restCaller);
        }
        else
        {
            throw new RuntimeException(object + " not implemented by TSRM Gateway yet.");
        }
    }
}
