/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.Map;
import java.util.regex.Matcher;

public interface Filter extends Comparable<Filter>
{
    String getId();

    int getOrder();

    int getInterval();

    void setActive(boolean active);

    boolean isActive();

    public String getRunbook();
    
    void setRunbook(String runbook);
    
    boolean hasRunbook();

    String getEventEventId();
    
    void setEventEventId(String eventEventId);

    boolean hasEvent();
    
    boolean hasGroovyScript();

    String getScript();

    long getLastExecutionTime();

    void setLastExecutionTime(long lastExecutionTimeInMillis);
    
    Matcher getMatcher(Map<String, String> params);
    
    /* Push gateway filter blocking/non-blocking and returnd data code
     * 
     * 0 = non-blocking, return immediately after receiving message
     * 
     * 1 = non-blocking, return after execution of gateway filter script, 
     *     filter script can set additional data in response (if any)
     *     
     * 2 (Default) = non-blocking, return after offering mesage to worker queue
     *  
     * 3 = blocking, execute runbook from RSView and get worksheet id back
     * 
     * 4 = blocking, execute runbook from RSControl and get the condition and severity back
     */
    int getBlockingCode();
}
