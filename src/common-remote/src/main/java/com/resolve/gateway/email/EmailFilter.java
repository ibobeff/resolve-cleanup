/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.email;

import com.resolve.gateway.BaseFilter;

public class EmailFilter extends BaseFilter
{
    public static final String QUERY = "QUERY";

    private String query;
    
    // public EmailFilter(String id, String runbook, String order, String
    // active, String field, String regex, String interval, String script)
    public EmailFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String query)
    {
        super(id, active, order, interval, eventEventId, runbook, script);
        setQuery(query);
    } // EmailFilter

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query != null ? query.trim() : query;
    }
}
