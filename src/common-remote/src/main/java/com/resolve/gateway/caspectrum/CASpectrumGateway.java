/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.caspectrum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MCASpectrum;
import com.resolve.rsremote.ConfigReceiveCASpectrum;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class CASpectrumGateway extends BaseClusteredGateway
{
    // Singleton
    private static volatile CASpectrumGateway instance = null;

    // This will be set in initialize() with the proper queue name from the
    // configuration.
    private String queue = "CASPECTRUM";

    private ConcurrentHashMap<String, String> filterToSubscription = new ConcurrentHashMap<String, String>();
    
    public static final String ALARM_ID_KEY_NAME = "@id";
    public static final String DEVICE_ID_KEY_NAME = "@mh";

    public static volatile ObjectService genericObjectService;

    //private final ResolveQuery resolveQuery;

    public static CASpectrumGateway getInstance(ConfigReceiveCASpectrum config)
    {
        if (instance == null)
        {
            instance = new CASpectrumGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static CASpectrumGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("CASpectrum Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    /**
     * Private constructor.
     *
     * @param config
     */
    private CASpectrumGateway(ConfigReceiveCASpectrum config)
    {
        // Important, call super here.
        super(config);
        queue = config.getQueue();
        //resolveQuery = new ResolveQuery(new ServiceNowQueryTranslator());
    }

    @Override
    public String getLicenseCode()
    {
        return "CASPECTRUM";
    }

    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_CASPECTRUM;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MCASpectrum.class.getSimpleName();
    }

    @Override
    protected Class<MCASpectrum> getMessageHandlerClass()
    {
        return MCASpectrum.class;
    }

    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    /**
     * Initializes the specific gateway resources.
     *
     * @param config
     */
    @Override
    protected void initialize()
    {
        ConfigReceiveCASpectrum caSpectrumConfig = (ConfigReceiveCASpectrum) configurations;

        queue = caSpectrumConfig.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/caspectrum/";

        genericObjectService = new GenericObjectService(caSpectrumConfig, filterToSubscription);
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new CASpectrumFilter((String) params.get(CASpectrumFilter.ID), (String) params.get(CASpectrumFilter.ACTIVE), (String) params.get(CASpectrumFilter.ORDER), (String) params.get(CASpectrumFilter.INTERVAL), 
                                    (String) params.get(CASpectrumFilter.EVENT_EVENTID), (String) params.get(CASpectrumFilter.RUNBOOK), (String) params.get(CASpectrumFilter.SCRIPT), (String) params.get(CASpectrumFilter.OBJECT), 
                                    (String) params.get(CASpectrumFilter.URLQUERY), (String) params.get(CASpectrumFilter.XMLQUERY),
                                    (String) params.get(CASpectrumFilter.LATESTALARMTIME));
    } // getFilter

    @Override
    public void start()
    {
        Log.log.warn("Starting CASpectrumListener");
        super.start();
    } // start

    /**
     * This method should acces the CA Spectrum server for its current date
     * and time in GMT. Need to create or access REST service in
     * CA Spectrum server for this. If we do not create or access the REST service then we
     * will use whatever current Date and Time in the Resolve server which may
     * be out of sync with CA Spectrum server.
     */
    private String getCASpectrumServerTimeInSec()
    {
        String result = null;
        
        // Code to access CA Spectrum Date Time Service
        
        if (StringUtils.isBlank(result))
        {
            Date currDate = new Date();
            result = Long.toString((int)(currDate.getTime()/1000));
        }
        
        return result;
    }
    
    @Override
    public void run()
    {
        /* This call is must for all the ClusteredGateways.
         * For testing filter(s) deploying through config file only 
         * enable the MOCK mode.
         */
        
        if (!((ConfigReceiveCASpectrum)configurations).isMock())
            super.sendSyncRequest();

        String caSpectrumServerTimeInSec = getCASpectrumServerTimeInSec();
        
        while (running)
        {
            try
            {
                long startTime = System.currentTimeMillis();

                /* if any remaining events are in primary data queue or 
                 * primary data executor has waiting messages still to be processed, 
                 * do not get messages for more new events.
                 */
                
                if (primaryDataQueue.isEmpty() && !primaryDataQueueExecutorBusy())
                {
                    Log.log.trace(getQueueName() + " Primary Data Queue is empty and Primary Data Queue Executor is free.....");
                    
                    for (Filter filter : orderedFilters)
                    {
                        if (shouldFilterExecute(filter, startTime))
                        {
                            CASpectrumFilter caSpectrumFilter = (CASpectrumFilter) filter;

                            /*
                             * On Creation of new filter Latest Alarm (Processed) Time will be blank or null, 
                             * then set it to current CA Spectrum Server GMT Time (in sec)  
                             */
                            
                            if (StringUtils.isBlank(caSpectrumFilter.getLatestAlarmTime()))
                            {
                                caSpectrumFilter.setLatestAlarmTime(caSpectrumServerTimeInSec);
                            }
                            
                            List<Map<String, String>> results = invokeCASpectrumService(caSpectrumFilter);
                            long newLatestAlarmTime = Long.parseLong(caSpectrumFilter.getLatestAlarmTime()); 
                            if (results.size() > 0)
                            {
                                // Enqueue runbook excution for each object
                                for (Map<String, String> caSpectrumObject : results)
                                {
                                    addToPrimaryDataQueue(caSpectrumFilter, caSpectrumObject);
                                    
                                    if (StringUtils.isNotBlank(caSpectrumFilter.getXmlQuery()))
                                	{
	                                    long alarmLastOccrTime = 0;
                                    	if (caSpectrumObject.containsKey(CASpectrumFilter.LAST_OCCURRENCE_TIME_ATTRIBUTE)
                                    		&& StringUtils.isNotBlank(caSpectrumObject.get(CASpectrumFilter.LAST_OCCURRENCE_TIME_ATTRIBUTE)))
	                                    {
	                                        alarmLastOccrTime = Long.parseLong(caSpectrumObject.get(CASpectrumFilter.LAST_OCCURRENCE_TIME_ATTRIBUTE));
	                                    }
	                                    else
	                                    {
	                                        Log.log.warn("CA Spectrum response is missing Alarm Last Occurrence Attribute (" + 
	                                                     CASpectrumFilter.LAST_OCCURRENCE_TIME_ATTRIBUTE + ")");
	                                    }
	                                        
	                                    if (alarmLastOccrTime == 0)
	                                    {
	                                        Log.log.debug("Alarm Last Occurrence Time (" + CASpectrumFilter.LAST_OCCURRENCE_TIME_ATTRIBUTE + 
	                                                      ") is 0 or missing, checking Alarm First Occurrence Time (" + CASpectrumFilter.FIRST_OCCURRENCE_TIME_ATTRIBUTE + ")");
	                                            
	                                        if (caSpectrumObject.containsKey(CASpectrumFilter.FIRST_OCCURRENCE_TIME_ATTRIBUTE)
	                                        		&& StringUtils.isNotBlank(caSpectrumObject.get(CASpectrumFilter.FIRST_OCCURRENCE_TIME_ATTRIBUTE)))
	                                        {
	                                            alarmLastOccrTime = Long.parseLong(caSpectrumObject.get(CASpectrumFilter.FIRST_OCCURRENCE_TIME_ATTRIBUTE));
	                                        }
	                                        else
	                                        {
	                                            Log.log.warn("CA Spectrum response is missing Alarm First Occurrence Attribute (" + 
	                                                         CASpectrumFilter.FIRST_OCCURRENCE_TIME_ATTRIBUTE + ")");
	                                        }
	                                                
	                                        if (alarmLastOccrTime == 0)
	                                        {
	                                            Log.log.warn("Both Alarm Last Occurrence Time (" + CASpectrumFilter.LAST_OCCURRENCE_TIME_ATTRIBUTE + 
	                                                         ") and First Occurrence Time (" + CASpectrumFilter.FIRST_OCCURRENCE_TIME_ATTRIBUTE + ") are 0 or missing, " +
	                                                         CASpectrumFilter.LATESTALARMTIME + " will not be updated");
	                                        }
	                                    }
	                                    if (alarmLastOccrTime > newLatestAlarmTime)
	                                    {
	                                        newLatestAlarmTime = alarmLastOccrTime;
	                                    }
                                	}
                                }
                                
                                if (StringUtils.isNotBlank(caSpectrumFilter.getXmlQuery()))
                                {
                                    Log.log.debug("Setting Latest Alarm Time to " + newLatestAlarmTime);
                                    caSpectrumFilter.setLatestAlarmTime(Long.toString(newLatestAlarmTime));
                                }
                            }
                        }
                        else
                        {
                            Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                        }
                    }
                }

                // process events collected by processFilter.
                //processFilterEvents(startTime);

                // If there is no filter yet no need to keep spinning, wait for
                // a while
                if (orderedFilters.size() == 0)
                {
                    Thread.sleep(interval);
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Warning: " + e.getMessage());
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", 
                          "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + 
                          "due to " + e.getMessage());
                
                try
                {
                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    } // run

    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> undeployedFilters)
    {
        for(Filter filter : undeployedFilters.values())
        {
            CASpectrumFilter caSpectrumFilter = (CASpectrumFilter) filter;
            
            if (filterToSubscription.containsKey(caSpectrumFilter.getId()))
            {
                try
                {
                    deleteSubscription(filterToSubscription.get(caSpectrumFilter.getId()), null, null);
                }
                catch (Exception e)
                {
                    //Ignore subscription deletion exceptions as CA Spectrum will delete it any way after timeout
                }
                
                filterToSubscription.remove(caSpectrumFilter.getId());
            }
        }
    }
    
    /**
     * This method invokes the CASpectrum RESTful service.
     *
     * @param filter
     * @return - Service response or null based on filter condition.
     * @throws Exception
     */
    private List<Map<String, String>> invokeCASpectrumService(CASpectrumFilter filter) throws Exception
    {
        List<Map<String, String>> result = Collections.emptyList();
        
        Log.log.trace("CASpectrumFilter " + filter);
        
        if (StringUtils.isNotBlank(filter.getObject()))
        {
            if (StringUtils.isNotBlank(filter.getUrlQuery()) || filter.getObject().equalsIgnoreCase("landscapes") )
            {
                result = genericObjectService.getObjects(filter.getObject(), filter.getUrlQuery(), null, null, null, filter.getId(), null);
            }
            else if (StringUtils.isNotBlank(filter.getXmlQuery()))
            {
                String txFormedXMLQuery = filter.getXmlQuery().replaceAll("\\$\\{" + CASpectrumFilter.LATESTALARMTIME + "\\}", filter.getLatestAlarmTime());
                Log.log.trace("Transformed XML Query: [" + txFormedXMLQuery + "]");
                result = genericObjectService.getObjects(filter.getObject(), null, txFormedXMLQuery, null, null, filter.getId(), null);
            }
        }
        
        return result;
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
        return;
    }
    
    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result, Filter filter)
    {
        List<String> objectIdKeyNames = new ArrayList<String>();
        
        switch (((CASpectrumFilter)filter).getObject())
        {
            case "alarms":
                objectIdKeyNames.add(ALARM_ID_KEY_NAME);
                break;
                
            case "devices":
                objectIdKeyNames.add(DEVICE_ID_KEY_NAME);
                break;
            
            case "subscription":
                objectIdKeyNames.add(ALARM_ID_KEY_NAME);
                objectIdKeyNames.add(DEVICE_ID_KEY_NAME);
                break;
        }
        
        if (!objectIdKeyNames.isEmpty())
        {
            for (String objectIdKeyName : objectIdKeyNames)
            {
                if(result.containsKey(objectIdKeyName))
                {
                    event.put(Constants.EXECUTE_REFERENCE, result.get(objectIdKeyName));
                    event.remove(objectIdKeyName);
                    break;
                }
                else if(result.containsKey(objectIdKeyName.toUpperCase()))
                {
                    event.put(Constants.EXECUTE_REFERENCE, result.get(objectIdKeyName.toUpperCase()));
                    event.remove(objectIdKeyName.toUpperCase());
                    break;
                }
            }
        }
    }

    /*
     * CASpectrum Public API (CASpectrumAPI) Implementation
     */
    
    public Map<String, String> issueAnAction(String actionCode, 
                                             String modelHandle, 
                                             Map<String, String> attribValMap, 
                                             int throttlesize,
                                             String username,
                                             String password) throws Exception
    {
        return genericObjectService.issueAnAction(actionCode, modelHandle, attribValMap, throttlesize, username, password);
    }
    
    public List<Map<String, String>> getAlarms(String xml, 
                                               String username,
                                               String password) throws Exception
    {
        return genericObjectService.getObjects("alarms", null, xml, username, password, null, null);
    }
    
    public List<Map<String, String>> getAllAlarms(List<String> attributes,
                                                  List<String> landscapeHandles,
                                                  int throttlesize,
                                                  String username,
                                                  String password) throws Exception
    {
        return genericObjectService.getObjects("alarms", attributes, landscapeHandles, throttlesize, username, password);
    }
    
    public Map<String, String> updateAlarm(String alarmId,
                                           Map<String, String> attribVal,
                                           String username,
                                           String password) throws Exception
    {
        return genericObjectService.updateObject("alarms", alarmId, attribVal, username, password);
    }
    
    public Map<String, String> deleteAlarm(String alarmId,
                                           String username,
                                           String password) throws Exception
    {
        return genericObjectService.deleteObject("alarms", alarmId, username, password);
    }
    
    public Map<String, String> createAssociation(String relationHandle,
                                                 String leftModelHandle,
                                                 String rightModelHandle,
                                                 String username,
                                                 String password) throws Exception
    {
        return genericObjectService.createAssociation(relationHandle, leftModelHandle, rightModelHandle, username, password);
    }
    
    public List<Map<String, String>> getAssociations(String relationHandle,
                                                     String modelHandle,
                                                     String side,
                                                     String username,
                                                     String password) throws Exception
    {
        return genericObjectService.getAssociations(relationHandle, modelHandle, side, username, password);
    }
    
    public Map<String, String> deleteAssociation(String relationHandle,
                                                 String leftModelHandle,
                                                 String rightModelHandle,
                                                 String username,
                                                 String password) throws Exception
    {
        return genericObjectService.deleteAssociation(relationHandle, leftModelHandle, rightModelHandle, username, password);
    }
    
    public Map<String, String> getAttributeEnum(String attrId, 
                                                String username, 
                                                String password) throws Exception
    {
        return genericObjectService.getAttributeEnum(attrId, username, password);
    }
    
    public Map<String, Map<String, List<Map<String, String>>>> getConnectivity(String ipAddress,
                                                                               String username,
                                                                               String password) throws Exception
    {
        return genericObjectService.getConnectivity(ipAddress, username, password);
    }
    
    public List<Map<String, String>> getAllDevices(List<String> attributes,
                                                   List<String> landscapeHandles,
                                                   int throttlesize,
                                                   String username,
                                                   String password) throws Exception
    {
        return genericObjectService.getObjects("devices", attributes, landscapeHandles, throttlesize, username, password);
    }
    
    public List<Map<String, String>> getLandscapes(String username,
                                                   String password) throws Exception
    {
        return genericObjectService.getLandscapes(username, password);
    }
    
    public Map<String, String> createModel(String landscapeHandle,
                                           String modelTypeHandle,
                                           int snmpAgentPort,
                                           String communityString,
                                           int retryCount,
                                           int timeout,
                                           String ipAddress,
                                           String parentModelHandle,                                                
                                           String parentToModelRelationHandle,
                                           Map<String, String> attribValMap,
                                           String username,
                                           String password) throws Exception
    {
        return genericObjectService.createModel(landscapeHandle, modelTypeHandle, snmpAgentPort, 
                                                communityString, retryCount, timeout, ipAddress,
                                                parentModelHandle, parentToModelRelationHandle,
                                                attribValMap, username, password);
    }
    
    public Map<String, String> getModel(String modelHandle,
                                        List<String> attributes,
                                        String username,
                                        String password) throws Exception
    {
        return genericObjectService.getModel(modelHandle, attributes, username, password);
    }
    
    public Map<String, String> updateModel(String modelHandle,
                                           Map<String, String> attribVal,
                                           String username,
                                           String password) throws Exception
    {
        return genericObjectService.updateObject("model", modelHandle, attribVal, username, password);
    }
    
    public Map<String, String> deleteModel(String modelHandle,
                                           String username,
                                           String password) throws Exception
    {
        return genericObjectService.deleteObject("model", modelHandle, username, password);
    }
    
    public List<Map<String, String>> getModels(String xml, 
                                               String username,
                                               String password) throws Exception
    {
        return genericObjectService.getObjects("models", null, xml, username, password, null, null);
    }
    
    public List<Map<String, String>> updateModels(String xml, 
                                                  String username,
                                                  String password) throws Exception
    {
        return genericObjectService.updateObjects("models", xml, username, password);
    }
    
    public Map<String, String> createEvent(String eventType,
                                           String modelHandle,
                                           Map<String, String> varBindIdValue,
                                           String username,
                                           String password) throws Exception
    {
        return genericObjectService.createEvent( eventType, modelHandle, varBindIdValue, username, password);
    }
    
    public List<Map<String, String>> createEvents(String createEventsRequestXml,
                                                  String username,
                                                  String password) throws Exception
    {
        return genericObjectService.createEvents(createEventsRequestXml, username, password);
    }
    
    public String createSubscription(String pullSubscriptionRequestXml,
                                     String username,
                                     String password) throws Exception
    {
        return genericObjectService.createSubscription(pullSubscriptionRequestXml, username, password);
    }
    
    public List<Map<String, String>> getSubscription(String subscriptionId,
                                                     String username,
                                                     String password) throws Exception
    {
        return genericObjectService.getObjects("subscription", null, null, username, password, null, subscriptionId);
    }
    
    public Map<String, String> deleteSubscription(String subscriptionId,
                                                  String username,
                                                  String password) throws Exception
    {
        return genericObjectService.deleteObject("subscription", subscriptionId, username, password);
    }
}
