/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.servicenow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsremote.ConfigReceiveServiceNow;
import com.resolve.util.SoapUtil;
import com.resolve.util.StringUtils;

/**
 * Abstract class for all the ServiceNow object service provider classes.
 * 
 */
public abstract class AbstractObjectService implements ObjectService
{
    protected final ConfigReceiveServiceNow configurations;
    protected final SoapCaller soapCaller;

    protected static final String OBJECT_CLASS = "class";

    public AbstractObjectService(ConfigReceiveServiceNow configurations) throws Exception
    {
        soapCaller = SoapCallerFactory.getInstance(configurations);

        this.configurations = configurations;
    }

    protected String getSoapEnvelope(String objectType, String query, String lastChangeDate)
    {
        StringBuilder soapEnvelope = new StringBuilder("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:get=\"http://www.service-now.com/GetData\">");
        soapEnvelope.append("<soapenv:Header/>");
        soapEnvelope.append("<soapenv:Body>");
        soapEnvelope.append("<get:execute>");
        soapEnvelope.append("<query>" + query + "</query>");            /* Is anyone doing anything about invalid characters here?? */
        soapEnvelope.append("<object>" + objectType + "</object>");
        soapEnvelope.append("<sys_created_on>" + lastChangeDate + " </sys_created_on>");
        soapEnvelope.append("</get:execute>");
        soapEnvelope.append("</soapenv:Body>");
        soapEnvelope.append("</soapenv:Envelope>");

        return soapEnvelope.toString();
    }

    protected String getSoapEnvelope(String sysId, String operation, Map<String, String> params)
    {
        StringBuilder soapEnvelope = new StringBuilder("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:inc=\"http://www.service-now.com/incident\">");
        // Is the ServiceNow makes WS-Security a requirement we'll inject
        // WS-Security headers in the Header element.
        soapEnvelope.append("<soapenv:Header/>");
        soapEnvelope.append("<soapenv:Body>");
        soapEnvelope.append("<inc:" + operation + ">");
        if (StringUtils.isNotEmpty(sysId))
        {                                                                       
            soapEnvelope.append("<sys_id>" + sysId + "</sys_id>");
        }
        for (String key : params.keySet())
        {
            soapEnvelope.append("<" + key + ">" + params.get(key) + "</" + key + ">");
        }

        soapEnvelope.append("</inc:" + operation + ">");
        soapEnvelope.append("</soapenv:Body>");
        soapEnvelope.append("</soapenv:Envelope>");

        return soapEnvelope.toString();
    }

    protected boolean validateObjectId(String objectId) throws Exception
    {
        if (StringUtils.isEmpty(objectId))
        {
            throw new Exception("ID must be provided");
        }
        else
        {
            return true;
        }
    }

    private Map<String, String> unmarshalObject(String xmlContent, String elementTagName) throws Exception
    {
        Map<String, String> result = new HashMap<String, String>();

        if (!StringUtils.isEmpty(xmlContent))
        {
            result = SoapUtil.getData(xmlContent, elementTagName);
        }
        return result;
    }

    /**
     * This method may be overriden in the subclass.
     */
    @Override
    public Map<String, String> create(String objectType, Map<String, String> objectProperties, String username, String password) throws Exception
    {
        String response = null;

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = configurations.getHttpbasicauthusername();
            password = configurations.getHttpbasicauthpassword();
        }
        soapCaller.setUsernameAndPassword(username, password); 

        String soapEnvelope = getSoapEnvelope(null, "insert", objectProperties);

        response = soapCaller.sendSoapRequest(soapEnvelope, objectType);

        return unmarshalObject(response, "insertResponse");
    }

    @Override
    public boolean createWorknote(String objectType, String parentObjectId, String note, String username, String password) throws Exception
    {
        boolean result = true;

        Map<String, String> objectProperties = new HashMap<String, String>();
        objectProperties.put("work_notes", note);
        update(objectType, parentObjectId, objectProperties, username, password);

        return result;
    }

    @Override
    public void update(String objectType, String objectId, Map<String, String> objectProperties, String username, String password) throws Exception
    {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = configurations.getHttpbasicauthusername();
            password = configurations.getHttpbasicauthpassword();
        }
        soapCaller.setUsernameAndPassword(username, password); 

        String soapEnvelope = getSoapEnvelope(objectId, "update", objectProperties);

        soapCaller.sendSoapRequest(soapEnvelope, objectType);
    }

    @Override
    public void delete(String objectType, String objectId, String username, String password) throws Exception
    {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = configurations.getHttpbasicauthusername();
            password = configurations.getHttpbasicauthpassword();
        }
        soapCaller.setUsernameAndPassword(username, password); 

        String soapEnvelope = getSoapEnvelope(objectId, "deleteRecord", new HashMap<String, String>());

        soapCaller.sendSoapRequest(soapEnvelope, objectType);
    }

    @Override
    public Map<String, String> selectByID(String objectType, String objectId, String username, String password) throws Exception
    {
        String response = null;

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = configurations.getHttpbasicauthusername();
            password = configurations.getHttpbasicauthpassword();
        }
        soapCaller.setUsernameAndPassword(username, password); 

        Map<String, String> objectProperties = new HashMap<String, String>();
        objectProperties.put("sys_id", objectId);
        String soapEnvelope = getSoapEnvelope(null, "getRecords", objectProperties);

        response = soapCaller.sendSoapRequest(soapEnvelope, objectType);

        return unmarshalObject(response, "getRecordsResult");
    }

    @Override
    public Map<String, String> selectByAlternateID(String objectType, String altIDName, String altIdValue, String username, String password) throws Exception
    {
        String response = null;

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = configurations.getHttpbasicauthusername();
            password = configurations.getHttpbasicauthpassword();
        }

        soapCaller.setUsernameAndPassword(username, password); 

        Map<String, String> objectProperties = new HashMap<String, String>();
        objectProperties.put(altIDName, altIdValue);
        String soapEnvelope = getSoapEnvelope(null, "getRecords", objectProperties);

        response = soapCaller.sendSoapRequest(soapEnvelope, objectType);

        return unmarshalObject(response, "getRecordsResult");
    }

    @Override
    public List<Map<String, String>> search(String objectType, String query, String orderBy, boolean isAscending, String username, String password) throws Exception
    {
        String response = null;

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = configurations.getHttpbasicauthusername();
            password = configurations.getHttpbasicauthpassword();
        }
        soapCaller.setUsernameAndPassword(username, password); 

        Map<String, String> objectProperties = new HashMap<String, String>();
        objectProperties.put("__encoded_query", query);
        if (StringUtils.isNotEmpty(orderBy))
        {
            if (isAscending)
            {
                objectProperties.put("__order_by", orderBy);
            }
            else
            {
                objectProperties.put("__order_by_desc", orderBy);
            }
        }
        String soapEnvelope = getSoapEnvelope(null, "getRecords", objectProperties);

        response = soapCaller.sendSoapRequest(soapEnvelope, objectType);

        return SoapUtil.getList(response, "getRecordsResult");
    }
    
    @Override
    public List<Map<String, String>> searchWithPageLimit(String objectType, String query, String orderBy, boolean isAscending, int pageLimit, String username, String password) throws Exception
    {
        return search(objectType, query, orderBy, isAscending, username, password );
    }
}
