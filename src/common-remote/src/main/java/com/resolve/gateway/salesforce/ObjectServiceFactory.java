/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.salesforce;

import com.resolve.rsremote.ConfigReceiveSalesforce;

public class ObjectServiceFactory
{
    public static ObjectService getObjectService(ConfigReceiveSalesforce configurations, String object)
    {
        if (object.equals(CaseObjectService.OBJECT_IDENTITY))
        {
            return new CaseObjectService(configurations);
        }
        else
        {
            throw new RuntimeException(object + " not implemented by Salesforce Gateway yet.");
        }
    }
}
