/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.Map;

import com.resolve.gateway.tcp.TCPFilter;
import com.resolve.gateway.tcp.TCPGateway;

public class MTCP extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MTCP.setFilters";

    private static final TCPGateway instance = TCPGateway.getInstance();

    public MTCP()
    {
        super.instance = instance;
    }

    @Override
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    @Override
    protected void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        if (instance.isActive())
        {
            TCPFilter tcpFilter = (TCPFilter) filter;
            filterMap.put(TCPFilter.PORT, "" + tcpFilter.getPort());
            filterMap.put(TCPFilter.BEGIN_SEPARATOR, tcpFilter.getBeginSeparator());
            filterMap.put(TCPFilter.END_SEPARATOR, tcpFilter.getEndSeparator());
            filterMap.put(TCPFilter.INCLUDE_BEGIN_SEPARATOR, String.valueOf(tcpFilter.getIncludeBeginSeparator()));
            filterMap.put(TCPFilter.INCLUDE_END_SEPARATOR, String.valueOf(tcpFilter.getIncludeEndSeparator()));
        }
    }
} // MTCP