package com.resolve.gateway.pushauth;

public class InvalidAuthConfigurationException extends Exception
{

    /**
     * 
     */
    private static final long serialVersionUID = -4638319903524993405L;

    public InvalidAuthConfigurationException()
    {
        super();
        // TODO Auto-generated constructor stub
    }

    public InvalidAuthConfigurationException(String arg0, Throwable arg1, boolean arg2, boolean arg3)
    {
        super(arg0, arg1, arg2, arg3);
        // TODO Auto-generated constructor stub
    }

    public InvalidAuthConfigurationException(String arg0, Throwable arg1)
    {
        super(arg0, arg1);
        // TODO Auto-generated constructor stub
    }

    public InvalidAuthConfigurationException(String arg0)
    {
        super(arg0);
        // TODO Auto-generated constructor stub
    }

    public InvalidAuthConfigurationException(Throwable arg0)
    {
        super(arg0);
        // TODO Auto-generated constructor stub
    }

}
