/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.gateway.hpsm.HPSMConstants;
import com.resolve.util.StringUtils;

@SuppressWarnings("serial")
public class HPSMIncident implements Serializable
{
    private static final long serialVersionUID = 576832085480788417L;
	protected String incidentID;
    protected String category;
    protected String openTime;
    protected String openedBy;
    protected String urgency;
    protected String updatedTime;
    protected String assignmentGroup;
    protected String closedTime;
    protected String closedBy;
    protected String closureCode;
    protected String affectedCI;
    protected List<String> description;
    protected List<String> solution;
    protected String assignee;
    protected String contact;
    protected List<String> journalUpdates;
    protected String alertStatus;
    protected String contactLastName;
    protected String contactFirstName;
    protected String company;
    protected String title;
    protected String ticketOwner;
    protected String updatedBy;
    protected String status;
    protected String area;
    protected BigDecimal SLAAgreementID;
    protected String siteCategory;
    protected String subarea;
    protected String problemType;
    protected String resolutionFixType;
    protected String userPriority;
    protected String location;
    protected List<String> explanation;
    protected String impact;
    protected String folder;
    protected String service;

    public HPSMIncident()
    {

    }

    public HPSMIncident(Map<String, Object> params)
    {
        this.setIncidentID((String) params.get(HPSMConstants.INCIDENT_ID));
        this.setCategory((String) params.get(HPSMConstants.INCIDENT_CATEGORY));
        this.setOpenTime((String) params.get(HPSMConstants.INCIDENT_OPENTIME));
        this.setOpenedBy((String) params.get(HPSMConstants.INCIDENT_OPENEDBY));
        this.setUrgency((String) params.get(HPSMConstants.INCIDENT_URGENCY));
        this.setUpdatedTime((String) params.get(HPSMConstants.INCIDENT_UPDATEDTIME));
        this.setAssignmentGroup((String) params.get(HPSMConstants.INCIDENT_ASSIGNMENTGROUP));
        this.setClosedTime((String) params.get(HPSMConstants.INCIDENT_CLOSEDTIME));
        this.setClosedBy((String) params.get(HPSMConstants.INCIDENT_CLOSEDBY));
        this.setClosureCode((String) params.get(HPSMConstants.INCIDENT_CLOSURECODE));
        this.setAffectedCI((String) params.get(HPSMConstants.INCIDENT_AFFECTEDCI));
        this.setAssignee((String) params.get(HPSMConstants.INCIDENT_ASSIGNEE));
        this.setContact((String) params.get(HPSMConstants.INCIDENT_CONTACT));

        this.setAlertStatus((String) params.get(HPSMConstants.INCIDENT_ALERTSTATUS));
        this.setContactLastName((String) params.get(HPSMConstants.INCIDENT_CONTACTLASTNAME));
        this.setContactFirstName((String) params.get(HPSMConstants.INCIDENT_CONTACTFIRSTNAME));
        this.setCompany((String) params.get(HPSMConstants.INCIDENT_COMPANY));
        this.setTitle((String) params.get(HPSMConstants.INCIDENT_TITLE));
        this.setTicketOwner((String) params.get(HPSMConstants.INCIDENT_TICKETOWNER));
        this.setUpdatedBy((String) params.get(HPSMConstants.INCIDENT_UPDATEDBY));
        this.setStatus((String) params.get(HPSMConstants.INCIDENT_STATUS));
        this.setArea((String) params.get(HPSMConstants.INCIDENT_AREA));
        this.setSLAAgreementID((BigDecimal) params.get(HPSMConstants.INCIDENT_SLAAGREEMENTID));
        this.setSiteCategory((String) params.get(HPSMConstants.INCIDENT_SITECATEGORY));
        this.setSubarea((String) params.get(HPSMConstants.INCIDENT_SUBAREA));
        this.setProblemType((String) params.get(HPSMConstants.INCIDENT_PROBLEMTYPE));
        this.setResolutionFixType((String) params.get(HPSMConstants.INCIDENT_RESOLUTIONFIXTYPE));
        this.setUserPriority((String) params.get(HPSMConstants.INCIDENT_USERPRIORITY));
        this.setLocation((String) params.get(HPSMConstants.INCIDENT_LOCATION));
        this.setImpact((String) params.get(HPSMConstants.INCIDENT_IMPACT));
        this.setFolder((String) params.get(HPSMConstants.INCIDENT_FOLDER));
        this.setService((String) params.get(HPSMConstants.INCIDENT_SERVICE));

        if (params.get(HPSMConstants.INCIDENT_DESCRIPTION) != null)
        {
            List<String> descriptions = (List<String>) params.get(HPSMConstants.INCIDENT_DESCRIPTION);
            this.setDescription(descriptions);
        }
        if (params.get(HPSMConstants.INCIDENT_SOLUTION) != null)
        {
            List<String> solutions = (List<String>) params.get(HPSMConstants.INCIDENT_SOLUTION);
            this.setSolution(solutions);
        }

        if (params.get(HPSMConstants.INCIDENT_JOURNALUPDATES) != null)
        {
            List<String> jurnalUpdates = (List<String>) params.get(HPSMConstants.INCIDENT_JOURNALUPDATES);
            this.setJournalUpdates(journalUpdates);
        }

        if (params.get(HPSMConstants.INCIDENT_EXPLANATION) != null)
        {
            List<String> explanations = (List<String>) params.get(HPSMConstants.INCIDENT_EXPLANATION);
            this.setExplanation(explanations);
        }
        
        //file attachments
    }

    //protected JAXBElement<AttachmentsType> attachments;
    public String getIncidentID()
    {
        return incidentID;
    }

    public void setIncidentID(String incidentID)
    {
        this.incidentID = incidentID;
    }

    public String getCategory()
    {
        return category;
    }

    public void setCategory(String category)
    {
        this.category = category;
    }

    public String getOpenTime()
    {
        return openTime;
    }

    public void setOpenTime(String openTime)
    {
        this.openTime = openTime;
    }

    public String getOpenedBy()
    {
        return openedBy;
    }

    public void setOpenedBy(String openedBy)
    {
        this.openedBy = openedBy;
    }

    public String getUrgency()
    {
        return urgency;
    }

    public void setUrgency(String urgency)
    {
        this.urgency = urgency;
    }

    public String getUpdatedTime()
    {
        return updatedTime;
    }

    public void setUpdatedTime(String updatedTime)
    {
        this.updatedTime = updatedTime;
    }

    public String getAssignmentGroup()
    {
        return assignmentGroup;
    }

    public void setAssignmentGroup(String assignmentGroup)
    {
        this.assignmentGroup = assignmentGroup;
    }

    public String getClosedTime()
    {
        return closedTime;
    }

    public void setClosedTime(String closedTime)
    {
        this.closedTime = closedTime;
    }

    public String getClosedBy()
    {
        return closedBy;
    }

    public void setClosedBy(String closedBy)
    {
        this.closedBy = closedBy;
    }

    public String getClosureCode()
    {
        return closureCode;
    }

    public void setClosureCode(String closureCode)
    {
        this.closureCode = closureCode;
    }

    public String getAffectedCI()
    {
        return affectedCI;
    }

    public void setAffectedCI(String affectedCI)
    {
        this.affectedCI = affectedCI;
    }

    public List<String> getDescription()
    {
        return description;
    }

    public void setDescription(List<String> description)
    {
        this.description = description;
    }

    public List<String> getSolution()
    {
        return solution;
    }

    public void setSolution(List<String> solution)
    {
        this.solution = solution;
    }

    public String getAssignee()
    {
        return assignee;
    }

    public void setAssignee(String assignee)
    {
        this.assignee = assignee;
    }

    public String getContact()
    {
        return contact;
    }

    public void setContact(String contact)
    {
        this.contact = contact;
    }

    public List<String> getJournalUpdates()
    {
        return journalUpdates;
    }

    public void setJournalUpdates(List<String> journalUpdates)
    {
        this.journalUpdates = journalUpdates;
    }

    public String getAlertStatus()
    {
        return alertStatus;
    }

    public void setAlertStatus(String alertStatus)
    {
        this.alertStatus = alertStatus;
    }

    public String getContactLastName()
    {
        return contactLastName;
    }

    public void setContactLastName(String contactLastName)
    {
        this.contactLastName = contactLastName;
    }

    public String getContactFirstName()
    {
        return contactFirstName;
    }

    public void setContactFirstName(String contactFirstName)
    {
        this.contactFirstName = contactFirstName;
    }

    public String getCompany()
    {
        return company;
    }

    public void setCompany(String company)
    {
        this.company = company;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTicketOwner()
    {
        return ticketOwner;
    }

    public void setTicketOwner(String ticketOwner)
    {
        this.ticketOwner = ticketOwner;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getArea()
    {
        return area;
    }

    public void setArea(String area)
    {
        this.area = area;
    }

    public BigDecimal getSLAAgreementID()
    {
        return SLAAgreementID;
    }

    public void setSLAAgreementID(BigDecimal SLAAgreementID)
    {
        this.SLAAgreementID = SLAAgreementID;
    }

    public String getSiteCategory()
    {
        return siteCategory;
    }

    public void setSiteCategory(String siteCategory)
    {
        this.siteCategory = siteCategory;
    }

    public String getSubarea()
    {
        return subarea;
    }

    public void setSubarea(String subarea)
    {
        this.subarea = subarea;
    }

    public String getProblemType()
    {
        return problemType;
    }

    public void setProblemType(String problemType)
    {
        this.problemType = problemType;
    }

    public String getResolutionFixType()
    {
        return resolutionFixType;
    }

    public void setResolutionFixType(String resolutionFixType)
    {
        this.resolutionFixType = resolutionFixType;
    }

    public String getUserPriority()
    {
        return userPriority;
    }

    public void setUserPriority(String userPriority)
    {
        this.userPriority = userPriority;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public List<String> getExplanation()
    {
        return explanation;
    }

    public void setExplanation(List<String> explanation)
    {
        this.explanation = explanation;
    }

    public String getImpact()
    {
        return impact;
    }

    public void setImpact(String impact)
    {
        this.impact = impact;
    }

    public String getFolder()
    {
        return folder;
    }

    public void setFolder(String folder)
    {
        this.folder = folder;
    }

    public String getService()
    {
        return service;
    }

    public void setService(String service)
    {
        this.service = service;
    }

    /**
     * Provides a {@link Map} representation of this Incident with all its value as String.
     *
     * @param uppercase if true then all the KEY's are going to be uppercase.
     * @return
     */
    public Map<String, String> toMap(boolean uppercase)
    {
        Map<String, String> result = new HashMap<String, String>();
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_ID), incidentID);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_CATEGORY), category);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_OPENTIME), openTime);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_OPENEDBY), openedBy);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_URGENCY), urgency);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_UPDATEDTIME), updatedTime);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_ASSIGNMENTGROUP), assignmentGroup);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_CLOSEDTIME), closedTime);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_CLOSEDBY), closedBy);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_CLOSURECODE), closureCode);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_AFFECTEDCI), affectedCI);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_DESCRIPTION), StringUtils.listToString(description));
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_SOLUTION), StringUtils.listToString(solution));
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_ASSIGNEE), assignee);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_CONTACT), contact);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_JOURNALUPDATES), StringUtils.listToString(journalUpdates));
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_ALERTSTATUS), alertStatus);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_CONTACTLASTNAME), contactLastName);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_CONTACTFIRSTNAME), contactFirstName);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_COMPANY), company);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_TITLE), title);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_TICKETOWNER), ticketOwner);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_UPDATEDBY), updatedBy);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_STATUS), status);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_AREA), area);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_SLAAGREEMENTID), ""+SLAAgreementID);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_SITECATEGORY), siteCategory);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_SUBAREA), subarea);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_PROBLEMTYPE), problemType);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_RESOLUTIONFIXTYPE), resolutionFixType);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_USERPRIORITY), userPriority);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_LOCATION), location);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_EXPLANATION), StringUtils.listToString(explanation));
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_IMPACT), impact);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_FOLDER), folder);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_SERVICE), service);
        return result;
    }

    /**
     * Provides a {@link Map} representation of this Incident.
     *
     * @return
     */
    public Map<String, Object> asMap()
    {
        return asMap(false);
    }

    /**
     * Provides a {@link Map} representation of this Incident.
     *
     * @param uppercase if true then all the KEY's are going to be uppercase.
     * @return
     */
    public Map<String, Object> asMap(boolean uppercase)
    {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_ID), incidentID);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_CATEGORY), category);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_OPENTIME), openTime);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_OPENEDBY), openedBy);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_URGENCY), urgency);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_UPDATEDTIME), updatedTime);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_ASSIGNMENTGROUP), assignmentGroup);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_CLOSEDTIME), closedTime);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_CLOSEDBY), closedBy);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_CLOSURECODE), closureCode);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_AFFECTEDCI), affectedCI);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_DESCRIPTION), description);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_SOLUTION), solution);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_ASSIGNEE), assignee);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_CONTACT), contact);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_JOURNALUPDATES), journalUpdates);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_ALERTSTATUS), alertStatus);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_CONTACTLASTNAME), contactLastName);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_CONTACTFIRSTNAME), contactFirstName);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_COMPANY), company);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_TITLE), title);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_TICKETOWNER), ticketOwner);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_UPDATEDBY), updatedBy);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_STATUS), status);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_AREA), area);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_SLAAGREEMENTID), SLAAgreementID);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_SITECATEGORY), siteCategory);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_SUBAREA), subarea);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_PROBLEMTYPE), problemType);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_RESOLUTIONFIXTYPE), resolutionFixType);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_USERPRIORITY), userPriority);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_LOCATION), location);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_EXPLANATION), explanation);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_IMPACT), impact);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_FOLDER), folder);
        result.put(getKey(uppercase, HPSMConstants.INCIDENT_SERVICE), service);
        return result;
    }

    private String getKey(boolean uppercase, String key)
    {
        if (uppercase)
        {
            key = key.toUpperCase();
        }
        return key;
    }
}
