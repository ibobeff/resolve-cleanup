package com.resolve.gateway.pushauth;

import java.io.IOException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpHeaders;
import org.eclipse.jetty.security.Authenticator;
import org.eclipse.jetty.security.LoginService;
import org.eclipse.jetty.security.ServerAuthException;
import org.eclipse.jetty.security.UserAuthentication;
import org.eclipse.jetty.security.authentication.BasicAuthenticator;
import org.eclipse.jetty.security.authentication.DeferredAuthentication;
import org.eclipse.jetty.server.Authentication;
import org.eclipse.jetty.server.UserIdentity;
import org.eclipse.jetty.util.B64Code;
import org.eclipse.jetty.util.StringUtil;

/**
 * This is a modified version of the code taken from Jetty (Apache license) to prefix the servlet path to the username when authenticating. 
 * 
 *
 */
public class BasicAuthenticatorEx extends BasicAuthenticator implements Authenticator
{
    
    protected final DeferredAuthentication _deferred=new DeferredAuthentication(this);

    /**
     * @see org.eclipse.jetty.security.Authenticator#validateRequest(javax.servlet.ServletRequest, javax.servlet.ServletResponse, boolean)
     */
    @Override
    public Authentication validateRequest(ServletRequest req, ServletResponse res, boolean mandatory) throws ServerAuthException
    {
        
        LoginService _loginService = getLoginService();
        
        HttpServletRequest request = (HttpServletRequest)req;
        HttpServletResponse response = (HttpServletResponse)res;
        String credentials = request.getHeader(HttpHeaders.AUTHORIZATION);

        try
        {
            if (!mandatory)
                return _deferred;
                
            if (credentials != null)
            {                 
                int space=credentials.indexOf(' ');
                if (space>0)
                {
                    String method=credentials.substring(0,space);
                    if ("basic".equalsIgnoreCase(method))
                    {
                        credentials = credentials.substring(space+1);
                        credentials = B64Code.decode(credentials,StringUtil.__ISO_8859_1);
                        int i = credentials.indexOf(':');
                        if (i>0)
                        {
                            String username = credentials.substring(0,i);
                            String password = credentials.substring(i+1);
                            //This is the altered line:
                            UserIdentity user = _loginService.login(PushAuthConfigUtil.flattenURI(request.getRequestURI()) +"\\" + username,password);
                            if (user!=null)
                            {
                                super.renewSession(request,response);
                                return new UserAuthentication(getAuthMethod(),user);
                            }
                        }
                    }
                }
            }

            if (DeferredAuthentication.isDeferred(response))
                return Authentication.UNAUTHENTICATED;
            
            response.setHeader(HttpHeaders.WWW_AUTHENTICATE, "basic realm=\"" + _loginService.getName() + '"');
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return Authentication.SEND_CONTINUE;
        }
        catch (IOException e)
        {
            throw new ServerAuthException(e);
        }
    }
}
