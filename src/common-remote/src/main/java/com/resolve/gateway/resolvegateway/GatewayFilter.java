package com.resolve.gateway.resolvegateway;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.resolve.gateway.BaseFilter;
import com.resolve.util.Constants;
import com.resolve.util.StringUtils;

public abstract class GatewayFilter extends BaseFilter
{
    public static final String DEPLOYED = "DEPLOYED";
    public static final String UPDATED = "UPDATED";
    
    protected Boolean deployed;
    protected Boolean updated;
    protected String queue;
    protected String gatewayName;
    protected String username;

    protected Map<String, Object> attributes = null;
    
    public GatewayFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script) {
        super(id, active, order, interval, eventEventId, runbook, script);
    }
    
    public Boolean isDeployed()
    {
        if(StringUtils.isBlank(queue))
            return false;
        
        if(queue.equals(Constants.DEFAULT_GATEWAY_QUEUE_NAME))
            return false;
        
        return true;
    }

    public void setDeployed(Boolean deployed)
    {
        this.deployed = deployed;
    }

    public Boolean isUpdated()
    {
        return updated;
    }

    public void setUpdated(Boolean updated)
    {
        this.updated = updated;
    }
    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }

    public String getGatewayName()
    {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName)
    {
        this.gatewayName = gatewayName;
    }
    
    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public Map<String, Object> getAttributes()
    {
        return attributes;
    }

    public void setAttributes(Map<String, Object> attributes)
    {
        this.attributes = attributes;
    }
    
    public Object getAttributeValue(String key, Map<String, Object> attrs) {
        
        Object value = null;
        
        if(StringUtils.isBlank(key) || attrs == null || attrs.size() == 0)
            return value;
        
        for(Iterator<String> it=attrs.keySet().iterator(); it.hasNext(); ) {
            String attr = it.next();
            if(attr.equals(key)) {
                value = (String)attrs.get(key);
                break;
            }
        }
        
        return value;
    }
    
    public void addAttributes(Map<String, Object> attributes) {
        
        if(this.attributes == null)
            this.attributes = new HashMap<String, Object>();
        
        for(String attribute:attributes.keySet()) {
            if(attribute.equals("QUEUE") || attribute.equals("EVENT_EVENTID") || attribute.equals("GATEWAY_NAME"))
                continue;
            
            Method[] methods = this.getClass().getMethods();
            boolean isAttr = true;
            for(Method method:methods) {
                String name = method.getName().toUpperCase();
                
                if(name.indexOf(attribute) != -1) {
                    isAttr = false;
                    break;
                }
            }
            
            if(isAttr)
                this.attributes.put(attribute, attributes.get(attribute));
        }
    }
    
} // class GatewayFilter
