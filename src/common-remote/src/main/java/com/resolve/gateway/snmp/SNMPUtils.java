/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.snmp;

import java.util.List;

import org.snmp4j.PDU;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.BitString;
import org.snmp4j.smi.Counter32;
import org.snmp4j.smi.Counter64;
import org.snmp4j.smi.Gauge32;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.IpAddress;
import org.snmp4j.smi.Null;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.Opaque;
import org.snmp4j.smi.TimeTicks;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.UnsignedInteger32;
import org.snmp4j.smi.Variable;
import org.snmp4j.smi.VariableBinding;

import com.resolve.gateway.SNMPData;
import com.resolve.gateway.SNMPResponse;
import com.resolve.util.Log;

public class SNMPUtils
{
    // The snmp trap OID is the second varbind
    private static final int SNMP_TRAP_OID_INDEX = 1;

    /**
     * SNMP {@link Counter32} value type
     */
    public static final int VALUE_TYPE_COUNTER32 = 1;
    /**
     * SNMP {@link Counter64} value type
     */
    public static final int VALUE_TYPE_COUNTER64 = 2;
    /**
     * SNMP {@link Gauge32} value type
     */
    public static final int VALUE_TYPE_GAUGE32 = 3;
    /**
     * SNMP {@link Integer32} value type
     */
    public static final int VALUE_TYPE_INTEGER32 = 5;
    /**
     * SNMP {@link IpAddress} value type
     */
    public static final int VALUE_TYPE_IPADDRESS = 6;
    /**
     * SNMP {@link Null} value type
     */
    public static final int VALUE_TYPE_NULL = 7;
    /**
     * SNMP {@link OID} value type
     */
    public static final int VALUE_TYPE_OBJECT_IDENTIFIER = 8;
    /**
     * SNMP {@link OctetString} value type
     */
    public static final int VALUE_TYPE_OCTET_STRING = 9;
    /**
     * SNMP {@link Opaque} value type
     */
    public static final int VALUE_TYPE_OPAQUE = 10;
    /**
     * SNMP {@link TimeTicks} value type
     */
    public static final int VALUE_TYPE_TIMETICKS = 11;
    /**
     * SNMP {@link UnsignedInteger32} value type
     */
    public static final int VALUE_TYPE_UNSIGNED_INTEGER32 = 12;

    public static Address getAddress(String host, int port)
    {
        return new UdpAddress(host + "/" + port);
    }

    /**
     * Get Address object from the host and port.
     * 
     * @param hostWithPort
     *            - like 10.20.2.123/161
     * @return
     */
    public static Address getAddress(String hostWithPort)
    {
        return new UdpAddress(hostWithPort);
    }

    /**
     * This method gets the SNMP Trap OID from a trap.
     * 
     * @param trap
     * @return
     */
    public static String getSnmpTrapOID(PDU trap)
    {
        String result = null;

        try
        {
            VariableBinding binding = trap.get(SNMP_TRAP_OID_INDEX);
            if (binding != null)
            {
                result = binding.getVariable().toString();
                // result = snmpTrapOid.toString();
            }
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            // well, snmp trap id not there, ignore it
        }
        return result;
    }

    public static SNMPResponse getSnmpResponse(Address peerAddress, String snmpTrapOid, VariableBinding binding)
    {
        SNMPResponse result = null;

        if (binding != null)
        {
            Long numericValue = null;
            String stringValue = null;

            OID oid = binding.getOid();
            Variable variable = binding.getVariable();
            stringValue = variable.toString();
            try
            {
                numericValue = variable.toLong();
            }
            catch (UnsupportedOperationException e)
            {
                // No problem, it's not a numeric type.
            }

            result = new SNMPResponse(peerAddress.toString(), snmpTrapOid, oid.toString(), numericValue, stringValue);
        }

        return result;
    }

    public static SNMPResponse get(int version, String deviceIpWithPort, String readCommunity, int retries, long timeout, String oidValue)
    {
        SNMPResponse result = null;
        Address address = getAddress(deviceIpWithPort);
        PDU trap = null;
        
        if (version == SnmpConstants.version1)
        {
            trap = SNMPV1Utils.get(address, readCommunity, retries, timeout, oidValue);
        }
        else if (version == SnmpConstants.version2c)
        {
            trap = SNMPV2cUtils.get(address, readCommunity, retries, timeout, oidValue);
        }
        else if (version == SnmpConstants.version3)
        {
            trap = SNMPV3Utils.get(address, readCommunity, retries, timeout, oidValue);
        }
        
        if (trap != null)
        {
            String snmpTrapOid = getSnmpTrapOID(trap);
            VariableBinding variable = (VariableBinding) trap.getVariableBindings().firstElement();
            result = getSnmpResponse(address, snmpTrapOid, variable);
        }
        return result;
    }

    public static SNMPResponse get(int version, String deviceIp, int port, String readCommunity, int retries, long timeout, String oidValue)
    {
        return get(version, deviceIp + "/" + port, readCommunity, retries, timeout, oidValue);
    }

    public static SNMPResponse getNext(int version, String deviceIpWithPort, String readCommunity, int retries, long timeout, String oid)
    {
        SNMPResponse result = null;
        Address address = getAddress(deviceIpWithPort);
        PDU trap = null;
        if (version == SnmpConstants.version1)
        {
            trap = SNMPV1Utils.getNext(address, readCommunity, retries, timeout, oid);
        }
        else if (version == SnmpConstants.version2c)
        {
            trap = SNMPV2cUtils.getNext(address, readCommunity, retries, timeout, oid);
        }
        else if (version == SnmpConstants.version3)
        {
            trap = SNMPV3Utils.getNext(address, readCommunity, retries, timeout, oid);
        }
        if (trap != null)
        {
            String snmpTrapOid = getSnmpTrapOID(trap);
            VariableBinding variable = (VariableBinding) trap.getVariableBindings().firstElement();
            result = getSnmpResponse(address, snmpTrapOid, variable);
        }
        return result;
    }

    public static SNMPResponse getNext(int version, String deviceIp, int port, String readCommunity, int retries, long timeout, String oid)
    {
        return getNext(version, deviceIp + "/" + port, readCommunity, retries, timeout, oid);
    }

    public static boolean set(int version, String deviceIp, int port, String writeCommunity, int retries, long timeout, String oid, String oidValue, String valueType) throws Exception
    {
        boolean result = true;
        Address address = getAddress(deviceIp, port);
        if (version == SnmpConstants.version1)
        {
            SNMPV1Utils.set(address, writeCommunity, retries, timeout, oid, oidValue, valueType);
        }
        else if (version == SnmpConstants.version2c)
        {
            SNMPV2cUtils.set(address, writeCommunity, retries, timeout, oid, oidValue, valueType);
        }
        else if (version == SnmpConstants.version3)
        {
            SNMPV3Utils.set(address, writeCommunity, retries, timeout, oid, oidValue, valueType);
        }
        return result;
    }

    /**
     * Sends Snmp trap to a device.
     * 
     * @param version
     * @param deviceIp
     * @param port
     * @param retries
     * @param timeout
     * @param writeCommunity
     * @param snmpTrapOid
     * @param oids
     *            is a Map with OID and their values. Currently we attempt to
     *            check the data type of the value and only support Integer32
     *            and OctetString
     * @return
     * @throws Exception
     */
    public static boolean sendTrap(int version, String deviceIp, int port, int retries, long timeout, String writeCommunity, String snmpTrapOid, List<SNMPData> traps) throws Exception
    {
        boolean result = false;

        if (traps != null && traps.size() > 0)
        {
            if (version == SnmpConstants.version1)
            {
                result = SNMPV1Utils.sendTrap(deviceIp, port, retries, timeout, writeCommunity, snmpTrapOid, traps);
            }
            else if (version == SnmpConstants.version2c)
            {
                result = SNMPV2cUtils.sendTrap(deviceIp, port, retries, timeout, writeCommunity, snmpTrapOid, traps);
            }
            else if (version == SnmpConstants.version3)
            {
                result = SNMPV3Utils.sendTrap(deviceIp, port, retries, timeout, writeCommunity, snmpTrapOid, traps);
            }
        }
        else
        {
            Log.log.info("There is no traps to send");
        }
        return result;
    }

    /**
     * TODO we should use reflection, may be later.
     * 
     * @param pdu
     * @param oid
     * @param oidValue
     * @param oidValueDataType
     */
    static void addValue(PDU pdu, String oid, String oidValue, String oidValueDataType)
    {
        if (SNMPData.Type.BitString.equalsIgnoreCase(oidValueDataType))
        {
            BitString bit = new BitString();
            bit.setValue(oidValue);
            pdu.add(new VariableBinding(new OID(oid), bit));
        }
        else if (SNMPData.Type.Integer32.equalsIgnoreCase(oidValueDataType))
        {
            pdu.add(new VariableBinding(new OID(oid), new Integer32(Integer.valueOf(oidValue))));
        }
        else if (SNMPData.Type.OctetString.equalsIgnoreCase(oidValueDataType))
        {
            pdu.add(new VariableBinding(new OID(oid), new OctetString(oidValue)));
        }
        else if (SNMPData.Type.OID.equalsIgnoreCase(oidValueDataType))
        {
            pdu.add(new VariableBinding(new OID(oid), new OID(oidValue)));
        }
        else if (SNMPData.Type.IpAddress.equalsIgnoreCase(oidValueDataType))
        {
            pdu.add(new VariableBinding(new OID(oid), new IpAddress(oidValue)));
        }
        else if (SNMPData.Type.Counter32.equalsIgnoreCase(oidValueDataType))
        {
            pdu.add(new VariableBinding(new OID(oid), new Counter32(Long.valueOf(oidValue))));
        }
        else if (SNMPData.Type.Counter64.equalsIgnoreCase(oidValueDataType))
        {
            pdu.add(new VariableBinding(new OID(oid), new Counter64(Long.valueOf(oidValue))));
        }
        else if (SNMPData.Type.Gauge32.equalsIgnoreCase(oidValueDataType))
        {
            pdu.add(new VariableBinding(new OID(oid), new Gauge32(Long.valueOf(oidValue))));
        }
        else if (SNMPData.Type.Null.equalsIgnoreCase(oidValueDataType))
        {
            pdu.add(new VariableBinding(new OID(oid), new Null()));
        }
        else if (SNMPData.Type.Unsigned32.equalsIgnoreCase(oidValueDataType))
        {
            pdu.add(new VariableBinding(new OID(oid), new UnsignedInteger32(Long.valueOf(oidValue))));
        }
        else if (SNMPData.Type.TimeTicks.equalsIgnoreCase(oidValueDataType))
        {
            pdu.add(new VariableBinding(new OID(oid), new TimeTicks(Long.valueOf(oidValue))));
        }
        else if (SNMPData.Type.Opaque.equalsIgnoreCase(oidValueDataType))
        {
            pdu.add(new VariableBinding(new OID(oid), new Opaque(oidValue.getBytes())));
        }
    }

    static VariableBinding getVariableBinding(String oid, String value, String valueType) throws Exception
    {
        VariableBinding result = null;
        try
        {
            if (SNMPData.Type.Null.equals(value) || value != null)
            {
                if (valueType.equals(SNMPData.Type.Counter32))
                {
                    result = new VariableBinding(new OID(oid), new Counter32(Long.valueOf(value)));
                }
                else if (valueType.equals(SNMPData.Type.Counter64))
                {
                    result = new VariableBinding(new OID(oid), new Counter64(Long.valueOf(value)));
                }
                else if (valueType.equals(SNMPData.Type.Gauge32))
                {
                    result = new VariableBinding(new OID(oid), new Gauge32(Long.valueOf(value)));
                }
                else if (valueType.equals(SNMPData.Type.Integer32))
                {
                    result = new VariableBinding(new OID(oid), new Integer32(Integer.valueOf(value)));
                }
                else if (valueType.equals(SNMPData.Type.IpAddress))
                {
                    result = new VariableBinding(new OID(oid), new IpAddress(value));
                }
                else if (valueType.equals(SNMPData.Type.Null))
                {
                    result = new VariableBinding(new OID(oid), new Null());
                }
                else if (valueType.equals(SNMPData.Type.OID))
                {
                    result = new VariableBinding(new OID(oid), new OID(value));
                }
                else if (valueType.equals(SNMPData.Type.OctetString))
                {
                    result = new VariableBinding(new OID(oid), new OctetString(value));
                }
                else if (valueType.equals(SNMPData.Type.Opaque))
                {
                    result = new VariableBinding(new OID(oid), new Opaque(value.getBytes()));
                }
                else if (valueType.equals(SNMPData.Type.TimeTicks))
                {
                    result = new VariableBinding(new OID(oid), new TimeTicks(Long.valueOf(value)));
                }
                else if (valueType.equals(SNMPData.Type.Unsigned32))
                {
                    result = new VariableBinding(new OID(oid), new UnsignedInteger32(Long.valueOf(value)));
                }
                else
                {
                    throw new Exception("Unsupported value type: " + valueType);
                }
            }
        }
        catch (Exception e)
        {
            throw new Exception("Error converting value: " + value + " to the specified type. " + e.getMessage(), e);
        }
        return result;
    }

//    public static void main(String[] args)
//    {
//        Log.init();
//
//        try
//        {
//            // sendTrap(2, "localhost", 162, 2, 3000, "public",
//            // SnmpConstants.coldStart.toString(),
//            // ".1.3.6.1.2.1.47.1.2.1.1.2.1", "Some string", "OCTET STRING");
//            // String coldStart = SnmpConstants.coldStart.toString();
//            // System.out.println(coldStart);
//
//            String host = "10.30.10.12";
//            int port = 161;
//            String readCommunity = "public";
//            int retries = 2;
//            long timeout = 2000; // milliseconds
//            String oid = "1.3.6.1.2.1.1.1.0";
//            int startIndex = (oid.startsWith(".") ? 1 : 0);
//            String baseOid = oid.substring(startIndex, oid.length() - 1);
//            String tmpOid = oid.replace("*", "0");
//
//            while (true)
//            {
//                SNMPResponse binding = getNext(SnmpConstants.version2c, host, port, readCommunity, retries, timeout, tmpOid);
//                System.out.printf("The Response = %s \n", binding.toString());
//                if (binding.getOid().startsWith(baseOid))
//                {
//                    System.out.printf("The Response = %s \n", binding.toString());
//                }
//                else
//                {
//                    break;
//                }
//                tmpOid = binding.getOid();
//            }
//
//            SNMPResponse binding = get(SnmpConstants.version2c, host, port, readCommunity, retries, timeout, ".1.3.6.1.2.1.1.4.0");
//            System.out.println("##### " + binding.toString());
//            set(SnmpConstants.version2c, host, port, readCommunity, retries, timeout, ".1.3.6.1.2.1.1.4.0", "Set by Bipul1", SNMPData.Type.OctetString);
//
//            binding = get(SnmpConstants.version2c, host, port, readCommunity, retries, timeout, ".1.3.6.1.2.1.1.4.0");
//            System.out.println(">>>>> " + binding.toString());
//        }
//        catch (Exception e)
//        {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//    }
}
