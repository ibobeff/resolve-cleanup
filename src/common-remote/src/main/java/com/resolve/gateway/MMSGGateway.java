package com.resolve.gateway;

import java.util.Map;

import com.resolve.gateway.Filter;
import com.resolve.gateway.MGateway;
import com.resolve.gateway.resolvegateway.msg.MSGGateway;
import com.resolve.gateway.resolvegateway.msg.MSGGatewayFilter;

public class MMSGGateway extends MGateway {

    private static final String RSCONTROL_SET_FILTERS = "MMSGGateway.setFilters";

    private static final MSGGateway instance = MSGGateway.getInstance();

    public MMSGGateway() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link MSGGatewayFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        
        if (instance.isActive()) {
            MSGGatewayFilter msgFilter = (MSGGatewayFilter) filter;
            
            filterMap.put(MSGGatewayFilter.USER, msgFilter.getUser());
            filterMap.put(MSGGatewayFilter.PASS, msgFilter.getPass());
            filterMap.put(MSGGatewayFilter.SSL, "" + msgFilter.getSsl());
        }
    }
}