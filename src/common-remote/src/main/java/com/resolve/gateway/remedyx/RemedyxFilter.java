/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.remedyx;

import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.RetainValue;

public class RemedyxFilter extends BaseFilter
{
    // Additional field names for this filter.
    public static final String QUERY = "QUERY";
    public static final String FORM_NAME = "FORM_NAME";
    public static final String LAST_VALUE_FIELD = "LAST_VALUE_FIELD";
    public static final String LAST_VALUE = "LAST_VALUE";
    public static final String LAST_ENTRY_ID = "LAST_ENTRY_ID";
    public static final String LIMIT = "LIMIT";

    private String formName;
    private String query;
    private String lastValueField;

    private String nativeQuery; // stores the native query after translation by
                                // Resolve Query translator.
    private String lastValue = "0";
    private String lastEntryId = "0";
    private int[] fieldIds;
    
    private int limit = 0;

    public RemedyxFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String formName, String query, String lastValueField, Integer limit)
    {
        super(id, active, order, interval, eventEventId, runbook, script);
        setFormName(formName);
        setQuery(query);
        setLastValueField(lastValueField);
        setLimit(limit);
    } // RemedyFilter

    public String getFormName()
    {
        return formName;
    }

    public void setFormName(String formName)
    {
        this.formName = formName != null ? formName.trim() : formName;
    }

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query != null ? query.trim() : query;
    }

    public String getLastValueField()
    {
        return lastValueField;
    }

    public void setLastValueField(String lastValueField)
    {
        this.lastValueField = lastValueField != null ? lastValueField.trim() : lastValueField;
    }

    @RetainValue
    public String getLastValue()
    {
        return lastValue;
    }

    public void setLastValue(String lastValue)
    {
        this.lastValue = lastValue != null ? lastValue.trim() : lastValue;
    }

    public String getNativeQuery()
    {
        return nativeQuery;
    }

    public void setNativeQuery(String nativeQuery)
    {
        this.nativeQuery = nativeQuery != null ? nativeQuery.trim() : nativeQuery;
    }

    @RetainValue
    public String getLastEntryId()
    {
        return lastEntryId;
    }

    public void setLastEntryId(String lastEntryId)
    {
        this.lastEntryId = lastEntryId != null ? lastEntryId.trim() : lastEntryId;
    }

    public int[] getFieldIds()
    {
        return fieldIds;
    }

    public void setFieldIds(int[] fieldIds)
    {
        this.fieldIds = fieldIds;
    }

	public int getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit != null ? limit : 0;
	}
}
