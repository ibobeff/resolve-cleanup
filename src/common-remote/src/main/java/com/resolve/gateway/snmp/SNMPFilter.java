/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.snmp;

import java.util.ArrayList;
import java.util.List;

import com.resolve.gateway.BaseFilter;
import com.resolve.util.StringUtils;

public class SNMPFilter extends BaseFilter
{
    // Additional field names for this filter.
    public static final String SNMP_VERSION = "SNMP_VERSION";
    public static final String IP_ADDRESSES = "IP_ADDRESSES";
    public static final String READ_COMMUNITY = "READ_COMMUNITY";
    public static final String TIMEOUT = "TIMEOUT";
    public static final String RETRIES = "RETRIES";
    public static final String TRAP_RECEIVER = "TRAP_RECEIVER";
    public static final String SNMP_TRAP_OID = "SNMP_TRAP_OID";
    public static final String OID = "OID";
    public static final String REGEX = "REGEX";
    public static final String COMPARATOR = "COMPARATOR";
    public static final String VALUE = "VALUE";

    private Integer snmpVersion;
    private String ipAddresses;
    private String readCommunity;
    private Integer timeout;
    private Integer retries;
    private boolean trapReceiver;
    private String snmpTrapOid;
    private String oid;
    private String regex;
    private String comparator; // numeric comparator like !=, >, >=, =, <=, <
    private Integer value;

    // stores devices IP with port (ip/port) as the key and a boolean as value
    // to
    // indicate if the filter passed or failed so we can decided when is the
    // next
    // time to .
    private List<String> devices;
    private long timeoutInMillis = 5000;

    public SNMPFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String snmpVersion, String ipAddresses, String readCommunity, String timeout, String retries, String trapReceiver, String snmpTrapOid, String oid, String regex, String comparator, String value)
    {
        super(id, active, order, interval, eventEventId, runbook, script);
        if (StringUtils.isNotBlank(snmpVersion))
        {
            this.snmpVersion = Integer.parseInt(snmpVersion.trim());
        }
        setIpAddresses(ipAddresses);
        populateDevices(ipAddresses);

        setReadCommunity(readCommunity);
        if (StringUtils.isNotBlank(timeout))
        {
            this.timeout = Integer.parseInt(timeout.trim());
        }
        if (StringUtils.isNotBlank(retries))
        {
            this.retries = Integer.parseInt(retries.trim());
        }
        if (trapReceiver != null && trapReceiver.equalsIgnoreCase("TRUE"))
        {
            this.trapReceiver = true;
        }
        setSnmpTrapOid(snmpTrapOid);
        setOid(oid);
        setRegex(regex);
        setComparator(comparator);
        if (StringUtils.isNotBlank(value ))
        {
            this.value = Integer.parseInt(value);
        }
    }

    public Integer getSnmpVersion()
    {
        return snmpVersion;
    }

    public void setSnmpVersion(Integer snmpVersion)
    {
        this.snmpVersion = snmpVersion;
    }

    public String getIpAddresses()
    {
        return ipAddresses;
    }

    public void setIpAddresses(String ipAddresses)
    {
        this.ipAddresses = ipAddresses != null ? ipAddresses.trim() : ipAddresses;
        populateDevices(ipAddresses != null ? ipAddresses.trim() : ipAddresses);
    }

    private void populateDevices(String ipAddresses)
    {
        // Expect the ipAddresses in the following format:
        // 123.23.23.12:162, 10.10.2.3, 10.20.2.34:163
        // if the ip doesn't have port defined then assign 161 (default snmp
        // port)
        if (StringUtils.isNotEmpty(ipAddresses))
        {
            devices = new ArrayList<String>();
            for (String ipAddress : ipAddresses.split(","))
            {
                String[] splitted = ipAddress.split(":");
                if (splitted.length == 2 && (StringUtils.isNotEmpty(splitted[1])))
                {
                    devices.add(splitted[0].trim() + "/" + splitted[1].trim());
                }
                else
                {
                    devices.add(splitted[0].trim() + "/161");
                }
            }
        }
    }

    public String getReadCommunity()
    {
        return readCommunity;
    }

    public void setReadCommunity(String readCommunity)
    {
        this.readCommunity = readCommunity != null ? readCommunity.trim() : readCommunity;
    }

    public Integer getTimeout()
    {
        return timeout;
    }

    public void setTimeout(Integer timeout)
    {
        this.timeout = timeout;
    }

    public long getTimeoutInMillis()
    {
        if (timeout != null)
        {
            timeoutInMillis = new Long(timeout) * 1000;
        }
        return timeoutInMillis;
    }

    public Integer getRetries()
    {
        return retries;
    }

    public void setRetries(Integer retries)
    {
        this.retries = retries;
    }

    public boolean isTrapReceiver()
    {
        return trapReceiver;
    }

    public void setTrapReceiver(Boolean trapReceiver)
    {
        this.trapReceiver = trapReceiver;
    }

    public String getSnmpTrapOid()
    {
        return snmpTrapOid;
    }

    public void setSnmpTrapOid(String trapId)
    {
        this.snmpTrapOid = trapId != null ? trapId.trim() : trapId;
    }

    public String getOid()
    {
        return oid;
    }

    public void setOid(String oid)
    {
        this.oid = oid != null ? oid.trim() : oid;
    }

    public String getRegex()
    {
        return regex;
    }

    public void setRegex(String regex)
    {
        this.regex = regex != null ? regex.trim() : regex;
    }

    public String getComparator()
    {
        return comparator;
    }

    public void setComparator(String comparator)
    {
        this.comparator = comparator != null ? comparator.trim() : comparator;
    }

    public Integer getValue()
    {
        return value;
    }

    public void setValue(Integer value)
    {
        this.value = value;
    }

    public List<String> getDevices()
    {
        return devices;
    }
}
