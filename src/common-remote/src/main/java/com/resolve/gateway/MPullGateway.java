package com.resolve.gateway;

import java.util.Iterator;
import java.util.Map;

import com.resolve.gateway.Filter;
import com.resolve.gateway.MGateway;
import com.resolve.gateway.resolvegateway.pull.PullGateway;
import com.resolve.gateway.resolvegateway.pull.PullGatewayFilter;

public class MPullGateway extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MPullGateway.setFilters";

    private static final PullGateway instance = PullGateway.getInstance();

    public MPullGateway() {
        super.instance = instance;
    }

    protected String getSetFilterMethodName() {
        return RSCONTROL_SET_FILTERS;
    }
    
    public PullGateway getInstance(String gatewayName) {
        
        return PullGateway.getInstance(gatewayName);
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link QRadarPullFilter} instance
     *            
     */
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter) {
        
        if (instance.isActive()) {
            PullGatewayFilter pullFilter = (PullGatewayFilter) filter;
            
            filterMap.put(PullGatewayFilter.QUERY, pullFilter.getQuery());
            filterMap.put(PullGatewayFilter.TIME_RANGE, pullFilter.getTimeRange());
            filterMap.put(PullGatewayFilter.LAST_ID, pullFilter.getLastId());
            filterMap.put(PullGatewayFilter.LAST_TIMESTAMP, pullFilter.getLastTimestamp());
            
            Map<String, Object> map = pullFilter.getAttributes();
            for(Iterator<String> it=map.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                filterMap.put(key, (String)map.get(key));
            }
        }
    }

} // class MPullGateway
