/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.Map;
import java.util.regex.Matcher;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * Base filter class that holds most common properties for all the filter.
 */
public class BaseFilter implements Filter
{
    // Field names for this filter used by TSRM Gateway related classes.
    public static final String ID = "ID";
    public static final String ACTIVE = "ACTIVE";
    public static final String ORDER = "ORDER";
    public static final String INTERVAL = "INTERVAL";
    public static final String EVENT_EVENTID = "EVENT_EVENTID";
    // public static final String PERSISTENT_EVENT = "PERSISTENT_EVENT";
    public static final String RUNBOOK = "RUNBOOK";
    public static final String SCRIPT = "SCRIPT";
    public static final String QUEUE = "QUEUE";

    /* The value is used by some gateway filter (remedyx) in its query as follows:
     * '7'=1 AND '5'<>"${GATEWAY_USER}"
     * In this case system will replace the ${GATEWAY_USER} with the username 
     * provided in the config.xml for remedyx gateway. This is here in the base class 
     * because we may use this in some other gateways as well.
     */
    public static final String GATEWAY_USER = "GATEWAY_USER";

    protected String id;
    protected boolean active;
    protected int order = 1;
    protected int interval = 10;
    protected String eventEventId;
    // protected boolean persistentEvent;
    protected String runbook;
    protected String script;

    private long lastExecutionTime = 0;
    
    private String nativeQuery; // stores the native query after translation by
    // Resolve Query translator.

    public BaseFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script)
    {
        /*
        this.id = id.trim();
        this.eventEventId = eventEventId.trim();
        // this.persistentEvent = false;

        this.runbook = runbook.trim();
        if (StringUtils.isNotEmpty(order.trim()))
        {
            this.order = Integer.parseInt(order.trim());
        }
        if (StringUtils.isNotEmpty(interval.trim()))
        {
            this.interval = Integer.parseInt(interval.trim());
        }
        this.active = true;
        if (active != null && active.trim().equalsIgnoreCase("FALSE"))
        {
            this.active = false;
        }
        this.script = script.trim();
        */
        setId(id);
        setEventEventId(eventEventId);
        setRunbook(runbook);
        setOrder(order);
        setInterval(interval);
        setActive(active);
        setScript(script);
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        if (id != null && StringUtils.isNotBlank(id.trim()))
        {
            this.id = id.trim();
        }
        else
        {
            Log.log.error("Error setting filter id to [" + id.trim() + "]");
            //throw new Exception ("Error setting filter id to [" + id.trim() + "]");
        }
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public void setActive(String active)
    {
        if (active != null && active.trim().equalsIgnoreCase("FALSE"))
        {
            this.active = false;
        }
        else
        {
            this.active = true;
        }
    } // setActive

    public int getOrder()
    {
        return order;
    }

    public void setOrder(String order)
    {
        if (order != null && StringUtils.isNotBlank(order.trim()))
        {
            try
            {
                this.order = Integer.parseInt(order.trim());
            }
            catch(Exception e)
            {
                Log.log.info("Error " + e.getMessage() + " occurred while setting filter order to " + order.trim() + ", defaulting to 1.", e);
            }
        }
    }

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public void setInterval(String interval)
    {
        if (interval != null && StringUtils.isNotBlank(interval.trim()))
        {
            try
            {
                this.interval = Integer.parseInt(interval.trim());
            }
            catch(Exception e)
            {
                Log.log.info("Error " + e.getMessage() + " occurred while setting filter interval to " + interval.trim() + ", defaulting to 10 seconds.", e);
            }
        }
    }
    
    @Override
    public String getEventEventId()
    {
        return eventEventId;
    }

    @Override
    public void setEventEventId(String eventEventId)
    {
        this.eventEventId = eventEventId != null ? eventEventId.trim() : eventEventId;
    }

    @Override
    public String getRunbook()
    {
        return runbook;
    }

    @Override
    public void setRunbook(String runbook)
    {
        this.runbook = runbook != null ? runbook.trim() : runbook;
    }

    public boolean hasRunbook()
    {
        // Oracle DB returns "undefined" for the null value so this comparison is necessary.
        return StringUtils.isNotBlank(runbook) && !runbook.equalsIgnoreCase("undefined");
    }

    public boolean hasEvent()
    {
        // Oracle DB returns "undefined" for the null value so this comparison is necessary.
        return StringUtils.isNotBlank(eventEventId) && !eventEventId.equalsIgnoreCase("undefined");
    }

    public String getScript()
    {
        return script;
    }

    public void setScript(String s)
    {
        this.script = s != null ? s.trim() : s;
    }

    public boolean hasGroovyScript()
    {
        return StringUtils.isNotBlank(script);
    }

    @RetainValue
    public long getLastExecutionTime()
    {
        return lastExecutionTime;
    }

    public void setLastExecutionTime(long lastExecutionTime)
    {
        this.lastExecutionTime = lastExecutionTime;
    }

    public String getNativeQuery()
    {
        return nativeQuery;
    }

    public void setNativeQuery(String nativeQuery)
    {
        this.nativeQuery = nativeQuery != null ? nativeQuery.trim() : nativeQuery;
    }

    public int compareTo(Filter compareObj)
    {
        int result = 0;

        if (this.order < compareObj.getOrder())
        {
            result = -1;
        }
        else if (this.order == compareObj.getOrder())
        {
            result = 0;
        }
        else if (this.order > compareObj.getOrder())
        {
            result = 1;
        }

        return result;
    } // compareTo

    @Override
    public Matcher getMatcher(Map<String, String> params)
    {
        //subclasses like XMPPFilter will implement this
        return null;
    }
    
    @Override
    public String toString()
    {
        return "name=" + id + ", active=" + active + ", order=" + order + ", interval=" + interval + ", eventEventId=" + eventEventId + ", runbook=" + runbook + ", script=" + script;
    }

    @Override
    public int getBlockingCode()
    {
        return 2;
    }
} // BaseFilter
