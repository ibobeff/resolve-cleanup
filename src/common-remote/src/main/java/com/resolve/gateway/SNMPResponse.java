/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

/**
 * This class stores the response from an Snmp operation like GET, GETNEXT or
 * TRAP receiver.
 */
public class SNMPResponse
{
    private String peerAddress;
    private String snmpTrapOid;
    private String oid;
    private Long numericValue;
    private String stringValue;

    /*
     * public SNMPResponse() { //default constructor }
     */
    public SNMPResponse(String peerAddress, String snmpTrapOid, String oid, Long numericValue, String stringValue)
    {
        super();
        this.peerAddress = peerAddress;
        this.snmpTrapOid = snmpTrapOid;
        this.oid = oid;
        this.numericValue = numericValue;
        this.stringValue = stringValue;
    }

    public String getPeerAddress()
    {
        return peerAddress;
    }

    public void setPeerAddress(String peerAddress)
    {
        this.peerAddress = peerAddress;
    }

    public String getSnmpTrapOid()
    {
        return snmpTrapOid;
    }

    public void setSnmpTrapOid(String snmpTrapOid)
    {
        this.snmpTrapOid = snmpTrapOid;
    }

    public String getOid()
    {
        return oid;
    }

    public void setOid(String oid)
    {
        this.oid = oid;
    }

    public Long getNumericValue()
    {
        return numericValue;
    }

    public void setNumericValue(Long numericValue)
    {
        this.numericValue = numericValue;
    }

    public String getStringValue()
    {
        return stringValue;
    }

    public void setStringValue(String stringValue)
    {
        this.stringValue = stringValue;
    }

    @Override
    public String toString()
    {
        return "SnmpResponse [trapId=" + snmpTrapOid + ", oid=" + oid + ", numericValue=" + numericValue + ", stringValue=" + stringValue + "]";
    }
}
