package com.resolve.gateway.resolvegateway;

import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.XDoc;

public abstract class ConfigReceiveResolveGateway extends ConfigReceiveGateway
{
    protected String rootNode = "";
    
    public ConfigReceiveResolveGateway(XDoc configDoc) throws Exception {
        super(configDoc);
    }
    
    public ConfigReceiveResolveGateway(XDoc configDoc, String gatewayName) throws Exception {
        super(configDoc, gatewayName);
    }
       
    public String getRootNode()
    {
        return rootNode;
    }

    public void setRootNode(String rootNode)
    {
        this.rootNode = rootNode;
    }
    
    public void load() throws Exception {
        ;
    }
    
    public void save() throws Exception {
        ;
    }

} // class ConfigReceiveResolveGateway
