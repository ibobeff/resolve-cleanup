/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.database;

import com.resolve.rsremote.ConfigReceiveDB;

public class DBGatewayFactory
{
    public static final DBGateway getInstance(ConfigReceiveDB configReceiveDB)
    {
        DBGateway dbGateway = null;
        if(configReceiveDB.isActive())
        {
            if(configReceiveDB.isMock())
            {
                dbGateway = MockDBGateway.getInstance(configReceiveDB);
            }
            else
            {
                dbGateway = DBGateway.getInstance(configReceiveDB);
            }
        }
        return dbGateway;
    }

    public static final DBGateway getInstance()
    {
    	// Temporary comment the mock lines out as the Main reference cannot be used here in this way. Also
    	// it seems that the mock never becomes true in the current code-base
//        boolean isMock = ((Main) MainBase.main).getConfigReceiveDB().isMock();
//        if(isMock)
//        {
//            return MockDBGateway.getInstance();
//        }
//        else
//        {
            return DBGateway.getInstance();
//        }
    }
}
