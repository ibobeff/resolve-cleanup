package com.resolve.gateway.tibcobespoke;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class TIBCOBespokeProcess
{
    private String busUri;
    private String processType;
    private String processId;
    private String serviceName;
    private String shutdownQueue;
    private boolean deletionFlag = false;
    
    private long lastHeartbeat;
    public final static String TYPE_PUBLISHER_REQUESTER = "PUBLISHER_REQUESTER";
    public final static String TYPE_BUSRESPONDER = "BUSRESPONDER";
    
    public TIBCOBespokeProcess(String busUri, String processType, String processId, String serviceName, String shutdownQueue)
    {
        this.busUri = busUri;
        this.processType = processType;
        this.processId = processId;
        this.serviceName = serviceName;
        this.shutdownQueue = shutdownQueue;
    }
    
    public TIBCOBespokeProcess(String formattedProcessString)
    {
        //Expected in format <==PROCESS_INFO==>processId<=>processType<=>processBusUri<=>processServiceName<==>shutdownQueue<==PROCESS_INFO==>
        Log.log.trace("TIBCOProcesses(formattedProcessString): " + formattedProcessString);
        String processInfo = StringUtils.substringBetween(formattedProcessString, "<==PROCESS_INFO==>");
        String[] processInfoSplit = processInfo.split("<=>");
        
        if(processInfoSplit.length >= 5)
        {                             
            this.busUri = processInfoSplit[2];
            this.processType = processInfoSplit[1];
            this.processId = processInfoSplit[0];
            this.serviceName = processInfoSplit[3];
            this.shutdownQueue = processInfoSplit[4];
        }
        else
        {
            throw new RuntimeException("Invalid format for heartbeat message recieved");
        }
            
    }
    
    public void updateLastHeartbeat()
    {
        lastHeartbeat = System.currentTimeMillis();
    }
    
    public String getBusUri()
    {
        return busUri;
    }
    
    public String getProcessType()
    {
        return processType;
    }
    
    public String getProcessId()
    {
        return processId;
    }
    
    public long getTimeSinceLastHeartbeat()
    {
        return System.currentTimeMillis() - lastHeartbeat;
    }
    
    public String getShutdownQueue()
    {
        return shutdownQueue;
    }
    
    public boolean getDeletionFlag()
    {
        return deletionFlag;
    }
    
    public void setDeleted()
    {
        deletionFlag = true;
    }
    
    public String toString()
    {
        return "busUri: " + busUri + ", processType: " + processType + ", processId: " + processId + ", serviceName: " + serviceName;
    }
    
}
