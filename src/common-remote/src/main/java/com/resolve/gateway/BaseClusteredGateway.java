/******************************************************************************
 * (C) Copyright 2014
 * 
 * Resolve Systems, LLC
 * 
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 * 
 ******************************************************************************/

package com.resolve.gateway;
import com.resolve.util.LicenseEnum;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.mail.Message;

import com.google.gson.JsonObject;
import com.resolve.esb.DefaultHandler;
import com.resolve.esb.ESBException;
import com.resolve.esb.MListener;
import com.resolve.esb.MMsgHeader;
import com.resolve.gateway.resolutionrouting.GatewayRRSettings;
import com.resolve.gateway.resolutionrouting.SIRRRSettings;
import com.resolve.gateway.resolutionrouting.UIRRSettings;
import com.resolve.gateway.resolvegateway.ResolveGateway;
import com.resolve.gateway.resolvegateway.msg.ConfigReceiveMSGGateway;
import com.resolve.gateway.resolvegateway.pull.ConfigReceivePullGateway;
import com.resolve.gateway.resolvegateway.push.ConfigReceivePushGateway;
import com.resolve.gateway.response.PlainTextResponse;
import com.resolve.gateway.util.dcs.BaseDataCollectionLogger;
import com.resolve.gateway.util.dcs.RawDataCollectionLogger;
import com.resolve.gateway.util.dcs.IncidentDataCollectionLogger;
import com.resolve.gateway.util.dcs.NoopDataCollectionLogger;
import com.resolve.query.mapper.ResolveMapper;
import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.ConfigMap;
import com.resolve.util.Constants;
import com.resolve.util.ExceptionUtils;
import com.resolve.util.FileUtils;
import com.resolve.util.GatewayEvent;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.util.IncidentUtil;
import com.resolve.util.Log;
import com.resolve.util.MacroSubstitution;
import com.resolve.util.MessageDispatcher;
import com.resolve.util.ObjectProperties;
import com.resolve.util.ObjectSizeCalculator;
import com.resolve.util.StringUtils;
import com.resolve.util.cef.CEFEvent;
import com.resolve.util.cef.CEFParseException;
import com.resolve.util.cef.CEFParser;
import com.resolve.util.eventsourcing.incident.EventMeta;
import com.resolve.util.eventsourcing.EventType;
import com.resolve.util.metrics.ExecutionMetrics;
import com.resolve.util.metrics.ExecutionMetricsException;
import com.resolve.util.queue.AbstractQueueListener;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;

import groovy.lang.Binding;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import com.resolve.util.eventsourcing.incident.IncidentCreatedData;
import com.resolve.util.eventsourcing.incident.IncidentTypeChangedData;

/**
 * This class provides a partial implementation for clustered gateway sub
 * classes implemting most common functionalities of the gateways. Most of the
 * gateways are part of a cluster which provide the High Availability (HA)
 * functionality. This class provides the necessary functionalities for a gatway
 * to be a member of a cluster.
 */
public abstract class BaseClusteredGateway extends BaseGateway implements ClusteredGateway
{
    public static final String GATEWAYTYPE_PRIMARY = "PRIMARY";
    public static final String GATEWAYTYPE_SECONDARY = "SECONDARY";
    public static final String GATEWAYTYPE_WORKER = "WORKER";
    public static final String FILTER_ID_NAME = "FILTER_ID";
    public static final String GATEWAY_NAME = "GATEWAY_NAME";
    public static final String GATEWAY_TYPE = "GATEWAY_TYPE";
    
    protected static final String QUEUE = "QUEUE";
    
    private static final String WORKER_QUEUE_SUFFIX = "_WORKER";
    private static final String HEALTH_CHECK_QUEUE_SUFFIX = "_HEALTH_CHECK";
    
    protected Pattern VAR_REGEX_GATEWAY_VARIABLE = Pattern.compile(Constants.VAR_REGEX_GATEWAY_VARIABLE);
    protected Pattern VAR_REGEX_CONNECTOR_VARIABLE= Pattern.compile(Constants.VAR_REGEX_CONNECTOR_VARIABLE);

    protected final MMsgHeader esbMessageHeader = new MMsgHeader();
    protected final MMsgHeader heartbeatHeader = new MMsgHeader();

    // executor service to add data into primary data queue
    protected ExecutorService executor = null;

    protected ConcurrentHashMap<String, Filter> filters = new ConcurrentHashMap<String, Filter>();

    protected volatile List<Filter> orderedFilters = new CopyOnWriteArrayList<Filter>();
    protected volatile Map<String, Filter> orderedFiltersMapById = new ConcurrentHashMap<String, Filter>();

    private NameProperties nameProperties = new NameProperties();
    private List<TreeSet<RoutingSchema>> routingSchemas = new ArrayList<TreeSet<RoutingSchema>>();

    private volatile boolean primary = true;
    private volatile boolean secondary = true;
    private volatile boolean worker = false;
    private boolean uppercase = true;
    private boolean originalPrimary = true;
    protected long heartbeatInterval = 20000;
    protected long failover = 60000;
    protected long lastHeartbeat;
    protected String topicName;
    protected String workerQueueName;
    private Map<String, String> message = new HashMap<String, String>();

    // just for analytics purpose
    private static long dataCounter;
    private static BaseDataCollectionLogger<Object> dcsGenericLogger;
    private static BaseDataCollectionLogger<Object> dcsIncidentEventLogger;
    // when this gate starts
    protected long startTime;

    // initialized during gateway instance initialize()
    // Provides the location where configurations like name properties are
    // stored (e.g., /config/netcool/);
    protected String gatewayConfigDir;

    protected volatile boolean isSyncDone = false;

    private GatewayHeartbeat heartbeatThread = null;

    protected int interval = 10000;
    protected int minimumInterval = 10000;
    protected int timeout = 120000;
    
    private int TASK_TIMEOUT = 60 * 1000; // 60 seconds
    protected long GLOBAL_SEND_TIMEOUT = 10 * 60 * 1000; // milliseconds

    protected MListener gatewayQueueListener = null;
    protected MListener guidQueueListener = null;
    protected MListener workerQueueListener = null;

    protected Thread thread;

    // used by primary to gather data and distribute to workers.
    protected Queue<Map<String, String>> primaryDataQueue = new LinkedList<Map<String, String>>();
    protected QueueListener<Map<String, String>> primaryDataQueueListener = new PrimaryDataQueueListener();

    // exclusively used by the worker gateways
    protected Queue<GatewayEvent<String, Object>> workerDataQueue = new LinkedList<GatewayEvent<String, Object>>();
    protected QueueListener<GatewayEvent<String, Object>> workerDataQueueListener = new WorkerDataQueueListener();

    // Set of undeployed filters to ignore data received while filter was getting undeployed
    
    protected Set<String> undeployedFilterIds = new HashSet<String>();
    
    private MListener defaultTopicListener = null;
    
    private boolean gatewayLicenseCheckStarted = false;
    private boolean gatewayWorkerSelfCheckStarted = false;
    private boolean gatewayTopicSelfCheckStarted = false;
    private boolean gatewayConnectionSelfCheckStarted = false;
    private boolean gatewayQueueSelfCheckStarted = false;
    
    private int gatewayPrimaryExecutorQueueSize = ConfigReceiveGateway.MIN_PRIMARY_DATA_QUEUE_EXECUTOR_QUEUE_SIZE;
    
    private String source;
    
    /**
     * Implemented method in the sub class will add any gateway specific
     * key/value pairs in the event map.
     * 
     * @param event
     * @param result
     */
    abstract protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result);

    /**
     * Overwrite method in the sub class to add any gateway specific
     * key/value pairs in the event map based on the filter object type
     *
     * @param event
     * @param result
     * @param filter
     */
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result, Filter filter)
    {
        addAdditionalValues(event, result);
    }
    
    protected BaseClusteredGateway(ConfigMap config)
    {
        super(config);
        dcsGenericLogger = Log.isDcsLoggingEnabled() ? new RawDataCollectionLogger() : new NoopDataCollectionLogger();
        dcsIncidentEventLogger = Log.isDcsLoggingEnabled() ? new IncidentDataCollectionLogger() : new NoopDataCollectionLogger();
        
        try
        {
            ConfigReceiveGateway gatewayConfig = (ConfigReceiveGateway) configurations;
            
            gatewayPrimaryExecutorQueueSize = gatewayConfig.getPrimaryDataQueueExecutorQueueSize();
            
            if (gatewayConfig.isActive() && (gatewayConfig.isPrimary() || gatewayConfig.isSecondary()))
            {
                executor = new ThreadPoolExecutor(gatewayConfig.getPrimaryDataQueueExecutorCorePoolSize(), 
                                                  gatewayConfig.getPrimaryDataQueueExecutorMaxPoolSize(), 
                                                  gatewayConfig.getPrimaryDataQueueExecutorKeepAliveTimeInSec(), 
                                                  TimeUnit.SECONDS, 
                                                  new LinkedBlockingQueue<Runnable>(gatewayPrimaryExecutorQueueSize));
            }
            
            String className = gatewayConfig.getClass().getName();
            String eventType = getGatewayEventType();
            source = gatewayConfig.getClass().getName().substring(className.indexOf("ConfigReceive") + 13, className.length());
            
            if(className.indexOf(".push.") != -1 || className.indexOf(".pull.") != -1 || className.indexOf(".msg.") != -1) {
                source = gatewayConfig.getGatewayName();
                eventType = gatewayConfig.getQueue();
            }
            //DEC-354 : Adding the TIMEOUT for groovy scripts executed on the gateway
            TASK_TIMEOUT = gatewayConfig.getGroovytimeout();
            
            // init message header
            esbMessageHeader.setEventType(eventType);
            
            // esbMessageHeader.setRouteDest(Constants.ESB_NAME_RSSERVER_EVENT,
            // "MEvent.eventResult");
            esbMessageHeader.setRouteDest(Constants.ESB_NAME_EXECUTEQUEUE, "MEvent.eventResult");
            
            this.thread = new Thread(this);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    }

    @Override
    public boolean isPrimary()
    {
        return primary;
    }

    @Override
    public void setPrimary(boolean primary)
    {
        this.primary = primary;
    }

    @Override
    public boolean isOriginalPrimary()
    {
        return originalPrimary;
    }

    @Override
    public boolean isSecondary()
    {
        return secondary;
    }

    @Override
    public boolean isWorker()
    {
        return worker;
    }

    public boolean isUppercase()
    {
        return uppercase;
    }

    public void setUppercase(boolean uppercase)
    {
        this.uppercase = uppercase;
    }

    @Override
    public void setLastHeartbeat()
    {
        this.lastHeartbeat = System.currentTimeMillis();
    } // setLastHeartbeat

    @Override
    public Map<String, String> getMessage()
    {
        return message;
    }

    @Override
    public void setMessage(Map<String, String> message)
    {
        this.message = message;
    }

    @Override
    public GatewayHeartbeat getHeartbeatThread()
    {
        return heartbeatThread;
    }

    @Override
    public void setHeartbeatThread(GatewayHeartbeat heartbeatThread)
    {
        this.heartbeatThread = heartbeatThread;
    }

    @Override
    public long getHeartbeatInterval()
    {
        return heartbeatInterval;
    }

    @Override
    public MMsgHeader getHeartbeatHeader()
    {
        return heartbeatHeader;
    }

    @Override
    public long getLastHeartbeat()
    {
        return lastHeartbeat;
    }

    @Override
    public void setLastHeartbeat(long lastHeartbeat)
    {
        this.lastHeartbeat = lastHeartbeat;
    }

    @Override
    public long getFailoverInterval()
    {
        return this.failover;
    }

    /**
     * Verifies if the System clock is set backward. In that case we need to
     * make sure that the heart beat counter is reset as well. Otherwise it will
     * fall behind and not send hearbeat until system time catch up to it..
     */
    @Override
    public void checkSystemClockReset()
    {
        if (lastHeartbeat > System.currentTimeMillis())
        {
            setLastHeartbeat();
            Log.log.debug("HEARTBEAT RESET: Due to system clock set backward, lastHeartBeat is reset to " + lastHeartbeat);
        }
    }

    @Override
    protected String getInstanceType()
    {
        if (isPrimary())
        {
            return GATEWAYTYPE_PRIMARY;
        }
        else if (isSecondary())
        {
            return GATEWAYTYPE_SECONDARY;
        }
        else if (isWorker())
        {
            return GATEWAYTYPE_WORKER;
        }
        else
        {
            return null;
        }
    }
	
	public void initPrimarySecondaryWorkerFlag() {
		ConfigReceiveGateway gatewayConfig = (ConfigReceiveGateway) configurations;
		primary = gatewayConfig.isPrimary();
		secondary = gatewayConfig.isSecondary();
		originalPrimary = primary;
		worker = gatewayConfig.isWorker();
	}

    @Override
    public void init() throws Exception
    {
        try
        {
            super.init();

            // init
            this.active = false;

            topicName = getQueueName() + "_TOPIC";
            // init heartbeat header
            // topic like XMPP_TOPIC, and the handler is MXMPP.heartbeat
            heartbeatHeader.setRouteDest(topicName, getMessageHandlerName() + ".heartbeat");

            startTime = System.currentTimeMillis();

            ConfigReceiveGateway gatewayConfig = (ConfigReceiveGateway) configurations;
            String gateway = gatewayConfig.getGatewayName();
            primary = gatewayConfig.isPrimary();
            secondary = gatewayConfig.isSecondary();
            originalPrimary = primary;
            worker = gatewayConfig.isWorker();
            workerQueueName = getQueueName() + WORKER_QUEUE_SUFFIX;
            uppercase = gatewayConfig.isUppercase();
            message.put("GATEWAY", gateway);
            message.put("PRIMARY", String.valueOf(originalPrimary));
            message.put("COMPONENT_ID", MainBase.main.configId.getGuid().toString());

            heartbeatInterval = gatewayConfig.getHeartbeat() * 1000;
            failover = gatewayConfig.getFailover() * 1000;

            //initialize first heartbeat to current time
            setLastHeartbeat();
            
            primaryDataQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(primaryDataQueueListener);
            workerDataQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(workerDataQueueListener);

            // if primary then no matter what it will be active.
            // this assignment is fine because this method is called
            // by Main if the gateway is active to begin with.
            if (primary)
            {
                setActive(true);
            }

            // Create ESB listeners for gateway and, container guid queues
            gatewayQueueListener = MainBase.main.mServer.createListener(getQueueName(), "com.resolve.gateway");
            guidQueueListener = MainBase.main.mServer.createListener(MainBase.main.configId.getGuid(), "com.resolve.gateway");
            
            if (isActive())
            {
            	Log.log.info(String.format("%s: Initializing ESB Listener for the queue: %s", gatewayClassName, 
            							   getQueueName()));
                gatewayQueueListener.init(false);

                Log.log.info(String.format("%s: Initializing ESB Listener for the queue: %s", gatewayClassName, 
                						   MainBase.main.configId.getGuid()));
                guidQueueListener.init(false);
            }

            //Only non-primary workers need to listen to instance specific queue
            if (isWorker() && !isPrimary())
            {
                guidQueueListener.init(false);
                Log.log.debug(gatewayClassName + ":  Initialized ESB Listener for the queue: " + MainBase.main.configId.getGuid());
            }

            if (isWorker() || (isPrimary() && !isWorker()))
            {
                if (!isWorker())
                {
                	Log.log.debug(gatewayClassName + ":  Initializing Resolve Self-Check Specific ESB Listener for the worker queue: " + workerQueueName);
                }
                else
                {
                	Log.log.debug(gatewayClassName + ":  Initializing ESB Listener for the worker queue: " + workerQueueName);
                }
                
                workerQueueListener = MainBase.main.mServer.createListener(workerQueueName, "com.resolve.gateway");
                workerQueueListener.init(false);
                
                if (!isWorker())
                {
                	Log.log.debug(gatewayClassName + ":  Initialized Resolve Self-Check Specific ESB Listener for the worker queue: " + workerQueueName);
                }
                else
                {
                	Log.log.debug(gatewayClassName + ":  Initialized ESB Listener for the worker queue: " + workerQueueName);
                }
            }

            // init topic

            MainBase.main.mServer.createPublication(Constants.ESB_NAME_GATEWAY_TOPIC);
            MainBase.main.mServer.subscribePublication(Constants.ESB_NAME_GATEWAY_TOPIC);
            Log.log.debug(gatewayClassName + ": Created and subscribed to publication " + Constants.ESB_NAME_GATEWAY_TOPIC);
            MainBase.main.mServer.createPublication(topicName);
            Log.log.debug(gatewayClassName + ": Created publication " + topicName);
            
            DefaultHandler defaultTopicHandler = new DefaultHandler(MainBase.main.mServer.getDefaultMSender(), MainBase.main.mServer.getDefaultClassPackage(), getMessageHandlerClass().getName());
            defaultTopicListener = MainBase.main.mServer.createListener(topicName, "com.resolve.gateway", defaultTopicHandler);
            defaultTopicListener.init(false);
            Log.log.debug(gatewayClassName + ": Created and initialized ESB Listener for queue name " + topicName);
            // above step already creates a listener, why re-create it
            //MainBase.main.mServer.subscribePublication(topicName, defaultTopicListener);

            // init handlers
            MainBase.main.mServer.addHandler(getMessageHandlerName(), getMessageHandlerClass());
            Log.log.info(gatewayClassName+":  Successfully created listener on "
            		+ "RabbitMQ for the Gateway with topic name --> " + topicName);
            Log.log.info(gatewayClassName+":"+"The gateway is initialized and connected to RSMQ");
        }
        catch(ESBException e) {
        	Log.log.error(gatewayClassName+":"+"There was an error setting up gateway queues and publications in RSMQ");
        	throw e;
        }
        catch (Exception e)
        {
        	Log.log.error(gatewayClassName + ":  Error when creating a listener on RabbitMQ "
            		+ "with topic name --> " + topicName);
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }

    @Override
    public void reinitialize()
    {
        Log.log.debug(gatewayClassName + ": Checking if secondary can fail over as primary");
        if (isValidLicenseForPrimary(true, true))
        {
            super.reinitialize();
    
            String instanceTypeB4Switch = getInstanceType();
            
            setActive(true);
            setPrimary(true);
    
            MainBase.getESB().getMServer().createQueueConsumer(gatewayQueueListener.getReceiveQueueName(), gatewayQueueListener);
            
            // Create queue listener if not created previously
            if (guidQueueListener.getMServer().getListener(guidQueueListener.getListenerId()) == null)
            {
                MainBase.getESB().getMServer().createQueueConsumer(guidQueueListener.getReceiveQueueName(), guidQueueListener);
            }
            
            MainBase.main.getLicenseService().switchGatewayRunningInstanceType(getLicenseCode(), MainBase.main.configId.getGuid(), instanceTypeB4Switch, getQueueName());
            MainBase.main.getLicenseService().sendGatewaySwitchRunningInstanceType(getLicenseCode(), MainBase.main.configId.getGuid(), instanceTypeB4Switch, getQueueName());
            
            // start the data reader
            this.running = true;
            thread = new Thread(this);
            thread.start();
            
            // Not original Primary, is secondary and has become primary due to fail over and is not a worker then initiate worker self check
            
            if (!isOriginalPrimary() && isSecondary() && isPrimary() && !isWorker())
            {
                try
                {
                    if (workerQueueListener == null)
                    {
                        workerQueueListener = MainBase.main.mServer.createListener(workerQueueName, "com.resolve.gateway");
                        workerQueueListener.init(false);
                    }
                    
                    gwWorkerSelfCheck();
                }
                catch (Exception e)
                {
                    Log.log.error(gatewayClassName+":"+e.getMessage(), e);
                }
            }
        }
        else
        {
            Log.log.warn(gatewayClassName + ": " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + 
                         " running on rsremote instance " + MainBase.main.configId.getGuid() + 
                         " can not become PRIMARY due to unavailability of gateway license, Please load valid licenses");
        }
    }

    public void start()
    {
        Log.setCurrentContext(getClass().getSimpleName());
        // initialize resources
        try
        {
			initPrimarySecondaryWorkerFlag();
            // check the license for this gateway
            if (isValidLicense(true, originalPrimary))
            {
            	init();
            	
                // if this is a clustered gateway then start the hearbeats.
                if (this instanceof ClusteredGateway)
                {
                    if(heartbeatThread == null)
                    {
                        heartbeatThread = new GatewayHeartbeat(this);
                        //simple workers don't need to send heartbeat
                        if(isPrimary() || isSecondary())
                        {
                            Log.log.debug(gatewayClassName+":Starting heartbeat thread");
                            running = true;
                            heartbeatThread.start();
                        }
                    }
                }

                // if this is just a worker gateway we do not need to keep the
                // thread running as the primary will distribute the data
                if (isPrimary())
                {
                    // start the gateway
                    thread = new Thread(this);
                    running = true;
                    thread.start();
                    
                    if (!isWorker())
                    {
                        // Initiate Gateway Worker Self-Check as Primary and Not Worker
                        gwWorkerSelfCheck();
                    }
                }

                /* any gateway could become a worker. sometime primary
                 * may even not take part in worker community
                 * and act as router to route work amongst workers
                 */
                if (isWorker())
                {
                    gwWorkerSelfCheck();
                    
                    if (!isPrimary())
                    {
                    	running = true;
                        // get all the latest filter, name properties etc.
                        sendSyncRequest();
                    }
                }
                
                // Primary/secondary gateway should perform Primary Queue self check and check connection periodically
                
                if (isPrimary() || isSecondary())
                {
                    gwQueueSelfCheck();
                    
                    if (!gatewayConnectionSelfCheckStarted)
                    {
                        if (checkGatewayConnection())
                        {
                            // Schedule next gateway connection self check
                            ScheduledExecutor.getInstance().executeRepeat(getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-ConnectionSelfCheck", this, "checkGatewayConnection", ((ConfigReceiveGateway)configurations).getConnectionSCInterval(), TimeUnit.SECONDS);
                            gatewayConnectionSelfCheckStarted = true;
                            Log.log.info(gatewayClassName +": Scheduled " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-ConnectionSelfCheck to run every " + ((ConfigReceiveGateway)configurations).getConnectionSCInterval() + " seconds");
                        }
                        else
                        {
                        	 Log.log.info(gatewayClassName +": gateway's " +getLicenseCode() + " gateway does not override checkGatewayConnection method required for connection self-check.");
                        }
                    }
                }
                
                gwTopicSelfCheck();
            }

            // start() can get invoked multiple times
            
            if (!gatewayLicenseCheckStarted)
            {
                /* schedule next license check
                 * even though initially license may be invalid to run the gateway
                 * it may become available later on when license is added/updated
                 */
                ScheduledExecutor.getInstance().executeRepeat(getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType(), this, "checkGatewayLicense", LICENSE_CHECK_INTERVAL, TimeUnit.MINUTES);
                gatewayLicenseCheckStarted = true;
                Log.log.info(gatewayClassName + ": Scheduled " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-GatewayLicenseCheck to run every " + LICENSE_CHECK_INTERVAL  + " minutes");
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        String gatewayRunningMsg = String.format(gatewayClassName+":"+"%s-%s-%s is %s", getLicenseCode(), getQueueName(), getInstanceType(),
				 								 (running ? "running!!!" : "NOT running"));
        if (running) {
        	Log.log.info(gatewayRunningMsg);
        } else {
        	Log.log.warn(gatewayRunningMsg);
        }
        Log.clearContext();
    } // start

    public void stop()
    {
        try {
            Log.log.warn(gatewayClassName + ": Stopping the " + getQueueName() + " gateway.");
            Log.log.warn("*************************************************");
            MainBase.main.mServer.unsubscribePublication(defaultTopicListener.getListenerId());
            MainBase.main.getLicenseService().removeGatewayRunningInstance(getLicenseCode(), MainBase.main.configId.getGuid(), getInstanceType(), getQueueName());
            MainBase.main.getLicenseService().sendGatewayRemoveRunningInstance(getLicenseCode(), MainBase.main.configId.getGuid(), getInstanceType(), getQueueName());
            
            active = false;
            running = false;
            try
            {
                thread = null;
                heartbeatThread.setActive(false);
                heartbeatThread = null;
            }
            catch (Exception e)
            {
                Log.log.warn(gatewayClassName + ": ERROR--> " + e.getMessage());
            }
        } finally {
            Log.clearContext();
        }

    }

    @Override
    public void deactivate()
    {
        super.deactivate();
        MainBase.getESB().getMServer().closeQueueConsumer(gatewayQueueListener.getListenerId());
        MainBase.getESB().getMServer().closeQueueConsumer(guidQueueListener.getListenerId());
        String instanceTypeB4Switch = getInstanceType();
        setActive(false);
        setPrimary(false);
        // stop the thread
        thread = null;
        MainBase.main.getLicenseService().switchGatewayRunningInstanceType(getLicenseCode(), MainBase.main.configId.getGuid(), instanceTypeB4Switch, getQueueName());
        MainBase.main.getLicenseService().sendGatewaySwitchRunningInstanceType(getLicenseCode(), MainBase.main.configId.getGuid(), instanceTypeB4Switch, getQueueName());
        
        // Not original Primary, is secondary after failback and is not a worker then remove the worker queue consumer self check
        
        if (!isOriginalPrimary() && isSecondary() && !isPrimary() && !isWorker())
        {
            if (workerQueueListener != null)
            {
                MainBase.getESB().getMServer().closeQueueConsumer(workerQueueListener.getListenerId());
                workerQueueListener.close();
                workerQueueListener = null;
            }
        }
        
        // Check license just to verify that it can still switch back as secondary
        checkGatewayLicense();
    }

    public synchronized boolean isSyncDone()
    {
        return isSyncDone;
    }

    public synchronized void setSyncDone(boolean isSyncDone)
    {
        this.isSyncDone = isSyncDone;
    }

    @Override
    public Map<String, String> getSyncRequestParameters()
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("GATEWAY_NAME", getQueueName());
        params.put("GATEWAY_CLASS", getLicenseCode());
        params.put("QUEUE_NAME", MainBase.main.configId.getGuid());
        params.put("MESSAGE_HANDLER_NAME", "com.resolve.gateway." + getMessageHandlerName());
        params.put("ORG_NAME", getOrgSuffix());
        return params;
    }

    /**
     * This method requests RSCONTROL to provide various items (filter, name
     * properties, routing schemas etc.) belongs to this gateway.
     * 
     * TODO refactor to use the sync ESB.call() method later.
     */
    @SuppressWarnings("unchecked")
    protected void sendSyncRequest()
    {
        // this will make sure that until the RSRemote is ready to receive
        // message it won't send anything out.
        while (!MainBase.esb.getMServer().isActive())
        {
            try
            {
                // just sleep for a second
                Thread.sleep(1000);
                if (Log.log.isTraceEnabled())
                {
                    Log.log.trace(gatewayClassName+":RSRemote not ready, not sending gateway synchronization message from " + getQueueName());
                }
            }
            catch (InterruptedException e)
            {
                Log.log.debug(gatewayClassName+":"+e.getMessage(), e);
            }
        }

        // synchronize the filters from RSControl
        isSyncDone = false;

        // get the parameters to be sent to RSControl.
        Map<String, String> param = getSyncRequestParameters();

        // we have to block here to receive message from RSCONTROL
        // otherwise we may be executing with contaminated configuration.
        long sleeptime = 2000L;
        long maxSleeptime = 60000L; // 60 seconds
        boolean sendSuccess = false;
        // long
        
        while ((isActive() || (isWorker() && !isPrimary())) && !isSyncDone())
        {
            try
            {
                Map<String, String> response = MainBase.getESB().call("RSCONTROL", "MInfo.info", null, 5000L);
                if (response == null)
                {
                    Log.log.info(gatewayClassName + ": None of the RSCONTROLs are running to receive gateway synchronization message, retry.");
                }
                else
                {
                    if (!sendSuccess)
                    {
                    	Log.log.info(gatewayClassName + ": None of the RSCONTROLs are running to receive gateway synchronization message, retry.");
                        sendSuccess = MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROL, getMessageHandlerName() + ".synchronizeGateway", param);
                    }
                    Log.log.info(gatewayClassName + ": Sync request cfor filter, name properties and routing schemas sent to RSControl successfully.");
                }
                
                if (sleeptime < maxSleeptime)
                {
                    sleeptime += 2000L;
                }
                else
                {
                    Log.log.warn(this.source + ": Has not received response to sync message for " + getQueueName() + " back from RSControl after 1 minute, still waiting:");
                }

                Thread.sleep(sleeptime);
                if (Log.log.isTraceEnabled())
                {
                	Log.log.trace(gatewayClassName + ": Have not received sync message back from RSControl, waiting:" + getQueueName());
                }
            }
            catch (InterruptedException e)
            {
                Log.log.debug(gatewayClassName+":"+e.getMessage(), e);
            }
            catch (Exception e)
            {
                Log.log.error(gatewayClassName+":"+e.getMessage(), e);
            }
        }
        
        Log.log.debug(gatewayClassName+":"+"received sync message back from RSControl with filters, routing schemas etc for Gateway instance " + param.get("QUEUE_NAME") + " ");
    }

    /**
     * This method is implemented by subclasses to update various custom fields
     * in the target server. For example in Netcool or HPOM gateway it will
     * update RESOLVE_STATUS and RESOLVE_RUNBOOKID.
     * 
     * @param filter
     * @param content
     */
    public void updateServerData(Filter filter, Map<String, String> content)
    {
        // Not implemented here, normaly subclasses like HPOM, Netcool etc.
        // should implement this method to update the RESOLVE_STATUS,
        // RESOLVE_RUNBOOKID field etc.
    }

    /**
     * This method decides if the filter should execute or not. It's based on
     * the interval for the filter and the last execution time.
     * 
     * @param filter
     * @param startTime
     * @return
     */
    protected boolean shouldFilterExecute(Filter filter, long startTime)
    {
        boolean result = false;
        // if this is primary then only we need to execute filters.
        if (isPrimary())
        {
            if (filter != null && filter.isActive())
            {
                if (filter.getLastExecutionTime() == 0)
                {
                    result = true;
                }
                else
                {
                    result = startTime >= (filter.getLastExecutionTime() + filter.getInterval() * 1000);
                }
                if (result)
                {
                	Log.log.trace(gatewayClassName + ": " + getQueueName() + " gateway: filter " + filter.getId() + " is ready to execute...");
                    // register this startTime as last execution time
                    filter.setLastExecutionTime(startTime);
                }
            }
            if (!result)
            {
                try
                {
                    Log.log.trace(gatewayClassName + ": Sleeping for " + minimumInterval);
                    Thread.sleep(1000L);
                    // This needs to be revisited. With 1 second sleep we have
                    // not see any CPU usage spike.
                    /*
                     * long min = minimumInterval; for(Filter filter1 :
                     * getFilters().values()) { long newInterval =
                     * System.currentTimeMillis() -
                     * filter1.getLastExecutionTime() + filter1.getInterval();
                     * if(min > newInterval) { min = newInterval; } }
                     */
                }
                catch (InterruptedException e)
                {
                    // nothing to do
                }
            }
        }
        return result;
    }

    /**
     * Processes data received from the target system. It checks the data with
     * any interested filter and process the filter.
     * 
     * @param data
     *            could be a {@link Message} or an XMPP message etc.
     * @throws Exception
     */
    protected void processData(Object data) throws Exception
    {
        throw new RuntimeException("If a subclass wants to use this method, it must be implemented by the that Subclass.");
    }

    @Override
    public void setFilter(Map<String, Object> params)
    {
        Filter filter = getFilter(params);

        // remove existing filter
        removeFilter(filter.getId());

        // update filters
        filters.put(filter.getId(), filter);

        int filterInterval = filter.getInterval() * 1000;
        if (filterInterval < minimumInterval)
        {
            minimumInterval = filterInterval;
        }
        // create new orderedFilters
        synchronized (getClass())
        {
            List<Filter> syncedOrderededFilters = new ArrayList<Filter>(filters.values());
            Collections.sort(syncedOrderededFilters);
            
            orderedFilters = new CopyOnWriteArrayList<Filter>(syncedOrderededFilters);
            
            orderedFiltersMapById.clear();
            
            for (Filter ofilter : orderedFilters)
            {
                orderedFiltersMapById.put(ofilter.getId(), ofilter);
            }
        }
    }

    /**
     * Adds event related items in the params {@link Map} by parsing the
     * incoming message. This method is mainly used by the gateways which
     * receives messages (e.g., XMPP, Email etc.).
     * 
     * @param message
     *            in the form of:
     * 
     *            Persistent Event: 1) EVENT= My Event EVENTREF=Event reference
     *            WIKI=ns.wiki name PROBLEMID=LOOKUP NUMBER=PRB1234-2
     *            PERSISTENT=true; 2) EVENT= My Event EVENTREF=Event reference
     *            WIKI=ns.wiki name PROBLEMID=LOOKUP ALERTID=Worksheet Alert ID
     *            PERSISTENT=true; 3) EVENT= My Event EVENTREF=Event reference
     *            WIKI=ns.wiki name PROBLEMID=LOOKUP REFERENCE=Worksheet
     *            Reference PERSISTENT=true;
     * 
     *            In-memory Event: 4) EVENT= My Event EVENTREF=Event reference
     *            PROBLEMID=LOOKUP NUMBER=PRB1234-1;
     * 
     * @param actual
     *            parameters added for the system to execute the event are:
     *            EVENT => EVENT_EVENTID EVENTREF=>EVENT_REFERENCE
     *            WIKI=>WIKI=ns.wiki PROBLEMID=>PROBLEMID
     *            PERSISTENT=>EVENT_START NUMBER=>NUMBER ALERTID=>ALERTID
     *            REFERENCE=>REFERENCE
     */
    protected void addEvent(String message, Map<String, String> params)
    {
        try
        {
            ResolveMapper resolveMapper = new ResolveMapper();
            HashMap<String, String> result = resolveMapper.mapQuery(message);

            // if the message has event then we'll check for whether it's
            // persistent or not in the message itself
            // because if the event is passed we'll ignore the event configured
            // in the filter configuration
            String eventId = result.get("EVENT");
            if (StringUtils.isNotBlank(eventId))
            {
                params.put(Constants.EVENT_EVENTID, eventId);
            }

            params.put(Constants.EVENT_REFERENCE, result.get("EVENTREF"));
            params.put(Constants.EXECUTE_WIKI, result.get("WIKI"));

            params.put(Constants.EXECUTE_PROBLEMID, result.get("PROBLEMID"));
            params.put(Constants.WORKSHEET_ALERTID, result.get("ALERTID"));
            params.put(Constants.WORKSHEET_NUMBER, result.get("NUMBER"));
            params.put(Constants.WORKSHEET_REFERENCE, result.get("REFERENCE"));
        }
        catch (Exception e)
        {
            Log.log.info("Error parsing the message, might be something Resolve does not understand, it's fine!");
            Log.log.info("Actual error: " + e.getMessage());
        }
    }

    /**
     * Listener for the primary data queue. This prevents us from reading data
     * through a loop because it's on demand.
     */
    private class PrimaryDataQueueListener extends AbstractQueueListener<Map<String, String>>
    {
        public boolean process(Map<String, String> message)
        {
            if (Log.log.isTraceEnabled())
            {
                Log.log.trace("Received data to be sent");
                Log.log.trace("Contents:" + message);
            }

            long sendStartTmNSec = System.nanoTime();
            
            // Check if filter in message still exists since it may have been undeployed before message gets processed
            
            Filter filter = null;
            
            if (message.containsKey(FILTER_ID_NAME))
            {
                filter = orderedFiltersMapById.get(message.get(FILTER_ID_NAME));
                
                if (filter != null)
                {
                    if (!sendToWorker(message))
                    {
                        Log.log.warn("Failed to send message to worker queue, check the log file. [" + message + "]");
                        return false;
                    }
                    
                    Log.log.debug("Sending message to worker via RabbitMQ took " + ((System.nanoTime() - sendStartTmNSec) / 1000) + " micro secs");
                    
                    // This method is implemented by subclasses to update various custom
                    // fields in the target server. For example in Netcool or HPOM
                    // gateway
                    // it will update RESOLVE_STATUS and RESOLVE_RUNBOOKID.
                    
                    long updateStartTmNSec = System.nanoTime();
                    
                    BaseClusteredGateway.this.updateServerData(filter, message);
                    
                    Log.log.debug("Update server data took " + ((System.nanoTime() - updateStartTmNSec) / 1000) + " micro secs");
                }
                else
                {
                    Log.log.warn("Skipping adding message [" + StringUtils.mapToLogString(message) + 
                                 "] received by Primary Data Queue Listener as specified filter with id [" + 
                                 message.get(FILTER_ID_NAME) + "] has been undeployed.");
                    return false;
                }
            }
            else
            {
                Log.log.warn("Message [" + StringUtils.mapToLogString(message) + 
                             "] received by Primary Data Queue Listener does not contain key " + 
                             FILTER_ID_NAME + ", skipping adding message to primary data queue.");
                return false;
            }
                        
            return true;
        }
    }

    public void addToPrimaryDataQueue(Filter filter, Map<String, String> params)
    {
        Runnable worker = new PrimaryDataQueueWorkerThread(this, filter, params);
        
        BlockingQueue<Runnable> bq = ((ThreadPoolExecutor)executor).getQueue();
        while ((bq.size() >= gatewayPrimaryExecutorQueueSize))
        {
            PrimaryDataQueueWorkerThread e = (PrimaryDataQueueWorkerThread)bq.poll();
            if (e!=null)
            {
                Log.log.warn("Primary Data Queue Is Full. Dropping Oldest Event: " + e.runbookParams);
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Primary Data Queue Is Full, Dropping Oldest Event", 
                          "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Primary Data Queue Is Full, Dropping Oldest Event " + 
                          "[" + e.runbookParams + "]");
            }
        }
        executor.execute(worker);
    }

    protected boolean sendToWorker(Map<String, String> params)
    {
        boolean result = true;
        // send to the worker queue
        if (MainBase.esb.sendInternalMessage(workerQueueName, getMessageHandlerName() + ".receiveData", params))
        {
            if (Log.log.isTraceEnabled())
            {
                Log.log.trace(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                Log.log.trace("Sent data to worker queue: " + workerQueueName);
                Log.log.trace(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            }
        }
        else
        {
            result = false;
            Log.log.warn("Could not send data to worker queue: " + workerQueueName);
        }
        return result;
    }

    @Override
    public String receiveData(Map<String, String> params)
    {
        String result = null;
        
        if (Log.log.isTraceEnabled())
        {
            dataCounter++;
            Log.log.trace("######################################");
            Log.log.trace(workerQueueName + " processed " + dataCounter + " item(s) so far.");
            Log.log.trace("######################################");
        }
        
        
        if((IncidentUtil.isAllowed(source) &&
                        ((com.resolve.rsremote.ConfigGeneral)MainBase.main.configGeneral).isIncidentDataCollectionEnabled()) ||
                        ((com.resolve.rsremote.ConfigGeneral)MainBase.main.configGeneral).isIncidentDataCollectionTestingEnabled()){
            String incidentInternalId = UUID.randomUUID().toString();
            params.put(Constants.EXECUTE_INCIDENT_ID, incidentInternalId);
        }
        
        if (params.containsKey(FILTER_ID_NAME))
        {
            String filterId = params.get(FILTER_ID_NAME);
            
            Filter filter = filters.get(filterId);
            
            if(params.containsKey(GATEWAY_NAME) && params.containsKey(GATEWAY_TYPE)) {

                ResolveGateway gateway = getGateway(params.get(GATEWAY_NAME), params.get(GATEWAY_TYPE));
                if(gateway != null)
                    filter = gateway.getFilters().get(filterId);
                else
                    filter = null;
            }

            if (filter == null)
            {
                // this may happen when synchronization is not done for this
                // gateway in this rsremote. so it may not
                // have the filter yet or filter may have been undeployed
                
                if (undeployedFilterIds.contains(filterId))
                {
                	 Log.log.warn(gatewayClassName + ": --> Filter: " + filterId + " was undeployed from this RSRemote, ignoring data");
                }
                else
                {
                	Log.log.warn(gatewayClassName + ": Filter--> " + filterId + " not available in this RSRemote, resending the data back to the worker queue");
                    MainBase.esb.sendInternalMessage(workerQueueName, getMessageHandlerName() + ".receiveData", params);
                }
            }
            else
            {
                dcsGenericLogger.log(params, source); 
                
                if(params.containsKey(Constants.EXECUTE_INCIDENT_ID)){
                    String incidentId = (String) params.get(Constants.EXECUTE_INCIDENT_ID);
                    String externalIncidentId = IncidentUtil.extractId(source, params);
                    EventMeta meta = new EventMeta(source, params);
                    IncidentCreatedData incidentCreatedData = new IncidentCreatedData(incidentId, externalIncidentId, meta);
                    dcsIncidentEventLogger.log(incidentCreatedData, EventType.INCIDENT_CREATED_EVENT.getName());
                } 
               
                // remove the key, FILTER_ID, from the params and not the value associated with it.
                params.remove(FILTER_ID_NAME);
                result = processFilter(filter, params);
            }
        }
        else
        {
            dcsGenericLogger.log(params, source); 
            
            if(params.containsKey(Constants.EXECUTE_INCIDENT_ID)){
                String incidentId = (String) params.get(Constants.EXECUTE_INCIDENT_ID);
                String externalIncidentId = IncidentUtil.extractId(source, params);
                EventMeta meta = new EventMeta(source, params);
                IncidentCreatedData incidentEventData =  new IncidentCreatedData(incidentId, externalIncidentId, meta);
                dcsIncidentEventLogger.log(incidentEventData, EventType.INCIDENT_CREATED_EVENT.getName());
            }
            for (Filter filter : orderedFilters)
            {
                result = processFilter(filter, params);
            }
        }
        
        return result;
    }

    /**
     * Listener for the worker data queue. This prevents us from reading data
     * through a loop because it's on demand.
     */
    private class WorkerDataQueueListener extends AbstractQueueListener<GatewayEvent<String, Object>>
    {
        public boolean process(GatewayEvent<String, Object> data)
        {
            boolean result = true;

            if (data != null)
            {
                if (Log.log.isTraceEnabled())
                {
                    Log.log.trace("Worker: Process the data received from primary gateway.");
                    Log.log.trace("Contents:" + data.content);
                }

                try
                {
                    if (!processFilterEvents(startTime, data))
                    {
                        Log.log.warn("Could not process gateway data, check the log file." + data.content);
                        result = false;
                    }
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
            else
            {
                Log.log.debug("Something wrong, worker is being called with empty data, check log file.");
            }
            return result;
        }
    }

    /**
     * This method is used by the Workers when they receives message to be
     * processed from primary.
     * 
     * @param filter
     * @param params
     */
    @SuppressWarnings("unchecked")
    protected String processFilter(final Filter filter, final Map<String, String> params)
    {
        try {
            Log.putCurrentNamedContext(Constants.GATEWAY_FILTER, filter.getId());
            String result = "";
            
            Map<String, String> content = setRunbookParam(params);
            
            // Run the groovy script before RR
            
            String eventId = null;
            String runbookName = null;
            
            boolean hasEvent = false;
            boolean hasRunbook = false;
            boolean hasSIR = false;
            
            Object groovyResult = null;
            
            boolean isJson = true;
            String jsonStr = params.get(Constants.HTTP_GATEWAY_RETURN_JSON);
            if (StringUtils.isNotEmpty(jsonStr))
            {
                isJson = Boolean.parseBoolean(jsonStr);
            }
            Map<String,Object> event = new HashMap<String,Object>();
            event.putAll(content);
            // Put any additional key/value needs to be added to the event.
            addAdditionalValues(event,content, filter);
            if (filter.hasGroovyScript())
            {
                String processNameForExecutionMetrics = String.format("%s-%s-%s gateway worker - groovy script execution for filter [%s]", 
                                                                      getGatewayEventType(), getQueueName(), MainBase.main.configId.getGuid(), 
                                                                      filter.getId());
                ExecutionMetrics.INSTANCE.registerEventStart(processNameForExecutionMetrics);
                
                String script = filter.getScript();
                String scriptName = filter.getId();

                Binding binding = new Binding();
                binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
                binding.setVariable(Constants.GROOVY_BINDING_DEBUG, debug);
                binding.setVariable(Constants.GROOVY_BINDING_NAME_PROPERTIES, nameProperties);
                binding.setVariable(Constants.GROOVY_BINDING_ESB, MainBase.esb);

                // Filter specific java.util.regex.Matcher object may come from the
                // subclasses for example, XMPP gateway provides a matcher.
                binding.setVariable(Constants.GROOVY_BINDING_REGEX_MATCHER, filter.getMatcher(content));

                // parameters
                binding.setVariable(Constants.GROOVY_BINDING_INPUTS, content);

                // assess output
                Map<String, String> outputs = new HashMap<String, String>();
                // pass all the params
                outputs.putAll(content);

                binding.setVariable(Constants.GROOVY_BINDING_OUTPUTS, outputs);

                // put additional bindings, for example gateways Netcool or DB
                // provides a java.sql.Connection in the script as "DB"
                addAdditionalBindings(script, binding);

                // execute script
                 
                try
                {               
                    Object obj = GroovyScript.executeAsync(script, scriptName, true, binding, TASK_TIMEOUT);
                    if (obj instanceof String) {
                        result = (String) obj;
                    // supporting both net.sf.json.JSONObject and com.google.gson.JsonObject
                    // supporting also plain text response (not wrapped up) - com.resolve.gateway.response.PlainTextResponse 
                    } else if (obj instanceof JSONObject || obj instanceof JsonObject || obj instanceof PlainTextResponse) {
                        groovyResult = obj;
                        result = obj.toString(); // not leaving it blank, JSON return type will be handled later
                    } else {
                        JSONObject message = new JSONObject();
                        message.accumulate("Message", "Executed Groovy script successfully.");
                        result = message.toString();
                    }
                }
                catch (Exception ex)
                {
                    String resultTxt = "Filter " + filter.getId() + " threw Groovy Script Error " + ex.getMessage();
                    sendAlert(ALERT_SEVERITY_CRITICAL, true, resultTxt, ExceptionUtils.getStackTrace(ex));
                    Log.log.error(gatewayClassName + ": Failed to execute script for the Filter: " + filter.getId(), ex);
                    
                    result = (String)resultTxt;
                        
                    // SHOULD THE MESSAGE BE PROCESSED IF FILTER GROOVY SCRIPT FAILS TO EXECUTE?
                }
                finally
                {
                    // subclasses may need some cleanup (like Netcool wants to
                    // return connection to the pool.
                    groovyScriptBindingCleanup(binding);
                }
                
                Log.log.trace(gatewayClassName +": Filter Groovy Script OUTPUTS --> " + outputs);

                // groovy script might have changed EventId
                if (StringUtils.isNotBlank(outputs.get(Constants.EXECUTE_EVENTID)))
                {
                    hasEvent = true;
                    eventId = outputs.get(Constants.EXECUTE_EVENTID);
                }

                // groovy script might have changed WIKI
                if (StringUtils.isNotBlank(outputs.get(Constants.EXECUTE_WIKI)))
                {
                    hasRunbook = true;
                    runbookName = outputs.get(Constants.EXECUTE_WIKI);
                }

                // set content to outputs
                content = outputs;
                
                try
                {
                    ExecutionMetrics.INSTANCE.registerEventEnd(processNameForExecutionMetrics);
                }
                catch (ExecutionMetricsException e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
            
            // Convert rbc_source key (if it exists) to RBC_SOURCE key
            if (content.containsKey(LicenseEnum.RBC_SOURCE_LC.getKeygenName()) && 
                (StringUtils.isNotBlank(content.get(LicenseEnum.RBC_SOURCE_LC.getKeygenName())))) {
                content.put(LicenseEnum.RBC_SOURCE.getKeygenName(), content.get(LicenseEnum.RBC_SOURCE_LC.getKeygenName()));
            }
            
            content.remove(LicenseEnum.RBC_SOURCE_LC.getKeygenName());
            
            // If filter script sets the RBC_SOURCE then do not overwrite        
            if (!content.containsKey(LicenseEnum.RBC_SOURCE.getKeygenName()) || 
                (StringUtils.isBlank(content.get(LicenseEnum.RBC_SOURCE.getKeygenName())))) {
                content.put(LicenseEnum.RBC_SOURCE.getKeygenName(), source);
            }
            
            // Convert rbc_queue key (if it exists) to RBC_QUEUE key
            if (content.containsKey(LicenseEnum.RBC_QUEUE_LC.getKeygenName()) && 
                (StringUtils.isNotBlank(content.get(LicenseEnum.RBC_QUEUE_LC.getKeygenName())))) {
                content.put(LicenseEnum.RBC_QUEUE.getKeygenName(), content.get(LicenseEnum.RBC_QUEUE_LC.getKeygenName()));
            }
            
            content.remove(LicenseEnum.RBC_QUEUE_LC.getKeygenName());
            
            // If filter script sets the RBC_QUEUE then do not overwrite
            if (!content.containsKey(LicenseEnum.RBC_QUEUE.getKeygenName()) || 
                (StringUtils.isBlank(content.get(LicenseEnum.RBC_QUEUE.getKeygenName())))) {
                content.put(LicenseEnum.RBC_QUEUE.getKeygenName(), getQueueName());
            }
            
            // Set org name (if configured) as RESOLVE.ORG_NAME but do not overwrite
            
            if (!content.containsKey(Constants.EXECUTE_ORG_NAME) &&
                StringUtils.isNotBlank(((com.resolve.rsremote.ConfigGeneral)MainBase.main.configGeneral).getOrg()))
            {
                content.put(Constants.EXECUTE_ORG_NAME, 
                            ((com.resolve.rsremote.ConfigGeneral)MainBase.main.configGeneral).getOrg());
            }
            
            Map<String, String> response = content;
            
            // RunbookId or EventId set by groovy script is never overwritten by RR
            
            if (!hasRunbook && !hasEvent)
            {
                response = checkRoutingSchemas(content);
            }
            
            RoutingRule rule = null;
            Boolean sirEnabled = null;
            if (!response.isEmpty() && response.get(Constants.EXECUTE_SIR_ENABLED) != null) {
                sirEnabled = Boolean.parseBoolean(response.get(Constants.EXECUTE_SIR_ENABLED));
            }
            hasSIR = (sirEnabled != null && sirEnabled);
            if (hasSIR) {
                List<TreeSet<RoutingSchema>> routingSchemaList = getRoutingSchemas();
                String rid = response.get(Constants.EXECUTE_RR_RID);
                
                for(TreeSet<RoutingSchema> routingSchemaSet : routingSchemaList)
                {
                    for (RoutingSchema routingSchema : routingSchemaSet)
                    {
                        if (routingSchema.getId().equals(response.get(Constants.EXECUTE_RR_SCHEMA_ID))) {
                            rule = routingSchema.getRoutingRules().get(rid.toLowerCase());
                            break;
                        }
                    }
                }
                
                if (rule != null) {
                    // Macro substitution
                    MacroSubstitution ms = new MacroSubstitution();
                    ms.addAllMappings(content);
                    // TODO : need to implement mapping in cases when none on the folowing parameters is not present
                    // create a compound reference containing all 3 possible components: reference, alertid and correlationid
                    // only if there is at least reference available in the request
                    String reference = content.get(Constants.EXECUTE_REFERENCE);
                    
                    if (StringUtils.isBlank(reference))
                    {
                        reference = content.get(Constants.EXECUTE_REFERENCE.toLowerCase());
                    }
                    
                    String alertId = content.get(Constants.EXECUTE_ALERTID);
                    
                    if (StringUtils.isBlank(alertId))
                    {
                        alertId = content.get(Constants.EXECUTE_ALERTID.toLowerCase());
                    }
                    
                    String correlationId = content.get(Constants.EXECUTE_CORRELATIONID);
                    
                    if (StringUtils.isBlank(correlationId))
                    {
                        correlationId = content.get(Constants.EXECUTE_CORRELATIONID.toLowerCase());
                    }
                    
                    try {
                        Map<String, Object> sirParams = new HashMap<String, Object>();
                        sirParams.put(Constants.EXECUTE_SIR_SOURCE_SYSTEM, ms.substitute(rule.getSirRRSettings().getSource(), false, true));
                        sirParams.put(Constants.EXECUTE_SIR_TITLE, ms.substitute(rule.getSirRRSettings().getTitle(), false, true));
                        sirParams.put(Constants.EXECUTE_SIR_TYPE, Constants.EXECUTE_SIR_SECURITY_TYPE);
                        sirParams.put(Constants.EXECUTE_SIR_INVESTIGATION_TYPE, ms.substitute(rule.getSirRRSettings().getType(), false, true));
                        sirParams.put(Constants.EXECUTE_SIR_PLAYBOOK, rule.getSirRRSettings().getPlaybook());
                        sirParams.put(Constants.EXECUTE_SIR_SEVERITY, ms.substitute(rule.getSirRRSettings().getSeverity(), false, true));
                        sirParams.put(Constants.EXECUTE_SIR_OWNER, rule.getSirRRSettings().getOwner());
                        sirParams.put(Constants.EXECUTE_REFERENCE, reference);
                        sirParams.put(Constants.EXECUTE_ALERTID, alertId);
                        sirParams.put(Constants.EXECUTE_CORRELATIONID, correlationId);
                        
                        if (StringUtils.isNotBlank(content.get(Constants.EXECUTE_ORG_NAME)))
                        {
                            sirParams.put(Constants.EXECUTE_ORG_NAME, content.get(Constants.EXECUTE_ORG_NAME));
                        }
                        
                        // RBA-14934 : Save All Data from a Gateway to an Investigation Record in SIR case - serialize in JSON format
                        JSONObject jsonContent = new JSONObject();
                        jsonContent.putAll(content);
                        sirParams.put(Constants.EXECUTE_SIR_INITIAL_REQUEST_DATA, jsonContent.toString());
                        
                        // check for problem ID based on reference, alertId and correlationId (Gateway RR use case) in that order
                        
                        Map<String, Object> esbSearchWSRsp = MainBase.getESB().call(Constants.ESB_NAME_RSCONTROL, "MAction.searchWSByReferences", sirParams, GLOBAL_SEND_TIMEOUT);
                        
                        if (esbSearchWSRsp != null && !esbSearchWSRsp.isEmpty() && esbSearchWSRsp.size() > 1)
                        {
                            Log.log.warn(gatewayClassName +": Expected single worksheet for" + 
                                         (StringUtils.isNotBlank(reference) ? " reference = " + reference : "") +
                                         (StringUtils.isNotBlank(alertId) ? " alertId = " + alertId : "") +
                                         (StringUtils.isNotBlank(correlationId) ? " correlationId = " + correlationId : "") +
                                         " but instead received " + esbSearchWSRsp.size() + " worksheets, picking first one!!!");
                        }
                        
                        if (esbSearchWSRsp != null && esbSearchWSRsp.containsKey(Constants.EXECUTE_SIR_PROBLEMID_ENFORSED_BY_UI_RR + "1") &&
                            StringUtils.isNotBlank((String)esbSearchWSRsp.get(Constants.EXECUTE_SIR_PROBLEMID_ENFORSED_BY_UI_RR + "1")))
                        {
                            sirParams.put(Constants.EXECUTE_SIR_PROBLEMID_ENFORSED_BY_UI_RR, (String)esbSearchWSRsp.get(Constants.EXECUTE_SIR_PROBLEMID_ENFORSED_BY_UI_RR + "1"));
                        }
                        
                        Map<String, Object> esbResponse = MainBase.getESB().call(Constants.ESB_NAME_RSCONTROL, "MAction.createSIRAndUpdateWSReferenceDetails", sirParams, GLOBAL_SEND_TIMEOUT);
                        String problemIdEnforced = esbResponse != null ? (String) esbResponse.get(Constants.EXECUTE_PROBLEMID_ENFORCED_BY_SIR) : null;
                        if (problemIdEnforced != null) {
                            // making sure the upcoming automation is executing into the same worksheet 
                            content.put(Constants.EXECUTE_PROBLEMID_ENFORCED_BY_SIR, problemIdEnforced);
                        }
                    } catch(Exception e) {
                        Log.log.error(gatewayClassName +": ERROR occured details -->" + e);
                        result = e.getLocalizedMessage();
                        return result;
                    }
                }
            }
            eventId = response.get(Constants.EXECUTE_EVENTID);
            runbookName = response.get(Constants.EXECUTE_WIKI);
            
            //check if the incoming params Map has wiki or event id.
            hasEvent = StringUtils.isNotBlank(eventId);
            hasRunbook = StringUtils.isNotBlank(runbookName);
            
            if (content != null)
            {
                //still no event, get it from the filter
                if (StringUtils.isBlank(eventId))
                {
                    // Oracle DB returns "undefined" for the null value so this comparison is necessary.
                    if (StringUtils.isNotBlank(filter.getEventEventId()) && !"undefined".equalsIgnoreCase(filter.getEventEventId()))
                    {
                        hasEvent = true;
                        eventId = filter.getEventEventId();
                        content.put(Constants.EXECUTE_EVENTID, eventId);
                    }
                }

                //still no runbook, get it from the filter
                if (StringUtils.isBlank(runbookName))
                {
                    // Oracle DB returns "undefined" for the null value so this comparison is necessary.
                    if (StringUtils.isNotBlank(filter.getRunbook()) && !"undefined".equalsIgnoreCase(filter.getRunbook()))
                    {
                        
                        hasRunbook = true;
                        runbookName = filter.getRunbook();
                        content.put(Constants.EXECUTE_WIKI, runbookName);
                        Log.log.info(gatewayClassName+": Resolution Routing rules did not match for an event. Filter runbook "+runbookName +" will be triggered.");
                    }
                }
            }
            
            // event has higher precedence
            if (hasEvent)
            {
                try
                {
                    if (StringUtils.isBlank(content.get(Constants.EXECUTE_USERID)))
                    {
                        content.put(Constants.EXECUTE_USERID, "system");
                    }
                    
                    if(params.containsKey(Constants.EXECUTE_INCIDENT_ID)){
                        String incidentId = (String) params.get(Constants.EXECUTE_INCIDENT_ID);
                        IncidentTypeChangedData incidentTypeChangedData = new IncidentTypeChangedData(incidentId, eventId);
                        dcsIncidentEventLogger.log(incidentTypeChangedData, EventType.INCIDENT_TYPE_CHANGED_EVENT.getName());
                    }

                    //the special thing about the event is, the content Map must have
                    //the EVENT_REFERENCE in it. The reference comes from the data
                    //or assigned through filter script etc.
                    MainBase.esb.sendEvent(content);
                }
                catch (Exception e)
                {
                    Log.log.error(gatewayClassName +": Failed to send Event: " + e.getMessage(), e);
                }
            }
            else if (hasRunbook)
            {
                if(params.containsKey(Constants.EXECUTE_INCIDENT_ID)){
                    
                    String rootRunbookId = UUID.randomUUID().toString();
                    content.put(Constants.EXECUTE_ROOT_RUNBOOK_ID, rootRunbookId);
                    String incidentId = (String) params.get(Constants.EXECUTE_INCIDENT_ID);
                    IncidentTypeChangedData incidentEventData = new IncidentTypeChangedData(incidentId, runbookName);
                    dcsIncidentEventLogger.log(incidentEventData, EventType.INCIDENT_TYPE_CHANGED_EVENT.getName());
                }
                Log.log.debug(gatewayClassName +": Processing Filter RunbookName --> " + (StringUtils.isNotBlank(runbookName) ? runbookName : "") +
                        ", Event Id --> " + (StringUtils.isNotBlank(eventId) ? eventId : ""));
                if(filter.getBlockingCode() > 2)
                {
                    result = executeRunbook(filter, runbookName, content);
                }
                else
                {
                    executeRunbook(filter, runbookName, content);
                    if(StringUtils.isEmpty(result))
                        result = "Runbook execution request is sent.";
                }
                
                // Handling JSONObject as a return type from the Groovy script
                // Here one can implement even more return types
                if (isJson && groovyResult != null &&
                // supporting both JSON implementations - net.sf.json.JSONObject and com.google.gson.JsonObject
                        (groovyResult instanceof JSONObject || groovyResult instanceof JsonObject)) {
                    result = groovyResult.toString();
                    // supporting also plain text response (not wrapped up) - com.resolve.gateway.response.PlainTextResponse
                } else if (groovyResult instanceof PlainTextResponse) {
                    result = groovyResult.toString();
                } else if (isJson) {
                    try {
                        JSONObject json = JSONObject.fromObject(result);
                    } catch(Exception e) {
                        JSONObject message = new JSONObject();
                        message.accumulate("Message", result);
                        result = message.toString();
                    }
                }
            }
            else
            {
                Log.log.debug(gatewayClassName + ": No runbook defined - discarding");
                if (StringUtils.isEmpty(result)) {
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "No runbook defined.");
                    result = message.toString();
                } else {
                    // Handling JSONObject as a return type from the Groovy script
                    // Here one can implement even more return types
                    if (isJson && 
                            groovyResult != null && 
                            // supporting both JSON implementations - net.sf.json.JSONObject and com.google.gson.JsonObject
                            (groovyResult instanceof JSONObject || groovyResult instanceof JsonObject)) {
                        result = groovyResult.toString();
                        // supporting also plain text response (not wrapped up) - com.resolve.gateway.response.PlainTextResponse
                    } else if (groovyResult instanceof PlainTextResponse) { 
                        result = groovyResult.toString();
                    } else if (isJson) {
                        try {
                            JSONObject json = JSONObject.fromObject(result);
                        } catch(Exception e) {
                            JSONObject message = new JSONObject();
                            message.accumulate("Message", result);
                            result = message.toString();
                        }
                    }
                }
                
                /*
                 * This message is set to RSCONTROL to log the event in ES with default RBC.
                 */
                MainBase.getESB().sendMessage(Constants.ESB_NAME_RSCONTROL, "MAction.logNoRBEvent", content);
            }
            return result;
        } finally {
            Log.clearCurrentNamedContext(Constants.GATEWAY_FILTER);
        }
    }

    private Map<String, String> checkRoutingSchemas(final Map<String, String> params)
    {
        Map<String, String> result = new HashMap<String, String>();
        List<TreeSet<RoutingSchema>> routingSchemaSetList = getRoutingSchemas();
        boolean ruleFound = false;
        
        if (routingSchemaSetList != null && !routingSchemaSetList.isEmpty())
        {
            for (TreeSet<RoutingSchema> routingSchemaSet : routingSchemaSetList)
            {
                if (!routingSchemaSet.isEmpty())
                {
                    for (RoutingSchema routingSchema : routingSchemaSet)
                    {
                        String rid = routingSchema.synthesizeRid(params);
                        
                        //lookup
                        if (StringUtils.isNotBlank(rid))
                        {
                            RoutingRule rule = null;
                            Log.log.debug(gatewayClassName +": RID to be used: " + rid);
                            rule = routingSchema.getRoutingRules().get(rid.toLowerCase());
                            if (rule != null)
                            {
                                ruleFound = true;
                                result.put(Constants.EXECUTE_RR_SCHEMA_ID, routingSchema.getId());
                                result.put(Constants.EXECUTE_RR_RID, rid);
                                if (rule.getSirRRSettings() == null) {
                                    result.put(Constants.EXECUTE_SIR_ENABLED, Boolean.FALSE.toString());
                                    result.put(Constants.EXECUTE_WIKI, rule.getGatewayRRSettings().getAutomation());
                                    result.put(Constants.EXECUTE_EVENTID, rule.getGatewayRRSettings().getEventId());
                                    
                                } else {
                                    result.put(Constants.EXECUTE_SIR_ENABLED, Boolean.TRUE.toString());
                                    result.put(Constants.EXECUTE_SIR_TITLE, rule.getSirRRSettings().getTitle());
                                    result.put(Constants.EXECUTE_SIR_TYPE, rule.getSirRRSettings().getType());
                                    result.put(Constants.EXECUTE_SIR_PLAYBOOK, rule.getSirRRSettings().getPlaybook());
                                    result.put(Constants.EXECUTE_SIR_SEVERITY, rule.getSirRRSettings().getSeverity());
                                    result.put(Constants.EXECUTE_SIR_OWNER, rule.getSirRRSettings().getOwner());
                                    // according to the way UI looks, the automation in this case should be the one specified for the GW
                                    result.put(Constants.EXECUTE_WIKI, rule.getGatewayRRSettings().getAutomation());
                                }
                                if(Log.log.isDebugEnabled())
                                {
                                    Log.log.debug(gatewayClassName + ": Found automation from RR rule: " + rule.getGatewayRRSettings().getAutomation());
                                    Log.log.debug(gatewayClassName + ": Found event ID from RR rule: " + rule.getGatewayRRSettings().getEventId());
                                    Log.log.debug(gatewayClassName +": Matched schema --> " + routingSchema.toString() + ", and rule --> " + rule);
                                    Log.log.debug(gatewayClassName +": Runbook Execution results --> " + result != null ? 
                                    		result.toString() : "No Results were returned from Runbook execution");
                                }
                                Log.log.info(gatewayClassName + ": Matched a rule for an event in schema "+routingSchema.getName()+". Resolution Routing runbook with name "+rule.getGatewayRRSettings().getAutomation()+" will be executed");
                                break;
                            }
                            else
                            {
                                Log.log.debug(gatewayClassName +": Did not find any rule for RID : " + rid + " in the schema : " + routingSchema.getName());
                            }
                        }
                    }
                    if (ruleFound)
                    {
                        break;
                    }
                }
            }
        }
        
        
        return result;
    }

    /**
     * This method is overridden by subclass who needs to clean up resources
     * like DB connection (e.g., Netcool)
     * 
     * @param binding
     */
    protected void groovyScriptBindingCleanup(Binding binding)
    {
    }

    /**
     * Puts additional bindings to the groovy script by subclasses (refer
     * NetcoolGateway for DB object).
     * 
     * @param binding
     */
    protected void addAdditionalBindings(String script, Binding binding)
    {
    }
    
    @SuppressWarnings("unchecked")
    protected String executeRunbook(Filter filter, String wikiName, Map<String, String> params)
    {
        String resultStr = "";
        
        boolean runBookDone = false;
        // In case the execution needs to go into the same worksheet where SIR has been created
        String problemIdEnforced = (String) params.get(Constants.EXECUTE_PROBLEMID_ENFORCED_BY_SIR);
        if (problemIdEnforced != null) {
            params.put(Constants.EXECUTE_PROBLEMID, problemIdEnforced);
        }
        //
        
        Map<String, Object> event = new HashMap<String, Object>();
        // event.put(filter.getId(), result);
        event.putAll(params);
        event.put("WIKI_PARAMS", params);
        
        if (StringUtils.isEmpty(wikiName))
        {
            event.put(Constants.EXECUTE_WIKI, filter.getRunbook());
        }
        else
        {
            event.put(Constants.EXECUTE_WIKI, wikiName);
        }
        
        if(params.containsKey(GATEWAY_NAME) && params.containsKey(GATEWAY_TYPE)) {
            
            ResolveGateway gateway = getGateway(params.get(GATEWAY_NAME), params.get(GATEWAY_TYPE));

            if(gateway != null)
                event.put(QUEUE, gateway.getTopicName());
        }
        
        else        
            event.put(QUEUE, topicName);

        GatewayEvent<String, Object> gwEvent = new GatewayEvent<String, Object>(System.currentTimeMillis(), event);
        
        int blocking = filter.getBlockingCode();
        Log.log.info(gatewayClassName +": Executing Runbook --> " + wikiName + " for the gateway");
        if (blocking > 2)
        {
            String problemid                = (String) params.get(Constants.EXECUTE_PROBLEMID);                  // required
//            String userId                   = (String) params.get(Constants.EXECUTE_USERID);                     // required
//            String wiki                     = (String) params.get(Constants.EXECUTE_WIKI);                       // required
            String processid                = (String) params.get(Constants.EXECUTE_PROCESSID);                  // optional
//            String reference                = (String) params.get(Constants.EXECUTE_REFERENCE);                  // optional
//            String alertId                  = (String) params.get(Constants.EXECUTE_ALERTID);                    // optional

            params.put("EVENT_TYPE", "RSREMOTE");
            params.put("WIKI", wikiName);
            params.put("ACTION", "EXECUTEPROCESS");
            params.put("USERID", "admin");
            
            if (problemid == null)
            {
                problemid = Constants.PROBLEMID_NEW;
                processid = Constants.PROBLEMID_NEW;
            }
            
            params.put(Constants.EXECUTE_PROBLEMID, problemid);
            params.put(Constants.EXECUTE_PROCESSID, processid);
            
            try 
            {
                if (blocking == 3)
                { // Get worksheet id back and call execution of runbook from RSView
                    Map<String, Object> result = MainBase.getESB().call(Constants.ESB_NAME_RSVIEW, "MAction.executeRunbook", params, GLOBAL_SEND_TIMEOUT);
                    
                    runBookDone = true;
                    resultStr = (String)result.get("RESULT");
                    
                    if (StringUtils.isNotEmpty(resultStr))
                    {
                        JSONObject json = new JSONObject();
                        
                        String[] lines = resultStr.split("\\n");
                        if (lines != null && lines.length > 1)
                        {
                            for (String line:lines)
                            {
                                String[] keyValues = line.split(":");
                                if (keyValues != null && keyValues.length == 2)
                                {
                                    String key = keyValues[0];
                                    if (key != null)
                                        key = key.trim();
                                    String value = keyValues[1];
                                    if (value != null)
                                        value = value.trim();
                                    json.accumulate(key, value);
                                }
                            }
                        }
                        else
                        {
                            json.accumulate("message", "No result is available at this moment. Please check it later.");
                        }
                        
                        resultStr = json.toString();
                    }
                    else
                        resultStr = "";
                }
                else
                { // Call execution runbook from RSControl and get the condition and severity back
                    Map<String, Object> result = MainBase.getESB().call(Constants.ESB_NAME_RSCONTROL, "MAction.executeRunbook", params, GLOBAL_SEND_TIMEOUT);
                    runBookDone = true;
                    if (result != null)
                        resultStr = parseRunbookResult(result, wikiName);
                    Log.log.info(gatewayClassName +": Raw Runbook Execution results --> " + resultStr != null ? 
                    		resultStr : "No Results were returned from Runbook execution");
                }
                
            }
            catch(Exception e) 
            {
                Log.log.error(gatewayClassName +": ERROR execution runbook --> " + e.toString());
                resultStr = "Failed to execute runbook.";
            }
        }
        
        if(params.containsKey(GATEWAY_NAME) && params.containsKey(GATEWAY_TYPE)) {

            ResolveGateway gateway = getGateway(params.get(GATEWAY_NAME), params.get(GATEWAY_TYPE));

            if(gateway != null)
                gateway.getWorkerDataQueue().offer(gwEvent);
        }
        
        else if(runBookDone == false) {
            workerDataQueue.offer(gwEvent);
        }
        return resultStr;
        
    } // executeRunbook()
    
    @SuppressWarnings("unchecked")
    public String parseRunbookResult(Map<String, Object> result, String wiki)
    {
        String problemId = (String)result.get(Constants.EXECUTE_PROBLEMID);
        String processId = (String)result.get(Constants.EXECUTE_PROCESSID);
        String number = (String)result.get(Constants.WORKSHEET_NUMBER);
        String status = (String)result.get(Constants.STATUS);
        String condition = (String)result.get(Constants.EXECUTE_CONDITION);
        String severity = (String)result.get(Constants.EXECUTE_SEVERITY);
        List<Map<String, String>> taskStatus = (List<Map<String, String>>)result.get(Constants.TASK_STATUS);

        JSONObject json = new JSONObject();
        
        try
        {
            json.accumulate("Wiki Runbook", wiki);
            json.accumulate("Worksheet Id", problemId);
            json.accumulate("Worksheet Number", number);
            json.accumulate("Process Id", processId);
            json.accumulate("Condition", condition);
            json.accumulate("Severity", severity);
            json.accumulate("Status", status);
            
            if (StringUtils.isNotEmpty(status) && !status.equalsIgnoreCase(Constants.EXECUTE_STATUS_UNKNOWN))
            {
                JSONArray tasks = new JSONArray();
                
                for (Map<String, String> task:taskStatus)
                {
                    JSONObject taskItem = new JSONObject();
                    taskItem.accumulate("name", task.get(Constants.EXECUTE_TASKNAME));
                    taskItem.accumulate("id", task.get(Constants.EXECUTE_ACTIONID));
                    taskItem.accumulate("completion", task.get(Constants.EXECUTE_COMPLETION));
                    taskItem.accumulate("condition", task.get(Constants.EXECUTE_CONDITION));
                    taskItem.accumulate("severity", task.get(Constants.EXECUTE_SEVERITY));
                    taskItem.accumulate("summary", task.get(Constants.EXECUTE_SUMMARY));
                    taskItem.accumulate("detail", task.get(Constants.EXECUTE_DETAIL));
                    
                    tasks.add(taskItem);
                }
            
                json.accumulate("Action Tasks", tasks);
            }
        }
        catch(Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        Log.log.info(gatewayClassName + ": Received Runbook Results " + json != null ? json.toString(): "Nothing returned");
        return json.toString();
        
    } // parseRunbookResult()

    /**
     * This sets the runbook parameter to the Map, the interesting part here is,
     * if the gateway is configured to make the Map's keys uppercase then it
     * will be done here otherwise it will keep whatever form it came in.
     */
    private Map<String, String> setRunbookParam(Map<String, String> params)
    {
        Map<String, String> result = params;

        if (isUppercase())
        {
            Map<String, String> params1 = new HashMap<String, String>();
            if (params != null)
            {
                for (String key : params.keySet())
                {
                    params1.put(key.toUpperCase(), params.get(key));
                }
                result = params1;
            }
        }
        return result;
    }

    /**
     * Processes events collected by processFilter. These events are mainly
     * runbook execution.
     */
    protected boolean processFilterEvents(long startTime, GatewayEvent<String, Object> event) throws Exception
    {
        boolean result = true;
        if (Log.log.isTraceEnabled())
        {
            Log.log.trace(gatewayClassName + ": Event received to process :" + event.content);
        }
        // check if events are timed out before try to send it to global queue.
        if ((MessageDispatcher.MAXIMUM_EVENT_WAITING_TIME == 0) || (startTime - event.timeStamp) < MessageDispatcher.MAXIMUM_EVENT_WAITING_TIME)
        {
            if (MessageDispatcher.sendMessage(esbMessageHeader, event, GLOBAL_SEND_TIMEOUT))
            {
                Log.log.trace(gatewayClassName +": Send execute runbook message to " + esbMessageHeader.getTarget());
            }
            else
            {
                result = false;
                Log.log.warn(gatewayClassName + ": Could not send execute runbook message to " + esbMessageHeader.getTarget());
            }
        }
        return result;
    }

    @Override
    public void removeFilter(String id)
    {
        Filter filter = filters.get(id);
        if (filter != null)
        {
            // remove from filters
            filters.remove(id);

            // create new orderedFilters
            synchronized (BaseClusteredGateway.class)
            {
                List<Filter> syncedOrderededFilters = new ArrayList<Filter>(filters.values());
                Collections.sort(syncedOrderededFilters);
                
                orderedFilters.clear();
                orderedFilters.addAll(syncedOrderededFilters);
                orderedFiltersMapById.remove(id);
            }
        }
    } // removeFilter

    @Override
    public Map<String, Filter> getFilters()
    {
        return filters;
    } // getFilters

    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList)
    {
        Log.log.info(gatewayClassName + ": Incoming filters:" + filterList);

        // find the username, if the username is not available then use system
        String username = "system";
        for (Map<String, Object> filter : filterList)
        {
            if (filter.containsKey("RESOLVE_USERNAME"))
            {
                username = (String) filter.get("RESOLVE_USERNAME");
                break;
            }
        }

        ConcurrentHashMap<String, Filter> originalFilters = new ConcurrentHashMap<String, Filter>();
        for (String key : filters.keySet())
        {
            originalFilters.put(key, filters.get(key));
        }

        // remove all filters
        filters.clear();
        Log.log.debug(gatewayClassName + ": Cleared original " + originalFilters.size() + " filters");

        // set filters
        for (Map<String, Object> params : filterList)
        {
            // ignore the username Map
            if (!params.containsKey("RESOLVE_USERNAME"))
            {
                String name = (String) params.get("ID");
                String intervalString = (String) params.get("INTERVAL");

                // add filter
                Filter filter = getFilter(params);

                /*
                 * Check if we need to retain some values. This means that even
                 * if a filteris redeployed we want to keep its existing value.
                 * Check the reference of"RetainValue" annotation
                 * RemedyxFilter.getLastValue() method.By using the annotation
                 * it becomes pretty simple to do across the board.Note that if
                 * the filter was undeployed and redeployed it will lose the
                 * value
                 */
                Filter originalFilter = originalFilters.get(filter.getId());
                if (originalFilter != null)
                {
                    Method[] methods = filter.getClass().getMethods();
                    for (Method method : methods)
                    {
                        RetainValue retainValue = method.getAnnotation(RetainValue.class);
                        if (retainValue != null)
                        {
                            try
                            {
                                // from gateway filter perspective we guarantee
                                // that
                                // filters are proper java bean and there are
                                // getter and setter method.
                                String setMethodName = method.getName().replaceFirst("get", "set");
                                Object value = method.invoke(originalFilter);
                                if (value != null) // if the value is null
                                                   // ignore.
                                {
                                    Method setMethod = filter.getClass().getMethod(setMethodName, method.getReturnType());
                                    setMethod.invoke(filter, value);
                                }
                            }
                            catch (Exception e)
                            {
                                // this is just a warning that we couldn't
                                // retain value but system shouldn't stop
                                Log.log.warn(e.getMessage(), e);
                            }
                        }
                    }
                    // delete the filter from original filters Map.
                    originalFilters.remove(filter.getId());
                }

                filters.put(name, filter);
                undeployedFilterIds.remove(filter.getId());

                // send a gateway alert for record
                sendAlert(ALERT_SEVERITY_INFO, false, "Filter " + name + " deployed by " + username, StringUtils.objectToLogString(filter, "getMatcher"));

                // update gateway minimum interval
                int newInterval = Integer.parseInt(intervalString) * 1000;
                if (newInterval <= 0)
                {
                    Log.log.error(gatewayClassName + ": Gateway filter interval must be greater than 0. Invalid interval, " + newInterval + " , will be ignored and will keep old or default interval: " + interval);
                }
            }
        }

        // at this point find out if there are any filter(s) which got deleted and send
        // an alert about them
        for (String key : originalFilters.keySet())
        {
            // send a gateway alert for record
            sendAlert(ALERT_SEVERITY_INFO, false, "Filter " + key + " deleted by " + username, StringUtils.objectToLogString(originalFilters.get(key), "getMatcher"));
            Log.log.debug(gatewayClassName + ": Undeployed Filter Id --> " + key);
            undeployedFilterIds.add(key);
        }

        // clean any resources to be cleared out when filters are undeployed.
        // sub classes will implement this method. at this point originalFilters
        // contains only the dweletable filters.

        cleanFilter(originalFilters);

        // create new orderedFilters
        synchronized (orderedFilters)
        {
            List<Filter> syncedOrderededFilters = new ArrayList<Filter>(filters.values());
            Collections.sort(syncedOrderededFilters);
            
            orderedFilters.clear();
            orderedFilters.addAll(syncedOrderededFilters);
            
            orderedFiltersMapById.clear();
            
            for (Filter filter : orderedFilters)
            {
                orderedFiltersMapById.put(filter.getId(), filter);
            }
        }
    } // clearAndSetFilters

    /**
     * Clean any resources to be cleared out when filters are undeployed. sub
     * classes will implement this method.
     * 
     * @param originalFilters
     */
    protected void cleanFilter(ConcurrentHashMap<String, Filter> originalFilters)
    {
        // let the subclass override this
    }
    
    @Override
    public void setFilterActive(String id, boolean active)
    {
        Filter filter = filters.get(id);
        if (filter != null)
        {
            filter.setActive(active);
        }
    } // setFilterActive

    @Override
    public NameProperties getNameProperties()
    {
        return nameProperties;
    }

    @Override
    public List<TreeSet<RoutingSchema>> getRoutingSchemas()
    {
        return routingSchemas;
    }

    private void sendMessage(String name, Map<String, String> raw)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("QUEUE", getQueueName());
        params.put("NAME", name);
        if (raw != null)
        {
            // params.put("CONTENT", StringUtils.mapToString(raw));
            params.put("CONTENT", raw);
        }
        MainBase.esb.sendInternalMessage("RSCONTROL", getMessageHandlerName() + ".setNameProperties", params);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public synchronized void setNameProperties(String name, Map prop, boolean doSave)
    {
        Log.log.debug("Saving name properties for " + name);
        getNameProperties().setNameProperties(name, prop);

        if (doSave)
        {
            ObjectProperties props = new ObjectProperties(prop);
            // send message to RSCONROL
            Log.log.debug(gatewayClassName + ":: Sending name property " + name + " to RSCONTROL for persistence.");
            sendMessage(name, props.getRaw());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public synchronized void setNameProperty(String name, String key, String value)
    {
        Log.log.debug(gatewayClassName + ": Saving name properties for " + name);
        getNameProperties().getNameProperties(name).put(key, value);

        setNameProperties(name, getNameProperties().getNameProperties(name), true);
    }

    @Override
    public synchronized void removeNameProperties(String name)
    {
        Log.log.debug(gatewayClassName+":Removing name properties for " + name);
        getNameProperties().removeNameProperties(name);

        String filename = MainBase.main.getProductHome() + gatewayConfigDir + "/" + name + ".properties";
        try
        {
            FileUtils.forceDelete(new File(filename));
        }
        catch (FileNotFoundException fnfe)
        {
            Log.log.error(fnfe.getMessage(), fnfe);
        }
        catch (IOException ioe)
        {
            Log.log.error(ioe.getMessage(), ioe);
        }

        // simply send the message without content, this will gurantee removal
        Log.log.debug(gatewayClassName + ": Sending name property " + name + " to RSCONTROL to remove.");
        sendMessage(name, null);
    }

    @Override
    public synchronized void removeNameProperty(String name, String key)
    {
        Log.log.debug("Removing name properties for " + name + " with key " + key);
        getNameProperties().getNameProperties(name).remove(key);
        // save it
        setNameProperties(name, getNameProperties().getNameProperties(name), true);
    }

    @SuppressWarnings("unchecked")
    @Override
    public synchronized void clearAndSetNameProperties(List<Map<String, Object>> paramList)
    {
        if (paramList != null && paramList.size() > 0)
        {
            for (Map<String, Object> paramMap : paramList)
            {
                String name = (String) paramMap.get("NAME");
                Log.log.debug(gatewayClassName + ": Loading Name Properties for --> " + name);
                if (StringUtils.isNotBlank(name))
                {
                    Map<String, Object> prop = new HashMap<String, Object>();
                    // String content = paramMap.get("CONTENT");
                    // Map<String, String> contentMap =
                    // StringUtils.stringToMap(content);
                    HashMap<String, String> contentMap = (HashMap<String, String>) ObjectProperties.deserialize((byte[]) paramMap.get("CONTENT"));
                    
                    if (contentMap != null && contentMap.size() > 0)
                    {
                        for (String key : contentMap.keySet())
                        {
                            Object obj = ObjectProperties.loadObject(contentMap.get(key));
                            if (obj == null)
                            {
                                // just a simple String property
                                prop.put(key, contentMap.get(key));
                                Log.log.debug(gatewayClassName + ": String value for key : " + key);
                            }
                            else
                            {
                                prop.put(key, obj);
                                Log.log.debug(gatewayClassName + ": Object value for key : " + key + ", type : " + obj.getClass());
                            }
                        }
                        getNameProperties().setNameProperties(name, prop);
                    }
                    else
                    {
                        Log.log.error(gatewayClassName + ": NAME PROPERTY " + name + " is missing its content, an empty map will be loaded for it");
                        getNameProperties().setNameProperties(name, prop);
                    }
                }
            }
        }
        else
        {
            Log.log.debug(gatewayClassName + ": There is no name properties for gateway --> " + getQueueName());
        }
    }

    /**
     * For all the key's in the Map, refer to getRoutingSchemas(String queueName) method in MGateway class from RSControl project.
     */
    @SuppressWarnings("unchecked")
    @Override
    public synchronized void clearAndSetRoutingSchemas(Map<String, Object> orgRRSchemaMap)
    {
        // clear and get ready to refresh
        routingSchemas.clear();
        
        List<String> orgHierarchy = (List<String>)orgRRSchemaMap.get("ORG_HIRARCHY");
        
        // for no org rsremote, this list will be null
        if (orgHierarchy == null)
        {
            // remove non-schema related key-value from map.
            orgRRSchemaMap.remove("ORG_HIRARCHY");
            
            // Map has only one entry now.
            Set<String> queueName = orgRRSchemaMap.keySet();
            
            List<Map<String, Object>> paramList = (List<Map<String, Object>>)orgRRSchemaMap.get(queueName.iterator().next());
            routingSchemas.add(populateRoutingSchemaList(paramList));
        }
        else
        {
            for (String queueName : orgHierarchy)
            {
                List<Map<String, Object>> paramList = (List<Map<String, Object>>)orgRRSchemaMap.get(queueName);
                routingSchemas.add(populateRoutingSchemaList(paramList));
            }
        }
    }
    
    private TreeSet<RoutingSchema> populateRoutingSchemaList(List<Map<String, Object>> paramList)
    {
        TreeSet<RoutingSchema> result = new TreeSet<RoutingSchema>();
        if (paramList != null && paramList.size() > 0)
        {
            for (Map<String, Object> paramMap : paramList)
            {
                String schemaId = (String) paramMap.get("sys_id");
                Integer schemaOrder = (Integer) paramMap.get("u_order");
                if (StringUtils.isNotBlank(schemaId) && schemaOrder != null)
                {
                    String schemaName = (String) paramMap.get("u_name");
                    Log.log.debug(gatewayClassName + ":Loading Routing Rules for Schema : " + schemaName);
                    String schemaFields = (String) paramMap.get("u_json_fields");
                    RoutingSchema routingSchema = new RoutingSchema(schemaId, schemaName, schemaOrder);
                    routingSchema.setFields(schemaFields, isUppercase());

                    List<Map<String, Object>> rules = (List<Map<String, Object>>) paramMap.get("rules");

                    for (Map<String, Object> rule : rules)
                    {
                        String rid = (String) rule.get("u_rid");
                        String owner = (String) rule.get("u_owner");
                        // String runbook = (String) rule.get("u_runbook");
                        // String eventId = (String) rule.get("u_event_id");
                        // RoutingRule routingRule = new RoutingRule(rid.toLowerCase(), runbook, eventId);
                        String uiPage = (String) rule.get("u_wiki");
                        String uiAutomation = (String) rule.get("u_automation");
                        boolean uiExecuteOnNewWorksheetOnly = false;
                        // RBA-14980 : Starting rsremote throws 'Caused by: java.lang.ClassCastException'
                        if (rule.get("u_new_worksheet") instanceof Boolean) {
                            uiExecuteOnNewWorksheetOnly = (boolean) rule.get("u_new_worksheet");
                        } else if (rule.get("u_new_worksheet") instanceof Double) {
                            uiExecuteOnNewWorksheetOnly = (double) rule.get("u_new_worksheet") >= 1d ? true : false;
                        }
                        UIRRSettings uiRRSettings = new UIRRSettings(uiPage, uiAutomation, uiExecuteOnNewWorksheetOnly);
                        String gwAutomation = (String) rule.get("u_runbook");
                        String gwEventId = (String) rule.get("u_event_id");
                        GatewayRRSettings gatewayRRSettings = new GatewayRRSettings(gwAutomation, gwEventId);
                        Object uSirRaw = rule.get("u_sir");
                        Boolean sir = false;
                        if (uSirRaw instanceof Boolean) {
                            sir = (Boolean) rule.get("u_sir");
                        } else if (uSirRaw instanceof Double) {
                            Double uSirDouble = (Double) uSirRaw;
                            sir = uSirDouble.doubleValue() >= 1.0 ? true : false;
                        }
                        SIRRRSettings sirRRSettings = null;
                        if (sir != null && sir.booleanValue() == true) {
                            String sirSource = (String) rule.get("u_sir_source");
                            String sirTitle = (String) rule.get("u_sir_title");
                            String sirType = (String) rule.get("u_sir_type");
                            String sirPlaybook = (String) rule.get("u_sir_playbook");
                            String sirSeverity = (String) rule.get("u_sir_severity");
                            String sirOwner = (String) rule.get("u_sir_owner");
                            String sirAutomation = (String) rule.get("u_sir_automation");
                            sirRRSettings = new SIRRRSettings(sirSource, sirTitle, sirType, sirPlaybook, sirSeverity, sirOwner, sirAutomation);
                        }
                        RoutingRule routingRule = new RoutingRule.RoutingRuleBuilder(rid, owner).uiRRSettings(uiRRSettings).gatewayRRSettings(gatewayRRSettings).sirRRSettings(sirRRSettings).build();
                        Log.log.debug(gatewayClassName+":Adding " + routingRule.toString());
                        routingSchema.addRoutingRule(routingRule);
                    }

                    if (Log.log.isDebugEnabled())
                    {
                        try
                        {
                            Log.log.debug(gatewayClassName + ": Total number of rules received for schema " + schemaName + " : " + rules.size());
                            Log.log.debug(gatewayClassName + ": Approximate size of rules collection object in bytes : " + ObjectSizeCalculator.getObjectSize(rules));
                            Log.log.debug(gatewayClassName + ": Approximate size of RoutingSchema object in bytes : " + ObjectSizeCalculator.getObjectSize(routingSchema));
                        }
                        catch(UnsupportedOperationException e)
                        {
                            //ignore, just failed
                            Log.log.debug(gatewayClassName + ": This is not a bug, just failed to get the size of the routingSchema object due to following: " + e.getMessage());
                        }
                    }

                    //add it to the set that is sorted by order
                    result.add(routingSchema);
                }
                else
                {
                    Log.log.info(gatewayClassName + ": Invalid schema received from database. Schema Id or Order not provided.");
                }
            }
            Log.log.info(gatewayClassName +": populated "+ result.size() +" schemas");
        }
        
        return result;
    }

    /**
     * This method requests RSCONTROL to provide routing schemas belongs to this gateway.
     */
    @Override
    public void sendRoutingSchemaSyncRequest()
    {
        // get the parameters to be sent to RSControl.
        Map<String, String> params = new HashMap<String, String>();
        params.put("GATEWAY_NAME", getQueueName());
        params.put("QUEUE_NAME", MainBase.main.configId.getGuid());
        params.put("MESSAGE_HANDLER_NAME", "com.resolve.gateway." + getMessageHandlerName());
        params.put("ORG_NAME", getOrgSuffix());

        Log.log.debug(gatewayClassName + ": Sending Gateway routing schema synchronization message to RSCONTROL from " + getQueueName());
        if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_SYSTEM_RSCONTROL, getMessageHandlerName() + ".synchronizeRoutingSchemas", params) == false)
        {
            Log.log.warn(gatewayClassName + ": Failed to send routing schema synchronization message to RSCONTROL");
        }
    }

    @Override
    public void processFilter(Map<String, String> params)
    {
        if (isPrimary())
        {
            Log.log.debug(gatewayClassName + ": Primary gateway received filter processing message, now deligating to worker: " + getQueueName());
            sendToWorker(params);
        }
    }

    @Override
    public Queue<Map<String, String>> getPrimaryDataQueue()
    {
        return primaryDataQueue;
    }

    static class PrimaryDataQueueWorkerThread implements Runnable 
    {
        final ClusteredGateway instance;
        final Filter filter;
        Map<String, String> runbookParams = new HashMap<String, String>();
        private String context;

        public PrimaryDataQueueWorkerThread(ClusteredGateway instance, Filter filter, Map<String, String> runbookParams)
        {
            this.instance = instance;
            this.filter = filter;
            this.runbookParams = runbookParams;
            context = Log.getCurrentContext(); // contextually decorate the command
        }

        @Override
        public void run()
        {
            // Set running context
            Log.setCurrentContext(context);
            Log.putCurrentNamedContext(Constants.GATEWAY_FILTER, filter.getId());

            Log.log.debug(Thread.currentThread().getName() + " thread adding to primary data queue for filter: " + filter.getId());
            processCommand();
        }

        private void processCommand()
        {
            if (!runbookParams.isEmpty()) {
                Map<String, String> cefExtensions = new HashMap<>();
                for (String key : runbookParams.keySet()) {
                    if(!key.equals(Constants.NOTIFICATION_MESSAGE_ATTACHMENTS)){
                        String value = runbookParams.get(key);
                        try {
                            if (CEFParser.isCEF(value)) {
                                CEFEvent event = CEFParser.parse(value);
                                cefExtensions.putAll(event.getExtensions());
                            }
                        } catch (CEFParseException e) {
                            Log.log.error("Unable to parse CEF message", e);
                        }
                    }
                }
                
                if (!cefExtensions.isEmpty()) {
                    runbookParams.putAll(cefExtensions);
                }
            }
            
            if (filter != null)
            {
                runbookParams.put(FILTER_ID_NAME, filter.getId());
            }

            // put into the queue so it's processed asynchronously.
            instance.getPrimaryDataQueue().offer(runbookParams);

            // This method is implemented by subclasses to update various custom
            // fields in the target server. For example in Netcool or HPOM
            // gateway
            // it will update RESOLVE_STATUS and RESOLVE_RUNBOOKID.
            // Moved to Primary Data Queue Listener
            //instance.updateServerData(filter, runbookParams);
        }
    }
    
    @SuppressWarnings("unchecked")
    public void gwWorkerSelfCheck() throws Exception
    {
        boolean isPrimaryAndNotWorker = isPrimary() && !isWorker();
        
        try
        {
            if ((isWorker() || isPrimary()) && gatewayWorkerSelfCheckStarted)
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Type", "Gateway Worker Queue Self-Check");
                params.put(workerQueueName, "Threshold " + ((ConfigReceiveGateway)configurations).getWorkerSCThreshold() + " seconds");
                params.put("Primary", Boolean.toString(isPrimary()));
                params.put("Secondary", Boolean.toString(isSecondary()));
                params.put("Worker", Boolean.toString(isWorker()));
                params.put("FromInstance", MainBase.main.configId.getGuid());
                Map<String, String> response = MainBase.getESB().call(workerQueueName, 
                                                                      (isPrimaryAndNotWorker ? "MRSSelfCheck" : getMessageHandlerName()) + ".selfCheck", 
                                                                      params, ((ConfigReceiveGateway)configurations).getWorkerSCThreshold()*1000);
                
                if (response == null)
                {
                    Log.alert(gatewayClassName + ": Instance " + MainBase.main.configId.getGuid() + " breached " + getGatewayEventType() + " " + workerQueueName + " Self-Check", 
                                    "Instance " + MainBase.main.configId.getGuid() + "Breached " + getGatewayEventType() + 
                              " " + workerQueueName + " self-check threshold of " + ((ConfigReceiveGateway)configurations).getWorkerSCThreshold() + 
                              " seconds!!!");
                }
            }
        }
        finally
        {
            if ((isWorker() || isPrimary()) && !gatewayWorkerSelfCheckStarted)
            {
                // schedule next self-check
                ScheduledExecutor.getInstance().executeRepeat(getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-WorkerSelfCheck", this, "gwWorkerSelfCheck", ((ConfigReceiveGateway)configurations).getWorkerSCInterval(), TimeUnit.SECONDS);
                gatewayWorkerSelfCheckStarted = true;
                Log.log.debug(gatewayClassName + ": Scheduled " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-WorkerSelfCheck to run every " + ((ConfigReceiveGateway)configurations).getWorkerSCInterval() + " seconds");
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    public void gwTopicSelfCheck() throws Exception
    {
        try
        {
            if (gatewayTopicSelfCheckStarted)
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Type", "Gateway Topic Queue Self-Check");
                params.put(topicName, "Threshold " + ((ConfigReceiveGateway)configurations).getTopicSCThreshold() + " seconds");
                params.put("Primary", Boolean.toString(isPrimary()));
                params.put("Secondary", Boolean.toString(isSecondary()));
                params.put("Worker", Boolean.toString(isWorker()));
                params.put("FromInstance", MainBase.main.configId.getGuid());
                Map<String, String> response = MainBase.getESB().call(topicName, getMessageHandlerName() + ".selfCheck", 
                                                                      params, ((ConfigReceiveGateway)configurations).getTopicSCThreshold()*1000);
                
                if (response == null)
                {
                    Log.alert(gatewayClassName + ": Instance " + MainBase.main.configId.getGuid() + " breached " + getGatewayEventType() + " " + topicName + " Self-Check", 
                              "Instance " + MainBase.main.configId.getGuid() + " Breached " + getGatewayEventType() + 
                              " " + topicName + " self-check threshold of " + ((ConfigReceiveGateway)configurations).getTopicSCThreshold() + 
                              " seconds!!!");
                }
            }
        }
        finally
        {
            if (!gatewayTopicSelfCheckStarted)
            {
                // schedule next self-check
                ScheduledExecutor.getInstance().executeRepeat(getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-TopicSelfCheck", this, "gwTopicSelfCheck", ((ConfigReceiveGateway)configurations).getTopicSCInterval(), TimeUnit.SECONDS);
                gatewayTopicSelfCheckStarted = true;
                Log.log.info(gatewayClassName + ": Scheduled " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-TopicSelfCheck to run every " + ((ConfigReceiveGateway)configurations).getTopicSCInterval() + " seconds");
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    public void gwQueueSelfCheck() throws Exception
    {
        try
        {
            if (gatewayQueueSelfCheckStarted)
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Type", "Gateway Primary Queue Self-Check");
                params.put(getQueueName(), "Threshold " + ((ConfigReceiveGateway)configurations).getQueueSCThreshold() + " seconds");
                params.put("Primary", Boolean.toString(isPrimary()));
                params.put("Secondary", Boolean.toString(isSecondary()));
                params.put("Worker", Boolean.toString(isWorker()));
                params.put("FromInstance", MainBase.main.configId.getGuid());
                Map<String, String> response = MainBase.getESB().call(getQueueName(), getMessageHandlerName() + ".selfCheck", 
                                                                      params, ((ConfigReceiveGateway)configurations).getQueueSCThreshold()*1000);
                
                if (response == null)
                {
                    Log.alert(gatewayClassName + ": Instance " + MainBase.main.configId.getGuid() + " breached " + getGatewayEventType() + " " + getQueueName() + " Self-Check", 
                              "Instance " + MainBase.main.configId.getGuid() + " Breached " + getGatewayEventType() + 
                              " " + getQueueName() + " self-check threshold of " + ((ConfigReceiveGateway)configurations).getQueueSCThreshold() + 
                              " seconds!!!");
                }
            }
        }
        finally
        {
            if (!gatewayQueueSelfCheckStarted)
            {
                // schedule next self-check
                ScheduledExecutor.getInstance().executeRepeat(getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-QueueSelfCheck", this, "gwQueueSelfCheck", ((ConfigReceiveGateway)configurations).getQueueSCInterval(), TimeUnit.SECONDS);
                gatewayQueueSelfCheckStarted = true;
                Log.log.info(gatewayClassName + ": Scheduled " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-QueueSelfCheck to run every " + ((ConfigReceiveGateway)configurations).getQueueSCInterval() + " seconds");
            }
        }
    }
    
    public boolean primaryDataQueueExecutorBusy()
    {
        if (executor != null)
        {
            return (((ThreadPoolExecutor)executor).getQueue()).size() > 0;
        }
        else
            return false;
    }
    
    public int getPrimaryDataQueueExecutorQueueSize()
    {
        return gatewayPrimaryExecutorQueueSize;
    }
    
    @Override
    public String getOrgSuffix()
    {
        return ((com.resolve.rsremote.ConfigGeneral)MainBase.main.configGeneral).getOrgSuffix();
    }
    
    private ResolveGateway getGateway(String gatewayName, String gatewayType) {         
    
        ResolveGateway gateway = null;
        
        if(StringUtils.isEmpty(gatewayName) || StringUtils.isEmpty(gatewayType))
            return null;

        switch(gatewayType) {
            case "Push":
                gateway = ConfigReceivePushGateway.getGateways().get(gatewayName);
                break;
            case "Pull":
                gateway = ConfigReceivePullGateway.getGateways().get(gatewayName);
                break;
            case "MSG":
                gateway = ConfigReceiveMSGGateway.getGateways().get(gatewayName);
                break;
            default:
                break;
        }
        
        return gateway;
    }
} // class BaseClusteredGateway