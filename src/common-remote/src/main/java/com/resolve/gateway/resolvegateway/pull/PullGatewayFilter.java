package com.resolve.gateway.resolvegateway.pull;

import java.util.Map;

import com.resolve.gateway.resolvegateway.GatewayFilter;

public class PullGatewayFilter extends GatewayFilter
{
    public static final String QUERY = "QUERY";
    public static final String TIME_RANGE = "TIME_RANGE";
    public static final String LAST_ID = "LAST_ID";
    public static final String LAST_VALUE = "LAST_VALUE";
    public static final String LAST_TIMESTAMP = "LAST_TIMESTAMP";
    
    private String type;
    private String query;
    private String timeRange;
    private String lastId;
    private String lastValue;
    private String lastTimestamp;

    public PullGatewayFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script) {
        super(id, active, order, interval, eventEventId, runbook, script);
    }
    
    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getQuery() {
        return this.query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
    
    public String getTimeRange()
    {
        return timeRange;
    }

    public void setTimeRange(String timeRange)
    {
        this.timeRange = timeRange;
    }

    public String getLastId()
    {
        return lastId;
    }

    public void setLastId(String lastId)
    {
        this.lastId = lastId;
    }

    public String getLastTimestamp()
    {
        return lastTimestamp;
    }
    
    public String getLastValue()
    {
        return lastValue;
    }

    public void setLastValue(String lastValue)
    {
        this.lastValue = lastValue;
    }

    public void setLastTimestamp(String lastTimestamp)
    {
        this.lastTimestamp = lastTimestamp;
    }

} // class PullGatewayFilter
