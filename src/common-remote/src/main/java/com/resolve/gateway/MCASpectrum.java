/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.Map;

import com.resolve.gateway.caspectrum.CASpectrumFilter;
import com.resolve.gateway.caspectrum.CASpectrumGateway;

/**
 * This class is used by the Message Listener and invokes it methods. Messages
 * destined for the gateways set this class name in the message header.
 *
 */
public class MCASpectrum extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MCASpectrum.setFilters";

    private static final CASpectrumGateway instance = CASpectrumGateway.getInstance();

    public MCASpectrum()
    {
        super.instance = instance;
    }

    @Override
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link CASpectrumFilter} instance
     *            
     */
    @Override
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        if (instance.isActive())
        {
            CASpectrumFilter caSpectrumFilter = (CASpectrumFilter) filter;
            filterMap.put(CASpectrumFilter.URLQUERY, caSpectrumFilter.getUrlQuery());
            filterMap.put(CASpectrumFilter.XMLQUERY, caSpectrumFilter.getXmlQuery());
            filterMap.put(CASpectrumFilter.OBJECT, caSpectrumFilter.getObject());
        }
    } // populateGatewaySpecificProperties
}
