package com.resolve.gateway.resolvegateway.push;

import com.resolve.gateway.resolvegateway.GatewayFilter;

public class PushGatewayFilter extends GatewayFilter
{
    public static final String PORT = "PORT";

    public static final String BLOCKING = "BLOCKING";

    public static final String URI = "URI";

    public static final String SSL = "SSL";

    protected String uri;
    
    protected Integer port = 7777;
    
    protected String blocking = "Default";
    
    protected Boolean ssl = false;

    public PushGatewayFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script) {
        super(id, active, order, interval, eventEventId, runbook, script);
    }

    public String getUri() {
        return this.uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
    
    public Integer getPort()
    {
        return port;
    }

    public void setPort(Integer port)
    {
        this.port = port;
    }
    
    public String getBlocking()
    {
        return blocking;
    }

    public void setBlocking(String blocking)
    {
        this.blocking = blocking;
    }

    public Boolean isSsl()
    {
        return ssl;
    }

    public void setSsl(Boolean ssl)
    {
        this.ssl = ssl;
    }
    
    public int getBlockingCode()
    {
        int code = 1;

        String blocking = getBlocking();
        if(blocking != null) {
            switch(blocking) {
                case "Default":
                    code = 1;
                    break;
                case "Gateway Script":
                    code =2;
                    break;
                case "Worksheet ID":
                    code = 3;
                    break;
                case "Execution Complete":
                    code = 4;
                    break;
                default:
                    break;
            }
        }

        return code;
    }

} // class PushGatewayFilter