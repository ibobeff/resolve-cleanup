/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.tcp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MTCP;
import com.resolve.rsremote.ConfigReceiveTCP;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class TCPGateway extends BaseClusteredGateway
{
    // Singleton
    private static volatile TCPGateway instance = null;

    private final ConfigReceiveTCP config;
    private TCPServer tcpServer;
    
    // This will be set in initialize() with the proper queue name from the
    // configuration.
    private String queue = "TCP";

    public static TCPGateway getInstance(ConfigReceiveTCP config)
    {
        if (instance == null)
        {
            instance = new TCPGateway(config);
        }
        return (TCPGateway) instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static TCPGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("TCP Gateway is not initialized correctly..");
        }
        else
        {
            return (TCPGateway) instance;
        }
    }

    protected TCPGateway(ConfigReceiveTCP config)
    {
        // Important, call super here.
        super(config);
        this.config = config;
        queue = config.getQueue();
        tcpServer = new TCPServer(config);
    }

    public ConfigReceiveTCP getConfig()
    {
        return config;
    }

    @Override
    protected void initialize()
    {
        ConfigReceiveTCP config = (ConfigReceiveTCP) configurations;
        queue = config.getQueue().toUpperCase();

        try
        {
            this.gatewayConfigDir = "/config/tcp/";
            if (Log.log.isDebugEnabled())
            {
                Log.log.debug(config.toString());
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to config TCP Gateway: " + e.getMessage(), e);
        }
    }

    @Override
    public void run()
    {
        //This call is must for all the ClusteredGateways.
        super.sendSyncRequest();
        
        //this is the point to instantiate servlet end point
        if(isPrimary() && isActive())
        {
            try
            {
                //add the URI based servlets to the default server
                for(Filter filter : orderedFilters)
                {
                    TCPFilter tcpFilter = (TCPFilter) filter;
                    Log.log.debug(tcpFilter.toString());
                    tcpServer.startListener(tcpFilter);
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", 
                          "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + 
                          "due to " + e.getMessage());
                //setActive(false);
            } 
        }
    }

    @Override
    public void start()
    {
        Log.log.warn("Starting TCP gateway");
        super.start();
    }

    @Override
    public void stop()
    {
        Log.log.warn("Stopping TCP gateway");
        super.stop();
        tcpServer.shutdown();
    }

    @Override
    public String getLicenseCode()
    {
        return "TCP";
    }

    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_TCP;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MTCP.class.getSimpleName();
    }

    @Override
    protected Class<MTCP> getMessageHandlerClass()
    {
        return MTCP.class;
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new TCPFilter((String) params.get(TCPFilter.ID), (String) params.get(TCPFilter.ACTIVE), (String) params.get(TCPFilter.ORDER), (String) params.get(TCPFilter.INTERVAL), (String) params.get(TCPFilter.EVENT_EVENTID)
                        , (String) params.get(TCPFilter.RUNBOOK), (String) params.get(TCPFilter.SCRIPT), (String) params.get(TCPFilter.PORT), (String) params.get(TCPFilter.BEGIN_SEPARATOR), (String) params.get(TCPFilter.END_SEPARATOR), (String) params.get(TCPFilter.INCLUDE_BEGIN_SEPARATOR), (String) params.get(TCPFilter.INCLUDE_END_SEPARATOR));
    }

    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
    }

    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList)
    {
        super.clearAndSetFilters(filterList);
        
        if(isPrimary() && isActive())
        {
            try
            {
                for(Filter filter : filters.values())
                {
                    TCPFilter tcpFilter = (TCPFilter) filter;
                    
                    if(Log.log.isDebugEnabled())
                    {
                        Log.log.debug("Filter: " + tcpFilter.toString());
                    }
                    
                    if(tcpFilter.getPort() != null && tcpFilter.getPort() > 0)
                    {
                        TCPListener tcpListener = tcpServer.getListener(tcpFilter.getPort());
                        //this checks if the port changed for the filter.
                        if(tcpListener != null && !tcpListener.getPort().equals(tcpFilter.getPort()))
                        {
                            tcpServer.stopListener(tcpFilter);
                            tcpListener = null;
                        }
                        
                        if(tcpListener == null)
                        {
                        	if(!tcpServer.hasStarted(tcpFilter.getPort()))
                        	{
                            	tcpServer.startListener(tcpFilter);
                            }
                        }
                        else
                        {
                            tcpListener.addFilter(tcpFilter);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            } 
        }
    }

    @Override
    protected void cleanFilter(ConcurrentHashMap<String, Filter> undeployedFilters)
    {
        for(Filter filter : undeployedFilters.values())
        {
            TCPFilter tcpFilter = (TCPFilter) filter;
            tcpServer.stopListener(tcpFilter);
        }
    }

    /**
     * This method processes the message received from the TCP client.
     *
     * @param message
     */
    public boolean processData(String filterId, String data)
    {
        boolean result = true;
        try
        {
            if(StringUtils.isNotBlank(filterId) && StringUtils.isNotBlank(data))
            {
                TCPFilter tcpFilter = (TCPFilter) filters.get(filterId);
                if (tcpFilter != null && tcpFilter.isActive())
                {
                    if(Log.log.isDebugEnabled())
                    {
                        Log.log.debug("Processing filter: " + tcpFilter.getId());
                        Log.log.debug("Data received through TCP gateway: " + data);
                    }
                    Map<String, String> runbookParams = new HashMap<String, String>();
                    runbookParams.put("TCP_CONTENT", data);
                    runbookParams.put(Constants.EVENT_EVENTID, tcpFilter.getEventEventId());

                    addToPrimaryDataQueue(tcpFilter, runbookParams);
                }
                else
                {
                    result = false;
                }
            }
        }
        catch (Exception e)
        {
            Log.log.warn("Error: " + e.getMessage(), e);
            try
            {
                Thread.sleep(interval);
            }
            catch (InterruptedException ie)
            {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
        return result;
    }
}