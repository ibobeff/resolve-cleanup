/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.tsrm;

import org.apache.http.HttpResponse;

import com.resolve.util.StringUtils;

/**
 * This class is available as a convenience class for handling error code
 * returned by the TSRM REST web services.
 * 
 * The following HTTP response codes are implemented by the TSRM REST API.
 * 
 * 200 Success. Not an error 201 Success. The response contains a link. Not an
 * error 304 Success. The data is retrieved from the cache. Not an error 400 The
 * request cannot be received because of an invalid URI, for example. 401 An
 * authorization violation occurred because of the configuration. 403 An
 * authorization violation occurred because the resource is blocked by a
 * mxe.rest.resourcetype.blockaccess system property. 404 The resource cannot be
 * found or an invalid resource type was provided. 405 The HTTP method cannot be
 * used for the resource. For example, you cannot use the DELETE method on a
 * business object set. 406 The requested representation is not supported. 412
 * The resource is being updated by another user. 500 All other errors.
 */
public class ErrorHandler
{
    public static final String ERROR_400 = "Error 400:";

    public static boolean verifyStatus(HttpResponse response) throws Exception
    {
        boolean result = true;
        String message = null;
        
        if(response == null)
        {
            result = false;
            message = "Null/empty response received from the TSRM server";
        }
        else
        {
            int statusCode = response.getStatusLine().getStatusCode();
            
            switch (statusCode)
            {
                case 200: // not an error
                case 201: // not an error
                case 304: // not an error
                    result = true;
                    break;
                case 400:
                    if(response.getEntity() != null)
                    {
                        message = StringUtils.toString(response.getEntity().getContent(), "utf-8");
                    }
                    else
                    {
                        message = ERROR_400 + " The request cannot be received because of an invalid URI, for example.";
                    }
                    result = false;
                    break;
                case 401:
                    message = "Error 401: An authorization violation occurred because of the configuration.";
                    result = false;
                    break;
                case 403:
                    message = "Error 403: An authorization violation occurred because the resource is blocked by a mxe.rest.resourcetype.blockaccess system property.";
                    result = false;
                    break;
                case 404:
                    message = "Error 404: The resource cannot be found or an invalid resource type was provided.";
                    result = false;
                    break;
                case 405:
                    message = "Error 405: The HTTP method cannot be used for the resource. For example, you cannot use the DELETE method on a business object set.";
                    result = false;
                    break;
                case 406:
                    message = "Error 406: The requested representation is not supported.";
                    result = false;
                    break;
                case 412:
                    message = "Error 412: The resource is being updated by another user.";
                    result = false;
                    break;
                case 500:
                    message = "Error 500: Some sort of run time error occurred at TSRM server.";
                    result = false;
                    break;
                default:
                    // although unknown error, cannot assume it's an error.
                    result = true;
                    break;
            }
        }
        if (!result)
        {
            throw new Exception(message);
        }
        return result;
    }
}
