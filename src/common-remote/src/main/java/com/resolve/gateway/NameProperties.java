/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.util.Log;
import com.resolve.util.Properties;

/**
 * NameProperties acts as a {@link ConcurrentHashMap} container for Gateway subclass properties.
 * 
 * 
 * @author Bipul.Dutta
 * @see ConcurrentHashMap
 *
 */
public class NameProperties extends ConcurrentHashMap<String, Properties>
{
    private static final long serialVersionUID = 4722798455444956512L;

	public Map getNameProperties(String name)
    {
        if(Log.log.isDebugEnabled())
        {
            Log.log.debug("Inside NameProperties.getNameProperties: " + name);
        }
        return (Map) this.get(name);
    } // getNameProperties

    public void setNameProperties(String name, Map properties)
    {
        Map map = (Map) this.get(name);
        if (map == null)
        {
            map = new Properties();
            put(name, (Properties) map);
        }
        map.clear();
        map.putAll(properties);
    } // setNameProperties

    public void removeNameProperties(String name)
    {
        this.remove(name);
    } // removeNameProperties

    public String getNameProperty(String name, String key)
    {
        String result = null;

        Map map = (Map) this.get(name);
        if (map != null)
        {
            result = (String) map.get(key);
        }
        return result;
    } // getNameProperty

    public void setNameProperty(String name, String key, String value)
    {
        Map map = (Map) this.get(name);
        if (map == null)
        {
            map = new Properties();
            put(name, (Properties) map);
        }
        map.put(key, value);
    } // setNameProperty

    public Object getNameObject(String name, String key)
    {
        Object result = null;
        if(Log.log.isDebugEnabled())
        {
            Log.log.debug("READING property name: " + name + ", key: " + key);
        }
        Map map = (Map) this.get(name);
        if (map != null)
        {
            result = map.get(key);
        }
        return result;
    } // getNameProperty

    public void setNameObject(String name, String key, Object value)
    {
        Map map = (Map) this.get(name);
        if (map == null)
        {
            map = new Properties();
            put(name, (Properties) map);
        }
        map.put(key, value);
    } // setNameProperty

    public void removeNameProperty(String name, String key)
    {
        String result = null;

        Map map = (Map) this.get(name);
        if (map != null)
        {
            map.remove(key);
        }
    } // removeNameProperty

} // NameProperties
