/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.resolve.rsbase.SessionObjectInterface;

public interface Gateway
{
	public static final String GET_QUEUE_NAME_METHOD_NAME = "getQueueName";
	
	/**
     * Health status codes.
     *
     * GREEN, YELLOW, RED, FAILED_TO_RUN status codes are primarily used by gateways to report its health status while 
     * FAILED_TO_CONDUCT, FAILED_TO_REPORT, FAILED_TO_REPORT_IN_TIME are used by component conducting health check on gateways
     */
    public static enum HealthStatusCode {
        GREEN, YELLOW, RED, FAILED_TO_RUN, FAILED_TO_CONDUCT, FAILED_TO_REPORT, FAILED_TO_REPORT_IN_TIME;
    }
    
    /**
     * This method provides the code used in license (e.g., EMAIL, REMEDYX). Resolve license
     * must have the same string to start this gateway.
     *
     * @return
     */
    String getLicenseCode();

    void init() throws Exception;

    void reinitialize();

    void start();

    void stop();

    boolean isActive();

    void setActive(boolean active);

    void deactivate();

    String getQueueName();

    void registerSessionObject(SessionObjectInterface sessionObject);
    
    Pair<HealthStatusCode, String> checkGatewayHealth(Map<String, Object> healthCheckOptions);
}
