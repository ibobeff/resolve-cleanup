package com.resolve.gateway.resolvegateway.push;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.gateway.resolvegateway.ConfigReceiveResolveGateway;
import com.resolve.gateway.resolvegateway.ResolveGatewayUtil;
import com.resolve.gateway.resolvegateway.push.PushGatewayFilter;
import com.resolve.gateway.resolvegateway.push.PushGatewayProperties;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceivePushGateway extends ConfigReceiveResolveGateway
{
    private static final String RECEIVE_NODE = "./RECEIVE/";
    
    static final Integer DEFAULT_WAIT_TIMEOUT = 120;
    static final Integer DEFAULT_EXECUTE_TIMEOUT = 120;
    
    private static volatile Map<String, PushGateway> gateways = new ConcurrentHashMap<String, PushGateway>();
    private static volatile Map<String, PushGatewayProperties> gatewayProperties = new ConcurrentHashMap<String, PushGatewayProperties>();

    public ConfigReceivePushGateway(XDoc configDoc, String gatewayName) throws Exception {
        super(configDoc, gatewayName);
    }
    
    public String getRootNode() {
        return RECEIVE_NODE + gatewayName.toUpperCase() + "/";
    }
    
    public static Map<String, PushGateway> getGateways()
    {
        return gateways;
    }

    public void loadPushGatewayProperties(String gatewayName, String node) {

        Map<String, String> properties = ResolveGatewayUtil.loadGatewayProperties(node);
      
        PushGatewayProperties push = new PushGatewayProperties();
        Map<String, Object> additional = push.getAdditionalProperties();
        
        for(Iterator<String> it=properties.keySet().iterator(); it.hasNext();) {
            String key = it.next();
            String value = properties.get(key);
            switch(key) {
                case "ACTIVE":
                    try {
                        Boolean active = new Boolean(value);
                        push.setActive(active);
                        this.setActive(active);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "MENUTITLE":
                    push.setDisplayName(value);
                    break;
                case "QUEUE":
                    String queue = value;
                    push.setQueue(queue);
                    push.setLicenseCode(queue); // Currently it's hardcoded the same as the queue name
                    push.setEventType(queue); // Currently it's hardcoded the same as the queue name
                    this.setQueue(queue);
                    break;
                case "IMPLPREFIX":
                    push.setClassName(value);
                    break;
                case "PACKAGE":
                    push.setPackageName(value);
                    break;
                case "LICENSE":
                    push.setLicenseCode(value);
                    break;
                case "EVENTTYPE":
                    push.setEventType(value);
                    break;
                case "PRIMARY":
                    try {
                        Boolean primary = new Boolean(value);
                        push.setPrimary(primary);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "SECONDARY":
                    try {
                        Boolean secondary = new Boolean(value);
                        push.setSecondary(secondary);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "WORKER":
                    try {
                        Boolean worker = new Boolean(value);
                        push.setWorker(worker);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "MOCK":
                    try {
                        Boolean mock = new Boolean(value);
                        push.setMock(mock);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "UPPERCASE":
                    try {
                        Boolean uppercase = new Boolean(value);
                        push.setUppercase(uppercase);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "INTERVAL":
                    try {
                        Integer interval = new Integer(value);
                        push.setInterval(interval);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "FAILOVER":
                    try {
                        Integer failover = new Integer(value);
                        push.setFailover(failover);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "HEARTBEAT":
                    try {
                        Integer heartbeat = new Integer(value);
                        push.setHeartbeat(heartbeat);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "SSL":
                    try {
                        Boolean ssl = new Boolean(value);
                        push.setWorker(ssl);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "SSLTYPE":
                    push.setSslType(value);
                    break;
                case "HOST":
                    push.setHost(value);
                    break;
                case "PORT":
                    try {
                        if(StringUtils.isBlank(value))
                            continue;
                        Integer port = new Integer(value);
                        push.setPort(port);
                    } catch(Exception e) {
                        Log.log.warn(e.getMessage(), e);
                    }
                    break;
                case "USERNAME":
                    push.setUser(value);
                    break;
                case "PASSWORD":
                    if (StringUtils.isNotBlank(value) && value.startsWith("ENC1:")) {
                        try {
                            value = CryptUtils.decrypt(value);
                        } catch(Exception e) {
                            Log.log.error(e.getMessage(), e);
                        }
                    }
                    
                    push.setPass(value);
                    break;
                case "SEC":
                    push.setSec(value);
                    break;
                case "HTTP_SSL":
                    try {
                        Boolean httpSsl = new Boolean(value);
                        push.setHttpSsl(httpSsl);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "HTTP_PORT":
                    try {
                        if(StringUtils.isBlank(value))
                            continue;
                        Integer httpPort = new Integer(value);
                        push.setHttpPort(httpPort);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "HTTP_USERNAME":
                    push.setHttpUser(value);
                    break;
                case "HTTP_PASSWORD":
                    if (StringUtils.isNotBlank(value) && value.startsWith("ENC1:")) {
                        try {
                            value = CryptUtils.decrypt(value);
                        } catch(Exception e) {
                            Log.log.error(e.getMessage(), e);
                        }
                    }
                    
                    push.setHttpPass(value);
                    break;
                case "HTTP_SSLCERTIFICATE":
                    push.setSslCertificate(value);
                    break;
                case "SSLPASSWORD":
                    if (StringUtils.isNotBlank(value) && value.startsWith("ENC1:")) {
                        try {
                            value = CryptUtils.decrypt(value);
                        } catch(Exception e) {
                            Log.log.error(e.getMessage(), e);
                        }
                    }
                    
                    push.setSslPassword(value);
                    break;
                case "EXECUTE_TIMEOUT":
                    try {
                        Integer executeTimeout = new Integer(value);
                        push.setExecuteTimeout(executeTimeout);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "SCRIPT_TIMEOUT":
                    try {
                        Integer scriptTimeout = new Integer(value);
                        push.setScriptTimeout(scriptTimeout);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                case "WAIT_TIMEOUT":
                    try {
                        Integer waitTimeout = new Integer(value);
                        push.setWaitTimeout(waitTimeout);
                    } catch(Exception e) {
                        Log.log.error(e.getMessage(), e);
                    }
                    break;
                default:
                    additional.put(key, value);
                    break;
            }
        }
        
//        Integer id = ResolveGatewayUtil.savePushGatewayProperties(push, additional);

        gatewayProperties.put(gatewayName, push);
    }
    
    @Override
    public void load() throws Exception {
        try {
            loadAttributes();
        } catch (Throwable e) {
            setActive(false);
            Log.log.warn("Couldn not load configuration for push gateway, check blueprint. " + e.getMessage(), e);
        }
    }
            
    public void loadPushGatewayFilters(String gatewayName, String queueName) {

        if(!isActive())
            return;
        
        Map<String, Map<String, Object>> gatewayFilters = ResolveGatewayUtil.loadGatewayFilters(gatewayName, "push", queueName);
        if(gatewayFilters == null)
            return;
        
        for(Iterator<String> it=gatewayFilters.keySet().iterator(); it.hasNext();) {
        
            String filterName = it.next();
            Map<String, Object> filter = gatewayFilters.get(filterName);
            
            String sysId = (String)filter.get("SYS_ID");
            String active = ((Boolean)filter.get("ACTIVE")).toString();
            String order = ((Integer)filter.get("ORDER")).toString();
//            String interval = ((Integer)filter.get("INTERVAL")).toString();
//            String filterName = (String)filter.get("ID");
            //This field (Interval) is redundant for Push gateways need to take it away
            String interval = "10";
            String eventId = (String)filter.get("EVENT_EVENTID");
            String runbook = (String)filter.get("RUNBOOK");
            String script = (String)filter.get("SCRIPT");
            
            PushGatewayFilter pushFilter = new PushGatewayFilter(filterName, active, order, interval, eventId, runbook, script);
            
            pushFilter.setDeployed(((Boolean)filter.get("DEPLOYED")));
            pushFilter.setUpdated(((Boolean)filter.get("UPDATED")));
            pushFilter.setGatewayName(gatewayName);
            pushFilter.setQueue((String)filter.get("QUEUE"));
            pushFilter.setUri((String)filter.get("URI"));
            pushFilter.setBlocking((String)filter.get("BLOCKING"));
            pushFilter.setPort((Integer)filter.get("PORT"));
            pushFilter.setSsl((Boolean)filter.get("SSL"));
            
            Map<String, Object> gatewayFilterAttributes = ResolveGatewayUtil.loadGatewayFilterAttributes(sysId, "push");
            if(gatewayFilterAttributes != null)
                pushFilter.setAttributes(gatewayFilterAttributes);

            PushGateway.addPushFilter(filterName, pushFilter);

            /* Don't need to load from file system because it should be saved in and loaded from the database
            StringBuilder sb = new StringBuilder();
            sb.append(MainBase.main.release.serviceName).append("/config/").append(gatewayName).append("/").append(filterName.toLowerCase()).append(".groovy");
            File scriptFile = getFile(sb.toString());
            
            try {
                if (scriptFile.exists()) {
                    String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                    if (StringUtils.isNotBlank(groovy)) {
                        pushFilter.setScript(groovy);
                        Log.log.debug("Loaded groovy script: " + scriptFile);
                    }
                }
            } catch (Exception e) {
                Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
            }
            
            ResolveGatewayUtil.savePushGatewayFilter(pushFilter, filterName, gatewayName); */
        }
    }
/*
    public void save(String gatewayName) throws Exception {
        
        File dir = getFile(MainBase.main.release.serviceName + "/config/" + gatewayName.toLowerCase());
        
        if (!dir.exists()) {
            FileUtils.forceMkdir(dir);
        }
        
        if (filters != null && isActive()) {
            
            filters.clear();
            
            for (Filter filter : PushGateway.getInstance().getFilters().values()) {
                PushGatewayFilter pushFilter = (PushGatewayFilter)filter;
                String groovy = pushFilter.getScript();
                
                String scriptFileName;
                File scriptFile = null;
                
                if (StringUtils.isNotBlank(groovy)) {
                    scriptFileName = pushFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/" + gatewayName.toLowerCase() + scriptFileName);
                }
                
                Map<String, Object> entry = new HashMap<String, Object>();
//                putFilterDataIntoEntry(entry, pushFilter);
                
//                filters.add(entry);
                
                if (scriptFile != null) {
                    try {
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    } catch (Exception e) {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }
            
//            xdoc.setListMapValue(RECEIVE_MOOGSOFT_FILTER, filters);
        }
        
        if (nameProperties != null && isActive()) {
            nameProperties.clear();
            for (String name : PushGateway.getInstance().getNameProperties().keySet()) {
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);
                Map props = PushGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }
        
        saveAttributes();
    }
    */
    public static PushGatewayProperties getGatewayProperties(String name)
    {
        return gatewayProperties.get(name);
    }

} // ConfigReceivePushGateway
