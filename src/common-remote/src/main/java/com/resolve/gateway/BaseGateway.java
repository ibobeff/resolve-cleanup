/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.resolve.rsbase.MainBase;
import com.resolve.groovy.ThreadGroovy;
import com.resolve.rsbase.SessionObjectInterface;
import com.resolve.rsremote.ConfigReceiveGateway;
import com.resolve.util.ConfigMap;
import com.resolve.util.Constants;
import com.resolve.util.ERR;
import com.resolve.util.LicenseEnum;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class provides a partial implementation for gateway sub classes
 * implemting most common functionalities of the gateways. In general
 * every gateway does not participate in a cluster environment (e.g., ITM, SSH)
 * so this class makes sure those non-clustered gateways get their common
 * functionalities from here.
 */
public abstract class BaseGateway implements Gateway
{
	public static final String GET_MESSAGE_HANDLER_METHOD_NAME = "getMessageHandlerNamePublic";
	
    protected final static String ALERT_SEVERITY_CRITICAL = "CRITICAL";
    protected final static String ALERT_SEVERITY_WARNING = "WARNING";
    protected final static String ALERT_SEVERITY_INFO = "INFO";

    protected final static int LICENSE_CHECK_INTERVAL = 30; //in minutes

    protected static final String SERVER_NOT_AUTH_MESSAGE = "Failed Authentication or authorization with the server. ";
    protected static final String SERVER_UNREACHABLE_MESSAGE = "Server is unreachable. ";
    protected static final String HEALTHY_GATEWAY_MESSAGE = "Gateway is healthy. ";
    protected static final String GATEWAY_LICENSE_IS_VALID_MESSAGE = "Gateway license is valid. ";
    protected static final String GATEWAY_LICENSE_EXPIRED_MESSAGE = "Gateway license has expired. ";
    
    protected boolean debug = false;

    protected boolean active = true;
    protected volatile boolean running = false;
    protected ConfigMap configurations;
    protected final String gatewayClassName = this.getClass().getSimpleName();

    private long gatewayLicenseExpiredTime = 0;
    
    //keep track of the alert messages being sent from this gateway so it doesn't
    //keep sending same alert over an over again.
    protected Map<String, String> alertCache = new HashMap<String, String>();

    abstract protected void initialize();

    abstract protected String getGatewayEventType();
    
    abstract protected String getMessageHandlerName();
    
    public String getMessageHandlerNamePublic() {
    	return getMessageHandlerName();
    }
    
    abstract protected Class<?> getMessageHandlerClass();
    
    public Class<?> getMessageHandlerClassPublic()  {
    	return getMessageHandlerClass();
    }
    
    public void reinitialize()
    {
        alertCache.clear();
        //Subclass must explicitly override this method if needed (e.g., XMPP).
    }

    public void deactivate()
    {
        alertCache.clear();
        //Subclass must explicitly override this method if needed (e.g., XMPP).
    }

    /**
     * This method is implemented by subclasses to update various custom fields
     * in the target server. For example in Netcool or HPOM gateway it will
     * update RESOLVE_STATUS and RESOLVE_RUNBOOKID.
     *
     * @param filter
     * @param content
     */
    protected void updateServerData(Filter filter, Map<String, String> content)
    {
        // Not implemented here, normaly subclasses like HPOM, Netcool etc.
        // should implement this method
        // to update the RESOLVE_STATUS, RESOLVE_RUNBOOKID field etc.
    }

    @Override
    public boolean isActive()
    {
        return active;
    }

    @Override
    public void setActive(boolean active)
    {
        this.active = active;
    }

    protected BaseGateway(ConfigMap config)
    {
        try
        {
            // assign to the configurations.
            configurations = config;
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }
    }

    @Override
    public void init() throws Exception
    {
        try
        {
            // Call this subclass method if there are any specific
            // initialization. Make sure it's always called first.
            initialize();
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
            throw e;
        }
    }

    /**
     * This method is implemented by the gateway that are open. For example if PS team
     * develops a gateway for certain customer then they do no need to buy license.
     * In that situation the PS developed gateway will override this method and return 
     * false.
     *  
     * @return
     */
    protected boolean requireLicense()
    {
        return true;
    }
    
    protected boolean isValidLicense(boolean checkToStart, boolean originalPrimary) // True - Check if ok to start, False - Check if ok to keep running
    {
        if(requireLicense())
        {
        	boolean isValidLicense = MainBase.main.getLicenseService().checkGatewayLicense(getLicenseCode(), MainBase.main.configId.getGuid(), getInstanceType(), getQueueN(), System.currentTimeMillis(), checkToStart, originalPrimary);
            if(isValidLicense) Log.log.info(gatewayClassName+":"+" License is valid");
            else	Log.log.info(gatewayClassName+":"+"Valid License not found");
            return isValidLicense;
        }
        else
        {
            return true;
        }
    }
    
    protected boolean isValidLicenseForPrimary(boolean checkToStart, boolean originalPrimary) // True - Check if ok to start, False - Check if ok to keep running
    {
        if(requireLicense())
        {
            return MainBase.main.getLicenseService().checkGatewayLicense(getLicenseCode(), MainBase.main.configId.getGuid(), "PRIMARY", getQueueN(), System.currentTimeMillis(), checkToStart, originalPrimary);
        }
        else
        {
            return true;
        }
    }
    
    private String getQueueN() {
    	return ((ConfigReceiveGateway)configurations).getQueue() + ((com.resolve.rsremote.ConfigGeneral)MainBase.main.configGeneral).getOrgSuffix();
    }
    
    public void checkGatewayLicense()
    {
        if(requireLicense())
        {
        	boolean gatewayLicenseExpired = true;
        	
            if(MainBase.main.getLicenseService().isGatewayLicenseExpired(getLicenseCode())) {
                long currentTime = System.currentTimeMillis();
                if (gatewayLicenseExpiredTime == 0)
                {
                    gatewayLicenseExpiredTime = currentTime;
                }
                
                //send the alert.
                
                Log.alert(ERR.E10021.getCode(), ERR.E10021.getMessage() + " " + getLicenseCode() + " gateway license has expired.", Constants.ALERT_TYPE_LICENSE);
                gatewayLicenseExpiredTime = currentTime;
                
                // for V2 PROD license type and matching env keep the gateway running even if license has expired
                
                try {
					if (MainBase.main.getLicenseService().getLicense().isV2AndExpiredAndProd() &&
						LicenseEnum.LICENSE_ENV_PRODUCTION.getKeygenName().equalsIgnoreCase(MainBase.main.configGeneral.getEnv())) {
						gatewayLicenseExpired = false;
					}
				} catch (Exception e) {
					Log.log.error(String.format("Error [%s] occurred while checking if current license is V2, Expired and for " +
						    					"Prod environment.",
						    					(StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : "")), e);
				}
            } else {
            	gatewayLicenseExpired = false;
            }
            
            if (!gatewayLicenseExpired) {
                boolean originalPrimary = this instanceof BaseClusteredGateway ? ((BaseClusteredGateway)this).isOriginalPrimary() : false;
                
                boolean isValid = isValidLicense(!running, originalPrimary); // originalPrimary matters only for start
        
                Log.log.info("License for : " + getLicenseCode() + (isValid ? " is valid" : " is not valid"));
        
                if(!isValid && running)
                {
                    // If Current Gateway is found to be running with invalid license then stop
                    Log.log.warn("Found running gateway with invalid license, stopping " + getLicenseCode() + "-" + getInstanceType() + "-" + getQueueName() + " gateway.");
                    stop();
                }
        
                //it is possible that a license got uploaded and we can start the gateway now
                if(isValid && !running /*&& MainBase.main.isClusterModeActive()*/)
                {
                    Log.log.info("Gateway license found, restarting " + getLicenseCode() + " gateway.");
                    start();
                }
            }
        }
        else
        {
            if(Log.log.isDebugEnabled())
            {
                Log.log.debug("License not required for " + getLicenseCode() + "-" + getInstanceType() + "-" + getQueueName());
            }
        }
    }

    /**
     * Clustered gateway override this method to indicate if it's PRIMARY, SECONDARY, or a WORKER.
     * Other gateways like ITM etc keeps it blank.
     *   
     * @return
     */
    protected String getInstanceType()
    {
        return "";
    }

    @Override
    public void registerSessionObject(SessionObjectInterface sessionObject)
    {
        Log.log.debug("Register " + sessionObject.getClass().getSimpleName() + ":" + sessionObject.toString());
        ThreadGroovy.getConnectionList().add(sessionObject);
    }

    protected void resetAlertCache()
    {
        alertCache.clear();
    }

    public void sendAlert(String severity, boolean cache, String code, String message)
    {
        //if we already sent it then no point repeating.
        if(!alertCache.containsKey(code))
        {
            Map<String, String> params = new HashMap<String, String>();
            params.put("SEVERITY", severity);
            params.put("COMPONENT", Log.component);
            params.put("GATEWAY", getQueueName());
            params.put("CODE", code);
            params.put("MESSAGE", message);

            if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_ALERT, "MGatewayAlert.alert", params) == false)
            {
                Log.log.warn("Failed to send gateway alert message to ALERT");
            }
            //just interested in the code nothing else.
            if(cache)
            {
                alertCache.put(code, null);
            }
        }
    }
    
    /**
     * Pull type gateway implementations should override this method to
     * check whether gateway is able to connect to external system.
     * 
     * @return True if implemented, False otherwise
     *  
     * @throws Exception If unable to connect to external system 
     */
    public boolean checkGatewayConnection() throws Exception
    {
        return false;
    }
    
    /**
     * Perform implemented health check and return health status 
     * code along with detail message (if any) from health check run.
     * 
     * Default implementation is to perform license check.
     * Gateway specific implementation should override this method
     * and add its own specific health checks apart from license check.
     * 
     * @param healthCheckOptions	Options to use in health check (if supported)
     * @return 
     */
    public Pair<HealthStatusCode, String> checkGatewayHealth(Map<String, Object> healthCheckOptions) {
    	boolean gtwLicExpired = MainBase.main.getLicenseService().isGatewayLicenseExpired(getLicenseCode());
    	
    	if (gtwLicExpired) {
    		try {
				if (MainBase.main.getLicenseService().getCurrentLicense().isV2AndExpiredAndProd()) {
					return Pair.of(HealthStatusCode.YELLOW, GATEWAY_LICENSE_EXPIRED_MESSAGE);
				} else {
					return Pair.of(HealthStatusCode.RED, GATEWAY_LICENSE_EXPIRED_MESSAGE);
				}
			} catch (Exception e) {
				String errMsg = String.format("Error %sin checking current license is V2, expired, " +
											  "and environment is PROD", 
											  (StringUtils.isNotBlank(e.getMessage()) ? 
											   "[" + e.getMessage() + "] " : ""));    		
				Log.log.error(errMsg, e);
				return Pair.of(Gateway.HealthStatusCode.FAILED_TO_RUN, 
							   String.format("Error Message = [%s], Stack Trace = [%s]", errMsg, 
							   		 		 ExceptionUtils.getStackTrace(e)));
			}
    	} else {
    		return Pair.of(HealthStatusCode.GREEN, GATEWAY_LICENSE_IS_VALID_MESSAGE);
    	}
    }
}
