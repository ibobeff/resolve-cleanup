package com.resolve.gateway.resolvegateway.msg;

import java.util.Map;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.resolve.gateway.Filter;
import com.resolve.gateway.MMSGGateway;
import com.resolve.gateway.resolvegateway.GatewayProperties;
import com.resolve.gateway.resolvegateway.ResolveGateway;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

public class MSGGateway extends ResolveGateway {
    
    private static volatile MSGGateway instance = null;
    private static volatile boolean mqServerAlive = false;
    
    private static volatile Map<String, MSGGatewayFilter> msgGatewayFilters = new ConcurrentHashMap<String, MSGGatewayFilter>();
    
    public static MSGGateway getInstance(ConfigReceiveMSGGateway config) {
        
       // if (instance == null) {
            instance = new MSGGateway(config);
        //}
        
        return instance;
    }

    protected MSGGateway(ConfigReceiveMSGGateway config) {
        
        super(config);
    }
    
    public static MSGGateway getInstance() {
        
        if (instance == null) {
            throw new RuntimeException("MSG Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }
    
    public static MSGGateway getInstance(String gatewayName) {
        
        Map<String, MSGGateway> gateways = ConfigReceiveMSGGateway.getGateways();
        
        for(String gateway:gateways.keySet()) {
            if(gateway.toLowerCase().equals(gatewayName))
                return gateways.get(gateway);
        }
        
        return null;
    }

    public static boolean isMQServerAlive()
    {
        return mqServerAlive;
    }

    public static void setMQServerAlive(boolean alive)
    {
        MSGGateway.mqServerAlive = alive;
    }
    
    public static Map<String, MSGGatewayFilter> getMSGFilters()
    {
        return msgGatewayFilters;
    }
    
    public static void addMSGFilter(String filterName, MSGGatewayFilter filter) {
        
        msgGatewayFilters.put(filterName, filter);
    }
    
    public static void removeMSGFilter(String filterName) {
        
        msgGatewayFilters.remove(filterName);
    }

    @Override
    public String getLicenseCode() {
        return queue;
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
    }

    @Override
    protected String getGatewayEventType() {
        return queue;
    }

    @Override
    protected String getMessageHandlerName() {
        return MMSGGateway.class.getSimpleName();
    }

    @Override
    protected Class<MMSGGateway> getMessageHandlerClass() {
        return MMSGGateway.class;
    }

    @Override
    public void start() {
        
        super.start();
    }
    
    @Override
    protected void initialize() {
    
    }

    @Override
    public void run() {
        
        if(isPrimary() && isActive())
            initMSGListeners();
    }

    protected void initMSGListeners() {
        
        for(Filter deployedFilter : orderedFilters) {
            try {
                initMSGListener(deployedFilter);
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    
    protected void initMSGListener(Filter deployedFilter) throws Exception {
        throw new UnsupportedOperationException("You must override this method in order to initiate the deployed filter processes!");
    }
    
    @Override
    public void deactivate() {
        
        suspend();
        
        if(heartBeat != null)
            heartBeat.stopHeartBeat();
        
        super.deactivate();
    }
    
    @Override
    public void resume() {

        if(isPrimary() && isActive())
            initMSGListeners();
    }

    public List<Filter> deployFilters(List<Map<String, Object>> filterList, String username) throws Exception {
        
        Log.log.debug("Calling Gateway specific deployFilters" + (filterList != null ? " " + filterList.size() + ", [" + filterList + "]" : ""));
        
        List<Filter> deployedFilters = new ArrayList<Filter>();
        
        String gatewayName = "";
        MSGGateway gateway = null;
        
        for (Map<String, Object> params : filterList) {
            if (!params.containsKey("RESOLVE_USERNAME")) {
                gatewayName = params.get("GATEWAY_NAME") != null ? params.get("GATEWAY_NAME").toString().trim() : null;
                if(gatewayName ==null)
                    continue;
                
                gateway = ConfigReceiveMSGGateway.getGateways().get(params.get("GATEWAY_NAME"));
                
                if(gateway == null) {
                    Log.log.warn("Gateway " + gatewayName + " does not exist");
                    continue;
                }
                
                int filterValidationErrCode = gateway.validateFilterFields(params);
                
                if (filterValidationErrCode != 0)
                    throw new Exception("Filter Validation Returned Error Code " + filterValidationErrCode + " : " + gateway.getValidationError(filterValidationErrCode));
                
                String name = (String) params.get("ID");
                String queue = (String) params.get("QUEUE");
                
                MSGGatewayFilter originalFilter = msgGatewayFilters.get(name);
                if(originalFilter != null) {
                    List<Filter> filterToBeRemoved = new ArrayList<Filter>();
                    filterToBeRemoved.add(originalFilter);
                    gateway.removeFilters(filterToBeRemoved, gateway);
                }
                
                MSGGatewayFilter filter = getFilter(params);

                filter.setDeployed(true);
                filter.setUpdated(false);
                filter.setQueue(queue);
                filter.setUsername(username);
                
                Map<String, Filter> gatewayFilters = gateway.getFilters();
                gatewayFilters.put(name, filter);
                
                filters.put(name, filter);
                msgGatewayFilters.put(name, filter);
                deployedFilters.add(filter);
            }
        }
            
        if (isPrimary() && isActive()) {
            for(Filter deployedFilter : deployedFilters) {
                try {
                    gateway.initMSGListener(deployedFilter);
                    
                    List<Filter> orderedFilters = gateway.getOrderedFilters();
                    Map<String, Filter> orderedFiltersMapById = gateway.getOrderedFiltersMapById();
                    
                    synchronized(orderedFilters)
                    {
                        for(Iterator<Filter> it=deployedFilters.iterator(); it.hasNext();)
                            orderedFilters.add(it.next());
    
                        Collections.sort(orderedFilters);
                        
                        orderedFiltersMapById.clear();
                        
                        for (Filter filter : orderedFilters)
                            orderedFiltersMapById.put(filter.getId(), filter);
                    }
                } catch (Exception e) {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
        
        return deployedFilters;
    } // deployFilters()

    public List<Filter> undeployFilters(List<Map<String, Object>> filterList, String username) throws Exception {
        
        Log.log.debug("Calling Gateway specific undeployFilters" + (filterList != null ? " " + filterList.size() + ", [" + filterList + "]" : ""));
 
        List<Filter> undeployedFilters = new ArrayList<Filter>();
        
        String gatewayName = config.getGatewayName();
        MSGGateway gateway = ConfigReceiveMSGGateway.getGateways().get(gatewayName);
        if(gateway == null)
            throw new Exception("Gateway " + gatewayName + " does not exist");
        
        for (Map<String, Object> params : filterList) {
            if (!params.containsKey("RESOLVE_USERNAME"))
            {
                String name = (String)params.get("ID");

                filters.remove(name);
                msgGatewayFilters.remove(name);

                MSGGatewayFilter filter = getFilter(params);

                filter.setDeployed(false);
                filter.setUpdated(false);
                filter.setUsername(username);
                
                undeployedFilters.add(filter);
            }
        }
            
        if (isPrimary() && isActive()) {
            try {
                List<Filter> orderedFilters = gateway.getOrderedFilters();
                Map<String, Filter> orderedFiltersMapById = gateway.getOrderedFiltersMapById();
                
                gateway.removeFilters(undeployedFilters, gateway);
                
                synchronized(orderedFilters)
                {
                    for(Iterator<Filter> it=undeployedFilters.iterator(); it.hasNext();) {
                        Filter undeployedFilter = it.next();
                        
                        for(Filter orderedFilter:orderedFilters) {
                            if(orderedFilter.getId().equals(undeployedFilter.getId()))
                                orderedFilters.remove(orderedFilter);
                        }
                    }

                    Collections.sort(orderedFilters);
                    
                    orderedFiltersMapById.clear();
                    
                    for (Filter filter : orderedFilters)
                        orderedFiltersMapById.put(filter.getId(), filter);
                }
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        return undeployedFilters;
    } //undeployFilters()

    protected void removeFilters(List<Filter> undeployedFilters, MSGGateway gateway) {

        Map<String, Filter> gatewayFilters = gateway.getFilters();
        
        for(Filter undeployedFilter : undeployedFilters)
        {
            MSGGatewayFilter filter = (MSGGatewayFilter)undeployedFilter;
            String id = filter.getId();

            try {
                stopFilterListeners(filter);
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }

            gatewayFilters.remove(id);
        }
    }
    
    protected void stopFilterListeners(Filter filter) {
        throw new UnsupportedOperationException("You must override this method in order to stop what had been started by that filter!");
    }
    
    @Override
    public MSGGatewayFilter getFilter(Map<String, Object> params) {
        
        return configureMSGGatewayFilter(params);
    }
    
    public Map<String, Object> loadSystemProperties(String gatewayName) {

        Map<String, Object> systemProperties = new HashMap<String, Object>();
        
        MSGGatewayProperties properties = ConfigReceiveMSGGateway.getGatewayProperties(gatewayName);

        systemProperties.put("HOST", ((MSGGatewayProperties)properties).getHost());
        systemProperties.put("PORT", ((MSGGatewayProperties)properties).getPort());
        systemProperties.put("SSL", ((MSGGatewayProperties)properties).isSsl());
        systemProperties.put("SSLTYPE", ((MSGGatewayProperties)properties).getSslType());
        systemProperties.put("USER", ((MSGGatewayProperties)properties).getUser());
        systemProperties.put("PASS", ((MSGGatewayProperties)properties).getPass());

        return systemProperties;
    }
    
    
    /*
    
    protected QueueConnection getQueueConnection(MSGGatewayFilter filter) throws Exception {
    
        return null;
    }
    
    protected QueueSession getQueueSession(MSGGatewayFilter filter, QueueConnection connection) throws Exception {
        
        return null;
    }
    
    protected String getConnectionKey(MSGGatewayFilter filter) {
        
        return null;
    }
    */

    /**
     * This method processes the message received from the IBMmq system.
     *
     * @param message
     */
    public boolean processData(MSGGatewayFilter msgGatewayFilter, MSGGateway gateway, Map<String, String> params) {
        
        boolean result = true;
        
        try {
            if (msgGatewayFilter != null && msgGatewayFilter.isActive()) {
                if (Log.log.isDebugEnabled()) {
                    Log.log.debug("Processing filter: " + msgGatewayFilter.getId());
                    Log.log.debug("Data received through IBMmq gateway: " + params);
                }
                Map<String, String> runbookParams = params;
                
                runbookParams.put(FILTER_ID_NAME, msgGatewayFilter.getId());
                runbookParams.put(GATEWAY_NAME, msgGatewayFilter.getGatewayName());
                runbookParams.put(GATEWAY_TYPE, "MSG");
                
                if (!params.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(params.get(Constants.EVENT_EVENTID)))
                    runbookParams.put(Constants.EVENT_EVENTID, msgGatewayFilter.getEventEventId());

                // addToPrimaryDataQueue(ibmmqFilter, runbookParams);
                // send to the worker queue
                if (MainBase.esb.sendInternalMessage(workerQueueName, gateway.getMessageHandlerName() + ".receiveData", runbookParams))
                {
                    if (Log.log.isTraceEnabled())
                    {
                        Log.log.trace(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        Log.log.trace("Sent data to worker queue: " + workerQueueName);
                        Log.log.trace(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                    }
                }
                else
                {
                    result = false;
                    Log.log.warn("Could not send data to worker queue: " + workerQueueName);
                }
            }
        } catch (Exception e) {
            Log.log.warn("Error: " + e.getMessage(), e);
            try {
                Thread.sleep(interval);
            } catch (InterruptedException ie) {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
        
        return result;
    }
    
    public String processBlockingData(String filterName, Map<String, String> params)
    {
        String result = "";
        
        try
        {
            if(StringUtils.isNotBlank(filterName) && params != null)
            {
                MSGGatewayFilter ibmmqFilter = (MSGGatewayFilter) filters.get(filterName);
                if (ibmmqFilter != null && ibmmqFilter.isActive()) {
                    if(Log.log.isDebugEnabled())
                    {
                        Log.log.debug("Processing filter: " + ibmmqFilter.getId());
                        Log.log.debug("Data received through IBMMQ gateway: " + params);
                    }
                    
                    Map<String, String> runbookParams = params;
                    
                    if(!params.containsKey(Constants.EVENT_EVENTID) || StringUtils.isBlank(params.get(Constants.EVENT_EVENTID)))
                    {
                        runbookParams.put(Constants.EVENT_EVENTID, ibmmqFilter.getEventEventId());
                    }

                    runbookParams.put("FILTER_ID", filterName);
                    
                    result = instance.receiveData(runbookParams);
                }
                
                else {
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "The filter is inactive or runbook is not available.");
                    result = message.toString();
                }
            }
        } catch (Exception e) {
            Log.log.warn("Error: " + e.getMessage(), e);
            try {
                Thread.sleep(interval);
            } catch (InterruptedException ie) {
                Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
            }
        }
        
//        System.out.println("Response is: " + result);
        
        return result;
    }

    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList) {
        Log.log.debug("Calling Gateway specific clearAndSetFilter" + (filterList != null ? " " + filterList.size() + ", [" + filterList + "]" : ""));
        
        try {
            super.clearAndSetFilters(filterList);
            
            if(((ConfigReceiveMSGGateway)configurations).isActive() && isPrimary())
                initMSGListeners();
        } catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    @Override
    public void stop() {
        suspend();
        
        if(heartBeat != null)
            heartBeat.stopHeartBeat();
        
        super.stop(); 
    }
    
    private MSGGatewayFilter configureMSGGatewayFilter(Map<String, Object> filterAttrs) {
        
        String id = null;
        String active = null;
        String order = null;
        String eventEventId = null;
        String runbook = null;
        String script = null;
        
        for(Iterator<String> it=filterAttrs.keySet().iterator(); it.hasNext();) {
            String key = (String)it.next();
            String value = (String)(filterAttrs.get(key));
            if("ID".equals(key))
                id = value;
            else if("ACTIVE".equals(key))
                active = value;
            else if("ORDER".equals(key))
                order = value;
            else if("EVENT_EVENTID".equals(key))
                eventEventId = value;
            else if("RUNBOOK".equals(key))
                runbook = value;
            else if("SCRIPT".equals(key))
                script = value;
        }
        
        MSGGatewayFilter filter = new MSGGatewayFilter(id, active, order, eventEventId, runbook, script);
        
        try {
            filter.setGatewayName((String)filterAttrs.get("GATEWAY_NAME"));
            filter.setQueue((String)filterAttrs.get("QUEUE"));
            filter.setUser((String)filterAttrs.get("USERNAME"));
            filter.setPass((String)filterAttrs.get("PASSWORD"));
            
            String ssl = (String)filterAttrs.get("SSL");
            filter.setSsl(ssl);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        
        filter.addAttributes(filterAttrs);
        
        return filter;
    }
    
    protected int getServerCheckInterval() {
        
        GatewayProperties properties = ConfigReceiveMSGGateway.getGatewayProperties(name);
        if(properties != null)
            return properties.getServerCheckInterval();

        return 0;
    }

    protected int getServerCheckRetry() {

        GatewayProperties properties = ConfigReceiveMSGGateway.getGatewayProperties(name);
        if(properties != null)
            return properties.getServerCheckRetry();

        return 0;
    }
    
}