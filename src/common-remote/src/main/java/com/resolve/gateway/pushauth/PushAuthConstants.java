package com.resolve.gateway.pushauth;

public final class PushAuthConstants
{

    public static final String PROPERTY_NAME_HTTP_AUTH_BASIC_PASSWORD = "httpAuthBasicPassword";   
    public static final String PROPERTY_NAME_HTTP_AUTH_BASIC_USERNAME = "httpAuthBasicUsername";
    public static final String PROPERTY_NAME_HTTP_AUTH_TYPE = "httpAuthType";
    public static final String PROPERTY_NAME_HTTP_URI = "uri";

}
