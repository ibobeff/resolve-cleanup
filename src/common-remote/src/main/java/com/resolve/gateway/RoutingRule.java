package com.resolve.gateway;

import java.io.Serializable;

import com.resolve.gateway.resolutionrouting.GatewayRRSettings;
import com.resolve.gateway.resolutionrouting.SIRRRSettings;
import com.resolve.gateway.resolutionrouting.UIRRSettings;

public class RoutingRule implements Serializable {

	private static final long serialVersionUID = 7603254752344441582L;
	private final String rid;
	private final String owner;
	private final UIRRSettings uiRSettings;
	private final GatewayRRSettings gatewayRRSettings;
	private final SIRRRSettings sirRRSettings;

	private RoutingRule(RoutingRuleBuilder builder) {

		this.rid = builder.rid;
		this.owner = builder.owner;
		this.uiRSettings = builder.uiRRSEttings;
		this.gatewayRRSettings = builder.gatewayRRSettings;
		this.sirRRSettings = builder.sirRRSettings;
	}

	public String getRid() {
		return rid;
	}

	public String getOwner() {
		return owner;
	}

	public UIRRSettings getUiRSettings() {
		return uiRSettings;
	}

	public GatewayRRSettings getGatewayRRSettings() {
		return gatewayRRSettings;
	}

	public SIRRRSettings getSirRRSettings() {
		return sirRRSettings;
	}

	public static class RoutingRuleBuilder {

		private final String rid;
		private final String owner;
		private UIRRSettings uiRRSEttings;
		private GatewayRRSettings gatewayRRSettings;
		private SIRRRSettings sirRRSettings;

		public RoutingRuleBuilder(String rid, String owner) {

			this.rid = rid;
			this.owner = owner;
		}

		public RoutingRuleBuilder uiRRSettings(UIRRSettings ui) {

			this.uiRRSEttings = ui;
			return this;
		}

		public RoutingRuleBuilder gatewayRRSettings(GatewayRRSettings gateway) {

			this.gatewayRRSettings = gateway;
			return this;
		}

		public RoutingRuleBuilder sirRRSettings(SIRRRSettings sir) {

			this.sirRRSettings = sir;
			return this;
		}

		public RoutingRule build() {

			return new RoutingRule(this);
		}
	}

	@Override
	public String toString() {
		
		// TODO add all properties to the toString() output
		return String.format("%s [rid=%s]", RoutingRule.class.getSimpleName(), rid);
	}
}
