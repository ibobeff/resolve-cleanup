/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.tsrm;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.resolve.query.ResolveQuery;
import com.resolve.query.translator.TSRMQueryTranslator;
import com.resolve.util.StringUtils;

/**
 * Abstract class for all the TSRM object service provider classes.
 * 
 */
public abstract class AbstractObjectService implements ObjectService
{
    //protected final ConfigReceiveTSRM configurations;
    protected RestCaller restCaller;
    private final ResolveQuery resolveQuery;
    
    private static String WORKLOG_OBJECT_IDENTITY = "worklog";

    protected static final String OBJECT_CLASS = "class";

    public AbstractObjectService(RestCaller restCaller)
    {
        this.restCaller = restCaller;
        this.resolveQuery = new ResolveQuery(new TSRMQueryTranslator());
        //this.configurations = configurations;
    }

    public RestCaller getRestCaller()
    {
        return restCaller;
    }

    public void setRestCaller(RestCaller restCaller)
    {
        this.restCaller = restCaller;
    }

    protected boolean validateObjectId(String objectId) throws Exception
    {
        if (StringUtils.isEmpty(objectId))
        {
            throw new Exception("ID must be provided");
        }
        else
        {
            return true;
        }
    }
    
    @SuppressWarnings("unchecked")
    public List<Map<String, String>> unmarshal(String xmlContent) throws Exception
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        SAXReader reader = new SAXReader();
        Document document = reader.read(new ByteArrayInputStream(xmlContent.getBytes("UTF-8")));
        List<Element> elements = document.getRootElement().elements(getMainNodeName());
        for (Element element : elements)
        {
            List<Element> children = element.elements();
            for (Element child : children)
            {
                result.add(unmarshalObject(child));
            }
        }
        return result;
    }

    public Map<String, String> unmarshalObject(String xmlContent) throws Exception
    {
        Map<String, String> result = new HashMap<String, String>();

        if (!StringUtils.isEmpty(xmlContent))
        {
            SAXReader reader = new SAXReader();
            Document document = reader.read(new ByteArrayInputStream(xmlContent.getBytes("UTF-8")));
            List<Element> mainNodes = document.getRootElement().elements(getMainNodeName());

            if (mainNodes != null && mainNodes.size() > 0)
            {
                // We expect only one element.
                Element mainNode = mainNodes.get(0);
                List<Element> nodes = mainNode.elements(getObjectNodeName());
                for (Element node : nodes)
                {
                    result = unmarshalObject(node);
                }
            }
        }
        return result;
    }

    /**
     * This method does the final object level (e.g., an SR object)
     * unmarshalling.
     * 
     * @param xmlContent
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    private Map<String, String> unmarshalObject(Element element) throws Exception
    {
        Map<String, String> result = new HashMap<String, String>();

        if (element != null)
        {
            List<Element> childElements = element.elements();
            for (Element childElement : childElements)
            {
                result.put(childElement.getName(), childElement.getText());
            }
        }
        return result;
    }

    /**
     * Prepends special phrase for worklog to each parameter's key. Since
     * Worklog is a child object, TSRM REST api has a different way of handling
     * the data. Example: worklog.id1.clientviewable=0 but caller will send only
     * the actual element name (i.e., clientviewable).
     * 
     * @param level
     * @param params
     * @return
     */
    protected Map<String, String> getModifiedParams(int level, Map<String, String> params)
    {
        // need to preppend worklog.idx to the param keys
        // e.g., worklog.id1.clientviewable
        Map<String, String> modifiedParams = new HashMap<String, String>();
        for (String key : params.keySet())
        {
            modifiedParams.put(WORKLOG_OBJECT_IDENTITY + ".id" + level + "." + key, params.get(key));
        }
        return modifiedParams;
    }

    /**
     * This method may be overriden in the subclass.
     */
    @Override
    public Map<String, String> create(Map<String, String> objectProperties, String username, String password) throws Exception
    {
        String response = null;

        // This sets it as class=INCIDENT.
        objectProperties.put(OBJECT_CLASS, getObjectNodeName());

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = restCaller.getUsername();
            password = restCaller.getPassword();
        }
        restCaller.setUrl(restCaller.synthesizeUrl(restCaller.getOriginalUrl(), username, password, getIdentity()));
        response = restCaller.postMethod(objectProperties);
        // response = restCaller.postMethodWithQueryString(objectProperties);
        return unmarshalObject(response);
    }

    @Override
    public boolean createWorklog(String parentObjectId, Map<String, String> params, String username, String password) throws Exception
    {
        throw new Exception("Awful! If supposterd by the subclass it should have been implemented by it.");
    }

    @Override
    public void update(String objectId, Map<String, String> objectProperties, String username, String password) throws Exception
    {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = restCaller.getUsername();
            password = restCaller.getPassword();
        }
        // this will throw Exception is objectId is null or empty.
        validateObjectId(objectId);

        restCaller.setUrl(restCaller.addObjectId(objectId, restCaller.getOriginalUrl(), username, password, getIdentity()));
        restCaller.postMethod(objectProperties);
    }

    @Override
    public void delete(String objectId, String username, String password) throws Exception
    {
        // this will throw Exception is objectId is null or empty.
        validateObjectId(objectId);

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = restCaller.getUsername();
            password = restCaller.getPassword();
        }
        restCaller.setUrl(restCaller.addObjectId(objectId, restCaller.getOriginalUrl(), username, password, getIdentity()));
        restCaller.deleteMethod(objectId);
    }

    @Override
    public Map<String, String> selectByID(String objectId, String username, String password) throws Exception
    {
        // this will throw Exception is objectId is null or empty.
        validateObjectId(objectId);

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = restCaller.getUsername();
            password = restCaller.getPassword();
        }
        restCaller.setUrl(restCaller.addObjectId(objectId, restCaller.getOriginalUrl(), username, password, getIdentity()));
        return unmarshalObject(restCaller.getMethod(null));
    }

    @Override
    public List<Map<String, String>> search(String query, String username, String password) throws Exception
    {
        List<Map<String, String>> result = new ArrayList<Map<String,String>>();
        
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = restCaller.getUsername();
            password = restCaller.getPassword();
        }
        restCaller.setUrl(restCaller.synthesizeUrl(restCaller.getOriginalUrl(), username, password, getIdentity()));
        String tmpQuery = null;
        try
        {
            tmpQuery = resolveQuery.translate(query);
        }
        catch (Exception e)
        {
            //the query must be in native format, ignoring this error and using the raw query.
            tmpQuery = query;
        }
        
        result = unmarshal(restCaller.getMethod(tmpQuery));
        
        return result;
    }

    @Override
    public String search(String objectURI, String query, String username, String password) throws Exception
    {
        String result = null;
        
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = restCaller.getUsername();
            password = restCaller.getPassword();
        }
        restCaller.setUrl(restCaller.synthesizeUrl(restCaller.getOriginalUrl(), username, password, objectURI));
        try
        {
            String nativeQuery = resolveQuery.translate(query);
            result = restCaller.getMethod(nativeQuery);
        }
        catch (Exception e)
        {
            //the query must be in native format, ignoring this error and using the raw query.
            result = restCaller.getMethod(query);
        }
        return result;
    }
}
