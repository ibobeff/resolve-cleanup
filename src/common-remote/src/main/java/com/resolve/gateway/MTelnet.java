/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.gateway;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.ssh.SSHGateway;
import com.resolve.gateway.ssh.SubnetMask;
import com.resolve.gateway.telnet.TelnetGateway;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MTelnet
{
    private static final String RSCONTROL_SET_POOLS = "MTelnet.setPools";

    /**
     * Returns a {@link TelnetGatewayConnection} using the given ipAddress.
     * 
     * @param ipAddress
     *            the IP address of the Telnet server you're connecting to
     * @return TelnetGatewayConnection object
     * @throws Exception
     *             if a connection error occurs
     */
    public static TelnetGatewayConnection checkOutConnection(String ipAddress) throws Exception
    {
        return TelnetGateway.getInstance().checkOutConnection(ipAddress);
    }

    /**
     * Returns a {@link TelnetGatewayConnection} using the given ipAddress and
     * port number
     * 
     * @param ipAddress
     *            the IP address of the Telnet server you're connecting to
     * @param port
     *            the HTTPS port number
     * @return TelnetGatewayConnection object
     * @throws Exception
     *             if a connection error occurs
     */
    public static TelnetGatewayConnection checkOutConnection(String ipAddress, int port) throws Exception
    {
        return TelnetGateway.getInstance().checkOutConnection(ipAddress, port);
    }

    /**
     * Returns a {@link TelnetGatewayConnection} using the given ipAddress and
     * port number
     * 
     * @param ipAddress
     *            the IP address of the Telnet server you're connecting to
     * @param port
     *            the HTTPS port number
     * @param terminalType
     *            the terminal type of the Telnet connection. Can be "VT100",
     *            "VT52", "ANSI", or "VTNT".
     * @return TelnetGatewayConnection object
     * @throws Exception
     *             if a connection error occurs
     */
    public static TelnetGatewayConnection checkOutConnection(String ipAddress, int port, String terminalType) throws Exception
    {
        return TelnetGateway.getInstance().checkOutConnection(ipAddress, port, terminalType);
    }

    /**
     * Returns a {@link TelnetGatewayConnection} using the given ipAddress
     * 
     * @param ipAddress
     *            the IP address of the Telnet server you're connecting to
     * @param port
     *            the HTTPS port number
     * @param terminalType
     *            the terminal type of the Telnet connection. Can be "VT100",
     *            "VT52", "ANSI", or "VTNT".
     * @param suppressGAOption
     *            controls whether to supress Telnet's go-ahead option
     * @param echoOption
     *            controls whether to enable Telnet's echo option
     * @return TelnetGatewayConnection object
     * @throws Exception
     *             if a connection error occurs
     */
    public static TelnetGatewayConnection checkOutConnection(String ipAddress, int port, String terminalType, boolean suppressGAOption, boolean echoOption) throws Exception
    {
        return TelnetGateway.getInstance().checkOutConnection(ipAddress, port, terminalType, suppressGAOption, echoOption);
    }

    /**
     * Checks a connection into the current instance of the
     * {@link TelnetGateway} using the given {@link TelnetGatewayConnection}
     * object
     * 
     * @param connection
     *            the TelnetGatewayConnection instance to pass to the
     *            TelnetGateway
     * @throws Exception
     *             if a connection error occurs
     */
    public static void checkInConnection(TelnetGatewayConnection connection) throws Exception
    {
        TelnetGateway.getInstance().checkInConnection(connection);
    }

    /**
     * Clears all {@link SubnetMask} objects from the {@link TelnetGateway}'s
     * {@link SubnetMask} pool. If paramList isn't null, it will use its List of
     * parameter {@link Maps} to propagate the pools with new SubnetMask
     * objects.
     * 
     * @param paramList
     *            A {@link List} of {@link Map} parameters. Map keys include:
     *            {SubnetMask.SUBNETMASK : String}
     */
    public static void clearAndSetPools(List<Map<String, String>> paramList)
    {
        // clear and set pools
        TelnetGateway.getInstance().clearAndSetPools(paramList);

        // send new pools
        getPools(null);

        // save config
        MainBase.main.exitSaveConfig();

    } // clearAndSetPools

    /**
     * Sets the {@link SubnetMask} pool of the current {@link TelnetGateway}
     * instance using the given {@link Map} of parameters.
     * 
     * @param params
     *            A {@link Map} of {@link SubnetMask} initialization parameters.
     *            paramList keys include: { SubnetMask.SUBNETMASK : String,
     *            SubnetMask.MAXCONNECTION : String, SubnetMask.TIMEOUT : String
     *            }
     * 
     */
    public static void setPool(Map params)
    {
        TelnetGateway.getInstance().setPool(params);

        // save config
        MainBase.main.exitSaveConfig();

    } // setPool

    /**
     * Sends an internal message to the Enterprise Service Bus (ESB) of the
     * {@link MainBase} which details all properties of each {@link SubnetMask}
     * object in the {@link TelnetGateway} {@link SubnetMask} pool.
     * 
     * @param params
     *            A {@link Map} of {@link SubnetMask} initialization parameters.
     *            paramList keys include: { SubnetMask.SUBNETMASK : String,
     *            SubnetMask.MAXCONNECTION : String, SubnetMask.TIMEOUT : String
     *            }
     */
    public static void getPools(Map params)
    {
        if (TelnetGateway.getInstance().isActive())
        {
            Map<String, String> content = new HashMap<String, String>();

            for (SubnetMask pool : TelnetGateway.getInstance().getPools().values())
            {
                Map<String, String> poolMap = new HashMap<String, String>();
                poolMap.put(SubnetMask.QUEUE, TelnetGateway.getInstance().getQueueName());
                poolMap.put(SubnetMask.SUBNETMASK, pool.getSubnetMask());
                poolMap.put(SubnetMask.MAXCONNECTION, "" + pool.getMaxConnection());
                poolMap.put(SubnetMask.TIMEOUT, "" + pool.getTimeout());
                content.put(pool.getSubnetMask(), StringUtils.remove(StringUtils.mapToString(poolMap), "null"));
            }

            // send content to rscontrol
            // if (content.size() > 0)
            {
                content.put("QUEUE", TelnetGateway.getInstance().getQueueName());

                if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROL, RSCONTROL_SET_POOLS, content) == false)
                {
                    Log.log.warn("Failed to send telnet connection pools to RSCONTROL");
                }
            }
        }
    } // getPools
}
