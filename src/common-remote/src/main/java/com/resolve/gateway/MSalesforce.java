/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.Map;

import com.resolve.gateway.salesforce.SalesforceFilter;
import com.resolve.gateway.salesforce.SalesforceGateway;

/**
 * This class is used by the Message Listener and invokes it methods. Messages
 * destined for the gateways set this class name in the message header.
 *
 */
public class MSalesforce extends MGateway
{
    private static final String TRUE = "TRUE";
    private static final String RSCONTROL_SET_FILTERS = "MSalesforce.setFilters";

    private static final SalesforceGateway instance = SalesforceGateway.getInstance();

    public MSalesforce()
    {
        super.instance = instance;
    }

    @Override
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    @Override
    protected void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        SalesforceFilter salesforceFilter = (SalesforceFilter) filter;
        filterMap.put(SalesforceFilter.QUERY, salesforceFilter.getQuery());
        filterMap.put(SalesforceFilter.OBJECT, salesforceFilter.getObject());
    }
}
