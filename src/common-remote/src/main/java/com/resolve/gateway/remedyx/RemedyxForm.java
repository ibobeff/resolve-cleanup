/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.remedyx;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class RemedyxForm
{
    public static final String NAME = "NAME";
    public static final String FIELD_LIST = "FIELD_LIST";
    public static final String QUEUE = "QUEUE";

    private String name;
    private String fieldList;
    private int[] fieldIds;

    public RemedyxForm(String name, String fieldList)
    {
        super();
        this.name = name;
        this.fieldList = fieldList;
        if (StringUtils.isNotEmpty(fieldList))
        {
            String[] tmpFieldIds = fieldList.split(",");
            fieldIds = new int[tmpFieldIds.length];
            for (int i = 0; i < tmpFieldIds.length; i++)
            {
                try
                {
                    fieldIds[i] = Integer.parseInt(tmpFieldIds[i].trim());
                }
                catch (NumberFormatException e)
                {
                    Log.log.warn("Invalid form field name in form:" + name + ", field: " + tmpFieldIds[i] + ", must be numeric.");
                }
            }
        }
    }

    public void setPool(String Name, String fieldList)
    {
        this.name = Name;
        this.fieldList = fieldList;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getFieldList()
    {
        return fieldList;
    }

    public void setFieldList(String fieldList)
    {
        this.fieldList = fieldList;
    }

    public int[] getFieldIds()
    {
        return fieldIds;
    }

    public void setFieldIds(int[] fieldIds)
    {
        this.fieldIds = fieldIds;
    }
}
