/******************************************************************************
* (C) Copyright 2015
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.caspectrum;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.HttpHeaders;

import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveCASpectrum;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.RestCallException;
import com.resolve.util.restclient.RestCaller;

/**
 * Generic CASpectrum object service provider class.
 * Uses REST API
 * 
 * @author hemant.phanasgaonkar
 */
public class GenericObjectService implements ObjectService
{
    protected final ConfigReceiveCASpectrum configurations;
    protected final RestCaller restCaller;
    
    private static final String ATTRIB_NAME_VALUE_PAIR_KEY_PREFIX = "attr";
    private static final String ATTRIB_NAME_VALUE_PAIR_VALUE_PREFIX = "val";
    
    private ConcurrentHashMap<String, String> filterIdToSubscriptionMap;

    public GenericObjectService(ConfigReceiveCASpectrum configurations, ConcurrentHashMap<String, String> filterIdToSubscriptionMap)
    {
        restCaller = new RestCaller(configurations.getUrl(), null, null, MainBase.main.configProxy);

        this.configurations = configurations;
        this.filterIdToSubscriptionMap = filterIdToSubscriptionMap;
    }
    
    private void setupRestCaller(String objectType, String username, String p_assword)
    {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(p_assword))
        {
            username = configurations.getHttpbasicauthusername();
            p_assword = configurations.getHttpbasicauthpassword();
        }

        restCaller.setHttpbasicauthusername(username);
        restCaller.setHttpbasicauthpassword(p_assword);
        
        restCaller.setUrlSuffix(objectType);
    }
    
    public List<Map<String, String>> getObjects(String objectType, 
                                                List<String> attributes, 
                                                List<String> landscapeHandles, 
                                                int throttlesize, 
                                                String username, 
                                                String p_assword) throws Exception
    {
        setupRestCaller(objectType, username, p_assword);
        
        Map<String, List<String>> reqParams = new HashMap<String, List<String>>();
        
        String key = null;
        
        switch (objectType)
        {
            case "alarms":
            case "devices":
            case "model":
            case "models":
                key = ATTRIB_NAME_VALUE_PAIR_KEY_PREFIX;
                break;
        }
        
        if (landscapeHandles != null && !landscapeHandles.isEmpty())
        {
            reqParams.put("landscape", landscapeHandles);
        }
        
        int throttlesizeToSet = throttlesize;
        
        if (throttlesize <= 0 || throttlesize > configurations.getThrottleSize())
        {
            throttlesizeToSet = configurations.getThrottleSize();
        }
        
        List<String> throttlesizes = new ArrayList<String>();
        throttlesizes.add(Integer.toString(throttlesizeToSet));
        
        reqParams.put("throttlesize", throttlesizes);
        
        String response = restCaller.getMethod(null, attributes, key, reqParams, null);

        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("Get " + objectType + (landscapeHandles != null && !landscapeHandles.isEmpty() ? ", for landscapes " + landscapeHandles : "") + " returned [" + response + "]");
        }
        
        return parseChunkedResponse(objectType, response, null, null);
    }
    
    public List<Map<String, String>> getObjects(String objectType, 
                                                String urlQuery, 
                                                String xmlQuery, 
                                                String username, 
                                                String p_assword, 
                                                String filterId,
                                                String subscriptionId) throws Exception
    {
        boolean isSubscription = objectType.equalsIgnoreCase("subscription") ? true : false;
        
        String objType = objectType;
        String filteredUrlQuery = urlQuery;
        
        if (StringUtils.isNotBlank(urlQuery) || objectType.equalsIgnoreCase("landscapes") ||
            (isSubscription && StringUtils.isBlank(urlQuery) && 
             StringUtils.isBlank(xmlQuery) && StringUtils.isNotBlank(subscriptionId)))
        {
            switch (objectType)
            {
                case "action":
                case "associations":
                case "model":
                    objType = objectType + "/" + urlQuery.substring(0, urlQuery.indexOf("?"));
                    filteredUrlQuery = urlQuery.substring(urlQuery.indexOf("?") + 1);
                    break;
                    
                case "alarms":
                case "devices":
                case "models":
                    objType = objectType;
                    filteredUrlQuery = urlQuery;
                    break;
                    
                case "attribute":
                case "connectivity":
                case "subscription":
                    objType = objectType + "/" + (StringUtils.isNotBlank(urlQuery) ? urlQuery : subscriptionId);
                    filteredUrlQuery = "";
                    break;
                    
                case "landscapes":
                    objType = objectType;
                    filteredUrlQuery = "";
                    break;
                    
                case "event":
                    objType = "";
                    filteredUrlQuery = "";
                    break;
            }
        }
        
        if (StringUtils.isBlank(objType))
        {
            if (StringUtils.isNotBlank(filteredUrlQuery))
            {
                throw new Exception("Get " + objectType + " not supported for URL query filter");
            }
            else
            {
                throw new Exception("Get " + objectType + " not supported for URL or XML query filter");
            }
        }
            
        setupRestCaller(objType, username, p_assword);
        
        String response = null;
        
        if (StringUtils.isNotBlank(urlQuery) || 
            (isSubscription && StringUtils.isBlank(urlQuery) && StringUtils.isBlank(xmlQuery)) ||
            (objType.equalsIgnoreCase("landscapes")))
        {
            response = restCaller.getMethod(null, filteredUrlQuery, null);
        } 
        else if (StringUtils.isNotBlank(xmlQuery))
        {
            if (isSubscription)
            {
                List<Map<String, String>> pollResult = new ArrayList<Map<String, String>> ();
                
                if (StringUtils.isNotBlank(subscriptionId) || filterIdToSubscriptionMap.containsKey(filterId))
                {
                    String pollSubscriptionId = StringUtils.isNotBlank(subscriptionId) ? subscriptionId : filterIdToSubscriptionMap.get(filterId);
                    
                    Log.log.debug("Polling subscription (" + pollSubscriptionId + ") ...");
                    
                    try
                    {
                        response = restCaller.getMethod(pollSubscriptionId, 
                                                        (Map<String, String>)null, (Map<String, String>)null);
                    
                        pollResult = parsePollResponse(response, pollSubscriptionId);
                        
                        Log.log.debug("Polling subscription (" + pollSubscriptionId + 
                                      ") returned " + pollResult.size() + " objects");
                    } 
                    catch (RestCallException rce)
                    {
                        if (rce.statusCode == HttpStatus.SC_BAD_REQUEST)
                        {
                            Log.log.info("Get Subscription for id (" + pollSubscriptionId + ") failed, " + rce.getMessage());
                            
                            if (StringUtils.isNotBlank(filterId))
                                filterIdToSubscriptionMap.remove(filterId);
                        }
                        else
                            throw rce;
                    }
                    finally
                    {
                        if (StringUtils.isNotBlank(filterId))
                        {
                            try
                            {
                                restCaller.deleteMethod(filterIdToSubscriptionMap.get(filterId));
                            }
                            catch (RestCallException rce)
                            {
                                if (rce.statusCode != HttpStatus.SC_OK)
                                {
                                    Log.log.info("Delete Subscription with id (" + filterIdToSubscriptionMap.get(filterId) + ") failed, " + rce.getMessage());
                                }
                            }
                            
                            filterIdToSubscriptionMap.remove(filterId);
                        }
                    }
                }
                
                if (StringUtils.isNotBlank(filterId))
                {
                    //Create subscription for next poll
                    
                    Map<String, String> reqHeaders = new HashMap<String, String>();
                    reqHeaders.put(HttpHeaders.CONTENT_TYPE, "application/xml");
                    
                    Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
                    expectedStatusCodes.add(new Integer(HttpStatus.SC_OK));
                    
                    response = restCaller.postMethod(null, reqHeaders, xmlQuery, expectedStatusCodes);
                    
                    Map<String, Object> responseJSON = new HashMap<String, Object>();
                    
                    StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);
                    
                    if (responseJSON.isEmpty())
                    {
                        throw new Exception("Subscribe " + objectType +
                                            (StringUtils.isNotBlank(xmlQuery) ? ", for xmlQuery " + xmlQuery : "") +" did not return JSON.");
                    }
                    
                    String newSubscriptionId = parseSubscriptionResponse(responseJSON);
                    
                    if (StringUtils.isBlank(newSubscriptionId))
                    {
                        throw new Exception("Subscribe " + objectType +
                                        (StringUtils.isNotBlank(xmlQuery) ? ", for xmlQuery " + xmlQuery : "") +" did not return subscription Id.");
                    }
                    
                    filterIdToSubscriptionMap.put(filterId, newSubscriptionId);
                    
                    Log.log.debug("Created Subscription (" + newSubscriptionId + 
                                    ") for polling next time around...");
                }
                
                return pollResult;
            }
            else
            {
                //Tunneled GET (POST)
                Map<String, String> reqHeaders = new HashMap<String, String>();
                reqHeaders.put(HttpHeaders.CONTENT_TYPE, "application/xml");
                
                Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
                expectedStatusCodes.add(new Integer(HttpStatus.SC_OK));
                
                response = restCaller.postMethod(null, reqHeaders, xmlQuery, expectedStatusCodes);
            }
        }
        else
        {
            throw new Exception("Invalid Get " + objectType + " request, missing URL or XML query");
        }
        
        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("Get " + objectType + (StringUtils.isNotBlank(urlQuery) ? " for urlQuery " + urlQuery : "") +
                          (StringUtils.isNotBlank(xmlQuery) ? " xmlQuery " + xmlQuery : "") + 
                          " returned [" + (StringUtils.isNotBlank(response) ? response : "") + "]");
        }
        
        return parseChunkedResponse(objectType, response, urlQuery, xmlQuery);
    }
    
    public Map<String, String> issueAnAction(String actionCode, 
                                             String modelHandle, 
                                             Map<String, String> attribValMap, 
                                             int throttlesize,
                                             String username, 
                                             String p_assword) throws Exception
    {
        setupRestCaller("action", username, p_assword);
        
        Map<String, String> reqParams = new HashMap<String, String>();
        
        if (StringUtils.isNotBlank(modelHandle))
        {
            reqParams.put("mh", modelHandle);
        }
        
        int throttlesizeToSet = throttlesize;
        
        if (throttlesize <= 0 || throttlesize > configurations.getThrottleSize())
        {
            throttlesizeToSet = configurations.getThrottleSize();
        }
        
        reqParams.put("throttlesize", Integer.toString(throttlesizeToSet));
        
        String response = restCaller.getMethod(actionCode, attribValMap, ATTRIB_NAME_VALUE_PAIR_KEY_PREFIX, ATTRIB_NAME_VALUE_PAIR_VALUE_PREFIX, reqParams, null);

        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("Get action, action_code = [" + actionCode + "]" + (StringUtils.isNotBlank(modelHandle) ? ", mh = [" + modelHandle + "]" : "") + "]" + 
                          (attribValMap != null && !attribValMap.isEmpty()? ", Attribute Value Map = [" + attribValMap + "]" : "") + " returned [" + response + "]");
        }
        
        return parseActionResponse("action", response, actionCode, modelHandle, attribValMap);
    }
    
    public Map<String, String> updateObject(String objectType, 
                                            String objectId, 
                                            Map<String, String> attribVal, 
                                            String username, 
                                            String p_assword) throws Exception
    {
        setupRestCaller(objectType, username, p_assword);
        
        String response = restCaller.putMethod(objectId, attribVal, ATTRIB_NAME_VALUE_PAIR_KEY_PREFIX, ATTRIB_NAME_VALUE_PAIR_VALUE_PREFIX, null, null, null);
        
        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("Update " + objectType + ", Id = " + objectId + 
                          (attribVal != null && !attribVal.isEmpty()? ", Attribute Value Map = [" + attribVal + "]" : "") + " returned [" + response + "]");
        }
        
        List<Map<String, String>> updatedObjects = parseUpdateResponseList(objectType, objectId, attribVal, response);
        
        if (updatedObjects == null || updatedObjects.isEmpty() || updatedObjects.size() != 1)
        {
            throw new Exception("Update " + objectType + ", Id = " + objectId + 
                                (attribVal != null && !attribVal.isEmpty()? ", Attribute Value Map = [" + 
                                attribVal + "]" : "") + 
                                " did not update any object or updated more than expected number of objects.");
        }
        
        return updatedObjects.get(0); 
    }
    
    public Map<String, String> deleteObject(String objectType, 
                                            String objectId, 
                                            String username, 
                                            String p_assword) throws Exception
    {
        setupRestCaller(objectType, username, p_assword);

        String response = restCaller.deleteMethod(objectId);

        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("Delete " + objectType + ", Id = " + objectId + " returned [" + response + "]");
        }

        Map<String, String> deleteObjectResult = parseDeleteResponse(objectType, objectId, response);

        if (deleteObjectResult == null || (!objectType.equals("subscription") && deleteObjectResult.isEmpty()))
        {
            throw new Exception("Delete " + objectType + ", Id = " + objectId + 
                                " did not delete any object.");
        }

        return deleteObjectResult; 
    }
    
    public Map<String, String> createAssociation(String relationHandle,
                                                 String leftModelHandle,
                                                 String rightModelHandle,
                                                 String username,
                                                 String p_assword) throws Exception
    {
        if (StringUtils.isBlank(relationHandle) || StringUtils.isBlank(leftModelHandle) ||
            StringUtils.isBlank(rightModelHandle))
        {
            throw new Exception("Create Association requires relation handle, left model handle and right model handle to be non-empty");
        }
        
        setupRestCaller("associations/relation/" + relationHandle + "/leftmodel/" + 
                        leftModelHandle + "/rightmodel/" + rightModelHandle, username, p_assword);
        
        Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
        expectedStatusCodes.add(new Integer(HttpStatus.SC_OK));
        
        String response = restCaller.postMethod(null, null, null, expectedStatusCodes);

        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("Create Association for relation handle = " + relationHandle + 
                          ", left model handle = " + leftModelHandle + ", right model handle = " +  
                          rightModelHandle + " returned [" + response + "]");
        }
        
        Map<String, String> createAssociationResult = parseCreateAssociationResponse(relationHandle, leftModelHandle, 
                                                                                     rightModelHandle, response);

        if (createAssociationResult == null || createAssociationResult.isEmpty())
        {
            throw new Exception("Create Association for relation handle = " + relationHandle + 
                                ", left model handle = " + leftModelHandle + ", right model handle = " +  
                                rightModelHandle + " failed");
        }

        return createAssociationResult; 
    }
    
    public List<Map<String, String>> getAssociations(String relationHandle,
                                                     String modelHandle,
                                                     String side,
                                                     String username,
                                                     String p_assword) throws Exception
    {
        if (StringUtils.isBlank(relationHandle) || StringUtils.isBlank(modelHandle) || StringUtils.isBlank(side) ||
            (!side.equals("left") && !side.equals("right")))
        {
            throw new Exception("Get Associations requires relation handle, " +
                                "model handle and side to be non-empty and side to be left or right.");
        }

        setupRestCaller("associations/relation/" + relationHandle + "/model/" + modelHandle, username, p_assword);

        Map<String, String> requestParams = new HashMap<String, String>();
        requestParams.put("side", side);

        String response = restCaller.getMethod(null, requestParams, null);

        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("Get Associations for relation handle = " + relationHandle + 
                          ", model handle = " + modelHandle + ", side = " + side + 
                          " returned [" + response + "]");
        }

        return parseGetAssociationsResponse(relationHandle, modelHandle, side, response);
    }
    
    public Map<String, String> deleteAssociation(String relationHandle, 
                                                 String leftModelHandle, 
                                                 String rightModelHandle, 
                                                 String username, 
                                                 String p_assword) throws Exception
    {
        if (StringUtils.isBlank(relationHandle) || StringUtils.isBlank(leftModelHandle) || StringUtils.isBlank(rightModelHandle))
        {
            throw new Exception("Delete Association requires relation handle, left model handle and right model handle to be non-empty");
        }

        setupRestCaller("associations/relation", username, p_assword);

        Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
        expectedStatusCodes.add(new Integer(HttpStatus.SC_OK));

        String response = restCaller.deleteMethod(relationHandle + "/leftmodel/" + leftModelHandle + "/rightmodel/" + rightModelHandle);

        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("Delete Association for relation handle = " + relationHandle + ", left model handle = " + leftModelHandle + ", right model handle = " + rightModelHandle + " returned [" + response + "]");
        }

        Map<String, String> deleteAssociationResult = parseDeleteAssociationResponse(relationHandle, leftModelHandle, rightModelHandle, response);

        if (deleteAssociationResult == null || deleteAssociationResult.isEmpty())
        {
            throw new Exception("Create Association for relation handle = " + relationHandle + ", left model handle = " + leftModelHandle + ", right model handle = " + rightModelHandle + " failed");
        }

        return deleteAssociationResult;
    }
    
    public Map<String, String> getAttributeEnum(String attrId, 
                                                String username, 
                                                String p_assword) throws Exception
    {
        if (StringUtils.isBlank(attrId))
        {
            throw new Exception("Get Atribute Enumerations requires attribute id");
        }
        
        setupRestCaller("attribute", username, p_assword);
        
        String response = restCaller.getMethod(attrId + "/enums", (Map<String, String>)null, (Map<String, String>)null);
        
        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("Get Attribute Enumerations for attribute id = " + attrId + 
                          " returned [" + response + "]");
        }
        
        return parseGetAttributeEnumResponse(attrId, response);
    }
    
    public Map<String, Map<String, List<Map<String, String>>>> getConnectivity(String ipAddress,
                                                                               String username,
                                                                               String p_assword) throws Exception
    {
        if (StringUtils.isBlank(ipAddress))
        {
            throw new Exception("Get connectivity requires ip address.");
        }

        setupRestCaller("connectivity", username, p_assword);

        String response = restCaller.getMethod(ipAddress, (Map<String, String>)null, (Map<String, String>)null);

        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("Get Connectivity for ip address = " + ipAddress + " returned [" + response + "]");
        }

        return parseGetConnectivityResponse(ipAddress, response);
    }
    
    public List<Map<String, String>> getLandscapes(String username, String p_assword) throws Exception
    {
        setupRestCaller("landscapes", username, p_assword);

        String response = restCaller.getMethod(null, (Map<String, String>) null, (Map<String, String>) null);

        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("Get Landscapes returned [" + response + "]");
        }

        return parseGetLandscapesResponse(response);
    }
    
    public Map<String, String> createModel(String landscapeHandle,
                                           String modelTypeHandle,
                                           int snmpAgentPort,
                                           String communityString,
                                           int retryCount,
                                           int timeout,
                                           String ipAddress,
                                           String parentModelHandle,                                                
                                           String parentToModelRelationHandle,
                                           Map<String, String> attribValMap,
                                           String username,
                                           String p_assword) throws Exception
    {
                    
        setupRestCaller("model", username, p_assword);

        Map<String, String> reqParams = new HashMap<String, String>();
        
        if (StringUtils.isNotBlank(landscapeHandle))
            reqParams.put("landscapeid", landscapeHandle);
        
        if (StringUtils.isNotBlank(modelTypeHandle))
            reqParams.put("mtypeid", modelTypeHandle);
        
        if (snmpAgentPort > 0)
            reqParams.put("agentport", Integer.toString(snmpAgentPort));
        
        if (StringUtils.isNotBlank(communityString))
            reqParams.put("commstring", communityString);
        
        if (retryCount > 0)
            reqParams.put("retry", Integer.toString(retryCount));
        
        if (timeout > 0)
            reqParams.put("timeout", Integer.toString(timeout));
        
        if (StringUtils.isNotBlank(ipAddress))
            reqParams.put("ipaddress", ipAddress);
        
        if (StringUtils.isNotBlank(parentModelHandle))
            reqParams.put("parentmh", parentModelHandle);
        
        if (StringUtils.isNotBlank(parentToModelRelationHandle))
            reqParams.put("relationid", parentToModelRelationHandle);
        
        Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
        expectedStatusCodes.add(new Integer(HttpStatus.SC_OK));

        String response = restCaller.postMethod(reqParams, null, null, attribValMap, 
                                                ATTRIB_NAME_VALUE_PAIR_KEY_PREFIX , 
                                                ATTRIB_NAME_VALUE_PAIR_VALUE_PREFIX, 
                                                expectedStatusCodes);

        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("Create Model for request parameters = " + reqParams + " and attribute value map = " + attribValMap + " returned [" + response + "]");
        }

        Map<String, String> createModelResult = parseCreateModelResponse(reqParams, attribValMap, response);

        if (createModelResult == null || createModelResult.isEmpty())
        {
            throw new Exception("Create Mode for relation request parameters = " + reqParams + " and attribute value map = " + attribValMap + " failed");
        }

        return createModelResult;
    }
    
    public Map<String, String> getModel(String modelHandle,
                                        List<String> attributes,
                                        String username,
                                        String p_assword) throws Exception
    {
        if (StringUtils.isBlank(modelHandle))
        {
            throw new Exception("Get model requires model handle.");
        }

        setupRestCaller("model", username, p_assword);

        String response = restCaller.getMethod(modelHandle, attributes, 
                                               ATTRIB_NAME_VALUE_PAIR_KEY_PREFIX,
                                               (Map<String, List<String>>) null, 
                                               (Map<String, String>) null);

        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("Get Model for model handle = " + modelHandle + " and attributes [" + 
                          attributes + "] returned [" + response + "]");
        }

        Map<String, String> getModelResult = parseGetModelResponse(modelHandle, attributes, response);

        if (getModelResult == null || getModelResult.isEmpty())
        {
            throw new Exception("Get Model for model handle = " + modelHandle + " and attributes = " + attributes + " failed");
        }

        return getModelResult;
    }
    
    public List<Map<String, String>> updateObjects(String objectType, 
                                                   String xmlQuery, 
                                                   String username, 
                                                   String p_assword) throws Exception
    {
        if (StringUtils.isBlank(objectType) || !objectType.equalsIgnoreCase("models"))
        {
            throw new Exception("Update objects is currently supported for models only");
        }
        
        setupRestCaller(objectType, username, p_assword);

        String response = null;

        if (StringUtils.isNotBlank(xmlQuery))
        {
            Map<String, String> reqHeaders = new HashMap<String, String>();
            reqHeaders.put(HttpHeaders.CONTENT_TYPE, "application/xml");

            Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
            expectedStatusCodes.add(new Integer(HttpStatus.SC_OK));

            response = restCaller.postMethod(null, reqHeaders, xmlQuery, expectedStatusCodes);
        }
        else
        {
            throw new Exception("Invalid Update " + objectType + " request, missing XML query");
        }

        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("Update " + objectType + (StringUtils.isNotBlank(xmlQuery) ? " xmlQuery " + xmlQuery : "") + 
                          " returned [" + (StringUtils.isNotBlank(response) ? response : "") + "]");
        }

        return parseUpdateModelsResponse(response, xmlQuery);
    }
    
    public Map<String, String> createEvent(String eventType,
                                           String modelHandle,
                                           Map<String, String> varBindIdValue,
                                           String username,
                                           String p_assword) throws Exception
    {
        if (StringUtils.isBlank(eventType) || StringUtils.isBlank(modelHandle))
        {
            throw new Exception("Create Event requires event type and model handle to be non-empty");
        }

        setupRestCaller("events/" + eventType + "/model/" + modelHandle, username, p_assword);

        Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
        expectedStatusCodes.add(new Integer(HttpStatus.SC_OK));

        String response = restCaller.postMethod(null, null, null, varBindIdValue, 
                                                "varbind", 
                                                ATTRIB_NAME_VALUE_PAIR_VALUE_PREFIX, 
                                                expectedStatusCodes);

        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("Create Event event type = " + eventType + ", model handle = " + modelHandle + 
                          " and var bind id value map = " + varBindIdValue + 
                          " returned [" + response + "]");
        }

        Map<String, String> createEventResult = parseCreateEventResponse(eventType, modelHandle, varBindIdValue, response);

        if (createEventResult == null || createEventResult.isEmpty())
        {
            throw new Exception("Create Event for event type = " + eventType + ", model handle = " + modelHandle + 
                                " and var bind id value map = " + varBindIdValue + " failed");
        }

        return createEventResult; 
    }
    
    public List<Map<String, String>> createEvents(String createEventsRequestXml,
                                                  String username,
                                                  String p_assword) throws Exception
    {
        if (StringUtils.isBlank(createEventsRequestXml))
        {
            throw new Exception("Create Events requires create events request xml");
        }

        setupRestCaller("events", username, p_assword);

        Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
        expectedStatusCodes.add(new Integer(HttpStatus.SC_OK));

        String response = restCaller.postMethod(null, null, createEventsRequestXml, expectedStatusCodes);

        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("Create Events create events request xml = [" + createEventsRequestXml + "] returned [" + response + "]");
        }

        return parseCreateEventsResponse(createEventsRequestXml, response);
    }
    
    public String createSubscription(String pullSubscriptionRequestXml,
                                     String username,
                                     String p_assword) throws Exception
    {
        if (StringUtils.isBlank(pullSubscriptionRequestXml))
        {
            throw new Exception("Create Subscription requires pull subscritption request xml");
        }

        setupRestCaller("subscription", username, p_assword);

        Map<String, String> reqHeaders = new HashMap<String, String>();
        reqHeaders.put(HttpHeaders.CONTENT_TYPE, "application/xml");
        
        Collection<Integer> expectedStatusCodes = new HashSet<Integer>();
        expectedStatusCodes.add(new Integer(HttpStatus.SC_OK));

        String response = restCaller.postMethod(null, reqHeaders, pullSubscriptionRequestXml, expectedStatusCodes);

        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("Create Subscription pull subscritption request xml = [" + pullSubscriptionRequestXml + "] returned [" + response + "]");
        }

        return parseCreateSubscriptionResponse(pullSubscriptionRequestXml, response);
    }
    
    // Private 
    
    @SuppressWarnings("unchecked")
    private List<Map<String, String>> parseChunkedResponse(String objectType, String response, String urlQuery, String xmlQuery) throws Exception
    {
        Map<String, Object> responseJSON = new HashMap<String, Object>();
        
        StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);
        
        if (responseJSON.isEmpty())
        {
            throw new Exception("Get " + objectType + (StringUtils.isNotBlank(urlQuery) ? ", for urlQuery " + urlQuery : "") +
                                (StringUtils.isNotBlank(xmlQuery) ? ", for xmlQuery " + xmlQuery : "") +" did not return JSON.");
        }

        List<Map<String, String>>  returnList = new ArrayList<Map<String, String>>();
        
        Map<String, Object> dataNHrefMap = parseObjects(objectType, responseJSON);
        
        while (!dataNHrefMap.isEmpty() && !(dataNHrefMap.containsKey("error") && dataNHrefMap.get("error").toString().equalsIgnoreCase("EndOfResult")) && dataNHrefMap.containsKey("@href"))
        {
            if (dataNHrefMap.containsKey("data"))
            {
                List<Map<String, String>> chunkData = (List<Map<String, String>>)dataNHrefMap.get("data");
                returnList.addAll(chunkData);
            }
            
            String chunkResponse = restCaller.getMethod(dataNHrefMap.get("@href").toString(), null);
            
            if (Log.log.isDebugEnabled())
            {
                Log.log.debug("Get " + dataNHrefMap.get("@href").toString() + " returned [" + chunkResponse + "]");
            }
            
            Map<String, Object> chunkResponseJSON = new HashMap<String, Object>();
            
            StringUtils.jsonObjectStringToMap(chunkResponse, "chunkResponseJSON", chunkResponseJSON);
            
            dataNHrefMap.clear();
            
            dataNHrefMap = parseObjects(objectType, chunkResponseJSON);
        }
        
        if (dataNHrefMap.containsKey("data"))
        {
            List<Map<String, String>> chunkData = (List<Map<String, String>>)dataNHrefMap.get("data");
            returnList.addAll(chunkData);
        }
        
        return returnList;
    }
    
    @SuppressWarnings("unchecked")
    private Map<String, Object> parseObjects(String objectType, Map<String, Object> responseJSON)
    {
        Map<String, Object> dataNHRef = new HashMap<String, Object>();
        List<Map<String, String>> objects = Collections.emptyList();
        
        String responseListKey = null;
        String totalKey = null;
        String objectResponsesKey = null;
        String objectKey = null; 
        String objectIdKey = null;
        
        switch (objectType)
        {
            case "alarms":
                responseListKey = "ns1.alarm-response-list";
                totalKey = "@total-alarms";
                objectResponsesKey = "ns1.alarm-responses";
                objectKey = "ns1.alarm";
                objectIdKey = "@id";
                break;
                
            case "models":
            case "devices":
                responseListKey = "ns1.model-response-list";
                totalKey = "@total-models";
                objectResponsesKey = "ns1.model-responses";
                objectKey = "ns1.model";
                objectIdKey = "@mh";
                break;
        }
        
        if (StringUtils.isBlank(responseListKey))
            return dataNHRef;
        
        Object responseListObj = responseJSON.get(responseListKey);
        
        if (responseListObj == null || !(responseListObj instanceof Map) || StringUtils.isBlank(totalKey))
            return dataNHRef;
        
        Map<String, Object> responseListMap =  (Map<String, Object>)responseListObj;
        
        if (responseListMap.containsKey("@error"))
        {
            dataNHRef.put("@error", responseListMap.get("@error").toString());
        }
        
        if (!responseListMap.containsKey(totalKey) || !responseListMap.containsKey(objectResponsesKey))
            return dataNHRef;
        
        Object objectResponsesObj = responseListMap.get(objectResponsesKey);
        
        if (objectResponsesObj == null || !(objectResponsesObj instanceof Map) || StringUtils.isBlank(objectKey))
            return dataNHRef;
        
        Map<String, Object> objectResponsesMap = (Map<String, Object>)objectResponsesObj;
        
        if (!objectResponsesMap.containsKey(objectKey))
            return dataNHRef;
        
        Map<String, Object> objectsMap = (Map<String, Object>)objectResponsesMap.get(objectKey);
        
        if (objectsMap == null || objectsMap.values().isEmpty())
            return dataNHRef;
        
        objects = new ArrayList<Map<String, String>>();

        dataNHRef.put("data", objects);
        
        int i = -1;
        
        Map<String, Object> tobjectsMap = objectsMap;
        
        if (objectsMap.containsKey(objectIdKey))
        {
            tobjectsMap = new HashMap<String, Object>(1);
            tobjectsMap.put(Integer.toString(0), objectsMap);
        }
        
        for (Object objectObj : tobjectsMap.values())
        {
            i++;
            if (!(objectObj instanceof Map))
                continue;
            
            Map<String, Object> objectMap = (Map<String, Object>)objectObj;
            
            if (!objectMap.containsKey(objectIdKey))
                continue;
            
            Map<String, String> objectNameValueMap = new HashMap<String, String>();
            objects.add(i, objectNameValueMap);
            
            objectNameValueMap.put(objectIdKey, objectMap.get(objectIdKey).toString());
            
            if (objectMap.containsKey("ns1.attribute"))
            {
                parseAttribute(objectMap, objectNameValueMap);
            }
                
            if (objectMap.containsKey("ns1.attribute-list"))
            {
                parseAttributeList(objectMap, objectNameValueMap);
            }
            
            if (objectMap.containsKey("@error"))
            {
                objectNameValueMap.put("@error", objectMap.get("@error").toString());
            }
            
            if (objectMap.containsKey("@error-message"))
            {
                objectNameValueMap.put("@error-message", objectMap.get("@error-message").toString());
            }
        }
        
        Object linkObj = responseListMap.get("ns1.link");
        
        if (linkObj == null || !(linkObj instanceof Map))
            return dataNHRef;
        
        Map<String, Object> linkMap = (Map<String, Object>)linkObj;
        
        if (!linkMap.containsKey("@href"))
            return dataNHRef;
        
        dataNHRef.put("@href", linkMap.get("@href").toString());
        
        return dataNHRef;
    }
    
    private List<Map<String, String>> parsePollResponse(String response, String subscriptionId) throws Exception
    {
        Map<String, Object> responseJSON = new HashMap<String, Object>();

        if (Log.log.isDebugEnabled())
        {
            Log.log.debug("First Poll Response for [" + subscriptionId + "]=[" + response + "]");
        }
        
        StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);
        
        if (responseJSON.isEmpty())
        {
            throw new Exception("Poll Subscription (" + subscriptionId + ") did not return JSON.");
        }

        List<Map<String, String>>  returnList = new ArrayList<Map<String, String>>();
        
        List<Map<String, String>> data = parseNotifications(responseJSON);
        
        Log.log.debug("First Poll Response returned " + data.size() + " objects");
        
        while (!data.isEmpty())
        {
            returnList.addAll(data);
            
            String nextResponse = restCaller.getMethod(subscriptionId, (Map<String, String>)null, (Map<String, String>)null);
            
            if (Log.log.isDebugEnabled())
            {
                Log.log.debug("Next Poll of Subscription (" + subscriptionId + ") returned [" + nextResponse + "]");
            }
            
            Map<String, Object> nextResponseJSON = new HashMap<String, Object>();
            
            StringUtils.jsonObjectStringToMap(nextResponse, "nextResponseJSON", nextResponseJSON);
            
            data.clear();
            
            data = parseNotifications(nextResponseJSON);
        }
        return returnList;
    }
    
    @SuppressWarnings("unchecked")
    private List<Map<String, String>> parseNotifications(Map<String, Object> responseJSON)
    {
        List<Map<String, String>> data = Collections.emptyList();;
        
        if (!responseJSON.containsKey("ns1.notification-list"))
            return data;
        
        Object notfL1VObj = responseJSON.get("ns1.notification-list");
        
        if (notfL1VObj == null || !(notfL1VObj instanceof Map))
            return data;
        
        Map<String, Object> notfL1VMap = (Map<String, Object>)notfL1VObj;
        
        if (!notfL1VMap.containsKey("ns1.added-instance") && !notfL1VMap.containsKey("ns1.removed-instance") &&
            !notfL1VMap.containsKey("ns1.modified-instance"))
        {
            return data;
        }
        
        data = new ArrayList<Map<String, String>>();
        
        Map<String, Object> notfL2VMap = notfL1VMap;
        
        for (String notfL2VMapKey : notfL2VMap.keySet())
        {
            String operKey = notfL2VMapKey;
            
            if (operKey.equals("ns1.modified-instance")) // Currently completely ignore modified instances
                continue;
            
            Object notfL3VObj = notfL2VMap.get(operKey);
            
            if (notfL3VObj == null || !(notfL3VObj instanceof Map))
                continue;
            
            Map<String, Object> notfL3VMap = (Map<String, Object>)notfL3VObj;
            
            if (notfL3VMap.containsKey("ns1.alarm") || notfL3VMap.containsKey("ns1.model"))
            {
                notfL3VMap = new HashMap<String, Object>(1);
                notfL3VMap.put(Integer.toString(0), (Map<String, Object>)notfL3VObj);
            }
            
            for (Object notfL4VObj : notfL3VMap.values())
            {
                if (notfL4VObj == null || !(notfL4VObj instanceof Map))
                    continue;
                
                Map<String, Object> notfL4VMap = (Map<String, Object>)notfL4VObj;
                
                if (!notfL4VMap.containsKey("ns1.alarm") && !notfL4VMap.containsKey("ns1.model"))
                    continue;
                
                for (String notfL4VMapKey : notfL4VMap.keySet())
                {
                    if (!notfL4VMapKey.equals("ns1.alarm") && !notfL4VMapKey.equals("ns1.model"))
                        continue;
                    
                    String objTypeKey = notfL4VMapKey;
                    
                    Object notfL5VObj = notfL4VMap.get(objTypeKey);
                    
                    if (notfL5VObj == null || !(notfL5VObj instanceof Map))
                        continue;
                    
                    Map<String, Object> notfL5VMap = (Map<String, Object>)notfL5VObj;
                    
                    if (!notfL5VMap.containsKey("@id") && !notfL5VMap.containsKey("@mh"))
                        continue;
                    
                    Map<String, String> objectNameValueMap = new HashMap<String, String>();
                    
                    data.add(objectNameValueMap);
                    
                    objectNameValueMap.put(operKey, "true");
                    
                    if (operKey.equals("ns1.added-instance") && notfL4VMap.containsKey("@preexisting"))
                    {
                        objectNameValueMap.put("@preexisting", notfL4VMap.get("@preexisting").toString());
                    }
                    
                    if (operKey.equals("ns1.removed-instance") && notfL4VMap.containsKey("@deleted"))
                    {
                        objectNameValueMap.put("@deleted", notfL4VMap.get("@deleted").toString());
                    }
                    
                    objectNameValueMap.put(objTypeKey, "true");
                    
                    String idKey = notfL5VMap.containsKey("@id") ? "@id" : "@mh";
                    
                    objectNameValueMap.put(idKey, notfL5VMap.get(idKey).toString());
                    
                    String objTypeAttrPrefix = objectNameValueMap.get(idKey);
                    
                    if (notfL5VMap.containsKey("@error"))
                    {
                        objectNameValueMap.put(objTypeAttrPrefix + "@error", notfL5VMap.get("error").toString());
                    }
                    
                    if (notfL5VMap.containsKey("@error-message"))
                    {
                        objectNameValueMap.put(objTypeAttrPrefix + "@error-message", notfL5VMap.get("error-message").toString());
                    }
                    
                    if (notfL5VMap.containsKey("ns1.attribute"))
                    {
                        parseAttribute(notfL5VMap, objectNameValueMap);
                    }
                    
                    if (notfL5VMap.containsKey("ns1.atttribute-list"))
                    {
                        parseAttributeList(notfL5VMap, objectNameValueMap);
                    }
                    
                    if (operKey.equals("ns1.modified-instance") && notfL4VMap.containsKey("ns1.attribute-change-value"))
                    {
                        Object notfL11VObj = notfL4VMap.get("ns1.attribute-change-value");
                        
                        if (notfL11VObj == null || !(notfL11VObj instanceof Map)) 
                            continue;
                        
                        Map<String, Object> notfL11VMap = (Map<String, Object>)notfL11VObj;
                        
                        if (notfL11VMap.containsKey("@id"))
                        {
                            objectNameValueMap.put("ns1.attribute-change-value" + "@id", notfL11VMap.get("@id").toString());
                        }
                        
                        if (notfL11VMap.containsKey("ns1.attribute"))
                        {
                            objectNameValueMap.put("ns1.attribute-change-value" + "." + "ns1.attribute", notfL11VMap.get("ns1.attribute").toString());
                        }
                        
                        if (notfL11VMap.containsKey("ns1.old-value"))
                        {
                            objectNameValueMap.put("ns1.attribute-change-value" + "." + "ns1.old-value", notfL11VMap.get("ns1.old-value").toString());
                        }
                        
                        if (notfL11VMap.containsKey("ns1.new-value"))
                        {
                            objectNameValueMap.put("ns1.attribute-change-value" + "." + "ns1.new-value", notfL11VMap.get("ns1.new-value").toString());
                        }
                    }
                }
            }
        }            
        
        return data;
    }
    
    @SuppressWarnings("unchecked")
    private String parseSubscriptionResponse(Map<String, Object> responseJSON)
    {
        String subscriptionId = null;
        
        Object notfL1VObj = responseJSON.get("ns1.subscription-response");
        
        if (notfL1VObj == null || !(notfL1VObj instanceof Map))
            return subscriptionId;
        
        Map<String, Object> notfL1VMap = (Map<String, Object>)notfL1VObj;
        
        if (notfL1VMap.containsKey("ns1.subscription-id") && StringUtils.isNotBlank(notfL1VMap.get("ns1.subscription-id").toString()))
        {
            subscriptionId = notfL1VMap.get("ns1.subscription-id").toString();
        }
        
        return subscriptionId;
    }
    
    @SuppressWarnings("unchecked")
    private void parseAttribute(Map<String, Object> parentMap, Map<String, String> objectNameValueMap)
    {
        Object childL1VObj = parentMap.get("ns1.attribute");
        
        if (childL1VObj == null || !(childL1VObj instanceof Map))
            return;
        
        Map<String, Object> childL1VMap = (Map<String, Object>)childL1VObj;
        
        if (childL1VMap.containsKey("@id"))
        {
            childL1VMap = new HashMap<String, Object>(1);
            childL1VMap.put(Integer.toString(0), (Map<String, Object>)childL1VObj);
        }
        
        for (Object childL2VObj : childL1VMap.values())
        {
            Map<String, Object> childL2VMap = (Map<String, Object>)childL2VObj;
                                   
            if (childL2VMap.containsKey("@id"))
            {
                String attribValue = childL2VMap.containsKey("$") ? childL2VMap.get("$").toString() : "";
                objectNameValueMap.put(childL2VMap.get("@id").toString(), attribValue);
            }
            
            String attribPrefix = StringUtils.isNotBlank(objectNameValueMap.get(childL2VMap.get("@id").toString())) ? 
                                  objectNameValueMap.get(childL2VMap.get("@id").toString()) : childL2VMap.get("@id").toString();
            
            if (childL2VMap.containsKey("@error"))
            {
                objectNameValueMap.put(attribPrefix + "@error", childL2VMap.get("@error").toString());
            }
            
            if (childL2VMap.containsKey("@error-message"))
            {
                objectNameValueMap.put(attribPrefix + "@error-message", childL2VMap.get("@error-message").toString());
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    private void parseAttributeList(Map<String, Object> parentMap, Map<String, String> objectNameValueMap)
    {
        Object childL1VObj = parentMap.get("ns1.attribute-list");
        
        if (childL1VObj == null || !(childL1VObj instanceof Map))
            return;
        
        Map<String, Object> childL1VMap = (Map<String, Object>)childL1VObj;
        
        if (childL1VMap.containsKey("@id"))
        {
            childL1VMap = new HashMap<String, Object>(1);
            childL1VMap.put(Integer.toString(0), (Map<String, Object>)childL1VObj);
        }
        
        for (Object childL2VObj : childL1VMap.values())
        {
            Map<String, Object> childL2VMap = (Map<String, Object>)childL2VObj;
                                   
            if (!childL2VMap.containsKey("@id"))
                continue;
            
            String attribValue = childL2VMap.containsKey("$") ? childL2VMap.get("$").toString() : "";
            
            if (StringUtils.isNotBlank(attribValue))
            {
                objectNameValueMap.put(childL2VMap.get("@id").toString(), attribValue);
            }
            
            String attribPrefix = StringUtils.isNotBlank(objectNameValueMap.get(childL2VMap.get("@id").toString())) ? 
                                  objectNameValueMap.get(childL2VMap.get("@id").toString()) : childL2VMap.get("@id").toString();
            
            if (childL2VMap.containsKey("@type"))
            {
                objectNameValueMap.put(attribPrefix + "@type", childL2VMap.get("@type").toString());
            }
            
            if (childL2VMap.containsKey("@error"))
            {
                objectNameValueMap.put(attribPrefix + "@error", childL2VMap.get("@error").toString());
            }
            
            if (childL2VMap.containsKey("ns1.instance"))
            {
                Object childL3VObj = childL2VMap.get("ns1.instance");
                
                if (childL3VObj == null || !(childL3VObj instanceof Map))
                    continue;
                
                Map<String, Object> childL3VMap = (Map<String, Object>)childL3VObj;
                
                if (childL3VMap.containsKey("@value"))
                {
                    childL3VMap = new HashMap<String, Object>(1);
                    childL3VMap.put(Integer.toString(0), (Map<String, Object>)childL3VObj);
                }
                
                for (String key : childL3VMap.keySet())
                {
                    Object childL4VObj = childL3VMap.get(key);
                    
                    if (childL4VObj == null || !(childL4VObj instanceof Map))
                        continue;
                    
                    Map<String, Object> childL4VMap = (Map<String, Object>)childL4VObj;
                    
                    if (childL4VMap.containsKey("@value"))
                    {
                        if (childL4VMap.containsKey("@oid"))
                        {
                            objectNameValueMap.put(attribPrefix + "@" + childL4VMap.get("@oid").toString() + "-" + Integer.toString(Integer.parseInt(key) + 1), childL4VMap.get("@value").toString());
                        }
                        else
                        {
                            objectNameValueMap.put(attribPrefix + "@-" + Integer.toString(Integer.parseInt(key) + 1), childL4VMap.get("@value").toString());
                        }
                    }
                }
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    private Map<String, String> parseActionResponse(String objectType, String response, String actionCode, String modelHandle, Map<String, String> attribValMap) throws Exception
    {
        Map<String, Object> responseJSON = new HashMap<String, Object>();
        
        StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);
        
        if (responseJSON.isEmpty())
        {
            throw new Exception("Get action, action_code = [" + actionCode + "]" + (StringUtils.isNotBlank(modelHandle) ? ", mh = [" + modelHandle + "]" : "") + "]" + 
                            (attribValMap != null && !attribValMap.isEmpty()? ", Attribute Value Map = [" + attribValMap + "]" : "") + " did not return JSON");
        }

        Map<String, String>  returnMap = new HashMap<String, String>();
        
        Object actionRespVObj = responseJSON.get("ns1.action-response");
        
        if (actionRespVObj == null || !(actionRespVObj instanceof Map))
            return returnMap;
        
        Map<String, Object> actionRespVMap = (Map<String, Object>)actionRespVObj;
        
        if (!actionRespVMap.containsKey("@error"))
            return returnMap;
        
        returnMap.put("@error", actionRespVMap.get("@error").toString());
        
        if (actionRespVMap.containsKey("@error-message"))
            returnMap.put("@error-message", actionRespVMap.get("@error-message").toString());
        
        if (!actionRespVMap.containsKey("ns1.attribute"))
            return returnMap;
        
        Object attribVObj = actionRespVMap.get("ns1.attribute");
        
        if (attribVObj == null || !(attribVObj instanceof Map))
            return returnMap;
        
        Map<String, Object> attribVMap = (Map<String, Object>)attribVObj;
        
        parseAttribute(attribVMap, returnMap);
        
        return returnMap;
    }
    
    @SuppressWarnings("unchecked")
    private List<Map<String, String>> parseUpdateResponseList(String objectType, String objectId, Map<String, String> attrVal, String response) throws Exception
    {
        Map<String, Object> responseJSON = new HashMap<String, Object>();
        
        StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);
        
        if (responseJSON.isEmpty())
        {
            throw new Exception("Update " + objectType + ", Id = " + objectId + 
                                (attrVal != null && !attrVal.isEmpty()? " and Attribute Value Map = [" + attrVal + "]" : "") + " did not return JSON");
        }

        List<Map<String, String>> returnList = new ArrayList<Map<String, String>>();
        
        String responseListKey = null;
        String objectResponsesKey = null;
        String objectKey = null; 
        String objectIdKey = null;
        
        switch (objectType)
        {
            case "alarms":
                responseListKey = "ns1.alarm-update-response-list";
                objectResponsesKey = "ns1.alarm-responses";
                objectKey = "ns1.alarm";
                objectIdKey = "@id";
                break;
                
            case "model":
            case "models":
                responseListKey = "ns1.model-update-response-list";
                objectResponsesKey = "ns1.model-responses";
                objectKey = "ns1.model";
                objectIdKey = "@mh";
                break;
        }
        
        if (StringUtils.isBlank(responseListKey))
            return returnList;
        
        Object responseListObj = responseJSON.get(responseListKey);
        
        if (responseListObj == null || !(responseListObj instanceof Map) || StringUtils.isBlank(objectResponsesKey))
            return returnList;
        
        Map<String, Object> responseListMap =  (Map<String, Object>)responseListObj;
        
        Object objectResponsesObj = responseListMap.get(objectResponsesKey);
        
        if (objectResponsesObj == null || !(objectResponsesObj instanceof Map) || StringUtils.isBlank(objectKey))
            return returnList;
        
        Map<String, Object> objectResponsesMap = (Map<String, Object>)objectResponsesObj;
        
        Object responseObjectObj = objectResponsesMap.get(objectKey);
        
        if (responseObjectObj == null || !(responseObjectObj instanceof Map) || StringUtils.isBlank(objectIdKey))
            return returnList;
        
        Map<String, Object> responseObjectMap = (Map<String, Object>)responseObjectObj;
        
        if (responseObjectMap.containsKey(objectIdKey))
        {
            responseObjectMap = new HashMap<String, Object>();
            responseObjectMap.put(Integer.toString(0), (Map<String, Object>)responseObjectObj);
        }
        
        HashMap<String, Map<String, String>> idToObjectMap = new HashMap<String, Map<String, String>>();
        
        for (Object childResponseObjectObj : responseObjectMap.values())
        {
            Map<String, Object> childResponseObjectMap = (Map<String, Object>)childResponseObjectObj;
            
            if (!childResponseObjectMap.containsKey(objectIdKey))
                continue;
            
            
            Map<String, String> objectNameValueMap = idToObjectMap.get(childResponseObjectMap.get(objectIdKey).toString());
            
            if (objectNameValueMap == null)
            {
                objectNameValueMap = new HashMap<String, String>();
                returnList.add(objectNameValueMap);
                idToObjectMap.put(childResponseObjectMap.get(objectIdKey).toString(), objectNameValueMap);
            }
                            
            objectNameValueMap.put(objectIdKey, childResponseObjectMap.get(objectIdKey).toString());
            
            if (childResponseObjectMap.containsKey("ns1.attribute"))
            {
                parseAttribute(childResponseObjectMap, objectNameValueMap);
            }
                
            if (childResponseObjectMap.containsKey("ns1.attribute-list"))
            {
                parseAttributeList(childResponseObjectMap, objectNameValueMap);
            }
            
            if (childResponseObjectMap.containsKey("@error"))
            {
                objectNameValueMap.put("@error", childResponseObjectMap.get("@error").toString());
            }
            
            if (childResponseObjectMap.containsKey("@error-message"))
            {
                objectNameValueMap.put("@error-message", childResponseObjectMap.get("@error-message").toString());
            }
        }
        
        return returnList;
    }
    
    @SuppressWarnings("unchecked")
    private Map<String, String> parseDeleteResponse(String objectType, String objectId, String response) throws Exception
    {
        Map<String, Object> responseJSON = new HashMap<String, Object>();
        
        StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);
        
        Map<String, String>  returnMap = new HashMap<String, String>();
        
        if (objectType.equalsIgnoreCase("subscription"))
            return returnMap;
        
        if (responseJSON.isEmpty())
        {
            throw new Exception("Delete " + objectType + ", " + objectType + " Id = [" + objectId + "] did not return JSON");
        }

        String deleteResponseKey = objectType.equalsIgnoreCase("alarms") ? "ns1.delete-alarm-response" : "ns1.delete-model-response";
        
        Object deleteRespVObj = responseJSON.get(deleteResponseKey);
        
        if (deleteRespVObj == null || !(deleteRespVObj instanceof Map))
            return returnMap;
        
        Map<String, Object> deleteRespVMap = (Map<String, Object>)deleteRespVObj;
        
        if (!deleteRespVMap.containsKey("ns1.error"))
            return returnMap;
        
        returnMap.put("ns1.error", deleteRespVMap.get("ns1.error").toString());
        
        if (deleteRespVMap.containsKey("ns1.error-message"))
            returnMap.put("ns1.error-message", deleteRespVMap.get("ns1.error-message").toString());
                
        return returnMap;
    }
    
    private Map<String, String> parseCreateAssociationResponse(String relationHandle, String leftModelHandle, String rightModelHandle, String response) throws Exception
    {
        Map<String, Object> responseJSON = new HashMap<String, Object>();
        
        StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);
        
        if (responseJSON.isEmpty())
        {
            throw new Exception("Create asociation, relation handle = [" + relationHandle + "], left model handle = [" + leftModelHandle + 
                                "], right model handle = [" + rightModelHandle + " did not return JSON");
        }
        
        List<Map<String, String>> associationObjects = parseAssociationResponse(responseJSON);
        
        if (associationObjects == null || associationObjects.isEmpty() || associationObjects.size() != 1)
        {
            throw new Exception("Create asociation, relation handle = [" + relationHandle + "], left model handle = [" + leftModelHandle + 
                                "], right model handle = [" + rightModelHandle + 
                                "] did not create any associations or created more than expected number of associations.");
        }
        
        return associationObjects.get(0); 
    }
    
    private List<Map<String, String>> parseGetAssociationsResponse(String relationHandle, String modelHandle, String side, String response) throws Exception
    {
        Map<String, Object> responseJSON = new HashMap<String, Object>();
        
        StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);
        
        if (responseJSON.isEmpty())
        {
            throw new Exception("Get asociations, relation handle = [" + relationHandle + "], model handle = [" + modelHandle + 
                                "], side = [" + side + " did not return JSON");
        }
        
        return parseAssociationResponse(responseJSON);
    }
    
    private Map<String, String> parseDeleteAssociationResponse(String relationHandle, String leftModelHandle, String rightModelHandle, String response) throws Exception
    {
        Map<String, Object> responseJSON = new HashMap<String, Object>();
        
        StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);
        
        if (responseJSON.isEmpty())
        {
            throw new Exception("Delete asociation, relation handle = [" + relationHandle + "], left model handle = [" + leftModelHandle + 
                                "], right model handle = [" + rightModelHandle + " did not return JSON");
        }
        
        List<Map<String, String>> associationObjects = parseAssociationResponse(responseJSON);
        
        if (associationObjects == null || associationObjects.isEmpty() || associationObjects.size() != 1)
        {
            throw new Exception("Delete asociation, relation handle = [" + relationHandle + "], left model handle = [" + leftModelHandle + 
                                "], right model handle = [" + rightModelHandle + 
                                "] did not delete any associations or deleted more than expected number of associations.");
        }
        
        return associationObjects.get(0); 
    }
    
    @SuppressWarnings("unchecked")
    private List<Map<String, String>> parseAssociationResponse(Map<String, Object> responseJSON) throws Exception
    {
        List<Map<String, String>> returnList = new ArrayList<Map<String, String>>();
        
        Set<String> associationRespKeys = responseJSON.keySet();
        
        if (associationRespKeys == null || associationRespKeys.isEmpty() || associationRespKeys.size() != 1)
        {
            throw new Exception("Association Resposne JSON object does not contain any keys or has more than one key");
        }
        
        String associationRespKey = associationRespKeys.iterator().next();
        
        if (!associationRespKey.equalsIgnoreCase("ns1.create-association-response") &&
            !associationRespKey.equalsIgnoreCase("ns1.delete-association-response") &&
            !associationRespKey.equalsIgnoreCase("ns1.association-response-list"))
        {
            throw new Exception("Association Resposne key is not one of the expected " +
                                "ns1.create-assocation-response, ns1.delete-association-response " +
                                "and ns1.association-response-list key");
        }
        
        Object associationRespVObj = responseJSON.get(associationRespKey);
        
        if (associationRespVObj == null || !(associationRespVObj instanceof Map))
            return returnList;
        
        Map<String, Object> associationRespVMap = (Map<String, Object>)associationRespVObj;
        
        Map<String, Object> associationVMap = null;
        Map<String, String> errorDataMap = new HashMap<String, String>();
        
        if (associationRespKey.equalsIgnoreCase("ns1.association-response-list"))
        {
            if (!associationRespVMap.containsKey("ns1.association-responses"))
                return returnList;
            
            Object associationResponsesVObj = associationRespVMap.get("ns1.association-responses");
            
            if (associationResponsesVObj == null || !(associationResponsesVObj instanceof Map))
                return returnList;
            
            Map<String, Object> associationResponsesVMap = (Map<String, Object>)associationResponsesVObj;
            
            if (!associationResponsesVMap.containsKey("ns1.association"))
                return returnList;
            
            Object associationVObj = associationResponsesVMap.get("ns1.association");
            
            if (associationVObj == null || !(associationVObj instanceof Map))
                return returnList;
            
            associationVMap = (Map<String, Object>)associationVObj;
        }
        else
        {
            String errorPrefix = associationRespKey.equalsIgnoreCase("ns1.create-association-response") ? "@" : "ns1.";
            
            if (!associationRespVMap.containsKey(errorPrefix + "error"))
                return returnList;
            
            errorDataMap.put(errorPrefix + "error", associationRespVMap.get(errorPrefix + "error").toString());
            
            if (associationRespVMap.containsKey(errorPrefix + "error-message"))
                errorDataMap.put(errorPrefix + "error-message", associationRespVMap.get(errorPrefix + "error-message").toString());
            
            if (associationRespVMap.containsKey("ns1.association"))
            {
                Object associationVObj = associationRespVMap.get("ns1.association");
                
                if (associationVObj == null || !(associationVObj instanceof Map))
                    return returnList;
                
                associationVMap = (Map<String, Object>)associationVObj;
            }
        }
        
        if (associationVMap != null && !associationVMap.isEmpty())
        {
            Map<String, Object> tAssociationVMap = associationVMap;
            
            if (tAssociationVMap.containsKey("@rh") && tAssociationVMap.containsKey("@leftmh") &&
                tAssociationVMap.containsKey("@rightmh"))
            {
                tAssociationVMap = new HashMap<String, Object>(1);
                tAssociationVMap.put(Integer.toString(0), associationVMap);
            }
            
            for (Object assocVObj : tAssociationVMap.values())
            {
                if (assocVObj == null || !(assocVObj instanceof Map))
                    continue;
                
                Map<String, Object> assocVMap = (Map<String, Object>)assocVObj;
                
                if (!assocVMap.containsKey("@rh") || !assocVMap.containsKey("@leftmh") &&
                    !assocVMap.containsKey("rightmh"))
                {
                    continue;
                }
                
                Map<String, String> objectNameValueMap = new HashMap<String, String>();
                returnList.add(objectNameValueMap);
                
                objectNameValueMap.put("@rh", assocVMap.get("@rh").toString());
                objectNameValueMap.put("@leftmh", assocVMap.get("@leftmh").toString());
                objectNameValueMap.put("@rightmh", assocVMap.get("@rightmh").toString());
            }
        }
        else
        {
            returnList.add(new HashMap<String, String>());
        }
        
        if (!errorDataMap.isEmpty())
        {
            for (Map<String, String> objectNameValueMap : returnList)
            {
                for (String errorDataKey : errorDataMap.keySet())
                {
                    objectNameValueMap.put(errorDataKey, errorDataMap.get(errorDataKey));
                }
            }
        }
        
        return returnList;
    }
    
    @SuppressWarnings("unchecked")
    private Map<String, String> parseGetAttributeEnumResponse(String attrId, String response) throws Exception
    {
        Map<String, Object> responseJSON = new HashMap<String, Object>();
        
        StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);
        
        if (responseJSON.isEmpty())
        {
            throw new Exception("Get attribute eumerations for attribute id = [" + attrId + 
                                "] did not return JSON");
        }

        Map<String, String>  returnMap = new HashMap<String, String>();
        
        Object attrEnumRespVObj = responseJSON.get("ns1.attrtibute-enum-response");
        
        if (attrEnumRespVObj == null || !(attrEnumRespVObj instanceof Map))
            return returnMap;
        
        Map<String, Object> attrEnumRespVMap = (Map<String, Object>)attrEnumRespVObj;
        
        if (!attrEnumRespVMap.containsKey("@error") && !attrEnumRespVMap.containsKey("ns1.attribute"))
            return returnMap;
        
        if (attrEnumRespVMap.containsKey("@error"))
            returnMap.put("@error", attrEnumRespVMap.get("@error").toString());
        
        if (attrEnumRespVMap.containsKey("@error-message"))
            returnMap.put("@error-message", attrEnumRespVMap.get("@error-message").toString());
        
        if (!attrEnumRespVMap.containsKey("ns1.attribute"))
            return returnMap;
        
        Object attribVObj = attrEnumRespVMap.get("ns1.attribute");
        
        if (attribVObj == null || !(attribVObj instanceof Map))
            return returnMap;
        
        parseAttribute(attrEnumRespVMap, returnMap);
        
        if (!attrEnumRespVMap.containsKey("ns1.enum"))
            return returnMap;
        
        Object enumVObj = attrEnumRespVMap.get("ns1.enum");
        
        if (enumVObj == null || !(enumVObj instanceof Map))
            return returnMap;
        
        Map<String, Object> enumVMap = (Map<String, Object>)enumVObj;
        
        Map<String, Object> tEnumVMap = enumVMap;
        
        if (tEnumVMap.containsKey("ns1.id") && tEnumVMap.containsKey("ns1.value"))
        {
            tEnumVMap = new HashMap<String, Object>(1);
            tEnumVMap.put(Integer.toString(0), enumVMap);
        }
        
        for (Object enumValObj : tEnumVMap.values())
        {
            if (enumValObj == null || !(enumValObj instanceof Map))
                continue;
            
            Map<String, Object> enumValMap = (Map<String, Object>)enumValObj;
            
            if (!enumValMap.containsKey("@id") && !enumValMap.containsKey("@value"))
                continue;
            
            returnMap.put(enumValMap.get("@id").toString(), enumValMap.get("@value").toString());
        }
        
        return returnMap;
    }
    
    @SuppressWarnings("unchecked")
    private Map<String, Map<String, List<Map<String, String>>>> parseGetConnectivityResponse(String ipAddress, String response) throws Exception
    {
        Map<String, Object> responseJSON = new HashMap<String, Object>();
        
        StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);
        
        if (responseJSON.isEmpty())
        {
            throw new Exception("Get connectivity, ipAddress = [" + ipAddress + "] did not return JSON");
        }
        
        Map<String, Map<String, List<Map<String, String>>>> returnMap = new HashMap<String, Map<String, List<Map<String, String>>>>();
        
        if (!responseJSON.containsKey("ns1.connection-response-list"))
            return returnMap;
        
        Object connRespListVObj = responseJSON.get("ns1.connection-response-list");
        
        if (connRespListVObj == null || !(connRespListVObj instanceof Map))
            return returnMap;
        
        Map<String, Object> connRespListMap = (Map<String, Object>)connRespListVObj;
        
        if (!connRespListMap.containsKey("ns1.connection-response"))
            return returnMap;
        
        Object connRespVObj = connRespListMap.get("ns1.connection-response");
        
        if (connRespVObj == null || !(connRespVObj instanceof Map))
            return returnMap;
        
        Map<String, Object> connRespVMap = (Map<String, Object>)connRespVObj;
        
        if (connRespVMap.containsKey("ns1.connection-element-left") ||
            connRespVMap.containsKey("ns1.connection-element-right"))
        {
            connRespVMap = new HashMap<String, Object>(1);
            connRespVMap.put(Integer.toString(0), connRespVObj);
        }
        
        for (String key : connRespVMap.keySet() /*Object connElmLOrRVObj : connRespVMap.values()*/)
        {
            Map<String, List<Map<String, String>>> l1Map = returnMap.get(key);
            
            if (l1Map == null)
            {
                l1Map = new HashMap<String, List<Map<String, String>>>();
                returnMap.put(key, l1Map);
            }
            
            Object connElmLOrRVObj = connRespVMap.get(key);
            
            if (connElmLOrRVObj == null || !(connElmLOrRVObj instanceof Map))
                continue;
            
            Map<String, Object> connElmLOrRVMap = (Map<String, Object>)connElmLOrRVObj;
            
            if (!connElmLOrRVMap.containsKey("ns1.connection-element-left") &&
                !connElmLOrRVMap.containsKey("ns1.connection-element-right"))
            {
                continue;
            }
            
            for (String connElmLOrRKey : connElmLOrRVMap.keySet())
            {
                List<Map<String, String>> l2List = l1Map.get(connElmLOrRKey);
                
                if (l2List == null)
                {
                    l2List = new ArrayList<Map<String, String>>();
                    l1Map.put(connElmLOrRKey, l2List);
                }
                
                Object childConnElmLOrRVObj = connElmLOrRVMap.get(connElmLOrRKey);
                
                if (childConnElmLOrRVObj == null || !(childConnElmLOrRVObj instanceof Map))
                    continue;
                
                Map<String, Object> childConnElmLOrRVMap = (Map<String, Object>)childConnElmLOrRVObj;
                
                if (childConnElmLOrRVMap.containsKey("ns1.ipaddress") ||
                    childConnElmLOrRVMap.containsKey("ns1.mh") ||
                    childConnElmLOrRVMap.containsKey("ns1.name") ||
                    childConnElmLOrRVMap.containsKey("ns1.type") ||
                    childConnElmLOrRVMap.containsKey("ns1.class"))
                {
                    childConnElmLOrRVMap = new HashMap<String, Object>(1);
                    childConnElmLOrRVMap.put(Integer.toString(0), childConnElmLOrRVObj);
                }
                
                for (Object childL1ConnElmLOrRVObj : childConnElmLOrRVMap.values())
                {
                    if (childL1ConnElmLOrRVObj == null || !(childL1ConnElmLOrRVObj instanceof Map))
                        continue;
                    
                    Map<String, Object> childL1ConnElmLOrRVMap = (Map<String, Object>)childL1ConnElmLOrRVObj;
                    
                    if (!childL1ConnElmLOrRVMap.containsKey("ns1.ipaddress") &&
                        !childL1ConnElmLOrRVMap.containsKey("ns1.mh") &&
                        !childL1ConnElmLOrRVMap.containsKey("ns1.name") &&
                        !childL1ConnElmLOrRVMap.containsKey("ns1.type") &&
                        !childL1ConnElmLOrRVMap.containsKey("ns1.class"))
                    {
                        continue;
                    }
                    
                    Map<String, String> objectNameValueMap = new HashMap<String, String>();
                    l2List.add(objectNameValueMap);
                    
                    if (childL1ConnElmLOrRVMap.containsKey("ns1.ipaddress"))
                    {
                        objectNameValueMap.put("ns1.ipaddress", childL1ConnElmLOrRVMap.get("ns1.ipaddress").toString());
                    }
                    
                    if (childL1ConnElmLOrRVMap.containsKey("ns1.mh"))
                    {
                        objectNameValueMap.put("ns1.mh", childL1ConnElmLOrRVMap.get("ns1.mh").toString());
                    }
                    
                    if (childL1ConnElmLOrRVMap.containsKey("ns1.name"))
                    {
                        objectNameValueMap.put("ns1.name", childL1ConnElmLOrRVMap.get("ns1.name").toString());
                    }
                    
                    if (childL1ConnElmLOrRVMap.containsKey("ns1.type"))
                    {
                        objectNameValueMap.put("ns1.type", childL1ConnElmLOrRVMap.get("ns1.type").toString());
                    }
                    
                    if (childL1ConnElmLOrRVMap.containsKey("ns1.class"))
                    {
                        objectNameValueMap.put("ns1.class", childL1ConnElmLOrRVMap.get("ns1.class").toString());
                    }
                }
            }
        }
        
        return returnMap;
    }
    
    @SuppressWarnings("unchecked")
    private List<Map<String, String>> parseGetLandscapesResponse(String response) throws Exception
    {
        Map<String, Object> responseJSON = new HashMap<String, Object>();
        
        StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);
        
        if (responseJSON.isEmpty())
        {
            throw new Exception("Get landscapes did not return JSON");
        }
        
        List<Map<String, String>> returnList = new ArrayList<Map<String, String>>();
        
        if (!responseJSON.containsKey("ns1.landscape-response"))
            return returnList;
        
        Object lndScpRespVObj = responseJSON.get("ns1.landscape-response");
        
        if (lndScpRespVObj == null || !(lndScpRespVObj instanceof Map))
            return returnList;
        
        Map<String, Object>lndScpRespVMap = (Map<String, Object>)lndScpRespVObj;
        
        if (!lndScpRespVMap.containsKey("@total-landscapes") && !lndScpRespVMap.containsKey("ns1.landscape"))
            return returnList;
        
        Object lndScpsVObj = lndScpRespVMap.get("ns1.landscape");
        
        if (lndScpsVObj == null || !(lndScpsVObj instanceof Map))
            return returnList;
        
        Map<String, Object> lndScpsVMap = (Map<String, Object>)lndScpsVObj;
        
        if (lndScpsVMap.containsKey("ns1.id") || lndScpsVMap.containsKey("ns1.name") ||
            lndScpsVMap.containsKey("ns1.isPrimary") || 
            lndScpsVMap.containsKey("ns1.spectrumVersion"))
        {
            lndScpsVMap = new HashMap<String, Object>(1);
            lndScpsVMap.put(Integer.toString(0), lndScpsVObj);
        }
        
        for (Object lndScpVObj : lndScpsVMap.values())
        {
            if (lndScpVObj == null || !(lndScpVObj instanceof Map))
                continue;
            
            Map<String, Object> lndScpVMap = (Map<String, Object>)lndScpVObj;
            
            if (!lndScpVMap.containsKey("ns1.id") && !lndScpsVMap.containsKey("ns1.name") &&
                !lndScpsVMap.containsKey("ns1.isPrimary") &&
                !lndScpsVMap.containsKey("ns1.spectrumVersion"))
            {
                continue;
            }
            
            Map<String, String> objectNameValueMap = new HashMap<String, String>();
            returnList.add(objectNameValueMap);
            
            if (lndScpVMap.containsKey("ns1.id"))
                objectNameValueMap.put("ns1.id", lndScpVMap.get("ns1.id").toString());
            
            if(lndScpVMap.containsKey("ns1.name"))
                objectNameValueMap.put("ns1.name", lndScpVMap.get("ns1.name").toString());
            
            if(lndScpVMap.containsKey("ns1.isPrimary"))
                objectNameValueMap.put("ns1.isPrimary", lndScpVMap.get("ns1.isPrimary").toString());
            
            if(lndScpVMap.containsKey("ns1.spectrumVersion"))
                objectNameValueMap.put("ns1.spectrumVersion", lndScpVMap.get("ns1.spectrumVersion").toString());
        }
        
        return returnList;
    }
    
    private Map<String, String> parseCreateModelResponse(Map<String, String> reqParams, Map<String, String> attribValMap, String response) throws Exception
    {
        Map<String, Object> responseJSON = new HashMap<String, Object>();
        
        StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);
        
        if (responseJSON.isEmpty())
        {
            throw new Exception("Create model, request parameters = [" + reqParams + "] and attribute value map = [" + attribValMap + 
                                "] did not return JSON");
        }
        
        List<Map<String, String>> modelObjects = parseModelResponse(responseJSON);
        
        if (modelObjects == null || modelObjects.isEmpty() || modelObjects.size() != 1)
        {
            throw new Exception("Create model, request parameters = [" + reqParams + "] and attribute value map = [" + attribValMap + 
                                "] did not create any model or created more than expected number of models.");
        }
        
        return modelObjects.get(0); 
    }
    
    private Map<String, String> parseGetModelResponse(String modelHandle, List<String> attributes, String response) throws Exception
    {
        Map<String, Object> responseJSON = new HashMap<String, Object>();
        
        StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);
        
        if (responseJSON.isEmpty())
        {
            throw new Exception("Get model, model handle = " + modelHandle + " and attributes = [" + attributes + 
                                "] did not return JSON");
        }
        
        List<Map<String, String>> modelObjects = parseModelResponse(responseJSON);
        
        if (modelObjects == null || modelObjects.isEmpty() || modelObjects.size() != 1)
        {
            throw new Exception("Get model, model handle = " + modelHandle + "] and attributes = [" + attributes + 
                                "] did not return any model or returned more than expected number of models.");
        }
        
        return modelObjects.get(0); 
    }
    
    private List<Map<String, String>> parseUpdateModelsResponse(String response, String xmlQuery) throws Exception
    {
        Map<String, Object> responseJSON = new HashMap<String, Object>();
        
        StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);
        
        if (responseJSON.isEmpty())
        {
            throw new Exception("Update models, xml query = [" + xmlQuery + "] did not return JSON");
        }
        
        return parseModelResponse(responseJSON);
    }
    
    @SuppressWarnings("unchecked")
    private List<Map<String, String>> parseModelResponse(Map<String, Object> responseJSON) throws Exception
    {
        List<Map<String, String>> returnList = new ArrayList<Map<String, String>>();
        
        Set<String> modelRespKeys = responseJSON.keySet();
        
        if (modelRespKeys == null || modelRespKeys.isEmpty() || modelRespKeys.size() != 1)
        {
            throw new Exception("Model Resposne JSON object does not contain any keys or has more than one key");
        }
        
        String modelRespKey = modelRespKeys.iterator().next();
        
        if (!modelRespKey.equalsIgnoreCase("ns1.create-model-response") &&
            !modelRespKey.equalsIgnoreCase("ns1.model-response-list") &&
            !modelRespKey.equalsIgnoreCase("ns1.model-update-response-list") &&
            !modelRespKey.equalsIgnoreCase("ns1.delete-model-response"))
        {
            throw new Exception("Model Resposne key is not one of the expected " +
                                "ns1.create-model-response, ns1.model-response-list, " +
                                "ns1.model-update-response-list and " +
                                "ns1.delete-model-response ");
        }
        
        Object modelRespVObj = responseJSON.get(modelRespKey);
        
        if (modelRespVObj == null || !(modelRespVObj instanceof Map))
            return returnList;
        
        Map<String, Object> modelRespVMap = (Map<String, Object>)modelRespVObj;
        
        Map<String, Object> modelVMap = null;
        Map<String, String> errorDataMap = new HashMap<String, String>();
        
        if (!modelRespKey.equalsIgnoreCase("ns1.delete-model-response"))
        {
            if (!modelRespVMap.containsKey("@error") &&
                !modelRespVMap.containsKey("ns1.model-responses"))
            {
                return returnList;
            }
            
            if (modelRespVMap.containsKey("@error"))
                errorDataMap.put("@error", modelRespVMap.get("@error").toString());
            
            if (modelRespVMap.containsKey("@error-message"))
                errorDataMap.put("@error-message", modelRespVMap.get("@error-message").toString());
            
            Object modelResponsesVObj = null;
            
            if (!modelRespKey.equalsIgnoreCase("ns1.create-model-response"))
            {
                if (!modelRespVMap.containsKey("ns1.model-responses"))
                    return returnList;
                
                modelResponsesVObj = modelRespVMap.get("ns1.model-responses");
            }
            else
            {
                Map<String, Object> tMap = new HashMap<String, Object>(1);
                tMap.put("ns1.model", modelRespVMap);
                modelResponsesVObj = tMap;
            }
            
            if (modelResponsesVObj == null || !(modelResponsesVObj instanceof Map))
                return returnList;
            
            Map<String, Object> modelResponsesVMap = (Map<String, Object>)modelResponsesVObj;
            
            if (!modelResponsesVMap.containsKey("ns1.model"))
                return returnList;
            
            Object modelVObj = modelResponsesVMap.get("ns1.model");
            
            if (modelVObj == null || !(modelVObj instanceof Map))
                return returnList;
            
            modelVMap = (Map<String, Object>)modelVObj;
        }
        else
        {
            if (!modelRespVMap.containsKey("ns1.error"))
                return returnList;
            
            errorDataMap.put("ns1.error", modelRespVMap.get("ns1.error").toString());
            
            if (modelRespVMap.containsKey("ns1.error-message"))
                errorDataMap.put("ns1.error-message", modelRespVMap.get("ns1.error-message").toString());            
        }
        
        if (modelVMap != null && !modelVMap.isEmpty())
        {
            Map<String, Object> tModelVMap = modelVMap;
            
            if (tModelVMap.containsKey("@mh"))
            {
                tModelVMap = new HashMap<String, Object>(1);
                tModelVMap.put(Integer.toString(0), modelVMap);
            }
            
            for (Object childModelVObj : tModelVMap.values())
            {
                if (childModelVObj == null || !(childModelVObj instanceof Map))
                    continue;
                
                Map<String, Object> childModelVMap = (Map<String, Object>)childModelVObj;
                
                if (!childModelVMap.containsKey("@mh"))
                {
                    continue;
                }
                
                Map<String, String> objectNameValueMap = new HashMap<String, String>();
                returnList.add(objectNameValueMap);
                
                objectNameValueMap.put("@mh", childModelVMap.get("@mh").toString());
                
                if (childModelVMap.containsKey("@error"))
                    objectNameValueMap.put("@error", childModelVMap.get("@error").toString());
                
                if (childModelVMap.containsKey("@error-message"))
                    objectNameValueMap.put("@error-message", childModelVMap.get("@error-message").toString());
                
                if (childModelVMap.containsKey("ns1.attribute"))
                    parseAttribute(childModelVMap, objectNameValueMap);
                
                if (childModelVMap.containsKey("ns1.attribute-list"))
                    parseAttributeList(childModelVMap, objectNameValueMap);
            }
        }
        else
        {
            returnList.add(new HashMap<String, String>());
        }
        
        if (!errorDataMap.isEmpty())
        {
            for (Map<String, String> objectNameValueMap : returnList)
            {
                for (String errorDataKey : errorDataMap.keySet())
                {
                    objectNameValueMap.put(errorDataKey, errorDataMap.get(errorDataKey));
                }
            }
        }
        
        return returnList;
    }
    
    private Map<String, String> parseCreateEventResponse(String eventType, String modelHandle, Map<String, String> varBindIdValue, String response) throws Exception
    {
        Map<String, Object> responseJSON = new HashMap<String, Object>();
        
        StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);
        
        if (responseJSON.isEmpty())
        {
            throw new Exception("Create event, event type = [" + eventType + "], model handle = [" + modelHandle + 
                                "] and var bind id value map = [" + varBindIdValue + "] did not return JSON");
        }
        
        List<Map<String, String>> evenObjects = parseEventResponse(responseJSON);
        
        if (evenObjects == null || evenObjects.isEmpty() || evenObjects.size() != 1)
        {
            throw new Exception("Create event, event type = [" + eventType + "], model handle = [" + modelHandle + 
                                "] and var bind id value map = [" + varBindIdValue + 
                                "] did not create any event or created more than expected number of events.");
        }
        
        return evenObjects.get(0); 
    }
    
    private List<Map<String, String>> parseCreateEventsResponse(String createEventsRequestXml, String response) throws Exception
    {
        Map<String, Object> responseJSON = new HashMap<String, Object>();
        
        StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);
        
        if (responseJSON.isEmpty())
        {
            throw new Exception("Create events, create events request xml = [" + createEventsRequestXml + "] did not return JSON");
        }
        
        return parseEventResponse(responseJSON);        
    }
    
    @SuppressWarnings("unchecked")
    private List<Map<String, String>> parseEventResponse(Map<String, Object> responseJSON) throws Exception
    {
        List<Map<String, String>> returnList = new ArrayList<Map<String, String>>();
        
        Set<String> eventRespKeys = responseJSON.keySet();
        
        if (eventRespKeys == null || eventRespKeys.isEmpty() || eventRespKeys.size() != 1)
        {
            throw new Exception("Event Resposne JSON object does not contain any keys or has more than one key");
        }
        
        String eventRespKey = eventRespKeys.iterator().next();
        
        if (!eventRespKey.equalsIgnoreCase("ns1.event-response") &&
            !eventRespKey.equalsIgnoreCase("ns1.event-response-list"))
        {
            throw new Exception("Even Resposne key is not one of the expected " +
                                "ns1.event-response, ns1.event-response-list");
        }
        
        Object eventRespVObj = responseJSON.get(eventRespKey);
        
        if (eventRespVObj == null || !(eventRespVObj instanceof Map))
            return returnList;
        
        Map<String, Object> eventRespVMap = (Map<String, Object>)eventRespVObj;
        
        if (!eventRespVMap.containsKey("@id") &&
            !eventRespVMap.containsKey("@error") &&
            !eventRespVMap.containsKey("ns1.event-response"))
        {
            return returnList;
        }
            
        Object eventResponsesVObj = null;
            
        if (eventRespKey.equalsIgnoreCase("ns1.event-response-list"))
        {
            if (!eventRespVMap.containsKey("ns1.event-response"))
                return returnList;
            
            eventResponsesVObj = eventRespVMap.get("ns1.event-response");
        }
        else
        {
            Map<String, Object> tMap = new HashMap<String, Object>(1);
            tMap.put("ns1.event-response", eventRespVMap);
            eventResponsesVObj = tMap;
        }
            
        if (eventResponsesVObj == null || !(eventResponsesVObj instanceof Map))
            return returnList;
        
        Map<String, Object> eventResponsesVMap = (Map<String, Object>)eventResponsesVObj;
        
        if (!eventResponsesVMap.containsKey("ns1.event-response"))
            return returnList;
        
        Object eventVObj = eventResponsesVMap.get("ns1.event-response");
        
        if (eventVObj == null || !(eventVObj instanceof Map))
            return returnList;
        
        Map<String, Object> eventVMap = (Map<String, Object>)eventVObj;
        
        if (eventVMap != null && !eventVMap.isEmpty())
        {
            Map<String, Object> tEventVMap = eventVMap;
            
            if (tEventVMap.containsKey("@id") && tEventVMap.containsKey("@error"))
            {
                tEventVMap = new HashMap<String, Object>(1);
                tEventVMap.put(Integer.toString(0), eventVMap);
            }
            
            for (Object childEventVObj : tEventVMap.values())
            {
                if (childEventVObj == null || !(childEventVObj instanceof Map))
                    continue;
                
                Map<String, Object> childEventVMap = (Map<String, Object>)childEventVObj;
                
                if (!childEventVMap.containsKey("@id") && !childEventVMap.containsKey("@error"))
                {
                    continue;
                }
                
                Map<String, String> objectNameValueMap = new HashMap<String, String>();
                returnList.add(objectNameValueMap);
                
                objectNameValueMap.put("@id", childEventVMap.get("@id").toString());
                objectNameValueMap.put("@error", childEventVMap.get("@error").toString());
            }
        }
        
        return returnList;
    }
    
    private String parseCreateSubscriptionResponse(String pullSubscriptionRequestXml, String response) throws Exception
    {
        Map<String, Object> responseJSON = new HashMap<String, Object>();
        
        StringUtils.jsonObjectStringToMap(response, "responseJSON", responseJSON);
        
        if (responseJSON.isEmpty())
        {
            throw new Exception("Create subscription, pull subscription request xml = [" + pullSubscriptionRequestXml + "] did not return JSON");
        }
        
        return parseSubscriptionResponse(responseJSON);        
    }
}
