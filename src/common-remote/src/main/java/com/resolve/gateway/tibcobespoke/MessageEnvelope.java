package com.resolve.gateway.tibcobespoke;

import org.apache.commons.lang3.StringUtils;

public class MessageEnvelope
{
    private static final String MSG_TAG = "<==MSG==>";   
    private static final String MSG_ID_TAG = "<==MSG_ID==>";
    private static final String RESPONSE_QUEUE = "<==RESPONSE_QUEUE==>";
    private static final String BUS_URI_TAG = "<==BUS_URI==>";
    private static final String PROCESS_ID_TAG = "<==PROCESS_ID==>";
    private static final String STORE_ID_TAG = "<==STORE_ID==>";
    private static final String MSG_TOPIC_TAG = "<==MSG_TOPIC==>";
    private static final String MSG_TYPE_TAG = "<==MSG_TYPE==>";
    private static final String MSG_TIMEOUT_TAG = "<==MSG_TIMEOUT==>";
    private static final String RABBIT_ID_TAG = "<==RABBIT_ID==>";
    private static final String RABBIT_CONTAINER_TAG = "<==RABBIT_CONTAINER_TAG==>";
    
    private StringBuffer messageBuffer;    
    public MessageEnvelope()
    {
        messageBuffer = new StringBuffer();
    }
    
    public MessageEnvelope(String message)
    {
        if(message.contains(MSG_TAG) || message.contains(RABBIT_CONTAINER_TAG))
        {
            messageBuffer = new StringBuffer(message);
        }
        else
        {
            messageBuffer = new StringBuffer();
            setTagAndValue(message, MSG_TAG);
        }
    }
    
    public String getEvelopeString()
    {
        return getCurString();
    }
    
    public void setMessage(String message)
    {
        setTagAndValue(message, MSG_TAG);
    }
    
    public String getMessage()
    {
        return StringUtils.substringBetween(getCurString(), MSG_TAG);
    }
    
    private void setTagAndValue(String value, String tag)
    {
        if(getCurString().contains(tag))
        {
            getCurString().replaceAll(tag+".*?"+tag, "");            
        }
        
        messageBuffer.append(tag);
        messageBuffer.append(value);
        messageBuffer.append(tag);
        
    }
    
    private String getCurString()
    {
        return messageBuffer.toString();
    }
    
    public void setMessageId(String id)
    {        
        setTagAndValue(id,MSG_ID_TAG);
    }
    
    public String getMessageId()
    {        
        return StringUtils.substringBetween(getCurString(), MSG_ID_TAG);
    }
    
    public void setResponseQueue(String responseQueue)
    {
        setTagAndValue(responseQueue, RESPONSE_QUEUE);
    }
    
    public String getResponseQueue()
    {
        return StringUtils.substringBetween(getCurString(), RESPONSE_QUEUE);
    }
    
    public void setBusUri(String busUri)
    {
        setTagAndValue(busUri, BUS_URI_TAG);
    }
    
    public String getBusUri()
    {
        return StringUtils.substringBetween(getCurString(), BUS_URI_TAG);
    }
    
    public void setProcessId(String processId)
    {
        setTagAndValue(processId, PROCESS_ID_TAG);
    }
    
    public String getProcessId()
    {
        return StringUtils.substringBetween(getCurString(), PROCESS_ID_TAG);
    }
    
    public void setStoreId(String storeId)
    {
        setTagAndValue(storeId, STORE_ID_TAG);        
    }
    
    public String getStoreId()
    {
        return StringUtils.substringBetween(getCurString(), STORE_ID_TAG);
    }
    
    public void setMessageTopic(String messageTopic)
    {
        setTagAndValue(messageTopic, MSG_TOPIC_TAG);
    }
    
    public String getMessageTopic()
    {
        return StringUtils.substringBetween(getCurString(), MSG_TOPIC_TAG);
    }
    
    public void setMessageType(String messageType)
    {
        setTagAndValue(messageType, MSG_TYPE_TAG);        
    }
    
    public String getMessageType()
    {
        return StringUtils.substringBetween(getCurString(), MSG_TYPE_TAG);
    }
    
    public void setTimeout(String timeout)
    {
        setTagAndValue(timeout, MSG_TIMEOUT_TAG);
    }
    
    public String getTimeout()
    {
        return StringUtils.substringBetween(getCurString(), MSG_TIMEOUT_TAG);
    }
    
    public void setRabbitContainer(String rabbitMessage)
    {
        setTagAndValue(rabbitMessage, RABBIT_CONTAINER_TAG);
    }
    
    public String getRabbitContainer()
    {
        return StringUtils.substringBetween(getCurString(), RABBIT_CONTAINER_TAG);
    }
    
    public void setRabbitId(String rabbitId)
    {
        setTagAndValue(rabbitId, RABBIT_ID_TAG);
    }
    
    public String getRabbitId()
    {
        return StringUtils.substringBetween(getCurString(), RABBIT_ID_TAG);
    }
    
}
