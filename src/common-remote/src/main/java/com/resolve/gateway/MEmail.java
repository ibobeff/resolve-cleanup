/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.email.EmailAddress;
import com.resolve.gateway.email.EmailFilter;
import com.resolve.gateway.email.EmailGateway;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * @author alokika.dash
 *
 */
public class MEmail extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MEmail.setFilters";
    private static final String RSCONTROL_SET_EMAILADDRESSES = "MEmail.setEmailAddresses";

    private static final EmailGateway instance = EmailGateway.getInstance();

    public MEmail()
    {
        super.instance = instance;
    }

    @Override
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * This method is invoked once filter synchronization message from RSCONTROL
     * is received.
     * @param paramList
     */
    @Override
    public void synchronizeGateway(Map<String, List<Map<String, Object>>> paramList)
    {
        if (paramList != null)
        {
            //synchronize all the common data like filter, name properties, routing schemas
            super.synchronizeGateway(paramList);
            
            //super class method sets them to be true, however the last step may fail
            isUpdateRSControl = false;
            instance.setSyncDone(false);
            
            List<Map<String, Object>> emailAddresses = (List<Map<String, Object>>) paramList.get("EMAILADDRESSES");
            int number = 0;
            if (emailAddresses != null)
            {
                number = emailAddresses.size();
            }
            Log.log.debug("NUMBER of email addresses " + number);

            clearAndSetEmailAddresses(emailAddresses);

            isUpdateRSControl = true;
            //tell the gateway that we have received RSControl sync message.
            instance.setSyncDone(true);
        }
    }

    /**
     * This method is called by the social UI when for example user add/deletes/update
     * a process email/im.
     */
    public void sendSyncRequest(Map<String, Object> paramList)
    {
        //we do not want to ask for synchronization if there is
        if (paramList != null && paramList.containsKey("UPDATE_SOCIAL_POSTER"))
        {
            Boolean updateSocialPoster = Boolean.parseBoolean((String) paramList.get("UPDATE_SOCIAL_POSTER"));
            if (updateSocialPoster && instance.isSocialPoster())
            {
                instance.sendSyncRequest();
            }
        }
        else
        {
            //some other consumer could ask for everybody to sync.
            instance.sendSyncRequest();
        }
    }

    /**
     * Applies the given {@link EmailFilter} instance's EmailFilter.QUERY property to the
     * given filterMap.
     * 
     * @param filterMap
     *            the filter map instance
     * @param filter
     *            the EmailFilter instance
     */
    @Override
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        if (instance.isActive())
        {
            EmailFilter emailFilter = (EmailFilter) filter;
            filterMap.put(EmailFilter.QUERY, emailFilter.getQuery());
        }
    } // populateGatewaySpecificProperties

    /**
     * Sends an internal message to RSControl listing all email filters used
     * in this EmailGateway
     * 
     * @param params
     *            a map of parameters. This method applies the map's
     *            {@link Constants}.SYS_UPDATED_BY values to the
     *            {@link EmailGateway}'s filterMap
     */public void getEmailAddresses(Map<String, Object> params)
    {
        if (instance.isActive())
        {
            Map<String, String> content = new HashMap<String, String>();

            for (EmailAddress emailAddress : instance.getEmailAddresses().values())
            {
                Map<String, Object> filterMap = new HashMap<String, Object>();
                filterMap.put(EmailAddress.EMAILADDRESS, emailAddress.getEmailAddress());
                filterMap.put(EmailAddress.EMAILP_ASSWORD, emailAddress.getEmailPassword());
                filterMap.put("QUEUE", instance.getQueueName());

                // This value comes all the way from the gateway filter
                // deployment UI.
                // This is important so RSConrtol can set the user name who
                // deployed the filter.
                if (params != null)
                {
                    filterMap.put(Constants.SYS_UPDATED_BY, params.get(Constants.SYS_UPDATED_BY));
                }

                content.put(emailAddress.getEmailAddress(), StringUtils.remove(StringUtils.mapToString(filterMap), "null"));
            }

            // send content to rscontrol
            if (isUpdateRSControl)
            {
                //this is important as RSControl need this
                content.put("QUEUE", instance.getQueueName());

                if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROL, RSCONTROL_SET_EMAILADDRESSES, content) == false)
                {
                    Log.log.warn("Failed to send email addresses to RSCONTROL");
                }
            }
        }
    } // getFilters

     /**
      * Clears the email addresses in the current instance of
      * {@link EmailGateway}. Sets the email address list of the current EmailGateway
      * instance to what is in the paramList {@link List}. Sends an internal message
      * to {@link RSControl} of the first email address in the paramList {@link Map}.
      * 
      * @param paramList
      *            a {@link List} object of email address {@link Map}s.
      */
     @FilterCompanionModel(modelName = "EmailAddress")
     public void clearAndSetEmailAddresses(List<Map<String, Object>> paramList)
    {
        // clear and set filters
        instance.clearAndSetEmailAddresses(paramList);

        // send new filters
        Map<String, Object> params = null;
        //extract the first Map so we get the username who is deploying the filter
        if (paramList != null && paramList.size() > 0)
        {
            params = paramList.get(0);
        }
        getEmailAddresses(params);
        // save config
        MainBase.main.exitSaveConfig();

    } // clearAndSetFilters

     /**
      * Enqueues a map of parameters into the EmailGateway. 
      * 
      * @param params
      *            a {@link Map} message to be sent to the EmailGateway singleton.
      */
     public void sendMessage(Map<String, Object> params)
    {
        Log.log.trace("Received message to be sent:" + params);
        if (instance.isActive() && instance.isPrimary())
        {
            if (params != null)
            {
                try
                {
                    //keep trying until the addToSendQueue succeeeded.
                    while (true)
                    {
                        if (instance.enqueue(params))
                        {
                            break;
                        }
                        else
                        {
                            //wait for 2 of seconds.
                            Thread.sleep(2000L);
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage(), e);
                }
            }
        }
    } // sendMessage
}
