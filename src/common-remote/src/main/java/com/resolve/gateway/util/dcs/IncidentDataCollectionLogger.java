package com.resolve.gateway.util.dcs;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;

public class IncidentDataCollectionLogger extends BaseDataCollectionLogger<Object> {

    private static final String EVENT_TYPE_KEY = "event-type";
    private ObjectMapper objectMapper;

    public IncidentDataCollectionLogger() {
        objectMapper = new ObjectMapper();
    }

    @Override
    protected Map<String, String> getPropertiesMap(String... properties)
    {
        Map<String, String> propertiesMap = new HashMap<>();
        propertiesMap.put(EVENT_TYPE_KEY, properties[0]);
        return propertiesMap;
    }

    @Override
    protected String getMessageType()
    {
        return "incident-event";
    }

    @Override
    protected Object transformData(Object data) throws Exception
    {
        String transformedData = objectMapper.writeValueAsString(data);

        return transformedData;
    }



}
