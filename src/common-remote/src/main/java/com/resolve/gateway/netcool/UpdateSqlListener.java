/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.netcool;

import com.resolve.util.Log;
import com.resolve.util.queue.AbstractQueueListener;

public class UpdateSqlListener extends AbstractQueueListener<String>
{
    public boolean process(String updateStatement)
    {
        boolean result = true;

        try
        {
            if (Log.log.isTraceEnabled())
            {
                Log.log.trace("NetcoolGateway: Received update SQL statement to execute");
                Log.log.trace("NetcoolGateway: Statement: " + updateStatement);
            }

            NetcoolGateway.getInstance().update(updateStatement);
        }
        catch (Exception e)
        {
            Log.log.error("NetcoolGateway: Failed to execute update statement: " + updateStatement, e);            result = false;
        }

        return result;
    }
}
