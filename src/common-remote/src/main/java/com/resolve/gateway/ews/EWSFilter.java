/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.ews;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import microsoft.exchange.webservices.data.core.service.schema.ItemSchema;
import microsoft.exchange.webservices.data.core.enumeration.search.LogicalOperator;
import microsoft.exchange.webservices.data.search.filter.SearchFilter;

import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.RetainValue;
import com.resolve.util.DateUtils;
import com.resolve.util.StringUtils;

public class EWSFilter extends BaseFilter
{
    // Additional field names for this filter.
    public static final String QUERY = "QUERY";
    public static final String INCLUDE_ATTACHMENT = "INCLUDE_ATTACHMENT";
    public static final String LAST_RECEIVED_TIME = "LAST_RECEIVED_TIME";
    public static final String FOLDER_NAMES = "FOLDER_NAMES";
    public static final String MARK_AS_READ = "MARK_AS_READ";
	public static final String ONLY_NEW = "ONLY_NEW";
    private String query;
    private Boolean includeAttachment = Boolean.FALSE;
    private String nativeQuery; // stores the native query after translation by
                                // Resolve Query translator.
    private String lastReceivedTime = null;
    private Long lastReceivedTimeMillis= new Long(-1);
    private List<SearchFilter> searchFilters = new ArrayList<SearchFilter>();
    private ConcurrentHashMap<String,Boolean> processResults = new ConcurrentHashMap<String,Boolean>();
    private Boolean markAsRead = Boolean.FALSE;
    private List<String> folderNames = new ArrayList<String>();
    private List<String> lastReceivedIds = new ArrayList<String>();
	private Boolean onlyNew = Boolean.TRUE;
    public EWSFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String query, String includeAttachment, String folderNames,String markAsRead, String onlyNew)

    {
        super(id, active, order, interval, eventEventId, runbook, script);
        setQuery(query);
        setIncludeAttachment(includeAttachment);
        setMarkAsRead(markAsRead);
		setOnlyNew(onlyNew);
        this.folderNames = StringUtils.stringToList(folderNames,",");
    } // EWSFilter

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query != null ? query.trim() : query;
    }
    
    public Boolean getIncludeAttachment()
    {
        return includeAttachment;
    }

    public void setIncludeAttachment(Boolean includeAttachment)
    {
        this.includeAttachment = includeAttachment;
    }
    
    public void setIncludeAttachment(String includeAttachment)
    {
        this.includeAttachment = Boolean.valueOf(includeAttachment);
    }
    
    public String getNativeQuery()
    {
        return nativeQuery;
    }

    public void setNativeQuery(String nativeQuery)
    {
        this.nativeQuery = nativeQuery != null ? nativeQuery.trim() : nativeQuery;
    }
    
    @RetainValue
    public String getLastReceivedTime()
    {
        return lastReceivedTime;
    }

    public void setLastReceivedTime(String lastReceivedTime)
    {
        this.lastReceivedTime = lastReceivedTime != null ? lastReceivedTime.trim() : lastReceivedTime;
    }

    public void setSearchFilters(List<SearchFilter> searchFilters)
    {
        this.searchFilters = searchFilters;
    }

    public List<SearchFilter> getSearchFilters()
    {
        return searchFilters;
    }
    
    public SearchFilter getCombinedSearchFilter()
    {
        SearchFilter result = null;
        SearchFilter searchFilter = null;
        
        if(this.searchFilters != null && this.searchFilters.size() > 0)
        {
            List<SearchFilter> filters = new ArrayList<SearchFilter>(searchFilters);
			if (onlyNew)
			{
	            if (StringUtils.isNotBlank(getLastReceivedTime()))
	            {
	                //the format is 2010-03-01T18:30:00Z
	                searchFilter = (new SearchFilter.IsGreaterThanOrEqualTo(ItemSchema.DateTimeReceived, DateUtils.convertStringToDate(getLastReceivedTime()))); 
	                filters.add(searchFilter);
	            }
			}
           result = new SearchFilter.SearchFilterCollection(LogicalOperator.And, filters);
     
       }else if (StringUtils.containsIgnoreCase(this.query,"RECIPIENTS")){
           if (onlyNew && StringUtils.isNotBlank(getLastReceivedTime()))
           {
               //the format is 2010-03-01T18:30:00Z
                searchFilter = (new SearchFilter.IsGreaterThanOrEqualTo(ItemSchema.DateTimeReceived, DateUtils.convertStringToDate(getLastReceivedTime()))); 
           }
           result = searchFilter;
       }
       
        return result;
    }
    
    public void setProcessResults(ConcurrentHashMap<String,Boolean>processResults)
    {
        this.processResults = processResults;
    }
    
    public ConcurrentHashMap<String,Boolean> getProcessResults()
    {
        return processResults;
    }
    
    public List<String> getLastReceivedIds() {
        return lastReceivedIds;
    }

    public void setLastReceivedIds (List<String> lastReceivedIds) {
        this.lastReceivedIds = lastReceivedIds;
    }

    public Long getLastReceivedTimeMillis() {
        return lastReceivedTimeMillis;
    }

    public void setLastReceivedTimeMillis(Long lastReceivedTimeMillis) {
        this.lastReceivedTimeMillis = lastReceivedTimeMillis;
    }

    public List<String> getFolderNames()
    {
        return folderNames;
    }

    public void setFolderNames(List<String> folderNames)
    {
        this.folderNames = folderNames;
    }

	public Boolean getMarkAsRead()
	{
		return markAsRead;
	}
	
	public void setMarkAsRead(Boolean markAsRead)
	{
		this.markAsRead = markAsRead;
	}
	
	public void setMarkAsRead(String markAsRead)
	{
		if (markAsRead != null)
		{
			this.markAsRead = Boolean.valueOf(markAsRead);
		}
	}

	public Boolean getOnlyNew()
    {
        return onlyNew;
    }

    public void setOnlyNew(String onlyNew)
    {
    	if (onlyNew != null)
    	{
	        this.onlyNew = Boolean.valueOf(onlyNew);
    	}
    }
    
    
}
