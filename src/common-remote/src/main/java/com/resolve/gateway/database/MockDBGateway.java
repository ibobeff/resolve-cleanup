/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.database;

import java.util.HashMap;
import java.util.Map;

import com.resolve.gateway.DataProvider;
import com.resolve.gateway.Filter;
import com.resolve.rsremote.ConfigReceiveDB;
import com.resolve.util.Log;

public class MockDBGateway extends DBGateway
{
    // Singleton
    private static volatile MockDBGateway instance = null;

    private DataProvider dataProvider;

    public static MockDBGateway getInstance(ConfigReceiveDB config)
    {
        if (instance == null)
        {
            instance = new MockDBGateway(config);
        }
        return (MockDBGateway) instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     * 
     * @return
     */
    public static MockDBGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("Database Gateway is not initialized correctly..");
        }
        else
        {
            return (MockDBGateway) instance;
        }
    }

    protected MockDBGateway(ConfigReceiveDB config)
    {
        // Important, call super here.
        super(config);
        dataProvider = new DBDataProvider();
    }

    @Override
    public void run()
    {
        while (running)
        {
            try
            {
                long startTime = System.currentTimeMillis();

                Log.log.trace("Local Event Queue is empty.....");

                for (Filter filter : orderedFilters)
                {
                    if (shouldFilterExecute(filter, startTime))
                    {
                        DBFilter dbFilter = (DBFilter) filter;

                        Map<String, String> runbookParams = dataProvider.getData(dbFilter);

                        addToPrimaryDataQueue(dbFilter, runbookParams);
                    }
                }

                // If there is no filter yet no need to keep spinning, wait
                // for a while
                if (orderedFilters.size() == 0)
                {
                    Thread.sleep(pollInterval);
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error when polling by DB Gateway, stop polling...");
                Log.log.warn("Error: " + e.getMessage(), e);
                try
                {
                    Thread.sleep(pollInterval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
            finally
            {
                Log.log.trace("DB Gateway poller is sleeping");
            }
        }
    }

    /**
     * This could very well be another Mock engine we'll build later
     */
    class DBDataProvider implements DataProvider
    {
        @Override
        public Map<String, String> getData(Filter filter)
        {
            Map<String, String> result = new HashMap<String, String>();
            result.put(filter.getId() + ".Key1", "Value1");
            result.put(filter.getId() + ".Key2", "Value2");
            result.put(filter.getId() + ".Key3", "Value3");
            result.put(filter.getId() + ".Key4", "Value4");
            result.put(filter.getId() + ".Key5", "Value5");
            return result;
        }
    }
}
