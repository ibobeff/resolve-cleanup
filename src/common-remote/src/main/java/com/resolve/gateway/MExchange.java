/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.List;
import java.util.Map;

import com.resolve.gateway.exchange.ExchangeFilter;
import com.resolve.gateway.exchange.ExchangeGateway;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MExchange extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MExchange.setFilters";

    private static ExchangeGateway instance = ExchangeGateway.getInstance();

    public MExchange()
    {
        super.instance = instance;
    }

    @Override
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * Enqueues a map of messages to the current ExchangeGateway instance.
     * 
     * @param message
     *            A map of messages to send.
     * 
     */
    public static void sendMessage(Map<String, Object> message)
    {
        if (instance.isActive() && instance.isPrimary())
        {
            if (message != null)
            {
                try
                {
                    ExchangeGateway.updateQueue.add(message);
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage(), e);
                }
            }
        }
    } // sendMessage

    /**
     * Iterates through a given list of map messages and enqueues them to the
     * current ExchangeGateway instance.
     * 
     * @param messages
     *            A map of messages to send.
     * 
     */
    public static void sendMessages(List<Map<String, Object>> messages)
    {
        if (instance.isPrimary() && messages != null && !messages.isEmpty())
        {
            for (Map<String, Object> message : messages)
            {
                try
                {
                    ExchangeGateway.updateQueue.add(message);
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage(), e);
                }
            }
        }
    }

    @Override
    protected void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        if (ExchangeGateway.active)
        {
            ExchangeFilter exchangeFilter = (ExchangeFilter) filter;

            filterMap.put(ExchangeFilter.SUBJECT, exchangeFilter.getSubject());
            filterMap.put(ExchangeFilter.CONTENT, exchangeFilter.getContent());
            filterMap.put(ExchangeFilter.SENDERNAME, exchangeFilter.getSenderName());
            filterMap.put(ExchangeFilter.SENDERADDRESS, exchangeFilter.getSenderAddress());
            
            Map criteriaMap = exchangeFilter.getCriteria();
            String criteriaString = StringUtils.objToString(criteriaMap);

            filterMap.put(ExchangeFilter.CRITERIA, criteriaString);
        }
    }

    /**
     * Moves the given email IDs to the given folder name. Sends them to a
     * subfolder name if given.
     * 
     * @param emailIDs
     *            a String array of emailID values
     * 
     * @param foldername
     *            name of the folder
     * 
     * @param subfolder
     *            name of the subfolder
     * 
     * @throws Exception
     * 
     * @return a boolean value on whether the operation was successful
     */public synchronized boolean moveEmailToFolder(String[] emailIDs, String foldername, String subfoldername)
    {
        boolean sent = false;

        try
        {
            sent = ExchangeGateway.moveEmailToFolder(emailIDs, foldername, subfoldername);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }

        return sent;
    }
}
