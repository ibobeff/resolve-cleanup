package com.resolve.gateway.tibcobespoke;

public class TIBCOBespokeRabbitWorker
{
    public TIBCOBespokeRabbitWorker()
    {
    }
    //Override me
    public String doWork(String message)
    {
        return "Need to override RabbitWorker";
    }
}
