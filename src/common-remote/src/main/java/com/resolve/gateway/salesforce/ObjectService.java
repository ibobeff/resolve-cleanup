/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.salesforce;

import java.util.List;
import java.util.Map;

public interface ObjectService
{
    /**
     * Every object has its ID property, the subclass requires to implement this
     * method. Example: Service request could be "NUMBER".
     * 
     * @return
     */
    String getIdPropertyName();

    /**
     * Identity of the object. Implementing class provides this. Example:
     * Incident is "incident" or Problem is "problem"
     * 
     * @return
     */
    String getIdentity();

    /**
     * Creates an object by supplying username, password.
     * 
     * @param params
     * @param username
     * @param password
     * @return a Map with all the properties of the new object.
     * @throws Exception
     */
    Map<String, String> create(Map<String, String> params, String username, String password) throws Exception;

    /**
     * Creates an object by supplying username, password.
     * 
     * @param parentObjectId
     *            such as SysId for an Incident. It's mandatory.
     * @param note
     *            to be added.
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    boolean createWorknote(String parentObjectId, String note, String username, String password) throws Exception;

    /**
     * Updates an object by supplying username, password.
     * 
     * @param objectId
     *            is mandatory.
     * @param params
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    void update(String objectId, Map<String, String> params, String username, String password) throws Exception;

    /**
     * Deletes an object by its id by supplying username, password.
     * 
     * @param objectId
     *            is mandatory.
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    void delete(String objectId, String username, String password) throws Exception;

    /**
     * Retrieves an object's data by its id.
     * 
     * @param objectId
     *            is mandatory
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    Map<String, String> selectByID(String objectId, String username, String password) throws Exception;

    /**
     * Retrieves an object's data by its alternate id (for example, Incident
     * Number).
     * 
     * @param number
     * @param username
     * @param password
     * @return
     */
    Map<String, String> selectByNumber(String number, String username, String password) throws Exception;

    /**
     * Read data from the object service by supplying username, password.
     * 
     * @param query
     * @param orderBy
     * @param isAscending
     *            if true the order by ascending.
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    List<Map<String, String>> search(String query, String orderBy, boolean isAscending, String username, String password) throws Exception;

    // void update(Map<String, String> objectProperties, String username, String
    // password) throws Exception;

}
