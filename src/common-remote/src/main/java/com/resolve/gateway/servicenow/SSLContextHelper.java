package com.resolve.gateway.servicenow;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.apache.commons.net.util.TrustManagerUtils;

import com.resolve.rsremote.ConfigReceiveServiceNow;
import com.resolve.util.FileUtils;
import com.resolve.util.StringUtils; 

public final class SSLContextHelper
{

    private static final String DEFAULT = "default";
    String keystoreFile;
    String keystoreFilePassword;
    
    String truststoreFile;
    String truststoreFilePassword;
        
    public SSLContextHelper(     String keystoreFile,
    String keystoreFilePassword,
    
    String truststoreFile,
    String truststoreFilePassword
        )
    {
        
        this.keystoreFile = keystoreFile;
        this.keystoreFilePassword = keystoreFilePassword;
        this.truststoreFile = truststoreFile;
        this.truststoreFilePassword = truststoreFilePassword;

    }
    
    public SSLContextHelper(ConfigReceiveServiceNow sn)
    {
        this(sn.getMutualtlskeystore(), sn.getMutualtlskeystorepass(), sn.getMutualtlstruststore(), sn.getMutualtlstruststorepass());
        
        
    }
    
    
    
    
        // TODO Auto-generated constructor stub 
    public SSLContext getSSLContext() throws NoSuchAlgorithmException, KeyManagementException, Exception
    {
        SSLContext context = SSLContext.getInstance("TLS");
        context.init(getKeyManager().getKeyManagers(), getTrustManager(), null);
        return context;
    }

   
    
    
    protected KeyManagerFactory getKeyManager() throws Exception
    {   KeyStore ks;
    
        char[] pass = keystoreFilePassword != null?keystoreFilePassword.toCharArray():null;
        ks = loadKeystore(keystoreFile, pass);
        
        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(ks, pass);
        return kmf;
    }
    
    
    protected TrustManager[] getTrustManager() throws Exception
    {   KeyStore ks;
         
        if(truststoreFile == null || org.apache.commons.lang3.StringUtils.isEmpty(truststoreFile))
        {
            
            return new TrustManager[] {TrustManagerUtils.getAcceptAllTrustManager()};
            
        }else
            if(DEFAULT.equalsIgnoreCase(truststoreFile))
            {
                
                return new TrustManager[] {TrustManagerUtils.getValidateServerCertificateTrustManager()};
                
            }
        
        char[] pass = StringUtils.isNotEmpty(truststoreFilePassword)?truststoreFilePassword.toCharArray():null;
        ks = loadKeystore(truststoreFile, pass);
        
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(ks);
        
     
        return tmf.getTrustManagers();
    }

    protected KeyStore loadKeystore(String keystoreFileToLoad, char[] pass) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, FileNotFoundException
    {
        KeyStore ks;
        if(org.apache.commons.lang3.StringUtils.endsWithIgnoreCase(keystoreFileToLoad, ".p12" ) || org.apache.commons.lang3.StringUtils.endsWithIgnoreCase(keystoreFileToLoad, ".pfx" ))
        {
            ks = KeyStore.getInstance("pkcs12");
            ks.load(new FileInputStream(FileUtils.getFile(keystoreFileToLoad)), pass);
            
            
        }else
        {
            
            ks = KeyStore.getInstance("JKS");
            ks.load(new FileInputStream(FileUtils.getFile(keystoreFileToLoad)), pass);
            
        }
        return ks;
    }
}
