/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDriver;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.owasp.esapi.ESAPI;

import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MDatabase;
import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveDB;
import com.resolve.util.Constants;
import com.resolve.util.ERR;
import com.resolve.util.Log;
import com.resolve.util.ResolveDefaultValidator;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;
import com.resolve.util.metrics.ExecutionMetrics;
import com.resolve.util.metrics.ExecutionMetricsException;

public class DBGateway extends BaseClusteredGateway
{
    // Singleton
    private static volatile DBGateway instance = null;

    // This will be set in initialize() with the proper queue name from the
    // configuration.
    private String queue = "DATABASE";

    private ConcurrentHashMap<String, DBPool> pools = new ConcurrentHashMap<String, DBPool>();
    private ConcurrentHashMap<String, GenericObjectPool<Connection>> initedPools = new ConcurrentHashMap<String, GenericObjectPool<Connection>>();

    public List<DBPool> startupPools = new ArrayList<DBPool>(); // Connection Pools

    protected long pollInterval = 0;
    
    // Key: filter id Value : Map of update fields by update query
    private Map<String, Map<String, Map<String, String>>> filterIdToUpdateFieldMap = new HashMap<String, Map<String, Map<String, String>>>();

    private boolean batchUpdateSupportIdentified = false;
    private boolean batchUpdateSupported = false;
    private Statement batchUpdateStatement = null;
    
    public static DBGateway getInstance(ConfigReceiveDB config)
    {
        if (instance == null)
        {
            instance = new DBGateway(config);
        }
        return (DBGateway) instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static DBGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("Database Gateway is not initialized correctly..");
        }
        else
        {
            return (DBGateway) instance;
        }
    }

    protected DBGateway(ConfigReceiveDB config)
    {
        // Important, call super here.
        super(config);
        queue = config.getQueue();
    }

    @Override
    protected void initialize()
    {
        ConfigReceiveDB config = (ConfigReceiveDB) configurations;
        queue = config.getQueue().toUpperCase();

        try
        {
            Log.log.info("Initializing DB Listener");
            initStartupPools();

            // init settings
            pollInterval = config.getPollInterval() * 1000; // convert to in
                                                            // seconds
            running = config.isPoll(); // running gets overidden in DB gateway
                                       // because POLL decides to startPolling.

            this.gatewayConfigDir = "/config/database/";
            
            if (Log.log.isDebugEnabled())
            {
                Log.log.debug(config.toString());
            }
        }
        catch (Exception e)
        {
            Log.log.error("Failed to config Email Gateway: " + e.getMessage(), e);
        }
    }

    /**
     * Clears and set new pool.
     *
     * @param poolList
     */
    public void clearAndSetPools(List<Map<String, Object>> poolList)
    {
        // remove all pools
        pools.clear();

        // set pools
        if (poolList != null)
        {
            for (Map<String, Object> params : poolList)
            {
                //ignore the username Map
                if(!params.containsKey("RESOLVE_USERNAME"))
                {
                    String name = (String) params.get(DBPool.NAME);
                    // add pool
                    pools.put(name, getPool(params));
                    params.remove(DBPool.P_ASSWORD);
                    Log.log.info("Adding Pool: " + params);
                }
            }
        }
        // create new pools
        synchronized (DBGateway.class)
        {
            startupPools.clear();
            startupPools.addAll(pools.values());
            try
            {
                initStartupPools();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

    } // clearAndSetPools

    public void setPool(Map<String, Object> params)
    {
        DBPool pool = getPool(params);

        // remove existing pool
        removePool(pool.getName());

        // update pools
        pools.put(pool.getName(), pool);

        // create new pools
        synchronized (getClass())
        {
            startupPools = new ArrayList<DBPool>(pools.values());
        }
    } // setPool

    public DBPool getPool(Map<String, Object> params)
    {
        return new DBPool((String) params.get(DBPool.NAME), (String) params.get(DBPool.DRIVERCLASS), (String) params.get(DBPool.CONNECTURI), String.valueOf(params.get(DBPool.MAXCONNECTION)), (String) params.get(DBPool.USERNAME), (String) params.get(DBPool.P_ASSWORD));

    } // getPool

    public void removePool(String name)
    {
        DBPool pool = pools.get(name);

        if (pool != null)
        {
            // remove from pools
            //pools.remove(name);
            
            if (initedPools.containsKey(name))
            {                
                GenericObjectPool<Connection> objectPool = initedPools.get(name);
                try
                {
                    objectPool.close();
                    // remove from pools
                    pools.remove(name);
                    initedPools.remove(name);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
            
            // create new pools
            synchronized (DBGateway.class)
            {
                startupPools.clear();
                startupPools.addAll(pools.values());
            }
        }
    } // removePool

    public Map<String, DBPool> getPools()
    {
        return pools;
    } // getPools

    private void initStartupPools() throws Exception
    {
        for (DBPool pool : startupPools)
        {
            Properties props = new Properties();
            props.setProperty(DBPool.USERNAME, pool.getDbUserName());
            props.setProperty(DBPool.P_ASSWORD, pool.getDbP_assword());
            props.setProperty(DBPool.NAME, pool.getName());
            props.setProperty(DBPool.MAXCONNECTION, "" + pool.getMaxConnection());
            initDBPool(pool.getDriverClass(), pool.getConnectURI(), props);

        }
    } // initStartupPools

    // This can be called by other classes or Groovy script to create a new
    // connection pool at runtime
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void initDBPool(String driverClassName, String connectURI, Properties props) throws Exception
    {
        String poolID = (String) props.get(DBPool.NAME);// driverClassName + connectURI;
        Log.log.info("Initialize JDBC Connection pool " + poolID + " for driver " + driverClassName + " , connect URI: " + connectURI);
        if (initedPools.get(poolID) != null)
        {
            // Old Logic - This pool was initialized before, so do nothing
            // RBA-13670 Change. If pool is not being used then close the pool and re-initialize it
            //if (Log.log.isTraceEnabled())
            //{
                //Log.log.info("DBPpool " + poolID + " is already initilaized. Number of active connections in " +  poolID + " is " + ((GenericObjectPool) initedPools.get(poolID)).getNumActive());
            //}
                
            if (((GenericObjectPool) initedPools.get(poolID)).getNumActive() == 0)
            {
                GenericObjectPool<Connection> objectPool = initedPools.get(poolID);
                try
                {
                    objectPool.close();
                    initedPools.remove(poolID);
                }
                catch (Exception e)
                {
                    Log.log.error("Failed to close already initialized DBPpool " + poolID + " with no active connections, " +
                                  " due to " + e.getMessage(), e);
                    
                    Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " DB Pool " + poolID + " Re-Initialization With No Active Connecions Failed", 
                              "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " DB Pool " + poolID + " Re-Initialization With No Active Connecions Failed " + 
                              "due to " + e.getMessage());
                    
                    return;
                }
            }
            else
            {
                Log.log.warn("DBPpool " + poolID + " is already initilaized. Number of active connections in " +  poolID + " is " +
                             ((GenericObjectPool) initedPools.get(poolID)).getNumActive() + " and hence cannot be re-initialized at this time.");
                
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " DB Pool " + poolID + " Re-Initialization Failed", 
                                "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " DB Pool " + poolID + " Re-Initialization Failed " + 
                                "since DBPool is already initialized and has " + ((GenericObjectPool) initedPools.get(poolID)).getNumActive() + " active connections.");
                return;
            }
        }
        //else
        //{
            try
            {
                if (Log.log.isTraceEnabled())
                {
                    Log.log.trace("Create a new DB Pool " + poolID + " for driver " + driverClassName + " , connect URI: " + connectURI);
                    Log.log.trace("Load SQL driver " + driverClassName);
                }
                Class.forName(driverClassName);
            }
            catch (ClassNotFoundException e)
            {
                Log.log.error("Error " + e.getMessage() + " in loading driver " + driverClassName);
                Log.alert(ERR.E20001, "Failed to load SQL driver " + driverClassName, e);
                throw e;
            }
        //}

        GenericObjectPool connectionPool = new GenericObjectPool(null);
        connectionPool.setTestOnBorrow(true);
        // If maximum connection number
        if (props.getProperty(DBPool.MAXCONNECTION) != null)
        {
            if (Log.log.isTraceEnabled())
            {
                Log.log.trace("Set maximum number of action connections to " + props.getProperty(DBPool.MAXCONNECTION));
            }
            connectionPool.setMaxActive(Integer.parseInt(props.getProperty(DBPool.MAXCONNECTION)));
        }

        // ConnectionFactory connectionFactory = new
        // DriverManagerConnectionFactory(connectURI, props);
        String username = props.getProperty(DBPool.USERNAME);
        String password = props.getProperty(DBPool.P_ASSWORD);
        ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(connectURI, username, password);
        // Simply initialize it, it's bogus but that's how it works.
        new PoolableConnectionFactory(connectionFactory, connectionPool, null, null, false, true);

        Class.forName("org.apache.commons.dbcp.PoolingDriver");
        PoolingDriver driver = (PoolingDriver) DriverManager.getDriver("jdbc:apache:commons:dbcp:");

        if (props.get(DBPool.NAME) != null)
        {
            // register driver with defined "poolname" value in props
            driver.registerPool(props.get(DBPool.NAME).toString(), connectionPool);
            if (Log.log.isTraceEnabled())
            {
                Log.log.trace("Set new JDBC connection pool name to " + props.getProperty(DBPool.NAME));
            }
        }
        else
        {
            // By default , register this pool with its poolID's hash code as
            // its name
            driver.registerPool(String.valueOf(poolID.hashCode()), connectionPool);
            if (Log.log.isTraceEnabled())
            {
                Log.log.trace("Set new JDBC connection pool name to default name: " + String.valueOf(poolID.hashCode()));
            }
        }

        // Set flag to avoid duplication of pools
        initedPools.put(poolID, connectionPool);

    } // initDBPool

    @Override
    public void run()
    {
        //This call is must for all the ClusteredGateways.
        super.sendSyncRequest();

        while (running)
        {
            try
            {
                long startTime = System.currentTimeMillis();

                /* if any remaining events are in primary data queue or 
                 * primary data executor has waiting messages still to be processed, 
                 * do not get messages for more new events.
                 */
                
                if (primaryDataQueue.isEmpty() && !primaryDataQueueExecutorBusy())
                {
                    Log.log.trace(getQueueName() + " Primary Data Queue is empty and Primary Data Queue Executor is free.....");

                    for (Filter filter : orderedFilters)
                    {
                        if (shouldFilterExecute(filter, startTime))
                        {
                            DBFilter dbFilter = (DBFilter) filter;
                            /* 
                             * Recreate the sql query when we are using the LASTVALUE fields
                             * For e.g. select * from worksheet where sys_created_on > 'TIMESTAMP18-JUL-12 09:24:40 PM' order by sys_created_on asc;
                             * is a recreated sql query where LASTVALUE field was "sys_created_on" 
                             * Query can have "RESOLVE_STATUS <> '1'" then we'll update the field in the same table
                             * so we do not poll the same data.
                             */
                            String sql = dbFilter.getSql();
                            if (!dbFilter.isLastValueEmpty())
                            {
                                String lastValue = (dbFilter.isLastValueQuote() ? "'" : "") + dbFilter.getLastValue() + (dbFilter.isLastValueQuote() ? "'" : "");
                                if (!StringUtils.isEmpty(dbFilter.getPrefix()))
                                {
                                    lastValue = dbFilter.getPrefix() + lastValue;
                                }

                                sql = dbFilter.getSql().replace("LASTVALUE", lastValue);
                            }
                            Connection connection = null;
                            ResultSet resultset = null;
                            Statement statement = null;
                            batchUpdateStatement = null;
                            long batchUpdatePrepareBatchTmNSec = 0l;
                            long processBatchStartTmNSec = 0l;
                            int result = 0;
                            
                            try
                            {
                                try
                                {
                                    Log.log.debug("Connection pool name: " + dbFilter.getPool());
                                    connection = getConnection(dbFilter.getPool());
                                    Log.log.debug("Connection is null? " + (connection == null));
                                }
                                catch (SQLException ex)
                                {
                                	Log.log.error("Connection Error on Filter: " + dbFilter.getId() + " Using Pool: " + dbFilter.getPool(), ex);
                                    sendAlert(ALERT_SEVERITY_CRITICAL, true, "Filter:" + dbFilter.getId() + ":Connection Error", ex.getMessage());
                                }
                                
                                Log.log.debug("Connection is null? " + (connection == null));
                                Log.log.debug("SQL to run: " + sql);

                                if (connection != null)
                                {
                                    statement = connection.createStatement();
                                    Log.log.debug("Polling SQL query for DB Gateway pool " + dbFilter.getPool() + " is : " + sql);

                                    if (StringUtils.isNotBlank(sql))
                                    {
                                        boolean hasUpdateQuery = StringUtils.isNotBlank(dbFilter.getUpdateQuery());
                                        //String updateQuery = null;
                                        //Map<String, String> updateFields = new HashMap<String, String>();
                                        if (hasUpdateQuery)
                                        {
                                            // Identify bacth update support only once
                                            if (!batchUpdateSupportIdentified)
                                            {
                                                batchUpdateSupportIdentified = true;
                                                
                                                if (connection.getMetaData().supportsBatchUpdates())
                                                {
                                                    batchUpdateSupported = true;
                                                }
                                            }
                                            
                                            if (batchUpdateSupported)
                                            {
                                                // Create batch update statement
                                                batchUpdateStatement = connection.createStatement();
                                            }
                                            
                                            Map<String, Map<String, String>> queryToUpdateFieldMap = filterIdToUpdateFieldMap.get(dbFilter.getId());
                                            
                                            if (queryToUpdateFieldMap == null)
                                            {
                                                queryToUpdateFieldMap = new HashMap<String, Map<String, String>>();
                                                filterIdToUpdateFieldMap.put(dbFilter.getId(), queryToUpdateFieldMap);
                                            }
                                            
                                            Set<String> savedUpdateQueries = queryToUpdateFieldMap.keySet();
                                            
                                            String savedUpdateQuery = null;
                                            
                                            if (!savedUpdateQueries.isEmpty())
                                            {
                                                savedUpdateQuery = savedUpdateQueries.iterator().next();
                                            }
                                            
                                            if (StringUtils.isBlank(savedUpdateQuery) ||
                                                !(savedUpdateQuery.equals(dbFilter.getUpdateQuery())))
                                            {
                                                queryToUpdateFieldMap.clear();
                                                
                                                Map<String, String> updateFields = new HashMap<String, String>();
                                                
                                                Matcher matcher = VAR_REGEX_GATEWAY_VARIABLE.matcher(dbFilter.getUpdateQuery());
                                                
                                                while (matcher.find())
                                                {
                                                    String field = matcher.group(1);
                                                    updateFields.put("${" + field.toLowerCase() + "}", "\\$\\{" + field + "\\}");
                                                }
                                                
                                                queryToUpdateFieldMap.put(dbFilter.getUpdateQuery(), updateFields);
                                            }
                                        }
                                        else
                                        {
                                            filterIdToUpdateFieldMap.remove(dbFilter.getId());
                                        }

                                        try
                                        {
                                            sql = SQLUtils.getSafeSQL(sql, getDatabaseType(dbFilter.getPool()));
                                            resultset = statement.executeQuery(
                                            				ESAPI.validator().getValidInput(
                                            					"run", sql, 
                                            					ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR,
                                            					ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH, 
                                            					false, false));
                                        }
                                        catch (SQLException ex)
                                        {
                                            Log.log.error("Error " + ex.getMessage() + " in getting safe sql for specified sql [" + sql + "] in filter " + dbFilter.getId());
                                            sendAlert(ALERT_SEVERITY_CRITICAL, true, "Filter:" + dbFilter.getId() + ":SQL Error", ex.getMessage() + 
                                                      " in getting safe sql for specified sql [" + sql + "] in filter " + dbFilter.getId());
                                        }
                                        catch(Throwable t)
                                        {
                                            Log.log.error("Error " + t.getMessage() + " in getting safe sql for specified sql [" + sql + "] in filter " + dbFilter.getId());
                                            sendAlert(ALERT_SEVERITY_CRITICAL, true, "Filter:" + dbFilter.getId() + ":SQL Error", t.getMessage() + 
                                                      " in getting safe sql for specified sql [" + sql + "] in filter " + dbFilter.getId());
                                        }

                                        result = 0;
                                        processBatchStartTmNSec = System.nanoTime();
                                        
                                        if (resultset != null)
                                        {
                                            ExecutionMetrics.INSTANCE.registerEventStart("DB primary GW - pull from DB and put into the worker queue");
                                            
                                            while (resultset.next())
                                            {
                                                if (!dbFilter.isLastValueEmpty())
                                                {
                                                    String resultsetValue = resultset.getString(dbFilter.getLastValueColumn());
                                                    
                                                    try
                                                    {
                                                        convertDate(resultsetValue, dbFilter, getDatabaseType(dbFilter.getPool()));
                                                    }
                                                    catch(Throwable t)
                                                    {
                                                        Log.log.error("Error " + t.getMessage() + " in converting date for filter " + dbFilter.getId());
                                                        sendAlert(ALERT_SEVERITY_CRITICAL, true, "Filter:" + dbFilter.getId() + ":SQL Error", t.getMessage());
                                                    }
                                                    
                                                    if (Log.log.isTraceEnabled())
                                                    {
                                                        Log.log.trace("LastValue for pool " + dbFilter.getId() + " was updated to " + dbFilter.getLastValue());
                                                    }
                                                }

                                                // define values from SELECT
                                                ResultSetMetaData resultsetmetadata = resultset.getMetaData();

                                                Map<String, String> runbookParams = new HashMap<String, String>();

                                                //String tmpUpdateQuery = updateQuery;
                                                for (int i = 1; i <= resultsetmetadata.getColumnCount(); i++)
                                                {
                                                    String name = resultsetmetadata.getColumnName(i);
                                                    String value = resultset.getString(i);

                                                    // remove null from value
                                                    if (value != null)
                                                    {
                                                        value = value.replace('\0', ' ').trim();
                                                    }
                                                    else
                                                    {
                                                        value = "";
                                                    }
                                                    runbookParams.put(name, value);

                                                    /*
                                                    if (hasUpdateQuery)
                                                    {
                                                        String key = "${" + name.toLowerCase() + "}";
                                                        if (updateFields.size() > 0 && updateFields.containsKey(key))
                                                        {
                                                            tmpUpdateQuery = tmpUpdateQuery.replaceAll(updateFields.get(key), value);
                                                        }
                                                    }*/
                                                }

                                                /*
                                                // finally update the record.
                                                if (hasUpdateQuery)
                                                {
                                                    Log.log.debug("Update statement:" + tmpUpdateQuery);
                                                    statement = connection.createStatement();
                                                    int recordCount = 0;
                                                    try
                                                    {
                                                        tmpUpdateQuery = SQLUtils.getSafeSQL(tmpUpdateQuery, getDatabaseType(dbFilter.getPool()));
                                                        recordCount = statement.executeUpdate(tmpUpdateQuery);
                                                    }
                                                    catch (SQLException ex)
                                                    {
                                                        sendAlert(ALERT_SEVERITY_CRITICAL, true, "Filter:" + dbFilter.getId() + ":Update SQL Error", ex.getMessage());
                                                    }

                                                    if (recordCount > 0)
                                                    {
                                                        Log.log.debug("Update successful");
                                                    }
                                                }*/

                                                //processFilter(dbFilter, null, runbookParams);
                                                Log.log.trace("Adding to Primary Data Queue Filter:" + dbFilter + ", Runbook: " + runbookParams.get(Constants.EXECUTE_WIKI));
                                                addToPrimaryDataQueue(dbFilter, runbookParams);
                                                
                                                if (batchUpdateStatement != null)
                                                {
                                                    long batchUpdatePrepareBatchStartTmNSec = System.nanoTime();
                                                    
                                                    updateServerDataInternal(dbFilter, runbookParams);
                                                    
                                                    batchUpdatePrepareBatchTmNSec += (System.nanoTime() - batchUpdatePrepareBatchStartTmNSec);
                                                }

                                                result++;
                                            }

                                            resultset.close();
                                            
                                            ExecutionMetrics.INSTANCE.registerEventEnd("DB primary GW - pull from DB and put into the worker queue", result);
                                        }

                                        if (Log.log.isTraceEnabled())
                                        {
                                            Log.log.trace("SQL query in last DB gateway poll returned " + result + " of rows. ");
                                            // Log.log.trace("Filter Name: ["+table.getId()+"]  Starting Runbook: ["+table.getRunbook()+"] Polling Interval: ["+pollingtime+" ms]");
                                        }
                                    }
                                }
                                // finish poll and update filter.lastQuery
                                // with ObjectServer timestamp
                                dbFilter.setLastPollingTime(System.currentTimeMillis());
                            }
                            finally
                            {
                                if (batchUpdateStatement != null)
                                {
                                    long batchUpdateStartTmNSec = System.nanoTime();
                                    
                                    ExecutionMetrics.INSTANCE.registerEventStart("DB primary GW - update (bulk)");
                                    int[] resultCnts = batchUpdateStatement.executeBatch();
                                    ExecutionMetrics.INSTANCE.registerEventEnd("DB primary GW - update (bulk)", result);
                                    
                                    Log.log.debug("Batch update (including batch preparation) of server data took " + 
                                                  (((System.nanoTime() - batchUpdateStartTmNSec) + batchUpdatePrepareBatchTmNSec) / 1000) + " micro secs");
                                    
                                    Log.log.debug("Batch update result count size is " + resultCnts.length);
                                    
                                    if (Log.log.isDebugEnabled())
                                    {
                                        Log.log.debug("Batch Update Result:");
                                        
                                        for (int i = 0; i < resultCnts.length; i++)
                                        {
                                            Log.log.debug("resultCnts[" + (i+1) + "] = " +  resultCnts[i]);
                                        }
                                    }
                                    
                                    batchUpdateStatement.clearBatch();
                                    batchUpdateStatement.close();
                                }
                                
                                Log.log.info("Processing rate is " + (long)(1.0d / (((((System.nanoTime() - processBatchStartTmNSec) * 1.0d) / 1000000000.0) / result) * 1.0d))  + " records per second.");
                                
                                if (resultset != null)
                                {
                                    resultset.close();
                                }

                                if (statement != null)
                                {
                                    statement.close();
                                }
                                
                                if (connection != null)
                                {
                                    returnConnection(dbFilter.getPool(), connection);
                                }
                            }
                        }
                    }

                    // If there is no filter yet no need to keep spinning, wait for a while
                    if (orderedFilters.size() == 0)
                    {
                        Thread.sleep(pollInterval);
                    }
                }

                // process events collected by processFilter.
                //processFilterEvents(startTime);
            }
            catch (Exception e)
            {
                Log.log.error("Error when polling by DB Gateway...", e);
                Log.log.warn("Error: " + e.getMessage(), e);
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", 
                          "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + 
                          "due to " + e.getMessage());
                try
                {
                    Thread.sleep(pollInterval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
            finally
            {
                Log.log.trace("DB Gateway poller is sleeping");
            }
        }
    }

    private void convertDate(String lastValue, DBFilter table, String dbType) throws SQLException
    {
        if (!table.isLastValueEmpty())
        {
            table.setLastValue(lastValue);
            if (dbType.equals("ORACLE"))
            {
                table.setLastValue(convertTimeToOracleStr(lastValue));
            }
        }
    }

    private String convertTimeToOracleStr(String mystr)
    {
        String newStr = null;
        // 2011-12-1 9:39:16. 780000000
        String[] splitStr = mystr.split("-");
        String year = (splitStr[0]).substring(2);
        String month = splitStr[1];
        if (month.equals(String.valueOf(1)))
        {
            newStr = year.concat("-JAN-").concat(splitStr[2]);
        }
        else if (month.equals(String.valueOf(2)))
        {
            newStr = year.concat("-FEB-").concat(splitStr[2]);
        }
        else if (month.equals(String.valueOf(3)))
        {
            newStr = year.concat("-MAR-").concat(splitStr[2]);
        }
        else if (month.equals(String.valueOf(4)))
        {
            newStr = year.concat("-APR-").concat(splitStr[2]);
        }
        else if (month.equals(String.valueOf(5)))
        {
            newStr = year.concat("-MAY-").concat(splitStr[2]);
        }
        else if (month.equals(String.valueOf(6)))
        {
            newStr = year.concat("-JUN-").concat(splitStr[2]);
        }
        else if (month.equals(String.valueOf(7)))
        {
            newStr = year.concat("-JUL-").concat(splitStr[2]);
        }
        else if (month.equals(String.valueOf(8)))
        {
            newStr = year.concat("-AUG-").concat(splitStr[2]);
        }
        else if (month.equals(String.valueOf(9)))
        {
            newStr = year.concat("-SEP-").concat(splitStr[2]);
        }
        else if (month.equals(String.valueOf(10)))
        {
            newStr = year.concat("-OCT-").concat(splitStr[2]);
        }
        else if (month.equals(String.valueOf(11)))
        {
            newStr = year.concat("-NOV-").concat(splitStr[2]);
        }
        else
        {
            newStr = year.concat("-DEC-").concat(splitStr[2]);
        }
        return newStr;

    } // convertTimeToOracleStr

    @Override
    public void start()
    {
        Log.log.warn("Starting DB Listener");
        super.start();
    }

    public Connection getConnection(String poolName) throws Exception
    {
        Connection result = null;
        if(StringUtils.isNotBlank(poolName) && initedPools.containsKey(poolName))
        {
            result = getConnectionPool(poolName).borrowObject();
        }
        return result;
    } // getConnection

    private String getDatabaseType(String pool)
    {
        String dbtype = null;
        DBPool connPool = pools.get(pool);
        
        if (connPool == null)
        {
            throw new Error("DBPool " + pool + " not found.");
        }
        
        String drivertype = connPool.getDriverClass();
        if (drivertype.equalsIgnoreCase(Constants.MYSQLDRIVER))
        {
            dbtype = "MYSQL";
        }
        else if (drivertype.equalsIgnoreCase(Constants.ORACLEDRIVER))
        {
            dbtype = "ORACLE";
        }
        else if (drivertype.equalsIgnoreCase(Constants.DB2DRIVER))
        {
            dbtype = "DB2";
        }
        else if (drivertype.equalsIgnoreCase(Constants.MSSQLDRIVER))
        {
            dbtype = "MSSQL";
        }
        else if (drivertype.equalsIgnoreCase(Constants.POSTGRESQLDRIVER))
        {
            dbtype = "POSTGRESQL";
        }
        else
        {
            throw new Error("Invalid Driver Class for DBGateway.java");
        }
        return dbtype;

    } // getDatabseType
    
    @Override
    public String getLicenseCode()
    {
        return "DB";
    }

    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_DB;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MDatabase.class.getSimpleName();
    }

    @Override
    protected Class<MDatabase> getMessageHandlerClass()
    {
        return MDatabase.class;
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new DBFilter((String) params.get(DBFilter.ID), 
                        (String) params.get(DBFilter.ACTIVE), 
                        (String) params.get(DBFilter.ORDER), 
                        (String) params.get(DBFilter.INTERVAL), 
                        (String) params.get(DBFilter.EVENT_EVENTID), 
                        (String) params.get(DBFilter.RUNBOOK), 
                        (String) params.get(DBFilter.SCRIPT), 
                        (String) params.get(DBFilter.POOL), 
                        (String) params.get(DBFilter.SQL), 
                        (String) params.get(DBFilter.LASTVALUE), 
                        (String) params.get(DBFilter.LASTVALUECOLUMN), 
                        (String) params.get(DBFilter.LASTVALUEQUOTE), 
                        (String) params.get(DBFilter.PREFIX), 
                        (String) params.get(DBFilter.UPDATESQL));
    }

    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
    }

    public String getURL(String poolName)
    {
        return "jdbc:apache:commons:dbcp:" + poolName;
    } // getURL

    public GenericObjectPool<Connection> getConnectionPool(String poolName)
    {
        return initedPools.get(poolName);
    }// getConnectionPool

    public int getNumActiveConnections(String poolName)
    {
        return initedPools.get(poolName).getNumActive();
    } // getNumActiveConnections

    public int getNumMaxConnections(String poolName)
    {
        return initedPools.get(poolName).getMaxActive();
    } // getNumMaxConnections

    public void returnConnection(String poolName, Connection conn) throws Exception
    {
        //return DriverManager.getConnection("jdbc:apache:commons:dbcp:" + poolName);
        if(StringUtils.isNotBlank(poolName) && initedPools.containsKey(poolName) && conn != null)
        {   
            
            try {
            	initedPools.get(poolName).returnObject(conn);
            }catch(SQLFeatureNotSupportedException e) {
                // return the connection anyway upon this exception as the connection is still good
                // the driver just doesn't support the jdbc feature
                Log.log.error("Error when polling by DB Gateway...", e);
                Log.log.warn("Error: " + e.getMessage(), e);
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", 
                          "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + 
                          "due to " + e.getMessage());
                    initedPools.get(poolName).returnObject(conn);
            }
            
        }
    } // returnConnection

    public int update(String poolName, String sql) throws Exception
    {
        int result = 0;
        if(StringUtils.isNotBlank(poolName))
        {
            Connection conn = getConnection(poolName);
            if(conn == null)
            {
                throw new Exception("Invalid connection pool name: " + poolName);
            }
            if(StringUtils.isNotBlank(sql))
            {
                Statement statement = conn.createStatement();
                result = statement.executeUpdate(SQLUtils.getSafeSQL(sql));
            }
        }
        return result;
    } // update
    
    @Override
    public void updateServerData(Filter filter, Map content)
    {
        if (!batchUpdateSupported)
        {
            ExecutionMetrics.INSTANCE.registerEventStart("DB primary GW - update (single)");
            updateServerDataInternal(filter, content);
            try
            {
                ExecutionMetrics.INSTANCE.registerEventEnd("DB primary GW - update (single)", 1);
            }
            catch (ExecutionMetricsException eme)
            {
                Log.log.warn(eme.getMessage(), eme);
            }
        }
    }
    
    private void updateServerDataInternal(Filter filter, Map content)
    {
        DBFilter dbFilter = (DBFilter) filter;
        
        if (StringUtils.isBlank(dbFilter.getUpdateQuery()))
            return;
        
        Map<String, Map<String, String>> queryToUpdateFieldMap = filterIdToUpdateFieldMap.get(dbFilter.getId());
        
        if (queryToUpdateFieldMap.isEmpty())
            return;
        
        if (content.isEmpty())
            return;
        
        Map<String, String> updateFields = queryToUpdateFieldMap.values().iterator().next();

        String updateQuery = dbFilter.getUpdateQuery();
        
        for (Object key : content.keySet())
        {
            if (updateFields.containsKey("${" + ((String)key).toLowerCase() + "}"))
            {
                updateQuery = updateQuery.replaceAll(updateFields.get("${" + ((String)key).toLowerCase() + "}"), (String)content.get(key));
            }
        }
        
        Log.log.debug("Update statement:" + updateQuery);
        
        if (batchUpdateStatement != null)
        {
            try
            {
                try {
                	String safeUpdateQuery = SQLUtils.getSafeSQL(updateQuery);
					batchUpdateStatement.addBatch(ESAPI.validator().getValidInput(
																"Update Server Data Statement for adding to batch", 
																safeUpdateQuery, 
																ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR, 
																ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH, 
																false, false));
				} catch (Exception e) {
					throw new SQLException(e);
				}
            }
            catch (SQLException e)
            {
                Log.log.error("Error when adding update to batch by DB Gateway...", e);
                Log.log.warn("Error: " + e.getMessage(), e);
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Adding Update To Batch Failed", 
                          "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Adding Update To Batch Failed " + 
                          "due to " + e.getMessage());
            }
        }
        else
        {
            Connection connection = null;
            Statement statement = null;
            try
            {
                try
                {
                    connection = getConnection(dbFilter.getPool());
                    
                    statement = connection.createStatement();
                    
                    int recordCount = 0;
                    
                    try
                    {
                        String safeUpdateQuery = SQLUtils.getSafeSQL(updateQuery, getDatabaseType(dbFilter.getPool()));
                        recordCount = statement.executeUpdate(
                        							ESAPI.validator().getValidInput(
                        										"Update Server Data Statement", 
                        										safeUpdateQuery, 
                        										ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR, 
                        										ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH, 
                        										false, false));
                    }
                    catch (SQLException ex)
                    {
                        Log.log.error("Error " + ex.getMessage() + " in getting update query for filter " + dbFilter.getId());
                        sendAlert(ALERT_SEVERITY_CRITICAL, true, "Filter:" + dbFilter.getId() + ":Update SQL Error", ex.getMessage());
                    }
                    catch (Throwable t)
                    {
                        Log.log.error("Error " + t.getMessage() + " in getting update query for filter " + dbFilter.getId());
                        sendAlert(ALERT_SEVERITY_CRITICAL, true, "Filter:" + dbFilter.getId() + ":Update Error", t.getMessage());
                    }
        
                    if (recordCount > 0)
                    {
                        Log.log.debug("Updated " + recordCount + " records");
                    }
                }
                catch (Exception e)
                {
                    Log.log.error("Error when updating by DB Gateway...", e);
                    Log.log.warn("Error: " + e.getMessage(), e);
                    Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Update Failed", 
                              "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Update Failed " + 
                              "due to " + e.getMessage());
                }
                finally
                {
                    if (statement != null)
                    {
                        statement.close();
                    }
                    
                    if (connection != null)
                    {
                        try
                        {
                            returnConnection(dbFilter.getPool(), connection);
                        }
                        catch (Exception e)
                        {
                            Log.log.error("Failed to return connection back to pool " + dbFilter.getPool() + 
                                          " due to " + e.getMessage(), e);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error when updating by DB Gateway...", e);
                Log.log.warn("Error: " + e.getMessage(), e);
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Update Failed", 
                          "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Update Failed " + 
                          "due to " + e.getMessage());
            }
        }
    }
}
