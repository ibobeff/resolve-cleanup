package com.resolve.gateway.resolvegateway.msg;

import com.resolve.gateway.resolvegateway.GatewayFilter;

public class MSGGatewayFilter extends GatewayFilter {

    public static final String USER = "USER";

    public static final String PASS = "PASS";
    
    public static final String SSL = "SSL";

    private String user;

    private String pass;
    
    private String ssl;

    public MSGGatewayFilter(String id, String active, String order, String eventEventId, String runbook, String script) {
         
        super(id, active, order, null, eventEventId, runbook, script);
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getPass()
    {
        return pass;
    }

    public void setPass(String pass)
    {
        this.pass = pass;
    }
    
    public String getSsl()
    {
        return ssl;
    }

    public void setSsl(String ssl)
    {
        this.ssl = ssl;
    }

}