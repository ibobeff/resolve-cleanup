/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.servicenow;

import java.util.HashMap;
import com.resolve.rsremote.ConfigReceiveServiceNow;

/**
 * This class acts as a service to manage incident.
 * 
 * @author bipuldutta
 * 
 */
@Deprecated
public class IncidentObjectService extends AbstractObjectService
{
    /*
    static String OBJECT_IDENTITY = "incident";

    private static String IDENTITY_PROPERTY = "NUMBER";
    */
    public IncidentObjectService(ConfigReceiveServiceNow configurations) throws Exception
    {
        super(configurations);
    }
    /*
    //@Override
    public String getIdPropertyName()
    {
        return IDENTITY_PROPERTY;
    }

    //@Override
    public String getIdentity()
    {
        return OBJECT_IDENTITY;
    }

    @Override
    protected String getSoapEnvelope(String objectType, String query, String lastChangeDate)
    {
        StringBuilder soapEnvelope = new StringBuilder("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:get=\"http://www.service-now.com/GetData\">");
        soapEnvelope.append("<soapenv:Header/>");
        soapEnvelope.append("<soapenv:Body>");
        soapEnvelope.append("<get:execute>");
        soapEnvelope.append("<query>" + query + "</query>");
        soapEnvelope.append("<object>" + this.getIdentity() + "</object>");
        soapEnvelope.append("<sys_created_on>" + lastChangeDate + " </sys_created_on>");
        soapEnvelope.append("</get:execute>");
        soapEnvelope.append("</soapenv:Body>");
        soapEnvelope.append("</soapenv:Envelope>");

        return soapEnvelope.toString();
    }*/

    @Override
    public String getLatestIncidentNumber()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String importSetApi(String stagingTableName, HashMap<String, String> params, String username, String p_assword) throws Exception
    {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * operation could be "insert", "update", "deleteRecord"
     */
    /*
    @Override
    protected String getSoapEnvelope(String sysId, String operation, Map<String, String> params)
    {
        StringBuilder soapEnvelope = new StringBuilder("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:inc=\"http://www.service-now.com/incident\">");
        // Is the ServiceNow makes WS-Security a requirement we'll inject
        // WS-Security headers in the Header element.
        soapEnvelope.append("<soapenv:Header/>");
        soapEnvelope.append("<soapenv:Body>");
        soapEnvelope.append("<inc:" + operation + ">");
        if (StringUtils.isNotEmpty(sysId))
        {
            soapEnvelope.append("<sys_id>" + sysId + "</sys_id>");
        }
        for (String key : params.keySet())
        {
            soapEnvelope.append("<" + key + ">" + params.get(key) + "</" + key + ">");
        }

        soapEnvelope.append("</inc:" + operation + ">");
        soapEnvelope.append("</soapenv:Body>");
        soapEnvelope.append("</soapenv:Envelope>");

        return soapEnvelope.toString();
    }*/
}
