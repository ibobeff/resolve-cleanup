/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.gateway;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.remedyx.RemedyxFilter;
import com.resolve.gateway.remedyx.RemedyxForm;
import com.resolve.gateway.remedyx.RemedyxGateway;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class is used by the Message Listener and invokes it methods. Messages
 * destined for the gateways set this class name in the message header.
 *
 */
public class MRemedyx extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MRemedyx.setFilters";
    private static final String RSCONTROL_SET_FORMS = "MRemedyx.setForms";

    private static final RemedyxGateway instance = RemedyxGateway.getInstance();

    public MRemedyx()
    {
        super.instance = instance;
    }

    @Override
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    @Override
    protected void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        RemedyxFilter remedyxFilter = (RemedyxFilter) filter;
        filterMap.put(RemedyxFilter.QUERY, remedyxFilter.getQuery());
        filterMap.put(RemedyxFilter.FORM_NAME, remedyxFilter.getFormName());
        filterMap.put(RemedyxFilter.LAST_VALUE_FIELD, remedyxFilter.getLastValueField());
        filterMap.put(RemedyxFilter.LIMIT, String.valueOf(remedyxFilter.getLimit()));
    }

    /**
     * Sets a form in the current {@link RememdyxGateway} instance, using the
     * params {@link Map} to define what form type and its subsequent
     * parameters.
     * 
     * @param params
     *            The Remedyx Form parameters. The params {@link Map} key values
     *            include: - RemedyxForm.NAME : String - RemedyxForm.FIELD_LIST
     *            : String
     */
    public void setForm(Map params)
    {
        instance.setForm(params);

        // save config
        MainBase.main.exitSaveConfig();

    } // setForm

    /**
     * Sends the forms from the current instance of the Remedyx Gateway to
     * MainBase's ESB as an internal message.
     * 
     * @param params
     * 
     */
    public void getForms(Map params)
    {
        if (instance.isActive())
        {
            Map<String, String> content = new HashMap<String, String>();

            for (String key : instance.getForms().keySet())
            {
                RemedyxForm remedyForm = instance.getForms().get(key);
                Map<String, String> formMap = new HashMap<String, String>();
                formMap.put(RemedyxForm.NAME, remedyForm.getName());
                formMap.put(RemedyxForm.FIELD_LIST, remedyForm.getFieldList());
                formMap.put(RemedyxForm.QUEUE, instance.getQueueName());
                content.put(remedyForm.getName(), StringUtils.remove(StringUtils.mapToString(formMap), "null"));
            }

            // send content to rscontrol
            if (isUpdateRSControl)
            {
                // this is important as RSControl need this
                content.put("QUEUE", instance.getQueueName());

                if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROL, RSCONTROL_SET_FORMS, content) == false)
                {
                    Log.log.warn("Failed to send Remedy forms to RSCONTROL");
                }
            }
        }
    } // getPools

    /**
     * Clear all forms in the current instance of {@link RemedyxGateway}. If
     * paramList isn't null, it will iterate through all of its form {@link Map}
     * s and add them to the {@link RemedyxGateway}
     * 
     * @param paramList
     *            a {@link List} of RemedyxGateway form {@link Map}s
     * 
     */
    @FilterCompanionModel(modelName = "RemedyxForm")
    public void clearAndSetForms(List<Map<String, Object>> paramList)
    {
        // clear and set filters
        instance.clearAndSetForms(paramList);

        // send new filters
        getForms(null);

        // save config
        MainBase.main.exitSaveConfig();

    } // clearAndSetForms

    /**
     * This method is invoked once filter synchronization message from RSCONTROL
     * is received.
     * 
     * @param paramList
     *            A {@link Map} of form and filter {@link List}s to synchronize in this
     *            {@link RemedyxGateway} instance.
     */
    public void synchronizeGateway(Map<String, List<Map<String, Object>>> paramList)
    {
        if (paramList != null)
        {
            //synchronize all the common data like filter, name properties, routing schemas
            super.synchronizeGateway(paramList);
            
            //super class method sets them to be true, however the last step may fail
            isUpdateRSControl = false;
            instance.setSyncDone(false);
            
            List<Map<String, Object>> forms = (List<Map<String, Object>>) paramList.get("FORMS");
            int number = 0;
            if (forms != null)
            {
                number = forms.size();
            }
            Log.log.debug("NUMBER of forms " + number);
            clearAndSetForms(forms);
            
            isUpdateRSControl = true;
            // tell the gateway that we have received RSControl sync message.
            instance.setSyncDone(true);
        }
    }
}
