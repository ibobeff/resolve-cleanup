package com.resolve.gateway;

import java.util.List;
import java.util.Map;

import com.resolve.gateway.http.HttpFilter;
import com.resolve.gateway.http.HttpGateway;
import com.resolve.util.Log;

public class MHttp extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MHttp.setFilters";

    private static final HttpGateway instance = HttpGateway.getInstance();

    public MHttp()
    {
        super.instance = instance;
    }

    @Override
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    @Override
    protected void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        if (instance.isActive())
        {
            HttpFilter httpFilter = (HttpFilter) filter;
            filterMap.put(HttpFilter.URI, httpFilter.getUri());
            filterMap.put(HttpFilter.PORT, "" + httpFilter.getPort());
            filterMap.put(HttpFilter.SSL, "" + httpFilter.isSsl());
            filterMap.put(HttpFilter.BLOCKING, "" + httpFilter.getBlocking());
            filterMap.put(HttpFilter.ALLOWED_IP, httpFilter.getAllowedIP());
            filterMap.put(HttpFilter.BASIC_AUTH_PW, httpFilter.getHttpAuthBasicPassword());
            filterMap.put(HttpFilter.BASIC_AUTH_UN, httpFilter.getHttpAuthBasicUsername());
            
            filterMap.put(HttpFilter.CONTENT_TYPE, httpFilter.getContentType());
            filterMap.put(HttpFilter.RESPONSE_FILTER, httpFilter.getResponsefilter());

        }
    }
    
    public void clearAndSetFilters(List<Map<String, Object>> paramList)
    {
        Log.log.debug("MHttp Received clearAndSetFilters" + (paramList != null ? " " + paramList.size() + ", [" + paramList + "]": ""));
        super.clearAndSetFilters(paramList);
    }
} // MHttp