package com.resolve.gateway.util.dcs;

import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;

import com.resolve.util.Log;

public abstract class BaseDataCollectionLogger<T>
{

    private static final String TYPE = "log-event-type";

    public void log(T data, String... properties)
    {
        if(getMessageType() == null) {
            return;
        }
        
        T transformedData = null;
        try
        {
            transformedData = transformData(data);
        }
        catch (Exception ex)
        {
            Log.log.error("Failed to serialize data, reason: {}", ex);
            return;
        }

        LoggingEvent loggingEvent = new LoggingEvent(null, Log.dcs, Level.TRACE, transformedData, null);
        setProperties(loggingEvent, properties);
        Log.dcs.callAppenders(loggingEvent);
    };

    private void setProperties(LoggingEvent event, String... properties)
    {
        Map<String, String> propertiesMap = getPropertiesMap(properties);
        propertiesMap.put(TYPE, getMessageType());
        for (Map.Entry<String, String> property : propertiesMap.entrySet())
        {
            event.setProperty(property.getKey(), property.getValue());
        }
    }

    protected abstract Map<String, String> getPropertiesMap(String... properties);

    /**
     * This is corresponding to a type inside a template in http-log-appender
     * 
     * @return
     */
    protected abstract String getMessageType();

    protected T transformData(T data) throws Exception
    {
        return data;
    }

}