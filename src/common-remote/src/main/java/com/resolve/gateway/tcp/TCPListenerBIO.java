/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This is a blocking version of the listener. Does not scale well with load. Just for reference.
 *
 */
class TCPListenerBIO implements Runnable
{
    private final Map<String, TCPFilter> filters;

    private volatile RequestProcessor processor; 
    private volatile ServerSocket listener;
    private volatile boolean running = false;
    
    private Integer port = null;
    private Integer timeout = 0; //not used for now
    
    private TCPGateway instance = TCPGateway.getInstance();

    TCPListenerBIO()
    {
        timeout = instance.getConfig().getTimeout();
        filters = new HashMap<String, TCPFilter>();
        processor = new RequestProcessor(instance, filters);
    }
    
    public Map<String, TCPFilter> getFilters()
    {
        return filters;
    }

    void addFilter(TCPFilter tcpFilter)
    {
        if(tcpFilter != null)
        {
            filters.put(tcpFilter.getId(), tcpFilter);
            processor.addFilter(tcpFilter);
            if(port == null)
            {
                port = tcpFilter.getPort();
            }
        }
    }

    void removeFilter(TCPFilter tcpFilter)
    {
        if(tcpFilter != null && filters.containsKey(tcpFilter.getId()))
        {
            filters.remove(tcpFilter.getId());
            processor.removeFilter(tcpFilter);
        }
    }

    Integer getPort()
    {
        return port;
    }

    void stop()
    {
        Log.log.info("Stopping TCP listener port: " + getPort());
        running = false;
        if(listener != null && !listener.isClosed())
        {
            try
            {
                listener.close();
                if(listener.isClosed())
                {
                    Log.log.debug("Closed TCP listener port: " + getPort());
                }
            }
            catch (IOException e)
            {
                Log.log.info(e.getMessage(), e);
            }
        }
    }

    @Override
    public void run()
    {
        Log.log.debug("Initializing TCP listener on port: " + getPort());
        running = true;
        
        try
        {
            listener = new ServerSocket(getPort());
            processor = new RequestProcessor(instance, filters);
            while (running) 
            {
                if(listener != null && !listener.isClosed())
                {
                    Socket socket = listener.accept();
                    if(Log.log.isDebugEnabled())
                    {
                        Log.log.debug("Received communication from IP: " + socket.getInetAddress());
                    }
                    if(timeout > 0)
                    {
                        socket.setSoTimeout(timeout);
                    }
                    processor.setSocket(socket);
                    processor.start();
                }
            }
        }
        catch (SocketException e)
        {
            Log.log.info(e.getMessage(), e);
        }
        catch (IOException e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    private static class RequestProcessor extends Thread
    {
        private volatile Thread blinker;

        private final TCPGateway instance;
        //private Map<String, TCPFilter> filters = new HashMap<String, TCPFilter>();
        private Map<String, TCPFilter> separators = new HashMap<String, TCPFilter>();
        private Socket socket;
        
        public RequestProcessor(final TCPGateway instance, final Map<String, TCPFilter> filters)
        {
            this.instance = instance;
            
            //this.filters = filters;
            if(filters != null)
            {
                for(TCPFilter tcpFilter : filters.values())
                {
                    TCPFilter existingFilter = separators.get(tcpFilter.getBeginSeparator());
                    if(existingFilter != null && !existingFilter.getId().equals(tcpFilter.getId()) && existingFilter.getPort().equals(tcpFilter.getPort()))
                    {
                        Log.log.warn("Duplicate begin separator for filter in the same port. Following filter will be ignored: \n" + tcpFilter.toString());
                    }
                    else
                    {
                        separators.put(tcpFilter.getBeginSeparator(), tcpFilter);
                    }
                }
            }
        }

        public void addFilter(TCPFilter tcpFilter)
        {
            if(tcpFilter != null)
            {
                separators.put(tcpFilter.getBeginSeparator(), tcpFilter);
                if(Log.log.isDebugEnabled())
                {
                    Log.log.debug("Filter added: " + tcpFilter.toString());
                    Log.log.debug("Total filter added: " + separators.size());
                }
            }
        }
        
        public void removeFilter(TCPFilter tcpFilter)
        {
            if(tcpFilter != null)
            {
                separators.remove(tcpFilter.getBeginSeparator());
                if(Log.log.isDebugEnabled())
                {
                    Log.log.debug("Filter removed: " + tcpFilter.toString());
                    Log.log.debug("Total filter after remove: " + separators.size());
                }
            }
        }
        
        public void setSocket(Socket socket)
        {
            this.socket = socket;
        }

        @Override
        public void start()
        {
            blinker = new Thread(this);
            blinker.start();
        }
        
        @Override
        public void run()
        {
            Thread thisThread = Thread.currentThread();
            try
            {
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                StringBuilder content = new StringBuilder();
                // Get messages from the client, line by line until the end of stream is found or client closes the connection
                boolean hasStarted = false;
                TCPFilter tcpFilter = null;
                while (blinker == thisThread)
                {
                    String input = null;
                    try
                    {
                        input = in.readLine();
                    }
                    catch(SocketTimeoutException e)
                    {
                        Log.log.error("Socket time out, most likely client abandoned the connection. " + e.getMessage());
                    }
                    if(Log.log.isDebugEnabled())
                    {
                        Log.log.debug("TCP socket read: " + input);
                    }
                    if(input == null)
                    {
                        break;
                    }
                    else
                    {
                        if(!hasStarted)
                        {
                            //check if any one of the filter matches with this line
                            tcpFilter = getMatchedFilter(input);
                            if(tcpFilter != null)
                            {
                                hasStarted = true;
                                if(tcpFilter.getIncludeBeginSeparator())
                                {
                                    input = input.substring(input.indexOf(tcpFilter.getBeginSeparator()));
                                }
                                else
                                {
                                    input = input.substring(input.indexOf(tcpFilter.getBeginSeparator()) + tcpFilter.getBeginSeparator().length());
                                }
                            }
                        }

                        if(hasStarted)
                        {
                            if (input.equals(tcpFilter.getEndSeparator()) || input.endsWith(tcpFilter.getEndSeparator()) || input.contains(tcpFilter.getEndSeparator()))
                            {
                                if(tcpFilter.getIncludeEndSeparator())
                                {
                                    input = input.substring(0, input.indexOf(tcpFilter.getEndSeparator()) + tcpFilter.getEndSeparator().length());
                                }
                                else
                                {
                                    input = input.substring(0, input.indexOf(tcpFilter.getEndSeparator()));
                                }
                                content.append(input);
                                if(tcpFilter != null)
                                {
                                    if(Log.log.isDebugEnabled())
                                    {
                                        Log.log.debug("Dev complete TCP Socket Content: " + content.toString());
                                    }
                                    instance.processData(tcpFilter.getId(), content.toString());
                                }
                                content.setLength(0);
                                hasStarted = false;
                                //break;
                            }
                            else
                            {
                                content.append(input);
                            }
                        }
                    }
                }
                
                out.println("TCP socket going to be closed.\n");
                out.flush();
            }
            catch (IOException e)
            {
                Log.log.error(e.getMessage(), e);
            }
            finally
            {
                //we are done listening so we can stop the thread 
                blinker = null;
                try
                {
                    if(socket != null && socket.isConnected())
                    {
                        socket.close();
                    }
                }
                catch (IOException e)
                {
                    Log.log.warn("Couldn't close a socket, what's going on?");
                }
            }
        }

        /**
         * This method matches the input with one of the filters' begin separator
         * and returns it. 
         * 
         * @param input
         * @return
         */
        private TCPFilter getMatchedFilter(String input)
        {
            TCPFilter result = null;
            if(StringUtils.isNotBlank(input))
            {
                for(String beginSeparator : separators.keySet())
                {
                    TCPFilter tmpFilter = separators.get(beginSeparator);
                    if(Log.log.isDebugEnabled())
                    {
                        Log.log.debug("Currently deployed filter: " + tmpFilter.toString());
                    }
                    
                    //the input could be just one line for example <TestData attr="1"><Data1>Hello</Data1></TestData>
                    //where begin separator is <TestData and end separator is /TestData>
                    String trimmedInput = input.trim();
                    if(trimmedInput.equals(beginSeparator) || trimmedInput.startsWith(beginSeparator) || trimmedInput.contains(beginSeparator))
                    {
                        result = separators.get(beginSeparator);
                        break;
                    }
                }
            }
            return result;
        }
    }
}
