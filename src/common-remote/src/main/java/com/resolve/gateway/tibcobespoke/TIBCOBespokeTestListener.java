package com.resolve.gateway.tibcobespoke;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class TIBCOBespokeTestListener extends MainBase
{
    TIBCOBespokeRabbitCommunicator processComm;
    int recieved = 0;
    int responsesSent = 0;
    
    public TIBCOBespokeTestListener() throws Exception
    {   
        
        int rabbitCorePoolSize = 100;
        int rabbitMaxPoolSize = 500;
        int rabbitKeepAliveTime = 120;
        int rabbitBlockingQueueSize = 50;
        
        try
        {
            Log.initAuth();
            Log.init("C:\\project\\release\\resolve-5.2.1-20151203\\resolve\\dist\\rsremote\\config\\log.cfg", "testLogger");
            processComm = new TIBCOBespokeRabbitCommunicator("127.0.0.1:4004", rabbitCorePoolSize, rabbitMaxPoolSize, rabbitKeepAliveTime, rabbitBlockingQueueSize, "TLSv1.2");
            processComm.subscribeAndRespond("TestListenerQueue#50000", new TIBCOBespokeRabbitWorker() {
                @Override
                public String doWork(String message)
                {
                    Log.log.trace(message);
                    
                    try
                    {                       
                        responsesSent++;
                    }
                    catch(Exception e)
                    {
                        Log.log.error("TIBCO: Issue in heartbeat thread", e);
                    }
                    System.out.println("ResponsesSent: "+ responsesSent + ", Thread: "+Thread.currentThread().getId());
                    return "ResponseMessage: " + responsesSent;
                }
            });
        }
        catch (Exception e)
        {
            System.err.println(ExceptionUtils.getStackTrace(e));
        }
    }
    
    //id = timestamp#uuid#processId
    public void respond(String replyMessageStoreId, String respondPayload) throws Exception
    {
        Log.log.trace("Replying to message: " + replyMessageStoreId );
        if (StringUtils.isEmpty(replyMessageStoreId))
        {
            throw new IllegalArgumentException("Could not repond to message. Message ID was empty or null");
        }
        if (StringUtils.isEmpty(respondPayload))
        {
            throw new IllegalArgumentException("Could not respond to message. Message to send was empty or null");
        }       
        
        //format expireTime-storeid-processid
        String[] idSplit = replyMessageStoreId.split("#");
        if(idSplit.length != 3)
        {
            throw new RuntimeException("Invalid TIB_MESSAGE_ID, expect format of 'expiretime#messageid#processid' given: "+replyMessageStoreId);
        }
        else
        {
            Long expireTime = Long.parseLong(idSplit[0]);
            if(System.currentTimeMillis() > expireTime)
            {
                throw new RuntimeException("The message has expired, increase filters ReplyTimeout to prevent this. Maximum 10 minutes on the resolve side. Most systems will not store greater than 2.");   
            }
            else
            {
                MessageEnvelope msgEnv = new MessageEnvelope(respondPayload);
                //The id Expected on the rstib side is "expireTimestamp#storeid" hence idSplit[0]#idSplit[1]
                // 
                msgEnv.setStoreId(idSplit[0]+"#"+idSplit[1]);        
                processComm.publish("TIBCO_RESPONDSUB#50000", msgEnv.getEvelopeString()); //Last one is a process id                
            }
        }
    }
    
    public void subscribe(String subscriptionTopic)
    {
        try
        {
            processComm.subscribe(subscriptionTopic, new TIBCOBespokeRabbitWorker() {
                @Override
                public String doWork(String message)
                {
                    try
                    {
                        System.out.println("Message Recieved: "+message);
                    }
                    catch(Exception e)
                    {
                        Log.log.error("TIBCORabbit: Issue processing data in queue", e);                            
                    }
                    
                    return "";
                }
            });
        }
        catch (Exception e)
        {
            System.err.println(ExceptionUtils.getStackTrace(e));
        }
    }
    
//    public static void main(String args[]) throws Exception
//    {        
//        new TIBCOBespokeTestListener();
//    }

    @Override
    public void init(String serviceName) throws Exception
    {
        // TODO Auto-generated method stub
        
    }

    @Override
    protected void exit(String[] argv)
    {
        // TODO Auto-generated method stub
        
    }
}
