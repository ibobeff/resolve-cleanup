/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.remedy;

import java.util.ArrayList;
import java.util.List;

public class RemedyForm
{
    public String name;

    public String runbook;

    public String fieldList;

    public String restriction;

    public List<RemedyFilter> filters = new ArrayList<RemedyFilter>();

}
