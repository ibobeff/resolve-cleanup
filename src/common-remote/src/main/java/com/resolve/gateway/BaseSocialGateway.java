/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.HashMap;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.util.ConfigMap;
import com.resolve.util.Constants;

/**
 * This class provides a partial implementation for social gateway sub classes
 * those takes part as a social gateway (currently only Email, EWS, and XMPP).
 */
public abstract class BaseSocialGateway extends BaseClusteredGateway
{
    protected BaseSocialGateway(ConfigMap config)
    {
        super(config);
    }

    @Override
    public Map<String, String> getSyncRequestParameters()
    {
        //get common params from super class
        Map<String, String> params = super.getSyncRequestParameters();
        //For social gateways we will need some extra addresses from RSControl 
        //during synchronization. Only primary or secondary could become a social poster.
        if(isPrimary() || isSecondary())
        {
            params.put("SOCIAL_POSTER", Boolean.TRUE.toString());
        }
        return params;
    }

    /**
     * This method sends ESB message to the RSView for posting blog on behalf of
     * the user.
     *
     * @param from
     *            is the email address of a user.
     * @param body
     */
    protected void postToSocial(String to, String from, String subject, String body)
    {
        postToSocial(to, from, subject, body, new HashMap<String, byte[]>());
    }

    /**
     * This method sends ESB message to the RSView for posting blog on behalf of
     * the user.
     *
     * @param from
     *            is the email address of a user.
     * @param body
     */
    protected void postToSocial(String to, String from, String subject, String body, Map<String, byte[]> attachments)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("TO", to);
        params.put("FROM", from);
        params.put("SUBJECT", subject);
        params.put("CONTENT", body);
        params.put("ATTACHMENTS", attachments);
        params.put("GATEWAY_EVENT_TYPE", getGatewayEventType());

        MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSVIEW, "MSocial.postToSocialComponent", params);
    }
}
