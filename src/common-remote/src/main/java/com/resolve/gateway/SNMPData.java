/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

/**
 * This interface defines the supported SNMP data types.
 */
public class SNMPData
{

    public static interface SNMPTrapOID
    {
        String ColdStart = "1.3.6.1.6.3.1.1.5.1";
        String WarmStart = "1.3.6.1.6.3.1.1.5.2";
        String LinkDown = "1.3.6.1.6.3.1.1.5.2";
        String LinkUp = "1.3.6.1.6.3.1.1.5.2";
        String AuthenticationFailure = "1.3.6.1.6.3.1.1.5.2";
        String EGPNeighborLoss = "1.3.6.1.6.3.1.1.5.6";
    }

    /**
     * Defines the supported data types.
     */
    public static interface Type
    {
        String BitString = "BitString";
        String Integer32 = "Integer32";
        String OctetString = "OctetString";
        String OID = "OID";
        String IpAddress = "IpAddress";
        String Counter32 = "Counter32";
        String Counter64 = "Counter64";
        String Gauge32 = "Gauge32";
        String Null = "Null";
        String Unsigned32 = "Unsigned32";
        String TimeTicks = "TimeTicks";
        String Opaque = "Opaque";
    };

    private String oid;
    private String value;
    private String dataType;

    public SNMPData(String oid, String value, String dataType)
    {
        super();
        this.oid = oid;
        this.value = value;
        this.dataType = dataType;
    }

    public String getOid()
    {
        return oid;
    }

    public void setOid(String oid)
    {
        this.oid = oid;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getDataType()
    {
        return dataType;
    }

    public void setDataType(String dataType)
    {
        this.dataType = dataType;
    }
}
