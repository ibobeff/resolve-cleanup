/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.gateway.ews;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.naming.AuthenticationException;

import org.antlr.runtime.RecognitionException;

import com.resolve.gateway.BaseSocialGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MEWS;
import com.resolve.query.QueryParser.MismatchedParensException;
import com.resolve.query.QueryToken;
import com.resolve.query.ResolveQuery;
import com.resolve.query.translator.EWSQueryTranslator;
import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveEWS;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.PropertySet;
import microsoft.exchange.webservices.data.core.WebProxy;
import microsoft.exchange.webservices.data.core.enumeration.property.BasePropertySet;
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.enumeration.search.FolderTraversal;
import microsoft.exchange.webservices.data.core.enumeration.search.LogicalOperator;
import microsoft.exchange.webservices.data.core.enumeration.search.SortDirection;
import microsoft.exchange.webservices.data.core.enumeration.service.ConflictResolutionMode;
import microsoft.exchange.webservices.data.core.enumeration.service.MessageDisposition;
import microsoft.exchange.webservices.data.core.enumeration.service.ServiceResult;
import microsoft.exchange.webservices.data.core.response.ServiceResponseCollection;
import microsoft.exchange.webservices.data.core.response.UpdateItemResponse;
import microsoft.exchange.webservices.data.core.service.folder.Folder;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.core.service.response.ResponseMessage;
import microsoft.exchange.webservices.data.core.service.schema.EmailMessageSchema;
import microsoft.exchange.webservices.data.core.service.schema.ItemSchema;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.credential.WebProxyCredentials;
import microsoft.exchange.webservices.data.property.complex.Attachment;
import microsoft.exchange.webservices.data.property.complex.AttachmentCollection;
import microsoft.exchange.webservices.data.property.complex.EmailAddress;
import microsoft.exchange.webservices.data.property.complex.EmailAddressCollection;
import microsoft.exchange.webservices.data.property.complex.FileAttachment;
import microsoft.exchange.webservices.data.property.complex.FolderId;
import microsoft.exchange.webservices.data.property.complex.ItemAttachment;
import microsoft.exchange.webservices.data.property.complex.ItemId;
import microsoft.exchange.webservices.data.property.complex.MessageBody;
import microsoft.exchange.webservices.data.search.FindFoldersResults;
import microsoft.exchange.webservices.data.search.FindItemsResults;
import microsoft.exchange.webservices.data.search.FolderView;
import microsoft.exchange.webservices.data.search.ItemView;
import microsoft.exchange.webservices.data.search.filter.SearchFilter;


public class EWSGateway extends BaseSocialGateway
{
    // Singleton
    private static volatile EWSGateway instance = null;

    // This will be set in initialize() with the proper queue name from the
    // configuration.
    private String queue = "EWS";

    private static final String EMAILID = "EMAILID";
    private static final String SENDERADDRESS = "SENDERADDRESS";
    private static final String SENDERNAME = "SENDERNAME";
    private static final String LAST_RECEIVED_TIME = "LAST_RECEIVED_TIME";
    private static final String INBOX = "INBOX";
    private static final String RECIPIENTS = "RECIPIENTS";
    private static final String CCRECIPIENTS = "CCRECIPIENTS";
    private static final String EMAILTRIGGER = "EMAILADDRESS";
    private static final Map<String, String> defaultEmailAttributes = new HashMap<String, String>();
    private final ResolveQuery resolveQuery;

    private final ConfigReceiveEWS configReceiveEWS;

    private static final String resolveTempDir = MainBase.main.configGeneral.home + "/tmp";

    // private ExchangeService exchangeService;
    private Folder inbox;
    private ItemView itemView;
    private final int maxEmails = 1000; // get maximum 1000 emails at a time.
    private boolean isSocialPost;

    private ConcurrentHashMap<String, ExchangeService> ewsServices = new ConcurrentHashMap<String, ExchangeService>();
    private ConcurrentHashMap<String, EWSAddress> ewsAddresses = new ConcurrentHashMap<String, EWSAddress>();
    private ConcurrentHashMap<String,ConcurrentHashMap<String, Long>> addressLastReceivedTime = new ConcurrentHashMap<String, ConcurrentHashMap<String, Long>>();
    static boolean isProxyEnabled =false;
    static String proxyUserName = "";
    static String proxyPassword = "";
    static String proxyHost = "";
    static int proxyPort = -1;
    static String proxyDomain = "";
    static WebProxy proxy=null;
    
    static
    {
        defaultEmailAttributes.put("torecipients", "string");
        defaultEmailAttributes.put("ccrecipients", "string");
        defaultEmailAttributes.put("recipients", "string");
        defaultEmailAttributes.put("subject", "string");
        defaultEmailAttributes.put("body", "string");
        defaultEmailAttributes.put("isread", "string");
    }

    public static EWSGateway getInstance(ConfigReceiveEWS config)
    {
        if (instance == null)
        {
            instance = new EWSGateway(config);
        }
        isProxyEnabled = config.isProxyEnabled();
       
        if (isProxyEnabled)
        {
            proxyUserName = config.getProxyUsername();
            proxyPassword = config.getProxyPassword();
            proxyHost = config.getProxyHost();
            proxyPort = config.getProxyPort();
            proxyDomain = config.getProxyDomain();
            WebProxyCredentials proxyCreds = new WebProxyCredentials(proxyUserName, proxyPassword, proxyDomain);
            proxy = new WebProxy(proxyHost, proxyPort, proxyCreds);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     * 
     * @return
     */
    public static EWSGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("EWSGateway Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    /**
     * Private constructor.
     * 
     * @param config
     */
    private EWSGateway(ConfigReceiveEWS config)
    {
        // Important, call super here.
        super(config);
        this.configReceiveEWS = config;
        queue = config.getQueue();
        resolveQuery = new ResolveQuery(new EWSQueryTranslator());
    }

    @Override
    public String getLicenseCode()
    {
        return "EWS";
    }

    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_EWS;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MEWS.class.getSimpleName();
    }

    @Override
    protected Class<MEWS> getMessageHandlerClass()
    {
        return MEWS.class;
    }

    public boolean isSocialPoster()
    {
        return isSocialPost;
    }

    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    /**
     * Initializes the specific gateway resources.
     */
    @Override
    protected void initialize()
    {
        ConfigReceiveEWS config = (ConfigReceiveEWS) configurations;

        queue = config.getQueue().toUpperCase();
        if (config.isActive())
        {

            String url = configReceiveEWS.getUrl();
            String username = configReceiveEWS.getUsername();
            String password = configReceiveEWS.getPassword();

           
            ExchangeService exchangeService = new ExchangeService();
            if (isProxyEnabled)
            {
                exchangeService.setWebProxy(proxy);
            }

            ExchangeCredentials credentials = new WebCredentials(username, password);
            exchangeService.setCredentials(credentials);
            this.isSocialPost = config.isSocialpost();
            this.gatewayConfigDir = "/config/ews/";

            try
            {
                exchangeService.setUrl(new URI(url));
                ewsServices.put(username, exchangeService);
                setAddressLastReceivedTime(username);
                itemView = new ItemView(maxEmails);
                itemView.getOrderBy().add(EmailMessageSchema.DateTimeReceived, SortDirection.Ascending);
                // now instantiate all the addresses
                for (String ewsAddress : ewsAddresses.keySet())
                {
                	EWSAddress address = ewsAddresses.get(ewsAddress);
                	initEwsService(address.getEWSAddress(),address.getEWSPassword(),url);
                }
            }
            catch (URISyntaxException e)
            {
                Log.log.error("EWSGateway: " + e.getMessage(), e);
            }
            catch (Exception e)
            {
                Log.log.error("EWSGateway: " + e.getMessage(), e);
            }
        }
    }
    private void initEwsService(String username,String pass_word,String url) throws URISyntaxException
    {
    	
        ExchangeService indieExchangeService = new ExchangeService();
        ExchangeCredentials indieCredentials = new WebCredentials(username, pass_word);
        indieExchangeService.setCredentials(indieCredentials);
        indieExchangeService.setUrl(new URI(url));
        if (isProxyEnabled)
        {
            indieExchangeService.setWebProxy(proxy);
        }
        ewsServices.put(username, indieExchangeService);
        setAddressLastReceivedTime(username);
    }
    private void initEwsFilter(String username)
    {
        for(Filter filter : orderedFilters)
        {
        	EWSFilter ewsFilter = (EWSFilter) filter;
        	ewsFilter.getProcessResults().putIfAbsent(username,Boolean.FALSE);
        }
    }
    private void setAddressLastReceivedTime(String username)
    {
    	EWSFilter ewsFilter = null;
    	for(String filterId : addressLastReceivedTime.keySet())
    	{
            if(addressLastReceivedTime.get(filterId).get(username) == null) 
            {
            	addressLastReceivedTime.get(filterId).putIfAbsent(username,new Long(-1));
            }
    	}
    }
    
    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        EWSFilter ewsFilter = new EWSFilter((String) params.get(EWSFilter.ID), (String) params.get(EWSFilter.ACTIVE), (String) params.get(EWSFilter.ORDER), (String) params.get(EWSFilter.INTERVAL), (String) params.get(EWSFilter.EVENT_EVENTID), (String) params.get(EWSFilter.RUNBOOK), (String) params.get(EWSFilter.SCRIPT), (String) params.get(EWSFilter.QUERY), (String) params.get(EWSFilter.INCLUDE_ATTACHMENT), (String) params.get(EWSFilter.FOLDER_NAMES), (String) params.get(EWSFilter.MARK_AS_READ),
                        (String) params.get(EWSFilter.ONLY_NEW));
        return ewsFilter;
    } // getFilter

    public Map<String, EWSAddress> getEWSAddresses()
    {
        return ewsAddresses;
    } // getEmailAddresses

    public void setEWSAddresses(Map<String, Object> params)
    {
        EWSAddress emailAddress = getEWSAddress(params);

        // remove existing emailAddress
        removeEWSAddress(emailAddress.getEWSAddress());

        // update emailAddress
        ewsAddresses.put(emailAddress.getEWSAddress(), emailAddress);
    }

    public EWSAddress getEWSAddress(Map<String, Object> params)
    {
        return new EWSAddress((String) params.get(EWSAddress.EWSADDRESS), (String) params.get(EWSAddress.EWSP_ASSWORD));
    }

    public void removeEWSAddress(String id)
    {
        EWSAddress emailAddress = ewsAddresses.get(id);
        if (emailAddress != null)
        {
            // remove from filters
            ewsAddresses.remove(id);
        }
    } // removeFilter
    public void clearAndSetEWSAddresses(List<Map<String, Object>> ewsAddressesList)
    {
        ewsAddresses.clear();
        if (ewsAddressesList != null && !ewsAddressesList.isEmpty())
        {
            ewsServices.clear();
        }
        String url = configReceiveEWS.getUrl();
        EWSAddress address = new EWSAddress(configReceiveEWS.getUsername(), configReceiveEWS.getPassword());
        try
        {
        	initEwsService(address.getEWSAddress(),address.getEWSPassword(),url);
        	initEwsFilter(address.getEWSAddress());
        }
        catch (URISyntaxException e)
        {
            Log.log.error("EWSGateway: Error at clearAndSetEWSAddresses " + configReceiveEWS.getUsername() + ". the following exception was thrown");
            Log.log.error(e.getMessage(), e);
        }
        catch (Exception e)
        {
            System.out.println("\n "+e.getMessage()+" "+ configReceiveEWS.getUsername() );
            Log.log.error("EWSGateway: Error at clearAndSetEWSAddresses " + configReceiveEWS.getUsername() + ". the following exception was thrown");
            Log.log.error(e.getMessage(), e);
        }

        String name = "";

        for (Map<String, Object> params : ewsAddressesList)
        {
            // ignore the username Map
            if (!params.containsKey("RESOLVE_USERNAME"))
            {
                try
                {
                    name = (String) params.get("EWSADDRESS");
                    ewsAddresses.put(name, getEWSAddress(params));
                    // Log clear and set address
                    params.remove(EWSAddress.EWSP_ASSWORD);
                    Log.log.info("EWSGateway: Adding EWS Address: " + params);

                    address = ewsAddresses.get(name);
                    initEwsService(address.getEWSAddress(),address.getEWSPassword(),url);
                    initEwsFilter(address.getEWSAddress());
                }
                catch (URISyntaxException e)
                {
                    Log.log.error("EWSGateway: Could not add address " + name + ". the following exception was thrown");
                    Log.log.error(e.getMessage(), e);
                    ewsAddresses.remove(name);
                }
                catch (Exception e)
                {
                    Log.log.error("EWSGateway: Could not add address " + name + ". the following exception was thrown");
                    Log.log.error(e.getMessage(), e);
                    ewsAddresses.remove(name);
                }
            }
        }
    } // clearAndSetEmailAddresses
    
    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList)
    {
    	super.clearAndSetFilters(filterList);
        new ConcurrentHashMap<String,ConcurrentHashMap<String, Long>>();
        ConcurrentHashMap<String,ConcurrentHashMap<String, Long>> tempAddressReceivedTime = new ConcurrentHashMap<String,ConcurrentHashMap<String, Long>>();
    	for(Filter filter : orderedFilters)
    	{
    		EWSFilter ewsFilter = (EWSFilter) filter;
            if(filter.getId() != null && addressLastReceivedTime.get(filter.getId()) != null)
            {
                tempAddressReceivedTime.put((String)filter.getId(), addressLastReceivedTime.get(filter.getId()));
                
            }
            else if(filter.getId() != null)
            {
                tempAddressReceivedTime.put((String)filter.getId(),new ConcurrentHashMap<String,Long>());
            }
    	}
    	addressLastReceivedTime = tempAddressReceivedTime;
    	setAddressLastReceivedTime(configReceiveEWS.getUsername());
    	for(String username : ewsAddresses.keySet())
    	{
    		setAddressLastReceivedTime(username);
    	}
    	for(Filter filter : orderedFilters)
    	{
    		for(String address : addressLastReceivedTime.get(filter.getId()).keySet())
    		{
    			((EWSFilter) filter).getProcessResults().put(address, Boolean.FALSE);
    		}
    	}
    }

    @Override
    public void start()
    {
    	  //LI-33 Logging for EWS gateway
    	Log.log.info("***************************************************");
    	Log.log.info("EWSGateway: Starting EWSGateway listener");
    	Log.log.info("***************************************************");
        //LI-33 Logging for EWS gateway
        Log.log.info("***************************************************");
        Log.log.info("EWSGateway: Starting EWSGateway listener");
        Log.log.info("***************************************************");
        super.start();
    } // start

    @Override
    public void run()
    {
        // This call is must for all the ClusteredGateways.
    	Log.log.info("***************************************************");
    	Log.log.info("EWSGateway: Sending sync request");
    	 Log.log.info("***************************************************");
        super.sendSyncRequest();

        while (running)
        {
            if (isActive())
            {
                long startTime = System.currentTimeMillis();

                /*
                 * if any remaining events are in primary data queue or
                 * primary data executor has waiting messages still to be
                 * processed, do not get messages for more new events.
                 */

                if (primaryDataQueue.isEmpty() && !primaryDataQueueExecutorBusy())
                {
                    Log.log.trace("EWSGateway: Local Event Queue is empty and Primary Data Queue Executor is free.....");
                    try
                    {
	                    for (Filter filter : orderedFilters)
	                    {
	                        try {
	                            Log.putCurrentNamedContext(Constants.GATEWAY_FILTER, filter.getId());
	                            // check whether it's time to execute this filter.
	                            if (shouldFilterExecute(filter, startTime))
	                            {
	                                EWSFilter ewsFilter = (EWSFilter) filter;
	                                if (!ewsFilter.getProcessResults().containsValue(Boolean.TRUE)&& StringUtils.isBlank(ewsFilter.getLastReceivedTime()) && ewsFilter.getOnlyNew())
	                                {
	                                    /*
	                                     * since this is the first time the filter
	                                     * is executing and there is no last email
	                                     * received time, try to get the latest
	                                     * email received for specified filter
	                                     * condition to use as the last received
	                                     * time.
	                                     */
	                                    // Following fails when gateway server time
	                                    // zone is ahead of Exchange Srever time
	                                    // zone
	                                    // ewsFilter.setLastReceivedTime(DateUtils.convertDateToXMLFormat(new
	                                    // Date()));
	                                    for(String key : ewsFilter.getProcessResults().keySet())
	                                    {
	                                        ewsFilter.getProcessResults().put(key,Boolean.FALSE);
	                                    }
	                                }
	                                else
	                                {
	                                    for(String key : ewsFilter.getProcessResults().keySet())
	                                    {
	                                        ewsFilter.getProcessResults().put(key,Boolean.TRUE);
	                                    }
	                                }
	                                
	                                List<String> newLastReceivedIds = new ArrayList<String>();
	                                for(String username: ewsServices.keySet())
	                                {
	                                    ExchangeService ewsService = ewsServices.get(username);
	                                    try 
	                                    {
	                                        Date addressLastReceivedDate = new Date (addressLastReceivedTime.get(filter.getId()).get(username));
	                                        ewsFilter.setLastReceivedTime(DateUtils.convertDateToXMLFormat(addressLastReceivedDate));
	                                        // uses Map due to legacy code that would requuire changing baseClusteredGateway to parameterize
	                                        List<Map> results = invokeEWSServer(ewsFilter,username);
	                                        
	                                        if(addressLastReceivedTime.get(filter.getId()).get(username)!= null)
	                                            ewsFilter.setLastReceivedTime(DateUtils.convertDateToXMLFormat(new Date(addressLastReceivedTime.get(filter.getId()).get(username))));
	                                        if (results.size() > 0)
	                                        {
	                                            Long lastReceivedTimeMillis = addressLastReceivedTime.get(filter.getId()).get(username) != null ? addressLastReceivedTime.get(filter.getId()).get(username) : new Long(-1);
	                                            List<String> lastRecievedIds = ewsFilter.getLastReceivedIds();
	                                            // Update the lastChangeDate property.
	                                            // since we intentionally get the objects
	                                            // ordered by changed date in ascending
	                                            // order,
	                                            // go to the bottom of the list and get
	                                            // change
	                                            // date from the last object.
	                                            // Date lastReceivedTime = (Date)
	                                            // results.get(results.size() -
	                                            // 1).get(LAST_RECEIVED_TIME);
	                                            Map<String, String> lastMessage = results.get(results.size() - 1);
	                                            Long lastReceivedTime = Long.parseLong((String)lastMessage.get(LAST_RECEIVED_TIME));
	        
	                                            if (ewsFilter.getProcessResults().get(username).booleanValue())
	                                            {
	                                                newLastReceivedIds.addAll(addToExecuteRunbookQueue(results,ewsFilter,lastReceivedTimeMillis,lastRecievedIds));
	                                            }
	                                            else
	                                            {
	                                                ewsFilter.getProcessResults().put(username,Boolean.TRUE);
	                                                // if this is first loop to get current
	                                                // time, add 1 second to only pick up
	                                                // new results
	                                                lastReceivedTime += 1000;
	                                            }
	        
	                                            long timeInMillis = lastReceivedTime;
	                                            // String lastChangeDate = (String)
	                                            // results.get(results.size() -
	                                            // 1).get(LAST_RECEIVED_TIME);
	                                            Log.log.debug("EWSGateway: " + LAST_RECEIVED_TIME + " for next query is " + DateUtils.convertDateToXMLFormat(new Date(timeInMillis)));
	                                            
	                                            ewsFilter.setLastReceivedTime(DateUtils.convertDateToXMLFormat(new Date(timeInMillis)));
	                                            addressLastReceivedTime.get(filter.getId()).put(username,new Long(timeInMillis));
	                                            
	                                            //filter is applied to all addresses to they must be stored seperately
	                                            if (addressLastReceivedTime.get(filter.getId()).get(username).equals(lastReceivedTimeMillis) || lastReceivedTimeMillis == -1)
	                                            {
	                                                // if last received time has not changed
	                                                // since last run, increase by 1 second
	                                                // to keep from picking up more
	                                                // duplicates
	                                                Long newTimeInMillis = addressLastReceivedTime.get(filter.getId()).get(username)+ 1000;
	                                                ewsFilter.setLastReceivedTime(DateUtils.convertDateToXMLFormat(new Date(newTimeInMillis)));
	                                                addressLastReceivedTime.get(filter.getId()).put(username,newTimeInMillis);
	                                            }
	                                        }
	                                        else if (!ewsFilter.getProcessResults().get(username))
	                                        {
	                                            Log.log.debug("EWSGateway: No Emails Available to Set Last Received Time, processing anything that comes in next");
	                                            ewsFilter.getProcessResults().put(username,Boolean.TRUE);
	                                        }
	                                    }
	                                    catch (URISyntaxException e)
	                                    {
	                                         Log.log.warn("EWSGateway: Error--> " + e.getMessage(), e);
	                                         String pollFailed = "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed";
	                                         String pollFailedReason = "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + e.getMessage();
	                                         Log.alert(pollFailed, pollFailedReason);
	                                    }
	                                    catch (AuthenticationException ae)
	                                    {
	                                         Log.log.warn("EWSGateway: Error--> " + ae.getMessage(), ae);
	                                         String pollFailed = "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed";
	                                         String pollFailedReason = "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + ae.getMessage();
	                                         Log.alert(pollFailed, pollFailedReason);
	                                    }
	                                    catch (RecognitionException ex)         //Construct message and throw more general Exception
	                                    {
	                                        StringBuilder message = new StringBuilder("Parser error around ");
	                                        message.append("line " + ex.line + ", index " + ex.charPositionInLine);
	                                        if(ex.token != null)
	                                        {
	                                            message.append(", token '" + ex.token.getText() + "'");
	                                        }
	                                        message.append(".");
	                                        Log.log.error(message,ex);
	                                        String pollFailed = "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed";
	                                        String pollFailedReason = "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + ex.getMessage();
	                                        Log.alert(pollFailed, pollFailedReason);
	                                    }
	                                    catch (MismatchedParensException ex)
	                                    {
	                                        Log.log.error(ex.getMessage(),ex);
	                                        String pollFailed = "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed";
	                                        String pollFailedReason = "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + ex.getMessage();
	                                        Log.alert(pollFailed, pollFailedReason);                                    }
	                                    catch (Exception e)
	                                    {
	                                        Log.log.warn("EWSGateway: Error --> " + e.getMessage(), e);
	                                        String pollFailed = "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed";
	                                        String pollFailedReason = "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + e.getMessage();
	                                        Log.alert(pollFailed, pollFailedReason);
	                                    }
	                                }
	                                ewsFilter.setLastReceivedIds(newLastReceivedIds);
	                            }
	                            else
	                            {
	                                Log.log.trace("EWSGateway: Filter--> " + filter.getId() + " is inactive or not ready to execute yet.....");
	                            }
	                        } finally {
	                            Log.clearCurrentNamedContext(Constants.GATEWAY_FILTER);
	                        }
	                    }
	
	                    // process events collected by processFilter.
	                    // processFilterEvents(startTime);
	
	                    // If there is no filter yet no need to keep spinning, wait
	                    // for
	                    // a while
	                    if (orderedFilters.size() == 0)
	                    {
	                        try {
								Thread.sleep(interval);
							} catch (InterruptedException e) {
								Log.log.warn("EWSGateway: sleep interrupted--> " + e.getMessage(), e);
							}
	                    }
                    } catch (ConcurrentModificationException e) 
                    {
                    	Log.log.warn("EWSGateway: Error--> " + e.getMessage(), e);
                        Log.alert("EWSGateway: Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + e.getMessage());
                    }
                }
            }

            // at this time, it's time to check
            // if any changes are detected about its state.
            // checkState();
        }
    } // run
    public List<String> addToExecuteRunbookQueue(List<Map> results,EWSFilter ewsFilter, Long lastReceivedTimeMillis,List<String> lastRecievedIds) 
    {
    	List<String> newLastReceivedIds = new ArrayList<String>();
    	// Let's execute runboook for each
        // incident
    	for(Map<String,String> emailMessage : results)
    	{
            // don't re-process something seen
            // in the last query
            String id = (String)emailMessage.get(EMAILID);
            Long emailReceivedTime = Long.parseLong((String)emailMessage.get(LAST_RECEIVED_TIME));
            // below condition will discrd only
            // if time is less than prev filter
            // interval's last fetched email. If
            // both are equal then below
            // condition will be true
            if (((EWSFilter) ewsFilter).getOnlyNew())
            {
                if (emailReceivedTime < lastReceivedTimeMillis)
                {
                    Log.log.error("EWSGateway: Discarding the received email as it's an old email. EMAILID: " + id + "EPOCH EMAIL RECEIVED TIME: " + emailReceivedTime + " EPOCH LAST EMAIl RECEIVED TIME IN PREV FILTER INTERVAL: " + lastReceivedTimeMillis);
                    Log.log.error("EWSGateway: EMAIL RECEIVED TIME: " + new Date(emailReceivedTime));
                    Log.log.error("EWSGateway: LAST EMAIl RECEIVED TIME IN PREV FILTER INTERVAL: " + new Date(lastReceivedTimeMillis));

                    continue;
                }
            }
            // Below condition will make sure if
            // you pass the above condition but
            // if id is same as prev fetched
            // email then will discard the email
            if (!lastRecievedIds.contains(id))
            {

                if (isSocialPost)
                {
                    String to = (String)emailMessage.get("EMAILID");
                    String from = (String)emailMessage.get("SENDERADDRESS");
                    String subject = (String)emailMessage.get(Constants.NOTIFICATION_MESSAGE_SUBJECT);
                    String content = (String)emailMessage.get(Constants.NOTIFICATION_MESSAGE_CONTENT);
                    postToSocial(to, from, subject, content);
                }

                // processFilter(ewsFilter,
                // null,
                // emailMessage);
                addToPrimaryDataQueue(ewsFilter, emailMessage);
                newLastReceivedIds.add((String)emailMessage.get(EMAILID));
            }
    	}
    	return newLastReceivedIds;
    }
    /**
     * This method invokes the EWS service and gets all the messages based on
     * the filter query.
     * 
     * @return - Place everything from the email into a List of Maps.
     * @throws Exception
     */
    private List<Map> invokeEWSServer(EWSFilter filter,String username) throws RecognitionException, MismatchedParensException, 
    		URISyntaxException, AuthenticationException , Exception
    {

        List<Map>result = new ArrayList<Map>();
        List<Folder> folders = new ArrayList<Folder>();
        ExchangeService exchangeService = null;

        if (filter.getSearchFilters() == null || filter.getSearchFilters().isEmpty())
        {
                String nativeQuery = resolveQuery.translate(filter.getQuery());
                filter.setNativeQuery(nativeQuery);
                filter.setSearchFilters(prepareFilter(nativeQuery));
        }

        exchangeService = ewsServices.get(username);

        // always rebind the inbox, we may need to see the performance later.

        // make sure folder settings for other services are not used
        folders.clear();

        // sometime it becomes null, we'll try to bind again.
        try {
        	inbox = Folder.bind(exchangeService, WellKnownFolderName.Inbox);
        }catch(Exception ex) {
        	if ( ex.getMessage().contains("Unauthorized") ) {
        		Log.log.error("EWSGateway: Error connecting to the inbox, check the "
        				+ "auth credentials or the server name");
        		throw new AuthenticationException("EWSGateway: Wrong credentials");
            }else {   	
            	throw new URISyntaxException("EWSGateway: "," Unable to connect to the Remote inbox");
            }
        }        FolderView folderView = new FolderView(5000);
        folderView.setTraversal(FolderTraversal.Deep);
        if (filter.getFolderNames().size() > 0)
        {
            FindFoldersResults folderResults = inbox.findFolders(folderView);
            for (Folder folder : folderResults.getFolders())
            {

                List<String> filterFolderNameList = filter.getFolderNames();
                for (String filterFolderName : filterFolderNameList)
                {
                    if (filterFolderName.equals(folder.getDisplayName()))
                    {
                        folders.add(folder);
                    }
                }
            }
            if(filter.getFolderNames().contains("Inbox") || filter.getFolderNames().contains("inbox"))
            {
            	folders.add(inbox);
            }
        }
        else
        {
            folders.add(inbox);
        }
    
        Map<Folder, List<Item>> itemFolderMap = new HashMap<Folder, List<Item>>();
        itemFolderMap = queryServer(filter,folders,username);
        List<Item> itemList = new ArrayList<Item>();
        for(List<Item> list: itemFolderMap.values())
        {
        	itemList.addAll(list);
        }
        int count = itemList.size();
        if (count > 0)
        {
        	Log.log.info("***********************************************************");
        	Log.log.info("EWSGateway: Gateway filter fetched " + count + " messages");
        	Log.log.info("***********************************************************");
        	result = createResultList(filter,exchangeService,itemList,itemFolderMap,folders,username);
        }
        
        return result;
    }
    private Map<Folder, List<Item>> queryServer(EWSFilter filter, List<Folder> folders,String username) throws Exception
    {
    	FindItemsResults<Item> results = null;
    	Map<Folder, List<Item>> itemFolderMap = new HashMap<Folder, List<Item>>();
    	if (!filter.getProcessResults().get(username).booleanValue())
        { // Find latest email received for specified filter
          // condition
            ItemView itemView = new ItemView(1000);
            itemView.getOrderBy().add(ItemSchema.DateTimeReceived, SortDirection.Ascending);
            itemView.setPropertySet(new PropertySet(BasePropertySet.IdOnly, ItemSchema.DateTimeReceived));

            int offset = 0;
            int resultSize = 0;
            do
            {
                offset += resultSize;
                itemView.setOffset(offset);
                // Note the Combined Search filter will not include
                // last server time conidtion if it is empty/null,
                // which on startup and no last received time in
                // filter, is
                for (Folder folder : folders)
                {
                    results = folder.findItems(filter.getCombinedSearchFilter(), itemView);
                    itemFolderMap.put(folder, results.getItems());
                }
                Log.log.debug("EWSGateway: Email for " + filter.getId() + " from " + username + " items returned " + results.getItems().size() + " email(s).");

                if (Log.log.isDebugEnabled() && results.getTotalCount() > 0)
                {
                    int last = results.getItems().size() - 1;
                    Log.log.debug("EWSGateway: " + LAST_RECEIVED_TIME + " of last email in batch is  " + DateUtils.convertDateToXMLFormat(new Date(((Item) results.getItems().get(last)).getDateTimeReceived().getTime())));
                }
                resultSize = results.getItems().size();
            } while (results.isMoreAvailable());
        }
        else
        {
            for (Folder folder : folders)
            {
                results = folder.findItems(filter.getCombinedSearchFilter(), itemView);
                itemFolderMap.put(folder, results.getItems());
            }

            Log.log.debug("EWSGateway: Email for " + filter.getId() + " from " + username + " returned " + results.getTotalCount() + " email(s).");
        }
    	return itemFolderMap;
    }
    private List<Map> createResultList(EWSFilter filter, ExchangeService exchangeService, List<Item> itemList,Map<Folder,List<Item>> itemFolderMap,List<Folder> folders, String username) throws URISyntaxException, Exception
    {
    	List<Map>result = new ArrayList<Map>();
    	if (StringUtils.containsIgnoreCase(filter.getQuery(), "RECIPIENTS"))
         {
             for (Item email : itemList)
             {
                 EmailMessage msg = (EmailMessage) email;
                 msg.load(new PropertySet(EmailMessageSchema.CcRecipients, EmailMessageSchema.ToRecipients, EmailMessageSchema.Subject, EmailMessageSchema.IsRead, EmailMessageSchema.Body));
                 msg.setIsRead(true);
                 Log.log.debug("EWSGateway: Filter results found subject: " + msg.getSubject());
                 if (recipientsQuery(filter, msg))
                 {
                     Map<String, Object> messageMap = prepareMessageMap(msg, filter.getIncludeAttachment(), resolveTempDir);
                     messageMap.put(INBOX, inbox.getId().getUniqueId());
                     messageMap.put(EMAILTRIGGER, username);
                     result.add(messageMap);
                 }
             }
             if (filter.getMarkAsRead())
             {
                 updateEmailServer(exchangeService, itemFolderMap, folders);
             }
         }
         else
         {
             for (Item email : itemList)
             {
                 EmailMessage msg = (EmailMessage) email;
                 Map<String,Object> messageMap = prepareMessageMap(msg, filter.getIncludeAttachment(), resolveTempDir);
                 msg.setIsRead(true);
                 messageMap.put(INBOX, inbox.getId().getUniqueId());
                 messageMap.put(EMAILTRIGGER, username);
                 result.add(messageMap);
             }
             if (filter.getMarkAsRead())
             {
                 updateEmailServer(exchangeService, itemFolderMap, folders);
             }
         }
    	return result;
    }
    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
        if (result.containsKey(EMAILID))
        {
            event.put(Constants.EXECUTE_ALERTID, event.get(EMAILID));
        }
        if (result.containsKey(Constants.NOTIFICATION_MESSAGE_SUBJECT))
        {
            event.put(Constants.EXECUTE_REFERENCE, result.get(Constants.NOTIFICATION_MESSAGE_SUBJECT));
        }
    }

    private void updateEmailServer(ExchangeService service, Map<Folder, List<Item>> itemFolderMap, List<Folder> folders) throws URISyntaxException, Exception
    {
        for (Folder folder : folders)
        {
            if (itemFolderMap.get(folder) != null && !itemFolderMap.get(folder).isEmpty())
            {
                ServiceResponseCollection<UpdateItemResponse> response = service.updateItems(itemFolderMap.get(folder), folder.getId(), ConflictResolutionMode.AutoResolve, MessageDisposition.SaveOnly, null);
                if (response.getOverallResult() == ServiceResult.Success)
                {
                    Log.log.trace("EWSGateway: updated " + itemFolderMap.get(folder).size() + " items inside folder" + folder.getDisplayName());
                }
            }
            else
            {
                Log.log.trace("EWSGateway: No Items from folder " + folder.getDisplayName() + " to update");
            }
        }
    }

    /*
     * Public API method implementation goes here
     */

    /**
     * 
     * @param params
     * @param username
     * @param password
     * @throws Exception
     */
    public void sendMessage(Map<String, Object> params, String username, String password) throws Exception
    {
        if (isPrimary())
        {
            if (!params.containsKey(Constants.NOTIFICATION_MESSAGE_TO) || StringUtils.isBlank((String) params.get(Constants.NOTIFICATION_MESSAGE_TO)))
            {
                throw new Exception("TO Recipient(s) must be provided");
            }
            else
            {
                String url = configReceiveEWS.getUrl();
                if (!StringUtils.isEmpty(username))
                {
                    if (StringUtils.isEmpty(password))
                    {
                        EWSAddress ewsAddress = ewsAddresses.get(username);
                        if (ewsAddress == null)
                        {
                            username = configReceiveEWS.getUsername();
                            password = configReceiveEWS.getPassword();
                        }
                        else
                        {
                            username = ewsAddress.getEWSAddress();
                            password = ewsAddress.getEWSPassword();
                        }
                    }
                }
                else
                {
                    username = configReceiveEWS.getUsername();
                    password = configReceiveEWS.getPassword();
                }

                ExchangeService service = new ExchangeService();
                ExchangeCredentials credentials = new WebCredentials(username, password);
                service.setCredentials(credentials);
                try
                {
                    service.setUrl(new URI(url));

                    EmailMessage msg = new EmailMessage(service);
                    if (params.get(Constants.NOTIFICATION_MESSAGE_SUBJECT) instanceof String)
                    {
                        msg.setSubject((String) params.get(Constants.NOTIFICATION_MESSAGE_SUBJECT));
                    }
                    else
                    {
                        msg.setSubject("");
                    }
                    msg.setBody(MessageBody.getMessageBodyFromText((String) params.get(Constants.NOTIFICATION_MESSAGE_CONTENT)));

                    List<String> recipients = StringUtils.convertStringToList((String) params.get(Constants.NOTIFICATION_MESSAGE_TO), ",");
                    for (String toRecipient : recipients)
                    {
                        msg.getToRecipients().add(toRecipient);
                    }
                    recipients = StringUtils.convertStringToList((String) params.get(Constants.NOTIFICATION_MESSAGE_CC), ",");
                    for (String ccRecipient : recipients)
                    {
                        msg.getCcRecipients().add(ccRecipient);
                    }
                    recipients = StringUtils.convertStringToList((String) params.get(Constants.NOTIFICATION_MESSAGE_BCC), ",");
                    for (String bccRecipient : recipients)
                    {
                        msg.getBccRecipients().add(bccRecipient);
                    }

                    if (params.containsKey(Constants.NOTIFICATION_MESSAGE_REPLY_TO))
                    {
                        String replyTo = (String) params.get(Constants.NOTIFICATION_MESSAGE_REPLY_TO);
                        if (StringUtils.isNotBlank(replyTo))
                        {
                            msg.setInReplyTo(replyTo);
                        }
                    }
                    Map<String, byte[]> attachments = (Map<String, byte[]>) params.get(Constants.NOTIFICATION_MESSAGE_ATTACHMENTS);
                    if (attachments != null && attachments.size() > 0)
                    {
                        // collect it so once the job is done we can remove them
                        // from
                        // the "tmp" directory
                        for (String attachment : attachments.keySet())
                        {
                            byte[] fileContent = attachments.get(attachment);
                            msg.getAttachments().addFileAttachment(attachment, fileContent);
                        }
                    }
                    //LI-33 Improve logging for gateways
                    Log.log.debug("EWSGateway: Sending email message with details --> " + (params != null
                    		? params.toString(): ""));
                    msg.send();
                }
                catch (URISyntaxException e)
                {
                    Log.log.error("EWSGateway: " + e.getMessage(), e);
                    throw new Exception(e.getMessage());
                }
                catch (Exception e)
                {
                    Log.log.error("EWSGateway: " + e.getMessage(), e);
                    throw new Exception(e.getMessage());
                }
            }
        }
    }

    /**
     * This method moves a email ID into a folder or sub folder. If the
     * "subFolderName" is provided then it must be available under the folder
     * defined by "folderName".
     * 
     * @param emailID
     *            is an array of {@link String} that stores te unique IDs of the
     *            emails to be moved to a folder or its subfolder.
     * @param folderName
     *            is the display name of a folder under Inbox.
     * @param subFolderName
     *            is the display name of a sub folder under the "folder".
     * @param username
     *            if not provided then uses the gateway configuration.
     * @param password
     *            ties with the username.
     * @return true or false based on the result of the operation.
     * @throws Exception
     */
    public boolean moveEmailToFolder(String emailID, String folderName, String subFolderName, String username, String password) throws Exception
    {
        boolean result = true;
        // validate that the emailID and folderName must be provided.
        if (StringUtils.isBlank(emailID) || StringUtils.isBlank(folderName))
        {
        	Log.log.error("EWSGateway: Right emailId " + emailID + " or folder name " + folderName + " was not provided");
            throw new Exception("emailID and folderName parameters are mandatory");
        }
        else
        {
            result = moveEmailToFolder(new String[] { emailID }, folderName, subFolderName, username, password);
        }
        return result;
    }

    /**
     * This method moves a list of email IDs into a folder or sub folder. If the
     * "subFolderName" is provided then it must be available under the folder
     * defined by "folderName".
     * 
     * @param emailIDs
     *            is an array of {@link String} that stores te unique IDs of the
     *            emails to be moved to a folder or its subfolder.
     * @param folderName
     *            is the display name of a folder under Inbox.
     * @param subFolderName
     *            is the display name of a sub folder under the "folder".
     * @param username
     *            if not provided then uses the gateway configuration.
     * @param password
     *            ties with the username.
     * @return true or false based on the result of the operation.
     * @throws Exception
     */
    public boolean moveEmailToFolder(String[] emailIDs, String folderName, String subFolderName, String username, String password) throws Exception
    {
        boolean result = true;
        if ((emailIDs == null || emailIDs.length == 0) || StringUtils.isBlank(folderName))
        {
            throw new Exception("At least one emailID and folderName must be provided");
        }
        else
        {
            String url = configReceiveEWS.getUrl();
            if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
            {
                username = configReceiveEWS.getUsername();
                password = configReceiveEWS.getPassword();
            }

            ExchangeService service = new ExchangeService();
            ExchangeCredentials credentials = new WebCredentials(username, password);
            service.setCredentials(credentials);
            try
            {
                service.setUrl(new URI(url));

                Folder inbox = Folder.bind(service, WellKnownFolderName.Inbox);
                FolderId folderIdToMove = null;
                List<ItemId> itemIDs = new ArrayList<ItemId>();
                for (String emailID : emailIDs)
                {
                    ItemId item = new ItemId(emailID);
                    itemIDs.add(item);
                }

                for (Folder folder : inbox.findFolders(new FolderView(1000)))
                {
                    if (folderName.trim().equalsIgnoreCase(folder.getDisplayName()))
                    {
                        folderIdToMove = folder.getId();
                        Log.log.debug("EWSGateway: Found the folder with ID: " + folder.getId().getUniqueId());
                        if (!StringUtils.isBlank(subFolderName))
                        {
                            folderIdToMove = null;
                            // caller actually wants to move to a sub folder.
                            for (Folder subFolder : folder.findFolders(new FolderView(1000)))
                            {
                                if (subFolderName.trim().equalsIgnoreCase(subFolder.getDisplayName()))
                                {
                                    Log.log.debug("EWSGateway: Found the sub-folder with ID: " + 
                                    				subFolder.getId().getUniqueId());
                                    folderIdToMove = subFolder.getId();
                                }
                            }
                        }
                    }
                }

                if (folderIdToMove == null)
                {
                    throw new Exception("EWSGateway: Folder name \"" + folderName + "\" and/or \"" + subFolderName + "\" not be found, please verify");
                }
                else
                {
                    service.moveItems(itemIDs, folderIdToMove);
                }
            }
            catch (URISyntaxException e)
            {
                throw new Exception("EWSGateway: " + e.getMessage(), e);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        return result;
    }

    private static Map<String, Object> prepareMessageMap(Item item, boolean includeAttachments, String temporaryDir) throws URISyntaxException, Exception
    {
        Map<String, Object> result = new HashMap<String, Object>();
        EmailMessage email = (EmailMessage) item;
        EmailAddressCollection recipientAddressCollection = null;
        EmailAddressCollection recipientsAddressCollection = null;
        EmailAddressCollection ccrecipientsAddressCollection = null;
        List<String> recipientsAddresses = new ArrayList<String>();
        List<String> ccrecipientsAddresses = new ArrayList<String>();
        email.load();
        recipientAddressCollection = email.getToRecipients();
        recipientAddressCollection.forEach((EmailAddress emailAddress) -> recipientsAddresses.add(emailAddress.getAddress()));
        ccrecipientsAddressCollection = email.getCcRecipients();
        ccrecipientsAddressCollection.forEach((EmailAddress emailAddress) -> ccrecipientsAddresses.add(emailAddress.getAddress()));
        ccrecipientsAddressCollection = email.getCcRecipients();
        result.put(RECIPIENTS, StringUtils.join(recipientsAddresses, ","));
        result.put(CCRECIPIENTS, StringUtils.join(ccrecipientsAddresses, ","));
        result.put(EMAILID, email.getId().getUniqueId());
        result.put(Constants.NOTIFICATION_MESSAGE_FROM, email.getFrom().getAddress());
        result.put(SENDERNAME, email.getSender().getName());
        result.put(SENDERADDRESS, email.getSender().getAddress());
        result.put(Constants.NOTIFICATION_MESSAGE_SUBJECT, email.getSubject());
        if (email.getBody() != null)
        {
            result.put(Constants.NOTIFICATION_MESSAGE_CONTENT, email.getBody().toString());
        }

        if (email.getDateTimeReceived() != null)
        {
            // convert to String
            result.put(LAST_RECEIVED_TIME, "" + email.getDateTimeReceived().getTime());
        }

        AttachmentCollection attachments = email.getAttachments();
        String duplicateString = "";
        if (includeAttachments && attachments != null && attachments.getCount() > 0)
        {
            Map<String, Object> attachmentMap = new HashMap<String, Object>();
            // collect it so once the job is done we can remove them from
            // the "tmp" directory
            List<File> files = new ArrayList<File>();
            Map<String, Integer> duplicateMap = new HashMap<String, Integer>();
            for (Attachment attachment : attachments)
            {
                try
                {
                    if (!duplicateMap.containsKey(attachment.getName()))
                    {
                        duplicateMap.put(attachment.getName(), 0);
                    }
                    if (attachment instanceof ItemAttachment)
                    {
                        ItemAttachment file = (ItemAttachment) attachment;
                        // convert to mime type - loses follow-up flags and
                        // categories - library not in RSControl so can't
                        // send object
                        file.load(ItemSchema.MimeContent);
                        if (attachmentMap.containsKey(attachment.getName()))
                        {
                            file.load(ItemSchema.MimeContent);
                            duplicateMap.put(attachment.getName(), duplicateMap.get(attachment.getName()) + 1);
                            duplicateString = " (" + duplicateMap.get(attachment.getName()) + ")";
                            attachmentMap.put(attachment.getName() + duplicateString, file.getItem().getMimeContent().getContent());
                        }
                        else
                        {
                            attachmentMap.put(attachment.getName(), file.getItem().getMimeContent().getContent());
                        }
                    }
                    else if (attachment instanceof FileAttachment)
                    {
                        String randomFileName = temporaryDir + "/" + FileUtils.createRandomFileName(".tmp", null);

                        FileAttachment file = (FileAttachment) attachment;
                        file.load(randomFileName);
                        File randomFile = new File(randomFileName);
                        if (attachmentMap.containsKey(attachment.getName()))
                        {
                            duplicateMap.put(attachment.getName(), duplicateMap.get(attachment.getName()) + 1);
                            attachmentMap.put(attachment.getName() + "(" + duplicateMap.get(attachment.getName()) + ")", FileUtils.readFileToByteArray(randomFile));

                        }
                        else
                        {
                            attachmentMap.put(attachment.getName(), FileUtils.readFileToByteArray(randomFile));
                        }
                        files.add(randomFile);
                    }
                    else
                    {
                        Log.log.error("EWSGateway: Unknown Attachment Type: " + attachment.getClass());
                    }
                }
                catch (Exception e)
                {
                    Log.log.error("EWSGateway: Failed to load Attachment: " + attachment.getName(), e);
                    attachmentMap.put(attachment.getName(), "Attachment Failed to Load, See Gateway Logs for More Details");
                }
            }
            result.put(Constants.NOTIFICATION_MESSAGE_ATTACHMENTS, attachmentMap);
            // now delete all the random files.
            for (File file : files)
            {
                FileUtils.deleteQuietly(file);
            }
        }
        return result;
    }

    private boolean recipientsQuery(EWSFilter filter, EmailMessage message) throws Exception
    {

        boolean result = true;
        boolean orresult = false;
        String andDelimeter = java.util.regex.Pattern.quote(QueryToken.AND);
        String orDelimeter = java.util.regex.Pattern.quote(QueryToken.OR);
        String[] andqueryItems = filter.getNativeQuery().split(andDelimeter);
        String[] orqueryItems = null;
        List<QueryToken> queryTokens = new ArrayList<QueryToken>();

        for (String queryItem : andqueryItems)
        {
            if (StringUtils.containsIgnoreCase(queryItem, "|OR|"))
            {
                orqueryItems = queryItem.split(orDelimeter);
                for (String orqueryItem : orqueryItems)
                {
                    result = orresult || match(QueryToken.get(orqueryItem), message);
                    orresult = result;
                }
            }
            else
            {
                result = result && match(QueryToken.get(queryItem), message);
                orresult = result;
            }
            if (!result)
            {
                break;
            }

        }
        return result;
    }

    private boolean match(QueryToken queryToken, EmailMessage message)
    {
        boolean result = false;
        StringBuilder recipientAddresses = new StringBuilder();
        try
        {
            // making it lowercase is important
            // that's how it's stored in the map.
            String keyFromQuery = queryToken.getKey().toLowerCase();
            String queryOperator = queryToken.getOperator();
            String valueFromQuery = queryToken.getValue();
            if (defaultEmailAttributes.containsKey(keyFromQuery))
            {
                // recipients
                if ("recipients".equalsIgnoreCase(keyFromQuery))
                {
                    recipientAddresses.setLength(0);
                    for (Iterator<EmailAddress> address = message.getToRecipients().iterator(); address.hasNext();)
                    {
                        recipientAddresses.append(address.next().getAddress());
                    }
                    for (Iterator<EmailAddress> address = message.getCcRecipients().iterator(); address.hasNext();)
                    {
                        recipientAddresses.append(address.next().getAddress());
                    }
                    result = resolveQuery.isStringValueMatch(recipientAddresses.toString(), valueFromQuery, queryOperator);
                }
                else if ("torecipients".equalsIgnoreCase(keyFromQuery))
                {
                    recipientAddresses.setLength(0);
                    for (Iterator<EmailAddress> address = message.getToRecipients().iterator(); address.hasNext();)
                    {
                        recipientAddresses.append(address.next().getAddress());
                    }
                    result = resolveQuery.isStringValueMatch(recipientAddresses.toString(), valueFromQuery, queryOperator);
                }
                else if ("ccrecipients".equalsIgnoreCase(keyFromQuery))
                {
                    recipientAddresses.setLength(0);
                    for (Iterator<EmailAddress> address = message.getCcRecipients().iterator(); address.hasNext();)
                    {
                        recipientAddresses.append(address.next().getAddress());
                    }
                    result = resolveQuery.isStringValueMatch(recipientAddresses.toString(), valueFromQuery, queryOperator);
                }
                else if ("isread".equalsIgnoreCase(keyFromQuery))
                {
                    result = resolveQuery.isStringValueMatch(message.getIsRead().toString(), valueFromQuery, queryOperator);
                }
                else if ("subject".equalsIgnoreCase(keyFromQuery))
                {
                    result = resolveQuery.isStringValueMatch(message.getSubject(), valueFromQuery, queryOperator);
                }
                else if ("body".equalsIgnoreCase(keyFromQuery))
                {
                    result = resolveQuery.isStringValueMatch(MessageBody.getStringFromMessageBody(message.getBody()), valueFromQuery, queryOperator);
                }
            }

        }
        catch (Exception e)
        {
            result = false;
            Log.log.trace("EWSGateway: Not matched:" + e.getMessage());
            Log.log.debug("EWSGateway: no match found for: " + queryToken);
        }
        return result;
    }

    /**
     * 
     * 
     * @param query
     *            example "isread |=| false |&&| subject |CONTAINS| "help
     *            " |&&| from |CONTAINS| "jim"
     * @return
     */
    private List<SearchFilter> prepareFilter(String query) throws Exception
    {
        List<SearchFilter> result = new ArrayList<SearchFilter>();
        List<SearchFilter> orList = new ArrayList<SearchFilter>();
        String[] tokens = tokens = query.split("\\|&&\\|");
        for (String token : tokens)
        {
            if (!StringUtils.containsIgnoreCase(token, "RECIPIENT"))
            {
                if (token.contains("|OR|"))
                {
                    for (String orToken : token.split("\\|OR\\|"))
                    {
                        orList.add(createEWSSearchFilter(orToken));
                    }
                    result.add(new SearchFilter.SearchFilterCollection(LogicalOperator.Or, orList));
                }
                else
                {
                    result.add(createEWSSearchFilter(token));
                }
            }
        }
        return result;
    }

    private SearchFilter createEWSSearchFilter(String token) throws Exception
    {
        String key = null;
        String value = null;
        // every token will have either |=| or |CONTAINS| in them
        String[] keyValues = new String[2];
        boolean isEqual = false;
        boolean isLessThan = false;
        boolean isGreaterThan = false;
        SearchFilter result = null;

        if (token.contains("RECEIVED"))
        {
            if (token.contains("|<=|"))
            {
                keyValues = token.split("\\|<=\\|");
                isLessThan = true;
                isGreaterThan = false;
                isEqual = true;
            }
            else if (token.contains("|<|"))
            {
                keyValues = token.split("\\|<\\|");
                isLessThan = true;
                isGreaterThan = false;
                isEqual = false;
            }
            else if (token.contains("|>=|"))
            {
                keyValues = token.split("\\|>=\\|");
                isLessThan = false;
                isGreaterThan = true;
                isEqual = true;
            }
            else if (token.contains("|>|"))
            {
                keyValues = token.split("\\|>\\|");
                isLessThan = false;
                isGreaterThan = true;
                isEqual = false;
            }
            else if (token.contains("|=|"))
            {
                keyValues = token.split("\\|=\\|");
                isLessThan = false;
                isGreaterThan = false;
                isEqual = true;
            }
            else
            {
            	Log.log.error("EWSGateway: Invalid tokens in the query for Received Time");
                throw new Exception("");
            }
        }
        else if (token.contains("|=|"))
        {
            keyValues = token.split("\\|=\\|");
            isEqual = true;
        }
        else if (token.contains("|CONTAINS|"))
        {
            keyValues = token.split("\\|CONTAINS\\|");
            isEqual = false;
        }
        else
        {
            throw new Exception("Invalid tokens in the query");
        }
        key = new String(keyValues[0].trim().toUpperCase());
        value = keyValues[1].trim();
        if ("ISREAD".equals(key))
        {
            result = new SearchFilter.IsEqualTo(EmailMessageSchema.IsRead, "true".equalsIgnoreCase(value));
        }
        else if ("SUBJECT".equals(key))
        {
            if (isEqual)
            {
                result = new SearchFilter.IsEqualTo(EmailMessageSchema.Subject, value);
            }
            else
            {
                result = new SearchFilter.ContainsSubstring(EmailMessageSchema.Subject, value);
            }
        }
        else if ("CONTENT".equals(key))
        {
            if (isEqual)
            {
                result = new SearchFilter.IsEqualTo(EmailMessageSchema.Body, value);
            }
            else
            {
                result = new SearchFilter.ContainsSubstring(EmailMessageSchema.Body, value);
            }
        }
        else if ("FROM".equals(key))
        {
            if (isEqual)
            {
                result = new SearchFilter.IsEqualTo(EmailMessageSchema.From, value);
            }
            else
            {
                result = new SearchFilter.ContainsSubstring(EmailMessageSchema.From, value);
            }
        }
        else if ("SENDER".equals(key))
        {
            if (isEqual)
            {
                result = new SearchFilter.IsEqualTo(EmailMessageSchema.Sender, value);
            }
            else
            {
                result = new SearchFilter.ContainsSubstring(EmailMessageSchema.Sender, value);
            }
        }
        else if ("EMAILID".equals(key))
        {
            if (isEqual)
            {
                result = new SearchFilter.IsEqualTo(EmailMessageSchema.Id, value);
            }
            else
            {
                result = new SearchFilter.ContainsSubstring(EmailMessageSchema.Id, value);
            }
        }
        else if ("RECEIVED".equals(key))
        {
            Date compareValue = DateUtils.convertStringToDate(value);
            if (compareValue == null)
            {
               throw new Exception("Invalid Date Format: " + value);
            }
            if (isLessThan)
            {
                if (isEqual)
                {
                    result = new SearchFilter.IsLessThanOrEqualTo(EmailMessageSchema.DateTimeReceived, compareValue);
                }
                else
                {
                    result = new SearchFilter.IsLessThan(EmailMessageSchema.DateTimeReceived, compareValue);
                }
            }
            else if (isGreaterThan)
            {
                if (isEqual)
                {
                    result = new SearchFilter.IsGreaterThanOrEqualTo(EmailMessageSchema.DateTimeReceived, compareValue);
                }
                else
                {
                    result = new SearchFilter.IsGreaterThan(EmailMessageSchema.DateTimeReceived, compareValue);
                }
            }
            else if (isEqual)
            {
                result = new SearchFilter.IsEqualTo(EmailMessageSchema.DateTimeReceived, compareValue);
            }
        }
        return result;
    }

    private List<Map> searchEmail(SearchFilter filter, int maxRecords, boolean includeAttachments, String username, String password) throws Exception
    {
        return searchEmail(filter, maxRecords, includeAttachments, username, password, null);
    }

    private List<Map> searchEmail(SearchFilter filter, int maxRecords, boolean includeAttachments, String username, String password, List<String> folderNames) throws Exception
    {
        List<Map> result = new ArrayList<Map>();

        String url = configReceiveEWS.getUrl();
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = configReceiveEWS.getUsername();
            password = configReceiveEWS.getPassword();
        }

        ExchangeService service = new ExchangeService();
        ExchangeCredentials credentials = new WebCredentials(username, password);
        service.setCredentials(credentials);
        try
        {
            service.setUrl(new URI(url));
          //LI-33 Improve logging for gateways
            Log.log.info("EWSGateway: Search email, trying to connect with url --> " + url != null? url.toString(): "");
            Folder inbox = Folder.bind(service, WellKnownFolderName.Inbox);

            ItemView itemView = new ItemView(maxRecords);
            itemView.getOrderBy().add(EmailMessageSchema.DateTimeReceived, SortDirection.Ascending);

            List<Item> emailList = new ArrayList<Item>();
            if (folderNames != null)
            {
                FolderView folderView = new FolderView(5000);
                folderView.setTraversal(FolderTraversal.Deep);
                FindFoldersResults folderResults = inbox.findFolders(folderView);
                List<Folder> folders = folderResults.getFolders();

                for (Folder folder : folders)
                {
                    if (folderNames.contains(folder.getDisplayName()))
                    {
                        FindItemsResults<Item> folderresults = folder.findItems(filter, itemView);
                        if (folderresults.getTotalCount() > 0)
                        {
                            emailList.addAll(folderresults.getItems());
                        }
                    }
                }
            }
            else
            {
                FindItemsResults<Item> results = inbox.findItems(filter, itemView);
                emailList.addAll(results.getItems());
            }

            int count = emailList.size();
            if (count > 0)
            {
            	//LI-33 Improve logging for gateways
            	Log.log.info("EWSGateway: Search emails found with count --> " + count);
                for (Item item : emailList)
                {
                    EmailMessage msg = (EmailMessage) item;
                    Map messageMap = prepareMessageMap(msg, includeAttachments, resolveTempDir);
                    messageMap.put(INBOX, inbox.getId().getUniqueId());
                    result.add(messageMap);
                }
            }
        }
        catch (URISyntaxException e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return result;
    }

    private EmailMessage findEmailMessageById(String emailId, String username, String password) throws Exception
    {
        EmailMessage result = null;

        String url = configReceiveEWS.getUrl();
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = configReceiveEWS.getUsername();
            password = configReceiveEWS.getPassword();
        }

        ExchangeService service = new ExchangeService();
        ExchangeCredentials credentials = new WebCredentials(username, password);
        service.setCredentials(credentials);
        try
        {
            service.setUrl(new URI(url));
          //LI-33 Improve logging for gateways
            Log.log.info("EWSGateway: Search email, trying to connect with url --> " + url != null? url.toString(): "");
            Folder inbox = Folder.bind(service, WellKnownFolderName.Inbox);

            List<SearchFilter> searchFilters = prepareFilter("EMAILID |=| " + emailId);
            SearchFilter searchFilter = new SearchFilter.SearchFilterCollection(LogicalOperator.And, searchFilters);

            ItemView itemView = new ItemView(1);

            FindItemsResults<Item> results = inbox.findItems(searchFilter, itemView);
            int count = results.getTotalCount();
            if (count > 0)
            {
            	//LI-33 Improve logging for gateways
            	Log.log.info("EWSGateway: Search emails found with count --> " + count);
                for (Item item : results)
                {
                    EmailMessage msg = (EmailMessage) item;
                    result = (EmailMessage) item;
                    // once we found the first item get out
                    // we expect only one message anyways.
                    break;
                }
            }
        }
        catch (URISyntaxException e)
        {
            Log.log.error("EWSGateway: " + e.getMessage(), e);
            throw e;
        }
        catch (Exception e)
        {
            Log.log.error("EWSGateway: " + e.getMessage(), e);
            throw e;
        }
        return result;
    }

    public List<Map> searchEmail(String query, int maxRecords, boolean includeAttachments, String username, String password, List<String> folderNames) throws Exception
    {
        List<SearchFilter> filters = new ArrayList<SearchFilter>();
        try
        {
            String nativeQuery = resolveQuery.translate(query);
            filters = prepareFilter(nativeQuery);
        }
        catch (Exception e)
        {
            Log.log.error("EWSGateway: Invalid EWS query \"" + query + "\".");
            throw new Exception("Invalid EWS query:" + query);
        }

        // we only support AND operator.
        SearchFilter searchFilter = new SearchFilter.SearchFilterCollection(LogicalOperator.And, filters);

        return searchEmail(searchFilter, maxRecords, includeAttachments, username, password, folderNames);
    }

    public List<Map> searchEmail(String query, int maxRecords, boolean includeAttachments, String username, String password) throws Exception
    {

        List<SearchFilter> filters = new ArrayList<SearchFilter>();
        try
        {
            String nativeQuery = resolveQuery.translate(query);
            filters = prepareFilter(nativeQuery);
        }
        catch (Exception e)
        {
            Log.log.error("EWSGateway: Invalid EWS query \"" + query + "\".");
            throw new Exception("Invalid EWS query:" + query);
        }

        // we only support AND operator.
        SearchFilter searchFilter = new SearchFilter.SearchFilterCollection(LogicalOperator.And, filters);

        return searchEmail(searchFilter, maxRecords, includeAttachments, username, password, null);
    }

    public void forwardEmail(String emailID, Map<String, Object> params, String username, String password) throws Exception
    {
        // validate that the emailID must be provided.
        if (StringUtils.isBlank(emailID))
        {
            throw new Exception("EWSGateway: emailID parameter is mandatory");
        }
        else
        {
            if (!params.containsKey(Constants.NOTIFICATION_MESSAGE_TO) || StringUtils.isBlank((String) params.get(Constants.NOTIFICATION_MESSAGE_TO)))
            {
                throw new Exception("EWSGateway: TO Recipient(s) must be provided as part of the \"params\" Map with \"TO\" key");
            }
            else
            {
                String url = configReceiveEWS.getUrl();
                if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
                {
                    username = configReceiveEWS.getUsername();
                    password = configReceiveEWS.getPassword();
                }

                ExchangeService service = new ExchangeService();
                ExchangeCredentials credentials = new WebCredentials(username, password);
                service.setCredentials(credentials);
                try
                {
                    service.setUrl(new URI(url));

                  //LI-33 Improve logging for gateways
                    Log.log.info("EWSGateway: Forward email, trying to connect with url --> "
                    		+ url != null? url.toString(): "");
                    EmailMessage msg = EmailMessage.bind(service, new ItemId(emailID));
                    if (msg != null)
                    {
                        msg.load();
                        ResponseMessage forward = msg.createForward();
                        // msg.f
                        String body = (String) params.get(Constants.NOTIFICATION_MESSAGE_CONTENT);
                        MessageBody forwardBody = new MessageBody(body);
                        List<String> recipients = StringUtils.convertStringToList((String) params.get(Constants.NOTIFICATION_MESSAGE_TO), ",");
                        for (String toRecipient : recipients)
                        {
                            msg.getToRecipients().add(toRecipient);
                            forward.getToRecipients().add(toRecipient);
                        }
                        forward.setBody(forwardBody);
                        forward.send();
                    }
                }
                catch (URISyntaxException e)
                {
                    Log.log.error(e.getMessage(), e);
                    throw new Exception(e.getMessage());
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                    throw new Exception(e.getMessage());
                }
            }
        }
    }
}
