package com.resolve.gateway.resolutionrouting;

public class SIRRRSettings {

	private final String source;
	private final String title;
	private final String type;
	private final String playbook;
	private final String severity;
	private final String owner;

	public SIRRRSettings(String source, String title, String type, String playbook, String severity, String owner, String automation) {
		this.source = source;
		this.title = title;
		this.type = type;
		this.playbook = playbook;
		this.severity = severity;
		this.owner = owner;
	}
	
	public String getSource() {
		return source;
	}

	public String getTitle() {
		return title;
	}

	public String getType() {
		return type;
	}

	public String getPlaybook() {
		return playbook;
	}

	public String getSeverity() {
		return severity;
	}

	public String getOwner() {
		return owner;
	}

}
