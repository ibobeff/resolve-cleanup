/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.email;

import java.util.Map;

import com.resolve.util.Log;
import com.resolve.util.queue.AbstractQueueListener;

public class SendEmailListener extends AbstractQueueListener<Map<String, Object>>
{
    private static final EmailGateway instance = EmailGateway.getInstance();

    public boolean process(Map<String, Object> message)
    {
        boolean result = true;

        if (Log.log.isTraceEnabled())
        {
            Log.log.trace("Received email message to be sent");
            Log.log.trace("Contents:" + message);
        }

        if (!instance.send(message))
        {
            Log.log.warn("Message wasn't sent, check the log file." + message);
            result = false;
        }

        return result;
    }
}
