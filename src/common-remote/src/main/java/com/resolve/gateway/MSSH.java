/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.gateway;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.ssh.SSHGateway;
import com.resolve.gateway.ssh.SubnetMask;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MSSH
{
    private static final String RSCONTROL_SET_POOLS = "MSSH.setPools";

    /*
     * USERNAME/PASSWORD
     */

    /**
     * Checks out a {@link SSHGatewayConnection} using the given login
     * information and hostname
     * 
     * @param username
     *            SSH username
     * @param password
     *            SSH password
     * @param hostname
     *            the hostname of the remote system that is being connected to.
     * @return an SSHGatewayConnection object
     * @throws Exception
     *             if a login error occurs
     */

    public static SSHGatewayConnection checkOutConnection(String username, String password, String hostname) throws Exception
    {
        return SSHGateway.getInstance().checkOutConnection(username, password, hostname);
    }

    /**
     * Checks out a {@link SSHGatewayConnection} using the given login
     * information, hostname, and port number
     * 
     * @param username
     *            SSH username
     * @param password
     *            SSH password
     * @param hostname
     *            the hostname of the remote system that is being connected to.
     * @param port
     *            the HTTPS port number
     * @return an SSHGatewayConnection object
     * @throws Exception
     *             if a login error occurs
     */
    public static SSHGatewayConnection checkOutConnection(String username, String password, String hostname, int port) throws Exception
    {
        return SSHGateway.getInstance().checkOutConnection(username, password, hostname, port);
    }

    /**
     * Checks out a {@link SSHGatewayConnection} using the given login
     * information, hostname, port number, and timeout limit
     * 
     * @param username
     *            SSH username
     * @param password
     *            SSH password
     * @param hostname
     *            the hostname of the remote system that is being connected to
     * @param port
     *            the HTTPS port number
     * @param timeout
     *            the timeout value for this checkout (in seconds)
     * @returnan SSHGatewayConnection object
     * @throws Exception
     *             if a login error occurs
     */
    public static SSHGatewayConnection checkOutConnection(String username, String password, String hostname, int port, int timeout) throws Exception
    {
        return SSHGateway.getInstance().checkOutConnection(username, password, hostname, port, timeout);
    }

    /*
     * PUBLIC KEY
     */
    /**
     * Checks out a {@link SSHGatewayConnection} using the given login
     * information, public key file, passphrase, and hostname
     * 
     * @param username
     *            SSH username
     * @param filename
     *            a path to a public key file
     * @param passphrase
     *            the passphrase associated with the public key file
     * @param hostname
     *            the hostname of the remote system that is being connected to.
     * @return SSHGatewayConnection object
     * @throws Exception
     *             if a login error occurs
     */
    public static SSHGatewayConnection checkOutConnection(String username, String filename, String passphrase, String hostname) throws Exception
    {
        return SSHGateway.getInstance().checkOutConnection(username, filename, passphrase, hostname);
    }

    /**
     * Checks out a {@link SSHGatewayConnection} using the given login
     * information, public key file, passphrase, hostname, and port number
     * 
     * @param username
     *            SSH username
     * @param filename
     *            a path to a public key file
     * @param passphrase
     *            the passphrase associated with the public key file
     * @param hostname
     *            the hostname of the remote system that is being connected to.
     * @param port
     *            the HTTPS port number
     * @return SSHGatewayConnection object
     * @throws Exception
     *             if a login error occurs
     */
    public static SSHGatewayConnection checkOutConnection(String username, String filename, String passphrase, String hostname, int port) throws Exception
    {
        return SSHGateway.getInstance().checkOutConnection(username, filename, passphrase, hostname, port);
    }

    /**
     * Checks out a {@link SSHGatewayConnection} using the given login
     * information, public key file, passphrase, hostname, and port number
     * 
     * @param username
     *            SSH username
     * @param filename
     *            a path to a public key file
     * @param passphrase
     *            the passphrase associated with the public key file
     * @param hostname
     *            the hostname of the remote system that is being connected to.
     * @param port
     *            the HTTPS port number
     * @param timeout
     *            the timeout value for this checkout (in seconds)
     * @return SSHGatewayConnection object
     * @throws Exception
     *             if a login error occurs
     */
    public static SSHGatewayConnection checkOutConnection(String username, String filename, String passphrase, String hostname, int port, int timeout) throws Exception
    {
        return SSHGateway.getInstance().checkOutConnection(username, filename, passphrase, hostname, port, timeout);
    }

    /*
     * INTERACTIVE
     */
    /**
     * Checks out a {@link SSHGatewayConnection} using the given login
     * information, a list of responses, hostname
     * 
     * @param username
     *            SSH username
     * @param responses
     *            a {@link List} of responses
     * @param hostname
     *            the hostname of the remote system that is being connected to.
     * @return SSHGatewayConnection object
     * @throws Exception
     *             if a login error occurs
     */

    public static SSHGatewayConnection checkOutConnection(String username, List responses, String hostname) throws Exception
    {
        return SSHGateway.getInstance().checkOutConnection(username, responses, hostname);
    }

    /**
     * Checks out a {@link SSHGatewayConnection} using the given login
     * information, a list of responses, hostname
     * 
     * @param username
     *            SSH username
     * @param responses
     *            a {@link List} of responses
     * @param hostname
     *            the hostname of the remote system that is being connected to.
     * @param port
     *            the HTTPS port number
     * @return SSHGatewayConnection object
     * @throws Exception
     *             if a login error occurs
     */
    public static SSHGatewayConnection checkOutConnection(String username, List responses, String hostname, int port) throws Exception
    {
        return SSHGateway.getInstance().checkOutConnection(username, responses, hostname, port);
    }

    /**
     * Checks out a {@link SSHGatewayConnection} using the given login
     * information, a list of responses, hostname
     * 
     * @param username
     *            SSH username
     * @param responses
     *            a {@link List} of responses
     * @param hostname
     *            the hostname of the remote system that is being connected to.
     * @param port
     *            the HTTPS port number
     * @param timeout
     *            the timeout value for this checkout (in seconds)
     * @return SSHGatewayConnection object
     * @throws Exception
     *             if a login error occurs
     */
    public static SSHGatewayConnection checkOutConnection(String username, List responses, String hostname, int port, int timeout) throws Exception
    {
        return SSHGateway.getInstance().checkOutConnection(username, responses, hostname, port, timeout);
    }

    /*
     * NONE - no SSH authentication
     */
    /**
     * Checks out a {@link SSHGatewayConnection} using the given login
     * information and hostname WITHOUT authentication
     * 
     * @param username
     *            SSH username
     * @param hostname
     *            the hostname of the remote system that is being connected to.
     * @return SSHGatewayConnection object
     * @throws Exception
     *             if a login error occurs
     */
    public static SSHGatewayConnection checkOutConnection(String username, String hostname) throws Exception
    {
        return SSHGateway.getInstance().checkOutConnection(username, hostname);
    }

    /**
     * Checks out a {@link SSHGatewayConnection} using the given login
     * information, hostname, and port number WITHOUT authentication
     * 
     * @param username
     *            SSH username
     * @param hostname
     *            the hostname of the remote system that is being connected to.
     * @param port
     *            the HTTPS port number
     * @return SSHGatewayConnection object
     * @throws Exception
     *             if a login error occurs
     */
    public static SSHGatewayConnection checkOutConnection(String username, String hostname, int port) throws Exception
    {
        return SSHGateway.getInstance().checkOutConnection(username, hostname, port);
    }

    /**
     * Checks out a {@link SSHGatewayConnection} using the given login
     * information, hostname, and port number WITHOUT authentication
     * 
     * @param username
     *            SSH username
     * @param hostname
     *            the hostname of the remote system that is being connected to.
     * @param port
     *            the HTTPS port number
     * @param timeout
     *            the timeout value for this checkout (in seconds)
     * @return SSHGatewayConnection object
     * @throws Exception
     *             if a login error occurs
     */
    public static SSHGatewayConnection checkOutConnection(String username, String hostname, int port, int timeout) throws Exception
    {
        return SSHGateway.getInstance().checkOutConnection(username, hostname, port, timeout);
    }

    /**
     * 
     * @param sshConnect
     *            Checks in a connections using the given
     *            {@link SSHGatewayConnection} object
     * @throws Exception
     *             if a login error occurs
     */
    public static void checkInConnection(SSHGatewayConnection sshConnect) throws Exception
    {
        SSHGateway.getInstance().checkInConnection(sshConnect);
    }

    /**
     * Clears all {@link SubnetMask} pools from the current SSHGateway instance.
     * If the paramList isn't null, it will replenish the subnet mask pool with
     * the initializaiton parameters in the paramList {@link List}.
     * 
     * @param paramList
     *            A list of {@link SubnetMask} initialization {@link Map}s.
     *            paramList keys include: { SubnetMask.SUBNETMASK : String,
     *            SubnetMask.MAXCONNECTION : String, SubnetMask.TIMEOUT : String
     *            }
     * 
     */
    @FilterCompanionModel(modelName = "SSHPool")
    public static void clearAndSetPools(List<Map<String, String>> paramList)
    {
        // clear and set pools
        SSHGateway.getInstance().clearAndSetPools(paramList);

        // send new pools
        getPools(null);

        // save config
        MainBase.main.exitSaveConfig();

    } // clearAndSetPools

    /**
     * Sets the {@link SubnetMask} pool in the current {@link SSHGateway}
     * instance, using the given params {@link Map} of initialization
     * parameters.
     * 
     * @param params
     *            A {@link Map} of {@link SubnetMask} initialization parameters.
     *            paramList keys include: { SubnetMask.SUBNETMASK : String,
     *            SubnetMask.MAXCONNECTION : String, SubnetMask.TIMEOUT : String
     *            }
     */
    public static void setPool(Map params)
    {
        SSHGateway.getInstance().setPool(params);

        // save config
        MainBase.main.exitSaveConfig();

    } // setPool

    /**
     * Sends an internal message to the Enterprise Service Bus (ESB) of the
     * {@link MainBase} which details all properties of each {@link SubnetMask}
     * object in the {@link SSHGateway} {@link SubnetMask} pool.
     * 
     * @param params
     *            A {@link Map} of {@link SubnetMask} initialization parameters.
     *            paramList keys include: { SubnetMask.SUBNETMASK : String,
     *            SubnetMask.MAXCONNECTION : String, SubnetMask.TIMEOUT : String
     *            }
     */
    public static void getPools(Map params)
    {
        if (SSHGateway.getInstance().isActive())
        {
            Map<String, String> content = new HashMap<String, String>();

            for (SubnetMask pool : SSHGateway.getInstance().getPools().values())
            {
                Map<String, String> poolMap = new HashMap<String, String>();
                poolMap.put(SubnetMask.QUEUE, SSHGateway.getInstance().getQueueName());
                poolMap.put(SubnetMask.SUBNETMASK, pool.getSubnetMask());
                poolMap.put(SubnetMask.MAXCONNECTION, "" + pool.getMaxConnection());
                poolMap.put(SubnetMask.TIMEOUT, "" + pool.getTimeout());
                content.put(pool.getSubnetMask(), StringUtils.remove(StringUtils.mapToString(poolMap), "null"));
            }

            // send content to rscontrol
            // if (content.size() > 0)
            {
                content.put("QUEUE", SSHGateway.getInstance().getQueueName());

                if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROL, RSCONTROL_SET_POOLS, content) == false)
                {
                    Log.log.warn("Failed to send connection pools to RSCONTROL");
                }
            }
        }
    } // getPools
} // MSSH
