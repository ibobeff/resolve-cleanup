/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.resolvegateway;

import com.resolve.gateway.resolvegateway.ResolveGateway;
import com.resolve.util.Log;

public class ServerHeartbeat extends Thread
{
    private ResolveGateway gateway;
    
    public ServerHeartbeat(ResolveGateway gateway) {
        this.gateway = gateway;
    }

    @Override
    public void run() {
        Log.log.info("Heartbeat is started.");
        
        int retry = 0;
        int serverRetry = gateway.getServerCheckRetry();
        
        while(gateway.isActive() && gateway.getServerCheckInterval() > 0) {
            try {
                try
                {
                    boolean alive = gateway.sendHeartbeat();
                    
                    String status = (alive)?"":" not";
                    Log.log.debug("Heart beat: " + System.currentTimeMillis());
                    Log.log.debug(gateway.name + " is" + status + " alive.");
                    
                    // If the server was up then down
                    if(gateway.isServerAlive()) {
                        while(!alive) {
                            if(retry < serverRetry) {
                                sleep(1000);
                                retry++;
                                continue;
                            }
                            
                            Log.log.warn("IBM MQ server is down.");
                            gateway.setServerAlive(false);
                            
                            gateway.suspend();
                            
                            break;
                        }
                    }
                    
                    // When server was down and now up again, recover the connections and listeners/subscribers
                    else {
                        if(alive) {
                            Log.log.warn("Server is back up.");

                            gateway.resume();
                            
                            gateway.setServerAlive(true);
                        }
                    }
                } catch(Exception e) {
                    Log.log.error(e.getMessage(), e);
                }

                int interval = gateway.getServerCheckInterval();
                sleep(interval*1000);
            } 
            catch(InterruptedException e) {
                Log.log.error(e.getMessage(), e);
                break;
            }
        } // while()
    } // run()
    
    public void stopHeartBeat() {
        
        try {
            this.interrupt();
            Log.log.info("Heartbeat is stopped.");
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }

} // class ServerHeartbeat