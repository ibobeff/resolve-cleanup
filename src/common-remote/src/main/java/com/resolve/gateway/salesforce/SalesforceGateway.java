/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.salesforce;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MSalesforce;
import com.resolve.query.ResolveQuery;
import com.resolve.query.translator.SalesforceQueryTranslator;
import com.resolve.rsremote.ConfigReceiveSalesforce;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SalesforceGateway extends BaseClusteredGateway
{
    // Singleton
    private static volatile SalesforceGateway instance = null;

    // This will be set in initialize() with the proper queue name from the
    // configuration.
    private String queue = "SALESFORCE";

    private static final String NUMBER = "number";
    private static final String SYS_ID = "id";
    public static String CHANGE_DATE = "sf:LastModifiedDate";

    public static volatile ObjectService caseObjectService;

    private final ResolveQuery resolveQuery;

    public static SalesforceGateway getInstance(ConfigReceiveSalesforce config)
    {
        if (instance == null)
        {
            instance = new SalesforceGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static SalesforceGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("Salesforce Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    /**
     * Private constructor.
     *
     * @param config
     */
    private SalesforceGateway(ConfigReceiveSalesforce config)
    {
        // Important, call super here.
        super(config);
        queue = config.getQueue();
        resolveQuery = new ResolveQuery(new SalesforceQueryTranslator());
    }

    @Override
    public String getLicenseCode()
    {
        return "SALESFORCE";
    }

    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_SALESFORCE;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MSalesforce.class.getSimpleName();
    }

    @Override
    protected Class<MSalesforce> getMessageHandlerClass()
    {
        return MSalesforce.class;
    }

    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    /**
     * Initializes the specific gateway resources.
     *
     * @param config
     */
    @Override
    protected void initialize()
    {
        ConfigReceiveSalesforce config = (ConfigReceiveSalesforce) configurations;

        if(Log.log.isTraceEnabled())
        {
            Log.log.trace("Salesforce Configuration:");
            Log.log.trace("    Account type:" + config.getAccounttype());
            Log.log.trace("    user:" + config.getHttpbasicauthusername());
            Log.log.trace("    pwd:" + config.getHttpbasicauthpassword());
            Log.log.trace("    interval:" + config.getInterval());
            Log.log.trace("    queue:" + config.getQueue());
            Log.log.trace("    url:" + config.getUrl());
        }

        queue = config.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/salesforce/";

        caseObjectService = ObjectServiceFactory.getObjectService(config, CaseObjectService.OBJECT_IDENTITY);
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new SalesforceFilter((String) params.get(SalesforceFilter.ID), (String) params.get(SalesforceFilter.ACTIVE), (String) params.get(SalesforceFilter.ORDER), (String) params.get(SalesforceFilter.INTERVAL), (String) params.get(SalesforceFilter.EVENT_EVENTID)
        // , (String) params.get(SalesforceFilter.PERSISTENT_EVENT)
                        , (String) params.get(SalesforceFilter.RUNBOOK), (String) params.get(SalesforceFilter.SCRIPT), (String) params.get(SalesforceFilter.OBJECT), (String) params.get(SalesforceFilter.QUERY), null);
    } // getFilter

    @Override
    public void start()
    {
        Log.log.warn("Starting SalesforceListener");
        super.start();
    } // start

    // ////////////////////////////////////////////////////////////////////
    // Method: run //
    // //
    // Description: This method goes through the filters provided in the//
    // salesforce filter ui, runs them through Salesforce and then //
    // runs runbooks (if there are any) based on the results of //
    // the filter by salesforce. //
    // //
    // Inputs: none //
    // Outputs: none //
    // //
    // ////////////////////////////////////////////////////////////////////
    @Override
    public void run()
    {
        //This call is must for all the ClusteredGateways.
        super.sendSyncRequest();

        String serverTime;
        try
        {
            serverTime = getSalesforceServerDateTime();
            while (running)
            {
                try
                {
                    long startTime = System.currentTimeMillis();
                    
                    /* if any remaining events are in primary data queue or 
                     * primary data executor has waiting messages still to be processed, 
                     * do not get messages for more new events.
                     */
                    
                    if (primaryDataQueue.isEmpty() && !primaryDataQueueExecutorBusy())
                    {
                        Log.log.trace("Local Event Queue is empty and Primary Data Queue Executor is free.....");
                        for (Filter filter : orderedFilters)
                        {
                            if (shouldFilterExecute(filter, startTime))
                            {
                                SalesforceFilter salesforceFilter = (SalesforceFilter) filter;
                                if (StringUtils.isEmpty(salesforceFilter.getLastChangeDate()))
                                {
                                    salesforceFilter.setLastChangeDate(serverTime);
                                }
    
                                List<Map<String, String>> results = invokeSalesforceService(salesforceFilter);
                                if (results.size() > 0)
                                {
    
                                    // Let's execute runboook for each
                                    for (Map<String, String> salesforceObject : results)
                                    {
                                        //processFilter(salesforceFilter, null, salesforceObject);
                                        addToPrimaryDataQueue(salesforceFilter, salesforceObject);
                                    }
    
                                    // Update the lastChangeDate property.
                                    // since we intentionally get the objects
                                    // ordered by changed date in ascending order,
                                    // go to the bottom of the list and get change
                                    // date from the last object.
                                    String lastChangeDate = (String) results.get(results.size() - 1).get(CHANGE_DATE);
                                    salesforceFilter.setLastChangeDate(lastChangeDate);
                                }
                            }
                            else
                            {
                                Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                            }
                        }
                    }
                    // process events collected by processFilter.
                    //processFilterEvents(startTime);
    
                    // If there is no filter yet no need to keep spinning, wait for
                    // a while
                    if (orderedFilters.size() == 0)
                    {
                        Thread.sleep(interval);
                    }
                }
                catch (Exception e)
                {
                    Log.log.warn("Error: " + e.getMessage(), e);
                    Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", 
                              "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + 
                              "due to " + e.getMessage());
                    
                    try
                    {
                        Thread.sleep(interval);
                    }
                    catch (InterruptedException ie)
                    {
                        Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                    }
                }
            }
        }
        catch (Exception e1)
        {
            Log.log.error(e1.getMessage(), e1);
            //stop();
        }
    } // run

    // ////////////////////////////////////////////////////////////////////
    // Method: invokeSalesforceService //
    // //
    // Description: Invokes the salesforce web service for the filter //
    // provided, runs the correct webservice based on the object //
    // provided in the filter //
    // //
    // Inputs: filter: this contains all of the information provided //
    // in the original filter, such as query, and object//
    // Outputs: result: Salesforce's reply to the query broken into //
    // keys and values, for easier use/parseing //
    // //
    // ////////////////////////////////////////////////////////////////////
    private List<Map<String, String>> invokeSalesforceService(SalesforceFilter filter) throws Exception
    {
        String nativeQuery = filter.getQuery();
        if (filter.getNativeQuery() == null)
        {
            try
            {
                nativeQuery = resolveQuery.translate(filter.getQuery());
            }
            catch (Exception e)
            {
                Log.log.debug("Did not translate, the query \"" + nativeQuery + "\" is treated as native.");
            }
        }
        filter.setNativeQuery(nativeQuery); // set it so we never attempt to
                                            // translate again.

        boolean hasVariable = VAR_REGEX_GATEWAY_VARIABLE.matcher(nativeQuery).find();
        String rankMethod = "LastModifiedDate";
        if (hasVariable)
        {
            if (nativeQuery.contains(SalesforceFilter.LAST_CHANGE_DATE))
            {
                nativeQuery = nativeQuery.replaceFirst(Constants.VAR_REGEX_GATEWAY_VARIABLE, filter.getLastChangeDate());
            }
            if (nativeQuery.contains(SalesforceFilter.LAST_TICKET_ID))
            {
                nativeQuery = nativeQuery.replaceFirst(Constants.VAR_REGEX_GATEWAY_VARIABLE, filter.getLastTicketId());
                rankMethod = "id";
            }
        }

        // ConfigReceiveSalesforce salesforceConfig = (ConfigReceiveSalesforce)
        // configurations;
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        if (CaseObjectService.OBJECT_IDENTITY.equalsIgnoreCase(filter.getObject()))
        {
            Log.log.debug("Salesforce Incident Query to execute:::" + filter.getQuery());
            result = caseObjectService.search(nativeQuery, rankMethod, true, null, null);
        }

        return result;
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
        // TODO In future we'll refactor if any object have some other keys to
        // be referenced here.
        if (result.containsKey(SYS_ID))
        {
            event.put(Constants.EXECUTE_REFERENCE, result.get(SYS_ID));
        }
        if (result.containsKey(NUMBER))
        {
            event.put(Constants.EXECUTE_ALERTID, result.get(NUMBER));
        }
    }

    /*
     * Public API method implementation goes here
     */
    public Map<String, String> createCase(Map<String, String> params, String username, String password) throws Exception
    {
        return caseObjectService.create(params, username, password);
    }

    public boolean createCaseWorknote(String parentSysId, String note, String username, String password) throws Exception
    {
        return caseObjectService.createWorknote(parentSysId, note, username, password);
    }

    public void updateCase(String sysId, Map<String, String> params, String username, String password) throws Exception
    {
        caseObjectService.update(sysId, params, username, password);
    }

    public void updateCaseStatus(String sysId, String status, String username, String password) throws Exception
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("Status", status);
        caseObjectService.update(sysId, params, username, password);
    }

    public void deleteCase(String sysId, String username, String password) throws Exception
    {
        caseObjectService.delete(sysId, username, password);
    }

    public Map<String, String> selectCaseBySysId(String sysId, String username, String password) throws Exception
    {
        return caseObjectService.selectByID(sysId, username, password);
    }

    public Map<String, String> selectCaseByNumber(String number, String username, String password) throws Exception
    {
        return caseObjectService.selectByNumber(number, username, password);
    }

    public List<Map<String, String>> searchCase(String query, String username, String password) throws Exception
    {
        return caseObjectService.search(query, null, false, username, password);
    }

    // ////////////////////////////////////////////////////////////////////
    // Method: getSalesforceServerDateTime //
    // //
    // Description: Gets the datetime from the salesforce server using //
    // the login provided in the blueprint file, this is used as //
    // the timestamp replacement in queries //
    // //
    // Inputs: none //
    // Outputs: result: Salesforce's time //
    // //
    // ////////////////////////////////////////////////////////////////////
    private String getSalesforceServerDateTime() throws Exception
    {
        String result = null;
        ConfigReceiveSalesforce config = (ConfigReceiveSalesforce) configurations;
        SoapCaller soapCaller = new SoapCaller(config, config.getUrl(), config.getHttpbasicauthusername(), config.getHttpbasicauthpassword());
        soapCaller.loginToSalesforce(config.getHttpbasicauthusername(), config.getHttpbasicauthpassword());
        result = soapCaller.getServerTime();

        if (StringUtils.isEmpty(result))
        {
            Calendar now = Calendar.getInstance();// TimeZone.getTimeZone("GMT"));
            TimeZone tz = now.getTimeZone();
            String tzStr = tz.getID();
            result = DateUtils.convertDateToSalesforceFormat(now.toString(), tzStr);
        }

        return result;
    }
}
