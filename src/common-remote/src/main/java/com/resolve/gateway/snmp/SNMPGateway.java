/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.snmp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.snmp4j.CommandResponder;
import org.snmp4j.CommandResponderEvent;
import org.snmp4j.MessageDispatcherImpl;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.mp.MPv1;
import org.snmp4j.mp.MPv2c;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.TcpAddress;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultTcpTransportMapping;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.MultiThreadedMessageDispatcher;
import org.snmp4j.util.ThreadPool;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MSNMP;
import com.resolve.gateway.SNMPData;
import com.resolve.gateway.SNMPResponse;
import com.resolve.rsremote.ConfigReceiveSNMP;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SNMPGateway extends BaseClusteredGateway implements CommandResponder
{
    private static final String DEVICE_ID = "DEVICE_ID";

    // Singleton
    private static volatile SNMPGateway instance = null;

    // This will be set in initialize() with the proper queue name from the
    // configuration.
    private String queue = "SNMP";

    // Used by the listener
    private MultiThreadedMessageDispatcher dispatcher;
    private Snmp snmp = null;
    private Address listenAddress;
    private ThreadPool threadPool;

    // private final ResolveQuery resolveQuery;

    public static SNMPGateway getInstance(ConfigReceiveSNMP config)
    {
        if (instance == null)
        {
            instance = new SNMPGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static SNMPGateway getInstance()
    {
        if (instance == null)
        {
            throw new RuntimeException("SNMP Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    /**
     * Private constructor.
     *
     * @param config
     */
    private SNMPGateway(ConfigReceiveSNMP config)
    {
        // Important, call super here.
        super(config);
        queue = config.getQueue();
        // resolveQuery = new ResolveQuery(new ServiceNowQueryTranslator());
    }

    @Override
    public String getLicenseCode()
    {
        return "SNMP";
    }

    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_SNMP;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MSNMP.class.getSimpleName();
    }

    @Override
    protected Class<MSNMP> getMessageHandlerClass()
    {
        return MSNMP.class;
    }

    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    /**
     * Initializes the specific gateway resources.
     *
     * @param config
     */
    @Override
    protected void initialize()
    {
        ConfigReceiveSNMP snmpConfig = (ConfigReceiveSNMP) configurations;
        queue = snmpConfig.getQueue().toUpperCase();
        this.gatewayConfigDir = "/config/snmp/";

        initTrapReceiver(snmpConfig);
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new SNMPFilter((String) params.get(SNMPFilter.ID), (String) params.get(SNMPFilter.ACTIVE), (String) params.get(SNMPFilter.ORDER), (String) params.get(SNMPFilter.INTERVAL),
                        (String) params.get(SNMPFilter.EVENT_EVENTID)
                        // , (String) params.get(SNMPFilter.PERSISTENT_EVENT)
                        , (String) params.get(SNMPFilter.RUNBOOK), (String) params.get(SNMPFilter.SCRIPT), (String) params.get(SNMPFilter.SNMP_VERSION), (String) params.get(SNMPFilter.IP_ADDRESSES), (String) params.get(SNMPFilter.READ_COMMUNITY), (String) params.get(SNMPFilter.TIMEOUT), (String) params.get(SNMPFilter.RETRIES), (String) params.get(SNMPFilter.TRAP_RECEIVER), (String) params.get(SNMPFilter.SNMP_TRAP_OID), (String) params.get(SNMPFilter.OID), (String) params.get(SNMPFilter.REGEX),
                        (String) params.get(SNMPFilter.COMPARATOR), (String) params.get(SNMPFilter.VALUE));
    } // getFilter

    @Override
    public void start()
    {
        Log.log.warn("Starting SNMPListener");
        super.start();
    } // start

    @Override
    public void run()
    {
        //This call is must for all the ClusteredGateways.
        super.sendSyncRequest();

        // first register the SNMP trap listener.
        snmp.addCommandResponder(this);

        while (running)
        {
            try
            {
                long startTime = System.currentTimeMillis();
                boolean hasNonTrapReceivers = false;

                /* if any remaining events are in primary data queue or 
                 * primary data executor has waiting messages still to be processed, 
                 * do not get messages for more new events.
                 */
                
                if (primaryDataQueue.isEmpty() && !primaryDataQueueExecutorBusy())
                {
                    Log.log.trace("Local Event Queue is empty and Primary Data Queue Executor is free.....");
                    for (Filter filter : orderedFilters)
                    {
                        SNMPFilter snmpFilter = (SNMPFilter) filter;
                        // we do not want to process Trap receiving filters.
                        if (!snmpFilter.isTrapReceiver()) 
                        {
                            hasNonTrapReceivers = true;
                            if (shouldFilterExecute(filter, startTime))
                            {
                                    List<Map<String, String>> results = invokeSnmpService(snmpFilter);
                                    if (results.size() > 0)
                                    {
                                        // Let's execute runboook for each
                                        // incident
                                        for (Map<String, String> snmpResponse : results)
                                        {
                                            //processFilter(snmpFilter, null, snmpResponse);
                                            addToPrimaryDataQueue(snmpFilter, snmpResponse);
                                        }
                                    }

                                    snmpFilter.setLastExecutionTime(System.currentTimeMillis());
                            }
                            else
                            {
                                Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                            }
                        }
                    }
                }

                // process events collected by processFilter.
                //processFilterEvents(startTime);

                // If there is no filter yet no need to keep spinning, wait for
                // a while
                if (orderedFilters.size() == 0 || !hasNonTrapReceivers)
                {
                    Thread.sleep(interval);
                }
            }
            catch (Exception e)
            {
                Log.log.warn("Error: " + e.getMessage(), e);
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", 
                          "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + 
                          "due to " + e.getMessage());
                
                try
                {
                    Thread.sleep(interval);
                }
                catch (InterruptedException ie)
                {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    } // run

    private void initTrapReceiver(ConfigReceiveSNMP config)
    {
        try
        {
            System.out.println("@@@@@@@@ Initialising Trap Receiver @@@@@@@");
            threadPool = ThreadPool.create("Trap", 2);
            dispatcher = new MultiThreadedMessageDispatcher(threadPool, new MessageDispatcherImpl());
            listenAddress = GenericAddress.parse("udp:" + config.getIpaddress() + "/" + config.getPort());

            TransportMapping transport;
            if (listenAddress instanceof UdpAddress)
            {
                transport = new DefaultUdpTransportMapping((UdpAddress) listenAddress);
            }
            else
            {
                transport = new DefaultTcpTransportMapping((TcpAddress) listenAddress);
            }
            snmp = new Snmp(dispatcher, transport);
            //snmp.getMessageDispatcher().addMessageProcessingModel(new MPv1());
            snmp.getMessageDispatcher().addMessageProcessingModel(new MPv2c());
            // snmp.getMessageDispatcher().addMessageProcessingModel(new
            // MPv3());
            // USM usm = new USM(SecurityProtocols.getInstance(), new
            // OctetString(((MPv3)
            // snmp.getMessageProcessingModel(MessageProcessingModel.MPv3)).createLocalEngineID()),
            // 0);
            // SecurityModels.getInstance().addSecurityModel(usm);
            
            snmp.listen();
        }
        catch (IOException e)
        {
            
            Log.log.warn(e.getMessage(), e);
        }
    }

    /**
     * Get the SNMP data.
     *
     * @param filter
     * @return
     */
    private List<Map<String, String>> invokeSnmpService(SNMPFilter filter)
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        // at this point ipAddress = IP:port (e.g., 10.20.2.109/161)
        for (String ipAddress : filter.getDevices())
        {
            Map<String, String> runbookParams = new HashMap<String, String>();

            String oid = filter.getOid();
            // TODO currently we only support version 2.
            int version = filter.getSnmpVersion() - 1;

            if (oid.endsWith("*")) // this is GETNEXT
            {
                // sometime the oid could be in the form of
                // .1.3.6.1.2.1.47.1.1.1.1.6.* with a leading dot (.)
                // but the response may have no leading dot.
                int startIndex = (oid.startsWith(".") ? 1 : 0);
                String baseOid = oid.substring(startIndex, oid.length() - 1);
                String tmpOid = oid.replace("*", "0");

                while (true)
                {
                    SNMPResponse snmpResponse = SNMPUtils.getNext(version, ipAddress, filter.getReadCommunity(), filter.getRetries(), filter.getTimeoutInMillis(), tmpOid);
                    if (snmpResponse.getOid().startsWith(baseOid))
                    {
                        matchFilterCriteria(runbookParams, snmpResponse, filter);
                    }
                    else
                    {
                        // the GETNEXT moved over to the next OID.
                        break;
                    }
                    tmpOid = snmpResponse.getOid();
                }
            }
            else
            // this is GET
            {
                SNMPResponse snmpResponse = SNMPUtils.get(version, ipAddress, filter.getReadCommunity(), filter.getRetries(), filter.getTimeoutInMillis(), oid);
                if (snmpResponse != null)
                {
                    matchFilterCriteria(runbookParams, snmpResponse, filter);
                }
            }
            if (runbookParams.size() > 0)
            {
                runbookParams.put(Constants.EVENT_EVENTID, filter.getEventEventId());
                runbookParams.put(DEVICE_ID, ipAddress);

                //processFilter(filter, null, runbookParams);
                addToPrimaryDataQueue(filter, runbookParams);
            }
        }

        return result;
    }

    private void matchFilterCriteria(Map<String, String> params, SNMPResponse snmpResponse, SNMPFilter filter)
    {
        Log.log.debug("SNMP Response: " + snmpResponse.toString());

        boolean isMatch = false;

        if (filter.getValue() != null && snmpResponse.getNumericValue() != null) // first
                                                                                 // preference
                                                                                 // given
                                                                                 // to
                                                                                 // numeric
                                                                                 // value
                                                                                 // comparison
        {
            isMatch = match(filter.getValue().longValue(), filter.getComparator(), snmpResponse.getNumericValue());
        }
        else
        {
            String regex = filter.getRegex();
            if (!StringUtils.isEmpty(regex))
            {
                String oidValue = snmpResponse.getStringValue();
                Log.log.trace("Regex: " + regex);
                if(regex.equals("*")) {
                    isMatch = true;
                }
                else {
                    Pattern pattern = Pattern.compile(regex);
                    Matcher matcher = pattern.matcher(oidValue);
                    if (matcher.find())
                    {
                        isMatch = true;
                    }
                }
            }
        }

        if (isMatch)
        {
            params.put(snmpResponse.getOid(), snmpResponse.getStringValue());
        }
    }

    private void matchTrapFilterCriteria(Map<String, String> params, Map<String, SNMPResponse> snmpResponses, SNMPFilter filter, String snmpTrapOid)
    {
        String oid = filter.getOid(); // it's important that TRAP Receiver
                                      // filter's OIDs are absolute, it means
                                      // that there is no wild card.

        // This filter is interested in a certain Trap (e.g., ColdStart,
        // WarmStart etc.)
        if (filter.getSnmpTrapOid() != null && filter.getSnmpTrapOid().equalsIgnoreCase(snmpTrapOid) && snmpResponses.containsKey(oid))
        {
            if (snmpResponses.size() > 0)
            {
                for (String key : snmpResponses.keySet())
                {
                    matchFilterCriteria(params, snmpResponses.get(key), filter);
                }
            }
        }
    }

    private boolean match(Long value, String comparator, Long numericValue)
    {
      
        boolean result = false;
        if ((comparator != null) && (!comparator.isEmpty()))
        {
            if (comparator.equals("!="))
            {
                result = numericValue.compareTo(value) != 0;
            }
            if (comparator.equals(">"))
            {
                result = numericValue.compareTo(value) > 0;
            }
            if (comparator.equals(">="))
            {
                result = numericValue.compareTo(value) > 0 || numericValue.compareTo(value) == 0;
            }
            if (comparator.equals("="))
            {
                result = numericValue.compareTo(value) == 0;
            }
            if (comparator.equals("<="))
            {
                result = numericValue.compareTo(value) < 0 || numericValue.compareTo(value) == 0;
            }
            if (comparator.equals("<"))
            {
                result = numericValue.compareTo(value) < 0;
            }
        }

        return result;
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> result)
    {
        // nothing at this point.
    }

    @Override
    public void processPdu(CommandResponderEvent event)
    {
        Address peerAddress = event.getPeerAddress();

        PDU trap = event.getPDU();
        String snmpTrapOid = SNMPUtils.getSnmpTrapOID(trap);

        Vector variableBindings = trap.getVariableBindings();
        if (snmpTrapOid != null && variableBindings != null && variableBindings.size() > 0)
        {
            Map<String, SNMPResponse> responses = new HashMap<String, SNMPResponse>();

            // collect all the SNMPResponses to be matched by filter criteria.
            for (int i = 0; i < variableBindings.size(); i++)
            {
                VariableBinding binding = (VariableBinding) variableBindings.get(i);
                // binding.get
                SNMPResponse response = SNMPUtils.getSnmpResponse(peerAddress, snmpTrapOid, binding);

                responses.put(response.getOid(), response);
            }

            // go through the Filter list and see anyone is interested in the
            // Trap.
            for (Filter filter : orderedFilters)
            {
                SNMPFilter snmpFilter = (SNMPFilter) filter;
                if (snmpFilter.isTrapReceiver())
                {
                    Map<String, String> runbookParams = new HashMap<String, String>();
                    matchTrapFilterCriteria(runbookParams, responses, snmpFilter, snmpTrapOid);
                    if (runbookParams.size() > 0)
                    {
                        runbookParams.put(Constants.EVENT_EVENTID, snmpFilter.getEventEventId());
                        runbookParams.put(DEVICE_ID, peerAddress.toString());
                        //processFilter(filter, null, runbookParams);
                        addToPrimaryDataQueue(filter, runbookParams);
                    }
                }
            }
        }
    }

    /*
     * Public API method implementation goes here
     */

    public boolean sendTrap(int version, String deviceIp, int port, int retries, long timeout, String writeCommunity, String snmpTrapOid, List<SNMPData> traps) throws Exception
    {
        ConfigReceiveSNMP snmpConfigurations = (ConfigReceiveSNMP) configurations;

        if (version < 1 || version > 3)
        {
            throw new Exception("Unsupported SNMP version: " + version + ", only version 1,2,3 is supported");
        }
        
        boolean result = true;

        // although the user sends actual version number like 1,2, or 3
        // internally
        // their numbers are 0, 1, and 2
        version = version - 1;

        if (StringUtils.isEmpty(writeCommunity))
        {
            writeCommunity = snmpConfigurations.getSendtrap_writecommunity();
        }
        retries = (retries <= 0 ? snmpConfigurations.getSendtrap_retries() : retries);
        timeout = (timeout <= 0 ? snmpConfigurations.getSendtrap_timeout() * 1000 : timeout * 1000);

        SNMPUtils.sendTrap(version, deviceIp, port, retries, timeout, writeCommunity, snmpTrapOid, traps);

        return result;
    }

    public SNMPResponse get(int version, String deviceIp, int port, String readCommunity, int retries, long timeout, String oidValue) throws Exception
    {
        ConfigReceiveSNMP snmpConfigurations = (ConfigReceiveSNMP) configurations;

        if (version < 1 || version > 3)
        {
            throw new Exception("Unsupported SNMP version: " + version + ", only version 1,2,3 is supported");
        }

        // although the user sends actual version number like 1,2, or 3
        // internally
        // their numbers are 0, 1, and 2
        version = version - 1;

        if (StringUtils.isEmpty(readCommunity))
        {
            readCommunity = snmpConfigurations.getReadcommunity();
        }
        retries = (retries <= 0 ? snmpConfigurations.getSendtrap_retries() : retries);
        timeout = (timeout <= 0 ? snmpConfigurations.getSendtrap_timeout() * 1000 : timeout * 1000);

        return SNMPUtils.get(version, deviceIp, port, readCommunity, retries, timeout, oidValue);
    }

    public SNMPResponse getNext(int version, String deviceIp, int port, String readCommunity, int retries, long timeout, String oid) throws Exception
    {
        ConfigReceiveSNMP snmpConfigurations = (ConfigReceiveSNMP) configurations;

        if (version < 1 || version > 3)
        {
            throw new Exception("Unsupported SNMP version: " + version + ", only version 1,2,3 is supported");
        }

        // although the user sends actual version number like 1,2, or 3
        // internally
        // their numbers are 0, 1, and 2
        version = version - 1;

        if (StringUtils.isEmpty(readCommunity))
        {
            readCommunity = snmpConfigurations.getReadcommunity();
        }
        retries = (retries <= 0 ? snmpConfigurations.getSendtrap_retries() : retries);
        timeout = (timeout <= 0 ? snmpConfigurations.getSendtrap_timeout() * 1000 : timeout * 1000);

        return SNMPUtils.getNext(version, deviceIp, port, readCommunity, retries, timeout, oid);
    }

    public boolean set(int version, String deviceIp, int port, String writeCommunity, int retries, long timeout, String oid, String oidValue, String valueType) throws Exception
    {
        ConfigReceiveSNMP snmpConfigurations = (ConfigReceiveSNMP) configurations;

        if (version < 1 || version > 3)
        {
            throw new Exception("Unsupported SNMP version: " + version + ", only version 1,2,3 is supported");
        }
        boolean result = true;

        // although the user sends actual version number like 1,2, or 3
        // internally
        // their numbers are 0, 1, and 2
        version = version - 1;

        if (StringUtils.isEmpty(writeCommunity))
        {
            writeCommunity = snmpConfigurations.getSendtrap_writecommunity();
        }
        retries = (retries <= 0 ? snmpConfigurations.getSendtrap_retries() : retries);
        timeout = (timeout <= 0 ? snmpConfigurations.getSendtrap_timeout() * 1000 : timeout * 1000);

        result = SNMPUtils.set(version, deviceIp, port, writeCommunity, retries, timeout, oid, oidValue, valueType);

        return result;
    }
}
