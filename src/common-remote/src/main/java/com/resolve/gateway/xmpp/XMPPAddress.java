/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.xmpp;

import com.resolve.util.CryptUtils;
import com.resolve.util.Log;

public class XMPPAddress
{
    // Field names for this pool used by DBGateway related classes.
    public static final String XMPPADDRESS = "XMPPADDRESS";
    public static final String XMPPP_ASSWORD = "XMPPPASSWORD";
    public static final String QUEUE = "QUEUE";

    private String xmppAddress;
    private String xmppPassword;

    public XMPPAddress(String xmppAddress, String xmppPassword)
    {
        super();
        this.xmppAddress = xmppAddress;
        try
        {
            this.xmppPassword = CryptUtils.decrypt(xmppPassword);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
        }
    }

    public void setAddress(String xmppAddress, String xmppPassword)
    {
        this.xmppAddress = xmppAddress;
        try
        {
            this.xmppPassword = CryptUtils.decrypt(xmppPassword);
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
        }
    }

    public String getXmppAddress()
    {
        return xmppAddress;
    }

    public void setXmppAddress(String xmppAddress)
    {
        this.xmppAddress = xmppAddress;
    }
    
    public String getXmppPassword()
    {
        return xmppPassword;
    }

    public void setXmppPassword(String xmppPassword)
    {
        this.xmppPassword = xmppPassword;
    }
}
