/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.database.DBFilter;
import com.resolve.gateway.database.DBGateway;
import com.resolve.gateway.database.DBGatewayFactory;
import com.resolve.gateway.database.DBPool;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MDatabase extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MDatabase.setFilters";
    private static final String RSCONTROL_SET_POOLS = "MDatabase.setPools";

    private static final DBGateway instance = DBGatewayFactory.getInstance();

    public MDatabase()
    {
        super.instance = instance;
    }

    @Override
    protected void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        if (instance.isActive())
        {
            DBFilter dbFilter = (DBFilter) filter;
            filterMap.put(DBFilter.POOL, dbFilter.getPool());
            filterMap.put(DBFilter.PREFIX, dbFilter.getPrefix());
            filterMap.put(DBFilter.LASTVALUECOLUMN, dbFilter.getLastValueColumn());
            filterMap.put(DBFilter.LASTVALUEQUOTE, "" + dbFilter.isLastValueQuote());
            filterMap.put(DBFilter.LASTVALUE, "" + dbFilter.getLastValue());
            filterMap.put(DBFilter.SQL, "" + dbFilter.getSql());
            filterMap.put(DBFilter.UPDATESQL, "" + dbFilter.getUpdateQuery());
        }
    }

    @Override
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    /**
     * This method is invoked once filter/pool synchronization message from
     * RSCONTROL is received.
     * 
     * @param paramList
     *            the list of synchronization parameters, particularly the POOLS
     *            and FILTERS used in the gateway
     * 
     */
    @Override
    public void synchronizeGateway(Map<String, List<Map<String, Object>>> paramList)
    {
        if (paramList != null)
        {
            //synchronize all the common data like filter, name properties, routing schemas
            super.synchronizeGateway(paramList);
            
            //super class method sets them to be true, however the last step may fail
            isUpdateRSControl = false;
            instance.setSyncDone(false);
            
            List<Map<String, Object>> pools = (List<Map<String, Object>>) paramList.get("POOLS");

            Log.log.debug("NUMBER of pools " + (pools != null ? pools.size() : 0));
            
            try
            {
                clearAndSetPools(pools);
            }catch(Exception e)
            {
                Log.log.warn("Failed to clear and set pools due to " + e.getMessage() + 
                             ", ignoring the error, setting sync done and consuming synchronizeGateway message from RSControl with " +
                              "parameters [" + paramList + "]", e);
            }
            
            isUpdateRSControl = true;
            instance.setSyncDone(true);
        }
    }

    /**
     * Clears the current pools and reinitializes new ones given the paramList
     * 
     * @param paramList
     *            the list of initialization parameters for the new pools
     * 
     */
    @FilterCompanionModel(modelName = "DatabaseConnectionPool")
    public void clearAndSetPools(List<Map<String, Object>> paramList)
    {
        // clear and set pools
        instance.clearAndSetPools(paramList);

        // send new pools
        getPools(null);

        // save config
        MainBase.main.exitSaveConfig();
    } // clearAndSetPools

    /**
     * Sets the database connection pool to be used by the database
     * 
     * @param params
     *            the list of initialization parameters for the new pool
     * 
     */
    public void setPool(Map<String, Object> params)
    {
        instance.setPool(params);

        // save config
        MainBase.main.exitSaveConfig();

    } // setPool

    /**
     * Sends an internal message to RSControl that lists all database pool
     * information.
     * 
     * @param params
     *            the list of initialization parameters for the new pool
     * 
     */
    public void getPools(Map<String, String> params)
    {
        if (instance.isActive())
        {
            Map<String, String> content = new HashMap<String, String>();

            if (instance.getPools() != null)
            {
                for (DBPool pool : instance.getPools().values())
                {
                    Map<String, String> poolMap = new HashMap<String, String>();
                    poolMap.put(DBPool.QUEUE, instance.getQueueName());
                    poolMap.put(DBPool.NAME, pool.getName());
                    poolMap.put(DBPool.DRIVERCLASS, pool.getDriverClass());
                    poolMap.put(DBPool.CONNECTURI, pool.getConnectURI());
                    poolMap.put(DBPool.MAXCONNECTION, "" + pool.getMaxConnection());
                    poolMap.put(DBPool.USERNAME, pool.getDbUserName());
                    poolMap.put(DBPool.P_ASSWORD, pool.getDbP_assword());
                    content.put(pool.getName(), StringUtils.remove(StringUtils.mapToString(poolMap), "null"));
                }

                // send content to rscontrol
                //if (content.size() > 0 && isUpdateRSControl)
                if (isUpdateRSControl)
                {
                    //this is important as RSControl need this
                    content.put("QUEUE", instance.getQueueName());

                    if (MainBase.esb.sendInternalMessage(Constants.ESB_NAME_RSCONTROL, RSCONTROL_SET_POOLS, content) == false)
                    {
                        Log.log.warn("Failed to send connection pools to RSCONTROL");
                    }
                }
            }
        }
    } // getPools
} // MDatabase
