/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.servicenow;

import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import  java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.utils.URIBuilder;
import org.owasp.esapi.ESAPI;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.restclient.ConfigProxy;

/**
 * The is a convenience class for consuming ServiceNow SOAP web services. This
 * is most generic and lowest level class used by more specific ServiceNow
 * object based (e.g., incident, problem) classes.
 */
public class SoapCaller
{
    private String url; 
    private List<ConnectionModifier> connectionModifiers = new LinkedList<SoapCaller.ConnectionModifier>();
    private ConnectionOpener connectionOpener = null;
    
    /**
     * Constructor
     * 
     * @param url
     *            in the form of
     *            https://demo08.service-now.com:443/GetData.do?SOAP
     */
    public SoapCaller(String url, ConnectionOpener opener,  ConnectionModifier ... modifiers)
    {
        this.url = url;
        this.connectionModifiers.addAll(Arrays.asList(modifiers));
        this.connectionOpener = opener;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }
 
    @SuppressWarnings("unchecked")
    public <T extends ConnectionModifier> T getModifier(Class<T> clzz)
    {
        for(ConnectionModifier c : connectionModifiers)
        {
            if(clzz.isInstance(c))
            {
                return (T) c;
            }
        }
        return null;
        
    }
    public <T extends ConnectionModifier> T removeModifier(Class<T> clzz)
    {
        T m = getModifier(clzz);
        
        if(m != null)
           connectionModifiers.remove(m);
        
        return m;
        
    }
    public void setUsernameAndPassword(String username, String password)
    {
        
        if(StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            
                removeModifier(HttpBasicAuthenticationModifier.class);
            
        }
        
        HttpBasicAuthenticationModifier mod =  getModifier(HttpBasicAuthenticationModifier.class);
        if(mod == null)
        {
               
               mod = new HttpBasicAuthenticationModifier(username, password);
               connectionModifiers.add(0, mod);
            
        }else
        {
            mod.setHttpbasicauthpassword(password);
            mod.setHttpbasicauthusername(username);
        }
        
        
    }
    
    
    public String sendSoapRequest(String soapEnvelope, String objecType)
    {
      
        try
        {
            
            
             return _sendSoapRequest(  soapEnvelope,   objecType);
             
        }
        catch (Exception e)
        {
            Log.log.error("Error sending SOAP request to the Server:" + e.getMessage(), e);
        }
         return null;
         
    }

    public String _sendSoapRequest(String soapEnvelope, String objecType) throws Exception
    {
        String response = null;
        URL url;
       
            
            
            url = new URL(this.url + (this.url.endsWith("/")? "": "/") + objecType + ".do?SOAP");
            URL intialUrl = url;
            
            for(ConnectionModifier m : this.connectionModifiers)
            {
                
                url =  m.modifyConnectionURL(url);
            }
           
            
            HttpURLConnection connection = (HttpURLConnection) openConnection(url);
         
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            int len = soapEnvelope.length();
            connection.setRequestProperty("Content-Length", Integer.toString(len));
                
            
            for(ConnectionModifier m : this.connectionModifiers)
            {
                
               connection =  m.modifyConnection(connection, intialUrl);
            }
           
            connection.connect();
            
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(ESAPI.encoder().encodeForXML(soapEnvelope), 0, len);
            out.flush();
            
            
            
            try (InputStream in = connection.getInputStream())
            { 
                response =  IOUtils.toString(in);
            } 
          
            
            connection.disconnect();
 
        return response;
    }
    protected URLConnection openConnection(URL url) throws Exception
    {
        return this.connectionOpener.openConnection(url);
    }

     
    
    
    public interface ConnectionModifier
    {
        
        public HttpURLConnection modifyConnection(HttpURLConnection urlConnection, URL url) throws Exception;

        public URL modifyConnectionURL(URL url);
        
        
    }
    public interface ConnectionOpener
    {
        
        public HttpURLConnection openConnection(URL url)  throws Exception;
        
        
    }
    public static class DefaultConnectionOpener implements ConnectionOpener
    {

        @Override
        public HttpURLConnection openConnection(URL url) throws Exception
        {
            
            return (HttpURLConnection) url.openConnection();
        }
        
        
        
    }
    
    
    
    public static class ProxyConnectionOpener implements ConnectionOpener
    {
        public String proxyHost;
        public int proxyPort;
        public String proxyType;
        
        public String username;
        public String password;
        
        
        public ProxyConnectionOpener(String proxyHost, int proxyPort, String proxyType)
        {
            super();
            this.proxyHost = proxyHost;
            this.proxyPort = proxyPort;
            this.proxyType = proxyType;

        }
        
        
        
        public ProxyConnectionOpener(String proxyHost, int proxyPort, String proxyType, String username, String password)
        {
            super();
            this.proxyHost = proxyHost;
            this.proxyPort = proxyPort;
            this.proxyType = proxyType;
            this.username = username;
            this.password = password;
        }



        public ProxyConnectionOpener(ConfigProxy configProxy)
        {
                this.password = configProxy.getProxyP_assword();
                this.username = configProxy.getProxyUser();
                this.proxyHost = configProxy.getProxyHost();
                this.proxyPort = configProxy.getProxyPort();
                this.proxyType = "HTTP";

        }



        public String getUsername()
        {
            return username;
        }



        public void setUsername(String username)
        {
            this.username = username;
        }



        public String getPassword()
        {
            return password;
        }



        public void setPassword(String password)
        {
            this.password = password;
        }



        public String getProxyHost()
        {
            return proxyHost;
        }



        public void setProxyHost(String proxyHost)
        {
            this.proxyHost = proxyHost;
        }



        public int getProxyPort()
        {
            return proxyPort;
        }



        public void setProxyPort(int proxyPort)
        {
            this.proxyPort = proxyPort;
        }



        @Override
        public HttpURLConnection openConnection(URL url) throws Exception
        {
            
            Proxy p = new Proxy(Proxy.Type.valueOf(proxyType), new InetSocketAddress(proxyHost, proxyPort));
           
           
            return (HttpURLConnection) url.openConnection(p);
        }
        
        
        
    }
    
    
    public static class HttpProxyAuthModifier implements ConnectionModifier
    {
        String username, password;
        
        
        public HttpProxyAuthModifier(String username, String password)
        {
            super();
            this.username = username;
            this.password = password;
        }

        @Override
        public HttpURLConnection modifyConnection(HttpURLConnection urlConnection, URL url) throws Exception
        {
             
            if(!StringUtils.isNotEmpty(username))
            {
                
                System.setProperty("http.proxyUser", username);
                System.setProperty("http.proxyPassword", password);
                
                String encoded =  com.resolve.util.Base64.encodeBytes(StringUtils.join(new String[] {username, password} , ":").getBytes());
                urlConnection.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
            }
            return urlConnection; 
            
            
        }

        @Override
        public URL modifyConnectionURL(URL url)
        {
        
            return url;
        }
        
        
        
        
        
        
        
    }
    public static class HttpHeaderFieldRedirectModifier implements ConnectionModifier
    {
        
        String redirectHeaderName;
        URL redirectUrl;
        boolean moveAuthToUrl = false;
        
        
        public HttpHeaderFieldRedirectModifier(String redirectHeaderName, URL redirectUrl, boolean moveAuthToUrl)
        {
            super();
            this.redirectHeaderName = redirectHeaderName;
            this.redirectUrl = redirectUrl;
            this.moveAuthToUrl = moveAuthToUrl;
        }

        @Override
        public HttpURLConnection modifyConnection(HttpURLConnection urlConnection, URL url) throws Exception
        {
            String encoded = urlConnection.getRequestProperty("Authorization");
            if(encoded != null  && moveAuthToUrl)
            {
                urlConnection.getRequestProperties().remove("Authorization");
                
           
                String decoded =  new String(java.util.Base64.getDecoder().decode(StringUtils.removeStartIgnoreCase(encoded,  "Basic ")));
                
                String[] split = StringUtils.splitByWholeSeparator(decoded,":");
                
                if(split.length > 1)
                {
                    URIBuilder builder = new URIBuilder(url.toString());
                    builder.setUserInfo(split[0], split[1]);
     
                    url = builder.build().toURL();
                    
                }
                
                
                
            }
            
            urlConnection.setRequestProperty(redirectHeaderName, url.toString());
 
            return urlConnection;
        }

        @Override
        public URL modifyConnectionURL(URL url)
        {
             return redirectUrl;
        }
        
        
        
        
    }
    
    public static class MutualTlsModifier implements ConnectionModifier
    {
        String keystoreFile;
        String keystoreFilePassword;
        
        String truststoreFile;
        String truststoreFilePassword;
            
        
        public MutualTlsModifier(String keystoreFile, String keystoreFilePassword, String truststoreFile, String truststoreFilePassword)
        {
            super();
            this.keystoreFile = keystoreFile;
            this.keystoreFilePassword = keystoreFilePassword;
            this.truststoreFile = truststoreFile;
            this.truststoreFilePassword = truststoreFilePassword;
        }

        @Override
        public HttpURLConnection modifyConnection(HttpURLConnection urlConnection, URL url) throws Exception
        {
            
            if(urlConnection instanceof HttpsURLConnection)
            {
                SSLContext context = new SSLContextHelper(keystoreFile, keystoreFilePassword, truststoreFile, truststoreFilePassword).getSSLContext();
                SSLSocketFactory fact = context.getSocketFactory();
                ((HttpsURLConnection) urlConnection).setSSLSocketFactory(fact);
                
            }
            return urlConnection;
        }
        @Override
        public URL modifyConnectionURL(URL url)
        {
        
            return url;
        }
        
       
    }
    
    public static class HttpBasicAuthenticationModifier implements ConnectionModifier
    {
        
        public String httpbasicauthusername;
        public String httpbasicauthpassword;
        

        public HttpBasicAuthenticationModifier(String httpbasicauthusername, String httpbasicauthpassword)
        {
            super();
            this.httpbasicauthusername = httpbasicauthusername;
            this.httpbasicauthpassword = httpbasicauthpassword;
        }


        public String getHttpbasicauthusername()
        {
            return httpbasicauthusername;
        }


        public void setHttpbasicauthusername(String httpbasicauthusername)
        {
            this.httpbasicauthusername = httpbasicauthusername;
        }


        public String getHttpbasicauthpassword()
        {
            return httpbasicauthpassword;
        }


        public void setHttpbasicauthpassword(String httpbasicauthpassword)
        {
            this.httpbasicauthpassword = httpbasicauthpassword;
        }


        @Override
        public HttpURLConnection modifyConnection(HttpURLConnection connection, URL u)
        { 
            if (StringUtils.isNotEmpty(httpbasicauthusername) && StringUtils.isNotEmpty(httpbasicauthpassword))
            {
                String authValue = this.httpbasicauthusername + ":" + this.httpbasicauthpassword;
           
                String encodedAuthorization = new String(Base64.encodeBase64(authValue.getBytes())); // converted
                                                                                                        // to
                                                                                                        // apache
                                                                                                        // encoding (what is that??)
                connection.setRequestProperty("Authorization", "Basic " + encodedAuthorization);
            }
            
            return connection;
        }


        @Override
        public URL modifyConnectionURL(URL url)
        {
          
            return url;
        }
        
        
        
    }
    
    
    
}
