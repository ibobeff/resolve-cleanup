/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.netcool;

import groovy.lang.Binding;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.owasp.esapi.ESAPI;

import com.resolve.gateway.BaseClusteredGateway;
import com.resolve.gateway.Filter;
import com.resolve.gateway.MNetcool;
import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveNetcool;
import com.resolve.thread.ScheduledExecutor;
import com.resolve.util.Constants;
import com.resolve.util.ERR;
import com.resolve.util.Log;
import com.resolve.util.MessageDispatcher;
import com.resolve.util.ResolveDefaultValidator;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;
import com.resolve.util.ThreadUtils;
import com.resolve.util.metrics.ExecutionMetrics;
import com.resolve.util.queue.QueueListener;
import com.resolve.util.queue.QueueManager;

public class NetcoolGateway extends BaseClusteredGateway
{
    private static final String NETCOOL_SERVER_SERIAL = "NETCOOL_SERVER_SERIAL";

    private static final String NETCOOL_SERIAL = "NETCOOL_SERIAL";

    private static final String NETCOOL_SERVER_NAME = "NETCOOL_SERVER_NAME";

    // Singleton
    private static volatile NetcoolGateway instance = null;

    private final static int STATE_PASSIVE_CONNECT = 0;
    private final static int STATE_ACTIVE_PRIMARY = 1;
    private final static int STATE_ACTIVE_BACKUP = 2;

    private final static int MAX_RETRY = 3;

    // Following values come from the blueprint.properties.
    private boolean updateResolveStatus = false;
    //private String resolveStatusValue;
    //private String runbookIdKey = "RESOLVE_RUNBOOKID";
    private boolean updateRunbookId = false;
    private boolean checkNetcoolConnectionIsValid = false;
    private long minEvictableIdleTimeMillis = 10 * 60 * 1000;
    private long timeBetweenEvictionRunsMillis = 5 * 60 * 1000;

    private int UID = 0;
    private String queue = "NETCOOL";
    private ConcurrentLinkedQueue<String> writeQueue = null;
    private String version;
    private String PRIMARY_POOL = "PrimaryObjectServer";
    private String SECONDARY_POOL = "SecondaryObjectServer";
    private String statusFieldname = "ResolveStatus";
    private String statusProcess = "1";
    private String statusType = "NUMBER";
    private String runbookIdFieldname = "RunbookId";

    private int state = STATE_PASSIVE_CONNECT;
    private int retryDelay = 5000;
    private int reconnectDelay = 60000;
    private boolean hasBackupOS = false;
    private int maxConnection = 10;

    @SuppressWarnings("rawtypes")
    private ConcurrentHashMap<String, GenericObjectPool> connectionPool = new ConcurrentHashMap<String, GenericObjectPool>();

    // boolean running = false;
    private boolean connChanged = true;

    private Connection conn;
    //private final Object lock = new Object();

    private String url;
    private String url2;
    private String lastUrl;
    private String username;
    private String password;
    private long lastSerial;
    private long lastServerSerial;
    private boolean reinitLastValues;
    private long initTS;
    private long diffTS;

    private long eventCounter;

    private final ConfigReceiveNetcool netcoolConfiguration;
  //Properties for JDBC Driver Manager  JIRA #RS-25998
    Properties connProps = new Properties();
    private String appName;

    public static NetcoolGateway getInstance(ConfigReceiveNetcool config)
    {
        if (instance == null)
        {
            instance = new NetcoolGateway(config);
        }
        return instance;
    }

    /**
     * This method is called once the initialization is done. If it gets called
     * before the initialization it will throw exception.
     *
     * @return
     */
    public static NetcoolGateway getInstance()
    {
        if (instance == null)
        {
            throw new IllegalStateException("Netcool Gateway is not initialized correctly..");
        }
        else
        {
            return instance;
        }
    }

    private NetcoolGateway(ConfigReceiveNetcool config)
    {
        super(config);
        debug = config.isDebug();
        queue = config.getQueue();
        netcoolConfiguration = config;
    } // NetcoolGateway

    @Override
    protected void initialize()
    {
        try
        {
            queue = netcoolConfiguration.getQueue().toUpperCase();
            interval = netcoolConfiguration.getInterval() * 1000;
            Log.log.debug("NetcoolGateway: queue name -->" + queue);
            statusFieldname = netcoolConfiguration.getStatus_fieldname();
            updateResolveStatus = netcoolConfiguration.isStatus_active();
            //resolveStatusValue = "" + netcoolConfiguration.getStatus_process();
            Log.log.debug("NetcoolGateway: statusFieldname -->" + statusFieldname);
            Log.log.debug("NetcoolGateway: updateResolveStatus -->" + updateResolveStatus);
            updateRunbookId = netcoolConfiguration.isRunbookid_active();
            Log.log.debug("NetcoolGateway: updateRunbookId -->" + updateRunbookId);
            runbookIdFieldname = netcoolConfiguration.getRunbookid_fieldname();
            Log.log.debug("NetcoolGateway: runbookIdFieldname -->" + runbookIdFieldname);
            statusProcess = netcoolConfiguration.getStatus_process();
            Log.log.debug("NetcoolGateway: statusProcess -->" + statusProcess);
            statusType = netcoolConfiguration.getStatus_type();
            
            if (StringUtils.isBlank(statusType) ||
            		(!"NUMBER".equalsIgnoreCase(statusType) && !"STRING".equalsIgnoreCase(statusType)))
            {
            	Log.log.error("NetcoolGateway: Invalid Status Type " + statusType + ": Value must be"            	            			+ " NUMBER or STRING");            	statusType = "NUMBER";
            }
            else
            {
            	statusType = statusType.toUpperCase();
            }

            debug = netcoolConfiguration.isDebug();
            retryDelay = netcoolConfiguration.getRetryDelay() * 1000;
            reconnectDelay = netcoolConfiguration.getReconnectDelay() * 1000;
            
            minEvictableIdleTimeMillis = netcoolConfiguration.getMinevictableidletime() * 1000;
            timeBetweenEvictionRunsMillis = netcoolConfiguration.getTimebetweenevictionruns() * 1000;
            
            checkNetcoolConnectionIsValid = netcoolConfiguration.isActiveconnectionvalidation();

            // init settings
            version = netcoolConfiguration.getVersion();

            maxConnection = netcoolConfiguration.getPoolsize();
            this.gatewayConfigDir = "/config/netcool/";

            this.lastSerial = 0;
            this.lastServerSerial = 0;
            this.connChanged = true;

            // init ObjectServer connection
            String driver = netcoolConfiguration.getDriver();
            if (StringUtils.isEmpty(driver)) driver = "com.sybase.jdbc3.jdbc.SybDriver";
            Log.log.debug("NetcoolGateway:Netcool Driver not found in blueprint using "+driver);
            
            try {
            	Class.forName(driver);
            }
            catch(ClassNotFoundException e) {
            	Log.log.error("NetcoolGateway:Could not find JDBC driver "+driver);
            }

            // init primary objectserver url
            this.url = netcoolConfiguration.getUrl();
 			Log.log.debug("NetcoolGateway: statusProcess -->" + this.url);
            if (StringUtils.isBlank(this.url) && StringUtils.isNotBlank(netcoolConfiguration.getIpaddress()))
            {
                this.url = "jdbc:sybase:Tds:" + netcoolConfiguration.getIpaddress() + ":" + netcoolConfiguration.getPort() + "?LITERAL_PARAMS=true";
            }
            Log.log.debug("NetcoolGateway: URL is -->" + this.url);
            // init backup objectserver url if defined
            if (!StringUtils.isBlank(netcoolConfiguration.getUrl2()))
            {
                this.url2 = netcoolConfiguration.getUrl2();
                hasBackupOS = true;
            }
            if (StringUtils.isBlank(this.url2) && StringUtils.isNotBlank(netcoolConfiguration.getIpaddress2()))
            {
                this.url2 = "jdbc:sybase:Tds:" + netcoolConfiguration.getIpaddress2() + ":" + netcoolConfiguration.getPort2() + "?LITERAL_PARAMS=true";
                hasBackupOS = true;
            }

            this.username = netcoolConfiguration.getUsername();
            this.password = netcoolConfiguration.getPassword();
            this.lastSerial = 0;
            this.lastServerSerial = 0;
            this.connChanged = true;

            //Properties for JDBC Driver Manager  JIRA #RS-25998
            connProps.put("user", username);
            connProps.put("password", password);
            this.appName = netcoolConfiguration.getApplicationname();
            if (!StringUtils.isBlank(this.appName))
            {
                    connProps.put("APPLICATIONNAME", this.appName);
            }
            
            // Add listener;
            QueueListener<String> emailListener = new UpdateSqlListener();
            writeQueue = QueueManager.getInstance().getResolveConcurrentLinkedQueue(emailListener);

            checkDeadlock();
            ScheduledExecutor.getInstance().executeRepeat(getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + "-CheckDeadLock", this, "checkDeadlock", 30, TimeUnit.SECONDS);

        }
        catch (Exception e)
        {
            Log.log.warn(gatewayClassName+":"+e.getMessage(), e);
            throw e;
        }
    }

    private void checkDeadlock()
    {
        ThreadUtils.detectDeadlocks();
        //ScheduledExecutor.getInstance().executeDelayed(this, "checkDeadlock", 30, TimeUnit.SECONDS);
    }

    @Override
    public Filter getFilter(Map<String, Object> params)
    {
        return new NetcoolFilter((String) params.get(NetcoolFilter.ID), (String) params.get(NetcoolFilter.ACTIVE), (String) params.get(NetcoolFilter.ORDER), (String) params.get(NetcoolFilter.INTERVAL), (String) params.get(NetcoolFilter.EVENT_EVENTID), (String) params.get(NetcoolFilter.RUNBOOK), (String) params.get(NetcoolFilter.SCRIPT), (String) params.get(NetcoolFilter.SQL));
    }

    @Override
    public void clearAndSetFilters(List<Map<String, Object>> filterList)
    {
        super.clearAndSetFilters(filterList);
        try
        {
            for (Filter filter : orderedFilters)
            {
                NetcoolFilter netcoolFilter = (NetcoolFilter) filter;
                /* if this is newly deployed filter then get last serial #, 
                 * server serial # and last successful query time stamp
                 * from the server. 
                 */
                if(netcoolFilter.getLastSerial() == 0)
                {
                    initTimestamp();
                    break;
                }
            }
        }
        catch (Exception e)
        {
        	Log.log.error("NetcoolGateway: Error when doing  clearAndSetFilters");
            Log.log.error(e.getMessage(), e);
        }
    }
    
    @Override
    public String getLicenseCode()
    {
        return "NETCOOL";
    }

    @Override
    protected String getGatewayEventType()
    {
        return Constants.GATEWAY_EVENT_TYPE_NETCOOL;
    }

    @Override
    protected String getMessageHandlerName()
    {
        return MNetcool.class.getSimpleName();
    }

    @Override
    protected Class<MNetcool> getMessageHandlerClass()
    {
        return MNetcool.class;
    }

    @Override
    public String getQueueName()
    {
        return queue + getOrgSuffix();
    }

    private boolean connect()
    {
        boolean result = false;

        Log.log.info("NetcoolGateway: Current state: " + getStateString());
        if ((running && isActive()) || (isWorker() && !isPrimary()))
        {
            // try connecting to primary
        	Log.log.info("NetcoolGateway: Attempting to connect with PRIMARY Netcool ObjectServer");            if (connectObjectServer(url))
            {
                state = STATE_ACTIVE_PRIMARY;
                Log.log.info("NetcoolGateway: New state --> " + getStateString());
                initPrimaryPool();
                result = true;
            }
            else
            {
                if (hasBackupOS)
                {
                    // already connected (just attempting primary above)
                    if (state == STATE_ACTIVE_BACKUP)
                    {
                        result = true;
                    }

                    // attempt to connect to backup
                    else
                    {
                    	Log.log.info("NetcoolGateway: Attempting to connect with BACKUP Netcool ObjectServer");                        if (connectObjectServer(url2))
                        {
                            state = STATE_ACTIVE_BACKUP;
                    		Log.log.info("NetcoolGateway: Attempting to connect with BACKUP Netcool ObjectServer");                            initSecondaryPool();
                            result = true;
                        }
                        else
                        {
                            state = STATE_PASSIVE_CONNECT;
                            Log.log.info("NetcoolGateway: New state --> " + getStateString());                            Log.alert(ERR.E50001);

                            result = false;
                        }
                    }
                }
                else
                {
                    result = false;
                }
            }
        }

        return result;
    } // connect

	private void closeConnection(Connection con) {
		if (con == null) {
			return;
		}

		try {
			con.close();
		} catch (Throwable ex){
			// ignore
		}
	}

    public boolean enqueue(String parameters)
    {
        return writeQueue.offer(parameters);
    }

    @SuppressWarnings("rawtypes")
    private class NetcoolConnFactory extends BasePoolableObjectFactory
    {
        String sqlCheck = "SELECT TOP 1 Severity From alerts.colors;";
        
        @Override
        public Object makeObject() throws Exception
        {
            Connection newconn = null;
            if (state == STATE_ACTIVE_PRIMARY)
            {
              //Driver Manager change  JIRA #RS-25998
               // newconn = DriverManager.getConnection(url, username, password);
                 newconn = DriverManager.getConnection(url, connProps);
            }
            else if (state == STATE_ACTIVE_BACKUP)
            {
              //Driver Manager manager  JIRA #RS-25998
              //  newconn = DriverManager.getConnection(url2, username, password);
                 newconn = DriverManager.getConnection(url2, connProps);
            }
            return newconn;
        }

        @Override
        public void destroyObject(Object obj) throws Exception
        {
        	Log.log.info("NetcoolGateway: Destroying connection object");            if (obj != null && !((Connection)obj).isClosed())
            {
                try
                {
                    ((Connection) obj).close();
                }
                catch (SQLException sqle)
                {
                	Log.log.error("NetcoolGateway: Error Destroying Connection", sqle);                }
            }
        }

        @Override
        public boolean validateObject(Object obj)
        {
        	Log.log.info("NetcoolGateway: Validating Connection Object");            boolean b = false;
            Connection conn = (Connection) obj;
            Statement s = null;
            try
            {
                s = conn.createStatement();
                s.setQueryTimeout(10);
                s.execute(sqlCheck);
                b = true;
            }
            catch (Throwable ex)
            {
                b = false;
                Log.log.error("NetcoolGateway: Connection Validation Failed", ex);
            }
            finally
            {
                try
                {
                    if (s != null)
                    {
                        s.close();
                    }
                }
                catch (SQLException sqle)
                {
                    // do nothing
                }
            }
            return b;
        }
    }

    private void initPrimaryPool()
    {
        // initConnectionPool(this.url, maxConnection, PRIMARY_POOL);
        initCustomizedConnectionPool(PRIMARY_POOL);
    }

    private void initSecondaryPool()
    {
        // initConnectionPool(this.url2, maxConnection, SECONDARY_POOL);
        initCustomizedConnectionPool(SECONDARY_POOL);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void initCustomizedConnectionPool(String poolName)
    {
        if (connectionPool.get(poolName) != null)
        {
            return;
        }

        GenericObjectPool pool = new GenericObjectPool(new NetcoolConnFactory());

        // If maximum connection number
        pool.setMaxActive(maxConnection);
        if (checkNetcoolConnectionIsValid)
        {
            pool.setTestOnBorrow(true);
        }
        pool.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        pool.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        connectionPool.put(poolName, pool);
    }

    private Connection getCustomizedConnection()
    {
        if (state == STATE_ACTIVE_PRIMARY)
        {
            return getCustomizedConnection(PRIMARY_POOL);
        }
        else if (state == STATE_ACTIVE_BACKUP)
        {
            return getCustomizedConnection(SECONDARY_POOL);
        }
        else
        {
            throw new RuntimeException("Failed to get valid connection. Netcool gateway is not active.");
        }
    }

    private Connection returnCustomizedConnection(Connection conn)
    {
        if (state == STATE_ACTIVE_PRIMARY)
        {
            return returnCustomizedConnection(PRIMARY_POOL, conn);
        }
        else if (state == STATE_ACTIVE_BACKUP)
        {
            return returnCustomizedConnection(SECONDARY_POOL, conn);
        }
        else
        {
            throw new RuntimeException("Failed to return connection. Netcool gateway is not active.");
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private Connection getCustomizedConnection(String pool)
    {
    	Log.log.debug("NetcoolGateway: Inside getCustomizedConnection");        GenericObjectPool obj = connectionPool.get(pool);
        if (Log.log.isTraceEnabled())
        {
            Log.log.trace("NetcoolGateway: Connection pool: " + pool + ", active=" + obj.getNumActive() + ", idle=" + obj.getNumIdle());
        }

        Connection conn = null;
        if (obj != null)
        {
            try
            {
                while (true)
                {
                    conn = (Connection) obj.borrowObject();
                    if (conn.isClosed()) // Bad connection, remove from pool.
                    {
                        obj.invalidateObject(conn);
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
            	Log.log.error("NetcoolGateway: Error when doing getCustomizedConnection");
                throw new RuntimeException(ex);
            }
        }
        
        return conn;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private Connection returnCustomizedConnection(String pool, Connection jconn)
    {
        GenericObjectPool obj = connectionPool.get(pool);
        Connection conn = null;
        if (obj != null)
        {
            try
            {
                obj.returnObject(jconn);
            }
            catch (Exception ex)
            {
                throw new RuntimeException(ex);
            }
        }
        return conn;
    }

    private boolean connectObjectServer(String url)
    {
        boolean result = false;
        int retry = MAX_RETRY;
        //REG - 1112 Fixing user name and pwd error
        String errorData = null;
        Log.log.debug("NetcoolGateway:Attempting to connect to Netcool at " + url + " with " + retry +" retry's" );
        if (state == STATE_PASSIVE_CONNECT)
        {
            closeConnection(conn);
            conn = null;
        }

        if (state == STATE_ACTIVE_BACKUP)
        {
            retry = 1;
        }

        while (!result && retry > 0)
        {
            try
            {
                // get connection
              //Driver Manager change JIRA #RS-25998
                //Connection newconn = DriverManager.getConnection(url, username, password);
                Connection newconn = DriverManager.getConnection(url, connProps);
                closeConnection(conn);
                Log.log.info("NetcoolGateway: Connected to Netcool at url: " + url + " in " +((MAX_RETRY-retry)+1)+" attempt(s)");                this.conn = newconn;

                // set lastUrl and connChanged
                if (StringUtils.isEmpty(lastUrl) || !lastUrl.equals(url))
                {
                    this.connChanged = true;
                }
                this.lastUrl = url;

                // get starting serial number
                initTimestamp();

                result = true;
            }
            catch (Exception e)
            {
                try
                {
                    if (state != STATE_ACTIVE_BACKUP)
                    {
                    	errorData = e.getMessage();
                        Log.log.warn("NetcoolGateway:Unable to connect to Netcool: " + errorData);
                        Thread.sleep(retryDelay);
                    }
                }
                catch (Exception e2)
                {
                }
                retry--;
            }
        }

        if (retry == 0 && state != STATE_ACTIVE_BACKUP)
        {
        	 Log.log.error("NetcoolGateway: ABORTING::: Failed to connect to Netcool at --> " + url);
        	 if(errorData != null)
        		 Log.log.error("NetcoolGateway: The cause of the error --> " + errorData);
        }

        return result;
    } // connectObjectServer

    private void disconnect()
    {
        try
        {
            if (conn != null)
            {
                conn.close();
            }
        }
        catch (Exception e)
        {
        	Log.log.error("NetcoolGateway: Unable to close connection. " + e.getMessage());        }
    } // disconnect

    public void run()
    {
        if (conn == null)
        {
            connect();
        }
        //Not needed as BaseClusteredGateway will log this
        //Log.log.info("NetcoolGateway: Sending sync request ");
        // This call is must for all the ClusteredGateways.
        super.sendSyncRequest();

        long lastPrimaryRetry = 0;
        String serverSerialKeyName = null;

        if (StringUtils.isBlank(url) && StringUtils.isBlank(url2))
        {
        	Log.log.info("NetcoolGateway: No netcool URL configured, this gateway will act as filter "
        	         		+ "processor from other event mgmt system via interfaces like Webservices.");
        }
        else
        {
            while (running)
            {
            	Log.log.trace("NetcoolGateway: state--> " + getStateString() + " active--> " + isActive());                
                try
                {
                    // connection established
                    if (conn != null && !conn.isClosed())
                    {
                        long startTime = System.currentTimeMillis();
    
                        /* if any remaining events are in primary data queue or 
                         * primary data executor has waiting messages still to be processed, 
                         * do not get messages for more new events.
                         */
                        
                        if (primaryDataQueue.isEmpty() && !primaryDataQueueExecutorBusy())
                        {
                            // check for new alerts
                            for (Filter filter : orderedFilters)
                            {
                                try {
                                    Log.putCurrentNamedContext(Constants.GATEWAY_FILTER, filter.getId());
                                    if (shouldFilterExecute(filter, startTime))
                                    {
                                        NetcoolFilter netcoolFilter = (NetcoolFilter) filter;
                                        
                                        if(Log.log.isTraceEnabled())
                                        {
                                            Log.log.trace("NetcoolGateway: Before poll--> event counter = " + 
                                                    eventCounter + ", Message counter=" + MessageDispatcher.messageCounter);
                                        }
                                        
                                        List<Map<String, String>> netcoolEvents = invokeObjectServer(netcoolFilter);
                                        
                                        if(netcoolEvents!=null && netcoolEvents.size()!=0)
                                            Log.log.info("NetcoolGateway: NetCoolFilter " + netcoolFilter.getId() +" returned " + netcoolEvents.size() +" records.");
                                        else
                                            Log.log.debug("NetcoolGateway: NetCoolFilter " + netcoolFilter.getId() + " returned 0 records.");
                                        
                                        long netCoolServerSerial = netcoolFilter.getLastServerSerial();
                                        
                                        Log.log.debug("NetcoolGateway: " + String.format("SERVERSERIAL B4 : %s", netcoolFilter.getLastServerSerial()));
                                        
                                        if(netcoolEvents.size() > 0)
                                        {
                                            int netCoolEventsCount = netcoolEvents.size() < getPrimaryDataQueueExecutorQueueSize() ? netcoolEvents.size() : getPrimaryDataQueueExecutorQueueSize();
                                            // structure for the single SQL
                                            // query update
                                            List<String> serverSerialList = new ArrayList<String>(netCoolEventsCount);

                                            ExecutionMetrics.INSTANCE.registerEventStart("NetCool primary GW - pull from DB and put into the worker queue");
                                            
                                            for (Map<String, String> netcoolEvent : netcoolEvents)
                                            {
                                                // Initialize serverSerialKeyName
                                                if (StringUtils.isBlank(serverSerialKeyName))
                                                {
                                                    for (String netcoolEventKey : netcoolEvent.keySet())
                                                    {
                                                        if (netcoolEventKey.toUpperCase().equals("SERVERSERIAL"))
                                                        {
                                                            serverSerialKeyName = netcoolEventKey;
                                                            Log.log.debug("NetcoolGateway: SERVERSERIAL Key Name in Netcool event is " + serverSerialKeyName);
                                                            break;
                                                        }
                                                   }
                                                    
                                                    if (StringUtils.isBlank(serverSerialKeyName))
                                                    {
                                                        throw new Exception("Unable to find Server Serial key name in received Netcool event " + StringUtils.mapToString(netcoolEvent));
                                                    }
                                                }
                                                
                                                addToPrimaryDataQueue(netcoolFilter, netcoolEvent);
                                                // *** FIX FOR KEEPING TRACK OF
                                                // WHAT'S PROCESSED
                                                if (netcoolEvent.containsKey(/*"SERIAL"*/serverSerialKeyName))
                                                {
                                                    Log.log.debug("NetcoolGateway: " + String.format("Current Serial  : %s", netcoolEvent.get(/*"SERVERSERIAL"*/serverSerialKeyName)));
                                                    long currentNetcoolServerSerial = Long.parseLong(netcoolEvent.get(/*"SERVERSERIAL"*/serverSerialKeyName));
                                                    if (currentNetcoolServerSerial > netCoolServerSerial)
                                                    {
                                                        netcoolFilter.setLastServerSerial(currentNetcoolServerSerial);
                                                    }
                                                }
                                                // adding the ServerSerial to
                                                // the list for the single SQL
                                                // query update
                                                serverSerialList.add(netcoolEvent.get(/*"SERVERSERIAL"*/serverSerialKeyName));
                                            }
                                            netcoolFilter.setLastQuery(System.currentTimeMillis());
                                            
                                            ExecutionMetrics.INSTANCE.registerEventStart("NetCool primary GW - acknowledgement (bulk update)");
                                            bulkUpdateServerDataSingleSQLQuery(netcoolFilter, serverSerialList);
                                            ExecutionMetrics.INSTANCE.registerEventEnd("NetCool primary GW - acknowledgement (bulk update)", netCoolEventsCount);
                                            
                                            ExecutionMetrics.INSTANCE.registerEventEnd("NetCool primary GW - pull from DB and put into the worker queue", netCoolEventsCount);
                                        }
                                    }
                                    else
                                    {
                                        Log.log.trace("NetcoolGateway: Filter-->" + filter.getId() + " is inactive or not ready to execute yet.....");
                                    }
                                } finally {
                                    Log.clearCurrentNamedContext(Constants.GATEWAY_FILTER);
                                }
                            }
                        }
                        // If there is no filter yet no need to keep spinning, wait for a while
                        if (orderedFilters.size() == 0)
                        {
                            Thread.sleep(interval);
                        }
    
                        // attempt to reconnect to primary if currently using backup
                        if (state == STATE_ACTIVE_BACKUP)
                        {
                            if (System.currentTimeMillis() - lastPrimaryRetry > reconnectDelay)
                            {
                                connect();
                                lastPrimaryRetry = System.currentTimeMillis();
                            }
                        }
                    }
                    // reconnect
                    else
                    {
                        Thread.sleep(reconnectDelay);
                        Log.log.info("NetcoolGateway:Reconnecting");
                        state = STATE_PASSIVE_CONNECT;
                        connect();
                    }
                }
                catch (Exception e)
                {
                	Log.log.warn("NetcoolGateway: Error --> " + e.getMessage(), e);
                    Log.alert("NetcoolGateway: Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", 
                              "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + 
                              "due to " + e.getMessage());
                    
                    try
                    {
                        Thread.sleep(interval);
                    }
                    catch (InterruptedException ie)
                    {
                    	Log.log.error("NetcoolGateway: sleep interrupted: " + ie.getMessage(), ie);
                    }
                }
            }
        }
    } // run

    private void initTimestamp() throws Exception
    {
        Statement stmt = null;
        ResultSet rs = null;

        try
        {
            if (conn == null)
            {
                boolean success = connect();
                
                if (!success)
                {
                    throw new Exception("Failed to initialize serial and timestamp. Unable to connect.");
                }
            }
            
            // initialize Serial
            Log.log.debug("NetcoolGateway: getting start Server Serial from Netcool");
            // This is inefficient
            // String sql = "SELECT Serial, getdate FROM alerts.status ORDER BY Serial DESC;";
            String sql = "SELECT max(Serial), max(ServerSerial), max(getdate) FROM alerts.status";

            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            if (rs.next())
            {
                // set lastSerial if new or changed connection
                if (connChanged)
                {
                    lastSerial = rs.getLong(1);
                    lastServerSerial = rs.getLong(2);
                    Log.log.debug("lastSerial: " + lastSerial);
                }

                reinitLastValues = true;
                initTS = rs.getLong(3);
                Log.log.debug("NetcoolGateway: initTS: " + initTS);

                diffTS = System.currentTimeMillis() / 1000 - initTS;
                Log.log.debug("NetcoolGateway: diffTS: " + diffTS);
            }
            rs.close();
            stmt.close();

            // initialize UID
            Log.log.debug("NetcoolGateway: getting UID for netcool user");
            sql = "SELECT UID FROM master.names WHERE Name='" + username + "';";
            //username is a user entered parameter and needs check for SQL Injection
            String safeSQL = SQLUtils.getSafeSQL(sql);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(ESAPI.validator().getValidInput(
					"Getting UID for netcool user", safeSQL, 
					ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR, 
					ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH,
					false, false));
            if (rs.next())
            {
                UID = rs.getInt("UID");
                Log.log.debug("NetcoolGateway: Name--> " + username + " UID--> " + UID);
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e)
        {
        	Log.log.error("NetcoolGateway: Failed to initialize serial and timestamp: " + e.getMessage(), e);            throw e;
        }
        finally
        {
            try
            {
                if (rs != null)
                {
                    rs.close();
                }
                if (stmt != null)
                {
                    stmt.close();
                }
            }
            catch (Exception sqlEx)
            {
            }
        }
    } // initTimestamp

    private List<Map<String, String>> invokeObjectServer(NetcoolFilter filter)
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();

        ResultSet rs = null;
        Statement stmt = null;

        try
        {
            // init lastSuccessfulQUery
            if (reinitLastValues)
            {
                filter.setLastSuccessfulQuery(this.initTS);
                filter.setLastSerial(lastSerial);
                filter.setLastServerSerial(lastServerSerial);
                reinitLastValues = false;
            }
            if (filter.getLastSuccessfulQuery() == 0)
            {
                filter.setLastSuccessfulQuery(this.initTS);
            }
            if (filter.getLastSerial() == 0)
            {
                filter.setLastSerial(lastSerial);
            }
            if (filter.getLastServerSerial() == 0)
            {
                filter.setLastServerSerial(lastServerSerial);
            }

            // init sql
            String sql = filter.getSql();
            String runbook = filter.getRunbook();

            // replace LASTSERIAL with value
            sql = sql.replaceFirst("LASTSERIAL", "" + filter.getLastSerial());

            // replace LASTSERVERSERIAL with value
            sql = sql.replaceFirst("LASTSERVERSERIAL", "" + filter.getLastServerSerial());

            // replace LASTQUERY with value
            sql = sql.replaceFirst("LASTQUERY", "" + filter.getLastSuccessfulQuery());

            Log.log.debug("NetcoolGateway: Executing filter --> " + filter.getId());
            Log.log.debug("NetcoolGateway: executing query for runbook --> " + runbook + "\n");
            Log.log.debug("NetcoolGateway:  sql query --> " + sql);
            if (StringUtils.isBlank(sql)) Log.log.error("NetcoolGateway: The sql provided on the filter is blank");
            Log.log.debug("NetcoolGateway:  lastSuccessfulQuery: " + new java.util.Date(filter.getLastSuccessfulQuery() * 1000));

            stmt = conn.createStatement();
            try
            {
            	String safeSQL = SQLUtils.getSafeSQL(sql);
            	
                rs = stmt.executeQuery(ESAPI.validator().getValidInput(
                													"Netcool Filter Query", safeSQL, 
                													ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR, 
                													ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH,
                													false, false));
            }
            catch (Exception ex)
            {
            	Log.log.error("NetcoolGateway: Failed to Execute Filter: " + filter.getId(), ex);
                sendAlert("CRITICAL", true, "Filter:" + filter.getId() + ":Update SQL Error", ex.getMessage());
            }

            int eventCounter = 0;
            while (rs.next())
            {
                Map<String, String> runbookParams = new HashMap<String, String>();

                // update lastSerial
                String serverName = rs.getString("ServerName").replace('\0', ' ').trim();
                long newSerial = rs.getLong("Serial");
                long newServerSerial = rs.getLong("ServerSerial");
                if (newSerial > filter.getLastSerial())
                {
                    filter.setLastSerial(newSerial);
                }
                if (newServerSerial > filter.getLastServerSerial())
                {
                    filter.setLastServerSerial(newServerSerial);
                }

                // define values from SELECT
                ResultSetMetaData rsmd = rs.getMetaData();
                for (int i = 1; i <= rsmd.getColumnCount(); i++)
                {
                    String name = rsmd.getColumnName(i);
                    String value = rs.getString(i);

                    // remove null from value
                    value = value.replace('\0', ' ').trim();
                    if (name.equalsIgnoreCase("PROCESSID"))
                    {
                        name = "NETCOOL_" + name;
                    }
                    if (name.equalsIgnoreCase("GUID"))
                    {
                        name = "NETCOOL_" + name;
                    }
                    if (name.equalsIgnoreCase("QUEUE"))
                    {
                        name = "NETCOOL_" + name;
                    }
                    if (isUppercase())
                    {
                        runbookParams.put(name.toUpperCase(), value);
                    }
                    else
                    {
                        runbookParams.put(name, value);
                    }
                }

                runbookParams.put(NETCOOL_SERVER_NAME, serverName);
                runbookParams.put(NETCOOL_SERIAL, "" + newSerial);
                runbookParams.put(NETCOOL_SERVER_SERIAL, "" + newServerSerial);
                result.add(runbookParams);
                eventCounter++;
            }
            rs.close();

            if (eventCounter > 0)
            {
                // update lastQuery timestamp for successful queries
                rs = stmt.executeQuery("SELECT max(getdate) FROM alerts.colors");

                if (rs.next())
                {
                    long ts = rs.getLong(1); // remote timestamp on

                    filter.setLastSuccessfulQuery(ts);
                    Log.log.trace("Updating lastSuccessfulQuery: " + ts);
                }
                rs.close();
            }

            stmt.close();
        }
        catch (Exception e)
        {
            String msg = e.getMessage();
            if (msg != null && msg.contains("ObjectServer is currently busy"))
            {
                Log.log.warn("ObjectServer is currently busy. Waiting for database locks to be released");
            }
            else if (msg != null && msg.indexOf("ObjectServer") > -1 && msg.indexOf("resynchronized") > -1 && msg.indexOf("resync lock to be released") > -1)
            {
                // Exception: ObjectServer is currently being resynchronized.
                // Waiting for resync lock to be released
            	Log.log.debug("NetcoolGateway: Waiting for ObjectServer resynchronization lock to be released: " + e.getMessage(), e);                connChanged = true;
                boolean reTry = true;
                // Keep retrying until initTimestamp() is successful.
                while (reTry)
                {
                    try
                    {
                        Thread.sleep(60000);
                        initTimestamp();
                        reTry = false;
                    }
                    catch (Exception ex)
                    {
                    	Log.log.error("NetcoolGateway: Failed to reinitialize timestamp after ObjectServer was locked by resynchronization", ex);                    }
                }
            }
            else
            {
            	Log.log.error("NetcoolGateway: Failed to poll for events: " + e.getMessage(), e);            }
        }
        finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                }
                catch (Exception sqlEx)
                {
                } // ignore

                rs = null;
            }

            if (stmt != null)
            {
                try
                {
                    stmt.close();
                }
                catch (Exception sqlEx)
                {
                } // ignore

                stmt = null;
            }
        }
        return result;
    } // invokeObjectServer

    public void start()
    {
        // init
    	Log.log.info("***************************************************************");
    	Log.log.info("NetcoolGateway: Starting NetcoolListener and gateway");
    	Log.log.info("**************************************************************");
        super.start();
        state = STATE_PASSIVE_CONNECT;

        if (StringUtils.isBlank(url) && StringUtils.isBlank(url2))
        {
        	Log.log.info("NetcoolGateway: No netcool URL configured, this gateway will act as "
     		+ "filter processor from other event mgmt system via interfaces like Webservices.");
        }
        else
        {
            // connect to Netcool
            connect();
        }
    } // start

    public void stop()
    {
    	Log.log.info("***************************************************************");
    	Log.log.info("NetcoolGateway: Stopping NetcoolListener and gateway");
    	Log.log.info("**************************************************************");
        super.stop();
        try
        {
            disconnect();
        }
        catch (Exception e)
        {
        	Log.log.info("NetcoolGateway: ERROR Stopping NetcoolListener and gateway");
        }
    } // stop

    private String getStateString()
    {
        String result = "";

        switch (state)
        {
            case STATE_PASSIVE_CONNECT:
                result = "PASSIVE_CONNECT";
                break;
            case STATE_ACTIVE_PRIMARY:
                result = "ACTIVE_PRIMARY";
                break;
            case STATE_ACTIVE_BACKUP:
                result = "ACTIVE_BACKUP";
                break;
        }

        return result;
    } // getStateString

    @Override
    protected void addAdditionalBindings(String script, Binding binding)
    {
        Connection jdbcConn = null;
        /* this is just a simple hack, there could be variety of
         * situation it can fail. but for now as long as DB word
         * is the start of the script or is preceded by a space.
         */
        if (script.startsWith(Constants.GROOVY_BINDING_DB) || script.contains(" " + Constants.GROOVY_BINDING_DB))
        {
            if (state == STATE_ACTIVE_PRIMARY)
            {
                jdbcConn = getCustomizedConnection(PRIMARY_POOL);
                binding.setProperty("NETCOOL_POOLNAME", PRIMARY_POOL);
            }
            else if (state == STATE_ACTIVE_BACKUP)
            {
                jdbcConn = getCustomizedConnection(SECONDARY_POOL);
                binding.setProperty("NETCOOL_POOLNAME", SECONDARY_POOL);
            }
        }
        binding.setVariable(Constants.GROOVY_BINDING_DB, jdbcConn);
    }

    @Override
    protected void groovyScriptBindingCleanup(Binding binding)
    {
        if (binding != null && binding.hasVariable(Constants.GROOVY_BINDING_DB))
        {
            Connection jdbcConn = (Connection) binding.getVariable(Constants.GROOVY_BINDING_DB);
            if (jdbcConn != null)
            {
                if (binding.getProperty("NETCOOL_POOLNAME").equals(PRIMARY_POOL))
                {
                    returnCustomizedConnection(PRIMARY_POOL, jdbcConn);
                }
                else
                {
                    returnCustomizedConnection(SECONDARY_POOL, jdbcConn);
                }
            }
        }
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> event, Map<String, String> content)
    {
        String serverName = (String) content.get(NETCOOL_SERVER_NAME);
        String newSerial = (String) content.get(NETCOOL_SERIAL);
        String newServerSerial = (String) content.get(NETCOOL_SERVER_SERIAL);

        if (!content.containsKey(Constants.EXECUTE_REFERENCE) && content.containsKey(NETCOOL_SERVER_NAME))
        {
            event.put(Constants.EXECUTE_REFERENCE, serverName + ":" + newServerSerial + ":" + newSerial);
        }
        if (updateRunbookId && !content.containsKey(Constants.EXECUTE_ALERTID))
        {
            event.put(Constants.EXECUTE_ALERTID, serverName + ":" + newServerSerial);
        }
    }

    /* (non-Javadoc)
     * @see com.resolve.gateway.BaseClusteredGateway#updateServerData(com.resolve.gateway.Filter, java.util.Map)
     */
    @SuppressWarnings("rawtypes")
    @Override
    public void updateServerData(Filter filter, Map content)
    {
        // The following is already implemented in the single SQL query update
        //
        /*
        //String serverName = (String) content.get(NETCOOL_SERVER_NAME);
        //String serverSerial = (String) content.get(NETCOOL_SERVER_SERIAL);
        String serial = (String) content.get(NETCOOL_SERIAL);
        
        // update status field
        String updateFields = "";
        if (updateResolveStatus)
        {
            int value = statusProcess;

            if (!StringUtils.isEmpty(updateFields))
            {
                updateFields += ", ";
            }
            if (content.containsKey(statusFieldname))
            {
                Log.log.debug("Found resolveStatusField in content");
                Object obj = content.get(statusFieldname);
                if (obj instanceof String)
                {
                    //commented, this would not change the status
                    //value = Integer.parseInt((String) content.get(resolveStatusField));
                    updateFields += statusFieldname + " = " + SQLUtils.getSafeSQL(""+ "'" + value + "'");
                }
                else if (obj instanceof Number )
                {
                    //commented, this would not change the status
                    //value = ((Integer) content.get(resolveStatusField)).intValue();
                    updateFields += statusFieldname + " = " + SQLUtils.getSafeSQL(""+value);
                }
                else
                {
                    Log.log.warn("Unsupported data type " + obj.getClass().getName() + " for RESOLVE_STATUS - " + statusFieldname);
                }
            }
            else
            {
                Log.log.warn("Content does not contain RESOLVE_STATUS field - " + statusFieldname);
            }
        }
        if (updateRunbookId)
        {
            String value = filter.getRunbook();

            if (!StringUtils.isEmpty(updateFields))
            {
                updateFields += ", ";
            }
            if (content.containsKey(runbookIdKey))
            {
                value = (String) content.get(runbookIdKey);
            }
            updateFields += runbookIdFieldname + " = " + "'" + SQLUtils.getSafeSQL(value) + "'";
        }
        if (!StringUtils.isEmpty(updateFields))
        {
            try
            {
                //BD: 2/5/15, after discussing with JimV & Duke, seems like we can use the Serial field instead of ServerSerial
                //String updateSQL = "update alerts.status set " + updateFields + " where ServerSerial=" + SQLUtils.getSafeSQL(""+serverSerial);
                //updateSQL += " and ServerName='" + SQLUtils.getSafeSQL(serverName) + "';";
                String updateSQL = "update alerts.status set " + updateFields + " where Serial=" + SQLUtils.getSafeSQL("'" + serial + "'");
                if(Log.log.isTraceEnabled())
                {
                    Log.log.trace("Update sql: " + updateSQL);
                }
                int rowsUpdated = conn.createStatement().executeUpdate(SQLUtils.getSafeSQL(updateSQL));
                if(Log.log.isTraceEnabled())
                {
                    Log.log.trace("(" + rowsUpdated + ")row affected");
                }
            }
            catch (SQLException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }*/
    }
    
    public void bulkUpdateServerDataSingleSQLQuery(Filter filter, List<String> serverSerialList) throws Exception
    {
        String updateFields = null;
        if (updateRunbookId)
        {
            String value = filter.getRunbook();
            updateFields = runbookIdFieldname + "=" + "'" + value + "'";
        }

        StringBuilder whereClause = null;

        if (updateResolveStatus)
        {
            if (updateFields != null)
            {
                updateFields += ", ";
            }
            else
            {
                updateFields = "";
            }
            if ("STRING".equalsIgnoreCase(statusType))
            {
	            updateFields += statusFieldname + "='" + statusProcess + "'";
            }
            else
            {
	            updateFields += statusFieldname + "=" + statusProcess;
            }

            if (serverSerialList.size() > 0)
            {
                whereClause = new StringBuilder(" where ServerSerial in (");
                int index = 1;
                for (String string : serverSerialList)
                {
                    whereClause.append(string);
                    if (index < serverSerialList.size())
                    {
                        whereClause.append(",");
                        index++;
                    }
                    else
                    {
                        whereClause.append(")");
                    }
                }
            }
        }

        if (updateFields != null)
        {
            try
            {

                StringBuilder updateSQLBuilder = new StringBuilder();
                updateSQLBuilder.append("update alerts.status set ");
                updateSQLBuilder.append(updateFields);
                if (whereClause != null)
                {
                    updateSQLBuilder.append(whereClause.toString());
                }

                if (Log.log.isTraceEnabled())
                {
                	Log.log.trace("NetcoolGateway: Update sql: " + updateSQLBuilder.toString());
                }
                PreparedStatement stmt;
				try {
					stmt = conn.prepareStatement(
									ESAPI.validator().getValidInput(
														"Bulk Update Server Data Statement",
														SQLUtils.getSafeSQL(
															updateSQLBuilder.toString()),
															ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR,
															(2 * ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH), 
															false, false));
				} catch (Exception e) {
					throw new SQLException(e);
				}
                int rowsUpdated = stmt.executeUpdate();
                if (Log.log.isTraceEnabled())
                {
                	Log.log.trace("NetcoolGateway: (" + rowsUpdated + ")row(s) affected");                }
            }
            catch (SQLException e)
            {
            	Log.log.error("NetcoolGateway: Error executing Netcool query" +e.getMessage(), e);                throw e;
            }
        }
    }
    
    /*
     * Public API
     */
    @SuppressWarnings("rawtypes")
    public void addJournal(Map params)
    {
        if (isActive() || (isWorker() && !isPrimary()))
        {
            try
            {
                String reference = (String) params.get("REFERENCE");
                String detail = (String) params.get("DETAIL");
                String actionname = (String) params.get("ACTIONNAME");
                String condition = (String) params.get("CONDITION");
                String severity = (String) params.get("SEVERITY");
                String duration = (String) params.get("DURATION");
                String serial = (String) params.get("SERIAL");

                String[] journalEntries = null;

                // override serial if REFERENCE is defined
                if (!StringUtils.isEmpty(reference))
                {
                    String[] nameServerSerial = reference.split(":");
                    if (nameServerSerial.length == 3)
                    {
                        serial = nameServerSerial[2];
                    }
                    else if (nameServerSerial.length == 1)
                    {
                        serial = reference;
                    }
                }

                if (StringUtils.isEmpty(serial))
                {
                    throw new Exception("Missing SERIAL parameter");
                }
                if (StringUtils.isEmpty(detail))
                {
                    throw new Exception("Missing DETAIL parameter");
                }

                // init text
                String text = "";
                if (!StringUtils.isEmpty(actionname))
                {
                    text += "Task: " + actionname + " ";
                }

                if (!StringUtils.isEmpty(condition))
                {
                    text += "Condition: " + condition + " ";
                }

                if (!StringUtils.isEmpty(severity))
                {
                    text += "Severity: " + severity + " ";
                }

                if (!StringUtils.isEmpty(duration))
                {
                    text += "Duration: " + duration + "\n\n";
                }

                // update detail
                text += detail;
                text = text.replace("\\", "\\\\");
                text = text.replace("'", "\\'");

                // init field and values
                int start = 0;
                int end = 0;
                int textLength = text.length();

                // init multiple journal entries
                int numJournals = textLength / 4000;
                if (textLength % 4000 != 0)
                {
                    numJournals++;
                }
                journalEntries = new String[numJournals];

                for (int i = 0; i < numJournals; i++)
                {
                    start = end;
                    end = 4000 * (i + 1);
                    if (end > textLength)
                    {
                        end = textLength;
                    }
                    journalEntries[i] = text.substring(start, end);
                }

                // generate text fields
                if (version.startsWith("3"))
                {
                	Log.log.debug("NetcoolGateway: Running Journal entries " + Arrays.toString(journalEntries));
                    for (int i = 0; i < journalEntries.length; i++)
                    {
                        // init
                        long date = System.currentTimeMillis() / 1000;
                        String textFields = "";
                        String textValues = "";
                        String unique = serial + ":" + i + ":" + date;

                        // set number of text fields
                        String journalText = journalEntries[i];
                        int journalLength = journalEntries[i].length();

                        int numTextFields = journalLength / 250;
                        if (journalLength % 250 != 0)
                        {
                            numTextFields++;
                        }

                        // loop number of fields
                        start = 0;
                        end = 0;
                        int textIdx = 1;
                        while (textIdx <= numTextFields)
                        {
                            // set field
                            textFields += "Text" + textIdx;

                            // set value
                            start = end;
                            end = 250 * textIdx;
                            if (end > journalLength)
                            {
                                end = journalLength;
                            }
                            textValues += "'" + journalText.substring(start, end) + "'";

                            if (textIdx < numTextFields)
                            {
                                textFields += ", ";
                                textValues += ", ";
                            }
                            textIdx++;
                        }

                        // add journal
                        String sql = "insert into alerts.journal (KeyField, Serial, UID, Chrono, " + textFields + ") values ('" + unique + "'," + serial + "," + UID + "," + date + "," + textValues + ");";
                        enqueue(sql);
                    }
                }
                else
                {
                	Log.log.debug("NetcoolGateway: Running Journal entries " + Arrays.toString(journalEntries));
                    for (int i = 0; i < journalEntries.length; i++)
                    {
                        String sql = "call procedure jinsert (" + serial + "," + i + ",getdate(),'" + journalEntries[i] + "');";
                        enqueue(sql);
                    }

                    String sql = "sync table alerts.journal;";
                    enqueue(sql);
                }

            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    } // addJournal

    // Execute any sql query against Object Server
    public List<Map<String, String>> select(String sql) throws Exception
    {
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        
        Connection selectConn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try
        {
            selectConn = getCustomizedConnection();
            if (selectConn == null)
            {
                Log.log.error("NetcoolGateway: Connection to the Object Server is null, something terrible happened! Check log for some trace.");
            }
            else
            {
                Log.log.debug("NetcoolGateway: Is the Connection to Object Server closed? " + selectConn.isClosed());
            }
            Log.log.debug("NetcoolGateway: Executing SQL: " + sql);

            String safeSQL = SQLUtils.getSafeSQL(sql);
            stmt = selectConn.createStatement();
            rs = stmt.executeQuery(ESAPI.validator().getValidInput(
					"Netcool Select Query", safeSQL, 
					ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR, 
					ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH,
					false, false));

            while (rs.next())
            {
                Map<String, String> content = new HashMap<String, String>();

                // define values from SELECT
                ResultSetMetaData rsmd = rs.getMetaData();
                for (int i = 1; i <= rsmd.getColumnCount(); i++)
                {
                    String name = rsmd.getColumnName(i);
                    String value = rs.getString(i);

                    // remove null from value
                    value = value.replace('\0', ' ').trim();
                    if (netcoolConfiguration.isUppercase())
                    {
                        content.put(name.toUpperCase(), value);
                    }
                    else
                    {
                        content.put(name, value);
                    }
                }
                result.add(content);
            }
        }
        finally
        {
            if (stmt != null)
            {
                try
                {
                    stmt.close();
                }
                catch (SQLException e2)
                {
                    Log.log.error(e2.getMessage(), e2);
                }
            }
            
            if (selectConn != null)
            {
                try
                {
                    returnCustomizedConnection(selectConn);
                }
                catch (Throwable sqlEx)
                {
                } // ignore
            }
        }
        Log.log.debug("NetcoolGateway: Netcool gateway custom query returned " + result.size() + " rows as result.");
        return result;
    }

    public String update(String sql) throws Exception
    {
        String resultStr = null;
        int result = -1;
        Statement stmt = null;
        boolean attempted = true;
        Connection con = getCustomizedConnection();
        try
        {
            stmt = con.createStatement();
            while (attempted)
            {
                Log.log.trace("NetcoolGateway: Synchrnous netcool update call, sql: " + sql);
                try
                {
                	String safeSQL = SQLUtils.getSafeSQL(sql);
                	result = stmt.executeUpdate(ESAPI.validator().getValidInput(
        					"Update Netcool Record", safeSQL, 
        					ResolveDefaultValidator.HQL_SQL_SAFE_STRING_VALIDATOR, 
        					ResolveDefaultValidator.MAX_HQL_SQL_SAFE_STRING_LENGTH,
        					false, false));
                    Log.log.debug("NetcoolGateway: (" + result + ")row affected");
                    attempted = false;
                    resultStr = "" + result;
                }
                catch (Exception e)
                {
                    // dont remove if ObjectServer is busy
                    if (e.getMessage().contains("ObjectServer is currently busy"))
                    {
                        Log.log.info("NetcoolGateway: Object Server is busy, will retry update. Exception is " + e.getMessage());
                        Thread.sleep(5000);
                    }
                    else
                    {
                        attempted = false;
                        resultStr = e.getMessage();
                        Log.log.warn("NetcoolGateway: Netcool Update Exception: " + e.getMessage(), e);
                    }
                }
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage());
            resultStr = e.getMessage();
            throw e;
        }
        finally
        {
            if (stmt != null)
            {
                try
                {
                    stmt.close();
                }
                catch (Throwable sqlEx)
                {
                } // ignore

                stmt = null;
            }

            if (con != null)
            {
                try
                {
                    returnCustomizedConnection(con);
                }
                catch (Throwable sqlEx)
                {
                } // ignore
            }
        }
        return resultStr;
    } // update
} // NetcoolGateway
