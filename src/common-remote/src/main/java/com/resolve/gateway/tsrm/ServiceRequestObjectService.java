/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.tsrm;

import java.util.List;
import java.util.Map;

import com.resolve.rsremote.ConfigReceiveTSRM;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

/**
 * This class acts as a service to manage service request.
 * 
 * @author bipuldutta
 * 
 */
public class ServiceRequestObjectService extends AbstractObjectService
{
    static String OBJECT_IDENTITY = "mxsr";
    static String MAIN_NODE_NAME = "MXSRSet";
    static String OBJECT_NODE_NAME = "SR";

    private static String IDENTITY_PROPERTY = "TICKETUID";

    public ServiceRequestObjectService(RestCaller restCaller)
    {
        super(restCaller);
    }

    @Override
    public String getIdPropertyName()
    {
        return IDENTITY_PROPERTY;
    }

    @Override
    public String getIdentity()
    {
        return OBJECT_IDENTITY;
    }

    @Override
    public String getMainNodeName()
    {
        return MAIN_NODE_NAME;
    }

    @Override
    public String getObjectNodeName()
    {
        return OBJECT_NODE_NAME;
    }

    @Override
    public Map<String, String> create(Map<String, String> objectProperties, String username, String password) throws Exception
    {
        try
        {
            return super.create(objectProperties, username, password);
        }
        catch (Exception ex)
        {
            if (ex.getMessage().startsWith(ErrorHandler.ERROR_400))
            {
                throw new Exception(ErrorHandler.ERROR_400 + " Service Request cannot be created, possibly duplicate Ticket Id");
            }
            throw ex;
        }
    }

    @Override
    public void delete(String objectId, String username, String password) throws Exception
    {
        try
        {
            super.delete(objectId, username, password);
        }
        catch (Exception ex)
        {
            if (ex.getMessage().startsWith(ErrorHandler.ERROR_400))
            {
                throw new Exception(ErrorHandler.ERROR_400 + " Service Request cannot be deleted, possibly its in an undeletable status or ticket has worklogs.");
            }
            throw ex;
        }
    }

    @Override
    public boolean createWorklog(String parentObjectId, Map<String, String> params, String username, String password) throws Exception
    {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = restCaller.getUsername();
            password = restCaller.getPassword();
        }

        // this will throw Exception is parentObjectId is null or empty.
        validateObjectId(parentObjectId);

        // appends the parentObjectId to the URL.
        String modifiedUrl = restCaller.addObjectId(parentObjectId, restCaller.getOriginalUrl(), username, password, getIdentity());
        restCaller.setUrl(modifiedUrl);

        restCaller.postMethod(getModifiedParams(1, params));

        return true;
    }

//    public static void main(String... args)
//    {
//        try
//        {
//            XDoc xDoc = new XDoc();
//            ConfigReceiveTSRM configurations = new ConfigReceiveTSRM(xDoc);
//            RestCaller restCaller = new RestCaller(configurations.getUrl());
//            ServiceRequestObjectService service = new ServiceRequestObjectService(restCaller);
//            List<Map<String, String>> result = service.unmarshal("<SRMboSet><SR><DUPFLAG>DUPLICATE</DUPFLAG><ASSETFILTERBY>USERCUST</ASSETFILTERBY></SR><SR><DUPFLAG>NEW</DUPFLAG><ASSETFILTERBY>USERCUST</ASSETFILTERBY></SR></SRMboSet>");
//            /*
//             * <SRMboSet> <SR> <DUPFLAG>DUPLICATE1</DUPFLAG>
//             * <ASSETFILTERBY>USERCUST</ASSETFILTERBY> </SR> <SR>
//             * <DUPFLAG>NEW</DUPFLAG> <ASSETFILTERBY>USERCUST</ASSETFILTERBY>
//             * </SR></SRMboSet>
//             */
//            for (Map<String, String> map : result)
//            {
//                System.out.printf(">>>>>\n");
//                for (String key : map.keySet())
//                {
//                    System.out.printf("Key: %s, Value: %s\n", key, map.get(key));
//                }
//            }
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//    }
}
