/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.tsrm;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.resolve.util.Base64;
import com.resolve.util.Log;
import com.resolve.util.SSLUtils;
import com.resolve.util.StringUtils;

/**
 * The is a convenience class for consuming TSRM REST web services. This is most
 * generic and lowest level class used by more specific TSRM object based (e.g.,
 * sr, asset) classes.
 */
public class RestCaller
{
    private String originalUrl;
    private String protocol = "http";
    private int port = 80;
    private boolean isSSL = false;
    // private String testUrl;
    private String username; // e.g., maxadmin
    private String password; // e.g., maxadmin
    private String httpbasicauthusername;
    private String httpbasicauthpassword;

    private String url;

    private static SSLSocketFactory sslSocketFactory = null;

    /**
     * Constructor
     * 
     * @param url
     *            in the form of
     *            http://tsrm_server:port/maxrest/rest/os/mxsr?_lid
     *            =maxadmin&_lpwd=maxadmin
     */
    public RestCaller(String originalUrl)
    {
        this.originalUrl = originalUrl;
        URL theUrl;
        try
        {
            theUrl = new URL(originalUrl);
            if ("https".equals(theUrl.getProtocol()))
            {
                try
                {
                    SSLContext ctx = SSLContext.getInstance("SSL");
                    SSLUtils.FakeX509TrustManager tm = new SSLUtils.FakeX509TrustManager();
                    ctx.init(null, new TrustManager[] { tm }, null);
                    sslSocketFactory = new SSLSocketFactory(ctx);
                    isSSL = true;
                    protocol = "https";
                    if(theUrl.getPort() == -1)
                    {
                        port = 443;
                    }
                }
                catch (NoSuchAlgorithmException e)
                {
                    Log.log.error(e.getMessage(), e);
                }
                catch (KeyManagementException e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
            else
            {
                if(theUrl.getPort() == -1)
                {
                    port = 80;
                }
            }
        }
        catch (MalformedURLException e1)
        {
            Log.log.error(e1.getMessage(), e1);
        }
    }

    public RestCaller(String originalUrl, String username, String password, String httpbasicauthusername, String httpbasicauthpassword)
    {
        this(originalUrl);
        this.username = username;
        this.password = password;
        this.httpbasicauthusername = httpbasicauthusername;
        this.httpbasicauthpassword = httpbasicauthpassword;
    }

    public String getOriginalUrl()
    {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl)
    {
        this.originalUrl = originalUrl;
    }

    public String getUrlX()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getTestUrl()
    {
        return getOriginalUrl() + "/login";
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getHttpbasicauthusername()
    {
        return httpbasicauthusername;
    }

    public void setHttpbasicauthusername(String httpbasicauthusername)
    {
        this.httpbasicauthusername = httpbasicauthusername;
    }

    public String getHttpbasicauthpassword()
    {
        return httpbasicauthpassword;
    }

    public void setHttpbasicauthpassword(String httpbasicauthpassword)
    {
        this.httpbasicauthpassword = httpbasicauthpassword;
    }

    private void appendUserInfo(StringBuilder sb, String username, String password)
    {
        sb.append("?_lid=" + username);
        sb.append("&_lpwd=" + password);
    }

    /**
     * Method used to create or update objects in TSRM.
     * 
     * @param params
     * @return
     * @throws Exception
     */
    private String executeMethod(HttpRequestBase method) throws Exception
    {
        String result = null;

        // Create an instance of HttpClient.
        DefaultHttpClient client = new DefaultHttpClient();

        try
        {
            if (isSSL)
            {

                ClientConnectionManager clientConnectionManager = client.getConnectionManager();
                // register https protocol in httpclient's scheme registry
                SchemeRegistry schemeRegistry = clientConnectionManager.getSchemeRegistry();
                schemeRegistry.register(new Scheme(protocol, port, sslSocketFactory));
            }

            setHttpBasicAuth(client, method);

            // Execute the method.
            HttpResponse response = client.execute(method);
            
            // If the status code is offensive it'll throws Exception
            ErrorHandler.verifyStatus(response);

            HttpEntity entity = response.getEntity();

            // Read the response body.
            result = StringUtils.toString(entity.getContent(), "utf-8");
        }
        catch (IOException e)
        {
            Log.log.error("Fatal transport error:: " + e.getMessage(), e);
            throw e;
        }
        finally
        {
            // Release the connection.
            client.getConnectionManager().shutdown();
        }
        return result;
    }

    private void setHttpBasicAuth(DefaultHttpClient httpclient, HttpRequestBase method)
    {
        if (StringUtils.isNotBlank(httpbasicauthusername) && StringUtils.isNotBlank(httpbasicauthpassword) && method.getURI() != null)
        {
            String user = httpbasicauthusername + ":" + httpbasicauthpassword;
            method.addHeader("Authorization", "Basic " + Base64.encodeBytes(user.getBytes()));
            Log.log.debug("HTTP Basic authorization header added");
            httpclient.getCredentialsProvider().setCredentials(new AuthScope(method.getURI().getHost(), method.getURI().getPort()), new UsernamePasswordCredentials(httpbasicauthusername, httpbasicauthpassword));
        }
    }

    public boolean testConnection()
    {
        boolean result = true;

        // Create a method instance.
        HttpGet method = new HttpGet(getTestUrl());

        DefaultHttpClient client = new DefaultHttpClient();

        try
        {
            setHttpBasicAuth(client, method);
            // Execute the method.
            client.execute(method);
        }
        catch (IOException e)
        {
            System.out.println("Fatal IOException error:: " + e.getMessage());
            result = false;
        }
        catch (Exception e)
        {
            System.out.println("Fatal transport error:: " + e.getMessage());
            result = false;
        }
        return result;
    }

    /**
     * Synthesizes a URL like
     * http://server_name:port/maxrest/rest/mxsr/sr?_lid=uid&_lpwd=pwd
     * 
     * @param url
     * @param username
     * @param password
     * @param object
     * @return
     */
    public String synthesizeUrl(String url, String username, String password, String object)
    {
        StringBuilder sb = new StringBuilder(url);
        sb.append(object);
        appendUserInfo(sb, username, password);

        return sb.toString();
    }

    /**
     * Adds object's ID to the url for operations like update and delete.
     * 
     * This will synthesize a URL like
     * http://server_name:port/maxrest/rest/os/mx/sr/1234?_lid=uid&_lpwd=pwd
     * 
     * @param objectId
     * @param url
     * @param username
     * @param password
     * @param object
     * @return
     */
    public String addObjectId(String objectId, String url, String username, String password, String object)
    {
        StringBuilder sb = new StringBuilder(url);
        sb.append(object);
        sb.append("/" + objectId);
        appendUserInfo(sb, username, password);

        return sb.toString();
    }

    /**
     * Makes HTTP Post method posting to a URL using parameter.
     * 
     * @param params
     * @return
     * @throws Exception
     */
    public String postMethod(Map<String, String> params) throws Exception
    {
        // Create a method instance.
        HttpPost method = new HttpPost(url);

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        for (String key : params.keySet())
        {
            //String tmpKey = java.net.URLEncoder.encode(key, "UTF-8").replace("+", "%20"); //this is for space
            //String tmpValue = java.net.URLEncoder.encode(params.get(key), "UTF-8").replace("+", "%20"); //this is for space
            //nameValuePairs.add(new BasicNameValuePair(tmpKey, tmpValue));
            nameValuePairs.add(new BasicNameValuePair(key, params.get(key)));
        }
        method.setEntity(new UrlEncodedFormEntity(nameValuePairs, "utf-8"));

        return executeMethod(method);
    }

    /**
     * Makes HTTP Post method posting to a URL using the query string.
     * 
     * @param params
     * @return
     * @throws Exception
     */
    public String postMethodWithQueryString(Map<String, String> params) throws Exception
    {
        StringBuilder modifiedUrl = new StringBuilder(url);

        for (String key : params.keySet())
        {
            modifiedUrl.append("&" + key);
            modifiedUrl.append("=" + URLEncoder.encode(params.get(key), "utf-8"));
        }

        HttpPost method = new HttpPost(modifiedUrl.toString());

        return executeMethod(method);
    }

    /**
     * Method used to create or update objects in TSRM.
     * 
     * @param params
     * @return
     * @throws Exception
     */
    public String putMethod(Map<String, String> params) throws Exception
    {
        // Create a method instance.
        HttpPut method = new HttpPut(url);

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        for (String key : params.keySet())
        {
            nameValuePairs.add(new BasicNameValuePair(key, params.get(key)));
        }

        method.setEntity(new UrlEncodedFormEntity(nameValuePairs));

        return executeMethod(method);
    }

    /**
     * Makes HTTP Put method posting to a URL.
     * 
     * @param params
     * @return
     * @throws Exception
     */
    public String putMethodWithQueryString(Map<String, String> params) throws Exception
    {
        StringBuilder modifiedUrl = new StringBuilder(url);

        for (String key : params.keySet())
        {
            modifiedUrl.append("&" + key);
            modifiedUrl.append("=" + URLEncoder.encode(params.get(key), "utf-8"));
        }

        HttpPut method = new HttpPut(modifiedUrl.toString());

        return executeMethod(method);
    }

    /**
     * Makes HTTP Get request to a URL.
     * 
     * @param query
     *            in the form "description=UltraCast&status=new" that will be
     *            appended to the URL string passed through the constructor
     *            parameter.
     * @return
     * @throws Exception
     */
    public String getMethod(String query) throws Exception
    {
        // Create a method instance.
        HttpGet method = null;
        if (StringUtils.isEmpty(query))
        {
            method = new HttpGet(this.url + "&_format=xml");
        }
        else
        {
            // always bring the xml format, so the unmarshaller has one format
            // to deal with. Note that _format=json also supported by TSRM.
            method = new HttpGet(this.url + "&_format=xml&" + query);
        }

        return executeMethod(method);
    }

    /**
     * Makes HTTP Delete method posting to a URL.
     * 
     * @param objectId
     * @throws Exception
     */
    public void deleteMethod(String objectId) throws Exception
    {
        // Create a method instance.
        HttpDelete method = new HttpDelete(this.url);

        executeMethod(method);
    }

    /*
     * public static void main(String... args) throws Exception { // String url
     * = "http://localhost:7080/tsrm/rest/"; // String url =
     * "http://172.16.5.101/maxrest/rest/mbo/"; // String url =
     * "http://172.16.5.101/maxrest/rest/os/"; //String url =
     * "http://10.30.10.17/maxrest/rest/os/"; String url =
     * "http://10.20.2.113:80/maxrest/rest/os/";
     * 
     * String object = "mxincident"; String username = "maxadmin"; String
     * password = "maxadmin";
     * 
     * String ticketId = "BD1000";
     * 
     * // Creating Ticket Map<String, String> params = new HashMap<String,
     * String>(); //params.put("ticketid", ticketId); // params.put("status",
     * "NEW"); params.put("isknownerror", "1"); params.put("description",
     * "Hello from earth"); params.put("longdescription",
     * "First ticket using httpclient.jar"); params.put("class", "INCIDENT");
     * 
     * // RestCaller restCaller = new // RestCaller(
     * "http://172.16.5.101/maxrest/rest/mbo/sr?_lid=maxadmin&_lpwd=maxadmin");
     * // System.out.println(restCaller.postMethod(
     * "http://172.16.5.101/maxrest/rest/mbo/sr?_lid=maxadmin&_lpwd=maxadmin&ticketid=100001&class=SR&status=NEW&description=BDTest2"
     * )); RestCaller restCaller = new RestCaller(url + object + "?_lid=" +
     * username + "&_lpwd=" + password);
     * restCaller.setHttpbasicauthusername("maxadmin");
     * restCaller.setHttpbasicauthpassword("maxadmin");
     * System.out.println(restCaller.postMethod(params));
     * //System.out.println(restCaller.postMethodWithQueryString(params));
     * //System.out.println(restCaller.postMethodWithQueryString(params));
     * 
     * //String result = restCaller.getMethod(null);
     * //System.out.println(result); }
     */
}
