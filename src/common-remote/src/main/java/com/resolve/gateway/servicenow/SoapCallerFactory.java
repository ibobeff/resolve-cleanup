package com.resolve.gateway.servicenow;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.resolve.rsbase.MainBase;
import com.resolve.rsremote.ConfigReceiveServiceNow;
import com.resolve.util.StringUtils;

public class SoapCallerFactory
{
          
    
    public static SoapCaller getInstance(ConfigReceiveServiceNow config) throws Exception
    {
        
        
     
        
        List<SoapCaller.ConnectionModifier> modifiers = new ArrayList<>();
        handleBasicAuth(config, modifiers);
        handleMutualTls(config, modifiers);
        handleHeaderRedir(config, modifiers);
        handleProxyAuth(config, modifiers);
        
        SoapCaller.ConnectionOpener opener;
        
        opener = handleProxyConfig(config);
        
        return new SoapCaller(config.getUrl(), opener, modifiers.toArray(new SoapCaller.ConnectionModifier[modifiers.size()]));
        
    }

    private static void handleProxyAuth(ConfigReceiveServiceNow config, List<SoapCaller.ConnectionModifier> modifiers)
    {
        if(StringUtils.isNotEmpty(config.getProxyhost()) &&   StringUtils.isNotEmpty(config.getProxyuser()))
        {
            modifiers.add(new SoapCaller.HttpProxyAuthModifier(config.getProxyuser(), config.getProxypass()));

        }
    }

    protected static SoapCaller.ConnectionOpener handleProxyConfig(ConfigReceiveServiceNow config)
    {
      
        if(StringUtils.isNotEmpty(config.getProxyhost()))
        {
            SoapCaller.ConnectionOpener opener;
            if(StringUtils.isNotEmpty(config.getProxyuser()))
                opener =  new SoapCaller.ProxyConnectionOpener(config.getProxyhost(), config.getProxyport(), config.getProxytype(), config.getProxyuser(), config.getProxypass());
            else
                opener =  new SoapCaller.ProxyConnectionOpener(config.getProxyhost(), config.getProxyport(), config.getProxytype());
         
            return opener;
            
        }else
            if(MainBase.main != null && MainBase.main.configProxy.proxyEnabled)
                return  new SoapCaller.ProxyConnectionOpener(MainBase.main.configProxy); 
           
        return  new SoapCaller.DefaultConnectionOpener();
    }

    protected static void handleBasicAuth(ConfigReceiveServiceNow config, List<SoapCaller.ConnectionModifier> modifiers)
    {
        if(StringUtils.isNotEmpty(config.getHttpbasicauthusername()))
        {
            modifiers.add(new SoapCaller.HttpBasicAuthenticationModifier(config.getHttpbasicauthusername(), config.getHttpbasicauthpassword()));
            
        }
    }

    protected static void handleHeaderRedir(ConfigReceiveServiceNow config, List<SoapCaller.ConnectionModifier> modifiers) throws MalformedURLException
    {
        if(config.isHeaderredirenabled())
        {
            modifiers.add(new SoapCaller.HttpHeaderFieldRedirectModifier(config.getHeaderredirheader(), new URL(config.getHeaderredirurl()), config.isHeaderredirencauth()));
        }
    }

    protected static void handleMutualTls(ConfigReceiveServiceNow config, List<SoapCaller.ConnectionModifier> modifiers)
    {
        if(config.isMutualtlsenabled())
        {
            
            
            modifiers.add(new SoapCaller.MutualTlsModifier(config.getMutualtlskeystore(), config.getMutualtlskeystorepass(),
                            config.getMutualtlstruststore(), config.getMutualtlstruststorepass()));
            
            
            
            
        }
    }
}
