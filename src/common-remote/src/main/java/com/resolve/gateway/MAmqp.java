/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.Map;

import com.resolve.gateway.amqp.AmqpFilter;
import com.resolve.gateway.amqp.AmqpGateway;

/**
 * This class is used by the Message Listener and invokes it methods. Messages
 * destined for the gateways set this class name in the message header.
 *
 */
public class MAmqp extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MAmqp.setFilters";

    private static final AmqpGateway localInstance = AmqpGateway.getInstance();

    public MAmqp()
    {
        super.instance = localInstance;
    }

    @Override
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    @Override
    protected void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        if (localInstance.isActive())
        {
            AmqpFilter amqpFilter = (AmqpFilter) filter;
            filterMap.put(AmqpFilter.TARGET_QUEUE, amqpFilter.getTargetQueue());
            filterMap.put(AmqpFilter.IS_EXCHANGE, "" + amqpFilter.isExchange());
        }
    }
}
