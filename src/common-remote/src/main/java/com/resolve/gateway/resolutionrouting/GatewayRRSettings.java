package com.resolve.gateway.resolutionrouting;

public class GatewayRRSettings {

	private final String automation;
	private final String eventId;

	public GatewayRRSettings(String automation, String eventId) {
		this.automation = automation;
		this.eventId = eventId;
	}

	public String getAutomation() {
		return automation;
	}

	public String getEventId() {
		return eventId;
	}

}
