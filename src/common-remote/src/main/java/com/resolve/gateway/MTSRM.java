/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.Map;

import com.resolve.gateway.tsrm.TSRMFilter;
import com.resolve.gateway.tsrm.TSRMGateway;

/**
 * This class is used by the Message Listener and invokes it methods. Messages
 * destined for the gateways set this class name in the message header.
 *
 */
public class MTSRM extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MTSRM.setFilters";

    private static final TSRMGateway localInstance = TSRMGateway.getInstance();

    public MTSRM()
    {
        super.instance = localInstance;
    }

    @Override
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    
    /**
     * Populates the {@link Filter} data into the given filterMap {@link Map} instance
     * 
     * @param filterMap
     *      the {@link Map} instance to hold the {@link Filter} data
     * @param filter
     *      a {@link TSRMFilter} instance
     *            
     */
    @Override
    public void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        TSRMFilter tsrmFilter = (TSRMFilter) filter;
        filterMap.put(TSRMFilter.QUERY, tsrmFilter.getQuery());
        filterMap.put(TSRMFilter.OBJECT, tsrmFilter.getObject());
    } // populateGatewaySpecificProperties
}
