package com.resolve.gateway.resolvegateway;

import java.util.Map;
import java.util.Queue;

import java.util.List;

import com.resolve.gateway.Filter;
import com.resolve.util.GatewayEvent;
import com.resolve.gateway.BaseClusteredGateway;

public abstract class ResolveGateway extends BaseClusteredGateway implements GatewayInterface
{
    protected ConfigReceiveResolveGateway config = null;
    
    protected boolean serverAlive = false;
    
    protected ServerHeartbeat heartBeat = null;

    protected String name = null;
    
    protected String queue = null;
    
    public ResolveGateway(ConfigReceiveResolveGateway config) {
        
        super(config);
        this.config = config;
    }
    
    @Override
    public String getLicenseCode() {
        return queue;
    }

    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
    }

    @Override
    protected String getGatewayEventType() {
        return queue;
    }

    public String getQueueName() {
        return queue + getOrgSuffix();
    }
    
    public String getTopicName() {
        return topicName;
    }

    public Queue<GatewayEvent<String, Object>> getWorkerDataQueue() {
        return workerDataQueue;
    }
    
    public List<Filter> getOrderedFilters() {
        return orderedFilters;
    }
    
    public void setOrderedFilters(List<Filter> orderedFilters) {
        this.orderedFilters = orderedFilters;
    }
    
    public Map<String, Filter> getOrderedFiltersMapById() {
        return orderedFiltersMapById;
    }
    
    public void setOrderedFiltersMapById(Map<String, Filter> orderedFiltersMapById) {
        this.orderedFiltersMapById = orderedFiltersMapById;
    }
    
    protected int validateFilterFields(Map<String, Object> params) {
        
        return 0;
    }
    
    protected String getValidationError(int errorCode) {
        
        return null;
    }
    
    protected boolean sendHeartbeat() {
        
        return true;
    }
    
    protected void suspend() {
        ;
    }
    
    protected void resume() {
        ;
    }

    protected boolean isServerAlive()
    {
        return serverAlive;
    }

    protected void setServerAlive(boolean alive)
    {
        serverAlive = alive;
    }
    
    protected int getServerCheckInterval() {

        return 0;
    }

    protected int getServerCheckRetry() {

        return 0;
    }
    
} // class ResolveGateway