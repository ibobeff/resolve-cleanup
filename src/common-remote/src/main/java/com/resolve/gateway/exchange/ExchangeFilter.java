/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.exchange;

import java.util.HashMap;
import java.util.Map;

import com.resolve.gateway.BaseFilter;

public class ExchangeFilter extends BaseFilter
{
    public static final String SUBJECT = "SUBJECT";
    public static final String CONTENT = "CONTENT";
    public static final String SENDERNAME = "SENDERNAME";
    public static final String SENDERADDRESS = "SENDERADDRESS";
    public static final String CRITERIA = "CRITERIA";

    private String subject;
    private String content;
    private String senderName;
    private String senderAddress;

    private Map<String, String> criteria = new HashMap<String, String>();
    long lastQuery; // local timestamp (millis)
    long lastSuccessfulQuery; // remote OMNIbus timestamp (secs)

    public ExchangeFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String subject, String content, String senderName, String senderAddress, Map<String, String> criteria)
    {
        super(id, active, order, interval, eventEventId, runbook, script);
        setSubject(subject);
        setContent(content);
        setSenderName(senderName);
        setSenderAddress(senderAddress);
        setCriteria(criteria);
    } // ExchangeFilter

    public String getSubject()
    {
        return subject;
    }

    public void setSubject(String subject)
    {
        this.subject = subject != null ? subject.trim() : subject;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content != null ? content.trim() : content;
    }

    public String getSenderName()
    {
        return senderName;
    }

    public void setSenderName(String sendername)
    {
        this.senderName = sendername != null ? sendername.trim() : sendername;
    }

    public String getSenderAddress()
    {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress)
    {
        this.senderAddress = senderAddress != null ? senderAddress.trim() : senderAddress;
    }

    public Map<String, String> getCriteria()
    {
        return this.criteria;
    }

    public void setCriteria(Map<String, String> criteria)
    {
        this.criteria = criteria;
    }

    public long getLastQuery()
    {
        return lastQuery;
    }

    public void setLastQuery(long lastQuery)
    {
        this.lastQuery = lastQuery;
    }

    public long getLastSuccessfulQuery()
    {
        return lastSuccessfulQuery;
    }

    public void setLastSuccessfulQuery(long lastSuccessfulQuery)
    {
        this.lastSuccessfulQuery = lastSuccessfulQuery;
    }
} // ExchangeFilter
