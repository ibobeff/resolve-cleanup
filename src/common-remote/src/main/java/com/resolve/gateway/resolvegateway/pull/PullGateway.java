package com.resolve.gateway.resolvegateway.pull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.resolve.gateway.Filter;
import com.resolve.gateway.MPullGateway;
import com.resolve.gateway.resolvegateway.GatewayProperties;
import com.resolve.gateway.resolvegateway.ResolveGateway;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class PullGateway extends ResolveGateway
{
    protected static volatile PullGateway instance = null;
    
    private static volatile Map<String, PullGatewayFilter> pullFilters = new ConcurrentHashMap<String, PullGatewayFilter>();
    
    protected PullGatewayInterface impl = null;

    protected PullGateway(ConfigReceivePullGateway config) {
        
        super(config);
    }
    
    public static PullGateway getInstance(ConfigReceivePullGateway config) {

        instance = new PullGateway(config);

        
        return instance;
    }
    
    public static PullGateway getInstance() {
        if (instance == null) {
            throw new RuntimeException("Pull Gateway is not initialized correctly..");
        } else {
            return instance;
        }
    }
    
    public static PullGateway getInstance(String gatewayName) {
        
        Map<String, PullGateway> gateways = ConfigReceivePullGateway.getGateways();
        
        for(String gateway:gateways.keySet()) {
            if(gateway.toLowerCase().equals(gatewayName))
                return gateways.get(gateway);
        }
        
        return null;
    }

    public static Map<String, PullGatewayFilter> getPullFilters()
    {
        return pullFilters;
    }
    
    public static void addPullFilter(String filterName, PullGatewayFilter filter) {
        
        pullFilters.put(filterName, filter);
    }
    
    public static void removePullFilter(String filterName) {
        
        pullFilters.remove(filterName);
    }
    
    @Override
    protected void addAdditionalValues(Map<String, Object> arg0, Map<String, String> arg1) {
    }

    @Override
    protected String getMessageHandlerName() {
        return MPullGateway.class.getSimpleName();
    }

    @Override
    protected Class<MPullGateway> getMessageHandlerClass() {
        return MPullGateway.class;
    }
    
    @Override
    public String getLicenseCode() {
        
        return queue;
    }
    
    @Override
    protected String getGatewayEventType() {
        
        return queue;
    }

    @Override
    protected void initialize() {
        
    }
    
    @Override
    public void reinitialize() {
        super.reinitialize();
        running = true;
    }
    
    @Override
    public void deactivate() {
        
        running = false;
        
        super.deactivate();
    }
    
    @Override
    public void stop() {
/*        
        // Save the filters to local config.xml when RSRemote is gracefully shutdown
        try {
            ((ConfigReceivePullGateway)config).saveFilters(name, filters);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
*/
        super.stop();
    }
    
    @Override
    public void run() {
        
        String gatewayName = config.getGatewayName();
        PullGateway gateway = null;
        super.sendSyncRequest();
        try {
            gateway = ConfigReceivePullGateway.getGateways().get(gatewayName);
            
            if(gateway == null)
                throw new Exception("Gateway " + gatewayName + " does not exist");
        } catch (Exception e) {
            Log.log.error(e.getMessage(), e);
            return;
        }

        while (running) {
            try {
                long startTime = System.currentTimeMillis();
                
                if (primaryDataQueue.isEmpty()) {
                    Log.log.trace("Local Primary Event Queue for " + getGatewayEventType() + " is empty.....");
                    List<Filter> orderedFilters = gateway.getOrderedFilters();
                    for (Filter filter : orderedFilters) {
                        try {
                            Log.putCurrentNamedContext(Constants.GATEWAY_FILTER, filter.getId());
                            if (shouldFilterExecute(filter, startTime)) {
                                Log.log.trace("Filter " + filter.getId() + " executing");
                                
                                //Need to get the complete filter detail here for making the call
//                                PullGatewayFilter currentFilter = ResolveGatewayUtil.loadGatewayFilters(gatewayName, "pull", queueName)
                                PullGatewayFilter currentFilter = (PullGatewayFilter)filter;
                                List<Map<String, String>> results = invokeService(currentFilter);
                                
                                
                            } else {
                                Log.log.trace("Filter:" + filter.getId() + " is inactive or not ready to execute yet.....");
                            }
                        } finally {
                            Log.clearCurrentNamedContext("Filter");
                        }
                    }
                    if (orderedFilters == null || orderedFilters.size() == 0)
                        Log.log.trace("There is not deployed filter for the gateway.");
                }

                if (orderedFilters.size() == 0)
                    Thread.sleep(interval);
            } catch (Exception e) {
                e.printStackTrace();
                Log.log.warn("Warning: " + e.getMessage());
                Log.alert("Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed", "Instance " + MainBase.main.configId.getGuid() + " " + getLicenseCode() + "-" + getQueueName() + "-" + getInstanceType() + " Polling Failed " + "due to " + e.getMessage());
                try {
                    Thread.sleep(interval);
                } catch (InterruptedException ie) {
                    Log.log.warn("sleep interrupted: " + ie.getMessage(), ie);
                }
            }
        }
    }
    
    /**
     * Add customized code here to get the data as List<Map<String, String>> format for each filter 
     * @return List<Map<String, String>>
     */
    public List<Map<String, String>> invokeService(PullGatewayFilter filter) {
        
        List<Map<String, String>> results = new ArrayList<Map<String, String>>();
        
        try {
            results = ((PullGatewayInterface)impl).retrieveData(filter);
            
            if(results.size() == 0)
                return results;
            
            for (Map<String, String> result : results) {
                result.put(Constants.WORKSHEET_ALERTID, result.get("evid"));
                
                result.put(FILTER_ID_NAME, filter.getId());
                result.put(GATEWAY_NAME, filter.getGatewayName());
                result.put(GATEWAY_TYPE, "Pull");
                
                addToPrimaryDataQueue(filter, result);
            }
            
            ((PullGatewayInterface)impl).updateAck(results);
            
            ((PullGatewayInterface)impl).updateLastValues(filter, results);
            
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        
        return results;
    }

    @Override
    public PullGatewayFilter getFilter(Map<String, Object> params) {
        
        return configurePullGatewayFilter(params);
    }

    public List<Filter> deployFilters(List<Map<String, Object>> filterList, String username) throws Exception {
        
        Log.log.debug("Calling Gateway specific deployFilters" + (filterList != null ? " " + filterList.size() + ", [" + filterList + "]" : ""));

        List<Filter> deployedFilters = new ArrayList<Filter>();
        
        String gatewayName = "";
        PullGateway gateway = null;
        
        for (Map<String, Object> params : filterList) {
            if (!params.containsKey("RESOLVE_USERNAME")) {
                gatewayName = params.get("GATEWAY_NAME") != null ? params.get("GATEWAY_NAME").toString().trim() : null;
                if(gatewayName ==null)
                    continue;
                
                gateway = ConfigReceivePullGateway.getGateways().get(params.get("GATEWAY_NAME"));
                
                if(gateway == null) {
                    Log.log.warn("Gateway " + gatewayName + " does not exist");
                    continue;
                }
                
                int filterValidationErrCode = gateway.validateFilterFields(params);
                
                if (filterValidationErrCode != 0)
                    throw new Exception("Filter Validation Returned Error Code " + filterValidationErrCode + " : " + gateway.getValidationError(filterValidationErrCode));
                
                String name = (String) params.get("ID");
                String queue = (String) params.get("QUEUE");

                PullGatewayFilter filter = getFilter(params);
                
                filter.setDeployed(true);
                filter.setUpdated(false);
                filter.setQueue(queue);
                filter.setUsername(username);
                
                Map<String, Filter> gatewayFilters = gateway.getFilters();
                gatewayFilters.put(name, filter);
                
                filters.put(name, filter);
                pullFilters.put(name, filter);
                deployedFilters.add(filter);
            }
        }
        PullGatewayFilter pullFilter;
        if (isPrimary() && isActive()) {
            try {
                List<Filter> orderedFilters = gateway.getOrderedFilters();
                Map<String, Filter> orderedFiltersMapById = gateway.getOrderedFiltersMapById();
                orderedFilters.clear();
                synchronized (orderedFilters)
                {
                    for(Iterator<Filter> it=deployedFilters.iterator(); it.hasNext();) {
                    	pullFilter = (PullGatewayFilter)it.next();
                    	int val = hasFilter(orderedFilters, pullFilter);
	               		 if(val == -1) {
	               			orderedFilters.add(pullFilter);
	               		 }else {
	               			PullGatewayFilter deployedFilter = (PullGatewayFilter)orderedFilters.get(val);
	               			//retaining the last value
	               			pullFilter.setTimeRange(deployedFilter.getTimeRange());
	               			pullFilter.setLastExecutionTime(deployedFilter.getLastExecutionTime()); 
	               			pullFilter.setLastId(deployedFilter.getLastId());
	               			pullFilter.setLastValue(deployedFilter.getLastValue());
	               			orderedFilters.remove(val);
	                		orderedFilters.add(pullFilter);
	               		 }
                    }
                    Collections.sort(orderedFilters);
                    orderedFiltersMapById.clear();
                    for (Filter filter : orderedFilters)
                        orderedFiltersMapById.put(filter.getId(), filter);
                }
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        return deployedFilters;
    } // deployFilters()

    private int hasFilter(List<Filter> orderedFilters, PullGatewayFilter pullFilter) {
     	boolean isPresent = false;
     	int index  = 0;
     	int foundVal = -1;
     	ListIterator<Filter> itr = orderedFilters.listIterator();
     	while(itr.hasNext()) {
     		PullGatewayFilter ordFilter = (PullGatewayFilter)itr.next();
     		if(ordFilter.getId().equals(pullFilter.getId())) {
     			foundVal = index;
     			break;
     		}
     		index += 1;
     	}
     	
     	return foundVal;
     }
    /**
     * This utility method is to update a filter data in the list of ordered filter data
     * @param filter
     * @return
     */
    public boolean updateOrderedFilter(PullGatewayFilter filter) {
        boolean updated = false;
        PullGatewayFilter pullFilter = null;
        if (isPrimary() && isActive()) {
            try {
                List<Filter> orderedFilters = this.getOrderedFilters();
                int index = 0;
                synchronized (orderedFilters)
                {
                    //Look for the updated filter in the orderdFilters list
                     for(Iterator<Filter> it=orderedFilters.iterator(); it.hasNext();) {
                         pullFilter = (PullGatewayFilter)it.next();
                         if(pullFilter.getId().equals(filter.getId())) {
                             break;
                         }
                         index += 1;
                     }
                    //If the deployed filter is found in the list update it
                    if(pullFilter != null) {
                        
//                      orderedFilters.remove(pullFilter);
                        orderedFilters.remove(index);
                        orderedFilters.add(filter);
                        Collections.sort(orderedFilters);
                        updated = true;
                    }
                }
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
        return updated;
    }
    
    
    public List<Filter> undeployFilters(List<Map<String, Object>> filterList, String username) throws Exception {
        
        Log.log.debug("Calling Gateway specific undeployFilters" + (filterList != null ? " " + filterList.size() + ", [" + filterList + "]" : ""));

        List<Filter> undeployedFilters = new ArrayList<Filter>();
        
        String gatewayName = "";
        PullGateway gateway = null;
        
        for (Map<String, Object> params : filterList) {
            if (!params.containsKey("RESOLVE_USERNAME"))
            {
                gatewayName = params.get("GATEWAY_NAME") != null ? params.get("GATEWAY_NAME").toString().trim() : null;
                if(gatewayName ==null)
                    continue;
                
                gateway = ConfigReceivePullGateway.getGateways().get(params.get("GATEWAY_NAME"));
                
                if(gateway == null) {
                    Log.log.warn("Gateway " + gatewayName + " does not exist");
                    continue;
                }
                String name = (String) params.get("ID");
                
                Map<String, Filter> filters = gateway.getFilters();
                filters.remove(name);

                filters.remove(name);
                pullFilters.remove(name);
                
                PullGatewayFilter filter = getFilter(params);
                
                filter.setDeployed(false);
                filter.setUpdated(false);
                filter.setUsername(username);
                
                undeployedFilters.add(filter);
            }
        }
        
        if (isPrimary() && isActive()) {
            try {
                List<Filter> orderedFilters = gateway.getOrderedFilters();
                Map<String, Filter> orderedFiltersMapById = gateway.getOrderedFiltersMapById();
                
                synchronized(orderedFilters)
                {
                    for(Iterator<Filter> it=undeployedFilters.iterator(); it.hasNext();) {
                        Filter undeployedFilter = it.next();
                        
                        for(Filter orderedFilter:orderedFilters) {
                            if(orderedFilter.getId().equals(undeployedFilter.getId()))
                                orderedFilters.remove(orderedFilter);
                        }
                    }

                    Collections.sort(orderedFilters);
                    
                    orderedFiltersMapById.clear();
                    
                    for (Filter filter : orderedFilters)
                        orderedFiltersMapById.put(filter.getId(), filter);
                }
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        return undeployedFilters;
    } // undeployFilters()
    
    public List<Filter> getOrderedFilters() {
        return orderedFilters;
    }
    
    public Map<String, Object> loadSystemProperties(String gatewayName) {

        Map<String, Object> systemProperties = new HashMap<String, Object>();
        
        PullGatewayProperties properties = (PullGatewayProperties)ConfigReceivePullGateway.getGatewayProperties(gatewayName);

        systemProperties.put("HOST", ((PullGatewayProperties)properties).getHost());
        systemProperties.put("PORT", ((PullGatewayProperties)properties).getPort());
        systemProperties.put("SSL", ((PullGatewayProperties)properties).isSsl());
        systemProperties.put("SSLTYPE", ((PullGatewayProperties)properties).getSslType());
        systemProperties.put("USER", ((PullGatewayProperties)properties).getUser());
        systemProperties.put("PASS", ((PullGatewayProperties)properties).getPass());

        return systemProperties;
    }
    
    private PullGatewayFilter configurePullGatewayFilter(Map<String, Object> filterAttrs) {
        
        String id = null;
        String active = null;
        String order = null;
        String interval = null;
        String eventEventId = null;
        String runbook = null;
        String script = null;
        
        for(Iterator<String> it=filterAttrs.keySet().iterator(); it.hasNext();) {
            String key = (String)it.next();
            String value = (String)(filterAttrs.get(key));
            if("ID".equals(key))
                id = value;
            else if("ACTIVE".equals(key))
                active = value;
            else if("ORDER".equals(key))
                order = value;
            else if("INTERVAL".equals(key))
                interval = value;
            else if("EVENT_EVENTID".equals(key))
                eventEventId = value;
            else if("RUNBOOK".equals(key))
                runbook = value;
            else if("SCRIPT".equals(key))
                script = value;
        }
        
        PullGatewayFilter filter = new PullGatewayFilter(id, active, order, interval, eventEventId, runbook, script);
        
        try {
            filter.setGatewayName((String)filterAttrs.get("GATEWAY_NAME"));
            filter.setQueue((String)filterAttrs.get("QUEUE"));
            filter.setType((String)filterAttrs.get("TYPE"));
            filter.setQuery((String)filterAttrs.get("QUERY"));
            filter.setTimeRange((String)filterAttrs.get("TIME_RANGE"));
            filter.setLastTimestamp((String)filterAttrs.get("LAST_TIMESTAMP"));
            filter.setLastId((String)filterAttrs.get("LAST_ID"));
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        
        filter.addAttributes(filterAttrs);

        return filter;
    }
    
    public String buildCustomQuery(Pattern pattern, String query, Map<String, String> fieldValuePairs) throws Exception {

        String customQuery = query;

        if(pattern == null || StringUtils.isEmpty(customQuery))
            throw new Exception("Pattern is not available or query is empty.");
        
        try {
            Matcher matcher = pattern.matcher(customQuery);

            if(!matcher.find())
                return query;
            
            for(Iterator<String> it=fieldValuePairs.keySet().iterator();it.hasNext();) {
                String key = it.next();
                String value = fieldValuePairs.get(key);
                
                if(StringUtils.isBlank(key))
                    continue;
                
                String keyStr = "\\$\\{" + key + "\\}";
                if(!customQuery.contains(key) || value.contains(keyStr))
                    continue;
                
                String field = matcher.group(0);

                if(field.equals("${" + key + "}"))
                    customQuery = customQuery.replaceAll(keyStr, value);
            }
            
            Log.log.debug("custom query = " + customQuery);
        } catch(Exception e) {
//                e.printStackTrace();
            Log.log.error("Failed to translate the input query, use default query instead.", e);
            query = "";
        }

        return customQuery;
    }
    
    protected int getServerCheckInterval() {

        GatewayProperties properties = ConfigReceivePullGateway.getGatewayProperties(name);
        if(properties != null)
            return properties.getServerCheckInterval();

        return 0;
    }

    protected int getServerCheckRetry() {

        GatewayProperties properties = ConfigReceivePullGateway.getGatewayProperties(name);
        if(properties != null)
            return properties.getServerCheckRetry();

        return 0;
    }
    
} // class PullGateway