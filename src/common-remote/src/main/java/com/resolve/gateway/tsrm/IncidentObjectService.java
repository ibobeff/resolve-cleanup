/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.tsrm;

import java.util.Map;

import com.resolve.util.StringUtils;

/**
 * This class acts as a service to manage incident.
 * 
 * @author bipuldutta
 * 
 */
public class IncidentObjectService extends AbstractObjectService
{
    static String OBJECT_IDENTITY = "mxincident";
    static String MAIN_NODE_NAME = "MXINCIDENTSet";
    static String OBJECT_NODE_NAME = "INCIDENT";

    private static String IDENTITY_PROPERTY = "TICKETUID";

    public IncidentObjectService(RestCaller restCaller)
    {
        super(restCaller);
    }

    @Override
    public String getIdPropertyName()
    {
        return IDENTITY_PROPERTY;
    }

    @Override
    public String getIdentity()
    {
        return OBJECT_IDENTITY;
    }

    @Override
    public String getMainNodeName()
    {
        return MAIN_NODE_NAME;
    }

    @Override
    public String getObjectNodeName()
    {
        return OBJECT_NODE_NAME;
    }

    @Override
    public Map<String, String> create(Map<String, String> objectProperties, String username, String password) throws Exception
    {
        try
        {
            return super.create(objectProperties, username, password);
        }
        catch (Exception ex)
        {
            if (ex.getMessage().startsWith(ErrorHandler.ERROR_400))
            {
                throw new Exception(ErrorHandler.ERROR_400 + " Incident cannot be created, possibly duplicate.");
            }
            throw ex;
        }
    }

    @Override
    public boolean createWorklog(String parentObjectId, Map<String, String> params, String username, String password) throws Exception
    {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            username = restCaller.getUsername();
            password = restCaller.getPassword();
        }

        // this will throw Exception is parentObjectId is null or empty.
        validateObjectId(parentObjectId);

        // appends the parentObjectId to the URL.
        String modifiedUrl = restCaller.addObjectId(parentObjectId, restCaller.getOriginalUrl(), username, password, getIdentity());
        restCaller.setUrl(modifiedUrl);

        restCaller.postMethod(getModifiedParams(1, params));

        return true;
    }

    @Override
    public void delete(String objectId, String username, String password) throws Exception
    {
        try
        {
            super.delete(objectId, username, password);
        }
        catch (Exception ex)
        {
            if (ex.getMessage().startsWith(ErrorHandler.ERROR_400))
            {
                throw new Exception(ErrorHandler.ERROR_400 + " Incident cannot be deleted, possibly its in an undeletable status or has some child objects.");
            }
            throw ex;
        }
    }
}
