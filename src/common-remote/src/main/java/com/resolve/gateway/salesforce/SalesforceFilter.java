/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.salesforce;

import com.resolve.gateway.BaseFilter;
import com.resolve.gateway.RetainValue;

public class SalesforceFilter extends BaseFilter
{
    // Additional field names for this filter.
    public static final String QUERY = "QUERY";
    public static final String OBJECT = "OBJECT";
    public static final String LAST_CHANGE_DATE = "LASTCHANGEDATE";
    public static final String LAST_TICKET_ID = "LAST_TICKET_ID";

    private String object;
    private String query;
    private String nativeQuery; // stores the native query after translation by
                                // Resolve Query translator.
    private String lastChangeDate;
    private String lastTicketId;

    public SalesforceFilter(String id, String active, String order, String interval, String eventEventId, String runbook, String script, String object, String query, String lastTicketId)
    {
        super(id, active, order, interval, eventEventId, runbook, script);
        setObject(object);
        setQuery(query);
        setLastTicketId(lastTicketId);
    } // SalesforceFilter

    public String getObject()
    {
        return object;
    }

    public void setObject(String object)
    {
        this.object = object != null ? object.trim() : object;
    }

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query != null ? query.trim() : query;
    }

    @RetainValue
    public String getLastChangeDate()
    {
        return lastChangeDate;
    }

    public void setLastChangeDate(String lastChangeDate)
    {
        this.lastChangeDate = lastChangeDate != null ? lastChangeDate.trim() : lastChangeDate;
    }

    public String getNativeQuery()
    {
        return nativeQuery;
    }

    public void setNativeQuery(String nativeQuery)
    {
        this.nativeQuery = nativeQuery != null ? nativeQuery.trim() : nativeQuery;
    }

    @RetainValue
    public String getLastTicketId()
    {
        return lastTicketId;
    }

    public void setLastTicketId(String lastTicketId)
    {
        this.lastTicketId = lastTicketId != null ? lastTicketId.trim() : "0";
    }
}
