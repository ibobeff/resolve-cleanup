package com.resolve.gateway.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.owasp.esapi.ESAPI;

import com.resolve.gateway.Filter;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.ResolveHttpServlet;
import com.resolve.util.StringUtils;
import com.resolve.util.SystemUtil;

import groovy.lang.Binding;
import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;

public class FilterServlet extends ResolveHttpServlet
{
    private static final long serialVersionUID = 7831538166676706839L;

	private final String filterName;
    private static long waitTime = 2000;

    protected ExecutorService executor = Executors.newFixedThreadPool(SystemUtil.getCPUCount());

    private HttpGateway instance = HttpGateway.getInstance();
    
    static String TYPE_JSON = "application/json";
    static String HTTP_REQUEST_BODY = "HTTP_REQUEST_BODY";

    public FilterServlet(String filterName)
    {
        this.filterName = filterName;
        // Load waitTime from blueprint.properties
        waitTime = instance.getWaitTimeout();
    }
    private void setContext( ) {
        Log.setCurrentContext("HttpGateway");
        Log.putCurrentNamedContext(Constants.GATEWAY_FILTER, filterName);
       
    }
    private void clearContext() {
        Log.clearContext();
        Log.clearCurrentNamedContext(Constants.GATEWAY_FILTER);
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try {
            setContext();
            Log.log.info("HTTPGateway: GET Request received --> " + request.getRequestURI());
//          response.setContentType("text/plain");
          
          Map<String, Filter> filters = instance.getFilters();
          HttpFilter httpFilter = (HttpFilter)filters.get(filterName);
          
          int blocking = 1;
          
          if(httpFilter.getBlocking() != null)
              blocking = ((HttpFilter)httpFilter).getBlocking().intValue();

          String contentType = request.getContentType();
          boolean isJson = true;
          if (StringUtils.isNotBlank(contentType) && !TYPE_JSON.equalsIgnoreCase(contentType) && blocking == 2)
          {
              isJson = false;
          }
        //GAT-26 setting content type based on filter drop down selection
          String filterContentType = httpFilter.getContentType();
          if (StringUtils.isNotBlank(filterContentType) && TYPE_JSON.equalsIgnoreCase(filterContentType))
          {
              isJson = true;
          }
          
          if (isJson)
          {
              ESAPI.httpUtilities().setHeader(response, "Content-Type", StringEscapeUtils.escapeJava("application/json"));
          }    
          else
          {
              if(blocking == 2)
                  ESAPI.httpUtilities().setHeader(response, "Content-Type", StringEscapeUtils.escapeJava(contentType));
              else
                  ESAPI.httpUtilities().setHeader(response, "Content-Type", StringEscapeUtils.escapeJava("application/json"));
          }        
          
          Log.log.info("HTTPGateway: Incoming get request isJson --> " + isJson);
          response.setStatus(HttpServletResponse.SC_OK);

          Map<String, String> params = new HashMap<String, String>();     
   
          //this loop will read from the query string parameters. e.g. ?name1=value&name2=value2
          Map<String, String[]> requestParameters = request.getParameterMap();
          for (String key : requestParameters.keySet())
          {
              if (requestParameters.get(key).length > 0)
              {
                  params.put(key, StringUtils.arrayToString(requestParameters.get(key), ","));
              }
          }
          
          if(blocking > 3) {
              String result = process(instance, filterName, request, response, params, isJson);
              if(blocking == 5) {
                  response.getWriter().println(getResponseFilter(result));
              }else {
                  response.getWriter().println(result);
              }
          }
          
          else if(blocking > 1) {
              String result = processBlocking(instance, filterName, request, response, params, isJson);
              response.getWriter().println(result);
          }

          else {
              executor.execute(new RequestProcessor(instance, filterName, request, response, params));
              
              JSONObject message = new JSONObject();
              message.accumulate("Message", "Success.");
              response.getWriter().println(message.toString());
          }
        } finally {
            clearContext();
        }

    } // doGet()
    
    private Binding getGroovyBinding(Map<String, String> content) {
    	
    	Binding binding = new Binding();
        binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
        binding.setVariable(Constants.GROOVY_BINDING_DEBUG, false);
        binding.setVariable(Constants.GROOVY_BINDING_INPUTS, content);
        binding.setVariable(Constants.GROOVY_BINDING_ESB, MainBase.esb);
        
        return binding;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try {
            setContext();
            if(Log.log.isTraceEnabled())
            {
                Log.log.trace(request.getRequestURI());
                Log.log.trace("HTTPGateway: Client IP --> " + request.getRemoteAddr());
                Log.log.trace("HTTPGateway: Client Host --> " + request.getRemoteHost());
            }
            Log.log.info("HTTPGateway: POST request received --> " + request.getRequestURI());


            Map<String, Filter> filters = instance.getFilters();
            HttpFilter httpFilter = (HttpFilter)filters.get(filterName);
            
            int blocking = 1;
            
            if(httpFilter.getBlocking() != null)
                blocking = ((HttpFilter)httpFilter).getBlocking().intValue();
            
            String contentType = request.getContentType();
            boolean isJson = true;
            if (StringUtils.isNotBlank(contentType) && !TYPE_JSON.equalsIgnoreCase(contentType) && blocking == 2)
            {
                isJson = false;
            }
            //GAT-26 setting content type based on filter drop down selection
            String filterContentType = httpFilter.getContentType();
            if (StringUtils.isNotBlank(filterContentType) && TYPE_JSON.equalsIgnoreCase(filterContentType))
            {
                isJson = true;
            }
            
            if (isJson)
            {
//                response.setContentType("application/json");
                ESAPI.httpUtilities().setHeader(response, "Content-Type", StringEscapeUtils.escapeJava("application/json"));
            }    
            else
            {
                if(blocking == 2)
                    ESAPI.httpUtilities().setHeader(response, "Content-Type", StringEscapeUtils.escapeJava(contentType));
                else
                    ESAPI.httpUtilities().setHeader(response, "Content-Type", StringEscapeUtils.escapeJava("application/json"));
            }        
            
            response.setStatus(HttpServletResponse.SC_OK);
            //get request Body for Post calls
            String inputString = StringUtils.toString(request.getInputStream(), "utf-8");
            Log.log.debug("HTTPGateway: POST Received request body --> " + inputString);
            //these are the post parameters through a form
            Map<String, String> params = new HashMap<String, String>();     
            //If receiving JSON, urlToMap will not work correctly, and instead add the whole JSON object as a key
            if (StringUtils.isNotBlank(contentType) && !TYPE_JSON.equalsIgnoreCase(contentType))
            {
                params = StringUtils.urlToMap(inputString);
            }
            params.put(HTTP_REQUEST_BODY, inputString);
            
            Log.log.info("HTTpgateway: Incoming post request isJson --> " + isJson);
            //this loop will read from the query string parameters. e.g. ?name1=value&name2=value2
            Map<String, String[]> requestParameters = request.getParameterMap();
            Log.log.debug("HTTPGateway: POST request received params --> " + params != null
                    ? params.toString(): "No Params sent");
            for (String key : requestParameters.keySet())
            {
                if (requestParameters.get(key).length > 0)
                {
                    params.put(key, StringUtils.arrayToString(requestParameters.get(key), ","));
                }
            }

            if(blocking > 3) {
                response.setContentType("application/json");
                String result = process(instance, filterName, request, response, params, isJson);
                if(blocking == 5) {
                    response.getWriter().println(getResponseFilter(result));
                }else {
                    response.getWriter().println(result);
                }
            }
            
            else if(blocking > 1) {
                String result = processBlocking(instance, filterName, request, response, params, isJson);
                response.getWriter().println(result);
            }

            else {
                executor.execute(new RequestProcessor(instance, filterName, request, response, params));
                
                JSONObject message = new JSONObject();
                message.accumulate("Message", "HTTP request submitted successfully.");
                response.getWriter().println(message.toString());
            }
        } finally {
            this.clearContext();
        }

    } // doPost()
    /**
     * 
     * @param request
     * @return
     * @throws IOException
     */
    public static String getBody(HttpServletRequest request) throws IOException {

        String body = null;
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;

        try {
            InputStream inputStream = request.getInputStream();
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int bytesRead = -1;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            } else {
                stringBuilder.append("");
            }
        } catch (IOException ex) {
            throw ex;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    throw ex;
                }
            }
        }

        body = stringBuilder.toString();
        return body;
    }
    
    private String getResponseFilter(String result) {
    	String response = "";
        	ObjectMapper mapper = new ObjectMapper();
        	JSONObject resultJson = JSONObject.fromObject(result);	
        	JSONObject msgJson = null;
        	JSONArray resJSONArr = null;
        	try {
        		msgJson = JSONObject.fromObject(resultJson.get("Message") != null ? resultJson.get("Message") : "{ok:None}");
        	}catch(Exception ex) {
        		return (String)resultJson.get("Message");
        	}
        	try {
        		resJSONArr = JSONArray.fromObject(msgJson.get("Action Tasks") != null ? msgJson.get("Action Tasks") : "{ok:None}");
        	}catch(Exception ex) {
        		return "{Error: No Action Tasks returned by the runbook execution}";
        	}
        	//If there was an action task it will pass this
        	try {
            	if(resJSONArr != null && !resJSONArr.get(0).equals(new String("null"))) {
            		Iterator<JSONObject> resjsonItr = resJSONArr.iterator();    
            		if(resjsonItr != null) {
            			Map<String,String> jsonMap = null;
		            	while(resjsonItr.hasNext()) {
		            		JSONObject jsonObj = resjsonItr.next();
		            		String actTaskName = jsonObj.getString("name");
		            		if(StringUtils.isNotBlank(actTaskName) && actTaskName.substring(0, actTaskName.indexOf("#")).
	    							equalsIgnoreCase("httpfilter_response")) {
		            			//Overwriting the previous result because we want to take the last AT
		            			//with the name httpfilter_response as our result
				    			jsonMap = mapper.readValue(jsonObj.toString(), new HashMap<String, String>().getClass());
		            		}
		            	}
		    			if(jsonMap != null && jsonMap.get("detail") != null){
		    				Log.log.info("AT with name httpfilter_response contains --> " + jsonMap.toString());
		    				return JSONObject.fromObject(jsonMap.get("detail")).toString();
		    			}else {
		    				return "The <httpfilter_response> doesn't have details set or is blank ";
		    			}
			    		
            		}
            		return "The provided Runbook doesn't have <httpfilter_response> action task ";
         
            	}else {
            		response =  "The provided Runbook doesn't have <httpfilter_response> action task ";
            	}
        	}catch(Exception ex) {
        		Log.log.error("Error processing <Filtered Response> blocking code type ");
        		Log.log.error("ERROR:",ex);
        		return "ERROR:: There was an error response filter option, either the <httpfilter_response> is missing"
        				+ "or the Runbbok didn't execute properly, check the worksheet data on Resolve ";
        	}
        	return response;
    }
    
    
    @SuppressWarnings("unused")
    private String process(final HttpGateway instance, final String filterName, final HttpServletRequest request, final HttpServletResponse response, final  Map<String, String> params) 
    {    
        return process(instance, filterName, request, response, params, true);
    }
    
    private String process(final HttpGateway instance, final String filterName, final HttpServletRequest request, final HttpServletResponse response, final  Map<String, String> params, boolean isJson) {
        
        String resultStr = processBlocking(instance, filterName, request, response, params);
        Log.log.info("HTTPGateway: Synchronous HTTPGateway processing result: " + resultStr + ", isJson = " + isJson);
        
        JSONObject message = new JSONObject();

        if(StringUtils.isBlank(resultStr)) {
            message.accumulate("Message", "No information is available at this time.");
            return message.toString();
        }

        try {
        	String result = resultStr;
        	
        	try {
	            JSONObject obj = JSONObject.fromObject(resultStr);
	            result = obj.getString("Message");
        	} catch (JSONException jsone) {
        		Log.log.info("HTTPGateway: Result Not Wrapped in Message");
        	}
            
            if(StringUtils.isBlank(result)) {
                message.accumulate("Message", "No information is available at this time.");
                return message.toString();
            }
            
            if(result.toLowerCase().indexOf("timed out") != -1 || result.indexOf("Failed to ") != -1)
                return resultStr;
  
            JSONObject json = JSONObject.fromObject(result);
            
            String problemId = json.getString("Worksheet Id");
            String processId = json.getString("Process Id");
            String wiki = json.getString("Wiki Runbook");
            
            Log.log.info("HTTPGateway: problemId = " + problemId + "!");
            Log.log.info("HTTPGateway: processId = " + processId + "!");
            
            if(StringUtils.isBlank(problemId) || StringUtils.isBlank(processId)) {
                message.accumulate("Message", "No information is available at this time!");
                return message.toString();
            }
        
            String status = instance.getRunbookResult(problemId, processId, wiki);
            
            if(StringUtils.isBlank(status))
                message.accumulate("Message", "Timed out.");
            
            else 
                message.accumulate("Message", status);

        } catch(Exception e) {
            Log.log.error("HTTPGateway: " + e.getMessage(), e);
            message.accumulate("Message", e.getMessage());
        }

        return message.toString();
    } // process()
    
    private String processBlocking(final HttpGateway instance, final String filterName, final HttpServletRequest request, final HttpServletResponse response, final  Map<String, String> params)
    {
        return processBlocking(instance, filterName, request, response, params, true);
    }    
    
    private String processBlocking(final HttpGateway instance, final String filterName, final HttpServletRequest request, final HttpServletResponse response, final  Map<String, String> params, boolean isJson) {
        
        String result = "Response is: ";
        
        FilterCallable callable = new FilterCallable(instance, filterName, params, isJson);
        FutureTask<String> futureTask = new FutureTask<String>(callable);
        
        try {
            executor.execute(futureTask);
            
            while (!futureTask.isDone()) {
                try {
                    Log.log.debug("HTTPGateway: Waiting for FutureTask to complete");
                    result = futureTask.get(waitTime, TimeUnit.SECONDS);
                } catch (InterruptedException | ExecutionException e) {
                    Log.log.error(e.toString());
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "Failed to process.");
                    result = message.toString();
                    break;
                } catch(TimeoutException e) {
                    futureTask.cancel(true);
                    Log.log.error(e.toString());
                    JSONObject message = new JSONObject();
                    message.accumulate("Message", "Task is timed out.");
                    result = message.toString();
                    break;
                }
            }
        } catch(Exception e) {
            Log.log.error("HTTPGateway: " + e.toString());
            JSONObject message = new JSONObject();
            message.accumulate("Message", "Failed to execute task.");
            result = message.toString();
        }
        
        return result;
    } // processBlocking()

    static class RequestProcessor implements Runnable
    {
        final HttpGateway instance;
        final String filterName;
        final HttpServletRequest request;
        final HttpServletResponse response;
        final  Map<String, String> params;
        
        public RequestProcessor(final HttpGateway instance, final String filterName, final HttpServletRequest request, final HttpServletResponse response, final  Map<String, String> params)
        {
            this.instance = instance;
            this.filterName = filterName;
            this.request = request;
            this.response = response;
            this.params = params;
        }

        @Override
        public void run()
        {
            try
            {
                // process the data through a filter.
                boolean isProcessed = instance.processData(filterName, params);

                if (isProcessed)
                {
                    response.setStatus(HttpServletResponse.SC_OK);
//                    response.setContentType("text/plain");
//                    response.getWriter().println("processed");
                }
                else
                {
                    // The requested resource is no longer available at the server
                    // this happens when a filter is no longer active, using http status code is 410
                    response.setStatus(HttpServletResponse.SC_GONE);
                    Log.log.info("HTTPGateway: Gateway filter " + filterName + " not available.");
                }
            }
            catch (Exception e)
            {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                Log.log.error("HTTPGateway: " + e.getMessage());
            }
        }
        
    } // end of class RequestProcessor
    
    static class FilterCallable implements Callable<String> {
        
        private HttpGateway instance;
        private String filterName;
        private Map<String, String> params;
        private boolean isJSON;

        
        public FilterCallable(final HttpGateway instance, final String filterName, final Map<String, String> params, boolean isJson) {
            this.instance = instance;
            this.filterName = filterName;
            this.params = params;
            this.isJSON = isJson;
            params.put(Constants.HTTP_GATEWAY_RETURN_JSON, Boolean.toString(isJSON));
        }
        
        @Override
        public String call() throws Exception {
            
            return instance.processBlockingData(filterName, params);
        }
        
    } // end of class FilterCallable
    
} // end of class FilterServlet
