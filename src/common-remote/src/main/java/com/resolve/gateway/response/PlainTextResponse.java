package com.resolve.gateway.response;

public class PlainTextResponse{

	private final String content;
	
	public PlainTextResponse(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return this.content;
	}
	
}
