/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway;

import java.util.List;
import java.util.Map;

import com.resolve.gateway.netcool.NetcoolFilter;
import com.resolve.gateway.netcool.NetcoolGateway;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MNetcool extends MGateway
{
    private static final String RSCONTROL_SET_FILTERS = "MNetcool.setFilters";

    private static NetcoolGateway instance = NetcoolGateway.getInstance();

    public MNetcool()
    {
        super.instance = instance;
    }

    @Override
    protected String getSetFilterMethodName()
    {
        return RSCONTROL_SET_FILTERS;
    }

    @Override
    protected void populateGatewaySpecificFilterProperties(Map<String, String> filterMap, Filter filter)
    {
        NetcoolFilter netcoolFilter = (NetcoolFilter) filter;
        filterMap.put(NetcoolFilter.SQL, netcoolFilter.getSql());
    }

    /**
     * Adds a journal entry to the current Netcool instance
     * 
     * @param params a {@link Map} of Netcool journal parameters. Netcool journal parameter keys include:
     *  - REFERENCE
     *  - DETAIL
     *  - ACTIONNAME
     *  - CONDITION
     *  - SEVERITY
     *  - DURATION
     *  - SERIAL
     */
    public static void addJournal(Map params)
    {
        instance.addJournal(params);
    } // addJournal

    /**
     * Executes a SELECT SQL query through the current Netcool instance, and returns a {@link List} of {@link Map} results
     * 
     * @param sql the SQL query
     * @return a {@link List} of result {@link Map}s
     */
    public static List select(String sql)
    {
        List result = null;
        try
        {
            result = instance.select(sql);
        }
        catch (Throwable e)
        {
            Log.log.error("Failed to perform SQL select: " + e.getMessage());
        }

        return result;
    } // select

    /**
     * Enqueues an UPDATE SQL query through the current Netcool instance
     * 
     * @param sql the SQL query
     */
    public static void update(String sql)
    {
        if (instance.isActive() || (instance.isWorker() && !instance.isPrimary()))
        {
            if (!StringUtils.isEmpty(sql))
            {
                try
                {
                  //keep trying until the addToSendQueue succeeeded.
                    while (true)
                    {
                        if (instance.enqueue(sql))
                        {
                            break;
                        }
                        else
                        {
                            //wait for 2 of seconds.
                            Thread.sleep(2000L);
                        }
                    }
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage(), e);
                }
            }
        }
    } // update

    /**
     * Enqueues an UPDATE SQL query through the current Netcool instance, to be executed asynchronously
     * 
     * @param sql the SQL query
     */
    public static void asyncUpdate(String sql)
    {
        update(sql);
    } // asyncYpdate

    /**
     * Executes a syncronous UPDATE SQL query through the current Netcool instance
     * 
     * @param sql the SQL query
     * @param the result of the update query
     */
    public static String syncUpdate(String sql)
    {
        String result = "error";
        if (instance.isActive() || (instance.isWorker() && !instance.isPrimary()))
        {
            if (!StringUtils.isEmpty(sql))
            {
                try
                {
                    result = "" + instance.update(sql);
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage(), e);
                    result = e.getMessage();
                }
            }
        }
        return result;
    } // syncUpdate
} // MNetcool
