package com.resolve.gateway.resolvegateway;

import java.util.HashMap;
import java.util.Map;

public class GatewayProperties
{
    protected Boolean active;
    protected String queue;
    protected String type;
    protected String orgName;
    protected String gatewayName;
    protected String displayName;
    protected String packageName;
    protected String className;
    protected String licenseCode;
    protected String eventType;

    protected Boolean primary;
    protected Boolean secondary;
    protected Boolean worker;
    protected Boolean mock;
    protected Boolean uppercase;
    protected Integer interval = 120;
    protected Integer failover = 120;
    protected Integer heartbeat = 120;

    protected Boolean ssl;
    protected String sslType;
    protected String host;
    protected Integer port;
    protected String user;
    protected String pass;
    protected String sec;
    
    protected Integer serverCheckInterval = 0;
    protected Integer serverCheckRetry = 0;

    protected Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Boolean getActive()
    {
        return active;
    }

    public void setActive(Boolean active)
    {
        this.active = active;
    }

    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }
    
    public String getOrgName()
    {
        return orgName;
    }

    public void setOrgName(String orgName)
    {
        this.orgName = orgName;
    }

    public String getGatewayName()
    {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName)
    {
        this.gatewayName = gatewayName;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public String getPackageName()
    {
        return packageName;
    }

    public void setPackageName(String packageName)
    {
        this.packageName = packageName;
    }

    public String getClassName()
    {
        return className;
    }

    public void setClassName(String className)
    {
        this.className = className;
    }
    
    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getLicenseCode()
    {
        return licenseCode;
    }

    public void setLicenseCode(String licenseCode)
    {
        this.licenseCode = licenseCode;
    }

    public String getEventType()
    {
        return eventType;
    }

    public void setEventType(String eventType)
    {
        this.eventType = eventType;
    }

    public Boolean getPrimary()
    {
        return primary;
    }

    public void setPrimary(Boolean primary)
    {
        this.primary = primary;
    }

    public Boolean getSecondary()
    {
        return secondary;
    }

    public void setSecondary(Boolean secondary)
    {
        this.secondary = secondary;
    }

    public Boolean getWorker()
    {
        return worker;
    }

    public void setWorker(Boolean worker)
    {
        this.worker = worker;
    }

    public Boolean getMock()
    {
        return mock;
    }

    public void setMock(Boolean mock)
    {
        this.mock = mock;
    }

    public Boolean getUppercase()
    {
        return uppercase;
    }

    public void setUppercase(Boolean uppercase)
    {
        this.uppercase = uppercase;
    }

    public Integer getInterval()
    {
        return interval;
    }

    public void setInterval(Integer interval)
    {
        this.interval = interval;
    }

    public Integer getFailover()
    {
        return failover;
    }

    public void setFailover(Integer failover)
    {
        this.failover = failover;
    }

    public Integer getHeartbeat()
    {
        return heartbeat;
    }

    public void setHeartbeat(Integer heartbeat)
    {
        this.heartbeat = heartbeat;
    }

    public Boolean isSsl()
    {
        return ssl;
    }

    public void setSsl(Boolean ssl)
    {
        this.ssl = ssl;
    }

    public String getSslType()
    {
        return sslType;
    }

    public void setSslType(String sslType)
    {
        this.sslType = sslType;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public Integer getPort()
    {
        return port;
    }

    public void setPort(Integer port)
    {
        this.port = port;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getPass()
    {
        return pass;
    }

    public void setPass(String pass)
    {
        this.pass = pass;
    }
    public String getSec()
    {
        return sec;
    }

    public void setSec(String sec)
    {
        this.sec = sec;
    }
    public Integer getServerCheckInterval()
    {
        return serverCheckInterval;
    }

    public void setServerCheckInterval(Integer serverCheckInterval)
    {
        this.serverCheckInterval = serverCheckInterval;
    }

    public Integer getServerCheckRetry()
    {
        return serverCheckRetry;
    }

    public void setServerCheckRetry(Integer serverCheckRetry)
    {
        this.serverCheckRetry = serverCheckRetry;
    }

    public Map<String, Object> getAdditionalProperties()
    {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties)
    {
        this.additionalProperties = additionalProperties;
    }

    public String getServerUrl() {

        StringBuffer sb = new StringBuffer();
        sb.append("https://").append(host);
        if(port != null)
            sb.append(":").append(port);
        
        return sb.toString();
    }

} // class GatewayProperties
