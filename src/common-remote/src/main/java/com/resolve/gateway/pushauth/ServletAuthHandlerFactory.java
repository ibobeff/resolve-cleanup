package com.resolve.gateway.pushauth;
 
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.util.component.ContainerLifeCycle;

import com.resolve.util.ConfigMap;

interface ServletAuthHandlerFactory
{
    /**
     * Creates a handler using the specified gateway config 
     * @param map The config
     * @param server  An instance of the ContainerLifeCycle for lifecycle bean registrations
     * @return  A filter that will authenticate based on the specified config
     */
    ConstraintSecurityHandler create(ConfigMap map, ContainerLifeCycle server, String context) throws InvalidAuthConfigurationException;
    
    
    
    /**
     * 
     * Register individual filter servlet path and associated configuration for filter-specific handling
     * @throws InvalidAuthConfigurationException If the configuration is invalid
     * @param map  The config map 
     * @param csh  The handler that was created earlier by calling create() 
     * @param filter  The filter instance
     */
    void registerFilter(ConfigMap map, ConstraintSecurityHandler csh, Object filter) throws InvalidAuthConfigurationException;


    
    /**
     * 
     * Unregister individual filter servlet path and associated configuration for filter-specific handling.  This is for cleanup
     * @param map  The config map 
     * @param csh  The handler that was created earlier by calling create() 
     * @param filter  The filter instance
     */
    void unregisterFilter(ConfigMap map, ConstraintSecurityHandler csh, Object filter) ;


 
    
}
