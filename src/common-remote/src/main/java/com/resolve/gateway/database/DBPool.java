/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.database;

import com.resolve.util.CryptUtils;

public class DBPool
{
    // Field names for this pool used by DBGateway related classes.
    public static final String NAME = "NAME";
    public static final String DRIVERCLASS = "DRIVERCLASS";
    public static final String CONNECTURI = "CONNECTURI";
    public static final String MAXCONNECTION = "MAXCONNECTION";
    public static final String USERNAME = "USERNAME";
    public static final String P_ASSWORD = "PASSWORD";
    public static final String QUEUE = "QUEUE";

    private String name;
    private String driverClass;
    private String connectURI;
    private int maxConnection = 1;
    private String dbUserName = "resolve";
    private String dbP_assword = "resolve";

    public DBPool(String name, String driverClass, String connectURI, String maxConnection, String dbUserName, String dbPassword)
    {
        super();
        this.name = name;
        this.driverClass = driverClass;
        this.connectURI = connectURI;
        this.maxConnection = Integer.parseInt(maxConnection);
        this.dbUserName = dbUserName;
        try
        {
            this.dbP_assword = CryptUtils.decrypt(dbPassword);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void setPool(String Name, String driverClass, String connectURI, String maxConnection, String dbUserName, String dbPassword)
    {
        this.name = Name;
        this.driverClass = driverClass;
        this.connectURI = connectURI;
        this.maxConnection = Integer.valueOf(maxConnection);
        this.dbUserName = dbUserName;
        try
        {
            this.dbP_assword = CryptUtils.decrypt(dbPassword);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDriverClass()
    {
        return driverClass;
    }

    public void setDriverClass(String driverClass)
    {
        this.driverClass = driverClass;
    }

    public String getConnectURI()
    {
        return connectURI;
    }

    public void setConnectURI(String connectURI)
    {
        this.connectURI = connectURI;
    }

    public int getMaxConnection()
    {
        return maxConnection;
    }

    public void setMaxConnection(int maxConnection)
    {
        this.maxConnection = maxConnection;
    }

    public String getDbUserName()
    {
        return dbUserName;
    }

    public void setDbUserName(String dbUserName)
    {
        this.dbUserName = dbUserName;
    }

    public String getDbP_assword()
    {
        return dbP_assword;
    }

    public void setDbP_assword(String dbP_assword)
    {
        this.dbP_assword = dbP_assword;
    }
}
