/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.gateway.remedy;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;

import com.remedy.arsys.api.ARException;
import com.remedy.arsys.api.ARServerUser;
import com.remedy.arsys.api.AccessNameID;
import com.remedy.arsys.api.Constants;
import com.remedy.arsys.api.Entry;
import com.remedy.arsys.api.EntryCriteria;
import com.remedy.arsys.api.EntryFactory;
import com.remedy.arsys.api.EntryID;
import com.remedy.arsys.api.EntryItem;
import com.remedy.arsys.api.EntryKey;
import com.remedy.arsys.api.EntryListCriteria;
import com.remedy.arsys.api.EntryListFieldInfo;
import com.remedy.arsys.api.EntryListInfo;
import com.remedy.arsys.api.Field;
import com.remedy.arsys.api.FieldCriteria;
import com.remedy.arsys.api.FieldFactory;
import com.remedy.arsys.api.FieldID;
import com.remedy.arsys.api.FieldKey;
import com.remedy.arsys.api.FieldListCriteria;
import com.remedy.arsys.api.FieldType;
import com.remedy.arsys.api.InternalID;
import com.remedy.arsys.api.JoinEntryID;
import com.remedy.arsys.api.NameID;
import com.remedy.arsys.api.QualifierInfo;
import com.remedy.arsys.api.SortInfo;
import com.remedy.arsys.api.StatusInfo;
import com.remedy.arsys.api.Timestamp;
import com.remedy.arsys.api.Util;
import com.remedy.arsys.api.Value;
import com.remedy.arsys.api.VerifyUserCriteria;
import com.resolve.esb.MMsgHeader;
import com.resolve.rsremote.ConfigReceiveRemedy;
import com.resolve.util.ERR;
import com.resolve.util.GatewayEvent;
import com.resolve.util.Log;
import com.resolve.util.MessageDispatcher;
import com.resolve.util.StringUtils;

public class RemedyGateway
{
    static boolean active;
    private static String host = "localhost";
    private static int port = -1;
    private static String username = "remedy";
    private static String password;
    private static int maxActive = 1;
    private static long pollInterval = 0;

    public static List<RemedyForm> pollForms = new ArrayList<RemedyForm>();

    private static volatile boolean running;
    private static volatile boolean stopped = true;
    // private static String wiki;

    final static MMsgHeader headerHeader = new MMsgHeader();

    private static GenericObjectPool sessions;
    public static long GLOBAL_SEND_TIMEOUT = 1000; // milliseconds

    public static void init(ConfigReceiveRemedy config)
    {
        try
        {
            // init settings
            host = config.getHost();
            port = config.getPort();
            username = config.getUsername();
            password = config.getPassword();
            active = config.isActive();
            maxActive = config.getMaxConnection();
            pollInterval = config.getPollInterval() * 1000; // in seconds
            running = config.isPoll();

            // init message header
            headerHeader.setEventType(com.resolve.util.Constants.GATEWAY_EVENT_TYPE_REMEDY);
            headerHeader.setRouteDest(com.resolve.util.Constants.ESB_NAME_EXECUTEQUEUE, "MEvent.eventResult");

            init(host, username, password, maxActive, active, true, port);
        }
        catch (Exception e)
        {
            Log.alert(ERR.E50002);
        }
    } // init

    // Customers most likely will call this method, instead of other init()
    // methods from groovy script, if rsremote is not initialized with gateway
    // active.
    public static void init(String h, String u, String p, int m, int pt) throws Exception
    {
        init(h, u, p, m, active, pt);
    }

    // Customers most likely will call this method, instead of other init()
    // methods.
    public static void init(String h, String u, String p, int m) throws Exception
    {
        init(h, u, p, m, active, -1);
    }

    public static void init(String h, String u, String p, int m, boolean a, int pt) throws Exception
    {
        init(h, u, p, m, a, false, pt);
    }

    public static void init(String h, String u, String p, int m, boolean a, boolean forceNewPool, int pt) throws Exception
    {
        active = a;

        // Gateway needs to be active, and then check if we're forced to create
        // a new pool by "forceNewPool".
        // If not, we check if there is any change to "hostname", "username" or
        // "password" to decide if we need
        // to create a new pool because of change of Remedy connection settings.
        if (active && ((sessions == null) || forceNewPool || !(host.equals(h) && (port == pt) && username.equals(u) && password.equals(p) && (maxActive == m))))
        {
            if (sessions != null)
            {
                try
                {
                    sessions.close();
                    sessions = null;
                }
                catch (Throwable t)
                // Ignore it
                {
                }
            }

            host = h;
            port = pt;
            username = u;
            password = p;
            maxActive = m;
            Log.log.info("RemedyGateway: Initializing new RemedyGateway pool: HOST=" + host + ", USERNAME=" + username + ", MAXCONNECTION=" + maxActive);
            sessions = new GenericObjectPool(new SessionFactory(), maxActive);
        }
        else
        {
            Log.log.info("RemedyGateway: No change to Remedy connection settings. Reuse old RemedyGatway pool.");
        }

        if (running)
        {
            startPolling();
        }
    }

    // Customers are not supposed to call this method. It is for debug purpose
    // only.
    public static GenericObjectPool getSessionPool()
    {
        return sessions;
    }

    public static void reinit()
    {
        stopPolling();
        while (running)
        {

        }

        try
        {
            if (sessions != null)
            {
                try
                {
                    sessions.close();
                    sessions = null;
                }
                catch (Throwable t) {                	
                	Log.log.error("RemedyGateway: Session can not be closed.");                
                }
            }
            Log.log.info("RemedyGateway: Reinitializing HOST=" + host + ", USERNAME=" + username + ", MAXCONNECTION=" + maxActive);
            sessions = new GenericObjectPool(new SessionFactory(), maxActive);
        }
        catch (Exception e)
        {
            Log.alert(ERR.E50002);
        }
    } // reinit

    // Get session from pool. Must be paired with returnSession() call.
    public static ARServerUser getSession() throws Exception
    {
        if (!active)
        {
            throw new IllegalStateException("RemedyGateway: is not active.");
        }
        ARServerUser result = (ARServerUser) sessions.borrowObject();
        Log.log.debug("RemedyGateway: Retrived a Remedy session from pool. Active session number is " + sessions.getNumActive());
        return result;
    }

    public static void returnSession(ARServerUser s) throws Exception
    {
        Log.log.debug("RemedyGateway: Returned a Remedy session to pool. Active session number is " + sessions.getNumActive());
        sessions.returnObject(s);
    }

    public static boolean isActive()
    {
        return active;
    }

    public static void setActive(boolean b)
    {
        active = b;
    }

    private static ARServerUser connect() throws Exception
    {
        ARServerUser session = null;
        if (host != null && username != null && password != null)
        {
            Log.log.info("RemedyGateway: Connecting to AR Server at: " + host + " with username " + username);

            session = new ARServerUser();
            session.setServer(host);
            if (port > 0)
            {
                session.setPort(port);
                Util.ARSetServerPort(session, new NameID(host), port, 0);
            }
            session.setUser(new AccessNameID(username));
            session.setPassword(new AccessNameID(password));

            try
            {
                session.verifyUser(new VerifyUserCriteria());
            }
            catch (ARException e)
            {
                Log.log.error("RemedyGateway: Error verifying user: " + e);
                session.clear();
                session = null;
                throw e;
            }

            Log.log.info("RemedyGateway: Connected to AR Server.");
        }
        return session;
    } // connect

    // Shutdown session pool
    public static void close() throws Exception
    {
        // Clear memory used by our user context object
        Log.log.debug("RemedyGateway: Try to close RemedyGateway");
        if (sessions != null)
        {
            sessions.close();
            sessions = null;
        }
        Log.log.info("RemedyGateway was closed");
    } // close

    public synchronized static void startPolling()
    {
    	Log.log.info("***************************************************");
    	Log.log.info("RemedyGateway: Starting RemedyGateway");
    	Log.log.info("***************************************************");
    	
        // poller is in the process of shutting down, we need to wait it to die
        if (!running && !stopped)
        {
            while (!stopped)
            {
                try
                {
                    Log.log.debug("Waiting for remedy poller to stop");
                    Thread.sleep(1000);
                }
                catch (Exception ex)
                {
                	Log.log.error("RemedyGateway: " + ex.getMessage());   
                }
            }
        }

        Log.log.info("RemedyGateway: RemedyGateway started");
        running = true;

        // Poller is not started or poller is dead because of exception, try to
        // start or restart it
        if (stopped)
        {
            Runnable task = new Runnable()
            {
                public void run()
                {
                    try
                    {
                        if (running)
                        {
                            updateLastValue(pollForms);
                        }
                        while (running)
                        {
                            try
                            {
                                poll(pollForms);
                            }
                            catch (Throwable th)
                            {
                                Log.log.error("RemedyGateway: Error when polling remedy server", th);
                                try
                                {
                                    // re-try after 20 seconds.
                                    Thread.sleep(20000);
                                }
                                catch (Exception exp)
                                {
                                	Log.log.error("RemedyGateway: " + exp.getMessage());   
                                }
                            }
                        }
                    }
                    catch (Throwable ex)
                    {
                        Log.log.error("RemedyGateway: Error when polling Remedy server, stop polling...", ex);
                    }
                    finally
                    {
                        Log.log.info("RemedyGateway: poller is stopped");
                        stopped = true;
                    }
                }
            };
            new Thread(task).start();
            stopped = false;
        }
    } // startPolling

    public synchronized static void stopPolling()
    {
        Log.log.info("RemedyGateway: poller is asked to stop");
        running = false;
    }

    private static void updateLastValue(List<RemedyForm> forms) throws Exception
    {
        nextform: for (RemedyForm form : forms)
        {
            boolean found = false;
            for (RemedyFilter filter : form.filters)
            {
                if ("LASTVALUE".equals(filter.lastValue))
                {
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                continue nextform;
            }

            String formName = form.name;
            String where = StringUtils.isEmpty(form.restriction) ? "1=1" : form.restriction;
            SortInfo[] sortInfs = new SortInfo[form.filters.size()];
            int i = 0;
            for (RemedyFilter dynmFilter : form.filters)
            {
                sortInfs[i++] = new SortInfo(new InternalID(Long.parseLong(dynmFilter.fieldID)), dynmFilter.sort);
            }

            String fieldList = StringUtils.isEmpty(form.fieldList) ? null : form.fieldList;

            List<String> ticketIDs = find(formName, where, sortInfs);
            Log.log.info("RemedyGateway: updateLastValue fine reutrned " + (ticketIDs == null ? null : ticketIDs.size()));
            if (ticketIDs != null && !ticketIDs.isEmpty())
            {
                int last = ticketIDs.size() - 1;
                String id = ticketIDs.get(last);
                Map ticket = retrieve(formName, id, fieldList);

                for (RemedyFilter filter : form.filters)
                {
                    if (ticket.get(filter.fieldName) != null && !StringUtils.isEmpty(ticket.get(filter.fieldName).toString()))
                    {
                        filter.lastValue = "" + ticket.get(filter.fieldName);
                        Log.log.trace("RemedyGateway: Update last value of field " + filter.fieldName + ". New value is " + ticket.get(filter.fieldName));
                    }
                }
            }
        }
    }

    private static void poll(List<RemedyForm> forms) throws Exception
    {
        boolean sleep = true;
        for (RemedyForm form : forms)
        {
            String formName = form.name;
            String staticFilter = StringUtils.isEmpty(form.restriction) ? "" : form.restriction + " AND ";

            // Build query where clause
            String where = staticFilter;

            SortInfo[] sortInfs = new SortInfo[form.filters.size()];
            int i = 0;
            for (RemedyFilter dynmFilter : form.filters)
            {

                sortInfs[i++] = new SortInfo(new InternalID(Long.parseLong(dynmFilter.fieldID)), dynmFilter.sort);

                // check if value needs to be quoted
                if (dynmFilter.quote)
                {
                    where += "'" + dynmFilter.fieldName + "' " + dynmFilter.operator + " \"" + dynmFilter.lastValue + "\"";

                }
                else
                {
                    where += "'" + dynmFilter.fieldName + "' " + dynmFilter.operator + " " + dynmFilter.lastValue;
                }

                if (i < form.filters.size())
                {
                    where += " AND ";

                }
            }

            Log.log.info("RemedyGateway: Remedy poller where clause for Form " + formName + ": " + where);

            String fieldList = StringUtils.isEmpty(form.fieldList) ? null : form.fieldList;

            List<String> ticketIDs = find(formName, where, sortInfs);
            if (ticketIDs != null && !ticketIDs.isEmpty())
            {
                // If found entries from Remedy, poller won't go to sleep at the
                // end of method
                sleep = false;
                for (String id : ticketIDs)
                {
                    if (!running)
                    {
                        return;
                    }
                    Map ticket = retrieve(formName, id, fieldList);

                    for (RemedyFilter filter : form.filters)
                    {
                        if (ticket.get(filter.fieldName) != null && !StringUtils.isEmpty(ticket.get(filter.fieldName).toString()))
                        {
                            filter.lastValue = "" + ticket.get(filter.fieldName);
                            Log.log.info("RemedyGateway: Lastvalue of field " + filter.fieldName + " is updated to " + ticket.get(filter.fieldName));
                        }
                    }

                    if (!StringUtils.isEmpty(form.runbook))
                    {
                        ticket.put("WIKI", form.runbook);
                    }
                    Log.log.info("RemedyGateway: Remedy poller sending new message for ticket: " + ticket);

                    GatewayEvent<String, String> g = new GatewayEvent<String, String>(System.currentTimeMillis(), ticket);
                    boolean b = false;
                    while (!b)
                    {
                        b = MessageDispatcher.sendMessage(headerHeader, g, GLOBAL_SEND_TIMEOUT);
                        if (!b)
                        {
                            // Sleep for 1 second and re-try
                            Thread.currentThread().sleep(1000);
                        }
                    }
                    // MainBase.esb.sendMessage(headerHeader, ticket);
                }
            }
        }

        // If nothing matched from Remedy, go to sleep
        if (sleep)
        {
            Thread.sleep(pollInterval);
        }
    } // poll

    public static String create(String form, String fieldValueStr) throws Exception
    {
        Map fieldValues = StringUtils.stringToMap(fieldValueStr, "=", "&");
        return create(form, fieldValues);
    } // create

    public static String create(String form, Map fieldValues) throws Exception
    {
        String result = "";

        ARServerUser s = getSession();
        try
        {
            Entry entry = (Entry) EntryFactory.getFactory().newInstance();
            entry.setContext(s);
            entry.setSchemaID(new NameID(form));

            // set values
            setEntryItemValues(entry, fieldValues);

            // create record
            entry.create();
            result = entry.getEntryID().toString();

            // clean up
            EntryFactory.getFactory().releaseInstance(entry);

            Log.log.debug("RemedyGateway: Created entry id: " + result);
        }
        catch (ARException e)
        {
            ARExceptionHandler(e, "RemedyGateway: Problem while creating entry for session: " + s);
        }
        finally
        {
            returnSession(s);
        }

        return result;
    } // create

    private static void setEntryItemValues(Entry entry, Map fieldValues)
    {
        ArrayList entries = new ArrayList();
        for (Iterator i = fieldValues.entrySet().iterator(); i.hasNext();)
        {
            Map.Entry rec = (Map.Entry) i.next();

            String key = (String) rec.getKey();
            String value = (String) rec.getValue();

            // only process field ids
            if (StringUtils.isNumeric(key))
            {
                entries.add(new EntryItem(new FieldID(Long.parseLong(key)), new Value(value)));
            }
        }

        // allocate EntryItems array
        EntryItem[] entryItems = new EntryItem[entries.size()];
        entries.toArray(entryItems);
        entry.setEntryItems(entryItems);
    } // setEntryItemValues

    public static String update(String form, String entryId, String fieldValueStr) throws Exception
    {
        Map fieldValues = StringUtils.stringToMap(fieldValueStr, "=", "&");
        return update(form, entryId, fieldValues);
    } // update

    public static String update(String form, String entryId, Map fieldValues) throws Exception
    {
        String result = null;
        ARServerUser session = getSession();
        try
        {
            // find record
            EntryKey entryKey = createEntryKey(form, entryId);
            Entry entry = EntryFactory.findByKey(session, entryKey, null);

            // set values
            setEntryItemValues(entry, fieldValues);

            // update record
            entry.store();

            // clean up
            EntryFactory.getFactory().releaseInstance(entry);
            result = entryId;

            Log.log.debug("RemedyGateway: Record: " + entryId + " successfully updated");
        }
        catch (ARException e)
        {
            ARExceptionHandler(e, "RemedyGateway: Problem while modifying record: ");
        }
        finally
        {
            returnSession(session);
        }

        return result;
    } // update

    public static List find2(String form, String criteria) throws Exception
    {
        List result = new ArrayList();

        Log.log.debug("RemedyGateway: Finding records records with criteria: " + criteria);
        ARServerUser session = getSession();

        try
        {
            QualifierInfo qualifier = prepareQualifierInfo(session, form, criteria);
            if (qualifier != null)
            {
                // Set the EntryListCriteria
                EntryListCriteria listCriteria = new EntryListCriteria();
                listCriteria.setSchemaID(new NameID(form));
                listCriteria.setQualifier(qualifier);

                // Make the call to retreive the query results list
                Integer nMatches = new Integer(0);
                Entry[] entryArr = EntryFactory.findObjects(session, listCriteria, null, false, nMatches);
                if (Log.log.isTraceEnabled())
                {
                    Log.log.info("RemedyGateway: Query returned " + nMatches + " matches.");
                }

                if (nMatches.intValue() > 0)
                {
                    // Print out the matches
                    Log.log.trace("EntryID:Description");

                    for (int i = 0; i < entryArr.length; i++)
                    {
                        result.add(entryArr[i].getEntryID().toString());
                        Log.log.trace("EntryID: " + entryArr[i].getEntryID());
                        Log.log.trace("Number of fields : " + entryArr[i].getEntryItems().length);
                        for (EntryItem item : entryArr[i].getEntryItems())
                        {
                            Log.log.trace("FieldID: " + item.getFieldID() + " -- FieldValue: " + item.getValue().getValue());
                        }
                    }
                }
                EntryFactory.getFactory().releaseInstance(entryArr);
            }
        }
        catch (ARException e)
        {
            ARExceptionHandler(e, "RemedyGateway: Problem while querying by qualifier: ");
            throw e;
        }
        finally
        {
            returnSession(session);
        }

        return result;
    } // find2

    public static List<String> find(String form, String criteria) throws Exception
    {
        return find(form, criteria, null);
    }

    public static List<String> find(String form, String criteria, SortInfo[] sortInfos) throws Exception
    {
        List<String> result = new ArrayList();

        Log.log.debug("RemedyGateway: Finding records records with criteria: " + criteria);
        ARServerUser session = getSession();

        try
        {
            QualifierInfo qualifier = prepareQualifierInfo(session, form, criteria);
            if (qualifier != null)
            {
                // Set the EntryListCriteria
                EntryListCriteria listCriteria = new EntryListCriteria();
                listCriteria.setSchemaID(new NameID(form));
                listCriteria.setQualifier(qualifier);
                if (sortInfos != null && sortInfos.length > 0)
                {
                    listCriteria.setSortInfos(sortInfos);
                }

                // Make the call to retreive the query results list
                Integer nMatches = new Integer(0);
                EntryListInfo[] entryInfo = EntryFactory.findEntryListInfos(session, listCriteria, null, false, nMatches);
                if (Log.log.isTraceEnabled())
                {
                    Log.log.info("RemedyGateway: Query returned " + nMatches + " matches.");
                }

                if (nMatches.intValue() > 0)
                {
                    // Print out the matches
                    Log.log.trace("EntryID:Description");

                    for (int i = 0; i < entryInfo.length; i++)
                    {
                        result.add(entryInfo[i].getEntryID().toString());
                        Log.log.trace(entryInfo[i].getEntryID().toString() + ":" + new String(entryInfo[i].getDescription()));
                    }
                }
                EntryFactory.getFactory().releaseInstance(entryInfo);
            }
        }
        catch (ARException e)
        {
            ARExceptionHandler(e, "RemedyGateway: Problem while querying by qualifier: ");
            throw e;
        }
        finally
        {
            returnSession(session);
        }

        return result;
    } // find

    private static QualifierInfo prepareQualifierInfo(ARServerUser session, String form, String criteria) throws ARException
    {
        QualifierInfo result = null;

        // Set the field criteria to retrieve all field info
        FieldCriteria fCrit = new FieldCriteria();
        fCrit.setRetrieveAll(true);

        // Retrieve all types of fields
        FieldListCriteria fListCrit = new FieldListCriteria(new NameID(form), new Timestamp(0), FieldType.AR_ALL_FIELD);

        // Load the field array with all fields in the test form
        Field[] formFields = FieldFactory.findObjects(session, fListCrit, fCrit);

        // Create the search qualifier.
        result = Util.ARGetQualifier(session, criteria, formFields, null, com.remedy.arsys.api.Constants.AR_QUALCONTEXT_DEFAULT);

        // cleanup
        FieldFactory.getFactory().releaseInstance(formFields);

        return result;
    } // prepareQualifierInfo

    public static Map retrieve(String form, String entryId) throws Exception
    {
        return retrieve(form, entryId, null);
    } // retrieve

    public static Map retrieve(String form, String entryId, String fieldList) throws Exception
    {
        ARServerUser session = getSession();
        try
        {
            return retrieveInternal(session, form, entryId, fieldList);
        }
        finally
        {
            returnSession(session);
        }
    }

    private static Map retrieveInternal(ARServerUser session, String form, String entryId, String fieldList) throws Exception
    {
        Map result = new Hashtable();
        Log.log.debug("RemedyGateway: Retrieving record with entry ID:" + entryId);

        try
        {
            // get list of fields to retrieve
            EntryCriteria entryCriteria = null;
            if (fieldList != null)
            {
                boolean hasFields = false;

                String[] fieldIds = fieldList.split("&");

                EntryListFieldInfo[] entryListFieldList = new EntryListFieldInfo[fieldIds.length];
                for (int i = 0; i < fieldIds.length; i++)
                {
                    String id = fieldIds[i];

                    if (!StringUtils.isEmpty(id))
                    {
                        Long fieldId = Long.parseLong(id.trim());
                        entryListFieldList[i] = new EntryListFieldInfo(new FieldID(fieldId));

                        hasFields = true;
                    }
                }

                // Set the entry criteria
                if (hasFields)
                {
                    entryCriteria = new EntryCriteria();
                    entryCriteria.setEntryListFieldInfo(entryListFieldList);
                }
            }

            // retrieve entry with field criterias
            //at this point we're deciding if this is a JoinEntryId or just regular EntryId
            EntryKey entryKey = createEntryKey(form, entryId);
            Entry entry = EntryFactory.findByKey(session, entryKey, entryCriteria);

            EntryItem[] entryItemList = entry.getEntryItems();
            if (entryItemList == null)
            {
                Log.log.error("RemedyGateway: No data found for entry ID:" + entryId);
            }
            else
            {
                FieldCriteria fCrit = new FieldCriteria();
                fCrit.setRetrieveAll(true);

                Log.log.trace("Number of fields: " + entryItemList.length);
                for (int i = 0; i < entryItemList.length; i++)
                {
                    // Get the field's details
                    FieldID id = entryItemList[i].getFieldID();
                    Field field = FieldFactory.findByKey(session, new FieldKey(new NameID(form), id), fCrit);

                    // Output field's name, value, id, and type
                    String value = entryItemList[i].getValue().toString();
                    if (value == null)
                    {
                        value = "";
                    }
                    else if (!StringUtils.isAsciiPrintable(value))
                    {
                        value = "value not printable";
                    }
                    result.put(field.getName().toString(), value);

                    Log.log.trace(field.getName().toString());
                    Log.log.trace(": " + entryItemList[i].getValue());
                    Log.log.trace(" ID: " + id);
                    Log.log.trace(" Field type: " + field.getDataType().toInt());
                    Log.log.trace("");
                    /*
                     * // Handle if Date.. if (field.getDataType().toInt() == 7)
                     * { System.out.print(" Date value: "); Timestamp
                     * callDateTimeTS = (Timestamp)
                     * entryItemList[i].getValue().getValue(); // First getValue
                     * returns a Value object, // second returns a generic
                     * Object that // can be cast as a Timestamp if
                     * (callDateTimeTS != null)
                     * System.out.print(callDateTimeTS.toDate()); }
                     */
                    // Release memory held by our field object
                    // field.clear();
                    FieldFactory.getFactory().releaseInstance(field);
                }
            }
            EntryFactory.getFactory().releaseInstance(entry);
        }
        catch (ARException e)
        {
            ARExceptionHandler(e, "RemedyGateway: Problem while querying by entry id: ");
            throw e;
        }

        return result;
    } // retrieve

    /**
     * Based on the format of the entry id, decide if this is an EntryID or a JoinEntryID
     * 
     * @param form
     * @param entryId
     * @return
     */
    private static EntryKey createEntryKey(String form, String entryId)
    {
        EntryKey entryKey = null;
        if(StringUtils.isNotBlank(entryId))
        {
            //in Remedy a join entry is like 000000000029861|000000000030417 and a regular entry id
            //is just a 15 character long string with numbers.
            if(entryId.indexOf("|") >= 0 && entryId.length() > Constants.AR_MAX_ENTRYID_SIZE)
            {
                entryKey = new EntryKey(new NameID(form), new JoinEntryID(entryId));
            }
            else
            {
                entryKey = new EntryKey(new NameID(form), new EntryID(entryId));
            }
        }
        
        return entryKey;
    }

    public static String print(String form, String entryId) throws Exception
    {
        return print(form, entryId, null);
    } // print

    public static String print(String form, String entryId, String fieldList) throws Exception
    {
        String result = "";

        ARServerUser session = getSession();
        try
        {
            Map entry = retrieveInternal(session, form, entryId, fieldList);

            for (Iterator i = entry.entrySet().iterator(); i.hasNext();)
            {
                Map.Entry field = (Map.Entry) i.next();
                String key = (String) field.getKey();
                String value = (String) field.getValue();

                result += key + "=" + value + "\n";
            }
        }
        finally
        {
            returnSession(session);
        }

        result += "\n\n";
        return result;
    } // print

    public static String delete(String form, String entryId) throws Exception
    {
        String result = null;
        ARServerUser session = getSession();
        try
        {
            // retrieve entry with field criterias
            EntryKey entryKey = createEntryKey(form, entryId);
            Entry entry = EntryFactory.findByKey(session, entryKey, null);

            // remove
            entry.remove();
            EntryFactory.getFactory().releaseInstance(entry);

            // set to entryId removed
            result = entryId;

            Log.log.debug("Record: " + entryId + " successfully deleted");
        }
        catch (ARException e)
        {
            ARExceptionHandler(e, "RemedyGateway: Problem while querying by entry id: ");
        }
        finally
        {
            returnSession(session);
        }

        return result;
    } // delete

    public static void printStatusList(StatusInfo[] statusList)
    {
        if (statusList == null || statusList.length == 0)
        {
            Log.log.info("RemedyGateway: Status List is empty.");
            return;
        }

        Log.log.trace("Message type: ");
        switch (statusList[0].getMessageType())
        {
            case Constants.AR_RETURN_OK:
                Log.log.trace("Note");
                break;
            case Constants.AR_RETURN_WARNING:
                Log.log.trace("Warning");
                break;
            case Constants.AR_RETURN_ERROR:
                Log.log.trace("Error");
                break;
            case Constants.AR_RETURN_FATAL:
                Log.log.trace("Fatal Error");
                break;
            default:
                Log.log.trace("Unknown (" + statusList[0].getMessageType() + ")");
                break;
        }

        Log.log.trace("Status List:");
        for (int i = 0; i < statusList.length; i++)
        {
            Log.log.trace(statusList[i].getMessageText());
            Log.log.trace(statusList[i].getAppendedText());
        }
    } // printStatusList

    public static Map createFieldValues(String log)
    {
        Map result = new Hashtable();

        Pattern pattern = Pattern.compile(".*\\((\\d+)\\) = (.+)");
        String[] lines = log.split("\n");

        for (int i = 0; i < lines.length; i++)
        {
            String line = lines[i];

            Matcher matcher = pattern.matcher(line);
            if (matcher.matches())
            {
                String key = matcher.group(1);
                String value = matcher.group(2);

                if (!StringUtils.isEmpty(key))
                {
                    if (value.equals("\"\""))
                    {
                        value = "";
                    }
                    result.put(key, value);
                }
            }
        }
        Log.log.trace("fieldValues: " + result);

        return result;
    } // createFieldValues

    public static void ARExceptionHandler(ARException e, String errMessage) throws Exception
    {
        /*
         * System.out.println(errMessage); if (session != null) {
         * printStatusList(session.getLastStatus()); }
         * System.out.print("Stack Trace:"); e.printStackTrace();
         */
        Log.log.error("RemedyGateway: ARExceptionHandler: " + errMessage, e);
        throw e;
    } // ARExceptionHandler

    private static class SessionFactory extends BasePoolableObjectFactory
    {
        public Object makeObject() throws Exception
        {
            return RemedyGateway.connect();
        }

        public void destroyObject(Object obj) throws Exception
        {
            try
            {
                Log.log.debug("RemedyGateway: destroys session: " + obj);
                if (obj != null)
                {
                    ((ARServerUser) obj).clear();
                }
            }
            catch (Exception ex) {
            	Log.log.error("RemedyGateway: " + ex.getMessage());   
            }
        }
    }
}