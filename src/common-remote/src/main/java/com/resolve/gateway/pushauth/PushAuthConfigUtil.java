package com.resolve.gateway.pushauth;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;

import com.resolve.util.Log;
import com.resolve.util.StringUtils;

class PushAuthConfigUtil
{
    static String getValueFromConfig(Object map, String name, boolean trim)
    {
      //  if(map.containsKey(name))
        {
            try
            {
                String out =   BeanUtils.getSimpleProperty(map, name);
                if(out != null && trim)
                    return out.trim();
                return out;
            }
            catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e)
            {
               Log.log.warn(e.getMessage(), e);
            }
            
            
        }
        return null;
    }

    static String flattenURI(String uri)
    {   
     
         return StringUtils.replaceChars(uri, "/:*", "_");
    }
}
