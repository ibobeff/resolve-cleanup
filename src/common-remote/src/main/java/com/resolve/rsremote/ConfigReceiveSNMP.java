/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.gateway.snmp.SNMPFilter;
import com.resolve.gateway.snmp.SNMPGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for SNMP gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <SNMP....../>
 */
public class ConfigReceiveSNMP extends ConfigReceiveGateway
{
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_SNMP_NODE = "./RECEIVE/SNMP/";
    private static final String RECEIVE_SNMP_FILTER = RECEIVE_SNMP_NODE + "FILTER";
    private static final String RECEIVE_SNMP_PROPERTIES = RECEIVE_SNMP_NODE + "PROPERTIES";

    // SNMP Filter Attributes
    private static final String RECEIVE_SNMP_ATTR_IPADDRESS = RECEIVE_SNMP_NODE + "@IPADDRESS";
    private static final String RECEIVE_SNMP_ATTR_PORT = RECEIVE_SNMP_NODE + "@PORT";
    private static final String RECEIVE_SNMP_ATTR_READ_COMMUNITY = RECEIVE_SNMP_NODE + "@READCOMMUNITY";

    private static final String RECEIVE_SNMP_ATTR_SENDTRAP = RECEIVE_SNMP_NODE + "SENDTRAP/";
    private static final String RECEIVE_SNMP_ATTR_SENDTRAP_WRITE_COMMUNITY = RECEIVE_SNMP_ATTR_SENDTRAP + "@WRITECOMMUNITY";
    private static final String RECEIVE_SNMP_ATTR_SENDTRAP_RETRIES = RECEIVE_SNMP_ATTR_SENDTRAP + "@RETRIES";
    private static final String RECEIVE_SNMP_ATTR_SENDTRAP_TIMEOUT = RECEIVE_SNMP_ATTR_SENDTRAP + "@TIMEOUT";

    private boolean active = false;
    private int interval = 120;
    private String queue = "SNMP";
    //SNMP trap receiving IP, customer may want different network interface to receive
    //traps, this in that case change the value in the blueprint.
    private String ipaddress = "127.0.0.1"; 
    private int port = 162; // SNMP trap receiving port
    private String readcommunity = "public";

    // SNMP Trap sending configuration
    private String sendtrap_writecommunity = "public";
    private int sendtrap_retries = 2;
    private int sendtrap_timeout = 10; // seconds

    List<Map<String, Object>> filters;
    List<Map<String, Object>> nameProperties;

    public ConfigReceiveSNMP(XDoc config) throws Exception
    {
        super(config);

        // receives Snmp traps from devices/systems.
        define("ipaddress", STRING, RECEIVE_SNMP_ATTR_IPADDRESS); 
        define("port", INTEGER, RECEIVE_SNMP_ATTR_PORT);
        define("readcommunity", STRING, RECEIVE_SNMP_ATTR_READ_COMMUNITY);

        // SNMP trap sender configuration
        define("sendtrap_writecommunity", STRING, RECEIVE_SNMP_ATTR_SENDTRAP_WRITE_COMMUNITY);
        define("sendtrap_retries", INTEGER, RECEIVE_SNMP_ATTR_SENDTRAP_RETRIES);
        define("sendtrap_timeout", INTEGER, RECEIVE_SNMP_ATTR_SENDTRAP_TIMEOUT);

    } // ConfigReceiveSNMP

    public String getRootNode()
    {
        return RECEIVE_SNMP_NODE;
    }

    @SuppressWarnings("unchecked")
    public void load() throws Exception
    {
        try
        {
            loadAttributes();

            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                SNMPGateway snmpGateway = SNMPGateway.getInstance(this);

                // filters
                filters = xdoc.getListMapValue(RECEIVE_SNMP_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String) values.get(SNMPFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/snmp/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(SNMPFilter.SCRIPT, groovy);
                                }
                            }

                            // init filter
                            snmpGateway.setFilter(values);

                            Log.log.debug("Loaded groovy script: " + scriptFile);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for SNMP gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    public void save() throws Exception
    {
        // create snmp directory
        File dir = getFile(MainBase.main.release.serviceName + "/config/snmp");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }

        // clear list of filters to save
        if (filters != null && this.active)
        {
            filters.clear();

            // save sql files
            for (Filter filter : SNMPGateway.getInstance().getFilters().values())
            {
                SNMPFilter snmpFilter = (SNMPFilter) filter;

                String groovy = snmpFilter.getScript();
                String scriptFileName = snmpFilter.getId().toLowerCase() + ".groovy";
                File scriptFile = getFile(MainBase.main.release.serviceName + "/config/snmp/" + scriptFileName);

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(SNMPFilter.ID, snmpFilter.getId());
                entry.put(SNMPFilter.ACTIVE, String.valueOf(snmpFilter.isActive()));
                entry.put(SNMPFilter.ORDER, String.valueOf(snmpFilter.getOrder()));
                entry.put(SNMPFilter.INTERVAL, String.valueOf(snmpFilter.getInterval()));
                entry.put(SNMPFilter.EVENT_EVENTID, snmpFilter.getEventEventId());
                // entry.put(SNMPFilter.PERSISTENT_EVENT,
                // String.valueOf(snmpFilter.isPersistentEvent()));
                entry.put(SNMPFilter.RUNBOOK, snmpFilter.getRunbook());
                // entry.put(SNMPFilter.SCRIPT, filter.getScript());
                entry.put(SNMPFilter.SNMP_VERSION, String.valueOf(snmpFilter.getSnmpVersion()));
                entry.put(SNMPFilter.IP_ADDRESSES, snmpFilter.getIpAddresses());
                entry.put(SNMPFilter.READ_COMMUNITY, snmpFilter.getReadCommunity());
                entry.put(SNMPFilter.TIMEOUT, String.valueOf(snmpFilter.getTimeout()));
                entry.put(SNMPFilter.RETRIES, String.valueOf(snmpFilter.getRetries()));
                entry.put(SNMPFilter.TRAP_RECEIVER, String.valueOf(snmpFilter.isTrapReceiver()));
                entry.put(SNMPFilter.SNMP_TRAP_OID, snmpFilter.getSnmpTrapOid());
                entry.put(SNMPFilter.OID, snmpFilter.getOid());
                entry.put(SNMPFilter.REGEX, snmpFilter.getRegex());
                entry.put(SNMPFilter.COMPARATOR, snmpFilter.getComparator());
                entry.put(SNMPFilter.VALUE, String.valueOf(snmpFilter.getValue()));
                filters.add(entry);

                try
                {
                    // create groovy file
                    FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_SNMP_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && this.active)
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : SNMPGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                Map props = SNMPGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public String getIpaddress()
    {
        return ipaddress;
    }

    public void setIpaddress(String ipaddress)
    {
        this.ipaddress = ipaddress;
    }

    public String getReadcommunity()
    {
        return readcommunity;
    }

    public void setReadcommunity(String readcommunity)
    {
        this.readcommunity = readcommunity;
    }

    public String getSendtrap_writecommunity()
    {
        return sendtrap_writecommunity;
    }

    public void setSendtrap_writecommunity(String sendtrap_writecommunity)
    {
        this.sendtrap_writecommunity = sendtrap_writecommunity;
    }

    public int getSendtrap_retries()
    {
        return sendtrap_retries;
    }

    public void setSendtrap_retries(int sendtrap_retries)
    {
        this.sendtrap_retries = sendtrap_retries;
    }

    public int getSendtrap_timeout()
    {
        return sendtrap_timeout;
    }

    public void setSendtrap_timeout(int sendtrap_timeout)
    {
        this.sendtrap_timeout = sendtrap_timeout;
    }
} // ConfigReceiveSNMP
