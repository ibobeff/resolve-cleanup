/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.gateway.Filter;
import com.resolve.gateway.tibcobespoke.TIBCOBespokeFilter;
import com.resolve.gateway.tibcobespoke.TIBCOBespokeGateway;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for ConfigReceiveTIBCOBespoke gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <ConfigReceiveTIBCOBespoke....../>
 */
public class ConfigReceiveTIBCOBespoke extends ConfigReceiveGateway
{
    private static final long serialVersionUID = 926630769210622398L;
    private static final String RECEIVE_TIBCOBESPOKE_NODE = "./RECEIVE/TIBCOBESPOKE/";
    private static final String RECEIVE_TIBCOBESPOKE_FILTER = RECEIVE_TIBCOBESPOKE_NODE + "FILTER";

    // ConfigReceiveTIBCOBespoke Filter Attributes
    private static final String RECEIVE_TIBCOBESPOKE_ATTR_TOMID = RECEIVE_TIBCOBESPOKE_NODE + "@TOMID";
    private static final String RECEIVE_TIBCOBESPOKE_ATTR_HANDLERCLASSNAME = RECEIVE_TIBCOBESPOKE_NODE + "@HANDLERCLASSNAME";
    private static final String RECEIVE_TIBCOBESPOKE_ATTR_CREDENTIAL = RECEIVE_TIBCOBESPOKE_NODE + "@CREDENTIAL";
    private static final String RECEIVE_TIBCOBESPOKE_ATTR_LOGTYPES = RECEIVE_TIBCOBESPOKE_NODE + "@LOGTYPES";
    private static final String RECEIVE_TIBCOBESPOKE_ATTR_BUSJARFILES = RECEIVE_TIBCOBESPOKE_NODE + "@BUSJARFILES";
    private static final String RECIEVE_TICBOBESPOKE_ATTR_LDLIBRARYPATH = RECEIVE_TIBCOBESPOKE_NODE + "@LDLIBRARYPATH";
    private static final String RECIEVE_TICBOBESPOKE_ATTR_PROCESSMAXTHREAD = RECEIVE_TIBCOBESPOKE_NODE + "@PROCESSMAXTHREAD";
    private static final String RECIEVE_TICBOBESPOKE_ATTR_PROCESSMINTHREAD = RECEIVE_TIBCOBESPOKE_NODE + "@PROCESSMINTHREAD";
    private static final String RECIEVE_TICBOBESPOKE_ATTR_RABBITCOREPOOLSIZE = RECEIVE_TIBCOBESPOKE_NODE + "@RABBITCOREPOOLSIZE";
    private static final String RECIEVE_TICBOBESPOKE_ATTR_RABBITMAXPOOLSIZE = RECEIVE_TIBCOBESPOKE_NODE + "@RABBITMAXPOOLSIZE";
    private static final String RECIEVE_TICBOBESPOKE_ATTR_RABBITKEEPALIVETIME = RECEIVE_TIBCOBESPOKE_NODE + "@RABBITKEEPALIVETIME";
    private static final String RECIEVE_TICBOBESPOKE_ATTR_RABBITBLOCKINGQUEUESIZE = RECEIVE_TIBCOBESPOKE_NODE + "@RABBITBLOCKINGQUEUESIZE";
    private static final String RECIEVE_TICBOBESPOKE_ATTR_RABBITSSLPROTOCOL = RECEIVE_TIBCOBESPOKE_NODE + "@RABBITSSLPROTOCOL";
    

    private String tomid = "";
    private String handlerClassname = "";
    private String credential = "";
    private String logTypes = "";
    private String busJarFiles = "";
    private String ldLibraryPath = "";    
    private String processMaxThread = "";
    private String processMinThread = "";
    private String rabbitCorePoolSize = "";
    private String rabbitMaxPoolSize = "";
    private String rabbitKeepAliveTime = "";
    private String rabbitBlockingQueueSize = "";
    private String rabbitSslProtocol = "TLSv1.2";
    

    public ConfigReceiveTIBCOBespoke(XDoc config) throws Exception
    {
        super(config);
        define("tomid", STRING, RECEIVE_TIBCOBESPOKE_ATTR_TOMID);
        define("handlerClassname", STRING, RECEIVE_TIBCOBESPOKE_ATTR_HANDLERCLASSNAME);
        define("credential", STRING, RECEIVE_TIBCOBESPOKE_ATTR_CREDENTIAL);
        define("logTypes", STRING, RECEIVE_TIBCOBESPOKE_ATTR_LOGTYPES);
        define("busJarFiles", STRING, RECEIVE_TIBCOBESPOKE_ATTR_BUSJARFILES);
        define("ldLibraryPath", STRING, RECIEVE_TICBOBESPOKE_ATTR_LDLIBRARYPATH);
        define("processMaxThread", STRING, RECIEVE_TICBOBESPOKE_ATTR_PROCESSMAXTHREAD);
        define("processMinThread", STRING, RECIEVE_TICBOBESPOKE_ATTR_PROCESSMINTHREAD);
        define("rabbitCorePoolSize", STRING, RECIEVE_TICBOBESPOKE_ATTR_RABBITCOREPOOLSIZE);
        define("rabbitMaxPoolSize", STRING, RECIEVE_TICBOBESPOKE_ATTR_RABBITMAXPOOLSIZE);
        define("rabbitKeepAliveTime", STRING, RECIEVE_TICBOBESPOKE_ATTR_RABBITKEEPALIVETIME);
        define("rabbitBlockingQueueSize", STRING, RECIEVE_TICBOBESPOKE_ATTR_RABBITBLOCKINGQUEUESIZE);
        define("rabbitSslProtocol", STRING, RECIEVE_TICBOBESPOKE_ATTR_RABBITSSLPROTOCOL);
    } 

    public String getRootNode()
    {
        return RECEIVE_TIBCOBESPOKE_NODE;
    }

    @SuppressWarnings("unchecked")
    public void load() throws Exception
    {
        try
        {
            loadAttributes();

            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                TIBCOBespokeGateway tibcoGateway = TIBCOBespokeGateway.getInstance(this);

                // filters
                filters = xdoc.getListMapValue(RECEIVE_TIBCOBESPOKE_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(TIBCOBespokeFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/tibcobespoke/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(TIBCOBespokeFilter.SCRIPT, groovy);
                                }
                            }
                            // init filter
                            tibcoGateway.setFilter(values);

                            Log.log.debug("Loaded groovy script: " + scriptFile);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for TIBCO gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    public void save() throws Exception
    {
        // create tibcobespoke directory
        File dir = getFile(MainBase.main.release.serviceName + "/config/tibcobespoke");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }

        // clear list of filters to save
        if (filters != null && isActive())
        {
            filters.clear();

            // save sql files
            for (Filter filter : TIBCOBespokeGateway.getInstance().getFilters().values())
            {
                TIBCOBespokeFilter tibcoFilter = (TIBCOBespokeFilter) filter;

                String groovy = tibcoFilter.getScript();
                String scriptFileName = tibcoFilter.getId().toLowerCase() + ".groovy";
                File scriptFile = getFile(MainBase.main.release.serviceName + "/config/tibcobespoke/" + scriptFileName);

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(TIBCOBespokeFilter.ID, tibcoFilter.getId());
                entry.put(TIBCOBespokeFilter.ACTIVE, String.valueOf(tibcoFilter.isActive()));
                entry.put(TIBCOBespokeFilter.ORDER, String.valueOf(tibcoFilter.getOrder()));
                entry.put(TIBCOBespokeFilter.INTERVAL, String.valueOf(tibcoFilter.getInterval()));
                entry.put(TIBCOBespokeFilter.EVENT_EVENTID, tibcoFilter.getEventEventId());
                entry.put(TIBCOBespokeFilter.RUNBOOK, tibcoFilter.getRunbook());
                
                entry.put(TIBCOBespokeFilter.REGEX, tibcoFilter.getRegex());
                entry.put(TIBCOBespokeFilter.BUS_URI, tibcoFilter.getBusUri());
                entry.put(TIBCOBespokeFilter.TOPIC, tibcoFilter.getTopic());
                entry.put(TIBCOBespokeFilter.CERTIFIED_MESSAGING, tibcoFilter.getCertifiedMessaging());
                entry.put(TIBCOBespokeFilter.XML_QUERY, tibcoFilter.getXmlQuery());
                entry.put(TIBCOBespokeFilter.UNESCAPE_XML, tibcoFilter.getUnescapeXml());
                entry.put(TIBCOBespokeFilter.PROCESS_AS_XML, tibcoFilter.getProcessAsXml());
                entry.put(TIBCOBespokeFilter.REQUIRE_REPLY, tibcoFilter.getRequireReply());
                entry.put(TIBCOBespokeFilter.REPLY_TIMEOUT, tibcoFilter.getReplyTimeout());
                filters.add(entry);

                try
                {
                    // create groovy file
                    FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_TIBCOBESPOKE_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && isActive())
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : TIBCOBespokeGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                // overwrite properties file
                Map props = TIBCOBespokeGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public String getTomid()
    {
        return tomid;
    }
    
    public void setTomid(String tomid)
    {
        this.tomid = tomid;
    }
    
    public String getHandlerClassname()
    {
        return handlerClassname;
    }
    
    public void setHandlerClassname(String handlerClassname)
    {
        this.handlerClassname = handlerClassname;
    }

    public String getCredential()
    {
        return credential;
    }
    
    public void setCredential(String credential)
    {
        this.credential = credential;
    } 
    
    public String getBusJarFiles()
    {
        return busJarFiles;
    }
    
    public void setBusJarFiles(String busJarFiles)
    {
        this.busJarFiles = busJarFiles;
    }
    
    public String getLdLibraryPath()
    {
        return ldLibraryPath;
    }
    
    public void setLdLibraryPath(String ldLibraryPath)
    {
        this.ldLibraryPath = ldLibraryPath;
    }
    
    public String getLogTypes()
    {
        return logTypes;
    }
    
    public void setLogTypes(String logTypes)
    {
        this.logTypes = logTypes;
    }
    
    public String getProcessMaxThread()
    {
        return processMaxThread;
    }
    
    public void setProcessMaxThread(String processMaxThread)
    {
        this.processMaxThread = processMaxThread;
    }
    
    public String getProcessMinThread()
    {
        return processMinThread;
    }
    
    public void setProcessMinThread(String processMinThread)
    {
        this.processMinThread = processMinThread;
    }
    
    public String getRabbitCorePoolSize()
    {
        return rabbitCorePoolSize;
    }
    
    public void setRabbitCorePoolSize(String rabbitCorePoolSize)
    {
        this.rabbitCorePoolSize = rabbitCorePoolSize;
    }
    
    public String getRabbitMaxPoolSize()
    {
        return rabbitMaxPoolSize;
    }
    
    public void setRabbitMaxPoolSize(String rabbitMaxPoolSize)
    {
        this.rabbitMaxPoolSize = rabbitMaxPoolSize;
    }
    
    public String getRabbitKeepAliveTime()
    {
        return rabbitKeepAliveTime;
    }
    
    public void setRabbitKeepAliveTime(String rabbitKeepAliveTime)
    {
        this.rabbitKeepAliveTime = rabbitKeepAliveTime;
    }
    
    public String getRabbitBlockingQueueSize()
    {
        return rabbitBlockingQueueSize;
    }
    
    public void setRabbitBlockingQueueSize(String rabbitBlockingQueueSize)
    {
        this.rabbitBlockingQueueSize = rabbitBlockingQueueSize;
    }

	public String getRabbitSslProtocol() {
		return rabbitSslProtocol;
	}

	public void setRabbitSslProtocol(String rabbitSslProtocol) {
		this.rabbitSslProtocol = rabbitSslProtocol;
	}
    
} // ConfigReceiveTIBCOBespoke
