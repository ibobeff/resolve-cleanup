/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.gateway.database.DBFilter;
import com.resolve.gateway.database.DBGateway;
import com.resolve.gateway.database.DBGatewayFactory;
import com.resolve.gateway.database.DBPool;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for Database gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <DB....../>
 */

public class ConfigReceiveDB extends ConfigReceiveGateway
{
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_DATABASE_NODE = "./RECEIVE/DB/";
    private static final String RECEIVE_DATABASE_FILTER = RECEIVE_DATABASE_NODE + "FILTER";
    private static final String RECEIVE_DATABASE_POOL = RECEIVE_DATABASE_NODE + "POOL";
    private static final String RECEIVE_DATABASE_PROPERTIES = RECEIVE_DATABASE_NODE + "PROPERTIES";

    // DATABASE Filter Attributes
    private static final String RECEIVE_DATABASE_ATTR_POLL = RECEIVE_DATABASE_NODE + "@POLL";
    private static final String RECEIVE_DATABASE_ATTR_POLLINTERVAL = RECEIVE_DATABASE_NODE + "@POLLINTERVAL";

    private boolean active = false;
    private boolean poll = false;
    private long pollInterval = 10; // secs
    private String queue = "DATABASE";
    private String resolveStatusField = "ResolveStatus";
    private String resolveStatusValue = "1";

    File propertiesFile = null;

    List<Map<String, Object>> pools;

    public ConfigReceiveDB(XDoc config) throws Exception
    {
        super(config);

        define("poll", BOOLEAN, RECEIVE_DATABASE_ATTR_POLL);
        define("pollInterval", LONG, RECEIVE_DATABASE_ATTR_POLLINTERVAL);
    } // ConfigReceiveDB

    public String getRootNode()
    {
        return RECEIVE_DATABASE_NODE;
    }

    public void load() throws Exception
    {
        try
        {
            loadAttributes();

            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                DBGateway dbGateway = DBGateway.getInstance(this);

                // filters
                pools = xdoc.getListMapValue(RECEIVE_DATABASE_POOL);

                // initialize pools
                if (pools.size() > 0)
                {
                    for (Map<String, Object> values : pools)
                    {
                        // init pool
                        dbGateway.setPool(values);
                    }
                }

                // filters
                filters = xdoc.getListMapValue(RECEIVE_DATABASE_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(DBFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/database/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(DBFilter.SCRIPT, groovy);
                                }
                            }

                            // init filter
                            dbGateway.setFilter(values);

                            Log.log.debug("Loaded groovy script: " + scriptFile);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for Database(DB) gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    public void save() throws Exception
    {
        // create servicenow directory
        File dir = getFile(MainBase.main.release.serviceName + "/config/database");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }

        // clear list of pools to save
        if (pools != null && this.active)
        {
            pools.clear();

            for (DBPool dbPool : DBGatewayFactory.getInstance().getPools().values())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(DBPool.NAME, dbPool.getName());
                entry.put(DBPool.DRIVERCLASS, dbPool.getDriverClass());
                entry.put(DBPool.CONNECTURI, dbPool.getConnectURI());
                entry.put(DBPool.MAXCONNECTION, String.valueOf(dbPool.getMaxConnection()));
                entry.put(DBPool.USERNAME, dbPool.getDbUserName());
                entry.put(DBPool.P_ASSWORD, CryptUtils.encrypt(dbPool.getDbP_assword()));
                pools.add(entry);
            }

            // pools
            xdoc.setListMapValue(RECEIVE_DATABASE_POOL, pools);
        }

        // clear list of filters to save
        if (filters != null)
        {
            filters.clear();

            // save sql files
            for (Filter filter : DBGatewayFactory.getInstance().getFilters().values())
            {
                DBFilter dbFilter = (DBFilter) filter;

                String groovy = dbFilter.getScript();
                String scriptFileName = dbFilter.getId().toLowerCase() + ".groovy";
                File scriptFile = getFile(MainBase.main.release.serviceName + "/config/database/" + scriptFileName);

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(DBFilter.ID, dbFilter.getId());
                entry.put(DBFilter.ACTIVE, String.valueOf(dbFilter.isActive()));
                entry.put(DBFilter.ORDER, String.valueOf(dbFilter.getOrder()));
                entry.put(DBFilter.INTERVAL, String.valueOf(dbFilter.getInterval()));
                entry.put(DBFilter.EVENT_EVENTID, dbFilter.getEventEventId());
                // entry.put(DBFilter.PERSISTENT_EVENT,
                // String.valueOf(dbFilter.isPersistentEvent()));
                entry.put(DBFilter.RUNBOOK, dbFilter.getRunbook());
                // entry.put(DBFilter.SCRIPT, dbFilter.getScript());
                entry.put(DBFilter.POOL, dbFilter.getPool());
                entry.put(DBFilter.SQL, dbFilter.getSql());
                entry.put(DBFilter.UPDATESQL, dbFilter.getUpdateQuery());
                entry.put(DBFilter.LASTVALUE, dbFilter.getLastValue());
                entry.put(DBFilter.LASTVALUECOLUMN, dbFilter.getLastValueColumn());
                entry.put(DBFilter.LASTVALUEQUOTE, String.valueOf(dbFilter.isLastValueQuote()));
                entry.put(DBFilter.PREFIX, dbFilter.getPrefix());
                filters.add(entry);

                try
                {
                    // create groovy file
                    FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_DATABASE_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && this.active)
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : DBGatewayFactory.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                Map props = DBGatewayFactory.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    } // isActive

    public void setActive(boolean active)
    {
        this.active = active;
    } // setActive

    public boolean isPoll()
    {
        return poll;
    } // isPoll

    public void setPoll(boolean poll)
    {
        this.poll = poll;
    } // setPoll

    public long getPollInterval()
    {
        return pollInterval;
    } // getPollInterval

    public void setPollInterval(long pollInterval)
    {
        this.pollInterval = pollInterval;
    } // setPollInterval

    public String getQueue()
    {
        return queue;
    } // getQueue

    public void setQueue(String queue)
    {
        this.queue = queue;
    } // setQueue

    public String getResolveStatusField()
    {
        return resolveStatusField;
    }

    public void setResolveStatusField(String resolveStatusField)
    {
        this.resolveStatusField = resolveStatusField;
    }

    public String getResolveStatusValue()
    {
        return resolveStatusValue;
    }

    public void setResolveStatusValue(String resolveStatusValue)
    {
        this.resolveStatusValue = resolveStatusValue;
    }

    @Override
    public String toString()
    {
        return "ConfigReceiveDB [active=" + active + ", poll=" + poll + ", pollInterval=" + pollInterval + ", queue=" + queue + ", resolveStatusField=" + resolveStatusField + ", resolveStatusValue=" + resolveStatusValue + "]";
    }

} // ConfigReceiveDB
