/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.util.List;

import org.dom4j.Element;

import com.resolve.gateway.remedy.RemedyFilter;
import com.resolve.gateway.remedy.RemedyForm;
import com.resolve.gateway.remedy.RemedyGateway;
import com.resolve.util.ConfigMap;
import com.resolve.util.Log;
import com.resolve.util.XDoc;

public class ConfigReceiveRemedy extends ConfigMap
{
    private static final long serialVersionUID = -2032399571779978962L;
	boolean active = false;
    String host = "localhost";
    String username = "remedy";
    String password = "";
    int port = 0;
    int maxConnection = 1;
    long pollInterval = 1;
    boolean poll = false;

    public ConfigReceiveRemedy(XDoc config) throws Exception
    {
        super(config);

        define("active", BOOLEAN, "./RECEIVE/REMEDY/@ACTIVE");
        define("host", STRING, "./RECEIVE/REMEDY/@HOST");
        define("username", STRING, "./RECEIVE/REMEDY/@USERNAME");
        define("password", SECURE, "./RECEIVE/REMEDY/@PASSWORD");
        define("port", INT, "./RECEIVE/REMEDY/@PORT");
        define("maxConnection", INT, "./RECEIVE/REMEDY/@MAXCONNECTION");
        define("pollInterval", LONG, "./RECEIVE/REMEDY/@POLLINTERVAL");
        define("poll", BOOLEAN, "./RECEIVE/REMEDY/@POLL");
    } // ConfigReceiveRemedy

    public void load() throws Exception
    {
        try
        {
            loadAttributes();

            String xpath = "./RECEIVE/REMEDY/POLLFORM";
            List formValues = xdoc.getNodes(xpath);
            if (active && (formValues.size() > 0))
            {
                for (Object obj : formValues)
                {
                    Element formElem = (Element) obj;
                    RemedyForm form = new RemedyForm();
                    form.name = formElem.attributeValue("NAME");
                    form.fieldList = formElem.attributeValue("FIELDLIST");
                    form.restriction = formElem.attributeValue("WHERE");
                    form.runbook = formElem.attributeValue("RUNBOOK");

                    List filterValues = formElem.elements();
                    if (filterValues.size() > 0)
                    {
                        for (Object fValue : filterValues)
                        {
                            Element filterElem = (Element) fValue;
                            RemedyFilter filter = new RemedyFilter();
                            filter.fieldName = filterElem.attributeValue("FIELDNAME");
                            filter.fieldID = filterElem.attributeValue("FIELDID");
                            filter.operator = filterElem.attributeValue("OPERATOR");
                            filter.lastValue = filterElem.attributeValue("LASTVALUE");
                            filter.sort = Integer.parseInt(filterElem.attributeValue("SORT"));
                            filter.quote = ("true".equals(filterElem.attributeValue("QUOTE")) ? true : false);
                            form.filters.add(filter);
                        }
                    }
                    RemedyGateway.pollForms.add(form);
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.error("RemedyGateway: Couldn not load configuration for Remedy gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    public void save() throws Exception
    {
        saveAttributes();

        if (active)
        {
            xdoc.removeElements("./RECEIVE/REMEDY/POLLFORM");

            Element parentElem = (Element) xdoc.getNode("./RECEIVE/REMEDY");

            for (RemedyForm rForm : RemedyGateway.pollForms)
            {
                Element formElem = parentElem.addElement("POLLFORM");
                formElem.addAttribute("NAME", rForm.name);
                formElem.addAttribute("FIELDLIST", rForm.fieldList);
                formElem.addAttribute("WHERE", rForm.restriction);
                formElem.addAttribute("RUNBOOK", rForm.runbook);

                for (RemedyFilter rFilter : rForm.filters)
                {
                    Element filterElem = formElem.addElement("FILTER");
                    filterElem.addAttribute("FIELDNAME", rFilter.fieldName);
                    filterElem.addAttribute("FIELDID", rFilter.fieldID);
                    filterElem.addAttribute("OPERATOR", rFilter.operator);
                    filterElem.addAttribute("LASTVALUE", rFilter.lastValue);
                    filterElem.addAttribute("SORT", rFilter.sort + "");
                    filterElem.addAttribute("QUOTE", rFilter.quote ? "true" : "false");
                }

            }
        }

    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public int getMaxConnection()
    {
        return maxConnection;
    }

    public void setMaxConnection(int maxConnection)
    {
        this.maxConnection = maxConnection;
    }

    public long getPollInterval()
    {
        return pollInterval;
    }

    public void setPollInterval(long pollInterval)
    {
        this.pollInterval = pollInterval;
    }

    public boolean isPoll()
    {
        return poll;
    }

    public void setPoll(boolean poll)
    {
        this.poll = poll;
    }

} // ConfigReceiveRemedy
