/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.gateway.ews.EWSAddress;
import com.resolve.gateway.ews.EWSFilter;
import com.resolve.gateway.ews.EWSGateway;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for EWS (Exchange Web Service) gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <EWS....../>
 */
public class ConfigReceiveEWS extends ConfigReceiveGateway
{
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_EWS_NODE = "./RECEIVE/EWS/";
    private static final String RECEIVE_EWS_ADDRESS = RECEIVE_EWS_NODE + "EWS_ADDRESS";
    private static final String RECEIVE_EWS_FILTER = RECEIVE_EWS_NODE + "FILTER";
    private static final String RECEIVE_EWS_PROPERTIES = RECEIVE_EWS_NODE + "PROPERTIES";

    // EWS Filter Attributes
    private static final String RECEIVE_EWS_ATTR_URL = RECEIVE_EWS_NODE + "@URL";
    private static final String RECEIVE_EWS_ATTR_USERNAME = RECEIVE_EWS_NODE + "@USERNAME";
    private static final String RECEIVE_EWS_ATTR_P_ASSWORD = RECEIVE_EWS_NODE + "@PASSWORD";
    private static final String RECEIVE_EWS_ATTR_SOCIAL_POST = RECEIVE_EWS_NODE + "@SOCIALPOST";
    
    //EWS Proxy Config Attributes
    private static final String RECEIVE_EWS_ATTR_PROXYENABLED = RECEIVE_EWS_NODE + "@PROXYENABLED";
    private static final String RECEIVE_EWS_ATTR_PROXYUSERNAME = RECEIVE_EWS_NODE + "@PROXYUSERNAME";
    private static final String RECEIVE_EWS_ATTR_PROXYP_ASSWORD = RECEIVE_EWS_NODE + "@PROXYPASSWORD";
    private static final String RECEIVE_EWS_ATTR_PROXYPORT = RECEIVE_EWS_NODE + "@PROXYPORT";
    private static final String RECEIVE_EWS_ATTR_PROXYHOST=RECEIVE_EWS_NODE + "@PROXYHOST";
    private static final String RECEIVE_EWS_ATTR_PROXYDOMAIN=RECEIVE_EWS_NODE + "@PROXYDOMAIN";
    
    private boolean active = false;
    private String url = "";
    private int interval = 120;
    private String username = "";
    private String password = "";
    private boolean isSocialPost = false;
    private String queue = "EWS";
    
    //EWS Proxy Config Attributes
    private boolean proxyEnabled=false;
    private String proxyUsername="";
    private String proxyPassword="";
    private String proxyHost="";
    private int proxyPort;
    private String proxyDomain="";

    List<Map<String, Object>> ewsAddresses;

    public ConfigReceiveEWS(XDoc config) throws Exception
    {
        super(config);

        define("url", STRING, RECEIVE_EWS_ATTR_URL);
        define("username", STRING, RECEIVE_EWS_ATTR_USERNAME);
        define("password", SECURE, RECEIVE_EWS_ATTR_P_ASSWORD);
        define("socialpost", BOOLEAN, RECEIVE_EWS_ATTR_SOCIAL_POST);
        define("proxyEnabled", BOOLEAN, RECEIVE_EWS_ATTR_PROXYENABLED);
        define("proxyUsername", STRING, RECEIVE_EWS_ATTR_PROXYUSERNAME);
        define("proxyPassword", SECURE, RECEIVE_EWS_ATTR_PROXYP_ASSWORD);
        define("proxyHost", STRING, RECEIVE_EWS_ATTR_PROXYHOST);
        define("proxyPort", INTEGER, RECEIVE_EWS_ATTR_PROXYPORT);
        define("proxyDomain", STRING, RECEIVE_EWS_ATTR_PROXYDOMAIN);
        
    } // ConfigReceiveEWS

    public String getRootNode()
    {
        return RECEIVE_EWS_NODE;
    }

    @SuppressWarnings("unchecked")
    public void load() throws Exception
    {
        
        try
        {
            loadAttributes();

            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                EWSGateway ewsGateway = EWSGateway.getInstance(this);

                // email addresses
                ewsAddresses = xdoc.getListMapValue(RECEIVE_EWS_ADDRESS);

                // initialize emailAddress
                if (ewsAddresses.size() > 0)
                {
                    for (Map<String, Object> values : ewsAddresses)
                    {
                        // init address
                        ewsGateway.setEWSAddresses(values);
                    }
                }

                // filters
                filters = xdoc.getListMapValue(RECEIVE_EWS_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(EWSFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/ews/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(EWSFilter.SCRIPT, groovy);
                                }
                            }

                            // init filter
                            ewsGateway.setFilter(values);

                            Log.log.debug("Loaded groovy script: " + scriptFile);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for EWS gateway, check blueprint. " + e.getMessage(), e);
        }

    } // load

    public void save() throws Exception
    {
        // create ews directory
        File dir = getFile(MainBase.main.release.serviceName + "/config/ews");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }

        // clear list of filters to save
        if (ewsAddresses != null && this.active)
        {
            ewsAddresses.clear();

            // save sql files
            for (EWSAddress filter : EWSGateway.getInstance().getEWSAddresses().values())
            {
                EWSAddress emailFilter = (EWSAddress) filter;

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(EWSAddress.EWSADDRESS, emailFilter.getEWSAddress());
                entry.put(EWSAddress.EWSP_ASSWORD, CryptUtils.encrypt(emailFilter.getEWSPassword()));
                ewsAddresses.add(entry);
            }

            // filters
            xdoc.setListMapValue(RECEIVE_EWS_ADDRESS, ewsAddresses);
        }

        // clear list of filters to save
        if (filters != null && this.active)
        {
            filters.clear();

            // save sql files
            for (Filter filter : EWSGateway.getInstance().getFilters().values())
            {
                EWSFilter ewsFilter = (EWSFilter) filter;

                String groovy = ewsFilter.getScript();
                String scriptFileName = ewsFilter.getId().toLowerCase() + ".groovy";
                File scriptFile = getFile(MainBase.main.release.serviceName + "/config/ews/" + scriptFileName);

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(EWSFilter.ID, ewsFilter.getId());
                entry.put(EWSFilter.ACTIVE, String.valueOf(ewsFilter.isActive()));
                entry.put(EWSFilter.ORDER, String.valueOf(ewsFilter.getOrder()));
                entry.put(EWSFilter.INTERVAL, String.valueOf(ewsFilter.getInterval()));
                entry.put(EWSFilter.EVENT_EVENTID, ewsFilter.getEventEventId());
                entry.put(EWSFilter.RUNBOOK, ewsFilter.getRunbook());
                // entry.put(EWSFilter.SCRIPT, filter.getScript());
                entry.put(EWSFilter.QUERY, ewsFilter.getQuery());
                if (ewsFilter.getIncludeAttachment() == null)
                {
                    entry.put(EWSFilter.INCLUDE_ATTACHMENT, "false");
                }
                else
                {
                    entry.put(EWSFilter.INCLUDE_ATTACHMENT, ewsFilter.getIncludeAttachment().toString());
                }
                
                if (StringUtils.isNotBlank(ewsFilter.getLastReceivedTime()))
                {
                    entry.put(EWSFilter.LAST_RECEIVED_TIME, ewsFilter.getLastReceivedTime());
                }
                if (ewsFilter.getFolderNames().size() > 0)
                {
                    entry.put(EWSFilter.FOLDER_NAMES, StringUtils.join(ewsFilter.getFolderNames(),","));
                }
                if (ewsFilter.getMarkAsRead() == null)
                {
                	entry.put(EWSFilter.MARK_AS_READ, "false");
                }
                else
                {
                    entry.put(EWSFilter.MARK_AS_READ, ewsFilter.getMarkAsRead().toString());
                }
				if (ewsFilter.getOnlyNew() == null)
                {
                    entry.put(EWSFilter.ONLY_NEW, "true");
                }
                else
                {
                    entry.put(EWSFilter.ONLY_NEW, ewsFilter.getOnlyNew().toString());
                }
                filters.add(entry);

                try
                {
                    // create groovy file
                    FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_EWS_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && this.active)
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : EWSGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                Map props = EWSGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public boolean isSocialpost()
    {
        return isSocialPost;
    }

    public void setSocialpost(boolean isSocialPost)
    {
        this.isSocialPost = isSocialPost;
    }

    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }
    

    public boolean isProxyEnabled()
    {
        return proxyEnabled;
    }

    public void setProxyEnabled(boolean isProxyEnabled)
    {
        this.proxyEnabled = isProxyEnabled;
    }

    public String getProxyUsername()
    {
        return proxyUsername;
    }

    public void setProxyUsername(String proxyUsername)
    {
        this.proxyUsername = proxyUsername;
    }

    public String getProxyPassword()
    {
        return proxyPassword;
    }

    public void setProxyPassword(String proxyPassword)
    {
        this.proxyPassword = proxyPassword;
    }

    public String getProxyHost()
    {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost)
    {
        this.proxyHost = proxyHost;
    }

    public int getProxyPort()
    {
        return proxyPort;
    }

    public void setProxyPort(int proxyPort)
    {
        this.proxyPort = proxyPort;
    }

    public String getProxyDomain()
    {
        return proxyDomain;
    }

    public void setProxyDomain(String proxyDomain)
    {
        this.proxyDomain = proxyDomain;
    }

}
