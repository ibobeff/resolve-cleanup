/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.gateway.email.EmailGateway;
import com.resolve.gateway.salesforce.SalesforceFilter;
import com.resolve.gateway.salesforce.SalesforceGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for Salesforce gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <Salesforce....../>
 */
public class ConfigReceiveSalesforce extends ConfigReceiveGateway
{
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_SALESFORCE_NODE = "./RECEIVE/SALESFORCE/";
    private static final String RECEIVE_SALESFORCE_FILTER = RECEIVE_SALESFORCE_NODE + "FILTER";
    private static final String RECEIVE_SALESFORCE_PROPERTIES = RECEIVE_SALESFORCE_NODE + "PROPERTIES";

    //partner or enterprise
    private static final String RECEIVE_SALESFORCE_ATTR_ACCOUNTTYPE = RECEIVE_SALESFORCE_NODE + "@ACCOUNTTYPE";
    // https://login.salesforce.com/services/Soap/c/24.0/
    private static final String RECEIVE_SALESFORCE_ATTR_URL = RECEIVE_SALESFORCE_NODE + "@URL";
    private static final String RECEIVE_SALESFORCE_ATTR_HTTPBASICAUTH_USERNAME = RECEIVE_SALESFORCE_NODE + "@HTTPBASICAUTHUSERNAME";
    private static final String RECEIVE_SALESFORCE_ATTR_HTTPBASICAUTH_P_ASSWORD = RECEIVE_SALESFORCE_NODE + "@HTTPBASICAUTHPASSWORD";

    private boolean active = false;
    private String accounttype = "enterprise";
    private String url = "";
    private int interval = 120;
    private String httpbasicauthusername = "";
    private String httpbasicauthpassword = "";
    private String queue = "SALESFORCE";

    public ConfigReceiveSalesforce(XDoc config) throws Exception
    {
        super(config);

        define("accounttype", STRING, RECEIVE_SALESFORCE_ATTR_ACCOUNTTYPE);
        define("url", STRING, RECEIVE_SALESFORCE_ATTR_URL);
        define("httpbasicauthusername", STRING, RECEIVE_SALESFORCE_ATTR_HTTPBASICAUTH_USERNAME);
        define("httpbasicauthpassword", SECURE, RECEIVE_SALESFORCE_ATTR_HTTPBASICAUTH_P_ASSWORD);
    } // ConfigReceiveSalesforce

    public String getRootNode()
    {
        return RECEIVE_SALESFORCE_NODE;
    }

    @SuppressWarnings("unchecked")
    public void load() throws Exception
    {
        try
        {
            loadAttributes();

            if (isActive()) // no need to unnecessarily load anything if the gateway is inactive.
            {
                SalesforceGateway salesforceGateway = SalesforceGateway.getInstance(this);

                // filters
                filters = xdoc.getListMapValue(RECEIVE_SALESFORCE_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(SalesforceFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/salesforce/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(SalesforceFilter.SCRIPT, groovy);
                                }
                            }

                            // init filter
                            salesforceGateway.setFilter(values);

                            Log.log.debug("Loaded groovy script: " + scriptFile);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for Salesforce gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    public void save() throws Exception
    {
        // create salesforce directory
        File dir = getFile(MainBase.main.release.serviceName + "/config/salesforce");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }

        // clear list of filters to save
        if (filters != null && this.active)
        {
            filters.clear();

            // save sql files
            for (Filter filter : SalesforceGateway.getInstance().getFilters().values())
            {
                SalesforceFilter salesforceFilter = (SalesforceFilter) filter;

                String groovy = salesforceFilter.getScript();
                String scriptFileName = salesforceFilter.getId().toLowerCase() + ".groovy";
                File scriptFile = getFile(MainBase.main.release.serviceName + "/config/salesforce/" + scriptFileName);

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(SalesforceFilter.ID, salesforceFilter.getId());
                entry.put(SalesforceFilter.ACTIVE, String.valueOf(salesforceFilter.isActive()));
                entry.put(SalesforceFilter.ORDER, String.valueOf(salesforceFilter.getOrder()));
                entry.put(SalesforceFilter.INTERVAL, String.valueOf(salesforceFilter.getInterval()));
                entry.put(SalesforceFilter.EVENT_EVENTID, salesforceFilter.getEventEventId());
                // entry.put(SalesforceFilter.PERSISTENT_EVENT,
                // String.valueOf(salesforceFilter.isPersistentEvent()));
                entry.put(SalesforceFilter.RUNBOOK, salesforceFilter.getRunbook());
                // entry.put(SalesforceFilter.SCRIPT, filter.getScript());
                entry.put(SalesforceFilter.QUERY, salesforceFilter.getQuery());
                entry.put(SalesforceFilter.OBJECT, salesforceFilter.getObject());
                // entry.put(SalesforceFilter.LAST_CHANGE_DATE,
                // SalesforceFilter.getLastChangeDate());
                filters.add(entry);

                try
                {
                    // create groovy file
                    FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_SALESFORCE_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && this.active)
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : SalesforceGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                Map props = EmailGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getAccounttype()
    {
        return accounttype;
    }

    public void setAccounttype(String accounttype)
    {
        this.accounttype = accounttype;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public String getHttpbasicauthusername()
    {
        return httpbasicauthusername;
    }

    public void setHttpbasicauthusername(String httpbasicauthusername)
    {
        this.httpbasicauthusername = httpbasicauthusername;
    }

    public String getHttpbasicauthpassword()
    {
        return httpbasicauthpassword;
    }

    public void setHttpbasicauthpassword(String httpbasicauthpassword)
    {
        this.httpbasicauthpassword = httpbasicauthpassword;
    }

    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }

    /*
     * public String getChangeDateField() { return changeDateField; }
     *
     * public void setChangeDateField(String changeDateField) {
     * this.changeDateField = changeDateField; }
     */
}
