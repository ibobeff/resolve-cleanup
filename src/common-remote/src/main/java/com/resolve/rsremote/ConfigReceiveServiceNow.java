/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.gateway.Filter;
import com.resolve.gateway.servicenow.ServiceNowFilter;
import com.resolve.gateway.servicenow.ServiceNowGateway;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for ServiceNow gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <SERVICENOW....../>
 */
public class ConfigReceiveServiceNow extends ConfigReceiveGateway
{
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_SERVICENOW_NODE = "./RECEIVE/SERVICENOW/";
    private static final String RECEIVE_SERVICENOW_FILTER = RECEIVE_SERVICENOW_NODE + "FILTER";

    // https://demo08.service-now.com/GetData.do?SOAP
    private static final String RECEIVE_SERVICENOW_ATTR_URL = RECEIVE_SERVICENOW_NODE + "@URL";
    private static final String RECEIVE_SERVICENOW_ATTR_IMPORTSETAPIURL = RECEIVE_SERVICENOW_NODE + "@IMPORTSETAPIURL";
    
    private static final String RECEIVE_SERVICENOW_ATTR_HTTPBASICAUTH_USERNAME = RECEIVE_SERVICENOW_NODE + "@HTTPBASICAUTHUSERNAME";
    private static final String RECEIVE_SERVICENOW_ATTR_HTTPBASICAUTH_P_ASSWORD = RECEIVE_SERVICENOW_NODE + "@HTTPBASICAUTHPASSWORD";
    private static final String RECEIVE_SERVICENOW_ATTR_DATETIME_WEBSERVICE_NAME = RECEIVE_SERVICENOW_NODE + "@DATETIMEWEBSERVICENAME";
    private static final String RECEIVE_SERVICENOW_ATTR_PAGELIMIT = RECEIVE_SERVICENOW_NODE + "@PAGELIMIT";
    private static final int RECEIVE_SERVICENOW_DEFAULT_PAGELIMIT = 10000;
    private static final String RECEIVE_SERVICENOW_ATTR_DATETIME_RESTSERVICE_URL = RECEIVE_SERVICENOW_NODE + "@DATETIMERESTSERVICEURL";

    
  
    /* 1.  Support Mutual TLS */
    private static final String RECEIVE_SERVICENOW_ATTR_ENABLE_MUTUAL_TLS = RECEIVE_SERVICENOW_NODE + "@MUTUALTLSENABLED";
    
    private static final String RECEIVE_SERVICENOW_ATTR_MUTUAL_TLS_TRUSTSTORE = RECEIVE_SERVICENOW_NODE + "@MUTUALTLSTRUSTSTORE";
    private static final String RECEIVE_SERVICENOW_ATTR_MUTUAL_TLS_TRUSTSTORE_PA_SS = RECEIVE_SERVICENOW_NODE + "@MUTUALTLSTRUSTSTOREPASS";
    
    private static final String RECEIVE_SERVICENOW_ATTR_MUTUAL_TLS_KEYSTORE = RECEIVE_SERVICENOW_NODE + "@MUTUALTLSKEYSTORE";
    private static final String RECEIVE_SERVICENOW_ATTR_MUTUAL_TLS_KEYSTORE_PA_SS = RECEIVE_SERVICENOW_NODE + "@MUTUALTLSKEYSTOREPASS";

     
    
    /* 2.  Support Proxy Header Redirect  */
    
    private static final String RECEIVE_SERVICENOW_ATTR_PROXY_HEADER_REDIRECT_ENABLE = RECEIVE_SERVICENOW_NODE + "@HEADERREDIRENABLE";
    private static final String RECEIVE_SERVICENOW_ATTR_PROXY_HEADER_REDIRECT_URL = RECEIVE_SERVICENOW_NODE + "@HEADERREDIRURL";
    private static final String RECEIVE_SERVICENOW_ATTR_PROXY_HEADER_REDIRECT_HEADER = RECEIVE_SERVICENOW_NODE + "@HEADERREDIRHEADER";
    private static final String RECEIVE_SERVICENOW_ATTR_PROXY_HEADER_REDIRECT_ENCAPSULATEAUTH = RECEIVE_SERVICENOW_NODE + "@HEADERREDIRENCAUTH"; 

    
    /* 3.  Support Http proxy override*/
    private static final String RECEIVE_SERVICENOW_ATTR_PROXY_HOST = RECEIVE_SERVICENOW_NODE + "@PROXYHOST"; 
    private static final String RECEIVE_SERVICENOW_ATTR_PROXY_PORT = RECEIVE_SERVICENOW_NODE + "@PROXYPORT"; 
    private static final String RECEIVE_SERVICENOW_ATTR_PROXY_USER = RECEIVE_SERVICENOW_NODE + "@PROXYUSER"; 
    private static final String RECEIVE_SERVICENOW_ATTR_PROXY_PA_SS = RECEIVE_SERVICENOW_NODE + "@PROXYPASS"; 
 
    private boolean active = false;
    private String url = "";
   

    private String importsetapiurl = "";
    private int interval = 120;
    private String httpbasicauthusername = "";
    private String httpbasicauthpassword = "";
    private String queue = "SERVICENOW";
    private String datetimewebservicename = "";
    // private String changeDateField = "CHANGEDATE";
    private int pagelimit = RECEIVE_SERVICENOW_DEFAULT_PAGELIMIT;
    private String datetimerestserviceurl = "";

    private boolean mutualtlsenabled = false;
    private String mutualtlstruststore = "";
    private String mutualtlstruststorepass = "";
    private String mutualtlskeystore = "";
    private String mutualtlskeystorepass = "";
    
    private String proxyhost = "";
    private int proxyport = -1;
    
    private boolean headerredirenabled = false;
    private String headerredirurl;
    private String headerredirheader;
    private boolean headerredirencauth = true;
    
    private String proxyuser;


    private String proxytype = "HTTP";
    private String proxypass;
    
    
    public ConfigReceiveServiceNow(XDoc config) throws Exception
    {
        super(config);

        define("url", STRING, RECEIVE_SERVICENOW_ATTR_URL);
        define("importsetapiurl", STRING, RECEIVE_SERVICENOW_ATTR_IMPORTSETAPIURL);
        define("httpbasicauthusername", STRING, RECEIVE_SERVICENOW_ATTR_HTTPBASICAUTH_USERNAME);
        define("httpbasicauthpassword", SECURE, RECEIVE_SERVICENOW_ATTR_HTTPBASICAUTH_P_ASSWORD);
        define("datetimewebservicename", STRING, RECEIVE_SERVICENOW_ATTR_DATETIME_WEBSERVICE_NAME);
        define("pagelimit", INT, RECEIVE_SERVICENOW_ATTR_PAGELIMIT);
        define("datetimerestserviceurl", STRING, RECEIVE_SERVICENOW_ATTR_DATETIME_RESTSERVICE_URL);
        
        
        
        /* VISA CHANGES */
        define("mutualtlsenabled", BOOLEAN, RECEIVE_SERVICENOW_ATTR_ENABLE_MUTUAL_TLS);
        
        define("mutualtlstruststore", STRING, RECEIVE_SERVICENOW_ATTR_MUTUAL_TLS_TRUSTSTORE);
        define("mutualtlstruststorepass", SECURE, RECEIVE_SERVICENOW_ATTR_MUTUAL_TLS_TRUSTSTORE_PA_SS);
        
        define("mutualtlskeystore", STRING, RECEIVE_SERVICENOW_ATTR_MUTUAL_TLS_KEYSTORE);
        define("mutualtlskeystorepass", SECURE, RECEIVE_SERVICENOW_ATTR_MUTUAL_TLS_KEYSTORE_PA_SS);
 

        define("headerredirenabled", BOOLEAN, RECEIVE_SERVICENOW_ATTR_PROXY_HEADER_REDIRECT_ENABLE);
        define("headerredirurl", SECURE, RECEIVE_SERVICENOW_ATTR_PROXY_HEADER_REDIRECT_URL);
        define("headerredirheader", STRING, RECEIVE_SERVICENOW_ATTR_PROXY_HEADER_REDIRECT_HEADER);
        define("headerredirencauth", BOOLEAN, RECEIVE_SERVICENOW_ATTR_PROXY_HEADER_REDIRECT_ENCAPSULATEAUTH);

        
        
        define("proxyhost", STRING, RECEIVE_SERVICENOW_ATTR_PROXY_HOST);
        define("proxyport", INTEGER, RECEIVE_SERVICENOW_ATTR_PROXY_PORT);
        
        define("proxyuser", SECURE, RECEIVE_SERVICENOW_ATTR_PROXY_USER);
        define("proxypass", SECURE, RECEIVE_SERVICENOW_ATTR_PROXY_PA_SS);

        
        

    } // ConfigReceiveServiceNow

    public String getRootNode()
    {
        return RECEIVE_SERVICENOW_NODE;
    }

    @SuppressWarnings("unchecked")
    public void load() throws Exception
    {
        try
        {
            loadAttributes();

            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                ServiceNowGateway serviceNowGateway = ServiceNowGateway.getInstance(this);

                // filters
                filters = xdoc.getListMapValue(RECEIVE_SERVICENOW_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(ServiceNowFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/servicenow/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(ServiceNowFilter.SCRIPT, groovy);
                                }
                            }

                            // init filter
                            serviceNowGateway.setFilter(values);

                            Log.log.debug("Loaded groovy script: " + scriptFile);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for ServiceNow gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void save() throws Exception
    {
        // create servicenow directory
        File dir = getFile(MainBase.main.release.serviceName + "/config/servicenow");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }

        // clear list of filters to save
        if (filters != null && this.active)
        {
            filters.clear();

            // save sql files
            for (Filter filter : ServiceNowGateway.getInstance().getFilters().values())
            {
                ServiceNowFilter serviceNowFilter = (ServiceNowFilter) filter;

                String groovy = serviceNowFilter.getScript();
                String scriptFileName = serviceNowFilter.getId().toLowerCase() + ".groovy";
                File scriptFile = getFile(MainBase.main.release.serviceName + "/config/servicenow/" + scriptFileName);

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(ServiceNowFilter.ID, serviceNowFilter.getId());
                entry.put(ServiceNowFilter.ACTIVE, String.valueOf(serviceNowFilter.isActive()));
                entry.put(ServiceNowFilter.ORDER, String.valueOf(serviceNowFilter.getOrder()));
                entry.put(ServiceNowFilter.INTERVAL, String.valueOf(serviceNowFilter.getInterval()));
                entry.put(ServiceNowFilter.EVENT_EVENTID, serviceNowFilter.getEventEventId());
                // entry.put(ServiceNowFilter.PERSISTENT_EVENT,
                // String.valueOf(serviceNowFilter.isPersistentEvent()));
                entry.put(ServiceNowFilter.RUNBOOK, serviceNowFilter.getRunbook());
                // entry.put(ServiceNowFilter.SCRIPT, filter.getScript());
                entry.put(ServiceNowFilter.QUERY, serviceNowFilter.getQuery());
                entry.put(ServiceNowFilter.OBJECT, serviceNowFilter.getObject());
                entry.put(ServiceNowFilter.LAST_CHANGE_DATE, serviceNowFilter.getLastChangeDate());
                entry.put(ServiceNowFilter.LAST_NUMBER, serviceNowFilter.getLastNumber());
                filters.add(entry);

                try
                {
                    // create groovy file
                    FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_SERVICENOW_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && this.active)
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : ServiceNowGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                Map props = ServiceNowGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getImportsetapiurl()
    {
        return importsetapiurl;
    }

    public void setImportsetapiurl(String importsetapiurl)
    {
        this.importsetapiurl = importsetapiurl;
    }
    
    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public String getHttpbasicauthusername()
    {
        return httpbasicauthusername;
    }

    public void setHttpbasicauthusername(String httpbasicauthusername)
    {
        this.httpbasicauthusername = httpbasicauthusername;
    }

    public String getHttpbasicauthpassword()
    {
        return httpbasicauthpassword;
    }

    public void setHttpbasicauthpassword(String httpbasicauthpassword)
    {
        this.httpbasicauthpassword = httpbasicauthpassword;
    }

    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }

    public String getDatetimewebservicename()
    {
        return datetimewebservicename;
    }

    public void setDatetimewebservicename(String datetimewebservicename)
    {
        this.datetimewebservicename = datetimewebservicename;
    }

    /*
     * public String getChangeDateField() { return changeDateField; }
     *
     * public void setChangeDateField(String changeDateField) {
     * this.changeDateField = changeDateField; }
     */
    
    public int getPagelimit()
    {
        return pagelimit;
    }

    public void setPagelimit(int pagelimit)
    {
        this.pagelimit = pagelimit;
    }
    
    public String getDatetimerestserviceurl()
    {
        return datetimerestserviceurl;
    }

    public void setDatetimerestserviceurl(String datetimerestserviceurl)
    {
        this.datetimerestserviceurl = datetimerestserviceurl;
    }

    public final boolean isMutualtlsenabled()
    {
        return mutualtlsenabled;
    }

    public final void setMutualtlsenabled(boolean mutualtlsenabled)
    {
        this.mutualtlsenabled = mutualtlsenabled;
    }

    public final String getMutualtlstruststore()
    {
        return mutualtlstruststore;
    }

    public final void setMutualtlstruststore(String mutualtlstruststore)
    {
        this.mutualtlstruststore = mutualtlstruststore;
    }

    public final String getMutualtlstruststorepass()
    {
        return mutualtlstruststorepass;
    }

    public final void setMutualtlstruststorepass(String mutualtlstruststorepass)
    {
        this.mutualtlstruststorepass = mutualtlstruststorepass;
    }

    public final String getProxytype()
    {
        return proxytype;
    }

    public final void setProxytype(String proxytype)
    {
        this.proxytype = proxytype;
    }

    public final String getMutualtlskeystore()
    {
        return mutualtlskeystore;
    }

    public final void setMutualtlskeystore(String mutualtlskeystore)
    {
        this.mutualtlskeystore = mutualtlskeystore;
    }

    public final String getProxyhost()
    {
        return proxyhost;
    }

    public final void setProxyhost(String proxyhost)
    {
        this.proxyhost = proxyhost;
    }

    public final int getProxyport()
    {
        return proxyport;
    }

    public final void setProxyport(int proxyport)
    {
        this.proxyport = proxyport;
    }

    public final boolean isHeaderredirenabled()
    {
        return headerredirenabled;
    }

    public final void setHeaderredirenabled(boolean headerredirenabled)
    {
        this.headerredirenabled = headerredirenabled;
    }

 
    public final String getHeaderredirheader()
    {
        return headerredirheader;
    }

    public final void setHeaderredirheader(String headerredirheader)
    {
        this.headerredirheader = headerredirheader;
    }

    public final String getMutualtlskeystorepass()
    {
        return mutualtlskeystorepass;
    }

    public final void setMutualtlskeystorepass(String mutualtlskeystorepass)
    {
        this.mutualtlskeystorepass = mutualtlskeystorepass;
    }

    public final String getHeaderredirurl()
    {
        return headerredirurl;
    }

    public final void setHeaderredirurl(String headerredirurl)
    {
        this.headerredirurl = headerredirurl;
    }
    
    public final String getProxyuser()
    {
        return proxyuser;
    }

    public final void setProxyuser(String proxyuser)
    {
        this.proxyuser = proxyuser;
    }

    public final String getProxypass()
    {
        return proxypass;
    }

    public final void setProxypass(String proxypass)
    {
        this.proxypass = proxypass;
    }

    public final boolean isHeaderredirencauth()
    {
        return headerredirencauth;
    }

    public final void setHeaderredirencauth(boolean headerredirencauth)
    {
        this.headerredirencauth = headerredirencauth;
    }
    
    
}
