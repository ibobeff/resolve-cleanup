/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.util.List;
import java.util.Map;

import com.resolve.util.ConfigMap;
import com.resolve.util.SystemUtil;
import com.resolve.util.XDoc;

/**
 * This abstract class hold some common information about a gateway.
 *
 * While looking at the Gateway code think Abstraction and OO concepts like inheritance etc.
 * There are hardly any copy/paste in these codes. It helps in reducing maintenance overhead
 * and keep the codebase more meaningful. These codes go through "FindBug" which makes sure
 * that there is no obvious problem with the code.
 */
public abstract class ConfigReceiveGateway extends ConfigMap
{
    private static final long serialVersionUID = 1L;

    protected static final String NAME_KEY = "NAME";

    // EWS Filter Attributes
    private static final String RECEIVE_GATEWAY_ATTR_ACTIVE = "@ACTIVE";
    private static final String RECEIVE_GATEWAY_ATTR_PRIMARY = "@PRIMARY";
    private static final String RECEIVE_GATEWAY_ATTR_SECONDARY = "@SECONDARY";
    private static final String RECEIVE_GATEWAY_ATTR_WORKER = "@WORKER";
    private static final String RECEIVE_GATEWAY_ATTR_UPPERCASE = "@UPPERCASE";
    private static final String RECEIVE_GATEWAY_ATTR_HEARTBEAT = "@HEARTBEAT";
    private static final String RECEIVE_GATEWAY_ATTR_FAILOVER = "@FAILOVER";
    private static final String RECEIVE_GATEWAY_ATTR_INTERVAL = "@INTERVAL";
    private static final String RECEIVE_GATEWAY_ATTR_QUEUE = "@QUEUE";
    private static final String RECEIVE_GATEWAY_ATTR_TYPE = "@TYPE";
    private static final String RECEIVE_GATEWAY_ATTR_MOCK = "@MOCK";
    //DEC-354 : Adding the TIMEOUT for groovy scripts executed on the gateway
    private static final String RECEIVE_GATEWAY_ATTR_GROOVYTIMEOUT = "@GROOVYTIMEOUT";
    
    private static final String RECEIVE_GATEWAY_ATTR_WORKER_SELF_CHECK_THRESHOLD = "@WORKERSELFCHECKTHRESHOLD";
    private static final int DEFAULT_WORKER_SELF_CHECK_THRESHOLD = 5;
    private static final String RECEIVE_GATEWAY_ATTR_WORKER_SELF_CHECK_INTERVAL = "@WORKERSELFCHECKINTERVAL";
    private static final int DEFAULT_WORKER_SELF_CHECK_INTERVAL = 300;
    
    private static final String RECEIVE_GATEWAY_ATTR_TOPIC_SELF_CHECK_THRESHOLD = "@TOPICSELFCHECKTHRESHOLD";
    private static final int DEFAULT_TOPIC_SELF_CHECK_THRESHOLD = 5;
    private static final String RECEIVE_GATEWAY_ATTR_TOPIC_SELF_CHECK_INTERVAL = "@TOPICSELFCHECKINTERVAL";
    private static final int DEFAULT_TOPIC_SELF_CHECK_INTERVAL = 300;
    
    private static final String RECEIVE_GATEWAY_ATTR_CONNECTION_SELF_CHECK_INTERVAL = "@CONNECTIONSELFCHECKINTERVAL";
    private static final int DEFAULT_CONNECTION_SELF_CHECK_INTERVAL = 300;
    
    private static final String RECEIVE_GATEWAY_ATTR_QUEUE_SELF_CHECK_THRESHOLD = "@QUEUESELFCHECKTHRESHOLD";
    private static final int DEFAULT_QUEUE_SELF_CHECK_THRESHOLD = 5;
    private static final String RECEIVE_GATEWAY_ATTR_QUEUE_SELF_CHECK_INTERVAL = "@QUEUESELFCHECKINTERVAL";
    private static final int DEFAULT_QUEUE_SELF_CHECK_INTERVAL = 300;
    private static final String RECEIVE_GATEWAY_ATTR_PRIMARY_DATA_QUEUE_EXECUTOR_QUEUE_SIZE = "@PRIMARYDATAQUEUEEXECUTORQUEUESIZE";
    public static final int MIN_PRIMARY_DATA_QUEUE_EXECUTOR_QUEUE_SIZE = 1000;
    private static final String RECEIVE_GATEWAY_ATTR_PRIMARY_DATA_QUEUE_EXECUTOR_CORE_POOL_SIZE = "@PRIMARYDATAQUEUEEXECUTORCOREPOOLSIZE";
    // Default max primary data queue executor core pool size will be cpu count - 1
    private static final String RECEIVE_GATEWAY_ATTR_PRIMARY_DATA_QUEUE_EXECUTOR_MAX_POOL_SIZE = "@PRIMARYDATAQUEUEEXECUTORMAXPOOLSIZE";
    // Default primary data queue executor max pool size will be cpu count - 1
    private static final String RECEIVE_GATEWAY_ATTR_PRIMARY_DATA_QUEUE_EXECUTOR_KEEP_ALIVE_TIME_IN_SECONDS = "@PRIMARYDATAQUEUEEXECUTORKEEPALIVETIMEINSECONDS";
    private static final long MIN_PRIMARY_DATA_QUEUE_EXECUTOR_KEEP_ALIVE_TIME_IN_SECONDS = 300l;
    
    private boolean active = false;
    private boolean primary = true;
    private boolean secondary = true;
    private boolean worker = false;
    private boolean uppercase = true;
    private int heartbeat = 20;
    private int failover = 60;
    private int interval = 120;
    private String queue = null;
    private boolean mock = false;

    protected String gatewayName;
    protected String gatewayType;
    protected String gatewayPackage;
  //DEC-354 : Adding the TIMEOUT for groovy scripts executed on the gateway
    protected int groovytimeout = 60 * 1000; 

    protected List<Map<String, Object>> filters;
    protected List<Map<String, Object>> nameProperties;
    
    private int workerSCThreshold = DEFAULT_WORKER_SELF_CHECK_THRESHOLD; // 5 sec
    private int workerSCInterval = DEFAULT_WORKER_SELF_CHECK_INTERVAL; // 300 sec (5 min)
    
    private int topicSCThreshold = DEFAULT_TOPIC_SELF_CHECK_THRESHOLD; // 5 sec
    private int topicSCInterval = DEFAULT_TOPIC_SELF_CHECK_INTERVAL; // 300 sec (5 min)
    
    private int connectionSCInterval = DEFAULT_CONNECTION_SELF_CHECK_INTERVAL; // 300 sec (5 min)
    
    private int queueSCThreshold = DEFAULT_QUEUE_SELF_CHECK_THRESHOLD; // 5 sec
    private int queueSCInterval = DEFAULT_QUEUE_SELF_CHECK_INTERVAL; // 300 sec (5 min)
    
    private int primaryDataQueueExecutorQueueSize = MIN_PRIMARY_DATA_QUEUE_EXECUTOR_QUEUE_SIZE; // 1000
    private int primaryDataQueueExecutorCorePoolSize = SystemUtil.getCPUCount() > 1 ? SystemUtil.getCPUCount() - 1 : SystemUtil.getCPUCount();
    private int primaryDataQueueExecutorMaxPoolSize = SystemUtil.getCPUCount() > 1 ? SystemUtil.getCPUCount() - 1 : SystemUtil.getCPUCount();
    private long primaryDataQueueExecutorKeepAliveTimeInSec = MIN_PRIMARY_DATA_QUEUE_EXECUTOR_KEEP_ALIVE_TIME_IN_SECONDS; // 5 min
    
    /**
     * The subclass implementation provides the element name (e.g., TSRM, EWS)
     * which helps the RSRemote to read configuration from config.xml
     * @return
     */
    abstract public String getRootNode();

    /**
     * The subclass implementation loads up the filters, name properties
     * etc. for the gateway.
     */
    abstract public void load() throws Exception;

    /**
     * The subclass implementation saves the filters, name properties etc.
     */
    abstract public void save() throws Exception;

    public ConfigReceiveGateway(XDoc xdoc, String gatewayName) throws Exception {
        
        this.gatewayName = gatewayName;
        this.xdoc = xdoc;
        
        String rootNode = getRootNode();

        define("active", BOOLEAN, rootNode + RECEIVE_GATEWAY_ATTR_ACTIVE);
        define("interval", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_INTERVAL);
        define("queue", STRING, rootNode + RECEIVE_GATEWAY_ATTR_QUEUE);
        define("type", STRING, rootNode + RECEIVE_GATEWAY_ATTR_TYPE);

        define("primary", BOOLEAN, rootNode + RECEIVE_GATEWAY_ATTR_PRIMARY);
        define("secondary", BOOLEAN, rootNode + RECEIVE_GATEWAY_ATTR_SECONDARY);
        define("worker", BOOLEAN, rootNode + RECEIVE_GATEWAY_ATTR_WORKER);
        define("heartbeat", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_HEARTBEAT);
        define("failover", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_FAILOVER);
        define("uppercase", BOOLEAN, rootNode + RECEIVE_GATEWAY_ATTR_UPPERCASE);
        define("mock", BOOLEAN, rootNode + RECEIVE_GATEWAY_ATTR_MOCK);
        //DEC-354 : Adding the TIMEOUT for groovy scripts executed on the gateway
        define("groovytimeout", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_GROOVYTIMEOUT);
        
        define("workerSCThreshold", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_WORKER_SELF_CHECK_THRESHOLD);
        define("workerSCInterval", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_WORKER_SELF_CHECK_INTERVAL);
        
        define("topicSCThreshold", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_TOPIC_SELF_CHECK_THRESHOLD);
        define("topicSCInterval", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_TOPIC_SELF_CHECK_INTERVAL);
        
        define("connectionSCInterval", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_CONNECTION_SELF_CHECK_INTERVAL);
        
        define("queueSCThreshold", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_QUEUE_SELF_CHECK_THRESHOLD);
        define("queueSCInterval", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_QUEUE_SELF_CHECK_INTERVAL);
        
        define("primaryDataQueueExecutorQueueSize", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_PRIMARY_DATA_QUEUE_EXECUTOR_QUEUE_SIZE);
        define("primaryDataQueueExecutorCorePoolSize", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_PRIMARY_DATA_QUEUE_EXECUTOR_CORE_POOL_SIZE);
        define("primaryDataQueueExecutorMaxPoolSize", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_PRIMARY_DATA_QUEUE_EXECUTOR_MAX_POOL_SIZE);
        define("primaryDataQueueExecutorKeepAliveTimeInSec", LONG, rootNode + RECEIVE_GATEWAY_ATTR_PRIMARY_DATA_QUEUE_EXECUTOR_KEEP_ALIVE_TIME_IN_SECONDS);
    }
    
    /**
     * This constructor handles the common attributes for gateway
     * configuration.
     *
     * @param config
     * @throws Exception
     */
    public ConfigReceiveGateway(XDoc config) throws Exception
    {
        super(config);

        String rootNode = getRootNode();

        define("active", BOOLEAN, rootNode + RECEIVE_GATEWAY_ATTR_ACTIVE);
        define("interval", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_INTERVAL);
        define("queue", STRING, rootNode + RECEIVE_GATEWAY_ATTR_QUEUE);

        define("primary", BOOLEAN, rootNode + RECEIVE_GATEWAY_ATTR_PRIMARY);
        define("secondary", BOOLEAN, rootNode + RECEIVE_GATEWAY_ATTR_SECONDARY);
        define("worker", BOOLEAN, rootNode + RECEIVE_GATEWAY_ATTR_WORKER);
        define("heartbeat", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_HEARTBEAT);
        define("failover", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_FAILOVER);
        define("uppercase", BOOLEAN, rootNode + RECEIVE_GATEWAY_ATTR_UPPERCASE);
        define("mock", BOOLEAN, rootNode + RECEIVE_GATEWAY_ATTR_MOCK);
        
        //DEC-354 : Adding the TIMEOUT for groovy scripts executed on the gateway
        define("groovytimeout", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_GROOVYTIMEOUT);
        
        define("workerSCThreshold", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_WORKER_SELF_CHECK_THRESHOLD);
        define("workerSCInterval", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_WORKER_SELF_CHECK_INTERVAL);
        
        define("topicSCThreshold", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_TOPIC_SELF_CHECK_THRESHOLD);
        define("topicSCInterval", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_TOPIC_SELF_CHECK_INTERVAL);
        
        define("connectionSCInterval", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_CONNECTION_SELF_CHECK_INTERVAL);
        
        define("queueSCThreshold", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_QUEUE_SELF_CHECK_THRESHOLD);
        define("queueSCInterval", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_QUEUE_SELF_CHECK_INTERVAL);
        
        define("primaryDataQueueExecutorQueueSize", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_PRIMARY_DATA_QUEUE_EXECUTOR_QUEUE_SIZE);
        define("primaryDataQueueExecutorCorePoolSize", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_PRIMARY_DATA_QUEUE_EXECUTOR_CORE_POOL_SIZE);
        define("primaryDataQueueExecutorMaxPoolSize", INTEGER, rootNode + RECEIVE_GATEWAY_ATTR_PRIMARY_DATA_QUEUE_EXECUTOR_MAX_POOL_SIZE);
        define("primaryDataQueueExecutorKeepAliveTimeInSec", LONG, rootNode + RECEIVE_GATEWAY_ATTR_PRIMARY_DATA_QUEUE_EXECUTOR_KEEP_ALIVE_TIME_IN_SECONDS);
        
    } // ConfigReceiveGateway

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }
    
    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }

    public boolean isPrimary()
    {
        return primary;
    }

    public void setPrimary(boolean primary)
    {
        this.primary = primary;
    }

    public boolean isSecondary()
    {
        return secondary;
    }

    public void setSecondary(boolean secondary)
    {
        this.secondary = secondary;
    }

    public boolean isWorker()
    {
        return worker;
    }

    public void setWorker(boolean worker)
    {
        this.worker = worker;
    }

    public int getHeartbeat()
    {
        return heartbeat;
    }

    public void setHeartbeat(int heartbeat)
    {
        this.heartbeat = heartbeat;
    }

    public int getFailover()
    {
        return failover;
    }

    public void setFailover(int failover)
    {
        this.failover = failover;
    }

    public boolean isUppercase()
    {
        return uppercase;
    }

    public void setUppercase(boolean uppercase)
    {
        this.uppercase = uppercase;
    }

    public boolean isMock()
    {
        return mock;
    }

    public void setMock(boolean mock)
    {
        this.mock = mock;
    }
    
    public String getGatewayName()
    {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName)
    {
        this.gatewayName = gatewayName;
    }
    
    public String getGatewayType()
    {
        return gatewayType;
    }

    public void setGatewayType(String gatewayType)
    {
        this.gatewayType = gatewayType;
    }
    
    public String getGatewayPackage()
    {
        return gatewayPackage;
    }

    public void setGatewayPackage(String gatewayPackage)
    {
        this.gatewayPackage = gatewayPackage;
    }

    public int getWorkerSCThreshold()
    {
        return workerSCThreshold;
    }
    
    public void setWorkerSCThreshold(int workerSCThreshold)
    {
        this.workerSCThreshold = workerSCThreshold;
    }
    
    public int getWorkerSCInterval()
    {
        return workerSCInterval;
    }
    
    public void setWorkerSCInterval(int workerSCInterval)
    {
        int workerSCIntervalTmp = workerSCInterval;
        
        if (workerSCIntervalTmp < workerSCThreshold)
        {
            workerSCIntervalTmp = workerSCThreshold * 2;
        }
        
        this.workerSCInterval = workerSCIntervalTmp;
    }
    
    public int getTopicSCThreshold()
    {
        return topicSCThreshold;
    }
    
    public void setTopicSCThreshold(int topicSCThreshold)
    {
        this.topicSCThreshold = topicSCThreshold;
    }
    
    public int getTopicSCInterval()
    {
        return topicSCInterval;
    }
    
    public void setTopicSCInterval(int topicSCInterval)
    {
        int topicSCIntervalTmp = topicSCInterval;
        
        if (topicSCIntervalTmp < topicSCThreshold)
        {
            topicSCIntervalTmp = topicSCThreshold * 2;
        }
        
        this.topicSCInterval = topicSCIntervalTmp;
    }
    
    public int getConnectionSCInterval()
    {
        return connectionSCInterval;
    }
    
    public void setConnectionSCInterval(int connectionSCInterval)
    {
        int connectionSCIntervalTmp = connectionSCInterval;
        
        /*
        if (connectionSCIntervalTmp < DEFAULT_CONNECTION_SELF_CHECK_INTERVAL)
        {
            connectionSCIntervalTmp = DEFAULT_CONNECTION_SELF_CHECK_INTERVAL;
        } Commented out for testing purpose only
        */
        
        this.connectionSCInterval = connectionSCIntervalTmp;
    }
    
    public int getQueueSCThreshold()
    {
        return queueSCThreshold;
    }
    
    public void setQueueSCThreshold(int queueSCThreshold)
    {
        this.queueSCThreshold = queueSCThreshold;
    }
    
    public int getQueueSCInterval()
    {
        return queueSCInterval;
    }
    //DEC-354 : Adding the TIMEOUT for groovy scripts executed on the gateway
    public int getGroovytimeout() {
		return groovytimeout;
	}

	public void setGroovytimeout(int groovytimeout) {
		this.groovytimeout = groovytimeout;
	}
    
    public void setQueueSCInterval(int queueSCInterval)
    {
        int queueSCIntervalTmp = queueSCInterval;
        
        if (queueSCIntervalTmp < queueSCThreshold)
        {
            queueSCIntervalTmp = queueSCThreshold * 2;
        }
        
        this.queueSCInterval = queueSCIntervalTmp;
    }
    


	public int getPrimaryDataQueueExecutorQueueSize()
    {
        return primaryDataQueueExecutorQueueSize;
    }
    
    public void setPrimaryDataQueueExecutorQueueSize(int primaryDataQueueExecutorQueueSize)
    {
        if (primaryDataQueueExecutorQueueSize >= primaryDataQueueExecutorQueueSize)
        {
            this.primaryDataQueueExecutorQueueSize = primaryDataQueueExecutorQueueSize;
        }
        else
            this.primaryDataQueueExecutorQueueSize = MIN_PRIMARY_DATA_QUEUE_EXECUTOR_QUEUE_SIZE;
    }
    
    public int getPrimaryDataQueueExecutorCorePoolSize()
    {
        return primaryDataQueueExecutorCorePoolSize;
    }
    
    public void setPrimaryDataQueueExecutorCorePoolSize(int primaryDataQueueExecutorCorePoolSize)
    {
        if (primaryDataQueueExecutorCorePoolSize > 0 && 
            primaryDataQueueExecutorCorePoolSize < (SystemUtil.getCPUCount() > 1 ? SystemUtil.getCPUCount() : 2))
        {
            this.primaryDataQueueExecutorCorePoolSize = primaryDataQueueExecutorCorePoolSize;
        }
        else
        {
            this.primaryDataQueueExecutorCorePoolSize = SystemUtil.getCPUCount() > 1 ? SystemUtil.getCPUCount() - 1 : SystemUtil.getCPUCount();
        }
    }
    
    public int getPrimaryDataQueueExecutorMaxPoolSize()
    {
        return primaryDataQueueExecutorMaxPoolSize;
    }
    
    public void setPrimaryDataQueueExecutorMaxPoolSize(int primaryDataQueueExecutorMaxPoolSize)
    {
        if (primaryDataQueueExecutorMaxPoolSize >= primaryDataQueueExecutorCorePoolSize)
        {
            this.primaryDataQueueExecutorMaxPoolSize = primaryDataQueueExecutorMaxPoolSize;
        }
        else
        {
            this.primaryDataQueueExecutorMaxPoolSize = primaryDataQueueExecutorCorePoolSize;
        }
    }
    
    public long getPrimaryDataQueueExecutorKeepAliveTimeInSec()
    {
        return primaryDataQueueExecutorKeepAliveTimeInSec;
    }
    
    public void setPrimaryDataQueueExecutorKeepAliveTimeInSec(long primaryDataQueueExecutorKeepAliveTimeInSec)
    {
        if (primaryDataQueueExecutorKeepAliveTimeInSec >= MIN_PRIMARY_DATA_QUEUE_EXECUTOR_KEEP_ALIVE_TIME_IN_SECONDS)
        {
            this.primaryDataQueueExecutorKeepAliveTimeInSec = primaryDataQueueExecutorKeepAliveTimeInSec;
        }
        else
        {
            primaryDataQueueExecutorKeepAliveTimeInSec = MIN_PRIMARY_DATA_QUEUE_EXECUTOR_KEEP_ALIVE_TIME_IN_SECONDS;
        }
    }
}
