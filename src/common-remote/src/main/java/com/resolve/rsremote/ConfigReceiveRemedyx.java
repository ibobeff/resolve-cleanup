/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.gateway.remedyx.RemedyxFilter;
import com.resolve.gateway.remedyx.RemedyxForm;
import com.resolve.gateway.remedyx.RemedyxGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for RemedyX gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <REMEDYX....../>
 */
public class ConfigReceiveRemedyx extends ConfigReceiveGateway
{
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_REMEDYX_NODE = "./RECEIVE/REMEDYX/";
    private static final String RECEIVE_REMEDYX_FILTER = RECEIVE_REMEDYX_NODE + "FILTER";
    private static final String RECEIVE_REMEDYX_FORM = RECEIVE_REMEDYX_NODE + "FORM";
    private static final String RECEIVE_REMEDYX_PROPERTIES = RECEIVE_REMEDYX_NODE + "PROPERTIES";

    // REMEDYX Filter Attributes
    private static final String RECEIVE_REMEDYX_ATTR_HOST = RECEIVE_REMEDYX_NODE + "@HOST";
    private static final String RECEIVE_REMEDYX_ATTR_PORT = RECEIVE_REMEDYX_NODE + "@PORT";
    private static final String RECEIVE_REMEDYX_ATTR_USERNAME = RECEIVE_REMEDYX_NODE + "@USERNAME";
    private static final String RECEIVE_REMEDYX_ATTR_P_ASSWORD = RECEIVE_REMEDYX_NODE + "@PASSWORD";
    private static final String RECEIVE_REMEDYX_ATTR_POLL = RECEIVE_REMEDYX_NODE + "@POLL";
    private static final String RECEIVE_REMEDYX_ATTR_POLL_INTERVAL = RECEIVE_REMEDYX_NODE + "@POLLINTERVAL";

    private boolean active = false;
    private String host = "";
    private int port = -1;
    private String username = "";
    private String password = "";
    private int interval = 120;
    private boolean poll = false;
    private int pollinterval = 60;
    private String queue = "REMEDYX";

    List<Map<String, Object>> forms;

    public ConfigReceiveRemedyx(XDoc config) throws Exception
    {
        super(config);

        define("host", STRING, RECEIVE_REMEDYX_ATTR_HOST);
        define("port", INTEGER, RECEIVE_REMEDYX_ATTR_PORT);
        define("username", STRING, RECEIVE_REMEDYX_ATTR_USERNAME);
        define("password", SECURE, RECEIVE_REMEDYX_ATTR_P_ASSWORD);
        define("poll", BOOLEAN, RECEIVE_REMEDYX_ATTR_POLL);
        define("pollinterval", INTEGER, RECEIVE_REMEDYX_ATTR_POLL_INTERVAL);
    } // ConfigReceiveRemedy

    public String getRootNode()
    {
        return RECEIVE_REMEDYX_NODE;
    }

    @SuppressWarnings("unchecked")
    public void load() throws Exception
    {
        try
        {
            loadAttributes();

            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                RemedyxGateway remedyGateway = RemedyxGateway.getInstance(this);

                // forms
                forms = xdoc.getListMapValue(RECEIVE_REMEDYX_FORM);

                // initialize filters
                if (forms.size() > 0)
                {
                    for (Map<String, Object> values : forms)
                    {
                        // init filter
                        remedyGateway.setForm(values);
                    }
                }

                // filters
                filters = xdoc.getListMapValue(RECEIVE_REMEDYX_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(RemedyxFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/remedyx/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(RemedyxFilter.SCRIPT, groovy);
                                }
                            }

                            // init filter
                            remedyGateway.setFilter(values);

                            Log.log.debug("Loaded groovy script: " + scriptFile);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn("RemedyGateway: " + e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for Remedyx gateway, check blueprint. " + e.getMessage(), e);
        }

    } // load

    public void save() throws Exception
    {
        // create remedy directory
        File dir = getFile(MainBase.main.release.serviceName + "/config/remedyx");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }

        // clear list of forms to save
        if (forms != null && this.active)
        {
            forms.clear();

            for (RemedyxForm dbPool : RemedyxGateway.getInstance().getForms().values())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(RemedyxForm.NAME, dbPool.getName());
                entry.put(RemedyxForm.FIELD_LIST, dbPool.getFieldList());
                forms.add(entry);
            }

            // pools
            xdoc.setListMapValue(RECEIVE_REMEDYX_FORM, forms);
        }

        // clear list of filters to save
        if (filters != null && this.active)
        {
            filters.clear();

            // save sql files
            for (Filter filter : RemedyxGateway.getInstance().getFilters().values())
            {
                RemedyxFilter remedyFilter = (RemedyxFilter) filter;

                String groovy = remedyFilter.getScript();
                String scriptFileName = remedyFilter.getId().toLowerCase() + ".groovy";
                File scriptFile = getFile(MainBase.main.release.serviceName + "/config/remedyx/" + scriptFileName);

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(RemedyxFilter.ID, remedyFilter.getId());
                entry.put(RemedyxFilter.ACTIVE, String.valueOf(remedyFilter.isActive()));
                entry.put(RemedyxFilter.ORDER, String.valueOf(remedyFilter.getOrder()));
                entry.put(RemedyxFilter.INTERVAL, String.valueOf(remedyFilter.getInterval()));
                entry.put(RemedyxFilter.EVENT_EVENTID, remedyFilter.getEventEventId());
                // entry.put(RemedyxFilter.PERSISTENT_EVENT,
                // String.valueOf(remedyFilter.isPersistentEvent()));
                entry.put(RemedyxFilter.RUNBOOK, remedyFilter.getRunbook());
                // entry.put(RemedyFilter.SCRIPT, filter.getScript());
                entry.put(RemedyxFilter.QUERY, remedyFilter.getQuery());
                entry.put(RemedyxFilter.FORM_NAME, remedyFilter.getFormName());
                entry.put(RemedyxFilter.LAST_VALUE_FIELD, remedyFilter.getLastValueField());
                entry.put(RemedyxFilter.LAST_VALUE, remedyFilter.getLastValue());
                entry.put(RemedyxFilter.LAST_ENTRY_ID, remedyFilter.getLastEntryId());
                entry.put(RemedyxFilter.LIMIT, remedyFilter.getLimit());
                filters.add(entry);

                try
                {
                    // create groovy file
                    FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_REMEDYX_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && this.active)
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : RemedyxGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                Map props = RemedyxGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public boolean isPoll()
    {
        return poll;
    }

    public void setPoll(boolean poll)
    {
        this.poll = poll;
    }

    public int getPollinterval()
    {
        return pollinterval;
    }

    public void setPollinterval(int pollinterval)
    {
        this.pollinterval = pollinterval;
    }

    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }

    @Override
    public String toString()
    {
        return "ConfigReceiveRemedyx [active=" + active + ", host=" + host + ", port=" + port + ", username=" + username + ", password=" + password + ", interval=" + interval + ", poll=" + poll + ", pollinterval=" + pollinterval + ", queue=" + queue + ", forms=" + forms + ", filters=" + filters + ", nameProperties=" + nameProperties + "]";
    }
} // ConfigReceiveRemedyx
