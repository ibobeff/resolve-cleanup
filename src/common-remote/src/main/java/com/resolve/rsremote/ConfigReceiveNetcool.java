/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.gateway.Filter;
import com.resolve.gateway.netcool.NetcoolFilter;
import com.resolve.gateway.netcool.NetcoolGateway;
import com.resolve.rsbase.MainBase;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for Netcool gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <NETCOOL....../>
 */
public class ConfigReceiveNetcool extends ConfigReceiveGateway
{
    private static final long serialVersionUID = -2416905991167537965L;
	private static final String RECEIVE_NETCOOL_NODE = "./RECEIVE/NETCOOL/";
    private static final String RECEIVE_NETCOOL_FILTER = RECEIVE_NETCOOL_NODE + "FILTER";
    private static final String RECEIVE_NETCOOL_STATUS = RECEIVE_NETCOOL_NODE + "STATUS/";
    private static final String RECEIVE_NETCOOL_RUNBOOKID = RECEIVE_NETCOOL_NODE + "RUNBOOKID/";

    // Netcool Filter Attributes
    private static final String RECEIVE_NETCOOL_ATTR_VERSION = RECEIVE_NETCOOL_NODE + "@VERSION";
    private static final String RECEIVE_NETCOOL_ATTR_USERNAME = RECEIVE_NETCOOL_NODE + "@USERNAME";
    private static final String RECEIVE_NETCOOL_ATTR_P_ASSWORD = RECEIVE_NETCOOL_NODE + "@PASSWORD";
    private static final String RECEIVE_NETCOOL_ATTR_POOLSIZE = RECEIVE_NETCOOL_NODE + "@POOLSIZE";
    private static final String RECEIVE_NETCOOL_ATTR_RETRYDELAY = RECEIVE_NETCOOL_NODE + "@RETRYDELAY";
    private static final String RECEIVE_NETCOOL_ATTR_IPADDRESS = RECEIVE_NETCOOL_NODE + "@IPADDRESS";
    private static final String RECEIVE_NETCOOL_ATTR_RECONNECTDELAY = RECEIVE_NETCOOL_NODE + "@RECONNECTDELAY";
    private static final String RECEIVE_NETCOOL_ATTR_DRIVER = RECEIVE_NETCOOL_NODE + "@DRIVER";
    private static final String RECEIVE_NETCOOL_ATTR_URL = RECEIVE_NETCOOL_NODE + "@URL";
    private static final String RECEIVE_NETCOOL_ATTR_PORT = RECEIVE_NETCOOL_NODE + "@PORT";
    private static final String RECEIVE_NETCOOL_ATTR_URL2 = RECEIVE_NETCOOL_NODE + "@URL2";
    private static final String RECEIVE_NETCOOL_ATTR_PORT2 = RECEIVE_NETCOOL_NODE + "@PORT2";
    private static final String RECEIVE_NETCOOL_ATTR_IPADDRESS2 = RECEIVE_NETCOOL_NODE + "@IPADDRESS2";
    private static final String RECEIVE_NETCOOL_ATTR_DEBUG = RECEIVE_NETCOOL_NODE + "@DEBUG";
    private static final String RECEIVE_NETCOOL_ACTIVECONNECTIONVALIDATION  = RECEIVE_NETCOOL_NODE + "@ACTIVECONNECTIONVALIDATION";
    private static final String RECEIVE_NETCOOL_MINEVICTABLEIDLETIME  = RECEIVE_NETCOOL_NODE + "@MINEVICTABLEIDLETIME";
    private static final String RECEIVE_NETCOOL_TIMEBETWEENEVICTIONRUNS  = RECEIVE_NETCOOL_NODE + "@TIMEBETWEENEVICTIONRUNS";

    private static final String RECEIVE_NETCOOL_ATTR_STATUS_ACTIVE = RECEIVE_NETCOOL_STATUS + "@ACTIVE";
    private static final String RECEIVE_NETCOOL_ATTR_STATUS_FIELDNAME = RECEIVE_NETCOOL_STATUS + "@FIELDNAME";
    private static final String RECEIVE_NETCOOL_ATTR_STATUS_PROCESS = RECEIVE_NETCOOL_STATUS + "@PROCESS";
    private static final String RECEIVE_NETCOOL_ATTR_STATUS_TYPE = RECEIVE_NETCOOL_STATUS + "@TYPE";

    private static final String RECEIVE_NETCOOL_ATTR_RUNBOOKID_ACTIVE = RECEIVE_NETCOOL_RUNBOOKID + "@ACTIVE";
    private static final String RECEIVE_NETCOOL_ATTR_RUNBOOKID_FIELDNAME = RECEIVE_NETCOOL_RUNBOOKID + "@FIELDNAME";

  //Application name  JIRA #RS-25998
    private static final String RECEIVE_NETCOOL_ATTR_APPNAME = RECEIVE_NETCOOL_NODE + "@APPLICATIONNAME";
    
    final static int IMPACT_CLASS = 10500;
    final static String DEFAULT_INTERVAL = "10";

    private boolean debug = false;
    private String ipaddress = "";
    private int port = 4100;
    private String ipaddress2 = "";
    private int port2 = 4100;
    private String username = "root";
    private String password = "";
    private int retryDelay = 5;
    private int reconnectDelay = 30;
    private String version = "7";
    private String driver = "";
    private String url = "";
    private String url2 = "";
    private int poolsize = 10;
    private long minevictableidletime = 600;
    private long timebetweenevictionruns = 300;

  //New Param as requested by Netcool   JIRA #RS-25998
    private String applicationname = "";
    
    // update fields - status
    private boolean status_active = false;
    private String status_fieldname = "ResolveStatus";
    private String status_process = "1";
    private String status_type = "NUMBER";

    // update fields - runbookid
    private boolean runbookid_active = false;
    private String runbookid_fieldname = "ResolveRunbookId";
    
    // netcool is active
    private boolean activeconnectionvalidation = false;

    public ConfigReceiveNetcool(XDoc config) throws Exception
    {
        super(config);

        // netcool
        define("debug", BOOLEAN, RECEIVE_NETCOOL_ATTR_DEBUG);
        define("ipaddress", STRING, RECEIVE_NETCOOL_ATTR_IPADDRESS);
        define("port", INTEGER, RECEIVE_NETCOOL_ATTR_PORT);
        define("ipaddress2", STRING, RECEIVE_NETCOOL_ATTR_IPADDRESS2);
        define("port2", INTEGER, RECEIVE_NETCOOL_ATTR_PORT2);
        define("username", STRING, RECEIVE_NETCOOL_ATTR_USERNAME);
        define("password", SECURE, RECEIVE_NETCOOL_ATTR_P_ASSWORD);
        define("retryDelay", INTEGER, RECEIVE_NETCOOL_ATTR_RETRYDELAY);
        define("reconnectDelay", INTEGER, RECEIVE_NETCOOL_ATTR_RECONNECTDELAY);
        define("version", STRING, RECEIVE_NETCOOL_ATTR_VERSION);
        define("driver", STRING, RECEIVE_NETCOOL_ATTR_DRIVER);
        define("url", STRING, RECEIVE_NETCOOL_ATTR_URL);
        define("url2", STRING, RECEIVE_NETCOOL_ATTR_URL2);
        define("poolsize", INTEGER, RECEIVE_NETCOOL_ATTR_POOLSIZE);
        define("activeconnectionvalidation", BOOLEAN, RECEIVE_NETCOOL_ACTIVECONNECTIONVALIDATION);
	    define("minevictableidletime", LONG, RECEIVE_NETCOOL_MINEVICTABLEIDLETIME);
	    define("timebetweenevictionruns", LONG, RECEIVE_NETCOOL_TIMEBETWEENEVICTIONRUNS);

	  //Adding application name;  JIRA #RS-25998
        define("applicationname", STRING, RECEIVE_NETCOOL_ATTR_APPNAME);
        
        // custom update fields - status
        define("status_active", BOOLEAN, RECEIVE_NETCOOL_ATTR_STATUS_ACTIVE);
        define("status_fieldname", STRING, RECEIVE_NETCOOL_ATTR_STATUS_FIELDNAME);
        define("status_process", STRING, RECEIVE_NETCOOL_ATTR_STATUS_PROCESS);
        define("status_type", STRING, RECEIVE_NETCOOL_ATTR_STATUS_TYPE);

        // custom update fields - runbookid
        define("runbookid_active", BOOLEAN, RECEIVE_NETCOOL_ATTR_RUNBOOKID_ACTIVE);
        define("runbookid_fieldname", STRING, RECEIVE_NETCOOL_ATTR_RUNBOOKID_FIELDNAME);
    } // ConfigReceiveNetcool

    public String getRootNode()
    {
        return RECEIVE_NETCOOL_NODE;
    }

    @SuppressWarnings("unchecked")
    public void load() throws Exception
    {
        try
        {
            loadAttributes();

            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                NetcoolGateway netcoolGateway = NetcoolGateway.getInstance(this);

                // filters
                filters = xdoc.getListMapValue(RECEIVE_NETCOOL_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(NetcoolFilter.ID)).toLowerCase();
                        File scriptFile = new File(MainBase.main.release.serviceName + "/config/netcool/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(NetcoolFilter.SCRIPT, groovy);
                                }
                            }

                            // init filter
                            netcoolGateway.setFilter(values);

                            Log.log.debug("Loaded groovy script: " + scriptFile);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for Netcool gateway, check blueprint. " + e.getMessage(), e);
        }

    } // load

    public void save() throws Exception
    {
        // create netcool directory
        File dir = new File(MainBase.main.release.serviceName + "/config/netcool");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }

        // clear list of filters to save
        if (filters != null && isActive())
        {
            filters.clear();

            // save sql files
            for (Filter filter : NetcoolGateway.getInstance().getFilters().values())
            {
                NetcoolFilter netcoolFilter = (NetcoolFilter) filter;

                String groovy = netcoolFilter.getScript();
                String scriptFileName = netcoolFilter.getId().toLowerCase() + ".groovy";
                File scriptFile = FileUtils.getFile(MainBase.main.release.serviceName + "/config/netcool/" + scriptFileName);

                String sql = netcoolFilter.getSql();
                String sqlfile = netcoolFilter.getId().toLowerCase() + ".sql";
                File file = FileUtils.getFile(MainBase.main.release.serviceName + "/config/netcool/" + sqlfile);

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(NetcoolFilter.ID, netcoolFilter.getId());
                entry.put(NetcoolFilter.ACTIVE, String.valueOf(netcoolFilter.isActive()));
                entry.put(NetcoolFilter.ORDER, String.valueOf(netcoolFilter.getOrder()));
                entry.put(NetcoolFilter.INTERVAL, String.valueOf(netcoolFilter.getInterval()));
                entry.put(NetcoolFilter.EVENT_EVENTID, netcoolFilter.getEventEventId());
                entry.put(NetcoolFilter.RUNBOOK, netcoolFilter.getRunbook());
                entry.put(NetcoolFilter.SQLFILE, sqlfile);
                entry.put(NetcoolFilter.SCRIPTFILE, scriptFileName);
                filters.add(entry);

                try
                {
                    // create sqlfile
                    FileUtils.writeStringToFile(file, sql, "UTF-8");
                    // create groovy file
                    FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                }

            }

            // filters
            xdoc.setListMapValue(RECEIVE_NETCOOL_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && isActive())
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : NetcoolGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                Map props = NetcoolGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public boolean isDebug()
    {
        return debug;
    }

    public void setDebug(boolean debug)
    {
        this.debug = debug;
    }

    public String getIpaddress()
    {
        return ipaddress;
    }

    public void setIpaddress(String ipaddress)
    {
        this.ipaddress = ipaddress;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public int getPoolsize()
    {
        return poolsize;
    }

    public void setPoolsize(int p)
    {
        this.poolsize = p;
    }

    public String getIpaddress2()
    {
        return ipaddress2;
    }

    public void setIpaddress2(String ipaddress2)
    {
        this.ipaddress2 = ipaddress2;
    }

    public int getPort2()
    {
        return port2;
    }

    public void setPort2(int port2)
    {
        this.port2 = port2;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public int getRetryDelay()
    {
        return retryDelay;
    }

    public void setRetryDelay(int retryDelay)
    {
        this.retryDelay = retryDelay;
    }

    public int getReconnectDelay()
    {
        return reconnectDelay;
    }

    public void setReconnectDelay(int reconnectDelay)
    {
        this.reconnectDelay = reconnectDelay;
    }

    public boolean isStatus_active()
    {
        return status_active;
    }

    public void setStatus_active(boolean status_active)
    {
        this.status_active = status_active;
    }

    public String getStatus_fieldname()
    {
        return status_fieldname;
    }

    public void setStatus_fieldname(String status_fieldname)
    {
        this.status_fieldname = status_fieldname;
    }
    
    public String getStatus_process()
    {
        return status_process;
    }

    public void setStatus_process(String status_process)
    {
        this.status_process = status_process;
    }

    public String getStatus_type()
    {
		return status_type;
	}

	public void setStatus_type(String status_type)
    {
		this.status_type = status_type;
	}

	public boolean isRunbookid_active()
    {
        return runbookid_active;
    }

    public void setRunbookid_active(boolean runbookid_active)
    {
        this.runbookid_active = runbookid_active;
    }

    public String getRunbookid_fieldname()
    {
        return runbookid_fieldname;
    }

    public void setRunbookid_fieldname(String runbookid_fieldname)
    {
        this.runbookid_fieldname = runbookid_fieldname;
    }

    public String getDriver()
    {
        return driver;
    }

    public void setDriver(String driver)
    {
        this.driver = driver;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getUrl2()
    {
        return url2;
    }

    public void setUrl2(String url2)
    {
        this.url2 = url2;
    }

    public boolean isActiveconnectionvalidation()
    {
        return this.activeconnectionvalidation;
    }
    
    public void setActiveconnectionvalidation(boolean activeconnectionvalidation)
    {
        this.activeconnectionvalidation = activeconnectionvalidation;
    }

	public long getMinevictableidletime() {
		return minevictableidletime;
	}

	public void setMinevictableidletime(long minevictableidletime) {
		this.minevictableidletime = minevictableidletime;
	}

	public long getTimebetweenevictionruns() {
		return timebetweenevictionruns;
	}

	public void setTimebetweenevictionruns(long timebetweenevictionruns) {
		this.timebetweenevictionruns = timebetweenevictionruns;
	}

    public String getApplicationname()
    {
        return applicationname;
    }

    public void setApplicationname(String applicationname)
    {
        this.applicationname = applicationname;
    }

} // ConfigReceiveNetcool
