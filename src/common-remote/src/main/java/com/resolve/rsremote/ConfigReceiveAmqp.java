/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.gateway.amqp.AmqpFilter;
import com.resolve.gateway.amqp.AmqpGateway;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveAmqp extends ConfigReceiveGateway
{
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_AMQP_NODE = "./RECEIVE/AMQP/";
    private static final String RECEIVE_AMQP_FILTER = RECEIVE_AMQP_NODE + "FILTER";

    private static final String RECEIVE_AMQP_ATTR_SERVERWITHPORT = RECEIVE_AMQP_NODE + "@SERVERWITHPORT";
    private static final String RECEIVE_AMQP_ATTR_VIRTUALHOST = RECEIVE_AMQP_NODE + "@VIRTUALHOST";
    private static final String RECEIVE_AMQP_ATTR_USERNAME = RECEIVE_AMQP_NODE + "@USERNAME";
    private static final String RECEIVE_AMQP_ATTR_P_ASSWORD = RECEIVE_AMQP_NODE + "@PASSWORD";
    private static final String RECEIVE_AMQP_ATTR_SSL = RECEIVE_AMQP_NODE + "@SSL";
    private static final String RECEIVE_AMQP_ATTR_RETRY = RECEIVE_AMQP_NODE + "@CONNECTIONRETRY";
    private static final String RECEIVE_AMQP_ATTR_WAIT = RECEIVE_AMQP_NODE + "@CONNECTIONWAIT";
    
    private boolean active = false;
    private String serverwithport = "";
    private String virtualhost = "/";
    private String username = "";
    private String password = "";
    private boolean ssl = true;
    private String queue = "AMQP";
    public int connectionretry;
    public int connectionwait;
    
    public ConfigReceiveAmqp(XDoc config) throws Exception
    {
        super(config);

        define("serverwithport", STRING, RECEIVE_AMQP_ATTR_SERVERWITHPORT);
        define("virtualhost", STRING, RECEIVE_AMQP_ATTR_VIRTUALHOST);
        define("username", STRING, RECEIVE_AMQP_ATTR_USERNAME);
        define("password", SECURE, RECEIVE_AMQP_ATTR_P_ASSWORD);
        define("ssl", BOOLEAN, RECEIVE_AMQP_ATTR_SSL);
        define("connectionretry", INTEGER, RECEIVE_AMQP_ATTR_RETRY);
        define("connectionwait", INTEGER, RECEIVE_AMQP_ATTR_WAIT);
    } // ConfigReceiveAmqp

    public String getRootNode()
    {
        return RECEIVE_AMQP_NODE;
    }

    @SuppressWarnings("unchecked")
    public void load() throws Exception
    {
        try
        {
            loadAttributes();

            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                AmqpGateway amqpGateway = AmqpGateway.getInstance(this);

                // filters
                filters = xdoc.getListMapValue(RECEIVE_AMQP_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(AmqpFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/amqp/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(AmqpFilter.SCRIPT, groovy);
                                }
                            }

                            // init filter
                            amqpGateway.setFilter(values);

                            Log.log.debug("Loaded groovy script: " + scriptFile);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for Amqp gateway, check blueprint. " + e.getMessage(), e);
        }

    } // load

    public synchronized void save() throws Exception
    {
        // create remedy directory
        File dir = getFile(MainBase.main.release.serviceName + "/config/amqp");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }

        // clear list of filters to save
        if (filters != null && this.active)
        {
            filters.clear();

            // save sql files
            for (Filter filter : AmqpGateway.getInstance().getFilters().values())
            {
                AmqpFilter amqpFilter = (AmqpFilter) filter;

                String groovy = amqpFilter.getScript();
                String scriptFileName = amqpFilter.getId().toLowerCase() + ".groovy";
                File scriptFile = getFile(MainBase.main.release.serviceName + "/config/amqp/" + scriptFileName);

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(AmqpFilter.ID, amqpFilter.getId());
                entry.put(AmqpFilter.ACTIVE, String.valueOf(amqpFilter.isActive()));
                entry.put(AmqpFilter.ORDER, String.valueOf(amqpFilter.getOrder()));
                entry.put(AmqpFilter.INTERVAL, String.valueOf(amqpFilter.getInterval()));
                entry.put(AmqpFilter.EVENT_EVENTID, amqpFilter.getEventEventId());
                entry.put(AmqpFilter.RUNBOOK, amqpFilter.getRunbook());
                entry.put(AmqpFilter.TARGET_QUEUE, amqpFilter.getTargetQueue());
                entry.put(AmqpFilter.IS_EXCHANGE, String.valueOf(amqpFilter.isExchange()));
                filters.add(entry);

                try
                {
                    // create groovy file
                    FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_AMQP_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && this.active)
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : AmqpGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                Map props = AmqpGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getServerwithport()
    {
        return serverwithport;
    }

    public void setServerwithport(String serverwithport)
    {
        this.serverwithport = serverwithport;
    }

    public String getVirtualhost()
    {
        return virtualhost;
    }

    public void setVirtualhost(String virtualhost)
    {
        this.virtualhost = virtualhost;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public boolean isSsl()
    {
        return ssl;
    }

    public void setSsl(boolean ssl)
    {
        this.ssl = ssl;
    }

    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }

    public int getConnectionretry()
    {
        return connectionretry;
    }

    public void setConnectionretry(int connectionretry)
    {
        this.connectionretry = connectionretry;
    }

    public int getConnectionwait()
    {
        return connectionwait;
    }

    public void setConnectionwait(int connectionwait)
    {
        this.connectionwait = connectionwait;
    }

    @Override
    public String toString()
    {
        return "ConfigReceiveAmqp [active=" + active + ", serverwithport=" + serverwithport + ", username=" + username + ", password=" + password + ", ssl=" + ssl + ", queue=" + queue + ", connectionretry=" + connectionretry + ", connectionwait=" + connectionwait + ", filters=" + filters + ", nameProperties=" + nameProperties + ", toString()=" + super.toString() + "]";
    }
} // ConfigReceiveAmqp
