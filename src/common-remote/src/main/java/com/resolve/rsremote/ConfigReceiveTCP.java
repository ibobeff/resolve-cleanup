/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.gateway.tcp.TCPFilter;
import com.resolve.gateway.tcp.TCPGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for TCP gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 * 
 * <TCP....../>
 */

public class ConfigReceiveTCP extends ConfigReceiveGateway
{
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_TCP_NODE = "./RECEIVE/TCP/";
    private static final String RECEIVE_TCP_FILTER = RECEIVE_TCP_NODE + "FILTER";

    private static final String RECEIVE_TCP_ATTR_TIMEOUT = RECEIVE_TCP_NODE + "@TIMEOUT";
    
    private boolean active = false;
    private String queue = "TCP";
    private int timeout = 5000; //socket timeout in milliseconds 

    public ConfigReceiveTCP(XDoc config) throws Exception
    {
        super(config);
        define("timeout", INTEGER, RECEIVE_TCP_ATTR_TIMEOUT);
    } // ConfigReceiveTCP

    public String getRootNode()
    {
        return RECEIVE_TCP_NODE;
    }

    public void load() throws Exception
    {
        try
        {
            loadAttributes();

            if (isActive()) // no need to unnecessarily load anything if the
                            // gateway
                            // is inactive.
            {
                TCPGateway tcpGateway = TCPGateway.getInstance(this);

                // filters
                filters = xdoc.getListMapValue(RECEIVE_TCP_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        // it's important to make it lowercase because we store
                        // the files in lowercase
                        // without lowering it, linux systems have problem of
                        // not reading the script content.
                        String filterName = ((String) values.get(TCPFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/tcp/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(TCPFilter.SCRIPT, groovy);
                                }
                            }

                            // init filter
                            tcpGateway.setFilter(values);

                            Log.log.debug("Loaded groovy script: " + scriptFile);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            // deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for TCP gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    public void save() throws Exception
    {
        // create servicenow directory
        File dir = getFile(MainBase.main.release.serviceName + "/config/tcp");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }

        // clear list of filters to save
        if (filters != null)
        {
            filters.clear();

            // save sql files
            for (Filter filter : TCPGateway.getInstance().getFilters().values())
            {
                TCPFilter tcpFilter = (TCPFilter) filter;

                String groovy = tcpFilter.getScript();
                String scriptFileName = tcpFilter.getId().toLowerCase() + ".groovy";
                File scriptFile = getFile(MainBase.main.release.serviceName + "/config/tcp/" + scriptFileName);

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(TCPFilter.ID, tcpFilter.getId());
                entry.put(TCPFilter.ACTIVE, String.valueOf(tcpFilter.isActive()));
                entry.put(TCPFilter.ORDER, String.valueOf(tcpFilter.getOrder()));
                entry.put(TCPFilter.INTERVAL, String.valueOf(tcpFilter.getInterval()));
                entry.put(TCPFilter.EVENT_EVENTID, tcpFilter.getEventEventId());
                // entry.put(TCPFilter.PERSISTENT_EVENT,
                // String.valueOf(TCPFilter.isPersistentEvent()));
                entry.put(TCPFilter.RUNBOOK, tcpFilter.getRunbook());
                entry.put(TCPFilter.PORT, String.valueOf(tcpFilter.getPort()));
                entry.put(TCPFilter.BEGIN_SEPARATOR, tcpFilter.getBeginSeparator());
                entry.put(TCPFilter.END_SEPARATOR, tcpFilter.getEndSeparator());
                entry.put(TCPFilter.INCLUDE_BEGIN_SEPARATOR, String.valueOf(tcpFilter.getIncludeBeginSeparator()));
                entry.put(TCPFilter.INCLUDE_END_SEPARATOR, String.valueOf(tcpFilter.getIncludeEndSeparator()));
                filters.add(entry);

                try
                {
                    // create groovy file
                    FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_TCP_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && this.active)
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : TCPGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                Map props = TCPGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    } // isActive

    public void setActive(boolean active)
    {
        this.active = active;
    } // setActive

    public String getQueue()
    {
        return queue;
    } // getQueue

    public void setQueue(String queue)
    {
        this.queue = queue;
    } // setQueue

    public int getTimeout()
    {
        return timeout;
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }

} // ConfigReceiveTCP
