/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.resolve.gateway.Filter;
import com.resolve.gateway.tsrm.TSRMFilter;
import com.resolve.gateway.tsrm.TSRMGateway;
import com.resolve.rsbase.MainBase;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for TSRM gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <TSRM....../>
 */
public class ConfigReceiveTSRM extends ConfigReceiveGateway
{
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_TSRM_NODE = "./RECEIVE/TSRM/";
    private static final String RECEIVE_TSRM_FILTER = RECEIVE_TSRM_NODE + "FILTER";
    private static final String RECEIVE_TSRM_PROPERTIES = RECEIVE_TSRM_NODE + "PROPERTIES";

    // TSRM Filter Attributes
    private static final String RECEIVE_TSRM_ATTR_URL = RECEIVE_TSRM_NODE + "@URL";
    private static final String RECEIVE_TSRM_ATTR_RECONNECTDELAY = RECEIVE_TSRM_NODE + "@RECONNECTDELAY";
    private static final String RECEIVE_TSRM_ATTR_RETRYDELAY = RECEIVE_TSRM_NODE + "@RETRYDELAY";
    private static final String RECEIVE_TSRM_ATTR_USERNAME = RECEIVE_TSRM_NODE + "@USERNAME";
    private static final String RECEIVE_TSRM_ATTR_P_ASSWORD = RECEIVE_TSRM_NODE + "@PASSWORD";
    private static final String RECEIVE_TSRM_ATTR_HTTPBASICAUTH_USERNAME = RECEIVE_TSRM_NODE + "@HTTPBASICAUTHUSERNAME";
    private static final String RECEIVE_TSRM_ATTR_HTTPBASICAUTH_P_ASSWORD = RECEIVE_TSRM_NODE + "@HTTPBASICAUTHPASSWORD";
    private static final String RECEIVE_TSRM_ATTR_URL2 = RECEIVE_TSRM_NODE + "@URL2";
    private static final String RECEIVE_TSRM_ATTR_USERNAME2 = RECEIVE_TSRM_NODE + "@USERNAME2";
    private static final String RECEIVE_TSRM_ATTR_P_ASSWORD2 = RECEIVE_TSRM_NODE + "@PASSWORD2";
    private static final String RECEIVE_TSRM_ATTR_HTTPBASICAUTH_USERNAME2 = RECEIVE_TSRM_NODE + "@HTTPBASICAUTHUSERNAME2";
    private static final String RECEIVE_TSRM_ATTR_HTTPBASICAUTH_P_ASSWORD2 = RECEIVE_TSRM_NODE + "@HTTPBASICAUTHPASSWORD2";

    private boolean active = false;
    private String url = "";
    private int interval = 120;
    private int reconnectDelay = 30;
    private int retryDelay = 5;
    private String username = "";
    private String password = "";
    private String httpbasicauthusername = "";
    private String httpbasicauthpassword = "";
    private String queue = "TSRM";
    private String url2 = null;
    private String username2 = null;
    private String password2 = null;
    private String httpbasicauthusername2 = null;
    private String httpbasicauthpassword2 = null;

    // private String changeDateField = "CHANGEDATE";
    // private String ticketIdField = "TICKETID";

    public ConfigReceiveTSRM(XDoc config) throws Exception
    {
        super(config);

        define("url", STRING, RECEIVE_TSRM_ATTR_URL);
        define("reconnectdelay", INTEGER, RECEIVE_TSRM_ATTR_RECONNECTDELAY);
        define("retrydelay", INTEGER, RECEIVE_TSRM_ATTR_RETRYDELAY);
        define("username", STRING, RECEIVE_TSRM_ATTR_USERNAME);
        define("password", SECURE, RECEIVE_TSRM_ATTR_P_ASSWORD);
        define("httpbasicauthusername", STRING, RECEIVE_TSRM_ATTR_HTTPBASICAUTH_USERNAME);
        define("httpbasicauthpassword", SECURE, RECEIVE_TSRM_ATTR_HTTPBASICAUTH_P_ASSWORD);
        define("url2", STRING, RECEIVE_TSRM_ATTR_URL2);
        define("username2", STRING, RECEIVE_TSRM_ATTR_USERNAME2);
        define("password2", SECURE, RECEIVE_TSRM_ATTR_P_ASSWORD2);
        define("httpbasicauthusername2", STRING, RECEIVE_TSRM_ATTR_HTTPBASICAUTH_USERNAME2);
        define("httpbasicauthpassword2", SECURE, RECEIVE_TSRM_ATTR_HTTPBASICAUTH_P_ASSWORD2);
        // define("changedatefield", STRING, RECEIVE_TSRM_CHANGEDATEFIELD);
        // define("creationdatefield", STRING, RECEIVE_TSRM_CREATIONDATEFIELD);
        // define("primary", BOOLEAN, RECEIVE_TSRM_ATTR_PRIMARY);
        // define("heartbeat", INTEGER, RECEIVE_TSRM_ATTR_HEARTBEAT);
        // define("failover", INTEGER, RECEIVE_TSRM_ATTR_FAILOVER);
    } // ConfigReceiveTSRM

    public String getRootNode()
    {
        return RECEIVE_TSRM_NODE;
    }

    @SuppressWarnings("unchecked")
    public void load() throws Exception
    {
        try
        {
            loadAttributes();

            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                TSRMGateway tsrmGateway = TSRMGateway.getInstance(this);

                // filters
                filters = xdoc.getListMapValue(RECEIVE_TSRM_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(TSRMFilter.ID)).toLowerCase();
                        File scriptFile = new File(MainBase.main.release.serviceName + "/config/tsrm/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(TSRMFilter.SCRIPT, groovy);
                                }
                            }

                            // init filter
                            tsrmGateway.setFilter(values);

                            Log.log.debug("Loaded groovy script: " + scriptFile);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for TSRM gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    public void save() throws Exception
    {
        // create tsrm directory
        File dir = FileUtils.getFile(MainBase.main.release.serviceName + "/config/tsrm");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }

        // clear list of filters to save
        if (filters != null && this.active)
        {
            filters.clear();

            // save sql files
            for (Filter filter : TSRMGateway.getInstance().getFilters().values())
            {
                TSRMFilter tsrmFilter = (TSRMFilter) filter;

                String groovy = tsrmFilter.getScript();
                String scriptFileName = tsrmFilter.getId().toLowerCase() + ".groovy";
                File scriptFile = FileUtils.getFile(MainBase.main.release.serviceName + "/config/tsrm/" + scriptFileName);

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(TSRMFilter.ID, tsrmFilter.getId());
                entry.put(TSRMFilter.ACTIVE, String.valueOf(tsrmFilter.isActive()));
                entry.put(TSRMFilter.ORDER, String.valueOf(tsrmFilter.getOrder()));
                entry.put(TSRMFilter.INTERVAL, String.valueOf(tsrmFilter.getInterval()));
                entry.put(TSRMFilter.EVENT_EVENTID, tsrmFilter.getEventEventId());
                // entry.put(TSRMFilter.PERSISTENT_EVENT,
                // String.valueOf(tsrmFilter.isPersistentEvent()));
                entry.put(TSRMFilter.RUNBOOK, tsrmFilter.getRunbook());
                // entry.put(TSRMFilter.SCRIPT, filter.getScript());
                entry.put(TSRMFilter.QUERY, tsrmFilter.getQuery());
                entry.put(TSRMFilter.OBJECT, tsrmFilter.getObject());
                entry.put(TSRMFilter.LAST_CHANGE_DATE, tsrmFilter.getLastChangeDate());
                entry.put(TSRMFilter.LAST_TICKET_ID, tsrmFilter.getLastTicketId());
                filters.add(entry);

                try
                {
                    // create groovy file
                    FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_TSRM_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && this.active)
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : TSRMGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                Map props = TSRMGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public int getReconnectdelay()
    {
        return reconnectDelay;
    }

    public void setReconnectdelay(int reconnectDelay)
    {
        this.reconnectDelay = reconnectDelay;
    }

    public int getRetrydelay()
    {
        return retryDelay;
    }

    public void setRetrydelay(int retryDelay)
    {
        this.retryDelay = retryDelay;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getHttpbasicauthusername()
    {
        return httpbasicauthusername;
    }

    public void setHttpbasicauthusername(String httpbasicauthusername)
    {
        this.httpbasicauthusername = httpbasicauthusername;
    }

    public String getHttpbasicauthpassword()
    {
        return httpbasicauthpassword;
    }

    public void setHttpbasicauthpassword(String httpbasicauthpassword)
    {
        this.httpbasicauthpassword = httpbasicauthpassword;
    }

    public String getUrl2()
    {
        return url2;
    }

    public void setUrl2(String url2)
    {
        this.url2 = url2;
    }

    public String getUsername2()
    {
        return username2;
    }

    public void setUsername2(String username2)
    {
        this.username2 = username2;
    }

    public String getPassword2()
    {
        return password2;
    }

    public void setPassword2(String password2)
    {
        this.password2 = password2;
    }

    public String getHttpbasicauthusername2()
    {
        return httpbasicauthusername2;
    }

    public void setHttpbasicauthusername2(String httpbasicauthusername2)
    {
        this.httpbasicauthusername2 = httpbasicauthusername2;
    }

    public String getHttpbasicauthpassword2()
    {
        return httpbasicauthpassword2;
    }

    public void setHttpbasicauthpassword2(String httpbasicauthpassword2)
    {
        this.httpbasicauthpassword2 = httpbasicauthpassword2;
    }

    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }

    @Override
    public String toString()
    {
        return "ConfigReceiveTSRM [active=" + active + ", url=" + url + ", interval=" + interval + ", username=" + username + ", password=" + password + ", httpbasicauthusername=" + httpbasicauthusername + ", httpbasicauthpassword=" + httpbasicauthpassword + ", queue=" + queue + "]";
    }
} // ConfigReceiveTSRM
