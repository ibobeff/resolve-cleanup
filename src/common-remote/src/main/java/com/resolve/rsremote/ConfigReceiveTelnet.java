/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.gateway.ssh.SubnetMask;
import com.resolve.gateway.telnet.TelnetGateway;
import com.resolve.util.ConfigMap;
import com.resolve.util.Log;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for Telnet Pool gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <TELNET....../>
 */
public class ConfigReceiveTelnet extends ConfigMap
{
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_TELNET_NODE = "./RECEIVE/TELNET/";
    private static final String RECEIVE_TELNET_POOL = RECEIVE_TELNET_NODE + "POOL";

    private static final String RECEIVE_TELNET_ATTR_ACTIVE = RECEIVE_TELNET_NODE + "@ACTIVE";
    private static final String RECEIVE_TELNET_ATTR_INTERVAL = RECEIVE_TELNET_NODE + "@INTERVAL";
    private static final String RECEIVE_TELNET_ATTR_QUEUE = RECEIVE_TELNET_NODE + "@QUEUE";
    private static final String RECEIVE_TELNET_ATTR_MAXCONNECTION = RECEIVE_TELNET_NODE + "@MAXCONNECTION";
    private static final String RECEIVE_TELNET_ATTR_PORT = RECEIVE_TELNET_NODE + "@PORT";
    private static final String RECEIVE_TELNET_ATTR_TIMEOUT = RECEIVE_TELNET_NODE + "@TIMEOUT";
    // private static final String RECEIVE_TELNET_ATTR_TIMEOUTCOUNTER =
    // RECEIVE_TELNET_NODE + "@TIMEOUTCOUNTER";

    private boolean active = false;
    private int interval = 120;
    private String queue = "TELNET";
    private int maxconnection = 10;
    private int port = 23; // default telnet port
    private int timeout = 120; // seconds
    // private int timeoutcounter = 10; //number of times timeout occurs before
    // giving warning message.
    List<Map<String, String>> pools;

    public ConfigReceiveTelnet(XDoc config) throws Exception
    {
        super(config);

        define("active", BOOLEAN, RECEIVE_TELNET_ATTR_ACTIVE);
        define("interval", INTEGER, RECEIVE_TELNET_ATTR_INTERVAL);
        define("queue", STRING, RECEIVE_TELNET_ATTR_QUEUE);
        define("maxconnection", INTEGER, RECEIVE_TELNET_ATTR_MAXCONNECTION);
        define("port", INTEGER, RECEIVE_TELNET_ATTR_PORT);
        define("timeout", INTEGER, RECEIVE_TELNET_ATTR_TIMEOUT);
        // define("timeoutcounter", INTEGER,
        // RECEIVE_TELNET_ATTR_TIMEOUTCOUNTER);
    } // ConfigReceiveSSH

    @SuppressWarnings("unchecked")
    public void load() throws Exception
    {
        try
        {
        loadAttributes();

        if (isActive()) // no need to unnecessarily load anything if the gateway
                        // is inactive.
        {
            TelnetGateway sshGateway = TelnetGateway.getInstance(this);

            // netmasks
            pools = xdoc.getListMapValue(RECEIVE_TELNET_POOL);
            if (pools != null && pools.size() > 0)
            {
                for (Map<String, String> values : pools)
                {
                    sshGateway.setPool(values);
                }
            }
        }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for Telnet Pool gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }

    public int getMaxconnection()
    {
        return maxconnection;
    }

    public void setMaxconnection(int maxconnection)
    {
        this.maxconnection = maxconnection;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public int getTimeout()
    {
        return timeout;
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }

    /*
     * public int getTimeoutcounter() { return timeoutcounter; }
     *
     * public void setTimeoutcounter(int timeoutcounter) { this.timeoutcounter =
     * timeoutcounter; }
     */

    @Override
    public void save() throws Exception
    {
        // clear list of pools to save
        if (pools != null && this.active)
        {
            pools.clear();

            for (SubnetMask subnetMask : TelnetGateway.getInstance().getPools().values())
            {
                // add xdoc entry
                Map<String, String> entry = new HashMap<String, String>();

                entry.put(SubnetMask.SUBNETMASK, subnetMask.getSubnetMask());
                entry.put(SubnetMask.MAXCONNECTION, String.valueOf(subnetMask.getMaxConnection()));
                entry.put(SubnetMask.TIMEOUT, String.valueOf(subnetMask.getTimeout()));
                pools.add(entry);
            }

            // pools
            xdoc.setListMapValue(RECEIVE_TELNET_POOL, pools);
        }

        saveAttributes();
    }
}
