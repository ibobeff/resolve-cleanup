/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.gateway.caspectrum.CASpectrumFilter;
import com.resolve.gateway.caspectrum.CASpectrumGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for CASpectrum gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <CASPECTRUM....../>
 */
public class ConfigReceiveCASpectrum extends ConfigReceiveGateway
{
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_CASPECTRUM_NODE = "./RECEIVE/CASPECTRUM/";
    private static final String RECEIVE_CASPECTRUM_FILTER = RECEIVE_CASPECTRUM_NODE + "FILTER";
    //private static final String RECEIVE_CASPECTRUM_PROPERTIES = RECEIVE_CASPECTRUM_NODE + "PROPERTIES";

    // http://<ca spectrum server>:<port>/spectrum/restful
    private static final String RECEIVE_CASPECTRUM_ATTR_URL = RECEIVE_CASPECTRUM_NODE + "@URL";
    private static final String RECEIVE_CASPECTRUM_ATTR_HTTPBASICAUTH_USERNAME = RECEIVE_CASPECTRUM_NODE + "@HTTPBASICAUTHUSERNAME";
    private static final String RECEIVE_CASPECTRUM_ATTR_HTTPBASICAUTH_P_ASSWORD = RECEIVE_CASPECTRUM_NODE + "@HTTPBASICAUTHPASSWORD";
    //private static final String RECEIVE_CASPECTRUM_ATTR_DATETIME_WEBSERVICE_NAME = RECEIVE_CASPECTRUM_NODE + "@DATETIMEWEBSERVICENAME";
    private static final String RECEIVE_CASPECTRUM_ATTR_THROTTLESIZE = RECEIVE_CASPECTRUM_NODE + "@THROTTLESIZE";
    private static final int RECEIVE_CASPECTRUM_DEFAULT_THROTTLESIZE = 1000;

    //private boolean active = false;
    private String url = "";
    private int interval = 120;
    private String httpbasicauthusername = "";
    private String httpbasicauthpassword = "";
    private String queue = "CASPECTRUM";
    //private String datetimewebservicename = "";
    // private String changeDateField = "CHANGEDATE";
    private int throttleSize = RECEIVE_CASPECTRUM_DEFAULT_THROTTLESIZE;

    public ConfigReceiveCASpectrum(XDoc config) throws Exception
    {
        super(config);

        define("url", STRING, RECEIVE_CASPECTRUM_ATTR_URL);
        define("httpbasicauthusername", STRING, RECEIVE_CASPECTRUM_ATTR_HTTPBASICAUTH_USERNAME);
        define("httpbasicauthpassword", SECURE, RECEIVE_CASPECTRUM_ATTR_HTTPBASICAUTH_P_ASSWORD);
        //define("datetimewebservicename", STRING, RECEIVE_CASPECTRUM_ATTR_DATETIME_WEBSERVICE_NAME);
        define("throttleSize", INT, RECEIVE_CASPECTRUM_ATTR_THROTTLESIZE);
    } // ConfigReceiveServiceNow
    
    public String getRootNode()
    {
        return RECEIVE_CASPECTRUM_NODE;
    }

    @SuppressWarnings("unchecked")
    public void load() throws Exception
    {
        try
        {
            loadAttributes();
            
            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                CASpectrumGateway caSpectrumGateway = CASpectrumGateway.getInstance(this);

                // filters
                filters = xdoc.getListMapValue(RECEIVE_CASPECTRUM_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(CASpectrumFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/caspectrum/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(CASpectrumFilter.SCRIPT, groovy);
                                    Log.log.debug("Loaded groovy script: " + scriptFile);
                                }
                            }

                            // init filter
                            caSpectrumGateway.setFilter(values);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for CA Spectrum gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void save() throws Exception
    {
        // create caspectrum directory
        File dir = getFile(MainBase.main.release.serviceName + "/config/caspectrum");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }
        
        // clear list of filters to save
        if (filters != null && isActive())
        {
            filters.clear();

            // save sql files
            for (Filter filter : CASpectrumGateway.getInstance().getFilters().values())
            {
                CASpectrumFilter caSpectrumFilter = (CASpectrumFilter) filter;

                String groovy = caSpectrumFilter.getScript();
                
                String scriptFileName;
                File scriptFile = null;
                
                if (StringUtils.isNotBlank(groovy))
                {
                    scriptFileName = caSpectrumFilter.getId().toLowerCase() + ".groovy";
                    scriptFile = getFile(MainBase.main.release.serviceName + "/config/caspectrum/" + scriptFileName);
                }
                
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(CASpectrumFilter.ID, caSpectrumFilter.getId());
                entry.put(CASpectrumFilter.ACTIVE, String.valueOf(caSpectrumFilter.isActive()));
                entry.put(CASpectrumFilter.ORDER, String.valueOf(caSpectrumFilter.getOrder()));
                entry.put(CASpectrumFilter.INTERVAL, String.valueOf(caSpectrumFilter.getInterval()));
                entry.put(CASpectrumFilter.EVENT_EVENTID, caSpectrumFilter.getEventEventId());
                entry.put(CASpectrumFilter.RUNBOOK, caSpectrumFilter.getRunbook());
                entry.put(CASpectrumFilter.SCRIPT, caSpectrumFilter.getScript());
                entry.put(CASpectrumFilter.URLQUERY, caSpectrumFilter.getUrlQuery());
                entry.put(CASpectrumFilter.XMLQUERY, caSpectrumFilter.getXmlQuery());
                entry.put(CASpectrumFilter.OBJECT, caSpectrumFilter.getObject());
                entry.put(CASpectrumFilter.LATESTALARMTIME, caSpectrumFilter.getLatestAlarmTime());
                
                filters.add(entry);

                if (scriptFile != null)
                {
                    try
                    {
                        // create groovy file
                        FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                    }
                    catch (Exception e)
                    {
                        Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                    }
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_CASPECTRUM_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && isActive())
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : CASpectrumGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                Map props = CASpectrumGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public String getHttpbasicauthusername()
    {
        return httpbasicauthusername;
    }

    public void setHttpbasicauthusername(String httpbasicauthusername)
    {
        this.httpbasicauthusername = httpbasicauthusername;
    }

    public String getHttpbasicauthpassword()
    {
        return httpbasicauthpassword;
    }

    public void setHttpbasicauthpassword(String httpbasicauthpassword)
    {
        this.httpbasicauthpassword = httpbasicauthpassword;
    }
    
    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }
    
    public int getThrottleSize()
    {
        return throttleSize;
    }

    public void setThrottleSize(int throttleSize)
    {
        this.throttleSize = throttleSize;
    }
}
