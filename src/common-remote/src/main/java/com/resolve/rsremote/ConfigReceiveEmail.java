/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.gateway.email.EmailAddress;
import com.resolve.gateway.email.EmailFilter;
import com.resolve.gateway.email.EmailGateway;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveEmail extends ConfigReceiveGateway
{
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_EMAIL_NODE = "./RECEIVE/EMAIL/";
    private static final String RECEIVE_EMAIL_ADDRESS = RECEIVE_EMAIL_NODE + "EMAIL_ADDRESS";
    private static final String RECEIVE_EMAIL_FILTER = RECEIVE_EMAIL_NODE + "FILTER";
    private static final String RECEIVE_EMAIL_PROPERTIES = RECEIVE_EMAIL_NODE + "PROPERTIES";
    private static final String RECEIVE_EMAIL_PROPERTIES_GENERAL = RECEIVE_EMAIL_NODE + "@PROPERTIES";

    // Email Filter Attributes
    private static final String RECEIVE_EMAIL_ATTR_PROTOCOL = RECEIVE_EMAIL_NODE + "@PROTOCOL";
    private static final String RECEIVE_EMAIL_ATTR_SOCIAL_POST = RECEIVE_EMAIL_NODE + "@SOCIALPOST";

    // POP3
    private static final String RECEIVE_EMAIL_POP3 = RECEIVE_EMAIL_NODE + "POP3/";
    private static final String RECEIVE_EMAIL_POP3_ATTR_IPADDRESS = RECEIVE_EMAIL_POP3 + "@IPADDRESS";
    private static final String RECEIVE_EMAIL_POP3_ATTR_PORT = RECEIVE_EMAIL_POP3 + "@PORT";
    private static final String RECEIVE_EMAIL_POP3_ATTR_SSL = RECEIVE_EMAIL_POP3 + "@SSL";
    private static final String RECEIVE_EMAIL_POP3_ATTR_FOLDER = RECEIVE_EMAIL_POP3 + "@FOLDER";
    private static final String RECEIVE_EMAIL_POP3_ATTR_USERNAME = RECEIVE_EMAIL_POP3 + "@USERNAME";
    private static final String RECEIVE_EMAIL_POP3_ATTR_P_ASSWORD = RECEIVE_EMAIL_POP3 + "@PASSWORD";

    //GENERIC EMAILS EG-2
    //private static final String RECEIVE_EMAIL_GENERAL = RECEIVE_EMAIL_NODE + "GENERAL/";
    private static final String RECEIVE_EMAIL_ATTR_IPADDRESS = RECEIVE_EMAIL_NODE + "@IPADDRESS";
    private static final String RECEIVE_EMAIL_ATTR_PORT = RECEIVE_EMAIL_NODE + "@PORT";
    private static final String RECEIVE_EMAIL_ATTR_SSL = RECEIVE_EMAIL_NODE + "@SSL";
    private static final String RECEIVE_EMAIL_ATTR_FOLDER = RECEIVE_EMAIL_NODE + "@FOLDER";
    private static final String RECEIVE_EMAIL_ATTR_USERNAME = RECEIVE_EMAIL_NODE + "@USERNAME";
    private static final String RECEIVE_EMAIL_ATTR_P_ASSWORD = RECEIVE_EMAIL_NODE + "@PASSWORD";
    private static final String RECEIVE_EMAIL_ATTR_DELETE = RECEIVE_EMAIL_NODE + "@DELETE";
    
    // SMTP
    private static final String RECEIVE_EMAIL_SMTP = RECEIVE_EMAIL_NODE + "SMTP/";
    private static final String RECEIVE_EMAIL_SMTP_ATTR_HOST = RECEIVE_EMAIL_SMTP + "@HOST";
    private static final String RECEIVE_EMAIL_SMTP_ATTR_PORT = RECEIVE_EMAIL_SMTP + "@PORT";
    private static final String RECEIVE_EMAIL_SMTP_ATTR_FROM = RECEIVE_EMAIL_SMTP + "@FROM";
    private static final String RECEIVE_EMAIL_SMTP_ATTR_USERNAME = RECEIVE_EMAIL_SMTP + "@USERNAME";
    private static final String RECEIVE_EMAIL_SMTP_ATTR_P_ASSWORD = RECEIVE_EMAIL_SMTP + "@PASSWORD";
    private static final String RECEIVE_EMAIL_SMTP_ATTR_SSL = RECEIVE_EMAIL_SMTP + "@SSL";
    private static final String RECEIVE_EMAIL_SMTP_ATTR_PROPERTIES = RECEIVE_EMAIL_SMTP + "@PROPERTIES";

    boolean messagedelete = true;
    boolean active = false;
    String ipaddr = "localhost";
    String queue = "EMAIL";
    int port = 110;
    boolean pop3ssl = false;
    String protocol = "pop3";
    boolean isSocialPost = false;
    String username = "";
    String password = "";
    int interval = 120;
    String folder = "INBOX";
    String properties = "pop3.properties";

    String smtphost = "localhost";
    int smtpport = 25;
    // boolean smtpsecure=false;
    String smtpfrom = "";
    String smtpusername = "";
    String smtppassword = "";
    boolean smtpssl = false;
    String smtpproperties = "";

    File propertiesFile = null;

    List<Map<String, Object>> emailAddresses;

    public ConfigReceiveEmail(XDoc config) throws Exception
    {
        super(config);

        define("protocol", STRING, RECEIVE_EMAIL_ATTR_PROTOCOL);
        define("socialpost", BOOLEAN, RECEIVE_EMAIL_ATTR_SOCIAL_POST);
        if(config.getStringValue(RECEIVE_EMAIL_ATTR_PROTOCOL).equalsIgnoreCase("pop3")) {
        	define("properties", STRING, RECEIVE_EMAIL_PROPERTIES);
		    define("ipaddr", STRING, RECEIVE_EMAIL_POP3_ATTR_IPADDRESS);
		    define("port", INTEGER, RECEIVE_EMAIL_POP3_ATTR_PORT);
		    define("ssl", BOOLEAN, RECEIVE_EMAIL_POP3_ATTR_SSL);
		    define("folder", STRING, RECEIVE_EMAIL_POP3_ATTR_FOLDER);
		    define("username", STRING, RECEIVE_EMAIL_POP3_ATTR_USERNAME);
		    define("password", SECURE, RECEIVE_EMAIL_POP3_ATTR_P_ASSWORD);
        }else {
        	define("properties", STRING, RECEIVE_EMAIL_PROPERTIES_GENERAL);
        	define("ipaddr", STRING, RECEIVE_EMAIL_ATTR_IPADDRESS);
	        define("port", INTEGER, RECEIVE_EMAIL_ATTR_PORT);
	        define("ssl", BOOLEAN, RECEIVE_EMAIL_ATTR_SSL);
	        define("folder", STRING, RECEIVE_EMAIL_ATTR_FOLDER);
	        define("username", STRING, RECEIVE_EMAIL_ATTR_USERNAME);
	        define("password", SECURE, RECEIVE_EMAIL_ATTR_P_ASSWORD);        
	        define("messagedelete", BOOLEAN, RECEIVE_EMAIL_ATTR_DELETE);
        }
        
        define("smtphost", STRING, RECEIVE_EMAIL_SMTP_ATTR_HOST);
        define("smtpport", INTEGER, RECEIVE_EMAIL_SMTP_ATTR_PORT);
        define("smtpfrom", STRING, RECEIVE_EMAIL_SMTP_ATTR_FROM);
        define("smtpusername", STRING, RECEIVE_EMAIL_SMTP_ATTR_USERNAME);
        define("smtppassword", SECURE, RECEIVE_EMAIL_SMTP_ATTR_P_ASSWORD);
        define("smtpssl", BOOLEAN, RECEIVE_EMAIL_SMTP_ATTR_SSL);
        define("smtpproperties", STRING, RECEIVE_EMAIL_SMTP_ATTR_PROPERTIES);

    } // ConfigReceiveEmail

    public String getRootNode()
    {
        return RECEIVE_EMAIL_NODE;
    }

    public void load() throws Exception
    {
        try
        {
            loadAttributes();

            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                EmailGateway emailGateway = EmailGateway.getInstance(this);

                // email addresses
                emailAddresses = xdoc.getListMapValue(RECEIVE_EMAIL_ADDRESS);

                // initialize emailAddress
                if (emailAddresses.size() > 0)
                {
                    for (Map<String, Object> values : emailAddresses)
                    {
                        // init filter
                        emailGateway.setEmailAddresses(values);
                    }
                }

                // filters
                filters = xdoc.getListMapValue(RECEIVE_EMAIL_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(EmailFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/email/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(EmailFilter.SCRIPT, groovy);
                                }
                            }

                            // init filter
                            emailGateway.setFilter(values);

                            Log.log.debug("Loaded groovy script: " + scriptFile);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for Email gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    public void save() throws Exception
    {
        // create email directory
        File dir = getFile(MainBase.main.release.serviceName + "/config/email");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }

        // clear list of filters to save
        if (emailAddresses != null && this.active)
        {
            emailAddresses.clear();

            // save sql files
            for (EmailAddress filter : EmailGateway.getInstance().getEmailAddresses().values())
            {
                EmailAddress emailFilter = (EmailAddress) filter;

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(EmailAddress.EMAILADDRESS, emailFilter.getEmailAddress());
                entry.put(EmailAddress.EMAILP_ASSWORD, CryptUtils.encrypt(emailFilter.getEmailPassword()));
                emailAddresses.add(entry);
            }

            // filters
            xdoc.setListMapValue(RECEIVE_EMAIL_ADDRESS, emailAddresses);
        }

        // clear list of filters to save
        if (filters != null && this.active)
        {
            filters.clear();

            // save sql files
            for (Filter filter : EmailGateway.getInstance().getFilters().values())
            {
                EmailFilter emailFilter = (EmailFilter) filter;

                String groovy = emailFilter.getScript();
                String scriptFileName = emailFilter.getId().toLowerCase() + ".groovy";
                File scriptFile = getFile(MainBase.main.release.serviceName + "/config/email/" + scriptFileName);

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(EmailFilter.ID, emailFilter.getId());
                entry.put(EmailFilter.ACTIVE, String.valueOf(emailFilter.isActive()));
                entry.put(EmailFilter.ORDER, String.valueOf(emailFilter.getOrder()));
                entry.put(EmailFilter.INTERVAL, String.valueOf(emailFilter.getInterval()));
                entry.put(EmailFilter.EVENT_EVENTID, emailFilter.getEventEventId());
                // entry.put(EmailFilter.PERSISTENT_EVENT,
                // String.valueOf(emailFilter.isPersistentEvent()));
                entry.put(EmailFilter.RUNBOOK, emailFilter.getRunbook());
                // entry.put(EmailFilter.SCRIPT, filter.getScript());
                //entry.put(EmailFilter.FIELD, emailFilter.getField());
                //entry.put(EmailFilter.REGEX, emailFilter.getRegex());
                entry.put(EmailFilter.QUERY, emailFilter.getQuery());
                filters.add(entry);

                try
                {
                    // create groovy file
                    FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_EMAIL_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && this.active)
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : EmailGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                Map props = EmailGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getIpaddr()
    {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr)
    {
        this.ipaddr = ipaddr;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public boolean isSsl()
    {
        return pop3ssl;
    }

    public void setSsl(boolean ssl)
    {
        this.pop3ssl = ssl;
    }

    public String getProtocol()
    {
        return protocol;
    }

    public void setProtocol(String protocol)
    {
        this.protocol = protocol;
    }

    public boolean isSocialpost()
    {
        return isSocialPost;
    }

    public void setSocialpost(boolean isSocialPost)
    {
        this.isSocialPost = isSocialPost;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public String getFolder()
    {
        return folder;
    }

    public void setFolder(String folder)
    {
        this.folder = folder;
    }

    public String getProperties()
    {
        return properties;
    }

    public void setProperties(String properties)
    {
        this.properties = properties;
    }

    public File getPropertiesFile()
    {
        return propertiesFile;
    }

    public void setPropertiesFile(File propertiesFile)
    {
        this.propertiesFile = propertiesFile;
    }

    public String getQueue()
    {
        return queue;
    } // getQueue

    public void setQueue(String queue)
    {
        this.queue = queue;
    } // setQueue

    public String getSmtphost()
    {
        return smtphost;
    }

    public void setSmtphost(String smtphost)
    {
        this.smtphost = smtphost;
    }

    public int getSmtpport()
    {
        return smtpport;
    }

    public void setSmtpport(int smtpport)
    {
        this.smtpport = smtpport;
    }

    public String getSmtpfrom()
    {
        return smtpfrom;
    }

    public void setSmtpfrom(String smtpfrom)
    {
        this.smtpfrom = smtpfrom;
    }

    public String getSmtpusername()
    {
        return smtpusername;
    }

    public void setSmtpusername(String smtpusername)
    {
        this.smtpusername = smtpusername;
    }

    public String getSmtppassword()
    {
        return smtppassword;
    }

    public void setSmtppassword(String smtppassword)
    {
        this.smtppassword = smtppassword;
    }

    public boolean isSmtpssl()
    {
        return smtpssl;
    }

    public void setSmtpssl(boolean ssl)
    {
        this.smtpssl = ssl;
    }

    public String getSmtpproperties()
    {
        return smtpproperties;
    }

    public void setSmtpproperties(String smtpproperties)
    {
        this.smtpproperties = smtpproperties;
    }

    public boolean isMessagedelete() {
	 	return messagedelete;
	}

	public void setMessagedelete(boolean messagedelete) {
		this.messagedelete = messagedelete;
	}
    
    @Override
    public String toString()
    {
        return "ConfigReceiveEmail [active=" + active + ", ipaddr=" + ipaddr + ", queue=" + queue + ", port=" + port + ", ssl=" + pop3ssl + ", protocol=" + protocol + ", username=" + username + ", password=" + password + ", interval=" + interval + ", folder=" + folder + ", properties=" + properties + ", smtphost=" + smtphost + ", smtpport=" + smtpport + ", smtpusername=" + smtpusername + ", smtppassword=" + smtppassword + "]";
    }
} // ConfigReceiveEmail
