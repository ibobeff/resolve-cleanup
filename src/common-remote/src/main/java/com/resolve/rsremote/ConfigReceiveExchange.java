/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.exchange.ExchangeFilter;
import com.resolve.gateway.exchange.ExchangeGateway;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

public class ConfigReceiveExchange extends ConfigReceiveGateway
{
    private static final long serialVersionUID = -7390323806285860175L;
	final static int IMPACT_CLASS = 10500;
    final static String DEFAULT_INTERVAL = "10";

    // boolean debug = false;
    boolean active = false;
    String queue = "EXCHANGE";
    String host = "127.0.0.1";
    String username = "username";
    String p_assword = "password";
    String domain = "domain";
    String mailbox = "mailbox";
    String mapiClientVersion = "12.4518.1014";
    boolean ewsmode = true;
    String ewsurl = "https://localhost/ews/exchange.asmx";
    boolean acceptAllCerts = true;
    int maxAttachmentSize = 0;

    int retryDelay = 5;
    int reconnectDelay = 30;

    List filters;

    public ConfigReceiveExchange(XDoc config) throws Exception
    {
        super(config);

        // netcool
        // define("debug", BOOLEAN, "./RECEIVE/EXCHANGE/@DEBUG");
        define("active", BOOLEAN, "./RECEIVE/EXCHANGE/@ACTIVE");
        define("queue", STRING, "./RECEIVE/EXCHANGE/@QUEUE");
        define("host", STRING, "./RECEIVE/EXCHANGE/@HOST");
        define("username", STRING, "./RECEIVE/EXCHANGE/@USERNAME");
        define("p_assword", SECURE, "./RECEIVE/EXCHANGE/@PASSWORD");
        define("domain", STRING, "./RECEIVE/EXCHANGE/@DOMAIN");
        define("mailbox", STRING, "./RECEIVE/EXCHANGE/@MAILBOX");
        define("ewsmode", BOOLEAN, "./RECEIVE/EXCHANGE/@EWSMODE");
        define("ewsurl", STRING, "./RECEIVE/EXCHANGE/@EWSURL");
        define("acceptAllCerts", BOOLEAN, "./RECEIVE/EXCHANGE/@ACCEPTALLCERTS");

        define("mapiClientVersion", STRING, "./RECEIVE/EXCHANGE/@MAPICLIENTVERSION");
        define("maxAttachmentSize", INTEGER, "./RECEIVE/EXCHANGE/@MAXATTACHMENTMESSAGESIZE");

        define("retryDelay", INTEGER, "./RECEIVE/EXCHANGE/@RETRYDELAY");
        define("reconnectDelay", INTEGER, "./RECEIVE/EXCHANGE/@RECONNECTDELAY");

    } // ConfigReceiveExchange


    public String getRootNode()
    {
        return "./RECEIVE/EXCHANGE/";
    }

    public void load() throws Exception
    {
        try
        {
            loadAttributes();

            if (this.active)
            {
                ExchangeGateway exchangeGateway = ExchangeGateway.getInstance(this);
                
                // filters
                filters = xdoc.getListMapValue("./RECEIVE/EXCHANGE/FILTER");

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Iterator i = filters.iterator(); i.hasNext();)
                    {
                        Map values = (Map) i.next();

                        String id = (String) values.get("ID");
                        String eventEventId = (String) values.get("EVENT_EVENTID");
                        String persistentEvent = (String) values.get("PERSISTENT_EVENT");
                        String runbook = (String) values.get("RUNBOOK");
                        String order = (String) values.get("ORDER");
                        String interval = (String) values.get("INTERVAL");
                        String active = (String) values.get("ACTIVE");
                        // String sqlfile = (String)values.get("SQLFILE");
                        // File file = new
                        // File(MainBase.main.release.serviceName+"/config/exchange/"+sqlfile);
                        File configDir = new File(MainBase.main.release.serviceName + "/config/exchange");

                        if (StringUtils.isEmpty(interval))
                        {
                            interval = DEFAULT_INTERVAL;
                        }

                        try
                        {
                            if (configDir.isDirectory())
                            {
                                File[] fileArray = configDir.listFiles();

                                if (fileArray != null)
                                {
                                    Map<String, String> criteriaMap = new HashMap();

                                    for (File fileLocal : fileArray)
                                    {
                                        String fileName = fileLocal.getName();
                                        // The config folder may have groovy script which we do not want here. 
                                        // Script is handled in the next step.
                                        if (!fileName.endsWith(".groovy"))
                                        {
                                            String value = FileUtils.readFileToString(fileLocal, "UTF-8");

                                            if (fileName != null && !fileName.equals("") && fileName.contains(id.toLowerCase()))
                                            {
                                                criteriaMap.put(fileName, value);
                                            }
                                        }
                                    }

                                    //it's important to make it lowercase because we store the files in lowercase
                                    //without lowering it, linux systems have problem of not reading the script content.
                                    String fileName = id.toLowerCase();

                                    File scriptFile = new File(MainBase.main.release.serviceName + "/config/exchange/" + fileName + ".groovy");
                                    String groovy = null;
                                    if (scriptFile.exists())
                                    {
                                        groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                        if (StringUtils.isBlank(groovy))
                                        {
                                            groovy = null; // TODO weird but need
                                                           // refactor of whole
                                                           // exchange gateway.
                                        }
                                    }
                                    
                                    String map = StringUtils.remove(StringUtils.mapToString(criteriaMap), "null");
                                    values.put(ExchangeFilter.CRITERIA, map);
                                    
                                    exchangeGateway.setFilter(values);
                                }
                            }

                            Log.log.info("Loaded filter: " + id);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for Exchange gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    public void save() throws Exception
    {
        // create netcool directory
        File dir = new File(MainBase.main.release.serviceName + "/config/exchange");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }

        // clear list of filters to save
        if (filters != null && this.active)
        {
            ExchangeGateway exchangeGateway = ExchangeGateway.getInstance(this);
            
            filters.clear();

            // save sql files
            for (Iterator i = exchangeGateway.getFilters().values().iterator(); i.hasNext();)
            {
                ExchangeFilter filter = (ExchangeFilter) i.next();
                String id = filter.getId();
                String eventId = filter.getEventEventId();
                String runbook = filter.getRunbook();
                String order = ""+filter.getOrder();
                String interval = "" + filter.getInterval();
                String active = "" + filter.isActive();
                Map<String, String> criteria = filter.getCriteria();

                if (criteria != null)
                {
                    for (Iterator itr = criteria.entrySet().iterator(); itr.hasNext();)
                    {
                        Map.Entry entry = (Map.Entry) itr.next();
                        String key = (String) entry.getKey();
                        String value = (String) entry.getValue();

                        String fileName = key.toLowerCase();
                        File file = new File(MainBase.main.release.serviceName + "/config/exchange/" + fileName);

                        try
                        {
                            FileUtils.writeStringToFile(file, value, "UTF-8");
                        }
                        catch (Exception exp)
                        {
                            Log.log.warn(exp.getMessage() + ". Skipping file: " + file.getAbsolutePath());
                        }
                    }
                }

                try
                {
                    // add xdoc entry
                    Map entry = new HashMap();
                    entry.put("ID", id);
                    entry.put("EVENT_EVENTID", eventId);
                    entry.put("RUNBOOK", runbook);
                    entry.put("ORDER", order);
                    entry.put("INTERVAL", interval);
                    entry.put("ACTIVE", active);
                    filters.add(entry);

                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage());
                }
            }

            // saveAttributes();

            // filters
            xdoc.setListMapValue("./RECEIVE/EXCHANGE/FILTER", filters);
        }

        saveAttributes();

    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getP_assword()
    {
        return p_assword;
    }

    public void setP_assword(String password)
    {
        this.p_assword = password;
    }

    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public String getDomain()
    {
        return this.domain;
    }

    public void setMailbox(String mailbox)
    {
        this.mailbox = mailbox;
    }

    public String getMailbox()
    {
        return this.mailbox;
    }

    public boolean getEwsmode()
    {
        return this.ewsmode;
    }

    public void setEwsmode(boolean ewsmode)
    {
        this.ewsmode = ewsmode;
    }

    public String getEwsurl()
    {
        return this.ewsurl;
    }

    public void setEwsurl(String ewsurl)
    {
        this.ewsurl = ewsurl;
    }

    public boolean getAcceptAllCerts()
    {
        return this.acceptAllCerts;
    }

    public void setAcceptAllCerts(boolean acceptAllCerts)
    {
        this.acceptAllCerts = acceptAllCerts;
    }

    public void setMapiClientVersion(String mapiClientVersion)
    {
        this.mapiClientVersion = mapiClientVersion;
    }

    public String getMapiClientVersion()
    {
        return this.mapiClientVersion;
    }

    public int getMaxAttachmentSize()
    {
        return this.maxAttachmentSize;
    }

    public void setMaxAttachmentSize(int maxAttachmentSize)
    {
        this.maxAttachmentSize = maxAttachmentSize;
    }

    public int getRetryDelay()
    {
        return retryDelay;
    }

    public void setRetryDelay(int retryDelay)
    {
        this.retryDelay = retryDelay;
    }

    public int getReconnectDelay()
    {
        return reconnectDelay;
    }

    public void setReconnectDelay(int reconnectDelay)
    {
        this.reconnectDelay = reconnectDelay;
    }

    public List getFilters()
    {
        return filters;
    }

    public void setFilters(List filters)
    {
        this.filters = filters;
    }
} // ConfigReceiveExchange
