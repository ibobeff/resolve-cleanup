/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.resolve.gateway.Filter;
import com.resolve.gateway.hpom.HPOMFilter;
import com.resolve.gateway.hpom.HPOMGateway;
import com.resolve.rsbase.MainBase;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for HPOM gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <HPOM....../>
 */
public class ConfigReceiveHPOM extends ConfigReceiveGateway
{

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_HPOM_NODE = "./RECEIVE/HPOM/";
    private static final String RECEIVE_HPOM_FILTER = RECEIVE_HPOM_NODE + "FILTER";
    private static final String RECEIVE_HPOM_PROPERTIES = RECEIVE_HPOM_NODE + "PROPERTIES";

    // HPOM Filter Attributes
    private static final String RECEIVE_HPOM_ATTR_URL = RECEIVE_HPOM_NODE + "@URL";
    private static final String RECEIVE_HPOM_ATTR_USERNAME = RECEIVE_HPOM_NODE + "@USERNAME";
    private static final String RECEIVE_HPOM_ATTR_P_ASSWORD = RECEIVE_HPOM_NODE + "@PASSWORD";

    private static final String RECEIVE_HPOM_ATTR_STATUS = RECEIVE_HPOM_NODE + "/STATUS/";
    private static final String RECEIVE_HPOM_ATTR_STATUS_ACTIVE = RECEIVE_HPOM_ATTR_STATUS + "@ACTIVE";
    private static final String RECEIVE_HPOM_ATTR_STATUS_FIELDNAME = RECEIVE_HPOM_ATTR_STATUS + "@FIELDNAME";
    private static final String RECEIVE_HPOM_ATTR_STATUS_PROCESS = RECEIVE_HPOM_ATTR_STATUS + "@PROCESS";

    private static final String RECEIVE_HPOM_ATTR_RUNBOOKID = RECEIVE_HPOM_NODE + "/RUNBOOKID/";
    private static final String RECEIVE_HPOM_ATTR_RUNBOOKID_ACTIVE = RECEIVE_HPOM_ATTR_RUNBOOKID + "@ACTIVE";
    private static final String RECEIVE_HPOM_ATTR_RUNBOOKID_FIELDNAME = RECEIVE_HPOM_ATTR_RUNBOOKID + "@FIELDNAME";
    
    private static final String RECEIVE_HPOM_ATTR_TIMEOUT = RECEIVE_HPOM_NODE + "@SOCKET_TIMEOUT";

    private boolean active = false;
    private String url = ""; // https://hpom_host:port/opr-webservice
    private int interval = 120;
    private String username = "";
    private String password = "";
    private String queue = "HPOM";
    // private String changeDateField = "CHANGEDATE";

    // update fields - status
    private boolean status_active = false;
    private String status_fieldname = "RESOLVE_STATUS";
    private int status_process = 1;

    // update fields - runbookid
    private boolean runbookid_active = false;
    private String runbookid_fieldname = "RESOLVE_RUNBOOKID";
    
    private int socket_timeout = 30;

    public ConfigReceiveHPOM(XDoc config) throws Exception
    {
        super(config);

        define("url", STRING, RECEIVE_HPOM_ATTR_URL);
        define("username", STRING, RECEIVE_HPOM_ATTR_USERNAME);
        define("password", SECURE, RECEIVE_HPOM_ATTR_P_ASSWORD);

        // custom update fields - status
        define("status_active", BOOLEAN, RECEIVE_HPOM_ATTR_STATUS_ACTIVE);
        define("status_fieldname", STRING, RECEIVE_HPOM_ATTR_STATUS_FIELDNAME);
        define("status_process", INTEGER, RECEIVE_HPOM_ATTR_STATUS_PROCESS);

        // custom update fields - runbookid
        define("runbookid_active", BOOLEAN, RECEIVE_HPOM_ATTR_RUNBOOKID_ACTIVE);
        define("runbookid_fieldname", STRING, RECEIVE_HPOM_ATTR_RUNBOOKID_FIELDNAME);
        
        define("socket_timeout", INTEGER, RECEIVE_HPOM_ATTR_TIMEOUT);

    } // ConfigReceiveHPOM

    public String getRootNode()
    {
        return RECEIVE_HPOM_NODE;
    }

    @SuppressWarnings("unchecked")
    public void load() throws Exception
    {
        try
        {
            loadAttributes();

            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                HPOMGateway hpomGateway = HPOMGateway.getInstance(this);

                // filters
                filters = xdoc.getListMapValue(RECEIVE_HPOM_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(HPOMFilter.ID)).toLowerCase();
                        File scriptFile = new File(MainBase.main.release.serviceName + "/config/hpom/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(HPOMFilter.SCRIPT, groovy);
                                }
                            }

                            // init filter
                            hpomGateway.setFilter(values);

                            Log.log.debug("Loaded groovy script: " + scriptFile);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for HPOM gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    public void save() throws Exception
    {
        // create hpom directory
        File dir = new File(MainBase.main.release.serviceName + "/config/hpom");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }

        // clear list of filters to save
        if (filters != null && this.active)
        {
            filters.clear();

            // save sql files
            for (Filter filter : HPOMGateway.getInstance().getFilters().values())
            {
                HPOMFilter hpomFilter = (HPOMFilter) filter;

                String groovy = hpomFilter.getScript();
                String scriptFileName = hpomFilter.getId().toLowerCase() + ".groovy";
                File scriptFile = FileUtils.getFile(MainBase.main.release.serviceName + "/config/hpom/" + scriptFileName);

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(HPOMFilter.ID, hpomFilter.getId());
                entry.put(HPOMFilter.ACTIVE, String.valueOf(hpomFilter.isActive()));
                entry.put(HPOMFilter.ORDER, String.valueOf(hpomFilter.getOrder()));
                entry.put(HPOMFilter.INTERVAL, String.valueOf(hpomFilter.getInterval()));
                entry.put(HPOMFilter.EVENT_EVENTID, hpomFilter.getEventEventId());
                // entry.put(HPOMFilter.PERSISTENT_EVENT,
                // String.valueOf(hpomFilter.isPersistentEvent()));
                entry.put(HPOMFilter.RUNBOOK, hpomFilter.getRunbook());
                // entry.put(HPOMFilter.SCRIPT, filter.getScript());
                entry.put(HPOMFilter.QUERY, hpomFilter.getQuery());
                entry.put(HPOMFilter.OBJECT, hpomFilter.getObject());
                // entry.put(HPOMFilter.LAST_CHANGE_DATE,
                // HPOMFilter.getLastChangeDate());
                filters.add(entry);

                try
                {
                    // create groovy file
                    FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_HPOM_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && this.active)
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : HPOMGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                Map props = HPOMGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }

    public boolean isStatus_active()
    {
        return status_active;
    }

    public void setStatus_active(boolean status_active)
    {
        this.status_active = status_active;
    }

    public String getStatus_fieldname()
    {
        return status_fieldname;
    }

    public void setStatus_fieldname(String status_fieldname)
    {
        this.status_fieldname = status_fieldname;
    }

    public int getStatus_process()
    {
        return status_process;
    }

    public void setStatus_process(int status_process)
    {
        this.status_process = status_process;
    }

    public boolean isRunbookid_active()
    {
        return runbookid_active;
    }

    public void setRunbookid_active(boolean runbookid_active)
    {
        this.runbookid_active = runbookid_active;
    }

    public String getRunbookid_fieldname()
    {
        return runbookid_fieldname;
    }

    public void setRunbookid_fieldname(String runbookid_fieldname)
    {
        this.runbookid_fieldname = runbookid_fieldname;
    }

	public int getSocket_timeout() {
		return socket_timeout;
	}

	public void setSocket_timeout(int socket_timeout) {
		this.socket_timeout = socket_timeout;
	}
}
