package com.resolve.rsremote;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.gateway.http.HttpFilter;
import com.resolve.gateway.http.HttpGateway;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for HTTP gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <HTTP....../>
 */

public class ConfigReceiveHTTP extends ConfigReceiveGateway
{
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_HTTP_NODE = "./RECEIVE/HTTP/";
    private static final String RECEIVE_HTTP_FILTER = RECEIVE_HTTP_NODE + "FILTER";

    private static final String RECEIVE_HTTP_SERVER_PORT = RECEIVE_HTTP_NODE + "@PORT";
    private static final String RECEIVE_HTTP_SERVER_TIMEOUT = RECEIVE_HTTP_NODE + "@TIMEOUT";
    private static final String RECEIVE_HTTP_SERVER_SSL = RECEIVE_HTTP_NODE + "@SSL";
    private static final String RECEIVE_HTTP_SERVER_SSL_CERTIFICATE = RECEIVE_HTTP_NODE + "@SSLCERTIFICATE";
    private static final String RECEIVE_HTTP_SERVER_SSL_P_ASSWORD = RECEIVE_HTTP_NODE + "@SSLPASSWORD";
    private static final String RECEIVE_HTTP_USERNAME = RECEIVE_HTTP_NODE + "@USERNAME";
    private static final String RECEIVE_HTTP_P_ASSWORD = RECEIVE_HTTP_NODE + "@PASSWORD";
    private static final String RECEIVE_HTTP_AUTH_TYPE = RECEIVE_HTTP_NODE + "@HTTPAUTHTYPE";


   // private static final String RECEIVE_GATEWAY_ATTR_HTTP_AUTH_TYPE = "@PUSHHTTPAUTHTYPE"; 
    
    private boolean active = false;
    private String queue = "HTTP";
    
    private int port = 7090;
    private int timeout = 1200;
    private boolean ssl = false;
    private String sslCertificate = MainBase.main.getProductHome() + "/config/jetty.jks";
    private String sslP_assword = "tomcat";
    private String httpAuthBasicUsername = null;
    private String httpAuthBasicPassword = null;
    private String httpAuthType = null;

    public ConfigReceiveHTTP(XDoc config) throws Exception
    {
        super(config);
        
        define("port", INT, RECEIVE_HTTP_SERVER_PORT);
        define("timeout", INT, RECEIVE_HTTP_SERVER_TIMEOUT);
        define("ssl", BOOLEAN, RECEIVE_HTTP_SERVER_SSL);
        define("sslCertificate", STRING, RECEIVE_HTTP_SERVER_SSL_CERTIFICATE);
        define("sslP_assword", SECURE, RECEIVE_HTTP_SERVER_SSL_P_ASSWORD);
 
        define("httpAuthType", STRING, RECEIVE_HTTP_AUTH_TYPE);

        define("httpAuthBasicUsername", STRING, RECEIVE_HTTP_USERNAME);
        define("httpAuthBasicPassword", SECURE, RECEIVE_HTTP_P_ASSWORD);
        
        
    } // ConfigReceiveHTTP

    public String getRootNode()
    {
        return RECEIVE_HTTP_NODE;
    }

    public void load() throws Exception
    {
        try
        {
            loadAttributes();

            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                HttpGateway httpGateway = HttpGateway.getInstance(this);

                // filters
                filters = xdoc.getListMapValue(RECEIVE_HTTP_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(HttpFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/http/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(HttpFilter.SCRIPT, groovy);
                                }
                            }

                            // init filter
                            httpGateway.setFilter(values);

                            Log.log.debug("Loaded groovy script: " + scriptFile);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for HTTP gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    public void save() throws Exception
    {
        // create servicenow directory
        File dir = getFile(MainBase.main.release.serviceName + "/config/http");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }


        // clear list of filters to save
        if (filters != null)
        {
            filters.clear();

            // save sql files
            for (Filter filter : HttpGateway.getInstance().getFilters().values())
            {
                HttpFilter httpFilter = (HttpFilter) filter;

                String groovy = httpFilter.getScript();
                String scriptFileName = httpFilter.getId().toLowerCase() + ".groovy";
                File scriptFile = getFile(MainBase.main.release.serviceName + "/config/http/" + scriptFileName);

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(HttpFilter.ID, httpFilter.getId());
                entry.put(HttpFilter.ACTIVE, String.valueOf(httpFilter.isActive()));
                entry.put(HttpFilter.ORDER, String.valueOf(httpFilter.getOrder()));
                entry.put(HttpFilter.INTERVAL, String.valueOf(httpFilter.getInterval()));
                entry.put(HttpFilter.EVENT_EVENTID, httpFilter.getEventEventId());
                // entry.put(HttpFilter.PERSISTENT_EVENT,
                // String.valueOf(HttpFilter.isPersistentEvent()));
                entry.put(HttpFilter.RUNBOOK, httpFilter.getRunbook());
                
                entry.put(HttpFilter.URI, httpFilter.getUri());
                entry.put(HttpFilter.PORT, String.valueOf(httpFilter.getPort()));
                entry.put(HttpFilter.SSL, String.valueOf(httpFilter.isSsl()));
                entry.put(HttpFilter.ALLOWED_IP, httpFilter.getAllowedIP());
                
                //GAT-26 and GAT-27
                entry.put(HttpFilter.CONTENT_TYPE, httpFilter.getContentType());
                entry.put(HttpFilter.RESPONSE_FILTER, httpFilter.getResponsefilter());
                
                entry.put(HttpFilter.BASIC_AUTH_UN,  httpFilter.getHttpAuthBasicUsername());
                entry.put(HttpFilter.BASIC_AUTH_PW,  httpFilter.getHttpAuthBasicPassword());
                
                 filters.add(entry);

                try
                {
                    // create groovy file
                    FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_HTTP_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && this.active)
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : HttpGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                Map props = HttpGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    } // isActive

    public void setActive(boolean active)
    {
        this.active = active;
    } // setActive

    public String getQueue()
    {
        return queue;
    } // getQueue

    public void setQueue(String queue)
    {
        this.queue = queue;
    } // setQueue

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }
    
    public int getTimeout()
    {
        return timeout;
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }

    public boolean getSsl()
    {
        return ssl;
    }

    public void setSsl(boolean ssl)
    {
        this.ssl = ssl;
    }

    public String getSslCertificate()
    {
        return sslCertificate;
    }

    public void setSslCertificate(String sslCertificate)
    {
        this.sslCertificate = sslCertificate;
    }

    public String getSslP_assword()
    {
        return sslP_assword;
    }

    public void setSslP_assword(String sslPassword)
    {
        this.sslP_assword = sslPassword;
    }

    public String getHttpAuthBasicUsername()
    {
        return httpAuthBasicUsername;
    }

    public void setHttpAuthBasicUsername(String httpAuthBasicUsername)
    {
        this.httpAuthBasicUsername = httpAuthBasicUsername;
    }

    public String getHttpAuthBasicPassword()
    {
        return httpAuthBasicPassword;
    }

    public void setHttpAuthBasicPassword(String httpAuthBasicPassword)
    {
        this.httpAuthBasicPassword = httpAuthBasicPassword;
    }

    public String getHttpAuthType()
    { 
        
        return httpAuthType;
    }

    public void setHttpAuthType(String httpAuthType)
    {
        this.httpAuthType = httpAuthType;
    }

  
} // ConfigReceiveHTTP
