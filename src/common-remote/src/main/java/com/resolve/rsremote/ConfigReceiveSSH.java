/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.gateway.ssh.SSHGateway;
import com.resolve.gateway.ssh.SubnetMask;
import com.resolve.util.ConfigMap;
import com.resolve.util.Log;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for SSH gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <SSH....../>
 */
public class ConfigReceiveSSH extends ConfigReceiveGateway
{
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_SSH_NODE = "./RECEIVE/SSH/";
    private static final String RECEIVE_SSH_POOL = RECEIVE_SSH_NODE + "POOL";

    private static final String RECEIVE_SSH_ATTR_ACTIVE = RECEIVE_SSH_NODE + "@ACTIVE";
    private static final String RECEIVE_SSH_ATTR_INTERVAL = RECEIVE_SSH_NODE + "@INTERVAL";
    private static final String RECEIVE_SSH_ATTR_QUEUE = RECEIVE_SSH_NODE + "@QUEUE";
    private static final String RECEIVE_SSH_ATTR_MAXCONNECTION = RECEIVE_SSH_NODE + "@MAXCONNECTION";
    private static final String RECEIVE_SSH_ATTR_PORT = RECEIVE_SSH_NODE + "@PORT";
    private static final String RECEIVE_SSH_ATTR_TIMEOUT = RECEIVE_SSH_NODE + "@TIMEOUT";
    private static final String RECEIVE_SSH_ATTR_TIMEOUTCOUNTER = RECEIVE_SSH_NODE + "@TIMEOUTCOUNTER";
    private static final String RECEIVE_SSH_ATTR_EXPIRY = RECEIVE_SSH_NODE + "@EXPIRY";
    private static final String RECEIVE_SSH_ATTR_CLEANUP = RECEIVE_SSH_NODE + "@CLEANUP";
    private static final String RECEIVE_SSH_ATTR_KEEPALIVEINTERVAL = RECEIVE_SSH_NODE + "@KEEPALIVEINTERVAL";

    private boolean active = false;
    private int interval = 120;
    private String queue = "SSH";
    private int maxconnection = 10;
    private int port = 22; // default ssh port
    private long expiry = -1;
    private long cleanup = -1;
    private int keepAliveInterval = -1;
    private int timeout = 120; // seconds
    private int timeoutcounter = 10; // number of times timeout occurs before
                                     // giving warning message.
    List<Map<String, String>> pools;

    public ConfigReceiveSSH(XDoc config) throws Exception
    {
        super(config);

        define("active", BOOLEAN, RECEIVE_SSH_ATTR_ACTIVE);
        define("interval", INTEGER, RECEIVE_SSH_ATTR_INTERVAL);
        define("queue", STRING, RECEIVE_SSH_ATTR_QUEUE);
        define("maxconnection", INTEGER, RECEIVE_SSH_ATTR_MAXCONNECTION);
        define("port", INTEGER, RECEIVE_SSH_ATTR_PORT);
        define("timeout", INTEGER, RECEIVE_SSH_ATTR_TIMEOUT);
        define("timeoutcounter", INTEGER, RECEIVE_SSH_ATTR_TIMEOUTCOUNTER);
        define("expiry", LONG, RECEIVE_SSH_ATTR_EXPIRY);
        define("cleanup", LONG, RECEIVE_SSH_ATTR_CLEANUP);
        define("keepAliveInterval", INTEGER, RECEIVE_SSH_ATTR_KEEPALIVEINTERVAL);
    } // ConfigReceiveSSH

    @SuppressWarnings("unchecked")
    public void load() throws Exception
    {
        try
        {
            loadAttributes();

            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                SSHGateway sshGateway = SSHGateway.getInstance(this);

                // netmasks
                pools = xdoc.getListMapValue(RECEIVE_SSH_POOL);
                if (pools != null && pools.size() > 0)
                {
                    for (Map<String, String> values : pools)
                    {
                        sshGateway.setPool(values);
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for SSH Pool gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }

    public int getMaxconnection()
    {
        return maxconnection;
    }

    public void setMaxconnection(int maxconnection)
    {
        this.maxconnection = maxconnection;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public int getTimeout()
    {
        return timeout;
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }

    public int getTimeoutcounter()
    {
        return timeoutcounter;
    }

    public void setTimeoutcounter(int timeoutcounter)
    {
        this.timeoutcounter = timeoutcounter;
    }
    
    public long getExpiry()
    {
        return expiry;
    }

    public void setExpiry(long expiry)
    {
        this.expiry = expiry;
    }
    
    public long getCleanup()
    {
        return cleanup;
    }

    public void setCleanup(long cleanup)
    {
        this.cleanup = cleanup;
    }

    public int getKeepAliveInterval() {
		return keepAliveInterval;
	}

	public void setKeepAliveInterval(int keepAliveInterval) {
		this.keepAliveInterval = keepAliveInterval;
	}

	@Override
    public void save() throws Exception
    {
        // clear list of pools to save
        if (pools != null && this.active)
        {
            pools.clear();

            for (SubnetMask subnetMask : SSHGateway.getInstance().getPools().values())
            {
                // add xdoc entry
                Map<String, String> entry = new HashMap<String, String>();

                entry.put(SubnetMask.SUBNETMASK, subnetMask.getSubnetMask());
                entry.put(SubnetMask.MAXCONNECTION, String.valueOf(subnetMask.getMaxConnection()));
                entry.put(SubnetMask.TIMEOUT, String.valueOf(subnetMask.getTimeout()));
                pools.add(entry);
            }

            // pools
            xdoc.setListMapValue(RECEIVE_SSH_POOL, pools);
        }

        saveAttributes();
    }
	
	@Override
	public String getRootNode() {
		return RECEIVE_SSH_NODE;
	}
}
