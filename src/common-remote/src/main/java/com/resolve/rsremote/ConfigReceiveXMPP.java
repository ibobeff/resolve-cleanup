/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.gateway.Filter;
import com.resolve.gateway.xmpp.XMPPAddress;
import com.resolve.gateway.xmpp.XMPPFilter;
import com.resolve.gateway.xmpp.XMPPGateway;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for XMPP gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <XMPP....../>
 */
public class ConfigReceiveXMPP extends ConfigReceiveGateway
{
    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_XMPP_NODE = "./RECEIVE/XMPP/";
    private static final String RECEIVE_XMPP_ADDRESS = RECEIVE_XMPP_NODE + "ADDRESS";
    private static final String RECEIVE_XMPP_FILTER = RECEIVE_XMPP_NODE + "FILTER";
    private static final String RECEIVE_XMPP_PROPERTIES = RECEIVE_XMPP_NODE + "PROPERTIES";

    // XMPP Filter Attributes
    private static final String RECEIVE_XMPP_ATTR_SERVER = RECEIVE_XMPP_NODE + "@SERVER";
    private static final String RECEIVE_XMPP_ATTR_PORT = RECEIVE_XMPP_NODE + "@PORT";
    private static final String RECEIVE_XMPP_ATTR_SERVICE = RECEIVE_XMPP_NODE + "@SERVICE";
    private static final String RECEIVE_XMPP_ATTR_USERNAME = RECEIVE_XMPP_NODE + "@USERNAME";
    private static final String RECEIVE_XMPP_ATTR_P_ASSWORD = RECEIVE_XMPP_NODE + "@PASSWORD";
    private static final String RECEIVE_XMPP_ATTR_SASL = RECEIVE_XMPP_NODE + "@SASL";
    private static final String RECEIVE_XMPP_ATTR_SASLMECHANISM = RECEIVE_XMPP_NODE + "@SASLMECHANISM";
    private static final String RECEIVE_XMPP_ATTR_SOCIAL_POST = RECEIVE_XMPP_NODE + "@SOCIALPOST";

    private boolean active = false;
    private int interval = 120;
    private String queue = "XMPP";
    private String server = "";
    private int port = 5222;
    private String service = "";
    private String username = "";
    private String p_assword = "";
    private boolean sasl = true;
    // Valid value: DIGEST-MD5, CRAM-MD5, PLAIN, ANONYMOUS
    private String saslmechanism = "PLAIN";
    private boolean isSocialPost = false;

    List<Map<String, Object>> xmppAddresses;

    public ConfigReceiveXMPP(XDoc config) throws Exception
    {
        super(config);

        define("server", STRING, RECEIVE_XMPP_ATTR_SERVER);
        define("port", INTEGER, RECEIVE_XMPP_ATTR_PORT);
        define("service", STRING, RECEIVE_XMPP_ATTR_SERVICE);
        define("username", STRING, RECEIVE_XMPP_ATTR_USERNAME);
        define("p_assword", SECURE, RECEIVE_XMPP_ATTR_P_ASSWORD);
        define("sasl", BOOLEAN, RECEIVE_XMPP_ATTR_SASL);
        define("saslmechanism", STRING, RECEIVE_XMPP_ATTR_SASLMECHANISM);
        define("socialpost", BOOLEAN, RECEIVE_XMPP_ATTR_SOCIAL_POST);
    } // ConfigReceiveXMPP

    public String getRootNode()
    {
        return RECEIVE_XMPP_NODE;
    }

    @SuppressWarnings("unchecked")
    public void load() throws Exception
    {
        try
        {
            loadAttributes();

            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                XMPPGateway xmppGateway = XMPPGateway.getInstance(this);

                xmppAddresses = xdoc.getListMapValue(RECEIVE_XMPP_ADDRESS);

                if (xmppAddresses.size() > 0)
                {
                    for (Map<String, Object> values : xmppAddresses)
                    {
                        xmppGateway.setXMPPAddresses(values);
                    }
                }

                // filters
                filters = xdoc.getListMapValue(RECEIVE_XMPP_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(XMPPFilter.ID)).toLowerCase();
                        File scriptFile = getFile(MainBase.main.release.serviceName + "/config/xmpp/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(XMPPFilter.SCRIPT, groovy);
                                }
                            }

                            // init filter
                            xmppGateway.setFilter(values);

                            Log.log.debug("Loaded groovy script: " + scriptFile);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for Email gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    public void save() throws Exception
    {
        // create xmpp directory
        File dir = getFile(MainBase.main.release.serviceName + "/config/xmpp");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }

        // clear list of filters to save
        if (xmppAddresses != null && this.active)
        {
            xmppAddresses.clear();

            // save sql files
            for (XMPPAddress filter : XMPPGateway.getInstance().getXMPPAddresses().values())
            {
                XMPPAddress emailFilter = (XMPPAddress) filter;

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(XMPPAddress.XMPPADDRESS, emailFilter.getXmppAddress());
                entry.put(XMPPAddress.XMPPP_ASSWORD, CryptUtils.encrypt(emailFilter.getXmppPassword()));
                xmppAddresses.add(entry);
            }
            xdoc.setListMapValue(RECEIVE_XMPP_ADDRESS, xmppAddresses);
        }

        // clear list of filters to save
        if (filters != null && this.active)
        {
            filters.clear();

            // save sql files
            for (Filter filter : XMPPGateway.getInstance().getFilters().values())
            {
                XMPPFilter xmppFilter = (XMPPFilter) filter;

                String groovy = xmppFilter.getScript();
                String scriptFileName = xmppFilter.getId().toLowerCase() + ".groovy";
                File scriptFile = getFile(MainBase.main.release.serviceName + "/config/xmpp/" + scriptFileName);

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(XMPPFilter.ID, xmppFilter.getId());
                entry.put(XMPPFilter.ACTIVE, String.valueOf(xmppFilter.isActive()));
                entry.put(XMPPFilter.ORDER, String.valueOf(xmppFilter.getOrder()));
                entry.put(XMPPFilter.INTERVAL, String.valueOf(xmppFilter.getInterval()));
                entry.put(XMPPFilter.EVENT_EVENTID, xmppFilter.getEventEventId());
                // entry.put(XMPPFilter.PERSISTENT_EVENT,
                // String.valueOf(xmppFilter.isPersistentEvent()));
                entry.put(XMPPFilter.RUNBOOK, xmppFilter.getRunbook());
                // entry.put(XMPPFilter.SCRIPT, filter.getScript());
                entry.put(XMPPFilter.REGEX, xmppFilter.getRegex());
                filters.add(entry);

                try
                {
                    // create groovy file
                    FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_XMPP_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && this.active)
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : XMPPGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                // overwrite properties file
                Map props = XMPPGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getP_assword()
    {
        return p_assword;
    }

    public void setP_assword(String password)
    {
        this.p_assword = password;
    }

    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }

    public String getServer()
    {
        return server;
    }

    public void setServer(String server)
    {
        this.server = server;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public String getService()
    {
        return service;
    }

    public void setService(String service)
    {
        this.service = service;
    }

    public boolean isSasl()
    {
        return sasl;
    }

    public void setSasl(boolean sasl)
    {
        this.sasl = sasl;
    }

    public String getSaslmechanism()
    {
        return saslmechanism;
    }

    public void setSaslmechanism(String saslmechanism)
    {
        this.saslmechanism = saslmechanism;
    }

    public boolean isSocialpost()
    {
        return isSocialPost;
    }

    public void setSocialpost(boolean isSocialPost)
    {
        this.isSocialPost = isSocialPost;
    }
} // ConfigReceiveXMPP
