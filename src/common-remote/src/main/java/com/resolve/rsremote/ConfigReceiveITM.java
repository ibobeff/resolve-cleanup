/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import com.resolve.util.ConfigMap;
import com.resolve.util.Log;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for ITM gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <ITM....../>
 */
public class ConfigReceiveITM extends ConfigMap
{

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_ITM_NODE = "./RECEIVE/ITM/";

    // ITM Gateway Attributes
    private static final String RECEIVE_ITM_ATTR_ACTIVE = RECEIVE_ITM_NODE + "@ACTIVE";
    private static final String RECEIVE_ITM_ATTR_SERVER = RECEIVE_ITM_NODE + "@SERVER";
    private static final String RECEIVE_ITM_ATTR_CMD = RECEIVE_ITM_NODE + "@CMD";
    private static final String RECEIVE_ITM_ATTR_USERNAME = RECEIVE_ITM_NODE + "@USERNAME";
    private static final String RECEIVE_ITM_ATTR_P_ASSWORD = RECEIVE_ITM_NODE + "@PASSWORD";
    private static final String RECEIVE_ITM_ATTR_LOGINDURATION = RECEIVE_ITM_NODE + "@LOGINDURATION";
    private static final String RECEIVE_ITM_ATTR_TIMEOUT = RECEIVE_ITM_NODE + "@TIMEOUT";
    private static final String RECEIVE_ITM_ATTR_SCRIPTDIR = RECEIVE_ITM_NODE + "@SCRIPTDIR";
    private static final String RECEIVE_ITM_ATTR_OUTPUTDIR = RECEIVE_ITM_NODE + "@OUTPUTDIR";
    private static final String RECEIVE_ITM_ATTR_REMOTE_DIR_WINDOWS = RECEIVE_ITM_NODE + "@REMOTE_WINDOWS_DIR";
    private static final String RECEIVE_ITM_ATTR_REMOTE_DIR_UNIX = RECEIVE_ITM_NODE + "@REMOTE_UNIX_DIR";
    private static final String RECEIVE_ITM_ATTR_DELETEOUTPUTS = RECEIVE_ITM_NODE + "@DELETEOUTPUTS";

    private boolean active = false;
    private String server = "";
    private String cmd = "";
    private String username = "";
    private String password = "";
    private int loginDuration = 1440;
    private int timeout = 1200;
    private String scriptDir = "";
    private String outputDir = "";
    private String remoteWindowsDir = "";
    private String remoteUnixDir = "";
    private boolean deleteOutputs = true;

    public ConfigReceiveITM(XDoc config) throws Exception
    {
        super(config);

        define("active", BOOLEAN, RECEIVE_ITM_ATTR_ACTIVE);
        define("server", STRING, RECEIVE_ITM_ATTR_SERVER);
        define("cmd", STRING, RECEIVE_ITM_ATTR_CMD);
        define("username", STRING, RECEIVE_ITM_ATTR_USERNAME);
        define("password", SECURE, RECEIVE_ITM_ATTR_P_ASSWORD);
        define("loginduration", INT, RECEIVE_ITM_ATTR_LOGINDURATION);
        define("timeout", INT, RECEIVE_ITM_ATTR_TIMEOUT);
        define("scriptDir", STRING, RECEIVE_ITM_ATTR_SCRIPTDIR);
        define("outputDir", STRING, RECEIVE_ITM_ATTR_OUTPUTDIR);
        define("remoteWindowsDir", STRING, RECEIVE_ITM_ATTR_REMOTE_DIR_WINDOWS);
        define("remoteUnixDir", STRING, RECEIVE_ITM_ATTR_REMOTE_DIR_UNIX);
        define("deleteOutputs", BOOLEAN, RECEIVE_ITM_ATTR_DELETEOUTPUTS);
    } // ConfigReceiveITM

    public void load() throws Exception
    {
        try
        {
            loadAttributes();
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for ITM gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    public void save() throws Exception
    {
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getCmd()
    {
        return cmd;
    }

    public String getServer()
    {
        return server;
    }

    public void setServer(String server)
    {
        this.server = server;
    }

    public void setCmd(String cmd)
    {
        this.cmd = cmd;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public int getLoginduration()
    {
        return loginDuration;
    }

    public void setLoginduration(int loginDuration)
    {
        this.loginDuration = loginDuration;
    }

    public int getTimeout()
    {
        return timeout;
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }

    public String getScriptDir()
    {
        return scriptDir;
    }

    public void setScriptDir(String scriptDir)
    {
        this.scriptDir = scriptDir;
    }

    public String getOutputDir()
    {
        return outputDir;
    }

    public void setOutputDir(String outputDir)
    {
        this.outputDir = outputDir;
    }

    public String getRemoteWindowsDir()
    {
        return remoteWindowsDir;
    }

    public void setRemoteWindowsDir(String remoteWindowsDir)
    {
        this.remoteWindowsDir = remoteWindowsDir;
    }

    public String getRemoteUnixDir()
    {
        return remoteUnixDir;
    }

    public void setRemoteUnixDir(String remoteUnixDir)
    {
        this.remoteUnixDir = remoteUnixDir;
    }

    public boolean isDeleteOutputs()
    {
        return deleteOutputs;
    }

    public void setDeleteOutputs(boolean deleteOutputs)
    {
        this.deleteOutputs = deleteOutputs;
    }

} // ConfigReceiveITM
