/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.rsremote;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.resolve.gateway.Filter;
import com.resolve.gateway.hpsm.HPSMFilter;
import com.resolve.gateway.hpsm.HPSMGateway;
import com.resolve.rsbase.MainBase;
import com.resolve.util.FileUtils;
import com.resolve.util.Log;
import com.resolve.util.ObjectProperties;
import com.resolve.util.StringUtils;
import com.resolve.util.XDoc;

/**
 * This class loads up the configuration for HPSM gateway. It reads the
 * configuration file from rsremote/config/config.xml. The element of interest
 * is:
 *
 * <HPSM....../>
 */
public class ConfigReceiveHPSM extends ConfigReceiveGateway
{

    private static final long serialVersionUID = 1L;

    private static final String RECEIVE_HPSM_NODE = "./RECEIVE/HPSM/";
    private static final String RECEIVE_HPSM_FILTER = RECEIVE_HPSM_NODE + "FILTER";
    private static final String RECEIVE_HPSM_PROPERTIES = RECEIVE_HPSM_NODE + "PROPERTIES";

    // HPSM Filter Attributes
    private static final String RECEIVE_HPSM_ATTR_URL = RECEIVE_HPSM_NODE + "@URL";
    private static final String RECEIVE_HPSM_ATTR_USERNAME = RECEIVE_HPSM_NODE + "@USERNAME";
    private static final String RECEIVE_HPSM_ATTR_P_ASSWORD = RECEIVE_HPSM_NODE + "@PASSWORD";

    private boolean active = false;
    private String url = ""; // "http://hpsm_host:port/SM/7/ws";
    private int interval = 120;
    private String username = "";
    private String password = "";
    private String queue = "HPSM";
    // private String changeDateField = "CHANGEDATE";

    public ConfigReceiveHPSM(XDoc config) throws Exception
    {
        super(config);

        define("url", STRING, RECEIVE_HPSM_ATTR_URL);
        define("username", STRING, RECEIVE_HPSM_ATTR_USERNAME);
        define("password", SECURE, RECEIVE_HPSM_ATTR_P_ASSWORD);
    } // ConfigReceiveHPSM

    public String getRootNode()
    {
        return RECEIVE_HPSM_NODE;
    }

    @SuppressWarnings("unchecked")
    public void load() throws Exception
    {
        try
        {
            loadAttributes();

            if (isActive()) // no need to unnecessarily load anything if the gateway
                            // is inactive.
            {
                HPSMGateway hpsmGateway = HPSMGateway.getInstance(this);

                // filters
                filters = xdoc.getListMapValue(RECEIVE_HPSM_FILTER);

                // initialize filters
                if (filters.size() > 0)
                {
                    for (Map<String, Object> values : filters)
                    {
                        //it's important to make it lowercase because we store the files in lowercase
                        //without lowering it, linux systems have problem of not reading the script content.
                        String filterName = ((String)values.get(HPSMFilter.ID)).toLowerCase();
                        File scriptFile = new File(MainBase.main.release.serviceName + "/config/hpsm/" + filterName + ".groovy");

                        try
                        {
                            if (scriptFile.exists())
                            {
                                String groovy = FileUtils.readFileToString(scriptFile, "UTF-8");
                                if (StringUtils.isNotBlank(groovy))
                                {
                                    values.put(HPSMFilter.SCRIPT, groovy);
                                }
                            }

                            // init filter
                            hpsmGateway.setFilter(values);

                            Log.log.debug("Loaded groovy script: " + scriptFile);
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                        }
                    }
                }
            }
        }
        catch (Throwable e)
        {
            //deactivate it
            setActive(false);
            Log.log.warn("Couldn not load configuration for HPSM gateway, check blueprint. " + e.getMessage(), e);
        }
    } // load

    public void save() throws Exception
    {
        // create hpsm directory
        File dir = new File(MainBase.main.release.serviceName + "/config/hpsm");
        if (!dir.exists())
        {
            FileUtils.forceMkdir(dir);
        }

        // clear list of filters to save
        if (filters != null && this.active)
        {
            filters.clear();

            // save sql files
            for (Filter filter : HPSMGateway.getInstance().getFilters().values())
            {
                HPSMFilter hpsmFilter = (HPSMFilter) filter;

                String groovy = hpsmFilter.getScript();
                String scriptFileName = hpsmFilter.getId().toLowerCase() + ".groovy";
                File scriptFile = FileUtils.getFile(MainBase.main.release.serviceName + "/config/hpsm/" + scriptFileName);

                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();

                entry.put(HPSMFilter.ID, hpsmFilter.getId());
                entry.put(HPSMFilter.ACTIVE, String.valueOf(hpsmFilter.isActive()));
                entry.put(HPSMFilter.ORDER, String.valueOf(hpsmFilter.getOrder()));
                entry.put(HPSMFilter.INTERVAL, String.valueOf(hpsmFilter.getInterval()));
                entry.put(HPSMFilter.EVENT_EVENTID, hpsmFilter.getEventEventId());
                // entry.put(HPSMFilter.PERSISTENT_EVENT,
                // String.valueOf(hpsmFilter.isPersistentEvent()));
                entry.put(HPSMFilter.RUNBOOK, hpsmFilter.getRunbook());
                // entry.put(HPSMFilter.SCRIPT, filter.getScript());
                entry.put(HPSMFilter.QUERY, hpsmFilter.getQuery());
                entry.put(HPSMFilter.OBJECT, hpsmFilter.getObject());
                // entry.put(HPSMFilter.LAST_CHANGE_DATE,
                // HPSMFilter.getLastChangeDate());
                filters.add(entry);

                try
                {
                    // create groovy file
                    FileUtils.writeStringToFile(scriptFile, groovy, "UTF-8");
                }
                catch (Exception e)
                {
                    Log.log.warn(e.getMessage() + ". Skipping file: " + scriptFile.getAbsolutePath());
                }
            }

            // filters
            xdoc.setListMapValue(RECEIVE_HPSM_FILTER, filters);
        }

        // clear list of properties to save
        if (nameProperties != null && this.active)
        {
            // empty properties
            nameProperties.clear();

            // recreate properties
            for (String name : HPSMGateway.getInstance().getNameProperties().keySet())
            {
                // add xdoc entry
                Map<String, Object> entry = new HashMap<String, Object>();
                entry.put(NAME_KEY, name);
                nameProperties.add(entry);

                // overwrite properties file
                Map props = HPSMGateway.getInstance().getNameProperties().getNameProperties(name);
                ObjectProperties prop = new ObjectProperties(props);
                prop.save(props);
            }
        }

        saveAttributes();
    } // save

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public int getInterval()
    {
        return interval;
    }

    public void setInterval(int interval)
    {
        this.interval = interval;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getQueue()
    {
        return queue;
    }

    public void setQueue(String queue)
    {
        this.queue = queue;
    }
}
