package com.resolve.services.hibernate.actiontask;

import java.util.List;

import com.resolve.persistence.model.ActionTaskArchive;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.constants.RightTypeEnum;

public enum ActionTaskArchiveService {

	INSTANCE;

	private ActionTaskService actionTaskService = ActionTaskService.INSTANCE;
	private ActionTaskArchiveRepository actionTaskArchiveRepository = ActionTaskArchiveRepository.INSTANCE;

	public List<ActionTaskArchive> getActionTaskArchiveHistory(String docId, String username) {
		try {
			HibernateUtil.beginTransaction(username);
			
			actionTaskService.validateActionTaskUserPermissions(docId, username, RightTypeEnum.view);
			List<ActionTaskArchive> history = actionTaskArchiveRepository.findAll(docId, username);
			
			HibernateUtil.commitTransaction();
			return history;
		} catch (Exception e) {
			HibernateUtil.rollbackTransaction();
			HibernateUtil.rethrowNestedTransaction(e);
			throw e;
		}

	}

}