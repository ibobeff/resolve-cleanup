package com.resolve.services.hibernate.actiontask;

import java.util.List;

import com.resolve.persistence.model.ActionTaskArchive;
import com.resolve.persistence.util.HibernateUtil;

public enum ActionTaskArchiveRepository {

	INSTANCE;

	public List<ActionTaskArchive> findAll(String docId, String username) {
		return HibernateUtil.getSessionFactory().getCurrentSession()
				.createQuery("FROM ActionTaskArchive AS a WHERE a.UTableId = :docId ORDER BY a.UVersion DESC", ActionTaskArchive.class)
				.setParameter("docId", docId)
				.list();
	}

}
