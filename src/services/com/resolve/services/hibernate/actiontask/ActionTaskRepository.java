package com.resolve.services.hibernate.actiontask;

import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.util.HibernateUtil;

public enum ActionTaskRepository {

	INSTANCE;

	public String findActionTaskName(String id) {
		return (String) HibernateUtil.getSessionFactory().getCurrentSession()
				.createQuery("SELECT at.UFullName FROM ResolveActionTask AS at WHERE at.sys_id = :id")
				.setParameter("id", id)
				.uniqueResult();
	}

	public ResolveActionTask findOne(String id) {
		return HibernateUtil.getSessionFactory().getCurrentSession().find(ResolveActionTask.class, id);
	}

}