package com.resolve.services.hibernate.accessrights;

import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.util.HibernateUtil;

public enum AccessRightsRepository {

	INSTANCE;

	public void save(AccessRights entity) {
		HibernateUtil.getSessionFactory().getCurrentSession().persist(entity);
	}

}