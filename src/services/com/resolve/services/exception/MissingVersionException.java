package com.resolve.services.exception;

/**
 * Thrown in case a request for a particular version of an entity is missing.
 * 
 * @author Martin Toshev
 */
public class MissingVersionException extends Exception {

	public MissingVersionException(String msg) {
		super(msg);
	}
}
