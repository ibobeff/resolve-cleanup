/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.social;


import com.resolve.services.ServiceGraph;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.DecisionTree;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Namespace;
import com.resolve.services.graph.social.model.component.PlaybookTemplate;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.Runbook;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.Worksheet;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class LookupComponent
{
    public static RSComponent getRSComponentById(String sysid)
    {
        RSComponent rscomp = null;

        try
        {
            if(StringUtils.isNotEmpty(sysid))
            {
                ResolveNodeVO node = ServiceGraph.findNode(null, sysid, null, null, "system");
                if(node != null)
                {
                    
                    NodeType nodeType = node.getUType();

                    if (nodeType == NodeType.PROCESS)
                    {
                        rscomp = new Process(node);
                    }
                    else if (nodeType == NodeType.TEAM)
                    {
                        rscomp = new Team(node);
                    }
                    else if (nodeType == NodeType.FORUM)
                    {
                        rscomp = new Forum(node);
                    }
                    else if (nodeType == NodeType.USER)
                    {
                        rscomp = new User(node);
                    }
                    else if (nodeType == NodeType.RSS)
                    {
                        rscomp = new Rss(node);
                    }
                    else if (nodeType == NodeType.ACTIONTASK)
                    {
                        rscomp = new ActionTask(node);
                    }
                    else if (nodeType == NodeType.RUNBOOK)
                    {
                        rscomp = new Runbook(node);
                    }
                    else if (nodeType == NodeType.DOCUMENT)
                    {
                        rscomp = new Document(node);
                    }
                    else if (nodeType == NodeType.DECISIONTREE)
                    {
                        rscomp = new DecisionTree(node);
                    }
                    else if (nodeType == NodeType.WORKSHEET)
                    {
                        rscomp = new Worksheet(node);
                    }
                    else if (nodeType == NodeType.NAMESPACE)
                    {
                        rscomp = new Namespace(node);
                    }
                    else if (nodeType == NodeType.PLAYBOOK_TEMPLATE)
                    {
                        rscomp = new PlaybookTemplate(node);
                    }
                }
            }
        }
        catch(Exception exp)
        {
            Log.log.info(exp, exp);
        }

        return rscomp;
    }
}
