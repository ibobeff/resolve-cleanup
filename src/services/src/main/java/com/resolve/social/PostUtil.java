/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.social;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;

import com.resolve.dto.UploadFileDTO;
import com.resolve.notification.NotificationAPI;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.rsbase.MainBase;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.SocialComment;
import com.resolve.search.model.SocialPost;
import com.resolve.search.model.SocialPostResponse;
import com.resolve.search.model.SocialTarget;
import com.resolve.search.model.UserInfo;
import com.resolve.search.social.SocialIndexAPI;
import com.resolve.search.social.SocialSearchAPI;
import com.resolve.services.CustomTableAPI;
import com.resolve.services.ServiceGraph;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceSocial;
import com.resolve.services.ServiceTag;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.ComponentNotification;
import com.resolve.services.graph.social.model.NotificationConfig;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Namespace;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.graph.social.model.component.tree.AdvanceTree;
import com.resolve.services.hibernate.util.ExecuteRunbook;
import com.resolve.services.hibernate.util.GraphUtil;
import com.resolve.services.hibernate.util.StoreUtility;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.hibernate.vo.SocialPostAttachmentVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.interfaces.SocialDTO;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.util.UserUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class PostUtil
{
    public static final String POST_SYS_ID = "postSysId";
    public static final String INTERNAL_POST_MEMBER_ROLE = "postmrole";
    public static final String INTERNAL_POST_RSS_ROLE = "postrssrole";

    public static final String CONTENT = "content";
    
    private String postid = null;
    private PostProcessEngine postEngine = null;
    private List<RSComponent> mentions = null;

    //TODO DEMO code start
    //Following is for a demo only, should be removed later
    private static final Map<String, String[]> demoDataMap = new HashMap<String, String[]>();
    
    static 
    {
        //sample data: text1|=|userid1|#|comment1|&|text2|=|userid2|#|comment2|&|
        //note that the user ids must be valid in resolve
        String demoData = PropertiesUtil.getPropertyString("social.autocomment.demo.data");
        
        if (StringUtils.isNotBlank(demoData))
        {
            String[] splitData = demoData.split("\\|&\\|");
            for (String data : splitData)
            {
                if (StringUtils.isNotBlank(data))
                {
                    String[] splitItem = data.split("\\|=\\|");
                    demoDataMap.put(splitItem[0].toLowerCase(), splitItem[1].split("\\|#\\|"));
                }
            }
        }
    }
    //DEMO code ends
    
    public PostUtil(Map<String, Object> params)
    {
        if (params != null)
        {
            this.postid = params.get(POST_SYS_ID) != null ? (String) params.get(POST_SYS_ID) : null;
        }
    }

    public PostUtil()
    {
    }

    private static final UserInfo getUserInfo(User user) throws Exception
    {
        if (user == null || StringUtils.isBlank(user.getSys_id()) || StringUtils.isBlank(user.getName()))
        {
            throw new Exception("Invalid user object.");
        }

        UserInfo result = new UserInfo(user.getSys_id(), user.getSys_id(), user.getName(), user.getRoles() + " " + INTERNAL_POST_MEMBER_ROLE, isAdminUser(user));
        return result;
    }

    /**
     * This method is copied from old SocialUtil.
     *
     * @param user
     * @return
     */
    private static boolean isAdminUser(User user)
    {
        boolean isAdmin = false;

        try
        {
            String csvUserRoles = user.getRoles().replaceAll(" ", ",").replaceAll("_", "&");
            Set<String> userRoles = new HashSet<String>(StringUtils.convertStringToList(csvUserRoles, ","));
            if (userRoles.contains(UserUtils.ADMIN))
            {
                isAdmin = true;
            }
        }
        catch (Throwable t)
        {
            Log.log.error("error validating if the social user is admin or not.", t);
        }

        return isAdmin;
    }

    public void post(String post, String username, String targetSysId, String compType)
    {
        postWithTitle(post, "", username, targetSysId, compType);

    }// post

    public void postWithTitle(String post, String title, String username, String targetSysId, String compType)
    {
        List<String> targetSysIds = new ArrayList<String>();
        targetSysIds.add(targetSysId);
        RSComponent temp = LookupComponent.getRSComponentById(targetSysId);
        if (temp != null && SearchConstants.SOCIALPOST_TYPE_RSS.equalsIgnoreCase(temp.getType().name()))
        {
            post(title, post, username, targetSysIds, SearchConstants.SOCIALPOST_TYPE_RSS);
        }
        else
        {
            post(title, post, username, targetSysIds, null);
        }
    }// post

    // for any component
    public void post(String subject, String content, String username, List<String> targetsSysId)
    {
        post(subject, content, username, targetsSysId, null);
    }

    public void post(String subject, String content, String username, List<String> targetsSysId, String postType)
    {
        post(subject, content, username, targetsSysId, postType, false);
    }
    
    public void post(String subject, String content, String username, List<String> targetsSysId, String postType, 
    		boolean parseTag) {
        if (StringUtils.isEmpty(content))
        {
            throw new RuntimeException("Please enter some text to submit as a Post.");
        }

        User user = ServiceSocial.findUserByUsername(username);

        if(targetsSysId.contains("system"))
        {
            //this is a very special case where an administrator posted a system message
            targetsSysId.clear();
            targetsSysId.add(user.getSys_id());
            postType = SearchConstants.SOCIALPOST_TYPE_MANUAL;
            //post(subject, content, username, targetsSysId, SearchConstants.SOCIALPOST_TYPE_MANUAL);
        }

        postEngine = new PostProcessEngine(content, parseTag);

        // process mentions
        mentions = postEngine.getPostUserMentions();
        if(mentions != null)
        {
            for(RSComponent component : mentions)
            {
                targetsSysId.add(component.getSys_id());
            }
        }

        // encode post
        content = postEngine.getModifiedPostOrComment();

        // set of roles - cumulative of all the targets
        TreeSet<String> roles = new TreeSet<String>();
        roles.add("admin");

        // set of edit roles - cumulative of all the targets
        TreeSet<String> editRoles = new TreeSet<String>();
        editRoles.add("admin");

        // set of edit roles - cumulative of all the targets
        TreeSet<String> postRoles = new TreeSet<String>();
        postRoles.add("admin");

        // list of targets
        List<RSComponent> targets = new ArrayList<RSComponent>();
        StringBuilder errors = new StringBuilder();
        for (String targetSysId : targetsSysId)
        {
            RSComponent temp = LookupComponent.getRSComponentById(targetSysId);
            
            boolean valid = false;
            try
            {
                if(StringUtils.isNotBlank(postType) && SearchConstants.SOCIALPOST_TYPE_SYSTEM.equalsIgnoreCase(postType))
                {
                    valid = true;
                }
                else
                {
                    //this condition makes sure even if a user is locked someone else could
                    //send message to her Inbox.
                    if(NodeType.USER.name().equals(temp.getType()))
                    {
                        valid = !temp.getName().equals(username);
                    }
                    
                    if(!valid)
                    {
                        valid = isValidTarget(temp, user);
                    }
                }
            }
            catch (Exception e)
            {
                errors.append(e.getMessage()).append("<br>");
            }
            

            // validate the target
            if (valid)
            {
                targets.add(temp);

                // roles
                String targetRoles = temp.getRoles();
                if (StringUtils.isNotBlank(targetRoles))
                {
                    String[] streamRoles = targetRoles.split(" ");
                    for (String s : streamRoles)
                    {
                        roles.add(s);
                    }
                }

                // edit roles
                String targetEditRoles = temp.getEditRoles();
                if (StringUtils.isNotBlank(targetEditRoles))
                {
                    String[] editRolesArr = targetEditRoles.split(" ");
                    for (String s : editRolesArr)
                    {
                        editRoles.add(s);
                    }
                }

                // post roles
                String targetPostRoles = temp.getPostRoles();
                if (StringUtils.isNotBlank(targetPostRoles))
                {
                    String[] postRolesArr = targetPostRoles.split(" ");
                    for (String s : postRolesArr)
                    {
                        postRoles.add(s);
                    }
                }
            }
        }// end of for loop

        if (targets != null && targets.size() > 0)
        {
            if(StringUtils.isNotEmpty(subject) && subject.length() > 100)
            {
                subject = subject.substring(0, 97) + "...";
                
                //massage the subject
                subject = massageSubject(subject);
            }
            
            SocialPost post = SearchUtils.prepareSocialPost(null, content, user);
            post.setReadRoles(roles);
            post.setEditRoles(editRoles);
            post.setPostRoles(postRoles);
            post.setTitle(subject);
            post.setType(postType);
            // it's natural that the author reads his/her post before
            // submitting.
            post.setNumberOfReads(post.getNumberOfReads() + 1);
            post.addReadUsername(user.getName());

            // create post
            try
            {
                indexPost(post, targets, user, postType);
                if(parseTag)
                {
                    verifyTag(post.getContent(), username);
                }
                
                // update the flag if the post has any attachments
                if (StringUtils.isNotEmpty(postid))
                {
                    // this we can decouple by sending a JMS message to do the
                    // update
                    // ****** WE CAN REFACTOR THIS
                    try
                    {
                        ServiceHibernate.updateSocialPostAttachmentActiveFlag(postid, true, username);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("error in updating the flag for postId :" + postid, e);
                    }
                }
                
                //forward email or IM
                notify(user, targets, post, null);
                
            }
            catch (Exception e)
            {
                Log.log.error("error doing Post by user:" + username, e);
            }
        }
        
        if(errors.length() > 0)
        {
            throw new RuntimeException(errors.toString());
        }

    }

    private void notify(final User user, final List<RSComponent> targets, final SocialPost post, 
    		final SocialComment comment) throws Exception
    {
        String content = null;
        long createdOn = post.getSysCreatedOn();
        if(comment != null && StringUtils.isNotBlank(comment.getContent()))
        {
            content = comment.getContent();
            createdOn = comment.getSysCreatedOn();
        }
        else
        {
            content = post.getContent();
        }
        
        //send email/IM notification
        for(RSComponent component : targets)
        {
            Set<String> socialUsernames = new TreeSet<String>();
            Collection<User> forwardedDirectlyToUsers = Collections.emptyList();
            
            Map<String, String> params = new HashMap<String, String>();
            params.put("sys_id", component.getSys_id());
            params.put("type", component.getType().name());
            params.put(CONTENT, getNotificationContent(user, post, content, createdOn, component).toString());
            
            if("process".equalsIgnoreCase(component.getType().name())
                            || "forum".equalsIgnoreCase(component.getType().name())
                            || "team".equalsIgnoreCase(component.getType().name()))
            {
                forwardedDirectlyToUsers = forwardToSocialComponent(params);
            }
            
            /*
             * This section of code forwards email to users who configures
             * notification settings. 
             */
            if(component.getType() == NodeType.USER)
            {
                //For inbox/system posts get the Global User Notifications
                if(post.isInbox(component.getSys_id()) || post.isSystemMessage())
                {
                    ComponentNotification compNotf = ServiceSocial.getSpecificNotificationFor("all", (User)component);
                    
                    if(compNotf.getNotifyTypes() != null && !compNotf.getNotifyTypes().isEmpty())
                    {
                        for(NotificationConfig notfCfg : compNotf.getNotifyTypes())
                        {
                            boolean boolValue = notfCfg.getValue().equalsIgnoreCase("true") ? true : false;
                            
                            switch(notfCfg.getType().name())
                            {
                                case "USER_FORWARD_EMAIL":
                                    component.setInboxForwardEmail(boolValue);
                                    break;
                                    
                                case "USER_DIGEST_EMAIL":
                                    component.setInboxDigestEmail(boolValue);
                                    break;
                                    
                                case "USER_SYSTEM_FORWARD_EMAIL":
                                    component.setSystemForwardEmail(boolValue);
                                    break;
                                    
                                case "USER_SYSTEM_DIGEST_EMAIL":
                                    component.setSystemDigestEmail(boolValue);
                                    break;
                                    
                                case "USER_ALL_FORWARD_EMAIL":
                                    component.setAllForwardEmail(boolValue);
                                    break;
                                    
                                case "USER_ALL_DIGEST_EMAIL":
                                    component.setAllDigestEmail(boolValue);
                                break;      
                            }                                
                        }
                    }
                }
                
                if(post.isInbox(component.getSys_id()) && component.isInboxForwardEmail())
                {
                    socialUsernames.add(component.getName());
                }
                
                if(post.isSystemMessage() && component.isSystemForwardEmail())
                {
                    socialUsernames.add(component.getName());
                }
                
                //this is for various other social components.
                Set<User> socialUsers = ServiceSocial.getUsersForForwardEmail(component, user.getName());
                for(User socialUser: socialUsers)
                {
                    socialUsernames.add(socialUser.getName());
                }
            }
            else
            {
                //this is for various other social components.
                Set<User> socialUsers = ServiceSocial.getUsersForForwardEmail(component, user.getName());
                for(User socialUser: socialUsers)
                {
                    socialUsernames.add(socialUser.getName());
                }
                
                //special case for Namespaces
                sendNotificationToNamespace(component, post, comment, socialUsernames, user.getName());
            }
            
            if(socialUsernames.size() > 0)
            {
                List<User> users = new ArrayList<User>();
                for(String username : socialUsernames)
                {
                    User socialUser = ServiceSocial.findUserByUsername(username);
                    if(socialUser != null)
                    {
                        users.add(socialUser);
                    }
                }
                
                users.removeAll(forwardedDirectlyToUsers);
                String sysId = (String) params.get("sys_id");
                String type = (String) params.get("type");
                // String content = (String) params.get("content");
                Map<String, String> contentMap = StringUtils.stringToMap((String) params.get(CONTENT));

                if (!users.isEmpty())
                {
                    sendNotificationToUsers("User: " + component.getDisplayName(), type, contentMap, null, null, users);
                }
                //forwardEmail(component, post, comment, socialUsernames);
            }
        }
    }

    private static void sendNotificationToNamespace(RSComponent component, SocialPost post, SocialComment comment, 
    		Set<String> socialUsers, String username) throws Exception
    {
        if(post != null && socialUsers != null)
        {
            //if the comp is of type Document, check for the Namespace setting also for that Document if anyone has registered to the Namespace Email Digest
            if(component instanceof Document || component.getType() == (NodeType.DOCUMENT))
            {
                try
                {
                    StringBuilder content = getEmailContent(component, post, comment);    

                    String ns = component.getDisplayName().split("\\.")[0];
                    RSComponent namespace = LookupComponent.getRSComponentById(ns.toLowerCase());
                    
                    if(namespace != null)
                    {
                    	//Updating the RSComponent sys_id from name space name to resolve node guid
                    	namespace.setSys_id(((Namespace)namespace).getNodeSysId());
                    
                        Set<User> socialUsersForNS = ServiceSocial.getUsersForForwardEmail(namespace, username);
                        for(User socialUser: socialUsersForNS)
                        {
                            String nsUser = socialUser.getName();
                            
                            //send email/IM only to people who are following the Namespace and not that specific document. Else they will get 2 emails for the same post
                            if(!socialUsers.contains(nsUser))
                            {
                                NotificationAPI.sendToUser(socialUser.getName(), post.getTitle(), content.toString(), null);
                                NotificationAPI.sendIMToUser(socialUser.getName(), null, content.toString());
                            }
                        }
                    }
                }
                catch (Throwable e)
                {
                    //ignore the exception and continue as notification is lower priority as compared to Post
                    Log.log.info("Error in sending the Namespace Notification", e);
                }                
            }
            
        }
    }

    public void addComment(String username, String postId, String commentStr) throws Exception
    {
        addComment(username, postId, commentStr, false);
    }
    
    public void addComment(String username, String postId, String commentStr, boolean parseTag)
    		throws Exception {
        if (StringUtils.isEmpty(commentStr))
        {
            throw new RuntimeException("Please enter some text to submit as a Comment.");
        }

        User user = ServiceSocial.findUserByUsername(username);

        SocialPostResponse post = ServiceSocial.getPostById(user, postId);

        postEngine = new PostProcessEngine(commentStr, parseTag);

        // encode post
        commentStr = postEngine.getModifiedPostOrComment();

        // create the Post comment
        SocialComment comment = SearchUtils.prepareSocialComment(null, postId, post.getAuthorUsername(), commentStr, user);
        
        // process mentions
        mentions = postEngine.getPostUserMentions();
        SocialIndexAPI.indexSocialComment(comment, username, com.resolve.search.SearchConstants.SOCIALPOST_TYPE_POST);
        if(parseTag)
        {
            verifyTag(comment.getContent(), username);
        }

        //if there are mentions then update the post
        List<RSComponent> targets = new ArrayList<RSComponent>();
        if(mentions != null)
        {
            TreeSet<SocialTarget> newTargets = getSocialTargets(mentions);
            SocialIndexAPI.addNewTargets(postId, newTargets, username);
            
            targets.addAll(mentions);
        }

        for(SocialTarget socialTarget : post.getTargets())
        {
            targets.add(LookupComponent.getRSComponentById(socialTarget.getSysId()));
        }
        
        //notify user through email or IM
        notify(user, targets, post, comment);
        
    }// addComment

    private boolean isValidTarget(RSComponent comp, User user) throws Exception
    {
        boolean valid = false;

        if (comp != null && user != null)
        {
            if (comp.getType() != null)
            {
                String type = comp.getType().name();
                if (comp.isLock())
                {
                    // this comp is locked , so cannot post to this comp
                    Log.log.trace("Component " + comp.getDisplayName() + " is Locked. So no Post allowed to this comp.");
                    throw new Exception("Component " + comp.getDisplayName() + " is Locked. So no Post allowed to this comp.");
                }
                else if (comp.isMarkDeleted())
                {
                    Log.log.trace("Component " + comp.getDisplayName() + " is Mark Deleted. So no Post allowed to this comp.");
                    throw new Exception("Component " + comp.getDisplayName() + " is Mark Deleted. So no Post allowed to this comp.");
                }
                else
                {
                    // for user, worksheet, namespacem --> no checks
                    // no Post allowed for Namespace
                    if (type.equalsIgnoreCase(NodeType.NAMESPACE.name())) 
                    {
                        valid = false;
                    }
                    else if (type.equalsIgnoreCase(NodeType.USER.name()) || type.equalsIgnoreCase(NodeType.WORKSHEET.name()))
                    {
                        valid = true;
                    }
                    else
                    {
                        // admin/edit and post roles should be able to do Post
                        String postRoles = comp.getEditRoles() + "," + comp.getPostRoles();
                        String userRoles = user.getRoles();
                        if (doesUserHasRoles(postRoles, userRoles))
                        {
                            valid = true;
                        }
                        else
                        {
                            // user has no roles/rights to Post on this comp
                            Log.log.trace("User " + user.getName() + " does not have Post rights for component " + comp.getDisplayName());
                            throw new Exception("User " + user.getName() + " does not have Post rights for component " + comp.getDisplayName());
                        }
                    }

                }
            }
        }// end of if

        return valid;

    }

    private boolean doesUserHasRoles(String compRoles, String userRoles)
    {
        boolean hasRoles = true;

        if (StringUtils.isNotEmpty(compRoles) && StringUtils.isNotEmpty(userRoles))
        {
            String csvUserRoles = com.resolve.services.hibernate.util.SocialUtil.convertSocialRolesStringToRoles(userRoles);
            Set<String> userRolesSet = new HashSet<String>(StringUtils.convertStringToList(csvUserRoles, ","));
            if (userRolesSet.contains("admin") || userRolesSet.contains("social_admin"))
            {
                hasRoles = true;
            }
            else
            {
                // set it to false as the default is true
                hasRoles = false;

                // this is not an admin user, so check the roles in detail
                String csvPostRoles = com.resolve.services.hibernate.util.SocialUtil.convertSocialRolesStringToRoles(compRoles);
                Set<String> compRolesSet = new HashSet<String>(StringUtils.convertStringToList(csvPostRoles, ","));
                for (String userRole : userRolesSet)
                {
                    if (compRolesSet.contains(userRole))
                    {
                        hasRoles = true;
                        break;
                    }
                }
            }
        }

        return hasRoles;
    }

    private boolean isUserValidToAddCommentsToThisPost(SocialPostResponse post, User user)
    {
        boolean valid = false;

        if (post != null && user != null)
        {
            if (post.isLocked())
            {
                // this comp is locked , so cannot post to this comp
                Log.log.trace("Component " + post.getSysId() + " is Locked. So no Comment allowed to this Post.");
            }
            else
            {
                Set<String> postRoles = post.getPostRoles();
                if (post.getPostRoles() == null)
                {
                    postRoles = post.getEditRoles();
                }

                String userRoles = user.getRoles();

                String csvUserRoles = com.resolve.services.hibernate.util.SocialUtil.convertSocialRolesStringToRoles(userRoles);
                Set<String> userRolesSet = new HashSet<String>(StringUtils.convertStringToList(csvUserRoles, ","));
                if (userRolesSet.contains("admin") || userRolesSet.contains("social_admin") || post.getAuthorUsername().equals(user.getName()))
                {
                    valid = true;
                }
                else
                {
                    // this is not an admin user, so check the roles in detail
                    for (String userRole : userRolesSet)
                    {
                        if (postRoles.contains(userRole))
                        {
                            valid = true;
                            break;
                        }
                    }
                    
                    if (!valid)
                    {
                        //if the user who is trying to add a comment, is in the 'targets' list, than he should be able to add the comment 
                        //as that means that its user to user communication
                        TreeSet<SocialTarget> targets = post.getTargets();
                        if (targets != null && targets.size() > 0)
                        {
                            for (SocialTarget target : targets)
                            {
                                if (NodeType.USER.name().equalsIgnoreCase(target.getType()))
                                {
                                    if (target.getSysId().equalsIgnoreCase(user.getSys_id()))
                                    {
                                        valid = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    
                }
            }

        }

        return valid;
    }

    private static boolean postToSocialComponent(String to, String subject, String content, User socialUser, String table, 
    		String type, String gatewayEventType) throws Exception
    {
        boolean isTargetFound = false;

        List<Map<String, Object>> components = new ArrayList<Map<String, Object>>();

        // if the post if from EMAIL or EWS then only post based on Email
        // account
        if (Constants.GATEWAY_EVENT_TYPE_EMAIL.equalsIgnoreCase(gatewayEventType) || Constants.GATEWAY_EVENT_TYPE_EWS.equalsIgnoreCase(gatewayEventType))
        {
            PostUtil postUtil = new PostUtil(null);

            components = getSocialComponentByEmailId(to, table);
            isTargetFound = (components != null && components.size() > 0);
            for (Map<String, Object> component : components)
            {
                String componentSysId = (String) component.get("sys_id");
                List<String> compSysIdList = new ArrayList<String>();
                compSysIdList.add(componentSysId);
                postUtil.post(subject, content, socialUser.getName(), compSysIdList, type);
            }
        }
        
        // if the post is from XMPP then only post based on IM account
        if (Constants.GATEWAY_EVENT_TYPE_XMPP.equalsIgnoreCase(gatewayEventType))
        {
            components = getSocialComponentByIMId(to, table);
            // if the target was not found before then only check again
            if (!isTargetFound)
            {
                isTargetFound = (components != null && components.size() > 0);
            }
            for (Map<String, Object> component : components)
            {
                String componentSysId = (String) component.get("sys_id");
                PostUtil postUtil = new PostUtil(null);
                postUtil.post(content, socialUser.getName(), componentSysId, type);
            }
        }
        return isTargetFound;
    }

    private static List<Map<String, Object>> getSocialComponentByEmailId(String emailId, String tableName) throws Exception
    {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

        if (StringUtils.isBlank(emailId))
        {
            Log.log.info("The Email ID is null or empty, so do not send to social component");
        }
        else
        {
            // find the email or IM for the process
            QueryDTO queryDTO = new QueryDTO();
            queryDTO.setUseSql(true);
            queryDTO.setSqlQuery("select * from " + tableName + " where u_receive_from='" + emailId + "'");
            
            /*
            if (GenericJDBCHandler.hasTable(tableName))
            {
                throw new Exception("Query table doesn't exist: " + tableName);
            }*/
            
            result = CustomTableAPI.getRecordData(queryDTO);
        }

        return result;
    }

    private static List<Map<String, Object>> getSocialComponentByIMId(String imId, String tableName) throws Exception
    {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

        if (StringUtils.isBlank(imId))
        {
            Log.log.info("The IM ID is null or empty, so do not send to social component");
        }
        else
        {
            // the XMPP address may come as yourname@gmail.com/Talk1235
            imId = imId.split("/")[0];
            // find the email or IM for the process
            QueryDTO queryDTO = new QueryDTO();
            queryDTO.setUseSql(true);
            queryDTO.setSqlQuery("select * from " + tableName + " where u_im='" + imId + "'");
            
            /*
            if (GenericJDBCHandler.hasTable(tableName))
            {
                throw new Exception("Query table doesn't exist: " + tableName);
            }*/
            
            result = CustomTableAPI.getRecordData(queryDTO);
        }

        return result;
    }

    private static Map<String, Object> getSocialComponent(String sysId, String tableName) throws Exception
    {
        Map<String, Object> result = new HashMap<String, Object>();

        if (StringUtils.isBlank(sysId))
        {
            Log.log.info("The Sys ID is null or empty, so do not send to social component");
        }
        else
        {
            // find the email or IM for the process
            QueryDTO queryDTO = new QueryDTO();
            queryDTO.setUseSql(true);
            queryDTO.setSqlQuery("select * from " + tableName + " where sys_id='" + sysId + "'");
            
            /*
            if (GenericJDBCHandler.hasTable(tableName))
            {
                throw new Exception("Query table doesn't exist: " + tableName);
            }*/
            
            List<Map<String, Object>> socialComponents = CustomTableAPI.getRecordData(queryDTO);
            if (socialComponents != null && socialComponents.size() > 0)
            {
                // it should have only one record anyways
                result = socialComponents.get(0);
            }
        }

        return result;
    }

    /**
     * In the result[0]=post content, result[1]=postSysId (if available)
     *
     * @param content
     * @return
     */
    private static String[] parseContent(String content)
    {
        String[] result = new String[2];

        if (Log.log.isTraceEnabled())
        {
            Log.log.trace("Email received from user:" + content);
        }

        // place all the parsing algorithm here
        Scanner scanner = new Scanner(content);
        StringBuilder sb = new StringBuilder();
        while (scanner.hasNextLine())
        {
            String line = scanner.nextLine();
            // This stops at a pattern as follows
            // On Mon, Mar 18, 2013 at 6:09 PM, John Smith
            // <john.smith@company.com> wrote:
            // On Mon, Mar 18, 2013 at 6:09 PM, John Smith <
            // 2013/3/27 <resolve1000@gmail.com>
            // --
            // later we may have to refactor this to add more pattern
            // String DATE_PATTERN =
            // "((19|20)\\d\\d)/(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01]).*?<.*";
            if (line.matches("((19|20)\\d\\d)/(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01]).*?<.*") || line.matches("On.*?<.*") || line.matches("On.*?wrote:.*") || line.matches("--.*"))
            {
                break;
            }
            sb.append(line);
        }

        // String comment = content.substring(0, content.indexOf("--")-1);
        result[0] = sb.toString();

        // We know the URL because we generated it, do exotic RegEx later during
        // refactoring
        String postSysId = null;
        if (content.contains("#RS.social.Main/postId="))
        {
            postSysId = content.substring(content.indexOf("#RS.social.Main/postId=") + 23, content.indexOf(">", content.indexOf("#RS.social.Main/postId=")));
            
            if (StringUtils.isNotBlank(postSysId))
            {
                result[1] = postSysId;
            }
        }

        return result;
    }

    private static Collection<User> forwardToSocialMembers(String tableName, String componentHeader, Map params, Collection<User> users) throws Exception
    {
        Collection<User> forwardedDirectlyToUsers = Collections.emptyList();
        
        String sysId = (String) params.get("sys_id");
        String type = (String) params.get("type");
        // String content = (String) params.get("content");
        Map<String, String> contentMap = StringUtils.stringToMap((String) params.get(CONTENT));

        if (StringUtils.isBlank(sysId))
        {
            Log.log.info("The Sys ID is null or empty, so do not send to social component");
        }
        else
        {
            // it should have only one record anyways
            Map<String, Object> processMap = getSocialComponent(sysId, tableName);
            String fromIm = null;
            String fromEmail = null;
            String sendTo = null; // could be a mailing list
            String emailDisplayName = null;
            String fromPassword = null;
            if (processMap.containsKey("u_im") && StringUtils.isNotBlank((String) processMap.get("u_im")))
            {
                fromIm = (String) processMap.get("u_im");
            }
            //if send to is not blank 
            if (processMap.containsKey("u_send_to") && StringUtils.isNotBlank((String) processMap.get("u_send_to")))
            {
                sendTo = (String) processMap.get("u_send_to");
            }
            if (processMap.containsKey("u_receive_from") && StringUtils.isNotBlank((String) processMap.get("u_receive_from")))
            {
                fromEmail = (String) processMap.get("u_receive_from");
            }
            if (processMap.containsKey("u_email_display_name") && StringUtils.isNotBlank((String) processMap.get("u_email_display_name")))
            {
                emailDisplayName = (String) processMap.get("u_email_display_name");
            }

            if (StringUtils.isNotBlank(sendTo))
            {
                //mailing list
                sendNotificationToMailingList(type, contentMap, fromEmail, sendTo, emailDisplayName);
            }
            else if ((StringUtils.isNotBlank(fromIm) || StringUtils.isNotBlank(fromEmail)) && StringUtils.isBlank(emailDisplayName))
            {
                sendNotificationToUsers(componentHeader, type, contentMap, fromIm, fromEmail, users);
                forwardedDirectlyToUsers = users;
            }
        }
        
        return forwardedDirectlyToUsers;
    }

    private static User getSocialUser(String gatewayEventType, String from, User socialUser)
    {
        if (Constants.GATEWAY_EVENT_TYPE_EMAIL.equalsIgnoreCase(gatewayEventType) || (Constants.GATEWAY_EVENT_TYPE_EWS.equalsIgnoreCase(gatewayEventType)))
        {
            if (StringUtils.isNotEmpty(from))
            {
                UsersVO user = UserUtils.getUserByEmail(from);
                if (user != null)
                {
                    socialUser = ServiceSocial.findUserByUsername(user.getUUserName());
                }
            }
        }
        else if (Constants.GATEWAY_EVENT_TYPE_XMPP.equalsIgnoreCase(gatewayEventType))
        {
            if (StringUtils.isNotEmpty(from))
            {
                // the XMPP address may come as yourname@gmail.com/Talk1235
                from = from.split("/")[0];
                UsersVO user = UserUtils.getUserByIM(from);
                if (user != null)
                {
                    socialUser = ServiceSocial.findUserByUsername(user.getUUserName());
                }
            }
        }
        return socialUser;
    }

    private static void sendNotificationToMailingList(String componentType, Map<String, String> contentMap, String fromEmail, String toEmail, String emailDisplayName) throws Exception
    {
        if (StringUtils.isNotBlank(toEmail) && StringUtils.isNotBlank(fromEmail))
        {
            @SuppressWarnings("unchecked")
            String subject = createEmailSubject(componentType, contentMap);
            String emailContent = getEmailContent(contentMap);
            NotificationAPI.send(toEmail, null, null, fromEmail, toEmail, subject, emailContent, null, emailDisplayName);
        }
    }

    private static void sendNotificationToUsers(String componentHeader, String componentType, Map<String, String> contentMap, String fromIm, String fromEmail, Collection<User> users) throws Exception
    {
        if (users != null && users.size() > 0)
        {
            String sender = contentMap.get("SENDER");
            String subject = createEmailSubject(componentType, contentMap);

            String emailContent = getEmailContent(contentMap);
            String imContent = getIMContent(componentHeader, contentMap);
            for (User user : users)
            {
                // make sure we don't send back to the sender.
                if (StringUtils.isNotBlank(user.getName()) && !user.getName().equalsIgnoreCase(sender))
                {
                    UsersVO userVO = UserUtils.getUser(user.getName());
                    if (userVO != null)
                    {
                        if (StringUtils.isNotEmpty(userVO.getUEmail()))
                        {
                            NotificationAPI.send(userVO.getUEmail(), null, null, fromEmail, subject, emailContent, null);
                        }
                        if (StringUtils.isNotEmpty(userVO.getUIM()))
                        {
                            NotificationAPI.sendIM(userVO.getUIM(), imContent, fromIm, null);
                        }
                    }
                }
            }
        }
    }

    private static String createEmailSubject(String componentType, Map<String, String> contentMap)
    {
        String displayName = (String) contentMap.get("DISPLAY_NAME");
        String content = contentMap.get("CONTENT");
        return createEmailSubject(componentType, displayName, content);
    }

    private static String createEmailSubject(String componentType, String displayName, String content)
    {
        String result = null;
        
        String subject = "";
        if(StringUtils.isNotBlank(content))
        {
            subject = StringUtils.smartSubstring(StringUtils.unescapeHtml(content.replaceAll("<.*?>", "")), 100);
        }
        
        if(StringUtils.isBlank(componentType) && StringUtils.isBlank(displayName))
        {
            result = subject;
        }
        else
        {
            result = "[" + componentType.toUpperCase() + "] (" + displayName + ") : " + subject;
        }
        
        return result;
    }

    /**
     * This method cleans up the content where hyperlink like following exists:
     *
     * <a href="javascript:void(0)"
     * onclick="window.open('http://www.yahoo.com','','')">yahoo</a> becomes <a
     * href="http://www.yahoo.com">yahoo</a>
     *
     * NOTE: This method will work as long as we do not change the incoming
     * format of the url.
     *
     * @param content
     * @return
     */
    private static String cleanupEmailContent(String content)
    {
        String result = content;
        if (StringUtils.isNotBlank(content) && content.contains("window.open('"))
        {
            content = content.replaceAll("javascript:void\\(0\\).*\\('", "");
            content = content.replaceAll("','',''\\)", "");
            result = content;
        }
        return result;
    }

    /**
     * This method cleans up the content so that only the URL can be rendered in
     * IM client:
     *
     * <a href=\"javascript:void(0)\"
     * onclick=\"window.open('http://www.yahoo.com','','')\">yahoo</a> becomes
     * http://www.yahoo.com
     *
     * NOTE: This method will work as long as we do not change the incoming
     * format of the url.
     *
     * @param content
     * @return
     */
    private static String cleanupIMContent(String content)
    {
        String result = content;
        if (StringUtils.isNotBlank(content))
        {
            if (content.contains("window.open('"))
            {
                content = content.replaceAll("\\<a.*\\('", "");
                content = content.replaceAll("','',''\\).*a\\>", "");
                result = content;
            }
            // <pre><a target="_blank"
            // href="http://localhost:8080/resolve/service/social/showentity?showpost=false&type=actiontask&sysId=8a9482f139785815013979d9e2280055&name=Comment#bipul">Comment#bipul</a></pre>
            else if (content.contains("/resolve/service/"))
            {
                content = content.replaceAll("</*pre>", "");
                content = content.replaceAll("\">.*/a>", "");
                content = content.replaceAll("<a.*href=\"", "");
                result = content;
            }
        }
        return result;
    }

    public static Collection<User> forwardToSocialComponent(Map params) throws Exception
    {
        Collection<User> forwardedDirectlyToUsers = Collections.emptyList();
        
        if (params == null || params.size() == 0)
        {
            Log.log.warn("Do not make empty call to this method, check your code!");
            throw new Exception("Do not make empty call to this method, check your code!");
        }
        else
        {
            try
            {
                String sysId = (String) params.get("sys_id");
                String type = (String) params.get("type");
                
                Collection<User> users = ServiceSocial.getAllUsersFollowingThisStream(sysId, "system");
                
                if (StringUtils.isNotBlank(type) && StringUtils.isNotBlank(sysId))
                {
                    String tableName = null;
                    String displayName = null;
                    
                    if ("process".equalsIgnoreCase(type))
                    {
                        Process process = SocialCompConversionUtil.createProcess(sysId);
                        tableName = "social_process";
                        displayName = "Process: " + process.getDisplayName();
                        //forwardedDirectlyToUsers = forwardToSocialMembers("social_process", "Process: " + process.getDisplayName(), params, users);
                    }
                    if ("forum".equalsIgnoreCase(type))
                    {
                        // query the social graph for data.
                        Forum forum = SocialCompConversionUtil.createForum(sysId);
                        tableName = "social_forum";
                        displayName = "Forum: " + forum.getDisplayName();
                        //forwardToSocialMembers("social_forum", "Forum: " + forum.getDisplayName(), params, users);
                    }
                    if ("team".equalsIgnoreCase(type))
                    {
                        // query the social graph for data.
                        Team team = SocialCompConversionUtil.createTeam(sysId);
                        tableName = "social_team";
                        displayName = "Team: " + team.getDisplayName();
                        //forwardToSocialMembers("social_team", "Team: " + team.getDisplayName(), params, users);
                    }
                    
                    if(StringUtils.isNotBlank(tableName) && StringUtils.isNotBlank(displayName))
                    {
                        forwardedDirectlyToUsers = forwardToSocialMembers(tableName, displayName, params, users);
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw e;
            }
        }
        
        return forwardedDirectlyToUsers;
    } // forwardToSocialComponent

    public static void postToSocialComponent(Map params) throws Exception
    {
        if (params == null || params.size() == 0)
        {
            Log.log.warn("Do not make empty call to this method, check your code!");
            throw new Exception("Do not make empty call to this method, check your code!");
        }
        else
        {
            try
            {
                PostUtil postUtil = new PostUtil(null);

                // when email/xmpp gateway send to social it doesn't have the type
                // which mean we'll see if any process/forum/team/user has the
                // email/xmpp address configured so we can post to that stream.
                if (params.containsKey("GATEWAY_EVENT_TYPE"))
                {
                    String gatewayEventType = (String) params.get("GATEWAY_EVENT_TYPE");
                    String to = (String) params.get("TO");
                    String from = (String) params.get("FROM");
                    String subject = (params.get("SUBJECT") == null) ? "" : ((String) params.get("SUBJECT"));
                    if(StringUtils.isBlank(subject))
                    {
                        //for backward compatibility.
                        subject = (params.get("TITLE") == null) ? "" : ((String) params.get("TITLE"));
                    }
                    String content = (String) params.get("CONTENT");
                    Map<String, byte[]> attachments = new HashMap<String, byte[]>();
                    if (params.containsKey("ATTACHMENTS"))
                    {
                        attachments = (Map<String, byte[]>) params.get("ATTACHMENTS");
                    }

                    // what a bizzare code, why getSocialUser needs socialUser?
                    // Cannot it simply return the User?
                    User socialUser = null;
                    socialUser = getSocialUser(gatewayEventType, from, socialUser);

                    if (socialUser != null && StringUtils.isNotBlank(content))
                    {
                        boolean isTargetFound = false;

                        // at this point save the attachment and modify the post
                        for (String key : attachments.keySet())
                        {
                            String[] fileInfo = StringUtils.stringToArray(key);
                            if (fileInfo != null && fileInfo.length == 2)
                            {
                                String fileName = fileInfo[0];
                                String contentType = fileInfo[1];
                                SocialPostAttachmentVO file = new SocialPostAttachmentVO();
                                file.setUContent(attachments.get(key));
                                file.setUFilename(fileName);
                                file.setUDisplayName(fileName);
                                file.setUType(contentType);

                                file = ServiceHibernate.saveSocialPostAttachment(file, socialUser.getName());

                                UploadFileDTO upload = new UploadFileDTO(file);
                                // upload.setSys_id(file.getSys_id());

                                content += "<br>" + upload.getUrl();
                            }
                        }

                        String[] post = parseContent(content);
                        String postContent = post[0];
                        String postSysId = post[1];
                        
                        if(StringUtils.isBlank(postSysId))
                        {
                            // Process
                            boolean isProcessFound = postToSocialComponent(to, subject, postContent, socialUser, "social_process", 
                            		AdvanceTree.PROCESS, gatewayEventType);
                            // Forums
                            boolean isForumFound = postToSocialComponent(to, subject, postContent, socialUser, "social_forum", 
                            		AdvanceTree.FORUMS, gatewayEventType);
                            // Teams
                            boolean isTeamFound = postToSocialComponent(to, subject, postContent, socialUser, "social_team", 
                            		AdvanceTree.TEAMS, gatewayEventType);

                            isTargetFound = isProcessFound || isForumFound || isTeamFound;

                            // if the "to" address is not attached to any social components 
                            // make a Blog post for the user. this may happen when a resolve user
                            // send an email to the default email address in the email gateway 
                            if (!isTargetFound)
                            {
                                // nothing found, it must be the user's blog
                                postUtil.postWithTitle(content, subject, socialUser.getName(), socialUser.getSys_id(), 
                                		AdvanceTree.BLOG);
                            }

                            //execute runbook
                            executeRunbook(content, socialUser);
                        }
                        else
                        {
                            try
                            {
                                postUtil.addComment(socialUser.getName(), postSysId, postContent);
                            }
                            catch (Exception e)
                            {
                                //Original post this email is reply to is not found or is deleted, treat it as new post
                                
                                // Process
                                boolean isProcessFound = postToSocialComponent(to, subject, postContent, socialUser, "social_process", 
                                		AdvanceTree.PROCESS, gatewayEventType);
                                // Forums
                                boolean isForumFound = postToSocialComponent(to, subject, postContent, socialUser, "social_forum", 
                                		AdvanceTree.FORUMS, gatewayEventType);
                                // Teams
                                boolean isTeamFound = postToSocialComponent(to, subject, postContent, socialUser, "social_team", 
                                		AdvanceTree.TEAMS, gatewayEventType);

                                isTargetFound = isProcessFound || isForumFound || isTeamFound;

                                // if the "to" address is not attached to any social components 
                                // make a Blog post for the user. this may happen when a resolve user
                                // send an email to the default email address in the email gateway 
                                if (!isTargetFound)
                                {
                                    // nothing found, it must be the user's blog
                                    postUtil.postWithTitle(content, subject, socialUser.getName(), socialUser.getSys_id(), 
                                    		AdvanceTree.BLOG);
                                }

                                //execute runbook
                                executeRunbook(content, socialUser);
                            }
                        }
                    }
                    else
                    {
                        Log.log.debug("The sender : " + from + " doesn't exist in resolve");
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    } // sendToSocialComponent

    /**
     * This method parses the content of a post and checks if has directives to execute a runbook.
     * The post must have a string like:
     * 
     * wiki=wikiname&problemid=new&param1=value1&param2=value2
     * 
     * Most importantly this directive must be in a new line, cannot be inside other comment.
     * 
     * @param content
     * @param socialUser
     */
    private static void executeRunbook(String content, User socialUser)
    {
        List<String> contentLines = StringUtils.readLines(content);
        for (String contentLine : contentLines)
        {
            if (StringUtils.isNotBlank(contentLine))
            {
                String trimmed = contentLine.trim().toUpperCase();
                if (trimmed.startsWith("WIKI="))
                {
                    Map<String, String> paramsMap = StringUtils.urlToMap(contentLine.trim());
                    Map<String, Object> runbookParams = new HashMap<String, Object>();
                    for (String key : paramsMap.keySet())
                    {
                        String value = paramsMap.get(key);
                        if (Constants.EXECUTE_WIKI.equalsIgnoreCase(key) && StringUtils.isNotBlank(value))
                        {
                            runbookParams.put(Constants.EXECUTE_WIKI, paramsMap.get(key));
                        }
                        else if (Constants.EXECUTE_PROBLEMID.equalsIgnoreCase(key) && StringUtils.isNotBlank(value))
                        {
                            runbookParams.put(Constants.EXECUTE_PROBLEMID, paramsMap.get(key));
                        }
                        else
                        {
                            runbookParams.put(key, paramsMap.get(key));
                        }
                    }

                    // now validate and send for execute
                    if (!runbookParams.containsKey(Constants.EXECUTE_WIKI))
                    {
                        Log.log.error("Invalid content, WIKI must be provided with the key as WIKI=test.test");
                    }
                    else
                    {
                        if (!runbookParams.containsKey(Constants.EXECUTE_PROBLEMID))
                        {
                            runbookParams.put(Constants.EXECUTE_PROBLEMID, "NEW");
                        }
                        // this is the user we found based on
                        // the sender's email address
                        // it'll be the runbook executor
                        runbookParams.put(Constants.EXECUTE_USERID, socialUser.getName());

                        Log.log.debug("Sending message to execute runbook: " + runbookParams.get(Constants.EXECUTE_WIKI) + " with paraam Map: " + runbookParams);
                        String message = ExecuteRunbook.start(runbookParams);
                        
                        if (StringUtils.isNotBlank(message))
                        {
                            if (message.toUpperCase().contains("ERROR") || message.toUpperCase().contains("FAILURE"))
                            {
                                Log.log.error ("Failed to execute runbook " + runbookParams.get(Constants.EXECUTE_WIKI) + ". " + message);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * This method sends email to user with all the social posts accumulated
     * during a time period.
     *
     * @param params
     */
    public static void dailyEmail(Map params) throws Exception
    {
        // Gather the list of users.
        Map<User, List<RSComponent>> users = ServiceSocial.getUserStreamsForDigest();
        //List<User> users = SocialUtil.getAllSocialUsers();

        long startTimeInMillis = -1;
        long endTimeInMillis = -1;

        Calendar calendar = Calendar.getInstance();

        int dayOfTheYear = calendar.get(Calendar.DAY_OF_YEAR);
        // go back one day.
        calendar.set(Calendar.DAY_OF_YEAR, dayOfTheYear - 1);

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        startTimeInMillis = calendar.getTimeInMillis();

        calendar.set(Calendar.HOUR_OF_DAY, 24);
        endTimeInMillis = calendar.getTimeInMillis();

        for (User user : users.keySet())
        {
            try
            {
                List<RSComponent> targets = users.get(user);
                Collection<SocialPostResponse> posts = new TreeSet<SocialPostResponse>();
                if(targets != null && targets.size() > 0)
                {
                    //all includes Inbox already
                    posts.addAll(getAllPosts(user.getName(), SearchConstants.DISPLAY_CATEGORY_ALL, targets, startTimeInMillis, endTimeInMillis));
                }
                else if(user.isInboxDigestEmail())
                {
                    posts.addAll(getAllPosts(user.getName(), SearchConstants.DISPLAY_CATEGORY_INBOX, targets, startTimeInMillis, endTimeInMillis));
                }
                
                if(user.isSystemDigestEmail())
                {
                    //we actually want to capture manually entered system notifications
                    posts.addAll(getAllPosts(user.getName(), SearchConstants.DISPLAY_CATEGORY_MANUAL, targets, startTimeInMillis, endTimeInMillis));
                }
                
                if (posts != null && posts.size() > 0)
                {
                    StringBuilder content = getDailyEmailContent(user, posts);
                    NotificationAPI.sendToUser(user.getName(), "Your Daily Resolve Digest", content.toString(), null);
                }

            }
            catch (Exception e)
            {
                // we got to move on the the next user.
                Log.log.info("SocialStore.getAllPosts() threw the following exception : " + e.getMessage() + ", moving onto the next user.");
            }
        }
    } // dailyEmail

    /**
     * params.put("USERNAME", senderUsername);
     * params.put("SUBJECT", subject);
     * params.put("POSTCONTENT", content);
     * params.put("TARGET", socialTeamname);
     * params.put("SENDEMAIL", Boolean.toString(sendEmail));
     *
     * @param params
     * @throws Exception
     */
    public static void postToTeam(Map params) throws Exception
    {
        if (params == null || params.size() == 0)
        {
            Log.log.warn("Do not make empty call to this method, check your code!");
            throw new Exception("Do not make empty call to this method, check your code!");
        }
        else
        {
            postToComponent(params, AdvanceTree.TEAMS);

            // Required by NotificationAPI
            if (params.containsKey("SENDEMAIL") && Boolean.valueOf((String) params.get("SENDEMAIL")))
            {
                String sender = (String) params.get("USERNAME");
                String content = (String) params.get("POSTCONTENT");
                String targetTeam = (String) params.get("TARGET");
                // This call actually doesn't create a Team but query it by
                // name.
                Team team = SocialCompConversionUtil.createTeamByName(targetTeam);//SocialUtil.createTeamByName(targetTeam);
                if (team == null)
                {
                    Log.log.warn("Team: " + targetTeam + " does not exist.");
                }
                else
                {
                    // The original API call to createTeamByName doesn't give
                    // users in it, hence this next call, weird? well...
                    team = ServiceSocial.getTeam(team.getSys_id(), false);
                    for (User user : team.getUsers())
                    {
                        try
                        {
                            NotificationAPI.sendToUser(user.getName(), content, content, null);
                        }
                        catch (Exception e)
                        {
                            Log.log.error("Error sending email notification to : " + user.getDisplayName());
                        }
                    }
                }
            }
        }
    }

    public static void postToComponent(Map<String, Object> params, String targetType) throws Exception
    {
        if (params == null || params.size() == 0 || StringUtils.isBlank(targetType))
        {
            throw new Exception("Invalid entry in params or targetType, check your code!");
        }
        else
        {
            String username = (String) params.get("USERNAME");
            String postContents = (String) params.get("POSTCONTENT");

            String target = (String) params.get("TARGET");
            String targetSysId = (String) params.get("TARGETSYSID");
            String targetCompType = targetType;
            String postId = (String) params.get("POSTSYSID");

            Map<String, Object> paramsMap = new HashMap<String, Object>();
            paramsMap.put(PostUtil.POST_SYS_ID, postId);
            // paramsMap.put(SearchConstants.TIMEZONE.getTagName(),
            // Post.TIMEZONE_DEFAULT);
            // paramsMap.put(SearchConstants.TIMEZONE_OFFSET.getTagName(),
            // Post.TIMEZONE_OFFSET_DEFAULT);
            String subject = (params.get("SUBJECT") == null) ? "" : ((String) params.get("SUBJECT"));
            String title = (params.get("TITLE") == null) ? "" : ((String) params.get("TITLE"));
            title = (StringUtils.isEmpty(title)) ? subject : title;

            if (StringUtils.isNotEmpty(target) && StringUtils.isNotEmpty(username))
            {
                if (StringUtils.isEmpty(targetSysId))
                {
                    if (targetCompType.equalsIgnoreCase(AdvanceTree.ACTIONTASKS))
                    {
                        ResolveActionTaskVO at = null;

                        try
                        {
                            if (StringUtils.isNotEmpty(targetSysId))
                            {
                                at = ServiceHibernate.getResolveActionTask(targetSysId, null, username);
                            }
                            else if (StringUtils.isNotEmpty(target) && target.indexOf("#") > -1)
                            {
                                at = ServiceHibernate.getResolveActionTask(null, target, username);
                            }
                        }
                        catch (Throwable t)
                        {
                            // do nothing
                        }

                        if (at != null)
                        {
                            targetSysId = at.getSys_id();
                        }

                    }
                    else if (targetCompType.equalsIgnoreCase(AdvanceTree.DOCUMENTS) || targetCompType.equalsIgnoreCase(AdvanceTree.RUNBOOKS))
                    {
                        WikiDocument doc = null;

                        try
                        {
                            if (StringUtils.isNotEmpty(targetSysId))
                            {
                                doc = StoreUtility.findById(targetSysId);
                            }
                            else if (StringUtils.isNotEmpty(target))
                            {
                                doc = StoreUtility.find(target);
                            }
                        }
                        catch (Throwable t)
                        {
                            // do nothing
                        }

                        if (doc != null)
                        {
                            targetSysId = doc.getSys_id();

                            // TODO - for the decision tree, change the
                            // DOCUMENTS to DECISIONTREE
                            targetCompType = StringUtils.isNotEmpty(doc.getUModelProcess()) ? AdvanceTree.RUNBOOKS : (StringUtils.isNotEmpty(doc.getUDecisionTree()) ? AdvanceTree.DOCUMENTS : AdvanceTree.DOCUMENTS);
                        }
                    }
                    else
                    {
                        SocialDTO record = null;

                        if (targetCompType.equalsIgnoreCase(AdvanceTree.FORUMS))
                        {
                            if (StringUtils.isNotBlank(target))
                            {
                                record = ServiceHibernate.getForumByName(target, "admin");
                            }
                            else if (StringUtils.isNotBlank(targetSysId))
                            {
                                record = ServiceHibernate.getForum(targetSysId, "admin");
                            }
                        }
                        else if (targetCompType.equalsIgnoreCase(AdvanceTree.TEAMS))
                        {
                            if (StringUtils.isNotBlank(target))
                            {
                                record = ServiceHibernate.getTeamByName(target, "admin");
                            }
                            else if (StringUtils.isNotBlank(targetSysId))
                            {
                                record = ServiceHibernate.getTeam(targetSysId, "admin");
                            }
                        }
                        else if (targetCompType.equalsIgnoreCase(AdvanceTree.PROCESS))
                        {
                            if (StringUtils.isNotBlank(target))
                            {
                                record = ServiceHibernate.getProcessByName(target, "admin");
                            }
                            else if (StringUtils.isNotBlank(targetSysId))
                            {
                                record = ServiceHibernate.getProcess(targetSysId, "admin");
                            }
                        }
                        else if (targetCompType.equalsIgnoreCase(AdvanceTree.RSS))
                        {
                            if (StringUtils.isNotBlank(target))
                            {
                                record = ServiceHibernate.getRssByName(target, "admin");
                            }
                            else if (StringUtils.isNotBlank(targetSysId))
                            {
                                record = ServiceHibernate.getRss(targetSysId, "admin");
                            }
                        }

                        if (record != null)
                        {
                            targetSysId = record.getSys_id();
                        }
                    }
                }

                if (StringUtils.isNotEmpty(postContents) && StringUtils.isNotEmpty(target) && StringUtils.isNotEmpty(targetSysId) && StringUtils.isNotEmpty(targetCompType) && StringUtils.isNotEmpty(username))
                {
                    new PostUtil(paramsMap).postWithTitle(postContents, title, username, targetSysId, targetCompType);
                }
            }
        }
    }

    /**
     * Gets all the social posts between a time range for all the components
     * this user is following as well as interested in getting email.
     *
     * @param username
     *            must be passed if not throws exception.
     * @param bucket
     *            "all", "inbox", "system" etc.
     * @param targets
     *            target Ids this user allowed to get posts.
     * @param startTimeInMillis
     *            must be >= 0
     * @param endTimeInMillis
     *            must be greater than startTimeInMillis.
     * @return
     * @throws Exception
     *             thrown if validation fails.
     * @param startTimeInMillis
     * @param endTimeInMillis
     * @return
     * @throws Exception
     */
    public static Collection<SocialPostResponse> getAllPosts(String username, String streamId, List<RSComponent> targets, long startTimeInMillis, long endTimeInMillis) throws Exception
    {
        Collection<SocialPostResponse> result = new ArrayList<SocialPostResponse>();

        if (StringUtils.isEmpty(username))
        {
            throw new Exception("Username must be provided");
        }
        if (startTimeInMillis < 0 || endTimeInMillis <= startTimeInMillis)
        {
            throw new Exception("Invalid time range of startTimeInMillis:" + startTimeInMillis + " and endTimeInMillis: " + endTimeInMillis);
        }

        // make call to SearchAPI for posts.
        QueryDTO query = new QueryDTO();
        //query.addFilterItem(new QueryFilter(QueryFilter.DATE_TYPE, DateUtils.getSocialIsoDateString(startTimeInMillis), "sysUpdatedOn", QueryFilter.GREATER_THAN_OR_EQUAL));
        //query.addFilterItem(new QueryFilter(QueryFilter.DATE_TYPE, DateUtils.getSocialIsoDateString(endTimeInMillis), "sysUpdatedOn", QueryFilter.LESS_THAN_OR_EQUAL));
        User user = ServiceSocial.findUserByUsername(username);
        if(user != null)
        {
            if(targets.size() > 0)
            {
                ResponseDTO<SocialPostResponse> response = searchPosts(query, streamId, null, targets, false, true, startTimeInMillis, endTimeInMillis, user);
                for (SocialPostResponse obj : response.getRecords())
                {
                    result.add(obj);
                }
            }
        }
        return result;
    }

    public static ResponseDTO<SocialPostResponse> searchPosts(QueryDTO query, String streamId, String streamType, boolean includeComments, boolean unreadOnly, User user) throws Exception
    {
        List<RSComponent> targets = ServiceSocial.getUserStreams(user.getSys_id(), user.getCompName(), true);
        return searchPosts(query, streamId, streamType, targets, includeComments, unreadOnly, null, null, user);
    }

    public static ResponseDTO<SocialPostResponse> advancedSearch(QueryDTO query, User user) throws Exception
    {
        List<RSComponent> targets = ServiceSocial.getUserStreams(user.getSys_id(), user.getCompName(), true);
        
        TreeSet<String> targetIds = new TreeSet<String>();
        for (RSComponent target : targets)
        {
            targetIds.add(target.getSys_id());
        }
        
        return SocialSearchAPI.advancedSearch(targetIds, query, getUserInfo(user));
    }

    public static ResponseDTO<SocialPostResponse> searchPosts(QueryDTO query, String streamId, String streamType, 
    														  List<RSComponent> targets, boolean includeComments, 
    														  boolean unreadOnly, Long startTimesInMillis, 
    														  Long endTimesInMillis, User user) throws Exception
    {
    	long startTime = System.currentTimeMillis();
        ResponseDTO<SocialPostResponse> result = new ResponseDTO<SocialPostResponse>();
        Map<String, String> followedTargetIdToDisplayName = new HashMap<String, String>();

        try
        {
            TreeSet<String> targetIds = new TreeSet<String>();
            String tmpStreamType = null;
            if(StringUtils.isNotBlank(streamType))
            {
                tmpStreamType = streamType.toLowerCase();
            }
            if (com.resolve.search.SearchConstants.DISPLAY_CATEGORIES.containsKey(streamId)
                            || com.resolve.search.SearchConstants.DISPLAY_CATEGORIES.containsKey(tmpStreamType))
            {
                for (RSComponent target : targets)
                {
                    targetIds.add(target.getSys_id());
                    followedTargetIdToDisplayName.put(target.getSys_id(), target.getDisplayName());
                }
                if(StringUtils.isNotBlank(query.getSearchTerm()))
                {
                    result = SocialSearchAPI.searchPosts(targetIds, query, null, streamType, includeComments, unreadOnly, getUserInfo(user));
                }
                else
                {
                    result = SocialSearchAPI.searchPostsByBucket(streamId, streamType, targetIds, query, includeComments, unreadOnly, startTimesInMillis, endTimesInMillis, getUserInfo(user));
                }
            }
            else if(StringUtils.isNotBlank(streamId))
            {
                RSComponent targetComponent = LookupComponent.getRSComponentById(streamId);
                if (targetComponent != null)
                {
                    targetIds.add(streamId);
                    if (targetComponent.getType() == NodeType.PROCESS || targetComponent.getType() == NodeType.TEAM)
                    {
                        targetIds.addAll(ServiceSocial.getComponentIdsBelongingToContainer(targetComponent));
                    }
                    // user selected a stream directly (like a forum)
                    result = SocialSearchAPI.searchPosts(targetIds, query, null, streamType, includeComments, unreadOnly, getUserInfo(user));
                }
            }
            else
            {
                for (RSComponent target : targets)
                {
                    targetIds.add(target.getSys_id());
                }
                result = SocialSearchAPI.searchPosts(targetIds, query, null, streamType, includeComments, unreadOnly, getUserInfo(user));
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }

        if(CollectionUtils.isNotEmpty(result.getRecords()))
        {
            //Update display name in targets
            for(SocialPostResponse spr : result.getRecords())
            {
                Set<SocialTarget> sts = spr.getTargets();
                
                if(sts != null && !sts.isEmpty())
                {
                    for(SocialTarget st : sts)
                    {
                        // Social target is followed RS component
                        if(followedTargetIdToDisplayName.containsKey(st.getSysId()))
                        {
                            st.setDisplayName(followedTargetIdToDisplayName.get(st.getSysId()));
                        }
                        else // Social target is not followed RS component, if type is USER then find the display name and set for UI 
                        {
                            if(st.getType().equals(NodeType.USER.name()))
                            {
                                ResolveNodeVO notFollowedUser = GraphUtil.findNode(null, null, st.getName(), NodeType.USER, user.getName());
                                
                                if(notFollowedUser != null)
                                {
                                    st.setDisplayName(notFollowedUser.getDisplayName());
                                }
                                else
                                {
                                    st.setDisplayName(st.getName());
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if (Log.log.isDebugEnabled()) {
        	Log.log.debug(String.format("Total time taken to return %d posts for %d Resolve Components: %d msec", 
        								(CollectionUtils.isNotEmpty(result.getRecords()) ? result.getRecords().size() : 0),
        								targets.size(), (System.currentTimeMillis() - startTime)));
        }
        
        return result;
    }

    /**
     * The paremeter must have following keys with values.
     *
     * params.put("USERNAME", senderUsername); params.put("SUBJECT", subject);
     * params.put("POSTCONTENT", content); params.put("TARGET", socialUsername);
     *
     * @param params
     * @throws Exception
     */
    public void postToUser(Map params) throws Exception
    {
        if (params.containsKey("POSTCONTENT") && params.containsKey("TARGET") && params.containsKey("USERNAME"))
        {
            String username = (String) params.get("USERNAME");
            String subject = (params.get("SUBJECT") == null) ? "" : ((String) params.get("SUBJECT"));
            String content = (String) params.get("POSTCONTENT");
            String target = (String) params.get("TARGET");
            if(StringUtils.isNotBlank(target))
            {
                //this is an attempt to see if target is a "username", there seems to
                //be no gurantee what this TARGET could be, will it be the sysId ever?
                User user = ServiceSocial.findUserByUsername(target);
                if(user != null)
                {
                    target = user.getSys_id();
                }
            }
            List<String> targetsSysIds = new ArrayList<String>();
            targetsSysIds.add(target);

            post(subject, content, username, targetsSysIds);
        }
    }

    public static void setLock(User user, String id, boolean isLocked) throws Exception
    {
        UserInfo userInfo = getUserInfo(user);
        SocialPost post = SocialSearchAPI.findSocialPostById(id, userInfo.getUsername());
        if(post == null)
        {
            throw new Exception("Post not found with the id: " + id);
        }
        else
        {
            if(userInfo.isAdmin() || user.getName().equals(post.getAuthorUsername()))
            {
                SocialIndexAPI.setLocked(id, user.getName(), isLocked);
            }
            else
            {
                throw new Exception("User has to be either the author or an admin to lock/unlock post.");
            }
        }
    }

    public static void indexPost(SocialPost post, List<RSComponent> components, User user, String postType) throws Exception
    {
        TreeSet<SocialTarget> targets = getSocialTargets(components);
        SocialIndexAPI.indexSocialPost(post, targets, user.getName(), postType, true);
        
        //TODO special demo related code for social post, should be removed later
        if(demoDataMap.size() > 0)
        {
            for(String text : demoDataMap.keySet())
            {
                if(post.getContent().toLowerCase().contains(text))
                {
                    String username = demoDataMap.get(text)[0];
                    String comment = demoDataMap.get(text)[1];
                    new PostUtil().addComment(username, post.getSysId(), comment);
                }
            }
        }
    }

    public static TreeSet<SocialTarget> getSocialTargetByIds(List<String> targetIds)
    {
        TreeSet<SocialTarget> result = new TreeSet<SocialTarget>();
        for (String targetId : targetIds)
        {
            RSComponent component = LookupComponent.getRSComponentById(targetId);
            SocialTarget socialTarget = getSocialTarget(component);
            if (socialTarget == null)
            {
                Log.log.debug("SocialTarget was not found for targetId: " + targetId + ", might have been deleted");
            }
            else
            {
                result.add(socialTarget);
            }
        }
        return result;
    }

    private static TreeSet<SocialTarget> getSocialTargets(List<RSComponent> components)
    {
        TreeSet<SocialTarget> result = new TreeSet<SocialTarget>();
        for (RSComponent component : components)
        {
            result.add(getSocialTarget(component));
        }
        return result;
    }

    private static SocialTarget getSocialTarget(RSComponent component)
    {
        SocialTarget result = null;
        if (component != null)
        {
            String namespace = "";
            if(component.getType() == NodeType.DOCUMENT)
            {
                namespace = component.getCompName().substring(0, component.getCompName().indexOf("."));
            }
            
            result = new SocialTarget(component.getSys_id(), component.getType().name(), null, component.getName(), component.getDisplayName(), namespace, component.getRoles(), component.getPostRoles(), component.getEditRoles());
        }
        return result;
    }

    public static void deletePost(User user, String id) throws SearchException
    {
        if(user != null && user.getName() != null)
        {
            SocialPost post = SocialSearchAPI.findSocialPostById(id, user.getName());
            if(post != null)
            {
                if(isAdminUser(user) || post.getAuthorUsername().equalsIgnoreCase(user.getName()))
                {
                    SocialIndexAPI.setDeleted(id, user.getName(), true);
                }
                else
                {
                    Log.log.info("User " + user.getName() + " cannot delete the post.");
                    throw new SearchException("User " + user.getName() + " cannot delete the post.");
                }
            }
        }
    }

    public static void deleteComment(User user, String id) throws SearchException
    {
        if(user != null && user.getName() != null)
        {
            SocialComment comment = SocialSearchAPI.findSocialCommentById(id, user.getName());
            if(comment != null)
            {
                if(isAdminUser(user) || comment.getCommentAuthorUsername().equalsIgnoreCase(user.getName()))
                {
                    SocialIndexAPI.deleteSocialCommentById(id, user.getName());
                }
                else
                {
                    Log.log.info("User " + user.getName() + " cannot delete the comment.");
                    throw new SearchException("User " + user.getName() + " cannot delete the comment.");
                }
            }
        }
    }

    public static void movePost(String postId, User user, List<String> newTargetIds) throws Exception
    {
        TreeSet<SocialTarget> newTargets = new TreeSet<SocialTarget>();
        for (String newTargetId : newTargetIds)
        {
            RSComponent comp = LookupComponent.getRSComponentById(newTargetId);
            newTargets.add(getSocialTarget(comp));
        }
        SocialIndexAPI.moveSocialPost(postId, newTargets, user.getName());
    }

    public static List<RSComponent> getUsersWhoLikedThis(String id, String username)
    {
        List<RSComponent> users = new ArrayList<RSComponent>();

        // search only deals with username
        Set<String> usernames = SocialSearchAPI.getUserIdsWhoLikedThis(id, username);

        if (usernames != null)
        {
            for (String likedUsername : usernames)
            {
                User likedUser = ServiceSocial.findUserByUsername(likedUsername);
                users.add(likedUser);
            }
        }
        return users;
    }

    public static void markPostAsReadByQuery(String streamId, String streamType, QueryDTO query, String username, boolean isRead) throws Exception
    {
        User user = ServiceSocial.findUserByUsername(username);

        List<RSComponent> targets = ServiceSocial.getUserStreams(user.getSys_id(), user.getCompName(), true);
        TreeSet<String> targetIds = new TreeSet<String>();
        if (com.resolve.search.SearchConstants.DISPLAY_CATEGORIES.containsKey(streamId))
        {
            for (RSComponent target : targets)
            {
                targetIds.add(target.getSys_id());
            }
        }
        else if(StringUtils.isNotBlank(streamId))
        {
            ResolveNodeVO targetNode = ServiceGraph.findNode(null, streamId, null, null, username);
            if(targetNode != null)
            {
                targetIds.add(targetNode.getUCompSysId());
                if (targetNode.getUType() == NodeType.PROCESS)
                {
                    Collection<ResolveNodeVO> nodes = ServiceGraph.getNodesFollowedBy(null, targetNode.getUCompSysId(), null, NodeType.PROCESS, username);
                    if(nodes != null)
                    {
                        for(ResolveNodeVO node : nodes)
                        {
                            targetIds.add(node.getUCompSysId());
                        }
                    }
                    
                    //this is for Users
                    nodes = ServiceGraph.getNodesFollowingMe(null, targetNode.getUCompSysId(), null, NodeType.PROCESS, username);
                    if(nodes != null)
                    {
                        for(ResolveNodeVO node : nodes)
                        {
                            targetIds.add(node.getUCompSysId());
                        }
                    }
                }
                else if(targetNode.getUType() == NodeType.TEAM)
                {
                    Collection<ResolveNodeVO> nodes = ServiceGraph.getNodesFollowedBy(null, targetNode.getUCompSysId(), null, NodeType.TEAM, username);
                    if(nodes != null)
                    {
                        for(ResolveNodeVO node : nodes)
                        {
                            targetIds.add(node.getUCompSysId());
                        }
                    }
                    
                    //this is for Users
                    nodes = ServiceGraph.getNodesFollowingMe(null, targetNode.getUCompSysId(), null, NodeType.TEAM, username);
                    if(nodes != null)
                    {
                        for(ResolveNodeVO node : nodes)
                        {
                            targetIds.add(node.getUCompSysId());
                        }
                    }
                }
            }
        }

        //this is an asynchronous call because the volume may be way too big.
        SocialIndexAPI.markPostAsRead(query, streamId, streamType, targetIds, getUserInfo(user));
    }

    /**
     * Posts RSS feed into ElasticSearch.
     *
     * Note: RSS information comes from RSSReader class.
     *
     * @param post
     * @param targetsSysId
     * @param username
     */
    public void postRSS(SocialPost post, List<String> targetsSysId, String username)
    {
        User user = ServiceSocial.findUserByUsername(username);

        postEngine = new PostProcessEngine(post.getContent(), false);

        // process mentions
        //mentions = postEngine.getPostUserMentions();

        // encode post
        String content = postEngine.getModifiedPostOrComment();

        // set of roles - cumulative of all the targets
        TreeSet<String> roles = new TreeSet<String>();
        roles.add("admin");

        // set of edit roles - cumulative of all the targets
        TreeSet<String> editRoles = new TreeSet<String>();
        editRoles.add("admin");

        // set of edit roles - cumulative of all the targets
        TreeSet<String> postRoles = new TreeSet<String>();
        postRoles.add("admin");

        // list of targets
        List<RSComponent> targets = new ArrayList<RSComponent>();
        StringBuilder errors = new StringBuilder();
        for (String targetSysId : targetsSysId)
        {
            RSComponent temp = LookupComponent.getRSComponentById(targetSysId);
            boolean valid = false;
            try
            {
                valid = isValidTarget(temp, user);
            }
            catch (Exception e)
            {
                errors.append(e.getMessage()).append("\n");
            }
            
            // validate the target
            if (valid)
            {
                targets.add(temp);

                // roles
                String targetRoles = temp.getRoles();
                if (StringUtils.isNotBlank(targetRoles))
                {
                    String[] streamRoles = targetRoles.split(" ");
                    for (String s : streamRoles)
                    {
                        roles.add(s);
                    }
                }

                // edit roles
                String targetEditRoles = temp.getEditRoles();
                if (StringUtils.isNotBlank(targetEditRoles))
                {
                    String[] editRolesArr = targetEditRoles.split(" ");
                    for (String s : editRolesArr)
                    {
                        editRoles.add(s);
                    }
                }

                // post roles
                String targetPostRoles = temp.getPostRoles();
                if (StringUtils.isNotBlank(targetPostRoles))
                {
                    String[] postRolesArr = targetPostRoles.split(" ");
                    for (String s : postRolesArr)
                    {
                        postRoles.add(s);
                    }
                }
            }
        }// end of for loop

        if (targets != null && targets.size() > 0)
        {
            post.setContent(content);
            post.setReadRoles(roles);
            post.setEditRoles(editRoles);
            post.setPostRoles(postRoles);
            try
            {
                indexPost(post, targets, user, SearchConstants.SOCIALPOST_TYPE_RSS);
            }
            catch (Exception e)
            {
                Log.log.error("error doing Post by user:" + username, e);
            }
        }
    }

    public static Map<String, Long> getUnreadPostCount(QueryDTO queryDTO, User user, List<RSComponent> userStreams)
    {
        Map<String, Long> result = new HashMap<String, Long>();
        try
        {
            TreeSet<String> targetIds = new TreeSet<String>();
            for (RSComponent target : userStreams)
            {
                targetIds.add(target.getSys_id());
            }

            result = SocialSearchAPI.getUnreadPostCount(queryDTO, targetIds, getUserInfo(user));
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }

    public static long countUnreadPostByStreamId(String streamId, String streamType, User user) throws SearchException, Exception
    {
        return SocialSearchAPI.countUnreadPostByStreamId(streamId, streamType, getUserInfo(user));
    }
    
    /**
     * Generates a email body as follows:
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
     *
     * Hello Process - Steve Smith
     * Wed, Aug 15, 2012 10:40 AM
     * It was great to see Sunrise this morning... More
     * 
     * ......
     * 
     * @param post
     * @return
     */
    private static StringBuilder getEmailContent(RSComponent component, SocialPost post)
    {
        StringBuilder result = new StringBuilder();
        
        result.append("<html>");
        result.append("<p>");
        result.append("<a href=\"" + MainBase.viewResolveUrl + "/jsp/rsclient.jsp#RS.social.Main/postId="+ post.getSysId() + "\"><b>" + component.getDisplayName() + "</b></a> - ");
        result.append("<a href=\"" + MainBase.viewResolveUrl + "/jsp/rsclient.jsp#RS.user.User/username="+ post.getAuthorUsername() + "\"><b>" + post.getAuthorDisplayName() + "</b></a><br/>");
        result.append("<i>" + DateUtils.convertTimeInMillisToString(post.getSysCreatedOn(), "E, MMM dd, yyyy HH:mm a") + "</i><br/>");
        result.append(cleanupEmailContent(post.getContent()) + "<br/>");
        result.append("</p>");
        result.append("</html>");
        return result;
    }

    private static StringBuilder getEmailContent(RSComponent component, SocialComment comment)
    {
        StringBuilder result = new StringBuilder();
        
        result.append("<html>");
        result.append("<p>");
        result.append("<a href=\"" + MainBase.viewResolveUrl + "/jsp/rsclient.jsp#RS.social.Main/postId="+ comment.getPostId() + "\"><b>" + component.getDisplayName() + "</b></a> - ");
        result.append("<a href=\"" + MainBase.viewResolveUrl + "/jsp/rsclient.jsp#RS.user.User/username="+ comment.getCommentAuthorUsername() + "\"><b>" + comment.getCommentAuthorDisplayName() + "</b></a><br/>");
        result.append("<i>" + DateUtils.convertTimeInMillisToString(comment.getSysCreatedOn(), "E, MMM dd, yyyy HH:mm a") + "</i><br/>");
        result.append(cleanupEmailContent(comment.getContent()) + "<br/>");
        result.append("</p>");
        result.append("</html>");
        return result;
    }

    private static StringBuilder getEmailContent(RSComponent component, SocialPost post, SocialComment comment)
    {
        StringBuilder content = null;
        if(comment != null)
        {
            content = getEmailContent(component, comment);
        }
        else
        {
            content = getEmailContent(component, post);
        }
        return content;
    }
    
    public static String getEmailContent(Map<String, String> contentMap)
    {
        StringBuilder content = new StringBuilder();
        content.append("<html>");
        content.append("<body style=\"font-family:Verdana\">");
        content.append("<p>");
        content.append("<a href=\"" + contentMap.get("LINK") + "\"><b>" + contentMap.get("DISPLAY_NAME") + "</b></a> - ");
        content.append("<a href=\"" + contentMap.get("SENDER_LINK") + "\"><b>" + contentMap.get("SENDER_NAME") + "</b></a><br/>");
        if(StringUtils.isNumeric(contentMap.get("CREATED_ON")))
        {
            content.append("<i>" + DateUtils.convertTimeInMillisToString(Long.parseLong(contentMap.get("CREATED_ON")), "E, MMM dd, yyyy HH:mm a") + "</i><br/>");
        }
        content.append(cleanupEmailContent(contentMap.get("CONTENT")) + "<br/>");
        content.append("</p>");
        content.append("</body>");
        content.append("</html>");
        return content.toString();
    }

    public static String getIMContent(String componentHeader, Map<String, String> contentMap)
    {
        StringBuilder content = new StringBuilder();

        content.append("Resolve Social Post\n");
        content.append("Link:" + contentMap.get("LINK") + "\n\n");
        // from GraphDB, component's display name comes wrongly as "Forum" or
        // "Process" always, so skipping it for now.
        // content.append(componentHeader + "\n");
        content.append("Posted by: " + contentMap.get("SENDER_NAME") + "\n");
        content.append("Content: " + cleanupIMContent(contentMap.get("CONTENT")) + "\n");
        return content.toString();
    }
    
    /**
     * Generates a email body as follows:
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     * Daily Digest for John Doe
     * Wed, Aug 15, 2012
     *
     * Steve Smith
     * Wed, Aug 15, 2012 10:40 AM
     * It was great to see Sunrise this morning... More
     *
     * ......
     *
     * @param user
     * @param posts
     * @return
     */
    public static StringBuilder getDailyEmailContent(User user, Collection<SocialPostResponse> posts)
    {
        StringBuilder content = new StringBuilder();
        if(posts != null && posts.size() > 0)
        {
            content.append("<html>");
            content.append("<body style=\"font-family:Verdana\">");
            content.append("<div style=\"background-color:#444;color:white;padding:5px;margin:0px;font-size:20px;font-weight:bold\">Daily Digest for " + user.getDisplayName() + "</div>");
            content.append("<div style=\"background-color:#444;color:white;padding:5px;margin:0px;font-size:16px\">" + DateUtils.convertCalendarToString(Calendar.getInstance(), "E, MMM dd, yyyy") + "</div>");
            content.append("<br/>");

            boolean isOdd = false;
            for(SocialPost post : posts)
            {
                Collection<SocialTarget> targets = post.getTargets();

                for(SocialTarget target: targets)
                {
                    RSComponent component = ServiceSocial.getComponentById(target.getSysId(), user.getName());
                    content.append("<p" + (isOdd ? " style=\"background-color:#f0f0f0\"" : "" ) + ">");
                    isOdd = !isOdd;
                    content.append("<b>" + component.getDisplayName() + "</b>");
                    content.append("<i>" + DateUtils.convertTimeInMillisToString(post.getSysCreatedOn(), "E, MMM dd, yyyy HH:mm a") + "</i><br/>");
                    //Social post comes with <pre> and messes up the email body, hence removing it here.
                    //Also we are only interested in the first 175 characters.
                    String cleanPost = StringUtils.smartSubstring(post.getContent().replaceAll("</*pre>", ""), 175);
                    String entityType = component.getClass().getSimpleName();
                    String entitySysId = target.getSysId();
                    //content.append(cleanPost + "... <a href=\"" + RSContext.getResolveUrl() + "/service/social/showentity?type="+ entityType.toLowerCase() + "&sysId=" + entitySysId + "\">More</a>");
                    content.append(cleanPost + "... <a href=\"" + MainBase.viewResolveUrl 
                    		+ "/jsp/rsclient.jsp#RS.social.Main/postId="+ post.getSysId() + "\">More</a>");
                    content.append("</p>");
                    
                    //############### INTENTIONALLY BREAKING OUT #############
                    //there maybe myltiple targets attached but it's fine to 
                    //display the first one in the digest email
                    break;
                }
            }
            content.append("</body>");
            content.append("</html>");
        }
        return content;
    }

    private static StringBuilder getNotificationContent(User user, SocialPost post, String content, long createdOn, 
    		RSComponent target)
    {
        StringBuilder result = new StringBuilder();

        result.append("SENDER|=|" + user.getName() + "|&|");
        result.append("SENDER_NAME|=|" + user.getDisplayName() + "|&|");
        //http://localhost:8888/resolve//jsp/rsclient.jsp##RS.user.User/john
        result.append("SENDER_LINK|=|" + MainBase.viewResolveUrl 
        		+ "/jsp/rsclient.jsp#RS.user.User/username="+ user.getName() + "|&|");
        result.append("CREATED_ON|=|" + createdOn + "|&|");
        result.append("DISPLAY_NAME|=|" + target.getDisplayName() + "|&|");
        result.append("CONTENT_TYPE|=|" + post.getType() + "|&|");
        result.append("CONTENT|=|" + cleanupContent(content, MainBase.viewResolveUrl) + "|&|");
        result.append("LINK|=|" + MainBase.viewResolveUrl + "/jsp/rsclient.jsp#RS.social.Main/postId="+ post.getSysId());
        return result;
    }

    private static String cleanupContent(final String content, final String resolveUrl)
    {
        String result = content.replaceAll("</*pre>", "");
        //check if there is any mentions/file etc in the content
        if(content.contains("/resolve/service/"))
        {
            result = content.replaceAll("/resolve/service/", resolveUrl + "/service/");
        }
        if(result.contains("/resolve/jsp/"))
        {
            result = result.replaceAll("/resolve/jsp/", resolveUrl + "/jsp/");
        }
        
        //adds absolute URL to Resolve so that it can be directly accessed from Email.
        if(result.contains("#RS.user.User"))
        {
            result = result.replaceAll("#RS.user.User", resolveUrl + "/jsp/rsclient.jsp#RS.user.User");
        }
        if(result.contains("#RS.search.Main"))
        {
            result = result.replaceAll("#RS.search.Main", resolveUrl + "/jsp/rsclient.jsp#RS.search.Main");
        }

        return result;
    }
    
    private static String massageSubject(String subject)
    {
        String result = subject;
        
        if(StringUtils.isNotBlank(result))
        {
            //escape out the <script></script> javascript as its malicious in nature
            result = StringUtils.escapeJavascript(result);
            
            //escape the meta tag
            result = StringUtils.escapeMeta(result);
        }
        
        return result;
    }
    
    private static void verifyTag(String contents, String username) throws Exception
    {
        Set<String> tags = SearchUtils.findTags(contents);
        for (String tagName : tags)
        {
            ResolveTagVO tag = ServiceTag.getTag(null, tagName, username);
            if(tag == null)
            {
                tag = new ResolveTagVO();
                tag.setUName(tagName);
                
                ServiceTag.saveTag(tag, username);
            }
        }//end of if
    }
}
