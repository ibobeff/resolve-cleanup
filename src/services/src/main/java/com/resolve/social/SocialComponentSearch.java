/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.social;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.services.ServiceGraph;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.ServiceSocialConstants;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.hibernate.vo.UsersVO;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.QuerySort.SortOrder;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SocialComponentSearch
{
    private String username = null;
    private QueryDTO query = null; 
    
    private String type = null;
    private String searchString = null;
    
    private int start = -1;
    private int limit = -1;
    private Set<String> userRoles = null;
    
    private final static String[] columns = { ServiceSocialConstants.SYS_ID, 
                                        ServiceSocialConstants.U_DISPLAY_NAME, 
                                        ServiceSocialConstants.SYS_UPDATED_BY, 
                                        ServiceSocialConstants.SYS_UPDATED_ON, 
                                        ServiceSocialConstants.SYS_CREATED_ON, 
                                        ServiceSocialConstants.SYS_CREATED_BY, 
                                        ServiceSocialConstants.U_READ_ROLES
                                      };
    
    private ResponseDTO<RSComponent> response = new ResponseDTO<RSComponent>();
    private long total = 0;
    
    public SocialComponentSearch(String username, QueryDTO query)
    {
        if(query == null)
        {
            throw new RuntimeException("QueryDTO is mandatory");
        }
        this.username = username;
        this.query = query;
        
        this.start = query.getStart();
        this.limit = query.getLimit();
        userRoles = UserUtils.getUserRoles(username);
        
        //set the filter values
        List<QueryFilter> filters = query.getFilterItems();
        if(filters != null && filters.size() > 0)
        {
            //[{"field":"type","condition":"equals","value":"document"},{"field":"name","condition":"startsWith","value":"test"}]
            for(QueryFilter filter : filters)
            {
                String field = filter.getField();
                if(field.equalsIgnoreCase("type"))
                {
                    this.type = filter.getValue();
                }
                else if(field.equalsIgnoreCase("name") && StringUtils.isNotEmpty(filter.getValue()))
                {
                    this.searchString = filter.getValue().toLowerCase();
                }
            }
        }

    }
    
    public ResponseDTO<RSComponent> getStreams()
    {
        
        List<RSComponent> result = new ArrayList<RSComponent>();
        
        if(StringUtils.isNotBlank(type))
        {
            if(type.equalsIgnoreCase(NodeType.ACTIONTASK.name()))
            {
                result.addAll(searchActiontask());
                total = totalRecords();
            }
            else if(type.equalsIgnoreCase(NodeType.DECISIONTREE.name()))
            {
                result.addAll(searchWiki());
                total = totalRecords();
            }
            else if(type.equalsIgnoreCase(NodeType.DOCUMENT.name()))
            {
                result.addAll(searchWiki());
                total = totalRecords();
            }
            else if(type.equalsIgnoreCase(NodeType.FORUM.name()))
            {
                result.addAll(searchSocialComponent());
                total = totalRecords();
            }
            else if(type.equalsIgnoreCase(NodeType.NAMESPACE.name()))
            {
                result.addAll(searchNamespace());
            }
            else if(type.equalsIgnoreCase(NodeType.PROCESS.name()))
            {
                result.addAll(searchSocialComponent());
                total = totalRecords();
            }
            else if(type.equalsIgnoreCase(NodeType.RSS.name()))
            {
                result.addAll(searchSocialComponent());
                total = totalRecords();
            }
            else if(type.equalsIgnoreCase(NodeType.RUNBOOK.name()))
            {
                result.addAll(searchWiki());
                total = totalRecords();
            }
            else if(type.equalsIgnoreCase(NodeType.TEAM.name()))
            {
                result.addAll(searchSocialComponent());
                total = totalRecords();
            }
            else if(type.equalsIgnoreCase(NodeType.USER.name()))
            {
                result.addAll(searchUser());
                total = totalRecords();
            }
            else if(type.equalsIgnoreCase(NodeType.WORKSHEET.name()))
            {
                result.addAll(searchWorksheet());
            }            
        }
        
        //sort 
        //sortResult(result);
        
        response.setRecords(result);
        response.setTotal(total);
        
        return response;
    }
    
    private int totalRecords()
    {
        int result = 0;
        String countQry = null;
        try
        {
            if(StringUtils.isNotEmpty(query.getHql()))
            {
                countQry = "select COUNT(*) as COUNT from " + this.query.getHql().substring(this.query.getHql().indexOf("from") + "from".length());
                result = ServiceHibernate.getTotalHqlCount(countQry);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting data for sql:" + countQry, e);
        }
                
        return result;
    }
    
    
    private List<RSComponent> searchActiontask()
    {
        List<RSComponent> result = new ArrayList<RSComponent>();
        StringBuilder hql = new StringBuilder("select at.sys_id as sys_id, at.UFullName as u_display_name, at.sysUpdatedBy as sys_updated_by, at.sysUpdatedOn as sys_updated_on, " +
                        "at.sysCreatedOn as sys_created_on, at.sysCreatedBy as sys_created_by, ar.UReadAccess as u_read_roles ");
        hql.append(" from ResolveActionTask as at, AccessRights as ar where ");
        hql.append(" ar.UResourceId = at.sys_id and ar.UResourceType ='actiontask' and ");

        String roleFilter = "";
        //qry only if its not an 'admin', else show everything
        if (!this.userRoles.contains("admin"))
        {
            for (String userRole : userRoles)
            {
                roleFilter = "(ar.UAdminAccess like '%" + userRole + "%' or ar.UReadAccess like '%" + userRole + "%') ";
            }
        }
        else
        {
            roleFilter = " 1=1 ";
        }

        //search filter
        String searchQry = ""; 
        if (StringUtils.isNotBlank(searchString))
        {
            searchQry = " and (LOWER(at.UFullName) like '%" + searchString + "%')";
        }
        
        //sort condition
        this.query.parseSortClause();
        String sortBy = this.query.getOrderBy();
        if(StringUtils.isBlank(sortBy))
        {
            sortBy = this.query.getSort();
        }
        
        //full qry
        hql.append(roleFilter).append(searchQry);
        if(StringUtils.isNotBlank(sortBy))
        {
            //have to hardcode this as the query has a join and its using a generic UI 
            sortBy = sortBy.replace("displayName", "at.UFullName");
            hql.append(" order by ").append(sortBy);
        }
        
        //set it in the query object
        this.query.setHql(hql.toString());

        //execute the qry and get the results
        result.addAll(executeQuery(hql.toString(), NodeType.ACTIONTASK.name()));
        
        return result;        
    }
    
    private List<RSComponent> searchWiki()
    {
        List<RSComponent> result = new ArrayList<RSComponent>();

        String hql =  queryForWiki(type, searchString);
        
        //set it in the query object
        this.query.setHql(hql.toString());

        //execute the qry and get the results
        result.addAll(executeQuery(hql.toString(), NodeType.DOCUMENT.name()));
        
        return result;
    }
    
    private List<RSComponent> searchSocialComponent()
    {
        List<RSComponent> result = new ArrayList<RSComponent>();
        
        String tableName = "";
        String socialType = "";
        if(type.equalsIgnoreCase(NodeType.FORUM.name()))
        {
            tableName = "social_forum";
            socialType = NodeType.FORUM.name();
        }
        else if(type.equalsIgnoreCase(NodeType.TEAM.name()))
        {
            tableName = "social_team";
            socialType = NodeType.TEAM.name();
        }
        else if(type.equalsIgnoreCase(NodeType.PROCESS.name()))
        {
            tableName = "social_process";
            socialType = NodeType.PROCESS.name();
        }
        else if(type.equalsIgnoreCase(NodeType.RSS.name()))
        {
            tableName = "rss_subscription";
            socialType = NodeType.RSS.name();
        }
        
        
        StringBuilder hql = new StringBuilder("select a1.sys_id as sys_id, a1.u_display_name as u_display_name, a1.sys_updated_by as sys_updated_by, a1.sys_updated_on as sys_updated_on, " +
                        "a1.sys_created_on as sys_created_on, a1.sys_created_by as sys_created_by, a1.u_read_roles as u_read_roles ");
        hql.append(" from " + tableName + " as a1 where ");
        
        String roleFilter = "";
        //qry only if its not an 'admin', else show everything
        if (!this.userRoles.contains("admin"))
        {
            for (String userRole : userRoles)
            {
                roleFilter = " (a1.u_read_roles like '%" + userRole + "%' or a1.u_edit_roles like '%" + userRole + "%' or a1.u_post_roles like '%" + userRole + "%') ";
            }
        }
        else
        {
            roleFilter = " 1=1 ";
        }
        
        //search filter
        String searchQry = "";
        if (StringUtils.isNotBlank(searchString))
        {
            searchQry = " and (LOWER(a1.u_display_name) like '%" + searchString + "%')";
        }
        
        //sort condition
        this.query.parseSortClause();
        String sortBy = this.query.getOrderBy();
        if(StringUtils.isBlank(sortBy))
        {
            sortBy = this.query.getSort();
        }
        
        
        //full qry
        hql.append(roleFilter).append(searchQry);
        
        // Make sure, not pulling any record with display name as null.
        hql.append(" and a1.u_display_name is not null ");
        
        if(StringUtils.isNotBlank(sortBy))
        {
            //have to hardcode this as the query has a join and its using a generic UI 
            sortBy = sortBy.replace("displayName", "a1.u_display_name");
            hql.append(" order by ").append(sortBy);
        }
        
        //set it in the query object
        this.query.setHql(hql.toString());

        //execute the qry and get the results
        result.addAll(executeQuery(hql.toString(), socialType));
        
        return result;
    }
    
    
    private List<RSComponent> searchNamespace()
    {
        List<RSComponent> result = new ArrayList<RSComponent>();
        
        //get this list from the graphdb api that Fen will provide
        //will have to do the check for access rights here after getting the components
//        User user = SocialCompConversionUtil.getSocialUser(username);
        try
        {
            ResponseDTO<ResolveNodeVO> response = ServiceGraph.findNamespaceNodes(searchString, NodeType.NAMESPACE, start, limit, username);
            if(response.getRecords() != null && response.getRecords().size() > 0)
            {
                for(ResolveNodeVO node : response.getRecords())
                {
                    result.add(new RSComponent(node));
                }
            }
            
            total = response.getTotal();
        }
        catch (Exception e)
        {
           Log.log.error("error getting the namespace", e);
        }
        
        return result;
    }
    
    
    
    private List<RSComponent> searchUser()
    {
        List<RSComponent> result = new ArrayList<RSComponent>();
        
        query.setModelName("Users");
        query.setSelectColumns("sys_id,UUserName,UFirstName,ULastName");
        
        StringBuilder hql = new StringBuilder("select a1.sys_id as sys_id, a1.UUserName as UUserName, a1.UFirstName as UFirstName, a1.ULastName as ULastName "); 
//                        +"a1.sysUpdatedBy as sysUpdatedBy, a1.sysUpdatedOn as sysUpdatedOn, " +
//                        "a1.sysCreatedOn as sysCreatedOn, a1.sysCreatedBy as sysCreatedBy " +
//                        ");
        hql.append(" from Users as a1 where ");
        
        
        String roleFilter = "";
        //qry only if its not an 'admin', else show everything
//        if (!this.userRoles.contains("admin"))
//        {
//            for (String userRole : userRoles)
//            {
//                roleFilter = "(ar.UAdminAccess like '%" + userRole + "%' or ar.UReadAccess like '%" + userRole + "%') ";
//            }
//        }
//        else
//        {
            roleFilter = " 1=1 and LOWER(a1.UUserName) NOT IN ('system','" + this.username.toLowerCase() + "') ";
//        }

        //search filter
        String searchQry = "";
        if (StringUtils.isNotBlank(searchString))
        {
            searchQry = " and (LOWER(a1.UUserName) like '%" + searchString + "%')";
        }
        
        //sort condition
        this.query.parseSortClause();
        String sortBy = this.query.getOrderBy();
        if(StringUtils.isBlank(sortBy))
        {
            sortBy = this.query.getSort();
        }
        
        //full qry
        hql.append(roleFilter).append(searchQry);
        if(StringUtils.isNotBlank(sortBy))
        {
            //have to hardcode this as the query has a join and its using a generic UI 
            sortBy = sortBy.replace("displayName", "a1.UFirstName");
            hql.append(" order by ").append(sortBy);
        }
        
        //set it in the query object
        this.query.setHql(hql.toString());

        //execute the qry and get the results
        result.addAll(executeQueryForUser(hql.toString()));
        
        return result;
        
    }
    
    private List<RSComponent> searchWorksheet()
    {
        List<RSComponent> result = new ArrayList<RSComponent>();
        
        try
        {
            ResponseDTO<ResolveNodeVO> response = ServiceGraph.findNodes(null, NodeType.WORKSHEET, start, limit, username);
            if(response.getRecords() != null && response.getRecords().size() > 0)
            {
                for(ResolveNodeVO node : response.getRecords())
                {
                    result.add(new RSComponent(node));
                }
            }
            
            total = response.getTotal();
        }
        catch (Exception e)
        {
            Log.log.error("Error Getting the WS nodes", e);
        }
        
        return result;
    }
    
    private String queryForWiki(String type, String searchString)
    {
        List<String> excludeWiki = PropertiesUtil.getPropertyList("search.exclude.document.pattern");
        
        StringBuilder hql = new StringBuilder("select wd.sys_id as sys_id, wd.UFullname as u_display_name, wd.sysUpdatedBy as sys_updated_by, wd.sysUpdatedOn as sys_updated_on, " +
                        "wd.sysCreatedOn as sys_created_on, wd.sysCreatedBy as sys_created_by, ar.UReadAccess as u_read_roles ");
        hql.append(" from WikiDocument as wd, AccessRights as ar where ");
        
        if (StringUtils.isNotEmpty(type))
        {
            if (type.equalsIgnoreCase(NodeType.DECISIONTREE.name()))
            {
                hql.append("  (wd.UIsRoot is not null and wd.UIsRoot is true) ");
            }
            else if (type.equalsIgnoreCase(NodeType.DOCUMENT.name()))
            {
                hql.append("  (wd.UHasActiveModel is null or wd.UHasActiveModel is false) ");
            }
            else if (type.equalsIgnoreCase(NodeType.RUNBOOK.name()))
            {
                hql.append(" (wd.UHasActiveModel is not null and wd.UHasActiveModel is true) ");
            }
        }
        else
        {
            hql.append(" 1=1 ");
        }
        
        hql.append(" and wd.UIsDeleted = false and wd.UIsLocked = false and wd.UIsHidden = false and ar.UResourceId = wd.sys_id and ar.UResourceType ='wikidoc' and ");
        if(excludeWiki != null && excludeWiki.size() > 0)
        {
            for(String exclude : excludeWiki)
            {
                hql.append(" wd.UFullname NOT LIKE '%" + exclude + "%' and ");
            }
        }

        StringBuilder roleFilter = new StringBuilder("(");
        
        //qry only if its not an 'admin', else show everything
        if (!this.userRoles.contains("admin"))
        {
            for (String userRole : userRoles)
            {
                roleFilter.append(" ar.UAdminAccess like '%" + userRole + "%' or ar.UReadAccess like '%" + userRole + "%' or");
            }
        }
        else
        {
            roleFilter.append(" 1=1 or");
        }

        //remove the last 'or'
        roleFilter.setLength(roleFilter.length()-2);
        
        roleFilter.append(")");

        //search filter
        String searchQry = "";
        if (StringUtils.isNotBlank(searchString))
        {
            searchQry = " and (LOWER(wd.UFullname) like '%" + searchString.toLowerCase() + "%')";
        }
        
        //sort condition
        this.query.parseSortClause();
        String sortBy = this.query.getOrderBy();
        if(StringUtils.isBlank(sortBy))
        {
            sortBy = this.query.getSort();
        }
        
        //full qry
        hql.append(roleFilter).append(searchQry);
        if(StringUtils.isNotBlank(sortBy))
        {
            //have to hardcode this as the query has a join and its using a generic UI 
            sortBy = sortBy.replace("displayName", "wd.UFullname");
            hql.append(" order by ").append(sortBy);
        }

        
        return hql.toString();
    }

    private List<RSComponent> executeQuery(String hql, String type)
    {
        List<RSComponent> result = new ArrayList<RSComponent>();
        try
        {
            //execute the qry
//            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelect(hql.toString(), limit, start);
            List<? extends Object> data = ServiceHibernate.executeHQLSelectVO(query, start, limit, true);
            if(data != null)
            {
                for(Object o : data)
                {
                    Object[] record = (Object[]) o;

                    Map<String, Object> recMap = new HashMap<String, Object>();
                    recMap.put(ServiceSocialConstants.TYPE, type);
                    
                    for(int count=0; count < record.length; count++)
                    {
                        Object val = record[count];
                        if(val != null && val instanceof Date)
                        {
                            val = new Long(((Date) val).getTime());
                        }
                        
                        recMap.put(columns[count], val);
                        
                    }//end of for loop
                    
                    result.add(new RSComponent(recMap));
                }//end of for loop
            }//end of if
            
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting data for sql:" + hql.toString(), e);
        }
        
        return result;
        
    }
    
    private List<RSComponent> executeQueryForUser(String hql)
    {
        List<RSComponent> result = new ArrayList<RSComponent>();
        try
        {
            //execute the qry
            List<? extends Object> data = ServiceHibernate.executeHQLSelectVO(query, start, limit, true);
            if(data != null)
            {
                String[] userColumns = this.query.getSelectColumns().split(",");
                
                for(Object o : data)
                {
                    Object[] record = (Object[]) o;
                    UsersVO instance = new UsersVO();

                    
                    //set the attributes in the object
                    for(int x=0; x < userColumns.length; x++)
                    {
                        String column = userColumns[x].trim();
                        Object value = record[x];

                        PropertyUtils.setProperty(instance, column, value);
                    }
                    
                    result.add(SocialCompConversionUtil.convertUserToSocialUser(instance, null));
                }//end of for loop
            }//end of if
            
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting data for sqk:" + hql.toString(), e);
        }
        
        return result;
        
    }
    
    private void sortResult(List<RSComponent> result)
    {
        boolean asc = true;
        if(this.query.getSortItems() != null && this.query.getSortItems().size() > 0)
        {
            for(QuerySort sort : this.query.getSortItems())
            {
                if(sort.getProperty().equalsIgnoreCase("name") && sort.getDirection() == SortOrder.DESC)
                {
                    asc = false;
                    break;
                }
            }
        }
        
        if(asc)
        {
            Collections.sort(result, new ComparatorRsComponentAsc());
        }
        else
        {
            Collections.sort(result, new ComparatorRsComponentDesc());
        }
    }
    
    private class ComparatorRsComponentAsc implements Comparator<RSComponent>
    {
        public final int compare(RSComponent a, RSComponent b)
        {
            return a.getDisplayName().compareTo(b.getDisplayName());
        } // end compare

    } // ComparatorBaseModel    

    private class ComparatorRsComponentDesc implements Comparator<RSComponent>
    {
        public final int compare(RSComponent a, RSComponent b)
        {
            int val = a.getDisplayName().compareTo(b.getDisplayName());
            return val * -1;
        } // end compare

    } // ComparatorBaseModel   
}
