/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.workflow.form;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.hibernate.query.Query;

import com.resolve.persistence.model.MetaAccessRights;
import com.resolve.persistence.model.MetaControl;
import com.resolve.persistence.model.MetaControlItem;
import com.resolve.persistence.model.MetaField;
import com.resolve.persistence.model.MetaFieldProperties;
import com.resolve.persistence.model.MetaFormAction;
import com.resolve.persistence.model.MetaFormTab;
import com.resolve.persistence.model.MetaFormTabField;
import com.resolve.persistence.model.MetaFormView;
import com.resolve.persistence.model.MetaSource;
import com.resolve.persistence.model.MetaxFieldDependency;
import com.resolve.persistence.model.MetaxFormViewPanel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class CopyOrRenameCustomForm
{
    private String fromViewName = null;
    private String toViewName = null;
    private String username = null;
    
    public CopyOrRenameCustomForm(String fromViewName, String toViewName, String username) throws Exception
    {
        if(StringUtils.isBlank(fromViewName) || StringUtils.isBlank(toViewName))
        {
            throw new Exception("From View Name and To View Name are both mandatory.");
        }
        this.fromViewName = fromViewName;
        this.toViewName = toViewName;
        this.username = username;
    }
    
    public MetaFormView copy() throws Exception
    {
        //get the object
        MetaFormView toView = loadMetaFormView();
        
        //set the ids to null
        setSysIdsToNull(toView);
        
		try {
        HibernateProxy.setCurrentUser(username);
			HibernateProxy.execute(() -> {
				persist(toView);
			});
		} catch (Exception e) {
			Log.log.error("Error in copying template: " + fromViewName, e);
			throw e;
		}
        
        return toView;
    }
    
    public MetaFormView rename() throws Exception
    {
        
        
        try
        {
          HibernateProxy.setCurrentUser(username);
        	return (MetaFormView) HibernateProxy.execute(() -> {
        		
        		MetaFormView fromMetaFormView = null;
                fromMetaFormView = findMetaFormView(fromViewName);
                if(fromMetaFormView == null)
                {
                    throw new Exception("Form View " + fromViewName + " does not exist.");
                }
                
                MetaFormView toMetaFormView = findMetaFormView(toViewName);
                if(toMetaFormView != null)
                {
                    throw new Exception("Form View " + toViewName + " already exist.");
                }
                   
                //start rename
                fromMetaFormView.setUViewName(toViewName);
                fromMetaFormView.setUFormName(toViewName);
                
                MetaAccessRights rights = fromMetaFormView.getMetaAccessRights();
                if(rights != null)
                {
                    rights.setUResourceName(toViewName);
                }
                
                MetaControl control = fromMetaFormView.getMetaControl();
                if(control != null)
                {
                    control.setUName(toViewName);
                }
                
                return fromMetaFormView;

        	});
        }
        catch (Exception e)
        {
            Log.log.error("Error in renaming template: " + fromViewName, e);
            throw e;
        }
        
    }//rename
    
    //private apis  
    private MetaFormView findMetaFormView(String viewNameLocal) throws Exception
    {
        MetaFormView result = null;
        String hql = " from MetaFormView where upper(UViewName) = '" + viewNameLocal.toUpperCase() + "'";
        Query query = HibernateUtil.createQuery(hql);
        List<?> temp = query.list();
        if (temp.size() > 0)
        {
            result = (MetaFormView) temp.get(0);
        }
        else
        {
            result = null;
        }
        
        return result;
    }
    
    ////////////////////////////////////////////////////////////////// LOADING IN MEMORY ////////////////////////////////////
    private MetaFormView loadMetaFormView() throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	return (MetaFormView) HibernateProxy.execute(() -> {
        		
        		MetaFormView fromMetaFormView = null;
                MetaFormView toMetaFormView = findMetaFormView(toViewName);
                if(toMetaFormView != null)
                {
                    throw new Exception("Form View " + toViewName + " already exist.");
                }

                //load the object
                fromMetaFormView = findMetaFormView(fromViewName);
                if(fromMetaFormView == null)
                {
                    throw new Exception("Form View " + fromViewName + " does not exist.");
                }
                
                //now continue loading tabs
                loadMetaFormTabs(fromMetaFormView.getMetaFormTabs());
                
                //sources
                loadMetaSources(fromMetaFormView.getMetaSources());
                
                //control
                MetaControl metaControl = fromMetaFormView.getMetaControl();
                if(metaControl != null)
                {
                    Collection<MetaControlItem> metaControlItems = metaControl.getMetaControlItems();
                    if(metaControlItems != null)
                    {
                        for(MetaControlItem item : metaControlItems)
                        {
                            Collection<MetaFormAction> metaFormActions = item.getMetaFormActions();
                            if(metaFormActions != null)
                            {
                                metaFormActions.size();
                            }
                            
                            loadMetaxFieldDependencies(item.getMetaxFieldDependencys());                        
                        }
                    }
                }

                //panels
                Collection<MetaxFormViewPanel> metaxFormViewPanels = fromMetaFormView.getMetaxFormViewPanels();
                if(metaxFormViewPanels != null)
                {
                    for(MetaxFormViewPanel panel : metaxFormViewPanels)
                    {
                        loadMetaFormTabs(panel.getMetaFormTabs());
                    }
                }
                
                return fromMetaFormView;
        	});
        }
        catch (Exception e)
        {
            Log.log.error("Error in loading template: " + fromViewName, e);
            throw e;
        }
    }
    
    private void loadMetaFormTabs(Collection<MetaFormTab> tabs)
    {
        if (tabs == null) {
        	return;
        }
    	
        for(MetaFormTab tab : tabs)
        {
            loadMetaFormTabFields(tab.getMetaFormTabFields());
            
            loadMetaxFieldDependencies(tab.getMetaxFieldDependencys());
        }
    }
    
    private void loadMetaFormTabFields(Collection<MetaFormTabField> metaFormTabFields)
    {
        if (metaFormTabFields == null) {
        	return;
        }
    	
        for (MetaFormTabField tabfield : metaFormTabFields)
        {
            loadMetaSource(tabfield.getMetaSource());
        }
    }
    
    private void loadMetaSources(Collection<MetaSource> metaSources)
    {
        if (metaSources == null) {
        	return;
        }
    	
        for(MetaSource source : metaSources)
        {
            loadMetaSource(source);
        }                            
    }
    
    private void loadMetaSource(MetaSource source)
    {
        if (source == null) {
        	return;
        }
    	
        MetaFieldProperties fieldProp = source.getMetaFieldProperties();
        
        loadMetaxFieldDependencies(fieldProp.getMetaxFieldDependencys());
    }
    
    private void loadMetaxFieldDependencies(Collection<MetaxFieldDependency> dependencies)
    {
        if(dependencies != null)
        {
            dependencies.size();
        }
    }
    
    ////////////////////////////////////////////////////////////////// END - LOADING IN MEMORY ////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////// START - SET SYSIDS TO NULL ////////////////////////////////////
    private void setSysIdsToNull(MetaFormView view)
    {
        view.setSys_id(null);
        
        //access rights
        view.getMetaAccessRights().setSys_id(null);
        view.getMetaAccessRights().setUResourceId(null);
        view.getMetaAccessRights().setUResourceName(null);
        
        //tabs
        setSysIdToNullForMetaFormTabs(view.getMetaFormTabs());
        
        //sources
        setSysIdToNullMetaSources(view.getMetaSources());
        
        //control
        MetaControl metaControl = view.getMetaControl();
        if(metaControl != null)
        {
            metaControl.setSys_id(null);
            
            Collection<MetaControlItem> metaControlItems = metaControl.getMetaControlItems();
            if(metaControlItems != null)
            {
                for(MetaControlItem item : metaControlItems)
                {
                    item.setSys_id(null);
                    item.setMetaControl(null);
                    
                    Collection<MetaFormAction> metaFormActions = item.getMetaFormActions();
                    if(metaFormActions != null)
                    {
                        for(MetaFormAction action : metaFormActions)
                        {
                            action.setSys_id(null);   
                            action.setMetaControlItem(null);
                        }                        
                    }                    
                    
                    setSysIdToNullMetaxFieldDependency(item.getMetaxFieldDependencys());
                }
            }
        }
        
        //panels
        Collection<MetaxFormViewPanel> metaxFormViewPanels = view.getMetaxFormViewPanels();
        if(metaxFormViewPanels != null)
        {
            for(MetaxFormViewPanel panel : metaxFormViewPanels)
            {
                panel.setSys_id(null);
                panel.setMetaFormView(null);
                
                setSysIdToNullForMetaFormTabs(panel.getMetaFormTabs());
            }
        }
    }
    
    private void setSysIdToNullForMetaFormTabs(Collection<MetaFormTab> tabs)
    {
        if (tabs == null) {
        	return;
        }
    	
        for(MetaFormTab tab : tabs)
        {
            tab.setSys_id(null);
            tab.setMetaFormView(null);
            tab.setMetaxFormViewPanel(null);
            
            Collection<MetaFormTabField> metaFormTabFields = tab.getMetaFormTabFields();
            if(metaFormTabFields != null)
            {
                for(MetaFormTabField tabfield : metaFormTabFields)
                {
                    tabfield.setSys_id(null);
                    tabfield.setMetaFormTab(null);
                    
                    setSysIdToNullMetaSource(tabfield.getMetaSource());
                }
            }

            //dependencies
            setSysIdToNullMetaxFieldDependency(tab.getMetaxFieldDependencys());
        }
    }
    
    private void setSysIdToNullMetaSources(Collection<MetaSource> metaSources)
    {
        if (metaSources == null) {
        	return;
        }
            
        for(MetaSource source : metaSources)
        {
            setSysIdToNullMetaSource(source);
        }                            
    }
    
    private void setSysIdToNullMetaSource(MetaSource metaSource)
    {
        if (metaSource == null) {
        	return;
        }
        
        metaSource.setSys_id(null);
        metaSource.setMetaFormView(null);
        
        MetaFieldProperties fieldProp = metaSource.getMetaFieldProperties();
        fieldProp.setSys_id(null);
        
        setSysIdToNullMetaxFieldDependency(fieldProp.getMetaxFieldDependencys());
    }

    private void setSysIdToNullMetaxFieldDependency(Collection<MetaxFieldDependency> dependencies)
    {
        if (dependencies == null) {
        	return;
        }
        
        for(MetaxFieldDependency dependency : dependencies)
        {
            dependency.setSys_id(null);
            dependency.setMetaFieldProperties(null);
            dependency.setMetaControlItem(null);
            dependency.setMetaFormTab(null);
        }                            
    }
    
    ////////////////////////////////////////////////////////////////// END - SET SYSIDS TO NULL ////////////////////////////////////

    ////////////////////////////////////////////////////////////////// START - SAVE THE COPIED FORM ////////////////////////////////////
    
    private void persist(MetaFormView toView)
    {
        //save it
        toView.setUViewName(toViewName);
        toView.setUDisplayName(toViewName);
        toView.setUFormName(toViewName);
        HibernateUtil.getDAOFactory().getMetaFormViewDAO().persist(toView);
        
        //access rights
        MetaAccessRights accessRights = toView.getMetaAccessRights();
        accessRights.setUResourceId(toView.getSys_id());
        accessRights.setUResourceName(toViewName);
        HibernateUtil.getDAOFactory().getMetaAccessRightsDAO().persist(accessRights);
        
        toView.setMetaAccessRights(accessRights);
        
        //tabs
        toView.setMetaFormTabs(saveMetaFormTabs(toView.getMetaFormTabs(), toView, null));       
        
        //sources
        Collection<MetaSource> metaSources = toView.getMetaSources();
        if(metaSources != null)
        {
            for(MetaSource metaSource : metaSources)
            {
                saveMetaSource(metaSource, toView);
            }
            
            toView.setMetaSources(metaSources);
        }
        
        //control 
        toView.setMetaControl(saveMetaControl(toView.getMetaControl()));
        
        //panels
        Collection<MetaxFormViewPanel> panels = toView.getMetaxFormViewPanels();
        if(panels != null)
        {
            for(MetaxFormViewPanel panel : panels)
            {
                panel.setMetaFormView(toView);
                HibernateUtil.getDAOFactory().getMetaxFormViewPanelDAO().persist(panel);

                //save tabs
                panel.setMetaFormTabs(saveMetaFormTabs(panel.getMetaFormTabs(), toView, panel));                
            }
        }        
    }
    
    private MetaControl saveMetaControl(MetaControl metaControl)
    {
        if(metaControl != null)
        {
            metaControl.setUName(toViewName);
            HibernateUtil.getDAOFactory().getMetaControlDAO().persist(metaControl);
            
            Collection<MetaControlItem> metaControlItems = metaControl.getMetaControlItems();
            if(metaControlItems != null)
            {
                for(MetaControlItem item : metaControlItems)
                {
                    item.setMetaControl(metaControl);
                    HibernateUtil.getDAOFactory().getMetaControlItemDAO().persist(item);
                    
                    Collection<MetaFormAction> metaFormActions = item.getMetaFormActions();
                    if(metaFormActions != null)
                    {
                        for(MetaFormAction action : metaFormActions)
                        {
                            action.setMetaControlItem(item);
                            HibernateUtil.getDAOFactory().getMetaFormActionDAO().persist(action);
                        }                        
                    }                    
                    
                    Collection<MetaxFieldDependency> metaxFieldDependencys = item.getMetaxFieldDependencys();
                    if(metaxFieldDependencys != null)
                    {
                        for(MetaxFieldDependency dependency : metaxFieldDependencys)
                        {
                            dependency.setMetaFieldProperties(null);
                            dependency.setMetaFormTab(null);
                            dependency.setMetaControlItem(item);
                            
                            HibernateUtil.getDAOFactory().getMetaxFieldDependencyDAO().persist(dependency);
                        }
                    }     
                    
                    item.setMetaFormActions(metaFormActions);
                    item.setMetaxFieldDependencys(metaxFieldDependencys);
                }
                
                metaControl.setMetaControlItems(metaControlItems);
            }
        }//end of if
        
        return metaControl;
    }
    
    private Collection<MetaFormTab> saveMetaFormTabs(Collection<MetaFormTab> tabs, MetaFormView toView, MetaxFormViewPanel panel)
    {
        if (tabs == null) {
        	return null;
        }
            
        for(MetaFormTab tab : tabs)
        {
            tab.setMetaxFormViewPanel(panel);
            HibernateUtil.getDAOFactory().getMetaFormTabDAO().persist(tab);

            Collection<MetaFormTabField> metaFormTabFields = tab.getMetaFormTabFields();
            if(metaFormTabFields != null)
            {
                for(MetaFormTabField tabfield : metaFormTabFields)
                {
                    tabfield.setMetaSource(saveMetaSource(tabfield.getMetaSource(), toView));
                    
                    MetaField metaField = tabfield.getMetaField();
                    if(metaField != null)
                    {
                        tabfield.setMetaField(metaField);
                    }
                    
                    tabfield.setMetaFormTab(tab);
                    HibernateUtil.getDAOFactory().getMetaFormTabFieldDAO().persist(tabfield);
                }
                
                tab.setMetaFormTabFields(metaFormTabFields);
            }
            
            //dependencies
            Collection<MetaxFieldDependency> dependencies = tab.getMetaxFieldDependencys();
            if(dependencies != null)
            {
                for(MetaxFieldDependency dependency : dependencies)
                {
                    dependency.setMetaFieldProperties(null);
                    dependency.setMetaControlItem(null);
                    dependency.setMetaFormTab(tab);
                    
                    HibernateUtil.getDAOFactory().getMetaxFieldDependencyDAO().persist(dependency);
                }
                
                tab.setMetaxFieldDependencys(dependencies);
            }
            
        }
        
        return tabs;
    }
    
    private MetaSource saveMetaSource(MetaSource metaSource, MetaFormView toView)
    {
        if (metaSource != null)
        {
            metaSource.setMetaFormView(toView);                                    
            HibernateUtil.getDAOFactory().getMetaSourceDAO().persist(metaSource);
            
            MetaFieldProperties fieldProp = metaSource.getMetaFieldProperties();
            HibernateUtil.getDAOFactory().getMetaFieldPropertiesDAO().persist(fieldProp);
            
            Collection<MetaxFieldDependency> dependencies = fieldProp.getMetaxFieldDependencys();
            if(dependencies != null)
            {
                for(MetaxFieldDependency dependency : dependencies)
                {
                    dependency.setMetaFieldProperties(fieldProp);
                    dependency.setMetaControlItem(null);
                    dependency.setMetaFormTab(null);

                    HibernateUtil.getDAOFactory().getMetaxFieldDependencyDAO().persist(dependency);
                }
                
                fieldProp.setMetaxFieldDependencys(dependencies);
            }
            
            metaSource.setMetaFieldProperties(fieldProp);
        }
        
        return metaSource;
    }
}
