/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.workflow.form;

import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.CustomFormUIType;
import com.resolve.services.hibernate.customtable.CreateFileUploadCustomTable;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.form.RsColumnDTO;
import com.resolve.services.vo.form.RsCustomTableDTO;
import com.resolve.services.vo.form.RsFormDTO;
import com.resolve.services.vo.form.RsPanelDTO;
import com.resolve.services.vo.form.RsTabDTO;
import com.resolve.services.vo.form.RsUIField;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExtSaveMetaFormView
{
    private RsFormDTO viewx = null;
    private String userId = null;

//    private MetaFormView dbView = null;
//    private MetaAccessRights viewAccessRights = null;// reference is kept global
//                                                     // as this is used for all
//                                                     // the SOURCE fields also

    // this is a list of sysid of INPUT types that are valid, anything that is
    // not in this list will be deleted as it is not referenced on the form
//    private List<String> validMetaSourceSysIds = new ArrayList<String>();
//
//    private CustomTable customTable = null;
    private String referenceTableNames = null;

    public ExtSaveMetaFormView(RsFormDTO viewx, String userId)
    {
        // convertToJsonTest(viewx);

        this.viewx = viewx;
        this.userId = userId;

    }// ExtSaveMetaFormView

    public ResponseDTO save()
    {
        // this.viewx.setTimezone("GMT-07:00");//all dates are coming in GMT
        // from the UI
        this.viewx.setUsername(userId);

        Log.log.debug("================      this data coming from FORM UI to save");
        ExtSaveMetaFormView.convertToJsonTest(viewx);// TESTING - delete it
                                                     // later on
        Log.log.debug("================     END OF  this data coming from FORM UI to save");

        ResponseDTO result = new ResponseDTO();

        try
        {
            // operations before saving the form
            preSaveOperations();

            //save it
            viewx = ServiceHibernate.saveMetaFormView(viewx, userId);

            result.setSuccess(true).setData(viewx);
        }
        catch (Throwable t)
        {
            result.setSuccess(false).setMessage("Error for the form :" + viewx.getViewName() + ". " + t.getMessage());

            Log.log.error(t.getMessage(), t);
        }

//        postSaveOperations();

        return result;
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private apis
    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void preSaveOperations() throws Exception
    {
        // create the FileUpload table if the FileUploadwidget is there on the
        // form
        doWidgetOperations();

        // create the custom table
        createCustomTableFromForm();

    }

//    private void postSaveOperations()
//    {
//        // TODO:post operation
//        // 1) create new wiki document for this form
//        createWikiDocument(viewx.getViewName(), viewx.getWikiName());
//
//        // update the name of the form in the template
//        updateTheTemplateTable();
//    }

    // api to go through all the widgets and do the pre requisite steps
    private void doWidgetOperations() throws Exception
    {
        if (this.viewx.getPanels() != null)
        {
            for (RsPanelDTO panel : this.viewx.getPanels())
            {
                // just a panel
                if (panel.getColumns() != null)
                {
                    preprocessFields(panel.getColumns());
                }
                // has tab panels
                else if (panel.getTabs() != null)
                {
                    for (RsTabDTO tab : panel.getTabs())
                    {
                        if (tab.getColumns() != null)
                        {
                            preprocessFields(tab.getColumns());
                        }
                    }
                }
            }// end of for
        }
    }

    private void preprocessFields(List<RsColumnDTO> columns) throws Exception
    {
        for (RsColumnDTO column : columns)
        {
            if (column.getFields() != null)
            {
                for (RsUIField field : column.getFields())
                {
                    String uiType = field.getUiType();

                    if (CustomFormUIType.FileUploadField.name().equalsIgnoreCase(uiType))
                    {
                        prepareFileUploadField(field);
                    }

                    if (CustomFormUIType.TabContainerField.name().equalsIgnoreCase(uiType))
                    {
                        // Check to see if the popup contains a FileManager type
                        String titles = field.getRefGridViewTitle();
                        if (titles != null && titles.length() > 0)
                        {
                            String[] vals = titles.split("\\|");
                            for (String val : vals)
                            {
                                if ("FileManager".equalsIgnoreCase(val))
                                {
                                    prepareFileUploadField(field);
                                }
                            }
                        }
                    }
                }// end of for loop
            }// end of if
        }// end of for loop
    }

    private void prepareFileUploadField(RsUIField field) throws Exception
    {
        // create it only if it is new field
        if (StringUtils.isBlank(field.getId()))
        {
            // create the table
            String tableName = field.getFileUploadTableName();
            String customTableName = this.viewx.getTableName();
            String referenceColumnName = "u_" + customTableName.toLowerCase() + "_sys_id";

            // set the value of the column
            field.setFileUploadTableName(tableName);
            field.setReferenceColumnName(referenceColumnName);

            // create the table
//            CustomTableUtil.createTableForCustomTable(tableName, referenceColumnName);
            new CreateFileUploadCustomTable(tableName, referenceColumnName, null, null).create();

            // update the referenceTable field
            if (StringUtils.isNotBlank(referenceTableNames))
            {
                referenceTableNames += "," + tableName;
            }
            else
            {
                referenceTableNames = tableName;
            }
        }
    }

    private void createCustomTableFromForm() throws Exception
    {
        this.viewx.setReferenceTableNames(referenceTableNames);

        //New Implementation
        String customTableName = this.viewx.getTableName();
        if (StringUtils.isNotEmpty(customTableName))
        {
            RsCustomTableDTO customTableMetaData = this.viewx.u_getCustomTable();

            System.out.println("this is to update the meta tables");
            ExtSaveMetaFormView.convertToJsonTest(customTableMetaData);// TESTING - delete it later on

            try
            {
                // new WorkflowMetaService().saveCustomTableModel(dbMetaData);
                ServiceHibernate.createCustomTable(customTableMetaData, userId, null);

                // load the custom table as if it is a new table, this will have sys_ids now which won't be there when the form is getting saved.
//                customTable = ServiceHibernate.findCustomTableAndLoadAllReferences(null, customTableName);
            }
            catch (Throwable e)
            {
                Log.log.error("Error in updating the meta tables when form is updated for form:" + this.viewx.getDisplayName(), e);
                throw new Exception(e.getMessage());
            }
        }        
        
        ///////////////////////////////////////////
        
        
        // TODO:
        // check if the form has a table mapped to it,
        // then first persist the table and then proceed
//        ResolveBaseModel dbMetaData = this.viewx.convertToBaseModel();
//
//        System.out.println("this is to update the meta tables");
//        ExtSaveMetaFormView.convertToJsonTest(dbMetaData);// TESTING - delete it
//                                                          // later on
//        try
//        {
//            String customTableName = this.viewx.getTableName();
//            if (StringUtils.isNotEmpty(customTableName))
//            {
//                // new WorkflowMetaService().saveCustomTableModel(dbMetaData);
//                new CreateCustomTableFromForm(dbMetaData).save(this.viewx.getFormAccessRights(), this.viewx.getUsername());
//
//                // load the custom table as if it is a new table, this will have
//                // sys_ids now which won't be there when the form is getting
//                // saved.
//                customTable = dao.findCustomTableAndLoadAllReferences(customTableName);
//            }
//        }
//        catch (Exception e)
//        {
//            Log.log.error("Error in updating the meta tables when form is updated for form:" + this.viewx.getDisplayName(), e);
//            throw e;
//        }
    }

//    private void convertViewToMetaFormView()
//    {
//        if (viewx != null)
//        {
//            String id = viewx.getId();
//            if (StringUtils.isNotEmpty(id))
//            {
//                // update
//                dbView = StoreUtility.findMetaFormView(id);
//            }
//            else
//            {
//                // insert - create a new view
//                dbView = new MetaFormView();
//            }
//
//            dbView.setUFormName(viewx.getFormName());
//            dbView.setUIsWizard(viewx.isWizard());
//            dbView.setUViewName(viewx.getViewName());
//
//            dbView.setUDisplayName(viewx.getDisplayName());
//            dbView.setUWindowTitle(viewx.getWindowTitle());
//            dbView.setUWikiName(viewx.getWikiName());
//            dbView.setUTableName(viewx.getTableName());
//            dbView.setUTableDisplayName(viewx.getTableDisplayName());
//            HibernateUtil.getDAOFactory().getMetaFormViewDAO().persist(dbView);
//
//            // access rights for the form
//            viewAccessRights = convertViewToMetaAccessRights(dbView, viewx.getViewRoles(), viewx.getEditRoles(), viewx.getAdminRoles(), dbView.getMetaAccessRights());
//            dbView.setMetaAccessRights(viewAccessRights);
//
//            // for the button panel
//            dbView.setMetaControl(convertViewToMetaControl(viewx.getButtonPanel(), dbView.getMetaControl()));
//
//            // set the additional properties of the form
//            dbView.setMetaFormViewProperties(convertViewToMetaFormViewProperties(viewx.getProperties(), dbView.getMetaFormViewProperties()));
//
//            // get the list of tabs for this form
//            dbView.setMetaxFormViewPanels(convertViewToMetaxFormViewPanels(dbView, viewx.getPanels(), dbView.getMetaxFormViewPanels()));
//
//            // cleanup
//            cleanup();
//
//            // save again with all the references
//            HibernateUtil.getDAOFactory().getMetaFormViewDAO().persist(dbView);
//
//        }// end of if
//    }// convertUiViewToDbModel

//    private MetaAccessRights convertViewToMetaAccessRights(MetaFormView parentObject, String viewRights, String editRights, String adminRights, MetaAccessRights dbMetaAccess)
//    {
//        if (dbMetaAccess == null)
//        {
//            dbMetaAccess = new MetaAccessRights();
//        }
//        dbMetaAccess.setUResourceId(parentObject.getsys_id());
//        dbMetaAccess.setUResourceType(MetaFormView.RESOURCE_TYPE);
//        dbMetaAccess.setUResourceName(parentObject.getUViewName());
//        dbMetaAccess.setUReadAccess(viewRights);
//        dbMetaAccess.setUWriteAccess(editRights);
//        dbMetaAccess.setUAdminAccess(adminRights);
//        // dbMetaAccess.setUExecuteAccess("admin");
//
//        HibernateUtil.getDAOFactory().getMetaAccessRightsDAO().persist(dbMetaAccess);
//
//        return dbMetaAccess;
//    }// convertViewToMetaAccessRights

//    private MetaControl convertViewToMetaControl(RsButtonPanelDTO viewObj, MetaControl dbObj)
//    {
//        if (dbObj == null)
//        {
//            dbObj = new MetaControl();
//        }
//
//        // this is for convenience
//        dbObj.setUName(dbView.getUViewName());// viewObj.getName());
//        dbObj.setUDisplayName(dbView.getUDisplayName());// viewObj.getDisplayName());
//
//        // persist it right away
//        HibernateUtil.getDAOFactory().getMetaControlDAO().persist(dbObj);
//
//        // add the items
//        dbObj.setMetaControlItems(convertViewToMetaControlItems(dbObj, viewObj.getControls(), dbObj.getMetaControlItems()));
//
//        return dbObj;
//
//    }// convertMetaControlFromUIToDbModel

//    private Collection<MetaControlItem> convertViewToMetaControlItems(MetaControl parentMetaControl, Collection<RsUIButton> viewObjs, Collection<MetaControlItem> dbObjs)
//    {
//        Collection<MetaControlItem> newDbObjs = new ArrayList<MetaControlItem>();
//
//        if (viewObjs != null)
//        {
//            int position = 1;
//            for (RsUIButton item : viewObjs)
//            {
//                MetaControlItem dbItem = null;
//                String id = item.getId();
//                if (StringUtils.isNotEmpty(id))
//                {
//                    dbItem = SaveMetaFormView.findMetaControlItem(id, dbObjs);
//                }
//
//                if (dbItem == null)
//                {
//                    dbItem = new MetaControlItem();
//                }
//
//                dbItem.setUName(item.getName());
//                dbItem.setUDisplayName(item.getDisplayName());
//                dbItem.setUType(StringUtils.isNotEmpty(item.getType()) ? item.getType() : "button");
//                // dbItem.setUParam(item.getParam());
//                dbItem.setUSequence(position);
//                dbItem.setMetaControl(parentMetaControl);
//
//                // persist it right away
//                HibernateUtil.getDAOFactory().getMetaControlItemDAO().persist(dbItem);
//
//                // add the form actions
//                dbItem.setMetaFormActions(convertViewToMetaFormActions(dbItem, item.getActions(), dbItem.getMetaFormActions()));
//
//                // add the dependencies
//                dbItem.setMetaxFieldDependencys(convertViewToMetaxFieldDependency(dbItem, item.getDependencies(), dbItem.getMetaxFieldDependencys()));
//
//                newDbObjs.add(dbItem);
//                position++;
//
//            }// end of for
//
//            SaveMetaFormView.filterOutControlItemsToDelete(newDbObjs, dbObjs);
//
//        }
//        else
//        {
//            // make sure that its null then
//            newDbObjs = null;
//        }
//
//        return newDbObjs;
//    }// convertViewToMetaControlItems

//    private Collection<MetaFormAction> convertViewToMetaFormActions(MetaControlItem parentMetaControlItem, Collection<RsButtonAction> viewObjs, Collection<MetaFormAction> dbObjs)
//    {
//        Collection<MetaFormAction> newDbObjs = new ArrayList<MetaFormAction>();
//
//        if (viewObjs != null)
//        {
//            int position = 1;
//            for (RsButtonAction item : viewObjs)
//            {
//                MetaFormAction dbItem = null;
//                String id = item.getId();
//                if (StringUtils.isNotEmpty(id))
//                {
//                    dbItem = SaveMetaFormView.findMetaFormAction(id, dbObjs);
//                }
//
//                if (dbItem == null)
//                {
//                    dbItem = new MetaFormAction();
//                }
//
//                dbItem.setUOperation(item.getOperation());
//                dbItem.setUCrudAction(item.getCrudAction());
//                dbItem.setURunbookName(item.getRunbook());
//                dbItem.setUScriptName(item.getScript());
//                dbItem.setUActionTaskName(item.getActionTask());
//                dbItem.setUAdditionalParams(item.getAdditionalParam());
//                dbItem.setUSequence(position);
//                dbItem.setMetaControlItem(parentMetaControlItem);
//                dbItem.setURedirectUrl(item.getRedirectUrl());
//                dbItem.setURedirectTarget(item.getRedirectTarget());
//
//                newDbObjs.add(dbItem);
//                position++;
//
//                // persist it right away
//                HibernateUtil.getDAOFactory().getMetaFormActionDAO().persist(dbItem);
//
//            }// end of for
//        }
//        else
//        {
//            newDbObjs = null;
//        }
//
//        // clenaup that is not required
//        SaveMetaFormView.filterOutFormActionItemsToDelete(newDbObjs, dbObjs);
//
//        return newDbObjs;
//    }// convertViewToMetaFormActions
//
//    private Collection<MetaxFieldDependency> convertViewToMetaxFieldDependency(MetaControlItem parentMetaControlItem, Collection<RsFieldDependency> viewObjs, Collection<MetaxFieldDependency> dbObjs)
//    {
//        Collection<MetaxFieldDependency> newDbObjs = new ArrayList<MetaxFieldDependency>();
//
//        if (viewObjs != null)
//        {
//            for (RsFieldDependency item : viewObjs)
//            {
//                MetaxFieldDependency dbItem = getMetaxFieldDependency(item, dbObjs);
//                dbItem.setMetaControlItem(parentMetaControlItem);
//
//                // persist it right away
//                HibernateUtil.getDAOFactory().getMetaxFieldDependencyDAO().persist(dbItem);
//                newDbObjs.add(dbItem);
//            }// end of for loop
//        }
//        else
//        {
//            newDbObjs = null;
//        }
//
//        // clenaup that is not required
//        filterOutMetaxFieldDependencyToDelete(newDbObjs, dbObjs);
//
//        return newDbObjs;
//    }
//
//    private static MetaxFieldDependency getMetaxFieldDependency(RsFieldDependency item, Collection<MetaxFieldDependency> dbObjs)
//    {
//        MetaxFieldDependency dbItem = null;
//
//        String id = item.getId();
//        if (StringUtils.isNotEmpty(id))
//        {
//            dbItem = findMetaxFieldDependency(id, dbObjs);
//        }
//
//        if (dbItem == null)
//        {
//            dbItem = new MetaxFieldDependency();
//        }
//
//        dbItem.setUAction(item.getAction());
//        dbItem.setUCondition(item.getCondition());
//        dbItem.setUTarget(item.getTarget());
//        dbItem.setUValue(item.getValue());
//        dbItem.setUActionOptions(item.getActionOptions());
//
//        return dbItem;
//    }

//    private MetaFormViewProperties convertViewToMetaFormViewProperties(RsFormProperties viewObj, MetaFormViewProperties dbObj)
//    {
//        if (dbObj == null)
//        {
//            dbObj = new MetaFormViewProperties();
//        }
//
//        if (viewObj != null)
//        {
//            dbObj.setULabelWidth(viewObj.getLabelWidth());
//            dbObj.setULabelAlign(viewObj.getLabelAlign());
//        }
//
//        // persist it right away
//        HibernateUtil.getDAOFactory().getMetaFormViewPropertiesDAO().persist(dbObj);
//
//        return dbObj;
//    }// convertViewToMetaFormViewProperties

//    private List<MetaxFormViewPanel> convertViewToMetaxFormViewPanels(MetaFormView parent, List<RsPanelDTO> viewObjs, Collection<MetaxFormViewPanel> dbObjs)
//    {
//        List<MetaxFormViewPanel> newDbObjs = new ArrayList<MetaxFormViewPanel>();
//
//        if (viewObjs != null && viewObjs.size() > 0)
//        {
//            int position = 1;
//            for (RsPanelDTO uipanel : viewObjs)
//            {
//                MetaxFormViewPanel dbItem = null;
//                String id = uipanel.getId();
//                if (StringUtils.isNotEmpty(id))
//                {
//                    dbItem = findMetaxFormViewPanel(id, dbObjs);
//                }
//
//                if (dbItem == null)
//                {
//                    dbItem = new MetaxFormViewPanel();
//                }
//
//                // boolean isTabPanel = uipanel.getIsTabPanel();//ALWAYS WILL BE
//                // TRUE as all the Forms will have atlease 1 panel
//
//                dbItem.setUPanelTitle(uipanel.getDisplayName());
//                dbItem.setUOrder(position);
//                dbItem.setUUrl(uipanel.getUrl());
//                dbItem.setUNoOfVerticalColumns(uipanel.getCols());
//
//                dbItem.setMetaFormView(parent);
//
//                // persist it right away
//                HibernateUtil.getDAOFactory().getMetaxFormViewPanelDAO().persist(dbItem);
//
//                dbItem.setMetaFormTabs(convertUITabPanelToDbModel(uipanel.getTabs(), dbItem.getMetaFormTabs(), dbItem));
//                HibernateUtil.getDAOFactory().getMetaxFormViewPanelDAO().persist(dbItem);// save
//                                                                                         // again
//
//                // add it to the collection
//                newDbObjs.add(dbItem);
//                position++;
//            }// end of for loop
//        }
//        else
//        {
//            newDbObjs = null;
//        }
//
//        // clenaup that is not required
//        // SaveMetaFormView.filterOutFormActionItemsToDelete(newDbObjs, dbObjs);
//
//        return newDbObjs;
//    }// convertViewToMetaxFormViewPanels

//    private Collection<MetaFormTab> convertUITabPanelToDbModel(Collection<RsTabDTO> viewObjs, Collection<MetaFormTab> dbObjs, MetaxFormViewPanel parent)
//    {
//        Collection<MetaFormTab> newDbObjs = new ArrayList<MetaFormTab>();
//
//        if (viewObjs != null)
//        {
//            MetaFormTab dbItem = null;
//            int orderNo = 1;
//            for (RsTabDTO item : viewObjs)
//            {
//                dbItem = convertUITabToDBTab(item, dbObjs, orderNo, parent);
//
//                if (dbItem != null)
//                {
//                    newDbObjs.add(dbItem);
//                }
//
//                orderNo++;
//
//            }// end of for
//
//            // delete the tabs that were before and now deleted by the user
//            SaveMetaFormView.filterOutTabsToDelete(newDbObjs, dbObjs);
//
//        }
//        else
//        {
//            // make sure that its null then
//            newDbObjs = null;
//        }
//        return newDbObjs;
//
//    }// convertMetaFormTabFromUIToDbModel

//    private MetaFormTab convertUITabToDBTab(RsTabDTO item, Collection<MetaFormTab> dbObjs, int orderNo, MetaxFormViewPanel parent)
//    {
//        MetaFormTab dbItem = null;
//
//        String id = item.getId();
//        if (StringUtils.isNotEmpty(id))
//        {
//            dbItem = SaveMetaFormView.findMetaFormTab(id, dbObjs);
//        }
//        else
//        {
//            dbItem = new MetaFormTab();
//        }
//        dbItem.setUTabName(item.getName());
//        dbItem.setUOrder(orderNo);
//        // dbItem.setUPosition(item.getPosition());
//        // dbItem.setUTabWidth(item.getTabWidth());
//        // dbItem.setUIsSelectedDefault(item.isSelectedDefault());
//        dbItem.setUNoOfVerticalColumns(item.getCols());
//        dbItem.setUUrl(item.getUrl());
//        // dbItem.setMetaFormView(dbView);//**DO NOT SET THIS AS THIS IS NOT THE
//        // PARENT ANYMORE
//        dbItem.setMetaxFormViewPanel(parent);
//
//        // persist it right away
//        HibernateUtil.getDAOFactory().getMetaFormTabDAO().persist(dbItem);
//
//        // delete the existing references
//        // delete all the records from meta_form_tab_field if any as this tab is
//        // URL and not have any fields
//        deleteMetaFormTabFields(dbItem.getMetaFormTabFields());
//
//        // manipulate the tab fields
//        if (StringUtils.isEmpty(dbItem.getUUrl()))
//        {
//
//            List<MetaFormTabField> newdbFields = new ArrayList<MetaFormTabField>();
//            int col = 1;
//            for (RsColumnDTO column : item.getColumns())
//            {
//                newdbFields.addAll(createMetaFormTabFieldFromUIFields(dbItem, column.getFields(), col));
//                col++;
//            }// end of for loop
//
//            dbItem.setMetaFormTabFields(newdbFields);
//
//            // deleteMetaSourceThatAreNotReferencedInForm(newdbFields);
//        }
//        // else
//        // {
//        // //delete all the records from meta_form_tab_field if any as this tab
//        // is URL and not have any fields
//        // SaveMetaFormView.deleteMetaFormTabFields(dbItem.getMetaFormTabFields());
//        // }
//
//        // dependencies
//        dbItem.setMetaxFieldDependencys(convertViewToMetaxFieldDependencyForTab(dbItem, item.getDependencies(), dbItem.getMetaxFieldDependencys()));
//
//        return dbItem;
//    }

//    private Collection<MetaxFieldDependency> convertViewToMetaxFieldDependencyForTab(MetaFormTab parentMetaFormTab, Collection<RsFieldDependency> viewObjs, Collection<MetaxFieldDependency> dbObjs)
//    {
//        Collection<MetaxFieldDependency> newDbObjs = new ArrayList<MetaxFieldDependency>();
//
//        if (viewObjs != null)
//        {
//            for (RsFieldDependency item : viewObjs)
//            {
//                MetaxFieldDependency dbItem = getMetaxFieldDependency(item, dbObjs);
//                dbItem.setMetaFormTab(parentMetaFormTab);
//
//                // persist it right away
//                HibernateUtil.getDAOFactory().getMetaxFieldDependencyDAO().persist(dbItem);
//                newDbObjs.add(dbItem);
//            }// end of for loop
//        }
//        else
//        {
//            newDbObjs = null;
//        }
//
//        // clenaup that is not required
//        filterOutMetaxFieldDependencyToDelete(newDbObjs, dbObjs);
//
//        return newDbObjs;
//    }

//    private List<MetaFormTabField> createMetaFormTabFieldFromUIFields(MetaFormTab parent, List<RsUIField> fields, int colNumber)
//    {
//        List<MetaFormTabField> newdbFields = new ArrayList<MetaFormTabField>();
//
//        if (fields != null && fields.size() > 0)
//        {
//            int fieldOrderNo = 1;
//            for (RsUIField item : fields)
//            {
//                if (StringUtils.isEmpty(item.getSourceType()))
//                {
//                    throw new RuntimeException("Source type (DB or INPUT) is mandatory.");
//                }
//
//                MetaFormTabField newdbItem = new MetaFormTabField();
//
//                newdbItem = new MetaFormTabField();
//                newdbItem.setUOrder(fieldOrderNo);// (item.getOrderNumber());
//                newdbItem.setUColumnNumber(colNumber);
//                newdbItem.setMetaFormTab(parent);
//
//                if (item.getSourceType().equalsIgnoreCase(DataSource.DB.getTagName()))
//                {
//                    // DB - will always have a id
//                    // MetaField metaField =
//                    // HibernateUtil.getDAOFactory().getMetaFieldDAO().findById(item.getDbcolumnId());
//                    MetaField metaField = findMetaField(item.getName());
//                    newdbItem.setMetaField(metaField);
//
//                    // create the MetaFieldProperties for this form
//                    MetaFieldProperties metaFieldProperties = convertINPUTUIToDbModelProperties(item, null, null, true);
//                    newdbItem.setMetaFieldFormProperties(metaFieldProperties);
//
//                }
//                else
//                {
//                    // source
//                    MetaSource metaSource = null;
//                    if (StringUtils.isNotEmpty(item.getId()))
//                    {
//                        metaSource = HibernateUtil.getDAOFactory().getMetaSourceDAO().findById(item.getId());
//                    }
//                    else
//                    {
//                        metaSource = new MetaSource();
//                    }
//
//                    metaSource = convertINPUTUIToDbModel(item, metaSource);
//
//                    newdbItem.setMetaSource(metaSource);
//                    validMetaSourceSysIds.add(metaSource.getsys_id());
//                }
//
//                // persist it right away
//                HibernateUtil.getDAOFactory().getMetaFormTabFieldDAO().persist(newdbItem);
//
//                // add to the collection
//                newdbFields.add(newdbItem);
//
//                fieldOrderNo++;
//            }
//        }
//
//        return newdbFields;
//    }

//    private static MetaxFormViewPanel findMetaxFormViewPanel(String id, Collection<MetaxFormViewPanel> dbObjs)
//    {
//        MetaxFormViewPanel result = null;
//
//        if (dbObjs != null)
//        {
//            for (MetaxFormViewPanel item : dbObjs)
//            {
//                if (item.getsys_id().equalsIgnoreCase(id))
//                {
//                    result = item;
//                    break;
//                }
//            }// end of for
//        }// end of if
//
//        return result;
//    }// findMetaxFormViewPanel

//    private MetaSource convertINPUTUIToDbModel(RsUIField item, MetaSource dbItem)
//    {
//        if (dbItem == null)
//        {
//            dbItem = new MetaSource();
//        }
//
//        dbItem.setUName(item.getName());
//        dbItem.setUDisplayName(item.getDisplayName());
//        dbItem.setUSource(DataSource.INPUT.name());
//        dbItem.setMetaFormView(dbView);
//        dbItem.setUDbType(item.getDbtype());
//
//        // u_type is same as the u_ui_type
//        // dbItem.setUType(item.getRstype());
//
//        // persist it
//        HibernateUtil.getDAOFactory().getMetaSourceDAO().persist(dbItem);
//        HibernateUtil.getCurrentSession().flush();
//
//        dbItem.setMetaFieldProperties(convertINPUTUIToDbModelProperties(item, dbItem, dbItem.getMetaFieldProperties(), true));
//
//        return dbItem;
//    }

//    public static MetaFieldProperties convertINPUTUIToDbModelProperties(RsUIField uifield, MetaSource dbItem, MetaFieldProperties prop, boolean isCustomForm)
//    {
//        if (prop == null)
//        {
//            prop = new MetaFieldProperties();
//        }
//
//        // u_type is same as the u_ui_type
//        if (dbItem != null)
//        {
//            dbItem.setUType(uifield.getUiType());
//        }
//
//        prop.setUUIType(uifield.getUiType());
//        // prop.setUUIXype(uifield.getUiXtype());
//        prop.setUJavascript(uifield.getJs());
//        prop.setUSize(uifield.getSize());
//        prop.setUIsMandatory(uifield.isMandatory());
//        prop.setUIsCrypt(uifield.isEncrypted());
//        prop.setUDefaultValue(uifield.getDefaultValue());
//        prop.setUWidth(uifield.getWidth());
//        prop.setUHeight(uifield.getHeight());
//        prop.setULabelAlign(uifield.getLabelAlign());
//
//        prop.setUName(uifield.getName());
//        prop.setUOrder(uifield.getOrderNumber());
//        prop.setUTable(uifield.getDbtable());
//
//        prop.setUBooleanMaxLength(uifield.getBooleanMaxLength());
//        prop.setUStringMinLength(uifield.getStringMinLength());
//        prop.setUStringMaxLength(uifield.getStringMaxLength());
//        prop.setUUIStringMaxLength(uifield.getUiStringMaxLength());
//        prop.setUSelectIsMultiSelect(uifield.getIsMultiSelect());
//        prop.setUSelectUserInput(uifield.getSelectUserInput());
//        prop.setUSelectMaxDisplay(uifield.getSelectMaxDisplay());
//        prop.setUSelectValues(uifield.getSelectValues());
//        prop.setUChoiceValues(uifield.getChoiceValues());
//        prop.setUCheckboxValues(uifield.getCheckboxValues());
//        prop.setUDateTimeHasCalendar(uifield.getDateTimeHasCalendar());
//        prop.setUIntegerMinValue(uifield.getIntegerMinValue());
//        prop.setUIntegerMaxValue(uifield.getIntegerMaxValue());
//        prop.setUDecimalMinValue(uifield.getDecimalMinValue());
//        prop.setUDecimalMaxValue(uifield.getDecimalMaxValue());
//        prop.setUJournalRows(uifield.getJournalRows());
//        prop.setUJournalColumns(uifield.getJournalColumns());
//        prop.setUJournalMinValue(uifield.getJournalMinValue());
//        prop.setUJournalMaxValue(uifield.getJournalMaxValue());
//        prop.setUListRows(uifield.getListRows());
//        prop.setUListColumns(uifield.getListColumns());
//        prop.setUListMaxDisplay(uifield.getListMaxDisplay());
//        prop.setUListValues(uifield.getListValues());
//        prop.setUReferenceTable(uifield.getReferenceTable());
//        prop.setUReferenceDisplayColumn(uifield.getReferenceDisplayColumn());
//        prop.setUReferenceDisplayColumnList(uifield.getReferenceDisplayColumnList());
//        prop.setUReferenceTarget(uifield.getReferenceTarget());
//        prop.setUReferenceParams(uifield.getReferenceParams());
//        prop.setULinkTarget(uifield.getLinkTarget());
//        prop.setULinkParams(uifield.getLinkParams());
//        prop.setULinkTemplate(uifield.getLinkTemplate());
//        prop.setUSequencePrefix(uifield.getSequencePrefix());
//        prop.setUHiddenValue(uifield.getHiddenValue());
//        
//        prop.setUChoiceTable(uifield.getChoiceTable());
//        prop.setUChoiceDisplayField(uifield.getChoiceDisplayField());
//        prop.setUChoiceValueField(uifield.getChoiceValueField());
//        prop.setUChoiceWhere(uifield.getChoiceWhere());
//        prop.setUChoiceField(uifield.getChoiceField());
//        prop.setUChoiceSql(uifield.getChoiceSql());
//        prop.setUChoiceOptionSelection(uifield.getChoiceOptionSelection());
//
//        prop.setUGroups(uifield.getGroups());
//        prop.setUUsersOfTeams(uifield.getUsersOfTeams());
//        prop.setURecurseUsersOfTeam(uifield.getRecurseUsersOfTeam());
//        prop.setUTeamsOfTeams(uifield.getTeamsOfTeams());
//        prop.setURecurseTeamsOfTeam(uifield.getRecurseTeamsOfTeam());
//        prop.setUAllowAssignToMe(uifield.getAllowAssignToMe());
//        prop.setUAutoAssignToMe(uifield.getAutoAssignToMe());
//
//        prop.setUTeamPickerTeamsOfTeams(uifield.getTeamPickerTeamsOfTeams());
//
//        prop.setUFileUploadTableName(uifield.getFileUploadTableName());
//        prop.setUFileUploadReferenceColumnName(uifield.getReferenceColumnName());
//        prop.setUAllowUpload(uifield.getAllowUpload());
//        prop.setUAllowDownload(uifield.getAllowDownload());
//        prop.setUAllowRemove(uifield.getAllowRemove());
//        prop.setUAllowFileTypes(uifield.getAllowedFileTypes());
//
//        // reference grid
//        prop.setURefGridReferenceByTable(uifield.getRefGridReferenceByTable());
//        prop.setURefGridSelectedReferenceColumns(uifield.getRefGridSelectedReferenceColumns());
//        prop.setURefGridReferenceColumnName(uifield.getRefGridReferenceColumnName());
//        prop.setUAllowReferenceTableAdd(uifield.getAllowReferenceTableAdd());
//        prop.setUAllowReferenceTableRemove(uifield.getAllowReferenceTableRemove());
//        prop.setUAllowReferenceTableView(uifield.getAllowReferenceTableView());
//        prop.setUReferenceTableUrl(uifield.getReferenceTableUrl());
//        prop.setURefGridViewPopup(uifield.getRefGridViewPopup());
//        prop.setURefGridViewPopupWidth(uifield.getRefGridViewPopupWidth());
//        prop.setURefGridViewPopupHeight(uifield.getRefGridViewPopupHeight());
//        prop.setURefGridViewPopupTitle(uifield.getRefGridViewTitle());
//        
//        // section control
//        prop.setUSectionColumns(uifield.getSectionColumns());
//        prop.setUSectionLayout(uifield.getSectionLayout());
//        prop.setUSectionNumberOfColumns(uifield.getSectionNumberOfColumns());
//        prop.setUSectionTitle(uifield.getSectionTitle());
//        prop.setUSectionType(uifield.getSectionType());
//
//        prop.setUWidgetColumns(uifield.getWidgetColumns());
//
//        prop.setUIsHidden(uifield.isHidden());
//        prop.setUIsReadOnly(uifield.isReadOnly());
//
//        prop.setUTooltip(uifield.getTooltip());
//
//        HibernateUtil.getDAOFactory().getMetaFieldPropertiesDAO().persist(prop);
//
//        // dependencies for the properties
//        if (isCustomForm)
//        {
//            prop.setMetaxFieldDependencys(convertViewToMetaxFieldDependency(prop, uifield.getDependencies(), prop.getMetaxFieldDependencys()));
//        }
//
//        // set metastyle - TODO - not sure we will do this or not
//        // prop.setMetaStyle(convertRSMetaSytleToDBModel(uifield.getRsStyle(),
//        // prop.getMetaStyle()));
//
//        return prop;
//    }// convertRSMetaSourceToDbMOdel

//    private static Collection<MetaxFieldDependency> convertViewToMetaxFieldDependency(MetaFieldProperties parentMetaFieldProperties, Collection<RsFieldDependency> viewObjs, Collection<MetaxFieldDependency> dbObjs)
//    {
//        Collection<MetaxFieldDependency> newDbObjs = new ArrayList<MetaxFieldDependency>();
//
//        if (viewObjs != null)
//        {
//            for (RsFieldDependency item : viewObjs)
//            {
//                MetaxFieldDependency dbItem = getMetaxFieldDependency(item, dbObjs);
//                dbItem.setMetaFieldProperties(parentMetaFieldProperties);
//
//                // persist it right away
//                HibernateUtil.getDAOFactory().getMetaxFieldDependencyDAO().persist(dbItem);
//                newDbObjs.add(dbItem);
//            }// end of for loop
//        }
//        else
//        {
//            newDbObjs = null;
//        }
//
//        // clenaup that is not required
//        filterOutMetaxFieldDependencyToDelete(newDbObjs, dbObjs);
//
//        return newDbObjs;
//    }
//
//    private static MetaxFieldDependency findMetaxFieldDependency(String id, Collection<MetaxFieldDependency> dbObjs)
//    {
//        MetaxFieldDependency result = null;
//
//        if (dbObjs != null)
//        {
//            for (MetaxFieldDependency item : dbObjs)
//            {
//                if (item.getsys_id().equalsIgnoreCase(id))
//                {
//                    result = item;
//                    break;
//                }
//            }// end of for
//        }// end of if
//
//        return result;
//    }// findMetaxFieldDependency
//
//    private static void filterOutMetaxFieldDependencyToDelete(Collection<MetaxFieldDependency> newDbObjs, Collection<MetaxFieldDependency> dbObjs)
//    {
//        if (dbObjs != null && dbObjs.size() > 0)
//        {
//            for (MetaxFieldDependency dependency : dbObjs)
//            {
//                if (shouldDeleteMetaxFieldDependency(newDbObjs, dependency))
//                {
//                    deleteMetaxFieldDependency(dependency);
//                }
//
//            }// end of for
//        }// end of if
//
//    }// filterOutControlItemsToDelete
//
//    private static boolean shouldDeleteMetaxFieldDependency(Collection<MetaxFieldDependency> newDbObjs, MetaxFieldDependency dependency)
//    {
//        boolean shouldDelete = true;
//        String dbId = dependency.getsys_id();
//
//        for (MetaxFieldDependency newDependency : newDbObjs)
//        {
//            String newId = newDependency.getsys_id();
//            if (StringUtils.isNotEmpty(newId))
//            {
//                if (newId.equals(dbId))
//                {
//                    shouldDelete = false;
//                    break;
//                }
//            }
//        }// end of for loop
//
//        return shouldDelete;
//
//    }// shouldDelete
//
//    private static void deleteMetaxFieldDependency(MetaxFieldDependency dependency)
//    {
//        HibernateUtil.getDAOFactory().getMetaxFieldDependencyDAO().delete(dependency);
//    }// deleteMetaFormAction

//    private void cleanup()
//    {
//        deleteMetaSourceThatAreNotReferencedInForm();
//    }
//
//    private void deleteMetaSourceThatAreNotReferencedInForm()
//    {
//        Collection<MetaSource> metaSources = this.dbView.getMetaSources();
//        if (metaSources != null && metaSources.size() > 0)
//        {
//            for (MetaSource metaSource : metaSources)
//            {
//                String sysId = metaSource.getsys_id();
//                if (!this.validMetaSourceSysIds.contains(sysId))
//                {
//                    // delete this metasource record as it is no longer
//                    // referenced on the form
//                    deleteMetaSource(metaSource);
//                }
//            }
//        }// end of if
//    }

//    private void deleteMetaSource(MetaSource metaSource)
//    {
//        if (metaSource != null)
//        {
//            DeleteMetaForm.deleteMetaFieldProperties(metaSource.getMetaFieldProperties(), null);
//
//            HibernateUtil.getDAOFactory().getMetaSourceDAO().delete(metaSource);
//        }
//
//    }

//    private void createWikiDocument(String viewName, String wikiName)
//    {
//        if (StringUtils.isNotBlank(wikiName))
//        {
//            String fullName = wikiName.trim();
//            String sectionId = SectionUtil.createSectionId();
//            String sectionContent = prepareSectionContentFor(viewName, sectionId);
//
//            // find the document
//            boolean isWikidocExist = ServiceHibernate.isWikidocExist(fullName, true);
//            if(!isWikidocExist)
//            {
//                // create doc with fullName
//                createDocumentWithName(fullName, sectionContent, this.userId);
//            }
//            else
//            {
//             // decided not to append the existing document as this can
//                // tamper it
//
//                // this is edit
//                // String currentContent = wikiDocument.getUContent();
//                // if(StringUtils.isNotBlank(currentContent))
//                // {
//                // //if the view is not defined in the content , then add it
//                // if(!(currentContent.indexOf(viewName) > -1))
//                // {
//                // currentContent += "\n";
//                // currentContent += sectionContent;
//                // updateDocument(wikiDocument, currentContent);
//                // }
//                // }
//                               
//            }
//        }
//    }

//    private String prepareSectionContentFor(String viewName, String sectionId)
//    {
//        StringBuffer sb = new StringBuffer();
//
//        sb.append("{section:type=").append(SectionType.FORM.getTagName()).append("|title=").append(viewName).append("|id=").append(sectionId).append("}").append("\n");
//
//        sb.append("#set($sys_id = $request.getParameter(\"sys_id\"))").append("\n");
//        sb.append(HtmlUtil.createFormScript(SectionUtil.createSectionId() + "_formviewer", StringUtils.isNotEmpty(viewName) ? viewName.trim() : null, null, "$!sys_id", null, 0));
//        sb.append("\n");
//
//        sb.append("{section}");
//
//        return sb.toString();
//    }
//
//    private void createDocumentWithName(String fullName, String sectionContent, String userId)
//    {
//        String[] arr = fullName.split("\\.");
//
//        Map<String, Object> params = new HashMap<String, Object>();
//        params.put(ConstantValues.USERNAME, userId);
//        params.put(ConstantValues.WIKI_NAMESPACE_KEY, arr[0]);
//        params.put(ConstantValues.WIKI_DOCNAME_KEY, arr[1]);
//        params.put(ConstantValues.WIKI_CONTENT_KEY, sectionContent);
//
//        params.put(ConstantValues.READ_ROLES_KEY, this.viewx.getViewRoles());
//        params.put(ConstantValues.WRITE_ROLES_KEY, this.viewx.getEditRoles());
//        params.put(ConstantValues.ADMIN_ROLES_KEY, this.viewx.getAdminRoles());
//
//        try
//        {
//            new MWiki().create(params);
//        }
//        catch (Exception e)
//        {
//            Log.log.error("Error creating wiki :" + fullName, e);
//        }
//
//    }

//    private void updateDocument(WikiDocument wikiDocument, String newContent)
//    {
//        Map<String, Object> params = new HashMap<String, Object>();
//        params.put(ConstantValues.USERNAME, userId);
//        params.put(ConstantValues.WIKI_NAMESPACE_KEY, wikiDocument.getUNamespace());
//        params.put(ConstantValues.WIKI_DOCNAME_KEY, wikiDocument.getUName());
//        params.put(ConstantValues.WIKI_CONTENT_KEY, newContent);
//
//        if (viewAccessRights != null)
//        {
//            params.put(ConstantValues.READ_ROLES_KEY, viewAccessRights.getUReadAccess());
//            params.put(ConstantValues.WRITE_ROLES_KEY, viewAccessRights.getUWriteAccess());
//            params.put(ConstantValues.ADMIN_ROLES_KEY, viewAccessRights.getUAdminAccess());
//        }
//
//        try
//        {
//            new MWiki().update(params);
//        }
//        catch (Exception e)
//        {
//            Log.log.error("Error creating wiki :" + wikiDocument.getUFullname(), e);
//        }
//    }

//    private MetaField findMetaField(String fieldName)
//    {
//        MetaField field = null;
//
//        if (StringUtils.isNotEmpty(fieldName))
//        {
//            // if(!fieldName.startsWith("u_"))
//            // {
//            // fieldName = "u_" + fieldName;
//            // }
//            Collection<MetaField> fields = this.customTable.getMetaFields();
//            if (fields != null && fields.size() > 0)
//            {
//                for (MetaField dbField : fields)
//                {
//                    if (fieldName.equals(dbField.getUName()))
//                    {
//                        field = dbField;
//                        break;
//                    }
//                }
//            }
//        }
//        return field;
//    }

//    private static void deleteMetaFormTabFields(Collection<MetaFormTabField> deleteMetaFormTabFields)
//    {
//        if (deleteMetaFormTabFields != null)
//        {
//            for (MetaFormTabField metaFormTabField : deleteMetaFormTabFields)
//            {
//                deleteMetaFormTabField(metaFormTabField);
//            }
//        }
//    }// deleteMetaFormTabFields

//    private static void deleteMetaFormTabField(MetaFormTabField metaFormTabField)
//    {
//        try
//        {
//            // NEED THIS FOR ORACLE as the DELETES are not COMMITTED immediately
//            HibernateUtil.beginTransaction("admin");
//
//            DeleteMetaForm.deleteMetaFieldProperties(metaFormTabField.getMetaFieldFormProperties(), null);
//
//            HibernateUtil.getDAOFactory().getMetaFormTabFieldDAO().delete(metaFormTabField);
//
//            HibernateUtil.getCurrentSession().flush();
//            HibernateUtil.commitTransaction();
//        }
//        catch (Throwable t)
//        {
//            Log.log.error(t.getMessage(), t);
//            HibernateUtil.rollbackTransaction();
//            HibernateUtil.rethrowNestedTransaction(t);
//        }
//    }// dceleteMetaFormTabField

    // FOR TESTING
    public static void convertToJsonTest(Object o)
    {
        ObjectMapper mapper = new ObjectMapper();
        try
        {
            Log.log.debug(mapper.writeValueAsString(o));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        Log.log.debug("==========================");
    }

//    private void updateTheTemplateTable()
//    {
//        // this form can be a template..so update the name in the template table
//        if (this.dbView != null)
//        {
//            String sql = "update WikiDocumentMetaFormRel set UFormName ='" + this.dbView.getUViewName() + "' where form = '" + this.dbView.getsys_id() + "'";
//            try
//            {
//                ServiceHibernate.executeHQLUpdate(sql, userId);
//            }
//            catch (Throwable t)
//            {
//                Log.log.error("Error in updating the template with the form name " + this.dbView.getUViewName(), t);
//            }
//        }
//    }

    // //for all the UI elements that create an additional table for the custom
    // table, add that over here
    // private void populateRefTableNames(ResolveBaseModel field)
    // {
    // if(field != null)
    // {
    // String uitype = field.get(RSMetaField.UITYPE);
    //
    // if(uitype.equals(CustomFormUIType.FileUploadField.name()))
    // {
    // String tableName = field.get(RSMetaField.FILE_UPLOAD_TABLE_NAME);
    // if(StringUtils.isNotBlank(tableName))
    // {
    // if(StringUtils.isNotBlank(referenceTableNames))
    // {
    // referenceTableNames = tableName;
    // }
    // else
    // {
    // referenceTableNames += "," + tableName;
    // }
    // }//end of if
    // }//end of if
    // }//end of if
    // }
    //

}
