/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.sql.Clob;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Map;

import org.hibernate.engine.jdbc.NonContextualLobCreator;

import com.resolve.services.ServiceHibernate;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CustomFormUtil
{
    public static final String USERID = "userId";
    public static final String CREATEDATE = "createDate";
    public static final String COMMENT = "note";

    private static final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");// 2012-11-15T15:45:58-08:00

    public static String appendJournalToString(String journalJson, String newComment, String username)
    {
        // replace \n with <br/>
        newComment = newComment.replaceAll("\n", "<br/>");

        // prepare the new array
        JSONArray jsonArray = convertJournalStringToObject(journalJson);

        // create a new obj
        JSONObject newJournal = new JSONObject();
        newJournal.put(USERID, username);
        newJournal.put(CREATEDATE, getUTCDefaultDate());// 2012-11-15T15:45:58-08:00
        newJournal.put(COMMENT, newComment);

        // add the new journal obj
        jsonArray.add(0, newJournal);

        // convert back to string
        journalJson = StringUtils.jsonArrayToString(jsonArray);

        return journalJson;
    }

    public static Clob appendJournal(Map entry, String fieldname, String newComment, String username)
    {
        Clob result = null;

        Clob journal = (Clob) entry.get(fieldname);
        result = ServiceHibernate.getClobFromString(appendJournalToString(ServiceHibernate.getStringFromClob(journal), newComment, username));
        entry.put(fieldname, result);

        return result;
    } // appendJournal

    public static String toStringJournalJson(String journalJson)
    {
        // prepare the new array
        JSONArray jsonObj = convertJournalStringToObject(journalJson);
        StringBuffer result = new StringBuffer();

        Iterator<JSONObject> it = jsonObj.iterator();
        while (it.hasNext())
        {
            JSONObject rec = it.next();

            if (result.length() > 0)
            {
                result.append("\n\n");
            }

            result.append(rec.getString(CREATEDATE)).append(" By ").append(rec.getString(USERID)).append("\n");
            result.append(rec.getString(COMMENT));
        }

        return result.toString();

    }

    public static String getUTCDefaultDate()
    {
        String utcdate = DEFAULT_DATE_FORMAT.format(GMTDate.getDate());
        String date = utcdate.substring(0, utcdate.length() - 2) + ":" + utcdate.substring(utcdate.length() - 2);
        return date;
    }

    private static JSONArray convertJournalStringToObject(String journalJson)
    {
        journalJson = StringUtils.isNotEmpty(journalJson) ? journalJson.trim() : "";

        JSONArray jsonObj = StringUtils.stringToJSONArray(journalJson);
        if (jsonObj == null)
        {
            jsonObj = new JSONArray();
        }

        return jsonObj;
    }

    public static Clob toClob(String str)
    {
        return NonContextualLobCreator.INSTANCE.wrap(NonContextualLobCreator.INSTANCE.createClob(str));
    } // toClob

    /*
    public static void main(String[] args)
    {
        GMTDate.initTimezone(null);
        // [{"userId":"admin","createDate":"2012-11-15T15:45:58-08:00","note":"some more notes"},{"userId":"admin","createDate":"2012-10-31T10:48:21-07:00","note":"tests"}]
        String json = "[{\"userId\":\"admin\",\"createDate\":\"2012-11-15T15:45:58-08:00\",\"note\":\"some more notes\"},{\"userId\":\"admin\",\"createDate\":\"2012-10-31T10:48:21-07:00\",\"note\":\"tests\"}]";
        // System.out.println(appendJournal(json, "new comment", "admin"));
        // System.out.println(toStringJournalData(json));
    }
    */

}