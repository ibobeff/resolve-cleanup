/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomFormDataConversionUtil
{
    public static final String CT_ADDRESS1 = "address1";
    public static final String CT_ADDRESS2 = "address2";
    public static final String CT_CITY = "city";
    public static final String CT_STATE = "state";
    public static final String CT_ZIP = "zip";
    public static final String CT_COUNTRY = "country";
    
	/*
    public static void main(String[] args)
    {
        String[] arr = {"Option 1","Option 2","Option 3"};
        String str = "Option 1|&|Option 2|&|Option 3";
        
        System.out.println(arrayToString(arr));
        
        System.out.println(stringToArray(str));
        System.out.println(stringToList(str));
        
        String strAddress = "sdfasdfa|&||&||&|sdfsadfa|&|97877|&|US";
        Map<String, String> address = stringToAddress(strAddress);
        System.out.println(address);
        System.out.println(addressToString(address));
        
        strAddress = "address1|&|address2|&|city1|&|state1|&|92677|&|US";
        address = stringToAddress(strAddress);
        System.out.println(address);
        System.out.println(addressToString(address));

        
    }*/
    
    
    //multi select checl boc
    /**
     * 
     * @param data - ["Option 1","Option 2","Option 3"]
     * 
     * @return Option 1|&|Option 2|&|Option 3
     */
    public static String arrayToMultiCheckboxString(String[] data)
    {
        return arrayToString(data);
    }
    
    /**
     * 
     * @param data --> Option 1|&|Option 2|&|Option 3
     * 
     * @return ["Option 1","Option 2","Option 3"]
     */
    public static String[] multiCheckboxStringToArray(String data)
    {
        return stringToArray(data);
    }
    
    /**
     * 
     * @param data  - list of strings
     * @return Option 1|&|Option 2|&|Option 3
     */
    public static String listToMultiCheckboxString(List<String> data)
    {
        return listToString(data);
    }
    
    /**
     * 
     * @param data --> Option 1|&|Option 2|&|Option 3
     * @return
     */
    public static List<String> multiCheckboxStringToList(String data)
    {
        return stringToList(data);
    }    
    
    //multiselect drop down
    /**
     * 
     * @param data - ["Option 1","Option 2","Option 3"]
     * 
     * @return Option 1|&|Option 2|&|Option 3
     */
    public static String arrayToMultiSelectDropDownString(String[] data)
    {
        return arrayToString(data);
    }
    
    /**
     * 
     * @param data --> Option 1|&|Option 2|&|Option 3
     * 
     * @return ["Option 1","Option 2","Option 3"]
     */
    public static String[] multiSelectDropDownStringToArray(String data)
    {
        return stringToArray(data);
    }
    
    /**
     * 
     * @param data  - list of strings
     * @return Option 1|&|Option 2|&|Option 3
     */
    public static String listToMultiSelectDropDownString(List<String> data)
    {
        return listToString(data);
    }
    
    /**
     * 
     * @param data --> Option 1|&|Option 2|&|Option 3
     * @return
     */
    public static List<String> multiSelectDropDownStringToList(String data)
    {
        return stringToList(data);
    }    
    
    //list
    /**
     * 
     * @param data - ["Option 1","Option 2","Option 3"]
     * 
     * @return Option 1|&|Option 2|&|Option 3
     */
    public static String arrayToListString(String[] data)
    {
        return arrayToString(data);
    }
    
    /**
     * 
     * @param data --> Option 1|&|Option 2|&|Option 3
     * 
     * @return ["Option 1","Option 2","Option 3"]
     */
    public static String[] listStringToArray(String data)
    {
        return stringToArray(data);
    }
    
    /**
     * 
     * @param data  - list of strings
     * @return Option 1|&|Option 2|&|Option 3
     */
    public static String listToListString(List<String> data)
    {
        return listToString(data);
    }
    
    /**
     * 
     * @param data --> Option 1|&|Option 2|&|Option 3
     * @return
     */
    public static List<String> listStringToList(String data)
    {
        return stringToList(data);
    }  
    

    //address
    /**
     * Accepts a Map with the following keys to create the Address - Custom Form component
     * 
     * address1, address2. city, state, zip, country 
     * 
     * 
     * @param data - map with the above mentioned keys
     * @return - address1|&|address2|&|city1|&|state1|&|92677|&|US
     */
    public static String addressToString(Map<String, String> data)
    {
        String address = null;
        
        if(data != null)
        {
            String add1 = (data.containsKey(CT_ADDRESS1) &&  data.get(CT_ADDRESS1) !=null) ? data.get(CT_ADDRESS1) : "";
            String add2 = (data.containsKey(CT_ADDRESS2) &&  data.get(CT_ADDRESS2) !=null) ? data.get(CT_ADDRESS2) : "";
            String city = (data.containsKey(CT_CITY) &&  data.get(CT_CITY) !=null) ? data.get(CT_CITY) : "";
            String state = (data.containsKey(CT_STATE) &&  data.get(CT_STATE) !=null) ? data.get(CT_STATE) : "";
            String zip = (data.containsKey(CT_ZIP) &&  data.get(CT_ZIP) !=null) ? data.get(CT_ZIP) : "";
            String country = (data.containsKey(CT_COUNTRY) &&  data.get(CT_COUNTRY) !=null) ? data.get(CT_COUNTRY) : "";
            
            address =     add1 + StringUtils.FIELDSEPARATOR 
                        + add2 + StringUtils.FIELDSEPARATOR
                        + city + StringUtils.FIELDSEPARATOR
                        + state + StringUtils.FIELDSEPARATOR
                        + zip + StringUtils.FIELDSEPARATOR
                        + country;
        }
        
        return address;
    }
    
    
    /**
     * 
     * @param address --> address1|&|address2|&|city1|&|state1|&|92677|&|US  ,  sdfasdfa|&||&||&|sdfsadfa|&|97877|&|US
     * @return - map with the values
     */
    public static Map<String, String> stringToAddress(String address)
    {
        Map<String, String> result = new HashMap<String, String>();
        
        if(StringUtils.isNotBlank(address))
        {
            String[] values = address.split(StringUtils.FIELDSEPARATOR_REGEX);
            if(values != null && values.length == 6)
            {
                for(int x=0; x < values.length; x++)
                {
                    String value = values[x];
                    if(x == 0)
                    {
                        result.put(CT_ADDRESS1, value);
                    } 
                    else if(x == 1)
                    {
                        result.put(CT_ADDRESS2, value);
                    }
                    else if(x == 2)
                    {
                        result.put(CT_CITY, value);
                    }
                    else if(x == 3)
                    {
                        result.put(CT_STATE, value);
                    }
                    else if(x == 4)
                    {
                        result.put(CT_ZIP, value);
                    }
                    else if(x == 5)
                    {
                        result.put(CT_COUNTRY, value);
                    }
                }//end of for loop
            }//end of if
        }//end of address
        
        return result;
    }
    
    //journal
    /**
     * Utility api to append a new comment to the journal
     * 
     * @param jsonCurrentJournal
     * @param newJournalComment
     * @param userId
     * @return
     */
    public static String appendNewJournalComment(String jsonCurrentJournal, String newJournalComment, String userId)
    {
        return CustomFormUtil.appendJournalToString(newJournalComment, newJournalComment, userId);
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //private apis
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private static String arrayToString(String[] array)
    {
        return StringUtils.arrayToString(array);
    }
    
    private static String listToString(List<String> list)
    {
        return StringUtils.listToString(list);
    }
    
    private static List<String> stringToList(String data)
    {
        return StringUtils.stringToList(data);
    }
    
    
    private static String[] stringToArray(String str)
    {
        return StringUtils.stringToArray(str);
    }
    
    
    
    
}
