/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.util;

import com.resolve.dto.AboutDTO;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.owasp.csrfguard.CsrfGuard;
import org.owasp.csrfguard.util.RandomGenerator;

import net.sf.json.JSONObject;

/**
 * Utility to be used in JSP files
 * 
 * @author jeet.marwah
 * 
 */
public class JspUtils
{
    /**
     * returns the product description
     * 
     * @return
     */
    public static String getResolveTitle()
    {
        String title = "Resolve RBA";
        String productDescription = PropertiesUtil.getPropertyString("product.description");
        if (!StringUtils.isEmpty(productDescription) && !productDescription.equalsIgnoreCase("Product Evaluation") && !productDescription.equalsIgnoreCase("Runbook Automation"))
        {
            title = productDescription;
        }

        return title;
    }// getResolveTitle


    /**
     * returns the product version
     * 
     * @return
     */
    public static String getResolveVersion()
    {
        AboutDTO dto = new AboutDTO();
        return dto.getVersion();
    }// getResolveVersion
   
    
    public static String getResolveInstanceType()
    {
        String type = "ON-PREMISE";
        String instanceType = PropertiesUtil.getPropertyString("product.type");
        if (!StringUtils.isEmpty(instanceType))
        {
            type = instanceType;
        }

        return type;
    }// getResolveTitle

    
    public static String getResolveNotificationLevel()
    {
        String notificationLevel = PropertiesUtil.getPropertyString("product.notificationLevel");
        if (!StringUtils.isEmpty(notificationLevel))
        {
            return notificationLevel;
        }
        else
            return "INFO";
    }// getResolveTitle

    public static String getResolveLogoInfo()
    {
        Map<String, String> myMap = new HashMap<String, String>();
        myMap.put("logo"           , PropertiesUtil.getPropertyString("product.logo"));
        myMap.put("logoHeight"     , PropertiesUtil.getPropertyString("product.logoHeight"));
        myMap.put("logoWidth"      , PropertiesUtil.getPropertyString("product.logoWidth"));
        myMap.put("logoHidden"     , PropertiesUtil.getPropertyString("product.logoHidden"));
        myMap.put("rightLogo"      , PropertiesUtil.getPropertyString("product.rightLogo"));
        myMap.put("rightLogoHeight", PropertiesUtil.getPropertyString("product.rightLogoHeight"));
        myMap.put("rightLogoWidth" , PropertiesUtil.getPropertyString("product.rightLogoWidth"));
        myMap.put("rightLogoHidden", PropertiesUtil.getPropertyString("product.rightLogoHidden"));
        JSONObject jsonObject = new JSONObject();
        jsonObject.putAll(myMap);
        return jsonObject.toString();
    }// getResolveLogoInfo

    public static String getResolveToolbarLogoInfo()
    {
        Map<String, String> myMap = new HashMap<String, String>();
        myMap.put("toolbarLogo"           , PropertiesUtil.getPropertyString("product.toolbarLogo"));
        myMap.put("toolbarLogoHeight"     , PropertiesUtil.getPropertyString("product.toolbarLogoHeight"));
        myMap.put("toolbarLogoWidth"      , PropertiesUtil.getPropertyString("product.toolbarLogoWidth"));
        myMap.put("toolbarLogoHidden"     , PropertiesUtil.getPropertyString("product.toolbarLogoHidden"));
        myMap.put("toolbarRightLogo"      , PropertiesUtil.getPropertyString("product.toolbarRightLogo"));
        myMap.put("toolbarRightLogoHeight", PropertiesUtil.getPropertyString("product.toolbarRightLogoHeight"));
        myMap.put("toolbarRightLogoWidth" , PropertiesUtil.getPropertyString("product.toolbarRightLogoWidth"));
        myMap.put("toolbarRightLogoHidden", PropertiesUtil.getPropertyString("product.toolbarRightLogoHidden"));
        JSONObject jsonObject = new JSONObject();
        jsonObject.putAll(myMap);
        return jsonObject.toString();
    }// getResolveToolbarLogoInfo
    
    @SuppressWarnings("unchecked")
    public static String[] getCSRFTokenForPage(HttpServletRequest request, String uri)
    {
        String[] csrfTokenNameAndVal = new String[2];
        
        CsrfGuard csrfGuard = CsrfGuard.getInstance();
        
        HttpSession session = request.getSession(false);
        
        Map<String, String> pageTokens = (Map<String, String>) session.getAttribute(CsrfGuard.PAGE_TOKENS_KEY);
        
        if (pageTokens != null && !(pageTokens instanceof ConcurrentHashMap))
        {
            synchronized(csrfGuard)
            {
                Map<String, String> newPageTokens = new ConcurrentHashMap<String, String>();
                
                newPageTokens.putAll(pageTokens);                         
                session.setAttribute(CsrfGuard.PAGE_TOKENS_KEY, newPageTokens);
                
                pageTokens = newPageTokens;
                
                Log.auth.info("JSP Utils Get CSRFToken For Page " + uri + " : pageTokens map in CsrfGuard replaced with concurrent hash map!!!");
            }
        }
        
        String tokenFromPages = (pageTokens != null ? pageTokens.get(uri) : null);
        
        // Always return new token for page
        
        if (StringUtils.isBlank(tokenFromPages))
        {
            if (pageTokens == null)
            {
                pageTokens = new ConcurrentHashMap<String, String>();
                session.setAttribute(CsrfGuard.PAGE_TOKENS_KEY, pageTokens);
            }
            
            try
            {
                pageTokens.put(uri, RandomGenerator.generateRandomId(csrfGuard.getPrng(), csrfGuard.getTokenLength()));
                
                tokenFromPages = (pageTokens != null ? pageTokens.get(uri) : null);
                
                Log.auth.info("JSP Utils Get CSRFToken For Page " + uri + " : tokenFromPages(" + uri + ") After Creation : " + (StringUtils.isNotBlank(tokenFromPages) ? tokenFromPages : "null"));
                
                if (StringUtils.isNotBlank(tokenFromPages))
                {
                    csrfTokenNameAndVal[0] = csrfGuard.getTokenName();
                    csrfTokenNameAndVal[1] = tokenFromPages;
                }
            } 
            catch (Exception e)
            {
                Log.auth.error("JSP Utils Get CSRFToken For Page " + uri + " : unable to generate the random token - " + e.getLocalizedMessage(), e);
                throw new RuntimeException(String.format("JSP Utils Get CSRFToken For Page : unable to generate the random token - " + e.getLocalizedMessage()), e);
            }
        }
        else
        {
            csrfTokenNameAndVal[0] = csrfGuard.getTokenName();
            csrfTokenNameAndVal[1] = tokenFromPages;
        }
        
        return csrfTokenNameAndVal;
    }
    
    @SuppressWarnings("unchecked")
    public static String[] findCSRFTokenForPage(HttpServletRequest request, String uri)
    {
        String[] csrfTokenNameAndVal = new String[] {"", ""};
        
        CsrfGuard csrfGuard = CsrfGuard.getInstance();
        
        HttpSession session = request.getSession(false);
        
        Map<String, String> pageTokens = (Map<String, String>) session.getAttribute(CsrfGuard.PAGE_TOKENS_KEY);
        
        if (pageTokens != null && !(pageTokens instanceof ConcurrentHashMap))
        {
            synchronized(csrfGuard)
            {
                Map<String, String> newPageTokens = new ConcurrentHashMap<String, String>();
                
                newPageTokens.putAll(pageTokens);                         
                session.setAttribute(CsrfGuard.PAGE_TOKENS_KEY, newPageTokens);
                
                pageTokens = newPageTokens;
                
                Log.auth.info("JSP Utils Get CSRFToken For Page " + uri + " : pageTokens map in CsrfGuard replaced with concurrent hash map!!!");
            }
        }
        
        String tokenFromPages = (pageTokens != null ? pageTokens.get(uri) : null);
        
        if (StringUtils.isNotBlank(tokenFromPages))
        {
            csrfTokenNameAndVal[0] = csrfGuard.getTokenName();
            csrfTokenNameAndVal[1] = tokenFromPages;
        }
        
        return csrfTokenNameAndVal;
    }
    
    public static String getCSRFTokenName()
    {
        CsrfGuard csrfGuard = CsrfGuard.getInstance();
        
        return csrfGuard.getTokenName();
    }
    
    /**
     * return the JSON string representation of the vie model used in rsform.jsp
     * 
     * @return
     */
    // public static String getMetaDataDiv(HttpServletRequest request)
    // {
    // String[] tableNames =
    // {"resolve_action_task"};//request.getParameterValues("TABLENAME");
    // String[] viewNames =
    // {"actiontask_view1"};//request.getParameterValues("VIEWNAME");
    // String[] viewTypes = {"table"};//request.getParameterValues("VIEWTYPE");
    // //table or form
    //
    // Map<String, Object> result = new HashMap<String, Object>();
    //
    // for(int pos=0; pos<viewNames.length; pos++)
    // {
    // String viewName = viewNames[pos];
    // String tableName = tableNames[pos];
    // String viewType = viewTypes[pos];
    //
    // //get the view from the metatables
    // RSMetaTableView view = getRSMetaTableView();
    //
    // // result.put(viewName, view);
    //
    // }
    //
    // // //query the db and prepare the JSON object
    // Map<String, String> map1 = new HashMap<String, String>();
    // map1.put("name1", "value1");
    // map1.put("name2", "value2");
    // map1.put("name3", "value3");
    // map1.put("name4", "value4");
    // map1.put("name5", "value5");
    //
    // List<String> list = new ArrayList<String>();
    // list.add("listValue1");
    // list.add("listValue2");
    // list.add("listValue3");
    // list.add("listValue4");
    // list.add("listValue5");
    //
    // result.put("mapObject", map1);
    // result.put("listObject", list);
    //
    // JSONObject jsonObject = new JSONObject();
    // jsonObject.putAll(result);
    //
    //
    // StringBuffer sb = new
    // StringBuffer("<div id='meta_table_schema'>").append(jsonObject.toString()).append("</div>");
    // return sb.toString();
    // }

    // test data
    // public static RSMetaTableView getRSMetaTableView()
    // {
    //
    // RSMetaTableView view = new RSMetaTableView();
    // view.setMeta_table_sys_id("");
    // view.setMeta_style_sys_id("");
    // view.setTableName("resolve_action_task");
    // view.setTableModelName("ResolveActionTask");
    // view.setTableDisplayName("ActionTask");
    // view.setName("actiontask_view1");
    // view.setDisplay_name("ActionTask - View 1");
    // view.setType("global");
    // view.setIs_global(true);
    //
    // view.addField(getUName());
    // view.addField(getUModule());
    // view.addField(getUpdatedDate());
    //
    // return view;
    // }
    //
    // private static RSMetaField getUName()
    // {
    // RSColumnConfig u_name_config = new RSColumnConfig();
    // u_name_config.setId("UName");//must be same as the column model name
    // u_name_config.setHeader("Name");
    // u_name_config.setWidth(100);
    // u_name_config.setRowHeader(true);
    // u_name_config.setAlignment("left");
    //
    //
    // RSMetaField u_name = new RSMetaField();
    // u_name.setName("u_name");
    // u_name.setTable("resolve_action_task");
    // u_name.setColumn("resolve_action_task.u_name");
    // u_name.setColumnModelName("UName");
    // u_name.setDisplay_name("Name");
    // u_name.setType("string");
    // u_name.setJavascript("");
    // u_name.setCustom_table_sys_id("");
    // u_name.setRsColumnConfig(u_name_config);
    //
    // return u_name;
    // }
    //
    // private static RSMetaField getUModule()
    // {
    // RSColumnConfig u_name_config = new RSColumnConfig();
    // u_name_config.setId("UNamespace");//must be same as the column name
    // u_name_config.setHeader("Module");
    // u_name_config.setWidth(50);
    // u_name_config.setRowHeader(true);
    // u_name_config.setAlignment("left");
    //
    //
    // RSMetaField u_name = new RSMetaField();
    // u_name.setName("u_namespace");
    // u_name.setTable("resolve_action_task");
    // u_name.setColumn("resolve_action_task.u_namespace");
    // u_name.setColumnModelName("UNamespace");
    // u_name.setDisplay_name("Module");
    // u_name.setType("string");
    // u_name.setJavascript("");
    // u_name.setCustom_table_sys_id("");
    // u_name.setRsColumnConfig(u_name_config);
    //
    // return u_name;
    //
    // }
    //
    // private static RSMetaField getUpdatedDate()
    // {
    // RSColumnConfig u_name_config = new RSColumnConfig();
    // u_name_config.setId("sysUpdatedOn");//must be same as the column name
    // u_name_config.setHeader("Last Updated");
    // u_name_config.setWidth(100);
    // u_name_config.setRowHeader(true);
    // u_name_config.setAlignment("left");
    //
    //
    // RSMetaField u_name = new RSMetaField();
    // u_name.setName("sys_updated_on");
    // u_name.setTable("resolve_action_task");
    // u_name.setColumn("resolve_action_task.sys_updated_on");
    // u_name.setColumnModelName("sysUpdatedOn");
    // u_name.setDisplay_name("Last Updated");
    // u_name.setType("datetime");
    // u_name.setJavascript("");//write the date format code here
    // u_name.setCustom_table_sys_id("");
    // u_name.setRsColumnConfig(u_name_config);
    //
    // return u_name;
    //
    // }

}// JspUtils
