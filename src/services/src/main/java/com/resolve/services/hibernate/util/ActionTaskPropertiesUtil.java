/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.query.Query;

import com.resolve.persistence.model.ResolveProperties;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.search.property.PropertyIndexAPI;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.vo.ResolvePropertiesVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.util.UpdateChecksumUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;


public class ActionTaskPropertiesUtil
{
    public static List<ResolvePropertiesVO> getResolveProperties(QueryDTO query, String username) throws Exception
    {
        List<ResolvePropertiesVO> result = new ArrayList<ResolvePropertiesVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        ResolveProperties instance = new ResolveProperties();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        result.add(instance.doGetVO());
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        ResolveProperties instance = (ResolveProperties) o;
                        result.add(instance.doGetVO());
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
            throw t;
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static Set<String> getAllAvailablePropertyModules() throws Exception
    {
        Set<String> result = new HashSet<String>();
        
        String hql = "select distinct UModule from ResolveProperties order by UModule" ;
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        
        try
        {
            List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
            if(list != null && list.size() > 0)
            {
                result.addAll((List<String>) list);
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error while executing sql:" + hql, e);
        }
        
        
        return result;
        
    }
    
    public static ResolvePropertiesVO findResolveProperties(String sys_id, String module, String name, String username) throws Exception
    {
        ResolveProperties model = null;
        ResolvePropertiesVO result = null;
        
        try
        {
            model = findResolvePropertiesModel(sys_id, module, name, username);
            if (model != null)
            {
                result = model.doGetVO();
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }

        return result;
    }
    
    public static ResolvePropertiesVO saveResolveProperties(ResolvePropertiesVO vo, String username) throws Exception
    {
        if(vo != null && vo.validate())
        {
            ResolveProperties model = null;
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equals(VO.STRING_DEFAULT))
            {
                //UPDATE
                //first check if the property with the same name already exist
                model = findResolvePropertiesModel(null, vo.getUModule(), vo.getUName(), username);
                if(model != null && !vo.getSys_id().equalsIgnoreCase(model.getSys_id()))
                {
                    throw new Exception("Property with name " + vo.getUName() + " already exist");
                }

                //get the right model from the DB using sysId
                model = findResolvePropertiesModel(vo.getSys_id(), null, null, username);
                if(model == null)
                {
                    throw new Exception("Property with sysId " + vo.getSys_id() + " does not exist.");
                }
            }
            else
            {
                model = findResolvePropertiesModel(null, vo.getUModule(), vo.getUName(), username);
                if(model != null)
                {
                    throw new Exception("Property with name " + vo.getUName() + " already exist");
                }
                
                model = new ResolveProperties();
            }
            
            //update the model
            model.applyVOToModel(vo);
            
            //check for the encrypt values
            String value = model.getUValue();
            if(StringUtils.isNotBlank(value))
            {
                boolean encrypt = StringUtils.isNotBlank(model.getUType()) && model.getUType().equalsIgnoreCase("encrypt") ? true : false;
                if(encrypt)
                {
                    if (!CryptUtils.isEncrypted(value))
                    {
                        String encValue = CryptUtils.encrypt(value);
                        model.setUValue(encValue);
                    }
                }
                else
                {
                    /* THIS IS SECURITY HOLE DO NOT UNCOMMENT
                    //if the flag is false and the value is encrypted, decrypt it
                    if (CryptUtils.isEncrypted(value))
                    {
                        String decryptValue = CryptUtils.decrypt(value);
                        model.setUValue(decryptValue);
                    }*/
                }
            }
            model.setChecksum(calculateChecksum(model));
            //persist
            model = SaveUtil.saveResolveProperties(model, username);
            vo = model.doGetVO();
            PropertyIndexAPI.indexProperty(vo.getSys_id(), username);
            
            // TO-DO index property
        }
        
        return vo;
    }
    
    public static void deleteResolvePropertiesByIds(String[] sysIds, boolean deleteAll, String username) throws Exception
    {
        if (deleteAll)
        {
            GeneralHibernateUtil.purgeDBTable("ResolveProperties", username);
            PropertyIndexAPI.purgeAllProperties(username);
        }
        else
        {
            if (sysIds != null)
            {
                //can be used as its 1 table only
                ServiceHibernate.deleteDBRow(username, "ResolveProperties", (List<String>) Arrays.asList(sysIds), false);
                PropertyIndexAPI.deleteProperties(Arrays.asList(sysIds), username);
            }
        }
    }
    
    //private apis
    private static ResolveProperties findResolvePropertiesModel(String sys_id, String module, String name, String username)
    {
        ResolveProperties model = null;
        
        if(StringUtils.isNotEmpty(sys_id))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	model = (ResolveProperties) HibernateProxy.execute(() -> {
                
            		return HibernateUtil.getDAOFactory().getResolvePropertiesDAO().findById(sys_id);
                
            	});
                
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        else if(StringUtils.isNotBlank(name))
        {
            //to make this case-insensitive
            //String sql = "from ResolveProperties where LOWER(UName) = '" + name.toLowerCase().trim() + "' " ;
            String sql = "from ResolveProperties where LOWER(UName) = :UName " ;
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("UName", name.toLowerCase().trim());
            
//            if(StringUtils.isNotBlank(module))
//            {
//                sql = sql + " and LOWER(UModule) = '" + module.trim().toLowerCase() + "' ";                
//            }
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                if(list != null && list.size() > 0)
                {
                    model = (ResolveProperties) list.get(0);
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing hql:" + sql, e);
            }
            
        }
        
        return model;
    }

	public static ResolvePropertiesVO getPropertyWithReferences(String sysId, String fullName, String username) {
		ResolvePropertiesVO result = null;
    	try
    	{
	    	result = findResolveProperties(sysId, null, null, username);
    	}
    	catch (Throwable e)
    	{
    		Log.log.error(e.getMessage(), e);
    	}
    	
    	return result;
	}
	
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static Set<String> getAllResolvePropertySysIds()
    {
        Set<String> sysIds = new TreeSet<String>();
        try
        {
            HibernateProxy.execute(() -> {
            	 // fetch the info based on the search criteria
                String sql = "select rp.sys_id from ResolveProperties as rp";

                Query query = HibernateUtil.createQuery(sql);
                List<Object> resolveProperties = query.list();
                if (resolveProperties != null && resolveProperties.size() > 0)
                {
                    for(Object sysId : resolveProperties)
                    {
                        sysIds.add(sysId.toString());
                    }
                }
            });
           
        }
        catch (Throwable t)
        {
            Log.log.info(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return sysIds;
    }
    
    /**
     * API to be invoked from update/upgrade script only!
     */
    public static void updateAllTaskPropertiessForChecksum()
    {
        try {
            UpdateChecksumUtil.updateComponentChecksum("ResolveProperties", ActionTaskPropertiesUtil.class);
        }
        catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static Integer calculateChecksum(ResolveProperties property)
    {
        return Objects.hash(property.getUName(), property.getUType(), property.getUModule());
    }
    
    public static Integer getTaskPropertyChecksum(String name, String username)
    {
        Integer checksum = null;
        ResolveProperties property = findResolvePropertiesModel(null, null, name, username);
        if (property != null)
        {
            checksum = property.getChecksum();
        }
        return checksum;
    }
}
