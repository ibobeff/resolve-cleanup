/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.rb.util;

public abstract class Common
{
    protected Integer id;
    protected String nodeLabel; //mostly nodeType or the AT name
    protected String description;
    //protected Integer x;
    //protected Integer y;
    protected Integer width = -1;
    protected Integer height = -1;
    protected String merge;
    protected Geometry geometry;
    protected Cell cell;

    protected Edge previousEdge;
    protected Edge nextEdge;

    public Common()
    {
        
    }
    
    public Common(Integer id, String nodeLabel, String description)
    {
        this.id = id;
        this.nodeLabel = nodeLabel;
        this.description = description;
        geometry = new Geometry(-1, -1, this.width, this.height, 0, "geometry");
        cell = new Cell(getStyle(), 1, -1, -1, -1, 1, geometry);
    }

    public Common(Integer id, String nodeLabel, String description, Integer x, Integer y)
    {
        this.id = id;
        this.nodeLabel = nodeLabel;
        this.description = description;
        geometry = new Geometry(x, y, this.width, this.height, 0, "geometry");
        cell = new Cell(getStyle(), 1, -1, -1, -1, 1, geometry);
    }

    public Common(Integer id, String nodeLabel, String description, Integer x, Integer y, Integer width, Integer height, String merge)
    {
        this.id = id;
        this.nodeLabel = nodeLabel;
        this.description = description;
        this.width = width;
        this.height = height;
        this.merge = merge;

        geometry = new Geometry(x, y, this.width, this.height, 0, "geometry");
        cell = new Cell(getStyle(), 1, -1, -1, -1, 1, geometry);
    }

    public Integer getId()
    {
        return id;
    }
    public void setId(Integer id)
    {
        this.id = id;
    }
    public String getNodeLabel()
    {
        return nodeLabel;
    }
    public void setNodeLabel(String nodeLabel)
    {
        this.nodeLabel = nodeLabel;
    }
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    abstract String getStyle();

    public Integer getX()
    {
        return this.getGeometry().getX();
    }

    public void setX(Integer x)
    {
        this.geometry.setX(x);
    }

    public Integer getY()
    {
        return this.getGeometry().getY();
    }

    public void setY(Integer y)
    {
        this.geometry.setY(y);
    }
    
    public Node getNode()
    {
        return new Node(getId(), getX(), getY(), getMerge(), getPreviousEdge(), getNextEdge());
    }

    public Integer getWidth()
    {
        return width;
    }

    public void setWidth(Integer width)
    {
        this.width = width;
    }

    public String getMerge()
    {
        return merge;
    }

    public void setMerge(String merge)
    {
        this.merge = merge;
    }

    public Geometry getGeometry()
    {
        return geometry;
    }

    public void setGeometry(Geometry geometry)
    {
        this.geometry = geometry;
    }

    public Cell getCell()
    {
        return cell;
    }

    public void setCell(Cell cell)
    {
        this.cell = cell;
    }

    public Edge getPreviousEdge()
    {
        return previousEdge;
    }

    public void setPreviousEdge(Edge previousEdge)
    {
        this.previousEdge = previousEdge;
    }

    public Edge getNextEdge()
    {
        return nextEdge;
    }

    public void setNextEdge(Edge nextEdge)
    {
        this.nextEdge = nextEdge;
    }
}
