package com.resolve.services.rb.util;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class ABXmlNodeExtractor
{
    private String sample;
    private List<XPathVariable> variables;
    public String getSample()
    {
        return sample;
    }
    public void setSample(String sample)
    {
        this.sample = sample;
    }
    public List<XPathVariable> getVariables()
    {
        return variables;
    }
    public void setVariables(List<XPathVariable> variables)
    {
        this.variables = variables;
    }
    public static ABXmlNodeExtractor getInstance(String parserConfig) throws JsonParseException, JsonMappingException, IOException {
        ABXmlNodeExtractor instance = null;
        ObjectMapper mapper = new ObjectMapper().configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        instance = mapper.readValue(parserConfig, ABXmlNodeExtractor.class);
        return instance;
    }
}
