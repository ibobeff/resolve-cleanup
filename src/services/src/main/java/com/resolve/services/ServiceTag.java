/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services;

import java.util.List;

import com.resolve.services.hibernate.util.TagUtil;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;

public class ServiceTag
{
    //list
    public static ResponseDTO<ResolveTagVO> listTags(QueryDTO query, String username) throws Exception
    {
        return TagUtil.listTags(query, username);
    }
    
    //get
    public static ResolveTagVO getTag(String tagSysId, String tagName, String username) throws Exception
    {
        return TagUtil.getTag(tagSysId, tagName, username);
    }
    
    //delete
    public static void deleteResolveTagsByIds(List<String> tagSysIds, String username)  throws Exception
    {
        TagUtil.deleteResolveTagsByIds(tagSysIds, username);
    }
    
    //save
    public static ResolveTagVO saveTag(ResolveTagVO tag, String username) throws Exception
    {
        return TagUtil.saveTag(tag, username);
    }
    
    //create tag
    public static ResolveTagVO createTag(String tagName, String username) throws Exception
    {
        return TagUtil.createTag(tagName, username);
    }
    
    //rename
    public static ResolveTagVO rename(String from, String to, String username) throws Exception
    {
        return TagUtil.rename(from, to, username);
    }
}