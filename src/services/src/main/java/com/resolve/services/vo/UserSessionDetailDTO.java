/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.Date;

public class UserSessionDetailDTO
{
    private String id;
    private String username;
    private String host;
    private Date createTime;
    private Date updateTime;
    private Integer durationInSecs;
    private String ssoType;
    private Long sessionDurationInMin;
    private String ssoSessionId;
    private Date lastSSOValidationTime;
    private String iframeId;
    private String sessionId;
    
    public String getUsername()
    {
        return username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }
    public String getHost()
    {
        return host;
    }
    public void setHost(String host)
    {
        this.host = host;
    }
    public Date getCreateTime()
    {
        return createTime;
    }
    public void setCreateTime(Date createTime)
    {
        this.createTime = createTime;
    }
    public Date getUpdateTime()
    {
        return updateTime;
    }
    public void setUpdateTime(Date updateTime)
    {
        this.updateTime = updateTime;
    }
    public Integer getDurationInSecs()
    {
        return durationInSecs;
    }
    public void setDurationInSecs(Integer durationInSecs)
    {
        this.durationInSecs = durationInSecs;
    }
    public String getId()
    {
        return id;
    }
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getSsoType()
    {
        return ssoType;
    }
    
    public void setSsoType(String ssoType)
    {
        this.ssoType = ssoType;
    }
    
    public Long getSessionDurationInMin()
    {
        return sessionDurationInMin;
    }
    
    public void setSessionDurationInMin(Long sessionDurationInMin)
    {
        this.sessionDurationInMin = sessionDurationInMin;
    }
    
    public String getSsoSessionId()
    {
        return ssoSessionId;
    }
    
    public void setSsoSessionId(String ssoSessionId)
    {
        this.ssoSessionId = ssoSessionId;
    }
    
    public Date getLastSSOValidationTime()
    {
        return lastSSOValidationTime;
    }
    
    public void setLastSSOValidationTime(Date lastSSOValidationTime)
    {
        this.lastSSOValidationTime = lastSSOValidationTime;
    }
    
    public String getIframeId()
    {
        return iframeId;
    }
    
    public void setIframeId(String iframeId)
    {
        this.iframeId = iframeId;
    }
    
    public String getSessionId()
    {
        return sessionId;
    }
    
    public void setSessionId(String sessionId)
    {
        this.sessionId = sessionId;
    }
}
