/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

public class ServerInfoDTO
{
    //version info
    private String version;

    //server info
    private String osArchitecture;
    private String osName;
    private String osVersion;
    private String address;
    private Integer osProcessor;
    private String jvmVendor;
    private String jvmName;
    private String jvmVersion;
    
    //memory info
    private String memoryMax;
    private String memoryTotal;
    private String memoryFreePercentage;
    
    //thread info
    private Integer threadPeak;
    private Integer threadLive;
    private Integer threadDeamon;
    
    public String getVersion()
    {
        return version;
    }
    public void setVersion(String version)
    {
        this.version = version;
    }
    public String getOsArchitecture()
    {
        return osArchitecture;
    }
    public void setOsArchitecture(String osArchitecture)
    {
        this.osArchitecture = osArchitecture;
    }
    public String getOsName()
    {
        return osName;
    }
    public void setOsName(String osName)
    {
        this.osName = osName;
    }
    public String getOsVersion()
    {
        return osVersion;
    }
    public void setOsVersion(String osVersion)
    {
        this.osVersion = osVersion;
    }
    public String getAddress()
    {
        return address;
    }
    public void setAddress(String address)
    {
        this.address = address;
    }
    public Integer getOsProcessor()
    {
        return osProcessor;
    }
    public void setOsProcessor(Integer osProcessor)
    {
        this.osProcessor = osProcessor;
    }

    public String getJvmVendor()
    {
        return jvmVendor;
    }
    public void setJvmVendor(String jvmVendor)
    {
        this.jvmVendor = jvmVendor;
    }
    public String getJvmName()
    {
        return jvmName;
    }
    public void setJvmName(String jvmName)
    {
        this.jvmName = jvmName;
    }
    public String getJvmVersion()
    {
        return jvmVersion;
    }
    public void setJvmVersion(String jvmVersion)
    {
        this.jvmVersion = jvmVersion;
    }
    public String getMemoryMax()
    {
        return memoryMax;
    }
    public void setMemoryMax(String memoryMax)
    {
        this.memoryMax = memoryMax;
    }
    public String getMemoryTotal()
    {
        return memoryTotal;
    }
    public void setMemoryTotal(String memoryTotal)
    {
        this.memoryTotal = memoryTotal;
    }
    public String getMemoryFreePercentage()
    {
        return memoryFreePercentage;
    }
    public void setMemoryFreePercentage(String memoryFreePercentage)
    {
        this.memoryFreePercentage = memoryFreePercentage;
    }
    public Integer getThreadPeak()
    {
        return threadPeak;
    }
    public void setThreadPeak(Integer threadPeak)
    {
        this.threadPeak = threadPeak;
    }
    public Integer getThreadLive()
    {
        return threadLive;
    }
    public void setThreadLive(Integer threadLive)
    {
        this.threadLive = threadLive;
    }
    public Integer getThreadDeamon()
    {
        return threadDeamon;
    }
    public void setThreadDeamon(Integer threadDeamon)
    {
        this.threadDeamon = threadDeamon;
    }
    
    

}
