/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.wiki.reports;

import java.util.Date;
import java.util.Map;

public class DTRecord
{
    private String dtRootDoc;
    private String dtNamespace;
    private String pathId;
    private String nodeId;
    private int seqId;
    private String userId;
    private long ts;
    private Date tsDateTime;
    private int nodeCount;
    private int totalCount;
    private long totalTime;
    private DTReportStatusType status;
    
    public DTRecord() {}
    public DTRecord(Map<String, Object> record)
    {
        if(record != null)
        {
            dtRootDoc = (String) record.get("u_dt_root_doc");
            dtNamespace = (String) record.get("dtnamespace");
            pathId = (String) record.get("pathid");
            nodeId = (String) record.get("nodeid");
            seqId = ((Number) record.get("seqid")).intValue();
            userId = (String) record.get("userid");
            ts = ((Number) record.get("ts")).longValue();
            tsDateTime = (Date) record.get("ts_datetime");
            nodeCount = ((Number) record.get("node_cnt")).intValue();
            totalCount = ((Number) record.get("tot_cnt")).intValue();
            totalTime = ((Number) record.get("tot_time")).longValue();
            status = DTReportStatusType.valueOf((String) record.get("status"));
        }
        
    }

    public String getDtRootDoc()
    {
        return dtRootDoc;
    }

    public void setDtRootDoc(String dtRootDoc)
    {
        this.dtRootDoc = dtRootDoc;
    }

    public String getDtNamespace()
    {
        return dtNamespace;
    }

    public void setDtNamespace(String dtNamespace)
    {
        this.dtNamespace = dtNamespace;
    }

    public String getPathId()
    {
        return pathId;
    }

    public void setPathId(String pathId)
    {
        this.pathId = pathId;
    }

    public String getNodeId()
    {
        return nodeId;
    }

    public void setNodeId(String nodeId)
    {
        this.nodeId = nodeId;
    }

    public int getSeqId()
    {
        return seqId;
    }

    public void setSeqId(int seqId)
    {
        this.seqId = seqId;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public long getTs()
    {
        return ts;
    }

    public void setTs(long ts)
    {
        this.ts = ts;
    }

    public Date getTsDateTime()
    {
        return tsDateTime;
    }

    public void setTsDateTime(Date tsDateTime)
    {
        this.tsDateTime = tsDateTime;
    }

    public int getNodeCount()
    {
        return nodeCount;
    }

    public void setNodeCount(int nodeCount)
    {
        this.nodeCount = nodeCount;
    }

    public int getTotalCount()
    {
        return totalCount;
    }

    public void setTotalCount(int totalCount)
    {
        this.totalCount = totalCount;
    }

    public long getTotalTime()
    {
        return totalTime;
    }

    public void setTotalTime(long totalTime)
    {
        this.totalTime = totalTime;
    }

    public DTReportStatusType getStatus()
    {
        return status;
    }

    public void setStatus(DTReportStatusType status)
    {
        this.status = status;
    }
    
    
    

}
