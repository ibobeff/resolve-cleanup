/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.util.WikiLockException;
import com.resolve.util.Log;

/**
 * This Utility is to facilitate the locking mechanism of wikidoc
 * 
 * @author jeet.marwah
 *
 */
public class DocUtils
{
    private final static String MAP_WIKI_DOC_LOCK = "MAP_WIKI_DOC_LOCK";
    private static Lock lock = new ReentrantLock();
    
    @SuppressWarnings("unchecked")
    private static void updateCache(String docFullName, WikiDocLock wikidocLock)
    {
        String key = getKey(docFullName);
        
        lock.lock();
        
        Map<String, WikiDocLock> wikiDocLockCache = (Map<String, WikiDocLock>) ServiceHibernate.findSharedJavaObject(MAP_WIKI_DOC_LOCK);
        if(wikiDocLockCache == null)
        {
            wikiDocLockCache = new HashMap<String, WikiDocLock>();
        }
        wikiDocLockCache.put(key, wikidocLock);
        
        //save it
        ServiceHibernate.saveSharedJavaObject(MAP_WIKI_DOC_LOCK, wikiDocLockCache);
        
        lock.unlock();
        
    }
    
    @SuppressWarnings("unchecked")
    private static void removeFromCache(String docFullName)
    {
        String key = getKey(docFullName);
        
        lock.lock();
        
        Map<String, WikiDocLock> wikiDocLockCache = (Map<String, WikiDocLock>) ServiceHibernate.findSharedJavaObject(MAP_WIKI_DOC_LOCK);
        if(wikiDocLockCache != null)
        {
            wikiDocLockCache.remove(key);
            
            //save it
            ServiceHibernate.saveSharedJavaObject(MAP_WIKI_DOC_LOCK, wikiDocLockCache);
        }
        
        lock.unlock();
    }
    
    @SuppressWarnings("unchecked")
    public static WikiDocLock getWikiDocLock(String docFullName)
    {
        WikiDocLock wikiDocLock = null;
        String key = getKey(docFullName);
        
        Map<String, WikiDocLock> wikiDocLockCache = (Map<String, WikiDocLock>) ServiceHibernate.findSharedJavaObject(MAP_WIKI_DOC_LOCK);
        if(wikiDocLockCache != null && wikiDocLockCache.containsKey(key))
        {
            wikiDocLock = wikiDocLockCache.get(key);
        }
        
        return wikiDocLock;
    }
    
    @SuppressWarnings("unchecked")
    public static WikiDocLock getWikiDocLockNonBlocking(String docFullName)
    {
        WikiDocLock wikiDocLock = null;
        String key = getKey(docFullName);
        
        Map<String, WikiDocLock> wikiDocLockCache = (Map<String, WikiDocLock>) ServiceHibernate.findSharedJavaObjectNonBlocking(MAP_WIKI_DOC_LOCK);
        if(wikiDocLockCache != null && wikiDocLockCache.containsKey(key))
        {
            wikiDocLock = wikiDocLockCache.get(key);
        }
        
        return wikiDocLock;
    }
    
    private static String getKey(String docFullName)
    {
//        return MainBase.main.configId.guid + "_" + docFullName;
        return docFullName;
    }
    
    
    /**
     * this API is to lock a document
     * 
     * @param user
     * @param docFullName
     */
	public static synchronized void lock(String user, String docFullName)
	{
	    WikiDocLock currentLock = getWikiDocLockNonBlocking(docFullName);
	    
	    if(currentLock != null)
	    {
	        if (!currentLock.getUsername().equals(user))
	        {
	            throw new WikiLockException(currentLock.getUsername(),docFullName + " is already locked by " + currentLock.getUsername() + " user");
	        }
	        else
	        {
	        	if (Log.log.isTraceEnabled()) {
	        		Log.log.trace(String.format("lock(%s, %s) returning current lock %s", user, docFullName, currentLock));
	        	}
	            return;
	        }
	    }
	    
		WikiDocLock lock = new WikiDocLock(user, docFullName);
		updateCache(docFullName, lock);
		if (Log.log.isTraceEnabled()) {
    		Log.log.trace(String.format("lock(%s, %s) returning new lock %s", user, docFullName, lock));
    	}
	}//unlock
	
	public static synchronized void requestSoftLock(String username, String docFullName)
	{
	    //first try to unlock it
	    unlockByAdmin(docFullName, username);
	    
	    //check if the lock is still there, if yes, than user cannot get the lock
	    WikiDocLock currentLock = getWikiDocLockNonBlocking(docFullName);
	    
        if(currentLock != null)
        {
            throw new WikiLockException(username, username + " does have rights to acquire the lock for this document.");
        }
	    
        WikiDocLock lock = new WikiDocLock(username, docFullName);
        updateCache(docFullName, lock);
        if (Log.log.isTraceEnabled()) {
    		Log.log.trace(String.format("requestSoftLock(%s, %s) returning new lock %s", username, docFullName, currentLock));
    	}
	}
	
	/**
	 * This API is to unlock a doc
	 * 
	 * @param docFullName
	 */
	public static synchronized void unlock(String docFullName)
	{
	    removeFromCache(docFullName);
	}//unlock
	
	/**
	 * Unlock by the user - admin and resolve.maint user should be able to unlock it anyway
	 * 
	 * @param docFullName
	 * @param username
	 */
	public static void unlock(String docFullName, String username)
	{
		WikiDocLock lock = getWikiDocLockNonBlocking(docFullName);
		if(lock!=null)
		{
			if (Log.log.isTraceEnabled()) {
				Log.log.trace(String.format("unlock(%s,%s) found existing cached (non-blocking) lock %s", docFullName, 
											username, lock));
			}
			
		    if(username.equals(lock.getUsername()))
		    {
		    	if (Log.log.isTraceEnabled()) {
					Log.log.trace(String.format("unlock(%s,%s) trying to get wiki doc lock for %s", docFullName, 
												username, docFullName));
				}
		    	
		        lock = getWikiDocLock(docFullName);
		        
		        if (lock != null)
		        {
		        	if (Log.log.isTraceEnabled()) {
						Log.log.trace(String.format("unlock(%s,%s) got wiki doc lock %s, removing from cache...", docFullName, 
													username, lock));
					}
		        	
		            removeFromCache(docFullName);
		        }
		    }
		}
		
	}//unlock
	
	/**
	 * 
	 * @param docFullName
	 * @param username
	 */
    public static void unlockByAdmin(String docFullName, String username)
    {
        WikiDocLock lock = getWikiDocLockNonBlocking(docFullName);
        if(lock!=null)
        {
            if(username.equals(lock.getUsername()) 
                            || username.equalsIgnoreCase("admin")
                            || username.equalsIgnoreCase("resolve.maint")
                            || isAdminUser(username)
                            || com.resolve.services.hibernate.util.StoreUtility.hasUserAccessRight(username, docFullName)
                            )
            {
                lock = getWikiDocLock(docFullName);
                
                if (lock != null)
                {
                    removeFromCache(docFullName);
                }
            }
        }
        
    }//unlockByAdmin
	
	
	/**
	 * utility method to unlock list of docs
	 * 
	 * @param docFullNames
	 * @param username
	 */
	public static void unlock(List<String> docFullNames, String username)
	{
	    for(String docFullName : docFullNames)
	    {
	        unlock(docFullName, username);
	    }
	}//unlock

	public static boolean isAdminUser(String username)
	{
	    boolean isAdmin = false;
	    
	    try
	    {
	        isAdmin = ServiceHibernate.isAdminUser(username);// UserUtils.hasRole(username, "admin");
	    }
	    catch(Throwable e)
	    {
	        isAdmin = false;
	    }
	    
	    return isAdmin;
	}//isAdminUser
}
