/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.social.follow;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.resolve.search.SearchException;
import com.resolve.search.model.UserInfo;
import com.resolve.search.social.SocialSearchAPI;
import com.resolve.services.ServiceGraph;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.DecisionTree;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Runbook;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.WikiUser;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SearchUserStreamsUtil
{
    public static final String INTERNAL_POST_MEMBER_ROLE = "postmrole";
    
    private ResolveNodeVO userNode = null;
    private String username = null;

    private Set<RSComponent> streams = new HashSet<RSComponent>();
    private Set<String> compSysIds = new HashSet<String>();
    
    private UserInfo userInfo = null;
    private User userComp = null;

    //switches
    private boolean recurse = true;//be default, recurse to get the comps that are followed

    public SearchUserStreamsUtil(String userSysId, String username) throws Exception
    {
        if (StringUtils.isEmpty(userSysId) && StringUtils.isEmpty(username))
        {
            throw new Exception("UserId or Username is required.");
        }
        
        String normalizedUserName = username;
        
        if (StringUtils.isNotBlank(username))
        {
            int pos = username.indexOf('\\');
            
            if (pos > 0)
            {
                normalizedUserName = username.substring(0, pos) + "\\" + username.substring(pos + 1, username.length());
            }
        }
        
        userNode = ServiceGraph.findNode(null, userSysId, normalizedUserName, NodeType.USER, username);
        if (userNode == null)
        {
            throw new Exception("User with id " + userSysId + " and username " + username + " does not exist.");
        }

        this.username = userNode.getUCompName();
        
        //set the userInfo based on username
        WikiUser wikiUser = ServiceHibernate.getUserRolesAndGroups(this.username);
        userInfo = new UserInfo(userNode.getUCompSysId(), "", userNode.getUCompName(), StringUtils.convertCollectionToString(wikiUser.getRoles().iterator(), " ") + " " + INTERNAL_POST_MEMBER_ROLE, ServiceHibernate.isAdminUser(this.username));

        userComp = new User(userNode);
        userComp.setRoles(StringUtils.convertCollectionToString(wikiUser.getRoles().iterator(), " "));

    }

    public boolean isRecurse()
    {
        return recurse;
    }

    public void setRecurse(boolean recurse)
    {
        this.recurse = recurse;
    }

    public Set<RSComponent> getUserStreams() throws Exception
    {
    	long startTime = System.currentTimeMillis();
   	 	long startTimeInstance = startTime;
   	 	
        //add this current user
        streams.addAll(evaluateNodeAndConvertToRSComponent(userNode, false));
   	 	
   	 	if (Log.log.isDebugEnabled()) {
   	 		Log.log.debug(String.format("Time taken to add user node %s to streams, streams count %d: %d msec", 
   	 									userNode, streams.size(), (System.currentTimeMillis() - startTimeInstance)));
   	 		startTimeInstance = System.currentTimeMillis();
   	 	}

        Collection<ResolveNodeVO> followingComps = ServiceGraph.getNodesFollowedBy(userNode.getSys_id(), null, null, null, username);
        
        if (Log.log.isDebugEnabled()) {
   	 		Log.log.debug(String.format("Time taken to find %d components followed by %s: %d msec", 
   	 									followingComps.size(), userNode, (System.currentTimeMillis() - startTimeInstance)));
   	 		startTimeInstance = System.currentTimeMillis();
   	 	}
        
        followingComps.addAll(ServiceGraph.getGroupNodes(userNode.getSys_id(), null, null, null, username));
        followingComps.addAll(ServiceGraph.getProcessAndTeamNodesFollowingMe(userNode.getSys_id(), username));
        
        if (Log.log.isDebugEnabled()) {
   	 		Log.log.debug(String.format("Time taken to find group, process, and team components followed by %s, " +
   	 									" total components followed is %d: %d msec", 
   	 									userNode, followingComps.size(), (System.currentTimeMillis() - startTimeInstance)));
   	 		startTimeInstance = System.currentTimeMillis();
   	 	}
        
        if (CollectionUtils.isNotEmpty(followingComps))
        {
        	followingComps.parallelStream().forEach(compNode -> {
        		try {
					streams.addAll(evaluateNodeAndConvertToRSComponent(compNode, true));
				} catch (Exception e) {
					Log.log.error(String.format("Error [%s] in converting %s node to Resolve Component", e.getMessage(),
												compNode), e);
					try {
						throw e;
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
        	});
        }
        
        if (Log.log.isDebugEnabled()) {
   	 		Log.log.debug(String.format("Time taken to convert %d components followed by user node %s and adding to " +
   	 									"streams, streams count %d: %d msec", 
   	 									followingComps.size(), userNode, streams.size(), 
   	 									(System.currentTimeMillis() - startTimeInstance)));
   	 		startTimeInstance = System.currentTimeMillis();
   	 	}
        
        streams.parallelStream().forEach(comp -> {
        	try {
				comp.setUnReadCount(SocialSearchAPI.countUnreadPostByStreamId(comp.getSys_id(), comp.getType().name(), 
																			  userInfo));
			} catch (SearchException e) {
				Log.log.error(String.format("Error [%s] in finding count of unread posts for Resolve Component %s", 
											e.getMessage(), comp), e);
				
				try {
					throw e;
				} catch (SearchException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
        });
        
        if (Log.log.isDebugEnabled()) {
   	 		Log.log.debug(String.format("Time taken to find and set unread posts counts by stream id for %d components in " +
   	 									"streams : %d msec", streams.size(), 
   	 									(System.currentTimeMillis() - startTimeInstance)));   	 		
   	 		Log.log.debug(String.format("Total time taken to get user streams for user node %s: %d msec", userNode,
   	 									(System.currentTimeMillis() - startTime)));
   	 	}
        
        return streams;
    }

    public Set<RSComponent> getSmallUserStreams() throws Exception
    {
        //add this current user
        streams.addAll(evaluateNodeAndConvertToRSComponent(userNode, false));

        Collection<ResolveNodeVO> followingComps = ServiceGraph.getNodesFollowedByExceptDocAndAT(userNode.getSys_id(), null, null, null, username);
        
        followingComps.addAll(ServiceGraph.getGroupNodes(userNode.getSys_id(), null, null, null, username));
        followingComps.addAll(ServiceGraph.getProcessAndTeamNodesFollowingMe(userNode.getSys_id(), username));
        
        if (followingComps != null && followingComps.size() > 0)
        {
            for (ResolveNodeVO componentNode : followingComps)
            {
                streams.addAll(evaluateNodeAndConvertToRSComponentExceptDocAndAT(componentNode, true));
            }
        }
        
        for(RSComponent comp : this.streams)
        {
            comp.setUnReadCount(SocialSearchAPI.countUnreadPostByStreamId(comp.getSys_id(), comp.getType().name(), userInfo));
        }

        return streams;
    }
    
    private Set<RSComponent> evaluateNodeAndConvertToRSComponent(ResolveNodeVO node, boolean directFollow) throws Exception {
        Set<RSComponent> result = new HashSet<RSComponent>();
        NodeType type = node.getUType();

        if (isRecurse()) {
            if (type == NodeType.FORUM) {
                //a FORUM can be a part of PROCESS - so the user can have indirect reference
                //get the list of PROCESS that this belongs to Forum
                Collection<ResolveNodeVO> nodesFollowingMe = ServiceGraph.getNodesFollowingMe(node.getSys_id(), null, null, 
                																			  null, username);
                // Forum is a group
                nodesFollowingMe.addAll(ServiceGraph.getMemberNodes(node.getSys_id(), null, null, null, username));
                
                if (CollectionUtils.isNotEmpty(nodesFollowingMe)) {
                	nodesFollowingMe.parallelStream().forEach(nodeFollowingMe -> {
                        NodeType nodeFollowingMeType = nodeFollowingMe.getUType();
                        
                        //if this a USER node, ignore it 
                        if (nodeFollowingMeType == NodeType.USER) {
                            return;
                        }

                        //if the comp is already considered before, ignore it
                        if(!compSysIds.contains(nodeFollowingMe.getSys_id())) {
                            try {
								result.addAll(convertNodeToRsComponent(nodeFollowingMe, false));
							} catch (Throwable t) {
								Log.log.error(String.format("Error %sin converting node %s following node %s of FORUM type " +
															"to RSComponent", 
															(StringUtils.isNotEmpty(t.getMessage()) ? 
															 "[" + t.getMessage() + "] " : ""), nodeFollowingMe, node));
							}
                        }

                        //**RECURSIVE - GOES INTO INFINITE LOOP 
                        //The above comment was in the older version...not sure if we want to recurse the Processes here - TBD
                    });
                }
            } else if (type == NodeType.TEAM) {
                //a TEAM can be a part of another TEAM
                //get list of Teams that this Team belongs to
                //a TEAM can be a part of PROCESS
                //get list of Processes that this Team belongs to
                //this will get everything
                Collection<ResolveNodeVO> nodesFollowingMe = ServiceGraph.getNodesFollowingMe(node.getSys_id(), null, null, 
                																			  null, username);
                // Team is a group
                nodesFollowingMe.addAll(ServiceGraph.getMemberNodes(node.getSys_id(), null, null, null, username));
                
                if (CollectionUtils.isNotEmpty(nodesFollowingMe)) {
                	nodesFollowingMe.parallelStream().forEach(nodeFollowingMe -> {
                        NodeType nodeFollowingMeType = nodeFollowingMe.getUType();

                        //if this a USER node, ignore it 
                        if (nodeFollowingMeType == NodeType.USER) {
                        	return;
                        }

                        if (nodeFollowingMeType == NodeType.TEAM) {
                            //if the comp is already considered before, ignore it
                            if(!compSysIds.contains(nodeFollowingMe.getSys_id())) {
                                //**RECURSIVE
                                compSysIds.add(nodeFollowingMe.getSys_id()); //prevent infinite loop
                                try {
									result.addAll(evaluateNodeAndConvertToRSComponent(nodeFollowingMe, false));
								} catch (Throwable t) {
									Log.log.error(String.format("Error %sin evaluating and converting node %s of type TEAM " +
																"following node %s of TEAM type to RSComponent", 
																(StringUtils.isNotEmpty(t.getMessage()) ? 
																 "[" + t.getMessage() + "] " : ""), nodeFollowingMe, node));
								}
                            }
                        } else if (nodeFollowingMeType == NodeType.PROCESS) {
                            //TEAM can be in another TEAM OR PROCESS
                            //this will be for PROCESS
                            //if the comp is already considered before, ignore it
                            if(!compSysIds.contains(nodeFollowingMe.getSys_id())) {
                                try {
									result.addAll(convertNodeToRsComponent(nodeFollowingMe, false));
								} catch (Exception t) {
									Log.log.error(String.format("Error %sin evaluating and converting node %s of type " +
																"PROCESS following node %s of TEAM type to RSComponent", 
																(StringUtils.isNotEmpty(t.getMessage()) ? 
																 "[" + t.getMessage() + "] " : ""), nodeFollowingMe, node));
								}
                            }
                        }
                    });
                }
            } else if (type == NodeType.PROCESS) {
                //a PROCESS can have other entities like DOC, FORUMS, TEAM etc
                //get a list of all the ENTITIES that this PROCESS contains, except for the USERS. 
                //do not include the USERS belonging to a Process
                Collection<ResolveNodeVO> nodesThatThisProcessContains = 
                								ServiceGraph.getNodesFollowedBy(node.getSys_id(), null, null, null, username);
                // Process is a group
                nodesThatThisProcessContains.addAll(ServiceGraph.getMemberNodes(node.getSys_id(), null, null, null, username));
                
                if (CollectionUtils.isNotEmpty(nodesThatThisProcessContains)) {
                	nodesThatThisProcessContains.parallelStream().forEach(nodesThatThisProcessContain -> {
                        NodeType nodeFollowingMeType = nodesThatThisProcessContain.getUType();

                        //if this a USER node, ignore it
                        if (nodeFollowingMeType == NodeType.USER) {
                        	return;
                        }

                        //**RECURSIVE
                        if(!compSysIds.contains(nodesThatThisProcessContain.getSys_id())) {
                            try {
								result.addAll(evaluateNodeAndConvertToRSComponent(nodesThatThisProcessContain, false));
							} catch (Throwable t) {
								Log.log.error(String.format("Error %sin converting node %s following node %s of PROCESS " +
															"type to RSComponent", 
															(StringUtils.isNotEmpty(t.getMessage()) ? 
															 "[" + t.getMessage() + "] " : ""), nodesThatThisProcessContain, 
															node));
							}
                        }
                    });//end of for loop
                }
            }
        }

        //add the main one
        result.addAll(convertNodeToRsComponent(node, directFollow));

        return result;
    }

    private Set<RSComponent> evaluateNodeAndConvertToRSComponentExceptDocAndAT(ResolveNodeVO node, 
    																		   boolean directFollow) throws Exception {
        Set<RSComponent> result = new HashSet<RSComponent>();
        NodeType type = node.getUType();

        if (isRecurse()) {
            /* FORUM, TEAM and PROCESS nodes are group/container. 
             * TEAM can contain TEAM nodes apart from USER nodes.
             * PROCESS can contain any nodes apart from PROCESS node.
             */
            if (type == NodeType.FORUM) {
                //a FORUM can be a part of PROCESS - so the user can have indirect reference
                //get the list of PROCESS that this belongs to Forum
                Collection<ResolveNodeVO> nodesFollowingMe = ServiceGraph.getNodesFollowingMe(node.getSys_id(), null, null, 
                																			  null, username);
                // Forum is a group
                nodesFollowingMe.addAll(ServiceGraph.getMemberNodes(node.getSys_id(), null, null, null, username));
                
                if (CollectionUtils.isNotEmpty(nodesFollowingMe)) {
                	nodesFollowingMe.parallelStream().forEach(nodeFollowingMe -> {
                        NodeType nodeFollowingMeType = nodeFollowingMe.getUType();
                        
                        if (Log.log.isDebugEnabled() && 
                            (nodeFollowingMeType == NodeType.DOCUMENT || nodeFollowingMeType == NodeType.ACTIONTASK ||
                             nodeFollowingMeType == NodeType.USER)) {
                            Log.log.debug(String.format("%s either FOLLOWS or is MEMBER of %s", nodeFollowingMe, node));
                        }
                        
                        //if this node is a USER or DOCUMENT or ACTIONTASK node, then ignore it
                        if (nodeFollowingMeType == NodeType.USER || 
                            nodeFollowingMeType == NodeType.DOCUMENT || 
                            nodeFollowingMeType == NodeType.ACTIONTASK) {
                        	return;
                        }

                        //if the comp is already considered before, ignore it
                        if(!compSysIds.contains(nodeFollowingMe.getSys_id())) {
                            try {
								result.addAll(convertNodeToRsComponent(nodeFollowingMe, false));
							} catch (Throwable t) {
								Log.log.error(String.format("Error %sin converting node %s following node %s of FORUM type " +
															"to RSComponent", 
															(StringUtils.isNotEmpty(t.getMessage()) ? 
															 "[" + t.getMessage() + "] " : ""), nodeFollowingMe, node));
							}
                        }
                    });
                }
            } else if (type == NodeType.TEAM) {
                //a TEAM can be a part of another TEAM
                //get list of Teams that this Team belongs to
                //a TEAM can be a part of PROCESS
                //get list of Processes that this Team belongs to
                //this will get everything
                Collection<ResolveNodeVO> nodesFollowingMe = ServiceGraph.getNodesFollowingMe(node.getSys_id(), null, null, 
                																			  null, username);
                // Team is a group
                nodesFollowingMe.addAll(ServiceGraph.getMemberNodes(node.getSys_id(), null, null, null, username));
                
                if (CollectionUtils.isNotEmpty(nodesFollowingMe)) {
                	nodesFollowingMe.parallelStream().forEach(nodeFollowingMe -> {
                        NodeType nodeFollowingMeType = nodeFollowingMe.getUType();

                        if (Log.log.isDebugEnabled() && 
                            (nodeFollowingMeType == NodeType.DOCUMENT || nodeFollowingMeType == NodeType.ACTIONTASK ||
                             nodeFollowingMeType == NodeType.USER)) {
                            Log.log.debug(String.format("%s either FOLLOWS or is MEMBER of %s", nodeFollowingMe, node));
                        }
                        
                        //if this node is a USER or DOCUMENT or ACTIONTASK node, then ignore it
                        if (nodeFollowingMeType == NodeType.USER || 
                            nodeFollowingMeType == NodeType.DOCUMENT || 
                            nodeFollowingMeType == NodeType.ACTIONTASK) {
                            return;
                        }

                        if (nodeFollowingMeType == NodeType.TEAM) {
                            //if the comp is already considered before, ignore it
                            if(!compSysIds.contains(nodeFollowingMe.getSys_id()))
                            {
                                //**RECURSIVE
                                compSysIds.add(nodeFollowingMe.getSys_id()); //prevent infinite loop
                                try {
									result.addAll(evaluateNodeAndConvertToRSComponent(nodeFollowingMe, false));
								} catch (Throwable t) {
									Log.log.error(String.format("Error %sin evaluating and converting node %s of type TEAM " +
																"following node %s of TEAM type to RSComponent", 
																(StringUtils.isNotEmpty(t.getMessage()) ? 
																 "[" + t.getMessage() + "] " : ""), nodeFollowingMe, node));
								}
                            }
                        } else if (nodeFollowingMeType == NodeType.PROCESS) {
                            //TEAM can be in another TEAM OR PROCESS
                            //this will be for PROCESS
                            //if the comp is already considered before, ignore it
                            if(!compSysIds.contains(nodeFollowingMe.getSys_id())) {
                                try {
									result.addAll(convertNodeToRsComponent(nodeFollowingMe, false));
								} catch (Throwable t) {
									Log.log.error(String.format("Error %sin evaluating and converting node %s of type " +
											"PROCESS following node %s of TEAM type to RSComponent", 
											(StringUtils.isNotEmpty(t.getMessage()) ? 
											 "[" + t.getMessage() + "] " : ""), nodeFollowingMe, node));
								}
                            }
                        }
                    });
                }
            } else if (type == NodeType.PROCESS) {
                //a PROCESS can have other entities like DOC, FORUMS, TEAM etc
                //get a list of all the ENTITIES that this PROCESS contains, except for the USERS. 
                //do not include the USERS belonging to a Process
                Collection<ResolveNodeVO> nodesThatThisProcessContains = ServiceGraph.getNodesFollowedBy(node.getSys_id(), 
                																						 null, null, null, 
                																						 username);
                // Process is a group
                nodesThatThisProcessContains.addAll(ServiceGraph.getMemberNodes(node.getSys_id(), null, null, null, username));
                
                if (CollectionUtils.isNotEmpty(nodesThatThisProcessContains)) {
                	nodesThatThisProcessContains.parallelStream().forEach(nodeFollowingMe -> {
                        NodeType nodeFollowingMeType = nodeFollowingMe.getUType();

                        if (Log.log.isTraceEnabled() && 
                            (nodeFollowingMeType == NodeType.DOCUMENT || nodeFollowingMeType == NodeType.ACTIONTASK ||
                             nodeFollowingMeType == NodeType.USER))  {
                        	Log.log.debug(String.format("%s either FOLLOWS or is MEMBER of %s", nodeFollowingMe, node));
                        }

                        //if this node is a USER or DOCUMENT or ACTIONTASK node, then ignore it
                        if (nodeFollowingMeType == NodeType.USER || 
                            nodeFollowingMeType == NodeType.DOCUMENT || 
                            nodeFollowingMeType == NodeType.ACTIONTASK) {
                            return;
                        }

                        //**RECURSIVE
                        if(!compSysIds.contains(nodeFollowingMe.getSys_id())) {
                            try {
								result.addAll(evaluateNodeAndConvertToRSComponentExceptDocAndAT(nodeFollowingMe, false));
							} catch (Throwable t) {
								Log.log.error(String.format("Error %sin converting node %s following node %s of PROCESS " +
															"type to RSComponent", 
															(StringUtils.isNotEmpty(t.getMessage()) ? 
															 "[" + t.getMessage() + "] " : ""), nodeFollowingMe, node));
							}
                        }
                    });//end of for loop
                }
            }
        }

        //add the node that is NOT FORUM or TEAM or PROCESS
        result.addAll(convertNodeToRsComponent(node, directFollow));

        return result;
    }
    
    private Set<RSComponent> convertNodeToRsComponent(ResolveNodeVO node, boolean directFollow) throws Exception
    {
        Set<RSComponent> result = new HashSet<RSComponent>();
        NodeType type = node.getUType();
        boolean markedDeleted = node.ugetUMarkDeleted();

        if (!markedDeleted)
        {
            //if the type is Document, we have to add Runbook and DT as separate listing for the UI
            if (type == NodeType.DOCUMENT)
            {
                Document doc = new Document(node);
                doc.setDirectFollow(directFollow);

                //add it to the list
                result.add(doc);

                //if the document is runbook
                if (doc.isRunbook())
                {
                    try
                    {
                        Runbook runbook = new Runbook(node);
                        runbook.setDirectFollow(directFollow);
                        result.add(runbook);
                    }
                    catch (Exception e)
                    {
                        //do nothing
                    }
                }

                //if the doc is DT
                if (doc.isDecisionTree())
                {
                    try
                    {
                        DecisionTree dt = new DecisionTree(node);
                        dt.setDirectFollow(directFollow);
                        result.add(dt);
                    }
                    catch (Exception e)
                    {
                        //do nothing
                    }
                }
            }
            else
            {
                RSComponent comp = new RSComponent(node);

                //set the flag
                comp.setDirectFollow(directFollow);
                if (comp.getSys_id().equalsIgnoreCase(this.userNode.getUCompSysId()))
                {
                    comp.setCurrentUser(true);
                }

                //add it to the list
                result.add(comp);
            }
        }
        
        compSysIds.add(node.getSys_id());

        return result;
    }
    
    private Set<RSComponent> evaluateProcessNodeAndConvertToRSComponentDocOnly(ResolveNodeVO node, boolean directFollow, 
    																		   boolean indirectlyFollowed) throws Exception {
        Set<RSComponent> result = new HashSet<RSComponent>();
        NodeType type = node.getUType();

        if (isRecurse())
        {
            if (type == NodeType.PROCESS)
            {
                //a PROCESS can have other entities like DOC, FORUMS, TEAM etc
                //get a list of all the DOC ENTITIES that this PROCESS contains.
                Collection<ResolveNodeVO> nodesFollowingMe = ServiceGraph.getDocumentNodesFollowedBy(node.getSys_id(), null, null, null, username, 0, 0);
                // Process is a group
                nodesFollowingMe.addAll(ServiceGraph.getDocumentMemberNodes(node.getSys_id(), null, null, null, username));
                
                if (CollectionUtils.isNotEmpty(nodesFollowingMe))
                {
                	nodesFollowingMe.parallelStream().forEach(docNodeFollowingMe -> {
                        //**RECURSIVE
                        if(!compSysIds.contains(docNodeFollowingMe.getSys_id())) {
                            try {
								result.addAll(evaluateProcessNodeAndConvertToRSComponentDocOnly(docNodeFollowingMe, false, 
																								indirectlyFollowed));
							} catch (Throwable t) {
								Log.log.error(String.format("Error %sin converting node %s following node %s of PROCESS " +
															"type to RSComponent", 
															(StringUtils.isNotEmpty(t.getMessage()) ? 
															 "[" + t.getMessage() + "] " : ""), docNodeFollowingMe, node));
							}
                        }
                    });//end of for loop
                }
            }
        }

        if (node.getUType() == NodeType.DOCUMENT /*|| node.getUType() == NodeType.ACTIONTASK */)
        {
            if (!indirectlyFollowed || !ServiceGraph.isDirectlyFollowedBy(userNode.getSys_id(), node.getSys_id(), username))
            {
                //add the DOCUMENT/ACTIONTASK nodes only
                result.addAll(convertNodeToRsComponent(node, directFollow));
            }
        }

        return result;
    }
    
    public Set<RSComponent> getIndirectDocumentUserStream() throws Exception
    {
        Collection<ResolveNodeVO> followingComps = ServiceGraph.getProcessNodesFollowedBy(userNode.getSys_id(), null, null, null, username);
        
        followingComps.addAll(ServiceGraph.getProcessGroupNodes(userNode.getSys_id(), null, null, null, username));
        followingComps.addAll(ServiceGraph.getProcessNodesFollowingMe(userNode.getSys_id(), username));
        
        if (followingComps != null && followingComps.size() > 0)
        {
            for (ResolveNodeVO componentNode : followingComps)
            {
                streams.addAll(evaluateProcessNodeAndConvertToRSComponentDocOnly(componentNode, true, true));
            }
        }
        
        for(RSComponent comp : this.streams)
        {
            comp.setUnReadCount(SocialSearchAPI.countUnreadPostByStreamId(comp.getSys_id(), comp.getType().name(), userInfo));
            //comp.setComponentNotification(ServiceSocial.getSpecificNotificationFor(comp.getSysId(), userComp));
        }

        return streams;
    }
    
//    private UserInfo getUserInfo() throws Exception
//    {
//        UserInfo result = new UserInfo(userNode.getUCompSysId(), "", userNode.getUCompName(), user.getRoles() + " " + INTERNAL_POST_MEMBER_ROLE, isAdminUser(user));
//        return result;
//    }
    
    public Set<RSComponent> getDirectDocumentUserStream(int start, int limit) throws Exception
    {
        Collection<ResolveNodeVO> followingComps = ServiceGraph.getDocumentNodesFollowedBy(userNode.getSys_id(), null, null, null, username, start, limit);
        
        if (followingComps != null && followingComps.size() > 0)
        {
            for (ResolveNodeVO componentNode : followingComps)
            {
                streams.addAll(evaluateProcessNodeAndConvertToRSComponentDocOnly(componentNode, true, false));
            }
        }
        
        for(RSComponent comp : this.streams)
        {
            comp.setUnReadCount(SocialSearchAPI.countUnreadPostByStreamId(comp.getSys_id(), comp.getType().name(), userInfo));
            //comp.setComponentNotification(ServiceSocial.getSpecificNotificationFor(comp.getSysId(), userComp));
        }

        return streams;
    }
    
    private Set<RSComponent> evaluateProcessNodeAndConvertToRSComponentActionTaskOnly(ResolveNodeVO node, boolean directFollow, boolean indirectlyFollowed) throws Exception
    {
        Set<RSComponent> result = new HashSet<RSComponent>();
        NodeType type = node.getUType();

        if (isRecurse())
        {
            if (type == NodeType.PROCESS)
            {
                //a PROCESS can have other entities like DOC, FORUMS, TEAM etc
                //get a list of all the DOC ENTITIES that this PROCESS contains.
                Collection<ResolveNodeVO> nodesFollowingMe = ServiceGraph.getActionTaskNodesFollowedBy(node.getSys_id(), null, null, null, username, 0, 0);
                // Process is a group
                nodesFollowingMe.addAll(ServiceGraph.getActionTaskMemberNodes(node.getSys_id(), null, null, null, username));
                
                if (nodesFollowingMe != null && !nodesFollowingMe.isEmpty())
                {
                    for (ResolveNodeVO docNodeFollowingMe : nodesFollowingMe)
                    {
                        //**RECURSIVE
                        if(!compSysIds.contains(docNodeFollowingMe.getSys_id()))
                        {
                            result.addAll(evaluateProcessNodeAndConvertToRSComponentActionTaskOnly(docNodeFollowingMe, false, indirectlyFollowed));
                        }
                    }//end of for loop
                }

            }
        }

        if (node.getUType() == NodeType.ACTIONTASK)
        {
            if (!indirectlyFollowed || !ServiceGraph.isDirectlyFollowedBy(userNode.getSys_id(), node.getSys_id(), username))
            {
                //add the ACTIONTASK nodes only
                result.addAll(convertNodeToRsComponent(node, directFollow));
            }
        }

        return result;
    }
    
    public Set<RSComponent> getIndirectActionTaskUserStream() throws Exception
    {
        Collection<ResolveNodeVO> followingComps = ServiceGraph.getProcessNodesFollowedBy(userNode.getSys_id(), null, null, null, username);
        
        followingComps.addAll(ServiceGraph.getProcessGroupNodes(userNode.getSys_id(), null, null, null, username));
        followingComps.addAll(ServiceGraph.getProcessNodesFollowingMe(userNode.getSys_id(), username));
        
        if (followingComps != null && followingComps.size() > 0)
        {
            for (ResolveNodeVO componentNode : followingComps)
            {
                streams.addAll(evaluateProcessNodeAndConvertToRSComponentActionTaskOnly(componentNode, true, true));
            }
        }
        
        for(RSComponent comp : this.streams)
        {
            comp.setUnReadCount(SocialSearchAPI.countUnreadPostByStreamId(comp.getSys_id(), comp.getType().name(), userInfo));
            //comp.setComponentNotification(ServiceSocial.getSpecificNotificationFor(comp.getSysId(), userComp));
        }

        return streams;
    }
    
    public Set<RSComponent> getDirectActionTaskUserStream(int start, int limit) throws Exception
    {
        Collection<ResolveNodeVO> followingComps = ServiceGraph.getActionTaskNodesFollowedBy(userNode.getSys_id(), null, null, null, username, start, limit);
        
        if (followingComps != null && followingComps.size() > 0)
        {
            for (ResolveNodeVO componentNode : followingComps)
            {
                streams.addAll(evaluateProcessNodeAndConvertToRSComponentActionTaskOnly(componentNode, true, false));
            }
        }
        
        for(RSComponent comp : this.streams)
        {
            comp.setUnReadCount(SocialSearchAPI.countUnreadPostByStreamId(comp.getSys_id(), comp.getType().name(), userInfo));
            //comp.setComponentNotification(ServiceSocial.getSpecificNotificationFor(comp.getSysId(), userComp));
        }

        return streams;
    }

}
