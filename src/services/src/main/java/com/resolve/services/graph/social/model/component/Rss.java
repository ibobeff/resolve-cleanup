/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.component;

import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.RSPublisher;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;

/**
 * 
 * @author jeet.marwah
 *
 */
public class Rss extends RSComponent implements RSPublisher
{
    private static final long serialVersionUID = 3833883385945478884L;
    
//    public static final String TYPE = "rss";

    public Rss()
    {
        super(NodeType.RSS);
    }
    
    public Rss(String sysId, String name) 
    {
        super(NodeType.RSS);
        setId(sysId);
        setName(name);
    }
    
    public Rss(ResolveNodeVO node) throws Exception
    {
        super(node);
    }
}
