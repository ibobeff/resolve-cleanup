/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.actiontask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;

import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.MetaFormAction;
import com.resolve.persistence.model.ResolveActionInvoc;
import com.resolve.persistence.model.ResolveActionInvocOptions;
import com.resolve.persistence.model.ResolveActionParameter;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.ResolveActionTaskMockData;
import com.resolve.persistence.model.ResolveAssess;
import com.resolve.persistence.model.ResolveParser;
import com.resolve.persistence.model.ResolvePreprocess;
import com.resolve.persistence.model.ResolveTaskExpression;
import com.resolve.persistence.model.ResolveTaskOutputMapping;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.search.actiontask.ActionTaskIndexAPI;
import com.resolve.services.ServiceGraph;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.graph.social.model.message.SubmitNotification;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.util.AssessorUtil;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.ParserUtil;
import com.resolve.services.hibernate.util.PreprocessorUtil;
import com.resolve.services.hibernate.util.ResolveArchiveUtils;
import com.resolve.services.hibernate.util.SocialUtil;
import com.resolve.services.social.notification.NotificationHelper;
import com.resolve.services.util.UserUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class DeleteActionTask
{
    protected ResolveActionTask resolveActionTask;
    protected String userName;
    protected List<String> actionTaskIds;
    protected String fullName;
    protected boolean warn;
    protected Set<String> referredWikiNameSet = new HashSet<String>();
    
    private List<ResolveActionInvocOptions> invocOptionsList = new ArrayList<ResolveActionInvocOptions>();
    
    public DeleteActionTask(List<String> actionTaskIds, String fullName, boolean warn, String userName) throws Exception
    {
        this.actionTaskIds = actionTaskIds == null ? new ArrayList<String>() : actionTaskIds;
        this.fullName = fullName;
        this.warn = warn;
        this.userName = userName;
    }
    
    public Set<String> delete() throws Exception {
        if (!warn || !foundReferences()) {
            Set<String> userRoles = UserUtils.getUserRoles(userName);
            Set<String> errorAT = new HashSet<String>();
            Set<String> deleteErrorTaskNameSet = new HashSet<String>(); // Collect full name of a task(s) which could not be deleted.
            Iterator<String> taskIdIterator = actionTaskIds.iterator();
            while (taskIdIterator.hasNext()) {
                String actionTaskId = taskIdIterator.next();
                try
                {
                  HibernateProxy.setCurrentUser(userName);
                	HibernateProxy.execute(() -> {
                		boolean userCanDelete = false;
                		AccessRights rights = null;
                    
	                    resolveActionTask = HibernateUtil.getDAOFactory().getResolveActionTaskDAO().findById(actionTaskId);
	                    if(resolveActionTask != null) {
	                        if (resolveActionTask.getUNamespace().toLowerCase().startsWith(Constants.RESOLVE_NAMESPACE) && !userName.equalsIgnoreCase(Constants.USER_RESOLVE_MAINT)) {
	                            Log.log.error("Only resolve.maint can delete tasks from 'resolve' namespace");
	                            deleteErrorTaskNameSet.add(resolveActionTask.getUFullName());
	                            taskIdIterator.remove();
	                            return;
	                        }
	                        //user should have Admin rights on this AT to delete it
	                        rights = resolveActionTask.getAccessRights();
	                        if (rights != null) {
	                            userCanDelete = com.resolve.services.util.UserUtils.hasRole(userName, userRoles, rights.getUAdminAccess());
	                        }
	                        
	                        if(rights == null || userCanDelete) {
	                            // Delete all the relationships
	                            deleteActionTaskReferrences();
	                            
	                            // delete the main rec
	                            HibernateUtil.getDAOFactory().getResolveActionTaskDAO().delete(resolveActionTask);
	                        }  else {
	                            errorAT.add(resolveActionTask.getUFullName());
	                            taskIdIterator.remove();
	                        }
	                    }
                    
	                    if(resolveActionTask != null && (rights == null || userCanDelete)) {
	                    	postDeleteOperation(actionTaskId);
	                    }
                	});
                } catch (Throwable e) {
                    Log.log.warn(e.getMessage(), e);
                    
                    errorAT.add(resolveActionTask.getUFullName());
//                    throw new Exception(e); 
                                      HibernateUtil.rethrowNestedTransaction(e);
                }
            }
            StringBuilder errorMessage = new StringBuilder();
            if (CollectionUtils.isNotEmpty(deleteErrorTaskNameSet) || CollectionUtils.isNotEmpty(errorAT)) {
                // if any of the task name sets is not empty, construct appropriate error message.
                if (CollectionUtils.isNotEmpty(deleteErrorTaskNameSet)) {
                    errorMessage.append(String.format("Task(s), %s, could not be deleted as only %s user can delete task(s) from resolve napespace\n", 
                                    StringUtils.convertCollectionToString(deleteErrorTaskNameSet.iterator(), ","), Constants.USER_RESOLVE_MAINT));
                }
                if (CollectionUtils.isNotEmpty(errorAT)) {
                    errorMessage.append(String.format("User %s does not have enough rights to delete tasks, %s",
                                    userName, StringUtils.convertCollectionToString(errorAT.iterator(), ",")));
                }
            }
            // clear the cache of the tasks which got deleted successfully.
            if (CollectionUtils.isNotEmpty(actionTaskIds)) {
                Map<String, Object> params = new HashMap<String, Object>();
                params.put(Constants.EXECUTE_ACTIONID, actionTaskIds);
                MainBase.getESB().sendInternalMessage(Constants.ESB_NAME_RSCONTROLS, "MAction.cacheFlush", params);
                actionTaskIds.forEach(id -> ActionTaskUtil.removeCacheById(id));
            }
            // If some tasks could not be deleted, inform user about it.
            if (errorMessage.length() > 0) {
                throw new Exception(errorMessage.toString());
            }
        }
        
        return referredWikiNameSet;
    }
    
    private void deleteActionTaskReferrences() throws Exception
    {
        // Delete Access Right
        if (resolveActionTask.getAccessRights() != null)
        {
            HibernateUtil.getDAOFactory().getAccessRightsDAO().delete(resolveActionTask.getAccessRights());
        }
        
        ResolveActionInvoc actionInvoc = resolveActionTask.getResolveActionInvoc();
        if (actionInvoc != null)
        {
            // Delete Assessor
            ResolveAssess assess = actionInvoc.getAssess();
            if (assess != null)
            {
                // Check if it's local assess
                if (assess.getUName().equals(resolveActionTask.getUFullName()))
                {
                    String[] assessIdList = new String[1];
                    assessIdList[0] = assess.getSys_id();
                    try
                    {
                        AssessorUtil.deleteResolveAssessByIds(assessIdList, false, userName);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Could not delete Assessor for ActionTask : " + resolveActionTask.getUFullName(), e);
                        throw new Exception("Could not delete Assessor for ActionTask : " + resolveActionTask.getUFullName());
                    }
                }
            }
            
            // Delete Preprocessor
            ResolvePreprocess preprocess = actionInvoc.getPreprocess();
            if (preprocess != null)
            {
                // Check if it's local preprocess
                if (preprocess.getUName().equals(resolveActionTask.getUFullName()))
                {
                    String[] preprocessIdArray = new String[1];
                    preprocessIdArray[0] = preprocess.getSys_id();
                    try
                    {
                        PreprocessorUtil.deleteResolvePreprocessByIds(preprocessIdArray, false, userName);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Could not delete Preprocessor for ActionTask : " + resolveActionTask.getUFullName(), e);
                        throw new Exception("Could not delete Preprocessor for ActionTask : " + resolveActionTask.getUFullName());
                    }
                }
            }
            
            // Delete parser
            ResolveParser parser = actionInvoc.getParser();
            if (parser != null)
            {
                // Check if it's local parser
                if (parser.getUName().equals(resolveActionTask.getUFullName()))
                {
                    String[] parserIdArray = new String[1];
                    parserIdArray[0] = parser.getSys_id();
                    
                    try
                    {
                        ParserUtil.deleteResolveParserByIds(parserIdArray, false, userName);
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Could not delete Parser for ActionTask : " + resolveActionTask.getUFullName(), e);
                        throw new Exception("Could not delete Parser for ActionTask : " + resolveActionTask.getUFullName());
                    }
                }
            }
            
            // Delete Parameters
            Collection<ResolveActionParameter> parameterColl = actionInvoc.getResolveActionParameters();
            if (parameterColl != null && parameterColl.size() > 0)
            {
                for (ResolveActionParameter parameter : parameterColl)
                {
                    HibernateUtil.getDAOFactory().getResolveActionParameterDAO().delete(parameter);
                }
            }
            
            // Delete InvocOptions
            Collection<ResolveActionInvocOptions> invocOptionColl = actionInvoc.getResolveActionInvocOptions();
            if (invocOptionColl != null && invocOptionColl.size() > 0)
            {
                for (ResolveActionInvocOptions invocOption : invocOptionColl)
                {
                    //if (invocOption.getUName().equals("INPUTFILE"))
                    {
                        HibernateUtil.getDAOFactory().getResolveActionInvocOptionsDAO().delete(invocOption);
                        
                        if (invocOption.getUName().equals("INPUTFILE"))
                        {
                            invocOptionsList.add(invocOption);
                        }
                    }
                }
            }
            
            // Delete mock data
            Collection<ResolveActionTaskMockData> mockDataColl = actionInvoc.getMockDataCollection();
            if (mockDataColl != null && mockDataColl.size() > 0)
            {
                for (ResolveActionTaskMockData mockData : mockDataColl)
                {
                    HibernateUtil.getDAOFactory().getResolveActionTaskMockDataDAO().delete(mockData);
                }
            }
            
            Collection<ResolveTaskExpression> expressions = actionInvoc.getExpressions();
            if (expressions != null && expressions.size() > 0)
            {
            	for (ResolveTaskExpression expression : expressions)
            	{
            		HibernateUtil.getDAOFactory().getResolveTaskExpressionDAO().delete(expression);
            	}
            }
            
            Collection<ResolveTaskOutputMapping> outputMappings = actionInvoc.getOutputMappings();
            if (outputMappings != null && outputMappings.size() > 0)
            {
            	for (ResolveTaskOutputMapping mapping : outputMappings)
            	{
            		HibernateUtil.getDAOFactory().getResolveTaskOutputMappingDAO().delete(mapping);
            	}
            }
            
            // Delete Action Invoc
            HibernateUtil.getDAOFactory().getResolveActionInvocDAO().delete(actionInvoc);
        }
    }
    
    private void postDeleteOperation(String actionTaskId) throws Exception
    {
        // Send notification
        sendNotifications();
        
        // Delete the node from graph
        deleteGraphNode();
        
        // Delete indexes
        deleteIndexes(actionTaskId);
        
        // Send esb msg to delete the CONTENT
        if(invocOptionsList.size() > 0)
        {
            for (ResolveActionInvocOptions invocOptions : invocOptionsList)
            {
                ResolveArchiveUtils.submitPurgeArchiveRequest("ResolveActionInvocOptions", invocOptions.getSys_id(), userName);
            }
        }
        
        deleteActionTaskFromFormButtonAction();
    }
    
    private void deleteGraphNode() throws Exception
    {
        try
        {
            ServiceGraph.deleteNode(null, resolveActionTask.getSys_id(), null, NodeType.ACTIONTASK, userName);
//            ServiceSocial.deleteComponent(resolveActionTask.getSys_id(), userName);
        }
        catch (Exception e)
        {
            Log.log.error("error while deleting actiontask from graph " + resolveActionTask.getUFullName(), e);
            //throw e;
        }
    }
    
    private void sendNotifications() throws Exception
    {
        List<SubmitNotification> submitNotifications = new ArrayList<SubmitNotification>();
        
        submitNotifications.add(NotificationHelper.getActionTaskNotification(resolveActionTask, UserGlobalNotificationContainerType.ACTIONTASK_PURGED, userName, false, true));
        
        //send the ESB message
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(ConstantValues.SOCIAL_NOTIFICATION_LIST, submitNotifications);
        
        SocialUtil.submitESBForUpdateSocialForWiki(params);
    }
    
    private void deleteIndexes(String actionTaskId)
    {
        Set<String> taskIds = new TreeSet<String>();
        taskIds.add(actionTaskId);
        ActionTaskIndexAPI.purgeActionTasks(taskIds, userName);
    }
    
    private void deleteActionTaskFromFormButtonAction()
    {
    	QueryDTO query = new QueryDTO();
        List <? extends Object> data = null;
        
        query.setModelName("MetaFormAction");
        query.setWhereClause("LOWER(UActionTaskName) = '" + resolveActionTask.getUFullName().toLowerCase() + "'");

        try
        {
            data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, -1, false);
        }
        catch(Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        if (data != null && data.size() > 0)
        {
        	for (Object o : data)
        	{
        		MetaFormAction metaFormAction = (MetaFormAction)o;
        		
        		try
                {
                  HibernateProxy.setCurrentUser(userName);
        			HibernateProxy.execute(() -> {
                    
        				HibernateUtil.getDAOFactory().getMetaFormActionDAO().delete(metaFormAction);
                    
        			});
                }
                catch(Exception e)
                {
                	Log.log.warn(e.getMessage(), e);
                                   HibernateUtil.rethrowNestedTransaction(e);
                }
        	}
        }
    }
    
    private boolean foundReferences()
    {
        Set<String> actionTaskNameSet = new HashSet<String>();
        QueryDTO query = new QueryDTO();
        query.setModelName("ResolveActionTask");
        query.setSelectColumns("UFullName");
        try
        {
            for (String actionTaskId : actionTaskIds)
            {
                query.setWhereClause("sys_id = '" + actionTaskId + "'");
                
                List<? extends Object> actionTaskNameList = GeneralHibernateUtil.executeHQLSelectModel(query, 0, 1, false);
                if (actionTaskNameList != null)
                {
                    actionTaskNameSet.add((String)actionTaskNameList.get(0));
                }
            }
        }
        catch(Exception e)
        {
            
        }
        
        if (actionTaskNameSet.size() > 0)
        {
            for (String taskFullname : actionTaskNameSet)
            {
                try
                {
                    referredWikiNameSet.addAll(ActionTaskUtil.getWikiNamesReferringActionTask(taskFullname));
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
        
        return referredWikiNameSet.size() > 0;
    }
}
