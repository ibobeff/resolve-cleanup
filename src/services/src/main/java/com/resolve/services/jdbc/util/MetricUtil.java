/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.jdbc.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.resolve.services.hibernate.vo.MetricThresholdVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MetricUtil
{
    public static List<MetricThresholdVO> getMetricThreshold(QueryDTO query)
    {
        List<MetricThresholdVO> result = new ArrayList<MetricThresholdVO>();

        String sql = query.getSelectSQLNonParameterizedSort();
        int start = query.getStart();
        int limit = query.getLimit();
        
        try
        {
            List<Map<String, Object>> data =  GenericJDBCHandler.executeSQLSelect(sql, start, limit, false);
            if(data != null)
            {
                for(Map<String, Object> record : data)
                {
                    MetricThresholdVO instance = new MetricThresholdVO();

                    //populate
                    instance.setId((String) record.get("sys_id"));
                    instance.setSys_id((String) record.get("sys_id"));
                    instance.setSysCreatedBy((String) record.get("sys_created_by"));
                    instance.setSysCreatedOn((Date) record.get("sys_created_on"));
                    
                    // sys_mod_count is different in Oracle and in MySql. As we are currently not using this column, commenting it out.
                    // This data is massaged at GenericJDBCHandler.getMassagedData.
//                    instance.setSysModCount(((Double) record.get("sys_mod_count")).intValue()); 
                    
                    instance.setSysUpdatedBy((String) record.get("sys_updated_by"));
                    instance.setSysUpdatedOn((Date) record.get("sys_updated_on"));
                    instance.setUGroup((String) record.get("u_group"));
                    instance.setUGuid((String) record.get("u_guid"));
                    instance.setUHigh((String) record.get("u_high"));
                    instance.setULow((String) record.get("u_low"));
                    instance.setUName((String) record.get("u_name"));
                    instance.setUAlertStatus((String) record.get("u_status"));
                    instance.setUType((String) record.get("u_type"));

                    instance.setUComponenttype((String) record.get("u_componenttype"));
                    instance.setUIpaddress((String) record.get("u_ipaddress"));
                    
                    instance.setUActive((String) record.get("u_active"));
                    instance.setUAction((String) record.get("u_action"));
                    instance.setUSource((String) record.get("u_source"));
                    instance.setURuleModule((String) record.get("u_rulemodule"));
                    instance.setURuleName((String) record.get("u_rulename"));
                    //instance.setUVersion((Integer) record.get("u_version"));
                    Object objNum = record.get("u_version");
                    if (objNum==null)
                    {
                        instance.setUVersion(0);
                    }
                    else if (objNum instanceof Double)
                    {
                        instance.setUVersion(((Double) objNum).intValue()); 
                    }
                    else if (objNum instanceof Integer)
                    {
                        instance.setUVersion((Integer) record.get("u_version"));
                    }
                    else
                    {
                        Log.log.error("u_version value type is " + objNum.getClass() + ", value = " + objNum);
                    }
                    
                    instance.setUConditionOperator((String) record.get("u_conditionoperator"));
                    instance.setUConditionValue((String) record.get("u_conditionvalue"));

                    result.add(instance);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in fetching data for :" + sql, e);
            throw new RuntimeException(e.getMessage());
        }
        
        return result;
        
    } // getMetricThreshold
  
} //MetricUtil
