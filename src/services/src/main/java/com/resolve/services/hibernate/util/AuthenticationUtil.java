/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.persistence.model.Groups;
import com.resolve.persistence.model.UserGroupRel;
import com.resolve.persistence.model.Users;
import com.resolve.persistence.util.CacheUtil;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ConfigActiveDirectoryVO;
import com.resolve.services.hibernate.vo.ConfigLDAPVO;
import com.resolve.services.hibernate.vo.ConfigRADIUSVO;
import com.resolve.services.hibernate.vo.OrganizationVO;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class AuthenticationUtil
{
    private static final String CONFIG_LDAP_CACHE_KEY = "CONFIG_LDAP";
    private static final String CONFIG_ACTIVE_DIRECTORY_CACHE_KEY = "CONFIG_ACTIVE_DIRECTORY";
    private static final String CONFIG_RADIUS_CACHE_KEY = "CONFIG_RADIUS";
//    private static final String DEFAULT_CONFIG_ACTIVE_DIRECTORY = "DEFAULT_CONFIG_ACTIVE_DIRECTORY";
//    private static final String DEFAULT_CONFIG_LDAP = "DEFAULT_CONFIG_LDAP";

    
    public static boolean updateAndCheckLogin(String username, String sourceAddr)
    {
        boolean result = false;

        try
        {
          HibernateProxy.setCurrentUser(username);
        	result = (boolean) HibernateProxy.execute(() -> {
        		boolean ret = false;
	            Users user = new Users();
	            user.setUUserName(username);
	            user = HibernateUtil.getDAOFactory().getUsersDAO().findFirst(user, "refreshRate");
	            if (user != null)
	            {
	                // last login
	                user.setULastLogin(new Date());
	
	                // last login device
	                user.setULastLoginDevice(sourceAddr);
	
	                // clear failed attempts
	                user.setUFailedAttempts(0);
	
	                // update sys_mod_count
	                int count = user.getSysModCount();
	                user.setSysModCount(++count);
	
	                // check if need new password
	                ret = user.ugetUPasswordNeedsReset();
	
	                HibernateUtil.getDAOFactory().getUsersDAO().update(user);
	            }
	            return ret;

            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);            
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return result;
    } // updateAndCheckLogin
    
    public static void updateLoginFailure(String username)
    {
        if (!StringUtils.isEmpty(username))
        {
            // logging
            Log.auth.info("login failed - username: " + username);

            try
            {
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {

	                long noOFAttemptsAllowedBeforeLock = PropertiesUtil.getPropertyLong("login.fail.attempts.allowed");
	
	                Users user = new Users();
	                user.setUUserName(username);
	                user = HibernateUtil.getDAOFactory().getUsersDAO().findFirst(user);
	                if (user != null)
	                {
	                    // update last login
	                    user.setUFailedAttempts(user.getUFailedAttempts() + 1);
	                    Log.auth.info("  attempts: " + user.getUFailedAttempts());
	
	                    long failedAttempts = user.getUFailedAttempts();
	                    // lock the user if the count is more then its allowed
	                    if (failedAttempts >= noOFAttemptsAllowedBeforeLock)
	                    {
	                        user.setULockedOut(true);
	                    }
	
	                    HibernateUtil.getDAOFactory().getUsersDAO().update(user);
	                }

                });
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
    } // updateLoginFailure
    
    public static void assignUserGroups(String username, List<String> memberOf, String authType)
    {
        if(StringUtils.isNotBlank(username) && !memberOf.isEmpty())
        {
            Log.auth.trace("assignUserGroups [" + StringUtils.listToString(memberOf) + "] for user :" + 
                           username + " and Authorization Type : " + authType);
            
            //get the list of all the groups that are marked external and their sysIds
            // HP TO DO 
            // Assumpiton that only single external authorization type system exists.
            // May not be valid. External Groups should also have authType it is set for.
            // Can single external group be set for multiple Auth Types? (More complicated)
            
            Map<String, String> externalGroups = getAllExternalGroups();
            
            //parse the group names from the memberof strings coming in from the Autherization System
            Set<String> authTypeGroups = getGroupsFromMembersOf(memberOf, authType);
            
            // for each authType group, check if exists in Resolve as external group
            Set<String> newGroup = new HashSet<String>();
            for (String authTypeGroupName : authTypeGroups)
            {
                if (externalGroups.containsKey(authTypeGroupName))
                {
                    Log.auth.trace("Found specified " + authType + " group " + authTypeGroupName + 
                                   " defined as external group in Resolve.");
                    
                    // if exist, add to new groups
                    newGroup.add(authTypeGroupName);
                }
                else
                {
                    Log.auth.warn("Specified group " + authTypeGroupName + " for Authorization Type " +
                                  authType + " is not found in Resolve as external groups and hence is being ignored.");
                }
            }
            
            //get external groups that the user currently is assigned to 
            Map<String, String> externalUserGroups = getExternalGroupsForUser(username);
            
            // for each "external" existing group
            for (String groupName : externalUserGroups.keySet())
            {
                Log.auth.trace("External Group :" + groupName);

                // if "external" group exists in new group, remove group from new group
                if (newGroup.contains(groupName))
                {
                    newGroup.remove(groupName);
                }

                // if "external" group not exist in new group, remove group from db
                else
                {
                    String groupRelId = externalUserGroups.get(groupName);
                    if (groupRelId != null)
                    {
                        deleteGroupRelWithSysId(groupRelId);
                    }
                }
            }//end of for loop
            
            if (!newGroup.isEmpty())
            {
                //add the new groups coming in from external Authorization system of authType
                Log.auth.trace("adding new " + authType + " groups for " + username + " :" + StringUtils.convertCollectionToString(newGroup.iterator(), ",") + ".");
                addNewGroupsToUser(username, newGroup, externalGroups);
            }
        }
    }
    
    public static void validateIfGroupRequiredForUser(String username, List<String> memberOf, String authType) throws Exception
    {
        //check if there are any groups assigned to this user by the authorization system
        if(memberOf == null || memberOf.size() == 0)
        {
            throw new Exception("User " + username + " has no groups assigned in the " +  authType + " authentication and authorization system.");
        }

        //parse the group names from the member of strings coming in from the Authorizing system
        Set<String> authTypeGroups = getGroupsFromMembersOf(memberOf, authType);
        
        // get the list of all the groups that are marked external and their sysIds
        // Assumption that there is single external authorization system
        Map<String, String> externalGroups = getAllExternalGroups();
        Set<String> resolveExternalGroups = externalGroups.keySet();
        if(resolveExternalGroups.size() == 0)
        {
            throw new Exception("No External groups exists in Resolve. User " + username + " is member of " + 
                                StringUtils.convertCollectionToString(authTypeGroups.iterator(), ",") + " " + authType + 
                                " authentication and authorization system groups.");
        }

        //make sure that atleast 1 auttType group exist in Resolve that this user belongs to
        boolean valid = false;
        for(String authTypeGroup : authTypeGroups)
        {
            if(resolveExternalGroups.contains(authTypeGroup))
            {
                valid = true;
                break;
            }
        }

        if(!valid)
        {
            throw new Exception("Resolve does not have single matching external " + authType + " authentication and authorization system group user " + username + " is member of. User " +
                                username + " is member of " + StringUtils.convertCollectionToString(authTypeGroups.iterator(), ",") + " groups.");
        }
        
    }
    
    public static ConfigActiveDirectoryVO getActiveDirectoryFor(String domain)
    {
        ConfigActiveDirectoryVO result = null;

        if (StringUtils.isNotBlank(domain))
        {
            domain = domain.trim();
            result = getActiveDirectoryFromCache(domain);
            if (result == null)
            {
                Log.log.debug("CACHE IS NOT WORKING CURRENTLY AND SYSTEM NEEDS A RESTART. SO GETTING AD INFO FROM DB.");
                
                //get the data from the table and add it to the cache
                ConfigActiveDirectoryVO configActiveDirectoryVO = ConfigActiveDirectoryUtil.findConfigActiveDirectoryByDomainName(domain);
                if (configActiveDirectoryVO != null)
                {
                    result = addActiveDirectoryToCache(configActiveDirectoryVO);
                }
            }
        }
//        else
//        {
//            //get the default if that does not exist
//            result = getDefaultActiveDirectory();
//            if(result == null)
//            {
////                result = 
//            }
//                
//        }

        return result;
    }
    
//    public static ConfigActiveDirectoryVO getDefaultActiveDirectory()
//    {
//        return getActiveDirectoryFromCache(DEFAULT_CONFIG_ACTIVE_DIRECTORY);
//    }
    
//    public static ConfigActiveDirectoryVO setDefaultActiveDirectory(ConfigActiveDirectoryVO configActiveDirectoryVO)
//    {
//        ConfigActiveDirectoryVO result = null;
//        
//        if (configActiveDirectoryVO != null)
//        {
//            //convert the VO to ActiveDirectory
//            try
//            {
//                result = configActiveDirectoryVO;
//
//                //add it to cache
//                setActiveDirectoryToCache(DEFAULT_CONFIG_ACTIVE_DIRECTORY, result);
//                setActiveDirectoryToCache(configActiveDirectoryVO.getUDomain(), result);
//            }
//            catch (Exception e)
//            {
//                Log.log.error("Error creating the Active Directory data", e);
//            }
//        }
//        
//        return result;
//    }
    
    public static boolean isActiveDirectoryDomainExist(String domain)
    {
        boolean exist = false;
        
        if(StringUtils.isNotBlank(domain))
        {
            ConfigActiveDirectoryVO ad = getActiveDirectoryFor(domain);
            if(ad != null)
            {
                exist = true;
            }
        }
        
        return exist;
    }
    
    public static ConfigLDAPVO getLDAPFor(String domain)
    {
        ConfigLDAPVO result = null;

        if (StringUtils.isNotBlank(domain))
        {
            domain = domain.trim();
            result = getLDAPFromCache(domain);
            if (result == null)
            {
                Log.log.debug("CACHE IS NOT WORKING CURRENTLY AND SYSTEM NEEDS A RESTART. SO GETTING LDAP INFO FROM DB.");
                
                //get the data from the table and add it to the cache
                ConfigLDAPVO configLDAPVO = ConfigLDAPUtil.findConfigLDAPByDomainName(domain);
                if (configLDAPVO != null)
                {
                    //convert the VO to ActiveDirectory
                    try
                    {
                        result = addLDAPToCache(configLDAPVO);

                    }
                    catch (Exception e)
                    {
                        Log.log.error("Error createing the Active Directory data", e);
                    }
                }
            }
        }
        return result;
    }
    
    
    private static ConfigLDAPVO updateLDAPCache(ConfigLDAPVO configLDAPVO, String username)
    {
        ConfigLDAPVO result = null;
        
        if (configLDAPVO != null)
        {
            //convert the VO to ActiveDirectory
            try
            {
                result = configLDAPVO;

                //add it to cache
                setLDAPToCache(configLDAPVO.getUDomain(), result);
            }
            catch (Exception e)
            {
                Log.log.error("Error createing the Active Directory data", e);
            }
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static void invalidateLDAPCache(ConfigLDAPVO configLDAPVO, String username)
    {
        if (configLDAPVO != null)
        {
            try
            {
                String domain = configLDAPVO.getUDomain();
                Log.log.debug("LDAP Domain " + domain + " invalidated in Cache");
                
                //get the map
                Map<String, ConfigLDAPVO> map = null;
                if(CacheUtil.isKeyInCache(CONFIG_LDAP_CACHE_KEY))
                {
                    map = (Map<String, ConfigLDAPVO>)CacheUtil.getCacheObj(CONFIG_LDAP_CACHE_KEY);
                }
                else
                {
                    map = new HashMap<String, ConfigLDAPVO>();
                }
                
                //remove the domain object
                if(map.containsKey(domain))
                {
                    map.remove(domain);
                    
                    //add it back to cache
                    CacheUtil.putCacheObj(CONFIG_LDAP_CACHE_KEY, map);
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error invalidating the LDAP data", e);
            }
        }
    }
    
    private static ConfigActiveDirectoryVO updateAD(ConfigActiveDirectoryVO configActiveDirectoryVO, String username)
    {
        ConfigActiveDirectoryVO result = null;
        
        if (configActiveDirectoryVO != null)
        {
            //convert the VO to ActiveDirectory
            try
            {
                result = configActiveDirectoryVO;

                //add it to cache
                setActiveDirectoryToCache(configActiveDirectoryVO.getUDomain(), result);
            }
            catch (Exception e)
            {
                Log.log.error("Error creating the Active Directory data", e);
            }
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static void invalidateADCache(ConfigActiveDirectoryVO configActiveDirectoryVO, String username)
    {
        if (configActiveDirectoryVO != null)
        {
            //convert the VO to ActiveDirectory
            try
            {
                String domain = configActiveDirectoryVO.getUDomain();
                Log.log.debug("AD Domain " + domain + " updated in Cache");
                
                //get the map
                Map<String, ConfigActiveDirectoryVO> map = null;
                if(CacheUtil.isKeyInCache(CONFIG_ACTIVE_DIRECTORY_CACHE_KEY))
                {
                    map = (Map<String, ConfigActiveDirectoryVO>)CacheUtil.getCacheObj(CONFIG_ACTIVE_DIRECTORY_CACHE_KEY);
                }
                else
                {
                    map = new HashMap<String, ConfigActiveDirectoryVO>();
                }
                
                //remove the domain object
                if(map.containsKey(domain))
                {
                    map.remove(domain);
                    
                    //add it back to cache
                    CacheUtil.putCacheObj(CONFIG_ACTIVE_DIRECTORY_CACHE_KEY, map);
                }
                
            }
            catch (Exception e)
            {
                Log.log.error("Error invalidating the Active Directory data", e);
            }
        }
    }
    
    public static boolean isLDAPDomainExist(String domain)
    {
        boolean exist = false;
        
        if(StringUtils.isNotBlank(domain))
        {
            ConfigLDAPVO ldap = getLDAPFor(domain);
            if(ldap != null)
            {
                exist = true;
            }
        }
        
        return exist;
    }
    
    public static boolean isOrgEnabled(String org_id)
    {
        boolean result = true;
        
        if (StringUtils.isNotEmpty(org_id))
        {
            OrganizationVO vo = OrganizationUtil.findOrganizationById(org_id);
            if (vo != null)
            {
                result = !vo.ugetUDisable();
            }
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    private static ConfigLDAPVO getLDAPFromCache(String domain)
    {
        ConfigLDAPVO result = null;
        
        if(StringUtils.isNotBlank(domain))
        {
            Map<String, ConfigLDAPVO> map = null;
            if(CacheUtil.isKeyInCache(CONFIG_LDAP_CACHE_KEY))
            {
                map = (Map<String, ConfigLDAPVO>)CacheUtil.getCacheObj(CONFIG_LDAP_CACHE_KEY);
                if (map != null && map.containsKey(domain))
                {
                    result = map.get(domain);
                }
            }
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    private static ConfigActiveDirectoryVO getActiveDirectoryFromCache(String domain)
    {
        ConfigActiveDirectoryVO result = null;
        
        if(StringUtils.isNotBlank(domain))
        {
            Map<String, ConfigActiveDirectoryVO> map = null;
            if(CacheUtil.isKeyInCache(CONFIG_ACTIVE_DIRECTORY_CACHE_KEY))
            {
                map = (Map<String, ConfigActiveDirectoryVO>)CacheUtil.getCacheObj(CONFIG_ACTIVE_DIRECTORY_CACHE_KEY);
                if(map != null && map.containsKey(domain))
                {
                    result = map.get(domain);
                }
            }
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    private static void setLDAPToCache(String domain, ConfigLDAPVO ldap)
    {
        if(StringUtils.isNotBlank(domain) && ldap != null)
        {
            Log.log.debug("LDAP Domain " + domain + " updated in Cache");
            Map<String, ConfigLDAPVO> map = null;
            if(CacheUtil.isKeyInCache(CONFIG_LDAP_CACHE_KEY))
            {
                map = (Map<String, ConfigLDAPVO>)CacheUtil.getCacheObj(CONFIG_LDAP_CACHE_KEY);
            }
            else
            {
                map = new HashMap<String, ConfigLDAPVO>();
            }
            
            map.put(domain, ldap);
            
            //add it back to cache
            CacheUtil.putCacheObj(CONFIG_LDAP_CACHE_KEY, map);
            
        }
    }
    
    @SuppressWarnings("unchecked")
    private static void setActiveDirectoryToCache(String domain, ConfigActiveDirectoryVO activeDirectory)
    {
        if(StringUtils.isNotBlank(domain) && activeDirectory != null)
        {
            Log.log.debug("AD Domain " + domain + " updated in Cache");
            Map<String, ConfigActiveDirectoryVO> map = null;
            if(CacheUtil.isKeyInCache(CONFIG_ACTIVE_DIRECTORY_CACHE_KEY))
            {
                map = (Map<String, ConfigActiveDirectoryVO>)CacheUtil.getCacheObj(CONFIG_ACTIVE_DIRECTORY_CACHE_KEY);
            }
            else
            {
                map = new HashMap<String, ConfigActiveDirectoryVO>();
            }
            
            map.put(domain, activeDirectory);
            
            //add it back to cache
            CacheUtil.putCacheObj(CONFIG_ACTIVE_DIRECTORY_CACHE_KEY, map);
            
        }
    }
    
    private static ConfigActiveDirectoryVO addActiveDirectoryToCache(ConfigActiveDirectoryVO configActiveDirectoryVO)
    {
        ConfigActiveDirectoryVO result = null;
        
        if (configActiveDirectoryVO != null)
        {
            //convert the VO to ActiveDirectory
            try
            {
                result = configActiveDirectoryVO;

                //add it to cache
                setActiveDirectoryToCache(configActiveDirectoryVO.getUDomain(), result);
            }
            catch (Exception e)
            {
                Log.log.error("Error createing the Active Directory data", e);
            }
        }
        
        return result;
    }
    
    
    private static ConfigLDAPVO addLDAPToCache(ConfigLDAPVO ConfigLDAPVO)
    {
        ConfigLDAPVO result = null;
        
        if (ConfigLDAPVO != null)
        {
            //convert the VO to ActiveDirectory
            try
            {
                result = ConfigLDAPVO;

                //add it to cache
                setLDAPToCache(ConfigLDAPVO.getUDomain(), result);
            }
            catch (Exception e)
            {
                Log.log.error("Error createing the Active Directory data", e);
            }
        }
        
        return result;
    }
    
    private static Map<String, String> getAllExternalGroups()
    {
        Map<String, String> result = new HashMap<String, String>();
        
        try
        {
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {
            	List<Groups> allGroups = HibernateUtil.getDAOFactory().getGroupsDAO().findAll();
                if (allGroups != null)
                {
                    for (Groups group : allGroups)
                    {
                        if (group.ugetUHasExternalLink())
                        {
                            result.put(group.getUName().trim(), group.getSys_id().trim());
                        }
                    }
                }
                
            });
        }
        catch (Exception e)
        {
            Log.log.warn("Error in getting the external groups", e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return result;
        
    }
    
    private static Set<String> getGroupsFromMembersOf(List<String> memberOf, String authType)
    {
        Set<String> result = new HashSet<String>();
        
        if(memberOf != null && memberOf.size() > 0)
        {
            for (String groupName : memberOf)
            {
                String newGroupName = "";
                if (groupName.toUpperCase().startsWith("CN="))
                {
                    int start = 3;

                    int end = groupName.indexOf(',');
                    if (end < 0)
                    {
                        end = groupName.length();
                    }
                    newGroupName = groupName.substring(start, end);
                    result.add(newGroupName.trim());
                    Log.auth.trace("Adding " + newGroupName + " to " + authType + " Groups.");
                }
                else
                {
                    result.add(groupName.trim());
                    Log.auth.trace("Adding " + groupName.trim() + " to " + authType + " Groups.");
                }
            }
        }

        return result;
    }
    
    private static Map<String, String> getExternalGroupsForUser(String username)
    {
        Map<String, String> result = new HashMap<String, String>();
        
        if(StringUtils.isNotBlank(username))
        {
            try
            {
              HibernateProxy.setCurrentUser("system");
                HibernateProxy.execute(() -> {
                	  Users user = UserUtils.findUser(null, username);
                      if(user != null)
                      {
                          for (Iterator<UserGroupRel> i = user.getUserGroupRels().iterator(); i.hasNext();)
                          {
                              UserGroupRel rel = i.next();
                              Groups group = rel.getGroup();

                              if (group.ugetUHasExternalLink())
                              {
                                  Log.auth.trace("Adding to External User Group :" + group.getUName());
                                  result.put(group.getUName(), rel.getSys_id());
                              }
                          }
                      }
                });
            }
            catch (Exception e)
            {
                Log.log.warn("Error in getting the external groups for user:" + username, e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
            
        }
        
        return result;
    }
    
    
    private static void deleteGroupRelWithSysId(String groupRelId)
    {
        if(StringUtils.isNotBlank(groupRelId))
        {
            try
            {
              HibernateProxy.setCurrentUser("system");
            	HibernateProxy.execute(() -> {
                
	                UserGroupRel rel = HibernateUtil.getDAOFactory().getUserGroupRelDAO().findById(groupRelId);
	                if(rel != null)
	                {
	                    Groups group = rel.getGroup();
	                    if(group != null)
	                    {
	                        Log.auth.trace("Deleteing Group-User Rel for group :" + group.getUName());
	                    }
	                    
	                    HibernateUtil.getDAOFactory().getUserGroupRelDAO().delete(rel);
	                }
                
            	});
            }
            catch (Exception e)
            {
                Log.log.warn("Error in deleting the groupRel:" + groupRelId, e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
    }
    
    /**
     * using the NOCACHE begintransaction as the collection reference has the @Cache annotation which we want to disable when 
     * we are adding any groups 
     * 
     * @param username
     * @param newGroup
     * @param externalGroups
     */
    private static void addNewGroupsToUser(String username, Set<String> newGroup, Map<String, String> externalGroups)
    {
        if(StringUtils.isNotBlank(username) && newGroup != null && newGroup.size() > 0)
        {
            
            try
            {
              HibernateProxy.setCurrentUser("system");
                HibernateProxy.executeNoCache(() -> {
                	Users user = UserUtils.findUser(null, username);
                    if (user != null)
                    {
                        for (String groupName : newGroup)
                        {
                            String groupId = externalGroups.get(groupName);
                            Log.auth.trace("------> groupId:" + groupId);
                            if (StringUtils.isNotBlank(groupId))
                            {
                                Groups group = HibernateUtil.getDAOFactory().getGroupsDAO().findById(groupId);
                                Log.auth.trace("-----> group --> " + group);
                                if (group != null)
                                {
                                    Log.auth.trace("---> adding user to group");

                                    UserGroupRel rel = new UserGroupRel();
                                    rel.setGroup(group);
                                    rel.setUser(user);
                                    
                                    HibernateUtil.getDAOFactory().getUserGroupRelDAO().insert(rel);
                                }
                            }
                        }
                    }
                    else
                    {
                        Log.auth.trace("Cannot find the user " + username + " to add the following groups:" + StringUtils.convertCollectionToString(newGroup.iterator(), ","));
                    }

                });
                
            }
            catch (Exception e)
            {
                Log.log.warn("Error while adding new groups:" + StringUtils.convertCollectionToString(newGroup.iterator(), ","), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    public static void invalidateRADIUSCache(ConfigRADIUSVO configRADIUSVO, String username)
    {
        if (configRADIUSVO != null)
        {
            try
            {
                String domain = configRADIUSVO.getUDomain();
                Log.log.debug("Invalidating RADIUS domain " + domain + " in cache");
                
                //get the map
                Map<String, ConfigRADIUSVO> map = null;
                if(CacheUtil.isKeyInCache(CONFIG_RADIUS_CACHE_KEY))
                {
                    map = (Map<String, ConfigRADIUSVO>)CacheUtil.getCacheObj(CONFIG_RADIUS_CACHE_KEY);
                }
                else
                {
                    map = new HashMap<String, ConfigRADIUSVO>();
                }
                
                //remove the domain object
                if(map != null && map.containsKey(domain))
                {
                    map.remove(domain);
                    
                    //add it back to cache
                    CacheUtil.putCacheObj(CONFIG_RADIUS_CACHE_KEY, map);
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error invalidating the RADIUS data", e);
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    private static void setRADIUSToCache(String domain, ConfigRADIUSVO radius)
    {
        if(StringUtils.isNotBlank(domain) && radius != null)
        {
            Log.log.debug("RADIUS Domain " + domain + " updated in Cache");
            Map<String, ConfigRADIUSVO> map = null;
            if(CacheUtil.isKeyInCache(CONFIG_RADIUS_CACHE_KEY))
            {
                map = (Map<String, ConfigRADIUSVO>)CacheUtil.getCacheObj(CONFIG_RADIUS_CACHE_KEY);
            }
            else
            {
                map = new HashMap<String, ConfigRADIUSVO>();
            }
            
            map.put(domain, radius);
            
            //add it back to cache
            CacheUtil.putCacheObj(CONFIG_RADIUS_CACHE_KEY, map);
            
        }
    }
    
    private static ConfigRADIUSVO addRADIUSToCache(ConfigRADIUSVO ConfigRADIUSVO)
    {
        ConfigRADIUSVO result = null;
        
        if (ConfigRADIUSVO != null)
        {
            try
            {
                result = ConfigRADIUSVO;

                //add it to cache
                setRADIUSToCache(ConfigRADIUSVO.getUDomain(), result);
            }
            catch (Exception e)
            {
                Log.log.error("Error " + e.getMessage() + 
                              " adding RADIUS config data for domain " + ConfigRADIUSVO.getUDomain() + " to cache", e);
            }
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    private static ConfigRADIUSVO getRADIUSFromCache(String domain)
    {
        ConfigRADIUSVO result = null;
        
        if(StringUtils.isNotBlank(domain))
        {
            Map<String, ConfigRADIUSVO> map = null;
            if(CacheUtil.isKeyInCache(CONFIG_RADIUS_CACHE_KEY))
            {
                map = (Map<String, ConfigRADIUSVO>)CacheUtil.getCacheObj(CONFIG_RADIUS_CACHE_KEY);
                if (map != null && map.containsKey(domain))
                {
                    result = map.get(domain);
                }
            }
        }
        
        return result;
    }
    
    public static ConfigRADIUSVO getRADIUSFor(String domain)
    {
        ConfigRADIUSVO result = null;

        if (StringUtils.isNotBlank(domain))
        {
            domain = domain.trim();
            //Not checking into the cache because when the Radius configuration is updated/deleted its not going to update the cache
           // result = getRADIUSFromCache(domain);
            if (result == null)
            {
                Log.log.debug("CACHE IS NOT WORKING CURRENTLY AND SYSTEM NEEDS A RESTART. SO GETTING RADIUS INFO FROM DB.");
                
                //get the data from the table and add it to the cache
                ConfigRADIUSVO configRADIUSVO = ConfigRADIUSUtil.findConfigRADIUSByDomainName(domain);
                if (configRADIUSVO != null)
                {
                    //add RADIUS to cache
                    try
                    {
                        result = addRADIUSToCache(configRADIUSVO);

                    }
                    catch (Exception e)
                    {
                        Log.log.error("Error " + e.getMessage() + " getting RADIUS config data for " + domain, e);
                    }
                }
            }
        }
//        else
//        {
//            //get the default if that does not exist
//            result = getDefaultLDAP();
//        }
        
        return result;
    }
    
    public static boolean isRADIUSDomainExist(String domain)
    {
        boolean exist = false;
        
        if(StringUtils.isNotBlank(domain))
        {
            ConfigRADIUSVO radius = getRADIUSFor(domain);
            if(radius != null)
            {
                exist = true;
            }
        }
        
        return exist;
    }
}
