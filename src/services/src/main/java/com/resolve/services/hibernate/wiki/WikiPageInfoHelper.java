/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikidocStatistics;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceWiki;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.vo.WikiPageInfoDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class WikiPageInfoHelper
{
    private WikiDocumentVO doc = null;
    private WikidocStatistics stats = null;
    
    private List<String> listOutgoingDocs = null;
    private List<String> listIncomingDocs = null;
    private List<String> listOfActionTaskReferencesInContent = null;
    private List<String> listOfRunbookReferencesInMain = null;
    private List<String> listOfRunbookReferencesInAbort = null;
    private List<String> listOfRunbookReferencesInDT = null;
    private List<String> listOfActionTaskReferencesInMain = null;
    private List<String> listOfActionTaskReferencesInException = null;

    private WikiPageInfoDTO info = new WikiPageInfoDTO();
    
    public WikiPageInfoHelper(String docSysId, String docFullName) throws Exception
    {
        this.doc = WikiUtils.getWikiDocWithGraphRelatedData(docSysId, docFullName, "system");
        if(doc == null)
        {
            throw new Exception("Document with sysId:" + docSysId + ", name :" + docFullName + " does not exist");
        } 
        
    }
    
    public WikiPageInfoDTO getInfo()  throws Exception
    {
        //populate the data that we need 
        populateModelsForDocument();
        
        //fill the dto with data
        info.setSys_id(doc.getSys_id());
        info.setId(doc.getSys_id());
        info.setSys_created_by(doc.getSysCreatedBy());
        info.setSys_created_on(doc.getSysCreatedOn());
        info.setSys_updated_by(doc.getSysUpdatedBy());
        info.setSys_updated_on(doc.getSysUpdatedOn());
        
        info.setFullName(doc.getUFullname());
        info.setSummary(doc.getUSummary());
        info.setVersion(doc.getUVersion());
        info.setExpireOn(doc.getUExpireOn());
        info.setRequestSubmissionOn(doc.getUReqestSubmissionOn());
        info.setLastReviewedOn(doc.getULastReviewedOn());
        info.setLastReviewedBy(doc.getULastReviewedBy());

        info.setViewed(stats.getUViewCount());
        info.setEdited(stats.getUEditCount());
        info.setExecuted(stats.getUExecuteCount());
        
        //rating
        info.setRating(ServiceWiki.getAverageResolutionRating(doc.getSys_id(), null, "system"));
        
        //tags
        String tags = doc.getUTag();
        if(StringUtils.isNotEmpty(tags))
        {
            info.getTags().addAll(StringUtils.convertStringToList(tags, ","));
        }
        
        //catalog items
        String catalogItems = doc.getUCatalog();
        if(StringUtils.isNotEmpty(catalogItems))
        {
            info.getCatalogItems().addAll(StringUtils.convertStringToList(catalogItems, ","));
        }
        
        info.getOutgoingRefLinks().addAll(listOutgoingDocs);
        info.getIncomingRefLinks().addAll(listIncomingDocs);
        
        info.getAtRefInContent().addAll(listOfActionTaskReferencesInContent);
        
        info.getAtRefInMainModel().addAll(listOfActionTaskReferencesInMain);
        info.getWikiMainModel().addAll(listOfRunbookReferencesInMain);
        
        info.getAtRefInAbortModel().addAll(listOfActionTaskReferencesInException);
        info.getWikiAbortModel().addAll(listOfRunbookReferencesInAbort);
        
        info.getWikiDecisionTreeModel().addAll(listOfRunbookReferencesInDT);
        
        return info;
    }
    
    @SuppressWarnings("unchecked")
    private void populateModelsForDocument() throws Exception
    {
        populateStats();
        
        //ContentWikidocWikidocRel - outgoingDocs
        //String hql = "select rel.UWikidocFullname from ContentWikidocWikidocRel rel, WikiDocument doc where rel.contentWikidoc = doc.sys_id and lower(doc.UFullname) = '" + doc.getUFullname().toLowerCase() + "'";
        String hql = "select rel.UWikidocFullname from ContentWikidocWikidocRel rel, WikiDocument doc where rel.contentWikidoc.sys_id = doc.sys_id and lower(doc.UFullname) = :UFullname";
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        
        queryParams.put("UFullname", doc.getUFullname().toLowerCase());
        
        this.listOutgoingDocs = (List<String>) GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
        
        //listIncomingDocs
        //hql = "select doc.UFullname from ContentWikidocWikidocRel rel, WikiDocument doc where rel.contentWikidoc = doc.sys_id and lower(rel.UWikidocFullname) = '" + doc.getUFullname().toLowerCase() + "'";
        hql = "select doc.UFullname from ContentWikidocWikidocRel rel, WikiDocument doc where rel.contentWikidoc.sys_id = doc.sys_id and lower(rel.UWikidocFullname) = :UFullname";
        
        this.listIncomingDocs = (List<String>) GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
     
        queryParams.clear();
        
        //ContentWikidocActiontaskRel - getListOfActionTaskReferecesInContent
        //hql = "select UActiontaskFullname from ContentWikidocActiontaskRel where contentWikidoc = '" + doc.getSys_id() + "'";
        hql = "select UActiontaskFullname from ContentWikidocActiontaskRel where contentWikidoc.sys_id = :sys_id";
        
        queryParams.put("sys_id", doc.getSys_id());
        
        this.listOfActionTaskReferencesInContent = (List<String>) GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
        
        //MainWikidocWikidocRel
        //hql = "select UWikidocFullname from MainWikidocWikidocRel where mainWikidoc = '" + doc.getSys_id() + "'";
        hql = "select UWikidocFullname from MainWikidocWikidocRel where mainWikidoc.sys_id = :sys_id";
        this.listOfRunbookReferencesInMain = (List<String>) GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
        
        //ExceptionWikidocWikidocRel
        //hql = "select UWikidocFullname from ExceptionWikidocWikidocRel where exceptionWikidoc = '" + doc.getSys_id() + "'";
        hql = "select UWikidocFullname from ExceptionWikidocWikidocRel where exceptionWikidoc.sys_id = :sys_id";
        this.listOfRunbookReferencesInAbort = (List<String>) GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
        
        //DTWikidocWikidocRel
        //hql = "select UWikidocFullname from DTWikidocWikidocRel where dtWikidoc = '" + doc.getSys_id() + "'";
        hql = "select UWikidocFullname from DTWikidocWikidocRel where dtWikidoc.sys_id = :sys_id";
        this.listOfRunbookReferencesInDT = (List<String>) GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
        
        //reference of Action Task in MAIN - table - main_wikidoc_actiontask_rel
        //hql = "select UActiontaskFullname from MainWikidocActiontaskRel where mainWikidoc = '" + doc.getSys_id() + "'";
        hql = "select UActiontaskFullname from MainWikidocActiontaskRel where mainWikidoc.sys_id = :sys_id";
        this.listOfActionTaskReferencesInMain = (List<String>) GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
        
        //reference of Action Task in ABORT - table - exception_wikidoc_actiontask_rel
        //hql = "select UActiontaskFullname from ExceptionWikidocActiontaskRel where exceptionWikidoc = '" + doc.getSys_id() + "'";
        hql = "select UActiontaskFullname from ExceptionWikidocActiontaskRel where exceptionWikidoc.sys_id = :sys_id";
        this.listOfActionTaskReferencesInException = (List<String>) GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
        
    }

    private void populateStats() throws Exception
    {
        try
        {

          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
                WikiDocument localDoc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(doc.getSys_id());
                if (localDoc != null)
                {
                    //load the stats
                    Collection<WikidocStatistics> stats = localDoc.getWikidocStatistics();
                    if(stats != null && stats.size() > 0)
                    {
                        //there can be only 1 rec - its a collection for lazy loading
                        this.stats = stats.iterator().next();
                    }
                }

                
        	});

            //to avoid NPE
            if(stats == null)
            {
                stats = new WikidocStatistics();
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }   
    }
    

}
