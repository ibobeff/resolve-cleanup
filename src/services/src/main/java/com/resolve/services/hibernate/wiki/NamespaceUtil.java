/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.vo.NamespaceVO;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.QuerySort.SortOrder;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;


public class NamespaceUtil
{
    private static HashSet<String> excludeNamespace = new HashSet<String>();

    static
    {
        String excludeNamespacePattern = PropertiesUtil.getPropertyString("search.exclude.namespace");
        if(StringUtils.isNotBlank(excludeNamespacePattern))
        {
            List<String> excludeNamespaceList = StringUtils.convertStringToList(excludeNamespacePattern.toLowerCase(), ",");
            for(String namespace : excludeNamespaceList)
            {
                excludeNamespace.add(namespace.toLowerCase());
            }
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static Set<String>  findAllDocumentsForNamespace(String namespace, String username)
    {
        Set<String> result = new HashSet<String>();
        
        if(StringUtils.isNotBlank(namespace))
        {
            //String hql = "select UFullname from WikiDocument where LOWER(UNamespace) = '" + namespace.toLowerCase() + "'";
            String hql = "select UFullname from WikiDocument where LOWER(UNamespace) = :namespace";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("namespace", namespace.toLowerCase());
            
            try
            {
                List list = GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
                if (list != null && list.size() > 0)
                {
                    result.addAll(list);
                }
            }
            catch (Throwable t)
            {
                Log.log.info("Unable to get documents for namespace " + namespace, t);
            }
        }
        
        return result;
        
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static Set<String>  findAllDocumentsSysIdsForNamespace(String namespace, String username)
    {
        Set<String> result = new HashSet<String>();
        
        if(StringUtils.isNotBlank(namespace))
        {
            //String hql = "select sys_id from WikiDocument where LOWER(UNamespace) = '" + namespace.toLowerCase() + "'";
            String hql = "select sys_id from WikiDocument where LOWER(UNamespace) = :namespace";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("namespace", namespace.toLowerCase());
            
            try
            {
                List list = GeneralHibernateUtil.executeHQLSelect(hql, queryParams);
                if (list != null && list.size() > 0)
                {
                    result.addAll(list);
                }
            }
            catch (Throwable t)
            {
                Log.log.info("Unable to get documents for namespace " + namespace, t);
            }
        }
        
        return result;
        
    }
    
    public static ResponseDTO<NamespaceVO> getDocumentNamespaces(QueryDTO query, String username)
    {
        ResponseDTO<NamespaceVO> response = null;
        
        String sql = getDocNamespaceQuery(query);//"SELECT UNamespace as UNamespace, count(*) as count FROM WikiDocument";
        query.setHql(sql);

        try
        {
//            String where = query.getQuery();
            int start = query.getStart();
            int limit = query.getLimit();
//            String sort = query.getSort();

            //get the response
            response = exeNSQuery(query, start, limit, username);
        }
        catch (Throwable t)
        {
            Log.log.info("Unable to get all namespace", t);
        }

        return response;
    }

//    public static List<NamespaceVO> getActionTaskNamespaces(QueryDTO query, String username)
//    {
//        List<NamespaceVO> results = new ArrayList<NamespaceVO>();
//        String sql = "SELECT UNamespace as UNamespace, count(*) as count FROM ResolveActionTask ";
//        try
//        {
//            int start = query.getStart();
//            int limit = query.getLimit();
//            String sort = query.getSort();
//            String where = query.getQuery();
//            if(StringUtils.isNotEmpty(where))
//            {
//                sql = sql + " where UNamespace like '%" + where +"%'" ;
//            }
//            sql = sql + " group by UNamespace ORDER BY UNamespace ";
//            
//            query.setHql(sql);
//
//            //get the response
//            ResponseDTO<NamespaceVO> response = exeNSQuery(query, start,limit, username);
//            results = (List<NamespaceVO>) response.getRecords();
//        }
//        catch (Throwable t)
//        {
//            Log.log.info("Unable to get all namespace", t);
//        }
//
//
//        return results;
//    }

    public static List<NamespaceVO> getAllNamespaces(QueryDTO query, boolean exclude)
    {
        List<NamespaceVO> results = new ArrayList<NamespaceVO>();

        try
        {
            List<NamespaceVO> allNamespaces = (List<NamespaceVO>)getDocumentNamespaces(query, "admin").getRecords();

            //some consumer may only require a filtered list based on "search.exclude.namespace" system property
            if(allNamespaces != null)
            {
                if(exclude)
                {
                    if(excludeNamespace.size() == 0)
                    {
                        results.addAll(allNamespaces);
                    }
                    else
                    {
                        for(NamespaceVO namespaceVO : allNamespaces)
                        {
                            if(!excludeNamespace.contains(namespaceVO.getUNamespace().toLowerCase()))
                            {
                                results.add(namespaceVO);
                            }
                        }
                    }
                }
                else
                {
                    results.addAll(allNamespaces);
                }
            }
        }
        catch (Throwable t)
        {
            Log.log.info("Unable to get all namespace", t);
        }

        return results;
    }

    public static List<NamespaceVO> getAllNamespaces(QueryDTO query)
    {
        return getAllNamespaces(query, false);
    }
    
    @SuppressWarnings({ "rawtypes" })
    private static ResponseDTO<NamespaceVO> exeNSQuery(QueryDTO query, int start,int limit, String username)
    {
        ResponseDTO<NamespaceVO> response = new ResponseDTO<NamespaceVO>();
        
        List listData = new ArrayList();
        List<NamespaceVO> allNS = new ArrayList<NamespaceVO>();
        List<NamespaceVO> results = new ArrayList<NamespaceVO>();
        
        try
        {
            listData = GeneralHibernateUtil.executeHQLSelect(query.getHql(), new HashMap<String, Object>());
            if (listData != null && listData.size() > 0)
            {
                for(Object rec : listData)
                {
                    Object[] arr = (Object[]) rec;

                    String UNamespace = (String) arr[0];
                    long countDocs = ((Long) arr[1]).longValue();
                    
                    NamespaceVO localVO = new NamespaceVO();
                    localVO.setSys_id(UNamespace);
                    localVO.setId(UNamespace);
                    localVO.setUNamespace(UNamespace);
                    localVO.setCount(countDocs);
                    
                    allNS.add(localVO);
                }
                
                //pagination
                int totalsize = listData.size();
                if(start < 0 || limit < 0)
                {
                    results.addAll(allNS);
                }
                else
                {
                    if(totalsize > start)
                    {
                        int thisPagesize = (totalsize < (start + limit)) ? totalsize : (start + limit);
    
                        for(int i = 0; i<thisPagesize; i++)
                        {
                            if(i >= start)
                            {
                                results.add((NamespaceVO) allNS.get(i));
                            }
                        }
                    }
                }

                response.setTotal(allNS.size());
                response.setRecords(results);
                
            }
        }
        catch (Throwable t)
        {
            Log.log.info("Unable to get all namespace", t);
        }
        
        return response;
    }

    private static String getDocNamespaceQuery(QueryDTO query)
    {
        String sql = "SELECT UNamespace as UNamespace, count(*) as count FROM WikiDocument";
        List<QueryFilter> filters = query.getFilters();
        if(filters != null && filters.size() > 0)
        {
            String whereClause = "";
            for(QueryFilter filter : filters)
            {
                String field = filter.getField();
                if(field.equals("UNamespace"))
                {
                    String comp = filter.getComp();
                    String value = null;
                    if(filter.getParameters() != null && filter.getParameters().size() > 0)
                    {
                        value = (String) filter.getParameters().get(0).getValue();
                    }
                    
                    if(StringUtils.isNotBlank(value))
                    {
                        if(whereClause.trim().length() == 0)
                            whereClause = "LOWER(" + field + ") " + comp + " '" + value.toLowerCase() + "'";
                        else
                            whereClause = whereClause + " and LOWER(" + field + ") " + comp + " '" + value.toLowerCase() + "'";
                    }
                }
//                else if(field.equals("count")) //query fails...maybe later on figure out a better way to filter based on count.
//                {
//                    String comp = filter.getComp();
//                    String value = (String) filter.getParameters().get(0).getValue();
//                    
//                    if(whereClause.trim().length() == 0)
//                        whereClause = field + " " + comp + " " + value;
//                    else
//                        whereClause = whereClause + " and " + field + " " + comp + " " + value;
//                }
            }
            
            if(StringUtils.isNotEmpty(whereClause))
            {
//                sql = sql + " where UNamespace like '%" + whereClause +"%'" ;
                sql = sql + " where " + whereClause;
            }
        }
        
        sql = sql + " group by UNamespace ORDER BY Lower(UNamespace) ";
        
        //sort
        boolean desc = false;
        List<QuerySort> sorts = query.getSortItems();
        if(sorts != null && sorts.size() > 0)
        {
            for(QuerySort sort : sorts)
            {
                if("UNamespace".equalsIgnoreCase(sort.getProperty()))
                {
                    if(sort.getDirection() == SortOrder.DESC)
                    {
                        desc = true;
                        break;
                    }
                }
            }
        }
        
        if(desc)
            sql = sql + " DESC";
        
        return sql;
    }
    
    

}
