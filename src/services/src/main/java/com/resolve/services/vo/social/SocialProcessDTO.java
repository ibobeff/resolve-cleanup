/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.social;

import java.util.Map;

import com.resolve.services.interfaces.SocialDTO;

public class SocialProcessDTO extends SocialDTO
{
    private static final long serialVersionUID = -8671115612222007431L;
    public final static String TABLE_NAME = "social_process";
    
    public SocialProcessDTO() {}
    public SocialProcessDTO(Map<String, Object> data)
    {
        super(data);
    }
}
