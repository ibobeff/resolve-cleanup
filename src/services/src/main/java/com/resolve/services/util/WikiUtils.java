/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.resolve.services.ServiceJDBC;
import com.resolve.services.ServiceWiki;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.util.Constants;
import com.resolve.util.StringUtils;

/**
 * @author jeet.marwah
 *
 */
public class WikiUtils
{
    public static String INVALID_WIKI_DOC_NAME_MSG = "Invalid Wiki Document Name.";
    public static String WIKI_DOC_NAME_SPL_CHAR_MSG = "Special character's are not allowed in wiki document name except '-', '_' and '.'.";

    public static String createProblemId(String documentName)
    {
        return "_problemId_" + documentName.replaceAll(" ", "_");
    }// createProblemId

    public static boolean isWikidocExist(String fullname)
    {
        return isWikidocExist(fullname, false);
    }// isWikidocExist

    public static boolean isWikidocExist(String fullname, boolean caseSensitive)
    {
        return ServiceWiki.isWikidocExist(fullname, caseSensitive);
    }// isWikidocExist

    public static WikiDocumentVO getWikiDoc(String fullname)
    {
        return ServiceWiki.getWikiDoc(null, fullname, "system");
    }// getWikiDoc

    public static String getWikidocSysId(String fullname)
    {
        return ServiceWiki.getWikidocSysId(fullname);

    }// isWikidocExist

    public static List<String> runbooksNotFound(List<String> runbookNameList)
    {
        return ServiceWiki.runbooksNotFound(runbookNameList);

    } // runbooksNotFound

    public static List<String> runbooksEditNotAvailable(List<String> runbookNameList, String user)
    {
        return ServiceWiki.runbooksEditNotAvailable(runbookNameList, user);

    } // runbooksNotEditable

    public static void addSectionToWikiDoc(String docFullName, String sectionString)
    {
        ServiceWiki.addSectionToWikiDoc(docFullName, sectionString);

    } // addSectionToWikiDoc

    /**
     * decision to show the wiki control panel to the user or not
     *
     * @param username
     * @return
     */
    public static String evaluateShowControlPanleProperty(String username)
    {
        String value = "true";

        // superusers will always see the CP
        if (!username.equals(Constants.ADMIN) && !username.equals(Constants.RESOLVE_MAINT))
        {
            String valueDefaultCPProperty = PropertiesUtil.getPropertyString(Constants.WIKI_CONTROLPANEL_SHOW_DEFAULT);
            String valueexcludeCPProperty = PropertiesUtil.getPropertyString(Constants.WIKI_EXCLUDE_ROLES_TO_SHOW_CONTROLPANEL);
            // get the user roles
            Set<String> userRoles = null;
            try
            {
                userRoles = UserUtils.getUserRoles(username);
            }
            catch (Throwable e)
            {
                userRoles = new TreeSet<String>();
            }

            // set the basic flag
            boolean showCP = StringUtils.isNotEmpty(valueDefaultCPProperty) && valueDefaultCPProperty.equalsIgnoreCase("false") ? false : true;
            if (showCP)
            {
                value = "true";
            }
            else
            {
                value = "false";
            }

            // check if the role is in the exclude property
            if (StringUtils.isNotEmpty(valueexcludeCPProperty))
            {
                for (String userRole : userRoles)
                {
                    // if there is a role to exclude
                    if (valueexcludeCPProperty.indexOf(userRole) > -1)
                    {
                        if (showCP)
                        {
                            value = "false";
                        }
                        else
                        {
                            value = "true";
                        }
                        break;
                    }
                }// end of for loop
            }

        }// end of if

        return value;

    }// evaluateShowControlPanleProperty

    static class CheckRule
    {
        public Character ch;
        public int maxCount = -1;
        public int count;
        public boolean checkContinuous = true;
    }

    public static void validateWikiDocName(String docName) throws Exception
    {
        boolean result = true;

        if (docName != null)
        {
            if (docName.matches("[^\\*\\^!~`&@#$<>?/()+=,\":;{}\\[\\]|]*"))
            {
                CheckRule dot = new CheckRule();
                dot.ch = '.';
                dot.maxCount = 1;

                CheckRule space = new CheckRule();
                space.ch = ' ';

                Map<Character, CheckRule> checker = new HashMap<Character, CheckRule>();
                checker.put(dot.ch, dot);
                checker.put(space.ch, space);

                Character prv = null;
                for (int i = 0; i < docName.length(); i++)
                {
                    Character ch = docName.charAt(i);

                    if (checker.containsKey(ch))
                    {
                        CheckRule cr = checker.get(ch);
                        if (cr.checkContinuous && ch.equals(prv))
                        {
                            result = false;
                            break;
                        }
                        cr.count += 1;
                        if (cr.maxCount > -1 && cr.count > cr.maxCount)
                        {
                            result = false;
                            break;
                        }

                        checker.put(ch, cr);
                    }

                    prv = ch;
                }
            }
            else
            {
                throw new Exception(WIKI_DOC_NAME_SPL_CHAR_MSG);
            }
        }

        if (!result)
        {
            throw new Exception(INVALID_WIKI_DOC_NAME_MSG);
        }

    }

    /**
     * This method returns a list of wikidoc names that have expired based on a user defined expiry value by querying
     * database table.
     * @param recertifyDate
     * @param namespaces
     * @param docnames
     * @return
     */
    public static List<String> getWikiDocList(Timestamp recertifyDate, String namespaces, String docnames)
    {
        return ServiceJDBC.getWikiDocList(recertifyDate, namespaces, docnames);
    } // getWikiDocList

    /**
     * This method returns a list of wikidoc names that have passed their set u_expire_time from the wikidoc
     * database table.
     * @return
     */
    public static List<String> getWikiDocArchiveList(String archiveNamespaces, String archiveDocnames)
    {
        return ServiceJDBC.getWikiDocArchiveList(archiveNamespaces, archiveDocnames);
    } // getWikiDocList

    public static String extractNamespace(String wikiName)
    {
        String result = wikiName;
        if(StringUtils.isNotBlank(wikiName) && wikiName.indexOf(".") > -1)
        {
            result = wikiName.substring(0, wikiName.indexOf("."));
        }
        return result;
    }
}
