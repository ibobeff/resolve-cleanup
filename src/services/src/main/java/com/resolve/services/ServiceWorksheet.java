/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;

import com.resolve.persistence.model.ArchiveActionResult;
import com.resolve.persistence.model.ArchiveWorksheet;
import com.resolve.rsbase.MainBase;
import com.resolve.util.eventsourcing.EventType;
import com.resolve.util.eventsourcing.IncidentResolutionTaskType;

import com.resolve.util.eventsourcing.EventMessageFactory;

import com.resolve.util.eventsourcing.incident.EventData;
import com.resolve.util.performance.ExecutionTimeUtil;
import com.resolve.util.performance.datasources.ExecutionStep;
import com.resolve.util.performance.datasources.ExecutionStepPhase;
import com.resolve.util.eventsourcing.EventMessage;
import com.resolve.search.SearchException;
import com.resolve.search.model.ExecuteState;
import com.resolve.search.model.ProcessRequest;
import com.resolve.search.model.TaskResult;
import com.resolve.search.model.Worksheet;
import com.resolve.services.util.ExecuteRunbookUtil;
import com.resolve.services.util.WorksheetUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.ScriptDebug;
import com.resolve.util.XDoc;

/**
 * This is the service facade for all things worksheet.
 * 
 */
public class ServiceWorksheet
{
    public static Set<String> getWorksheetProcessids(String problemid)
    {
        return WorksheetUtil.getWorksheetProcessids(problemid);
    }

    public static String getProcessNumberByProcessId(String processid)
    {
        return WorksheetUtil.getProcessNumberByProcessId(processid);
    }

    public static String getProblemNumberByProblemId(String problemid)
    {
        return WorksheetUtil.getProblemNumberByProblemId(problemid);
    }

    public static String createExecuteProcessRequestCAS(String wiki, String newProcessNumber, String problem_sid, String process_sid, Map<String, Object> params, String user, boolean isEvent) throws Exception
    {
        return WorksheetUtil.createExecuteProcessRequestCAS(wiki, newProcessNumber, problem_sid, process_sid, params, user, isEvent);
    }

    public static List<Map<String, Object>> getWorksheetMapsByKeys(List<String> rowKeys, String[] colNames)
    {
        return WorksheetUtil.getWorksheetMapsByKeys(rowKeys, colNames);
    }

    @Deprecated
    public static Map<String, Object> findWorksheetMapByKey(String worksheetId)
    {
        return findWorksheetMapByKey(worksheetId, "system");
    }
    
    /**
     * Look for a worksheet row by its row key. It will return all columns.
     *
     * @param worksheetId
     *            sysId of requested worksheet
     * @return a Java map of all columns of the requested worksheet. Map entry names are original worksheet column names. All columns will be included in return values.
     *
     */
    public static Map<String, Object> findWorksheetMapByKey(String worksheetId, String username)
    {
        Map<String, Object> result = null;
        
        try
        {
            Worksheet worksheet = WorksheetUtil.findWorksheetById(worksheetId, username);
            result = worksheet.asMap();
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }
    
    /**
     * Get worksheet WSDATA map from database by sysId (row key)
     *
     * @param problemid
     *            row key (sysId) of requested worksheet row
     * @return WSDATA Map of requested worksheet. Map is keyed by property names.
     *
     */
    public static WSDataMap getWorksheetWSDATA(String problemId)
    {
        return WorksheetUtil.getWorksheetWSDATA(problemId);
    }

    /**
     * Returns WsDataMap with specified keys.
     *
     * @param problemId
     * @param keyList
     *      Keys of which values are needed.
     * @param username
     * 
     * @return
     *      Map of the specified keys or null.
     */

    public static Map<String, Object> getWorksheetWSDATA(String problemId, List<String> keyList, String username)
    {
        return WorksheetUtil.getWorksheetWSDATA(problemId, keyList, username);
    }

    /**
     * find a WSDATA property value of a requested worksheet
     *
     * @param problemId
     *            row key (sysId)  of requested worksheet
     * @param username
     * @return property value or null if property doesn't exist
     * 
     */
    public static Object getWorksheetWSDATAProperty(String problemId, String propertyName, String username,
    												boolean ignoreESinMemCache) {
        return WorksheetUtil.getWorksheetData(problemId, propertyName, username, ignoreESinMemCache);
    }
    
    /**
     * find a WSDATA property value of a requested worksheet
     *
     * @param problemId
     *            row key (sysId)  of requested worksheet
     * @param username
     * @return property value or null if property doesn't exist
     * 
     */
    public static Object getWorksheetWSDATAProperty(String problemId, String propertyName, String username)
    {
    	return ServiceWorksheet.getWorksheetWSDATAProperty(problemId, propertyName, username, false);
    }

    /**
     * Set a WSDATA property value of requested worksheet
     *
     * @param problemId
     *            sysId (row key) of requested worksheet row
     * @param propertyName
     *            WSDATA property name
     * @param propertyValue
     *            WSDATA property value
     */
    public static void setWorksheetWSDATAProperty(String problemId, String propertyName, Object propertyValue, String username)
    {
        WorksheetUtil.setWorksheetWSDATAProperty(problemId, propertyName, propertyValue, username);
    }
    
    /**
     * Delete the properties from WsDataMap.
     * 
     * @param problemId
     *      sysId of requested worksheet row
     * @param propertyNames
     *      Array of properties to be deleted.
     * @param username
     */
    
    public static void deleteWorksheetWSDATA(String problemId, String[] propertyNames, String username)
    {
        WorksheetUtil.deleteWorksheetWSDATA(problemId, propertyNames, username);
    }

    /**
     * Save whole WSDATA map to database, which includes all its properties.
     *
     * @param problemid
     *            sysId of requested worksheet row
     * @param wsData
     *            a Java Map of all WSDATA property name/value pairs
     *
     */
    public static void saveWorksheetWSDATA(String problemid, WSDataMap wsData, String username)
    {
        WorksheetUtil.saveWorksheetWSDATA(problemid, wsData, username);
    }

    /**
     * Saves the WSDATA.
     *
     * @param problemId
     * @param data
     */
    public static void updateWorksheetWSDATA(String problemId, Map<String, Object> data, String username)
    {
        WorksheetUtil.updateWorksheetWSDATA(problemId, data, username);
    }

    /**
     * Look for a worksheet row by its row key and target column names. Can specify which columns will be returned.
     *
     * @param rowKey
     *            sysId of requested worksheet row
     * @param columns
     *            requested columns of requested worksheet
     * @return a Java map of a worksheet row or null if worksheet
     *         can not be found. Map entry names are original column names. Only requested columns will be included in returned map values.
     */
    public static Map<String, Object> findWorksheetMapByKey(String rowKey, String[] columns)
    {
        return WorksheetUtil.getWorksheetMapByKey(rowKey, columns);
    }

    /**
     * Create a new worksheet in Cassandra.
     *
     * @param params
     *            Map contains column values for a worksheet.
     * @param username
     *            user name of worksheet owner
     * @return worksheet row key (sysId) of new worksheet
     */
    public static String createNewWorksheet(Map params, String username)
    {
        String result = null;
        
        Worksheet worksheet = null;
        
        try
        {
            worksheet = WorksheetUtil.createNewWorksheet(params, username);
        }
        catch(Throwable t)
        {
            throw t;
        }
        
        if(worksheet != null)
        {
            result = worksheet.getSysId();
        }
        return result;
    } // createWorksheetCAS
    
    public static void createNewExecutionSummary(Map<String, String> rbcParams, String username)
    {
        WorksheetUtil.createNewExecutionSummary(rbcParams, username);
    }

    @Deprecated
    public static String initWorksheet(String problemid, String reference, String alertid, String correlationid, String userid, String processid, boolean isEvent, Map params) throws Exception
    {
        String result = null;
        
        Worksheet worksheet = initWorksheet2(problemid, reference, alertid, correlationid, userid, processid, isEvent, params);
        
        if(worksheet != null)
        {
            result = worksheet.getSysId();
        }
        return result;
    }
    
    public static Worksheet initWorksheet2(String problemid, String reference, String alertid, String correlationid, String userid, String processid, boolean isEvent, Map params) throws Exception
    {
    	ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.START);
        Worksheet worksheet = WorksheetUtil.initWorksheet(problemid, reference, alertid, correlationid, userid, processid, isEvent, params);
              
        if(worksheet != null) {
            if(worksheet.getIncidentId() != null && !worksheet.getIncidentId().isEmpty()) {
                
                String rootResolutionId = (String) params.get(Constants.EXECUTE_ROOT_RUNBOOK_ID);
                String content = (String) params.get(Constants.EXECUTE_WIKI);
                EventMessage em = EventMessageFactory.getInstance().getIncidentResolutionCreatedMessage(worksheet.getIncidentId(), rootResolutionId, content, IncidentResolutionTaskType.WIKI.toString());
                MainBase.esb.sendInternalMessage(Constants.ESB_NAME_EVENT_SOURCING_DCS, "", em.toJSON());
                
                //event to update summary,condition, ect.
                em = EventMessageFactory.getInstance()
                                .getIncidentResolutionUpdatedMessage(worksheet.getIncidentId(), rootResolutionId, worksheet.getSummary(), worksheet.getCondition(), worksheet.getSeverity());
                MainBase.esb.sendInternalMessage(Constants.ESB_NAME_EVENT_SOURCING_DCS, "", em.toJSON());
            }
        }
        
    	ExecutionTimeUtil.log((String) params.get("RESOLVE.JOB_ID"), null, null, ExecutionStep.METHOD_EXECUTION, ExecutionStepPhase.END);
        return worksheet;
    }
    
    /**
     * Look for the oldest worksheet with a given process id
     *
     * @param processid
     *            process id value
     * @return rowKey (sysId) of existing worksheet
     */
    public static String getOldestWorksheetByProcessId(String processid)
    {
        return WorksheetUtil.getOldestWorksheetByProcessId(processid);
    }

    /**
     * Look for a worksheet with matching column values. Currenly supported columns are u_reference, u_alert_id, and u_number.
     *
     * @param params
     *            Map of matching column name/value pairs
     * @return rowKey (sysId) of matching worksheet
     */
    public static String findWorksheetByColumnValues(Map<String, String> params)
    {
        return WorksheetUtil.findWorksheetIdByColumnValues(params);
    }// findWorksheetByColumnValues

    /**
     * Get name value pairs of worksheet u_reference column and u_alert_id column if values are not null
     *
     * @param problemid
     *            worksheet row key (sysId)
     * @return a Java map with column names and values as entries
     * @throws Exception 
     */
    public static Map<String, String> getWorksheetReferences(String problemid) throws Exception
    {
        try
        {
            return WorksheetUtil.getWorksheetReferences(problemid);
        }
        catch (SearchException e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
    } // getWorksheetReferenceCAS

    /**
     * Retrieve worksheet's sysId (row key) by using its related process id.
     *
     * @param processid
     *            process id value
     * @return worksheet id value
     */
    public static String getWorksheetIdByProcessId(String processId)
    {
        return WorksheetUtil.getWorksheetIdByProcessId(processId);
    }// getWorksheetByProcessId

    @Deprecated
    public static Worksheet findWorksheetById(String worksheetId)
    {
        return WorksheetUtil.findWorksheetById(worksheetId, "system");
    }
    
    public static Worksheet findWorksheetById(String worksheetId, String username)
    {
        return WorksheetUtil.findWorksheetById(worksheetId, username);
    }
    
    /**
     * Retrieve u_worksheet column value from a worksheet and return it as XDoc object
     *
     * @param problemid
     *            worksheet sysId (id)
     * @return XDoc object built from u_worksheet value
     */
    public static XDoc getWorksheetXMLObject(String problemid) throws Exception
    {
        return WorksheetUtil.getWorksheetXMLObject(problemid);
    }

    /**
     * Delete worksheet rows by row keys (ids)
     *
     * @param worksheetIds
     *            row keys of rows which will be deleted
     * @param isSync
     *            true if delete is synchronous
     *            
     * @return
     */
    public static Collection<String> deleteWorksheetRows(List<String> worksheetIds, boolean isSync, String username)
    {
        return WorksheetUtil.deleteWorksheetRows(worksheetIds, isSync, username);
    }


    /**
     * Delete process requests rows by row keys (ids)
     *
     * @param processRequestIds
     *            row keys of rows which will be deleted
     * @param isSync
     *            true if delete is synchronous
     *            
     * @return
     */
    public static void deleteProcessRequestRows(List<String> processRequestIds, boolean isSync, String username)
    {
        WorksheetUtil.deleteProcessRequestRows(processRequestIds, isSync, username);
    }

    /**
     * Delete ALL process requests with the specified status.
     * @param status
     * 			String specifying the status of process requests to be deleted.
     * @param isSync
     * 			true if delete is synchronous
     * 
     * @param username
     */
    public static void deleteAllProcessRequestRows(String status, String username)
    {
        WorksheetUtil.deleteAllProcessRequestRows(status, username);
    }
    
    /**
     * Delete process task results rows by row keys (ids)
     *
     * @param taskResultIds
     *            row keys of rows which will be deleted
     * @param isSync
     *            true if delete is synchronous
     *            
     * @return
     */
    public static void deleteTaskResultRows(List<String> taskResultIds, boolean isSync, String username)
    {
        WorksheetUtil.deleteTaskResultRows(taskResultIds, isSync, username);
    }

    /**
     * Delete execute state rows by row keys (ids)
     *
     * @param executeStateIds
     *            row keys of rows which will be deleted
     * @return
     */
    public static void deleteExecuteStateRows(List<String> executeStateIds)
    {
        WorksheetUtil.deleteExecuteStateRows(executeStateIds);
    }
    
    /**
     * Delete execute state rows by row keys (ids)
     *
     * @param executeStateIds
     *            row keys of rows which will be deleted
     * @return
     */
    public static void deleteExecuteStateRows(List<String> executeStateIds, String username)
    {
        WorksheetUtil.deleteExecuteStateRows(executeStateIds, username);
    }

    public static String createNewActionResult(Map<String, Object> valueMap)
    {
        return createNewActionResult(valueMap, true);
    }
    
    /**
     * Insert a new Action Result row. Not only action result column values are reuqired, also additional values for worksheet result list column family.
     *
     * @param valueMap a Java map for all column name/value pairs
     *
     * @return row key of new row
     */
    public static String createNewActionResult(Map<String, Object> valueMap, boolean updateSir)
    {
        return WorksheetUtil.createNewActionResult(valueMap, updateSir);
    }

    @Deprecated
    public static void updateWorksheet(String problemid, Map<String, Object> problemRecord)
    {
        WorksheetUtil.updateWorksheet(problemid, problemRecord);
    }

    public static void updateWorksheet(String problemid, Map<String, Object> problemRecord, String username)
    {
        WorksheetUtil.updateWorksheet(problemid, problemRecord, username);
    }
    
    public static void updateWorksheet(Worksheet worksheet)
    {
        WorksheetUtil.updateWorksheet(worksheet);
    }

    public static void updatProcessRequest(ProcessRequest processRequest, String username)
    {
        WorksheetUtil.updatProcessRequest(processRequest, username);
    }

    public static boolean isThisWorksheetCurrent(String problemId)
    {
        return WorksheetUtil.isThisWorksheetCurrent(problemId);
    }

    public static boolean isProcessCompleted(String problemId, String wiki)
    {
        return WorksheetUtil.isProcessCompleted(problemId, wiki);
    }

    public static interface SearchQueryCallback
    {
        public void processKeys(List<String> rowKeys, Map<String,Object> params);
    }

    public static String getProcessStatusForRunbook(String processID)
    {
        return WorksheetUtil.getProcessStatusForRunbook(processID);
    }
    
    public static Pair<String, Integer> getProcessStatusAndATExecCountForRunbook(String processID, 
    																			 boolean ignoreESInMemCache) {
        return WorksheetUtil.getProcessStatusAndATExecCountForRunbook(processID, ignoreESInMemCache);
    }
    
    public static Pair<String, Integer> getProcessStatusAndATExecCountForRunbook(String processID)
    {
    	return getProcessStatusAndATExecCountForRunbook(processID, false);
    }
    
    public static String getSummaryResultForRunbook(String processID)
    {
        return WorksheetUtil.getSummaryResultForRunbook(processID);
    }

    public static String getDetailResultForRunbook(String processID)
    {
        return WorksheetUtil.getDetailResultForRunbook(processID);
    }

    public static String getWSDATAForRunbook(String problemid, String username)
    {
        return WorksheetUtil.getWSDATAForRunbook(problemid, username);
    }

    public static String getExecuteStatusForActionTask(String executeId)
    {
        return WorksheetUtil.getExecuteStatusForActionTask(executeId);
    }

    public static String getSummaryResultForActionTask(String executeId)
    {
        return WorksheetUtil.getSummaryResultForActionTask(executeId);
    }

    public static String getDetailResultForActionTask(String executeId)
    {
        return WorksheetUtil.getDetailResultForActionTask(executeId);
    }

    public static String initExecuteProcess(Map<String, Object> params, String problemid, String processid, String userid, String reference, String alertid, String queryString) throws Exception
    {
        return ExecuteRunbookUtil.initExecuteProcess(params, problemid, processid, userid, reference, alertid, queryString);
    }

    public static String getContentForTaskDetail(String taskresultid)
    {
        return WorksheetUtil.getContentForTaskDetail(taskresultid);
    }

    public static String getRawContentForTaskDetail(String executeresultid)
    {
        return WorksheetUtil.getRawContentForTaskDetail(executeresultid);
    }
    
    @Deprecated
    public static boolean isWorksheetExist(String sys_id, String number, String reference, String alertid, String correlationid)
    {
        return WorksheetUtil.isWorksheetExist(sys_id, number, reference, alertid, correlationid, null, null, "system");
    }
    
    public static boolean isWorksheetExist(String sys_id, String number, String reference, String alertid, String correlationid, String orgId, String orgName)
    {
        return WorksheetUtil.isWorksheetExist(sys_id, number, reference, alertid, correlationid, orgId, orgName, "system");
    }
    
    @Deprecated
    public static String findWorksheetSysId(String sys_id, String number, String reference, String alertid, String correlationid) throws Exception
    {
        return findWorksheetSysId(sys_id, number, reference, alertid, correlationid, null, null, "system");
    }
    
    public static String findWorksheetSysId(String sys_id, String number, String reference, String alertid, String correlationid, String orgId, String orgName, String username) throws Exception
    {
        return WorksheetUtil.findWorksheetSysId(sys_id, number, reference, alertid, correlationid, orgId, orgName, username);
    }
    
    public static void flushWorksheetDebug(ScriptDebug debug)
    {
        WorksheetUtil.flushWorksheetDebug(debug);
    }
    
    public static List<Object> getNodeByProblemId(String problemId, String status)
    {
        return WorksheetUtil.getNodeByProblemId(problemId, status);
    }
    
    public static ProcessRequest findProcessRequestById(String sysId)
    {
        return WorksheetUtil.findProcessRequestById(sysId);
    }

    public static List<ExecuteState> getEventRowsByReference(boolean event_end, String event_reference, String problemid, String event_eventid)
    {
        return WorksheetUtil.getEventRowsByReference(event_end, event_reference, problemid, event_eventid);
    }

    public static List<ExecuteState> findExecuteStates(String sysId, String processId, String problemId, String targetAddress, String reference)
    {
        return WorksheetUtil.findExecuteStates(sysId, processId, problemId, targetAddress, reference);
    }

    public static ExecuteState findExecuteStateById(String sysId)
    {
        return WorksheetUtil.findExecuteStateById(sysId);
    }

    public static ExecuteState findExecuteStateByProcessId(String processId)
    {
        return WorksheetUtil.findExecuteStateByProcessId(processId);
    }

    public static void saveExecuteState(ExecuteState executeState)
    {
        WorksheetUtil.saveExecuteState(executeState);
    }
    
    public static XDoc getWorksheetXML(String problem_id, Map params)
    {
        return WorksheetUtil.getWorksheetXML(problem_id, params);
    }

    public static List<ExecuteState> getEventRowsFor(boolean event_end, String processid, String problemid, String eventid)
    {
        return WorksheetUtil.getEventRowsFor(event_end, processid, problemid, eventid);
    }
    
    public static List<Map<String, Object>> findTaskResultByWorksheet(String sysId, String number, String[] actionTasks, String username) throws Exception
    {
        return WorksheetUtil.findTaskResultByWorksheet(sysId, number, actionTasks, username);
    }
    
    public static Worksheet getArchiveWorksheet(String id, String username)
    {
        Worksheet result = null;
        
        ArchiveWorksheet worksheet = WorksheetUtil.getArchiveWorksheet(id, username);
        if(worksheet != null)
        {
            result = WorksheetUtil.convert(worksheet);
        }
        
        return result;
    }
    
    public static List<Worksheet> listArchiveWorksheets(QueryDTO queryDTO, String username)
    {
        return WorksheetUtil.listArchiveWorksheets(queryDTO, username);
    }

    public static ResponseDTO<TaskResult> listArchiveWorksheetResultsByWorksheetId(String worksheetId, QueryDTO queryDTO, Boolean hidden, String username)
    {
        return WorksheetUtil.listArchiveWorksheetResultsByWorksheetId(worksheetId, queryDTO, hidden, null, null,username);
    }
    
    public static TaskResult getArchiveTaskResult(String id, String username)
    {
        TaskResult result = null;
        
        ArchiveActionResult taskResult = WorksheetUtil.getArchiveTaskResult(id, username);
        if (taskResult != null)
        {
            result = WorksheetUtil.convert(taskResult);
        }
        
        return result;
    }
    
    public static ResponseDTO serveResultMacro(String id, String wiki, QueryDTO query, Boolean hidden, String username) throws SearchException
    {
        return WorksheetUtil.serveResultMacro(id, wiki, query, hidden, username);
    }
} // ServiceWorksheet
