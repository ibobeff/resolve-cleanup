/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.Date;
import java.util.List;

public class FeedbackDTO
{
    private boolean useful = true;
    private int rating;
    private String comment;
    private List<Integer> answers;
    
    private boolean flagged;
    private Date lastReviewedDate;
    
    public boolean isUseful()
    {
        return useful;
    }
    public void setUseful(boolean useful)
    {
        this.useful = useful;
    }
    public int getRating()
    {
        return rating;
    }
    public void setRating(int rating)
    {
        this.rating = rating;
    }
    public String getComment()
    {
        return comment;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }
    public List<Integer> getAnswers()
    {
        return answers;
    }
    public void setAnswers(List<Integer> answers)
    {
        this.answers = answers;
    }
    public boolean isFlagged()
    {
        return flagged;
    }
    public void setFlagged(boolean flagged)
    {
        this.flagged = flagged;
    }
    public Date getLastReviewedDate()
    {
        return lastReviewedDate;
    }
    public void setLastReviewedDate(Date lastReviewedDate)
    {
        this.lastReviewedDate = lastReviewedDate;
    }
}
