package com.resolve.services.task.util;

import org.codehaus.jackson.JsonNode;

import com.resolve.services.rb.util.ABMarkupExtractor;
import com.resolve.util.StringUtils;

public class ParserCodeGenerator
{

    JsonNode jsonReqParams;
    String parserType;
    ABMarkupExtractor extractor;

    /*****
     * Entry point for the assessment code generator
     * 
     * @param uiJson
     *            , JSON markup from UI containing various nodes including
     *            markups, sampeOutput, and columnSeperator
     * @throws Exception
     */
    public ParserCodeGenerator(JsonNode jsonReqParams) throws Exception
    {
        this.jsonReqParams = jsonReqParams;
        this.parserType = jsonReqParams.findValue("parserType").asText();
        if (!StringUtils.equalsIgnoreCase(parserType, "xml"))
            extractor = new ABMarkupExtractor(jsonReqParams);
    }

    public String generateCode() throws Exception
    {
        String parserCode = "";
        
        if(StringUtils.equalsIgnoreCase(this.parserType,"table"))
        {
            ColumnStructuredParserGen parserGen = new ColumnStructuredParserGen(extractor.getRegex() ,extractor.getParseStart(),extractor.getParseEnd(),extractor.getSample(),extractor.getVariables(),extractor.getFlowVariables(),extractor.getWsdataVariables(),extractor.getOutputVariables(),extractor.getOptions());
            parserCode = parserGen.generateCode();
        }
        else if (StringUtils.equalsIgnoreCase(this.parserType,"text"))
        {
            FreeformParserGen parserGen = new FreeformParserGen(extractor.getRegex(),extractor.getParseStart(),extractor.getParseEnd(),extractor.getType(),extractor.getVariables(),extractor.getFlowVariables(),extractor.getWsdataVariables(),extractor.getOutputVariables(),extractor.getOptions());
            parserCode = parserGen.generateCode();
        }else {
            XPathCodeGen parserGen = new XPathCodeGen(this.jsonReqParams);
            parserCode = parserGen.generateParserCode();
        }
        return parserCode;
    }
}
