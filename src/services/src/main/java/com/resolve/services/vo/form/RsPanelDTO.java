/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.form;

import java.util.List;


/**
 * this may be optional based on the UI specs
 * 
 * @author jeet.marwah
 *
 */
public class RsPanelDTO extends RsUIComponent
{
    private int cols;//same as the count of the 'columns'

    private String url;//if its a url, there will be no cols
    
    //for panel
    private List<RsColumnDTO> columns;
    
    //for tab panel
    private List<RsTabDTO> tabs;
    
    public RsPanelDTO()
    {
        
    }

    public int getCols()
    {
        return cols;
    }

    public void setCols(int cols)
    {
        this.cols = cols;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public List<RsColumnDTO> getColumns()
    {
        return columns;
    }

    public void setColumns(List<RsColumnDTO> columns)
    {
        this.columns = columns;
    }

    public List<RsTabDTO> getTabs()
    {
        return tabs;
    }

    public void setTabs(List<RsTabDTO> tabs)
    {
        this.tabs = tabs;
    }    
    
    
    
}
