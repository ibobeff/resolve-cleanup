package com.resolve.services.rb.util;

public class XPathVariable
{
    private String variable;
    private String xpath;
    private Boolean flow;
    private Boolean wsdata;
    private Boolean output;
    public String getVariable()
    {
        return variable;
    }
    public void setVariable(String variable)
    {
        this.variable = variable;
    }
    public String getXpath()
    {
        return xpath;
    }
    public void setXpath(String xpath)
    {
        this.xpath = xpath;
    }
    public Boolean getFlow()
    {
        return flow;
    }
    public void setFlow(Boolean flow)
    {
        this.flow = flow;
    }
    public Boolean getWsdata()
    {
        return wsdata;
    }
    public void setWsdata(Boolean wsdata)
    {
        this.wsdata = wsdata;
    }
    public Boolean getOutput()
    {
        return output;
    }
    public void setOutput(Boolean output)
    {
        this.output = output;
    }
}
