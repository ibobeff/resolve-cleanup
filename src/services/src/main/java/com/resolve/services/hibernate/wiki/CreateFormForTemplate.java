/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.util.ArrayList;
import java.util.List;

import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.MetaAccessRights;
import com.resolve.persistence.model.MetaFormView;
import com.resolve.services.hibernate.customtable.CustomFormUtil;
import com.resolve.services.hibernate.util.SaveUtil;
import com.resolve.services.vo.Operation;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.form.CRUDAction;
import com.resolve.services.vo.form.DataSource;
import com.resolve.services.vo.form.RsButtonAction;
import com.resolve.services.vo.form.RsButtonPanelDTO;
import com.resolve.services.vo.form.RsColumnDTO;
import com.resolve.services.vo.form.RsFormDTO;
import com.resolve.services.vo.form.RsFormProperties;
import com.resolve.services.vo.form.RsPanelDTO;
import com.resolve.services.vo.form.RsTabDTO;
import com.resolve.services.vo.form.RsUIButton;
import com.resolve.services.vo.form.RsUIField;
import com.resolve.util.constants.HibernateConstantsEnum;
import com.resolve.workflow.form.ExtSaveMetaFormView;

public class CreateFormForTemplate
{
    public static final String SCRIPT_TO_CREATE_WIKI_FROM_TEMPLATE = "Create Wiki Using Template";
    private MetaFormView dbView = null;

    private String formViewName = null;
    private AccessRights rights = null;
    private String username = null;
    
    
    public CreateFormForTemplate(String formViewName, AccessRights rights, String username)
    {
        this.formViewName = formViewName;
        this.rights = rights;
        this.username = username;
        
    }
    
    public MetaFormView createForm() throws Exception
    {
        dbView = CustomFormUtil.findMetaFormView(null, formViewName, username);
        
        //if this view does not exist, then create it
        if(dbView == null)
        {
            RsFormDTO form = getRsFormDTO();
            
            ResponseDTO response = new ExtSaveMetaFormView(form, username).save();
            if(response.isSuccess())
            {
                dbView = CustomFormUtil.findMetaFormView(((RsFormDTO)response.getData()).getId(), null, username);
            }
            else
            {
                throw new Exception(response.getMessage());
            }
        }
        else
        {
            //only update the rights
            MetaAccessRights viewRights = dbView.getMetaAccessRights();
            viewRights.setUReadAccess(rights.getUReadAccess());
            viewRights.setUWriteAccess(rights.getUWriteAccess());
            viewRights.setUAdminAccess(rights.getUAdminAccess());
            
            dbView.setMetaAccessRights(viewRights);
            
            //save it
            SaveUtil.saveMetaFormView(dbView, username);
        }
        
        return dbView;
    }
    
    private RsFormDTO getRsFormDTO()
    {
        RsFormDTO form = new RsFormDTO();
        form.setDisplayName(formViewName);
        form.setFormName(formViewName);
        form.setViewName(formViewName);
        form.setWizard(false);
        
        //access rights
        form.setViewRoles(rights.getUReadAccess());
        form.setEditRoles(rights.getUWriteAccess());
        form.setAdminRoles(rights.getUAdminAccess());
        
        //panels
        form.setPanels(getPanels());
        
        //button panel
        form.setButtonPanel(getButtonPanel());
        
        //properties
        form.setProperties(getProperties());
        
        
        return form;
        
    }
    
    private RsFormProperties getProperties()
    {
        RsFormProperties properties = new RsFormProperties();
        
        
        return properties;
    }
    
    private  RsButtonPanelDTO getButtonPanel()
    {
        RsButtonPanelDTO buttonPanel = new  RsButtonPanelDTO();
        buttonPanel.setControls(getControls());
        
        return buttonPanel;
    }
    
    private List<RsUIButton> getControls()
    {
        List<RsUIButton> controls = new ArrayList<RsUIButton>();

        //only 1 button
        RsUIButton submit = new RsUIButton();
        submit.setName("CUSTOM");
        submit.setDisplayName("Create Document");
        submit.setType("button");

        //actions
        submit.setActions(getActions());        
        
        controls.add(submit);
        
        return controls;
    }
    
    private List<RsButtonAction> getActions()
    {
        List<RsButtonAction> actions = new ArrayList<RsButtonAction>();
        
        actions.add(getScriptAction());
        actions.add(getRedirectAction());
        
        return actions;
    }
    
    private RsButtonAction getScriptAction()
    {
        RsButtonAction action = new RsButtonAction();
        action.setCrudAction(CRUDAction.SUBMIT.name());
        action.setOperation(Operation.SCRIPT.name());
        action.setScript(SCRIPT_TO_CREATE_WIKI_FROM_TEMPLATE);
        action.setAdditionalParam("TIMEOUT|=|0");
        action.setRedirectTarget("_blank");
        action.setOrderNumber(1);

        return action;
    }
    
    private RsButtonAction getRedirectAction()
    {
        RsButtonAction action = new RsButtonAction();
        action.setCrudAction(CRUDAction.SUBMIT.name());
        action.setOperation(Operation.REDIRECT.name());
        action.setRedirectUrl("RS.wiki.Main/name=${namespace}.${docname}");
        action.setRedirectTarget("_self");
        action.setOrderNumber(2);
        
        return action;
    }
    
    private List<RsPanelDTO> getPanels()
    {
        List<RsPanelDTO> panels = new ArrayList<RsPanelDTO>();
        
        //their is only 1 panel
        RsPanelDTO panel = new RsPanelDTO();
        panel.setTabs(getTabs());
        
        panels.add(panel);       
        
        return panels;
    }
    
    private List<RsTabDTO> getTabs()
    {
        List<RsTabDTO> tabs = new ArrayList<RsTabDTO>();
        
        //only 1 tab for this form
        RsTabDTO tab = new RsTabDTO();
        tab.setCols(1);
        tab.setColumns(getColumns());
        
        tabs.add(tab);
        
        return tabs;
    }
    
    private List<RsColumnDTO> getColumns()
    {
        List<RsColumnDTO> columns = new ArrayList<RsColumnDTO>();
        
        //only 1 column for this form
        RsColumnDTO column = new RsColumnDTO();
        column.setFields(getFields());
        
        columns.add(column);
        
        return columns;
    }
    
    private List<RsUIField> getFields()
    {
        List<RsUIField> fields = new ArrayList<RsUIField>();
        
        fields.add(createTextField("namespace", "Namespace", 1));
        fields.add(createTextField("docname", "Doc Name", 2));
        fields.add(createHiddenField("template_namespace"));
        fields.add(createHiddenField("template_docname"));
        
        
        return fields;
    }
    
    private RsUIField createTextField(String name, String displayName, int order)
    {
        RsUIField field = new RsUIField();
        field.setXtype("textfield");
        field.setName(name);
        field.setDisplayName(displayName);
        field.setOrderNumber(order);
        field.setSourceType(DataSource.INPUT.name());
        field.setUiType(HibernateConstantsEnum.UI_TEXTFIELD.getTagName());
        field.setLabelAlign("left");
        field.setStringMinLength(1);
        field.setStringMaxLength(100);
        field.setUiStringMaxLength(100);
        field.setReadOnly(false);
        field.setHidden(false);
        field.setMandatory(true);
        field.setEncrypted(false);
        
        return field;
    }
    
    private RsUIField createHiddenField(String name)
    {
        RsUIField field = new RsUIField();
        field.setXtype("hiddenfield");
        field.setName(name);
        field.setSourceType(DataSource.INPUT.name());
        field.setUiType(HibernateConstantsEnum.UI_HIDDEN.getTagName());
        field.setReadOnly(false);
        field.setHidden(false);
        field.setMandatory(false);
        field.setEncrypted(false);
        
        return field;
    }

}
