package com.resolve.services.rr.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;

import com.resolve.persistence.model.BaseModel;
import com.resolve.persistence.model.RRRule;
import com.resolve.persistence.model.RRRuleField;
import com.resolve.persistence.model.RRSchema;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.ConfigSQL;
import com.resolve.services.ServiceResolutionRouting;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.RRRuleFieldVO;
import com.resolve.services.hibernate.vo.RRRuleVO;
import com.resolve.services.hibernate.vo.RRSchemaVO;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.interfaces.VO;
import com.resolve.services.rr.util.TransactionHelper.AtomicCollectionOperation;
import com.resolve.services.rr.util.TransactionHelper.AtomicObjectOperation;
import com.resolve.services.rr.util.TransactionHelper.AtomicRawOperation;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.util.BeanUtil;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;


public class ResolutionRoutingUtil
{
    /**
     * the internal generic crud helper class
     * @author xun.yang
     *
     * @param <T>
     */
    private static interface ExtraAction<M> {
        public M action(M model) throws Exception;
        
    }
  
    private static class Helper<T>{
       
        
        public BaseModel<T> save(final String modelName, BaseModel<T> model, String username) throws Exception {
            final String fullModelName = "com.resolve.persistence.model." + modelName;
            
            if (model == null)
            {
                model = BeanUtil.getInstance(fullModelName);
            }
            
            validateAccess(model, username);
            
            final BaseModel<T> tmp = model;
            return new TransactionHelper<T>(username).doAtomicOp(new AtomicObjectOperation<T>() {
                @SuppressWarnings("unchecked")
                public BaseModel<T> action()
                {   
                    if(StringUtils.isEmpty(tmp.getSys_id()))
                        return (BaseModel<T>) HibernateUtil.saveObj(fullModelName, tmp);
                    HibernateUtil.updateObj(modelName, tmp);
                    return tmp;
                }                
            });
        }
        
        public BaseModel<T> save(
                        final String modelName,
                        final BaseModel<T> model,
                        String username,
                        final ExtraAction<BaseModel<T>> beforeSaveAction,
                        final ExtraAction<BaseModel<T>> afterSaveAction
                        ) throws Exception {
            
            validateAccess(model, username);
            
            final String fullModelName = "com.resolve.persistence.model." + modelName;
            return new TransactionHelper<T>(username).doAtomicOp(new AtomicObjectOperation<T>() {
                @SuppressWarnings("unchecked")
                public BaseModel<T> action() throws Exception
                {   
                    BaseModel<T> returned = null;
                    if(beforeSaveAction!=null)
                        returned = beforeSaveAction.action(model);
                    if(StringUtils.isEmpty(model.getSys_id()))
                        returned = (BaseModel<T>) HibernateUtil.saveObj(fullModelName, model);
                    else {
                        HibernateUtil.updateObj(modelName, model);    
                    }
                    returned = model;
                    if(afterSaveAction!=null)
                        returned = afterSaveAction.action(model);
                    return returned;
                }                
            });
        }
        
        public BaseModel<T> get(final String modelName, final String id, String username) throws Exception {
            final String fullModelName = "com.resolve.persistence.model." + modelName;
            @SuppressWarnings("unchecked")
			BaseModel<T> model = (BaseModel<T>) HibernateProxy.execute(() -> {
            	return new TransactionHelper<T>(username).doAtomicOp(new AtomicObjectOperation<T>() {
                    public BaseModel<T> action()
                    {   
                        BaseModel<T> model = (BaseModel<T>) HibernateUtil.findById(fullModelName,id);
                        return model;
                    }                
                });
            });
            
            validateAccess(model, username);
            
            return model;
        }
        
        public BaseModel<T> getByName(String modelName,final String name,String username) throws Exception{
            final String fullModelName = "com.resolve.persistence.model." + modelName; 
            BaseModel<T> model = new TransactionHelper<T>(username).doAtomicOp(new AtomicObjectOperation<T>() {
                @SuppressWarnings({ "unchecked", "rawtypes" })
                public BaseModel<T> action()
                {   
                    //Query hqlQuery = HibernateUtil.createQuery("from " + fullModelName  + " where name = :name");
                    //hqlQuery.setParameter("name", name);
                    Query hqlQuery = HibernateUtil.createQuery(fullModelName, "name", name);
                    return (BaseModel<T>) hqlQuery.uniqueResult();
                }                
            });
            
            validateAccess(model, username);
            
            return model;
        }
        
        public BaseModel<T> get(final String modelName, final String id, String username,final ExtraAction<BaseModel<T>> extraAction) throws Exception {
            final String fullModelName = "com.resolve.persistence.model." + modelName;
            BaseModel<T> retModel = new TransactionHelper<T>(username).doAtomicOp(new AtomicObjectOperation<T>() {
                @SuppressWarnings("unchecked")
                public BaseModel<T> action() throws Exception
                {   
                    BaseModel<T> model = (BaseModel<T>) HibernateUtil.findById(fullModelName,id);
                    extraAction.action(model);
                    return model;
                }                
            });
            
            validateAccess(retModel, username);
            
            return retModel;
        }
        
        public List<BaseModel<T>> list(final QueryDTO query, String username) throws Exception
        {
            QueryFilter orgFilter = null;
            boolean hasNoOrgAcess = false;
            
            Collection<String> orgIdList = null;
            
            if (query.getFilterItems() != null && !query.getFilterItems().isEmpty())
            {
                for (QueryFilter qryFltr : query.getFilterItems())
                {
                    if ("sysOrg".equals(qryFltr.getField()) || "schema.sysOrg".equals(qryFltr.getField()))
                    {
                        orgFilter = qryFltr;
                        
                        if (StringUtils.isNotBlank(qryFltr.getValue()) &&
                            !VO.STRING_DEFAULT.equalsIgnoreCase(qryFltr.getValue()) &&
                            !"nil".equalsIgnoreCase(qryFltr.getValue()))
                        {
                            Set<OrgsVO> hierarchyOrgVOs = new HashSet<>();
                            /*
                             * UserUtils.getAllOrgsInHierarchy API takes org id as parameter and returns entire org hierarchy.
                             * In some cases, we append '|nil' to the org id as to get compoents who has access to no org as well.
                             * That use case is not applicable here, as we are gathering only the orgs. The corresponding use case is
                             * taken care by the flag 'hasNoOrgAcess' below.
                             */
                            Arrays.asList(qryFltr.getValue().split("\\|")).stream().filter(id -> !id.toLowerCase().equals("nil")).forEach(id -> {
                                hierarchyOrgVOs.addAll(UserUtils.getAllOrgsInHierarchy(id, null));
                            });
                            
                            if (hierarchyOrgVOs != null && !hierarchyOrgVOs.isEmpty())
                            {
                                for (OrgsVO hierarchyOrgVO : hierarchyOrgVOs)
                                {
                                    if (!hasNoOrgAcess && hierarchyOrgVO.getUHasNoOrgAccess().booleanValue())
                                    {
                                        hasNoOrgAcess = true;
                                    }
                                    
                                    if (orgIdList == null)
                                    {
                                        orgIdList = new ArrayList<String>(); 
                                    }
                                    
                                    orgIdList.add(hierarchyOrgVO.getSys_id());
                                }
                            }
                        }
                        else
                        {
                            hasNoOrgAcess = UserUtils.isNoOrgAccessible(username);
                        }
                        
                        break;
                    }
                }
                
                if (orgFilter == null)
                {
                    orgIdList = UserUtils.getUserOrgHierarchyIdList(username);
                    hasNoOrgAcess = UserUtils.isNoOrgAccessible(username);
                }
            }
            else
            {
                orgIdList = UserUtils.getUserOrgHierarchyIdList(username);
                hasNoOrgAcess = UserUtils.isNoOrgAccessible(username);
            }
            
            if (orgIdList != null && !orgIdList.isEmpty())
            {
                String orgIdListQueryString = StringUtils.collectionToString(orgIdList, "|");
                
                if (StringUtils.isNotBlank(orgIdListQueryString))
                {
                    if (hasNoOrgAcess)
                    {
                        orgIdListQueryString += "|nil";
                    }
                }
                
                if (orgFilter != null)
                {
                    orgFilter.setValue(orgIdListQueryString);
                    orgFilter.setType("auto");
                    orgFilter.setCondition("equals");
                    orgFilter.setCaseSensitive(Boolean.FALSE);
                }
                else
                {
                    if (!query.getModelName().equalsIgnoreCase("rrrule")) { // sysOrg only set on RRSchema and not on RRRule.
                        orgFilter = new QueryFilter("auto", orgIdListQueryString, "sysOrg", "equals");
                        query.addFilterItem(orgFilter);
                    }
                }
            }
            else
            {
                if (hasNoOrgAcess)
                {
                    if (orgFilter != null)
                    {
                        orgFilter.setValue("nil");
                        orgFilter.setType("auto");
                        orgFilter.setCondition("equals");
                        orgFilter.setCaseSensitive(Boolean.FALSE);
                    }
                    else
                    {
                        orgFilter = new QueryFilter("auto", "nil", "sysOrg", "equals");
                        query.addFilterItem(orgFilter);
                    }
                }
            }
            
            return new TransactionHelper<T>(username).doAtomicOp(new AtomicCollectionOperation<T>() {
                @SuppressWarnings({ "unchecked", "rawtypes" })
                public List<BaseModel<T>> action()
                {
                	String ridvalue = null;
                	boolean ridcontainspipe = false;
                	for(QueryFilter filter : query.getFilters()){
                		if(filter.getField().equals("rid") && filter.getValue().contains("|")){
                			ridcontainspipe=true;
                			ridvalue = filter.getValue();
                			filter.setValue(filter.getValue().replaceAll("\\|", "%7C"));
                		}
                	}
                    Query hqlQuery;
                    List<BaseModel<T>> result = new ArrayList<BaseModel<T>>();
                    
					try {
						hqlQuery = HibernateUtil.createQuery(query);
						
						if(ridcontainspipe){
	                    	hqlQuery.setParameter("rid_0",ridvalue);
	                    }
	                    if(query.getStart()>=0)
	                        hqlQuery.setFirstResult(query.getStart());
	                    if(query.getLimit()>=0)
	                        hqlQuery.setMaxResults(query.getLimit());
	                    Log.log.debug("Querying RR Rules/Schemes...");
	                    result = hqlQuery.list();
	                    Log.log.debug("Query returned " + (result != null ? result.size() : "no") + " RR Rules/Schemes.");
					} catch (Exception e) {
						Log.log.error("Error " + e.getLocalizedMessage() + " occurred in creating HQL query.", e);
					}
                    return result;
                }
            });
        }
    
        public void delete(String modelName,final Object obj, String username) throws Exception {
            final String fullModelName = "com.resolve.persistence.model." + modelName;
            new TransactionHelper<T>(username).doAtomicOp(new AtomicObjectOperation<T>() {
                public BaseModel<T> action() throws Exception
                {   
                    HibernateUtil.deleteObj(fullModelName, obj);
                    return null;
                }                
            });
        }
        
        private void validateAccess(BaseModel<T> model, String username) throws Exception
        {
            RRSchema schema = null;
            
            if (model instanceof RRSchema)
            {
                schema = (RRSchema) model;
            }
            else if (model instanceof RRRule)
            {
                schema = ((RRRule)model).getSchema();
            }
            
            if (schema != null)
            {
                // Check users access to schema
                
                if (StringUtils.isNotBlank(schema.getSysOrg()))
                {
                    if (!UserUtils.isOrgAccessible(schema.getSysOrg(), username, false, false))
                    {
                        throw new Exception ("User " + username + 
                                             " does not have access to Org schema belongs to. Schema's Org id is " + 
                                             schema.getSysOrg());
                    }
                }
                else
                {
                    if (!UserUtils.isNoOrgAccessible(username))
                    {
                        throw new Exception ("User " + username + 
                                             " does not have access to Org schema belongs to. Schema's Org is None.");
                    }
                }
            }
        }
    }
    
    public static List<RRSchemaVO> listSchemas(QueryDTO query, String username) throws Exception{
        List<BaseModel<RRSchemaVO>> list = new Helper<RRSchemaVO>().list(query, username);
        List<RRSchemaVO> voList = new ArrayList<RRSchemaVO>();
        for(int i = 0;i<list.size();i++) {
            voList.add(list.get(i).doGetVO());
        }
        return voList;
    }
    
    public static List<String> listSchemaIdsByRuleId(final List<String> ids, String username) throws Exception{
        return new TransactionHelper<List<String>>(username).doAtomicOp(new AtomicRawOperation<List<String>>() {

            @SuppressWarnings("rawtypes")
            public List<String> action() throws Exception
            {
                Query hqlQuery = HibernateUtil.createQuery("select distinct schema.sys_id from RRRule rule where rule.sys_id IN (:ids)");
                hqlQuery.setParameterList("ids",ids);
                @SuppressWarnings("unchecked")
                List<String> list = hqlQuery.list();
                return list;
            }
            
        });
    }
    
    public static List<RRSchemaVO> listSchemasByIds(final List<String> ids, String username) throws Exception{
        return new TransactionHelper<List<RRSchemaVO>>(username).doAtomicOp(new AtomicRawOperation<List<RRSchemaVO>>() {

            @SuppressWarnings({ "unchecked", "rawtypes" })
            public List<RRSchemaVO> action() throws Exception
            {
                //Query hqlQuery = HibernateUtil.createQuery("from RRSchema schema where schema.sys_id IN(:ids)");
                //hqlQuery.setParameterList("ids", ids);
                Query hqlQuery = HibernateUtil.createQuery("RRSchema", "schema", "sys_id", ids);
                List<RRSchema> list = hqlQuery.list();
                List<RRSchemaVO> voList = new ArrayList<RRSchemaVO>();
                for(RRSchema schema:list)
                    voList.add(schema.doGetVO());
                Collections.sort(voList,new Comparator<RRSchemaVO>() {

                    public int compare(RRSchemaVO arg0, RRSchemaVO arg1)
                    {
                        return arg0.getOrder()>arg1.getOrder()?1:-1;
                    }
                    
                });
                return voList;
            }
            
        });
    }
    
    public static RRSchemaVO getSchema(String id,String username) throws Exception {
        BaseModel<RRSchemaVO> model = new Helper<RRSchemaVO>().get("RRSchema", id, username);
        return model.doGetVO();
    }
    
    public static RRSchemaVO getSchemaByName(String name, String username) throws Exception{
        BaseModel<RRSchemaVO> model = new Helper<RRSchemaVO>().getByName("RRSchema",name,username);
        if(model==null)
            throw new RRInterestingException(RRInterestingException.SCHEMA_NOT_FOUND,"Schema:" + name + " not found!");
        return model.doGetVO();
    }
    
    public static RRSchemaVO saveSchema(RRSchemaVO vo,final String username, boolean reorder, long updatedOnTime, 
    		ConfigSQL configSQL) throws Exception {
        Helper<RRSchemaVO> helper = new Helper<RRSchemaVO>();
        RRSchema schema;
        BaseModel<RRSchemaVO> model = null;
        
        Log.log.debug("Calling saveSchema with username " + username + " and reorder " + reorder);
        
        // Get schemes from DB
        
        QueryDTO queryDTO = new QueryDTO();
        
        queryDTO.setModelName("RRSchema");
        queryDTO.setOrderBy("tableData.order asc");
        
        List<RRSchemaVO> dbSchemaVOs = listSchemas(queryDTO, "system");
                
        if(!StringUtils.isEmpty(vo.getSys_id())&&!vo.getSys_id().equals(VO.STRING_DEFAULT))
        {
            // Check if any rules/mappings exists for schema before editing if not reordering
            
            List<RRRuleVO> rulesUsingSchema = new ArrayList<RRRuleVO>();
            
            schema = (RRSchema) helper.get("RRSchema", vo.getSys_id(), username);
            
            if (schema.getOrder().intValue() != vo.getOrder().intValue())
            {
                if (!UserUtils.isAdminUser(username))
                {
                    throw new Exception(ServiceResolutionRouting.INSUFFICIENT_RIGHTS_TO_REORDER_SCHEMA);
                }
                
                Log.log.debug("Setting " + vo + " sysUpdatedOn to " + updatedOnTime);
                
                vo.setSysUpdatedOn(GMTDate.getDate(updatedOnTime));
            }
            
            // if any of the field, but the order number, is changed, make sure the schema is not used in any of the rule(s)
            if (!schema.getName().equalsIgnoreCase(vo.getName()) ||
                ((StringUtils.isNotBlank(schema.getSource()) && StringUtils.isBlank(vo.getSource())) ||
                 (StringUtils.isBlank(schema.getSource()) && (StringUtils.isNotBlank(vo.getSource()) && !vo.getSource().equals(VO.STRING_DEFAULT))) ||
                 (StringUtils.isNotBlank(schema.getSource()) && StringUtils.isNotBlank(vo.getSource()) &&
                  !vo.getSource().equals(VO.STRING_DEFAULT) && !schema.getSource().equalsIgnoreCase(vo.getSource()))) ||
                ((StringUtils.isNotBlank(schema.getGatewayQueues()) && StringUtils.isBlank(vo.getGatewayQueues())) ||
                 (StringUtils.isBlank(schema.getGatewayQueues()) && StringUtils.isNotBlank(vo.getGatewayQueues()) && !vo.getGatewayQueues().equals(VO.STRING_DEFAULT)) ||
                 (StringUtils.isNotBlank(schema.getGatewayQueues()) && StringUtils.isNotBlank(vo.getGatewayQueues()) && 
                  !vo.getGatewayQueues().equals(VO.STRING_DEFAULT) && !schema.getGatewayQueues().equalsIgnoreCase(vo.getGatewayQueues()))) || 
                ((StringUtils.isNotBlank(schema.getJsonFields()) && StringUtils.isBlank(vo.getJsonFields())) ||
                 (StringUtils.isBlank(schema.getJsonFields()) && StringUtils.isNotBlank(vo.getJsonFields()) && !vo.getJsonFields().equals(VO.STRING_DEFAULT)) ||
                 (StringUtils.isNotBlank(schema.getJsonFields()) && StringUtils.isNotBlank(vo.getJsonFields()) && 
                  !vo.getJsonFields().equals(VO.STRING_DEFAULT) && !schema.getJsonFields().equalsIgnoreCase(vo.getJsonFields()))))
            {
                queryDTO = new QueryDTO();
                
                queryDTO.setModelName("RRRule");
                queryDTO.addFilterItem(new QueryFilter("schema.name",QueryFilter.EQUALS, vo.getName()));
                queryDTO.setStart(0);
                queryDTO.setLimit(1);
                
                rulesUsingSchema = listRules(queryDTO, username);
            }
            
            if (!rulesUsingSchema.isEmpty())
            {
                throw new Exception(ServiceResolutionRouting.ERR_MSG_SCHEMA_NOT_UPDATED_BECAUSE_IN_USE);
            }
        }
        else
        {
            /*
             * for new Schema, make sure that the order number is not greater than the total number of schemas.
             * Remember, the order number starts from zero!
             * Only super users (admin/resolve_maint/system) will be able to save new schema with any order number, for any other user,
             * the order will always be the last.
             */
            if (UserUtils.isSuperUser(username))
            {
                if (vo.getOrder().intValue() > dbSchemaVOs.size())
                {
                    vo.setOrder(new Integer(dbSchemaVOs.size()));
                }
            }
            else
            {
                vo.setOrder(new Integer(dbSchemaVOs.size()));
            }
            
            schema = new RRSchema();
        }
        
        schema.applyVOToModel(vo);
        
        // make sure same named Schema doesn't already exist.
        QueryDTO query = new QueryDTO();
        query.setModelName("RRSchema");
        query.setStart(0);
        query.setLimit(1);
        query.addFilterItem(new QueryFilter("name",QueryFilter.EQUALS,vo.getName()));
        Log.log.debug("Added Filter for RRSchema: name = " + vo.getName().toLowerCase());
        if(!StringUtils.isEmpty(vo.getSys_id())&&!VO.STRING_DEFAULT.equals(vo.getSys_id()))
        {
            query.addFilterItem(new QueryFilter("sys_id",QueryFilter.NOT_EQUALS,vo.getSys_id()));
            Log.log.debug("Added Filter for RRSchema: sys_id = " + vo.getSys_id().toLowerCase());
        }
        if(!helper.list(query, username).isEmpty())
            throw new Exception("Schema:" + vo.getName() + " already exists!");
        
        final QueryDTO ruleQuery = new QueryDTO();
        ruleQuery.setModelName("RRRule");
        ruleQuery.setLimit(-1);
        ruleQuery.addFilterItem(new QueryFilter("schema.id",QueryFilter.EQUALS,vo.getSys_id()));
        final String schemaId = vo.getSys_id();
        final Helper<RRRuleVO> ruleHelper = new Helper<RRRuleVO>();
        model = helper.save("RRSchema",schema, username,null,new ExtraAction<BaseModel<RRSchemaVO>>()
        {
            public BaseModel<RRSchemaVO> action(BaseModel<RRSchemaVO> model) throws Exception
            {
                if(StringUtils.isEmpty(schemaId))
                    return model;
                RRSchemaVO schema = model.doGetVO();
                List<BaseModel<RRRuleVO>> list = ruleHelper.list(ruleQuery, username);
                ObjectMapper mapper = new ObjectMapper();
                JsonNode fields = mapper.readTree(schema.getJsonFields());
                
                /*
                 * Check user specified field names (case inensitive) for duplicate and
                 * field names (case sensitive) for not one of the reserved Resolve keywords
                 */
                
                Set<String> schemaLCFldNames = new HashSet<String>();
                
                for (int i = 0; i < fields.size(); i++) {
                    JsonNode jsonField = fields.get(i);
                    
                    if (ServiceResolutionRouting.invalidSchemaFieldNames.contains(jsonField.get("name").asText()))
                    {
                        throw new Exception(StringUtils.setToString(ServiceResolutionRouting.invalidSchemaFieldNames, 
                                                                    ServiceResolutionRouting.COMMA_SPACE_SEPARATOR) + 
                                            ServiceResolutionRouting.RESOLVE_RESERVED_FIELD_ERR_MSG_MIDDLE + 
                                            schema.getName() + ServiceResolutionRouting.RESOLVE_ERR_MSG_SCHEMA_FIELD_NAME + 
                                            jsonField.get("name").asText() + 
                                            ServiceResolutionRouting.RESOLVE_RESERVED_FIELD_ERR_MSG_SUFFIX);
                    }
                    
                    if (schemaLCFldNames.contains(jsonField.get("name").asText().toLowerCase()))
                    {
                        throw new Exception (ServiceResolutionRouting.RESOLVE_DUPLICATE_CASE_INSENSITIVE_SCHEMA_FIELD_NAME_PREFIX +
                                             schema.getName() + ServiceResolutionRouting.RESOLVE_ERR_MSG_SCHEMA_FIELD_NAME + 
                                             jsonField.get("name").asText() + 
                                             ServiceResolutionRouting.RESOLVE_DUPLICATE_CASE_INSENSITIVE_SCHEMA_FIELD_NAME_SUFFIX);
                    }
                    else
                    {
                        schemaLCFldNames.add(jsonField.get("name").asText().toLowerCase());
                    }                                    
                }
                
                List<RRRuleField> newRidFields;
                
                for(BaseModel<RRRuleVO> base:list) {
                    RRRule rule = (RRRule)base;
                    List<RRRuleField> ridFields = rule.getRidFields();
                    
                    // Check existing rule field names (case inensitive) for duplicate
                    
                    Set<String> ruleLCFldNames = new HashSet<String>();
                    
                    for (RRRuleField field:ridFields) {
                        if (ruleLCFldNames.contains(field.getName().toLowerCase()))
                        {
                            throw new Exception ("Saved schema " + rule.getSchema().getName() + " has duplicate field " + field.getName() + ". Schema field names (case insensitive) must be unique.");
                        }
                        else
                        {
                            schemaLCFldNames.add(field.getName().toLowerCase());
                        }                                    
                    }
                    
                    Collections.sort(ridFields,new Comparator<RRRuleField>() {

                        public int compare(RRRuleField o1, RRRuleField o2)
                        {
                            if(o1.getOrder()==o2.getOrder())
                                return 0;
                            return o1.getOrder()>o2.getOrder()?1:-1;
                        }
                        
                    });
                    newRidFields = new ArrayList<RRRuleField>();
                    for(int i = 0;i<fields.size();i++) {
                        JsonNode jsonField = fields.get(i);
                        boolean found = false;
                        for(RRRuleField field:ridFields) {
                            if(jsonField.get("name").asText().equalsIgnoreCase(field.getName())) {
                                field.setOrder(jsonField.get("order").asInt());
                                field.setName(jsonField.get("name").asText());
                                newRidFields.add(field);
                                found = true;
                                break;
                            }   
                        }
                        if(found)
                            continue;
                        newRidFields.add(new RRRuleField(null,jsonField.get("name").asText(),"",jsonField.get("order").asInt(),rule));
                    }
                    
                    for(int i = 0;i<newRidFields.size();i++) 
                        if(i<ridFields.size()) 
                            newRidFields.get(i).setValue(ridFields.get(i).getValue());
                    rule.getRidFields().clear();
                    rule.getRidFields().addAll(newRidFields);
                    List<String> rid = new ArrayList<String>();
                    for(RRRuleField field:rule.getRidFields()) 
                        rid.add(field.getValue());
                    rule.setRid(StringUtils.join(rid,StringUtils.DELIMITER));
                    ruleHelper.save("RRRule", rule, username);
                }
                return model;
            }
        });
        
        if (reorder)
        {
            ReOrderSchemes(username, configSQL);
        }
        
        return model.doGetVO();
    }
    
    public static void deleteSchemas(List<String> ids, final String username, ConfigSQL configSQL) throws Exception
    {
        Helper<RRSchemaVO> schemaHelper = new Helper<RRSchemaVO>();
        final Helper<RRRuleVO> ruleHelper = new Helper<RRRuleVO>();
        
        for(String id:ids)
        {
            List<RRRuleVO> rulesUsingSchema = new ArrayList<RRRuleVO>();
            
            RRSchema schema = (RRSchema) schemaHelper.get("RRSchema", id, username);
            
            QueryDTO queryDTO = new QueryDTO();
            
            queryDTO.setModelName("RRRule");
            QueryFilter schemaFilter = new QueryFilter("auto", schema.getName().toLowerCase(), "schema.name", "equals");
            queryDTO.addFilterItem(schemaFilter);
            queryDTO.setStart(0);
            queryDTO.setLimit(1);
            
            rulesUsingSchema = listRules(queryDTO, username);
            
            if (!rulesUsingSchema.isEmpty())
            {
                throw new Exception(ServiceResolutionRouting.ERR_MSG_SCHEMA_NOT_DELETED_BECAUSE_IN_USE + 
                                    " Schema currently in use is \"" + schema.getName() + "\".");
            }
        }
        
        for(String id:ids)
        {
            BaseModel<RRSchemaVO> model = schemaHelper.get("RRSchema", id, username, new ExtraAction<BaseModel<RRSchemaVO>>() {

                public BaseModel<RRSchemaVO> action(BaseModel<RRSchemaVO> model) throws Exception
                {
                    RRSchema schema = (RRSchema)model;
                    for(RRRule rule:schema.getRules()) {
                        rule.setSchema(null);
                        ruleHelper.save("RRRule", rule, username);
                    }
                    return model;
                }
            });
           
            schemaHelper.delete("RRSchema", model, username);
        }
        
        ReOrderSchemes(username, configSQL);
    }
    
    public static List<RRRuleVO> listRules(QueryDTO query, String username) throws Exception {
        List<BaseModel<RRRuleVO>> list = new Helper<RRRuleVO>().list(query, username);
        List<RRRuleVO> voList = new ArrayList<RRRuleVO>();
        for(int i = 0;i<list.size();i++)
            voList.add(list.get(i).doGetVO());
        return voList;
    }
    
    public static RRRuleVO getRule(String id,String username) throws Exception {
        @SuppressWarnings("unchecked")
		BaseModel<RRRuleVO> model = (BaseModel<RRRuleVO>) HibernateProxy.execute(() -> {
        	return new Helper<RRRuleVO>().get("RRRule", id, username,new ExtraAction<BaseModel<RRRuleVO>>() {

                public BaseModel<RRRuleVO> action(BaseModel<RRRuleVO> model)
                {
                if (model == null) {
                    return model;
                }
                    for(RRRuleField field:((RRRule)model).getRidFields()) 
                        field.doGetVO();
                    return model;
                }
            });
        	
        });
        RRRuleVO rule =  null;
        if (model != null) {
            rule = model.doGetVO();
        }
        if(null != rule && null!=rule.getSchema()) {
            BaseModel<RRSchemaVO> schema = new Helper<RRSchemaVO>().get("RRSchema", rule.getSchema().getSys_id(), username);
            RRSchemaVO schemaVO = schema.doGetVO();
            rule.setSchema(schemaVO);
        }
        
        return rule;
    }
    
    public static void deleteRuleByQuery(QueryDTO query,String username) throws Exception {
        Helper<RRRuleVO> helper = new Helper<RRRuleVO>();
        List<BaseModel<RRRuleVO>> list = helper.list(query, username);
        while(!list.isEmpty()) {
            for(BaseModel<RRRuleVO> model:list)
                helper.delete("RRRule", model, username);
            list = helper.list(query, username);
        }
            
    }
    
    public static void deleteRulesById(List<String> ids,String username) throws Exception {
        Helper<RRRuleVO> helper = new Helper<RRRuleVO>();
        for(String id:ids) {
            BaseModel<RRRuleVO> model = helper.get("RRRule", id, username);
            helper.delete("RRRule", model, username);
        }       
    }
    
    public static RRRuleVO saveRule(final RRRuleVO vo, final String username) throws Exception {
        RRRule rule;
        Helper<RRRuleVO> helper = new Helper<RRRuleVO>();
        if(!StringUtils.isEmpty(vo.getSys_id())&&!VO.STRING_DEFAULT.equals(vo.getSys_id())) 
            rule = (RRRule) helper.get("RRRule", vo.getSys_id(), username);
        else {
            rule = new RRRule();
            rule.setRidFields(new ArrayList<>());
        }
        
        if(vo.getRidFields()!=null) {
            List<String> list = new ArrayList<String>();
            for(RRRuleFieldVO field:vo.getRidFields()) {
                list.add(field.getValue());
            }
            rule.setRid(StringUtils.join(list,StringUtils.DELIMITER));
        }
        
        RRSchemaVO schema = null;
        if(vo.getSchema()!=null){
            schema = getSchema(vo.getSchema().getId(),username);
        }
        vo.setSchema(schema);
        rule.applyVOToModelWithFieldOrdered(vo);
        
        QueryDTO query = new QueryDTO();
        query.setModelName("RRRule");
        query.addFilterItem(new QueryFilter("rid",QueryFilter.EQUALS,rule.getRid()));
        if(schema==null)
            query.addFilterItem(new QueryFilter("schema",QueryFilter.EQUALS,"NIL"));
        else
            query.addFilterItem(new QueryFilter("schema.name",QueryFilter.EQUALS,schema.getName()));
        if(!StringUtils.isEmpty(vo.getSys_id())&&!vo.getSys_id().equals(VO.STRING_DEFAULT))
            query.addFilterItem(new QueryFilter("sys_id",QueryFilter.NOT_EQUALS,rule.getSys_id()));
        List<BaseModel<RRRuleVO>> list =helper.list(query, username); 
        if(!list.isEmpty())
            throw new RRInterestingException(RRInterestingException.RULE_EXIST,"Rule:" + rule.getRid() + " for schema:"+schema.getName()+" already exists!",list.get(0).doGetVO());
        BaseModel<RRRuleVO> model = helper.save("RRRule",rule, username);
        return model.doGetVO();
    }
    
    public static List<String> listRuleModules(final QueryDTO query,String username) throws Exception{
        QueryFilter orgFilter = null;
        boolean hasNoOrgAcess = false;
        
        Collection<String> orgIdList = null;
        
        if (query.getFilterItems() != null && !query.getFilterItems().isEmpty())
        {
            for (QueryFilter qryFltr : query.getFilterItems())
            {
                if ("sysOrg".equals(qryFltr.getField()) || "schema.sysOrg".equals(qryFltr.getField()))
                {
                    orgFilter = qryFltr;
                    
                    if (StringUtils.isNotBlank(qryFltr.getValue()) &&
                        !VO.STRING_DEFAULT.equalsIgnoreCase(qryFltr.getValue()) &&
                        !"nil".equalsIgnoreCase(qryFltr.getValue()))
                    {
                        Set<OrgsVO> hierarchyOrgVOs = UserUtils.getAllOrgsInHierarchy(qryFltr.getValue(), null);
                        
                        if (hierarchyOrgVOs != null && !hierarchyOrgVOs.isEmpty())
                        {
                            for (OrgsVO hierarchyOrgVO : hierarchyOrgVOs)
                            {
                                if (!hasNoOrgAcess && hierarchyOrgVO.getUHasNoOrgAccess().booleanValue())
                                {
                                    hasNoOrgAcess = true;
                                }
                                
                                if (orgIdList == null)
                                {
                                    orgIdList = new ArrayList<String>(); 
                                }
                                
                                orgIdList.add(hierarchyOrgVO.getSys_id());
                            }
                        }
                    }
                    else
                    {
                        hasNoOrgAcess = UserUtils.isNoOrgAccessible(username);
                    }
                    
                    break;
                }
            }
            
            if (orgFilter == null)
            {
                orgIdList = UserUtils.getUserOrgHierarchyIdList(username);
                hasNoOrgAcess = UserUtils.isNoOrgAccessible(username);
            }
        }
        else
        {
            orgIdList = UserUtils.getUserOrgHierarchyIdList(username);
            hasNoOrgAcess = UserUtils.isNoOrgAccessible(username);
        }
        
        if (orgIdList != null && !orgIdList.isEmpty())
        {
            String orgIdListQueryString = StringUtils.collectionToString(orgIdList, "|");
            
            if (StringUtils.isNotBlank(orgIdListQueryString))
            {
                if (hasNoOrgAcess)
                {
                    orgIdListQueryString += "|nil";
                }
            }
            
            if (orgFilter != null)
            {
                orgFilter.setValue(orgIdListQueryString);
                orgFilter.setType("auto");
                orgFilter.setCondition("equals");
                orgFilter.setCaseSensitive(Boolean.FALSE);
            }
            else
            {
                orgFilter = new QueryFilter("auto", orgIdListQueryString, "sysOrg", "equals");
                query.addFilterItem(orgFilter);
            }
        }
        else
        {
            if (hasNoOrgAcess)
            {
                if (orgFilter != null)
                {
                    orgFilter.setValue("nil");
                    orgFilter.setType("auto");
                    orgFilter.setCondition("equals");
                    orgFilter.setCaseSensitive(Boolean.FALSE);
                }
                else
                {
                    orgFilter = new QueryFilter("auto", "nil", "sysOrg", "equals");
                    query.addFilterItem(orgFilter);
                }
            }
        }
        
        query.setSelectColumns("distinct module");
        return new TransactionHelper<List<String>>(username).doAtomicOp(new AtomicRawOperation<List<String>>() {

            @SuppressWarnings({ "unchecked", "rawtypes" })
            public List<String> action()
            {
                Query hqlQuery;
				try {
					hqlQuery = HibernateUtil.createQuery(query);
					if(query.getStart()>=0)
                        hqlQuery.setFirstResult(query.getStart());
                    if(query.getLimit()>=0)
                        hqlQuery.setMaxResults(query.getLimit());
				} catch (Exception e) {
					Log.log.error("Error " + e.getLocalizedMessage() + " occurred in creating HQL query.", e);
					return new ArrayList<String>();
				}
                return hqlQuery.list();
            }
        });
    }
    
    public static Integer getNextSchemaOrder(String username) throws Exception {
        String sql = "select max(u_order) from rr_schema";
        return 1 + new TransactionHelper<Integer>(username).doAtomicOp(new AtomicRawOperation<Integer>() {

            @SuppressWarnings("rawtypes")
			public Integer action()
            {
                Query hqlQuery;
				try {
					hqlQuery = (Query) HibernateProxy.execute(() -> {return HibernateUtil.createSQLQuery(sql);});
				} catch (Exception e) {
					Log.log.error("Error " + e.getLocalizedMessage() + " occurred in creating query from SQL [" + 
								  sql + "].", e);
					return -1;
				}
                Object obj = hqlQuery.uniqueResult();
                if(obj==null)
                    return -1;
                return Integer.valueOf(obj.toString());
            }
            
        });
    }
    
    public static void ReOrderSchemes(String username, ConfigSQL configSQL) throws Exception
    {
        String uppercaseDBType = configSQL.getDbtype().toUpperCase();
        
        final String updateSchemesOrderSql;
        
        Log.log.debug("Calling ReOrderSchemes with username " + username);
        
        if (uppercaseDBType.equals("MYSQL"))
        {
            updateSchemesOrderSql = 
            		"update rr_schema mrs set mrs.u_order = " + 
            			"(select ors.rwnum from " + 
            				"(select rs.sys_id m_sys_id, rs.rwnum from " + 
            					"(select irs.sys_id, @i\\:=@i+1 rwnum from rr_schema irs, (select @i\\:=-1) as iter " + 
            						"order by irs.u_order asc, irs.sys_updated_on desc) rs ) ors where mrs.sys_id = ors.m_sys_id)";
        }
        else
        {
            updateSchemesOrderSql = "update rr_schema mrs set mrs.u_order = " +
            							"(select ors.rwnum from " + 
            								"(select rs.sys_id m_sys_id, rownum - 1 as rwnum from " +
                                    			"(select sys_id from rr_schema order by u_order asc, sys_updated_on desc) rs ) ors " +
                                    				"where mrs.sys_id = ors.m_sys_id)";
        }
        
		try {
        HibernateProxy.setCurrentUser(username);
			HibernateProxy.execute(() -> {
				NativeQuery<?> query = HibernateUtil.getCurrentSession().createNativeQuery(updateSchemesOrderSql);
				int recsUpdated = query.executeUpdate();

				/*
				Query<?> query = HibernateUtil.createQuery(updateSchemesOrderSql);

				int recsUpdated = query.executeUpdate();
				*/

				Log.log.debug("Updated " + recsUpdated + " records.");
				return recsUpdated;
			});

		} catch (Exception e) {
			Log.log.error("Error " + e.getMessage() + " in reordering schemas:" + updateSchemesOrderSql, e);
			HibernateUtil.rethrowNestedTransaction(e);

		}
    }
    
    public static Map<String, Object> getMappingComponents(String id, String username) {
        Map<String, Object> result = new HashMap<>();
        RRRuleVO ruleVO = null;
        try {
            ruleVO = getRule(id, username);
        }
        catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
        
        if (null != ruleVO) {
            if (StringUtils.isNotBlank(ruleVO.getWiki())) {
                result.put(ImpexEnum.WIKI.getValue(), Arrays.asList(String.format("%s:%s", ruleVO.getWiki(), ruleVO.getWikiId())));
            }
            List<String> runbooks = new ArrayList<>();
            if (StringUtils.isNotBlank(ruleVO.getRunbook())) {
                runbooks.add(String.format("%s:%s", ruleVO.getRunbook(), ruleVO.getRunbookId()));
            }
            if (StringUtils.isNotBlank(ruleVO.getWiki())) {
                runbooks.add(String.format("%s:%s", ruleVO.getAutomation(), ruleVO.getAutomationId()));
            }
            if (CollectionUtils.isNotEmpty(runbooks)) {
                result.put(ImpexEnum.RUNBOOK.getValue(), runbooks);
            }
            if (StringUtils.isNotBlank(ruleVO.getSirPlaybook())) {
                result.put(ImpexEnum.SECURITY_TEMPLATE.getValue(), Arrays.asList(String.format("%s:%s", ruleVO.getSirPlaybook(), ruleVO.getSirId())));
            }
        }
        return result;
    }
    
    public static Map<String, Object> getSchemaMappings(String schemaId, String username) {
        Map<String, Object> result = new HashMap<>();
        List<RRRule> list = new ArrayList<>();
        try {
            HibernateUtil.action(hibernateUtil -> {
                RRSchema schema = HibernateUtil.getDAOFactory().getRRSchemaDAO().findById(schemaId);
                schema.getRules().size();
                list.addAll(schema.getRules());
            }, username);
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
        
        List<String>  mappingList = null;
        if (CollectionUtils.isNotEmpty(list)) {
            mappingList = list.stream().map(rule -> String.format("%s:%s", rule.getRid(), rule.getSys_id())).collect(Collectors.toList());
        }
        
        if (CollectionUtils.isNotEmpty(mappingList)) {
            result.put(ImpexEnum.RID_MAPPING.getValue(), mappingList);
        }
        return result;
    }
}
