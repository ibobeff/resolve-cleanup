/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.actiontask;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.OrganizationUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SearchActionTask
{
    private QueryDTO query = null; 
    private String username = null;

    private int start = -1;
    private int limit = -1;
    private Set<String> userRoles = null;
    
    public SearchActionTask(QueryDTO query, String username) throws Exception
    {
        if(query == null || StringUtils.isEmpty(username))
        {
            throw new RuntimeException("Query and username are mandatory to search for wiki");
        }
        
        this.query = query;
        this.username = username;
        
        this.start = query.getStart();
        this.limit = query.getLimit();
        userRoles = UserUtils.getUserRoles(this.username);
    }
    
    public ResponseDTO<ResolveActionTaskVO> execute() throws Exception
    {
        ResponseDTO<ResolveActionTaskVO> response = new ResponseDTO<ResolveActionTaskVO>();
        List<ResolveActionTaskVO> vos = new ArrayList<ResolveActionTaskVO>();
        int total  = 0;
        
        this.query.setPrefixTableAlias("at.");
        query.setSelectColumns("");
        
        //manipulate the query first
        manipuateQueryDTOForResolveActionTaskGrid();
        manipulateFilter();
        
        //prepare the sql 
        String hql = createHql();
        
        //set it in the query object
        this.query.setHql(hql.toString());
        
        try
        {
            //execute the qry
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, start, limit, true);
            if(data != null)
            {
                //grab the map of sysId-organizationname  
                Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();

                String selectedColumns = query.getSelectColumns();
                if (StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");

                    //this will be the array of objects
                    for (Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        ResolveActionTask instance = new ResolveActionTask();

                        //set the attributes in the object
                        for (int x = 0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];

                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        //get the access rights for this doc. - ** takes longer...from 80ms to 200 ms
//                        AccessRights ar = getAccessRightsFor(instance.getSys_id());
//                        instance.setAccessRights(ar);

                        vos.add(convertModelToVO(instance, mapOfOrganization));
                    }//end of for loop
                }
                else
                {
                    for (Object o : data)
                    {
                        ResolveActionTask instance = (ResolveActionTask) o;
                        vos.add(convertModelToVO(instance, mapOfOrganization));
                    }
                }
            }//end of if
            

            //get the total count
            total = ServiceHibernate.getTotalHqlCount(query);
            
            
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting data for sql:" + hql.toString(), e);
            throw e;
        }
        
        response.setRecords(vos);
        response.setTotal(total);
        
        return response;
    }
    
    
    //private apis
    
    private String createHql()
    {
        StringBuilder hql = new StringBuilder("select ");
//        hql.append(" at.sys_id,at.sysCreatedOn,at.sysCreatedBy,at.sysUpdatedOn,at.sysUpdatedBy,at.UName,at.UNamespace,at.UFullName,at.USummary,at.UActive,at.assignedTo,at.assignedTo.UUserName as assignedToUsername ");
        hql.append(" at ") ;
        hql.append(" from ResolveActionTask as at, AccessRights as ar where at.sys_id = ar.UResourceId and at.accessRights = ar.sys_id and ar.UResourceType ='").append(ResolveActionTask.RESOURCE_TYPE).append("' and ");
//        hql.append(" at.assignedTo.UUserName = 'admin' and ");
        
        //role filter 
        String roleFilter = "";
        //qry only if its not an 'admin', else show everything
        if (!this.userRoles.contains("admin") && !username.equals("resolve.maint"))
        {
            StringBuilder filters = new StringBuilder("(");
            for (String userRole : userRoles)
            {
                filters.append(" ar.UAdminAccess like '%" + userRole + "%' or ar.UReadAccess like '%" + userRole + "%' or");
            }
            filters.setLength(filters.length() - 2);
            filters.append(")");  
            roleFilter = filters.toString();
        }
        else
        {
            roleFilter = " 1=1 ";
        }
        hql.append(roleFilter);
        
        
        //additional where clause
        String whereClause = this.query.getWhereClause();
        if(StringUtils.isNotBlank(whereClause))
        {
//            StringBuilder updatedWhereClause = new StringBuilder();
//            String[] clauses = whereClause.split("and");
//            for(String clause : clauses)
//            {
//                updatedWhereClause.append(query.getPrefixTableAlias() + clause.trim() + " and ");
//            }
//            
//            updatedWhereClause.setLength(updatedWhereClause.length() - 4);
//            
            hql.append(" and ").append(whereClause);
        }
        
        //search filter
        String searchQry = this.query.getFilterWhereClause();
        if (StringUtils.isNotBlank(searchQry))
        {
            hql.append(" and ").append(searchQry);
//            hql.append(" and ").append("at.assignedTo.UUserName = 'admin' ");
        }
        
        //sort condition --> [{"property":"name","direction":"DESC"}]
        //comes from the UI...so will have to add the 'wd.' in front of the properties
        this.query.parseSortClause();
        String sortBy = this.query.getOrderBy();
        if(StringUtils.isBlank(sortBy))
        {
            sortBy = this.query.getSort();
        }
        
        //sort by
        if(StringUtils.isNotBlank(sortBy))
        {
            hql.append(" order by ").append(sortBy.trim());
        }

        
        
        return hql.toString();
    }
    
    
    
    
    
    
    private static ResolveActionTaskVO convertModelToVO(ResolveActionTask model, Map<String, String> mapOfOrganization)
    {
        ResolveActionTaskVO vo = null;
    
        if (model != null)
        {
            if (mapOfOrganization == null)
            {
                mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
            }
    
            vo = model.doGetVO();
            if (StringUtils.isNotBlank(vo.getSysOrg()))
            {
                vo.setSysOrganizationName(mapOfOrganization.get(vo.getSysOrg()));
            }
            else
            {
                vo.setSysOrganizationName("");
            }
            
            //nullify some objects to reduce the network traffic
            if(vo.getResolveActionInvoc() != null)
            {
                vo.getResolveActionInvoc().setAssess(null);
                vo.getResolveActionInvoc().setParser(null);
                vo.getResolveActionInvoc().setPreprocess(null);
                vo.getResolveActionInvoc().setResolveActionInvocOptions(null);
                vo.getResolveActionInvoc().setResolveActionTaskMockData(null);
            }
    
            //updating the inputOutputDesc
//            String inputOutputDesc = getInputsOrOutputs(model.getResolveActionInvoc());
           // inputOutputDesc = filter(inputOutputDesc);
//            inputOutputDesc = StringEscapeUtils.unescapeHtml(inputOutputDesc);
           
            //update the desc
//            vo.setInputOutputDesc(inputOutputDesc);
            
            if(vo.getAssignedTo() != null)
            {
                vo.setAssignedToUsername(vo.getAssignedTo().getUUserName());
            }
    
        }
    
        return vo;
    }
    
    private void manipuateQueryDTOForResolveActionTaskGrid()
    {
        String whereClause = "";
        String delimiter = "/";
    
        if (StringUtils.isNotBlank(this.query.getWhereClause()))
        {
            String whereClauseArray[] = query.getWhereClause().split(",");
            if (whereClauseArray.length == 1)
                whereClause = whereClauseArray[0];
            else if (whereClauseArray.length == 2)
            {
                whereClause = whereClauseArray[0];
                delimiter = whereClauseArray[1];
            }
            if (whereClause.equalsIgnoreCase("root_menu_id") || whereClause.equalsIgnoreCase("root_module_id"))
            {
                whereClause = "";
            }
            if (StringUtils.isNotBlank(whereClause))
            {
                QueryFilter qrf = null;
//                whereClause = whereClause.replaceAll("//", "/");
                //            whereClause = whereClause.substring(1);
                StringBuffer where = new StringBuffer();
                if (delimiter.equalsIgnoreCase("/"))
                {
                    if (whereClause.equals("/"))
                    {
//                        where.append(" where (at.UMenuPath is null or at.UMenuPath = '')");
//                        where.append(" at.UMenuPath is null or at.UMenuPath = '')");
                    }
                    else
                    {
                        qrf = new QueryFilter();
                        qrf.setType("auto");
                        qrf.setField("UMenuPath");
                        StringBuilder qrfFieldValueBldr = new StringBuilder();
                        
//                        where.append(" (at.UMenuPath = '").append(whereClause).append("' OR at.UMenuPath = '" + whereClause.substring(1) + "') ");
                        boolean hasSlash = whereClause.endsWith("/");
                        if(hasSlash)
                        {
                            whereClause = whereClause.substring(0, whereClause.lastIndexOf("/"));
                        }
                        
                        where.append(" (at.UMenuPath = '").append(whereClause).append("' ");
                        qrfFieldValueBldr.append(whereClause);
                        
                        if(hasSlash)
                        {
                            where.append(" OR at.UMenuPath like '").append(whereClause).append("/%' ");
                            qrfFieldValueBldr.append("|" + whereClause + "%");
                        }
                        where.append(")");
                        
                        qrf.setValue(qrfFieldValueBldr.toString());
                        qrf.setCaseSensitive(Boolean.TRUE);
                        
                        this.query.addFilterItem(qrf);
                    }
                }
                else
                {
                    qrf = new QueryFilter();
                    qrf.setType("auto");
                    qrf.setField("UNamespace");
                    StringBuilder qrfFieldValueBldr = new StringBuilder();
                    
                    whereClause = whereClause.replaceAll("/", ".");
//                    where.append(" (at.UNamespace = '").append(whereClause).append("' OR at.UNamespace = '" + whereClause.substring(1) + "') ");
                    boolean hasDot = whereClause.endsWith(".");
                    if(hasDot)
                    {
                        whereClause = whereClause.substring(0, whereClause.lastIndexOf("."));
                    }
                    
                    where.append(" (at.UNamespace = '").append(whereClause).append("' ");
                    qrfFieldValueBldr.append(whereClause);
                    
                    if(hasDot)
                    {
                        where.append(" OR at.UNamespace like '").append(whereClause).append(".%' ");
                        qrfFieldValueBldr.append("|" + whereClause + "%");
                    }
                    where.append(")");
                    
                    qrf.setValue(qrfFieldValueBldr.toString());
                    qrf.setCaseSensitive(Boolean.TRUE);
                    
                    this.query.addFilterItem(qrf);
                }
                
                Log.log.debug("where = [" + where.toString() + "], qrf = [" + (qrf !=  null ? qrf : "null") + "]");
                
                whereClause = /*where.toString()*/"";
            }
            //set the where clause
            query.setWhereClause(whereClause);
        }
    
    }
    
    private void manipulateFilter()
    {
        //[{"field":"assignedToUsername","type":"auto","condition":"equals","value":"admin"}]
        //assignedToUsername --> at.assignedTo.UUserName
        
        List<QueryFilter> filters = this.query.getFilterItems();
        if(filters != null && filters.size() > 0)
        {
            QueryFilter filterToRemove = null;
            for(QueryFilter filter : filters)
            {
                String field = filter.getField();
                if(field.equalsIgnoreCase("assignedToUsername"))
                {
                    String comp = filter.getComp();
                    String value = (String) filter.getParameters().get(0).getValue();
                    
                    String whereClause = StringUtils.isNotBlank(query.getWhereClause()) ? (query.getWhereClause() + " and ") :  "";
                    whereClause = whereClause + " at.assignedTo.UUserName " + comp + " '" + value + "'";
                    query.setWhereClause(whereClause);
                    
                    filterToRemove = filter;
                    
//                    filter.setField("assignedTo.UUserName");
                }
            }
            
            //this will throw concurrency error if done inside the loop
            filters.remove(filterToRemove);

            this.query.setFilterItems(filters);
        }
        
    }
}
