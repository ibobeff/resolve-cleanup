package com.resolve.services.rb.util;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.assess.AssessResult;
import com.resolve.groovy.script.GroovyScript;
import com.resolve.services.util.BindingScript;
import com.resolve.services.util.WSDataMapImpl;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import groovy.lang.Binding;
import net.sf.json.JSONObject;

public class ParserCodeTestProcess extends Thread
{
    private String groovyScript;
    private String sampleData;
    private Boolean success;
    private ABProcessConst stage;
    private String message;
    private Map<String,Object> result;
    private String username;
    
    public ParserCodeTestProcess(String groovyScript, String sampleData,String username)
    {
        super();
        this.username = username;
        this.groovyScript = groovyScript;
        this.sampleData = sampleData;
        this.stage = ABProcessConst.STARTED;
        this.success=true;
        this.result = new HashMap<String, Object>();
    }
    
    public JSONObject getStatus() throws Exception {
        JSONObject status = new JSONObject();
        status.put("stage", this.stage.toString());
        status.put("success",this.success);
        status.put("message", this.message);
        status.put("result", this.result);
        return status;
    }
    
    private void cleanUp(){
        int count = 0;
        while(count<=5) {
            count++;
            try
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        try
        {
            ResolutionBuilderUtils.pollParserTest(this.username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public void run()
    {
        if (StringUtils.isNotBlank(groovyScript))
        {
            this.stage = ABProcessConst.PARSER_TEST_SETUP;
            if (Log.log.isDebugEnabled())
            {
                Log.log.debug("Test groovy script");
                // Log.log.debug("     Task Id: " + taskId);
                Log.log.debug("     Script: " + groovyScript);
                Log.log.debug("     Sample data: " + sampleData);
            }
            Binding binding = new Binding();
            
            binding.setVariable(Constants.GROOVY_BINDING_PARSER, new BindingScript(BindingScript.TYPE_PARSER, binding));
            binding.setVariable(Constants.GROOVY_BINDING_RAW, sampleData);
            binding.setVariable(Constants.GROOVY_BINDING_LOG, Log.log);
            binding.setVariable(Constants.GROOVY_BINDING_DEBUG, true);
            AssessResult assessResult = new AssessResult("", "1", "", sampleData, 10, "local");
            binding.setVariable(Constants.GROOVY_BINDING_RESULT, assessResult);
            
            //parser gets these bindings 
            //pass the inputs so it returns debug info
            Map<String, Object> inputs = new HashMap<String, Object>();
            inputs.put("TEST", Boolean.TRUE);
            binding.setVariable(Constants.GROOVY_BINDING_INPUTS, inputs);
            binding.setVariable(Constants.GROOVY_BINDING_OUTPUTS, new HashMap());
            binding.setVariable(Constants.GROOVY_BINDING_FLOWS, new HashMap());
            binding.setVariable(Constants.GROOVY_BINDING_PARAMS, new HashMap());
            binding.setVariable(Constants.GROOVY_BINDING_WSDATA, new WSDataMapImpl("test", new HashMap()));
            binding.setVariable(Constants.GROOVY_BINDING_PROPERTIES, new HashMap());
            this.stage = ABProcessConst.PARSER_TEST_RUN;
            // execute script
            try
            {
                // timeout is 60 seconds
                Object scriptResult = GroovyScript.executeAsync(groovyScript, "test", false, binding, 60 * 1000L);
                if(scriptResult != null)
                {
                    if(scriptResult instanceof Map)
                    {
                        result = (Map) scriptResult;
                    }
                    else
                    {
                        Log.log.debug("Parser code returned unexpected object type of : " + scriptResult.getClass() + ", expected java.util.Map.");
                    }
                }
                else
                {
                    Log.log.debug("Parser code did not return any data");
                }
            }
            catch (Exception ex)
            {
                Log.log.error("Failed to execute script");
                this.message = "Failed to execute script";
                this.success = false;
            }

            if(Log.log.isTraceEnabled())
            {
                Log.log.trace("Filter Script DATA: " + result);
            }
        }
        this.stage = ABProcessConst.DONE;
        this.cleanUp();
    }

}
