/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.customtable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.dom4j.Element;

import com.resolve.persistence.model.CustomTable;
import com.resolve.persistence.model.MetaAccessRights;
import com.resolve.persistence.model.MetaField;
import com.resolve.persistence.model.MetaFieldProperties;
import com.resolve.persistence.util.ColumnDefinition;
import com.resolve.persistence.util.CustomTableDefinition;
import com.resolve.persistence.util.CustomTableMappingUtil;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.persistence.util.ManyToOneDefinition;
import com.resolve.persistence.util.PropertyDefinition;
import com.resolve.services.ServiceCustomTable;
import com.resolve.services.constants.CustomFormUIType;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.util.constants.HibernateConstantsEnum;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.hibernate.vo.CustomTableVO;
import com.resolve.services.hibernate.vo.MetaFieldVO;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.util.WorkflowUtil;
import com.resolve.services.vo.form.RsCustomTableDTO;
import com.resolve.services.vo.form.RsUIField;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class CreateCustomTable
{
    private RsCustomTableDTO customTableFromUI = null;
    private String username = null;
    private CustomTableVO customTable = null;
    
    private List<RsUIField> existingColsExceptSysFields = null;
    private List<RsUIField> listOfNewColumnsAddedFromForm = null;
    
    private boolean createOrAlterConcreteTable = false;
    private boolean newSysColumnsExist = false;
    
    private Element customTableSchemaElement = null;
    
    public CreateCustomTable(RsCustomTableDTO customTable, String username) throws Exception
    {
        if(customTable == null || StringUtils.isBlank(username))
        {
            throw new RuntimeException("Table info and username are mandatory to create table.");
        }
        
        this.customTableFromUI = customTable;
        this.username = username;

        //initialize
        init();
        
    }
    
    
    public CustomTableVO create() throws Throwable
    {
        createTable();
        
        //prepare the new refreshed table VO
        customTable = ServiceCustomTable.getCustomTableWithReferences(this.customTableFromUI.getSys_id(), this.customTableFromUI.getUName(), "system");
        
        return customTable;
    }
    
    private void createTable() throws Throwable
    {
        if(createOrAlterConcreteTable)
        {
            //create the table
            createConcreteTable();
            
            //reinit hibernate
            reInitHibernate();
        }
        
        //add/update the columns to the metaField tables
        updateMetaTables();
        
        //update the SectionContainerField with all the references 
//        updateSectionContainerFieldJson();
        
        //update Default MEta table View for this table
        updateDefaultMetaTableView();
    }
    
    private void reInitHibernate()
    {
        HibernateUtil.reinit();
    }
    
    private void updateDefaultMetaTableView() throws Throwable
    {
        new CreateMetaTableView(this.customTableFromUI.getUName(), username).createDefaultView(customTableFromUI);
    }
    
    private void updateMetaTables()  throws Throwable
    {
        List<RsUIField> allColumns = new ArrayList<RsUIField>();
        allColumns.addAll(existingColsExceptSysFields);
        allColumns.addAll(listOfNewColumnsAddedFromForm);
        
        if(customTable == null)
        {
            //add sys columns also
            allColumns.addAll(getSysFields(customTableFromUI.getUName(), customTableFromUI.getViewRoles(), customTableFromUI.getEditRoles(), customTableFromUI.getAdminRoles()));
        }
        else
        {
            if(!newSysColumnsExist)
            {
                allColumns.addAll(getAdditionalSysFields(customTableFromUI.getUName(), customTableFromUI.getViewRoles(), customTableFromUI.getEditRoles(), customTableFromUI.getAdminRoles()));
            }
        }
        
        CustomTable customTableDB = null;
        String tableName = customTableFromUI.getUName();
        String modelName = tableName;
        String displayName = customTableFromUI.getUDisplayName();//tableModel.get(WorkflowConstants.CUSTOM_TABLE_DISPLAYNAME.getTagName());
        String type = customTableFromUI.getUType();//tableModel.get(WorkflowConstants.CUSTOM_TABLE_TYPE.getTagName());
        String referenceTableNames = customTableFromUI.getUreferenceTableNames();//tableModel.get(RSCustomTable.REFERENCE_TABLE_NAMES);
        
        try
        {

            if(customTable == null)
            {
                customTableDB = new CustomTable();
            }
            else
            {
            	
              HibernateProxy.setCurrentUser(username);
                customTableDB =  (CustomTable) HibernateProxy.execute(() -> {
                		return HibernateUtil.getDAOFactory().getCustomTableDAO().findById(customTable.getSys_id());
                	});
            }
            
            customTableDB.setUName(tableName);
            customTableDB.setUModelName(modelName);
            customTableDB.setUDisplayName(displayName);
            customTableDB.setUType(type);
            customTableDB.setUSource(customTableFromUI.getUSource());//(String) tableModel.get(WorkflowConstants.CUSTOM_TABLE_SOURCE.getTagName()));
            customTableDB.setUDestination(customTableFromUI.getUDestination());//(String) tableModel.get(WorkflowConstants.CUSTOM_TABLE_DESTINATION.getTagName()));
            if (this.customTableSchemaElement != null)
            {
                customTableDB.setUSchemaDefinition(this.customTableSchemaElement.asXML());
                customTableDB.setChecksum(calculateCTChecksum(customTableDB.getUSchemaDefinition()));
            }

            if(StringUtils.isNotBlank(referenceTableNames))
            {
                //make sure its unique
                Set<String> refTableNames = new HashSet<String>();
                if(StringUtils.isNotBlank(customTableDB.getUReferenceTableNames()))
                {
                    refTableNames.addAll(StringUtils.convertStringToList(customTableDB.getUReferenceTableNames(), ","));
                }
                refTableNames.addAll(StringUtils.convertStringToList(referenceTableNames, ","));
                referenceTableNames = StringUtils.convertCollectionToString(refTableNames.iterator(), ",");

                customTableDB.setUReferenceTableNames(referenceTableNames);
            }

            CustomTable customTableDBFinal = customTableDB;
            HibernateProxy.execute(() -> {
	            //persist it immediately
	            HibernateUtil.getDAOFactory().getCustomTableDAO().persist(customTableDBFinal);
	            
	            //add the metafields to this table
	            customTableDBFinal.setMetaFields(createMetaFields(customTableDBFinal, customTableDBFinal.getMetaFields(), allColumns));
            
            });
            
            //create the VO for furthur ref
            this.customTable = customTableDB.doGetVO();
        }
        catch(Throwable t)
        {
            Log.log.error(t.getMessage(), t);

            throw new Exception("Error in creating/updating the custom table :" + this.customTableFromUI.getUName());
        }
    }
    
    private int calculateCTChecksum(String schema)
    {
        int checksum = 0;
        
        if (StringUtils.isNotBlank(schema)) {
            schema = schema.replaceAll(ImpexEnum.REMOVE_SYS_COLUMNS_REGEX.getValue(), "");
            if (Log.log.isDebugEnabled())
                Log.log.debug(String.format("CT schema for checksum: %s", schema));
            checksum = Objects.hash(schema);
        }
        
        return checksum;
    }
    
    private Collection<MetaField> createMetaFields(CustomTable customTableDB, Collection<MetaField> metaFields, List<RsUIField> allColumns)
    {
        Collection<MetaField> fields = null;

        if(allColumns.size() > 0)
        {
            fields = new ArrayList<MetaField>();

            for(RsUIField field : allColumns)
            {
                fields.add(createMetaField(customTableDB, field, metaFields));
            }
            
        }

        return fields;
    }
    
    private MetaField createMetaField(CustomTable customTableDB, RsUIField uifield, Collection<MetaField> metaFields)
    {
        MetaField field = findMetaField(uifield, metaFields);

        //create the recs only if its new, else return the reference
        if(field == null)
        {
            field = new MetaField();

            String columnName = uifield.getDbcolumn();//(String) uifield.get(RSMetaField.COLUMN);//WorkflowConstants.META_FIELD_COLUMN.getTagName());
//            columnName = columnName.trim().toLowerCase().replace(" ", "_");

            //u_ will be coming from UI
//            if(!columnName.startsWith("sys_"))
//            {
//                columnName = "u_" + columnName.trim().toLowerCase().replace(" ", "_");
//            }

            //update the model with the columnname
//            uifield.set(RSMetaField.NAME, columnName);

            //these fields cannot be edited once created
            field.setUName(columnName);
            field.setUTable(customTableDB.getUName());
            field.setUColumn(columnName);
            field.setUColumnModelName(columnName);
            field.setUType(uifield.getUiType());//(String)uifield.get(RSMetaField.TYPE));//WorkflowConstants.META_FIELD_TYPE.getTagName()));
            field.setUDbType(uifield.getDbtype());//(String)uifield.get(RSMetaField.DBTYPE));//WorkflowConstants.META_FIELD_DB_TYPE.getTagName()));


            field.setUDisplayName(uifield.getDisplayName());//(String)uifield.get(RSMetaField.DISPLAYNAME));//WorkflowConstants.META_FIELD_DISPLAYNAME.getTagName()));

            //persist it immediately
            HibernateUtil.getDAOFactory().getMetaFieldDAO().persist(field);

            field.setCustomTable(customTableDB);
            field.setMetaFieldProperties(setMetaFieldProperties(field.getMetaFieldProperties(), uifield));
            field.setMetaAccessRights(setMetaAccessRights(customTableDB, field, field.getMetaAccessRights(), uifield));
        }
        return field;
    }
    
    private MetaFieldProperties setMetaFieldProperties(MetaFieldProperties metaFieldProperties,  RsUIField uifield)
    {
        if(metaFieldProperties == null)
        {
            metaFieldProperties = new MetaFieldProperties();
        }

        //**NOTE - same as CustomTableUtil.java --> setMetaFieldProperties
        metaFieldProperties.setUName(uifield.getName());//(RSMetaField.NAME) != null ? (String)uifield.get(RSMetaField.NAME) : null);
        metaFieldProperties.setUTable(uifield.getDbtable());//(RSMetaField.TABLE) != null ? (String)uifield.get(RSMetaField.TABLE) : null);
        metaFieldProperties.setUUIType(uifield.getUiType());//(RSMetaField.UITYPE) != null ? (String)uifield.get(RSMetaField.UITYPE) : null);
        metaFieldProperties.setUOrder(uifield.getOrderNumber());//(RSMetaField.ORDER) != null ? (Integer)uifield.get(RSMetaField.ORDER) : null);
//        metaFieldProperties.setUJavascript(uifield.get(RSMetaField.JAVASCRIPT) != null ? (String)uifield.get(RSMetaField.JAVASCRIPT) : null);
        metaFieldProperties.setUSize(uifield.getSize());//(RSMetaField.SIZE) != null ? (Integer)uifield.get(RSMetaField.SIZE) : null);
        metaFieldProperties.setUIsMandatory(uifield.isMandatory());//get(RSMetaField.ISMANDATORY) != null ? (Boolean)uifield.get(RSMetaField.ISMANDATORY) : false);
        metaFieldProperties.setUIsCrypt(uifield.isEncrypted());//get(RSMetaField.ISCRYPT) != null ? (Boolean)uifield.get(RSMetaField.ISCRYPT) : false);
        metaFieldProperties.setUDefaultValue(uifield.getDefaultValue());//(RSMetaField.DEFAULTVALUE) != null ? (String)uifield.get(RSMetaField.DEFAULTVALUE) : null);
        metaFieldProperties.setUWidth(uifield.getWidth());//(RSMetaField.WIDTH) != null ? (Integer)uifield.get(RSMetaField.WIDTH) : null);
        metaFieldProperties.setUHeight(uifield.getHeight());//(RSMetaField.HEIGHT) != null ? (Integer)uifield.get(RSMetaField.HEIGHT) : null);
        metaFieldProperties.setULabelAlign(uifield.getLabelAlign());//(RSMetaField.LABEL_ALIGN) != null ? (String)uifield.get(RSMetaField.LABEL_ALIGN) : null);
        metaFieldProperties.setUStringMinLength(uifield.getStringMinLength());//(RSMetaField.STRINGMINLENGTH) != null ? (Integer)uifield.get(RSMetaField.STRINGMINLENGTH) : null);
        metaFieldProperties.setUStringMaxLength(uifield.getStringMaxLength());//(RSMetaField.STRINGMAXLENGTH) != null ? (Integer)uifield.get(RSMetaField.STRINGMAXLENGTH) : null);
        metaFieldProperties.setUUIStringMaxLength(uifield.getUiStringMaxLength());//(RSMetaField.UISTRINGMAXLENGTH) != null ? (Integer)uifield.get(RSMetaField.UISTRINGMAXLENGTH) : null);
        metaFieldProperties.setUBooleanMaxLength(uifield.getBooleanMaxLength());//(RSMetaField.BOOLEANMAXLENGTH) != null ? (Integer)uifield.get(RSMetaField.BOOLEANMAXLENGTH) : null);
        metaFieldProperties.setUSelectIsMultiSelect(uifield.getIsMultiSelect());//(RSMetaField.SELECT_IS_MULTISELECT) != null ? (Boolean)uifield.get(RSMetaField.SELECT_IS_MULTISELECT) : false);
        metaFieldProperties.setUSelectUserInput(uifield.getSelectUserInput());//(RSMetaField.SELECT_USER_INPUT) != null ? (Boolean)uifield.get(RSMetaField.SELECT_USER_INPUT) : false);
        metaFieldProperties.setUSelectMaxDisplay(uifield.getSelectMaxDisplay());//(RSMetaField.SELECT_MAX_DISPLAY) != null ? (Integer)uifield.get(RSMetaField.SELECT_MAX_DISPLAY) : null);
        metaFieldProperties.setUSelectValues(uifield.getSelectValues());//(RSMetaField.SELECT_VALUES) != null ? (String)uifield.get(RSMetaField.SELECT_VALUES) : null);
        metaFieldProperties.setUChoiceValues(uifield.getChoiceValues());//(RSMetaField.CHOICE_VALUES) != null ? (String)uifield.get(RSMetaField.CHOICE_VALUES) : null);
        metaFieldProperties.setUCheckboxValues(uifield.getCheckboxValues());//(RSMetaField.CHECKBOX_VALUES) != null ? (String)uifield.get(RSMetaField.CHECKBOX_VALUES) : null);
        metaFieldProperties.setUDateTimeHasCalendar(uifield.getDateTimeHasCalendar());//(RSMetaField.DATETIME_HAS_CALENDAR) != null ? (Boolean)uifield.get(RSMetaField.DATETIME_HAS_CALENDAR) : false);
        metaFieldProperties.setUIntegerMinValue(uifield.getIntegerMinValue());//(RSMetaField.INTEGER_MIN_VALUE) != null ? (Integer)uifield.get(RSMetaField.INTEGER_MIN_VALUE) : null);
        metaFieldProperties.setUIntegerMaxValue(uifield.getIntegerMaxValue());//(RSMetaField.INTEGER_MAX_VALUE) != null ? (Integer)uifield.get(RSMetaField.INTEGER_MAX_VALUE) : null);
        metaFieldProperties.setUDecimalMinValue(uifield.getDecimalMinValue());//(RSMetaField.DECIMAL_MIN_VALUE) != null ? (Double)uifield.get(RSMetaField.DECIMAL_MIN_VALUE) : null);
        metaFieldProperties.setUDecimalMaxValue(uifield.getDecimalMaxValue());//(RSMetaField.DECIMAL_MAX_VALUE) != null ? (Double)uifield.get(RSMetaField.DECIMAL_MAX_VALUE) : null);
        metaFieldProperties.setUJournalRows(uifield.getJournalRows());//(RSMetaField.JOURNAL_ROWS) != null ? (Integer)uifield.get(RSMetaField.JOURNAL_ROWS) : null);
        metaFieldProperties.setUJournalColumns(uifield.getJournalColumns());//(RSMetaField.JOURNAL_COLUMNS) != null ? (Integer)uifield.get(RSMetaField.JOURNAL_COLUMNS) : null);
        metaFieldProperties.setUJournalMinValue(uifield.getJournalMinValue());//(RSMetaField.JOURNAL_MIN_VALUES) != null ? (Integer)uifield.get(RSMetaField.JOURNAL_MIN_VALUES) : null);
        metaFieldProperties.setUJournalMaxValue(uifield.getJournalMaxValue());//(RSMetaField.JOURNAL_MAX_VALUES) != null ? (Integer)uifield.get(RSMetaField.JOURNAL_MAX_VALUES) : null);
        metaFieldProperties.setUListRows(uifield.getListRows());//(RSMetaField.LIST_ROWS) != null ? (Integer)uifield.get(RSMetaField.LIST_ROWS) : null);
        metaFieldProperties.setUListColumns(uifield.getListColumns());//(RSMetaField.LIST_COLUMNS) != null ? (Integer)uifield.get(RSMetaField.LIST_COLUMNS) : null);
        metaFieldProperties.setUListMaxDisplay(uifield.getListMaxDisplay());//(RSMetaField.LIST_MAX_DISPLAY) != null ? (Integer)uifield.get(RSMetaField.LIST_MAX_DISPLAY) : null);
        metaFieldProperties.setUListValues(uifield.getListValues());//(RSMetaField.LIST_VALUES) != null ? (String)uifield.get(RSMetaField.LIST_VALUES) : null);


        String refTableName = uifield.getReferenceTable();//(RSMetaField.REFERENCE_TABLE) != null ? (String) uifield.get(RSMetaField.REFERENCE_TABLE) : null);
        String refColumnName = uifield.getReferenceColumnName();//(RSMetaField.REFERENCE_DISPLAY_COLUMN) != null ? (String)uifield.get(RSMetaField.REFERENCE_DISPLAY_COLUMN) : null;
        String refParams = uifield.getReferenceParams();//(RSMetaField.REFERENCE_PARAMS) != null ? (String)uifield.get(RSMetaField.REFERENCE_PARAMS) : null;
        
        //TODO: Prepare URL based on new EXTJS UI
        if (StringUtils.isNotEmpty(refParams) && StringUtils.isNotBlank(refTableName) && StringUtils.isNotEmpty(refColumnName))
        {
            String moduleName = WorkflowUtil.getModuleName(refTableName);

            if (moduleName != null && !moduleName.equals(""))
            {
                refParams = moduleName + "?main=" + WorkflowUtil.getMainName(refTableName) + "&sys_id=${sys_id}";
            }
            else
            {
                refParams = "/resolve/service/wiki/view/" + refTableName + "/${view}?sys_id=${sys_id}&view=${view}";
            }
        }
        metaFieldProperties.setUReferenceTable(refTableName);
        metaFieldProperties.setUReferenceDisplayColumn(refColumnName);
        metaFieldProperties.setUReferenceDisplayColumnList(uifield.getReferenceDisplayColumnList());//(RSMetaField.REFERENCE_DISPLAY_COLUMN_LIST) != null ? (String)uifield.get(RSMetaField.REFERENCE_DISPLAY_COLUMN_LIST) : null);
        metaFieldProperties.setUReferenceTarget(uifield.getReferenceTarget());//(RSMetaField.REFERENCE_TARGET) != null ? (String)uifield.get(RSMetaField.REFERENCE_TARGET) : null);
        metaFieldProperties.setUReferenceParams(refParams);


        metaFieldProperties.setULinkTarget(uifield.getLinkTarget());//(RSMetaField.LINK_TARGET) != null ? (String)uifield.get(RSMetaField.LINK_TARGET) : null);
        metaFieldProperties.setULinkParams(uifield.getLinkParams());//(RSMetaField.LINK_PARAMS) != null ? (String)uifield.get(RSMetaField.LINK_PARAMS) : null);
        metaFieldProperties.setULinkTemplate(uifield.getLinkTemplate());//(RSMetaField.LINK_TEMPLATE) != null ? (String)uifield.get(RSMetaField.LINK_TEMPLATE) : null);
        metaFieldProperties.setUSequencePrefix(uifield.getSequencePrefix());//(RSMetaField.SEQUENCE_PREFIX) != null ? (String)uifield.get(RSMetaField.SEQUENCE_PREFIX) : null);
        metaFieldProperties.setUHiddenValue(uifield.getHiddenValue());//(RSMetaField.HIDDEN_VALUE) != null ? (String)uifield.get(RSMetaField.HIDDEN_VALUE) : null);
        metaFieldProperties.setUGroups(uifield.getGroups());//(RSMetaField.GROUPS) != null ? (String)uifield.get(RSMetaField.GROUPS) : null);
        metaFieldProperties.setUUsersOfTeams(uifield.getUsersOfTeams());//(RSMetaField.USERS_OF_TEAMS) != null ? (String)uifield.get(RSMetaField.USERS_OF_TEAMS) : null);
        metaFieldProperties.setURecurseUsersOfTeam(uifield.getRecurseUsersOfTeam());//(RSMetaField.RECURSE_USERS_OF_TEAMS) != null ? (Boolean)uifield.get(RSMetaField.RECURSE_USERS_OF_TEAMS) : false);
        metaFieldProperties.setUTeamsOfTeams(uifield.getTeamsOfTeams());//(RSMetaField.TEAMS_OF_TEAMS) != null ? (String)uifield.get(RSMetaField.TEAMS_OF_TEAMS) : null);
        metaFieldProperties.setURecurseTeamsOfTeam(uifield.getRecurseTeamsOfTeam());//(RSMetaField.RECURSE_TEAMS_OF_TEAMS) != null ? (Boolean)uifield.get(RSMetaField.RECURSE_TEAMS_OF_TEAMS) : false);
        metaFieldProperties.setUAutoAssignToMe(uifield.getAutoAssignToMe());//(RSMetaField.AUTO_ASSIGN_TO_ME) != null ? (Boolean)uifield.get(RSMetaField.AUTO_ASSIGN_TO_ME) : false);
        metaFieldProperties.setUAllowAssignToMe(uifield.getAllowAssignToMe());//(RSMetaField.ALLOW_ASSIGN_TO_ME) != null ? (Boolean)uifield.get(RSMetaField.ALLOW_ASSIGN_TO_ME) : false);
        metaFieldProperties.setUIsHidden(uifield.isHidden());//(RSMetaField.IS_HIDDEN) != null ? (Boolean)uifield.get(RSMetaField.IS_HIDDEN) : false);
        metaFieldProperties.setUIsReadOnly(uifield.isReadOnly());//(RSMetaField.IS_READONLY) != null ? (Boolean)uifield.get(RSMetaField.IS_READONLY) : false);

        metaFieldProperties.setUTeamPickerTeamsOfTeams(uifield.getTeamPickerTeamsOfTeams());//(RSMetaField.TP_TEAMS_OF_TEAMS) != null ? (String)uifield.get(RSMetaField.TP_TEAMS_OF_TEAMS) : null);

        //file upload property
        metaFieldProperties.setUFileUploadTableName(uifield.getFileUploadTableName());//(RSMetaField.FILE_UPLOAD_TABLE_NAME) != null ? (String)uifield.get(RSMetaField.FILE_UPLOAD_TABLE_NAME) : null);
        metaFieldProperties.setUFileUploadReferenceColumnName(uifield.getReferenceColumnName());//(RSMetaField.FILE_UPLOAD_REFERENCE_COL_NAME) != null ? (String)uifield.get(RSMetaField.FILE_UPLOAD_REFERENCE_COL_NAME) : null);
        metaFieldProperties.setUAllowUpload(uifield.getAllowUpload());//(RSMetaField.ALLOW_UPLOAD) != null ? (Boolean)uifield.get(RSMetaField.ALLOW_UPLOAD) : false);
        metaFieldProperties.setUAllowDownload(uifield.getAllowDownload());//(RSMetaField.ALLOW_DOWNLOAD) != null ? (Boolean)uifield.get(RSMetaField.ALLOW_DOWNLOAD) : false);
        metaFieldProperties.setUAllowRemove(uifield.getAllowRemove());//(RSMetaField.ALLOW_REMOVE) != null ? (Boolean)uifield.get(RSMetaField.ALLOW_REMOVE) : false);
        metaFieldProperties.setUAllowFileTypes(uifield.getAllowedFileTypes());//(RSMetaField.ALLOW_FILE_TYPES) != null ? (String)uifield.get(RSMetaField.ALLOW_FILE_TYPES) : null);

        //reference grid
        metaFieldProperties.setURefGridReferenceByTable(uifield.getRefGridReferenceByTable());//(RSMetaField.REF_GRID_REF_BY_TABLE) != null ? (String)uifield.get(RSMetaField.REF_GRID_REF_BY_TABLE) : null);
        metaFieldProperties.setURefGridSelectedReferenceColumns(uifield.getRefGridSelectedReferenceColumns());//(RSMetaField.REF_GRID_SELECTED_REF_COL) != null ? (String)uifield.get(RSMetaField.REF_GRID_SELECTED_REF_COL) : null);
        metaFieldProperties.setURefGridReferenceColumnName(uifield.getRefGridReferenceColumnName());//(RSMetaField.REF_GRID_REF_COL_NAME) != null ? (String)uifield.get(RSMetaField.REF_GRID_REF_COL_NAME) : null);
        metaFieldProperties.setUAllowReferenceTableAdd(uifield.getAllowReferenceTableAdd());//(RSMetaField.REF_GRID_ALLOW_REF_TABLE_ADD) != null ? (String)uifield.get(RSMetaField.REF_GRID_ALLOW_REF_TABLE_ADD) : null);
        metaFieldProperties.setUAllowReferenceTableRemove(uifield.getAllowReferenceTableRemove());//(RSMetaField.REF_GRID_ALLOW_REF_TABLE_REMOVE) != null ? (String)uifield.get(RSMetaField.REF_GRID_ALLOW_REF_TABLE_REMOVE) : null);
        metaFieldProperties.setUAllowReferenceTableView(uifield.getAllowReferenceTableView());//(RSMetaField.REF_GRID_REF_ALLOW_REF_TABLE_VIEW) != null ? (String)uifield.get(RSMetaField.REF_GRID_REF_ALLOW_REF_TABLE_VIEW) : null);
        metaFieldProperties.setUReferenceTableUrl(uifield.getReferenceTableUrl());//(RSMetaField.REF_GRID_REF_TABLE_URL) != null ? (String)uifield.get(RSMetaField.REF_GRID_REF_TABLE_URL) : null);
        metaFieldProperties.setURefGridViewPopup(uifield.getRefGridViewPopup());//(RSMetaField.REF_GRID_VIEW_POPUP) != null ? (String)uifield.get(RSMetaField.REF_GRID_VIEW_POPUP) : null);
        metaFieldProperties.setURefGridViewPopupWidth(uifield.getRefGridViewPopupWidth());//RSMetaField.REF_GRID_VIEW_POPUP_WIDTH) != null ? (String)uifield.get(RSMetaField.REF_GRID_VIEW_POPUP_WIDTH) : null);
        metaFieldProperties.setURefGridViewPopupHeight(uifield.getRefGridViewPopupHeight());//(RSMetaField.REF_GRID_VIEW_POPUP_HEIGHT) != null ? (String)uifield.get(RSMetaField.REF_GRID_VIEW_POPUP_HEIGHT) : null);
        metaFieldProperties.setURefGridViewPopupTitle(uifield.getRefGridViewTitle());//(RSMetaField.REF_GRID_VIEW_POPUP_TITLE) != null ? (String)uifield.get(RSMetaField.REF_GRID_VIEW_POPUP_TITLE) : null);

        metaFieldProperties.setUWidgetColumns(uifield.getWidgetColumns());//(RSMetaField.WIDGET_COLUMNS) != null ? (Integer)uifield.get(RSMetaField.WIDGET_COLUMNS) : -1);
        
        HibernateUtil.getDAOFactory().getMetaFieldPropertiesDAO().persist(metaFieldProperties);

        //dependencies defined from the UI - TO BE MOVED TO FORM PROPERTIES LEVEL
//        metaFieldProperties.setMetaxFieldDependencys(setDependenciesToMetaFieldProperties(metaFieldProperties, metaFieldModel));

        return metaFieldProperties;
    }
    
    private MetaField findMetaField(RsUIField uifield, Collection<MetaField> metaFields)
    {
        MetaField field = null;
        String sysid = uifield.getId();//get(RSMetaField.SYS_ID);
        String columnName = uifield.getName();//get(RSMetaField.NAME);

        if(metaFields != null && metaFields.size() > 0)
        {
            if(StringUtils.isNotBlank(sysid))
            {
                for(MetaField dbField : metaFields)
                {
                    if(sysid.trim().equals(dbField.getSys_id()))
                    {
                        field = dbField;
                        break;
                    }
                }
            }
            else if(StringUtils.isNotBlank(columnName))
            {
                for(MetaField dbField : metaFields)
                {
                    if(columnName.trim().equals(dbField.getUName()))
                    {
                        field = dbField;
                        break;
                    }
                }
            }
        }

        return field;
    }
    
    private MetaAccessRights setMetaAccessRights(CustomTable customTableDB, MetaField field, MetaAccessRights accessRights, RsUIField uifield)
    {
        if(accessRights == null)
        {
            accessRights = new MetaAccessRights();
        }
        String resourceName = customTableDB.getUName() + "." + field.getUColumn();

        accessRights.setUResourceId(field.getSys_id());
        accessRights.setUResourceName(resourceName);
        accessRights.setUResourceType(MetaField.RESOURCE_TYPE);
        accessRights.setUAdminAccess(uifield.getAdminRoles());//(String)rights.get(RsUIField.ADMIN_ACCESS_RIGHTS));
        accessRights.setUReadAccess(uifield.getViewRoles());//(String)rights.get(RsUIField.READ_ACCESS_RIGHTS));
        accessRights.setUWriteAccess(uifield.getEditRoles());//(String)rights.get(RsUIField.EDIT_ACCESS_RIGHTS));
        accessRights.setUExecuteAccess("admin");

        //persist it
        HibernateUtil.getDAOFactory().getMetaAccessRightsDAO().persist(accessRights);

        return accessRights;
    }
    
    private static List<RsUIField> getSysFields(String tableName, String viewRoles, String editRoles, String adminRoles)
    {
        List<RsUIField> sysFields = new ArrayList<RsUIField>();
        
        sysFields.add(createField(tableName, HibernateConstants.SYS_ID, HibernateConstantsEnum.SYS_ID_DISPLAY.getTagName(), HibernateConstantsEnum.STRING_TYPE.getTagName(), CustomFormUIType.TextField.name(), HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName(), Integer.parseInt(HibernateConstantsEnum.SMALL_40_VALUE.getTagName()), viewRoles, editRoles, adminRoles));
        sysFields.add(createField(tableName, HibernateConstants.SYS_CREATED_BY, HibernateConstantsEnum.SYS_CREATED_BY_DISPLAY.getTagName(), HibernateConstantsEnum.STRING_TYPE.getTagName(), CustomFormUIType.TextField.name(), HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName(), Integer.parseInt(HibernateConstantsEnum.SMALL_40_VALUE.getTagName()), viewRoles, editRoles, adminRoles));
        sysFields.add(createField(tableName, HibernateConstants.SYS_UPDATED_BY, HibernateConstantsEnum.SYS_UPDATED_BY_DISPLAY.getTagName(), HibernateConstantsEnum.STRING_TYPE.getTagName(), CustomFormUIType.TextField.name(), HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName(), Integer.parseInt(HibernateConstantsEnum.SMALL_40_VALUE.getTagName()), viewRoles, editRoles, adminRoles));
        
        sysFields.add(createField(tableName, HibernateConstants.SYS_CREATED_ON, HibernateConstantsEnum.SYS_CREATED_ON_DISPLAY.getTagName(), HibernateConstantsEnum.DATE_TIME_TYPE.getTagName(), CustomFormUIType.DateTime.name(), HibernateConstantsEnum.CUSTOMTABLE_TYPE_TIMESTAMP.getTagName(), 0, viewRoles, editRoles, adminRoles));
        sysFields.add(createField(tableName, HibernateConstants.SYS_UPDATED_ON, HibernateConstantsEnum.SYS_UPDATED_ON_DISPLAY.getTagName(), HibernateConstantsEnum.DATE_TIME_TYPE.getTagName(), CustomFormUIType.DateTime.name(), HibernateConstantsEnum.CUSTOMTABLE_TYPE_TIMESTAMP.getTagName(), 0, viewRoles, editRoles, adminRoles));
        
        //add the additional sys fields
        sysFields.addAll(getAdditionalSysFields(tableName, viewRoles, editRoles, adminRoles));
        
        return sysFields;
    }
    
    private static List<RsUIField> getAdditionalSysFields(String tableName, String viewRoles, String editRoles, String adminRoles)
    {
        List<RsUIField> sysFields = new ArrayList<RsUIField>();
        sysFields.add(createField(tableName, HibernateConstants.SYS_PERMISSION, HibernateConstantsEnum.SYS_PERMISSION_DISPLAY.getTagName(), HibernateConstantsEnum.STRING_TYPE.getTagName(), CustomFormUIType.TextField.name(), HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName(), Integer.parseInt(HibernateConstantsEnum.SMALL_40_VALUE.getTagName()), viewRoles, editRoles, adminRoles));
        sysFields.add(createField(tableName, HibernateConstants.SYS_ORGANIZATION, HibernateConstantsEnum.SYS_ORGANIZATION_DISPLAY.getTagName(), HibernateConstantsEnum.STRING_TYPE.getTagName(), CustomFormUIType.TextField.name(), HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName(), Integer.parseInt(HibernateConstantsEnum.SMALL_40_VALUE.getTagName()), viewRoles, editRoles, adminRoles));
        
        return sysFields;
    }
    
    private static RsUIField createField(String tableName, String name, String displayName, String utype, String uitype, String dbtype, int integerMaxValue, String viewRoles, String editRoles, String adminRoles)
    {
        RsUIField field = new RsUIField();
        
        field.setName(name);
        field.setColumnModelName(name);
        field.setDbcolumn(name);
        field.setDisplayName(displayName);
        field.setDbtable(tableName);
//        field.setRstype(utype);
        field.setUiType(uitype);
        field.setDbtype(dbtype);
        field.setIntegerMaxValue(integerMaxValue);
        
        field.setViewRoles(viewRoles);
        field.setEditRoles(editRoles);
        field.setAdminRoles(adminRoles);
        
        return field;
    }
    
    private void createConcreteTable() throws Throwable
    {
        try
        {
            CustomTableDefinition ctd =  new CustomTableDefinition(this.customTableFromUI.getUName());
            
            //populate the definition
            addColumnsToCustomTableDefinition(ctd);
            
            //update and get the element 
            customTableSchemaElement = CustomTableMappingUtil.updateTable(ctd);
            
            //save the mapping file
            CustomTableMappingUtil.saveMappingFile();
        }
        catch(Throwable t)
        {
            Log.log.info(t.getMessage(), t);
            throw t;
        }
        
    }
    
    private void addColumnsToCustomTableDefinition(CustomTableDefinition ctd)
    {
        String dbType = "";
        String uiType = "";
        String referenceTableName = "";
        
        List<RsUIField> allColumns = new ArrayList<RsUIField>();
        allColumns.addAll(existingColsExceptSysFields);
        allColumns.addAll(listOfNewColumnsAddedFromForm);
        
        for(RsUIField field : allColumns)
        {
            ColumnDefinition pd = null; 

            String fieldName = field.getName();//model.get(WorkflowConstants.META_FIELD_COLUMN.getTagName());
            dbType = field.getDbtype();//model.get(WorkflowConstants.META_FIELD_DB_TYPE.getTagName());
            uiType = field.getUiType();//model.get(WorkflowConstants.META_FIELD_UI_TYPE.getTagName());
            
            if(StringUtils.isNotBlank(fieldName))
            {
                fieldName = fieldName.startsWith(HibernateConstants.COLUMN_PREFIX) ? fieldName : HibernateConstants.COLUMN_PREFIX + fieldName;
                if(dbType.equals(Constants.CUSTOMTABLE_TYPE_STRING))
                {
                    if(StringUtils.isNotBlank(uiType) && uiType.equals(HibernateConstantsEnum.REFERENCE_TYPE.getTagName()))
                    {
                        referenceTableName = field.getReferenceTable();//model.get(WorkflowConstants.META_FIELD_REFERENCE_TABLE.getTagName());
                        if(referenceTableName != null && !referenceTableName.isEmpty())
                        {
                            String modelName = HibernateUtil.table2Class(referenceTableName);
                            if(StringUtils.isBlank(modelName))
                            {
                                modelName = referenceTableName;
                            }
                            
                           //Releation definition
                            pd = new ManyToOneDefinition(fieldName, modelName);
                        }
                    }
                    else
                    {
                        Integer maxValue = field.getStringMaxLength();//model.get(WorkflowConstants.META_FIELD_STRING_MAX_LENGTH.getTagName());

                        if(maxValue != null && maxValue != 0)
                        {
                            pd = new PropertyDefinition(fieldName, dbType, maxValue);
                        }
                        else
                        {
                            pd = new PropertyDefinition(fieldName, dbType, Integer.valueOf(HibernateConstantsEnum.LARGE_4000_VALUE.getTagName()).intValue());
                        }
                    }
                }
                else
                {
                    pd = new PropertyDefinition(fieldName, dbType);
                }
                
                //add it to the definition
                if(pd != null)
                {
//                    Log.log.debug("<<<<<<<<<<<<<<<<<         Adding :" + fieldName);
                    ctd.addColumnDefinition(pd);
                }
            }
            
            
        }
        
        
        
    }

    private void init() throws Exception
    {
        boolean isvalid = this.customTableFromUI.validate();
        if(!isvalid)
        {
            throw new RuntimeException("Following fields are mandatory to create a custom table :" + this.customTableFromUI.getErrorLog());
        }
        
        this.customTable = ServiceCustomTable.getCustomTableWithReferences(this.customTableFromUI.getSys_id(), this.customTableFromUI.getUName(), "system");
        if(StringUtils.isEmpty(this.customTableFromUI.getSys_id()) && this.customTable != null)
        {
            throw new RuntimeException(this.customTableFromUI.getUName() + " Custom Table already Exist. So aborting the create process.");
        }
        
        if(this.customTable != null)
        {
            existingColsExceptSysFields = getListOfExistingColumsInCustomTableExceptSysFields(customTable.getMetaFields());
            listOfNewColumnsAddedFromForm = getListOfNewColumnsAddedFromForm(existingColsExceptSysFields);
            if(listOfNewColumnsAddedFromForm.size() > 0)
            {
                createOrAlterConcreteTable = true;
            }
            else
            {
                //check if the new cols are there, if not, than also we need to add the new cols else skip the create process as all the columns exist
                newSysColumnsExist = doesNewSysColumnsExist(this.customTable.getMetaFields());
                if(!newSysColumnsExist)
                {
                    createOrAlterConcreteTable = true;
                }
            }
        }
        else
        {
            existingColsExceptSysFields = new ArrayList<RsUIField>();
            listOfNewColumnsAddedFromForm =  getListOfNewColumnsAddedFromForm(existingColsExceptSysFields);
            //this is a new table and needs to be created
            createOrAlterConcreteTable = true;
        }
        
    }
    
    private List<RsUIField> getListOfExistingColumsInCustomTableExceptSysFields(Collection<MetaFieldVO>  metaFields)
    {
        List<RsUIField> fields = new ArrayList<RsUIField>();

        for (MetaFieldVO metaField : metaFields)
        {
            //filter out the sys fields
            if(metaField.getUName().toLowerCase().startsWith("sys_"))
            {
                continue;
            }

            RsUIField field = ExtMetaFormLookup.convertMetaFieldToRsUIField(null, metaField, null, RightTypeEnum.admin, "admin", null, null);
            fields.add(field);
        }

        return fields;

    }
    
    @SuppressWarnings("unused")
    private List<RsUIField> getListOfNewColumnsAddedFromForm(List<RsUIField> existingColumnsInCustomTable)
    {
        List<RsUIField> newFieldsWithNoSysId = new ArrayList<RsUIField>();

        //get the list of fields from the UI
        List<RsUIField> uiFormFields = this.customTableFromUI.getFields();

        if(uiFormFields != null)
        {
            for (RsUIField newOrUpdatedFieldFromUI : uiFormFields)
            {
                String sysIdFromUI = newOrUpdatedFieldFromUI.getDbcolumnId();
                String uitype = newOrUpdatedFieldFromUI.getUiType();

                if(StringUtils.isBlank(sysIdFromUI))
                {
                    //verify if the same name exist 
                    if(uniqueFieldName(newOrUpdatedFieldFromUI, existingColumnsInCustomTable))
                    {
                        //then its a new column added
                        newFieldsWithNoSysId.add(newOrUpdatedFieldFromUI);
                    }
                    else
                    {
                        throw new RuntimeException("Field " + newOrUpdatedFieldFromUI.getName() + " already exist. Aborting table creation.");
                    }
                }
                else
                {
                    //update this field to the existing list as it may have other UI attributes to update
                    for(RsUIField existOriginal : existingColumnsInCustomTable)
                    {
                        String sysIdOriginal = existOriginal.getId();
                        if(sysIdOriginal.equalsIgnoreCase(sysIdFromUI))
                        {
                            existingColumnsInCustomTable.remove(existOriginal);
                            
                            // Check if dbColumn is same, if not indicates new db dolumn
                            if (newOrUpdatedFieldFromUI.getDbcolumn().equals(existOriginal.getDbcolumn()))
                            {
                                existingColumnsInCustomTable.add(newOrUpdatedFieldFromUI);
                            }
                            else
                            {
                                newOrUpdatedFieldFromUI.setDbcolumnId(""); // Reset DbcolumnId to treat as new field
                                newOrUpdatedFieldFromUI.setId(""); //Reset Id to treat as new field
                                //Add existing column as new column
                                newFieldsWithNoSysId.add(newOrUpdatedFieldFromUI);
                            }
                            break;
                        }
                    }
                }
            }
        }

        return newFieldsWithNoSysId;

    }
    
    private boolean uniqueFieldName(RsUIField newOrUpdatedFieldFromUI, List<RsUIField> existingColumnsInCustomTable)
    {
        boolean isUnique = true;
        
        String fieldName = newOrUpdatedFieldFromUI.getName();
        if (existingColumnsInCustomTable != null)
        {
            for (RsUIField existOriginal : existingColumnsInCustomTable)
            {
                String dbFieldName = existOriginal.getName();
                if (dbFieldName.toLowerCase().equalsIgnoreCase(fieldName.toLowerCase()))
                {
                    isUnique = false;
                    break;
                }
            }
        }
        return isUnique;
    }
    
    private boolean doesNewSysColumnsExist(Collection<MetaFieldVO> metaFields)
    {
        boolean exists = false;
        
        if(metaFields != null)
        {
            for(MetaFieldVO field : metaFields)
            {
                String name = field.getUName();
                if(name.equalsIgnoreCase(HibernateConstants.SYS_ORGANIZATION) || name.equalsIgnoreCase(HibernateConstants.SYS_PERMISSION))
                {
                    exists = true;
                    break;
                }
            }//end of for
        }//end of if
        
        return exists;
        
    }
    
    /**
     * Get list of all the fields that are of type SectionContainerField
     * Get the sectionColumn for that field
     * get the list of columns that are referenced by this section column and convert them to RsUIField 
     * add new set of RsUIField to this 
     */
//    private void updateSectionContainerFieldJson()
//    {
//        Collection<MetaFieldVO> fields = this.customTable.getMetaFields();
//        for(MetaFieldVO field : fields)
//        {
//            String uiType = field.getMetaFieldProperties().getUUIType();
//            if(uiType.equalsIgnoreCase(CustomFormUIType.SectionContainerField.name()))
//            {
//                //this is the one that we want to work on
//                updateSectionContainerField(field);
//            }
//        }
//    }
    
//    private void updateSectionContainerField(MetaFieldVO field)
//    {
//        //get the json
////        String fieldName = field.getUName();
//        MetaFieldPropertiesVO properties = field.getMetaFieldProperties();
//        String sectionColumnsJson = properties.getUSectionColumns();
//        List<RsUIField> updatedFields = new ArrayList<RsUIField>();
//
//        //update only if there is a change in json, else not required
//        boolean updateJson = updateSectionColumnJson(field, sectionColumnsJson, updatedFields);
//        
//        if(updateJson)
//        {
//            //convert the list to json string
//            sectionColumnsJson = convertObjectToJson(updatedFields);
//            
//            //save the section column info
//            properties.setUSectionColumns(sectionColumnsJson);
//            saveMetaProperties(properties);
//        }
//
//    }
    
//    private boolean updateSectionColumnJson(MetaFieldVO field, String sectionColumnsJson, List<RsUIField> updatedFields)
//    {
//        //update only if there is a change in json, else not required
//        boolean updateJson = false;
//        
//      //convert them to RsUIField list 
//        JSONArray cols = StringUtils.stringToJSONArray(sectionColumnsJson);
//        // iterate through the fields in the column an convert
//        // them to base models and add them to fields list
//        for (int i = 0; i < cols.size(); i++)
//        {
//            JSONObject col = cols.getJSONObject(i);
//            JSONArray columnFields = (JSONArray) col.get(HibernateConstants.META_FIELD_PROP_SECTION_COLUMN_CONTROLS);
//            for (int j = 0; j < columnFields.size(); j++)
//            {
//                JsonConfig jsonConfig = new JsonConfig();
//                jsonConfig.setRootClass(RsUIField.class);
//                RsUIField rf = (RsUIField) JSONSerializer.toJava(columnFields.getJSONObject(j), jsonConfig);
//                if (rf != null)
//                {
//                    String id = rf.getId();
//                    String type = rf.getSourceType(); //DB or INPUT
//                    String innerFieldName = rf.getName();
//                    
//                    if(StringUtils.isEmpty(id))
//                    {
//                        updateJson = true;
//                        
//                        //than update this
//                        rf = getUpdatedFieldInfo(field);
//                    }
//                }
//                
//                //update that in the list
//                updatedFields.add(rf);
//                
//            }//end of for loop
//        }//end of outer for loop
//        
//        return updateJson;
//    }
    
//    private RsUIField getUpdatedFieldInfo(MetaFieldVO field)
//    {
//        RsUIField dbField = ServiceHibernate.convertMetaFieldToRsUIField(null, field, null, RightTypeEnum.admin, "admin", null, null);
//        return dbField;
//    }
    
//    private void saveMetaProperties(MetaFieldPropertiesVO properties)
//    {
//        MetaFieldProperties propDb = new MetaFieldProperties(properties);
//        SaveUtil.saveMetaFieldProperties(propDb, username);
//    }
    
//    private String convertObjectToJson(List<RsUIField> updatedFields)
//    {
//        String json = JSONSerializer.toJSON(updatedFields).toString();
//        return json;
//    }
    
    
    
}
