/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.actiontask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.wiki.SaveHelper;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ActiontaskCleanupUtil
{
	private static final String UPDATE_AT_ACCESS_RUGHTS_ERR_MSG = 
									"Error while executing the update for action task accessrights %s.";
	
    public static void cleanup()
    {
        Log.log.debug("*** START OF ACTIONTASK TABLE CLEANUP ***");
        
        updateActionTaskInvoDescription();
        updateNullToRoot();
        updateMenuPathToStartWithFrontSlash();
        updateMenuPathNotToEndWithFrontSlash();
        updateAccessRightsForActiontaskNotHavingRights();
         deleteActiontasksNotHavingInvocRefs();
        
        Log.log.debug("*** END OF ACTIONTASK TABLE CLEANUP ***");
    }
    
    
    private static void deleteActiontasksNotHavingInvocRefs()
    {
        Log.log.debug("*** START OF deleteActiontasksNotHavingInvocRefs ***");
        try
        {
            Set<String> sysIds = GeneralHibernateUtil.getSysIdsFor("ResolveActionTask", "resolveActionInvoc is null OR resolveActionInvoc.sys_id NOT IN (select sys_id from ResolveActionInvoc)", "system");
            if (sysIds != null && sysIds.size() > 0)
            {
                ActionTaskUtil.deleteResolveActionTaskByIds(new ArrayList<String>(sysIds), false, "system");
            }
        }
        catch (Exception e)
        {
            Log.log.error("error while executing the delete for actiotask with no invocations", e);
        }
        
        Log.log.debug("*** END OF deleteActiontasksNotHavingInvocRefs ***");
    }
    
    public static void updateActionTaskInvoDescription()
    {
        String sql = "select resolveActionInvoc.sys_id, UFullName from ResolveActionTask where resolveActionInvoc.sys_id IN (select sys_id from ResolveActionInvoc where UDescription is null)";
        Log.log.debug("*** START OF updateActionTaskInvoDescription ***");
        
        try
        {
            List<? extends Object> badData = GeneralHibernateUtil.executeHQLSelect(sql, new HashMap<String, Object>());
            if(badData != null && badData.size() > 0)
            {
                Log.log.debug("Total # of recs to be processed : " + badData.size());
                int count = 0;
                for(Object data : badData)
                {
                    Object[] record = (Object[]) data;
                    String invocSysId = (String) record[0];
                    String name = (String) record[1];

                    String updateSql = "update ResolveActionInvoc set UDescription = '" + name + "' where sys_id = '" + invocSysId + "'";
                    try
                    {
                        GeneralHibernateUtil.executeHQLUpdate(updateSql, "system");
                    }
                    catch (Throwable e)
                    {
                        Log.log.error("error while executing the update for ResolveActionInvoc UDescription", e);
                    }
                    
                    //for logging purpose
                    count++;
                    if(count % 50 == 0)
                        Log.log.debug("Processed # of recs : " + count);
                    
                }//end of for loop
            }
            
        }
        catch (Exception e)
        {
            Log.log.error("error while updating the actiontask name in description of invoc", e);
        }
        
        Log.log.debug("*** END OF updateActionTaskInvoDescription ***");
    }
    
    //select sys_id, u_menu_path from resolve_action_task where u_menu_path is null;-- update it to '/'
    private static void updateNullToRoot()
    {
        Log.log.debug("*** START OF updateNullToRoot ***");
        
        String sql = "update ResolveActionTask set UMenuPath='/' where UMenuPath is null or UMenuPath = '' ";
        try
        {
            GeneralHibernateUtil.executeHQLUpdate(sql, "system");
        }
        catch (Exception e)
        {
            Log.log.error("error while executing the update for actiotask menupath", e);
        }
        
        Log.log.debug("*** END OF updateNullToRoot ***");
    }
    
    //select sys_id, u_menu_path from resolve_action_task where u_menu_path NOT LIKE '/%';-- check for the first char and change it to '/'
    private static void updateMenuPathToStartWithFrontSlash()
    {
        String sql = "select sys_id, UMenuPath from ResolveActionTask where UMenuPath NOT LIKE '/%'";
        Log.log.debug("*** START OF updateMenuPathToStartWithFrontSlash ***");
        
        try
        {
            List<? extends Object> badData = GeneralHibernateUtil.executeHQLSelect(sql, new HashMap<String, Object>());
            if(badData != null && badData.size() > 0)
            {
                Log.log.debug("Total # of recs to be processed : " + badData.size());
                int count = 0;
                for(Object data : badData)
                {
                    Object[] record = (Object[]) data;
                    String sysId = (String) record[0];
                    String menuPath = (String) record[1];
                    String newMenuPath = "/" + menuPath.replace('\\', '/');
                    
                    String updateSql = "update ResolveActionTask set UMenuPath = '" + newMenuPath + "' where sys_id = '" + sysId + "'";
                    try
                    {
                        GeneralHibernateUtil.executeHQLUpdate(updateSql, "system");
                    }
                    catch (Throwable e)
                    {
                        Log.log.error("error while executing the update for actiotask menupath", e);
                    }
                    
                    //for logging purpose
                    count++;
                    if(count % 50 == 0)
                        Log.log.debug("Processed # of recs : " + count);
                    
                }//end of for loop
            }
        }
        catch (Throwable e)
        {
            Log.log.error("error while executing the update for actiotask menupath", e);
        }
        
        Log.log.debug("*** END OF updateMenuPathToStartWithFrontSlash ***");
    }
    
    private static void updateMenuPathNotToEndWithFrontSlash()
    {
        String sql = "select sys_id, UMenuPath from ResolveActionTask where UMenuPath LIKE '%/'";
        Log.log.debug("*** START OF updateMenuPathNotToEndWithFrontSlash ***");
        
        try
        {
            List<? extends Object> badData = GeneralHibernateUtil.executeHQLSelect(sql, new HashMap<String, Object>());
            if(badData != null && badData.size() > 0)
            {
                Log.log.debug("Total # of recs to be processed : " + badData.size());
                int count = 0;
                for(Object data : badData)
                {
                    Object[] record = (Object[]) data;
                    String sysId = (String) record[0];
                    String menuPath = (String) record[1];
                    if(menuPath.length() > 1 && menuPath.endsWith("/"))
                    {
                        String newMenuPath = menuPath.substring(0, menuPath.length()-1);
                        
                        String updateSql = "update ResolveActionTask set UMenuPath = '" + newMenuPath + "' where sys_id = '" + sysId + "'";
                        try
                        {
                            GeneralHibernateUtil.executeHQLUpdate(updateSql, "system");
                        }
                        catch (Throwable e)
                        {
                            Log.log.error("error while executing the update for actiotask menupath", e);
                        }
                    }
                    
                    //for logging purpose
                    count++;
                    if(count % 50 == 0)
                        Log.log.debug("Processed # of recs : " + count);
                    
                }//end of for loop
            }
        }
        catch (Throwable e)
        {
            Log.log.error("error while executing the update for actiotask menupath", e);
        }
        
        Log.log.debug("*** END OF updateMenuPathNotToEndWithFrontSlash ***");
    }

    /**
     * --list of AT that does not have the access rights record and needs to be fixed, check their AccessRights flag to true 
    select sys_id, u_fullname from resolve_action_task where sys_id not in (
        select a.sys_id
        from resolve_action_task a, access_rights b
        where a.sys_id = b.u_resource_sys_id
     )
     */
    private static void updateAccessRightsForActiontaskNotHavingRights()
    {
        String whereClause = "sys_id NOT IN (" +
        		" select a.sys_id from ResolveActionTask a, AccessRights b where a.sys_id = b.UResourceId " +                        
        		") and resolveActionInvoc is not null";
        Log.log.debug("*** START OF updateAccessRightsForActiontaskNotHavingRights ***");
        try
        {
            Set<String> sysIds = GeneralHibernateUtil.getSysIdsFor("ResolveActionTask", whereClause, "system");
            if(sysIds != null && sysIds.size() > 0)
            {
                Log.log.debug("Total # of recs to be processed : " + sysIds.size());
                int count = 0;
                for(String sysId : sysIds)
                {
                    try
                    {
                        ResolveActionTaskVO vo = ServiceHibernate.getActionTaskFromIdWithReferences(sysId, null, "system");
                        if(vo != null)
                        {
                            vo.setUIsDefaultRole(true);
                            ServiceHibernate.saveResolveActionTask(vo, "system");
                        }
                    }
                    catch (Throwable e)
                    {
                    	String errMsg = e.getMessage();
                    	if (StringUtils.isBlank(errMsg)) {
                    	    errMsg = String.format("Error while executing an update of an action task with id: %s as part of creating new access rights.", sysId);
                    	}
                    	
                    	if (e != null && StringUtils.isNotBlank(e.getMessage()) && 
                    		e.getMessage().startsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_PREFIX) &&
                            (e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_SUFFIX) ||
                             e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_HA_VIOLATED_SUFFIX))) {
                        	errMsg = e.getMessage();
                        } else {
                            Log.log.warn(errMsg);
                        }
                    }
                    
                    //for logging purpose
                    count++;
                    if(count % 20 == 0)
                        Log.log.debug(String.format("Processed # of recs : %d", count));
                }//end of for loop
            }
        }
        catch(Throwable t)
        {
            Log.log.error("error while executing the update for actiotask accessrights", t);
        }
        Log.log.debug("*** END OF updateAccessRightsForActiontaskNotHavingRights ***");
    }
}
