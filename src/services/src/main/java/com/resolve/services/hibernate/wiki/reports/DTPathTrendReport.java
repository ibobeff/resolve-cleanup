/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.wiki.reports;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.services.vo.ArrayLocation;
import com.resolve.services.vo.Paginator;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.StringUtils;

/**
 * 
 * Report for 
 *      Total Path Count Trend Reports
 *      Abort Path Count Trend Reports
 *      Path Duration Trend Reports
 *       
 * @author jeet.marwah
 *
 */
public class DTPathTrendReport extends DTPathReport
{

    public DTPathTrendReport(QueryDTO query, String username) throws Exception
    {
        super(query, username);
    }

    @Override
    public ResponseDTO<Map<String, Object>> getData() throws Exception
    {
        ResponseDTO<Map<String, Object>> response = new ResponseDTO<Map<String, Object>>();
        Set<String> namespaces = new HashSet<String>();
        Set<String> dtFullNames = new HashSet<String>();
        Set<String> authors = new HashSet<String>();
        List<PathTrendReportDTO> pathCountTrendReportsDTO = new ArrayList<PathTrendReportDTO>();
        int totalRecs = 0;
        
        //prepare the data for the report
        Collection<DTGroupByPathIdDTO> paths = getRecordsByPathId(); 
        if(paths != null)
        {
            for(DTGroupByPathIdDTO path : paths)
            {
                PathTrendReportDTO dtExecution = new PathTrendReportDTO(path);  
                
                //get the first record to get the generic data
                DTRecord dtRecord = path.getRecords().get(0);
                String dtFullName = StringUtils.isNotEmpty(dtRecord.getDtRootDoc()) ? dtRecord.getDtRootDoc() : dtRecord.getNodeId();
                String namespace = dtRecord.getDtNamespace();
                String author = dtRecord.getUserId();

                //add it to the collection
                pathCountTrendReportsDTO.add(dtExecution);
                namespaces.add(namespace);
                dtFullNames.add(dtFullName);
                authors.add(author);
            }//end of for loop

            totalRecs = pathCountTrendReportsDTO.size();

            //pagination for the data
            if(start > 0 && limit > 0 && page > 0)
            {
                ArrayLocation pagination = Paginator.calculateArrayLocation(totalRecs, page, limit);
                pathCountTrendReportsDTO = pathCountTrendReportsDTO.subList(pagination.getStart(), pagination.getEnd());
            }
            
        }
        
        
        Map<String, Object> responseData = new HashMap<String, Object>();
        responseData.put("data", pathCountTrendReportsDTO);
        responseData.put("namespaces", namespaces);
        responseData.put("dtFullNames", dtFullNames);
        responseData.put("authors", authors);
        
        response.setData(responseData);
        response.setTotal(totalRecs);
        
        return response;
    }
    
    
    

}
