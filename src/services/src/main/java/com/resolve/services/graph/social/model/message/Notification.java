/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.message;


/**
 * notification is a type of post
 * 
 * @author jeet.marwah
 *
 */
public abstract class Notification extends Post
{

	private static final long serialVersionUID = -8312934537286280400L;
    
}
