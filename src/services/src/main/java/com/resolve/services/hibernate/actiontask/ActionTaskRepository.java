package com.resolve.services.hibernate.actiontask;

import org.hibernate.query.Query;

import com.resolve.persistence.dao.ResolveActionTaskDAO;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;

public enum ActionTaskRepository {

	INSTANCE;

	public String findActionTaskName(String id) throws Exception {
			return (String) HibernateProxy.execute(() -> {
			Query query = HibernateUtil.createQuery("SELECT at.UFullName FROM ResolveActionTask AS at WHERE at.sys_id = :id")
					.setParameter("id", id);
			return query.uniqueResult();
		});
	}

	public ResolveActionTask findOne(String id) throws Exception {
		return (ResolveActionTask) HibernateProxy.execute(() -> {
			ResolveActionTaskDAO actionTaskDAO = HibernateUtil.getDAOFactory().getResolveActionTaskDAO();
			return actionTaskDAO.findById(id);
		});
	}

}