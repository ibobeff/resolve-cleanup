package com.resolve.services.hibernate.util;

import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.hibernate.query.Query;

import com.resolve.persistence.model.PushGatewayFilter;
import com.resolve.persistence.model.PushGatewayFilterAttr;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.GatewayFilterVO;
import com.resolve.services.hibernate.vo.GatewayVO;
import com.resolve.services.hibernate.vo.PushGatewayFilterVO;
import com.resolve.sql.SQLConnection;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class PushGatewayFilterUtil extends GatewayFilterUtil
{
    private static final String pushGatewayFilterVOClassName = "com.resolve.services.hibernate.vo.PushGatewayFilterVO";

    public static GatewayFilterVO insertPushGatewayFilter(Map<String, String>params, 
    											String gatewayName) throws Exception {
        PushGatewayFilterVO filterVo = null;
        PushGatewayFilter pushFilter;
        try {
            String filterName = params.get("uname");
            String queueName = params.get("uqueue");
            if(StringUtils.isBlank(queueName))
                queueName = Constants.DEFAULT_GATEWAY_QUEUE_NAME;

            String username = params.get("username");
            Map<String, String> attrs;
            pushFilter = populatePushGatewayFilterFields(params, gatewayName);
            List<PushGatewayFilterAttr> attrList = new ArrayList<PushGatewayFilterAttr>();
            if(pushFilter != null) {
                attrs = findPushGatewayFilterAttributes(params);

                if(attrs != null) {
                    for(Iterator<String> it=attrs.keySet().iterator(); it.hasNext();) {
                        String attrName = it.next();
                        String value = attrs.get(attrName);

                        if (StringUtils.isNotBlank(attrName) && attrName.equals("upassword") && 
                        		StringUtils.isNotBlank(value)) {
                            try {
                                value = CryptUtils.encrypt(value);
                            } catch(Exception eee) {
                                Log.log.error(eee.getMessage(), eee);
                                throw eee;
                            }
                        }
                        attrList.add(populatePushGatewayFilterAttribute(attrName, value, username,pushFilter));
                    }
                   // pushFilter.setAttrs(attrList);
                }
            }
            
            PushGatewayFilter pushFilterFinal = pushFilter;
            pushFilter = (PushGatewayFilter) HibernateProxy.execute(() -> {
            	 PushGatewayFilter result = HibernateUtil.getDAOFactory().getPushGatewayFilterDAO().persist(pushFilterFinal);
                 if(attrList != null) {
                 	ListIterator< PushGatewayFilterAttr> lstItr = attrList.listIterator();
     	            while(lstItr.hasNext()) {
     	            	PushGatewayFilterAttr filterAttr = lstItr.next();
     	            	HibernateUtil.getDAOFactory().getPushGatewayFilterAttrDAO().persist(filterAttr);
     	            }
                 }
                 return result;
            });
           

            filterVo = pushFilter.doGetVO();
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        } 
        return filterVo;
    }
    
    /**
     * 
     * @param params
     * @param gatewayName
     * @return
     * @throws Exception
     */
    public static PushGatewayFilter populatePushGatewayFilterFields(Map<String, String>params, 
    											String gatewayName) throws Exception {

    	PushGatewayFilter pushGatewayFilter;
        try
        {
        	pushGatewayFilter = new PushGatewayFilter();
            // pullGatewayFilter.setSys_id(new Long(System.nanoTime()).toString());
        	pushGatewayFilter.setUActive(new Boolean(params.get("uactive") == null? "false" : params.get("uactive")));
        	pushGatewayFilter.setUDeployed(new Boolean(params.get("udeployed") == null ? "false" : params.get("udeployed")));
        	//pushGatewayFilter.setUDeployed(new Boolean("true")); //Should always be true because we are deploying
        	pushGatewayFilter.setUUpdated(new Boolean("true"));
        	pushGatewayFilter.setUOrder(new Integer(params.get("uorder") == null || params.get("uorder") == "" ? "0" :params.get("uorder") ));   
        	pushGatewayFilter.setUInterval(new Integer(params.get("uinterval") == null ? "10" : params.get("uinterval")));
        	pushGatewayFilter.setUName(params.get("uname"));
        	pushGatewayFilter.setUGatewayName(gatewayName);
             String queueName = params.get("uqueue");
             if(StringUtils.isBlank(queueName))
            	 pushGatewayFilter.setUQueue(Constants.DEFAULT_GATEWAY_QUEUE_NAME);
             else
            	 pushGatewayFilter.setUQueue(queueName);
             String port = "0";
             if(StringUtils.isBlank(params.get("uport"))) {
            	 pushGatewayFilter.setUPort(new Integer(port));
             }else {
            	 pushGatewayFilter.setUPort(new Integer(params.get("uport")));
             }
             
             pushGatewayFilter.setUEventEventId(params.get("ueventEventId"));
             pushGatewayFilter.setURunbook(params.get("urunbook"));
             pushGatewayFilter.setUScript(params.get("uscript"));
             pushGatewayFilter.setUUri(params.get("uuri"));
             
             pushGatewayFilter.setUBlocking(params.get("ublocking"));
//             pushGatewayFilter.setUSsl(params.get("uussl") == null? false: Boolean.parseBoolean(params.get("uussl")));
             pushGatewayFilter.setUSsl(params.get("ussl") == null? false: Boolean.parseBoolean(params.get("ussl")));
             pushGatewayFilter.setSysCreatedBy(params.get("username"));
             pushGatewayFilter.setSysCreatedOn(new Timestamp(System.currentTimeMillis()));
             pushGatewayFilter.setSys_id(null);
        } catch(Exception e) {
             Log.log.error(e.getMessage(), e);
             throw e;
        }

        return pushGatewayFilter;
    }
    // This method is called by RSView to save filter definitions or update filter
    // by RSControl to deploy/redeploy/undeploy filters in database
    public static PushGatewayFilterVO upsertPushGatewayFilter(Map<String, String>params, 
    						String gatewayName, boolean isUpdated) throws Exception {
        PushGatewayFilterVO filterVo = null;
        PushGatewayFilter pushFilter;

        String filterName = params.get("uname");
        String username = params.get("username");
        String queueName = params.get("uqueue");
        String filterId = "";
        if(filterName == null)
            throw new Exception("Filter name is not available.");
        
        if(StringUtils.isBlank(queueName)) {
            queueName = Constants.DEFAULT_GATEWAY_QUEUE_NAME;
            params.put("uqueue", queueName);
        }
        PushGatewayFilter pushGatewayFilter = populatePushGatewayFilterFields
        			(params,gatewayName);
        //Deployment means the filter has been updated with the original filter data and no further update require
        //uupdated is a flag used by the FE to indicate this deployed filter needs to be updated
        pushGatewayFilter.setUUpdated(false);
        Map<String, String> attrs;
        List<PushGatewayFilterVO> filters= getPushFiltersByName(gatewayName, filterName, queueName);
        
        if(filters.size() < 1 ) {
		        List<PushGatewayFilterAttr> attrList = new ArrayList<PushGatewayFilterAttr>();
		        if(pushGatewayFilter != null) {
		            attrs = findPushGatewayFilterAttributes(params);
		
			            if(attrs != null) {
			                for(Iterator<String> it=attrs.keySet().iterator(); it.hasNext();) {
			                    String attrName = it.next();
			                    String value = attrs.get(attrName);
			
			                    if (StringUtils.isNotBlank(attrName) && attrName.equals("upassword") && StringUtils.isNotBlank(value) 
			                            && !value.startsWith("ENC:")) {
			                        try {
			                            value = CryptUtils.encrypt(value);
			                        } catch(Exception eee) {
			                            Log.log.error(eee.getMessage(), eee);
			                            throw eee;
			                        }
			                    }
			                    attrList.add(populatePushGatewayFilterAttribute(attrName, value, username, pushGatewayFilter));
			                }
			            }
		        	}
        
		        try {	
			        	
				            
		            HibernateProxy.execute( () -> {
		            	HibernateUtil.getDAOFactory().getPushGatewayFilterDAO().persist(pushGatewayFilter);
			            if(attrList != null) {
			            	ListIterator< PushGatewayFilterAttr> lstItr = attrList.listIterator();
				            while(lstItr.hasNext()) {
				            	PushGatewayFilterAttr filterAttr = lstItr.next();
				            	 HibernateUtil.getDAOFactory().getPushGatewayFilterAttrDAO().persist(filterAttr);
				            }
			            }
			            return null;
		            });
				            
				           
			       } catch(Exception e) {
			           e.printStackTrace();
			           Log.log.error(e.getMessage(), e);
			           throw e;
			       }
        }else {
        	//filter is already deployed let's just update the updated status to false 
        	//because that's what deploy is supposed to do
        	updatePushGatewayFilterFields(params,gatewayName,false);
        }
        
        return filterVo;
    }

    private static Map<String, String> findPushGatewayFilterAttributes(Map<String, String> params) throws Exception {

        Map<String, String> attrs = new HashMap<String, String>();

        try {
            Method[] methods = Class.forName(pushGatewayFilterVOClassName).newInstance().getClass().getMethods();

            for(Iterator<String> it=params.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                if(key == null)
                    continue;
                
                boolean found= false;

                for(Method method:methods) {
                    String name = method.getName();
                    if(method.getAnnotation(MappingAnnotation.class) != null) {
                        String attr = name.substring(3);
                        if(key.equalsIgnoreCase(attr)) {
                            found = true;
                            break;
                        }
                    }
                }

                if(found) {
                    continue;
                }else if(!key.startsWith("u") || key.equals("username"))
                    continue;
                else
                    attrs.put(key,  params.get(key) == null? "": params.get(key).toString() );
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }

        return attrs;
    }

//    public static HashMap<String, String> getPushGatewayFilterAttributes(String filterId) throws Exception {
//        
//        SQLConnection conn = null;
//        PreparedStatement selectStmt = null;
//        ResultSet rs = null;
//
//        HashMap<String, String> attributes = new HashMap<String, String>();
//
//        String sql = "SELECT u_name, u_value FROM push_gateway_filter_attr WHERE upper(push_filter_id)=?";
//
//        try {
//            conn = SQL.getConnection();
//            
//            selectStmt = conn.prepareStatement(sql);
//            selectStmt.setString(1,  filterId);
//
//            rs = selectStmt.executeQuery();
//
//            while(rs.next())
//                attributes.put(rs.getString(1), rs.getString(2));
//        } catch(Exception e) {
//            Log.log.error(e.getMessage(), e);
//            throw e;
//        } finally {
//            try {
//                if(selectStmt != null)
//                    selectStmt.close();
//                if(rs != null)
//                    rs.close();
//                if(conn != null)
//                    conn.close();
//            } catch (Exception e) {
//                Log.log.error(e.getMessage(), e);
//            }
//        }
//
//        return attributes;
//    }

 @SuppressWarnings("unchecked")
public static HashMap<String, String> getPushGatewayFilterAttributes(String filterId) throws Exception {
        
        HashMap<String, String> attributes = new HashMap<String, String>();
        List<PushGatewayFilterAttr> filterAttrs = new ArrayList<PushGatewayFilterAttr>();
        PushGatewayFilterAttr filterAttr = new PushGatewayFilterAttr();
        try {

//            filterAttr.setUPushFilterId(filterId);
            PushGatewayFilter pushFilter = new PushGatewayFilter();
            pushFilter.setSys_id(filterId);
            filterAttr.setPushGatewayFilter(pushFilter);
            filterAttrs = (List<PushGatewayFilterAttr>) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getPushGatewayFilterAttrDAO().find(filterAttr);
            });
            if(filterAttrs!= null && filterAttrs.size() >= 0) {
                for(PushGatewayFilterAttr pushFilterAttr: filterAttrs) {
                    if(filterAttr != null) {
                        attributes.put(pushFilterAttr.getUName() == null ? "" :pushFilterAttr.getUName().toLowerCase(), pushFilterAttr.getUValue());
                    }
                }
            }
           
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }

        return attributes;
    }

 @SuppressWarnings("unchecked")
public static List<PushGatewayFilterAttr> getPushGatewayFilterAttributeList(String filterId) throws Exception {
     
     List<PushGatewayFilterAttr> filterAttrs = new ArrayList<PushGatewayFilterAttr>();
     PushGatewayFilterAttr filterAttr = new PushGatewayFilterAttr();
     PushGatewayFilter pushFilter = new PushGatewayFilter();
     try {
         pushFilter.setSys_id(filterId);
         filterAttr.setPushGatewayFilter(pushFilter);
         filterAttrs = (List<PushGatewayFilterAttr>) HibernateProxy.execute(() -> {
        	 return HibernateUtil.getDAOFactory().getPushGatewayFilterAttrDAO().find(filterAttr);     
         });
     } catch(Exception e) {
         Log.log.error(e.getMessage(), e);
         throw e;
     }
     return filterAttrs;
 }
//    public static List<String> getPushGatewayFilterAttributeIds(String filterId) {
//
//        SQLConnection conn = null;
//        PreparedStatement selectStmt = null;
//        ResultSet rs = null;
//
//        List<String> attributeIds = new ArrayList<String>();
//
//        String sql = "SELECT sys_id FROM push_gateway_filter_attr WHERE upper(push_filter_id)=?";
//
//        try {
//            conn = SQL.getConnection();
//
//            selectStmt = conn.prepareStatement(sql);
//            selectStmt.setString(1, filterId);
//
//            rs = selectStmt.executeQuery();
//
//            while(rs.next()) {
//                attributeIds.add(rs.getString(1));
//            }
//        } catch(Exception e) {
//            Log.log.error(e.getMessage(), e);
//        } finally {
//            try
//            {
//                if (selectStmt != null)
//                    selectStmt.close();
//                if (rs != null)
//                    rs.close();
//                if (conn != null)
//                    SQL.close(conn);
//            }
//            catch (Exception e)
//            {
//                Log.log.error(e.getMessage(), e);
//            }
//        }
//
//        return attributeIds;
//    }
    
    @SuppressWarnings("unchecked")
	public static List<String> getPushGatewayFilterAttributeIds(String filterId) {

    	 PushGatewayFilterAttr filterAttr = new PushGatewayFilterAttr();
         List<PushGatewayFilterAttr> filterAttrs = new ArrayList<PushGatewayFilterAttr>();
         List<String> attributeIds = new ArrayList<String>();
         try {
//             filterAttr.setUPushFilterId(filterId);
             PushGatewayFilter pushFilter = new PushGatewayFilter();
             pushFilter.setSys_id(filterId);
             filterAttr.setPushGatewayFilter(pushFilter);
             filterAttrs =  (List<PushGatewayFilterAttr>) HibernateProxy.execute(() -> {
            	 return HibernateUtil.getDAOFactory().getPushGatewayFilterAttrDAO().find(filterAttr);
             });
             if(filterAttrs != null && filterAttrs.size() >= 0) {
                 for(PushGatewayFilterAttr filter: filterAttrs) {
                     attributeIds.add(filter.getSys_id());
                 }
             }
         } catch(Exception e) {
             e.printStackTrace();
             Log.log.error(e.getMessage(), e);
                                             HibernateUtil.rethrowNestedTransaction(e);
         } 
        return attributeIds;
    }

    private static void insertPushGatewayFilterAttribute(String attrName, String value, String filterId, String username, SQLConnection conn) throws Exception {
        
        PreparedStatement insertStmt = null;

        String sql = "INSERT INTO push_gateway_filter_attr " +
                     "(sys_id, sys_created_by, sys_created_on, u_name, u_value, push_filter_id) " +
                     "VALUES (?, ?, ?, ?, ?, ?)";

         try {
             insertStmt = conn.prepareStatement(sql);

             insertStmt.setString(1, (new Long(System.nanoTime()).toString()));
             insertStmt.setString(2, username);
             insertStmt.setTimestamp(3, (new Timestamp(System.currentTimeMillis())));
             insertStmt.setString(4, attrName);
             insertStmt.setString(5, value);
             insertStmt.setString(6, filterId);

             insertStmt.executeUpdate();
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        } finally {
            try {
                if (insertStmt != null)
                    insertStmt.close();
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
    }

private static PushGatewayFilterAttr populatePushGatewayFilterAttribute(String attrName, String value,
		String username, PushGatewayFilter pushFilter) throws Exception {
        
		PushGatewayFilterAttr pushFilAttr = new PushGatewayFilterAttr();

         try {
        	 if(value != null) {
	             pushFilAttr.setSys_id(null);
	        	 pushFilAttr.setSysCreatedBy(username);
	        	 pushFilAttr.setSysCreatedOn(new Timestamp(System.currentTimeMillis()));
	        	 pushFilAttr.setUName(attrName);
	        	 pushFilAttr.setUValue(value);
	        	 pushFilAttr.setPushGatewayFilter(pushFilter);
        	 }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
         return pushFilAttr;
    }
    
//    
//    private static void updatePushGatewayFilterAttribute(String attrName, String value, String filterId, String username, SQLConnection conn) throws Exception {
//
//        PreparedStatement updateStmt = null;
//
//        String sql = "UPDATE push_gateway_filter_attr SET u_value=?, sys_updated_by=?, sys_updated_on=? " +
//                     "WHERE push_filter_id=? AND u_name=?";
//
//        try {
//            updateStmt = conn.prepareStatement(sql);
//
//            updateStmt.setString(1, value);
//            updateStmt.setString(2,  username);
//            updateStmt.setTimestamp(3,  (new Timestamp(System.currentTimeMillis())));
//            updateStmt.setString(4, filterId);
//            updateStmt.setString(5, attrName);
//
//            updateStmt.executeUpdate();
//        } catch(Exception e) {
//            Log.log.error(e.getMessage(), e);
//            throw e;
//        } finally {
//            try {
//                if (updateStmt != null)
//                    updateStmt.close();
//            } catch (Exception e) {
//                Log.log.error(e.getMessage(), e);
//            }
//        }
//    }

    private static void updatePushGatewayFilterAttribute(String attrName, 
    	String value, String filterId, String username) throws Exception { 
    					
    	  try {
                          
              HibernateProxy.execute( () -> {
	             Query query = HibernateUtil.getCurrentSession().createQuery("update" 
	                          + " PushGatewayFilterAttr set UValue = :uvalue, sysUpdatedBy= :sysupdatedby," 
	                          +  "  sysUpdatedOn= :sysupdatedon where UPushFilterId = :pushfilterid and UName= :uname ");
	             query.setParameter("uvalue", value);
	             query.setParameter("sysupdatedby", username);
	             query.setParameter("sysupdatedon", (new Timestamp(System.currentTimeMillis())));
	             query.setParameter("pushfilterid", filterId);
	             query.setParameter("uname", attrName);
	
	             return query.executeUpdate();
              });
             
             ;
          } catch(Exception e) {
              Log.log.error(e.getMessage(), e);
              throw e;
          } 
    }

    
    public static void updatePushGatewayFilterFields(Map<String, String>params, 
    					String gatewayName, boolean isUpdated) throws Exception {
    	try {
            
            HibernateProxy.execute(() -> {
            	Query query = HibernateUtil.getCurrentSession().createQuery("update PushGatewayFilter"
   		             + " set UActive = :uactive, UDeployed= :udeployed, UOrder = :uorder, UInterval = :uinterval," +
   		              "  UEventEventId = :ueventid, URunbook= :urunbook, UBlocking = :ublocking, UPort = :uport," +
   		              "  USsl= :ussl, UUri= :uuri, sysUpdatedBy = :updtBy, sysUpdatedOn = :updtOn, UUpdated = :isUpdated" +
   		              "  where UQueue = :uqueue and UName= :uname and UGatewayName= :ugatewayname");
   		
   		     query.setParameter("uactive", new Boolean(params.get("uactive") == null? "false": params.get("uactive")));
   		     query.setParameter("udeployed", new Boolean(params.get("udeployed") == null? "false": params.get("udeployed") ));
   		     query.setParameter("uorder", new Integer(params.get("uorder") == null? "1": params.get("uorder")));
   		     query.setParameter("uinterval",new Integer(params.get("uinterval") == null? "10": params.get("uinterval")));
   		     query.setParameter("ueventid", params.get("ueventEventId"));
   		     query.setParameter("urunbook", params.get("urunbook"));          
   		     query.setParameter("ublocking", params.get("ublocking"));
   		     query.setParameter("uport", new Integer(params.get("uport") == null ? "0": params.get("uport")));
   		     query.setParameter("ussl", new Boolean(params.get("uussl") == null ? "0": params.get("uussl")));
   		     query.setParameter("uuri", params.get("uuri"));
   		     
   		     query.setParameter("updtBy", params.get("username"));
   		     query.setParameter("updtOn", new Timestamp(System.currentTimeMillis()));
   		     if(isUpdated)
   		         query.setParameter("isUpdated", new Boolean(true));
   		     else
   		         query.setParameter("isUpdated", new Boolean(false));
   		     query.setParameter("uname",params.get("uname"));
   		     query.setParameter("ugatewayname", gatewayName);
   		     query.setParameter("uqueue", params.get("uqueue"));
   		     
   		     return query.executeUpdate();
            });
    	 
		
		 } catch(Exception e) {
		     Log.log.error(e.getMessage(), e);
		     throw e;
		 }
    }
    
    public static void updatePushGatewayFilterById(Map<String, String>params, 
			String gatewayName, boolean isUpdated) throws Exception {
		try {
			
			HibernateProxy.execute(() -> {
				Query query = HibernateUtil.getCurrentSession().createQuery("update PushGatewayFilter"
				         + " set UActive = :uactive, UDeployed= :udeployed, UOrder = :uorder, UInterval = :uinterval," +
				          "  UEventEventId = :ueventid, URunbook= :urunbook, UBlocking = :ublocking, UPort = :uport," +
				          "  USsl= :ussl, UUri= :uuri, sysUpdatedBy = :updtBy, sysUpdatedOn = :updtOn, UUpdated = :isUpdated" +
				          "  where sys_id = :sys_id and UName= :uname and UGatewayName= :ugatewayname");
				
				 query.setParameter("uactive", new Boolean(params.get("uactive") == null? "false": params.get("uactive")));
				 query.setParameter("udeployed", new Boolean(params.get("udeployed") == null? "false": params.get("udeployed") ));
				 query.setParameter("uorder", new Integer(params.get("uorder") == null? "1": params.get("uorder")));
				 query.setParameter("uinterval",new Integer(params.get("uinterval") == null? "10": params.get("uinterval")));
				 query.setParameter("ueventid", params.get("ueventEventId"));
				 query.setParameter("urunbook", params.get("urunbook"));          
				 query.setParameter("ublocking", params.get("ublocking"));
				 query.setParameter("uport", new Integer(params.get("uport") == null ? "0": params.get("uport")));
				 query.setParameter("ussl", new Boolean(params.get("uussl") == null ? "0": params.get("uussl")));
				 query.setParameter("uuri", params.get("uuri"));
				 
				 query.setParameter("updtBy", params.get("username"));
				 query.setParameter("updtOn", new Timestamp(System.currentTimeMillis()));
				 if(isUpdated)
				     query.setParameter("isUpdated", new Boolean(true));
				 else
				     query.setParameter("isUpdated", new Boolean(false));
				 query.setParameter("uname",params.get("uname"));
				 query.setParameter("ugatewayname", gatewayName);
				 query.setParameter("sys_id", params.get("id"));
				 
				 return query.executeUpdate();
			});			 
		
		} catch(Exception e) {
			 Log.log.error(e.getMessage(), e);
			 throw e;
		}
}

    public static PushGatewayFilterVO getPushFilterByName(String gatewayName, String filterName, String queueName) throws Exception {
        
        List<PushGatewayFilterVO> filters = getPushFiltersByName(gatewayName, filterName, queueName);
        if(filters.size() > 0)
            return filters.get(0);
        
        return null;
    }
    public static void updateDeployedFilterStatus(PushGatewayFilterVO pushGatewayFilterVO,
					boolean deployed, boolean updated) throws Exception {
    	PushGatewayFilter filter = new PushGatewayFilter();
    	filter.applyVOToModel(pushGatewayFilterVO);
    	updateDeployedFilterStatus(filter,deployed,updated);
    }
    
    /**
     * Update the deployed filter status so that later it can be identified as been updated
     * 
     * @param pushGatewayFilter {@link PushGatewayFilter} model record which needs to be updated
     * @param deployed : {@link Boolean} flag representing deployed status
     * @param updated : {@link Boolean} flag representing updated status.
     * @throws Exception
     */
    public static void updateDeployedFilterStatus(PushGatewayFilter pushGatewayFilter,
    						boolean deployed, boolean updated) throws Exception {
//    	PushGatewayFilter filter = new PushGatewayFilter();
    	try {
//	    	List<PushGatewayFilterVO> pushFilterDeployedVoList = getPushFiltersByName(gatewayName,
//	    			filterName, queueName);
//	    	List<PushGatewayFilterVO> pushFilterOriginalVoList = getPushFiltersByName(gatewayName,
//	    			filterName, Constants.DEFAULT_GATEWAY_QUEUE_NAME);
	    	//There should be only one
//	    	PushGatewayFilterVO originalFilter = pushFilterOriginalVoList.get(0);
//	    	for(PushGatewayFilterVO pushFilterVo: pushFilterDeployedVoList) {
//		    	filter.applyVOToModel(originalFilter); 
//		    	filter.setSys_id(pushFilterVo.getSys_id());
    			pushGatewayFilter.setUUpdated(updated);
    			pushGatewayFilter.setUDeployed(deployed);
    			
               	HibernateProxy.execute(() -> {
               		HibernateUtil.getDAOFactory().getPushGatewayFilterDAO().update(pushGatewayFilter);
                });
//	    	}
    	}catch(Exception ex) {
    		Log.log.debug("Error when updating the deployed filter status", ex);
                                  HibernateUtil.rethrowNestedTransaction(ex);
    	}
    }
    @SuppressWarnings({"unchecked", "deprecation", "rawtypes"})
    public static List<PushGatewayFilterVO> getPushFiltersByName(String gatewayName, 
    						String filterName, String queueName) throws Exception {
        
    	List<PushGatewayFilterVO> result = null;
        
        try {            
        	result = (List<PushGatewayFilterVO>) HibernateProxy.execute(() -> {
        List<PushGatewayFilterVO> filters = new ArrayList<PushGatewayFilterVO>();
        PushGatewayFilterVO filterVo = null;
        String sysId = null;
        List<PushGatewayFilter> pushFilters = new ArrayList<PushGatewayFilter>();
        PushGatewayFilter pushFilter = new PushGatewayFilter();
        	if (StringUtils.isNotBlank(filterName)) {
        		pushFilter.setUName(filterName);
        	}
            pushFilter.setUGatewayName(StringUtils.toCamelCase(gatewayName));
            if(StringUtils.isNotBlank(queueName))
                pushFilter.setUQueue(queueName);
            pushFilters =  HibernateUtil.getDAOFactory().getPushGatewayFilterDAO().find(pushFilter);
            
            if(pushFilters != null && pushFilters.size() >= 0) {
                for(PushGatewayFilter gatewayPushFilter: pushFilters) {
                	filterVo = new PushGatewayFilterVO();
                	filterVo = gatewayPushFilter.doGetVO();
                    filterVo.setChecksum(gatewayPushFilter.getChecksum());
                    sysId = gatewayPushFilter.getSys_id();
                    Collection<PushGatewayFilterAttr> attrList = gatewayPushFilter.getAttrs();
                    Map<String, String> attrMap = new HashMap<String, String>();
                    if(attrList != null) {
	                    for(PushGatewayFilterAttr attrGw: attrList) {
	                    	attrMap.put(attrGw.getUName(), attrGw.getUValue());
	                    }                    
                    }
                }
            }
                
            return filters;
        	});            
        }  catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        }
        
        
		return result;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public static PushGatewayFilterVO getPushFilterById(String id) throws Exception {
    	
        try {        	
            return (PushGatewayFilterVO) HibernateProxy.execute(() -> {
            	PushGatewayFilterVO vo = null;
                List<PushGatewayFilter> pushFilters = new ArrayList<PushGatewayFilter>();
                PushGatewayFilter pushFilter = new PushGatewayFilter();
            	pushFilter.setSys_id(id);
            	pushFilters = HibernateUtil.getDAOFactory().getPushGatewayFilterDAO().find(pushFilter);            
                if(pushFilters != null) {
                	pushFilter = pushFilters.get(0);
                	if(pushFilter != null) {
                		vo = pushFilter.doGetVO();
                	}
                	Collection<PushGatewayFilterAttr> attrs = pushFilter.getAttrs();
                    Map<String, String> pushAttrMap = new HashMap<String, String>();
                    for(PushGatewayFilterAttr attr: attrs) {
//                    	filter.getAttrs().add(attr.doGetVO());
                    	pushAttrMap.put(attr.getUName(), attr.getUValue());
                    }
                    vo.setAttrs(pushAttrMap);
                }
                
                return vo;
            });
        }  catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        }
    }
    
    public static List<GatewayVO> listPushFilters(String gatewayName, String queueName, Boolean deployed) throws Exception {

    	try {
        	
        	return (List<GatewayVO>) HibernateProxy.execute(() -> {
        		
        		List<GatewayVO> filters = new ArrayList<GatewayVO>();
                PushGatewayFilter pushGatewayFilter = new PushGatewayFilter();
        	
				pushGatewayFilter.setUGatewayName(gatewayName);
				if (deployed != null) {
					if (!deployed)
						pushGatewayFilter.setUQueue(Constants.DEFAULT_GATEWAY_QUEUE_NAME);
					else
						pushGatewayFilter.setUQueue(queueName.toUpperCase());
				}           
            	
            	List<PushGatewayFilter> pushGatewayFilters = HibernateUtil.getDAOFactory().getPushGatewayFilterDAO().find(pushGatewayFilter);                
                
                for(PushGatewayFilter pushFilter: pushGatewayFilters) {
                    
                    PushGatewayFilterVO filter = new PushGatewayFilterVO();
                    filter = pushFilter.doGetVO();
                    Collection<PushGatewayFilterAttr> attrs = pushFilter.getAttrs();
                    Map<String, String> pushAttrMap = new HashMap<String, String>();
                    for(PushGatewayFilterAttr attr: attrs) {
//                    	filter.getAttrs().add(attr.doGetVO());
                    	pushAttrMap.put(attr.getUName(), attr.getUValue());
                    }
                    filter.setAttrs(pushAttrMap);
                    filters.add(filter);
                }
                return filters;
            });
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }    

	/**
	 * 
	 * @param gatewayName
	 * @param ids
	 * @return
	 */
    public static boolean deletePushFilters(String gatewayName, String[] ids) {
    	 int num = ids.length;
         boolean result = false;
         try {                     
             for(int i=0; i<num; i++) {
//                 idList.add(ids[i]);
                 PushGatewayFilter pushFilter = new PushGatewayFilter();
                 pushFilter.setSys_id(ids[i]);
                 pushFilter.setAttrs(getPushGatewayFilterAttributeList(ids[i]));
                 result = (boolean) HibernateProxy.execute(() -> {
                	 HibernateUtil.getDAOFactory().getPushGatewayFilterDAO().delete(pushFilter);
                	 return true;
                 });
             }                        
         } catch(Exception e) {
             Log.log.error("++++++++++++++++++++++++++++++++++++++++");
             Log.log.error("Error thrown when deleting push filters, the delete may have failed");
             Log.log.error("++++++++++++++++++++++++++++++++++++++++");
             Log.log.error(e.getMessage(), e);
                                             HibernateUtil.rethrowNestedTransaction(e);
         } 
         return result;
    }
    
    public static Map<String, Map<String, Object>> loadPushGatewayFilters(String gatewayName, String queueName, Boolean deployed) throws Exception {

    	 //Response 
        Map<String, Map<String, Object>> filterMap = null;
        //hibernate query results
        List<PushGatewayFilter> results = new ArrayList<PushGatewayFilter>();
        try {
        	PushGatewayFilter gatewayFilter = new PushGatewayFilter();
            gatewayFilter.setUGatewayName(gatewayName);
            gatewayFilter.setUQueue(queueName);

            PushGatewayFilter gf = gatewayFilter;
            
            results =  (List<PushGatewayFilter>) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getPushGatewayFilterDAO().find(gf);
            });            		
            
            filterMap = new HashMap<String, Map<String, Object>>();
             Map<String, Object> filter = new HashMap<String, Object>();
             
             if(results.size() > 0) {
//                 for(PullGatewayFilter gwFilter: results) {
                     gatewayFilter = results.get(0);
                     filter.put("SYS_ID",gatewayFilter.getSys_id());
                     filter.put("ACTIVE", gatewayFilter.getUActive());
                     filter.put("DEPLOYED", gatewayFilter.getUDeployed());
                     filter.put("UPDATED", gatewayFilter.getUUpdated());
                     filter.put("QUEUE", gatewayFilter.getUQueue());
                     filter.put("ORDER", gatewayFilter.getUOrder());
                     filter.put("INTERVAL", gatewayFilter.getUInterval());
                     filter.put("ID", gatewayFilter.getUName());
                     filter.put("GATEWAY_NAME", gatewayFilter.getUGatewayName());
                     filter.put("EVENT_ID", gatewayFilter.getUEventEventId());
                     filter.put("RUNBOOK", gatewayFilter.getURunbook());
                     filter.put("SCRIPT", gatewayFilter.getUScript());
                     filter.put("BLOCKING", gatewayFilter.getUBlocking());
                     filter.put("URI", gatewayFilter.getUUri());
                     filter.put("SSL", gatewayFilter.getUSsl());
                     filter.put("PORT", gatewayFilter.getUPort());
                     filterMap.put(gatewayFilter.getUName(), filter);
//                 }
            }
        }catch(Exception ex) {
            Log.log.error(ex.getMessage(), ex);
            throw ex;
        }
        return filterMap;
    }

    
/*
    public static Integer insertPushGatewayFilter(PushGatewayFilterVO push) throws Exception {

        SQLConnection conn = null;
        PreparedStatement insertStmt = null;
        ResultSet rs = null;
        Integer id = null;

        String sql = "INSERT INTO PUSH_GATEWAY_FILTER SET (" +
                     "sys_id, u_active, u_deployed, u_order, u_interval, u_name, u_gateway_name, u_event_eventid, u_runbook, " +
                     "u_script, u_uri, u_port, u_blocking, u_ssl) " +
                     "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try
        {
            conn = SQL.getConnection();

            insertStmt = conn.prepareStatement(sql);

            insertStmt.setString(1, (new Long(System.nanoTime()).toString()));
            insertStmt.setBoolean(2, push.getUActive());
            insertStmt.setBoolean(3, push.getUDeployed());
            insertStmt.setInt(4, push.getUOrder());
            insertStmt.setInt(5, push.getUInterval());
            insertStmt.setString(6, push.getId());
            insertStmt.setString(7, push.getUGatewayName());
            insertStmt.setString(8, push.getUEventEventId());
            insertStmt.setString(9, push.getURunbook());
            insertStmt.setString(10, push.getUScript());
            insertStmt.setString(11, push.getUUri());
            insertStmt.setInt(12, push.getUPort());
            insertStmt.setString(13, push.getUBlocking());
            insertStmt.setBoolean(14, push.getUSsl());

            rs = insertStmt.executeQuery();

            if(rs.next())
                id = rs.getInt(1);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        } finally {
            try
            {
                if (insertStmt != null)
                    insertStmt.close();
                if (rs != null)
                    rs.close();
                if (conn != null)
                    SQL.close(conn);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        return id;
    }
*/
    // This method is called by RSView to update filter to refresh the updated flag
//    public static void updatePushGatewayFilter(PushGatewayFilterVO push, String filterId) throws Exception {
//
//        SQLConnection conn = null;
//        PreparedStatement updateStmt = null;
//
//        String sql = "UPDATE push_gateway_filter SET " +
//                     "U_ACTIVE=?, u_deployed=?, u_updated=?, u_order=?, u_name=?, u_gateway_name=?, u_queue=?, u_event_eventid=?, " +
//                     "u_runbook=?, u_script=?, u_uri=?, u_port=?, u_blocking=?, u_ssl=? " +
//                     "WHERE sys_id=?";
//        try {
//            conn = SQL.getConnection();
//
//            updateStmt = conn.prepareStatement(sql);
//
//            updateStmt.setBoolean(1, push.getUActive());
//            updateStmt.setBoolean(2, push.getUDeployed());
//            updateStmt.setBoolean(3, push.getUUpdated());
//            updateStmt.setInt(4, push.getUOrder());
//            updateStmt.setString(5, push.getUName());
//            updateStmt.setString(6, push.getUGatewayName());
//            updateStmt.setString(7, push.getUQueue());
//            updateStmt.setString(8, push.getUEventEventId());
//            updateStmt.setString(9, push.getURunbook());
//            updateStmt.setString(10, push.getUScript());
//            updateStmt.setString(11, push.getUUri());
//            updateStmt.setInt(12, push.getUPort());
//            updateStmt.setString(13, push.getUBlocking());
//            updateStmt.setBoolean(14, push.getUSsl());
//
//            updateStmt.setString(15, filterId);
//
//            updateStmt.executeUpdate();
//        } catch(Exception e) {
//            Log.log.error(e.getMessage(), e);
//            throw e;
//        } finally {
//            try
//            {
//                if (updateStmt != null)
//                    updateStmt.close();
//                if (conn != null)
//                    SQL.close(conn);
//            }
//            catch (Exception e)
//            {
//                Log.log.error(e.getMessage(), e);
//            }
//        }
//    }
    public static void updatePushGatewayFilter(PushGatewayFilterVO push,
    		String filterId) throws Exception {

    	 try {
             HibernateProxy.execute(() -> {
            	 Query query = HibernateUtil.getCurrentSession().createQuery("update PushGatewayFilter"
                         + " set u_active = :uactive, u_deployed= :udeployed, u_updated= :uupdated, u_order = :uorder, " +
                          " u_interval = :uinterval, u_name= :uname, u_gateway_name= :ugatewayname, u_queue = :uqueue," +
                          " u_event_eventid = :ueventid, u_runbook= :urunbook, u_blocking= :ublocking, u_uri = :uuri," +
                          " u_port = :uport, u_ssl= :ussl, u_script= :uscript where sys_id = :sysid");

                query.setParameter("uactive", push.getUActive());
                query.setParameter("udeployed", push.getUDeployed());
                query.setParameter("uupdated", push.getUUpdated());
                query.setParameter("uorder", push.getUOrder());
                query.setParameter("uinterval", push.getUInterval());
                query.setParameter("uname", push.getUName());
                query.setParameter("ugatewayname", push.getUGatewayName());
                query.setParameter("uqueue", push.getUQueue());
                query.setParameter("ueventid", push.getUEventEventId());
                query.setParameter("urunbook", push.getURunbook());
                query.setParameter("uscript", push.getUScript());
                query.setParameter("ublocking", push.getUBlocking());
                query.setParameter("uuri", push.getUUri());
                query.setParameter("uport", push.getUPort());
                query.setParameter("ussl", push.getUSsl());
                query.setParameter("sysid", filterId);
                return query.executeUpdate();
                
             });
             
         } catch(Exception e) {
             Log.log.error(e.getMessage(), e);
             throw e;
         }
    }

    @SuppressWarnings("unchecked")
	public static String getPushGatewayFilterId(PushGatewayFilter 
		 passedPushGateway) throws Exception { 

		 try {
	         List<PushGatewayFilter> gatewayFilters = new ArrayList<PushGatewayFilter>();
	         gatewayFilters =  (List<PushGatewayFilter>) HibernateProxy.execute(() -> {
	        	return HibernateUtil.getDAOFactory().getPushGatewayFilterDAO().find(passedPushGateway);
	         });
	             if(gatewayFilters != null && gatewayFilters.size() >= 0) {
	                 for(PushGatewayFilter pullGatewayFilter: gatewayFilters) {
	                     return pullGatewayFilter.getSys_id();
	                 }
	             }
	     	}catch(Exception ex) {
	             Log.log.error(ex.getMessage(), ex);
	             throw ex;
	     	}
		 return null;
    }
 
    public static List<? extends GatewayVO> getPushFiltersForQueue(String gatewayName, String queueName) throws Exception {
     
    	return getPushFiltersByName(gatewayName, null, queueName);
    }
} // class PushGatewayFilterUtil
