/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.Map;

import com.resolve.graph.social.constants.NonNeo4jRelationType;
import com.resolve.graph.social.constants.SocialRelationshipTypes;
import com.resolve.services.catalog.Catalog;
import com.resolve.util.StringUtils;

/**
 * This will be deleted ...use the one in package com.resolve.services.social.impex
 * 
 * used to represent a relationship in graph 
 * 
 * @author jeet.marwah
 *
 */
@Deprecated
public class GraphRelationshipDTO implements Comparable<GraphRelationshipDTO>
{
    
    //start of the relation node
    private SocialRelationshipTypes sourceType;
    private String sourceName;
    
    //end of the relation node
    private SocialRelationshipTypes targetType;
    private String targetName;
    
    //presently used for doc and catalog relationship 
    private Catalog targetObject;
    
    //relation type between the nodes , eg. MEMBER, FAVORITE, etc
    private NonNeo4jRelationType relationType;
    private Map<String, String> relProperty;
    
    //to decide if to create stubs/placeholder on the target system or not, valid values are - IGNORE, CREATE
    //later on we can enhance to DELETE_AND_CREATE
    private String mode = "IGNORE";
    
    public boolean validate() throws Exception
    {
        boolean valid = true;
        
        if(StringUtils.isEmpty(sourceName) || sourceType == null)
        {
            throw new Exception("Source name or type is not available. They both are mandatory");
        }
        
        if(StringUtils.isEmpty(targetName) || targetType == null)
        {
            throw new Exception("Target name or type is not available. They both are mandatory");
        }

        return valid;
    }
    
    public SocialRelationshipTypes getSourceType()
    {
        return sourceType;
    }
    public void setSourceType(SocialRelationshipTypes sourceType)
    {
        this.sourceType = sourceType;
    }
    public String getSourceName()
    {
        return sourceName;
    }
    public void setSourceName(String sourceName)
    {
        this.sourceName = sourceName;
    }
    public SocialRelationshipTypes getTargetType()
    {
        return targetType;
    }
    public void setTargetType(SocialRelationshipTypes targetType)
    {
        this.targetType = targetType;
    }
    public String getTargetName()
    {
        return targetName;
    }
    public void setTargetName(String targetName)
    {
        this.targetName = targetName;
    }
    
    public NonNeo4jRelationType getRelationType()
    {
        return relationType;
    }
    public void setRelationType(NonNeo4jRelationType relationType)
    {
        this.relationType = relationType;
    }
    public Map<String, String> getRelProperty()
    {
        return relProperty;
    }
    public void setRelProperty(Map<String, String> relProperty)
    {
        this.relProperty = relProperty;
    }
    public Catalog getTargetObject()
    {
        return targetObject;
    }
    public void setTargetObject(Catalog targetObject)
    {
        this.targetObject = targetObject;
    }
    public String getMode()
    {
        return mode;
    }
    public void setMode(String mode)
    {
        this.mode = mode;
    }

    @Override
    public int hashCode()
    {
        String strForHashcode = getSourceName()+ "-" + getSourceType().name()+ "-" + getTargetName() + "-" + getTargetType().name();
        int hash = 31 + strForHashcode.hashCode();
        return hash;
    }
    
    
    public int compareTo(GraphRelationshipDTO obj)
    {
        int result = 0;
        
        if(obj.getSourceName() != null && this.getSourceName() != null && !obj.getSourceName().equalsIgnoreCase(getSourceName()))
        {
            result = getSourceName().compareTo(obj.getSourceName());
        }
        else if(obj.getSourceType() != null && this.getSourceType() != null && !obj.getSourceType().equals(getSourceType()))
        {
            result = getSourceType().compareTo(obj.getSourceType());
        }
        else if(obj.getTargetName() != null && this.getTargetName() != null && !obj.getTargetName().equalsIgnoreCase(getTargetName()))
        {
            result = getTargetName().compareTo(obj.getTargetName());
        }
        else if(obj.getTargetType() != null && this.getTargetType() != null)
        {
            result = getTargetType().compareTo(obj.getTargetType());
        }
        
        return result;
    }

    
    public boolean equals(Object otherObj)
    {
        boolean isEquals = false;
        
        if (otherObj instanceof GraphRelationshipDTO)
        {
            GraphRelationshipDTO otherTask = (GraphRelationshipDTO) otherObj;

            isEquals = otherTask.getSourceName().equals(this.getSourceName()) 
                            && otherTask.getSourceType().equals(this.getSourceType()) 
                            && otherTask.getTargetName().equals(this.getTargetName()) 
                            && otherTask.getTargetType().equals(this.getTargetType());
        }
        
        return isEquals;

    }
    

}
