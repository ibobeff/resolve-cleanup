/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import com.resolve.persistence.model.ArchiveWorksheet;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ArchiveWorksheetVO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ArchiveUtil
{
    
    public static ArchiveWorksheetVO findByExample(ArchiveWorksheetVO example)
    {
        ArchiveWorksheet worksheet = null;
        ArchiveWorksheetVO result = null;
        
        if (example != null)
        {
            try
            {
				worksheet = (ArchiveWorksheet) HibernateProxy.execute(() -> {
					ArchiveWorksheet searchedWorksheet;
					if (StringUtils.isNotBlank(example.getSys_id())) {
						searchedWorksheet = HibernateUtil.getDAOFactory().getArchiveWorksheetDAO()
								.findById(example.getSys_id());
					} else {
						searchedWorksheet = new ArchiveWorksheet(example);
						searchedWorksheet = HibernateUtil.getDAOFactory().getArchiveWorksheetDAO().findFirst(searchedWorksheet);
					}

					return searchedWorksheet;
				});
            }
            catch (Exception t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
            
            if(worksheet != null)
            {
                result = worksheet.doGetVO();
            }
        }
        
        return result;
    }

}
