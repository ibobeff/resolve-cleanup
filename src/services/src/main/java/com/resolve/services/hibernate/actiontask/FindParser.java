/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.actiontask;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.query.Query;

import com.resolve.persistence.model.ResolveActionInvoc;
import com.resolve.persistence.model.ResolveParser;
import com.resolve.persistence.model.ResolveParserRel;
import com.resolve.persistence.util.DBCacheRegionConstants;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.ParserUtil;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveParserVO;
import com.resolve.services.vo.ATReferenceDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class FindParser
{
    private ResolveParserVO vo = null;
    private ResolveParser model = null;

    private String sysId = null;
    private String name = null;
    private String username = null;
    
    private Set<String> invocSysIds = new HashSet<String>();
    
    public FindParser(String sysId, String name, String username) throws Exception
    {
        if(StringUtils.isEmpty(sysId) && StringUtils.isEmpty(name))
        {
            throw new Exception("sysId or name of the assessor is mandatory");
        }
        
        this.sysId = sysId;
        this.name = name;
        this.username = username;
    }
    

    public ResolveParserVO getWithoutRefs() throws Exception
    {
        //load the object first
        lookupParser();
        
        if (model != null)
        {
            //prepare the vo
            vo = model.doGetVO();
        }
        return vo;
    }

    
    public ResolveParserVO get() throws Exception
    {
        //load the object first
        lookupParser();
        
        if (model != null)
        {
            //prepare the vo
            vo = model.doGetVO();
            
            //prepare the actiontask references
            prepareActionTaskReferences();
            
            //prepare the assessor refs in 
            prepareParserReferencing();
            
            //prepare the assessor refs by :
            findParsersReferencedBy();
        }
        
        return vo;
    }
    
    private void prepareActionTaskReferences() throws Exception
    {
        Collection<ResolveActionTaskVO> ats = ActionTaskUtil.findResolveActionTaskBasedOnInvoc(invocSysIds, username);
        if(ats.size() > 0)
        {
            for(ResolveActionTaskVO at : ats)
            {
                ATReferenceDTO ref = new ATReferenceDTO();
                ref.setDescription(at.getUSummary());
                ref.setName(at.getUFullName());
                ref.setType("Actiontask");
                ref.setRefInOrBy("Referenced By");
                
                //add the ref
                vo.getReferences().add(ref);
            }
            
        }
    }
    
    
    private void prepareParserReferencing() throws Exception
    {
        Set<String> assessorsReferenced = vo.getRefParsers();
        if(assessorsReferenced.size() > 0)
        {
            Collection<ResolveParserVO> parsers = ParserUtil.findResolveParserByNames(assessorsReferenced, username);
            for(ResolveParserVO parser : parsers)
            {
                ATReferenceDTO ref = new ATReferenceDTO();
                ref.setDescription(parser.getUDescription());
                ref.setName(parser.getUName());
                ref.setType("Parser");
                ref.setRefInOrBy("Referencing");
                
                //add the ref
                vo.getReferences().add(ref);
            }
        }
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void findParsersReferencedBy()
    {
        String parserName = vo.getUName();
        String sql = "select a from ResolveParserRel a where LOWER(a.URefParser) = '" + parserName + "' ";

        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            Query query = HibernateUtil.createQuery(sql);
	            List<ResolveParserRel> list = query.list();
	            if (list != null && list.size() > 0)
	            {
	                for(ResolveParserRel rel : list)
	                {
	                    ResolveParser parser = rel.getParser();
	                    
	                    ATReferenceDTO ref = new ATReferenceDTO();
	                    ref.setDescription(parser.getUDescription());
	                    ref.setName(parser.getUName());
	                    ref.setType("Parser");
	                    ref.setRefInOrBy("Referenced By");
	                    
	                    //add the ref
	                    vo.getReferences().add(ref);
	                }
	            }

            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }
    
    private void lookupParser() throws Exception
    {

        if(StringUtils.isNotBlank(sysId) && HibernateUtil.getCachedDBObject(DBCacheRegionConstants.PARSER_CACHE_REGION, sysId) != null) {
            model = (ResolveParser) HibernateUtil.getCachedDBObject(DBCacheRegionConstants.PARSER_CACHE_REGION, sysId);
            return;
        }
        
        try
        {

          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
                if (StringUtils.isNotBlank(sysId))
                {
                    model = HibernateUtil.getDAOFactory().getResolveParserDAO().findById(sysId);
                }
                else if (StringUtils.isNotBlank(name))
                {
                    //to make this case-insensitive
                    //String sql = "select a from ResolveParser a where LOWER(a.UName) = '" + name.toLowerCase().trim() + "'";
                    String sql = "select a from ResolveParser a where LOWER(a.UName) = :UName";
                    
                    Map<String, Object> queryParams = new HashMap<String, Object>();
                    
                    queryParams.put("UName", name.trim().toLowerCase());
                    
                    List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                    if (list != null && list.size() > 0)
                    {
                        model = (ResolveParser) list.get(0);
                    }
                }
                
                if(model != null)
                {
                    //load the references
                    if(model.getResolveParserRels() != null)
                    {
                        model.getResolveParserRels().size();
                    }
                    
                    if(model.getResolveActionInvocs() != null)
                    {
                        for(ResolveActionInvoc invoc : model.getResolveActionInvocs())
                        {
                            invocSysIds.add(invoc.getSys_id());
                        }
                    }
                    
                    if (StringUtils.isNotBlank(model.getSys_id())) 
                    {
                      HibernateUtil.cacheDBObject(DBCacheRegionConstants.PARSER_CACHE_REGION, new String[] { "com.resolve.persistence.model.ResolveParser"}, model.getSys_id(), model);
                    }
                }
        	});
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
    }
    
}
