/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.form;

/**
 * base model class for the ui widgets/comps
 * 
 * @author jeet.marwah
 *
 */
public abstract class RsUIComponent
{
    private String id;
    private String xtype;
    private String name;
    private String displayName;
    private int orderNumber = -1;
    private boolean isMandatory = false;
    private boolean isEncrypted = false;
    private boolean isHidden = false;
    private boolean isReadOnly = false;
    
    //this is instructions for the user for the popup or help
    private String helptext;
    
    public String getId()
    {
        return id;
    }
    public void setId(String id)
    {
        this.id = id;
    }
    public String getXtype()
    {
        return xtype;
    }
    public void setXtype(String xtype)
    {
        this.xtype = xtype;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getDisplayName()
    {
        return displayName;
    }
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }
    public int getOrderNumber()
    {
        return orderNumber;
    }
    public void setOrderNumber(int orderNumber)
    {
        this.orderNumber = orderNumber;
    }
    public String getHelptext()
    {
        return helptext;
    }
    public void setHelptext(String helptext)
    {
        this.helptext = helptext;
    }
    public boolean isMandatory()
    {
        return isMandatory;
    }
    public void setMandatory(boolean isMandatory)
    {
        this.isMandatory = isMandatory;
    }
    public boolean isEncrypted()
    {
        return isEncrypted;
    }
    public void setEncrypted(boolean isEncrypted)
    {
        this.isEncrypted = isEncrypted;
    }
    public boolean isHidden()
    {
        return isHidden;
    }
    public void setHidden(boolean isHidden)
    {
        this.isHidden = isHidden;
    }
    public boolean isReadOnly()
    {
        return isReadOnly;
    }
    public void setReadOnly(boolean isReadOnly)
    {
        this.isReadOnly = isReadOnly;
    }

    
    
    
}
