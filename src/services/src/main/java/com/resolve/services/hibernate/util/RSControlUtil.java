/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import com.resolve.persistence.model.ResolveEventHandler;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ResolveEventHandlerVO;
import com.resolve.util.Log;

public class RSControlUtil
{
    public static ResolveEventHandlerVO getResolveEventHandler(String name, String type)
    {
        ResolveEventHandler handler = null;
        ResolveEventHandlerVO result = null;

        try
        {
        	handler = (ResolveEventHandler) HibernateProxy.execute(() -> {
            	ResolveEventHandler query = new ResolveEventHandler();
                query.setUName(name);
                query.setUType(type);
                return HibernateUtil.getDAOFactory().getResolveEventHandlerDAO().findFirst(query);
            });           
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        if(handler != null)
        {
            result = handler.doGetVO();
        }

        return result;
    } // getGenericHandler
}
