/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.model.SocialPost;
import com.resolve.search.model.SocialPostResponse;
import com.resolve.search.model.UserInfo;
import com.resolve.search.social.SocialIndexAPI;
import com.resolve.search.social.SocialSearchAPI;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.ComponentNotification;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.hibernate.util.GraphUtil;
import com.resolve.services.hibernate.util.SocialAdminUtil;
import com.resolve.services.hibernate.util.SocialNotificationUtil;
import com.resolve.services.hibernate.util.SocialUtil;
import com.resolve.services.hibernate.util.vo.ComboboxModelDTO;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.migration.social.NonNeo4jRelationType;
import com.resolve.services.social.follow.SearchUserStreamsUtil;
import com.resolve.services.social.follow.TeamTreeGraph;
import com.resolve.services.social.notification.SearchUserStreamsForDigest;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.UserStatDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.social.LookupComponent;
import com.resolve.social.PostUtil;
import com.resolve.social.SocialComponentSearch;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ServiceSocial
{
    
    /**
    * Searches posts by {@link QueryDTO}.
    *
    * @param query
    * @param user
    * @return
    * @throws Exception
    */
    public static ResponseDTO<SocialPostResponse> searchPosts(QueryDTO query, String streamId, NodeType streamType, boolean includeComments, boolean unreadOnly, User user) throws Exception
    {
        try
        {
            return PostUtil.searchPosts(query, streamId, streamType.name().toLowerCase(), includeComments, unreadOnly, user);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }  
    
    
    public static ResponseDTO<SocialPostResponse> advancedSearch(QueryDTO query, User user) throws Exception
    {
        try
        {
            return PostUtil.advancedSearch(query, user);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }    
    
    public static void post(String subject, String content, String username, List<String> targetsSysId)
    {
        post(subject, content, username, targetsSysId, false);
    }

    public static void post(String subject, String content, String username, List<String> targetsSysId, boolean parseTag)
    {
        new PostUtil().post(subject, content, username, targetsSysId, null, parseTag);
    }    
    
    /**
     * This API return return a list of all streams that the current user
     * following or join for each stream -- have total unread count of post and
     * last activity date
     *
     * @return
     */
    public static List<RSComponent> getUserStreams(String userSysId, String username, boolean recurse) throws Exception
    {
        SearchUserStreamsUtil userStreams = new SearchUserStreamsUtil(userSysId, username);
        userStreams.setRecurse(recurse);
        
        return new ArrayList<RSComponent>(userStreams.getUserStreams());
    }

    /**
     * This API return returns a list of small streams that the current user is
     * following or join for each stream -- have total unread count of post and
     * last activity date
     *
     * @return
     */
    public static List<RSComponent> getSmallUserStreams(String userSysId, String username, boolean recurse) throws Exception
    {
        SearchUserStreamsUtil userStreams = new SearchUserStreamsUtil(userSysId, username);
        userStreams.setRecurse(recurse);
        
        return new ArrayList<RSComponent>(userStreams.getSmallUserStreams());
    }
    
    public static User findUserByUsername(String username)
    {
        return SocialCompConversionUtil.getSocialUser(username);
    }
    
    

    /**
     *
     * If the component is to be added to Home or not. Similar to addToHome
     *
     * Make sure that the user is Following this comp before pinning/unpinning
     * it
     *
     *
     * @param user
     * @param id
     *            --> comp sysId
     * @param pinIt
     * @throws Exception
     */
    public static void setPinUnPin(User user, String id, boolean pinIt) throws Exception
    {
        if (StringUtils.isNotEmpty(id))
        {
            ResolveNodeVO node = ServiceGraph.findNode(null, id, null, null, "system");
            if(node != null)
            {
                node.setUPinned(pinIt);
                ServiceGraph.persistNode(node, "system");
            }
            else
            {
                throw new Exception("Node with sysId " + id + " does not exist.");
            }
        }
    }
    
    /**
     * This API return a list of all the streams (like Runbook,
     * Document, ActionTask ..) in the system that the user has access to, user
     * can search for join and post to.
     *
     * The list will filter out by QUeryDTO
     *
     * QueryDTO -- including the pagination
     *
     * @param query
     * @return
     */
    public static ResponseDTO<RSComponent> getAllStreams(User user, QueryDTO query)
    {
        // For each component, we will have to save 'type' for this.
        // So there is a grid that user can search all the data from graph and
        // the ones he can follow
        // with this api we are saying, 'give me all the components that this
        // user has access rights to Follow'
        // there will be pagination information in the query DTO

        // Note : for 3.6, we will creating a lucene DB that will be sync with
        // the SQL DB and than this query will be targeted to lucene which will
        // be faster and can be generic enough to
        // search all the components at one shot.
        ResponseDTO<RSComponent> result = new SocialComponentSearch(user.getName(), query).getStreams();
        return result;
    }

    
    /**
     * This api should search all the components with this 'id' and should have
     * view rights. Need to find if user is following OR to return even if he is
     * not following and has rights to it
     *
     *
     * @param id
     * @param username
     * @return
     */
    public static RSComponent getComponentById(String id, String username)
    {
        return LookupComponent.getRSComponentById(id);
    }
    
    public static Map<String, Long> getUnreadPostCount(QueryDTO queryDTO, User user, List<RSComponent> userStreams)
    {
        return PostUtil.getUnreadPostCount(queryDTO, user, userStreams);
    }
    

    
    /**
     * Get the list of people who like the provided post/comment by id
     *
     * @param id
     * @return
     * @throws Exception
     */
    public static List<RSComponent> getLikes(String id, String username) throws Exception
    {
        return PostUtil.getUsersWhoLikedThis(id, username);
    }
    
    

    /**
     * The idea here is to dynamically generate the list of component types like
     * Actiontask, Runbook, etc. Hardcoding here - will be changing to lookup
     * table of types when we make Dynamic types enabled in 3.6 or above
     *
     * @return
     */
    public static List<ComboboxModelDTO> getListOfAllComponentTypes()
    {
        List<ComboboxModelDTO> result = new ArrayList<ComboboxModelDTO>();

        result.add(new ComboboxModelDTO("ActionTask", NodeType.ACTIONTASK.name()));
        result.add(new ComboboxModelDTO("DecisionTree", NodeType.DECISIONTREE.name()));
        result.add(new ComboboxModelDTO("Document", NodeType.DOCUMENT.name()));
        result.add(new ComboboxModelDTO("Forum", NodeType.FORUM.name()));
        result.add(new ComboboxModelDTO("NameSpace", NodeType.NAMESPACE.name()));
        result.add(new ComboboxModelDTO("Process", NodeType.PROCESS.name()));
        result.add(new ComboboxModelDTO("Rss", NodeType.RSS.name()));
        result.add(new ComboboxModelDTO("Runbook", NodeType.RUNBOOK.name()));
        result.add(new ComboboxModelDTO("Team", NodeType.TEAM.name()));
        result.add(new ComboboxModelDTO("User", NodeType.USER.name()));
        result.add(new ComboboxModelDTO("WorkSheet", NodeType.WORKSHEET.name()));

        // sort the list
        Collections.sort(result);

        return result;

    }
    

    public static SocialPostResponse getPostById(User user, String postid) throws SearchException
    {
        SocialPostResponse result = null;
        try
        {
            result = SocialSearchAPI.findSocialPostResponseById(postid, new UserInfo(user.getSys_id(), "", user.getName()));
            if(result == null || result.isDeleted())
            {
                throw new SearchException("Failed retrieving already deleted post or post not found");
            }
        }
        catch (SearchException e)
        {
            Log.log.warn(e.getMessage());
            throw e;
        }
        return result;
    }
    
    public static void addComment(String username, String postId, String commentStr, boolean parseTag) throws Exception
    {
        new PostUtil().addComment(username, postId, commentStr, parseTag);
    }
    
    @Deprecated
    public static RSComponent createWorksheetNode(String sysId, String username) throws Exception
    {
        return SocialUtil.createWorksheetNode(sysId, username, null, null);
    }
    
    public static RSComponent createWorksheetNode(String sysId, String username, String orgId, String orgName) throws Exception
    {
        return SocialUtil.createWorksheetNode(sysId, username, orgId, orgName);
    }
    
    /**
     *
     * If the stream/comp is set to LOCKED, than no Post or Comments can be
     * added to that stream.
     *
     * @param user
     *            --> user who wants to lock this component
     * @param id
     *            --> component Id
     * @param locked
     *            --> true or false
     * @throws Exception
     */
    public static void setStreamLockUnlock(User user, String id, boolean locked) throws Exception
    {
        SocialAdminUtil.setStreamLockUnlock(user, id, locked);
    }    
    
    

    /**
     * User can lock the Post/Comment
     *
     * @param user
     *            --> user who wants to lock this Post
     * @param id
     *            --> Postid
     * @param locked
     *            --> true or false
     */
    public static void setPostLock(User user, String id, boolean locked) throws Exception
    {
        PostUtil.setLock(user, id, locked);
    }
    


    /**
     * only switch the post targets to the new targets anything others didn't
     * change
     *
     * @param id
     * @param newTargets
     */
    public static void movePost(String postId, User user, List<String> newTargetIds) throws Exception
    {
        PostUtil.movePost(postId, user, newTargetIds);
    }
    



    /**
     * If user delete the post, also delete all comments for the post.
     *
     * @param user
     * @param id
     */
    public static void deletePost(User user, String id) throws Exception
    {
        PostUtil.deletePost(user, id);
    }
    
    
    /**
     * Deletes social comment.
     *
     * @param user
     * @param id
     */
    public static void deleteComment(User user, String id) throws Exception
    {
        PostUtil.deleteComment(user, id);
    }
    
    /**
     * For this user, make it so that the user has "read" each of the posts in
     * the stream
     *
     * @param user
     * @param id
     */
    public static void markPostAsReadByQuery(String streamId, String streamType, QueryDTO query, String username, boolean isRead) throws Exception
    {
        PostUtil.markPostAsReadByQuery(streamId, streamType, query, username, isRead);
    }
    
    /**
     * User can make the post/comment as read
     *
     * @param id
     * @param read
     */
    public static void setRead(User user, String id, boolean read) throws Exception
    {
        SocialIndexAPI.setRead(id, user.getName(), read);
    }
    



    /**
     * User can make the Post/comment starred
     *
     * @param id
     * @param starred
     */
    public static void setStarred(User user, String id, boolean starred) throws Exception
    {
        SocialIndexAPI.setStarred(id, user.getName(), starred);
    }
    

    /**
     * User is marking a Post as an Answer.
     *
     * @param user
     * @param commentId
     * @param answer
     */
    public static void setAnswer(User user, String commentId, boolean isAnswer) throws Exception
    {
        SocialIndexAPI.setAnswer(commentId, user.getName(), isAnswer);
    }
    


    /**
     * Set the streams provided in the list to be followed by the user or
     * unfollowed based on the follow parameter
     *
     * @param user
     * @param ids
     * @param follow
     * @throws Exception
     */
    public static void setFollowStreams(User user, List<String> ids, boolean follow) throws Exception
    {
        SocialUtil.setFollowStreams(user, ids, follow);
    }
    
    public static List<User> getAllUsersFollowingThisStream(String streamId, String username) throws Exception
    {
        return SocialUtil.getAllUsersFollowingThisStream(streamId, username);
    }

    public static List<User> getListOfUsersThisUserIsFollowing(User user)  throws Exception
    {
        return SocialUtil.getListOfUsersThisUserIsFollowing(user);
    }
    
    public static UserStatDTO getUserStats(String username, boolean countStats, boolean unreadStats) throws Exception
    {
        return SocialSearchAPI.getUserStats(username);
    }
    
    
    public static Set<User> getUsersForForwardEmail(RSComponent component, String username) throws Exception
    {
        return SocialNotificationUtil.getUsersForForwardEmail(component, username);
    }
    
    
    public static ComponentNotification getSpecificNotificationFor(String streamId, User user) throws Exception
    {
        return SocialNotificationUtil.getSpecificNotificationFor(streamId, user);
    }
    
    public static List<ComponentNotification> getGlobalNotificationsForUser(User user) throws Exception
    {
        return SocialNotificationUtil.getGlobalNotificationsForUser(user);
    }
    
    
    public static void updateGlobalNotificationsForUser(User user, Set<String> selectedNotificationTypes) throws Exception
    {
        SocialNotificationUtil.updateGlobalNotificationsForUser(user, selectedNotificationTypes);
    }
    
    
    public static void updateSpecificNotificationTypeFor(String streamId, User user, UserGlobalNotificationContainerType type, boolean value) throws Exception
    {
        SocialNotificationUtil.updateSpecificNotificationTypeFor(streamId, user, type, value);
    }
    
    public static void setFollowStreams(List<String> userIds, List<String> streamIds, boolean follow) throws Exception
    {
        if (userIds != null && streamIds != null)
        {
            StringBuilder errors = new StringBuilder();
            
            for (String userId : userIds)
            {
                User user = SocialCompConversionUtil.getSocialUserById(userId);

                try
                {
                    setFollowStreams(user, streamIds, follow);
                }
                catch (Exception e)
                {
                    errors.append(e.getMessage()).append("\n");
                    Log.log.error("Error for user following the streams:", e);
                }
            }
            
            //if there are any errors, throw it
            if(errors.length() > 0)
            {
                throw new Exception(errors.toString());
            }
        }
    }
    
    public static List<RSComponent> findUsersRegisteredToReceiveThisNotificationForComp(String compSysId, UserGlobalNotificationContainerType notificationType) throws Exception
    {
        return SocialNotificationUtil.findUsersRegisteredToReceiveThisNotificationForComp(compSysId, notificationType);
    }

    public static Team getTeam(String teamID, boolean recurse) throws Exception
    {
        TeamTreeGraph teamGraph = new TeamTreeGraph(teamID, recurse, "system");
        return teamGraph.getTeam();
    }
    
    public static Set<User> findUsersBelongingToTeamAndItsChildTeams(String teamId, boolean child)
    {
        return SocialNotificationUtil.findUsersBelongingToTeamAndItsChildTeams(teamId, child);
    }
    
    public static Map<User, List<RSComponent>> getUserStreamsForDigest() throws Exception
    {
        return new SearchUserStreamsForDigest().getUserStreamsForDigest();
    }
    
    public static void bulkInsertOrUpdateDocumentGraphNodes(Collection<Document> documents, String username) throws Exception
    {
        SocialNotificationUtil.bulkInsertOrUpdateDocumentGraphNodes(documents, username);
    }

    public static Set<String> getComponentIdsBelongingToContainer(RSComponent targetComponent) throws Exception
    {
        return SocialNotificationUtil.getComponentIdsBelongingToContainer(targetComponent);        
    }

    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // ///////////////////////////////////// TODO: ELASTIC SEARCH RELATED APIS
    // /////////////////////////////////////////////
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Posts system message (e.g., forum update etc.)
     *
     * @param subject
     * @param content
     * @param username
     * @param targetsSysId
     * @param postType
     */
    public static void postSystemMessage(String subject, String content, String username, List<String> targetsSysId, 
    		String resolveUrl)
    {
        new PostUtil().post(subject, content, username, targetsSysId, SearchConstants.SOCIALPOST_TYPE_SYSTEM);
    }

    public static void postRSS(SocialPost post, List<String> targetsSysId, String username)
    {
        new PostUtil().postRSS(post, targetsSysId, username);
    }

    public static void removeExpiredPosts(Map<String, String> params)
    {
        // try
        // {
        // DeleteSocial.removeExpiredPosts(params);
        // }
        // catch(Throwable t)
        // {
        // Log.log.info(t.getMessage() + t);
        // }
    }

    public static String removeExpiredPostsBefore(Map<String, String> params)
    {
        String result = "";
        // String timestampStr = params.get(SocialConstants.EXPIRE_TIMESTAMP) !=
        // null ? params.get(SocialConstants.EXPIRE_TIMESTAMP) : null;
        // String targetType =
        // params.get(SearchConstants.TARGET_TYPE.getTagName()) != null ?
        // params.get(SearchConstants.TARGET_TYPE.getTagName()):null;
        //
        // if (StringUtils.isNotBlank(timestampStr) &&
        // timestampStr.trim().matches("\\d+"))
        // {
        // long timestamp = Long.parseLong(timestampStr.trim());
        //
        // try
        // {
        // DeleteSocial.removeExpiredPostsBefore(timestamp, targetType);
        // SimpleDateFormat sdf = new
        // SimpleDateFormat("MMM-dd yyyy hh:mm:ss.SSS");
        // result = "Posts before " + sdf.format(new Date(timestamp)) +
        // " removed";
        // Log.log.info("Posts before " + sdf.format(new Date(timestamp)) +
        // " removed");
        // }
        // catch (Throwable t)
        // {
        // Log.log.error("Failed to remove Expired Posts using timestamp " +
        // timestamp, t);
        // result = "Failed to Remove Posts before " + (new
        // Date(timestamp)).toString() + ": " + t.getMessage();
        // }
        // }
        // else
        // {
        // Log.log.warn("Invalid Expire Timestamp Passed: " + timestampStr);
        // result = "Invalid Expire Timestamp Passed: " + timestampStr;
        // }
        //
        return result;
    }


    public static void postToUser(Map params) throws Exception
    {
        try
        {
            new PostUtil().postToUser(params);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }

    public static void forwardToSocialComponent(Map params) throws Exception
    {
        PostUtil.forwardToSocialComponent(params);
    }

    public static void postToSocialComponent(Map params) throws Exception
    {
        PostUtil.postToSocialComponent(params);
    }

    public static void dailyEmail(Map params) throws Exception
    {
        PostUtil.dailyEmail(params);
    }

    public static void postToTeam(Map params) throws Exception
    {
        PostUtil.postToTeam(params);
    }

    public static void postToComponent(Map<String, Object> params, String targetType) throws Exception
    {
        PostUtil.postToComponent(params, targetType);
    }

    /**
     * This API return returns a list of indirect DOCUMENT streams that the current user is
     * following.
     *
     * @return
     */
    public static List<RSComponent> getIndirectDocumentUserStream(String userSysId, String username, boolean recurse) throws Exception
    {
        SearchUserStreamsUtil userStreams = new SearchUserStreamsUtil(userSysId, username);
        userStreams.setRecurse(recurse);
        
        return new ArrayList<RSComponent>(userStreams.getIndirectDocumentUserStream());
    }
    
    /**
     * This API return returns a list of direct DOCUMENT streams that the current user is
     * following.
     *
     * @return
     */
    public static List<RSComponent> getDirectDocumentUserStream(String userSysId, String username, boolean recurse, int start, int limit) throws Exception
    {
        SearchUserStreamsUtil userStreams = new SearchUserStreamsUtil(userSysId, username);
        userStreams.setRecurse(recurse);
        
        return new ArrayList<RSComponent>(userStreams.getDirectDocumentUserStream(start, limit));
    }
    
    /**
     * This API return returns total count of direct DOCUMENT streams that the current user is
     * following.
     *
     * @return
     */
    public static long getDirectlyFollowedDocumentCount(String srcNodeId, String username) throws Exception
    {
        return GraphUtil.countSrcInDirectRelationshipToDocumentDest(srcNodeId, username, NonNeo4jRelationType.FOLLOWER.name());
    }
    
    /**
     * This API return returns a list of indirect ACTIONTASK streams that the current user is
     * following.
     *
     * @return
     */
    public static List<RSComponent> getIndirectActionTaskUserStream(String userSysId, String username, boolean recurse) throws Exception
    {
        SearchUserStreamsUtil userStreams = new SearchUserStreamsUtil(userSysId, username);
        userStreams.setRecurse(recurse);
        
        return new ArrayList<RSComponent>(userStreams.getIndirectActionTaskUserStream());
    }
    
    /**
     * This API return returns a list of direct ACTIONTASK streams that the current user is
     * following.
     *
     * @return
     */
    public static List<RSComponent> getDirectActionTaskUserStream(String userSysId, String username, boolean recurse, int start, int limit) throws Exception
    {
        SearchUserStreamsUtil userStreams = new SearchUserStreamsUtil(userSysId, username);
        userStreams.setRecurse(recurse);
        
        return new ArrayList<RSComponent>(userStreams.getDirectActionTaskUserStream(start, limit));
    }
    
    /**
     * This API return returns total count of direct DOCUMENT streams that the current user is
     * following.
     *
     * @return
     */
    public static long getDirectlyFollowedActionTaskCount(String srcNodeId, String username) throws Exception
    {
        return GraphUtil.countSrcInDirectRelationshipToActionTaskDest(srcNodeId, username, NonNeo4jRelationType.FOLLOWER.name());
    }
}
