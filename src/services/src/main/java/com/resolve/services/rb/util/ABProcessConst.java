package com.resolve.services.rb.util;

public enum ABProcessConst
{
    INPROGRESS,
    STARTED,
    AB_GEN_GET_GENERAL,
    AB_GEN_GET_WIKI,
    AB_GEN_GENERATE_FORM,
    AB_GEN_GENERATE_TASK,
    AB_GEN_GENERATE_XML,
    AB_GEN_UPDATE_WIKI,
    AB_GEN_UPDATE_AB,
    PARSER_TEST_SETUP,
    PARSER_TEST_RUN,
    DONE
}
