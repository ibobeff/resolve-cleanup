/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/
package com.resolve.services.rb.util;

/**
 * This class is used to deserialize wiki parameters stored as JSON in wikidocument.
 */
public class WikiParameter
{
    //{"name":"MyParam","displayName":"New Param","type":"String","sourceName":"10.20.2.124,10.50.2.71","source":"CONSTANT","hidden":false,"column":1,"order":0,"id":"","value":"10.20.2.124 10.50.2.71"},
    private String id;
    private String name;
    private String displayName;
    private String type;
    private String source;
    private String sourceName;
    private Boolean hidden;
    private String column;
    private Integer order;
    private String value;
    private String fieldTip;
    public String getId()
    {
        return id;
    }
    public void setId(String id)
    {
        this.id = id;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getDisplayName()
    {
        return displayName;
    }
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    public String getSource()
    {
        return source;
    }
    public void setSource(String source)
    {
        this.source = source;
    }
    public String getSourceName()
    {
        return sourceName;
    }
    public void setSourceName(String sourceName)
    {
        this.sourceName = sourceName;
    }
    public Boolean getHidden()
    {
        return hidden;
    }
    public void setHidden(Boolean hidden)
    {
        this.hidden = hidden;
    }
    public String getColumn()
    {
        return column;
    }
    public void setColumn(String column)
    {
        this.column = column;
    }
    public Integer getOrder()
    {
        return order;
    }
    public void setOrder(Integer order)
    {
        this.order = order;
    }
    public String getValue()
    {
        return value;
    }
    public void setValue(String value)
    {
        this.value = value;
    }
    
    public String getFieldTip()
    {
        return fieldTip;
    }
    public void setFieldTip(String fieldTip)
    {
        this.fieldTip = fieldTip;
    }
    @Override
    public String toString()
    {
        return "{\"id\":\"" + id + "\", \"name\":\"" + name + "\", \"displayName\":\"" + displayName + "\", \"type\":\"" + type + "\", \"source\":\"" + source + "\", \"sourceName\":\"" + sourceName + "\", \"hidden\":" + hidden + ", \"column\":\"" + column + "\", order\":" + order + ", \"value\":\"" + value + "\",fieldTip:\""+fieldTip+ "\"}";
    }
}
