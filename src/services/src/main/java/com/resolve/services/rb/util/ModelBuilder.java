/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.rb.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.codehaus.jettison.json.JSONArray;

import com.resolve.persistence.model.WikiDocument;
import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.RBConditionVO;
import com.resolve.services.hibernate.vo.RBConnectionParamVO;
import com.resolve.services.hibernate.vo.RBConnectionVO;
import com.resolve.services.hibernate.vo.RBCriterionVO;
import com.resolve.services.hibernate.vo.RBGeneralVO;
import com.resolve.services.hibernate.vo.RBIfVO;
import com.resolve.services.hibernate.vo.RBLoopVO;
import com.resolve.services.hibernate.vo.RBTaskHeaderVO;
import com.resolve.services.hibernate.vo.RBTaskVO;
import com.resolve.services.hibernate.vo.ResolveActionInvocVO;
import com.resolve.services.hibernate.vo.ResolveActionParameterVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.BeanUtil;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This class generates automation model XML that is used in runbook created
 * from resolution builder meta data.
 *
 */
public class ModelBuilder
{
    private int id=100;
    
    private int startTaskId = 2;
    private int endTaskId = 9999;
    private final End end;
    
    private int gapY = 75;

    public ModelBuilder()
    {
        end = new End(endTaskId, 200 + (Task.width / 2) - (TerminatorNode.width / 2), 100, null, null);    
    }
    
    private int getNextId()
    {
        return id++;
    }
    
    public String buildModelXml(String generalId, String username)
    {
        String result = null;
        
        RBTree tree = ResolutionBuilderUtils.getRBTree(generalId, username);
        if(tree != null)
        {
            RBGeneralVO generalVO = (RBGeneralVO) tree.getData();
            
            try
            {
                String taskModuleName = ResolutionBuilderUtils.getWikiName(generalVO.getNamespace(), generalVO.getName());

                int x = 200;
                int y = 20; //is a running number on y axis
      
                //by default Start has the loop turned on
                Start start = new Start(startTaskId, x + (Task.width / 2) - (TerminatorNode.width / 2), y, "\"PROCESS_LOOP\":\"true\"", null);
                Node node = start.getNode();
                node.x -= ((Task.width / 2) - (TerminatorNode.width / 2));
    
                Set<Object> tasks = new TreeSet<Object>();
                Set<Edge> edges = new TreeSet<Edge>();
    
                Map<Double, Object> orderedChildren = getOrderedChildren(tree);
                
                for(Double order : orderedChildren.keySet())
                {
                    Object child = orderedChildren.get(order);
                    if(child instanceof RBTree)
                    {
                        RBTree subTree = (RBTree) child;
                        Object data = subTree.getData();
                        if(data instanceof RBLoopVO)
                        {
                            node = addLoop((RBLoopVO) data, taskModuleName, node, tasks, edges, subTree);
                        }
                    
                        if(data instanceof RBConnectionVO)
                        {
                            node = addConnection(node, taskModuleName, (RBConnectionVO) data, subTree, tasks, edges, null, null);
                        }
                    }                
                }
                //End end = new End(endTaskId, node.x + 32, node.y + gapY, null, null);
                end.setX(node.x + (Task.width / 2) - (TerminatorNode.width / 2));
                end.setY(node.y + gapY);
                
                Edge endEdge = node.nextEdge;
                if(endEdge == null)
                {
                    endEdge = new Edge(getNextId(), node.id, endTaskId, "", "", null);
                }
                else
                {
                    endEdge.setTarget(endTaskId);
                }
                edges.add(endEdge);
                
                Model model = new Model(start, end, tasks, edges);
                result = model.toString();
                Log.log.debug("Model XML created: " + result);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        return result;
    }

    private Node addLoop(final RBLoopVO loopVO, final String taskModuleName, final Node node, final Set<Object> tasks, final Set<Edge> edges, final RBTree tree)
    {
        Node result = node;
        
        //add the looper
        String inputs = generateLooperInputs(loopVO);
        String outputs = generateLooperOutputs(loopVO);
        String urlParams = generateLooperDescription(loopVO);
        String description = getFullTaskName("looper", ABConstants.DEFAULT_NAMESPACE) + "?" + urlParams;
        Node looper = addTask(node, ABConstants.DEFAULT_NAMESPACE, "looper", "looper" + loopVO.getOrder(), "ALWAYS", tasks, edges, null, "ANY", inputs, outputs, description, null, false);
        
        //store this so noop aligns with looper, other tasks shifts 100px to right
        int looperX = looper.x;
        int looperY = looper.y;
        
        //looper task sets the OUTPUTS["CONTINUE"] variable
        //String expression = "OUTPUTS[&quot;CONTINUE&quot;].equals(&quot;true&quot;)";
        String expression = "OUTPUTS[&quot;CONTINUE&quot;]";
        Precondition precondition = new Precondition(getNextId(), looperX + 32, looperY + gapY, expression);
        tasks.add(precondition);

        //add the green edge from the looper to first task
        Edge looperEdge = new Edge(getNextId(), looper.id, precondition.id, "", "", "");
        edges.add(looperEdge);

        //add the green edge from the precondition to the first task
        Edge greenEdge = new Edge(getNextId(), precondition.id, -1, "", "", "green");
        edges.add(greenEdge);
        precondition.setNextEdge(greenEdge);
        
        //noop task for the end of loop.
        Noop noop = new Noop(getNextId(), precondition.getX() - precondition.getWidth(), -1, "ANY");
        tasks.add(noop);

        //add looping connections, tasks, get the last node
        Node lastNode = addLoopingTask(precondition.getNode(), taskModuleName, tree, tasks, edges, looper, noop.getNode());
        noop.setY(lastNode.y + gapY);
        //last node needs to have an edge back
        Geometry geometry = new Geometry(lastNode.x, lastNode.y, 100, 100, 0, "geometry", lastNode.x+100, lastNode.y+10, looperX+200, looperY+10);
        Edge returnEdge = new Edge(getNextId(), lastNode.id, looper.id, "", "", null, "vertical", geometry);
        edges.add(returnEdge);

        
        //add the red edge from the looper task to noop task
        Edge redEdge = new Edge(getNextId(), precondition.id, noop.getId(), "", "", "red");
        edges.add(redEdge);
        
        result = noop.getNode();
        return result;
    }

    private String generateConnectionInputs(RBConnectionVO connectionVO)
    {
        StringBuilder result = new StringBuilder();
        List<RBConnectionParamVO> params = connectionVO.getParams();
        if(params != null && !params.isEmpty())
        {
            for(RBConnectionParamVO param : params)
            {
                if(StringUtils.isNotBlank(param.getSourceName()))
                {
                    if("CONSTANT".equalsIgnoreCase(param.getSource()))
                    {
                        //"USERNAME":"resolve","HOST":"127.0.0.1">
                        result.append("\"").append(param.getName()).append("\":\"").append(param.getSourceName()).append("\",");
                    }
                    else
                    {
                        //"USERNAME":"$PARAMS{resolve}","HOST":"$FLOWS{myhost}">
                        result.append("\"").append(param.getName()).append("\":\"$").append(ABConstants.SOURCE.get(param.getSource())).append("{").append(param.getSourceName()).append("}\",");
                    }
                }
            }
        }
        if(result.length() > 1)
        {
            result.setLength(result.length() - 1); //truncate last ,
        }
        return result.toString();
    }

    private String generateConnectionInputs(RBConnectionVO connectionVO, Collection<String> paramsToEncrypt)
    {
        StringBuilder result = new StringBuilder();
        List<RBConnectionParamVO> params = connectionVO.getParams();
        if(params != null && !params.isEmpty())
        {
            for(RBConnectionParamVO param : params)
            {
                if(StringUtils.isNotBlank(param.getSourceName()))
                {
                    if("CONSTANT".equalsIgnoreCase(param.getSource()))
                    {
                        //"USERNAME":"resolve","HOST":"127.0.0.1">
                        try
                        {
                            result.append("\"").append(param.getName()).append("\":\"").append(paramsToEncrypt.contains(param.getName()) ? CryptUtils.encrypt(param.getSourceName()) : param.getSourceName()).append("\",");
                        }
                        catch (Exception e)
                        {
                            Log.log.error(e.getMessage(), e);
                        }
                    }
                    else
                    {
                        //"USERNAME":"$PARAMS{resolve}","HOST":"$FLOWS{myhost}">
                        result.append("\"").append(param.getName()).append("\":\"$").append(ABConstants.SOURCE.get(param.getSource())).append("{").append(param.getSourceName()).append("}\",");
                    }
                }
            }
        }
        
        if(result.length() > 1)
        {
            result.setLength(result.length() - 1); //truncate last ,
        }
        return result.toString();
    }
    
    private String generateTaskInputs(RBTaskVO taskVO, RBConnectionVO connectionVO) {
        
        return generateTaskInputs(taskVO, connectionVO, null);
    }
    
    private String generateTaskInputs(RBTaskVO taskVO, RBConnectionVO connectionVO, final String taskModuleName)
    {
        StringBuilder result = new StringBuilder();
        
        if(taskVO != null)
        {
            if(StringUtils.isNotBlank(taskVO.getRefRunbookParams()) && !taskVO.getRefRunbookParams().equalsIgnoreCase(VO.STRING_DEFAULT))
            {
                result.append(taskVO.getRefRunbookParams());
            }
            else
            {
                List<WikiParameter> wikiParams = getWikiVariables(taskModuleName);
                
//                String command = taskVO.getCommand();
//                if(StringUtils.isNotEmpty(command))
//                    result.append("\"command\":\"").append(command).append("\",");
//              
                // Variable substitution for commandUrl in HTTP Connector
                String commandUrl = taskVO.getCommandUrl();
                if(StringUtils.isNotEmpty(commandUrl)) {
                    if(taskModuleName != null && wikiParams.size() != 0)
                        commandUrl = replaceParams(commandUrl, wikiParams);

                    result.append("\"commandUrl\":\"").append(commandUrl).append("\",");
                }
                
                Log.log.debug("commandUrl = " + commandUrl);
                
                String method = taskVO.getMethod();
                if(StringUtils.isNotEmpty(method))
                    result.append("\"method\":\"").append(method).append("\",");
                
                // Variable substitution for body in HTTP Connector
                String body = taskVO.getBody();
                if(StringUtils.isNotEmpty(body)) {
                    if(taskModuleName != null && wikiParams.size() != 0)
                        body = replaceParams(body, wikiParams);
                        
                    result.append("\"body\":\"").append(body).append("\",");
                }
                
                Log.log.debug("body = " + body);
                
                List<RBTaskHeaderVO> headers = taskVO.getHeaders();
                if(headers.size() != 0) {
                    StringBuilder sb = new StringBuilder();
                    
                    for(RBTaskHeaderVO header:headers) {
                        String name = header.getName();
                        String value = header.getValue();
                        
                        // Variable substitution for header values in HTTP Connector
                        if(value.indexOf("$") != -1)
                            value = replaceParams(value, wikiParams);
                        
                        if(StringUtils.isNotEmpty(name))
                            sb.append(name).append(":").append(value).append("|");
                    }

                    String output = sb.toString();
                    String str = output.substring(0, output.length()-1);
                    result.append("\"headers\": \"").append(str).append("\", ");
                }

                String promptSource = null;
                String promptSourceName = null;
                if(StringUtils.isNotBlank(taskVO.getPromptSourceName()))
                {
                    promptSource = taskVO.getPromptSource();
                    promptSourceName = taskVO.getPromptSourceName();
                }
                else
                {
                    List<RBConnectionParamVO> params = connectionVO.getParams();
                    if(params != null && !params.isEmpty())
                    {
                        for(RBConnectionParamVO param : params)
                        {
                            if("prompt".equalsIgnoreCase(param.getName()))
                            {
                                promptSource = param.getSource();
                                promptSourceName = param.getSourceName();
                                break;
                            }
                        }
                    }
                }
                
                if(StringUtils.isNotBlank(promptSourceName))
                {
                    if("CONSTANT".equalsIgnoreCase(promptSource))
                    {
                        //"prompt":"\\$"
                        result.append("\"").append("prompt").append("\":\"").append(promptSourceName).append("\",");
                    }
                    else
                    {
                        //"USERNAME":"$PARAMS{resolve}","HOST":"$FLOWS{myhost}">
                        result.append("\"").append("prompt").append("\":\"$").append(ABConstants.SOURCE.get(promptSource)).append("{").append(promptSourceName).append("}\",");
                    }
                }
                
                String expectTimeout = taskVO.getExpectTimeout() + "";
                result.append("\"EXPECT_TIMEOUT\":\"").append(expectTimeout).append("\",");
                
                if(result.length() > 1)
                {
                    result.setLength(result.length() - 1); //truncate last ,
                }
            }
        }

        return result.toString();
    }
    
    private String replaceParams(String input, List<WikiParameter> wikiParams) {
        
        String output = input;

        for(WikiParameter param:wikiParams) {
            String name = param.getName();
            String source = param.getSource();
            String sourceName = param.getSourceName();
            
            if(source.equals("CONSTANT")) {
                String keyStr = "$PARAM{" + name + "}";
                
                Log.log.debug("keyStr = " + keyStr);
                
                while(output.contains(keyStr))
                    output = output.replace(keyStr, sourceName);
            }
        }
        
        Log.log.debug("input = " + input);
        Log.log.debug("output = " + output);
        
        return output;
    }
    
    private List<WikiParameter> getWikiVariables(final String taskModuleName) {
        
        String sysId = WikiUtils.getWikidocSysId(taskModuleName);
        
        WikiDocument doc = WikiUtils.getWikiDocumentModel(sysId, taskModuleName, "admin", false);
        
        String params = doc.getUWikiParameters();
        
        List<WikiParameter> wikiParams = new ArrayList<WikiParameter>();
        
        try {
            JSONArray json = new JSONArray(params);
            for(int i=0; i<json.length(); i++) {
                String record = json.getString(i);
                if(StringUtils.isBlank(record))
                    continue;
                
                WikiParameter param = new WikiParameter();
                
                String[] entries = record.substring(1, record.length()-1).split(",");
                
                for(int j=0; j<entries.length; j++) {
                    String entry = entries[j];
                    if(StringUtils.isBlank(entry))
                        continue;

                    String paramKey = "";
                    String paramValue = "";
                    
                    String[] keyValue = entry.split(":");
                    String key = keyValue[0];
                    String value = keyValue[1];
                    if(StringUtils.isNotEmpty(key) && StringUtils.isNotEmpty(value) && key.length() > 2 && value.length() > 2) {
                        paramKey = key.substring(1, key.length()-1);
                        paramValue = value.substring(1, value.length()-1);
                        
                        Log.log.debug("key = " + paramKey);
                        Log.log.debug("value = " + paramValue);
                        
                        if(StringUtils.isNotEmpty(paramKey)) {
                            if(paramKey.equals("name"))
                                param.setName(paramValue);
                            else if(paramKey.equals("displayName"))
                                param.setDisplayName(paramValue);
                            else if(paramKey.equals("source"))
                                param.setSource(paramValue);
                            else if(paramKey.equals("sourceName"))
                                param.setSourceName(paramValue);
                        }
                    }
                } // end of for(record)

                wikiParams.add(param);
            } // end of for(json array)
        } catch(Exception e) {
            Log.log.error(e.getMessage());
        }
        
        return wikiParams;
    }

    private String generateTaskOutputs(RBConnectionVO connectionVO)
    {
        StringBuilder result = new StringBuilder("");
        return result.toString();
    }

    private String generateConnectionDescription(RBConnectionVO connectionVO)
    {
        String result = null;
        
        StringBuilder url = new StringBuilder();
        List<RBConnectionParamVO> params = connectionVO.getParams();
        if(params != null && !params.isEmpty())
        {
            for(RBConnectionParamVO param : params)
            {
                if(StringUtils.isNotBlank(param.getSourceName()))
                {
                    if("CONSTANT".equalsIgnoreCase(param.getSource()))
                    {
                        //host%3D10.20.2.230%26port%3D22
                        url.append(encodeInner(param.getName())).append("%3D").append(encodeInner(param.getSourceName())).append("%26");
                    }
                    else
                    {
                        //host%3D%24FLOW%7Bhost1%7D%26port%3D%24FLOW%7Bport1%7D
                        url.append(encodeInner(param.getName())).append("%3D%24").append(ABConstants.SOURCE.get(param.getSource())).append("%7B").append(encodeInner(param.getSourceName())).append("%7D").append("%26");
                    }
                }
            }
        }
        if(url.length() > 3)
        {
            url.setLength(url.length() - 3); //truncate last %26
/*            try
            {
                result = URLEncoder.encode(url.toString(), "utf-8");
                //result.replaceAll("+", "%20");
            }
            catch (UnsupportedEncodingException e)
            {
                Log.log.error(e.getMessage(), e);
            }
*/          result = url.toString();
        }

        return result;
    }

    private String generateTaskDescription(RBTaskVO taskVO, RBConnectionVO connectionVO)
    {
        String result = null;
        
        if(taskVO != null)
        {
            String command = taskVO.getCommand();
            String encodedCommand = taskVO.getEncodedCommand();
            String promptSource = null;
            String promptSourceName = null;
            if(StringUtils.isNotBlank(taskVO.getPromptSourceName()))
            {
                promptSource = taskVO.getPromptSource();
                promptSourceName = taskVO.getPromptSourceName();
            }
            else
            {
                List<RBConnectionParamVO> params = connectionVO.getParams();
                if(params != null && !params.isEmpty())
                {
                    for(RBConnectionParamVO param : params)
                    {
                        if("prompt".equalsIgnoreCase(param.getName()))
                        {
                            promptSource = param.getSource();
                            promptSourceName = param.getSourceName();
                            break;
                        }
                    }
                }
            }
            
            StringBuilder url = new StringBuilder();
    
            if(StringUtils.isBlank(encodedCommand))
            {
                url.append("command%3D").append(encodeInner(command)).append("%26");
            }
            else
            {
                url.append("command%3D").append(encodedCommand).append("%26");
            }
            
            if(StringUtils.isNotBlank(promptSourceName))
            {
                if("CONSTANT".equalsIgnoreCase(promptSource))
                {
                    //host%3D10.20.2.230%26port%3D22
                    url.append(encodeInner("prompt")).append("%3D").append(encodeInner(promptSourceName)).append("%26");
                }
                else
                {
                    //host%3D%24FLOW%7Bhost1%7D%26port%3D%24FLOW%7Bport1%7D
                    url.append(encodeInner("prompt")).append("%3D%24").append(ABConstants.SOURCE.get(promptSource)).append("%7B").append(encodeInner(promptSourceName)).append("%7D").append("%26");
                }
            }
            
            if(url.length() > 3)
            {
                url.setLength(url.length() - 3); //truncate last %26
                result = url.toString();
                //result = URLEncoder.encode(url.toString(), "utf-8");
                //result = encode(url.toString());
            }
        }
        
        return result;
    }

    private String encodeInner(String item)
    {
        String result = "";
        //the crazy encoding that UI needs
        try
        {
            if(StringUtils.isNotBlank(item))
            {
                String enc1 = URLEncoder.encode(item.toString(), "utf-8");
                String enc2 = enc1.replace("+", "%20");
                result = enc2.replace("%27", "'");
            }
        }
        catch (UnsupportedEncodingException e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return result;
    }

    private String generateLooperInputs(RBLoopVO loopVO)
    {
        StringBuilder result = new StringBuilder();
        if(loopVO.getCount() > 0)
        {
            result.append("\"COUNT\":\"").append(loopVO.getCount()).append("\"");
        }
        else
        {
            if("CONSTANT".equalsIgnoreCase(loopVO.getItemSource()))
            {
                result.append("\"ITEMS\":\"").append(loopVO.getItemSourceName()).append("\"");
            }
            else
            {
                result.append("\"ITEMS\":\"$").append(ABConstants.SOURCE.get(loopVO.getItemSource())).append("{").append(loopVO.getItemSourceName()).append("}\"");
            }
            if(StringUtils.isNotBlank(loopVO.getItemDelimiter()))
            {
                if(ABConstants.DELIMITERS.containsKey(loopVO.getItemDelimiter().toLowerCase()))
                {
                    result.append(", \"DELIMITER\":\"").append(ABConstants.DELIMITERS.get(loopVO.getItemDelimiter().toLowerCase())).append("\"");
                }
                else
                {
                    result.append(", \"DELIMITER\":\"").append(loopVO.getItemDelimiter()).append("\"");
                }
            }
        }
        return result.toString();
    }

    private String generateLooperOutputs(RBLoopVO loopVO)
    {
        StringBuilder result = new StringBuilder("");
        return result.toString();
    }

    private String generateLooperDescription(RBLoopVO loopVO)
    {
        String result = null;
        
        StringBuilder url = new StringBuilder();
        if(StringUtils.isNotBlank(loopVO.getItemSourceName()))
        {
            if("CONSTANT".equalsIgnoreCase(loopVO.getItemSource()))
            {
                //ITEMS%3DBPAR1%7D%26DELIMETER%3D%252C
                url.append("ITEMS%3D").append(encodeInner(loopVO.getItemSourceName()));
            }
            else
            {
                //ITEMS%3D%24FLOW%7BPAR1%7D%26DELIMETER%3D%252C
                url.append("ITEMS%3D%24").append(ABConstants.SOURCE.get(loopVO.getItemSource())).append("%7B").append(encodeInner(loopVO.getItemSourceName())).append("%7D%26");
                if(StringUtils.isNotBlank(loopVO.getItemDelimiter()))
                {
                    if(ABConstants.DELIMITERS.containsKey(loopVO.getItemDelimiter().toLowerCase()))
                    {
                        String delimiter = ABConstants.DELIMITERS.get(loopVO.getItemDelimiter());
                        url.append("DELIMITER%3D").append(encodeInner(delimiter));
                    }
                    else
                    {
                        url.append("DELIMITER%3D").append(encodeInner(loopVO.getItemDelimiter()));
                    }
                }
            }
        }
        else
        {
            if(loopVO.getCount() > 0)
            {
                url.append("COUNT%3D").append(loopVO.getCount());
            }
            else
            {
                Log.log.warn("Count or Item source must be provided for looper action task.");
            }
        }
        
        if(url.length() > 1)
        {
            result = url.toString();
/*            try
            {
                result = URLEncoder.encode(url.toString(), "utf-8");
            }
            catch (UnsupportedEncodingException e)
            {
                Log.log.error(e.getMessage(), e);
            }
*/
        }
       
        return result;
    }

    /**
     * new BigDecimal(FLOWS[&quot;LOOP_ITEM&quot;] ).compareTo(new BigDecimal(&quot;0&quot;)) &lt; 0
     * 
     * @param criteria
     * @return
     */
    private String generateExpression(Collection<RBCriterionVO> criteria)
    {
        StringBuilder result = new StringBuilder();
        String separator = " &amp;&amp; ";
        for(RBCriterionVO criterion : criteria)
        {
            //these are identified as numeric operators. = operator works are String for now
            boolean isNumeric = "gt~gte~lt~lte".contains(criterion.getComparison().trim());
            
            String left = null;
            if("constant".equalsIgnoreCase(criterion.getVariableSource()))
            {
                left = "&quot;" + criterion.getVariable() + "&quot;";
            }
            else
            {
                left = ABConstants.SOURCE_MAP.get(criterion.getVariableSource().toUpperCase()) + "[&quot;" + criterion.getVariable() + "&quot;]"; 
            }

            String right = null;
            if("constant".equalsIgnoreCase(criterion.getSource()))
            {
                right = "&quot;" + criterion.getSourceName() + "&quot;"; 
            }
            else
            {
                right = ABConstants.SOURCE_MAP.get(criterion.getSource().toUpperCase()) + "[&quot;" + criterion.getSourceName() + "&quot;]"; 
            }

            if("contains".equalsIgnoreCase(criterion.getComparison().trim()))
            {
                result.append(left).append(".contains(").append(right).append(") ");
            }
            else if("notContains".equalsIgnoreCase(criterion.getComparison().trim()))
            {
                result.append("!").append(left).append(".contains(").append(right).append(") ");
            }
            else if("startsWith".equalsIgnoreCase(criterion.getComparison().trim()))
            {
                result.append(left).append(".startsWith(").append(right).append(") ");
            }
            else if("endsWith".equalsIgnoreCase(criterion.getComparison().trim()))
            {
                result.append(left).append(".endsWith(").append(right).append(") ");
            }
            else
            {
                if(isNumeric)
                {
                    left = "new BigDecimal(" + left + ")";
                    right = "new BigDecimal(" + right + ")";
                    result.append(left).append(".compareTo(").append(right).append(") ").append(ABConstants.URLOPERATORS.get(criterion.getComparison().trim())).append(" 0");
                }
                else
                {
                    result.append(left).append(" ").append(ABConstants.URLOPERATORS.get(criterion.getComparison().trim())).append(" ").append(right);
                }
            }
            result.append(separator); //add &&
        }
        if(result.length() > separator.length())
        {
            result.setLength(result.length() - separator.length()); //remove last &&
        }
        return result.toString();
    }

    private Node addLoopingTask(final Node preCondition, final String taskModuleName, final RBTree tree, final Set<Object> tasks, final Set<Edge> edges, final Node looperNode, final Node noopNode)
    {
        Node result = preCondition;
        result.x += 100; 
        
        //get connections
        Map<Double, Object> orderedChildren = getOrderedChildren(tree);
        
        for(Double order : orderedChildren.keySet())
        {
            Object child = orderedChildren.get(order);
            if(child instanceof RBTree)
            {
                RBTree subTree = (RBTree) child;
                Object data = subTree.getData();
                if(data instanceof RBConnectionVO)
                {
                    result = addConnection(result, taskModuleName, (RBConnectionVO) data, subTree, tasks, edges, looperNode, noopNode);
                }
            }                
        }        
        return result;
    }

    private Node addConnection(final Node lastNode, final String taskModuleName, final RBConnectionVO connectionVO, final RBTree tree, final Set<Object> tasks, final Set<Edge> edges, final Node onConnectionFailNode, final Node onTaskFailNode)
    {
        Node result = lastNode;
        
        Map<Double, Object> orderedChildren = getOrderedChildren(tree);
        
        if(orderedChildren != null && orderedChildren.size() > 0)
        {
            //add the sshConnect task which puts the connection into SESSIONS
            //if there is some problem connecting to the server then we stop
            String inputs = generateConnectionInputs(connectionVO);
            String outputs = generateTaskOutputs(connectionVO);
            String description = null;

            //noop task for the end of connection.
            Noop noop = null;
            int lastNodeId = getNextId();
            
            //get connect task
            String connectTask = ABConstants.CONNECT_TASKS.get(connectionVO.getType().toLowerCase());
            if(connectTask != null)
            {
                ResolveActionTaskVO connectActionTaskVO = ActionTaskUtil.getActionTaskWithReferences(null, getFullTaskName(connectTask, ABConstants.DEFAULT_NAMESPACE), "admin");
                ResolveActionInvocVO connectActionInvokeVO = connectActionTaskVO.getResolveActionInvoc();
                
                if (connectActionInvokeVO != null)
                {
                    Collection<ResolveActionParameterVO> resolveActionParameterVOs = connectActionInvokeVO.getResolveActionParameters();
                    
                    if (resolveActionParameterVOs != null && !resolveActionParameterVOs.isEmpty())
                    {
                        Collection<String> paramsToEncrypt = new ArrayList<String>();
                        
                        for (ResolveActionParameterVO resolveActionParameterVO : resolveActionParameterVOs)
                        {
                            if (resolveActionParameterVO.getUEncrypt())
                            {
                                paramsToEncrypt.add(resolveActionParameterVO.getUName());
                            }
                        }
                        
                        if (!paramsToEncrypt.isEmpty())
                        {
                            inputs = generateConnectionInputs(connectionVO, paramsToEncrypt);
                        }
                    }
                                   
                }
                description = getFullTaskName(connectTask, ABConstants.DEFAULT_NAMESPACE) + "?" + generateConnectionDescription(connectionVO);
                result = addTask(lastNode, ABConstants.DEFAULT_NAMESPACE, connectTask, null, "STOPONBAD", tasks, edges, null, null, inputs, outputs, description, onConnectionFailNode, false);
            }
            else
            {
                //noop task for the end of connection. used in "local" mode
                noop = new Noop(lastNodeId,  -1, -1, "ANY");
                tasks.add(noop);
            }
            Node endNode = new Node(lastNodeId, -1, -1, null);
            
            for(Double order : orderedChildren.keySet())
            {
                Object child = orderedChildren.get(order);
                if(child instanceof RBTree)
                {
                    RBTree subTree = (RBTree) child;
                    Object data = subTree.getData();
                    if(data instanceof RBIfVO)
                    {
                        result = addIf(result, taskModuleName, subTree, tasks, edges, connectionVO, endNode);
                    }
                }                
                else if(child instanceof RBTaskVO)
                {
                    //for task 
                    result = addTask(result, taskModuleName, (RBTaskVO) child, tasks, edges, null, connectionVO, endNode);
                }
            }
            
            //get disconnect task
            String disconnectTask = ABConstants.DISCONNECT_TASKS.get(connectionVO.getType().toLowerCase());
            if(disconnectTask != null)
            {
                result = addTask(lastNodeId, result, ABConstants.DEFAULT_NAMESPACE, disconnectTask, null, "ALWAYS", tasks, edges, null, "ANY", null, null, disconnectTask.concat("#").concat(ABConstants.DEFAULT_NAMESPACE), null, false);
            }

            //this seems to be "local" connection
            if(noop != null)
            {
                //add the edge from the last task
                Edge lastEdge = new Edge(getNextId(), result.id, noop.getId(), "", "", "");
                edges.add(lastEdge);
                
                noop.setX(result.x);
                noop.setY(result.y + gapY);
                result = noop.getNode();
            }
        }
        
        return result;
    }

    private Map<Double, Object> getOrderedChildren(final RBTree tree)
    {
        Map<Double, Object> result = new TreeMap<Double, Object>();
        for(Object child : tree.getChildren())
        {
            try
            {
                Object data = child;
                if(!(child instanceof RBTaskVO))
                {
                    data = BeanUtil.getObjectProperty(child, "data");
                }
                Double order = (Double) BeanUtil.getObjectProperty(data, "order");
                result.put(order, child);
            }
            catch (Exception e)
            {
                Log.log.info("No \"order\" for this object." + e.getMessage());
            }
        }
        return result;
    }

    private Node addIf(final Node previousNode, final String taskModuleName, RBTree tree, final Set<Object> tasks, final Set<Edge> edges, final RBConnectionVO connectionVO, final Node onFailNode)
    {
        Node result = previousNode;
        Edge previousEdge = previousNode.nextEdge;
        
        //get ifs or tasks
        Map<Double, Object> orderedChildren = getOrderedChildren(tree);
        
        boolean isEdgeUsed = false; //this makes sure edge is used once
        for(Double order : orderedChildren.keySet())
        {
            Object child = orderedChildren.get(order);
            if(child instanceof RBTree)
            {
                RBTree subTree = (RBTree) child;
                Object data = subTree.getData();
                if(data instanceof RBConditionVO)
                {
                    //add pre-conditions
                    String expression = generateExpression(((RBConditionVO) data).getCriteria());
                    Precondition precondition = new Precondition(getNextId(), result.x + (Task.width / 2) - (Precondition.width / 2), result.y + gapY, expression);
                    tasks.add(precondition);
                    
                    //this edge is from the previous node to this condition block
                    Edge edge = null;
                    if(previousEdge == null || isEdgeUsed)
                    {
                        edge = new Edge(getNextId(), result.id, precondition.getId(), "", "", null);
                        edges.add(edge);
                    }
                    else
                    {
                        isEdgeUsed = true;
                        edge = previousEdge;
                        edge.setTarget(precondition.getId());
                    }
                    
                    //add all the tasks inside this condition and return the last task to be connected with noop.
                    Node lastConditionTask = addCondition(precondition.getNode(), taskModuleName, subTree, tasks, edges, connectionVO, onFailNode);
                    
                    //add the noop task at the end.
                    Noop noop = new Noop(getNextId(), precondition.getX() - precondition.getWidth(), lastConditionTask.y + gapY, "ANY");
                    tasks.add(noop);
                    
                    //connect last task to noop
                    Edge lastEdge = lastConditionTask.nextEdge;
                    if(lastEdge == null)
                    {
                        lastEdge = new Edge(getNextId(), lastConditionTask.id, noop.getId(), "", "", null);
                    }
                    else
                    {
                        lastEdge.setTarget(noop.getId());
                    }
                    edges.add(lastEdge);

                    //connect false to noop
                    Edge redEdge = new Edge(getNextId(), precondition.getId(), noop.getId(), "", "", "red");
                    edges.add(redEdge);
                    
                    result = noop.getNode();
                }
            }
        }
        return result;
    }

    private Node addCondition(final Node preconditionNode, final String taskModuleName, RBTree tree, final Set<Object> tasks, final Set<Edge> edges, final RBConnectionVO connectionVO, final Node onFailNode)
    {
        Node result = preconditionNode;
        result.x += 50; //shift 50 pixels
        
        Map<Double, Object> orderedChildren = getOrderedChildren(tree);
        
        boolean isEdgeUsed = false; //this makes sure edge is used once
        for(Double order : orderedChildren.keySet())
        {
            Object child = orderedChildren.get(order);
            if(child instanceof RBTaskVO)
            {
                if(!isEdgeUsed)
                {
                    //first task will have the green edge
                    result = addTask(result, taskModuleName, (RBTaskVO) child, tasks, edges, "green", connectionVO, null);
                }
                else
                {
                    isEdgeUsed = true;
                    result = addTask(result, taskModuleName, (RBTaskVO) child, tasks, edges, connectionVO, onFailNode);
                }
            }
        }
        return result;
    }

    private Node addTask(final Node lastNode, final String taskModuleName, RBTaskVO taskVO, Set<Object> tasks, Set<Edge> edges, final RBConnectionVO connectionVO, final Node onFailNode)
    {
        return addTask(lastNode, taskModuleName, taskVO, tasks, edges, null, connectionVO, onFailNode);
    }
    
    private Node addTask(final Node lastNode, final String taskModuleName, RBTaskVO taskVO, Set<Object> tasks, Set<Edge> edges, String edgeColor, final RBConnectionVO connectionVO, final Node onFailNode)
    {
        Node result = lastNode;
     
        if(taskVO != null)
        {
            String inputs = generateTaskInputs(taskVO, connectionVO, taskModuleName);
            String outputs = generateTaskOutputs(connectionVO);
            String description = (StringUtils.isNotBlank(taskVO.getRefRunbookParams()) && !taskVO.getRefRunbookParams().equalsIgnoreCase(VO.STRING_DEFAULT)) ? "" : getFullTaskName(taskVO.getName(), taskModuleName) + "?" + generateTaskDescription(taskVO, connectionVO);
            String taskName = taskVO.getName();
            
            result = addTask(lastNode, taskModuleName, taskName, null, taskVO.getContinuation(), tasks, edges, edgeColor, null, inputs, outputs, description, onFailNode, RBTaskVO.SUBRUNBOOK_ENTITY_TYPE.equals(taskVO.getEntityType()));            
        }
        return result;
    }
    
    private Node addTask(final Node lastNode, final String taskModuleName, final String taskName, final String displayName, final String continuation, final Set<Object> tasks, final Set<Edge> edges, final String edgeColor, final String merge, final String inputs, final String outputs, final String description, final Node onFailNode, boolean isRunbookRef)
    {
        return addTask(-1, lastNode, taskModuleName, taskName, displayName, continuation, tasks, edges, edgeColor, merge, inputs, outputs, description, onFailNode, isRunbookRef);
    }
    
    private Node addTask(final int taskId1, final Node lastNode, final String taskModuleName, final String taskName, final String displayName, final String continuation, final Set<Object> tasks, final Set<Edge> edges, final String edgeColor, final String merge, final String inputs, final String outputs, final String description, final Node onFailNode, boolean isRunbookRef)
    {
        Node result = lastNode;
     
        if(StringUtils.isNotBlank(taskModuleName) && StringUtils.isNotBlank(taskName))
        {
            int taskId = getNextId();
            //sometime id may come from somewhere else
            if(taskId1 >= 0)
            {
                taskId = taskId1;
            }
            int x = lastNode.x;
            int y = lastNode.y + gapY; //just add 100 
            
            Task task = new Task(taskId, taskName, taskModuleName, x, y, description, inputs, outputs, merge, isRunbookRef);
            if(displayName != null)
            {
                task.setNodeLabel(displayName);
            }
            tasks.add(task);
            Edge previousEdge = lastNode.nextEdge;
            if(previousEdge == null)
            {
                previousEdge = new Edge(getNextId(), lastNode.id, taskId, "", "", edgeColor);
            }
            else
            {
                previousEdge.setTarget(taskId);
            }
            edges.add(previousEdge);

            if(StringUtils.isBlank(continuation) || "ALWAYS".equalsIgnoreCase(continuation))
            {
                //just grey edge
                Edge nextEdge = new Edge(getNextId(), task.getId(), -1, "", "", null);
                edges.add(nextEdge);
                result = new Node(taskId, x, y, merge, null, nextEdge); 
            }
            else
            {
                if("STOPONBAD".equalsIgnoreCase(continuation))
                {
                    //red goes directly to end task
                    Geometry geometry = new Geometry(lastNode.x, lastNode.y, 100, 100, 0, "geometry", x+100, y+10, x+200, y+10, 550, y+10);
                    Edge redEdge = null;
                    if(onFailNode == null)
                    {
                        redEdge = new Edge(getNextId(), task.getId(), end.getId(), "", "", "red", "vertical;strokeColor=red", geometry);
                    }
                    else
                    {
                        redEdge = new Edge(getNextId(), task.getId(), onFailNode.id, "", "", "red", "vertical;strokeColor=red", geometry);
                    }
                    edges.add(redEdge);
                    
                    //green edge for the next Node
                    Edge greenEdge = new Edge(getNextId(), task.getId(), -1, "", "", "green");
                    edges.add(greenEdge);
                    result = new Node(taskId, x, y, merge, null, greenEdge); 
                }
            }
        }
        return result;
    }
    
    private String getFullTaskName(String taskName, String moduleName)
    {
        StringBuilder result = new StringBuilder();
        if(StringUtils.isNotBlank(moduleName) && StringUtils.isNotBlank(taskName))
        {
            //taskname#modulename for normal task
            result.append(taskName).append("#").append(moduleName);
        }
        return result.toString();
    }
}
