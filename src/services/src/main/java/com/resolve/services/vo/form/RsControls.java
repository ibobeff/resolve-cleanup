/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.form;

import java.util.List;

public class RsControls
{
    private List<RsUIField> controls;

    public List<RsUIField> getControls()
    {
        return controls;
    }

    public void setControls(List<RsUIField> controls)
    {
        this.controls = controls;
    }
    
    

}
