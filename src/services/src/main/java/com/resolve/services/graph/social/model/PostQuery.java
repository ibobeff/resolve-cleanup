/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model;

import java.util.HashMap;
import java.util.Map;

public class PostQuery
{
    private String username = null;
    private String sysOrg = null;//this is the orgId of the user who is querying to get the list of Post
    private String displayName = null;
    private String sysid = null;
    private String comptype = null;
    private String parent_comptype = null;
    private String parent_sysid = null;
    private String searchPostKey = null;
    private String searchPostTime = null;
    private String queryType = null;
    private String resultsType = null;
    private Boolean doSearch = false;
    
    private Map<String, Object> params = new HashMap<String, Object>();

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    public String getSysid()
    {
        return sysid;
    }

    public void setSysid(String sysid)
    {
        this.sysid = sysid;
    }

    public String getComptype()
    {
        return comptype;
    }

    public void setComptype(String comptype)
    {
        this.comptype = comptype;
    }

    public String getParent_comptype()
    {
        return parent_comptype;
    }

    public void setParent_comptype(String parent_comptype)
    {
        this.parent_comptype = parent_comptype;
    }

    public String getParent_sysid()
    {
        return parent_sysid;
    }

    public void setParent_sysid(String parent_sysid)
    {
        this.parent_sysid = parent_sysid;
    }

    public Map<String, Object> getParams()
    {
        return params;
    }
    
    public void addParam(String name, Object value)
    {
        this.params.put(name, value);
    }

    public void setParams(Map<String, Object> params)
    {
        this.params = params;
    }
    
    public void setSearchPostKey(String searchPostKey)
    {
        this.searchPostKey = searchPostKey;
    }
    
    public String getSearchPostKey()
    {
        return searchPostKey;
    }
    
    public void setSearchPostTime(String searchPostTime)
    {
        this.searchPostTime = searchPostTime;
    }
    
    public String getSearchPostTime()
    {
        return searchPostTime;
    }
    
    public void setQueryType(String queryType)
    {
        this.queryType = queryType;
    }
    
    public String getQueryType()
    {
        return this.queryType;
    }
    
    public void setResultsType(String resultsType)
    {
        this.resultsType = resultsType;
    }
    
    public String getResultsType()
    {
        return this.resultsType;
    }

    public Boolean getDoSearch()
    {
        return doSearch;
    }

    public void setDoSearch(Boolean doSearch)
    {
        this.doSearch = doSearch;
    }

    public String getSysOrg()
    {
        return sysOrg;
    }

    public void setSysOrg(String sysOrg)
    {
        this.sysOrg = sysOrg;
    }
    
}
