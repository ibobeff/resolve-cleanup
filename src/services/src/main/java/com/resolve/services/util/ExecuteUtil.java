/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import com.resolve.rsbase.MainBase;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWiki;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

public class ExecuteUtil
{
    public static String filterOutput(String string)
    {
        String output = new String(string);
        
        if(output.indexOf(Constants.DETAIL_TYPE_HTML) > -1)
        {
            output = output.replaceAll(Constants.DETAIL_TYPE_HTML, "");
            output = output.replaceAll(Constants.VALUE_START, "");
            output = output.replaceAll(Constants.VALUE_END, "");
            
            output = "<html><body>"+output+"</body></html>";
        }
        else
        {
            output = output.replaceAll(Constants.VALUE_START, "");
            output = output.replaceAll(Constants.VALUE_END, "");
            output = output.replaceAll("<pre>", "");
            output = output.replaceAll("</pre>", "");
        }
        
        return output;
    } // filterOutput
    
    /**
     * 
     * @param actionTaskDefinition ==> syntax = task#namespace(wiki::nodeId) or task#namespace
     * @return
     */
    public static String getActionTaskDetail(String problemId, String actionTaskDefinition)
    {
        JSONObject output = new JSONObject();
        String actionTaskFullName = null;
        String nodeId = null;
        
        try
        {
            actionTaskFullName = actionTaskDefinition.substring(0, actionTaskDefinition.indexOf("("));
            int pos = actionTaskDefinition.indexOf('(');
            if (pos > 0)
            {
                nodeId = actionTaskDefinition.substring(pos + 1, actionTaskDefinition.length()-1);
            }
        }
        catch(Exception e)
        {
            actionTaskFullName = actionTaskDefinition;
        }
        

        try
        {
            ResolveActionTaskVO rat = ServiceHibernate.getResolveActionTask(null, actionTaskFullName, "system");
            String result = ServiceHibernate.getActionResultDetail(problemId, rat.getSys_id(), nodeId);
            output.accumulate(actionTaskFullName, result);
        }
        catch(Exception e)
        {
            output.accumulate("error", e.getMessage());
        }
        
        Log.log.debug(output.toString());
        
        return filterOutput(output.toString());

    } //getActionTaskDetail
    
    /**
     * 
     * This api is to send a message to RSCONTROL to execute the wiki/runbook document.
     * 
     * 
     * @param name - name of the job scheduler, this is mandatory, eg - EMA_EVENT_ARCHIVE
     * @param wiki - wiki full name, this is mandatory, eg - EventCatalog.UpdateEventHistory
     * @param params - params to pass to the wiki, optional field
     * 
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static void sendExecuteProcess(String name, String wiki, Map params)
    {
        if(StringUtils.isNotBlank(name) && StringUtils.isNotBlank(wiki))
        {
            if(params == null)
                params = new HashMap();//to avoid the null pointers
            
            try
            {
                Map content = new Hashtable();
                content.put(Constants.ESB_PARAM_EVENTTYPE, Constants.GATEWAY_EVENT_TYPE_CRON);
                content.put(Constants.EXECUTE_PROBLEMID, "NEW");
                content.put(Constants.EXECUTE_USERID, "system");
                content.put(Constants.EXECUTE_WIKI, wiki);
                content.put(Constants.EXECUTE_REFERENCE, "Cron: "+name);
                content.putAll(params);
                
                if (ServiceWiki.isWikiDocumentExecutable(wiki))
                {
                  // send message
                  MainBase.esb.sendMessage(Constants.ESB_NAME_EXECUTEQUEUE, "MAction.executeProcess", content);
                } else {
                  Log.log.warn("You are trying to execute wiki: " + wiki + " that is not executable!");
                }
            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
    } // sendExecuteProcess
    
    /**
     * this api will send the command to a target defined.
     * 
     * @param  executeCmd - eg. RSCONTROL#MAction.archive, this is mandatory
     * @param params - optional
     */
    @SuppressWarnings("rawtypes")
    public static void sendESBRequest(String executeCmd, Map params)
    {
        if (StringUtils.isNotBlank(executeCmd))
        {
            if(params == null)
                params = new HashMap();
            
            try
            {
                int pos = executeCmd.indexOf('#');
                if (pos > 0)
                {
                    // get target and classmethod
                    String target = executeCmd.substring(0, pos);
                    String classmethod = executeCmd.substring(pos + 1, executeCmd.length());

                    // send message
                    MainBase.esb.sendMessage(target, classmethod, params);
                }
            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
    } // sendESBRequest
    
} // ExecuteUtil
