/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.persistence.Query;

import org.apache.commons.beanutils.PropertyUtils;

import com.bmc.thirdparty.org.apache.commons.collections.CollectionUtils;
import com.resolve.persistence.model.ResolveSysScript;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ResolveSysScriptVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.util.UpdateChecksumUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SystemScriptUtil
{
    public static List<ResolveSysScriptVO> getResolveSysScripts(QueryDTO query, String username)
    {
        List<ResolveSysScriptVO> result = new ArrayList<ResolveSysScriptVO>();

        int limit = query.getLimit();
        int offset = query.getStart();

        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if (data != null)
            {
                //grab the map of sysId-organizationname  
                Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();

                String selectedColumns = query.getSelectColumns();
                if (StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");

                    //this will be the array of objects
                    for (Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        ResolveSysScript instance = new ResolveSysScript();

                        //set the attributes in the object
                        for (int x = 0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];

                            PropertyUtils.setProperty(instance, column, value);
                        }

                        result.add(convertModelToVO(instance, mapOfOrganization));
                    }//end of for loop
                }
                else
                {
                    for (Object o : data)
                    {
                        ResolveSysScript instance = (ResolveSysScript) o;
                        result.add(convertModelToVO(instance, mapOfOrganization));
                    }
                }
            }
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return result;
    }

    public static ResolveSysScriptVO findResolveSysScriptByIdOrName(String sys_id, String ssName, String username)
    {
        ResolveSysScript model = null;
        ResolveSysScriptVO result = null;

        try
        {
            model = findResolveSysScriptModelByIdOrName(sys_id, ssName, username);
            if (model != null)
            {
                result = convertModelToVO(model, null);
            }
        }
        catch (Throwable t)
        {
            Log.log.warn(t.getMessage(), t);
        }

        return result;
    }

    public static ResolveSysScriptVO saveResolveSysScript(ResolveSysScriptVO vo, String username)  throws Exception
    {
        if (vo != null && vo.validate())
        {
            ResolveSysScript model = null;
            if (StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equalsIgnoreCase(VO.STRING_DEFAULT))
            {
                //update
                model = findResolveSysScriptModelByIdOrName(vo.getSys_id(), null, username);
                if(model == null)
                {
                    throw new Exception("System Script with sysId " + vo.getSys_id() + " does not exist.");
                }
                
                //check if the title is same or not, if not, than validate if the title is unique
                //this will throw exception if there are more than 1 recs with the same title
                ResolveSysScript duplicate = findResolveSysScriptModelByIdOrName(null, vo.getUName(), username);
                if(duplicate != null && !duplicate.getSys_id().equalsIgnoreCase(vo.getSys_id()))
                {
                    throw new Exception("'" + vo.getUName() + "' already exist. Please try again.");
                }
            }
            else
            {
                model = findResolveSysScriptModelByIdOrName(null, vo.getUName(), username);
                if(model != null)
                {
                    throw new Exception("'" + vo.getUName() + "' already exist. Please try again.");
                }
                
                //insert
                model = new ResolveSysScript();
            }
            model.applyVOToModel(vo);
            model.setChecksum(calculateChecksum(model));
            model = SaveUtil.saveResolveSysScript(model, username);
            vo = convertModelToVO(model, null);
        }

        return vo;
    }

    public static void deleteResolveSysScriptByIds(String[] sysIds, String username) throws Exception
    {
        if (sysIds != null)
        {
            for (String sysId : sysIds)
            {
                deleteResolveSysScriptById(sysId, username);
            }
        }
    }

    private static void deleteResolveSysScriptById(String sysId, String username) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {

	            ResolveSysScript model = HibernateUtil.getDAOFactory().getResolveSysScriptDAO().findById(sysId);
	            if (model != null)
	            {
	                HibernateUtil.getDAOFactory().getResolveSysScriptDAO().delete(model);
	            }

        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new Exception(e);
        }
    }

    private static ResolveSysScript findResolveSysScriptModelByIdOrName(String sys_id, String ssName, String username)
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	return (ResolveSysScript) HibernateProxy.execute(() -> {
        		ResolveSysScript result = null;

	            if (StringUtils.isNotBlank(sys_id))
	            {
	                result = HibernateUtil.getDAOFactory().getResolveSysScriptDAO().findById(sys_id);
	            }
	            else if (StringUtils.isNotBlank(ssName))
	            {
	                ResolveSysScript ss = new ResolveSysScript();
	                ss.setUName(ssName);
	                result = HibernateUtil.getDAOFactory().getResolveSysScriptDAO().findFirst(ss);
	            }

	            return result;
        	});

        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return null;
    }

    private static ResolveSysScriptVO convertModelToVO(ResolveSysScript model, Map<String, String> mapOfOrganization)
    {
        ResolveSysScriptVO vo = null;

        if (mapOfOrganization == null)
        {
            mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
        }

        if (model != null)
        {
            vo = model.doGetVO();
            if (StringUtils.isNotBlank(vo.getSysOrg()))
            {
                vo.setSysOrganizationName(mapOfOrganization.get(vo.getSysOrg()));
            }
            else
            {
                vo.setSysOrganizationName("");
            }
        }

        return vo;
    }
    

    /**
     * API to be invoked from update/upgrade script only!
     */
    public static void updateAllSystemScriptsForChecksum()
    {
        try {
            UpdateChecksumUtil.updateComponentChecksum("ResolveSysScript", SystemScriptUtil.class);
        }
        catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static Integer calculateChecksum(ResolveSysScript script)
    {
        return Objects.hash(script.getUName(), script.getUScript());
    }
    
    public static Integer getSystemScriptChecksum(String name, String username)
    {
        Integer checksum = null;
        ResolveSysScript script = findResolveSysScriptModelByIdOrName(null, name, username);
        if (script != null)
        {
            checksum = script.getChecksum();
        }
        return checksum;
    }
}
