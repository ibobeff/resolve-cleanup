package com.resolve.services.exception;

public class VersionLimitReachedException extends RuntimeException {

	public VersionLimitReachedException(String message) {
		super(message);
	}

}
