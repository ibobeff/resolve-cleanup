/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved This software is distributed under license and may not be
 * used, copied, modified, or distributed without the express written permission
 * of Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.util;

import static com.resolve.util.Log.log;
import static org.apache.commons.lang3.StringUtils.*;

import java.time.Duration;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.hibernate.query.Query;

import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.PlaybookActivities;
import com.resolve.persistence.model.ResolveSecurityIncident;
import com.resolve.persistence.model.SIRActivityMetaData;
import com.resolve.persistence.model.SIRActivityRuntimeData;
import com.resolve.persistence.model.SIRPhaseMetaData;
import com.resolve.persistence.model.SIRPhaseRuntimeData;
import com.resolve.persistence.model.Users;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.sir.util.ActivityUtils;
import com.resolve.services.sir.util.PhaseUtils;
import com.resolve.util.Guid;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class PlaybookActivityUtils
{
    public static final String DUPLICATE_ACTIVITIES_TASKS_IN_PHASE_PROHIBITED_MSG = "Duplicate activities/tasks in a phase are prohibited.";
    
    private static final String FAILED_TO_CREATE_ORG_SPECIFIC_PLAYBOOK_ACTIVITIES_LOG = 
    										"Failed to create Org specific playbook activities for specified playbook " +
    										"template %s with version number %d for Org Id %s.";
    private static final String ERROR_GETTING_PLAYBOOK_ACTIVITIES_LOG = 
    										"Error %s in getting Playbok Activities for %s Playbook Template Full Name =" + 
    										" %s, Playbook Template Version = %d.";
    private static final String ERROR_CONVERTING_SPECIFIED_SLA_UNITS_TO_LONG_LOG = 
    												"Error %s occurred while converting specified SLA %s %s value to " +
    												Long.class.getName() + ".";
    private static final String ADD_ACTIVITY_SLA_DAYS_HOURS_MINUTES_LOG = 
    																"Add Activity SLA: %s, Activity Map Values: %sD%sH%sM";
    private static final String ADDED_NEW_SIR_PHASE_RUNTIME_DATA_LOG = "Added new SIR Phase Runtime Data: %s.";
    private static final String ACTIVITY_ALREADY_PRESENT_IN_PHASE_LOG = 
    																"Activity/Task %s is already present in Phase %s. " + 
    																DUPLICATE_ACTIVITIES_TASKS_IN_PHASE_PROHIBITED_MSG;
    private static final String ADDED_NEW_SIR_ACTIVITY_RUNTIME_DATA_LOG = "Added new SIR Activity Runtime Data: %s.";
    private static final String FAILED_TO_CREATE_PHASE_ACTIVITY_RUNTIME_DATA_LOG = 
    														"Failed to create SIR phase and activity runtime data. %s";
    private static final String FAILED_TO_INCREMENT_SIR_REF_COUNT_OF_WIKI_DOC_LOG = 
    												"Failed to increment SIR reference count of the wiki document %s. %s";
    private static final String UNABLE_TO_ADD_PLAYBOOK_ACTIVITY_TO_INCIDENT_LOG = 
    											"Unable to add Playbok Activity for IncidentId = %s, ActivityJSON = %s.%s";
    private static final String UNABLE_TO_SAVE_PLAYBOOK_ACTIVITIES_FOR_INCIDENT_LOG = 
    											"Unable to save Playbok Activities for IncidentId = %s, Activities " +
    											"JSON Array = %s.";
    private static final String FAILED_TO_INCREMENT_SIR_REFERENCE_COUNT_OF_WIKI_DOC_BY_LOG = 
    												"Failed to add %d to SIR reference count of the wiki document %s. $s";
    private static final String UNABLE_TO_DETERMINE_IF_SIR_HAS_ANY_CUSTOM_ACTIVITIES_LOG = 
    												"Unable to determine if SIR with id %s has any custom activities.";
            
    @SuppressWarnings("unchecked")
    public static JSONArray getPbActivities(String incidentId, String playbookTemplateFullName, Integer version, 
    										String orgId, String username) throws Exception
    {
        
        
        if (StringUtils.isNotBlank(incidentId) || 
            (StringUtils.isNotBlank(playbookTemplateFullName) && 
             version != null && version.intValue() > 0))
        {
            try
            {
            	return (JSONArray) HibernateProxy.execute(() -> {
            		JSONArray pbaJSONArray = null;
                    List<Object> pbas = null;
                    
                    if (StringUtils.isNotBlank(incidentId))
                    {
                        // fetch the playbook activities based on incidentId first
                        StringBuilder sql = new StringBuilder("select pbas from PlaybookActivities as pbas where pbas.USirSysId = :USirSysId");
        
                        Query query = HibernateUtil.createQuery(sql.toString());
                        
                        query.setParameter("USirSysId", incidentId);
                        
                        pbas = query.list();
                    }
                    
                    if (pbas != null && pbas.size() == 1)
                    {
                        pbaJSONArray = StringUtils.stringToJSONArray(((PlaybookActivities)pbas.get(0)).getUActivitiesJSON());
                    }
                    
                    if ((pbaJSONArray == null || pbaJSONArray.isEmpty()) && 
                        StringUtils.isNotBlank(playbookTemplateFullName) && 
                        version != null && version.intValue() > 0)
                    {
                        StringBuilder sql2 = new StringBuilder("select pbas from PlaybookActivities as pbas where pbas.UFullname = :UFullname and pbas.UVersion = :UVersion");
                        
                        if (isNotBlank(orgId)) {
                        	sql2.append(" and pbas.sysOrg = :sysOrg");
                        } else {
                        	sql2.append(" and (pbas.sysOrg is null or pbas.sysOrg = '')");
                        }
                        
                        Query query2 = HibernateUtil.createQuery(sql2.toString());
                        
                        query2.setParameter("UFullname", playbookTemplateFullName);
                        query2.setParameter("UVersion", version);
                        
                        if (isNotBlank(orgId)) {
                        	query2.setParameter("sysOrg", orgId);
                        }
                        
                        pbas = query2.list();
                        
                        if (pbas != null && pbas.size() == 1) {
                        	pbaJSONArray = StringUtils.stringToJSONArray(((PlaybookActivities)pbas.get(0)).getUActivitiesJSON());
                        }
                        
                        if ((pbaJSONArray == null || pbaJSONArray.isEmpty()) && 
                            StringUtils.isNotBlank(playbookTemplateFullName) && 
                            version != null && version.intValue() > 0 && isNotBlank(orgId)) {
                        	
                        	String sql3 = "select pbas from PlaybookActivities as pbas where " +
                        				  "pbas.UFullname = :UFullname and " +
                        				  "pbas.UVersion = :UVersion and " +
                        				  "(pbas.sysOrg is null or pbas.sysOrg = '')";
                        	
                        	Query query3 = HibernateUtil.createQuery(sql3);
                            
                            query3.setParameter("UFullname", playbookTemplateFullName);
                            query3.setParameter("UVersion", version);
                            
                            pbas = query3.list();
                            
                            if (pbas != null && pbas.size() == 1)
                            {
                                pbaJSONArray = StringUtils.stringToJSONArray(((PlaybookActivities)pbas.get(0)).getUActivitiesJSON());
                                
                                /*
                            	 * Template phase and activity meta data is for None Org.
                            	 * Replace None Org phase and activity meta data with 
                            	 * existing /new Org specific phase and activity meta data.
                            	 */
                            	
                                JSONArray txedPBAJSONArray = txfromPBActivitiesForOrg(pbaJSONArray, orgId, username);
                            	
                            	PlaybookActivities pbActivities = new PlaybookActivities();
                                
                                pbActivities.setSysCreatedBy(username);
                                pbActivities.setSysCreatedOn(new Date());
                                pbActivities.setSysModCount(new Integer(0));
                                pbActivities.setSysOrg(orgId);
                                
                                pbActivities.setUFullname(playbookTemplateFullName);
                                pbActivities.setUVersion(version);
                                
                                pbActivities.setUActivitiesJSON(StringUtils.jsonArrayToString(txedPBAJSONArray));
                                
                                // Persist Org specific template playbook activities
                                
                                PlaybookActivities nPBActivities = HibernateUtil.getDAOFactory().getPlaybookActivitiesDAO().persist(pbActivities);
                                
                                if (nPBActivities == null) {
                                	Log.log.error(String.format(FAILED_TO_CREATE_ORG_SPECIFIC_PLAYBOOK_ACTIVITIES_LOG, 
                                								playbookTemplateFullName, version.intValue(), orgId));
                                	throw new Exception("Failed to create Org specific playbook activities for specified " +
                                				  		"playbook template with specifiec version number for specified Org. ");
                                }
                                
                                pbaJSONArray = StringUtils.stringToJSONArray(nPBActivities.getUActivitiesJSON());
                            }
                        }
                    }
                    return pbaJSONArray;
            	});
            }
            catch (Throwable t)
            {
                Log.log.error(String.format(ERROR_GETTING_PLAYBOOK_ACTIVITIES_LOG, t.getLocalizedMessage(),
                              (StringUtils.isNotBlank(incidentId) ? "IncidentId = " + incidentId + ", " : "") +
                              playbookTemplateFullName, version), t);
                
                throw t;
            }
        }
        
        return null;
        
    }// getPbActivities
    
    @SuppressWarnings({ "unchecked", "unused" })
    private static void upsertPbActivities(String incidentId, String playbookTemplateFullName, Integer version, JSONArray pbaJSONArray, String username) throws Exception
    {
        PlaybookActivities dbPlaybookActivities = null;
        PlaybookActivities upsertPlaybookActivities = null;
        
        StringBuilder sql = new StringBuilder("select pbas from PlaybookActivities as pbas where pbas.USirSysId = :USirSysId");

        Query query = HibernateUtil.createQuery(sql.toString());
        
        query.setString("USirSysId", incidentId);
        
        List<Object> pbas = query.list();
        
        if (pbas != null && pbas.size() == 1)
        {
            dbPlaybookActivities = (PlaybookActivities)pbas.get(0);
        }
        
        Date currTimeStamp = new Date();
        
        if (dbPlaybookActivities != null)
        {
            dbPlaybookActivities.setUFullname(null);
            dbPlaybookActivities.setUVersion(new Integer(0));
            dbPlaybookActivities.setUSirSysId(incidentId);
            
            dbPlaybookActivities.setSysUpdatedBy(username);
            dbPlaybookActivities.setSysUpdatedOn(currTimeStamp);
            dbPlaybookActivities.setSysModCount(dbPlaybookActivities.getSysModCount() + 1);
            
            dbPlaybookActivities.setUActivitiesJSON(StringUtils.jsonArrayToString(pbaJSONArray));
            
            upsertPlaybookActivities = HibernateUtil.getDAOFactory().getPlaybookActivitiesDAO().update(dbPlaybookActivities);
        }
        else
        {
            dbPlaybookActivities = new PlaybookActivities();
            
            dbPlaybookActivities.setUFullname(null);
            dbPlaybookActivities.setUVersion(new Integer(0));
            dbPlaybookActivities.setUSirSysId(incidentId);
            
            dbPlaybookActivities.setSysCreatedBy(username);
            dbPlaybookActivities.setSysCreatedOn(currTimeStamp);
            dbPlaybookActivities.setSysUpdatedOn(currTimeStamp);
            dbPlaybookActivities.setSysModCount(new Integer(0));
            
            dbPlaybookActivities.setUActivitiesJSON(StringUtils.jsonArrayToString(pbaJSONArray));
            
            upsertPlaybookActivities = HibernateUtil.getDAOFactory().getPlaybookActivitiesDAO().persist(dbPlaybookActivities);
        }
    }
    
    public static JSONObject addPbActivity(String incidentId, String playbookTemplateFullName, Integer version, 
    									   String activityJSON, String orgId, String username) throws Exception
    {
        JSONObject npbaJSONObj = null;
        
        if (StringUtils.isNotBlank(incidentId) && StringUtils.isNotBlank(activityJSON))
        {
            Map<String, String> activityMap = StringUtils.jsonObjectToMap((StringUtils.stringToJSONObject(activityJSON)));
            
            /*
             * Create Phase MetaData (for Resolve 6.2 i.e. SIR 2.0 and above only), if not found.
             * Phase Meta Data cannot be updated and is unique per Org.
             * 
             * Unique Phase Meta Data Key
             * 
             * Phase Name + Org
             */
            
            SIRPhaseMetaData sirPhaseMetaData = 
                            PhaseUtils.createSIRPhaseMetaData(
                                            activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY), 
                                                            orgId, username);
            
            if (sirPhaseMetaData == null)
            {
                throw new Exception("Failed to create SIR phase meta data.");
            }
            
            activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY, 
                            sirPhaseMetaData.getSys_id());
            
            /*
             * Create Activity MetaData (for Resolve 6.2 i.e. SIR 2.0 and above only), if not found.
             * Activity Meta Data cannot be updated and is unique per Org.
             * 
             * Unique Activity Meta Data Keys
             * 
             * Org + Referenced Wiki Id + is Required + SLA Duration 
             * Org + Name (unique)
             * 
             * Please check SIRActivityMetaData legacyName comments.
             */
            
            Duration sla = SIRActivityMetaData.DEFAULT_SLA_DURATION;
            
            if (activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY) &&
                StringUtils.isNotBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY)))
            {
                try
                {
                    Long slaDays = Long.parseLong(
                    					activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY), 10);
                    
                    if ((Long.MAX_VALUE -  TimeUnit.NANOSECONDS.convert(slaDays, TimeUnit.DAYS)) > 0)
                    {
                        sla = sla.plusDays(slaDays);
                    }
                    else
                    {
                        sla = sla.plusDays(Long.MAX_VALUE / TimeUnit.NANOSECONDS.convert(1, TimeUnit.DAYS));
                    }
                }
                catch (NumberFormatException nfe)
                {
                    Log.log.warn(String.format(ERROR_CONVERTING_SPECIFIED_SLA_UNITS_TO_LONG_LOG, nfe.getLocalizedMessage(),
                    						   activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY),
                                 			   WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY), nfe);
                }
            }
            
            if (sla.toNanos() < Long.MAX_VALUE && 
            	activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY) &&
                StringUtils.isNotBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY)))
            {
                try
                {
                    Long slaHours = Long.parseLong(
                    					activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY), 10);
                    
                    if ((Long.MAX_VALUE - sla.toNanos()) > TimeUnit.NANOSECONDS.convert(slaHours, TimeUnit.HOURS))
                    {
                        sla = sla.plusHours(slaHours);
                    }
                    else
                    {
                        sla = sla.plusHours((Long.MAX_VALUE - sla.toNanos()) / 
                        					TimeUnit.NANOSECONDS.convert(1, TimeUnit.HOURS));
                    }
                }
                catch (NumberFormatException nfe)
                {
                    Log.log.warn(String.format(ERROR_CONVERTING_SPECIFIED_SLA_UNITS_TO_LONG_LOG, nfe.getLocalizedMessage(),
                    						   activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY),
                    						   WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY), nfe);
                }
            }
            
            if (sla.toNanos() < Long.MAX_VALUE && activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY) &&
                StringUtils.isNotBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY)))
            {
                try
                {
                    Long slaMinutes = Long.parseLong(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY), 10);
                    
                    if ((Long.MAX_VALUE - sla.toNanos()) > TimeUnit.NANOSECONDS.convert(slaMinutes, TimeUnit.MINUTES))
                    {
                        sla = sla.plusMinutes(slaMinutes);
                    }
                    else
                    {
                        sla = sla.plusMinutes((Long.MAX_VALUE - sla.toNanos()) / TimeUnit.NANOSECONDS.convert(1, TimeUnit.MINUTES));
                    }
                }
                catch (NumberFormatException nfe)
                {
                    Log.log.warn(
                    		String.format(ERROR_CONVERTING_SPECIFIED_SLA_UNITS_TO_LONG_LOG, nfe.getLocalizedMessage(),
                    					  activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY),
                    					  WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY), nfe);
                }
            }
            
            /*
             * DO NOT REMOVE : SLA granularity is up to minutes only.
             * SLA is persisted in DB with default granularity which 
             * is nano seconds. Code below normalizes SLA to minutes
             * granularity.
             */
            
            if (!sla.isZero())
            {
                sla = Duration.ofMinutes(sla.toMinutes());
            }
            
            activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY, Long.toString(sla.toDays()));
            activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY, Long.toString(sla.toHours() - (TimeUnit.HOURS.convert(sla.toDays(), TimeUnit.DAYS))));
            activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY, Long.toString(sla.toMinutes() - (TimeUnit.MINUTES.convert(sla.toHours(), TimeUnit.HOURS))));
            
            Log.log.debug(String.format(ADD_ACTIVITY_SLA_DAYS_HOURS_MINUTES_LOG, sla,
                            			activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_DAYS_KEY), 
                            			activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_HOURS_KEY), 
                            			activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_SLA_MINUTES_KEY)));
            
            if (isBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY)))
            {
                activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY, Guid.getGuid());
            }
            
            if (isBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_IS_REQUIRED_KEY)))
            {
                activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_IS_REQUIRED_KEY, SIRActivityMetaData.DEFAULT_IS_REQAUIRED.toString());
            }
            
            SIRActivityMetaData sirActivityMetaData = 
                            ActivityUtils.createSIRActivityMetaData(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY), 
                                                                    activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_DESCRIPTION_KEY), 
                                                                    Boolean.valueOf(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_IS_REQUIRED_KEY)).booleanValue(), 
                                                                    activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY), null, 
                                                                    Boolean.valueOf(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_TEMPLATE_ACTIVITY_KEY)).booleanValue(), 
                                                                    sla, activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY),
                                                                    orgId, username);
            
            if (sirActivityMetaData == null)
            {
                throw new Exception("Failed to create SIR activity meta data.");
            }
            
            activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY, 
                            sirActivityMetaData.getName());
            activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ALT_ACTIVITY_ID_KEY, 
                            sirActivityMetaData.getAltActivityId());
            activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY,
                            sirActivityMetaData.getSys_id());
            activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_TEMPLATE_ACTIVITY_KEY,
            				sirActivityMetaData.ugetIsTemplateActivity().toString());
            
            validateActivity(activityMap);
            
            if (activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY) && 
                StringUtils.isNotBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY)) &&
                activityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY) && 
                StringUtils.isNotBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY)))
            {
                try
                {
                    try
                    {
                      HibernateProxy.setCurrentUser(username);
                        HibernateProxy.execute(() -> {
                        
	                        // Find phase runtime data without index as playbook can only have unique phases
	                    
	                        SIRPhaseRuntimeData dbSIRPhaseRuntimeData = 
	                                    PhaseUtils.findSIRPhaseRuntimeData(sirPhaseMetaData.getSys_id(), null,
	                                                                       incidentId, orgId, username);
	                        
	                        if (dbSIRPhaseRuntimeData == null)
	                        {
	                            // New Phase
	                            
	                            int runtimeDataPhaseCount = PhaseUtils.getRuntimeDataPhaseCount(incidentId, orgId, 
	                                                                                            username);
	                            
	                            ResolveSecurityIncident referencedRSI = new ResolveSecurityIncident();
	                            
	                            referencedRSI.setSys_id(incidentId);
	                            referencedRSI.setPrimary(null);
	                            referencedRSI.setSirStarted(null);
	                            
	                            if (isNotBlank(orgId)) {
	                                referencedRSI.setSysOrg(orgId);
	                            }
	                            
	                            dbSIRPhaseRuntimeData = 
	                                        PhaseUtils.setupSIRPhaseRuntimeData(
	                                                        sirPhaseMetaData, referencedRSI,
	                                                        Integer.valueOf(runtimeDataPhaseCount + 1), orgId);
	                            
	                            SIRPhaseRuntimeData insertedSIRPhaseRuntimeData = 
	                                        HibernateUtil.getDAOFactory().getSIRPhaseRuntimeDataDAO().insertOrUpdate(
	                                                            dbSIRPhaseRuntimeData);
	                            
	                            dbSIRPhaseRuntimeData = insertedSIRPhaseRuntimeData;
	                            
	                            Log.log.debug(String.format(ADDED_NEW_SIR_PHASE_RUNTIME_DATA_LOG, dbSIRPhaseRuntimeData));
	                        }
	                        
	                        /*
	                         * Find activity runtime data without index as playbook phase 
	                         * can only have unique custom activities
	                         */
	                        
	                        SIRActivityRuntimeData dbSIRActivityRuntimeData = 
	                                        ActivityUtils.findSIRActivityRuntimeData(
	                                                        sirPhaseMetaData.getSys_id(),
	                                                        dbSIRPhaseRuntimeData.getPosition(),
	                                                        sirActivityMetaData.getSys_id(),
	                                                        null, incidentId, orgId, username);
	                        
	                        if (dbSIRActivityRuntimeData != null)
	                        {
	                            Log.log.warn(String.format(ACTIVITY_ALREADY_PRESENT_IN_PHASE_LOG, sirActivityMetaData,
	                            						   sirPhaseMetaData));
	                            throw new Exception("Found duplicte activity/task in sir/case phase. " + DUPLICATE_ACTIVITIES_TASKS_IN_PHASE_PROHIBITED_MSG);
	                        }
	                        
	                        int runtimeDataActivityCount = 
	                                        ActivityUtils.getRuntimeDataActivityCount(incidentId, 
	                                                                                  sirPhaseMetaData.getSys_id(),
	                                                                                  dbSIRPhaseRuntimeData.getPosition(),
	                                                                                  orgId, username);
	                        
	                        ResolveSecurityIncident referencedRSI = new ResolveSecurityIncident();
	                        
	                        referencedRSI.setSys_id(incidentId);
	                        referencedRSI.setPrimary(null);
	                        referencedRSI.setSirStarted(null);
	                        
	                        if (isNotBlank(orgId)) {
	                            referencedRSI.setSysOrg(orgId);
	                        }
	                        
	                        dbSIRActivityRuntimeData = ActivityUtils.setupSIRActivityRuntimeData(
	                                                            sirPhaseMetaData, 
	                                                            sirActivityMetaData, 
	                                                            referencedRSI, 
	                                                            Integer.valueOf(runtimeDataActivityCount + 1), 
	                                                            orgId);
	                        
	                        // Set status to specified value and if not specified then default to Open
	                        
	                        if (isNotBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_STATUS_KEY)))
	                        {
	                            dbSIRActivityRuntimeData.setStatus(
	                                            activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_STATUS_KEY));
	                        }
	                        else
	                        {
	                            dbSIRActivityRuntimeData.setStatus(WikiDocumentVO.PLAYBOOK_ACTIVITY_RUNTIME_STATUS_DEFAULT_VALUE);
	                        }
	                        
	                        // Set assignee if specified
	                        
	                        if (isNotBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_ASSIGNEE_KEY)))
	                        {
	                            Users assignedUser = UserUtils.findUser(null, 
	                                                                    activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACTIVITY_RUNTIME_ASSIGNEE_KEY));
	                            
	                            if (assignedUser != null)
	                            {
	                                dbSIRActivityRuntimeData.setAssignee(assignedUser.getUUserName());
	                                dbSIRActivityRuntimeData.setAssignedUser(assignedUser);
	                            }
	                        }
	                        
	                        SIRActivityRuntimeData insertedSIRActivityRuntimeData = 
	                                        HibernateUtil.getDAOFactory().getSIRActivityRuntimeDataDAO().insertOrUpdate(
	                                                        dbSIRActivityRuntimeData);
	                        
	                        Log.log.debug(String.format(ADDED_NEW_SIR_ACTIVITY_RUNTIME_DATA_LOG, 
	                        							insertedSIRActivityRuntimeData));
	                        
	                        /*
	                         *  Persist Resolve Security Incident (and any other objects in session)
	                         *  even if outer transactions are not committed yet.
	                         */
                        
                    	});
                    }
                    catch(Throwable e)
                    {
                        if (StringUtils.isNotBlank(e.getLocalizedMessage()) &&
                            e.getLocalizedMessage().contains(DUPLICATE_ACTIVITIES_TASKS_IN_PHASE_PROHIBITED_MSG))
                        {
                            Log.log.warn(String.format(FAILED_TO_CREATE_PHASE_ACTIVITY_RUNTIME_DATA_LOG, 
                                         			   e.getLocalizedMessage()));
                        }
                        else
                        {
                            Log.log.error(String.format(FAILED_TO_CREATE_PHASE_ACTIVITY_RUNTIME_DATA_LOG, 
                      			   						e.getLocalizedMessage()), e);
                        }
                                              HibernateUtil.rethrowNestedTransaction(e);

                    }
                    
                    npbaJSONObj = (JSONObject) HibernateProxy.execute(() -> {
                    
	                    // First try to get last activity order for phase (if it exists)
	                    
	                    JSONArray pbaJSONArray = PlaybookActivityUtils.getPbActivities(incidentId, playbookTemplateFullName, 
	                    															   version, orgId, username);
	                    
	                    JSONArray nPBAJSONArray = new JSONArray();
	                    
	                    boolean foundPhase = false;
	                    
	                    int i = 0;
	                    
	                    for (; pbaJSONArray != null && i < pbaJSONArray.size(); i++)
	                    {
	                        Map<String, String> dbActivityMap = StringUtils.jsonObjectToMap(pbaJSONArray.getJSONObject(i));
	                        
	                        if (!foundPhase)
	                        {
	                            if (dbActivityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY) && 
	                                dbActivityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY).equals(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY)))
	                            {
	                                foundPhase = true;
	                            }
	                        }
	                        
	                        if (foundPhase)
	                        {
	                            if (dbActivityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY).equals(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY)))
	                            {
	                                if (dbActivityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY) &&
	                                    StringUtils.isNotBlank(dbActivityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY)))
	                                {
	                                    nPBAJSONArray.add(pbaJSONArray.getJSONObject(i));
	                                }
	                            }
	                            else
	                            {
	                                break;
	                            }
	                        }
	                        else
	                        {              
	                            nPBAJSONArray.add(pbaJSONArray.getJSONObject(i));
	                        }
	                    }
	                    
	                    activityMap.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_INCIDENT_ID_KEY, incidentId);
	                    
	                    JSONObject tempNpbaJSONObj = new JSONObject();
	                    
	                    tempNpbaJSONObj.putAll(activityMap);
	                    
	                    nPBAJSONArray.add(tempNpbaJSONObj);
	                    
	                    for (; pbaJSONArray != null && i < pbaJSONArray.size(); i++)
	                    {
	                        Map<String, String> dbActivityMap = StringUtils.jsonObjectToMap(pbaJSONArray.getJSONObject(i));
	                        
	                        if (dbActivityMap.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY) && 
	                            !dbActivityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY).equals(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY)))
	                        {
	                            nPBAJSONArray.add(pbaJSONArray.getJSONObject(i));
	                        }
	                    }
	                    
	                    upsertPbActivities(incidentId, playbookTemplateFullName, version,
	                                       nPBAJSONArray, username);
	                    
	                    return tempNpbaJSONObj;
                    
                    });
                    
                    WikiDocument activityDocModel = WikiUtils.getWikiDocumentModel(null, activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY), "system", false);
                    WikiDocumentVO activityDoc = activityDocModel.doGetVO();
                    HibernateUtil.getCurrentSession().evict(activityDocModel);
                    
                    activityDoc.setUSIRRefCount(activityDoc.getUSIRRefCount().longValue() + 1);
                    
                    // No possibility of recursion as playbook cannot reference itself or any other playbook
                    try
                    {
                      HibernateProxy.setCurrentUser(username);
                        HibernateProxy.execute(() -> {
	                        WikiDocument activityWikiDoc = new WikiDocument();
	                        activityWikiDoc.applyVOToModel(activityDoc);
	                        HibernateUtil.getDAOFactory().getWikiDocumentDAO().update(activityWikiDoc);
                        });
                    }
                    catch(Throwable e)
                    {
                        Log.log.warn(String.format(FAILED_TO_INCREMENT_SIR_REF_COUNT_OF_WIKI_DOC_LOG, 
                        						   activityDoc.getUFullname(), e.getMessage()), e);
                                              HibernateUtil.rethrowNestedTransaction(e);

                    }
                }
                catch (Throwable t)
                {
                    if (StringUtils.isNotBlank(t.getLocalizedMessage()) &&
                        t.getLocalizedMessage().contains(DUPLICATE_ACTIVITIES_TASKS_IN_PHASE_PROHIBITED_MSG) ||
                        (t.getCause() != null && StringUtils.isNotBlank(t.getCause().getLocalizedMessage()) &&
                         t.getCause().getLocalizedMessage().contains(DUPLICATE_ACTIVITIES_TASKS_IN_PHASE_PROHIBITED_MSG)))
                    {
                        Log.log.warn(String.format(UNABLE_TO_ADD_PLAYBOOK_ACTIVITY_TO_INCIDENT_LOG, incidentId, activityJSON,
                                     			   " " + DUPLICATE_ACTIVITIES_TASKS_IN_PHASE_PROHIBITED_MSG));
                    }
                    else
                    {
                        Log.log.error(String.format(UNABLE_TO_ADD_PLAYBOOK_ACTIVITY_TO_INCIDENT_LOG, 
                        							incidentId, activityJSON, " "), t);
                    }
                    
                    throw t;
                }
            }
        }
        
        return npbaJSONObj;
    }
    
    /*
     * General validation of activity to check whether it has Phase, Activity Name and Wiki associated
     * with it. If any of these are missing, invalidate the activity save operation. 
     */
    private static void validateActivity(Map<String, String> activityMap) throws Exception
    {
        if (StringUtils.isBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_PHASE_KEY)) ||
                        StringUtils.isBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_ACITIVITY_NAME_KEY)) ||
                        StringUtils.isBlank(activityMap.get(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY)))
        {
            throw new Exception("Activity name, Phase name and Wiki cannot be blank.");
        }           
    }
    
    public static void savePbActivities(String incidentId, String playbookTemplateFullName, Integer version, JSONArray pbaJSONArray, String username) throws Exception
    {
        if (StringUtils.isNotBlank(incidentId) && pbaJSONArray != null && !pbaJSONArray.isEmpty() &&
            StringUtils.isNotBlank(playbookTemplateFullName) && 
            version != null && version.intValue() > 0)
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() ->                
                upsertPbActivities(incidentId, playbookTemplateFullName, version,
                                   pbaJSONArray, username));               
                
            }
            catch (Throwable t)
            {
                Log.log.info(String.format(UNABLE_TO_SAVE_PLAYBOOK_ACTIVITIES_FOR_INCIDENT_LOG,
                						   StringUtils.jsonArrayToString(pbaJSONArray)), t);
                                
                throw t;
            }
        }
    }
    
    public static Map<String, Integer> getReferencedWikiNamesWithCounts(String playbookTemplateFullName, Integer version,
    																	String username) throws Exception
    {
        Map<String, Integer> refWikisWithCount = new HashMap<String, Integer>();
        
        JSONArray pbActivityJSONArray = getPbActivities(null, playbookTemplateFullName, version, null, username);
        
        refWikisWithCount = getReferencedWikiNamesWithCounts(pbActivityJSONArray);
        
        return refWikisWithCount;
    }
    
    public static Map<String, Integer> getReferencedWikiNamesWithCounts(JSONArray pbActivityJSONArray)
    {
        Map<String, Integer> refWikisWithCount = new HashMap<String, Integer>();
        
        if (pbActivityJSONArray != null && !pbActivityJSONArray.isEmpty())
        {
            for(int i = 0; i <  pbActivityJSONArray.size(); i++)
            {
                JSONObject pbActivityJSONObj = pbActivityJSONArray.getJSONObject(i);
                
                if (pbActivityJSONObj.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY) &&
                    StringUtils.isNotBlank(pbActivityJSONObj.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY)))
                {
                    Integer refCnt = refWikisWithCount.get(pbActivityJSONObj.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY));
                    
                    if (refCnt == null)
                    {
                        refWikisWithCount.put(pbActivityJSONObj.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY), new Integer(0));
                        refCnt = refWikisWithCount.get(pbActivityJSONObj.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY));
                    }
                    
                    refWikisWithCount.put(pbActivityJSONObj.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_WIKI_NAME_KEY),
                                          new Integer(refCnt.intValue() + 1));
                }
            }
        }
        
        return refWikisWithCount;
    }
    
    public static void addPlaybookActivitySIRRefCounts(String incidentId, String playbookTemplateFullName, Integer version, 
    												   Integer addFactor, String orgId, String username) throws Exception
    {
        Map<String, Integer> refWikisWithCount = new HashMap<String, Integer>();
        
        JSONArray pbActivityJSONArray = getPbActivities(incidentId, playbookTemplateFullName, version, orgId, username);
        
        refWikisWithCount = getReferencedWikiNamesWithCounts(pbActivityJSONArray);
        
        for (String wikiName : refWikisWithCount.keySet())
        {
            WikiDocumentVO activityDoc = WikiUtils.getWikiDoc(null, wikiName, "system");
            
            if (activityDoc != null)
            {
                activityDoc.setUSIRRefCount(activityDoc.getUSIRRefCount().longValue() + addFactor.intValue());
                
                try
                {
                  HibernateProxy.setCurrentUser("system");
                    HibernateProxy.execute(() -> {
                    
	                    WikiDocument activityWikiDoc = new WikiDocument();
	                    activityWikiDoc.applyVOToModel(activityDoc);
	                    HibernateUtil.getDAOFactory().getWikiDocumentDAO().update(activityWikiDoc);
                    
                    });
                }
                catch(Throwable e)
                {
                    Log.log.warn(String.format(FAILED_TO_INCREMENT_SIR_REFERENCE_COUNT_OF_WIKI_DOC_BY_LOG, 
                    						   addFactor.intValue(),
                    						   (activityDoc != null ? activityDoc.getUFullname() : "null"),
                    						   e.getMessage()), e);
                                      HibernateUtil.rethrowNestedTransaction(e);

                }
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    public static boolean hasSIRActivities(String incidentId) throws Exception
    {
        boolean hasSIRActivities = false;
        
        if (StringUtils.isNotBlank(incidentId))
        {
            try
            {
            	hasSIRActivities = (boolean) HibernateProxy.execute(() -> {
            		 if (StringUtils.isNotBlank(incidentId))
                     {
                         // fetch the playbook activities based on incidentId first
                         StringBuilder sql = new StringBuilder("select pbas from PlaybookActivities as pbas where pbas.USirSysId = :USirSysId");
         
                         Query query = HibernateUtil.createQuery(sql.toString());
                         
                         query.setString("USirSysId", incidentId);
                         
                         List<Object> pbas = query.list();
                     
                         if (pbas != null && pbas.size() == 1)
                         {
                             return true;
                         }
                     }
            		 return false;
                });
            }
            catch (Throwable t)
            {
                Log.log.info(String.format(UNABLE_TO_DETERMINE_IF_SIR_HAS_ANY_CUSTOM_ACTIVITIES_LOG, incidentId), t);
                
                throw t;
            }
        }
        
        return hasSIRActivities;
    }
    
    private static JSONArray txfromPBActivitiesForOrg(JSONArray pbaJSONArray, String orgId, 
    												  String username) throws Exception {
    	JSONArray result = new JSONArray();

    	try {
           HibernateProxy.setCurrentUser(username);
    		result = (JSONArray) HibernateProxy.execute(() -> {
    			JSONArray txedPBAJSONArray = new JSONArray();
	            for (int i=0; i<pbaJSONArray.size(); i++) {
	                JSONObject jsonActivity = (JSONObject)pbaJSONArray.get(i);
	                
	                if (jsonActivity.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY) &&
	                    StringUtils.isNotBlank(jsonActivity.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY)) &&
	                    jsonActivity.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY) &&
	                    StringUtils.isNotBlank(jsonActivity.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY)))
	                {
	                	// Phase Meta Data
	                	
	                	if (!PhaseUtils.isStandardsPhaseByPersistenceId(
	                			jsonActivity.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY))) {
	                		// Custom phase
	                		
	            			SIRPhaseMetaData noneOrgSPMD = 
	            					HibernateUtil.getDAOFactory().getSIRPhaseMetaDataDAO().findById(
	            						jsonActivity.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY));
	            			
	            			if (noneOrgSPMD == null) {
	            				log.error("Failed to find phase meta data by meta phase id " + 
	            						  jsonActivity.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY) + 
	            						  " for None Org.");
	            				throw new Exception("Failed to find phase meta data by meta phase id for None Org.");
	            			}
	            			
	            			SIRPhaseMetaData orgSPMD = PhaseUtils.findSIRPhaseMetaData(noneOrgSPMD.getName(), orgId, 
	            																	   username);
	            			
	            			if (orgSPMD == null) {
	            				orgSPMD = PhaseUtils.createCustomSIRPhaseMetaData(noneOrgSPMD.getName(), orgId, username);
	            			
	                			if (orgSPMD == null) {
	                				log.error("Failed to create phase meta data for meta phase name " + noneOrgSPMD.getName() + 
	                						  " associated with Org id " + orgId);
	                	            throw new Exception("Failed to create phase meta data for meta phase associated with " +
	                						  			"specific Org id.");
	                			}
	            			}
	            			                			
	            			jsonActivity.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY, orgSPMD.getSys_id());
	            		}
	                	
	                	// Activity Meta Data
	                	
	            		SIRActivityMetaData noneOrgSAMD = HibernateUtil.getDAOFactory().getSIRActivityMetaDataDAO().findById(
															jsonActivity.getString(
																WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY));
	            		
	            		if (noneOrgSAMD == null) {
	        				log.error("Failed to find activity meta data by meta activity id " + 
	        						  jsonActivity.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY) + 
	        						  " for None Org.");
	        				throw new Exception("Failed to find activity meta data by meta acivity id for None Org.");
	        			}
	            		
	            		SIRActivityMetaData orgSAMD = ActivityUtils.findSIRActivityMetaData(noneOrgSAMD.getIsRequired().booleanValue(),
	            																			noneOrgSAMD.getRefWikiSysId(),
	            																			noneOrgSAMD.getSLA(), orgId, 
	            																			noneOrgSAMD.getName(), username, 
	            																			false);
	            		
	            		if (orgSAMD == null) {
	            			orgSAMD = ActivityUtils.createSIRActivityMetaData(noneOrgSAMD.getName(), 
	            															  noneOrgSAMD.getDescription(), 
	            															  noneOrgSAMD.getIsRequired().booleanValue(), 
	            															  noneOrgSAMD.getRefWikiSysId(),
	            															  noneOrgSAMD.getIsTemplateActivity().booleanValue(),
	            															  noneOrgSAMD.getLegacyName(),
	            															  noneOrgSAMD.getSLA(), 
	            															  noneOrgSAMD.getAltActivityId(), orgId, username);
	            		
	                		if (orgSAMD == null) {
	            				log.error("Failed to create activity meta data for meta activity name " + 
	            						  noneOrgSAMD.getName() + " associated with Org id " + orgId);
	            	            throw new Exception("Failed to create activity meta data for meta activity associated " +
	            						  			"with specific Org id.");
	            			}
	            		}
	            		
	            		jsonActivity.put(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY, orgSAMD.getSys_id());
	                }
	                
	                txedPBAJSONArray.add(i, jsonActivity);
	            }
	            return txedPBAJSONArray;
    		});            
           
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
                                            HibernateUtil.rethrowNestedTransaction(t);
        }
    	
    	return result;
    }
}
