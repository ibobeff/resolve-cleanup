package com.resolve.services.util;

import com.resolve.dto.SectionModel;
import com.resolve.dto.SectionType;
import com.resolve.util.JavaUtils;

public class GenericSectionUtil {
	public final static String MAIN_SECTION_ID = SectionModel.ID_PREFIX + "main";

	public static String validateSectionStr(String contentStr, SectionType sectionType) {
		StringBuffer content = new StringBuffer();
		if (sectionType == null) {
			sectionType = SectionType.SOURCE;
		}
		// if the content has no sections as of yet, define the whole content as 1
		// section
		if (contentStr.trim().startsWith("{section")) {
			content.append(contentStr);
		} else {
			content.append("{section:").append(SectionModel.TYPE).append("=").append(sectionType.getTagName())
					.append("|").append(SectionModel.TITLE).append("=").append("main").append("|")
					.append(SectionModel.ID).append("=").append(MAIN_SECTION_ID).append("|").append(SectionModel.HEIGHT)
					.append("=").append(SectionModel.DEFAULT_IFRAME_HEIGHT).append("}").append("\n");
			content.append(contentStr.trim()).append("\n");
			content.append("{section}");
		}

		return content.toString();
	}

	public static String createSectionId() {
		return (SectionModel.ID_PREFIX + JavaUtils.generateUniqueNumber());
	}// createSectionId
}
