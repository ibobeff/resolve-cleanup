/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.message;


/**
 * System notifications like CRUD operations on AT, RUnbooks, etc
 * 
 * @author jeet.marwah
 *
 */
public class SystemNotification extends Notification
{
    private static final long serialVersionUID = -1605675311002776204L;

	public SystemNotification(UserGlobalNotificationContainerType notificationType)
    {
        setType(notificationType.name());
    }
}
