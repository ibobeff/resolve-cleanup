package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.Restrictions;

import com.resolve.persistence.model.ArtifactConfiguration;
import com.resolve.persistence.model.ArtifactType;
import com.resolve.persistence.model.CEFDictionaryItem;
import com.resolve.persistence.model.CustomDictionaryItem;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ArtifactTypeVO;
import com.resolve.services.hibernate.vo.CEFDictionaryItemVO;
import com.resolve.services.hibernate.vo.CustomDictionaryItemVO;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.util.UpdateChecksumUtil;
import com.resolve.services.vo.ArtifactTypeDTO;
import com.resolve.services.vo.GenericDictionaryItemDTO;
import com.resolve.services.vo.GenericDictionaryItemDTO.ArtifactTypeSource;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.cef.CEFParseException;

public class ArtifactTypeUtil {
	private static final String ERROR_NOT_FOUND = "Artifact type not found";
	private static final String ERROR_DELETING_STANDARD = "Standard artifact type cannot be deleted";
	private static final String ERROR_INCORRECT_NAME = "Artifact type should have correct name";
	private static final String ERROR_ADDING_CEF = "Artifact type %s already has %s CEF item";
	private static final String ERROR_ADDING_CUSTOM = "Artifact type %s already has %s custom item";
	private static final String ERROR_REMOVING_CEF = "Artifact type %s doesn't have %s CEF item";
	private static final String ERROR_REMOVING_CUSTOM = "Artifact type %s doesn't have %s custom item";
	private static final String U_NAME = "UName";

	public static List<ArtifactTypeVO> getAllArtifactType() {
		List<ArtifactTypeVO> artifactTypeVOs = new ArrayList<>();
		try {
			HibernateProxy.execute(() -> {
				List<ArtifactType> result = HibernateUtil.getDAOFactory().getArtifactTypeDAO().findAll();
				for (ArtifactType at : result) {
					artifactTypeVOs.add(convertToArtifactTypeVO(at));
				}
			});
			
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}
		return artifactTypeVOs;
	}

	public static ArtifactTypeVO getArtifactTypeByName(String name) {
		
		try {
			return (ArtifactTypeVO) HibernateProxy.execute(() -> {
				ArtifactTypeVO vo = null;
				ArtifactType result = (ArtifactType) HibernateUtil.getCurrentSession().createCriteria(ArtifactType.class)
						.add(Restrictions.eq(U_NAME, name).ignoreCase()).uniqueResult();
				if (result != null) {
					vo = convertToArtifactTypeVO(result);
				}
				
				return vo;
			});		
			
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
			return null;
		}
		
	}

	public static ArtifactTypeVO saveArtifact(ArtifactTypeDTO dto) {
		ArtifactTypeVO artifactVO = null;
		try {
			if (StringUtils.isEmpty(dto.getUName())) {
				throw new IllegalArgumentException(ERROR_INCORRECT_NAME);
			}

			
			artifactVO = (ArtifactTypeVO) HibernateProxy.execute(() -> {
				ArtifactType oldArtifact = (ArtifactType) HibernateUtil.getCurrentSession().createCriteria(ArtifactType.class)
						.add(Restrictions.eq(U_NAME, dto.getUName()).ignoreCase()).uniqueResult();

				if (oldArtifact == null) {
					oldArtifact = new ArtifactType();
				}
				
				oldArtifact.setUName(dto.getUName());
				oldArtifact.setUStandard(dto.getUStandard());

				oldArtifact.setCefItems(getProcessedCEFDictionaryItems(oldArtifact, dto));
				oldArtifact.setCustomItems(getProcessedCustomDictionaryItems(oldArtifact, dto));
			ArtifactType result = HibernateUtil.getDAOFactory().getArtifactTypeDAO().insertOrUpdate(oldArtifact);
				
			updateChecksum(result);
			return convertToArtifactTypeVO(result);
				
			});
			return artifactVO;
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}
		
		return artifactVO;
	}
	
	public static ArtifactTypeVO addArtifactItem(ArtifactTypeVO artifactVO, GenericDictionaryItemDTO dto)
			throws CEFParseException {
		if (dto != null && dto.getUArtifactTypeSource() != null && dto.getUShortName() != null) {
			switch (dto.getUArtifactTypeSource()) {
				case CEF: 
					return addArtifactCEFItem(artifactVO, dto);
				case CUSTOM:
					return addArtifactCustomItem(artifactVO, dto);
				default:
					throw new IllegalArgumentException("Invalid artifact item type");
				}
		}
		throw new IllegalArgumentException("Invalid artifact item");
	}

	public static ArtifactTypeVO removeArtifactItem(ArtifactTypeVO artifactVO, GenericDictionaryItemDTO dto)
			throws CEFParseException {
		if (dto != null && dto.getUArtifactTypeSource() != null && dto.getUShortName() != null) {
			switch (dto.getUArtifactTypeSource()) {
				case CEF: 
					return removeArtifactCEFItem(artifactVO, dto);
				case CUSTOM:
					return removeArtifactCustomItem(artifactVO, dto);
				default:
					throw new IllegalArgumentException("Invalid artifact item type");
				}
		}
		throw new IllegalArgumentException("Invalid artifact item");
	}

	public static boolean deleteArtifactType(String name) {
		try {
			if (StringUtils.isEmpty(name)) {
				throw new IllegalArgumentException(ERROR_INCORRECT_NAME);
			}

			HibernateProxy.execute(() -> {
				ArtifactType oldArtifactType = (ArtifactType) HibernateUtil.getCurrentSession().createCriteria(ArtifactType.class)
						.add(Restrictions.eq(U_NAME, name.toLowerCase()).ignoreCase()).uniqueResult();
	
				if (oldArtifactType == null) {
					throw new IllegalArgumentException(ERROR_NOT_FOUND);
				} else {
					if (oldArtifactType.getUStandard()) {
						throw new IllegalArgumentException(ERROR_DELETING_STANDARD);
					}
				}
			
				HibernateUtil.getDAOFactory().getArtifactTypeDAO().delete(oldArtifactType);
			});

			return true;
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}
		
		return false;
	}

	private static ArtifactTypeVO addArtifactCEFItem(ArtifactTypeVO artifactVO, GenericDictionaryItemDTO dto) throws CEFParseException {
		CEFDictionaryItemVO cefVO = CEFUtil.getCEFDictionaryByShortName(dto.getUShortName());
		if (cefVO == null) {
			throw new IllegalArgumentException("Unknown CEF dictionary item");
		}
		
		if (artifactVO.getCefItems() != null) {
			for (CEFDictionaryItemVO vo : artifactVO.getCefItems()) {
				if (vo.getUShortName().equalsIgnoreCase(dto.getUShortName())) {
					Log.log.error(String.format(ERROR_ADDING_CEF, artifactVO.getUName(), dto.getUShortName()));
					return null;
				}
			}
		} else {
			artifactVO.setCefItems(new HashSet<CEFDictionaryItemVO>());
		}
		
		artifactVO.getCefItems().add(cefVO);
		ArtifactTypeVO result = saveArtifact(ConverterUtil.convertArtifactTypeVOToDTO(artifactVO));

		return result;
	}

	private static ArtifactTypeVO addArtifactCustomItem(ArtifactTypeVO artifactVO, GenericDictionaryItemDTO dto) throws CEFParseException {
		CustomDictionaryItemVO customVO = CustomDictionaryUtil.getCustomDictionaryByShortName(dto.getUShortName());
		if (customVO == null) {
			throw new IllegalArgumentException("Unknown custom dictionary item");
		}
		
		if (artifactVO.getCustomItems() != null) {
			for (CustomDictionaryItemVO vo : artifactVO.getCustomItems()) {
				if (vo.getUShortName().equalsIgnoreCase(dto.getUShortName())) {
					Log.log.error(String.format(ERROR_ADDING_CUSTOM, artifactVO.getUName(), dto.getUShortName()));
					return null;
				}
			}
		} else {
			artifactVO.setCustomItems(new HashSet<CustomDictionaryItemVO>());
		}
		
		artifactVO.getCustomItems().add(customVO);
		ArtifactTypeVO result = saveArtifact(ConverterUtil.convertArtifactTypeVOToDTO(artifactVO));

		return result;
	}

	private static ArtifactTypeVO removeArtifactCEFItem(ArtifactTypeVO artifactVO, GenericDictionaryItemDTO dto) throws CEFParseException {
		CEFDictionaryItemVO cefVO = CEFUtil.getCEFDictionaryByShortName(dto.getUShortName());
		if (cefVO == null) {
			throw new IllegalArgumentException("Unknown CEF dictionary item");
		}
		
		if (artifactVO.getCefItems() != null) {
			boolean itemPresent = false;
			Set<CEFDictionaryItemVO> vos = new HashSet<>();
			
			for (CEFDictionaryItemVO vo : artifactVO.getCefItems()) {
				if (vo.getUShortName().equalsIgnoreCase(dto.getUShortName())) {
					itemPresent = true;
				} else {
					vos.add(vo);
				}
			}

			if (!itemPresent) {
				Log.log.error(String.format(ERROR_REMOVING_CEF, artifactVO.getUName(), dto.getUShortName()));
				return null;
			}

			artifactVO.setCefItems(vos);
			ArtifactTypeVO result = saveArtifact(ConverterUtil.convertArtifactTypeVOToDTO(artifactVO));

			return result;
		}
		
		return null;
	}

	private static ArtifactTypeVO removeArtifactCustomItem(ArtifactTypeVO artifactVO, GenericDictionaryItemDTO dto) throws CEFParseException {
		CustomDictionaryItemVO customVO = CustomDictionaryUtil.getCustomDictionaryByShortName(dto.getUShortName());
		if (customVO == null) {
			throw new IllegalArgumentException("Unknown custom dictionary item");
		}
		
		if (artifactVO.getCustomItems() != null) {
			boolean itemPresent = false;
			Set<CustomDictionaryItemVO> vos = new HashSet<>();
			
			for (CustomDictionaryItemVO vo : artifactVO.getCustomItems()) {
				if (vo.getUShortName().equalsIgnoreCase(dto.getUShortName())) {
					itemPresent = true;
				} else {
					vos.add(vo);
				}
			}

			if (!itemPresent) {
				Log.log.error(String.format(ERROR_REMOVING_CUSTOM, artifactVO.getUName(), dto.getUShortName()));
				return null;
			}

			artifactVO.setCustomItems(vos);
			ArtifactTypeVO result = saveArtifact(ConverterUtil.convertArtifactTypeVOToDTO(artifactVO));

			return result;
		}
		return null;
	}

	private static Set<CEFDictionaryItem> getProcessedCEFDictionaryItems(ArtifactType oldArtifact, ArtifactTypeDTO dto) {
		Set<CEFDictionaryItem> items = new HashSet<>();

		if (CollectionUtils.isNotEmpty(dto.getDictionaryItems())) {
			for (GenericDictionaryItemDTO item : dto.getDictionaryItems()) {
				if (item.getUArtifactTypeSource() == ArtifactTypeSource.CEF) {
					boolean cefPresent = false;
					if (oldArtifact.getCefItems() != null) {
						for (CEFDictionaryItem cef : oldArtifact.getCefItems()) {
							if (cef.getUShortName().equalsIgnoreCase(item.getUShortName())) {
								items.add(cef);
								cefPresent = true;
								break;
							}
						}
					}
					
					// Add cef item only if it's absent
					if (!cefPresent) {
						CEFDictionaryItem cefItem = (CEFDictionaryItem) HibernateUtil.getCurrentSession()
								.createCriteria(CEFDictionaryItem.class)
								.add(Restrictions.eq(CEFUtil.U_SHORT_NAME, item.getUShortName()).ignoreCase())
								.uniqueResult();
						if (cefItem == null) {
							throw new IllegalArgumentException("Unknown CEF item");
						}
						if (cefItem.getArtifactType() != null) {
							throw new IllegalArgumentException("CEF item already assigned to artifact type");
						}
						items.add(cefItem);
					}
				}
			}
		}

		return items;
	}
	
	private static Set<CustomDictionaryItem> getProcessedCustomDictionaryItems(ArtifactType oldArtifact, ArtifactTypeDTO dto) {
		Set<CustomDictionaryItem> items = new HashSet<>();

		if (CollectionUtils.isNotEmpty(dto.getDictionaryItems())) {
			for (GenericDictionaryItemDTO item : dto.getDictionaryItems()) {
				if (item.getUArtifactTypeSource() == ArtifactTypeSource.CUSTOM) {
					boolean customPresent = false;
					if (oldArtifact.getCustomItems() != null) {
						for (CustomDictionaryItem custom : oldArtifact.getCustomItems()) {
							if (custom.getUShortName().equalsIgnoreCase(item.getUShortName())) {
								items.add(custom);
								customPresent = true;
								break;
							}
						}
					}
					
					if (!customPresent) {
						CustomDictionaryItem customItem = (CustomDictionaryItem) HibernateUtil.getCurrentSession().createCriteria(CustomDictionaryItem.class)
								.add(Restrictions.eq(CustomDictionaryUtil.U_SHORT_NAME, item.getUShortName()).ignoreCase()).uniqueResult();
						if (customItem == null) {
							CustomDictionaryUtil.saveCustomDictionary(ConverterUtil.convertGenericDTOToCustomDictionaryItemDTO(item));
							customItem = (CustomDictionaryItem) HibernateUtil.getCurrentSession().createCriteria(CustomDictionaryItem.class)
									.add(Restrictions.eq(CustomDictionaryUtil.U_SHORT_NAME, item.getUShortName()).ignoreCase()).uniqueResult();
						}
						items.add(customItem);
					}
				}
			}
		}
		
		return items;
	}
	
	private static ArtifactTypeVO convertToArtifactTypeVO(ArtifactType at) {
		ArtifactTypeVO atVO = new ArtifactTypeVO();
		atVO.setUName(at.getUName());
		atVO.setUStandard(at.getUStandard());
		atVO.setSys_id(at.getSys_id());
		
		if (CollectionUtils.isNotEmpty(at.getCefItems())) {
			Set<CEFDictionaryItemVO> vos = new HashSet<>();

			for(CEFDictionaryItem item : at.getCefItems()) {
				vos.add(item.doGetVO());
			}
			
			atVO.setCefItems(vos);
		}

		if (CollectionUtils.isNotEmpty(at.getCustomItems())) {
			Set<CustomDictionaryItemVO> vos = new HashSet<>();

			for(CustomDictionaryItem item : at.getCustomItems()) {
				vos.add(item.doGetVO());
			}
			
			atVO.setCustomItems(vos);
		}

		return atVO;
	}
	
	public static List<ArtifactType> getArtifactTypeNames(String username) {
	    List<ArtifactType> result = new ArrayList<>();
	    try {
            HibernateUtil.action(util -> {
                result.addAll(HibernateUtil.getDAOFactory().getArtifactTypeDAO().findAll());
            }, username);
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
	    return result;
	}
	
	public static ArtifactType getArtifactTypeById(String sysId, String username) {
	    ArtifactType type = null;
	    List<ArtifactType> typeList = new ArrayList<>();
	    try {
            HibernateUtil.action(util -> {
                ArtifactType localType = HibernateUtil.getDAOFactory().getArtifactTypeDAO().findById(sysId);
                if (CollectionUtils.isNotEmpty(localType.getCefItems()))
                    localType.getCefItems().size();
                if (CollectionUtils.isNotEmpty(localType.getCustomItems()))
                    localType.getCustomItems().size();
                if (CollectionUtils.isNotEmpty(localType.getArtifactConfigurations()))
                    localType.getArtifactConfigurations().size();
                typeList.add(localType);
            }, username);
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
	    if(CollectionUtils.isNotEmpty(typeList)) {
	        type = typeList.get(0);
	    }
	    return type;
	}
	
	public static Map<String, Object> getTypeComponents(String sysId, String username) {
	    Map<String, Object> result = new LinkedHashMap<>();
	    
	    ArtifactType type = getArtifactTypeById(sysId, username);
	    if (type != null) {
	        List<String> keys = new ArrayList<>();
	        Set<CEFDictionaryItem> cefItems =  type.getCefItems();
	        if (CollectionUtils.isNotEmpty(cefItems)) {
	            keys.addAll(cefItems.stream().sorted(Comparator.comparing(CEFDictionaryItem::getUShortName)) // sort in ascending order by short name
	                                         .map(key -> String.format("%s - CEF:%s", key.getUShortName(), key.getSys_id())) // map the key to the needed format
	                                         .collect(Collectors.toList())); // collect the mapped keys
	        }
	        Set<CustomDictionaryItem> customItems = type.getCustomItems();
	        if (CollectionUtils.isNotEmpty(customItems)) {
                keys.addAll(customItems.stream().sorted(Comparator.comparing(CustomDictionaryItem::getUShortName))
                                                .map(key -> String.format("%s - CUSTOM:%s", key.getUShortName(), key.getSys_id()))
                                                .collect(Collectors.toList()));
            }
	        result.put(ImpexEnum.ARTIFACT_CEF_KEY.getValue(), keys);
	    }
	    List<ArtifactConfiguration> configList = ArtifactConfigurationUtil.getConfigurationsByTypeId(sysId, username);
        if (CollectionUtils.isNotEmpty(configList)) {
            List<String> configNameList = new ArrayList<>();
            configNameList.addAll(configList.stream().map(config -> String.format("%s:%s", config.getUName(), config.getSys_id())).collect(Collectors.toList()));
            result.put(ImpexEnum.ARTIFACT_ACTION.getValue(), configNameList);
        }
	    
	    return result;
	}
	
    public static List<ArtifactTypeVO> getAllArtifactTypeNoRef(QueryDTO query, String username) {
        List<ArtifactTypeVO> result = new ArrayList<>();
        List<ArtifactType> artifactTypes = new ArrayList<>();
        
        try {
            HibernateUtil.action(util -> {
                artifactTypes.addAll(HibernateUtil.getDAOFactory().getArtifactTypeDAO().findAll(query));
            }, username);
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
        
        if (CollectionUtils.isNotEmpty(artifactTypes)) {
            result = artifactTypes.stream().map(type -> convertTypeToVONoRef(type)).collect(Collectors.toList());
        }
        return result;
    }
	
	private static ArtifactTypeVO convertTypeToVONoRef(ArtifactType type) {
	    ArtifactTypeVO vo = new ArtifactTypeVO();
	    vo.setSys_id(type.getSys_id());
	    vo.setUName(type.getUName());
	    vo.setChecksum(type.getChecksum());
	    vo.setSysUpdatedOn(type.getSysUpdatedOn());
	    return vo;
	}
	
	public static void updateChecksum(ArtifactType type) {
	    final Integer checksum = calculateTypeChecksum(type);
	    type.setChecksum(checksum);
	    try {
            HibernateUtil.action(util -> {
                HibernateUtil.getDAOFactory().getArtifactTypeDAO().update(type);
            }, "admin");
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
	}
	
	// API to be called from Update Script ONLY
	public static void updateAllArtifactTypesForChecksum() {
        try {
            UpdateChecksumUtil.updateComponentChecksum("ArtifactType", ArtifactTypeUtil.class);
        }
        catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
	
	public static Integer calculateChecksum(ArtifactType type)
    {
	    return calculateTypeChecksum(type);
    }
	
	private static Integer calculateTypeChecksum(ArtifactType type) {
	    Integer checksum = 0;
	    type = getArtifactTypeById(type.getSys_id(), "admin");
        List<Object> objList = new ArrayList<>();
        objList.add(type.getUName());
        if (CollectionUtils.isNotEmpty(type.getArtifactConfigurations()))
            objList.addAll(type.getArtifactConfigurations().stream().map(config -> config.getChecksum()).collect(Collectors.toList()));
        if (CollectionUtils.isNotEmpty(type.getCefItems()))
            objList.addAll (type.getCefItems().stream().map(cefKey -> cefKey.getChecksum()).collect(Collectors.toList()));
        if (CollectionUtils.isNotEmpty(type.getCustomItems()))
            objList.addAll (type.getCustomItems().stream().map(customKey -> customKey.getChecksum()).collect(Collectors.toList()));
        checksum = Objects.hash(objList.toArray());
	    return checksum;
	}
	
	public static void updateAllCefKeysForChecksum() {
        try {
            UpdateChecksumUtil.updateComponentChecksum("CEFDictionaryItem", ArtifactTypeUtil.class);
        }
        catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    public static Integer calculateChecksum(CEFDictionaryItem cefKey)
    {
        return Objects.hash(cefKey.getUShortName());
    }
    
    public static void updateAllCustomKeysForChecksum() {
        try {
            UpdateChecksumUtil.updateComponentChecksum("CustomDictionaryItem", ArtifactTypeUtil.class);
        }
        catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    public static Integer calculateChecksum(CustomDictionaryItem cefKey)
    {
        return Objects.hash(cefKey.getUShortName());
    }
    
    public static Integer getArtifactTypeChecksom(String name) {
        Integer checksum = null;
        ArtifactTypeVO typeVO = getArtifactTypeByName(name);
        if (typeVO != null) {
            checksum = typeVO.getChecksum();
        }
        return checksum;
    }
    
    public static Integer getCefKeyChecksom(String name, String username) {
        Integer checksum = 0;
        List<Integer> checksums = new ArrayList<>(); 
        try {
            HibernateUtil.action(util -> {
                CEFDictionaryItem cefItem = new CEFDictionaryItem();
                cefItem.setUShortName(name);
                cefItem = HibernateUtil.getDAOFactory().getCEFDictionaryDAO().findFirst(cefItem, new String[0]);
                if (cefItem != null) {
                    checksums.add(cefItem.getChecksum());
                }
            }, username);
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
        if (CollectionUtils.isNotEmpty(checksums)) {
            checksum = checksums.get(0);
        }
        return checksum;
    }

    public static Integer getCustomKeyChecksom(String name, String username) {
        Integer checksum = null;
        List<Integer> checksums = new ArrayList<>(); 
        try {
            HibernateUtil.action(util -> {
                CustomDictionaryItem custom = new CustomDictionaryItem();
                custom.setUShortName(name);
                custom = HibernateUtil.getDAOFactory().getCustomDictionaryDAO().findFirst(custom, new String[0]);
                if (custom != null) {
                    checksums.add(custom.getChecksum());
                }
            }, username);
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
        if (CollectionUtils.isNotEmpty(checksums)) {
            checksum = checksums.get(0);
        }
        return checksum;
    }
}
