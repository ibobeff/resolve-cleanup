package com.resolve.services.hibernate.actiontask;

public enum VersionPersistAction
{
    SAVE,
    COMMIT,
    UPDATE
}
