/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.migration.gateways;

import java.util.ArrayList;
import java.util.List;

import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.migration.gateways.MigrateLegacyGateways;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.SysId;

public class MigrateLegacyJMSGateway extends MigrateLegacyGateways {
    
    private String gatewayName;
    
    public MigrateLegacyJMSGateway(String gatewayName) {
        this.gatewayName = gatewayName;
    }

    public void migrate() {
        
        if(StringUtils.isBlank(gatewayName))
            return;

        Log.log.info("======= Start migration of " + gatewayName + " gateway ========");
        Log.log.info("");
        
        long startAt = System.currentTimeMillis();
        
        try {
        	HibernateProxy.execute(() -> {
            	SQLConnection conn = new SQLConnection(HibernateUtil.getCurrentSession().connection());
            	migrateStandardFields(conn);
            	migrateCustomFields(conn);
            	updateSysAppModule(gatewayName, gatewayName+" Gateway", conn);
        	});
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
                                           HibernateUtil.rethrowNestedTransaction(e);
        }
        long endAt = System.currentTimeMillis();
        
        Log.log.info("Migration is completed in " + (endAt - startAt)/100 + " seconds");
        Log.log.info("");
        
        Log.log.info("======= Finish migration of " + gatewayName + " gateway ========");
        Log.log.info("");
    }
    
/* ========================================================================== */

    public List<String> getStandardQueries() {
        
        List<String> sqlList = new ArrayList<String>();
        
        String sql1 = "INSERT INTO jms_gateway_filter (sys_id, sys_created_by, sys_created_on, sys_is_deleted, sys_mod_count, sys_org, "
                        + "sys_updated_by, sys_updated_on, u_active, u_event_eventid, u_interval, u_name, u_order, u_queue, u_runbook, u_script, u_gateway_name, "
                        + "u_deployed, u_updated, u_user, u_pass, u_port, u_ssl) "
                        + "SELECT sys_id, sys_created_by, sys_created_on, sys_is_deleted, sys_mod_count, sys_org, "
                        + "sys_updated_by, sys_updated_on, u_active, u_event_eventid, u_interval, u_name, u_order, u_queue, u_runbook, u_script, '" + gatewayName + "', ";
        
        StringBuilder sb = new StringBuilder(sql1);
        
        String modelName = gatewayName.toLowerCase() + "_filter";
        
        // Append the column names in the legacy filter table to populate the u_script and u_query fields in the new SDK filter table.
        // If those two fields are not available, simple append '' for these two fields.
        switch(modelName) {
            case "amqp_filter":
                sb.append("0, 0, '', '', '', ''");
                break;
            default:
                break;
        }
        
        sb.append("FROM ").append(modelName); 
        
        sqlList.add(sb.toString());
        
        String sql2 = "UPDATE jms_gateway_filter SET u_deployed=1 WHERE u_queue<>'" + Constants.DEFAULT_GATEWAY_QUEUE_NAME + "'";
        sqlList.add(sql2);

        return sqlList;
    }
    
    public List<String> getCustomQueries(SQLConnection conn) throws Exception {
        
        List<String> sqlList = new ArrayList<String>();
        
        String modelName = gatewayName.toUpperCase() + "_FILTER";
        switch(modelName) {
            case "AMQP_FILTER":
                List<String> filterIds = getAllFilterIds(modelName, conn);
                sqlList= getAMQPCustomQueries(filterIds, conn);
                break;
            default:
                break;
        }
        
        return sqlList;
    }
    
    private static List<String> getAMQPCustomQueries(List<String> filterIds, SQLConnection conn) throws Exception {
        
        List<String> sqlList = new ArrayList<String>();
        
        if(filterIds.size() > 0) {
            for(String filterId:filterIds) {
                String sysId = SysId.generate(conn);
                
                String sql1 = "INSERT INTO jms_gateway_filter_attr (sys_id, sys_created_by, sys_created_on, sys_is_deleted, "
                                + "sys_mod_count, sys_org, sys_updated_by, sys_updated_on, u_name, u_value, jms_filter_id) "
                                + "SELECT '" + sysId + "', sys_created_by, sys_created_on, sys_is_deleted, sys_mod_count, sys_org, sys_updated_by, sys_updated_on, "
                                + "'uformName'" + ", u_form_name, '" + filterId + "' FROM ampq_filter WHERE sys_id='" + filterId + "'";
                
                sysId = SysId.generate(conn);
                String sql2 = "INSERT INTO jms_gateway_filter_attr (sys_id, sys_created_by, sys_created_on, sys_is_deleted, "
                                + "sys_mod_count, sys_org, sys_updated_by, sys_updated_on, u_name, u_value, jms_filter_id) "
                                + "SELECT '" + sysId + "', sys_created_by, sys_created_on, sys_is_deleted, sys_mod_count, sys_org, sys_updated_by, sys_updated_on, "
                                + "'ulastValueField'" + ", u_last_value_field, '" + filterId + "' FROM ampq_filter WHERE sys_id='" + filterId + "'";
                
                Log.log.debug(sql1);
                Log.log.debug(sql2);
                
                sqlList.add(sql1);
                sqlList.add(sql2);
            }
        }
        
        return sqlList;
    }

} // class MigrateLegacyJMSGateway
