/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.exception;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.MessageFormat;

import javax.servlet.ServletException;

import com.resolve.util.Log;

/**
 * Generic Wiki exception class
 * 
 * @author jeet.marwah
 * 
 */
public class WikiException extends Exception implements WikiExceptionCodes
{
    private static final long serialVersionUID = -8793477786340868135L;
	private int module;
    private int code;
    private Throwable exception;
    private Object[] args;
    private String message;

    public WikiException(String message)
    {
        this.message = message;
    }
    
    public WikiException(Exception e)
    {
        this.exception = e;
        this.message = e.getMessage();
    }

    public WikiException(int module, int code, Exception e)
    {
        setModule(module);
        setCode(code);
        setException(e);
        this.message = e.getMessage();
    }

    public WikiException(int module, int code, String message, Throwable e, Object[] args)
    {
        setModule(module);
        setCode(code);
        setException(e);
        setArgs(args);
        setMessage(message);
        if (Log.log.isTraceEnabled()) 
        {
            Log.log.trace(getMessage(), e);
        }
    }

    public WikiException(int module, int code, String message, Throwable e)
    {
        this(module, code, message, e, null);
    }

    public WikiException(int module, int code, String message)
    {
        this(module, code, message, null, null);
    }

    public WikiException()
    {
    }

    public int getModule()
    {
        return module;
    }

    public String getModuleName()
    {
        return "" + module;
    }

    public void setModule(int module)
    {
        this.module = module;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public Throwable getException()
    {
        return exception;
    }

    public void setException(Throwable exception)
    {
        this.exception = exception;
    }

    public Object[] getArgs()
    {
        return args;
    }

    public void setArgs(Object[] args)
    {
        this.args = args;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
    
    public String getGUIMessage()
    {
//    	return "<font size=\"3\" color=\"red\">" + message + "</font>";
    	return message;
    }

    public String getMessage()
    {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Error number ");
        buffer.append(getCode());
        buffer.append(" in ");
        buffer.append(getModuleName());
        buffer.append(": ");

        if (message != null)
        {
            if (args == null)
                buffer.append(message);
            else
            {
                MessageFormat msgFormat = new MessageFormat(message);
                try
                {
                    buffer.append(msgFormat.format(args));
                }
                catch (Exception e)
                {
                    buffer.append("Cannot format message " + message + " with args ");
                    for (int i = 0; i < args.length; i++)
                    {
                        if (i != 0) buffer.append(",");
                        buffer.append(args[i]);
                    }
                }
            }
        }

        if (exception != null)
        {
            buffer.append("\nWrapped Exception: ");
            buffer.append(exception.getMessage());
        }
        return buffer.toString();
    }

    public String getFullMessage()
    {
        StringBuffer buffer = new StringBuffer(getMessage());
        buffer.append("\n");
        buffer.append(getStackTraceAsString());
        buffer.append("\n");
        /*
         * List list = XWikiBatcher.getSQLStats().getRecentSqlList(); if
         * (list.size() > 0) { buffer.append("Recent SQL:\n"); for (int i = 0; i
         * < list.size(); i++) { buffer.append(list.get(i));
         * buffer.append("\n"); } }
         */
        return buffer.toString();
    }

    public void printStackTrace(PrintWriter s)
    {
        super.printStackTrace(s);
        if (exception != null)
        {
            s.write("\n\nWrapped Exception:\n\n");
            if (exception.getCause() != null)
                exception.getCause().printStackTrace(s);
            // else if (exception instanceof org.hibernate.JDBCException)
            // {
            // (((JDBCException)
            // exception).getSQLException()).printStackTrace(s);
            // }
            // else if (exception instanceof MethodInvocationException)
            // {
            // (((MethodInvocationException)
            // exception).getWrappedThrowable()).printStackTrace(s);
            // }
            else if (exception instanceof ServletException)
            {
                (((ServletException) exception).getRootCause()).printStackTrace(s);
            }
            else
            {
                exception.printStackTrace(s);
            }
        }
    }

    public void printStackTrace(PrintStream s)
    {
        super.printStackTrace(s);
        if (exception != null)
        {
            s.print("\n\nWrapped Exception:\n\n");
            if (exception.getCause() != null)
                exception.getCause().printStackTrace(s);
            // else if (exception instanceof org.hibernate.JDBCException)
            // {
            // (((JDBCException)
            // exception).getSQLException()).printStackTrace(s);
            // }
            // else if (exception instanceof MethodInvocationException)
            // {
            // (((MethodInvocationException)
            // exception).getWrappedThrowable()).printStackTrace(s);
            // }
            else if (exception instanceof ServletException)
            {
                (((ServletException) exception).getRootCause()).printStackTrace(s);
            }
            else
            {
                exception.printStackTrace(s);
            }
        }
    }

    public String getStackTraceAsString()
    {
        StringWriter swriter = new StringWriter();
        PrintWriter pwriter = new PrintWriter(swriter);
        printStackTrace(pwriter);
        pwriter.flush();
        return swriter.getBuffer().toString();
    }

    public String getStackTraceAsString(Throwable e)
    {
        StringWriter swriter = new StringWriter();
        PrintWriter pwriter = new PrintWriter(swriter);
        e.printStackTrace(pwriter);
        pwriter.flush();
        return swriter.getBuffer().toString();
    }

}
