/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.lucene.document.Document;

import com.resolve.services.graph.ResolveGraphNode;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.tree.AdvanceTree;
import com.resolve.services.util.UserUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;


public class Post extends ResolveGraphNode implements Serializable
{
    private static final long serialVersionUID = 2702555276391964204L;

    public static final String POST_TYPE = "POST";
    public static final String COMMENT = "COMMENT";

    public static final boolean TIMEZONE_OFFSET_DEFAULT = false;
    public static final String TIMEZONE_DEFAULT = "GMT-08:00";

    public static final String SYS_ID = "sys_id";
    public static final String DISPLAYNAME = "displayName";
    public static final String SYS_CREATED_ON = "sys_created_on";
    public static final String SYS_UPDATED_ON = "sys_updated_on";
    public static final String SYS_CREATED_BY = "sys_created_by";
    public static final String SYS_UPDATED_BY = "sys_updated_by";
    public static final String TITLE_URL = "titleurl";
    public static final String ISROOT = "isRoot";

    protected String displayName;

    private String title = "";
    private String titleurl = "";
    private String author = "";
    private String content = "";
    private long expiration;
    private String likeCount;
    private boolean starred;


    //for timezone - no persistance required
    private String timezone;
    private boolean offset = TIMEZONE_OFFSET_DEFAULT;

    private String contentAndComments;
    protected List<Comment> comments = new ArrayList<Comment>();
    protected List<Post> postComments = new ArrayList<Post>();
    protected List<RSComponent> targets; //this Post is targeted to these list of components
    protected List<RSComponent> mentions; //show this Post in the Inbox of the User
    protected List<RSComponent> receivers;//used for Notification - these are the list of users who have registered to get this Notification Post
    protected User whoSent;

    public Document document;

    // set on the UI -- so nothing to do on the server side - so if any of the Comment post has 'answer=true', the parent Post is marked as 'solved=true'
    private boolean solved;
    private boolean answer;

    private boolean readed;

    private boolean isCurrentUser;
    private boolean like;

    private boolean hasDeleteRights;

    public Post()
    {
//        setType(POST_TYPE);//default POST
    }

    public Post(String sys_id)
    {
//        setId(sys_id);
//        setSys_id(sys_id);
//        setType(POST_TYPE);//default POST
    }

    @SuppressWarnings("rawtypes")
    public Post(Map params)
    {
//        setType(POST_TYPE);//default POST
//        setId((String)ServiceSocialConstants.PARAMS_SYS_ID);
//        setSys_id((String)ServiceSocialConstants.PARAMS_SYS_ID);
//        setDisplayName((String)ServiceSocialConstants.PARAMS_DISPLAYNAME);
//        setTitle((String)ServiceSocialConstants.PARAMS_TITLE);
//        setTitleUrl((String)ServiceSocialConstants.PARAMS_TITLE_URL);
//        setAuthor((String)ServiceSocialConstants.PARAMS_AUTHOR);
//        setContent((String)ServiceSocialConstants.PARAMS_CONTENT);
//        setExpiration((String)ServiceSocialConstants.PARAMS_EXPIRATION);
    }

    /**
     * This API is support the existed reference
     *
     * @return
     */
    public String getSys_id()
    {
        return id;
    }

    /**
     * This API is support the existed reference
     *
     * @param sysId
     */
    public void setSys_id(String sysId)
    {
        this.id = sysId;
    }

    public String getDisplayName()
    {
        return displayName;
    }
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

//    public void convertObjectToNode(Node node) throws Exception
//    {
//        // sys_id
//        if(StringUtils.isEmpty(id))
//        {
//            id = Long.toString(node.getId()) + "." + Long.toString(sys_updated_on);
//        }
//        sys_id = id; //making sure that sys_id is same as id for Post
//
//        // displayName - if not there - use the id
//        if (StringUtils.isEmpty(displayName))
//        {
//            displayName = id;
//        }
//
//        //set the sys fields
//        convertObjectToNodeForSysFields(node);
//
//        // titleurl
//        if(StringUtils.isNotEmpty(titleurl))
//        {
//            node.setProperty(TITLE_URL, titleurl);
//        }
//
//        node.setProperty(DISPLAYNAME, displayName);
//        node.setProperty(ISROOT, isRoot);
//        node.setProperty(SocialFactory.NODE_TYPE, getType());
//        node.setProperty(ROLES, (StringUtils.isNotEmpty(getRoles()) ? getRoles() : ""));
//        node.setProperty(EDIT_ROLES, (StringUtils.isNotEmpty(getEditRoles()) ? getEditRoles() : ""));
//        node.setProperty(POST_ROLES, (StringUtils.isNotEmpty(getPostRoles()) ? getPostRoles() : ""));
//    }

//    public void convertNodeToObject(Node node)
//    {
//        convertNodeToObjectForSysFields(node);
//
//        id = (String) node.getProperty(SYS_ID);
//        displayName = (String) node.getProperty(DISPLAYNAME);
//        type = node.hasProperty(SocialFactory.NODE_TYPE) ? (String) node.getProperty(SocialFactory.NODE_TYPE) : "";
//        titleurl = (node.hasProperty(TITLE_URL)) ? (String) node.getProperty(TITLE_URL) : "";
//        isRoot = node.hasProperty(ISROOT) ? (Boolean) node.getProperty(ISROOT) : false;
//
//        roles = node.hasProperty(ROLES) ? (String) node.getProperty(ROLES) : "";
//        editRoles = node.hasProperty(EDIT_ROLES) ? (String) node.getProperty(EDIT_ROLES) : "";
//        postRoles = node.hasProperty(POST_ROLES) ? (String) node.getProperty(POST_ROLES) : "";
//
//    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return this.title;
    }

    public void setTitleUrl(String titleUrl)
    {
        this.titleurl = titleUrl;
    }

    public String getTitleUrl()
    {
        return this.titleurl;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public String getAuthor()
    {

        return this.author;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public String getContent()
    {
        return this.content;
    }

    public String getContentAndComments()
    {
        contentAndComments = buildPost();
        return this.contentAndComments;
    }

    public void setContentAndComments(String contentAndComments)
    {
        this.contentAndComments = contentAndComments;
        processContentAndComments(contentAndComments);
    }

    public void processContentAndComments(String contentAndComments)
    {
        Comment comment = new Comment();

        try
        {
            if(StringUtils.isNotEmpty(contentAndComments) && !contentAndComments.equalsIgnoreCase("NULL"))
            {
                String[] params = contentAndComments.split(StringUtils.LINESEPARATOR_REGEX);

                if(params != null)
                {
                    for (int i = 0; i < params.length; i++)
                    {
                        if(i==0)
                        {
                            this.content = params[i];
                        }
                        else
                        {
                            String commentString  = params[i];

                            if(StringUtils.isNotEmpty(commentString) && !commentString.equalsIgnoreCase("NULL"))
                            {
                                String[] paramsLocal = commentString.split(StringUtils.FIELDSEPARATOR_REGEX);

                                if(paramsLocal != null && paramsLocal.length >= 3)
                                {
                                    comment = new Comment();
                                    comment.setUserName(paramsLocal[0]);
                                    comment.setCommentText(paramsLocal[1]);
                                    comment.setSys_created_on(Long.valueOf(paramsLocal[2]));

                                    if(paramsLocal.length >= 4)
                                    {
                                        comment.setDisplayName(paramsLocal[3]);
                                    }

                                    if(paramsLocal.length >= 5)
                                    {
                                        comment.setSys_id(paramsLocal[4].equals("null")?"":paramsLocal[4]);
                                    }

                                    comments.add(comment);
                                }
                            }
                        }
                    }
                }
             }

            //sort it
            Collections.sort((ArrayList<Comment>)comments, new CommentComparator());

        }
        catch(Throwable t)
        {
            Log.log.info("catched exception : " + t.getMessage());
        }
    }

    public void processBasedOnDeleteComment(String contentAndComments, String commentid)
    {
        Comment comment = new Comment();

        try
        {
            if(StringUtils.isNotEmpty(contentAndComments) && !contentAndComments.equalsIgnoreCase("NULL"))
            {
                String[] params = contentAndComments.split(StringUtils.LINESEPARATOR_REGEX);

                if(params != null)
                {
                    for (int i = 0; i < params.length; i++)
                    {
                        if(i==0)
                        {
                            this.content = params[i];
                        }
                        else
                        {
                            String commentString  = params[i];

                            if(StringUtils.isNotEmpty(commentString) && !commentString.equalsIgnoreCase("NULL"))
                            {
                                String[] paramsLocal = commentString.split(StringUtils.FIELDSEPARATOR_REGEX);

                                if(paramsLocal != null && paramsLocal.length >= 3)
                                {
                                    comment = new Comment();
                                    comment.setUserName(paramsLocal[0]);
                                    comment.setCommentText(paramsLocal[1]);
                                    comment.setSys_created_on(Long.valueOf(paramsLocal[2]));

                                    if(paramsLocal.length >= 4)
                                    {
                                        comment.setDisplayName(paramsLocal[3]);
                                    }

                                    if(paramsLocal.length >= 5)
                                    {
                                        comment.setSys_id(paramsLocal[4].equals("null")?"":paramsLocal[4]);
                                    }

                                    if(StringUtils.isNotEmpty(comment.getSys_id()) && StringUtils.isNotEmpty(commentid)
                                                    && comment.getSys_id().equalsIgnoreCase(commentid))
                                    {
                                        //do nothing
                                    }
                                    else
                                    {
                                        comments.add(comment);
                                    }
                                }
                            }
                        }
                    }
                }
             }

            //sort it
            Collections.sort((ArrayList<Comment>)comments, new CommentComparator());

        }
        catch(Throwable t)
        {
            Log.log.info("catched exception : " + t.getMessage());
        }
    }

    public String buildPost()
    {
        StringBuffer sf = new StringBuffer();
        sf.append(content);

        if(comments != null && !comments.isEmpty())
        {
            sf.append(StringUtils.LINESEPARATOR);
        }

        for(Comment comment: comments)
        {
            sf.append(comment.getUserName());
            sf.append(StringUtils.FIELDSEPARATOR);
            sf.append(comment.getComment());
            sf.append(StringUtils.FIELDSEPARATOR);
            sf.append(comment.getSys_created_on());
            sf.append(StringUtils.FIELDSEPARATOR);
            sf.append(comment.getDisplayName());
            sf.append(StringUtils.FIELDSEPARATOR);
            sf.append(comment.getSys_id());
            sf.append(StringUtils.FIELDSEPARATOR);
            sf.append(StringUtils.LINESEPARATOR);
        }

        return sf.toString();
    }

    public void addPostComments(List<Post> postComments)
    {
        this.postComments = postComments;
    }

    public void addPostComment(Post comment)
    {
        postComments.add(comment);
    }

    public Collection<Post> getPostComments()
    {
       return postComments;
    }


    public void addComments(List<Comment> comments)
    {
        this.comments = comments;
    }

    public void addComment(Comment comment)
    {
        comments.add(comment);
    }

    public Collection<Comment> getComments()
    {
       return comments;
    }

    public long getExpiration()
    {
        return this.expiration;
    }

    public void setExpiration(long expiration)
    {
        this.expiration = expiration;
    }

    public void setExpiration(String expiration)
    {
        this.expiration = Long.parseLong(expiration);
    }

    public void setLikeCount(String likeCount)
    {
        this.likeCount = likeCount;
    }

    public String getLikeCount()
    {
        return this.likeCount;
    }

    public void markStarred(boolean starred)
    {
        this.starred = starred;
    }

    public boolean isStarred()
    {
        return this.starred;
    }

    /**
     * @return the target
     */
    public Collection<RSComponent> getTarget()
    {
        return targets;
    }

    /**
     * @param target the target to set
     */
    public void setTarget(List<RSComponent> targets)
    {
        this.targets = targets;
    }

    public List<RSComponent> getMentions()
    {
        return this.mentions;
    }

    public void setMentions(List<RSComponent> mentions)
    {
        this.mentions = mentions;
    }

    public List<RSComponent> getReceivers()
    {
        return receivers;
    }

    public void setReceivers(List<RSComponent> receivers)
    {
        this.receivers = receivers;
    }

    /**
     * Need user sys_id and also name and roles
     *
     * @param user
     */
    public void setUser(User user)
    {
        this.whoSent = user;
    }

    public User getUser()
    {
        return whoSent;
    }

    class CommentComparator implements Comparator<Comment>
    {
        public int compare(Comment obj1, Comment obj2)
        {
            return (new Long(obj1.getSys_created_on())).compareTo(new Long(obj2.getSys_created_on()));
        }
    }

    /**
     * @return the timezone
     */
    public String getTimezone()
    {
        return timezone;
    }

    /**
     * @param timezone the timezone to set
     */
    public void setTimezone(String timezone)
    {
        this.timezone = timezone;
    }

    /**
     * @return the offset
     */
    public boolean isOffset()
    {
        return offset;
    }

    /**
     * @param offset the offset to set
     */
    public void setOffset(boolean offset)
    {
        this.offset = offset;
    }

    @Override
    public int hashCode()
    {
        int hashcode = 1;

        hashcode = 31*hashcode + ((id == null)?0:id.hashCode());
        hashcode = 31*hashcode + (int) sys_updated_on;

        return hashcode;
    }

    public boolean equals(Object obj)
    {
        boolean result = true;

        if(this == obj)
        {
            result = true;
        }

        if(obj == null)
        {
            result = false;
        }

        if(getClass() != obj.getClass())
        {
            result = false;
        }

        Post postLocal = (Post) obj;

        if(id == null)
        {
            if(postLocal.getSys_id() != null)
            {
                result = false;
            }

            if(postLocal.getSys_updated_on() != sys_updated_on)
            {
                result = false;
            }
        }
        else if(!id.equals(postLocal.getSys_id()) && sys_updated_on != postLocal.getSys_updated_on())
        {
            result = false;
        }

        return result;
    }

    public void setDocument(Document doc)
    {
        this.document = doc;
    }

    public Document getDocument()
    {
        return this.document;
    }

    public boolean isSolved()
    {
        return this.solved;
    }

    public void setSolved(boolean solved)
    {
        this.solved = solved;
    }

    public boolean isReaded()
    {
        return this.readed;
    }

    public void setReaded(boolean readed)
    {
        this.readed = readed;
    }

    public boolean isAnswer()
    {
        return answer;
    }

    public void setAnswer(boolean answer)
    {
        this.answer = answer;
    }

    public boolean isCurrentUser()
    {
        return isCurrentUser;
    }

    public void setIsCurrentUser(boolean isCurrentUser)
    {
        this.isCurrentUser = isCurrentUser;
    }

    public boolean isLike()
    {
        return this.like;
    }

    public void setLike(boolean like)
    {
        this.like = like;
    }

    public boolean isHasDeleteRights()
    {
        return hasDeleteRights;
    }

    public void setHasDeleteRights(boolean hasDeleteRights)
    {
        this.hasDeleteRights = hasDeleteRights;
    }

    public void evaluateDeleteRights(User loggedInUser, String compType)
    {
        Boolean hasDeleteRights = false;

        // if the request is coming from inbox, then the user can delete the
        // post and so no validation is required.
        if (StringUtils.isNotBlank(compType) && compType.trim().equalsIgnoreCase(AdvanceTree.INBOX))
        {
            hasDeleteRights = true;
        }
        else
        {
            hasDeleteRights = isAdminUser(loggedInUser);
            if (!hasDeleteRights && StringUtils.isNotBlank(this.getEditRoles()))
            {
                String csvEditRoles = com.resolve.services.hibernate.util.SocialUtil.convertSocialRolesStringToRoles(this.getEditRoles());
                String csvUserRoles = com.resolve.services.hibernate.util.SocialUtil.convertSocialRolesStringToRoles(loggedInUser.getRoles());
                Set<String> userRolesSet = new HashSet<String>(StringUtils.convertStringToList(csvUserRoles, ","));

                hasDeleteRights = UserUtils.hasRole(loggedInUser.getName(), userRolesSet, csvEditRoles);
            }

            if (!hasDeleteRights)
            {
                hasDeleteRights = loggedInUser.getName().equalsIgnoreCase(this.getAuthor());
            }
        }

        this.setHasDeleteRights(hasDeleteRights);

    }

    
    private static boolean isAdminUser(User user)
    {
        boolean isAdmin = false;

        try
        {
            String csvUserRoles = user.getRoles().replaceAll(" ", ",").replaceAll("_", "&");
            Set<String> userRoles = new HashSet<String>(StringUtils.convertStringToList(csvUserRoles, ","));
            if (userRoles.contains(UserUtils.ADMIN))
            {
                isAdmin = true;
            }
        }
        catch (Throwable t)
        {
            Log.log.error("error validating if the social user is admin or not.", t);
        }

        return isAdmin;
    }

}
