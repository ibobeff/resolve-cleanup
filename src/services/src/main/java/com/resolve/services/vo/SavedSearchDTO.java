package com.resolve.services.vo;

import java.util.List;

public class SavedSearchDTO {
	private String name;
	private List<QueryFilter> filters;
	private List<QuerySort> sorts;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<QueryFilter> getFilters() {
		return filters;
	}

	public void setFilters(List<QueryFilter> filters) {
		this.filters = filters;
	}

	public List<QuerySort> getSorts() {
		return sorts;
	}

	public void setSorts(List<QuerySort> sorts) {
		this.sorts = sorts;
	}
}
