/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.rb.util;

import com.resolve.util.StringUtils;

public class Noop extends Common implements Comparable<Common>
{
    private static final int width = 100;
    private static final int height = 40;

    private static final String style = "rounded;labelBackgroundColor=none;strokeColor=#A9A9A9;fillColor=#D3D3D3;gradientColor=white;verticalLabelPosition=middle;verticalAlign=middle";
 
    private Params params;
    
    public Noop(int id, int x, int y, String merge)
    {
        super(id, "noop", "noop#resolve", x, y, width, height, merge);
        params = new Params(null, null);
    }

    @Override
    String getStyle()
    {
        return StringUtils.isNotBlank(merge) && merge.equals("ANY") ? style + ";dashed=1" : style;
    }

    public int compareTo(Common o)
    {
        return this.getId().compareTo(o.getId());
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("    <Task ").append("label=\"noop\" description=\"noop#resolve\" ")
            .append("tooltip=\"undefined\" href=\"noop#resolve\" detail=\"Does nothing. Can be used as placeholder in Runbooks.\" ");
        if(StringUtils.isNotBlank(merge))
        {
            sb.append(" merge=\"merge = " + merge + "\" ");
        }
        sb.append(" id=\"" + getId() + "\">\n"); 
        sb.append(cell.toString()).append("\n"); 
        sb.append(params.toString()).append("\n");
        sb.append("    </Task>"); 
        
        return sb.toString();
    }
}
