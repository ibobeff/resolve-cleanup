/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.sir;

public enum McAfeeSIRPhasesEnum {
    RESPONSE("Response"),
    RESUMPTION("Resumption"),
    RECOVERY("Recovery"),
    RESTORATION("Restoration");
    
    public static final String MCAFEE_STANDARDS = "McAfee";
    private static final String COLON = ":";
    private static final String PERIOD = ".";
        
    private final String phaseName;
    private final String persistenceId;
    
    McAfeeSIRPhasesEnum(String phaseName)
    {
        this.phaseName = phaseName;
        
        String tmpPersistenceId = MCAFEE_STANDARDS + COLON + phaseName;
        
        if (tmpPersistenceId.length() > 32) {
            tmpPersistenceId = tmpPersistenceId.substring(0, 28) + PERIOD + PERIOD + PERIOD;
        }
        
        this.persistenceId = tmpPersistenceId;
    }
    
    public String getPhaseName()
    {
        return phaseName;
    }
    
    public String getPersistenceId() {
        return persistenceId;
    }
}
