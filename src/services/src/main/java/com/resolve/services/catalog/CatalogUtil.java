/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.catalog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.query.Query;

import com.resolve.persistence.model.CatalogAttachment;
import com.resolve.persistence.model.ResolveCatalog;
import com.resolve.persistence.model.ResolveCatalogEdge;
import com.resolve.persistence.model.ResolveCatalogNode;
import com.resolve.persistence.model.ResolveCatalogNodeTagRel;
import com.resolve.persistence.model.ResolveCatalogNodeWikidocRel;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.SaveUtil;
import com.resolve.services.hibernate.util.TagUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.CatalogAttachmentVO;
import com.resolve.services.hibernate.vo.ResolveCatalogVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.WikiDocumentDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class CatalogUtil
{
    /**
     * get the list of catalogs for the grid
     * 
     * @param query
     * @param username
     * @return
     * @throws Exception
     */
    public static ResponseDTO<Catalog> getCatalogs(QueryDTO query, String username) throws Exception
    {
        List<Catalog> catalogResult = new ArrayList<Catalog>();
        int size = 0;

        //to get all the catalogs
        if (query == null)
        {
            query = new QueryDTO();
        }

        //make sure that the model is ResolveCatalog
        query.setModelName("ResolveCatalog");

        //get the catalog vos
        List<ResolveCatalogVO> catalogs = getResolveCatalogs(query, username);
        for (ResolveCatalogVO catalog : catalogs)
        {
            Catalog c = new Catalog();
            c.setId(catalog.getSys_id());
            c.setName(catalog.getUName());
            c.setCatalogType(catalog.getUType());
            c.setSys_created_by(catalog.getSysCreatedBy());
            c.setSys_created_on(catalog.getSysCreatedOn().getTime());
            c.setSys_updated_by(catalog.getSysUpdatedBy());
            c.setSys_updated_on(catalog.getSysCreatedOn().getTime());

            catalogResult.add(c);
        }

        //get the total count
        size = ServiceHibernate.getTotalHqlCount(query);

        //prepare the response
        ResponseDTO<Catalog> response = new ResponseDTO<Catalog>();
        response.setRecords(catalogResult);
        response.setTotal(size);

        return response;
    }

    public static Catalog getCatalog(String sysId, String catalogName, String username) throws Exception
    {
        //get the list of all the nodes of this catalog
        Catalog root = new GetCatalog(sysId, catalogName, username).getCatalog();
        return root;
    }

    public static boolean isExistingCatalog(String sysId, String name, String username) throws Exception
    {
        boolean result = false;

        ResolveCatalog model = findCatalogModel(sysId, name, username);
        if (model != null)
        {
            result = true;
        }

        return result;
    }

    public static ResolveCatalogVO findCatalog(String sysId, String name, String username)
    {
        ResolveCatalogVO result = null;

        ResolveCatalog model = findCatalogModel(sysId, name, username);
        if (model != null)
        {
            result = model.doGetVO();
        }

        return result;
    }

    public static Catalog save(Catalog catalog, String username) throws Exception
    {
        SaveCatalog save = new SaveCatalog(catalog, username);
        catalog = save.persist();

        return catalog;
    }
    
    public static Catalog merge(Catalog catalog, String username) throws Exception
    {
        throw new Exception("YET TO BE IMPLEMENTED");
    }

    public static void delete(String sysId, String name, String username) throws Exception
    {

        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            ResolveCatalog catalog = findCatalogModel(sysId, name, username);
	            if (catalog != null)
	            {
	                //delete the nodes first
	                Collection<ResolveCatalogNode> catalogNodes = catalog.getCatalogNodes();
	                for (ResolveCatalogNode catalogNode : catalogNodes)
	                {
	                    //delete the edges
	                    Collection<ResolveCatalogEdge> catalogEdges = catalogNode.getCatalogEdges();
	                    for (ResolveCatalogEdge edge : catalogEdges)
	                    {
	                        HibernateUtil.getDAOFactory().getResolveCatalogEdgeDAO().delete(edge);
	                    }
	
	                    //delete wiki rels
	                    Collection<ResolveCatalogNodeWikidocRel> catalogWikiRels = catalogNode.getCatalogWikiRels();
	                    for (ResolveCatalogNodeWikidocRel catalogWikiRel : catalogWikiRels)
	                    {
	                        HibernateUtil.getDAOFactory().getResolveCatalogNodeWikidocRelDAO().delete(catalogWikiRel);
	                    }
	
	                    //delete tags rel
	                    Collection<ResolveCatalogNodeTagRel> catalogTagRels = catalogNode.getCatalogTagRels();
	                    for (ResolveCatalogNodeTagRel catalogTagRel : catalogTagRels)
	                    {
	                        HibernateUtil.getDAOFactory().getResolveCatalogNodeTagRelDAO().delete(catalogTagRel);
	                    }
	
	                    //delete the node
	                    HibernateUtil.getDAOFactory().getResolveCatalogNodeDAO().delete(catalogNode);
	                }
	
	                //delete the catalog
	                HibernateUtil.getDAOFactory().getResolveCatalogDAO().delete(catalog);
	            }

            });

        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }

    public static Catalog renameCatalog(String fromSysId, String fromName, String toName, String username) throws Exception
    {
        //make sure that there is no catalog by the name 'toName'
        ResolveCatalog testCatalogName = CatalogUtil.findCatalogModel(null, toName, username);
        if (testCatalogName != null)
        {
            throw new Exception("Catalog with name " + toName + " already exist. Please make the name unique.");
        }

        //get the catalog, update the name and save it
        Catalog catalog = getCatalog(fromSysId, fromName, username);
        if (catalog != null)
        {
            catalog.setName(toName);

            //this will update the paths 
            catalog = save(catalog, username);
        }

        return catalog;
    }

    public static Catalog copyCatalog(String fromSysId, String fromName, String toName, String username) throws Exception
    {
        //make sure that there is no catalog by the name 'toName'
        ResolveCatalog testCatalogName = CatalogUtil.findCatalogModel(null, toName, username);
        if (testCatalogName != null)
        {
            throw new Exception("Catalog with name " + toName + " already exist. Please make the name unique.");
        }

        //get the catalog, update the name and save it
        Catalog catalog = getCatalog(fromSysId, fromName, username);
        if (catalog != null)
        {
            catalog.setName(toName);
            catalog.purgeIds();

            //this will update the paths 
            catalog = save(catalog, username);
        }

        return catalog;
    }

    ///////////////////////////////////////////////////////////////////////////////
    //////////////// Catalog Attachments
    ///////////////////////////////////////////////////////////////////////////////
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResponseDTO<CatalogAttachmentVO> getCatalogAttachments(QueryDTO query, String username) throws Exception
    {
        if (query == null)
        {
            //get all the attachments
            query = new QueryDTO();
            query.setSelectColumns("sys_id,sysCreatedOn,sysCreatedBy,sysUpdatedOn,sysUpdatedBy,fileName,displayName,type,location,size");
            query.setModelName("CatalogAttachment");
            query.setType(QueryDTO.TABLE_TYPE);
            query.setLimit(-1);
            query.setStart(0);
        }

        //set it in the query object
        int start = query.getStart();
        int limit = query.getLimit();
        int total = 0;
        List result = null;

        try
        {
            //execute the qry
            //            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, start, limit, true);
            result = GeneralHibernateUtil.executeSelectHQL(query, start, limit, true);
            total = ServiceHibernate.getTotalHqlCount(query);
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting catalog attachment list", e);
            throw e;
        }

        ResponseDTO<CatalogAttachmentVO> response = new ResponseDTO<CatalogAttachmentVO>();
        response.setRecords(result);
        response.setTotal(total);

        return response;
    }

    public static CatalogAttachmentVO saveCatalogAttachment(CatalogAttachmentVO catalogVO, String username) throws Exception
    {
        if (catalogVO != null)
        {
            CatalogAttachment catalog = null;
            if (StringUtils.isNotBlank(catalogVO.getSys_id()) && !catalogVO.getSys_id().equals(VO.STRING_DEFAULT))
            {
                //update
                catalog = findCatalogAttachmentFor(catalogVO.getSys_id(), null);
                if (catalog != null)
                {
                    catalog.applyVOToModel(catalogVO);
                }
                else
                {
                    //find with the name
                    catalog = findCatalogAttachmentFor(null, catalogVO.getFilename());
                    if (catalog != null)
                    {
                        catalog.setContent(catalogVO.getContent());
                        catalog.setSize(catalogVO.getU_size());
                    }
                    else
                    {
                        catalog = new CatalogAttachment(catalogVO);
                    }
                }
            }
            else
            {
                //find with the name
                catalog = findCatalogAttachmentFor(null, catalogVO.getFilename());
                if (catalog != null)
                {
                    catalog.setContent(catalogVO.getContent());
                    catalog.setSize(catalogVO.getU_size());
                }
                else
                {
                    catalog = new CatalogAttachment(catalogVO);
                }
            }

            catalog = SaveUtil.saveCatalogAttachment(catalog, username);
            catalogVO = catalog.doGetVO();
        }

        return catalogVO;
    }

    public static CatalogAttachmentVO downloadCatalogFile(String sysId, String username) throws Exception
    {
        CatalogAttachmentVO vo = null;

        if (StringUtils.isNotBlank(sysId))
        {
            CatalogAttachment attachment = null;
            try
            {
              HibernateProxy.setCurrentUser("admin");
                attachment = (CatalogAttachment) HibernateProxy.execute(() -> {

                	return HibernateUtil.getDAOFactory().getCatalogAttachmentDAO().findById(sysId);

                });
            }
            catch (Exception t)
            {
                Log.log.error(t.getMessage(), t);
                throw t;
            }

            if (attachment != null)
            {
                vo = attachment.doGetVO();
            }
        }

        return vo;
    }

    public static ResponseDTO<Catalog> getRefCatalogs(String catalogId, String catalogName, String username) throws Exception
    {
        boolean all = false;
        List<Catalog> filteredResult = new ArrayList<Catalog>();
        ResponseDTO<Catalog> result = new ResponseDTO<Catalog>();
        Set<String> catalogRefSysIds = new HashSet<String>();
        String currentCatalogSysId = null;

        if(StringUtils.isEmpty(catalogId) && StringUtils.isEmpty(catalogName))
        {
            //send all the catalogs as this is a case where a New catalog is getting created
            all = true;
        }
        else
        {
        
            Catalog currentCatalog = getCatalog(catalogId, catalogName, username);
            if (currentCatalog == null)
            {
                all = true;
    //            throw new Exception("Catalog with id '" + catalogId + "' or name '" + catalogName + "' does not exist.");
            }
            else
            {
                currentCatalogSysId = currentCatalog.getParentSysId();
                
                //get list of catalogs that reference this one
                catalogRefSysIds.addAll(getCatalogSysIdsThatIsReferenceing(currentCatalogSysId));
            }
        }

        //get all the catalogs from the main table..same as grid which is the main SysId 
        List<Catalog> catalogs = getCatalogs(null, username).getRecords();
        if (catalogs.size() > 0)
        {
            if(all)
            {
                filteredResult.addAll(catalogs);
            }
            else
            {
                for (Catalog catalog : catalogs)
                {
                    
                    //make sure that the refering catalog is not there in the drop down
                    //and the current catalog is also not in the drop down
                    if (!catalogRefSysIds.contains(catalog.getId()) && !currentCatalogSysId.equalsIgnoreCase(catalog.getId()))
                    {
                        filteredResult.add(catalog);
                    }
                }
            }

            result.setRecords(filteredResult);
            result.setTotal(filteredResult.size());
        }

        return result;
    }

    /**
     * pathTagName -- like: /TestFen100/New Group/New Item
     * tags -- the list of tag name assign to the catalog
     *         like: network, security, alarm ...
     * 
     * @param pathTagName
     * @param tags
     * @return
     */
    public static Collection<WikiDocumentDTO> getWikidocmentOf(String pathName, List<String> tags, String username) throws Exception
    {
        Set<WikiDocumentDTO> result = new HashSet<WikiDocumentDTO>();
        Set<String> selectedTags = new HashSet<String>(); //list of unique tags 

        if (StringUtils.isNotBlank(pathName))
        {
            //String nodeSql = "from ResolveCatalogNode where LOWER(UPath) = '" + pathName.toLowerCase() + "'";
            String nodeSql = "from ResolveCatalogNode where LOWER(UPath) = :UPath";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("UPath", pathName.toLowerCase());
            
            try
            {

              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
                    List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(nodeSql, queryParams);
                    if (list != null && list.size() == 1)
                    {
                        ResolveCatalogNode model = (ResolveCatalogNode) list.get(0);

                        //get the doc sysIds
                        Collection<ResolveCatalogNodeWikidocRel> catalogWikiRels = model.getCatalogWikiRels();
                        if (catalogWikiRels != null && catalogWikiRels.size() > 0)
                        {
                            for (ResolveCatalogNodeWikidocRel catalogWikiRel : catalogWikiRels)
                            {
                                WikiDocument doc = catalogWikiRel.getWikidoc();
                                if (doc != null && doc.ugetUIsActive() && !doc.ugetUIsHidden() && !doc.ugetUIsLocked() && !doc.ugetUIsDeleted())
                                {
                                    result.add(WikiUtils.prepareWikiDTO(doc));
                                }//end of if
                            }//end of for loop
                        }

                        //get tags
                        Collection<ResolveCatalogNodeTagRel> catalogTagRels = model.getCatalogTagRels();
                        if (catalogTagRels != null && catalogTagRels.size() > 0)
                        {
                            for (ResolveCatalogNodeTagRel catalogTagRel : catalogTagRels)
                            {
                                if(catalogTagRel.getTag() != null)
                                    selectedTags.add(catalogTagRel.getTag().getUName());
                                else
                                {
                                    //BAD DATA - Delete it
                                    HibernateUtil.getDAOFactory().getResolveCatalogNodeTagRelDAO().delete(catalogTagRel);
                                }
                            }//end of for loop
                        }
                    }
                    else if (list.size() > 1)
                    {
                        throw new Exception("Path '" + pathName + "' is not unique. This case should never exist.");
                    }
                    else
                    {
                        throw new Exception("Path '" + pathName + "' does not exist.");
                    }
            	});

            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                throw e;
            }
        }//end of if

        //prepare the set of the tags
        if (tags != null)
        {
            selectedTags.addAll(tags);
        }

        //get the doc for all these tags
        if(selectedTags.size() > 0)
        {
            result.addAll(TagUtil.findDocumentsWithTags(selectedTags, username));
        }

        return result;
    }

    //private apis
    private static List<ResolveCatalogVO> getResolveCatalogs(QueryDTO query, String username) throws Exception
    {
        List<ResolveCatalogVO> result = new ArrayList<ResolveCatalogVO>();

        int limit = query.getLimit();
        int offset = query.getStart();

        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if (data != null)
            {
                //grab the map of sysId-organizationname  
                //                Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();

                //get the Roles for all the organizations
                //                Map<String, String> mapOfAppsAndRoles =  getMapOfAppsAndRoles();

                String selectedColumns = query.getSelectColumns();
                if (StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");

                    //this will be the array of objects
                    for (Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        ResolveCatalog instance = new ResolveCatalog();

                        //set the attributes in the object
                        for (int x = 0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];

                            PropertyUtils.setProperty(instance, column, value);
                        }

                        result.add(instance.doGetVO());
                    }//end of for loop
                }
                else
                {
                    for (Object o : data)
                    {
                        ResolveCatalog instance = (ResolveCatalog) o;
                        result.add(instance.doGetVO());
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }

        return result;

    }

    public static ResolveCatalog findCatalogModel(String sysId, String name, String username)
    {
        ResolveCatalog model = null;

        if (StringUtils.isNotEmpty(sysId))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	model = (ResolveCatalog) HibernateProxy.execute(() -> {

            		return HibernateUtil.getDAOFactory().getResolveCatalogDAO().findById(sysId);

            	});

            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        else if (StringUtils.isNotBlank(name))
        {
            //to make this case-insensitive

            //String sql = "from ResolveCatalog where LOWER(UName) = '" + name.toLowerCase().trim() + "' ";
            String sql = "from ResolveCatalog where LOWER(UName) = :UName ";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("UName", name.toLowerCase().trim());
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                if (list != null && list.size() > 0)
                {
                    model = (ResolveCatalog) list.get(0);
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing sql:" + sql, e);
            }

        }
        return model;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static CatalogAttachment findCatalogAttachmentFor(String sysId, String fileName) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser("admin");
        	return (CatalogAttachment) HibernateProxy.execute(() -> {
                CatalogAttachment file = null;
                
	            if (StringUtils.isNotBlank(sysId))
	            {
	                file = HibernateUtil.getDAOFactory().getCatalogAttachmentDAO().findById(sysId);
	            }
	            else if (StringUtils.isNotBlank(fileName))
	            {
	                String sql = "from CatalogAttachment where LOWER(fileName) = '" + fileName.trim().toLowerCase() + "'";
	                Query q = HibernateUtil.createQuery(sql);
	                List<CatalogAttachment> list = q.list();
	                if (list != null && list.size() > 0)
	                {
	                    file = list.get(0);
	                }
	            }
	            
	            return file;
        	});

        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
    }//findCatalogAttachmentFor

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static Set<String> getCatalogSysIdsThatIsReferenceing(String catalogId)
    {
        Set<String> catalogRefSysIds = new HashSet<String>();

        //String sql = "select DISTINCT catalog.sys_id from ResolveCatalogNode where UReferenceCatalogSysId = '" + catalogId + "'";
        String sql = "select DISTINCT catalog.sys_id from ResolveCatalogNode where UReferenceCatalogSysId = :UReferenceCatalogSysId";
        
        Map<String, Object> queryParams = new HashMap<String, Object>();
        
        queryParams.put("UReferenceCatalogSysId", catalogId);
        
        try
        {
            List sysIds = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
            if (sysIds != null)
            {
                catalogRefSysIds.addAll(sysIds);
            }

        }
        catch (Exception e)
        {
            Log.log.error("Error executing the hql:" + sql, e);
        }

        return catalogRefSysIds;

    }

}
