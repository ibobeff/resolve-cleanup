package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.collections.CollectionUtils;

import com.resolve.persistence.model.MSGGatewayFilter;
import com.resolve.persistence.model.MSGGatewayFilterAttr;
import com.resolve.persistence.model.PullGatewayFilter;
import com.resolve.persistence.model.PullGatewayFilterAttr;
import com.resolve.persistence.model.PushGatewayFilter;
import com.resolve.persistence.model.PushGatewayFilterAttr;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceGateway;
import com.resolve.services.hibernate.vo.GatewayFilterVO;
import com.resolve.services.hibernate.vo.GatewayVO;
import com.resolve.services.hibernate.vo.MSGGatewayFilterVO;
import com.resolve.services.hibernate.vo.PullGatewayFilterVO;
import com.resolve.services.hibernate.vo.PushGatewayFilterVO;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

public class GatewayFilterUtil
{
    public static GatewayFilterVO saveGatewayFilter(Map<String, String>params, String gatewayName) throws Exception {
        
        String id = (String)params.get("id");
/*        String blocking = (String)params.get("ublocking");
        String ack = (String)params.get("uack");*/
        String filterName = (String)params.get("uname");
        GatewayFilterVO result = null;
        String type = ServiceGateway.getGatewayType(gatewayName);
        if(StringUtils.isBlank(type) || type.equals("None"))
            throw new Exception("Gateway type is not available.");

        if(type.equals("Push")) {
            if(StringUtils.isNotBlank(id)) {
                
                //Since we don't get the attributes table sys ids we will have to get all the attributes of the filter
                //Delete the filter 
            	String[] ids = new String[1];
            	ids[0] = id; 
                boolean isDeleted = PushGatewayFilterUtil.deletePushFilters(gatewayName, ids);
                if(isDeleted) {
                	//Reinserting them
                	PushGatewayFilterUtil.insertPushGatewayFilter(params, gatewayName);
                	//Need to update the filter flag stating that the filter was updated
                	List<PushGatewayFilterVO> deployedFilters = PushGatewayFilterUtil.getPushFiltersByName(gatewayName, filterName, null);
                	PushGatewayFilterVO original = PushGatewayFilterUtil.getPushFilterByName(gatewayName, filterName, Constants.DEFAULT_GATEWAY_QUEUE_NAME);
                    for(PushGatewayFilterVO deployedFilter:deployedFilters) {
                        if(deployedFilter.getSys_id().equals(original.getSys_id()))
                            continue;
                        else
//                        	params.put("id", deployedFilter.getSys_id());
//                        	PushGatewayFilterUtil.updatePushGatewayFilterById(params, gatewayName, true);
                        	PushGatewayFilterUtil.updateDeployedFilterStatus(deployedFilter, true, true);
                    }
                }                
            }            
            else
                result = PushGatewayFilterUtil.insertPushGatewayFilter(params, gatewayName);
            
            if(result == null)
                result = PushGatewayFilterUtil.getPushFilterByName(gatewayName, filterName, Constants.DEFAULT_GATEWAY_QUEUE_NAME);
            Integer checksum = getPushFilterChecksum(result, params.get("username"));
            updatePushFilterChecksum(checksum, result.getSys_id(), params.get("username"));
        }
        
        else if(type.equals("MSG")) {
            if(StringUtils.isNotBlank(id)) {
                MSGGatewayFilterUtil.updateMSGGatewayFilter(params, gatewayName, false);
                
                MSGGatewayFilterVO original = MSGGatewayFilterUtil.getMSGFilterByName(gatewayName, filterName, Constants.DEFAULT_GATEWAY_QUEUE_NAME);
                if(original == null)
                    throw new Exception("Cannot find filter with sys id of " + id);
                
               List<MSGGatewayFilterVO> deployedFilters = MSGGatewayFilterUtil.getMSGFiltersByName(gatewayName, filterName, null);
                
                for(MSGGatewayFilterVO deployedFilter:deployedFilters) {
                    if(deployedFilter.getSys_id().equals(original.getSys_id()))
                        continue;
                    
                    if(!deployedFilter.equals(original)) {
                        deployedFilter.setUUpdated(true);
                        MSGGatewayFilterUtil.updateMSGGatewayFilter(deployedFilter, deployedFilter.getSys_id());
                    }
                }
            }
            
            else
                result = MSGGatewayFilterUtil.insertMSGGatewayFilter(params, gatewayName);
            
            if(result == null)
                result = MSGGatewayFilterUtil.getMSGFilterByName(gatewayName, filterName, Constants.DEFAULT_GATEWAY_QUEUE_NAME);
            Integer checksum = getMSGFilterChecksum(result, params.get("username")); // a separate API just to calculate checksum so as we could reuse it. 
            updateMSGFilterChecksum(checksum, result.getSys_id(), params.get("username")); // Update the checksum.
        }
        
        else {
            if(StringUtils.isNotBlank(id)) {
                PullGatewayFilterUtil.updatePullGatewayFilter(params, gatewayName, false);
                
                PullGatewayFilterVO original = PullGatewayFilterUtil.getPullFilterByName(gatewayName, filterName, Constants.DEFAULT_GATEWAY_QUEUE_NAME);
                if(original == null)
                    throw new Exception("Cannot find filter with sys id of " + id);
                
                List<PullGatewayFilterVO> deployedFilters = PullGatewayFilterUtil.getPullFiltersByName(gatewayName, filterName, null);
                
                for(PullGatewayFilterVO deployedFilter:deployedFilters) {
                    if(deployedFilter.getSys_id().equals(original.getSys_id()))
                        continue;
                    
                    if(!deployedFilter.equals(original)) {
                        deployedFilter.setUUpdated(true);
                        PullGatewayFilterUtil.updatePullGatewayFilter(deployedFilter, deployedFilter.getSys_id());
                    }
                }
            }
            
            else
                result = PullGatewayFilterUtil.insertPullGatewayFilter(params, gatewayName);

            if(result == null)
                result = PullGatewayFilterUtil.getPullFilterByName(gatewayName, filterName, Constants.DEFAULT_GATEWAY_QUEUE_NAME);
            Integer checksum = getPullFilterChecksum(result, params.get("username"));
            updatePullFilterChecksum(checksum, result.getSys_id(), params.get("username"));
        }
        return result;
    }
    
    public static Integer getPullFilterChecksum(GatewayFilterVO vo, String username) {
        Integer checksum = null;
        if (vo != null) {
            List<PullGatewayFilterAttr> attList = new ArrayList<>();
            StringBuilder builder = new StringBuilder();
            try {
                HibernateUtil.action(util -> {
                    PullGatewayFilterAttr attr = new PullGatewayFilterAttr();
                    attr.setUPullFilterId(vo.getSys_id());
                    attList.addAll(HibernateUtil.getDAOFactory().getPullGatewayFilterAttrDAO().find(attr, new String[0])); // read all filter attributes
                }, username);
            }
            catch (Throwable e) {
                Log.log.error(e.getMessage(), e);
            }
            if (CollectionUtils.isNotEmpty(attList)) {
                attList.stream()
                   .sorted((attr1, attr2) -> attr1.getUName().compareTo(attr2.getUName())) // sort attributes by name in ascending order
                   .forEach(attr -> { // Collect sorted name - value in a big giant String
                       builder.append(attr.getUName()).append(attr.getUValue());
                       }
                   );
                if (builder.length() > 0) {
                    checksum = Objects.hash(builder.toString());
                }
            }
            checksum = Objects.hash(((PullGatewayFilterVO)vo).hashCode(), checksum); // generate final checksum
        }
        return checksum;
    }
    
    private static void updatePullFilterChecksum (Integer checksum, String sysId, String username) {
        try {
            HibernateUtil.action(util -> {
                PullGatewayFilter filter = HibernateUtil.getDAOFactory().getPullGatewayFilterDAO().findById(sysId);
                filter.setChecksum(checksum);
                HibernateUtil.getDAOFactory().getPullGatewayFilterDAO().update(filter);
            }, username);
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static Integer getPushFilterChecksum(GatewayFilterVO vo, String username) {
        Integer checksum = null;
        if (vo != null) {
            List<PushGatewayFilterAttr> attList = new ArrayList<>();
            StringBuilder builder = new StringBuilder();
            try {
                HibernateUtil.action(util -> {
                    PushGatewayFilter filter = HibernateUtil.getDAOFactory().getPushGatewayFilterDAO().findById(vo.getSys_id());
                    PushGatewayFilterAttr attr = new PushGatewayFilterAttr();
                    attr.setPushGatewayFilter(filter);
                    attList.addAll(HibernateUtil.getDAOFactory().getPushGatewayFilterAttrDAO().find(attr, new String[0])); // read all filter attributes
                }, username);
            }
            catch (Throwable e) {
                Log.log.error(e.getMessage(), e);
            }
            if (CollectionUtils.isNotEmpty(attList)) {
                attList.stream()
                   .sorted((attr1, attr2) -> attr1.getUName().compareTo(attr2.getUName())) // sort attributes by name in ascending order
                   .forEach(attr -> { // Collect sorted name - value in a big giant String
                       builder.append(attr.getUName()).append(attr.getUValue());
                       }
                   );
                if (builder.length() > 0) {
                    checksum = Objects.hash(builder.toString());
                }
            }
            checksum = Objects.hash(((PushGatewayFilterVO)vo).hashCode(), checksum); // generate final checksum
        }
        return checksum;
    }
    
    private static void updatePushFilterChecksum(Integer checksum, String sysId, String username) {
        try {
            HibernateUtil.action(util -> {
                PushGatewayFilter filter = HibernateUtil.getDAOFactory().getPushGatewayFilterDAO().findById(sysId);
                filter.setChecksum(checksum);
                HibernateUtil.getDAOFactory().getPushGatewayFilterDAO().update(filter);
            }, username);
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static Integer getMSGFilterChecksum(GatewayFilterVO vo, String username) {
        Integer checksum = null;
        if (vo != null) {
            List<MSGGatewayFilterAttr> attList = new ArrayList<>();
            StringBuilder builder = new StringBuilder();
            try {
                HibernateUtil.action(util -> {
                    MSGGatewayFilterAttr attr = new MSGGatewayFilterAttr();
                    attr.setUMSGFilterId(vo.getSys_id());
                    attList.addAll(HibernateUtil.getDAOFactory().getMSGGatewayFilterAttrDAO().find(attr, new String[0])); // read all filter attributes
                }, username);
            }
            catch (Throwable e) {
                Log.log.error(e.getMessage(), e);
            }
            if (CollectionUtils.isNotEmpty(attList)) {
                attList.stream()
                   .sorted((attr1, attr2) -> attr1.getUName().compareTo(attr2.getUName())) // sort attributes by name in ascending order
                   .forEach(attr -> { // Collect sorted name - value in a big giant String
                       builder.append(attr.getUName()).append(attr.getUValue());
                       }
                   );
                if (builder.length() > 0) {
                    checksum = Objects.hash(builder.toString());
                }
            }
            checksum = Objects.hash(((MSGGatewayFilterVO)vo).hashCode(), checksum); // generate final checksum   
        }
        return checksum;
    }
    
    private static void updateMSGFilterChecksum (Integer checksum, String sysId, String username) {
        try {
            HibernateUtil.action(util -> {
                MSGGatewayFilter filter = HibernateUtil.getDAOFactory().getMSGGatewayFilterDAO().findById(sysId);
                filter.setChecksum(checksum);
                HibernateUtil.getDAOFactory().getMSGGatewayFilterDAO().update(filter);
            }, username);
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static void updateGatewayFilter(Map<String, String> params) {

        String gatewayName = (String)params.get("ugatewayName");
        String type = (String)params.get("type");
        try {
            if(StringUtils.isNotBlank(type) && type.equals("PUSH")) {
                PushGatewayFilterUtil.upsertPushGatewayFilter(params, gatewayName, false);
            	//Update the deployment status in the original filter
//                PushGatewayFilterVO original = PushGatewayFilterUtil.getPushFilterByName(gatewayName, 
//            					filterName, Constants.DEFAULT_GATEWAY_QUEUE_NAME);
//                PushGatewayFilterUtil.updateDeployedFilterStatus(original, true, false);
            }
            else if(StringUtils.isNotBlank(type) && type.equals("PULL"))
                PullGatewayFilterUtil.updatePullGatewayFilter(params, gatewayName, false);
            
            else if(StringUtils.isNotBlank(type) && type.equals("MSG"))
                MSGGatewayFilterUtil.updateMSGGatewayFilter(params, gatewayName, false);
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static void undeployGatewayFilter(Map<String, String> params) {
        
        String gatewayName = (String)params.get("ugatewayName");
        String type = (String)params.get("type");
        
        try {
            if(StringUtils.isNotBlank(type) && type.equals("PUSH")) {
                String queueName = params.get("uqueue");
                String filterName = params.get("uname");
                PushGatewayFilter pushFilter = new PushGatewayFilter();
                pushFilter.setUGatewayName(gatewayName);
                pushFilter.setUName(filterName);
                pushFilter.setUQueue(queueName);
                String sysId = PushGatewayFilterUtil.getPushGatewayFilterId(pushFilter);
                if(sysId != null) {
                    String[] ids = new String[1];
                    ids[0] = sysId;
                    PushGatewayFilterUtil.deletePushFilters(gatewayName, ids);
//                    PushGatewayFilterVO original = PushGatewayFilterUtil.getPushFilterByName(gatewayName, 
//        					filterName, Constants.DEFAULT_GATEWAY_QUEUE_NAME);
//                    PushGatewayFilterUtil.updateDeployedFilterStatus(original, false, false);
                }
                
                else
                    throw new Exception("Cannot undeploy filter with name " + filterName);
            }
            
            else if(StringUtils.isNotBlank(type) && type.equals("PULL")) {
                String queueName = params.get("uqueue");
                String filterName = params.get("uname");
                PullGatewayFilter pullFilter = new PullGatewayFilter();
                pullFilter.setUGatewayName(gatewayName);
                pullFilter.setUName(filterName);
                pullFilter.setUQueue(queueName);
                String sysId = PullGatewayFilterUtil.getPullGatewayFilterId(pullFilter);
                if(sysId != null) {
                    String[] ids = new String[1];
                    ids[0] = sysId;
                    PullGatewayFilterUtil.deletePullFilters(gatewayName, ids);
                }
                
                else
                    throw new Exception("Cannot undeploy filter with name " + filterName);
            }
            
            else if(StringUtils.isNotBlank(type) && type.equals("MSG")) {
                String queueName = params.get("uqueue");
                String filterName = params.get("uname");
                
                String sysId = MSGGatewayFilterUtil.getMSGGatewayFilterId(gatewayName, filterName, queueName);
                if(sysId != null) {
                    String[] ids = new String[1];
                    ids[0] = sysId;
                    MSGGatewayFilterUtil.deleteMSGFilters(gatewayName, ids);
                }
                
                else
                    throw new Exception("Cannot undeploy filter with name " + filterName);
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static GatewayVO getFilterByName(String gatewayName, String filterName, String queueName) throws Exception {
        
        GatewayVO result = null;
        String type = ServiceGateway.getGatewayType(gatewayName);
        
        if(StringUtils.isBlank(type) || type.equals("None"))
            throw new Exception("Gateway type is not available.");
        
        if(type.equals("Push"))
            result = PushGatewayFilterUtil.getPushFilterByName(gatewayName, filterName, queueName);
        
        else if(type.equals("Pull"))
            result = PullGatewayFilterUtil.getPullFilterByName(gatewayName, filterName, queueName);

        else if(type.equals("MSG"))
            result = MSGGatewayFilterUtil.getMSGFilterByName(gatewayName, filterName, queueName);

        return result;
    }
    
 public static GatewayFilterVO getDeployedFilterById(String gatewayName, 
		 							String id) throws Exception {
        
	 GatewayFilterVO result = null;
	 GatewayFilterVO Original = null;
        String type = ServiceGateway.getGatewayType(gatewayName);
        
        if(StringUtils.isBlank(type) || type.equals("None"))
            throw new Exception("Gateway type is not available.");
        
        if(type.equals("Push")) {
        	Original = PushGatewayFilterUtil.getPushFilterById(id);
            if(Original != null) {
            	List<PushGatewayFilterVO> deployedFilters = 
        			PushGatewayFilterUtil.getPushFiltersByName(gatewayName, Original.getUName(), null);
            	for(PushGatewayFilterVO deployedFilter:deployedFilters) {
                    if(!deployedFilter.getSys_id().equals(id))
                        result = deployedFilter;
                }
            }
        }
        else if(type.equals("Pull")) {
        	Original = PullGatewayFilterUtil.getPullFilter(id);
        	if(Original != null) {
	        	List<PullGatewayFilterVO> deployedFilters = 
	    			PullGatewayFilterUtil.getPullFiltersByName(gatewayName, Original.getUName(), null);
	        	for(PullGatewayFilterVO deployedFilter:deployedFilters) {
	                if(!deployedFilter.getSys_id().equals(id))
	                    result = deployedFilter;
	            }
	        }
        }
        else if(type.equals("MSG")) {
            Original = MSGGatewayFilterUtil.getMSGFilter(id);
            if(Original != null) {
                List<MSGGatewayFilterVO> deployedFilters = 
                    MSGGatewayFilterUtil.getMSGFiltersByName(gatewayName, Original.getUName(), null);
                for(MSGGatewayFilterVO deployedFilter:deployedFilters) {
                    if(!deployedFilter.getSys_id().equals(id))
                        result = deployedFilter;
                }
            }
        }

        return result;
    }
    
    public static boolean deleteFilter(String gatewayName, String[] sysIds) throws Exception {
        
        boolean result = false;
        String type = ServiceGateway.getSDKGatewayType(gatewayName);
        
        if(StringUtils.isBlank(type) || type.equals("None"))
            throw new Exception("Gateway type is not available.");
        
        if(type.equalsIgnoreCase("Push"))
            result = PushGatewayFilterUtil.deletePushFilters(gatewayName, sysIds);
        
        else if(type.equalsIgnoreCase("Pull"))
            result = PullGatewayFilterUtil.deletePullFilters(gatewayName, sysIds);

        else if(type.equalsIgnoreCase("MSG"))
            result = MSGGatewayFilterUtil.deleteMSGFilters(gatewayName, sysIds);

        return result;
    }

    protected static boolean filterExists(String filterName, List<String> filters) {
        
        boolean exists = false;

        if(filters.size() == 0)
            return exists;
        
        for(int i=0; i<filters.size(); i++) {
            if(filters.get(i).equals(filterName)) {
                exists = true;
                break;
            }
        }
        
        return exists;
    }
    
    public static Long getDeployedGatewayFilterCount(String entityFilterClass, String queueName, String gatewayName) throws Exception {
       
             Long deployedFilterCount = 0L;
             StringBuilder sb = new StringBuilder();
             sb.append("select count(*) from ").append(entityFilterClass).append(" where ");
             sb.append("u_queue=:uqueue and u_gateway_name=:ugatewayname");
             Log.log.debug(sb.toString());
             Map<String, Object> queryParams = new HashMap<String, Object>();

             try {
                 
                 queryParams.put("uqueue", queueName);
                 queryParams.put("ugatewayname", gatewayName);
                 
                 List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sb.toString(), queryParams);
                 if (list != null && list.size() == 1)
                 {
                     deployedFilterCount = (Long) list.get(0);
                 }
                 
             } catch(Exception e) {
                 Log.log.error(e.getMessage(), e);
                 throw e;
             }
             return deployedFilterCount;
         }
    /**
     *  Utility method to identify whether the string is in the JSON format 
     * @param val
     * @return
     */
    public static JSONObject jsonField(String val) {
    	JSONObject convJSON = null;
    	try {
    		if(val!= null)
    			convJSON = JSONObject.fromObject(val);
    	
    	}catch(JSONException jex) {
    		Log.log.warn("Error converting JSON text field to JSON", jex);
    	}
    	
    	return convJSON;
    }
    
    /**
     * <br> Get the encrypt field value of JSON and encrypt the value</br> 
     * <br> field, if doEncrypt is true and Decrypt if doEncrypt is False</br>
     * @param jsonData
     * @param doEncrypt
     * @return
     */
    @SuppressWarnings("unchecked")
    public static String encryptDecryptJSONVal(JSONObject jsonData, boolean doEncrypt) {
    	Iterator<String> keys = jsonData.keys();
    	
    	try {
    		while(keys.hasNext()) {
    			String key = keys.next();
    			if(jsonData.getString(key) != null) {
    				String val = jsonData.getString(key);
    				//This Condition needs some explaining, So we are encrypting only if
    				//The Check box says encrypt and the value in the JSON is not already encrypted
    				if(key.equalsIgnoreCase("encrypt") && val.equalsIgnoreCase("true")
    						&& doEncrypt && !CryptUtils.isEncrypted(jsonData.getString("value"))) {
    					jsonData.put("value",CryptUtils.encrypt(jsonData.getString("value")));
    				}
    			}
    		}
    	}catch(Exception ex) {
    		Log.log.warn("Error encrypting the JSOn value filed of the filter", ex);
    		return "";
    	}
    	
    	return jsonData.toString();
    }
    
}
