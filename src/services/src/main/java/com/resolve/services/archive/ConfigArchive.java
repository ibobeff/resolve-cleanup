/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.archive;

import java.time.Duration;

import com.resolve.util.ConfigMap;
import com.resolve.util.Log;
import com.resolve.util.XDoc;

public class ConfigArchive extends ConfigMap
{
    private static final long serialVersionUID = 7160569253687427224L;
    
    public static final int MAX_BLOCK_SIZE = 10000; // Max batch size
    public static final int MIN_BLOCK_SIZE = 100; // Min batch size
    // Small Block Size is 20% or 1/5 or 0.2 times Block Size
    public static final int SMALL_BLOCK_SIZE_FACTOR_DENOMINATOR = 5;
    
	static boolean active = true;
	/*
	 * Duration (in days) after which worksheets and its associated data (provided associated data is not updated after
	 * worksheets becomes due for archival) is due for archival.
	 *  
	 * Worksheets and its associated data such as WSDATA, task results, process requests etc 
	 * NOT updated after (Current YYYYMMDD 00:00:00.000 (GMT) - expiry (in days)) - 1 ms instant is due for archival.
	 * 
	 * Current days worksheets cannot be archived as smallest granularity of ES worksheet, process request, task results
	 * and other associated indices is in days.
	 * 
	 * Archival primarily works off of daily worksheet and its associated data (WSDATA, task results, process requests etc)
	 * indices.
	 * 
	 * Main archiving goals (in order of priority)
	 * 
	 * #1 Reduce # of daily indices
	 * #2 Reduce # of documents (size) in non daily indices
	 * 
	 *  Without loosing consistency of archived worksheets and its associated data such as WSDATA, AT task results,
	 *  process requests etc.
	 */
    static long expiry = 1;						// 1 day = 24h * 60m * 60s = 86400 secs  
    static String schedule = "0 0 2 * * ?";     // 2am daily
    /*
     * Ignore worksheets which are due for archival but has associated OPEN process requests
     * updated before "cleanup" duration in days from "expiry" instant.
     */
    static long cleanup = 7;               		// 1 week = 7 days * 24h * 60m * 60s = 604800 secs
    static int blocksize = MAX_BLOCK_SIZE;      // ES query batch/scroll/cursor size
    // DB table batch size
    static int smalltableblocksize = MAX_BLOCK_SIZE / SMALL_BLOCK_SIZE_FACTOR_DENOMINATOR;
    
    // Make sure both flags, sirArchive and sirRestore, should not be true at the same time.
    static boolean sirBackup = true;           // Backup all SIR components runtime data (Notes, Artifacts, Attachments and AuditLogs) in SQL database
    static boolean sirRestore = false;         // Restore backed up SIR components runtime data from SQL database to Elasticsearch
    /*
     * Cron job schedule for SIR backup process. (Restore does not need to be in scheduler) run every 17 minutes interval.
     * This backs up updated or new artifacts, attachments, audit logs and notes for SIR.
     */
    static String sirArchiveSchedule = "0 17/17 * * * ?";          
    
    
    static long sirExpiry = 1;					// Expiry for SIR objects in ES 
    											// (Worksheets & related objects + SIR specific objects) in days
    static long sirCleanup = 7;					// Cleanup for SIR Worksheet related objects in ES in days
    
    public ConfigArchive(XDoc config) throws Exception
    {
        super(config);
        
        define("active", BOOLEAN, "./ARCHIVE/@ACTIVE");
        define("expiry", LONG, "./ARCHIVE/@EXPIRY");
        define("schedule", STRING, "./ARCHIVE/@SCHEDULE");
        define("cleanup", LONG, "./ARCHIVE/@CLEANUP");
        define("blocksize", INT, "./ARCHIVE/@BLOCKSIZE");
        define("smalltableblocksize", INT, "./ARCHIVE/@SMALLTABLEBLOCKSIZE");
        
        define("sirBackup", BOOLEAN, "./ARCHIVE/SIRBACKUP/@ACTIVE");
        define("sirRestore", BOOLEAN, "./ARCHIVE/SIRRESTORE/@ACTIVE");
        define("sirArchiveSchedule", STRING, "./ARCHIVE/SIRARCHIVE/@SCHEDULE");
        define("sirExpiry", LONG, "./ARCHIVE/@SIREXPIRY");
        define("sirCleanup", LONG, "./ARCHIVE/@SIRCLEANUP");
    } // ConfigArchive
    
    public void load()
    {
        loadAttributes();
        if (Log.log.isTraceEnabled()) {
        Log.log.trace(String.format("Non-SIR Config - active [%b], expiry [%d], schedule [%s], " +
        						    "cleanup [%s], blocksize [%d], smalltableblocksize [%d]", isActive(), getExpiry(), 
        						   getSchedule(), getCleanup(), getBlocksize(), getSmalltableblocksize()));
        Log.log.trace(String.format("SIR Config - sirBackup [%b], sirRestore [%b], " +
        						    "sirArchiveSchedule [%s] sirExpiry [%d], sirCleanup [%d]", isSirBackup(), isSirRestore(), 
        						    getSirArchiveSchedule(), getSirExpiry(), getSirCleanup()));
        }
    } // load

    public void save()
    {
        saveAttributes();
    } // save
    
    // In Days
    public static long getExpiry()
    {
        return expiry;
    }
    
    public long getExpirySecs() {
        return expiry * Duration.ofDays(1).toMinutes() * 60;
    }
    
    public static Duration getExpiryDuration() {
        return  Duration.ofDays(expiry);
    }
    
    public void setExpiry(long expiry)
    {
        ConfigArchive.expiry = expiry;
    }
    
    public void setExpiryDays(Duration expiryDuration) {
    	ConfigArchive.expiry = expiryDuration.toDays();
    }
    
    public String getSchedule()
    {
        return schedule;
    }

    public void setSchedule(String schedule)
    {
        ConfigArchive.schedule = schedule;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        ConfigArchive.active = active;
    }

    // In Days
    public static long getCleanup()
    {
        return cleanup;
    }
    
    public long getCleanupSecs()
    {
        return cleanup * Duration.ofDays(1).toMinutes() * 60;
    }
    
    public static Duration getCleanupDuration() {
        return  Duration.ofDays(cleanup);
    }
    
    public void setCleanup(long cleanup)
    {
        ConfigArchive.cleanup = cleanup;
    }
    
    public void setCleanupDuration(Duration cleanupDuration) {
    	ConfigArchive.cleanup = cleanupDuration.toDays();
    }
    
    public static int getBlocksize()
    {
        return blocksize;
    }

    public void setBlocksize(int blocksize)
    {
    	int tBlockSize = blocksize;
    	
    	if (tBlockSize > MAX_BLOCK_SIZE) {
    		tBlockSize = MAX_BLOCK_SIZE;
    	} else if (tBlockSize < MIN_BLOCK_SIZE) {
    		tBlockSize = MIN_BLOCK_SIZE;
    	}
    	
    	tBlockSize = ((int)(tBlockSize / SMALL_BLOCK_SIZE_FACTOR_DENOMINATOR)) * 
    				 SMALL_BLOCK_SIZE_FACTOR_DENOMINATOR;
    	
        ConfigArchive.blocksize = tBlockSize;
        ConfigArchive.smalltableblocksize = ConfigArchive.blocksize / 
        									SMALL_BLOCK_SIZE_FACTOR_DENOMINATOR;
    }

    public static int getSmalltableblocksize()
    {
        return smalltableblocksize;
    }

    public void setSmalltableblocksize(int smalltableblocksize)
    {
    	int tSmallTableBlockSize = smalltableblocksize;
    	
    	if (tSmallTableBlockSize > (MAX_BLOCK_SIZE / SMALL_BLOCK_SIZE_FACTOR_DENOMINATOR)) {
    		tSmallTableBlockSize = MAX_BLOCK_SIZE / SMALL_BLOCK_SIZE_FACTOR_DENOMINATOR;
    	} else if (tSmallTableBlockSize < (MIN_BLOCK_SIZE / SMALL_BLOCK_SIZE_FACTOR_DENOMINATOR)) {
    		tSmallTableBlockSize = MIN_BLOCK_SIZE / SMALL_BLOCK_SIZE_FACTOR_DENOMINATOR;
    	}
    	
    	tSmallTableBlockSize = ((int)(tSmallTableBlockSize / SMALL_BLOCK_SIZE_FACTOR_DENOMINATOR)) *
    						   SMALL_BLOCK_SIZE_FACTOR_DENOMINATOR;
    	
        ConfigArchive.smalltableblocksize = tSmallTableBlockSize;
        ConfigArchive.blocksize = ConfigArchive.smalltableblocksize * 
        						  SMALL_BLOCK_SIZE_FACTOR_DENOMINATOR;
    }

    public boolean isSirBackup()
    {
        return sirBackup;
    }

    public void setSirBackup(boolean sirBackup)
    {
    	ConfigArchive.sirBackup = sirBackup;
    }

    public boolean isSirRestore()
    {
        return sirRestore;
    }

    public static void setSirRestore(boolean sirRestore)
    {
    	ConfigArchive.sirRestore = sirRestore;
    }

    public String getSirArchiveSchedule()
    {
        return sirArchiveSchedule;
    }

    public void setSirArchiveSchedule(String sirArchiveSchedule)
    {
        ConfigArchive.sirArchiveSchedule = sirArchiveSchedule;
    }
    
    public static long getSirExpiry() {
        return sirExpiry;
    }
    
    public static Duration getSirExpiryDuration() {
        return  Duration.ofDays(sirExpiry);
    }
    
    public void setSirExpiry(long sirExpiry) {
        ConfigArchive.sirExpiry = sirExpiry;
    }
    
    public void setSirExpiryDays(Duration sirExpiryDuration) {
    	ConfigArchive.sirExpiry = sirExpiryDuration.toDays();
    }
    
    public static long getSirCleanup() {
        return sirCleanup;
    }
    
    public static Duration getSirCleanupDuration() {
        return  Duration.ofDays(sirCleanup);
    }
    
    public void setSirCleanup(long sirCleanup) {
        ConfigArchive.sirCleanup = sirCleanup;
    }
    
    public void setSirCleanupDays(Duration sirCleanupDuration) {
    	ConfigArchive.sirCleanup = sirCleanupDuration.toDays();
    }
} // ConfigArchive