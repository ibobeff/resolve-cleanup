/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.component.container;

import java.util.ArrayList;
import java.util.Collection;

import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.RSContainer;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.DecisionTree;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Namespace;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.Runbook;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;


/**
 * Process is only a publisher. It is a CONTAINER
 * 
 * Process cannot have any other Process inside it. Its a collection of resources
 * 
 * @author jeet.marwah
 *
 */
public class Process extends RSComponent implements RSContainer
{
    private static final long serialVersionUID = 9211366523145386348L;

    public static final String TYPE = "process";

    private Collection<Team> teams = null;
    private Collection<User> users = null;
    private Collection<Runbook> runbooks = null;
    private Collection<DecisionTree> dts = null;
    private Collection<Document> documents = null;
    private Collection<ActionTask> actiontasks = null;
    private Collection<Rss> rss = null;
    private Collection<Forum> forums = null;
    private Collection<Namespace> namespaces = null;
   
    public Process()
    {
        super(NodeType.PROCESS);

        this.setTeams(new ArrayList<Team>());
        this.setForums(new ArrayList<Forum>());
        this.setNameSpaces(new ArrayList<Namespace>());
        this.setUsers(new ArrayList<User>());
        this.setRunbooks(new ArrayList<Runbook>());
        this.setDecisionTrees(new ArrayList<DecisionTree>());
        this.setDocuments(new ArrayList<Document>());
        this.setActiontasks(new ArrayList<ActionTask>());
        this.setRsss(new ArrayList<Rss>());

    }
    
    public Process(String sysId, String name)
    {
        this();
        setSys_id(sysId);
        setName(name);
    }
    
    public Process(ResolveNodeVO node) throws Exception
    {
        super(node);
        this.setTeams(new ArrayList<Team>());
        this.setForums(new ArrayList<Forum>());
        this.setNameSpaces(new ArrayList<Namespace>());
        this.setUsers(new ArrayList<User>());
        this.setRunbooks(new ArrayList<Runbook>());
        this.setDecisionTrees(new ArrayList<DecisionTree>());
        this.setDocuments(new ArrayList<Document>());
        this.setActiontasks(new ArrayList<ActionTask>());
        this.setRsss(new ArrayList<Rss>());
    }
    
    /**
     * @return the actiontasks
     */
    public Collection<ActionTask> getActiontasks()
    {
        return actiontasks;
    }

    /**
     * @param actiontasks the actiontasks to set
     */
    public void setActiontasks(Collection<ActionTask> actiontasks)
    {
        this.actiontasks = actiontasks;
    }

    /**
     * @return the rss
     */
    public Collection<Rss> getRss()
    {
        return rss;
    }

    /**
     * @param rss the rss to set
     */
    public void setRss(Collection<Rss> rss)
    {
        this.rss = rss;
    }

    /**
     * @return the forums
     */
    public Collection<Forum> getForums()
    {
        return forums;
    }

    /**
     * @param forums the forums to set
     */
    public void setForums(Collection<Forum> forums)
    {
        this.forums = forums;
    }
    
    public Collection<Namespace> getNameSpaces()
    {
        return this.namespaces;
    }
    
    public void setNameSpaces(Collection<Namespace> namespaces)
    {
        this.namespaces = namespaces;
    }

    /**
     * @return the runbooks
     */
    public Collection<Runbook> getRunbooks()
    {
        return runbooks;
    }

    /**
     * @return the decisiontrees
     */
    public Collection<DecisionTree> getDecisionTrees()
    {
        return dts;
    }
    
    public void setTeams(Collection<Team> teams)
    {
        this.teams = teams;
    }
    
    public Collection<Team> getTeams()
    {
        return this.teams;
    }
    
    public void setUsers(Collection<User> users)
    {
        this.users = users;
    }
    
    public Collection<User> getUsers()
    {
        return this.users;
    }
    
    public void setRunbooks(Collection<Runbook> runbooks)
    {
        this.runbooks = runbooks;
    }
    
    public void setDecisionTrees(Collection<DecisionTree> dts)
    {
        this.dts = dts;
    }
   
    public void setRsss(Collection<Rss> rss)
    {
        this.rss = rss;
    }
    
    public Collection<Rss> getRsss()
    {
        return this.rss;
    }
    
    public void setDocuments(Collection<Document> documents)
    {
        this.documents = documents;
    }
    
    public Collection<Document> getDocuments()
    {
        return this.documents;
    }
}
