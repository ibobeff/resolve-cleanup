/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.Iterator;
import java.util.Map;

import com.resolve.persistence.dao.ExchangeserverFilterDAO;
import com.resolve.persistence.model.ExchangeserverFilter;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExchangeGatewayUtil extends GatewayUtil
{
    private static volatile ExchangeGatewayUtil instance = null;
    public static ExchangeGatewayUtil getInstance()
    {
        if(instance == null)
        {
            instance = new ExchangeGatewayUtil();
        }
        return instance;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public void setFilters(String modelName, Map filters)
    {
        try
        {
        	HibernateProxy.execute(() -> {
        		ExchangeserverFilterDAO filterDao = HibernateUtil.getDAOFactory().getExchangeserverFilterDAO();

                String queue = deleteByQueue(ExchangeserverFilter.class.getSimpleName(), filters);

                // update each filter
                for (Iterator i = filters.entrySet().iterator(); i.hasNext();)
                {
                    Map.Entry entry = (Map.Entry) i.next();
//                    String id = (String) entry.getKey();
                    String filterStringMap = (String) entry.getValue();
                    Map filterMap = StringUtils.stringToMap(filterStringMap);

                    String name = (String) filterMap.get("ID");
                    String eventEventId = (String) filterMap.get("EVENT_EVENTID");
                    String runbook = (String) filterMap.get("RUNBOOK");
                    String script = (String) filterMap.get("SCRIPT");
                    Integer order = new Integer((String) filterMap.get("ORDER"));
                    Integer interval = new Integer((String) filterMap.get("INTERVAL"));
                    Boolean active = new Boolean((String) filterMap.get("ACTIVE"));
                    String criteriaString = (String) filterMap.get("CRITERIA");
                    Map<String, String> criteriaMap = (Map) StringUtils.stringToObj(criteriaString);

                    ExchangeserverFilter row = new ExchangeserverFilter();
                    row.setUQueue(queue);
                    row.setUName(name);
                    row.setUEventEventId(eventEventId);
                    row.setURunbook(runbook);
                    row.setUScript(script);
                    setCriteria(row, criteriaMap);
                    row.setUOrder(order);
                    row.setUInterval(interval);
                    row.setUActive(active);

                    //Set the created or modified by here.
                    String sysModifiedBy = (String) filterMap.get(Constants.SYS_UPDATED_BY);
                    row.setSysCreatedBy(sysModifiedBy);
                    row.setSysUpdatedBy(sysModifiedBy);

                    filterDao.save(row);
                }

        	});
            
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    } // setFilters

    @SuppressWarnings("rawtypes")
    private static void setCriteria(ExchangeserverFilter row, Map<String, String> criteria)
    {
        if(criteria != null && !criteria.isEmpty())
        {
            for(Iterator itr = criteria.entrySet().iterator(); itr.hasNext();)
            {
                Map.Entry entry = (Map.Entry) itr.next();
                String key = (String) entry.getKey();
                String value = (String) entry.getValue();

                if(key.contains(".subject"))
                {
                    row.setUSubject(value);
                }
                else if(key.contains(".content"))
                {
                    row.setUContent(value);
                }
                else if(key.contains(".sendername"))
                {
                    row.setUSendername(value);
                }
                else if(key.contains(".senderaddress"))
                {
                    row.setUSenderaddress(value);
                }
            }
        }
    }

}
