/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.migration.entities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.resolve.persistence.model.GroupRoleRel;
import com.resolve.persistence.model.Groups;
import com.resolve.persistence.model.UserGroupRel;
import com.resolve.persistence.model.UserRoleRel;
import com.resolve.persistence.model.Users;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.graph.ResolveGraphNode;
import com.resolve.services.hibernate.util.SocialUtil;
import com.resolve.services.hibernate.vo.ResolveNodePropertiesVO;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MigrateUser extends Entities
{
    private String sysId;
    private String userRoles = "";

    public MigrateUser(String sysId)
    {
        this.sysId = sysId;
    }

    private Users migrateUser() throws Exception
    {
        Users user = findUserById(sysId);
        
        if (user != null)
        {
            Set<ResolveNodePropertiesVO> props = new HashSet<ResolveNodePropertiesVO>();
            
            if(StringUtils.isNotBlank(user.getUFirstName()) || StringUtils.isNotBlank(user.getULastName()))
            {
                String displayName = StringUtils.isNotBlank(user.getUFirstName()) ? user.getUFirstName() : "";

                displayName += (StringUtils.isNotBlank(displayName) ? " " : "");
                
                displayName += (StringUtils.isNotBlank(user.getULastName()) ? user.getULastName() : "");
                
                displayName.trim();
                
                if(StringUtils.isNotBlank(displayName))
                {
                    props.add(createProp(ResolveGraphNode.DISPLAYNAME, displayName));
                }
            }

            ResolveNodeVO node = new ResolveNodeVO();
            
            copyBaseModelToResolveNode(node, user);
            
            node.setUCompName(user.getUUserName());
            node.setUCompSysId(user.getSys_id());
            node.setUType(NodeType.USER);
            node.setUMarkDeleted(false);
            node.setULock(user.ugetULockedOut());
            node.setUPinned(false);
            
            if(!props.isEmpty())
            {
                node.setProperties(props);
            }
            
            //roles
            node.setUReadRoles(SocialUtil.convertRolesToSocialRolesString(userRoles));

            //persist
            persistNode(node);
            Log.log.trace(Thread.currentThread().getName() + " : Migrated Users:" + user.getUUserName());
        }
        
        return user;
    }
    
//    @Override
    public void run()
    {
      //there is no need to use the save api for the doc as its just fixing the wiki content.
        Users user = null;

        try
        {
            user = migrateUser();
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the User during the migration script. Please ignore this error as this will not affect the migration process." +
                          (user != null ? " The User is " + user.getUUserName() : ""), e);
        }

    }

    private Users findUserById(String sysId) throws Exception
    {
        Users user = null;
        Set<String> roles = new HashSet<String>();
        
        try
        {
          HibernateProxy.setCurrentUser("system");
        	user = (Users) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getUsersDAO().findById(sysId);
            });
            
            //user-roles
            Collection<UserRoleRel> userRoleRel = user.getUserRoleRels();
            if(userRoleRel != null)
            {
                for(UserRoleRel rel : userRoleRel)
                {
                    if(rel.getRole() != null)
                        roles.add(rel.getRole().getUName());
                }
            }
            
            //user-groups
            Collection<UserGroupRel> userGroupRel = user.getUserGroupRels();
            if(userGroupRel != null)
            {
                for(UserGroupRel rel : userGroupRel)
                {
                    Groups group = rel.getGroup();
                    if(group != null)
                    {
                        Collection<GroupRoleRel> groupRoleRel = group.getGroupRoleRels();
                        if(groupRoleRel != null)
                        {
                            for(GroupRoleRel relGR : groupRoleRel)
                            {
                                if(relGR.getRole() != null)
                                {
                                    roles.add(relGR.getRole().getUName());
                                }
                            }
                        }
                    }
                }
            }

        }
        catch (Exception e)
        {
            Log.log.error("Error getting Users with sysId: " + sysId, e);
            throw e;
        }
        
        //prepare the user roles 
        userRoles = StringUtils.convertCollectionToString(roles.iterator(), " ");

        return user;
    }

    @Override
    public Collection<String> call() throws Exception
    {
        Users user = null;
        Collection<String> pCompSysIds = new ArrayList<String>();
        
        try
        {
            user = migrateUser();
            
            if(user != null)
            {
                pCompSysIds.add(user.getSys_id());
            }
        }
        catch(Exception e)
        {
            Log.log.error("Error saving the User during the migration script. Please ignore this error as this will not affect the migration process." +
                          (user != null ? " The User is " + user.getUUserName() : ""), e);
            throw e;
        }
        
        return pCompSysIds;
    }
}
