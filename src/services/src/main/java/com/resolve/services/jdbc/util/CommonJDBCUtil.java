/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.jdbc.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.model.ResolveEvent;
import com.resolve.util.StringUtils;

public class CommonJDBCUtil
{
    public static void logEvent(String msg, String username)
    {
        if(StringUtils.isNotBlank(msg))
        {
            if(StringUtils.isEmpty(username))
            {
                username = "system";
            }

            ResolveEvent data = new ResolveEvent();
            data.setUValue(msg);
            
            List<ResolveEvent> list = new ArrayList<ResolveEvent>();
            list.add(data);
            
            Map<String, String> attColMap = MappingAttributeToColumnsUtil.getMappingForResolveEvent();
            
            new GenericJDBCHandler(ResolveEvent.class, "resolve_event", attColMap, username).insertObjects(list);
            
        }
    }
}
