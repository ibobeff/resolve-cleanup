/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.util.Collection;
import java.util.Set;

import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.WikiAttachment;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.util.WikiAttachmentUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ReplaceHelper
{
    private String fromDocument = null; 
    private String toDocument = null;
    private String username = null;
    
    //switches
    private boolean validateUser = true;
    
    public ReplaceHelper(String fromDocument, String toDocument, String username)
    {
        if(StringUtils.isEmpty(username) || StringUtils.isEmpty(toDocument) || StringUtils.isEmpty(fromDocument))
        {
            throw new RuntimeException("Username, From and To Document names are mandatory");
        }
        
        //check if the names are not same
        if (fromDocument.equalsIgnoreCase(toDocument))
        {
            throw new RuntimeException("Old and New Document '" + toDocument + "' have the same name.");
        }
        
        this.fromDocument = fromDocument;
        this.toDocument = toDocument;
        this.username = username;
    }
    
    public boolean isValidateUser()
    {
        return validateUser;
    }

    public void setValidateUser(boolean validateUser)
    {
        this.validateUser = validateUser;
    }
    
    public WikiDocumentVO replace() throws Exception
    {
        replaceWiki();
        
        //get the new wiki created and return
        WikiDocumentVO newWiki = WikiUtils.getWikiDoc(null, toDocument, username);
        return newWiki;
        
    }
    
    //private apis
    private void replaceWiki() throws Exception
    {
        WikiDocument toDoc = WikiUtils.getWikiDocumentModel(null, toDocument, username, true);
        if(toDoc == null)
        {
            Log.log.info("will copy the document " + fromDocument + " to " + toDocument);
            
            //if the to document is not present, then just copy it
            WikiUtils.copyDocument(fromDocument, toDocument, false, true, username);
        }
        else
        {
            WikiDocument fromDoc = WikiUtils.getWikiDocumentModel(null, fromDocument, username, true);
            if(fromDoc == null)
            {
                throw new WikiException("Document '" + fromDocument + "' does not exist.");
            }
            
            //validate if the user has rights to replace this document or not, only users with admin roles can do this operation
            //this is true when there is only 1 document to rename
            if(isValidateUser())
            {
                Set<String> userRoles = UserUtils.getUserRoles(username);
                String docRoles = fromDoc.getAccessRights().getUAdminAccess();
                boolean hasRights = com.resolve.services.util.UserUtils.hasRole(username, userRoles, docRoles);
                if(!hasRights)
                {
                    throw new Exception("User " + username + " does not have role on the document " + fromDocument + " for Replace operation.");
                }
            }
            
            //make the 'fromDoc' to a 'toDoc' . That we can ensure everything is updated
            fromDoc.setSys_id(toDoc.getSys_id());
            fromDoc.setSysCreatedBy(toDoc.getSysCreatedBy());
            fromDoc.setSysCreatedOn(toDoc.getSysCreatedOn());
            fromDoc.setSysUpdatedBy(toDoc.getSysUpdatedBy());
            fromDoc.setSysUpdatedOn(toDoc.getSysUpdatedOn());
            
            fromDoc.setUFullname(toDoc.getUFullname());
            fromDoc.setUName(toDoc.getUName());
            fromDoc.setUNamespace(toDoc.getUNamespace());
            
            AccessRights toDocAR = toDoc.getAccessRights();
            AccessRights fromDocAR = fromDoc.getAccessRights();
            fromDocAR.setSys_id(toDocAR.getSys_id());
            fromDocAR.setSysCreatedBy(toDocAR.getSysCreatedBy());
            fromDocAR.setSysCreatedOn(toDocAR.getSysCreatedOn());
            fromDocAR.setSysUpdatedBy(toDocAR.getSysUpdatedBy());
            fromDocAR.setSysUpdatedOn(toDocAR.getSysUpdatedOn());
            fromDocAR.setUResourceId(fromDoc.getSys_id());
            fromDocAR.setUResourceName(fromDoc.getUFullname());
            
            
            //update the todocument 
//            toDoc.setUContent(fromDoc.getUContent());
//            toDoc.setUModelProcess(fromDoc.getUModelProcess());
//            toDoc.setUModelException(fromDoc.getUModelException());
//            toDoc.setUModelFinal(fromDoc.getUModelFinal());
//            toDoc.setUDecisionTree(fromDoc.getUDecisionTree());
//            toDoc.setUIsHidden(fromDoc.getUIsHidden());
//            toDoc.setUIsDeleted(fromDoc.getUIsDeleted());
//            toDoc.setUIsActive(fromDoc.getUIsActive());
//            toDoc.setUIsLocked(fromDoc.getUIsLocked());
//            toDoc.setUIsRoot(fromDoc.getUIsRoot());
            
            //save with attachments 
            Collection<WikiAttachment> attachments = WikiAttachmentUtil.prepareAttachment(fromDocument, username);
            
            //save it
            SaveHelper saveHelper = new SaveHelper(fromDoc, username);
            saveHelper.setAttachments(attachments);
            saveHelper.setUserShouldFollow(false);
            saveHelper.setComment("Replaced from " + fromDocument);
            saveHelper.save();
        }
    }
}
