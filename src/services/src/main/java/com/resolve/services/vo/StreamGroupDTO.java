/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.resolve.services.graph.social.model.RSComponent;

public class StreamGroupDTO
{
    private String displayName = "";
    private String sys_id = "";
    private boolean expanded = true;
    private List<? extends Object> records = null;

    public String getDisplayName()
    {
        return displayName;
    }

    public StreamGroupDTO setDisplayName(String displayName)
    {
        this.displayName = displayName;
        this.setSys_id(displayName);
        return this;
    }

    public List<? extends Object> getRecords()
    {
        return records;
    }

    public StreamGroupDTO setRecords(List<? extends Object> records)
    {
        this.records = records;
        return this;
    }
    
    public <T extends RSComponent> StreamGroupDTO setStreamsFromCollection(Collection<T> c, boolean canBeFolder) {
        return setStreamsFromCollection(c, canBeFolder, null);
    }

    public <T extends RSComponent> StreamGroupDTO setStreamsFromCollection(Collection<T> c, boolean canBeFolder, RSComponent parentComp)
    {
        List<StreamDTO> temp = new ArrayList<StreamDTO>();
        RSComponent.sortCollection(c);
        if (c != null)
        {
            String parentSysId = "";
            String parentCompType = "";
            if( parentComp != null ) {
                parentSysId = parentComp.getSys_id(); 
                parentCompType = parentComp.getClass().getSimpleName();
            }
            for (T item : c)
            {
                temp.add(new StreamDTO(item, canBeFolder,parentSysId, parentCompType ));
            }
        }
        return this.setRecords(temp);
    }

    public boolean isExpanded()
    {
        return expanded;
    }

    public StreamGroupDTO setExpanded(boolean expanded)
    {
        this.expanded = expanded;
        return this;
    }

    public String getSys_id()
    {
        return sys_id;
    }

    public void setSys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }

}
