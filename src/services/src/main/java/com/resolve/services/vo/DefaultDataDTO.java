/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;


public class DefaultDataDTO
{
    private String fieldName;
    private String defaultValue;
    
    public DefaultDataDTO() {}
    public DefaultDataDTO(String fieldName, String defaultValue) 
    {
        this.fieldName = fieldName;
        this.defaultValue = defaultValue;
    }
    
    public String getFieldName()
    {
        return fieldName;
    }
    public void setFieldName(String fieldName)
    {
        this.fieldName = fieldName;
    }
    public String getDefaultValue()
    {
        return defaultValue;
    }
    public void setDefaultValue(String defaultValue)
    {
        this.defaultValue = defaultValue;
    }
    
    
    

}
