package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.Restrictions;

import com.resolve.persistence.model.ArtifactConfiguration;
import com.resolve.persistence.model.ArtifactType;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ArtifactConfigurationVO;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.util.UpdateChecksumUtil;
import com.resolve.services.vo.ArtifactConfigurationDTO;
import com.resolve.util.Log;

public class ArtifactConfigurationUtil {
	private static final String ERROR_NOT_FOUND = "Artifact configuration not found";
	private static final String ERROR_INCORRECT_NAME = "Artifact configuration should have correct name";
	private static final String ERROR_UNKNOWN_ARTIFACT_TYPE = "Unknown artifact type";
	private static final String ERROR_UNKNOWN_AUTOMATION = "Unknown automation";
	private static final String ERROR_UNKNOWN_ACTION_TASK = "Unknown action task";
	private static final String ERROR_MISSING_TYPE = "Artifact configuration is missing a type";
	private static final String SYS_ID = "sys_id";
	private static final String U_NAME = "UName";
	
	public static List<ArtifactConfigurationVO> getAllArtifacts() {
		List<ArtifactConfigurationVO> artifactVOs = new ArrayList<>();
		try {
			HibernateProxy.execute(() -> {
				List<ArtifactConfiguration> result = HibernateUtil.getDAOFactory().getArtifactConfigurationDAO().findAll();
				for (ArtifactConfiguration artifact : result) {
					artifactVOs.add(convertToArtifactVO(artifact));
				}
			});
			
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}
		return artifactVOs;
	}

	public static ArtifactConfigurationVO getArtifactByName(String name) {
		
		try {
			
			return (ArtifactConfigurationVO) HibernateProxy.execute(() -> {
				ArtifactConfigurationVO vo = null;
				ArtifactConfiguration result = (ArtifactConfiguration) HibernateUtil.getCurrentSession().createCriteria(ArtifactConfiguration.class)
						.add(Restrictions.eq(U_NAME, name).ignoreCase()).uniqueResult();
				if (result != null) {
					vo = convertToArtifactVO(result);
				}
				return vo;
				
			});
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
			return null;
		}
		
	}
	
	public static ArtifactConfigurationVO saveArtifact(ArtifactConfigurationDTO dto) throws Exception {
		ArtifactConfigurationVO artifactVO = null;
		try {
			if (StringUtils.isEmpty(dto.getUName())) {
				throw new IllegalArgumentException(ERROR_INCORRECT_NAME);
			}

			if (dto.getArtifactType() == null || dto.getArtifactType().getUName() == null) {
				throw new IllegalArgumentException(ERROR_MISSING_TYPE);
			}

			if ((dto.getTask() != null && dto.getAutomation() != null)
					|| (dto.getTask() == null && dto.getAutomation() == null)) {
				throw new IllegalArgumentException("Artifact entity can contain only task or automation.");
			}

			artifactVO = (ArtifactConfigurationVO) HibernateProxy.execute(() -> {
				ArtifactType artifactType = (ArtifactType) HibernateUtil.getCurrentSession().createCriteria(ArtifactType.class)
						.add(Restrictions.eq(U_NAME, dto.getArtifactType().getUName()).ignoreCase()).uniqueResult();

				if (artifactType == null) {
					throw new IllegalArgumentException(ERROR_UNKNOWN_ARTIFACT_TYPE);
				}

				ArtifactConfiguration oldArtifact = (ArtifactConfiguration) HibernateUtil.getCurrentSession().createCriteria(ArtifactConfiguration.class)
						.add(Restrictions.eq(U_NAME, dto.getUName()).ignoreCase()).uniqueResult();
				
				if (oldArtifact == null) {
					oldArtifact = new ArtifactConfiguration();
				}
				
				oldArtifact.setUName(dto.getUName());
				oldArtifact.setParam(dto.getParam());
				oldArtifact.setArtifactType(artifactType);
				
				if (dto.getAutomation() != null) {
					WikiDocument automation = (WikiDocument) HibernateUtil.getCurrentSession()
							.createCriteria(WikiDocument.class)
							.add(Restrictions.eq(SYS_ID, dto.getAutomation().getId()).ignoreCase()).uniqueResult();
					if (automation == null) {
						throw new IllegalArgumentException(ERROR_UNKNOWN_AUTOMATION);
					}
					oldArtifact.setAutomation(automation);
				}
				
				if (dto.getTask() != null) {
					ResolveActionTask task = (ResolveActionTask) HibernateUtil.getCurrentSession()
							.createCriteria(ResolveActionTask.class)
							.add(Restrictions.eq(SYS_ID, dto.getTask().getId()).ignoreCase()).uniqueResult();
					if (task == null) {
						throw new IllegalArgumentException(ERROR_UNKNOWN_ACTION_TASK);
					}
					oldArtifact.setTask(task);
				}
			Integer checksum = 0;
            if (oldArtifact.getAutomation() != null) {
                checksum = Objects.hash(oldArtifact.getUName(), oldArtifact.getAutomation().getChecksum());
            } else {
                checksum = Objects.hash(oldArtifact.getUName(), oldArtifact.getTask().getChecksum());
            }
            oldArtifact.setChecksum(checksum);
				ArtifactConfiguration result = HibernateUtil.getDAOFactory().getArtifactConfigurationDAO().insertOrUpdate(oldArtifact);
			ArtifactTypeUtil.updateChecksum(artifactType);
				return convertToArtifactVO(result);
				
			});
			
			return artifactVO;
		} catch (Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}
	}

	public static boolean deleteArtifact(String name) {
	    
		try {
			
			if (StringUtils.isEmpty(name)) {
				throw new IllegalArgumentException(ERROR_INCORRECT_NAME);
			}

			HibernateProxy.execute(() -> {
				ArtifactType type = null;
				ArtifactConfiguration oldArtifact = (ArtifactConfiguration) HibernateUtil.getCurrentSession().createCriteria(ArtifactConfiguration.class)
						.add(Restrictions.eq(U_NAME, name.toLowerCase()).ignoreCase()).uniqueResult();
	
				if (oldArtifact == null) {
					throw new IllegalArgumentException(ERROR_NOT_FOUND);
				}
			
				type = oldArtifact.getArtifactType();
				HibernateUtil.getDAOFactory().getArtifactConfigurationDAO().delete(oldArtifact);
				
				ArtifactTypeUtil.updateChecksum(type);
			});
			
			return true;
		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}
		
		return false;
	}

	private static ArtifactConfigurationVO convertToArtifactVO(ArtifactConfiguration artifact) {
		ArtifactConfigurationVO vo = new ArtifactConfigurationVO();
		vo.setUName(artifact.getUName());
		vo.setParam(artifact.getParam());
		vo.setChecksum(artifact.getChecksum());
		if (artifact.getArtifactType() != null) {
			vo.setArtifactType(artifact.getArtifactType().doGetVO());
		}
		
		if (artifact.getAutomation() != null) {
			vo.setAutomation(artifact.getAutomation().doGetVO());
		}
		
		if (artifact.getTask() != null) {
			vo.setTask(artifact.getTask().doGetVO());
		}

		return vo;
	}
	
	public static List<ArtifactConfiguration> getConfigurationsByTypeId(String typeId, String username) {
	    List<ArtifactConfiguration> configList = new ArrayList<>();
	    
	    try {
            HibernateUtil.action(util -> {
                ArtifactType type = HibernateUtil.getDAOFactory().getArtifactTypeDAO().findById(typeId);
                
                ArtifactConfiguration config = new ArtifactConfiguration();
                config.setArtifactType(type);
                configList.addAll(HibernateUtil.getDAOFactory().getArtifactConfigurationDAO().find(config));
            }, username);
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
	    return configList;
	}
	
	public static List<ArtifactConfiguration> getConfigurationsById(String configId, String username) {
        List<ArtifactConfiguration> configList = new ArrayList<>();
        
        try {
            HibernateUtil.action(util -> {
                configList.add(HibernateUtil.getDAOFactory().getArtifactConfigurationDAO().findById(configId));
            }, username);
        }
        catch (Throwable e) {
            Log.log.error(e.getMessage(), e);
        }
        return configList;
    }
	
	public static Map<String, Object> getActionComponents(String configId, String username) {
	    Map<String, Object> result = new HashMap<>();
	    List<ArtifactConfiguration> actionList = getConfigurationsById(configId, username);
	    if (CollectionUtils.isNotEmpty(actionList)) {
	        List<String> runbooks = actionList.stream()
	                                          .filter(action -> action.getAutomation() != null) // filter automation actions
	                                          .map(action -> String.format("%s:%s", action.getAutomation().getUFullname(), action.getAutomation().getSys_id())) // map the action to the needed format
	                                          .collect(Collectors.toList()); // collect the mapped actions.
	        if (CollectionUtils.isNotEmpty(runbooks)) {
	            result.put(ImpexEnum.RUNBOOK.getValue(), runbooks);
	        }
	        
	        List<String> tasks = actionList.stream()
	                                       .filter(action -> action.getTask() != null) // filter task actions
	                                       .map(action -> String.format("%s:%s", action.getTask().getUFullName(), action.getTask().getSys_id()))
	                                       .collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(tasks)) {
                result.put(ImpexEnum.TASK.getValue(), tasks);
            }
	    }
	    return result;
	}
	
	// API to be called from Update Script ONLY
    public static void updateAllArtifactConfigsForChecksum() {
        try {
            UpdateChecksumUtil.updateComponentChecksum("ArtifactConfiguration", ArtifactConfigurationUtil.class);
        }
        catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static Integer calculateChecksum(ArtifactConfiguration config) {
        Integer checksum = 0;
        ArtifactConfigurationVO configVO = getArtifactByName(config.getUName());
        if (configVO != null) {
            if (configVO.getAutomation() != null) {
                checksum = Objects.hash(configVO.getUName(), configVO.getAutomation().getChecksum());
            } else {
                checksum = Objects.hash(configVO.getUName(), configVO.getTask().getChecksum());
            }
        }
        return checksum;
    }
    
    public static Integer getArtifactConfigChecksum(String name) {
        Integer checksum = null;
        ArtifactConfigurationVO configVO = getArtifactByName(name);
        if (configVO != null) {
            checksum = configVO.getChecksum();
        }
        return checksum;
    }
}
