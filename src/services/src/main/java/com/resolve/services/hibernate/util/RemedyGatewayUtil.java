/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.dao.RemedyFormDAO;
import com.resolve.persistence.model.RemedyxForm;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class RemedyGatewayUtil extends GatewayUtil
{
    private static volatile RemedyGatewayUtil instance = null;
    public static RemedyGatewayUtil getInstance()
    {
        if(instance == null)
        {
            instance = new RemedyGatewayUtil();
        }
        return instance;
    }

    @SuppressWarnings("rawtypes")
    public void setRemedyForms(Map forms)
    {
        try
        {
			HibernateProxy.execute(() -> {
				RemedyFormDAO remedyFormDao = HibernateUtil.getDAOFactory().getRemedyFormDAO();

				String queue = deleteByQueue(RemedyxForm.class.getSimpleName(), forms);

				for (Object form : forms.entrySet()) {
					Map.Entry entry = (Map.Entry) form;
					String formString = (String) entry.getValue();
					Map formMap = StringUtils.stringToMap(formString);

					String formName = (String) formMap.get("NAME");
					String fieldList = (String) formMap.get("FIELD_LIST");

					// init record entry with values
					RemedyxForm row = new RemedyxForm();

					row.setUQueue(queue);
					row.setUName(formName);
					row.setUFieldList(fieldList);
					remedyFormDao.save(row);
				}

			});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    } // setForms

    @SuppressWarnings("unchecked")
    public static List<Map<String, String>> getRemedyForms(String queueName)
    {
        List<Map<String, String>> forms = new ArrayList<Map<String, String>>();        
        try
        {
            HibernateProxy.execute(() -> {
            	List<RemedyxForm> formList = HibernateUtil.createQuery("select o from RemedyxForm o where UQueue='" + queueName + "'").list();

                for (RemedyxForm form : formList)
                {
                    Map<String, String> formMap = new HashMap<String, String>();
                    formMap.put("NAME", form.getUName());
                    formMap.put("FIELD_LIST", form.getUFieldList());

                    forms.add(formMap);
                }

            });
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        return forms;
    } // getPools
}
