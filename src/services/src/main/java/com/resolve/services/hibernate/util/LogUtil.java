/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.persistence.model.AlertLog;
import com.resolve.persistence.model.AuditLog;
import com.resolve.persistence.model.GatewayLog;
import com.resolve.persistence.model.ResolveImpexLog;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.AlertLogVO;
import com.resolve.services.hibernate.vo.AuditLogVO;
import com.resolve.services.hibernate.vo.GatewayLogVO;
import com.resolve.services.hibernate.vo.ResolveImpexLogVO;
import com.resolve.services.hibernate.vo.ResolveImpexModuleVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class LogUtil
{

    public static ResolveImpexLogVO getResolveImpexLog(String sysId)
    {
        ResolveImpexLogVO result = null;
        ResolveImpexLog model = null;

        if (StringUtils.isNotBlank(sysId))
        {
            try
            {
                model = (ResolveImpexLog) HibernateProxy.execute(() -> {
                	return HibernateUtil.getDAOFactory().getResolveImpexLogDAO().findById(sysId);
                });
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }

            if (model != null)
            {
                result = model.doGetVO();
            }
        }

        return result;
    }
    
    public static List<ResolveImpexLogVO> getResolveImpexLogs(QueryDTO query)
    {
        List<ResolveImpexLogVO> result = new ArrayList<ResolveImpexLogVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        query.setSelectColumns("sys_id,UType,ULogSummary,sysCreatedOn,sysUpdatedOn,sysUpdatedBy,sysCreatedBy");
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        ResolveImpexLog instance = new ResolveImpexLog();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        result.add(instance.doGetVO());
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        ResolveImpexLog instance = (ResolveImpexLog) o;
                        result.add(instance.doGetVO());
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
    public static void saveImpexLog(ResolveImpexModuleVO moduleVO, StringBuilder log, String type, String userName)
    {
        ResolveImpexLogVO impexLogVO = new ResolveImpexLogVO();
        impexLogVO.setUType(type);
        impexLogVO.setResolveImpexModule(moduleVO);
        impexLogVO.setULogHistory(log.toString());
        
        String summary = null;
        if (log.length() >= 400)
        {
            summary = log.substring(0, 399);
        }
        else
        {
            summary = log.toString();
        }
        impexLogVO.setULogSummary(summary);
        
        try
        {
          HibernateProxy.setCurrentUser(userName);
            HibernateProxy.execute(()->            
            	HibernateUtil.getDAOFactory().getResolveImpexLogDAO().insert(new ResolveImpexLog(impexLogVO)));;
         
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
    }

    public static void deleteResolveImpexLogBySysId(List<String> sysIds, String username)
    {
        if (sysIds != null)
        {
            for (String sysId : sysIds)
            {
                try
                {
                  HibernateProxy.setCurrentUser(username);
                	HibernateProxy.execute(() -> {

	                    ResolveImpexLog model = HibernateUtil.getDAOFactory().getResolveImpexLogDAO().findById(sysId);
	                    if(model != null)
	                    {
	                        HibernateUtil.getDAOFactory().getResolveImpexLogDAO().delete(model);
	                    }

                	});
                }
                catch (Throwable t)
                {
                    Log.log.error(t.getMessage(), t);
                                      HibernateUtil.rethrowNestedTransaction(t);
                }
            }
        }
    }

    public static AuditLogVO getAuditLog(String sysId)
    {
        AuditLogVO result = null;
        AuditLog model = null;

        if (StringUtils.isNotBlank(sysId))
        {
            try
            {
                model = (AuditLog) HibernateProxy.execute(() -> {
                	return HibernateUtil.getDAOFactory().getAuditLogDAO().findById(sysId);
                });

            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }

            if (model != null)
            {
                result = model.doGetVO();
            }
        }

        return result;
    }
    
    public static List<AuditLogVO> getAuditLogs(QueryDTO query)
    {
        List<AuditLogVO> result = new ArrayList<AuditLogVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        AuditLog instance = new AuditLog();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        result.add(instance.doGetVO());
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        AuditLog instance = (AuditLog) o;
                        result.add(instance.doGetVO());
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
    public static void deleteAuditLogBySysId(List<String> sysIds, String username)
    {
        if (sysIds != null)
        {
            for (String sysId : sysIds)
            {
                try
                {
                  HibernateProxy.setCurrentUser(username);
                    HibernateProxy.execute(() -> {

	                    AuditLog model = HibernateUtil.getDAOFactory().getAuditLogDAO().findById(sysId);
	                    if(model != null)
	                    {
	                        HibernateUtil.getDAOFactory().getAuditLogDAO().delete(model);
	                    }

                    });
                }
                catch (Throwable t)
                {
                    Log.log.error(t.getMessage(), t);
                                      HibernateUtil.rethrowNestedTransaction(t);
                }
            }
        }
    }
    
    public static List<AlertLogVO> getAlertLogs(QueryDTO query)
    {
        List<AlertLogVO> result = new ArrayList<AlertLogVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        AlertLog instance = new AlertLog();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        result.add(instance.doGetVO());
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        AlertLog instance = (AlertLog) o;
                        result.add(instance.doGetVO());
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    }

    public static AlertLogVO getAlertLog(String sysId)
    {
        AlertLogVO result = null;
        AlertLog model = null;

        if (StringUtils.isNotBlank(sysId))
        {
            try
            {

                model = (AlertLog) HibernateProxy.execute(() -> {
                	return HibernateUtil.getDAOFactory().getAlertLogDAO().findById(sysId);
                });
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }

            if (model != null)
            {
                result = model.doGetVO();
            }
        }

        return result;
    }
    
    public static void deleteAlertLogBySysId(List<String> sysIds, String username)
    {
        if (sysIds != null)
        {
            for (String sysId : sysIds)
            {
                try
                {
                  HibernateProxy.setCurrentUser(username);
                	HibernateProxy.execute(() -> {

	                    AlertLog model = HibernateUtil.getDAOFactory().getAlertLogDAO().findById(sysId);
	                    if(model != null)
	                    {
	                        HibernateUtil.getDAOFactory().getAlertLogDAO().delete(model);
	                    }

                	});
                }
                catch (Throwable t)
                {
                    Log.log.error(t.getMessage(), t);
                                      HibernateUtil.rethrowNestedTransaction(t);
                }
            }
        }
    }

    public static GatewayLogVO getGatewayLog(String sysId)
    {
        GatewayLogVO result = null;
        GatewayLog model = null;

        if (StringUtils.isNotBlank(sysId))
        {
            try
            {

                model = (GatewayLog) HibernateProxy.execute(() -> {
                	return HibernateUtil.getDAOFactory().getGatewayLogDAO().findById(sysId);
                });
            }
            catch (Throwable t)
            {
                Log.log.error(t.getMessage(), t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }

            if (model != null)
            {
                result = model.doGetVO();
            }
        }

        return result;
    }
    
    public static List<GatewayLogVO> getGatewayLogs(QueryDTO query)
    {
        List<GatewayLogVO> result = new ArrayList<GatewayLogVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        GatewayLog instance = new GatewayLog();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        result.add(instance.doGetVO());
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        GatewayLog instance = (GatewayLog) o;
                        result.add(instance.doGetVO());
                    }
                }
            }    
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
    public static void deleteGatewayLogBySysId(List<String> sysIds, String username)
    {
        if (sysIds != null)
        {
            for (String sysId : sysIds)
            {
                try
                {
                  HibernateProxy.setCurrentUser(username);
                	HibernateProxy.execute(() -> {
	
	                    GatewayLog model = HibernateUtil.getDAOFactory().getGatewayLogDAO().findById(sysId);
	                    if(model != null)
	                    {
	                        HibernateUtil.getDAOFactory().getGatewayLogDAO().delete(model);
	                    }

                	});
                }
                catch (Throwable t)
                {
                    Log.log.error(t.getMessage(), t);
                                      HibernateUtil.rethrowNestedTransaction(t);
                }
            }
        }
    }
    

}
