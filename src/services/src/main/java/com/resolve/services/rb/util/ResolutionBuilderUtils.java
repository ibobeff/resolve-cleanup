/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.rb.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.jms.IllegalStateException;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.dom4j.Document;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.hibernate.query.Query;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.resolve.persistence.model.BaseModel;
import com.resolve.persistence.model.ModelToVO;
import com.resolve.persistence.model.RBCondition;
import com.resolve.persistence.model.RBConnection;
import com.resolve.persistence.model.RBConnectionParam;
import com.resolve.persistence.model.RBCriterion;
import com.resolve.persistence.model.RBGeneral;
import com.resolve.persistence.model.RBIf;
import com.resolve.persistence.model.RBLoop;
import com.resolve.persistence.model.RBTask;
import com.resolve.persistence.model.RBTaskCondition;
import com.resolve.persistence.model.RBTaskHeader;
import com.resolve.persistence.model.RBTaskSeverity;
import com.resolve.persistence.model.RBTaskVariable;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWiki;
import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.RBConditionVO;
import com.resolve.services.hibernate.vo.RBConnectionParamVO;
import com.resolve.services.hibernate.vo.RBConnectionVO;
import com.resolve.services.hibernate.vo.RBCriterionVO;
import com.resolve.services.hibernate.vo.RBGeneralVO;
import com.resolve.services.hibernate.vo.RBIfVO;
import com.resolve.services.hibernate.vo.RBLoopVO;
import com.resolve.services.hibernate.vo.RBTaskConditionVO;
import com.resolve.services.hibernate.vo.RBTaskHeaderVO;
import com.resolve.services.hibernate.vo.RBTaskSeverityVO;
import com.resolve.services.hibernate.vo.RBTaskVO;
import com.resolve.services.hibernate.vo.RBTaskVariableVO;
import com.resolve.services.hibernate.vo.ResolveActionInvocOptionsVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveAssessVO;
import com.resolve.services.hibernate.vo.ResolveParserVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.wiki.DocUtils;
import com.resolve.services.hibernate.wiki.SaveHelper;
import com.resolve.services.hibernate.wiki.WikiDocLock;
import com.resolve.services.interfaces.VO;
import com.resolve.services.task.util.ParserCodeGenerator;
import com.resolve.services.util.PlaybookUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.form.RsFormDTO;
import com.resolve.util.BeanUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

import net.sf.json.JSONObject;

public class ResolutionBuilderUtils
{
    private static Map<String, String> TEMPLATES = new HashMap<String, String>();
    static
    {
        TEMPLATES.put("ssh", "sshTemplate#resolve");
        TEMPLATES.put("telnet", "telnetTemplate#resolve");
        TEMPLATES.put("local", "localTemplate#resolve");
        TEMPLATES.put("assess", "assessTemplate#resolve");
        TEMPLATES.put("http", "httpTemplate#resolve");
    }

    // Private methods

    @SuppressWarnings("unchecked")
    private static <T> Object save(String modelName, BaseModel<T> model, String username) throws Exception
    {
        Object result = null;
        
        PlaybookUtils.v2LicenseViolationsAndEnvCheck(modelName);
        
        try
        {
            String fullModelName = "com.resolve.persistence.model." + modelName;
            if (model == null)
            {
                model = BeanUtil.getInstance(fullModelName);
            }

            String id = model.getSys_id();

            if (StringUtils.isNotBlank(id))
            {
                // we should find the model from DB                
              HibernateProxy.setCurrentUser(username);
                model = (BaseModel<T>) HibernateProxy.execute(() -> HibernateUtil.findById(fullModelName, id));
            }
            else
            {
                /*model.setSys_id(null);
                model.setSysOrg(null);
                model.setSysCreatedBy(username);
                model.setSysUpdatedBy(username);
                Date now = new Date(DateUtils.GetUTCDateLong());
                //model.setSysCreatedOn(now);
                //model.setSysUpdatedOn(now);
                model.setSysModCount(0);*/
            }
            BaseModel<T> modelFinal = model;
            BaseModel<T> savedModel = (BaseModel<T>) HibernateProxy.execute(() -> HibernateUtil.saveObj(modelName, modelFinal));
           
            if (savedModel != null)
            {
                result = savedModel.doGetVO();
            }
        }
        catch (Throwable e)
        {
            result = null;
            Log.log.error(e.getMessage(), e);
            throw new Exception(e.getMessage());
        }
        return result;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static List findByQuery(QueryDTO query) throws Exception
    {
        List result = new ArrayList();

        if (StringUtils.isBlank(query.getModelName()))
        {
            throw new Exception("Missing modelName");
        }
        else
        {
            try
            {
				HibernateProxy.execute(() -> {
					Query hqlQuery = HibernateUtil.createQuery(query);
					List<ModelToVO<VO>> rows = hqlQuery.list();
					for (ModelToVO<VO> row : rows) {
						result.add(row.doGetVO());
					}
				});
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                throw new Exception(e.getMessage());
            }
        }
        return result;
    }

    static String getWikiName(String namespace, String name) throws Exception
    {
        StringBuilder result = new StringBuilder();
        if (StringUtils.isBlank(namespace) || StringUtils.isBlank(name))
        {
            throw new Exception("Namespace and name must be provided.");
        }
        else
        {
            result = result.append(namespace).append(".").append(name);
        }
        return result.toString();
    }

    // Public methods
    @SuppressWarnings("unchecked")
    public static List<RBGeneralVO> listGeneral(QueryDTO queryDTO, String username)
    {
        List<RBGeneralVO> result = new ArrayList<RBGeneralVO>();
        try
        {
            queryDTO.setModelName("RBGeneral");
            result = (List<RBGeneralVO>) findByQuery(queryDTO);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    }

    public static RBGeneralVO getGeneral(String sysId, String username)
    {
        RBGeneralVO result = null;
        if (StringUtils.isNotBlank(sysId))
        {
            RBGeneral entity = getGeneralEntity(sysId, username);
            if (entity != null)
            {
                result = entity.doGetVO();
                WikiDocumentVO wiki = ServiceWiki.getWikiDoc(entity.getWikiId(), null, username);
                if(wiki != null)
                {
                    result.setWikiParameters(wiki.getUWikiParameters());
                }
            }
        }
        return result;
    }// getGeneral

    public static RBGeneral getGeneralEntity(String sysId, String username)
    {
        RBGeneral result = null;
        if (StringUtils.isNotBlank(sysId))
        {
            try
            {                
                result = (RBGeneral) HibernateProxy.execute(()->{
                	return HibernateUtil.getDAOFactory().getRBGeneralDAO().findById(sysId);
                });
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        return result;
    }// getGeneralEntity

    public static RBGeneralVO createGeneral(RBGeneralVO vo, boolean createWiki, String username) throws Exception
    {
        RBGeneralVO result = null;

        if (vo != null)
        {
            try
            {
                String name = vo.getName();
                String namespace = vo.getNamespace();
                if (StringUtils.isBlank(name) || StringUtils.isBlank(namespace))
                {
                    throw new Exception("Name and Namespace are mandatory");
                }
                else
                {
                    String wikiName = getWikiName(vo.getNamespace(), vo.getName());
                    
                    String wikiSysId = vo.getWikiId();
                    if(StringUtils.isBlank(wikiSysId))
                    {
                        // find wiki/runbook with the same name
                        wikiSysId = ServiceWiki.getWikidocSysId(wikiName);
                        if (StringUtils.isNotBlank(wikiSysId) && createWiki)
                        {
                            throw new Exception("Runbook already exists for " + wikiName);
                        }
                    }
                    else
                    {
                        createWiki = false;
                    }
                    
                    //First create wikidoc since wikidoc creation also now creates rb_general

                    WikiDocumentVO wikiVO = null;
                    
                    if (createWiki)
                    {
                        WikiDocumentVO entity = new WikiDocumentVO();
                        entity.setUName(vo.getName());
                        entity.setUNamespace(vo.getNamespace());
                        entity.setUFullname(wikiName);
                        entity.setUSummary(vo.getSummary());
                        entity.setUHasResolutionBuilder(Boolean.TRUE);
                        entity.setUDisplayMode("wiki");
                        entity.setUIsDefaultRole(true);
                        wikiVO = ServiceWiki.saveAndCommitWikiDoc(entity, null, username, true);
                        wikiSysId = wikiVO.getSys_id();
                        Log.log.debug("Wiki created with id : " + wikiVO.getSys_id());
                    }
                    else
                    {
                        wikiVO = ServiceWiki.getWikiDoc(wikiSysId, null, username);
                    }
                    
                    if (wikiVO == null || StringUtils.isBlank(wikiVO.getUResolutionBuilderId()))
                    {
                        throw new Exception("Failed to get created wiki document or id of RB General record.");
                    }
                    
                    final WikiDocumentVO finalWikiVO = wikiVO;
                    //now get the general from wikidoc 
                  HibernateProxy.setCurrentUser(username);
                    RBGeneral generalEntity = (RBGeneral) HibernateProxy.execute(() -> {
                    	return HibernateUtil.getDAOFactory().getRBGeneralDAO().findById(finalWikiVO.getUResolutionBuilderId());
                    });
                    result = generalEntity.doGetVO();
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw e;
            }
        }
        return result;
    }

    public static RBGeneralVO updateGeneral(RBGeneralVO vo, String username, boolean isSave) throws Exception
    {
        RBGeneralVO result = null;

        if (vo != null)
        {
            try
            {
                String id = vo.getSys_id();
                if (StringUtils.isBlank(id))
                {
                    throw new Exception("ID is blank, nothing to do.");
                }

                RBGeneral general = getGeneralEntity(id, username);

                if (general == null)
                {
                    throw new Exception("Resolution Builder not found with the id: " + id);
                }
                
                String msgObj = String.format("RBGeneral %s.%s", general.getNamespace(), general.getName());
                
                PlaybookUtils.v2LicenseViolationsAndEnvCheck(msgObj);
                
                WikiDocumentVO entity = ServiceWiki.getWikiDoc(general.getWikiId(), null, username);

                // It should never happen now since wikidoc always gets created
                if (entity == null)
                {
                    // we need to create the wiki
                    entity = new WikiDocumentVO();
                }
                
                //If there is soft lock on the doc, request soft lock
                
                WikiDocLock lock = DocUtils.getWikiDocLockNonBlocking(entity.getUFullname());
                
                if (lock != null && !lock.getUsername().equals(username))
                {
                    String lockedByUSerName = lock.getUsername();
                    /*
                    try
                    {
                        DocUtils.requestSoftLock(username, entity.getUFullname());
                    }
                    catch(Exception e)
                    {
                        Log.log.debug("" + username + " failed to get soft lock on " + entity.getUFullname(), e);
                        throw new Exception("Save Failed : Wiki document is locked by " + lockedByUSerName + ", " + username + " does not have access rights to break the lock");
                    }*/
                    throw new Exception("Save Failed : Wiki document is locked by " + lockedByUSerName);
                }
                
                general.setName(vo.getName());
                general.setNamespace(vo.getNamespace());
                general.setSummary(vo.getSummary());
                general.setNoOfColumn(vo.getNoOfColumn());
                general.setNewWorksheet(vo.getNewWorksheet());
                general.setGenerated(vo.getGenerated());

                HibernateProxy.execute(() -> {
                	HibernateUtil.saveObj("RBGeneral", general);
                });
                

                result = general.doGetVO();
                String wikiName = getWikiName(general.getNamespace(), general.getName());
                
                if (/*(StringUtils.isNotBlank(entity.getUSummary()) && !entity.getUSummary().equals(vo.getSummary())) 
                                || (StringUtils.isNotBlank(vo.getSummary()) && !vo.getSummary().equals(entity.getUSummary())) 
                                ||*/ (StringUtils.isNotBlank(entity.getUWikiParameters()) && !entity.getUWikiParameters().equals(vo.getWikiParameters())) 
                                || (StringUtils.isNotBlank(vo.getWikiParameters()) && !vo.getWikiParameters().equals(entity.getUWikiParameters())))
                {
                    entity.setUName(vo.getName());
                    entity.setUNamespace(vo.getNamespace());
                    entity.setUFullname(wikiName);
                    //entity.setUSummary(vo.getSummary());
                    entity.setUResolutionBuilderId(result.getSys_id());
                    entity.setUHasResolutionBuilder(Boolean.TRUE);
                    entity.setUWikiParameters(vo.getWikiParameters());

                    WikiDocumentVO wikiVO = ServiceWiki.saveAndCommitWikiDoc(entity, null, username, isSave);
                    Log.log.debug("Wiki saved with id : " + wikiVO.getSys_id());
                    result.setWikiParameters(entity.getUWikiParameters());
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw e;
            }
        }
        return result;
    }

    public static boolean deleteGeneral(String sysId, boolean deleteWiki, String username) throws Exception
    {
        boolean result = true;
        if (StringUtils.isNotBlank(sysId))
        {
            try
            {
                RBGeneralVO general = getGeneral(sysId, username);
                if (general == null)
                {
                    Log.log.debug("Invalid Resolution Builder id to delete: " + sysId);
                }
                else
                {
                    // delete connections
                    List<RBConnectionVO> connections = findConnectionsByParentId(sysId, ABConstants.BUILDER, username);
                    for (RBConnectionVO connection : connections)
                    {
                        deleteConnection(connection.getSys_id(), username);
                    }

                    // delete loops
                    List<RBLoopVO> loops = findLoopsByParentId(sysId, username);
                    for (RBLoopVO loopVO : loops)
                    {
                        deleteLoop(loopVO.getSys_id(), username);
                    }

                    // delete wiki
                    if (deleteWiki)
                    {
                        String wikiName = getWikiName(general.getNamespace(), general.getName());

                        WikiDocumentVO wiki = WikiUtils.getWikiDoc(null, wikiName, username);
                        if (wiki != null)
                        {
                            ServiceWiki.deleteWikiDocument(null, wikiName, username);
                        }
                    }

                    RBGeneral generalEntity = getGeneralEntity(sysId, username);
                    if (generalEntity != null)
                    {
                        HibernateProxy.execute(() -> {
                        	HibernateUtil.deleteObj("RBGeneral", generalEntity);
                        });
                    }

                    // delete custom form, due to some bizzare exception issue
                    // with custom form
                    // it is deleted at the end.
                    String formName = CustomForm.synthesizeFormName(general);
                    try
                    {
                        ServiceHibernate.deleteMetaFormByName(formName);
                    }
                    catch (Throwable e)
                    {
                        // ignore, may not have found
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw e;
            }
        }
        return result;
    }
    
    public static JSONObject pollABGeneration(String username) throws Exception {
        if(!automationGenerationProcesses.containsKey(username))
            return null;
//            throw new Exception("Cannot find generation process tied to " + username);
        AutomationGenerationProcess proc = automationGenerationProcesses.get(username);
        if(proc.getStatus().getBoolean("success")==false||proc.getStatus().getString("stage").equals(ABProcessConst.DONE)) 
            automationGenerationProcesses.remove(username);
        return proc.getStatus();
    }
    
    private static ConcurrentHashMap<String,AutomationGenerationProcess> automationGenerationProcesses = new ConcurrentHashMap<String,AutomationGenerationProcess>();
    public static ABProcessConst generate(String id, String username) throws Exception
    {
        AutomationGenerationProcess proc = new AutomationGenerationProcess(id,username);
        if(automationGenerationProcesses.contains(username))
            return ABProcessConst.INPROGRESS;
        automationGenerationProcesses.put(username, proc);
        proc.start();
        return ABProcessConst.STARTED;
    }
    
    private static String escapeCommand(String command)
    {
        String escapedCommand = command;
        
        String[] commandParts = null;
        
        if (StringUtils.isNotBlank(command))
        {
            commandParts = command.split(" ");
        }
        
        if (commandParts != null && commandParts.length > 0)
        {
            StringBuilder sb = new StringBuilder();
            
            for (int i = 0; i < commandParts.length; i++)
            {
                if (i > 0)
                {
                    sb.append(" ");
                }
                
                String commandPart = commandParts[i];
                
                String[] commandPartLines = commandPart.split("\\n");
                
                for (int j = 0; j < commandPartLines.length; j++)
                {
                    if (j > 0)
                    {
                        sb.append("\n");
                    }
                    
                    String commandPartLine = commandPartLines[j];
                    
                    if (commandPartLine.startsWith("$PARAM{") || commandPartLine.startsWith("$INPUT{") || 
                        commandPartLine.startsWith("$FLOW{") || commandPartLine.startsWith("$WSDATA{"))
                    {
                        sb.append(StringUtils.substringBefore(commandPartLine, "{"));
                        
                        if (!commandPartLine.startsWith("$WSDATA{"))
                        {
                            sb.append("S");
                        }
                        
                        sb.append(".");
                        
                        sb.append(StringUtils.substringBetween(commandPartLine, "{", "}"));
                    }
                    else
                    {
                        if (!commandPartLine.startsWith("$PROPERTY{"))
                        {
                            commandPartLine.replace("\\", "\\\\").replace("$", "\\$");
                            sb.append(commandPartLine);
                        }
                    }
                }
            }
            
            escapedCommand = sb.toString();
        }
        
        return escapedCommand;
    }
    
    static void generateTask(RBGeneralVO general, RBTaskVO taskVO, String username) throws Exception
    {
        String command = taskVO.getCommand();
        String commandUrl = taskVO.getCommandUrl();
        
        if (StringUtils.isNotBlank(taskVO.getName()))
        {
            String templateName = null;
            
            if(StringUtils.isNotBlank(command) || StringUtils.isNotBlank(commandUrl))
            {
                templateName = TEMPLATES.get(taskVO.getConnectionType().toLowerCase());
            }
            else
            {
                templateName = TEMPLATES.get("assess");
            }
            
            if (templateName == null)
            {
                throw new IllegalStateException("Unsupported connection or Automation Builder template task not found.");
            }
            ResolveActionTaskVO actionTaskVO = ServiceHibernate.getResolveActionTask(null, templateName, username);

            if (actionTaskVO == null)
            {
                throw new Exception("Missing required " + templateName + " action task template.");
            }
            
            String taskName = taskVO.getName();
            // we use full wiki name as the module
            String taskModuleName = getWikiName(general.getNamespace(), general.getName());
            String taskFullName = taskName + "#" + taskModuleName;
            try
            {
                List<String> taskIds = new LinkedList<String>();
                taskIds.add(actionTaskVO.getSys_id());

                ActionTaskUtil.copyActionTasks(taskIds, taskName, taskModuleName, true, username);
                
                Log.log.debug("Task created/overwritten: " + taskFullName);
                ResolveActionTaskVO newActionTask = ActionTaskUtil.getActionTaskWithReferences(null, taskFullName, username);
                

                // assign the new action task to the user creating it
                newActionTask.setAssignedTo(ServiceHibernate.getUser(username));
                
                // set the assessor script from RBTaskVO
                newActionTask.getResolveActionInvoc().getAssess().setUScript(taskVO.getAssessorScript());
                
                newActionTask.getResolveActionInvoc().setUTimeout(taskVO.getTimeout());
                
                //set default Log Raw Result flag based on the actiontask.log.raw.result value in System Property
                newActionTask.setULogresult(ActionTaskUtil.getLogRawResultFlag());
                
                // local is special, it needs the command to be set externally
                if ("local".equalsIgnoreCase(taskVO.getConnectionType()))
                {
                    newActionTask.getResolveActionInvoc().setUCommand(command);
                }
                
                if(StringUtils.isNotBlank(taskVO.getQueueName()))
                {
                    String queueValue = null;
                    if("CONSTANT".equalsIgnoreCase(taskVO.getQueueNameSource()))
                    {
                        queueValue = taskVO.getQueueName();
                    }
                    else
                    {
                        queueValue = "$" + ABConstants.SOURCE.get(taskVO.getQueueNameSource()) + "{" + taskVO.getQueueName() + "}";
                    }
                    ResolveActionInvocOptionsVO queueOption = new ResolveActionInvocOptionsVO();
                    queueOption.setUName("QUEUE_NAME");
                    queueOption.setUValue(queueValue);
                    queueOption.setUDescription("Name of the JMS Queue to send the ActionTask request");
                    newActionTask.getResolveActionInvoc().getResolveActionInvocOptions().add(queueOption);
                }
                
                if(StringUtils.isNotBlank(taskVO.getInputfile()))
                {
                    Collection<ResolveActionInvocOptionsVO> inokeOpts = newActionTask.getResolveActionInvoc().getResolveActionInvocOptions();
                    
                    for (ResolveActionInvocOptionsVO invokeOpt : inokeOpts)
                    {
                        if (invokeOpt.getUName().equalsIgnoreCase("INPUTFILE"))
                        {
                            invokeOpt.setUValue(taskVO.getInputfile());
                            break;
                        }
                    }
                }
                
                //set the parser code
                ResolveParserVO parser = newActionTask.getResolveActionInvoc().getParser();
                parser.setUScript(taskVO.getParserScript());
                newActionTask.getResolveActionInvoc().setParser(parser);
                
                //set the summary and detail
                ResolveAssessVO assess = newActionTask.getResolveActionInvoc().getAssess();
                assess.setUScript(taskVO.getAssessorScript());
                newActionTask.getResolveActionInvoc().setAssess(assess);

                //we decided that the task name will be used as summary
                newActionTask.setUSummary(taskName);
                newActionTask.setUDescription(taskVO.getDescription());
                // menu path will be like /automationbuilder/namespace/wikiname
                newActionTask.setUMenuPath("/automationbuilder/" + taskModuleName.replace(".", "/"));
                
                if(StringUtils.isNotBlank(command) || StringUtils.isNotBlank(commandUrl))
                {
                	// the command and commandUrl will be hardcoded in the content script instead if getting read from runbook input params.
	                Collection<ResolveActionInvocOptionsVO> invocOptions = newActionTask.getResolveActionInvoc().getResolveActionInvocOptions();
	                if (invocOptions != null)
	                {
	                	for (ResolveActionInvocOptionsVO invocOption : invocOptions)
	                	{
	                		if (invocOption.getUName().equals("INPUTFILE"))
	                		{
	                			String script = null;
	                			String value = invocOption.getUValue();
	                			
	                			if (StringUtils.isNotBlank(command))
	                			{
	                				//String escapedCommand = command.replace("\\", "\\\\").replace("$", "\\$");
	                				String escapedCommand = escapeCommand(command);
	                				if (StringUtils.isNotBlank(value))
	                				{
	                					script = value.replace("INPUTS[\"command\"]", "\"\"\"" + escapedCommand + "\"\"\"");
	                				}
	                			}
	                			else
	                			{
	                				if (StringUtils.isNotBlank(value))
	                				{
	                					script = value.replace("INPUTS[\"commandUrl\"]", "\"\"\"" + commandUrl + "\"\"\"");
	                				}
	                			}
	                			invocOption.setUValue(script);
	                			break;
	                		}
	                	}
	                }
                }
                
                ActionTaskUtil.saveResolveActionTask(newActionTask, username);
            }
            catch (Exception e)
            {
            	if (StringUtils.isBlank(e.getMessage()) || 
                    !(e.getMessage().startsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_PREFIX) &&
                      (e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_SUFFIX) ||
                       e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_HA_VIOLATED_SUFFIX)))) {
                    Log.log.error("Error generating task " + taskFullName, e);
                }
            	
                throw e;
            }
        }
    }

    public static RBLoopVO getLoop(String sysId, String username)
    {
        RBLoopVO result = null;
        if (StringUtils.isNotBlank(sysId))
        {
            RBLoop entity = getLoopEntity(sysId, username);
            if (entity != null)
            {
                result = entity.doGetVO();
            }
        }

        return result;
    }

    public static RBLoop getLoopEntity(String sysId, String username)
    {
        RBLoop result = null;
        if (StringUtils.isNotBlank(sysId))
        {
            try
            {
                result = (RBLoop) HibernateProxy.execute(() -> {
                	return HibernateUtil.getDAOFactory().getRBLoopDAO().findById(sysId);
                });
                
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<RBLoopVO> findLoopsByParentId(String parentId, String username)
    {
        List<RBLoopVO> result = new ArrayList<RBLoopVO>();
        if (StringUtils.isNotBlank(parentId))
        {
            try
            {
                QueryDTO queryDTO = new QueryDTO();
                queryDTO.setModelName("RBLoop");
                queryDTO.addFilterItem(new QueryFilter("parentId", QueryFilter.EQUALS, parentId));
                result = (List<RBLoopVO>) findByQuery(queryDTO);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        return result;
    }

    public static RBLoopVO createLoop(RBLoopVO vo, String username) throws Exception
    {
        return (RBLoopVO) save("RBLoop", new RBLoop(vo), username);
    }

    public static RBLoopVO updateLoop(RBLoopVO vo, String username) throws Exception
    {
        RBLoopVO result = null;

        if (vo != null)
        {
            try
            {
                String id = vo.getSys_id();
                if (StringUtils.isBlank(id))
                {
                    throw new Exception("ID is blank, nothing to do.");
                }

                RBLoop entity = getLoopEntity(id, username);

                if (entity == null)
                {
                    throw new Exception("Loop not found with the id: " + id);
                }
                
                PlaybookUtils.v2LicenseViolationsAndEnvCheck("RBLoop");
                
                entity.setCount(vo.getCount());
                entity.setDescription(vo.getDescription());
                entity.setItemSource(vo.getItemSource());
                entity.setItemSourceName(vo.getItemSourceName());
                entity.setItemDelimiter(vo.getItemDelimiter());
                entity.setOrder(vo.getOrder());
                entity.setParentId(vo.getParentId());
                entity.setParentType(vo.getParentType());

                HibernateProxy.execute(() -> {
                	  HibernateUtil.saveObj("RBLoop", entity);
                });
              
                result = entity.doGetVO();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw e;
            }
        }
        return result;

    }

    public static boolean deleteLoop(String sysId, String username)
    {
        boolean result = true;
        if (StringUtils.isNotBlank(sysId))
        {
            try
            {
                List<RBConnectionVO> connections = findConnectionsByParentId(sysId, ABConstants.LOOP, username);
                for (RBConnectionVO connection : connections)
                {
                    deleteConnection(connection.getSys_id(), username);
                }
                RBLoop loop = getLoopEntity(sysId, username);
                if (loop != null)
                {
					HibernateProxy.execute(() -> {
						HibernateUtil.deleteObj("RBLoop", loop);
					});
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        return result;
    }

    public static RBConnectionVO getConnection(String sysId, String username)
    {
        RBConnectionVO result = null;
        if (StringUtils.isNotBlank(sysId))
        {
            RBConnection entity = getConnectionEntity(sysId, username);
            if (entity != null)
            {
                result = entity.doGetVO();
            }
        }
        return result;
    }

    public static RBConnection getConnectionEntity(String sysId, String username)
    {
        RBConnection result = null;
        if (StringUtils.isNotBlank(sysId))
        {
            try
            {
            	result = (RBConnection) HibernateProxy.execute(() -> {
            		return HibernateUtil.getDAOFactory().getRBConnectionDAO().findById(sysId);
            	});
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }

        return result;
    }

    /**
     * Finds all connection by their parents.
     * 
     * @param parentId
     * @param parentType
     *            "builder" or "loop"
     * @param username
     * @return
     */
    @SuppressWarnings("unchecked")
    public static List<RBConnectionVO> findConnectionsByParentId(String parentId, String parentType, String username)
    {
        List<RBConnectionVO> result = new ArrayList<RBConnectionVO>();
        if (StringUtils.isNotBlank(parentId))
        {
            try
            {
                QueryDTO queryDTO = new QueryDTO();
                queryDTO.setModelName("RBConnection");
                queryDTO.addFilterItem(new QueryFilter("parentId", QueryFilter.EQUALS, parentId)).addFilterItem(new QueryFilter("parentType", QueryFilter.EQUALS, parentType));
                result = (List<RBConnectionVO>) findByQuery(queryDTO);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        return result;
    }

    public static RBConnectionVO createConnection(RBConnectionVO vo, String username) throws Exception
    {
        if (StringUtils.isBlank(vo.getParentType()) || VO.STRING_DEFAULT.equalsIgnoreCase(vo.getParentType()))
        {
            vo.setParentType("builder");
        }
        return (RBConnectionVO) save("RBConnection", new RBConnection(vo), username);
    }

    public static RBConnectionVO updateConnection(RBConnectionVO vo, String username) throws Exception
    {
        RBConnectionVO result = null;

        if (vo != null)
        {
            try
            {
                String id = vo.getSys_id();
                if (StringUtils.isBlank(id))
                {
                    throw new Exception("ID is blank, nothing to do.");
                }

                RBConnection entity = getConnectionEntity(id, username);

                if (entity == null)
                {
                    throw new Exception("Connection not found with the id: " + id);
                }
                
                PlaybookUtils.v2LicenseViolationsAndEnvCheck("RBConnection");
                
                entity.setName(vo.getName());
                entity.setType(vo.getType());
                entity.setOrder(vo.getOrder());

                // this is update, so let's make sure params are properly
                // handled.
                Collection<RBConnectionParam> originalParams = entity.getParams();

                List<RBConnectionParamVO> newParams = vo.getParams();
                Map<String, RBConnectionParamVO> newParamMap = new HashMap<String, RBConnectionParamVO>();
                if (newParams != null)
                {
                    for (RBConnectionParamVO param : newParams)
                    {
                        newParamMap.put(param.getName(), param);
                    }
                }

                Collection<RBConnectionParam> updatedParams = new ArrayList<RBConnectionParam>();
                if (originalParams != null)
                {
                    for (RBConnectionParam param : originalParams)
                    {
                        if (newParamMap.containsKey(param.getName()))
                        {
                            RBConnectionParamVO newParam = newParamMap.get(param.getName());
                            param.setDataType(newParam.getDataType());
                            param.setDefaultValue(newParam.getDefaultValue());
                            param.setDisplayName(newParam.getDisplayName());
                            param.setSource(newParam.getSource());
                            param.setSourceName(newParam.getSourceName());
                            param.setConnection(entity);
                            updatedParams.add(param);
                            newParamMap.remove(param.getName());
                        }
                        else
                        {
                           HibernateProxy.execute(() -> {
                            	HibernateUtil.deleteObj("RBConnectionParam", param);
                            });
                        }
                    }
                }

                if (newParams != null)
                {
                    // these are new params
                    for (String key : newParamMap.keySet())
                    {
                        RBConnectionParam param = new RBConnectionParam(newParamMap.get(key));
                        param.setConnection(entity);
                        updatedParams.add(param);
                    }
                }

                entity.setParams(updatedParams);
                HibernateProxy.execute(() -> {
                	HibernateUtil.saveObj("RBConnection", entity);
                });
                

                result = entity.doGetVO();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw e;
            }
        }
        return result;
    }

    /**
     * Deletes connections as well as conditions and tasks that belongs to it.
     * 
     * @param sysId
     * @param username
     * @return
     * @throws Exception 
     */
    public static boolean deleteConnection(String sysId, String username) throws Exception
    {
        boolean result = true;
        if (StringUtils.isNotBlank(sysId))
        {
            List<RBIfVO> ifs = findIfsByParentId(sysId, "connection", username);
            for (RBIfVO ifVO : ifs)
            {
                deleteIf(ifVO.getSys_id(), username);
            }
            List<RBTaskVO> tasks = findTasksByParentId(sysId, "connection", null, username);
            for (RBTaskVO task : tasks)
            {
                deleteTask(task.getSys_id(), username);
            }
            RBConnection connection = getConnectionEntity(sysId, username);
            if (connection != null)
            {
				HibernateProxy.execute(() -> {
					HibernateUtil.deleteObj("RBConnection", connection);
				});
				
            }
        }
        return result;
    }

    public static RBIfVO getIf(String sysId, String username)
    {
        RBIfVO result = null;
        if (StringUtils.isNotBlank(sysId))
        {
            RBIf entity = getIfEntity(sysId, username);
            if (entity != null)
            {
                result = entity.doGetVO();
            }
        }

        return result;
    }

    public static RBIf getIfEntity(String sysId, String username)
    {
        RBIf result = null;
        if (StringUtils.isNotBlank(sysId))
        {
            try
            {
            	result = (RBIf) HibernateProxy.execute(() -> {
                	return HibernateUtil.getDAOFactory().getRBIfDAO().findById(sysId);
                });
                
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<RBIfVO> findIfsByParentId(String parentId, String parentType, String username)
    {
        List<RBIfVO> result = new ArrayList<RBIfVO>();
        if (StringUtils.isNotBlank(parentId))
        {
            try
            {
                QueryDTO queryDTO = new QueryDTO();
                queryDTO.setModelName("RBIf");
                queryDTO.addFilterItem(new QueryFilter("parentId", QueryFilter.EQUALS, parentId)).addFilterItem(new QueryFilter("parentType", QueryFilter.EQUALS, parentType));
                result = (List<RBIfVO>) findByQuery(queryDTO);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        return result;
    }

    public static RBIfVO createIf(RBIfVO vo, String username) throws Exception
    {
        return (RBIfVO) save("RBIf", new RBIf(vo), username);
    }

    public static RBIfVO updateIf(RBIfVO vo, String username) throws Exception
    {
        RBIfVO result = null;

        if (vo != null)
        {
            try
            {
                String id = vo.getSys_id();
                if (StringUtils.isBlank(id))
                {
                    throw new Exception("ID is blank, nothing to do.");
                }

                RBIf entity = getIfEntity(id, username);

                if (entity == null)
                {
                    throw new Exception("If not found with the id: " + id);
                }
                
                PlaybookUtils.v2LicenseViolationsAndEnvCheck("RBIf");
                
                entity.setDescription(vo.getDescription());
                entity.setOrder(vo.getOrder());
                entity.setParentId(vo.getParentId());
                entity.setParentType(vo.getParentType());

                HibernateProxy.execute(() -> {
                	HibernateUtil.saveObj("RBIf", entity);
                });
                
               

                result = entity.doGetVO();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw e;
            }
        }
        return result;

    }

    public static boolean deleteIf(String sysId, String username) throws Exception
    {
        boolean result = true;
        if (StringUtils.isNotBlank(sysId))
        {
			try {
				HibernateProxy.execute(() -> {
					List<RBConditionVO> conditions = findConditionsByParentId(sysId, "if", username);
					for (RBConditionVO condition : conditions) {
						deleteCondition(condition.getSys_id(), username);
					}
					HibernateUtil.delete("RBIf", sysId);
				});
        	} catch (Throwable e) {
        		result = false;
	            Log.log.error(e.getMessage(), e);
	            throw new Exception(e);
	        }
        }
        return result;
    }

    public static RBConditionVO getCondition(String sysId, String username)
    {
        RBConditionVO result = null;
        if (StringUtils.isNotBlank(sysId))
        {
            RBCondition entity = getConditionEntity(sysId, username);
            if (entity != null)
            {
                result = entity.doGetVO();
            }
        }

        return result;
    }

    public static RBCondition getConditionEntity(String sysId, String username)
    {
        RBCondition result = null;
        if (StringUtils.isNotBlank(sysId))
        {
            try
            {
                result = (RBCondition) HibernateProxy.execute(() -> {
                	return HibernateUtil.getDAOFactory().getRBConditionDAO().findById(sysId);
                });
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    public static List<RBConditionVO> findConditionsByParentId(String parentId, String parentType, String username)
    {
        List<RBConditionVO> result = new ArrayList<RBConditionVO>();
        if (StringUtils.isNotBlank(parentId))
        {
            try
            {
                QueryDTO queryDTO = new QueryDTO();
                queryDTO.setModelName("RBCondition");
                queryDTO.addFilterItem(new QueryFilter("parentId", QueryFilter.EQUALS, parentId)).addFilterItem(new QueryFilter("parentType", QueryFilter.EQUALS, parentType));
                result = (List<RBConditionVO>) findByQuery(queryDTO);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        return result;
    }

    public static RBConditionVO createCondition(RBConditionVO vo, String username) throws Exception
    {
        return (RBConditionVO) save("RBCondition", new RBCondition(vo), username);
    }

    public static RBConditionVO updateCondition(RBConditionVO vo, String username) throws Exception
    {
        RBConditionVO result = null;

        if (vo != null)
        {
            try
            {
                String id = vo.getSys_id();
                if (StringUtils.isBlank(id))
                {
                    throw new Exception("ID is blank, nothing to do.");
                }

                RBCondition entity = getConditionEntity(id, username);

                if (entity == null)
                {
                    throw new Exception("Condition not found with the id: " + id);
                }
                
                PlaybookUtils.v2LicenseViolationsAndEnvCheck("RBCondition");
                
                entity.setOrder(vo.getOrder());

                // this is update, so let's make sure params are properly
                // handled.
                Collection<RBCriterion> originalParams = entity.getCriteria();

                List<RBCriterionVO> newParams = vo.getCriteria();
                Map<String, RBCriterionVO> newParamMap = new HashMap<String, RBCriterionVO>();
                Collection<RBCriterion> updatedParams = new ArrayList<RBCriterion>();

                if (newParams != null)
                {
                    for (RBCriterionVO param : newParams)
                    {
                        if (StringUtils.isNotBlank(param.getSys_id()) && !"UNDEFINED".equalsIgnoreCase(param.getSys_id()))
                        {
                            newParamMap.put(param.getSys_id(), param);
                        }
                        else
                        {
                            RBCriterion criterion = new RBCriterion(param);
                            criterion.setParent(entity);
                            updatedParams.add(criterion);
                        }
                    }
                }

                if (originalParams != null)
                {
                    for (RBCriterion param : originalParams)
                    {
                        if (newParamMap.containsKey(param.getSys_id()))
                        {
                            RBCriterionVO newParam = newParamMap.get(param.getSys_id());
                            param.setVariable(newParam.getVariable());
                            param.setVariableSource(newParam.getVariableSource());
                            param.setComparison(newParam.getComparison());
                            param.setSource(newParam.getSource());
                            param.setSourceName(newParam.getSourceName());
                            param.setParent(entity);
                            updatedParams.add(param);
                            newParamMap.remove(param.getSys_id());
                        }
                        else
                        {
                        	HibernateProxy.execute(() -> {
                        		HibernateUtil.deleteObj("RBCriterion", param);
                            });
                        }
                    }
                }

                entity.setCriteria(updatedParams);

                HibernateProxy.execute(() -> {
                	HibernateUtil.saveObj("RBCondition", entity);
                });

                result = entity.doGetVO();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw e;
            }
        }
        return result;

    }

    public static boolean deleteCondition(String sysId, String username)
    {
        boolean result = true;
        if (StringUtils.isNotBlank(sysId))
        {
            List<RBTaskVO> tasks = findTasksByParentId(sysId, "condition", null, username);
            for (RBTaskVO task : tasks)
            {
                deleteTask(task.getSys_id(), username);
            }
            RBCondition condition = getConditionEntity(sysId, username);
            if (condition != null)
			{
				try {
					HibernateProxy.execute(() -> {
						HibernateUtil.deleteObj("RBCondition", condition);
					});
				} catch (Exception e) {
					Log.log.error(e.getMessage(), e);
                                 HibernateUtil.rethrowNestedTransaction(e);
				}
            }
        }
        return result;
    }

    public static RBTaskVO getTask(String sysId, String username)
    {
        RBTaskVO result = null;
        if (StringUtils.isNotBlank(sysId))
        {
            RBTask entity = getTaskEntity(sysId, username);
            if (entity != null)
            {
                result = entity.doGetVO();
            }
        }

        return result;
    }

    public static RBTask getTaskEntity(String sysId, String username)
    {
        RBTask result = null;
        if (StringUtils.isNotBlank(sysId))
        {
            try
            {
            	result = (RBTask) HibernateProxy.execute(() -> {
            		return  HibernateUtil.getDAOFactory().getRBTaskDAO().findById(sysId);
            	});
            	
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }

        return result;
    }

    /**
     * Finds all the Tasks for a parent.
     * 
     * @param parentId
     * @param parentType
     *            "condition" or "connection"
     * @param username
     * @return
     */
    @SuppressWarnings("unchecked")
    public static List<RBTaskVO> findTasksByParentId(String parentId, String parentType, RBConnectionVO connection, String username)
    {
        List<RBTaskVO> result = new ArrayList<RBTaskVO>();
        if (StringUtils.isNotBlank(parentId))
        {
            try
            {
                QueryDTO queryDTO = new QueryDTO();
                queryDTO.setModelName("RBTask");
                queryDTO.addFilterItem(new QueryFilter("parentId", QueryFilter.EQUALS, parentId)).addFilterItem(new QueryFilter("parentType", QueryFilter.EQUALS, parentType));
                List<RBTaskVO> taskVOs = (List<RBTaskVO>) findByQuery(queryDTO);
                if (connection != null)
                {
                    //for local task if the queueName is set all the tasks get that from the local connection
                    String queueName = null;
                    String queueNameSource = null;
                    for(RBConnectionParamVO param : connection.getParams())
                    {
                        if("queueName".equalsIgnoreCase(param.getName()))
                        {
                            if(StringUtils.isNotBlank(param.getSourceName()))
                            {
                                queueName = param.getSourceName();
                                queueNameSource = param.getSource();
                            }
                            break;
                        }
                    }

                    for (RBTaskVO taskVO : taskVOs)
                    {
                        taskVO.setConnectionType(connection.getType());
                        //set queue name from the connection, all the time
                        taskVO.setQueueName(queueName);
                        taskVO.setQueueNameSource(queueNameSource);
                    }
                }
                result = taskVOs;
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        return result;
    }

    /**
     * Finds all the Tasks inside a RB.
     * 
     * @param generalId
     * @param username
     * @return
     */
    static List<RBTaskVO> findTasksByGeneralId(String generalId, String username)
    {
        List<RBTaskVO> result = new ArrayList<RBTaskVO>();
        if (StringUtils.isNotBlank(generalId))
        {
            try
            {
                // loops
                List<RBLoopVO> loops = findLoopsByParentId(generalId, username);
                for (RBLoopVO loop : loops)
                {
                    // connections inside loop
                    List<RBConnectionVO> connections = findConnectionsByParentId(loop.getSys_id(), ABConstants.LOOP, username);
                    for (RBConnectionVO connection : connections)
                    {
                        // if inside connection
                        List<RBIfVO> ifVOs = findIfsByParentId(connection.getSys_id(), ABConstants.CONNECTION, username);
                        for (RBIfVO ifVO : ifVOs)
                        {
                            // condition inside if
                            List<RBConditionVO> conditions = findConditionsByParentId(ifVO.getSys_id(), ABConstants.IF, username);
                            for (RBConditionVO conditionVO : conditions)
                            {
                                result.addAll(findTasksByParentId(conditionVO.getSys_id(), ABConstants.CONDITION, connection, username));
                            }
                        }
                        // directly under connection
                        result.addAll(findTasksByParentId(connection.getSys_id(), ABConstants.CONNECTION, connection, username));
                    }
                }
                // direct connections
                List<RBConnectionVO> connections = findConnectionsByParentId(generalId, ABConstants.BUILDER, username);
                for (RBConnectionVO connection : connections)
                {
                    // if inside connection
                    List<RBIfVO> ifVOs = findIfsByParentId(connection.getSys_id(), ABConstants.CONNECTION, username);
                    for (RBIfVO ifVO : ifVOs)
                    {
                        // condition inside if
                        List<RBConditionVO> conditions = findConditionsByParentId(ifVO.getSys_id(), ABConstants.IF, username);
                        for (RBConditionVO conditionVO : conditions)
                        {
                            result.addAll(findTasksByParentId(conditionVO.getSys_id(), ABConstants.CONDITION, connection, username));
                        }
                    }
                    // directly under connection
                    result.addAll(findTasksByParentId(connection.getSys_id(), ABConstants.CONNECTION, connection, username));
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        return result;
    }

    public static RBTaskVO createTask(RBTaskVO vo, String username) throws Exception
    {
        if(isTaskExists(vo))
        {
            throw new Exception("Task already exists: " + vo.getName());
        }
        return (RBTaskVO) save("RBTask", new RBTask(vo), username);
    }

    @SuppressWarnings("unchecked")
    private static boolean isTaskExists(RBTaskVO vo)
    {
        boolean result = false;
        if (StringUtils.isNotBlank(vo.getGeneralId()) && !vo.getGeneralId().equalsIgnoreCase(VO.STRING_DEFAULT))
        {
            try
            {
                QueryDTO queryDTO = new QueryDTO();
                queryDTO.setModelName("RBTask");
                queryDTO.addFilterItem(new QueryFilter("generalId", QueryFilter.EQUALS, vo.getGeneralId()));
                if(StringUtils.isNotBlank(vo.getName()) && !vo.getName().equalsIgnoreCase(VO.STRING_DEFAULT))
                {
                    queryDTO.addFilterItem(new QueryFilter("name", QueryFilter.EQUALS, vo.getName()));
                    
                    if (vo.getRefRunbookParams() != null && 
                        !vo.getRefRunbookParams().trim().equalsIgnoreCase(VO.STRING_DEFAULT) &&
                        StringUtils.isNotBlank(vo.getRefRunbookParams().trim()))
                    {
                        queryDTO.addFilterItem(new QueryFilter("parentId", QueryFilter.EQUALS, vo.getParentId()));
                        queryDTO.addFilterItem(new QueryFilter(QueryFilter.DOUBLE_TYPE, vo.getOrder().toString(), "orderNumber", QueryFilter.EQUALS));
                    }
                }
                List<RBTaskVO> taskVOs = (List<RBTaskVO>) findByQuery(queryDTO);
                if(taskVOs != null && taskVOs.size() > 0)
                {
                    for(RBTaskVO taskVO : taskVOs)
                    {
                        if(!taskVO.getSys_id().equalsIgnoreCase(vo.getSys_id()))
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        return result;
    }

    public static RBTaskVO updateTask(RBTaskVO vo, String username) throws Exception
    {
        RBTaskVO result = null;

        if (vo != null)
        {
            if(isTaskExists(vo))
            {
                throw new Exception("Task already exists: " + vo.getName());
            }
            
            String msgObj = String.format("RBTask \"%s\"", vo.getName());
            
            PlaybookUtils.v2LicenseViolationsAndEnvCheck(msgObj);
            
            try
            {
                String id = vo.getSys_id();
                if (StringUtils.isBlank(id))
                {
                    throw new Exception("ID is blank, nothing to do.");
                }

                RBTask entity = getTaskEntity(id, username);

                if (entity == null)
                {
                    throw new Exception("Task not found with the id: " + id);
                }

                entity.setParentId(vo.getParentId());
                entity.setParentType(vo.getParentType());
                entity.setName(vo.getName());
                entity.setOrderNumber(vo.getOrder());
                entity.setCommand(vo.getCommand());
                entity.setCommandUrl(vo.getCommandUrl());
                entity.setEncodedCommand(vo.getEncodedCommand());
                entity.setPromptSource(vo.getPromptSource());
                entity.setPromptSourceName(vo.getPromptSourceName());
                entity.setParserType(vo.getParserType());
                entity.setSampleOutput(vo.getSampleOutput());
                entity.setRepeat(vo.getRepeat());
                entity.setDescription(vo.getDescription());
                entity.setSummary(vo.getSummary());
                entity.setDetail(vo.getDetail());
                entity.setMethod(vo.getMethod());
                entity.setBody(vo.getBody());
                entity.setDefaultAssess(vo.getDefaultAssess());
                entity.setDefaultSeverity(vo.getDefaultSeverity());
                entity.setContinuation(vo.getContinuation());
                entity.setQueueName(vo.getQueueName());
                entity.setQueueNameSource(vo.getQueueNameSource());
                entity.setParserConfig(vo.getParserConfig());
                entity.setAssessorAutoGenEnabled(vo.getAssessorAutoGenEnabled());
                entity.setParserAutoGenEnabled(vo.getParserAutoGenEnabled());
                entity.setTimeout(vo.getTimeout());
                entity.setExpectTimeout(vo.getExpectTimeout());
                entity.setInputfile(vo.getInputfile());
                entity.setRefRunbookParams(vo.getRefRunbookParams());
                if (entity.getAssessorAutoGenEnabled() != null && entity.getAssessorAutoGenEnabled() != false)
                {
                    entity.setAssessorScript(generateAssessmentCode(vo));
                }
                else
                {
                    entity.setAssessorScript(vo.getAssessorScript());
                }
                if (entity.getParserAutoGenEnabled() != null  && entity.getParserAutoGenEnabled() != false)
                {
                    JsonNode jsonReqParams  = new ObjectMapper().configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false).valueToTree(vo);
                    ParserCodeGenerator codeGen = new ParserCodeGenerator(jsonReqParams);
                    StringBuilder parserCode = new StringBuilder();
                    if(StringUtils.isBlank(vo.getCommand()) && StringUtils.isBlank(vo.getCommandUrl()))
                    {
                        parserCode.append("//in assessor task raw comes from previous task\n");
                        parserCode.append("if(FLOWS[\"PREV_RAW\"] != null)\n");
                        parserCode.append("{\n");
                        parserCode.append("     RAW = FLOWS[\"PREV_RAW\"];\n");
                        parserCode.append("}\n");
                    }
                    parserCode.append(codeGen.generateCode());
                    entity.setParserScript(parserCode.toString());
                }
                else
                {
                    entity.setParserScript(vo.getParserScript());
                }
                
                // CONDITION
                Map<String, RBTaskConditionVO> incomingConditionMap = new HashMap<String, RBTaskConditionVO>();
                List<RBTaskCondition> conditions = new ArrayList<RBTaskCondition>();

                if (vo.getConditions() != null)
                {
                    for (RBTaskConditionVO conditionVO : vo.getConditions())
                    {
                        if (StringUtils.isNotBlank(conditionVO.getSys_id()) && !"UNDEFINED".equalsIgnoreCase(conditionVO.getSys_id()))
                        {
                            incomingConditionMap.put(conditionVO.getSys_id(), conditionVO);
                        }
                        else
                        {
                            RBTaskCondition assess = new RBTaskCondition(conditionVO);
                            assess.setTask(entity);
                            conditions.add(assess);
                        }
                    }
                }

                if (entity.getConditions() != null)
                {
                    for (RBTaskCondition condition : entity.getConditions())
                    {
                        if (incomingConditionMap.containsKey(condition.getSys_id()))
                        {
                            RBTaskConditionVO newAssess = incomingConditionMap.get(condition.getSys_id());
                            condition.setConditionId(newAssess.getConditionId());
                            condition.setResult(newAssess.getResult());
                            condition.setCriteria(newAssess.getCriteria());
                            condition.setVariableSource(newAssess.getVariableSource());
                            condition.setVariable(newAssess.getVariable());
                            condition.setComparison(newAssess.getComparison());
                            condition.setSource(newAssess.getSource());
                            condition.setSourceName(newAssess.getSourceName());
                            condition.setOrder(newAssess.getOrder());

                            condition.setTask(entity);
                            conditions.add(condition);
                        }
                        else
                        {
                            HibernateProxy.execute(() -> {
                            	 HibernateUtil.delete("RBTaskCondition", condition.getSys_id());
                            });
                            
                        }
                    }
                }
                entity.setConditions(conditions);

                // SEVERITY
                Map<String, RBTaskSeverityVO> incomingSeverityMap = new HashMap<String, RBTaskSeverityVO>();
                Collection<RBTaskSeverity> severities = new ArrayList<RBTaskSeverity>();

                if (vo.getSeverities() != null)
                {
                    for (RBTaskSeverityVO severityVO : vo.getSeverities())
                    {
                        if (StringUtils.isNotBlank(severityVO.getSys_id()) && !"UNDEFINED".equalsIgnoreCase(severityVO.getSys_id()))
                        {
                            incomingSeverityMap.put(severityVO.getSys_id(), severityVO);
                        }
                        else
                        {
                            RBTaskSeverity severity = new RBTaskSeverity(severityVO);
                            severity.setTask(entity);
                            severities.add(severity);
                        }
                    }
                }

                if (entity.getSeverities() != null)
                {
                    for (RBTaskSeverity severity : entity.getSeverities())
                    {
                        if (incomingSeverityMap.containsKey(severity.getSys_id()))
                        {
                            RBTaskSeverityVO newSeverity = incomingSeverityMap.get(severity.getSys_id());
                            severity.setSeverityId(newSeverity.getSeverityId());
                            severity.setSeverity(newSeverity.getSeverity());
                            severity.setCriteria(newSeverity.getCriteria());
                            severity.setVariableSource(newSeverity.getVariableSource());
                            severity.setVariable(newSeverity.getVariable());
                            severity.setComparison(newSeverity.getComparison());
                            severity.setSource(newSeverity.getSource());
                            severity.setSourceName(newSeverity.getSourceName());
                            severity.setOrder(newSeverity.getOrder());
                            severity.setTask(entity);
                            severities.add(severity);
                        }
                        else
                        {
                            HibernateProxy.execute(() -> {
                            	HibernateUtil.delete("RBTaskSeverity", severity.getSys_id());
                            });
                        }
                    }
                }
                entity.setSeverities(severities);

                // VARIABLE
                Map<String, RBTaskVariableVO> incomingVariableMap = new HashMap<String, RBTaskVariableVO>();
                Collection<RBTaskVariable> variables = new ArrayList<RBTaskVariable>();

                if (vo.getVariables() != null)
                {
                    for (RBTaskVariableVO variableVO : vo.getVariables())
                    {
                        if (StringUtils.isNotBlank(variableVO.getSys_id()))
                        {
                            incomingVariableMap.put(variableVO.getSys_id(), variableVO);
                        }
                        else
                        {
                            RBTaskVariable variable = new RBTaskVariable(variableVO);
                            variable.setTask(entity);
                            variables.add(variable);
                        }
                    }
                }

                if (entity.getVariables() != null)
                {
                    for (RBTaskVariable variable : entity.getVariables())
                    {
                        if (incomingVariableMap.containsKey(variable.getSys_id()))
                        {
                            RBTaskVariableVO newVariable = incomingVariableMap.get(variable.getSys_id());
                            variable.setName(newVariable.getName());
                            variable.setColor(newVariable.getColor());
                            variable.setList(newVariable.isList());
                            variable.setFlow(newVariable.isFlow());
                            variable.setWsdata(newVariable.isWsdata());
                            variable.setTask(entity);
                            variables.add(variable);
                        }
                        else
                        {
                        	HibernateProxy.execute(() -> {
                        		HibernateUtil.delete("RBTaskVariable", variable.getSys_id());
                            });
                        }
                    }
                }
                entity.setVariables(variables);
                
                // HEADER
                Map<String, RBTaskHeaderVO> incomingHeaderMap = new HashMap<String, RBTaskHeaderVO>();
                Collection<RBTaskHeader> headers = new ArrayList<RBTaskHeader>();

                if (vo.getHeaders() != null)
                {
                    for (RBTaskHeaderVO headerVO : vo.getHeaders())
                    {
                        String sysId = headerVO.getSys_id();
                        if (StringUtils.isNotBlank(sysId) && !sysId.equals("UNDEFINED"))
                        {
                            incomingHeaderMap.put(sysId, headerVO);
                        }
                        else
                        {
                            RBTaskHeader header = new RBTaskHeader(headerVO);
                            header.setTask(entity);
                            headers.add(header);
                        }
                    }
                }

                if (entity.getHeaders() != null)
                {
                    for (RBTaskHeader header : entity.getHeaders())
                    {
                        if (incomingHeaderMap.containsKey(header.getSys_id()))
                        {
                            RBTaskHeaderVO newHeader = incomingHeaderMap.get(header.getSys_id());
                            header.setName(newHeader.getName());
                            header.setValue(newHeader.getValue());
                            header.setTask(entity);
                            headers.add(header);
                        }
                        else
                        {
                        	HibernateProxy.execute(() -> {
                        		HibernateUtil.delete("RBTaskHeader", header.getSys_id());
                            });
                        }
                    }
                }
                
                entity.setHeaders(headers);

                HibernateProxy.execute(() -> {
                	HibernateUtil.saveObj("RBTask", entity);
                });

                result = entity.doGetVO();

            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                throw e;
            }
        }
        return result;
    }

    public static boolean deleteTask(String sysId, String username)
    {
        boolean result = true;
        if (StringUtils.isNotBlank(sysId))
        {
            try
            {
                RBTask task = getTaskEntity(sysId, username);
                if (task != null)
                {
        			HibernateProxy.execute(() -> {
        				HibernateUtil.deleteObj("RBTask", task);
        			});
                }
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
                HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        return result;
    }

    /**
     * This method provides the tree for rendering the tree structure for RB.
     * 
     * @param id
     * @param username
     * @return
     */
    public static RBTree getRBTree(String id, String username)
    {
        RBTree root = null;

        RBGeneralVO general = getGeneral(id, username);

        if (general != null)
        {
            root = new RBTree(general); // root
            addLoops(root, id, username);
            addConnections(root, id, ABConstants.BUILDER, username);
        }

        return root;
    }

    private static void addLoops(RBTree root, String builderId, String username)
    {
        List<RBLoopVO> loops = findLoopsByParentId(builderId, username);
        for (RBLoopVO loop : loops)
        {
            RBTree loopTree = new RBTree(loop);
            root.addChildren(loopTree);
            addConnections(loopTree, loop.getSys_id(), ABConstants.LOOP, username);
        }
    }

    private static void addConnections(RBTree parentTree, String parentId, String parentType, String username)
    {
        List<RBConnectionVO> connections = findConnectionsByParentId(parentId, parentType, username);
        for (RBConnectionVO connection : connections)
        {
            RBTree connectionTree = new RBTree(connection);
            parentTree.addChildren(connectionTree);
            addIfs(connectionTree, connection.getSys_id(), ABConstants.CONNECTION, username);
            addTasks(connectionTree, connection.getSys_id(), ABConstants.CONNECTION, username);
        }
    }

    private static void addIfs(RBTree parentTree, String parentId, String parentType, String username)
    {
        List<RBIfVO> ifs = findIfsByParentId(parentId, parentType, username);
        for (RBIfVO ifVO : ifs)
        {
            RBTree ifTree = new RBTree(ifVO);
            parentTree.addChildren(ifTree);
            addConditions(ifTree, ifVO.getSys_id(), ABConstants.IF, username);
        }
    }

    private static void addConditions(RBTree parentTree, String parentId, String parentType, String username)
    {
        List<RBConditionVO> conditions = findConditionsByParentId(parentId, parentType, username);
        for (RBConditionVO conditionVO : conditions)
        {
            RBTree conditionTree = new RBTree(conditionVO);
            parentTree.addChildren(conditionTree);
            addTasks(conditionTree, conditionVO.getSys_id(), ABConstants.CONDITION, username);
        }
    }

    private static void addTasks(RBTree parentTree, String parentId, String parentType, String username)
    {
        List<RBTaskVO> tasks = findTasksByParentId(parentId, parentType, null, username);
        for (RBTaskVO taskVO : tasks)
        {
            parentTree.addChildren(taskVO);
        }
    }

    /**************
     * Method used to convert Interface defined delimiters such as SPACE and
     * COMMA into Regex defined delimiters such as \\s and ,
     * 
     * @param automationBuilderInterfaceNamedDelimiters
     *            - List of deliters
     * @return List of Regex compatible delimiters
     */
    public static ArrayList<String> findRegexDelimiters(ArrayList<String> automationBuilderInterfaceNamedDelimiters)
    {
        ArrayList<String> regexDelimiters = new ArrayList<String>();

        Iterator<String> listIterator = automationBuilderInterfaceNamedDelimiters.iterator();
        while (listIterator.hasNext())
        {
            String curItem = listIterator.next();
            if (curItem.equals("SPACE"))
            {
                regexDelimiters.add("\\\\s+");
            }
            else if (curItem.equals("COMMA"))
            {
                regexDelimiters.add(",");
            }
            else if (curItem.equals("TAB"))
            {
                regexDelimiters.add("\\\\t+");
            }
            else
            {
                regexDelimiters.add(curItem);
            }
        }

        return regexDelimiters;
    }

    /*******
     * Searches through JsonNode for an object with key: "action" and value
     * specified by parameter action.
     * 
     * @param markupsRoot
     *            - JsonNode consisting of markups only
     * @param action
     *            - expected action name
     * @return JsonNode if object found meeting criteria otherwise null returned
     */
    // extracts from markups JsonNode with action = value
    public static JsonNode findNodeWithAction(JsonNode markupsRoot, String action)
    {
        Iterator<JsonNode> iterator = markupsRoot.getElements();
        while (iterator.hasNext())
        {
            JsonNode curNode = iterator.next();
            if (curNode.findValue("action").asText().equals(action))
            {
                return curNode;
            }
        }

        return null;
    }
    
    public static JSONObject pollParserTest(String username) throws Exception {
        if(!parserCodeTestProcesses.containsKey(username))
            return null;
//            throw new Exception("Cannnot find parser testing process tied to "+ username);
        
        ParserCodeTestProcess proc = parserCodeTestProcesses.get(username);
        JSONObject status = proc.getStatus();
        if(status.getBoolean("success")==false||status.getString("stage").equals(ABProcessConst.DONE.toString()))
            parserCodeTestProcesses.remove(username);
        return status;
    }
    
    private static ConcurrentHashMap<String,ParserCodeTestProcess> parserCodeTestProcesses = new ConcurrentHashMap<String,ParserCodeTestProcess>();

    public static ABProcessConst testCode(String groovyScript, String sampleData, String username)
    {
        if(parserCodeTestProcesses.containsKey(username))
            return ABProcessConst.INPROGRESS;
        ParserCodeTestProcess proc = new ParserCodeTestProcess(groovyScript,sampleData,username);
        proc.start();
        parserCodeTestProcesses.put(username, proc);
        return ABProcessConst.STARTED;
    }

    private static String generateAssessmentCode(RBTaskVO taskVO)
    {
        StringBuilder result = new StringBuilder();

        //we use some methods from this class during condition and severity evaluation
        result.append("import static com.resolve.util.ComparisonUtils.*;\n\n");
        
        // The parser code provides the parsed data inside DATA variable.
        //result.append("//add DATA into the FLOWS\n");
        //result.append("FLOWS.putAll(DATA)\n");

        if(StringUtils.isBlank(taskVO.getCommand()) && StringUtils.isBlank(taskVO.getCommandUrl()))
        {
            result.append("\nRAW=FLOWS[\"PREV_RAW\"]\n");
        }
        else
        {
            result.append("\nFLOWS[\"PREV_RAW\"]=RAW\n");
        }
        // CONDITION
        Map<Integer, List<RBTaskConditionVO>> orderedConditions = new TreeMap<Integer, List<RBTaskConditionVO>>();
        List<RBTaskConditionVO> conditions = taskVO.getConditions();
        for (RBTaskConditionVO condition : conditions)
        {
            List<RBTaskConditionVO> theList = orderedConditions.get(condition.getOrder());
            if (theList == null)
            {
                theList = new ArrayList<RBTaskConditionVO>();
            }
            theList.add(condition);
            orderedConditions.put(condition.getOrder(), theList);
        }

        StringBuilder conditionScript = new StringBuilder();
        conditionScript.append("//CONDITION\n");
        if (orderedConditions.size() > 0)
        {
            conditionScript.append("if(");
            for (Integer order : orderedConditions.keySet())
            {
                List<RBTaskConditionVO> conditionList = orderedConditions.get(order);
                String condition = conditionList.get(0).getResult(); // get the
                                                                     // result
                                                                     // from
                                                                     // first
                                                                     // object
                for (RBTaskConditionVO conditionVO : conditionList)
                {
                    conditionScript.append(getExpression(conditionVO.getCriteria(), conditionVO.getVariableSource(), conditionVO.getVariable(), conditionVO.getComparison(), conditionVO.getSource(), conditionVO.getSourceName()));
                    conditionScript.append("\n    && ");
                }
                conditionScript.setLength(conditionScript.length() - 9);
                conditionScript.append("))\n");
                conditionScript.append("{\n");
                conditionScript.append("     RESULT.condition=\"").append(condition).append("\"\n");
                conditionScript.append("}\n");
                conditionScript.append("else if(");
            }
            conditionScript.setLength(conditionScript.length() - 8);
            // set the default condition
            conditionScript.append("else\n");
            conditionScript.append("{\n");
            conditionScript.append("     RESULT.condition=\"").append(taskVO.getDefaultAssess()).append("\"\n");
            conditionScript.append("}\n");
        }
        else
        {
            // set the default condition
            conditionScript.append("//set the default condition\n");
            conditionScript.append("RESULT.condition=\"").append(taskVO.getDefaultAssess()).append("\"\n");
        }

        // SEVERITY
        Map<Double, List<RBTaskSeverityVO>> orderedSeverities = new TreeMap<Double, List<RBTaskSeverityVO>>();
        List<RBTaskSeverityVO> severities = taskVO.getSeverities();
        for (RBTaskSeverityVO condition : severities)
        {
            List<RBTaskSeverityVO> theList = orderedSeverities.get(condition.getOrder());
            if (theList == null)
            {
                theList = new ArrayList<RBTaskSeverityVO>();
            }
            theList.add(condition);
            orderedSeverities.put(condition.getOrder(), theList);
        }

        StringBuilder severityScript = new StringBuilder();
        severityScript.append("//SEVERITY\n");
        if (orderedSeverities.size() > 0)
        {
            severityScript.append("if(");
            for (Double order : orderedSeverities.keySet())
            {
                List<RBTaskSeverityVO> severityList = orderedSeverities.get(order);
                String severity = severityList.get(0).getSeverity(); // get the
                                                                     // severity
                                                                     // first
                                                                     // object
                for (RBTaskSeverityVO severityVO : severityList)
                {
                    severityScript.append(getExpression(severityVO.getCriteria(), severityVO.getVariableSource(), severityVO.getVariable(), severityVO.getComparison(), severityVO.getSource(), severityVO.getSourceName()));
                    severityScript.append("\n    && ");
                }
                severityScript.setLength(severityScript.length() - 9);
                severityScript.append("))\n");
                severityScript.append("{\n");
                severityScript.append("     RESULT.severity=\"").append(severity).append("\"\n");
                severityScript.append("}\n");
                severityScript.append("else if(");
            }
            severityScript.setLength(severityScript.length() - 8); // take out
                                                                   // the last
                                                                   // "else if"
            // set the default condition
            severityScript.append("else\n");
            severityScript.append("{\n");
            severityScript.append("     RESULT.severity=\"").append(taskVO.getDefaultSeverity()).append("\"\n");
            severityScript.append("}\n");
        }
        else
        {
            // set the default severity
            severityScript.append("//set the default severity\n");
            severityScript.append("RESULT.severity=\"").append(taskVO.getDefaultSeverity()).append("\"\n");
        }

        result.append(conditionScript);
        result.append("\n");
        result.append(severityScript);
        // append the summary and details.
        result.append("\nRESULT.summary=\"\"\"\\\n").append(taskVO.getSummary()).append("\n\"\"\"");
        result.append("\nRESULT.detail=\"\"\"\\\n").append(taskVO.getDetail()).append("\n\"\"\"");
        result.append("\n//<==ASSESSOR_CODE_ABOVE_THIS_LINE==>\n");

        return result.toString();
    }

    private static String getExpression(final String criteria, final String variableSource, final String variableName, final String comparison, final String source, final String sourceName)
    {
        StringBuilder result = new StringBuilder();

        String variableString = null;
        String sourceString = null;
/*
        if ("contains".equalsIgnoreCase(comparison) || "eq".equalsIgnoreCase(comparison))
        {
            variableString = getComparisonItem(variableSource, variableName, "string");
            sourceString = getComparisonItem(source, sourceName, "string");
        }
        else
        {
            variableString = getComparisonItem(variableSource, variableName, "numeric");
            sourceString = getComparisonItem(source, sourceName, "numeric");
        }
*/
        variableString = getComparisonItem(variableSource, variableName, "string");
        sourceString = getComparisonItem(source, sourceName, "string");

        if ("all".equalsIgnoreCase(criteria))
        {
            result.append("compareAll(").append(variableString).append(",").append(sourceString).append(",\"").append(ABConstants.OPERATORS.get(comparison)).append("\")");
        }
        else
        {
            result.append("compareAny(").append(variableString).append(",").append(sourceString).append(",\"").append(ABConstants.OPERATORS.get(comparison)).append("\")");
        }

        // FLOWS["var1"].toBigDecimal() > FLOWS["src1"].toBigDecimal()
        // FLOWS["var1"].toBigDecimal() <= 23
        // result.append(variableString).append(ABConstants.OPERATORS.get(comparison)).append(sourceString);

        return result.toString();
    }

    private static String getComparisonItem(String itemSource, String item, String itemType)
    {
        StringBuilder result = new StringBuilder();
        if ("constant".equalsIgnoreCase(itemSource))
        {
            if ("string".equalsIgnoreCase(itemType))
            {
                result.append("\"").append(item).append("\"");
            }
            else
            {
                result.append(item);
            }
        }
        else
        {
            result.append(ABConstants.SOURCE_MAP.get(itemSource)).append("[\"").append(item).append("\"]");
            /*
             * if("numeric".equalsIgnoreCase(itemType)) {
             * result.append(ABConstants
             * .SOURCE_MAP.get(itemSource)).append("[\""
             * ).append(item).append("\"].toBigDecimal()"); } else {
             * result.append
             * (ABConstants.SOURCE_MAP.get(itemSource)).append("[\""
             * ).append(item).append("\"]"); }
             */
        }
        return result.toString();
    }
    
    @SuppressWarnings("rawtypes")
    private static Collection<String> findTaskFullNamesFromModelXml(String modelXml) throws Exception
    {
        Collection<String> result = new HashSet<String>();
        SAXReader reader = new SAXReader();
        reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
        reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        reader.setEntityResolver(new EntityResolver() {
            @Override
            public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
                return new InputSource(new ByteArrayInputStream(new byte[0]));
            }
        });
        Document document = reader.read(new ByteArrayInputStream(modelXml.getBytes()));
        //Document document = DocumentHelper.parseText(modelXml);
        
        List list = document.selectNodes("/mxGraphModel/root/Task");
        
        if (list != null && !list.isEmpty())
        {
            for (Iterator i = list.iterator(); i.hasNext();)
            {
                Node xmlNode = (Node) i.next();
                String taskDesc = xmlNode.valueOf("@description").trim();
                
                Log.log.trace("Task description is [" + taskDesc + "]");
                
                int paramPos = taskDesc.indexOf('?');
                
                if (paramPos < 0)
                {
                    paramPos = taskDesc.length();
                }
                
                String taskfullname = taskDesc.substring(0, paramPos);
                
                Log.log.trace("Task fullname from model is [" + taskfullname + "]");
                
                result.add(taskfullname);
            }
        }
        
        return result;
    }
    
    public static List<ResolveActionTaskVO> listActionTask(String wikiName, String username) throws Exception
    {
        Collection<ResolveActionTaskVO> result = new HashSet<ResolveActionTaskVO>();
        WikiDocumentVO wikiDoc = ServiceWiki.getWikiDoc(null, wikiName, username);
        if (StringUtils.isBlank(wikiDoc.getUResolutionBuilderId()))
        {
            Log.log.debug("This wiki is not build using Automation Builder");
        }
        else
        {
            Collection<String> addedTaskFullNames = new HashSet<String>();
            
            RBGeneralVO general = getGeneral(wikiDoc.getUResolutionBuilderId(), username);
            
            if (general != null)
            {
                List<RBLoopVO> loopVOs = findLoopsByParentId(general.getSys_id(), username);
                List<RBConnectionVO> connectionVOs = findConnectionsByParentId(general.getSys_id(), ABConstants.BUILDER, username);
                List<RBTaskVO> taskVOs = new ArrayList<RBTaskVO>();
                for (RBLoopVO loop : loopVOs)
                {
                    List<RBConnectionVO> rbLoopConnectionVOs = findConnectionsByParentId(loop.getSys_id(), ABConstants.LOOP, username);
                    
                    if (rbLoopConnectionVOs != null )
                    {
                        connectionVOs.addAll(rbLoopConnectionVOs);
                    }
                }
    
                for (RBConnectionVO connectionVO : connectionVOs)
                {
                    List<RBTaskVO> rbParentTaskVOs = findTasksByParentId(connectionVO.getSys_id(), ABConstants.CONNECTION, connectionVO, username);
                    
                    if (rbParentTaskVOs != null && !rbParentTaskVOs.isEmpty())
                    {
                        taskVOs.addAll(rbParentTaskVOs);
                    }
                    
                    List<RBIfVO> ifVOs = findIfsByParentId(connectionVO.getSys_id(), ABConstants.CONNECTION, username);
                    for (RBIfVO ifVO : ifVOs)
                    {
                        List<RBConditionVO> conditionsVOs = findConditionsByParentId(ifVO.getSys_id(), ABConstants.IF, username);
                        for (RBConditionVO conditionVO : conditionsVOs)
                        {
                            List<RBTaskVO> rbTaskVOs = findTasksByParentId(conditionVO.getSys_id(), ABConstants.CONDITION, connectionVO, username);
                            
                            if (rbTaskVOs != null && !rbTaskVOs.isEmpty())
                            {
                                taskVOs.addAll(rbTaskVOs);
                            }
                        }
                    }
                }
                
                for (RBTaskVO taskVO : taskVOs)
                {
                    String taskModuleName = getWikiName(general.getNamespace(), general.getName());
                    String taskFullName = taskVO.getName() + "#" + taskModuleName;
                    ResolveActionTaskVO rsATVO = ServiceHibernate.getActionTaskFromIdWithReferences(null, taskFullName, username);
                    
                    if (rsATVO != null)
                    {
                        result.add(rsATVO);
                        addedTaskFullNames.add(rsATVO.getUFullName());
                        Log.log.trace("Task fullname from automation builder is [" + rsATVO.getUFullName() + "]");
                    }
                }
            }
            
            // Check the model if it exists 
            if (StringUtils.isNotBlank(wikiDoc.getUModelProcess()))
            {
                Collection<String> taskFullNames = findTaskFullNamesFromModelXml(wikiDoc.getUModelProcess());
                
                if (taskFullNames != null && !taskFullNames.isEmpty() &&
                    addedTaskFullNames != null && !addedTaskFullNames.isEmpty())
                {
                    taskFullNames.removeAll(addedTaskFullNames);
                }
                
                if (taskFullNames != null && !taskFullNames.isEmpty())
                {
                    for (String taskFullName : taskFullNames)
                    {
                        ResolveActionTaskVO rsATVO = ServiceHibernate.getActionTaskFromIdWithReferences(null, taskFullName, username);
                        
                        if (rsATVO != null)
                        {
                            result.add(rsATVO);
                        }
                    }
                }
            }
        }
        
        return new ArrayList<ResolveActionTaskVO>(result);
    }

    public static RBGeneralVO copy(String fromDocResolutionBuilderId, String wikiSysId, String wikiName, String wikiNamespace, String username) throws Exception
    {
        RBGeneralVO result = null;

        if (StringUtils.isBlank(fromDocResolutionBuilderId) || StringUtils.isBlank(wikiName) || StringUtils.isBlank(wikiNamespace))
        {
            Log.log.error("This resolutionBuilderId, wikiName, and wikiNamespace must be provided.");
            throw new Exception("This resolutionBuilderId, wikiName, and wikiNamespace must be provided.");
        }
        else
        {
            RBGeneralVO generalVO = getGeneral(fromDocResolutionBuilderId, username);
            if (generalVO != null)
            {
                String oldGeneralId = generalVO.getSys_id();

                // create a new one
                generalVO.setId(null);
                generalVO.setSys_id(null);
                generalVO.setSysCreatedBy(null);
                generalVO.setSysCreatedOn(null);
                generalVO.setSysUpdatedBy(null);
                generalVO.setSysUpdatedOn(null);
                generalVO.setWikiId(wikiSysId);
                generalVO.setName(wikiName);
                generalVO.setNamespace(wikiNamespace);
                // save the new RBGeneral
                RBGeneralVO newGeneralVO = createGeneral(generalVO, false, username);
                String newResolutionBuilderId = newGeneralVO.getSys_id();

                List<RBLoopVO> loopVOs = findLoopsByParentId(oldGeneralId, username);
                copyLoop(username, newResolutionBuilderId, loopVOs);

                List<RBConnectionVO> connectionVOs = findConnectionsByParentId(oldGeneralId, ABConstants.BUILDER, username);
                copyConnection(username, newResolutionBuilderId, connectionVOs);
                
                result = newGeneralVO;
            }
        }
        return result;
    }

    private static void copyLoop(String username, String newResolutionBuilderId, List<RBLoopVO> loopVOs) throws Exception
    {
        for (RBLoopVO loop : loopVOs)
        {
            String oldLoopId = loop.getSys_id();

            loop.setId(null);
            loop.setSys_id(null);
            loop.setSysCreatedBy(null);
            loop.setSysCreatedOn(null);
            loop.setSysUpdatedBy(null);
            loop.setSysUpdatedOn(null);
            loop.setParentId(newResolutionBuilderId);
            RBLoopVO newLoopVO = createLoop(loop, username);
            String newLoopId = newLoopVO.getSys_id();

            List<RBConnectionVO> loopConnectionVOs = findConnectionsByParentId(oldLoopId, ABConstants.LOOP, username);
            copyConnection(username, newLoopId, loopConnectionVOs);
        }
    }

    private static void copyConnection(String username, String newResolutionBuilderId, List<RBConnectionVO> connectionVOs) throws Exception
    {
        for (RBConnectionVO connectionVO : connectionVOs)
        {
            String oldConnectionId = connectionVO.getSys_id();

            connectionVO.setId(null);
            connectionVO.setSys_id(null);
            connectionVO.setSysCreatedBy(null);
            connectionVO.setSysCreatedOn(null);
            connectionVO.setSysUpdatedBy(null);
            connectionVO.setSysUpdatedOn(null);
            connectionVO.setParentId(newResolutionBuilderId);
            RBConnectionVO newConnectionVO = createConnection(connectionVO, username);
            String newConnectionId = newConnectionVO.getSys_id();

            List<RBIfVO> ifVOs = findIfsByParentId(oldConnectionId, ABConstants.CONNECTION, username);
            copyIf(username, connectionVO, newConnectionId, newResolutionBuilderId, ifVOs);

            // tasks from connection
            List<RBTaskVO> taskVOs = findTasksByParentId(oldConnectionId, ABConstants.CONNECTION, connectionVO, username);
            copyTask(username, newConnectionId, newResolutionBuilderId, taskVOs);
        }
    }

    private static void copyIf(String username, RBConnectionVO connectionVO, String newConnectionId, String newResolutionBuilderId, List<RBIfVO> ifVOs) throws Exception
    {
        for (RBIfVO ifVO : ifVOs)
        {
            String oldIfId = ifVO.getSys_id();

            ifVO.setId(null);
            ifVO.setSys_id(null);
            ifVO.setSysCreatedBy(null);
            ifVO.setSysCreatedOn(null);
            ifVO.setSysUpdatedBy(null);
            ifVO.setSysUpdatedOn(null);
            ifVO.setParentId(newConnectionId);
            RBIfVO newIfVO = createIf(ifVO, username);
            String newIfId = newIfVO.getSys_id();

            List<RBConditionVO> conditionsVOs = findConditionsByParentId(oldIfId, ABConstants.IF, username);
            copyCondition(username, connectionVO, newIfId, newResolutionBuilderId, conditionsVOs);
        }
    }

    private static void copyCondition(String username, RBConnectionVO connectionVO, String newIfId, String newResolutionBuilderId, List<RBConditionVO> conditionsVOs) throws Exception
    {
        for (RBConditionVO conditionVO : conditionsVOs)
        {
            String oldConditionId = conditionVO.getSys_id();

            conditionVO.setId(null);
            conditionVO.setSys_id(null);
            conditionVO.setSysCreatedBy(null);
            conditionVO.setSysCreatedOn(null);
            conditionVO.setSysUpdatedBy(null);
            conditionVO.setSysUpdatedOn(null);
            conditionVO.setParentId(newIfId);
            RBConditionVO newConditionVO = createCondition(conditionVO, username);
            String newConditionId = newConditionVO.getSys_id();

            // tasks from connection
            List<RBTaskVO> conditionTaskVOs = findTasksByParentId(oldConditionId, ABConstants.CONDITION, connectionVO, username);
            copyTask(username, newConditionId, newResolutionBuilderId, conditionTaskVOs);
        }
    }

    private static void copyTask(String username, String newConditionId, String newResolutionId, List<RBTaskVO> taskVOs) throws Exception
    {
        for (RBTaskVO taskVO : taskVOs)
        {
            taskVO.setId(null);
            taskVO.setSys_id(null);
            taskVO.setSysCreatedBy(null);
            taskVO.setSysCreatedOn(null);
            taskVO.setSysUpdatedBy(null);
            taskVO.setSysUpdatedOn(null);
            taskVO.setParentId(newConditionId);
            taskVO.setGeneralId(newResolutionId);

            createTask(taskVO, username);
        }
    }
    
    public static RBGeneralVO rename(String resolutionBuilderId, String newNamespace, String newName, String username) throws Exception
    {
        RBGeneralVO result = null;

        if (StringUtils.isBlank(resolutionBuilderId))
        {
            Log.log.error("This resolutionBuilderId must be provided.");
            throw new Exception("This resolutionBuilderId must be provided.");
        }
        else
        {
            RBGeneralVO generalVO = getGeneral(resolutionBuilderId, username);

            if (generalVO != null)
            {
                generalVO.setName(newName);
                generalVO.setNamespace(newNamespace);
                updateGeneral(generalVO, username, true);
                
                if(generalVO.getGenerated().booleanValue())
                {
                    CustomForm form = new CustomForm(generalVO, username);
                    RsFormDTO formDTO = form.create();
                    String formId = formDTO.getId();
                    String formName = formDTO.getName();
                    Log.log.debug("Form created with name : " + formName + " and formId : " + formId);
                }
            }
        }
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static List<RBGeneralVO> findRBGeneralByUniqueKey(String namespace, String name, String username)
    {
        List<RBGeneralVO> result = new ArrayList<RBGeneralVO>();
        if (StringUtils.isNotBlank(namespace) && StringUtils.isNotBlank(name))
        {
            try
            {
                QueryDTO queryDTO = new QueryDTO();
                queryDTO.setModelName("RBGeneral");
                queryDTO.addFilterItem(new QueryFilter("namespace", QueryFilter.EQUALS, namespace)).addFilterItem(new QueryFilter("name", QueryFilter.EQUALS, name));
                result = (List<RBGeneralVO>) findByQuery(queryDTO);
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        return result;
    }

    public static String generateParserCode(String jsonProperty)
    {
            String result = "";
            try
            {
                JsonNode jsonReqParams = new ObjectMapper().readTree(jsonProperty);
                ParserCodeGenerator codeGen = new ParserCodeGenerator(jsonReqParams);
                StringBuilder parserCode = new StringBuilder();
                if(jsonReqParams.findValue("isAssessTask").asBoolean())
                {
                    parserCode.append("//in assessor task raw comes from previous task\n");
                    parserCode.append("if(FLOWS[\"PREV_RAW\"] != null)\n");
                    parserCode.append("{\n");
                    parserCode.append("     RAW = FLOWS[\"PREV_RAW\"];\n");
                    parserCode.append("}\n");
                }
                parserCode.append(codeGen.generateCode());
                result = parserCode.toString();             
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        
        return result;
    }
}
