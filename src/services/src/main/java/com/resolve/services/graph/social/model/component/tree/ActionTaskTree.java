/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.component.tree;

import java.util.Collection;

import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Process;

public class ActionTaskTree
{
    private Collection<ActionTask> actiontasks;
    private Collection<Process> processes;
    private User owner;

    public void setProcesses(Collection<Process> processes)
    {
        this.processes = processes;
    }

    public Collection<Process> getProcesses()
    {
        return this.processes;
    }

    public void setActionTasks(Collection<ActionTask> actiontasks)
    {
        this.actiontasks = actiontasks;
    }

    public Collection<ActionTask> getActiontasks()
    {
        return this.actiontasks;
    }

    public void setOwner(User user)
    {
        this.owner = user;
    }

    public User getOwner()
    {
        return this.owner;
    }
}
