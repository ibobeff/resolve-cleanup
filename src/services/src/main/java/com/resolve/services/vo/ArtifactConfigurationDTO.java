package com.resolve.services.vo;

public class ArtifactConfigurationDTO {
	private String UName;
	private ArtifactTypeDTO artifactType;
	private ResolveActionTaskDTO task;
	private WikiDocumentDTO automation;
	private String param;

	public String getUName() {
		return UName;
	}

	public void setUName(String uName) {
		UName = uName;
	}

	public ArtifactTypeDTO getArtifactType() {
		return artifactType;
	}

	public void setArtifactType(ArtifactTypeDTO artifactType) {
		this.artifactType = artifactType;
	}

	public ResolveActionTaskDTO getTask() {
		return task;
	}

	public void setTask(ResolveActionTaskDTO task) {
		this.task = task;
	}

	public WikiDocumentDTO getAutomation() {
		return automation;
	}

	public void setAutomation(WikiDocumentDTO automation) {
		this.automation = automation;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}
}
