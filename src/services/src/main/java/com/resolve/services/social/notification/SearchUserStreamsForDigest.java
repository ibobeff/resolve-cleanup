/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.social.notification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.persistence.model.ResolveNode;
import com.resolve.persistence.model.SocialUserNotification;
import com.resolve.services.ServiceGraph;
import com.resolve.services.ServiceSocial;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;

/**
 * util to get map of users with the list of component/s post that should be available in the digest
 * specific api for '_DIGEST_EMAIL' notification type
 * 
 * Incase you want to verify, refer to the older 5.1 version of com.resolve.graph.social.SearchUserStreamsForDigest.java
 * 
 * @author jeet.marwah
 *
 */
public class SearchUserStreamsForDigest
{
    private List<SocialUserNotification> allUsersWithDigestEmail = null;
    
    private Set<String> userSysIdsProcessed = new HashSet<String>();
    private Set<String> userSysIdsToProcess = new HashSet<String>();
    private Map<User, List<RSComponent>> result = new HashMap<User, List<RSComponent>>();
    
    
    public Map<User, List<RSComponent>> getUserStreamsForDigest() throws Exception
    {
        //get all users that has the Digest email conf
        allUsersWithDigestEmail = findAllDigestEmailForAllUsers();
        
        //first do it for USER_ALL_DIGEST_EMAIL notification 
        processAllDigestEmail();
        
        //process other recs 
        processOtherUsers();
        
        
        return result;
        
    }
    
    private void processAllDigestEmail() throws Exception
    {
        //first do it for USER_ALL_DIGEST_EMAIL notification 
        List<SocialUserNotification> allDigestEmail = filterNotificationsFor(UserGlobalNotificationContainerType.USER_ALL_DIGEST_EMAIL);
        for(SocialUserNotification userNot : allDigestEmail)
        {
//            boolean emailDigest = StringUtils.isNotEmpty(userNot.getUNotificationValue()) && userNot.getUNotificationValue().equalsIgnoreCase("true") ? true : false;
//            if(emailDigest)
//            {
                String userSysId = userNot.getUser().getSys_id();
                ResolveNodeVO userNode = ServiceGraph.findNode(null, userSysId, null, NodeType.USER, "system");
                if(userNode != null)
                {
                    if(userSysIdsProcessed.contains(userSysId))
                    {
                        //ignore if its already processed
                        continue;
                    }
                    
                    //add all the components that this User is following
                    result.put(new User(userNode), ServiceSocial.getUserStreams(userSysId, null, true));
                    
                    //update the list so that this is not processed again
                    userSysIdsProcessed.add(userSysId);
                }
//            }
        }//end of for loop
    }
    
    private void processOtherUsers() throws Exception
    {
        for(String userSysId : this.userSysIdsToProcess)
        {
            //ignore if its already done for ALL
            if(this.userSysIdsProcessed.contains(userSysId))
            {
                continue;
            }
            
            ResolveNodeVO userNode = ServiceGraph.findNode(null, userSysId, null, NodeType.USER, "system");
            if(userNode != null)
            {
                //get notifications for this user
                List<SocialUserNotification> userNotifications = filterNotificationForUser(userSysId);
                if(userNotifications.size() > 0)
                {
                    List<RSComponent> comps = new ArrayList<RSComponent>();
                    boolean selfAdded = false; //flag to avoid the duplicate being added 
                    for(SocialUserNotification userNotification : userNotifications)
                    {
                        if(userNotification.getUNotification().equalsIgnoreCase(UserGlobalNotificationContainerType.USER_DIGEST_EMAIL.name())
                                        || userNotification.getUNotification().equalsIgnoreCase(UserGlobalNotificationContainerType.USER_SYSTEM_DIGEST_EMAIL.name()))
                        {
                            //add self
                            if(!selfAdded)
                            {
                                comps.add(new RSComponent(userNode));
                                selfAdded = true; 
                            }
                        }
                        else
                        {
                            ResolveNode targetNode = userNotification.getNode();
                            if(targetNode != null)
                            {
                                comps.add(new RSComponent(targetNode.doGetVO()));
                            }
                            
                        }
                        
                    }//end of for loop
                    
                    
                    if(comps.size() > 0)
                    {
                        result.put(new User(userNode), comps);
                    }
                    
                }
            }//end of if
        }//end of for loop
    }
    
    
    
    @SuppressWarnings({ "rawtypes" })
    private List<SocialUserNotification> findAllDigestEmailForAllUsers() throws Exception
    {
        List<SocialUserNotification> allUsersWithDigestEmail = new ArrayList<SocialUserNotification>();
        
        String hql =    "from SocialUserNotification where " +
                        "UNotification like '%DIGEST_EMAIL' " +
                        "and UNotificationValue = 'true' " + //all the ones that are set to 'true'
                        "order by user.sys_id";
        
        try
        {
            List userNotifications = GeneralHibernateUtil.executeHQLSelect(hql, new HashMap<String, Object>());
            if (userNotifications != null && userNotifications.size() > 0)
            {
                for(Object o : userNotifications)
                {
                    SocialUserNotification not = (SocialUserNotification) o;
                    
                    userSysIdsToProcess.add(not.getUser().getSys_id());
                    allUsersWithDigestEmail.add(not);
                }
            }

        }
        catch (Exception e)
        {
            Log.log.error("Error executing the sql:" + hql, e);
            throw new Exception("Error executing the sql:" + hql, e);
        }
        
        return allUsersWithDigestEmail;
        
    }
    
    
    private List<SocialUserNotification> filterNotificationsFor(UserGlobalNotificationContainerType type)
    {
        List<SocialUserNotification> filter = new ArrayList<SocialUserNotification>();
        
        for(SocialUserNotification userNot : allUsersWithDigestEmail)
        {
            if(type.name().equalsIgnoreCase(userNot.getUNotification()))
            {
                filter.add(userNot);
            }
        }
        
        return filter;
    }
    
    private List<SocialUserNotification> filterNotificationForUser(String userSysId)
    {
        List<SocialUserNotification> filter = new ArrayList<SocialUserNotification>();
        
        for(SocialUserNotification userNot : allUsersWithDigestEmail)
        {
            if(userSysId.equalsIgnoreCase(userNot.getUser().getSys_id()))
            {
                filter.add(userNot);
            }
        }
        
        return filter;
    }
    
}
