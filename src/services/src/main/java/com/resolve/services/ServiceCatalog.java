/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.resolve.services.catalog.Catalog;
import com.resolve.services.catalog.CatalogUtil;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.hibernate.vo.CatalogAttachmentVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.services.vo.WikiDocumentDTO;

public class ServiceCatalog
{
    
    /**
     * Persist the catalog 
     * 
     * @param catalog
     * @param username
     * @return
     * @throws Exception
     */
    public static Catalog saveCatalog(Catalog catalog, String username) throws Exception
    {
        return CatalogUtil.save(catalog, username);
    }

    /**
     * Delete a catalog
     * 
     * @param sysId
     * @param name
     * @param username
     * @throws Exception
     */
    public static void deleteCatalog(String sysId, String name, String username) throws Exception
    {
       CatalogUtil.delete(sysId, name, username); 
    }

    /**
     * All the catalogs for the grid
     * 
     * @param query
     * @param username
     * @return
     * @throws Exception
     */
    public static ResponseDTO<Catalog> getCatalogs(QueryDTO query, String username) throws Exception
    {
        return CatalogUtil.getCatalogs(query, username);
    }
    
    /**
     * Get the details of a Catalog.
     * 
     * @param sysId
     * @param catalogName
     * @param username
     * @return
     * @throws Exception
     */
    public static Catalog getCatalog(String sysId, String catalogName, String username) throws Exception
    {
        return CatalogUtil.getCatalog(sysId, catalogName, username);
    }
//    public static Catalog getCatalogByName(String catalogName) throws Exception
//    {
//        return null;
//    }
    
    public static boolean isExistingCatalog(String sysId, String catalogName, String username) throws Exception
    {
        return CatalogUtil.isExistingCatalog(sysId, catalogName, username);
    }
    
    
    /**
     * return only the sysId and displayName, not the entire catalog
     * Uses getCatalogs to get the data
     * 
     * @param query
     * @return
     * @throws Exception
     */
    public static List<Catalog> getAllCatalogs(QueryDTO query, String username) throws Exception
    {
        List<Catalog> result = new ArrayList<Catalog>();
        
        ResponseDTO<Catalog> data = getCatalogs(query, username);
        List<Catalog> catalogResult = data.getRecords();
        
        //do this to reduce the data over wire
        for(Catalog cat : catalogResult)
        {
            Catalog newCat = new Catalog();
            newCat.setId(cat.getId());
            newCat.setName(cat.getName());
            
            result.add(newCat);
        }
        
        return result;
    }
    
    public static ResponseDTO<Catalog> getRefCatalogs(String catalogId, String catalogName, String username) throws Exception
    {
        return CatalogUtil.getRefCatalogs(catalogId, catalogName, username);
    }

    
    

    
    public static void updateCatalogItemPathsToDocument(Document doc, Set<String> catalogPaths, String username) throws Exception
    {
        //TODO:
        throw new Exception("Not implemented yet");
        
    }
    
    public static Set<String> getCatalogNodesForWiki(Document doc) throws Exception
    {
        //TODO:
        throw new Exception("Not implemented yet");
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    //////////////// Catalog Attachments
    ///////////////////////////////////////////////////////////////////////////////
    public static CatalogAttachmentVO saveCatalogAttachment(CatalogAttachmentVO catalogVO, String username) throws Exception
    {
        return CatalogUtil.saveCatalogAttachment(catalogVO, username);
    }

    public static CatalogAttachmentVO downloadCatalogFile(String sysId, String username) throws Exception
    {
        return  CatalogUtil.downloadCatalogFile(sysId, username);
    }
    
    public static ResponseDTO<CatalogAttachmentVO> getCatalogAttachments(QueryDTO query,  String username) throws Exception
    {
        return CatalogUtil.getCatalogAttachments(query, username);
    }
    
    
    /**
     * pathTagName -- like: /TestFen100/New Group/New Item
     * tags -- the list of tag name assign to the catalog
     *         like: network, security, alarm ...
     * 
     * @param pathName : String representing path name.
     * @param tags : List of tags
     * @param username : String represending user who is invoking this API
     * 
     * @return Collection of {@link WikiDocumentDTO}
     */
    public static Collection<WikiDocumentDTO> getWikidocmentOf(String pathName, List<String> tags, String username)  throws Exception
    {
        return CatalogUtil.getWikidocmentOf(pathName, tags, username);
    }
}
