/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.query.Query;

import com.resolve.persistence.dao.WikiDocumentDAO;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikidocAttachmentRel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceGraph;
import com.resolve.services.ServiceResolutionBuilder;
import com.resolve.services.ServiceWiki;
import com.resolve.services.exception.WikiException;
import com.resolve.services.graph.social.model.message.SubmitNotification;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.RBGeneralVO;
import com.resolve.services.rb.util.ResolutionBuilderUtils;
import com.resolve.services.social.notification.NotificationHelper;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;

public class DeleteHelper
{
    private final static int DELETE_BATCH = 500;

    public static void deleteAllWikiDocuments(String username) throws Exception
    {
        List<String> sysIds = new ArrayList<String>(GeneralHibernateUtil.getSysIdsFor("WikiDocument", null, "system"));
        deleteWikiDocuments(new HashSet<String>(sysIds), username);
    }
    
    public static void deleteWikiForNamespaces(Set<String> namespaces, String username)  throws Exception
    {
        if(namespaces != null)
        {
            for(String namespace : namespaces)
            {
                Set<String> sysIds = NamespaceUtil.findAllDocumentsSysIdsForNamespace(namespace, username);
                deleteWikiDocuments(sysIds, username);
                
//                for(String sysId : sysIds)
//                {
//                    try
//                    {
//                        deleteWikiDocument(sysId, null, username);
//                    }
//                    catch (Exception e)
//                    {
//                       Log.log.error("error deleting/purging document with name : " + sysId, e);
//                    }
//                }
            }
            
        }
        
    }
    
    public static void deleteWikiDocuments(Set<String> docSysIds, String username) throws Exception
    {
        if(docSysIds != null && docSysIds.size() > 0)
        {
            //if there is only 1 doc, check the roles 
            if(docSysIds.size() == 1)
            {
                deleteWikiDocument(docSysIds.iterator().next(), null, username);
            }
            else
            {
                Set<String> userRoles = UserUtils.getUserRoles(username);
                if (userRoles.contains("admin"))
                {
                    List<String> listDocId = new ArrayList<String>(docSysIds);

                    if (listDocId != null)
                    {
                        if (listDocId.size() > DELETE_BATCH)
                        {
                            int totalSize = listDocId.size();

                            int toIndex = 0;
                            int fromIndex = 0;
                            List<String> batch500 = null;

                            while (toIndex < totalSize)
                            {
                                toIndex = fromIndex + DELETE_BATCH;
                                if (toIndex > totalSize)
                                {
                                    toIndex = totalSize;
                                }

                                batch500 = listDocId.subList(fromIndex, toIndex);

                                //process it
                                deleteWikiDocUsingSysIds(batch500, username);

                                //set the values
                                fromIndex = toIndex;
                            }//end of while loop
                        }
                        else
                        {
                            deleteWikiDocUsingSysIds(listDocId, username);
                        }

                    }
                }
                else
                {
                    throw new WikiException("User should be an 'admin' to do delete operation.");
                }
            }
            
//            for(String docSysId : docSysIds)
//            {
//                try
//                {
//                    deleteWikiDocument(docSysId, null, username);
//                }
//                catch (Exception e)
//                {
//                   Log.log.error("error deleting/purging document with sysId : " + docSysId, e);
//                }
//            }
        }
    }
    
    
    public static void deleteWikiDocument(String sysId, String docFullName, String username) throws Exception
    {
        if(StringUtils.isNotBlank(docFullName) || StringUtils.isNotBlank(sysId))
        {
            Set<String> userRoles = UserUtils.getUserRoles(username);
            WikiDocument doc = WikiUtils.getWikiDocumentModel(sysId, docFullName, username, false);
            if(doc != null)
            {
                if ((StringUtils.isNotBlank(doc.getUDisplayMode()) &&
                     doc.getUDisplayMode().equalsIgnoreCase(WikiDocument.DISPLAY_MODE_PLAYBOOK)) && doc.ugetUSIRRefCount().longValue() > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    
                    sb.append(doc.getUFullname() + " is ");
                    
                    if (StringUtils.isNotBlank(doc.getUDisplayMode()) &&
                        doc.getUDisplayMode().equalsIgnoreCase(WikiDocument.DISPLAY_MODE_PLAYBOOK))
                    {
                        sb.append(WikiDocument.DISPLAY_MODE_PLAYBOOK);
                        
                        if (doc.ugetUIsTemplate())
                        {
                            sb.append(" template ");
                        }
                    }
                    else if (/*!doc.ugetUIsDeletable()*/ doc.ugetUSIRRefCount().longValue() > 0)
                    {
                        sb.append(" referenced in playbook(s) ");
                    }
                    
                    sb.append(" and hence cannot be deleted.");
                    
                    Log.log.info(sb.toString());
                    throw new Exception(sb.toString());
                }
                
                String docAdminRoles = doc.getAccessRights().getUAdminAccess();
                boolean hasRights = com.resolve.services.util.UserUtils.hasRole(username, userRoles, docAdminRoles);
                if(hasRights)
                {
                    List<String> listDocId = new ArrayList<String>();
                    if (StringUtils.isNotBlank(sysId))
                    {
                        listDocId.add(sysId);
                    }
                    else if (StringUtils.isNotBlank(docFullName))
                    {
                        String id = WikiUtils.getWikidocSysId(docFullName);
                        listDocId.add(id);
                    }
                    
                    //check if this document is a DT, and add the sysIds for those docs to be deleted
                    if(doc.ugetUIsRoot())
                    {
                        //there is a possibility that it has a '_Decision' document. There is only 1 document of that type
                        String docName =  doc.getUFullname() + "_Decision";
                        WikiDocument docDecision = WikiUtils.getWikiDocumentModel(null, docName, username, false);
                        if(docDecision != null)
                        {
                            listDocId.add(docDecision.getSys_id());
                        }
                    }
                    
                    if (listDocId.size() > 0)
                    {
                        deleteWikiDocUsingSysIds(listDocId, username);
                    }
                }
                else
                {
                    Log.log.error("User " + username + " does not have 'admin' role on the document " + doc.getUFullname() + " to Delete it.");
                }
            }
        }
    }
    
    
    
    
    //private apis
    
    /**
     * deletes the list of ids in sql format
     * eg. 'qweqweqa223', 'asdadasd323', 'sfdrte54323', 'asrtwsertwert343' 
     * 
     * This operation will always be done by an 'admin' role user. 
     * 
     * @param queryStrOfids
     * @throws WikiException
     */
    private static void delete(Collection<String> listDocId, String queryStrOfids, String username) throws Exception
    {

        //list of index logs to remove the indexing
        List<String> listWikiDocuments = new ArrayList<String>();
        List<SubmitNotification> notifications = new ArrayList<SubmitNotification>();
        List<WikiDocument> listWikiDocs = new ArrayList<>();

        WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();

        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
            	Set<String> idsToDeleteContent = new HashSet<String>();
            	String docFullName = null;

	            if (StringUtils.isNotEmpty(queryStrOfids))
	            {
	                //prepare the notification list
	                for (String docId : listDocId)
	                {
	                    WikiDocument doc = dao.findById(docId);
	                    if (doc != null)
	                    {
	                        if ((StringUtils.isNotBlank(doc.getUDisplayMode()) &&
	                             doc.getUDisplayMode().equalsIgnoreCase(WikiDocument.DISPLAY_MODE_PLAYBOOK)) && doc.ugetUSIRRefCount().longValue() > 0)
	                        {
	                            throw new Exception("Document with name " + doc.getUFullname() + 
	                                                " is either a playbook (template or associated with SIR) or is document referenced in playbook activity which cannot be deleted.");
	                        }
	                        
	                        docFullName = doc.getUFullname();
	
	                        try
	                        {
	                            //if there is resolution builder for this wiki we need to delete it here
	                            if(StringUtils.isNotBlank(doc.getUResolutionBuilderId()))
	                            {
	                                ServiceResolutionBuilder.deleteGeneral(doc.getUResolutionBuilderId(), false, username);
	                            }
	                            else // Check for bad data (missing rb_general fk in doc) and if found delete it
	                            {
	                                List<RBGeneralVO> rbGeneralVOs = ResolutionBuilderUtils.findRBGeneralByUniqueKey(doc.getUNamespace(), doc.getUName(), username);
	                                
	                                if(rbGeneralVOs != null && !rbGeneralVOs.isEmpty())
	                                {
	                                    ServiceResolutionBuilder.deleteGeneral(rbGeneralVOs.iterator().next().getSys_id(), false, username);
	                                }
	                            }
	                            notifications.add(NotificationHelper.getDocumentNotification(doc, UserGlobalNotificationContainerType.DOCUMENT_PURGED, username, false, true));
	                        }
	                        catch (Throwable t)
	                        {
	                            Log.log.error(t.getStackTrace());
	                        }
	                        
	                        listWikiDocuments.add(docId);
	                        listWikiDocs.add(doc);
	                    }
	                }
	                
	                if (listWikiDocuments.size() > 0)
	                {
	                    //delete the graph nodes
	                    for(String sysId : listWikiDocuments)
	                    {
	                        try
	                        {
	                            ServiceGraph.deleteNode(null, sysId, null, NodeType.DOCUMENT, username);
	                        }
	                        catch (Exception e)
	                        {
	                            Log.log.error("error deleting document node with sysId : " + sysId, e);
	                        }
	                        idsToDeleteContent = getListToDeleteContent("'" + sysId + "'");
	                        if (!idsToDeleteContent.isEmpty())
	                        {
	                            //remove attachment indices
	                            ServiceWiki.purgeWikiAttachmentIndexes(idsToDeleteContent, sysId, username);
	                        }
	                    }
	                }
	                
	                String queryStrOfidsToDelete = SQLUtils.prepareQueryString(listWikiDocuments);
	                
	                //IF THERE IS A BUG, CHECK THE OLDER VERSION ALSO FROM SVN
	                deleteDependenciesOf(queryStrOfidsToDelete, idsToDeleteContent);
	                HibernateUtil.deleteQuery("AccessRights", "UResourceId", listWikiDocuments);
	                
	                for (WikiDocument doc : listWikiDocs) {
	                	dao.delete(doc);
	                }
	            }

	            //Finally submit all the notifications we have gathered.
	            NotificationHelper.submitNotifications(notifications, true);
	            
	            //remove the lock from the lock utils
	            if(StringUtils.isNotBlank(docFullName))
	            	DocUtils.unlock(docFullName);
        	});

            
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        //remove wiki document incides
        ServiceWiki.purgeWikiDocumentIndexes(new HashSet<String>(listDocId), username);
    }//delete
    

    /**
     * deletes only the dependencies of all the doc ids passed
     * 
     * IF THERE IS A BUG, CHECK THE OLDER VERSION ALSO FROM SVN
     * 
     * @param wikiDocids
     * @param idsToDeleteContent
     * @throws Exception 
     */
    private static void deleteDependenciesOf(String wikiDocids, Set<String> idsToDeleteContent) throws Exception
    {

        if (StringUtils.isNotEmpty(wikiDocids))
        {
            Collection<String> wikiDocIdCollection = SQLUtils.convertQueryStringToSet(wikiDocids);
            //HibernateUtil.deleteQuery("ContentWikidocWikidocRel", "contentWikidoc IN (" + wikiDocids + ")");
            HibernateUtil.deleteQuery("ContentWikidocWikidocRel", "contentWikidoc", wikiDocIdCollection);
            
            //HibernateUtil.deleteQuery("MainWikidocWikidocRel", "mainWikidoc IN (" + wikiDocids + ")");
            HibernateUtil.deleteQuery("MainWikidocWikidocRel", "mainWikidoc", wikiDocIdCollection);
            
            //HibernateUtil.deleteQuery("ExceptionWikidocWikidocRel", "exceptionWikidoc IN (" + wikiDocids + ")");
            HibernateUtil.deleteQuery("ExceptionWikidocWikidocRel", "exceptionWikidoc", wikiDocIdCollection);
            
            //HibernateUtil.deleteQuery("FinalWikidocWikidocRel", "finalWikidoc IN (" + wikiDocids + ")");
            HibernateUtil.deleteQuery("FinalWikidocWikidocRel", "finalWikidoc", wikiDocIdCollection);
            
            //HibernateUtil.deleteQuery("ContentWikidocActiontaskRel", "contentWikidoc IN (" + wikiDocids + ")");
            HibernateUtil.deleteQuery("ContentWikidocActiontaskRel", "contentWikidoc", wikiDocIdCollection);
            
            //HibernateUtil.deleteQuery("MainWikidocActiontaskRel", "mainWikidoc IN (" + wikiDocids + ")");
            HibernateUtil.deleteQuery("MainWikidocActiontaskRel", "mainWikidoc", wikiDocIdCollection);
            
            //HibernateUtil.deleteQuery("ExceptionWikidocActiontaskRel", "exceptionWikidoc IN (" + wikiDocids + ")");
            HibernateUtil.deleteQuery("ExceptionWikidocActiontaskRel", "exceptionWikidoc", wikiDocIdCollection);
            
            //HibernateUtil.deleteQuery("FinalWikidocActiontaskRel", "finalWikidoc IN (" + wikiDocids + ")");
            HibernateUtil.deleteQuery("FinalWikidocActiontaskRel", "finalWikidoc", wikiDocIdCollection);
            
            //            HibernateUtil.deleteQuery("AccessRights", "UResourceId IN (" + wikiDocids + ")");//moving the delete of access rights with the doc itself
            //HibernateUtil.deleteQuery("WikiArchive", "UTableId IN (" + wikiDocids + ")");
            HibernateUtil.deleteQuery("WikiArchive", "UTableId", wikiDocIdCollection);
            
            //HibernateUtil.deleteQuery("WikidocResolveTagRel", "wikidoc IN (" + wikiDocids + ")");
            HibernateUtil.deleteQuery("WikidocResolveTagRel", "wikidoc", wikiDocIdCollection);
            
            //HibernateUtil.deleteQuery("WikidocStatistics", "wikidoc IN (" + wikiDocids + ")");
            HibernateUtil.deleteQuery("WikidocStatistics", "wikidoc", wikiDocIdCollection);
            
            //HibernateUtil.deleteQuery("RoleWikidocHomepageRel", "wikidoc IN (" + wikiDocids + ")");
            HibernateUtil.deleteQuery("RoleWikidocHomepageRel", "wikidoc", wikiDocIdCollection);
            
            //HibernateUtil.deleteQuery("WikidocResolutionRating", "wikidoc IN (" + wikiDocids + ")");
            HibernateUtil.deleteQuery("WikidocResolutionRating", "wikidoc", wikiDocIdCollection);
            
            //HibernateUtil.deleteQuery("WikidocQualityRating", "wikidoc IN (" + wikiDocids + ")");
            HibernateUtil.deleteQuery("WikidocQualityRating", "wikidoc", wikiDocIdCollection);
            
            DecisionTreeHelper.removeDecisionTrees(wikiDocids);
            //HibernateUtil.deleteQuery("DTWikidocWikidocRel", "dtWikidoc IN (" + wikiDocids + ")");
            HibernateUtil.deleteQuery("DTWikidocWikidocRel", "dtWikidoc", wikiDocIdCollection);
        }

        //delete the attachment content
        deleteWikiAttachmentRecords(idsToDeleteContent);

        if (StringUtils.isNotEmpty(wikiDocids))
        {
            //HibernateUtil.deleteQuery("WikidocAttachmentRel", "parentWikidoc in (" + wikiDocids + ")");
            Collection<String> wikiDocIdCollection = SQLUtils.convertQueryStringToSet(wikiDocids);
            HibernateUtil.deleteQuery("WikidocAttachmentRel", "parentWikidoc", wikiDocIdCollection);
        }  
    }//deleteDependenciesOf
    
    @SuppressWarnings("unchecked")
    private static Set<String> getListToDeleteContent(String UParentWikidocSys_Id)
    {
        Set<String> ids = new HashSet<String>();
        String sql = " from WikidocAttachmentRel where parentWikidoc in (" + UParentWikidocSys_Id.trim() + ") ";

        List<WikidocAttachmentRel> list = null;

        try
        {
            list = (List<WikidocAttachmentRel>) HibernateProxy.execute(() -> {
            	Query q = HibernateUtil.createQuery(sql);
                return q.list();
            });            
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        //prepare the qry string 
        if (list != null && list.size() > 0)
        {
            for (WikidocAttachmentRel rel : list)
            {
                if (rel != null && rel.getWikiAttachment() != null)
                {
                    ids.add(rel.getWikiAttachment().getSys_id());
                }
            }
        }

        return ids;
    }//getListToDeleteContent

    private static void deleteWikiDocUsingSysIds(List<String> listDocId, String username) throws Exception
    {
        String queryStrOfids = SQLUtils.prepareQueryString(listDocId);
        //        Log.log.debug("Deleting sysIds: " + queryStrOfids);

        //delete 
        delete(listDocId, queryStrOfids, username);
    }
    
    private static void deleteWikiAttachmentRecords(Set<String> idsToDeleteContent) throws Exception
    {
        if(idsToDeleteContent != null && idsToDeleteContent.size() > 0)
        {
            /*
            //batch of 200 sysIds to take care of the limitation of the IN clause
            List<List<String>> batchSysIds = JavaUtils.chopped(new ArrayList<String>(idsToDeleteContent), 200);

            for(List<String> batchSysId : batchSysIds)
            {
                String sqlSysIds = SQLUtils.prepareQueryString(batchSysId);
                
                HibernateUtil.deleteQuery("WikiAttachmentContent", "wikiAttachment in (" + sqlSysIds + ")");
                HibernateUtil.deleteQuery("WikiAttachment", "sys_id in (" + sqlSysIds + ")");
            }
            */
            /*
             * Above logic is moved to a commpon place in HibernateUtil.
             */
            HibernateUtil.delete("WikiAttachmentContent", new ArrayList<String>(idsToDeleteContent));
            HibernateUtil.delete("WikiAttachment", new ArrayList<String>(idsToDeleteContent));
        }
    }
    

}
