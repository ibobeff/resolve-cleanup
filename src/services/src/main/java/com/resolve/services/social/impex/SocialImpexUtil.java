/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.social.impex;

import com.resolve.services.social.impex.export.ExportGraph;
import com.resolve.services.social.impex.immport.ImportGraph;


/**
 * impex for the social 
 * 
 * @author jeet.marwah
 *
 */
public class SocialImpexUtil
{
    public static void importGraph(SocialImpexVO socialImpex) throws Throwable
    {
        ImportGraph immport = new ImportGraph(socialImpex.getFolderLocation(), "system");
        immport.immport();
    }
    
    public static void exportGraph(SocialImpexVO socialImpex) throws Throwable
    {
        ExportGraph export = new ExportGraph(socialImpex, "system");
        export.export();
    }
}
