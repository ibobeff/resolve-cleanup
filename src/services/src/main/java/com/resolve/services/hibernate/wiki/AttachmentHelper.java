/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.resolve.persistence.dao.WikiAttachmentContentDAO;
import com.resolve.persistence.dao.WikiAttachmentDAO;
import com.resolve.persistence.dao.WikidocAttachmentRelDAO;
import com.resolve.persistence.model.WikiAttachment;
import com.resolve.persistence.model.WikiAttachmentContent;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikidocAttachmentRel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceWiki;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.util.WikiAttachmentUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.WikiAttachmentVO;
import com.resolve.services.hibernate.vo.WikidocAttachmentRelVO;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.ERR;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class AttachmentHelper
{
    private String docSysId = null;
    private String docFullName = null;
    private String username = null;
    private Collection<WikidocAttachmentRelVO> attachmentsRelsToAdd = null;
    private Collection<WikiAttachment> attachmentsToAdd = null;
    private Collection<WikiAttachmentVO> attachmentsVOsToAdd = null;
    
    private Collection<WikiAttachmentVO> attachmentsAdded = new HashSet<WikiAttachmentVO>();
    
    //switches
    private boolean impex = false;
    
    private WikiDocument doc = null;
    
    public AttachmentHelper(String docSysId, String docFullName, Collection<WikidocAttachmentRelVO> attachmentsRelsToAdd, Collection<WikiAttachment> attachmentsToAdd, String username)
    {
        if(StringUtils.isEmpty(docSysId) && StringUtils.isEmpty(docFullName))
        {
            throw new RuntimeException("Doc sysId or fullname is required.");
        }
        
        if(StringUtils.isEmpty(username))
        {
            throw new RuntimeException("Username is mandatory");
        }
        
        this.docSysId = docSysId;
        this.docFullName = docFullName;
        this.attachmentsToAdd = attachmentsToAdd;
        this.attachmentsRelsToAdd = attachmentsRelsToAdd;
        this.username = username;
    }
    
    public AttachmentHelper(String docSysId, String docFullName, Collection<WikiAttachmentVO> attachmentsVOsToAdd, String username)
    {
        if(StringUtils.isEmpty(docSysId) && StringUtils.isEmpty(docFullName))
        {
            throw new RuntimeException("Doc sysId or fullname is required.");
        }
        
        if(StringUtils.isEmpty(username))
        {
            throw new RuntimeException("Username is mandatory");
        }
        
        this.docSysId = docSysId;
        this.docFullName = docFullName;
        this.username = username;
        this.attachmentsVOsToAdd = attachmentsVOsToAdd;
    }
    
    public boolean isImpex()
    {
        return impex;
    }

    public void setImpex(boolean impex)
    {
        this.impex = impex;
    }

    //return collection of attachment sysIds
    public Collection<WikiAttachmentVO> attach(boolean eager) throws Exception
    {
        doc = WikiUtils.validateUserForDocument(docSysId, docFullName, RightTypeEnum.edit, username);
        
        if (attachmentsRelsToAdd != null)
        {
            for (WikidocAttachmentRelVO rel : attachmentsRelsToAdd)
            {
                addAttachment(rel);
            }
        }
        else if (attachmentsToAdd != null)
        {
            for (WikiAttachment attachment : attachmentsToAdd)
            {
                boolean global = false;
                
                Collection<WikidocAttachmentRel> list;
                if(eager)
                    list = getWikidocAttachmentRelsEager(docSysId, attachment);
                else {
                    Collection<WikidocAttachmentRel> collection = attachment.getWikidocAttachmentRels();
                    list = (Collection<WikidocAttachmentRel>)collection;
                }
                
                if (list != null)
                {
                    for (WikidocAttachmentRel attachmentRel : list)
                    {
                        if (attachmentRel.getWikiAttachment().getUFilename().equals(attachment.getUFilename()))
                        {
                            String sysId = attachmentRel.getWikidoc().getSys_id();
                            String parentSysId = attachmentRel.getParentWikidoc().getSys_id();
                            if (sysId != null && !sysId.equals(parentSysId))
                            {
                                global = true;
                                break;
                            }
                        }
                    }
                }
                addAttachment(attachment, global);
                attachmentsAdded.add(attachment.doGetVO());
            }
        }
        else if(attachmentsVOsToAdd != null)
        {
            for (WikiAttachmentVO attachmentVO : attachmentsVOsToAdd)
            {
                WikiAttachment attachment = new WikiAttachment(attachmentVO);
                addAttachment(attachment, false);
//                attachmentVO = attachment.doGetVO();
                attachmentsAdded.add(attachment.doGetVO());
            }
        }
        
        return attachmentsAdded;
    }
    
    public Collection<WikiAttachmentVO> attach() throws Exception {
        
        return attach(false);
    }
    
    private void addAttachment(WikidocAttachmentRelVO rel) throws Exception
    {
        if(rel != null)
        {
            WikiAttachmentVO attachmentVO = rel.getWikiAttachment();
            if(attachmentVO != null && attachmentVO.ugetWikiAttachmentContent() != null)
            {
                WikiAttachment attachment = new WikiAttachment(attachmentVO);
                addAttachment(attachment, rel.isGlobal());
                attachmentsAdded.add(attachment.doGetVO());
            }
        }
    }
    
    private void addAttachment(WikiAttachment attachment, boolean global) throws Exception
    {
        if(attachment != null && attachment.ugetWikiAttachmentContent() != null)
        {
            WikiAttachment dbAttachment = WikiAttachmentUtil.findWikiAttachmentFor(doc.getUFullname(), null, attachment.getUFilename(), true);
            if (dbAttachment != null)//UPDATE
            {
                dbAttachment.setUSize(attachment.getUSize());

                // if attachment with the same name exist, just update it.
                WikiAttachmentContent wac = dbAttachment.ugetWikiAttachmentContent();
                wac.setUContent(attachment.ugetWikiAttachmentContent().getUContent());

                dbAttachment.usetWikiAttachmentContent(wac);

                updateAttachment(dbAttachment, global);
            }
            else
            {
                //insert
                insertAttachment(attachment, global);
            }
            
            //set the content to null else it will be with the VO also
            attachment.usetWikiAttachmentContent(null);
        }
    }

    
    private void updateAttachment(WikiAttachment attachment, boolean global)
    {
        WikiAttachmentContent wac = attachment.ugetWikiAttachmentContent();
        if (wac != null)
        {            
            try
            {
              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
                WikiAttachmentContentDAO daoWikiAttachmentContent = HibernateUtil.getDAOFactory().getWikiAttachmentContentDAO();
                WikiAttachmentDAO daoWikiAttachment = HibernateUtil.getDAOFactory().getWikiAttachmentDAO();

                //delete the existing attachment content if it exist
                //                String sql = "delete from WikiAttachmentContent where wikiAttachment = '" + attachment.getsys_id() + "'";
                //                Query query = HibernateUtil.createQuery(sql);
                //                query.executeUpdate();

                //add the new content                
                wac.setWikiAttachment(attachment);
                daoWikiAttachment.persist(attachment);
                daoWikiAttachmentContent.persist(wac);
                
                if (global)
                {
                    WikidocAttachmentRelDAO globalDaoRel = HibernateUtil.getDAOFactory().getWikidocAttachmentRelDAO();
                    Collection<WikidocAttachmentRel> collection = attachment.getWikidocAttachmentRels();
                    if (collection != null)
                    {
                        for (WikidocAttachmentRel attachmentRel : collection)
                        {
                            if (attachmentRel.getWikiAttachment().getUFilename().equals(attachment.getUFilename()))
                            {
                                WikiDocument globalDoc = WikiUtils.getWikiDocumentModel(null, "System.Attachment", username, false);
                                attachmentRel.setParentWikidoc(globalDoc);
                                
                                globalDaoRel.persist(attachmentRel);
                                break;
                            }
                        }
                    }
                }

            	});

            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
            finally
            {
                //to avoid memory leaks and release memory
                attachment.usetWikiAttachmentContent(null);
            }
        }
    } // updateAttachment
    
    private void insertAttachment(WikiAttachment attachment, boolean isGlobal) throws WikiException
    {

        try
        {

          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
                WikiAttachmentDAO daoWikiAttachment = HibernateUtil.getDAOFactory().getWikiAttachmentDAO();
                WikiAttachmentContentDAO daoWikiAttachmentContent = HibernateUtil.getDAOFactory().getWikiAttachmentContentDAO();
                WikidocAttachmentRelDAO daoREL = HibernateUtil.getDAOFactory().getWikidocAttachmentRelDAO();

                WikiAttachmentContent wac = attachment.ugetWikiAttachmentContent();
                wac.setWikiAttachment(attachment);

                WikidocAttachmentRel rel = new WikidocAttachmentRel();
                rel.setWikidoc(doc);
                
                if(isGlobal)
                {
                    WikiDocument globalDoc = WikiUtils.getWikiDocumentModel(null, "System.Attachment", username, false);
                    rel.setParentWikidoc(globalDoc);
                    
                    if (WikiAttachmentUtil.findWikiAttachmentFor("System.Attachment", null, attachment.getUFilename(), false) == null)
                    {
                        WikidocAttachmentRelDAO globalDaoRel = HibernateUtil.getDAOFactory().getWikidocAttachmentRelDAO();
                        WikidocAttachmentRel globalRel = new WikidocAttachmentRel();

                        globalRel.setParentWikidoc(globalDoc);
                        globalRel.setWikidoc(globalDoc);
                        globalRel.setWikiAttachment(attachment);
                        
                        globalDaoRel.persist(globalRel);
                    }
                }
                else
                {
                    rel.setParentWikidoc(doc);
                }
                rel.setWikiAttachment(attachment);

                daoWikiAttachment.persist(attachment);
                daoWikiAttachmentContent.persist(wac);
                daoREL.persist(rel);

                
        	});
        	
            //log it in the index table, not if it is from Importing
            if (!isImpex())
            {
                indexAttachment(doc.getSys_id(), attachment.getSys_id());
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new WikiException("Failed to upload an attachment.");
        }
        finally
        {
            //to avoid memory leaks and release memory
            attachment.usetWikiAttachmentContent(null);
        }

    }//addAttachment

    private void indexAttachment(String wikidocId, String attachmentId)
    {
        //index the attachment
        Set<String> attachSysIds = new HashSet<String>();
        attachSysIds.add(attachmentId);
        
        try
        {
            ServiceWiki.indexWikiAttachments(wikidocId, null, attachSysIds, username);
        }
        catch (Exception e)
        {
            Log.log.error("error indexing the attachment with Id : " + attachmentId, e);
        }
    }
    
    private List<WikidocAttachmentRel> getWikidocAttachmentRelsEager(String docSysId, WikiAttachment attachment)
    {
        List<WikidocAttachmentRel> wikiAttachmentRels = new ArrayList<WikidocAttachmentRel>();
        
        SQLConnection conn = null;
        ResultSet rs = null;
        
        try
        {
            conn = SQL.getConnection();
                
            String sql = "SELECT wikiattachment.u_filename, wikidoc_attachment_rel.u_wikidoc_sys_id, wikidoc_attachment_rel.u_parent_wikidoc_sys_id " + 
                         "FROM wikidoc_attachment_rel, wikiattachment " + 
                         "WHERE wikidoc_attachment_rel.u_attachment_sys_id = wikiattachment.sys_id AND wikiattachment.sys_id=?";
            Log.log.trace(sql);
            
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, attachment.getSys_id());
            
            rs = ps.executeQuery();

            while(rs.next()) {                
                String fileName = rs.getString(1);
                String wikiDocId = rs.getString(2);
                if(wikiDocId == null)
                    continue;
                String wikiParentDocId = rs.getString(3);
                
                WikidocAttachmentRel rel = new WikidocAttachmentRel();
                
                WikiAttachment wikiAttachment = new WikiAttachment();
                wikiAttachment.setUFilename(fileName);
                
                WikiDocument wikiDoc = new WikiDocument();
                doc.setSys_id(wikiDocId);
                
                WikiDocument parent = new WikiDocument();
                parent.setSys_id(wikiParentDocId);
                
                rel.setWikiAttachment(wikiAttachment);
                rel.setWikidoc(wikiDoc);
                rel.setParentWikidoc(parent);
                
                wikiAttachmentRels.add(rel);
            }
        }
        catch (Throwable t)
        {
            Log.alert(ERR.E20005, t);
        }
        finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }
        
        return wikiAttachmentRels;
    }

} // AttachmentHelper
