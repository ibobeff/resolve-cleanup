/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public interface ConstantValues
{
	public DateFormat fullDateFormatter = new SimpleDateFormat("MMM dd yyyy, HH:mm");
	public DateFormat ddMMyyyy_Format = new SimpleDateFormat("dd/MM/yyyy");
	public DateFormat ddMMyyyyHHmm_Format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	
	public DateFormat dateWithoutYear = new SimpleDateFormat("MMM dd, HH:mm");
	public DateFormat dateWithoutTime = new SimpleDateFormat("MMM dd yyyy");
	
	//used for saving the objects in the HashMap
	public String WIKI_DOCUMENT_NAME = "wikiDocument";
	public String WIKI_DOCUMENT_SYS_ID = "wikiDocumentSysId";
	public String WIKI_DOCUMENT_SYS_IDS = "wikiDocumentSysIds";
	public String WIKI_DOCUMENT_CONTENT = "wikiDocumentContent";
	public String WIKI_DOCUMENT_MODEL = "wikiDocumentModel";
	public String WIKI_DOCUMENT_EXCEPTION = "wikiDocumentException";
	public String WIKI_DOCUMENT_DT = "wikiDocumentDT";
	
//	public String WIKI_DOCUMENTS = "wikiDocuments";
	public String WIKI_REQUEST_CONTEXT = "wikiRequestContext";
	public String WIKI_ADMIN = "admin";
	public String WIKI_RESOLVE_MAINT = "resolve.maint";
	public String IMPORT_EXPORT_MODULE_NAME = "module";
	public String IMPORT_EXPORT_FILE_UPLOAD = "impexFile";
    public String IMPORT_EXPORT_MODULE_URL = "moduleUrl";
	public String IMPORT_DEFAULTACTION_OVERWRITE = "0";
	public String IMPORT_DEFAULTACTION_SKIP = "1";
	public String UPDATE_ATTACHMENT = "updateAttachment";
	public String LOCATION = "location";
	public String IMPEX_GENERICCONTAINERMODEL = "impexGenericContainerModel";	
	
	public String SOCIAL_OLD_TYPE = "oldTypeDocOrRunbook";
	public String SOCIAL_RS_COMPONENT = "newRSComponent";
	public String SOCIAL_DELETE_COMPONENT = "deleteComponent";
	public String SOCIAL_DELETE_LIST_OF_DOCSYSID = "deleteListOfDocSysId";
	public String SOCIAL_DELETE_LIST_OF_ATSYSID = "deleteListOfAtSysId";
	public String SOCIAL_DELETE_LIST_OF_USERS_SYSID = "deleteListOfUsersSysId";
    public String SOCIAL_NOTIFICATION_LIST = "socialNotificationList";
		
	
    public String DELETE_INDEX = "DELETE_INDEX";
    public String WIKIDOCUMENT_SYS_ID = "WIKIDOCUMENT_SYS_ID";
    public String WIKIDOCUMENT_SYS_IDS = "WIKIDOCUMENT_SYS_IDS";
    public String INDEX_WIKIDOCUMENT_SYS_ID = "INDEX_WIKIDOCUMENT_SYS_ID";
    public String INDEX_WIKIDOCUMENTS_SYS_ID = "INDEX_WIKIDOCUMENTS_SYS_ID";
    public String ATTACHMENT_SYS_IDS = "ATTACHMENT_SYS_IDS";
    public String ATTACHMENT_SYS_ID = "ATTACHMENT_SYS_ID";
    public String WD_ATTACHMENT_SYS_ID = "WD_ATTACHMENT_SYS_ID";
    public String OBJECT_PERSISTED = "OBJECT_PERSISTED";
    public String OBJECT_PREVIOUS_VERSION = "OBJECT_PREVIOUS_VERSION";
    public String MODEL_CLASS_NAME = "MODEL_CLASS_NAME";
    public String LIST_OF_SYS_IDS = "LIST_OF_SYS_IDS";
    public String LIST_OF_ACTIONTASK = "LIST_OF_ACTIONTASK";
    public String MAP_OF_ACTIONTASK = "MAP_OF_ACTIONTASK";
        
	
	public String NAMESPACE_SYSTEM = "System";
	public String DOCUMENT_PAGEINFO = "PageInfo";
	public String DOCUMENT_EXECUTE = "Execute";
	public String DOCUMENT_ACTIONFORM = "ActionForm";
	public String DOCUMENT_ATTACHMENT = "Attachment";
	
	
	//use for model 
	public String WIKI_XML = "modelXML";
	public String POPUP_MENU = "popupMenu";
	public String MODEL_TITLE = "modelTitle";
	public String IMAGES = "images";
	public String CONTENT = "CONTENT";
	public String MODELONLY = "modelonly";

	//KEY constants - in the key-values pair
	public String WIKI_ACTION_KEY = "action";
	public String ESB_UPDATE_KEY = "ESB_UPDATE";
	public String RENDER_WIKI_CONTENT_ONLY = "RENDER_WIKI_CONTENT_ONLY";
	public String RENDER_DT_QA_AS_DATA = "renderDtQaAsData";
    public String IS_WYSIWYG_EDIT = "isWYSIWYG";
    public String LIST_OF_SECTIONS_FROM_UI = "listOfSectionsFromUI";
    public String SELECTED_SECTION_TO_EDIT = "selectedSectionToEdit";
    public String WIKI_NAMESPACE_KEY = "namespace";
	public String WIKI_DOCNAME_KEY = "docname";
	public String WIKI_URL_PARAMS = "urlParams";
	public String WIKI_SECTION_KEY = "sectionname";
	public String WIKI_TEMPLATE_NAMESPACE_KEY = "template_namespace";
	public String WIKI_TEMPLATE_DOCNAME_KEY = "template_docname";
	public String WIKI_TEMPLATE_VALUES_KEY = "template_values";
	public String WIKI_DOC_FULLNAME_KEY = "fullname";
	public String WIKI_DOC_TITLE_KEY = "title";
	public String WIKI_DOC_SYS_ID_KEY = "docsys_id";
	public String WIKI_CONTENT_KEY = "content";//***should be same as WikiGUIConstants.WIKI_CONTENT_KEY
	public String WYSIWYG_DATA_KEY = "WYSIWYGDataXML";
	public String WIKI_HMVALUES_KEY = "hmValues";//hashmap values key
	public String WIKI_TAGS_KEY = "tags";
	public String WIKI_ATTACH_TYPE_KEY = "type";
	public String WIKI_ATTACHMENTS_KEY = "attachments";
	public String WIKI_FILENAME_KEY = "filename";
	public String FILENAME_KEY = "filename";//used to download a generic file
	public String WIKI_SUMMARY_KEY = "summary";
	public String WIKI_TITLE_KEY = "title";
	public String WIKI_WYSIWYG_KEY = "wysiwyg";
	public String WIKI_CREATEDBY_KEY = "createdBy";
	public String WIKI_ATTACHFILE_LINK_KEY = "attachFileUrl";
	public String WIKI_DELETEFILE_LINK_KEY = "deleteFileUrl";
	public String WIKI_DOWNLOADFILE_LINK_KEY = "downloadFileUrl";
	public String WIKI_EDIT_TYPE_KEY = "wikiEditType";
	public String WIKI_SAVE_LINK_KEY = "saveUrl";
	public String WIKI_SAVEANDEXIT_LINK_KEY = "saveAndExitUrl";
	public String WIKI_FORWARD_URL_KEY = "forwardUrl";
	public String WIKI_PAGEINFO_URL_KEY = "pageInfoUrl";
	public String WIKI_IS_DELETED = "isDeleted";
	public String WIKI_IS_LOCKED = "isLocked";
	public String WIKI_IS_HIDDEN = "isHidden";
	public String WIKI_USERAUTH_KEY = "userAuth";
	public String WIKI_DOCLIST_KEY = "listdoc";
//	public String WIKI_UI_CONTROL_ID_KEY = "controlId";//***should be same as WikiGUIConstants.WIKI_UI_CONTROL_ID_KEY
	public String WIKI_HTTP_REQUEST = "request";
	public String WIKI_HTTP_RESPONSE = "response";
	public String WIKI_CONTEXT = "wikiContext";
	public String WIKI_RPC_PAYLOAD = "payload";
	public String WIKI_REQUEST_TYPE = "wikiRequestType";
	public String WIKI_ALL_NAMESPACES_KEY = "allNamespaces";
	public String SEARCH_PARAM_KEY = "searchParameter";
    public String WHERE_CLAUSE_KEY = "whereClauseKey";
	public String LIST_WIKIACTIONS_KEY = "listOfWikiActions";
	public String MAIN_WIKI_DOC_TO_VIEW_KEY = "mainWikiDocToView";
	public String REVISION_NO_KEY = "revisionNo";
	public String WIKI_NAME_KEY = "wiki";
	public String WIKI_LOOKUP_KEY = "lookup";
	public String WIKI_DOC_KEY = "doc";
	public String WIKI_CURRDOC_KEY = "currdoc";
	public String WIKI_USER_KEY = "wikiUser";
	public String REVISION_MODEL_KEY = "revisionModel";
	public String COMPARE_REVISION1_MODEL_KEY = "revisionModel1";
	public String COMPARE_REVISION2_MODEL_KEY = "revisionModel2";
	public String GOTO_WIKIDOCUMENT_SEARCH_STRING = "gotoWikiDocumentSearchString"; 
	public String WIKIDOC_DOES_NOT_EXISTS = "wikiDocDoesNotExist";
	public String SHOW_TOC = "showTOC";
	public String MODEL_XML_KEY = "modelXml";
	public String WIKIDOC_STATS_COUNTER_NAME_KEY = "WikiDocStatsCounterName";
	public String USERNAME = "USERNAME";
	public String WIKI_ADMIN_KEY = "wikiAdmin";
	public String WIKI_GENERIC_OBJECT_KEY = "wikigenericObjectKey";
	public String WIKI_ADVANCE_SEARCH_CRITERIA_KEY = "wikiAdvanceSearchCriteria";
	public String WIKI_HOME_PAGE_KEY = "wikiHomePage";
	public String WIKI_ADD_NEW_PAGE_KEY = "addNewPageKey";
    public String WIKI_IS_ADMIN_KEY = "isAdminUser";
    public String REVIEWED_CHECKBOX_VALUE = "reviewed_checkbox_value";
	
	
	public String GLOBAL_ATTACH_LIST_KEY = "globalAttachList";
	
	public String SEARCH_NO_OF_RECORDS = "searchNoOfRecords";
	public String SEARCH_SORT_DESCENDING = "searchSortDescending";
	public String SEARCH_FROM_DATE = "searchFromDate";
	public String SEARCH_TO_DATE = "searchToDate";
	public String SEARCH_BASED_ON_TAGS = "tagsNameToSearch";
	public String BROWSER_CLIENT_TIMEZONE = "browserTimeZone";
	public String WIKI_GLOBAL_FLAG = "wikiGlobalFlag";
	
	public String WIKIDOCS_DATE = "wikidocsDate";
    
    public String WIKIDOCS_KEY = "wikidocsKey";
	
	public String WIKIDOCS_NEW_NAMESPACE_KEY = "wikidocsNewNamespace";
	public String WIKIDOCS_NEW_FILENAME_KEY = "wikidocsNewFilename";
	public String WIKIDOCS_TO_MOVE_KEY = "wikidocsToMove";
	public String WIKIDOCS_TO_COPY_KEY = "wikidocsToCopy";
	public String WIKIDOCS_TO_DELETE_KEY = "wikidocsToDelete";
	public String WIKIDOCS_TO_UNDELETE_KEY = "wikidocsToUnDelete";
    public String WIKIDOCS_TO_ACTIVATE_DEACTIVATE_KEY = "wikidocsToActivateDeactivate";
	public String WIKIDOCS_TO_LOCK_UNLOCK_KEY = "wikidocsToLockUnlock";
	public String WIKIDOCS_TO_HIDE_UNHIDE_KEY = "wikidocsToHideUnhide";
	public String WIKIDOCS_DELETE_PURGE_FLAG_KEY = "purge";
	public String WIKIDOCS_OVERWRITE_FLAG_KEY = "OVERWRITE";
	public String WIKIDOCS_REPLACE_FLAG_KEY = "REPLACE";
	public String WIKI_ACTION = "wikiaction";
	public String WIKI_ACTION_SAVE = "save";
	public String WIKI_ACTION_SAVE_AND_EXIT = "saveandexit";
	
	public String WIKIDOC_OLD_NAMESPACE = "WIKIDOC_OLD_NAMESPACE";
    public String WIKIDOC_NEW_NAMESPACE = "WIKIDOC_NEW_NAMESPACE";
    public String WIKIDOC_OLD_DOCUMENT_NAME = "WIKIDOC_OLD_DOCUMENT_NAME";
    public String WIKIDOC_NEW_DOCUMENT_NAME = "WIKIDOC_NEW_DOCUMENT_NAME";
    public String WIKIDOC_NAMESPACE = "WIKIDOC_NAMESPACE";
    public String WIKIDOC_DOCUMENT_NAME = "WIKIDOC_DOCUMENT_NAME";
    public String WIKIDOC_DOCUMENT_NAMES = "WIKIDOC_DOCUMENT_NAMES";
    public String WIKIDOC_FULL_NAMES = "WIKIDOC_FULL_NAMES";
    public String WIKIDOC_CHANGE_MAP = "WIKIDOC_CHANGE_MAP";
    public String PURGE_WIKIDOCUMENT = "PURGE_WIKIDOCUMENT";
    public String GROUP_DOCUMENTS = "GROUP_DOCUMENTS";

	public String SYSTEM_TOOLBAR_PAGE_NAME = "toolbarForWikiPageName";
	public String SYSTEM_TOOLBAR_DOC_SUMMARY = "wikiDocumentSummary";
	public String SYSTEM_TOOLBAR_PAGE_LAST_UPDATED_USERNAME = "pluUserName";
	public String SYSTEM_TOOLBAR_PAGE_LAST_UPDATED_ON = "pageUpdatedOn";
	public String SYSTEM_TOOLBAR_PAGE_LAST_REVIEWED_ON = "pageReviewedOn";
	public String SYSTEM_TOOLBAR_PAGE_EXPIRED_ON = "pageExpiredOn";
	public String SYSTEM_TOOLBAR_PAGE_CREATEDBY_USERNAME = "createdBy";
	public String SYSTEM_TOOLBAR_PAGE_CREATEDON = "createdOn";
	public String SYSTEM_TOOLBAR_PAGE_LAST_UPDATED_VERSION = "docVersion";
	public String SYSTEM_TOOLBAR_PAGE_LAST_UPDATED_VIEWED = "viewedCount";
	public String SYSTEM_TOOLBAR_PAGE_LAST_UPDATED_EDITED = "editedCount";
	public String SYSTEM_TOOLBAR_PAGE_LAST_UPDATED_EXECUTED = "executedCount";
	public String SYSTEM_TOOLBAR_PAGE_LAST_UPDATED_AVG_INDEX_RATING = "avgIndexRating";
	public String SYSTEM_TOOLBAR_RECENTLY_VISITED_LINKS = "recentlyVisitedLinks";
	public String SYSTEM_TOOLBAR_OUTGOING_LINKS = "outgoingLinks";
	public String SYSTEM_TOOLBAR_INCOMING_LINKS = "incomingLinks";
	public String SYSTEM_TOOLBAR_DOCUMENT_TAGS = "documentTags";	
	public String SLEEP_IN_MILLISECONDS = "sleepInMS";
	
	public String PAGEINFO_PAGE_URL = "pageinfoPageUrl";
	public String PAGEINFO_APPLICATION_URL = "pageinfoApplicationUrl";
	
	public String PAGEINFO_LIST_OF_ACTION_TASK_IN_CONTENT = "listOfActionTaskInContent";
	public String PAGEINFO_LIST_OF_ACTION_TASK_IN_MAIN = "listOfActionTaskInMain";
	public String PAGEINFO_LIST_OF_ACTION_TASK_IN_FINAL = "listOfActionTaskInFinal";
	public String PAGEINFO_LIST_OF_ACTION_TASK_IN_ABORT = "listOfActionTaskInAbort";
	public String PAGEINFO_LIST_OF_RUNBOOKS_IN_MAIN = "listOfRunbooksInMain";
	public String PAGEINFO_LIST_OF_RUNBOOKS_IN_FINAL = "listOfRunbooksInFinal";
	public String PAGEINFO_LIST_OF_RUNBOOKS_IN_ABORT = "listOfRunbooksInAbort";

	public String RENDERED_CONTENT_KEY = "renderedContent";
	public String SOURCE_CONTENT_KEY = "sourceContent";
	public String MAIN_XML_KEY = "mainXML";
	public String FINAL_XML_KEY = "finalXML";
	public String ABORT_XML_KEY = "abortXML";
	public String DTREE_XML_KEY = "dtreeXML";
	public String DTREE_OPTIONS_KEY = "dtreeOptions";

	public String COMPARE_REVISION_URL_KEY = "compareRevisionURL";
	public String ERROR_EXCEPTION_KEY = "errorException";
	public String ERROR = "ERROR";
	public String SUCCESS = "SUCCESS";
	
	public String AVAILABLE_TAGS_KEY = "availableTags";
	public String SELECTED_TAGS_KEY = "selectedTags";
	
	public String AVAILABLE_ROLES_KEY = "AllAvailableRoles";
	public String READ_ROLES_KEY = "readRoles";
	public String WRITE_ROLES_KEY = "writeRoles";
	public String ADMIN_ROLES_KEY = "adminRoles";
	public String EXECUTE_ROLES_KEY = "executeRoles";
	public String DEFAULT_ROLE = "defaultRoles";

	public String LOCATION_KEY = "LOCATION";
	public String TYPE_KEY = "TYPE";
	public String LASTMODIFIED = "LASTMODIFIED";
	public String CONTROL_ID = "CONTROL_ID";
	public String TIMEZONE = "TIMEZONE";
	public String GWT_ENTRY_POINT = "GWT_ENTRY_POINT";
	public String LINK_GWT_RSACTIONTASK = "/resolve/jsp/rsactiontask.jsp?main=actiontask&type=table";


	
	//parameters that are passed along with the URL request
	public String WIKI_PARAMETER_REVISION = "rev";
	public String WIKI_PARAMETER_REV1 = "rev1";
	public String WIKI_PARAMETER_REV2 = "rev2";
	public String WIKI_PARAMETER_FLAG = "model";
	public String WIKI_PARAMETER_ATTACHMENT_SYS_ID = "attach";
	public String WIKI_PARAMETER_ATTACHMENT_FILE_NAME = "attachFileName";
	public String WIKI_PARAMETER_ATTACHMENT_TYPE = "attachType";
	public String WIKI_PARAMETER_DELETE_ATTACHMENTS = "deleteAttachments";
	
	public String EDIT_TOOLBAR = "editToolbar";
	
	public String WIKI_PARAMETER_METHOD = "METHOD";
	public String METHOD_WIKITAG = "METHOD_WIKITAG";
	public String METHOD_ACTION = "METHOD_ACTION";
	public String METHOD_WIKILINK = "METHOD_WIKILINK";
	public String DATA_WIKITAG = "DATA_WIKITAG";
	public String DATA_ACTION = "DATA_ACTION";
	public String DATA_WIKILINK = "DATA_WIKILINK";

	//same as TreeViewApplet.java
	public String TYPE_WIKITAG = "WIKITAG";
	public String TYPE_ACTION = "ACTION";
	public String TYPE_WIKILINK = "WIKILINK";

	//properties names - mapping the properties table
	public String PROP_WIKI_RIGHTS = "wiki.rights";
	public String PROP_ACTIONTASK_RIGHTS = "actiontask.rights";
	public String PROP_WIKITEMPLATE_RIGHTS = "wikitemplate.rights";
	public String PROP_WIKI_PROPERTY_TABLE_HOMEPAGE = "wiki.property.table.homepage";
	public String PROP_WIKI_LOOKUP_DEFAULT_DOC = "wiki.lookup.default.doc";
//	public String PROP_FLAG_USE_PROPERTY_TABLE_HOMEPAGE = "flag.use_propery_table_homepage";
	
	
	public String PARAMS_WIKI = "wiki";
	public String LINK_RSWIKI_JSP = "/resolve/jsp/rswiki.jsp?";
	public String RSWIKI_JSP = "rswiki.jsp?";

	//standard data in the tables
	//	public String DATA_WIKIDOC = "wikidoc";

	//content, model, exception, final - used for diffrev as parameters
	//	public String[] WIKI_FLAG_VALUES = {"content", "model", "exception", "final" };

	public String WIKI_VIEW_JSP = "view";//name of the view.jsp
	public String WIKI_VIEWLIST_JSP = "viewList";//name of the view.jsp
	public String WIKI_EDIT_JSP = "edit";//name of the edit.jsp
	public String WIKI_ATTACH_JSP = "attach";//name of the attach.jsp
	public String WIKI_NAVIGATE_JSP = "navigate";//name of the view.jsp
    public String WIKI_DIFF_JSP = "diff";//name of the view.jsp
	public String WIKI_NAMESPACE_HOME = "Runbook";
	public String WIKI_WEB_HOME = "WebHome";
	public String WIKI_SYSTEM_HOMEPAGE = "System.HomePage";
	public String WIKI_PROPERTY_TABLE_ROLES = "wiki.property.table.roles";
	public String RESULT_VIEW_JSP = "result";//name of the view.jsp
	public String ERROR_JSP = "error";
	
	//General entry points
	public String URL_FORWARD_TO_GWT_ENTRY_POINT = "/forward/gwt";
	
	
	//Constants used in Controllers
	public String URL_PUBLIC_VIEW = "/public/view";//WikiAction.view.toString();
	public String URL_PUBLIC_VIEWDOC = "/public/view/{namespace}/{docname}";//WikiAction.view.toString();
	public String URL_PUBLIC_VIEWMODELC = "/public/viewmodelc/{namespace}/{docname}";//WikiAction.viewmodelc.toString();
	public String URL_PUBLIC_VIEWEXCPC = "/public/viewexcpc/{namespace}/{docname}";//WikiAction.viewexcpc.toString();
	public String URL_PUBLIC_VIEWATTACH = "/public/viewattach/{namespace}/{docname}";//WikiAction.viewattach.toString();
	public String URL_PUBLIC_DOWNLOAD = "/public/download/{namespace}/{docname}";//WikiAction.download.toString();
	public String URL_PUBLIC_LOOKUP = "/public/lookup";//WikiAction.lookup.toString()
	public String URL_PUBLIC_CURRENT = "/public/current";//There is no WikiAction for this one yet ***
	public String URL_PUBLIC_INCLUDE_DOC_CONTENT = "/public/include";//There is no WikiAction for this one yet ***
	
	public String URL_VIEW = "/wiki/view";//WikiAction.view.toString();
	public String URL_LOOKUP = "/wiki/lookup";//WikiAction.lookup.toString()
	public String URL_CURRENT = "/wiki/current";//There is no WikiAction for this one yet ***
	public String URL_HOMEPAGE = "/wiki/homepage";//There is no WikiAction for this one yet ***
	public String URL_INCLUDE_DOC_CONTENT = "/wiki/include";//There is no WikiAction for this one yet ***

//	public String URL_REGISTER = "/wiki/register";//this is to register the url when the user clicks back button
	
	public String URL_LISTDOC = "/wiki/listdoc/{namespace}/{docname}";//WikiAction.listdoc.toString()

	
	public String URL_VIEWDOC = "/wiki/view/{namespace}/{docname}";//WikiAction.view.toString();
    public String URL_VIEWDOC_SECTION = "/wiki/view/{namespace}/{docname}/{sectionname}";//WikiAction.view.toString();
	public String URL_DELETE = "/wiki/delete/{namespace}/{docname}";//WikiAction.delete.toString();
	public String URL_VIEWATTACH = "/wiki/viewattach/{namespace}/{docname}";//WikiAction.viewattach.toString();
	public String URL_NAVIGATE = "/wiki/navigate/{namespace}/{docname}";//WikiAction.navigate.toString();
	public String URL_NAVIGATE_DATA = "/wiki/navigate/data";//WikiAction.navigate.toString();
	public String URL_DOWNLOAD_BASE = "/wiki/download";
	public String URL_DOWNLOAD = "/wiki/download/{namespace}/{docname}";//WikiAction.download.toString();
	public String URL_ATTACH = "/wiki/attach/{namespace}/{docname}";//WikiAction.attach.toString();
	public String URL_DELETEATTACH = "/wiki/deleteattach/{namespace}/{docname}";//WikiAction.deleteattach.toString();
	public String URL_EDITDOC = "/wiki/edit/{namespace}/{docname}";//WikiAction.edit.toString();
	public String URL_SAVEDOC = "/wiki/save/{namespace}/{docname}";//WikiAction.save.toString();
	public String URL_SAVEANDEXIT = "/wiki/saveandexit/{namespace}/{docname}";//WikiAction.saveandexit.toString();

	public String URL_VIEWMODEL_BASE = "/wiki/viewmodel";
	public String URL_EDITMODEL = "/wiki/editmodel/{namespace}/{docname}";//WikiAction.editmodel.toString();
	public String URL_VIEWMODEL = "/wiki/viewmodel/{namespace}/{docname}";//WikiAction.editmodel.toString();
	public String URL_EDITABORT = "/wiki/editabort/{namespace}/{docname}";//WikiAction.editabort.toString();
	public String URL_EDITFINAL = "/wiki/editfinal/{namespace}/{docname}";//WikiAction.editfinal.toString();
	public String URL_SAVEMODEL = "/wiki/savemodel/{namespace}/{docname}";//WikiAction.savemodel.toString();
	public String URL_SAVEABORT = "/wiki/saveabort/{namespace}/{docname}";//WikiAction.saveabort.toString();
	public String URL_SAVEFINAL = "/wiki/savefinal/{namespace}/{docname}";//WikiAction.savefinal.toString();
	
	public String URL_EDITDTREE = "/wiki/editdtree/{namespace}/{docname}";//WikiAction.editfinal.toString();
	public String URL_SAVEDTREE = "/wiki/savedtree/{namespace}/{docname}";//WikiAction.savemodel.toString();
	
	public String URL_VIEWMODELC = "/wiki/viewmodelc/{namespace}/{docname}";//WikiAction.viewmodelc.toString();
	public String URL_EDITMODELC = "/wiki/editmodelc/{namespace}/{docname}";//WikiAction.editmodelc.toString();
	public String URL_SAVEMODELC = "/wiki/savemodelc/{namespace}/{docname}";//WikiAction.savemodelc.toString();
	//	public String URL_SEMODELC = "/wiki/semodelc/{namespace}/{docname}";//WikiAction.semodelc.toString();

	public String URL_VIEWEXCPC = "/wiki/viewexcpc/{namespace}/{docname}";//WikiAction.viewexcpc.toString();
	public String URL_EDITEXCPC = "/wiki/editexcpc/{namespace}/{docname}";//WikiAction.editexcpc.toString();
	public String URL_SAVEEXCPC = "/wiki/saveexcpc/{namespace}/{docname}";//WikiAction.saveexcpc.toString();

	public String URL_VIEWFINALC = "/wiki/viewfinalc/{namespace}/{docname}";//WikiAction.viewfinalc.toString();
	public String URL_EDITFINALC = "/wiki/editfinalc/{namespace}/{docname}";//WikiAction.editfinalc.toString();
	public String URL_SAVEFINALC = "/wiki/savefinalc/{namespace}/{docname}";//WikiAction.savefinalc.toString();

	public String URL_DIFFREV = "/wiki/diffrev/{namespace}/{docname}";//WikiAction.diffrev.toString()
	
	public String URL_IMPEX_IMPORT = "/wiki/impex/import";//WikiAction.impex.toString()
	public String URL_IMPEX_EXPORT = "/wiki/impex/export";//WikiAction.impex.toString()
	public String URL_IMPEX_DOWNLOAD = "/wiki/impex/download";//WikiAction.impex.toString()
	
	public String URL_GET_PARAMS_GET_INPUT = "/wiki/params/getinput";
	public String URL_GET_PARAMS_GET_OUTPUT = "/wiki/params/getoutput";
	public String URL_GET_PARAMS_GET_MODEL = "/wiki/params/getmodel";
	public String URL_GET_PARAMS_GET_POPUPLIST = "/wiki/params/getpopuplist";
	public String URL_GET_PARAMS_GET_CHANGECOLOR = "/wiki/params/changecolor";
	public String URL_GET_MENU_LIST = "/wiki/params/getmenulist";
	public String MODEL_XML_VALIDATION = "/wiki/modelxml/validation";
	public String DTREE_XML_VALIDATION = "/wiki/dtreexml/validation";
	
	
	public String URL_RESULT_TASK_DETAIL = "/result/task/detail";
	public String URL_RESULT_TASK_RAW = "/result/task/raw";
	public String URL_RESULT_ARCHIVE_TASK_DETAIL = "/result/archivetask/detail";
	public String URL_RESULT_ARCHIVE_TASK_RAW = "/result/archivetask/raw";
	public String URL_RESULT_RPC_HTML = "/result/rpc/html";
	public String URL_DETAIL_RPC_HTML = "/detail/rpc/html";
	
	public String URL_WORKSHEET = "/worksheet";
	public String URL_RESOLVE_SERVICE = "/resolve/service"; 
	
	//spring Ids references
	public String WIKI_SPRINGID_FOR_WEBAPI = "webApi";
	public String WIKI_SPRINGID_FOR_HIBERNATESTORE = "hibernateStore";
	public String WIKI_SPRINGID_FOR_WIKIREQUESTCONTEXT = "wikiRequestContext";
	public String WIKI_SPRINGID_FOR_WIKIRENDERCONTEXT = "renderContext";
	public String ACTIONTASK_SPRINGID_FOR_RIGHTS = "actionTaskRightService";
	public String RESOLVEASSESS_SPRINGID_FOR_RIGHTS = "resolveAssessRightService";
    public String RESOLVEPREPROCESS_SPRINGID_FOR_RIGHTS = "resolvePreprocessRightService";
    public String RESOLVEPARSER_SPRINGID_FOR_RIGHTS = "resolveParserRightService";
    public String WIKI = "wiki";

	
	//key values in the Properties table
	public String PROPERTIES_SESSION_TIMEOUT = "session.timeout";
	public String PROPERTIES_SESSION_URL_COUNT = "session.url.count";
	public String PROPERTIES_WIKI_USER_HISTORY_COUNT = "wiki.user.history.count";
	public String PROPERTIES_WIKI_ARCHIVED_ALLOWED = "wiki.archive.count";
    public String PROPERTIES_EXCLUDE_DOCUMENT_PATTERN = "search.exclude.document.pattern";

	// ModelAndView key
	public String MODEL_CONTENT_KEY = "content";
	
	//status check string if its the final record and refresh needs to stop
	public String STOP_AJAX_REFRESH = "STOP_REFRESH";
	
	public String COMPONENT_TYPE = "componentType";
	public String CR_TYPE = "crType";
	public String CR_SUMMARY = "crSummary";
	public String CR_DETAIL = "crDetail";
	public String CR_SOURCE = "crSource";
	public String CR_DESTINATION = "crDestination";
	public String CR_PRIORITY = "crPriority";
	
	public String VIEW_LOOKUP = "view_lookup";
	
	public String DT_TMETRIC_LOG = "DT_TMETRIC_LOG";
	
	public int BATCH_FOR_SQL_IN_CLAUSE = 500;
	
	public String ANSWER_PARAMS_MAP = "answer_params_map";
    
    public String GIBBERISH_TOKEN = "__rs_@_@_rs__";

}
