/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.migration.gateways;

import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.util.HibernateUtil;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public abstract class MigrateLegacyGateways {
    
    public static void migrate(Map params)
    {
        List<String> gateways = getExistingGateways(params);
        
        if(gateways.size() == 0)
            return;
        
        for(String gateway:gateways) {
            String type = (String)params.get(gateway);
            if(StringUtils.isBlank(type))
                continue;
            
            try {
                switch(type) {
                    case "Push":
                        MigrateLegacyPushGateway push = new MigrateLegacyPushGateway(gateway);
                        push.migrate();
                        break;
                    case "Pull":
                        MigrateLegacyPullGateway pull = new MigrateLegacyPullGateway(gateway);
                        pull.migrate();
                        break;
                    case "MSG":
                        MigrateLegacyJMSGateway jms = new MigrateLegacyJMSGateway(gateway);
                        jms.migrate();
                        break;
                    default:
                        break;
                }
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    
    public static List<String> getExistingGateways(Map<String, String> params) {

        List<String> gateways = new ArrayList<String>();
        SQLConnection conn = null;
        
        List<String> gatewayNames = new ArrayList<String>();
        for(Iterator<String> keys=params.keySet().iterator();keys.hasNext();)
            gatewayNames.add(keys.next());
        
        try {
            conn = SQL.getConnection();
            
            DatabaseMetaData md = conn.getMetaData();
            String[] types = {"TABLE"};
            ResultSet rs = md.getTables(null, null, "%_filter", types);
            
            while (rs.next()) {
                String tableName = rs.getString(3);
                int index = tableName.indexOf("_filter");
                if(index == -1)
                    continue;

                for(String name:gatewayNames) {
                    if(name.equalsIgnoreCase(tableName.substring(0, index)))
                        gateways.add(name);
                }
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
        } finally {
            try {
                if (conn != null)
                    SQL.close(conn);
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        return gateways;
    }
    
    protected static void updateSysAppModule(String name, String title, SQLConnection conn) throws Exception {
        
        PreparedStatement pStmt = null;
        String value = "RS.gateway.Custom/name=" + name;

        String sql = "update sys_app_module set query='" + value + "' where title=?";

        try {
            pStmt = conn.prepareStatement(sql);

            pStmt.setString(1, title);
            
            pStmt.executeQuery();
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        } finally {
            try
            {
                if (pStmt != null)
                    pStmt.close();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    
    protected static List<String> getAllFilterIds(String modelName, SQLConnection conn) throws Exception {
        
        List<String> filterIds = new ArrayList<String>();
        
        PreparedStatement selectStmt = null;
        ResultSet rs = null;
        String sysId = null;
        
        String sql = "SELECT sys_id FROM " + modelName;
        
        try {
            selectStmt = conn.prepareStatement(sql);
            rs = selectStmt.executeQuery();

            while(rs.next()) {
                sysId = rs.getString("sys_id");
                filterIds.add(sysId);
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        } finally {
            try
            {
                if (selectStmt != null)
                    selectStmt.close();
                if (rs != null)
                    rs.close();
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        return filterIds;
    }

    protected void migrateStandardFields(SQLConnection conn) throws Exception {

        Statement insertStmt = null;

        List<String> sqlList = getStandardQueries();
        
        if(sqlList.size() == 0)
            return;

        try {
            insertStmt = conn.createStatement();
            
            for(int i=0; i<sqlList.size(); i++)
                insertStmt.addBatch(sqlList.get(i));
            
            insertStmt.executeBatch();
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        } finally {
            try {
                if (insertStmt != null)
                    insertStmt.close();
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    
    protected void migrateCustomFields(SQLConnection conn) throws Exception {
        
        Statement insertStmt = null;
        
        List<String> sqlList = getCustomQueries(conn);
        
        if(sqlList.size() == 0)
            return;
        
        try {
            insertStmt = conn.createStatement();
            
            for(int i=0; i<sqlList.size(); i++) {
                insertStmt.addBatch(HibernateUtil.getValidQuery(sqlList.get(i)));
            }
            insertStmt.executeBatch();
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        } finally {
            try {
                if (insertStmt != null)
                    insertStmt.close();
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
    }
    
    public List<String> getStandardQueries() {
        
        return null;
    }
    
    public List<String> getCustomQueries(SQLConnection conn) throws Exception {
        
        return null;
    }
    
} // class MigrateLegacyGateways
