/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.resolve.rsbase.ConfigCAS;
import com.resolve.rsbase.ConfigSQL;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.vo.MetricThresholdVO;
import com.resolve.services.jdbc.util.CommonJDBCUtil;
import com.resolve.services.jdbc.util.GenericJDBCHandler;
import com.resolve.services.jdbc.util.MetricUtil;
import com.resolve.services.jdbc.util.WikiUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;

public class ServiceJDBC
{
    /**
     * log an event 
     * 
     * @param msg
     * @param username
     */
    public static void logEvent(String msg, String username)
    {
        CommonJDBCUtil.logEvent(msg, username);
    }

    public static List<String> getWikiDocList(Timestamp recertifyDate, String namespaces, String docnames)
    {
        return WikiUtils.getWikiDocList(recertifyDate, namespaces, docnames);
    }
    
    public static List<String> getWikiDocArchiveList(String archiveNamespaces, String archiveDocnames)
    {
        return WikiUtils.getWikiDocArchiveList(archiveNamespaces, archiveDocnames);
    }
    
    public static List<Map<String, Object>> executeSQLSelect(String sql, int start, int limit) throws Exception
    {
        return GenericJDBCHandler.executeSQLSelect(sql, start, limit, false);
    }
    
    public static List<Map<String, Object>> executeSQLSelectForUI(String sql, int start, int limit) throws Exception
    {
        //date is string in this case as UI understands the string format of date
        return GenericJDBCHandler.executeSQLSelect(sql, start, limit, true);
    }
    
    public static int getTotalCountSafe(String table,String selectColumns ,String whereClause
    		, ConfigSQL configSQL) throws Exception
    {
        return GenericJDBCHandler.getTotalCountSafe(table,selectColumns,whereClause, configSQL);
    }

    public static int getTotalCount(String countSql) throws Exception
    {
        return GenericJDBCHandler.getTotalCount(countSql);
    }
    
    public static Map<String, String> getSQLMetaData(String tableName) throws Exception
    {
        return GenericJDBCHandler.getSQLMetaData(tableName);
    }
    
    public static void executeSQLUpdate(String sql, String username) throws Exception
    {
        GenericJDBCHandler.executeSQLUpdate(sql, username);
    }
    
    public static List<MetricThresholdVO> getMetricThreshold(QueryDTO query)
    {
        return MetricUtil.getMetricThreshold(query);
    }

    @Deprecated
    public static boolean hasTable(String table)
    {
        return GenericJDBCHandler.hasTable(table);
    }
    
    /**
     * initialize the persistence for Non-Hibernate components only.
     *
     * @param configSQL
     * @param configCAS
     * @param customMappingFileLocation
     * @param hibernateCfgLocation
     */
    public static void initPersistenceForNonHibernate(ConfigSQL configSQL, ConfigCAS configCAS, String customMappingFileLocation, String hibernateCfgLocation)
    {
        //StackTraceElement[] stElms = new Throwable().getStackTrace();
        
        GeneralHibernateUtil.initPersistence(configSQL, configCAS, customMappingFileLocation, hibernateCfgLocation);
    }

}
