/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.customtable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.hibernate.query.Query;

import com.resolve.persistence.dao.OrderbyProperty;
import com.resolve.persistence.model.MetaFormView;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.vo.ComboboxModelDTO;
import com.resolve.services.hibernate.vo.MetaFormViewVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.workflow.form.dto.CustomFormDTO;

public class CustomFormUtil
{

    @SuppressWarnings("unchecked")
	public static List<MetaFormViewVO> getAllMetaForms()
    {
        List<MetaFormViewVO> result = new ArrayList<MetaFormViewVO>();
        List<MetaFormView> list = null;

        OrderbyProperty orderByProperty = new OrderbyProperty("UViewName", true);

        try
        {        	
            Log.log.debug("Querying MetaFormViews...");
          HibernateProxy.setCurrentUser("system");
            list = (List<MetaFormView>) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getMetaFormViewDAO().findAll(orderByProperty);
            });
            Log.log.debug("Returned " + (list != null ? list.size() : 0) + " MetaFormViews...");

        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        if (list != null)
        {
            for (MetaFormView model : list)
            {
                result.add(model.doGetVO());
            }
        }

        return result;
    }
    
    public static List<MetaFormViewVO> getMetaFormViews(QueryDTO query, String username) throws Exception
    {
        return new SearchCustomForm(query, username).execute();
    }
    
    public static MetaFormView findMetaFormView(String id, String viewName, String username) throws Exception
    {
        MetaFormView obj = null;
        if (StringUtils.isNotEmpty(id))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	obj = (MetaFormView) HibernateProxy.execute(() -> {

            		return HibernateUtil.getDAOFactory().getMetaFormViewDAO().findById(id);

            	});
            }
            catch (Throwable t)
            {
                Log.log.info("Unable to Determine Whether Wiki Docs Exist", t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
        else if (StringUtils.isNotBlank(viewName))
        {
            //String sql = "from MetaFormView where LOWER(UViewName) = '" + viewName.trim().toLowerCase() + "'";
            String sql = "from MetaFormView where LOWER(UViewName) = :UViewName";
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("UViewName", viewName.trim().toLowerCase());
            
            List<? extends Object> recs = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
            if (recs != null && recs.size() > 0)
            {
                if (recs.size() == 1)
                    obj = (MetaFormView) recs.get(0);
                else
                    throw new Exception("More than 1 form exist with name " + viewName);
            }
        }

        return obj;
    }// findMetaFormView
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static List<ComboboxModelDTO> getAllMetaFormViewNamesWithIds() throws Exception
    {
        List<ComboboxModelDTO> result = new ArrayList<ComboboxModelDTO>();
        
        QueryDTO queryDTO = new QueryDTO();
        
        queryDTO.setSqlQuery("select u_view_name, sys_id from meta_form_view order by u_view_name asc");        
        queryDTO.setUseSql(true);
                
        try
        {
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {
            	 Query query = HibernateUtil.createSQLQuery(queryDTO.getSelectSQLNonParameterizedSort());
                 
                 List<Object[]> data = query.list();
                 
                 if(data != null)
                 {
                     for (Object[] aRow : data)
                     {
                         ComboboxModelDTO comboBoxModelDTO = new ComboboxModelDTO((String)aRow[0], (String)aRow[1]);
                         result.add(comboBoxModelDTO);
                     }
                 }
            });
                       
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting data for sql:" + queryDTO.getSqlQuery(), e);
            throw e;
        }
                
        return result;
    }
    
    public static List<CustomFormDTO> getCustomFormsForUI(QueryDTO query, String username) throws Exception
    {
        return new SearchCustomForm(query, username).executeForUI();
    }

	public static MetaFormViewVO getCustomFormWithReferences(String sysId, String fullName, String username) {
        MetaFormView model = null;
        MetaFormViewVO result = null;
        
        try
        {
            model = findMetaFormView(sysId, null, username);
            if (model != null)
            {
                result = model.doGetVO();
            }
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
        }

        return result;
	}
	
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static Set<String> getAllCustomFormSysIds()
    {
        Set<String> sysIds = new TreeSet<String>();
        try
        {
        	HibernateProxy.execute(() -> {
                // fetch the info based on the search criteria
                String sql = "select mfv.sys_id from MetaFormView as mfv";

                Query query = HibernateUtil.createQuery(sql);
                List<Object> customForms = query.list();
                if (customForms != null && customForms.size() > 0)
                {
                    for(Object sysId : customForms)
                    {
                        sysIds.add(sysId.toString());
                    }
                }
               
        	});
        }
        catch (Throwable t)
        {
            Log.log.info(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return sysIds;
    }
    
    public static Integer getFormChecksum(String formName, String username)
    {
        Integer checksum = null;
        
        MetaFormView form = null;
        try
        {
            form = findMetaFormView(null, formName, username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        if (form != null)
        {
            checksum = form.getChecksum();
        }
        return checksum;
    }
    
    public static Boolean isFormPresent(String formId, String formName, String username) {
        Boolean result = false;
        
        try {
              HibernateProxy.setCurrentUser(username);
        	result = (Boolean) HibernateProxy.execute(() -> {
            
	            if (StringUtils.isNotBlank(formId)) {
	                return HibernateUtil.getDAOFactory().getMetaFormViewDAO().findById(formId) != null;
	            } else if (StringUtils.isNotBlank(formName)) {
	                return HibernateUtil.createHQLQuery("select 1 from MetaFormView formview where formview.UFormName = :name")
	                    .setParameter("name", formName)
	                    .uniqueResult() != null;
	            } else {
	            	return false;
	            }
        	});
        } catch(Exception e) {
            // No need to log any message.
            Log.log.error("Error checking for form", e);
                                           HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return result;
    }
}
