/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.social.follow;

import java.util.Collection;
import java.util.Set;

import com.resolve.services.ServiceGraph;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;


/**
 * 
 * Class to get the Team details - Users and Teams
 * If 'recurse' is true, it will get all the Teams recursively 
 * If 'ignoreUsers' is true, it will return only 'Teams' and filter out the 'Users'
 * 
 * @author jeet.marwah
 *
 */
public class TeamFollow extends ContainerFollow
{

    public TeamFollow(String teamSysId, String teamName, String username) throws Exception
    {
        super(ServiceGraph.findNode(null, teamSysId, teamName, NodeType.TEAM, username), username);
    }
    
    @Override
    public Set<RSComponent> getStreams() throws Exception
    {
//        this.streams.addAll(convertNodeToRsComponent(teamNode, directFollow));
        
        Collection<ResolveNodeVO> nodesThatThisTeamContains = ServiceGraph.getNodesFollowedBy(rootNode.getSys_id(), null, null, null, username);
        if(nodesThatThisTeamContains != null)
        {
            for(ResolveNodeVO node : nodesThatThisTeamContains)
            {
                NodeType type = node.getUType();
                if(type == NodeType.TEAM)
                {
                    streams.addAll(convertNodeToRsComponent(node, isDirectFollow()));
                    
                    //if the recurse is true, get the child teams
                    if(isRecurse())
                    {
                        TeamFollow searchChildTeam = new TeamFollow(node.getUCompSysId(), node.getUCompName(), username);
                        searchChildTeam.setDirectFollow(false);
                        searchChildTeam.setIgnoreUsers(true);
                        searchChildTeam.setRecurse(true);
                        
                        streams.addAll(searchChildTeam.getStreams());
                    }
                }
//                else if(type == NodeType.USER)
//                {
//                    if(!isIgnoreUsers())
//                    {
//                        streams.addAll(convertNodeToRsComponent(node, isDirectFollow()));
//                    }
//                }
            }
        }
        
        //get the Users in a Team 
        if(!isIgnoreUsers())
        {
            Collection<ResolveNodeVO> nodesFollowingMe = ServiceGraph.getNodesFollowingMe(rootNode.getSys_id(), null, null, null, username);
            if (nodesFollowingMe != null)
            {
                for (ResolveNodeVO nodeFollowingMe : nodesFollowingMe)
                {
                    NodeType nodeFollowingMeType = nodeFollowingMe.getUType();

                    //if this a USER node, ignore it 
                    if (nodeFollowingMeType == NodeType.USER)
                    {
                        streams.addAll(convertNodeToRsComponent(nodeFollowingMe, isDirectFollow()));
                    }
                }
            }
        }
        
        
        
        return streams;
    }
    
}
