/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.component.tree;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;

public class TeamTree
{
    private Collection<Process> processes;
    private Collection<Team> teams;
    private Set<String> postPathTeamsToRootProcess = new HashSet<String>();
    private Set<String> postPathTeamsToRootTeams = new HashSet<String>();
    private User owner;

    public void setProcesses(Collection<Process> processes)
    {
        this.processes = processes;
    }

    public Collection<Process> getProcesses()
    {
        return this.processes;
    }

    public void setTeams(Collection<Team> teams)
    {
        this.teams = teams;
    }

    public Collection<Team> getTeams()
    {
        return this.teams;
    }

    public void setOwner(User user)
    {
        this.owner = user;
    }

    public User getOwner()
    {
        return this.owner;
    }

    public void setPostPathTeamsToRootProcess(Set<String> postPathTeamsToRootProcess)
    {
        this.postPathTeamsToRootProcess = postPathTeamsToRootProcess;
    }

    public Set<String> getPostPathTeamsToRootProcess()
    {
        return this.postPathTeamsToRootProcess;
    }

    public void setPostPathTeamsToRootTeams(Set<String> postPathTeamsToRootTeams)
    {
        this.postPathTeamsToRootTeams = postPathTeamsToRootTeams;
    }

    public Set<String> getPostPathTeamsToRootTeams()
    {
        return this.postPathTeamsToRootTeams;
    }

    public List<Team> getTeamsForJson()
    {
        RSComponent.sortCollection(teams);
        List<Team> list = new ArrayList<Team>();
        for (Team p : this.teams)
        {
            list.add(p);
        }
        return list;
    }

}
