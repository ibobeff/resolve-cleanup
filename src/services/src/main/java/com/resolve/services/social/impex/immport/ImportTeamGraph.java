/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.social.impex.immport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.resolve.services.ServiceGraph;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.social.impex.GraphRelationshipDTO;
import com.resolve.util.Log;

public class ImportTeamGraph extends ImportComponentGraph
{
    public ImportTeamGraph(GraphRelationshipDTO rel, String username) throws Exception
    {
        super(rel, username);
    }

    @Override
    public void immport() throws Exception
    {
        Log.log.debug("Importing Team Relationship:" + rel);
        ResolveNodeVO teamNode = ServiceGraph.findNode(null, null, rel.getSourceName(), rel.getSourceType(), username);
        if (teamNode != null)
        {
            switch (rel.getTargetType())
            {
                case USER:
                    //*** Note: User will always follow rest of the Entities
                    ResolveNodeVO userNode = ServiceGraph.findNode(null, null, rel.getTargetName(), rel.getTargetType(), username);
                    if (userNode != null)
                    {
                        Collection<ResolveNodeVO> followComponents = new ArrayList<ResolveNodeVO>();
                        followComponents.add(teamNode);

                        ServiceGraph.follow(userNode.getSys_id(), null, null, null, followComponents, followerEdgeProps, username);
                    }
                    else
                    {
                        Log.log.debug("User not available for Relationship:" + rel);
                    }
                    break;

                case TEAM:
                    ResolveNodeVO teamNodeDst = ServiceGraph.findNode(null, null, rel.getTargetName(), rel.getTargetType(), username);
                    if (teamNodeDst != null)
                    {
                        Collection<ResolveNodeVO> followComponents = new ArrayList<ResolveNodeVO>();
                        followComponents.add(teamNode);

                        ServiceGraph.follow(teamNodeDst.getSys_id(), null, null, null, followComponents, followerEdgeProps, username);
                    }
                    else
                    {
                        Log.log.debug("Target Team not available for Relationship:" + rel);
                    }

                    break;

                default:
                    break;
            }
        }
        else
        {
            Log.log.debug("Team Node Does not exist for " + rel);
        }
    }

}
