/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.migration.entities;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.hibernate.util.SocialUtil;
import com.resolve.services.hibernate.vo.ResolveNodePropertiesVO;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;

public class MigrateWikiDoc extends Entities
{
    private String sysId;
    private static AtomicInteger count = new AtomicInteger(0);
    
    public MigrateWikiDoc(String sysId)
    {
        this.sysId = sysId;
    }
    

//    @Override
    public void run()
    {
        //there is no need to use the save api for the doc as its just fixing the wiki content.
        WikiDocument doc = null;
        count.incrementAndGet();
       try
        {
            doc = findWikiDocumentById(sysId);
            if (doc != null)
            {
                Set<ResolveNodePropertiesVO> props = new HashSet<ResolveNodePropertiesVO>();
                props.add(createProp(Document.IS_DT, doc.ugetUIsRoot() + ""));
                props.add(createProp(Document.IS_RUNBOOK, doc.ugetUHasActiveModel() + ""));

                ResolveNodeVO node = new ResolveNodeVO();
                node.setUCompName(doc.getUFullname());
                node.setUCompSysId(sysId);
                node.setUType(NodeType.DOCUMENT);
                node.setUMarkDeleted(doc.getUIsDeleted());
                node.setULock(doc.getUIsLocked());
                node.setUPinned(false);
                node.setProperties(props);
                
                //access rights
                AccessRights ar = doc.getAccessRights();
                if(ar != null)
                {
                    node.setUReadRoles(SocialUtil.convertRolesToSocialRolesString(ar.getUReadAccess() + "," + ar.getUAdminAccess()));
                    node.setUEditRoles(SocialUtil.convertRolesToSocialRolesString(ar.getUReadAccess() + "," + ar.getUAdminAccess()));
                    node.setUPostRoles(SocialUtil.convertRolesToSocialRolesString(ar.getUReadAccess() + "," + ar.getUAdminAccess()));
                }

                //persist
                persistNode(node);
                Log.log.debug("Migrated Document:" + doc.getUFullname() + ", #:" + count.intValue());
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the doc during the migration script. Please ignore this error as this will not affect the migration process. The document is " + doc.getUFullname(), e);
        }
        
    }

    private WikiDocument findWikiDocumentById(String sysId) throws Exception
    {
        WikiDocument doc = null;
        try
        {
          HibernateProxy.setCurrentUser("system");
        	doc = (WikiDocument) HibernateProxy.execute(() -> {

        		return HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(sysId);

        	});
        }
        catch (Exception e)
        {
            Log.log.error("Error getting document with sysId: " + sysId, e);
            throw e;
        }

        return doc;
    }
    
}
