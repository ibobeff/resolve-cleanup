/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.wiki.reports;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

/**
 * DTO for 
 *      Total Path Count Trend Reports
 *      Abort Path Count Trend Reports
 *      Path Duration Trend Reports
 * 
 * @author jeet.marwah
 *
 */
public class PathTrendReportDTO
{
    private String day;
    private long ts; //convenience for UI, same as day
    private int count = 0;
    private double totalTimeInSecs; //used for Path Duration Trend Reports
    private DTReportDetailDTO details;
    
    public PathTrendReportDTO()
    {
        
    }
    
    public PathTrendReportDTO(DTGroupByPathIdDTO path)
    {
        if(path != null)
        {
            DTRecord dtRecord = path.getRecords().get(0);

            Date executionDate = dtRecord.getTsDateTime();
            this.day = DateFormat.getDateInstance(DateFormat.MEDIUM).format(executionDate);
            
            try
            {
                //convenience for UI
                this.ts = DateFormat.getDateInstance(DateFormat.MEDIUM).parse(this.day).getTime();
            }
            catch (ParseException e)
            {
                this.ts = executionDate.getTime();
            }
            
            this.count = dtRecord.getTotalCount();

            //update the details
            details = new DTReportDetailDTO(path.getRecords());
            
            //set the total time in secs
            totalTimeInSecs = details.getTotalTimeInSecs();
        }
    }

    public String getDay()
    {
        return day;
    }

    public void setDay(String day)
    {
        this.day = day;
    }

    public int getCount()
    {
        return count;
    }

    public void setCount(int count)
    {
        this.count = count;
    }

    public DTReportDetailDTO getDetails()
    {
        return details;
    }

    public void setDetails(DTReportDetailDTO details)
    {
        this.details = details;
    }

    public double getTotalTimeInSecs()
    {
        return totalTimeInSecs;
    }

    public void setTotalTimeInSecs(double totalTimeInSecs)
    {
        this.totalTimeInSecs = totalTimeInSecs;
    }

    public long getTs()
    {
        return ts;
    }

    public void setTs(long ts)
    {
        this.ts = ts;
    }

    
    
    
}
