/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.migration.entities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.SocialUtil;
import com.resolve.services.hibernate.vo.ResolveNodePropertiesVO;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;

public class MigrateWikiDocs  extends Entities
{
    private Set<String> sysIds;
    private int count = 0;
    
    public MigrateWikiDocs(Set<String> sysIds)
    {
        this.sysIds = sysIds;
    }
    

    public void run()
    {
        Set<WikiDocument> docs = null;
        
        try
        {
            docs = loadWikiDocuments();
        }
        catch(Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        
        for(WikiDocument doc : docs)
        {
            try
            {
                migrateWikiDoc(doc, true);
            }
            catch(Exception t)
            {
                Log.log.error("Error saving the doc during the migration script. Please ignore this error as this will not affect the migration process. The document is " + doc.getUFullname(), t);
            }
        }
    }
    
    private Set<WikiDocument> loadWikiDocuments() throws Exception
    {
        Set<WikiDocument> result = new HashSet<WikiDocument>();
        
        String hql =    "select wd.sys_id, wd.UFullname, wd.UIsRoot, wd.UHasActiveModel, wd.UIsDeleted, wd.UIsLocked, wd.sysCreatedOn, wd.sysCreatedBy, wd.sysUpdatedOn, wd.sysUpdatedBy, wd.sysModCount, wd.sysOrg, " +
                        "ar.UReadAccess, ar.UWriteAccess, ar.UAdminAccess  " +
                        "from WikiDocument as wd, AccessRights as ar where ar.UResourceId = wd.sys_id and ar.UResourceType ='wikidoc' ";
        
        Map<String, Collection<Object>> queryInParams = new HashMap<String, Collection<Object>>();
        
        if(this.sysIds != null && sysIds.size() > 0)
        {
            //hql = hql + "and wd.sys_id IN (" + SQLUtils.prepareQueryString(new ArrayList<String>(this.sysIds)) + ")";
            hql = hql + "and wd.sys_id IN (:sys_id)";
            queryInParams.put("sys_id", new ArrayList<Object>(this.sysIds));
        }
        
        
        List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectInOnly(hql, queryInParams);
        if(data != null)
        {
            
            String[] columns = {"sys_id", "UFullname", "UIsRoot", "UHasActiveModel", "UIsDeleted", "UIsLocked", "sysCreatedOn", "sysCreatedBy", "sysUpdatedOn", "sysUpdatedBy", "sysModCount", "sysOrg", "accessRights.UReadAccess", "accessRights.UWriteAccess", "accessRights.UAdminAccess"};
            
            for(Object o : data)
            {
                Object[] record = (Object[]) o;
                WikiDocument instance = new WikiDocument();
                instance.setAccessRights(new AccessRights());

                //set the attributes in the object
                for (int x = 0; x < columns.length; x++)
                {
                    String column = columns[x].trim();
                    Object value = record[x];

                    PropertyUtils.setProperty(instance, column, value);
                }
                
                //add it to the set
                result.add(instance);
                
            }//end of for loop
        }
        
        return result;
    }
    
    private ResolveNodeVO migrateWikiDoc(WikiDocument doc, boolean persist) throws Exception
    {
        ResolveNodeVO pnode = null;
        
        if (doc != null)
        {
            Set<ResolveNodePropertiesVO> props = new HashSet<ResolveNodePropertiesVO>();
            props.add(createProp(Document.IS_DT, doc.ugetUIsRoot() + ""));
            props.add(createProp(Document.IS_RUNBOOK, doc.ugetUHasActiveModel() + ""));

            ResolveNodeVO node = new ResolveNodeVO();
            
            copyBaseModelToResolveNode(node, doc);

            node.setUCompName(doc.getUFullname());
            node.setUCompSysId(doc.getSys_id());
            node.setUType(NodeType.DOCUMENT);
            node.setUMarkDeleted(doc.getUIsDeleted());
            node.setULock(doc.getUIsLocked());
            node.setUPinned(false); // Bug - Move pinned to ResolveEdge of type FAVORITE
            node.setProperties(props);
            
            //access rights
            AccessRights ar = doc.getAccessRights();
            if(ar != null)
            {
                node.setUReadRoles(SocialUtil.convertRolesToSocialRolesString(ar.getUReadAccess() + "," + ar.getUAdminAccess()));
                node.setUEditRoles(SocialUtil.convertRolesToSocialRolesString(ar.getUReadAccess() + "," + ar.getUAdminAccess()));
                node.setUPostRoles(SocialUtil.convertRolesToSocialRolesString(ar.getUReadAccess() + "," + ar.getUAdminAccess()));
            }

            //persist
            
            if(persist)
            {
                pnode = persistNode(node);
                count++;
            }
            else
            {
                pnode = node;
            }
            Log.log.trace("Migrated Document:" + doc.getUFullname() + ", (" + Thread.currentThread().getName() + ")#:" + count/*.intValue()*/);
        }
        
        return pnode;
    }


    @Override
    public Collection<String> call() throws Exception
    {
        Set<WikiDocument> docs = null;
        
        try
        {
            docs = loadWikiDocuments();
        }
        catch(Exception t)
        {
            Log.log.error(t.getMessage(), t);
            throw t;
        }
        
        Collection<ResolveNodeVO> resolveNodeVOs = new ArrayList<ResolveNodeVO>(HibernateUtil.getJdbcBatchSize() / (RESOLVE_NODE_REC_COUNT + 2));
        int recsToPerist = 0;
        count = 0;
        Collection<String> pCompSysIds = new ArrayList<String>(HibernateUtil.getJdbcBatchSize() / (RESOLVE_NODE_REC_COUNT + 2));
        
        for(WikiDocument doc : docs)
        {
            try
            {
                ResolveNodeVO pnode = migrateWikiDoc(doc, false);
                
                if(pnode != null)
                {
                    resolveNodeVOs.add(pnode);
                    recsToPerist++; 
                }
            }
            catch(Exception t)
            {
                Log.log.error("Error saving the doc during the migration script. Please ignore this error as this will not affect the migration process. The document is " + doc.getUFullname(), t);
                throw t;
            }
            
            if(recsToPerist % (HibernateUtil.getJdbcBatchSize() / (RESOLVE_NODE_REC_COUNT + 2)) == 0)
            {
                Collection<ResolveNodeVO> pResolveNodeVOs = persistNodes(resolveNodeVOs, (HibernateUtil.getJdbcBatchSize() / (RESOLVE_NODE_REC_COUNT + 2)));
                count += pResolveNodeVOs.size();
                
                for(ResolveNodeVO pResolveNodeVO : pResolveNodeVOs)
                {
                    pCompSysIds.add(pResolveNodeVO.getUCompSysId());
                }
                
                recsToPerist = 0;
                resolveNodeVOs.clear();
            }
        }
        
        if(!resolveNodeVOs.isEmpty())
        {
            Collection<ResolveNodeVO> pResolveNodeVOs = persistNodes(resolveNodeVOs, resolveNodeVOs.size());
            count += pResolveNodeVOs.size();
            
            for(ResolveNodeVO pResolveNodeVO : pResolveNodeVOs)
            {
                pCompSysIds.add(pResolveNodeVO.getUCompSysId());
            }
            
            recsToPerist = 0;
            resolveNodeVOs.clear();
        }
        
        return pCompSysIds;
    }
}
