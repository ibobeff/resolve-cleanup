/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.vo;

public class UserImpexVO
{
    public String sysId;
    public String displayName;
    public boolean exportPreference = true;
    public boolean exportNotification = true;
     
}
