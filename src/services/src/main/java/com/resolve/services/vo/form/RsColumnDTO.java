/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.form;

import java.util.List;


public class RsColumnDTO //extends RsUIComponent
{
    private List<RsUIField> fields;

    public List<RsUIField> getFields()
    {
        return fields;
    }

    public void setFields(List<RsUIField> fields)
    {
        this.fields = fields;
    }

    
    
}
