package com.resolve.services.archive;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.apache.log4j.Level;
import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.search.SearchAction;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.hibernate.ReplicationMode;

import com.resolve.persistence.model.ArchiveSirArtifact;
import com.resolve.persistence.model.ArchiveSirAttachment;
import com.resolve.persistence.model.ArchiveSirAuditLog;
import com.resolve.persistence.model.ArchiveSirNote;
import com.resolve.persistence.model.BaseModel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.search.APIFactory;
import com.resolve.search.SearchAdminAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.model.PBArtifacts;
import com.resolve.search.model.PBAttachments;
import com.resolve.search.model.PBAuditLog;
import com.resolve.search.model.PBNotes;
import com.resolve.search.playbook.PlaybookIndexAPI;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ArchiveSIRData {
    private static List<String> sirComponentIndices = new ArrayList<String>();
    private static ObjectMapper mapper = new ObjectMapper();
    private static Map<String, Object> componentMap = new HashMap<String, Object>();
    public static boolean archiveSIRDataInProgress;
    
    static {
        sirComponentIndices.add(SearchConstants.INDEX_NAME_PB_NOTES);
        sirComponentIndices.add(SearchConstants.INDEX_NAME_PB_ARTIFACTS);
        sirComponentIndices.add(SearchConstants.INDEX_NAME_PB_ATTACHMENT);
        sirComponentIndices.add(SearchConstants.INDEX_NAME_PB_AUDIT_LOG);
    }
    
    public static void startArchive () throws Exception {
    	if (archiveSIRDataInProgress) {
    		throw new Exception("Previous run of Archive SIR Data is still in progress, please try later...");
    	}
    	
    	long methodStartTime = Log.start("Starting Backup of newly created or updated SIR Data: ", Level.INFO);
    	
    	archiveSIRDataInProgress = true;
    	
    	try {
	        for (String index : sirComponentIndices) {
	            archiveSirModel(index);
	        }
	        
	        componentMap.clear();
    	} finally {
    		archiveSIRDataInProgress = false;
    	}
    	
    	Log.duration("Completed Backup of newly created or updated SIR Data: ", Level.INFO, methodStartTime);
    }
    
    private static void archiveSirModel(String index) throws Exception
    {
        long count = SearchAdminAPI.getClient().prepareSearch(index)
                        .setSize(0).get().getHits().getTotalHits();
        
        /*
         * HP TODO This has to use ES scroll API if modified documents count 
         * in non daily index/aliases is more than 10000 between 2 archive runs.
         * 
         * Current assumption is that for SIR volume this will not be the case 
 	     * and hence just check the size and if more than 10000 then
 	     * error out.
         * 
         * Another assumption is none of the document in these SIR indices
         * will ever be updated in bulk during update/upgrade causing above
         * limit of 10000 modifications to breach.
         */
        
        if (count > ConfigArchive.MAX_BLOCK_SIZE) {
        	String errMsg = String.format("Number of documents in SIR index %s modified since last archive run is %d" +
        								  " which is more than expected max of %d. Application needs to use ES scroll " +
        								  "APIs in %s.%s in file %s to process such large number of modified documents.",
        								  index, count, ConfigArchive.MAX_BLOCK_SIZE, 
        								  Thread.currentThread().getStackTrace()[1].getClassName(),
        								  Thread.currentThread().getStackTrace()[1].getMethodName(),
        								  Thread.currentThread().getStackTrace()[1].getFileName());
        	Log.log.error(errMsg);
        	throw new Exception(errMsg);
        }
        
        SearchRequestBuilder srb = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
        srb.setIndices(index);
        srb.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
        srb.setSize((int) count);
        
        srb.setQuery(QueryBuilders.termQuery("modified", "true"));
        SearchResponse response = srb.execute().actionGet();
        
        if (response != null) 
        {
            SearchHit[] result = response.getHits().getHits();
            if (result != null && result.length > 0)
            {
                long start = new Date().getTime(); 
                for (SearchHit hit : result)
                {
                    Map<String, Object> activityMap = hit.getSourceAsMap();
                    
                    Map<String, Object> tempMap = new HashMap<String, Object>();
                    tempMap.putAll(activityMap);
                    tempMap.put("modified", false);
                    
                    massageMapForSQL(activityMap, null);
                    
                    mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                    
                    switch (index)
                    {
                        case SearchConstants.INDEX_NAME_PB_NOTES :
                        {
                            massageMapForSQL(activityMap, new String[] {"category","auditMessage"});
                            ArchiveSirNote archiveNote = mapper.convertValue(activityMap, ArchiveSirNote.class);
                            if (archiveNote.getSys_id().length() > 32)
                            {
                                archiveNote.setSys_id(archiveNote.getSys_id().substring(0, 32));
                            }
                            if (persistSirModel(archiveNote))
                            {
                                indexSirNote(tempMap, mapper);
                            }
                            break;
                        }
                        case SearchConstants.INDEX_NAME_PB_ARTIFACTS :
                        {
                            massageMapForSQL(activityMap, new String[] {"activityName"});
                            ArchiveSirArtifact archiveArtifact = mapper.convertValue(activityMap, ArchiveSirArtifact.class);
                            if (persistSirModel(archiveArtifact))
                            {
                                indexSirArtifact(tempMap, mapper);
                            }
                            break;
                        }
                        case SearchConstants.INDEX_NAME_PB_ATTACHMENT :
                        {
                            ArchiveSirAttachment archiveAttachment = mapper.convertValue(activityMap, ArchiveSirAttachment.class);
                            if (persistSirModel(archiveAttachment))
                            {
                                indexSirAttachment(tempMap, mapper);
                            }
                            break;
                        }
                        case SearchConstants.INDEX_NAME_PB_AUDIT_LOG :
                        {
                            massageMapForSQL(activityMap, new String[] {"summary", "worksheetId"});
                            ArchiveSirAuditLog archiveAuditlog = mapper.convertValue(activityMap, ArchiveSirAuditLog.class);
                            if (persistSirModel(archiveAuditlog))
                            {
                                indexSirAuditlog(tempMap, mapper);
                            }
                            break;
                        }
                    }
                }
                
                Log.log.info(String.format("%d %s record(s) got inserted in %d milliseconds", count, index, (new Date().getTime() - start)));
            }
        }
    }
    
    private static void massageMapForSQL(Map<String, Object> activityMap, String[] columnsToRemove)
    {
        if (columnsToRemove == null)
        {
            activityMap.remove("sysTtl");
            activityMap.remove("sysCreatedDt");
            activityMap.remove("sysUpdatedDt");
            activityMap.remove("id");
            
            activityMap.put("sys_id", activityMap.get("sysId"));
            activityMap.remove("sysId");
            activityMap.remove("auditMessage");
        }
        else
        {
            for (String column : columnsToRemove)
            {
                activityMap.remove(column);
            }
        }
    }
    
    /*
     * Persist SIR model from ES to SQL
     */
    private static boolean persistSirModel(Object sirArchiveModel)
    {
        boolean result = false;
        try
        { 
          HibernateProxy.setCurrentUser("admin");
        	HibernateProxy.execute(() -> {
        		HibernateUtil.getCurrentSession().replicate("", sirArchiveModel, ReplicationMode.OVERWRITE);
        	});
            
            result = true;
        }
        catch(Exception ex)
        {
            Log.log.error("Error while archiving SIR model.", ex);
                      HibernateUtil.rethrowNestedTransaction(ex);
        }
        
        return result;
    }
    
    public static void deleteData(List<String> idList, String component)
    {
        if (StringUtils.isNotBlank(component))
        {
            switch (component)
            {
                case "artifact" :
                {
                    deleteSirModelRecords(idList, "ArchiveSirArtifact");
                    break;
                }
                
                case "attachment" : 
                {
                    deleteSirModelRecords(idList, "ArchiveSirAttachment");
                    break;
                }
            }
        }
    }
    
    private static void deleteSirModelRecords(List<String> idList, String model)
    {
        if (idList != null && idList.size() > 0)
        {
            StringBuilder ids = new StringBuilder();
            for (String id : idList)
            {
                ids.append("'").append(id).append("',");
            }
            ids.append("''");
            
            String hql = "delete from " + model + " where sys_id in (" + ids + ")";
            
            try
            {
              HibernateProxy.setCurrentUser("admin");
            	HibernateProxy.execute(() -> {
            		HibernateUtil.createQuery(hql).executeUpdate();
            	});
            }
            catch(Exception ex)
            {
                Log.log.error("Error while archiving SIR model.", ex);
                              HibernateUtil.rethrowNestedTransaction(ex);
            }
        }
    }
    
    public static void startRestore() throws Exception
    {
//        restoreSirModel("ArchiveSirActivity");
        restoreSirModel("ArchiveSirNote");
        restoreSirModel("ArchiveSirArtifact");
        restoreSirModel("ArchiveSirAttachment");
        restoreSirModel("ArchiveSirAuditLog");
    }
    /*
     * Restores SIR Activities from SQL to ES
     */
    @SuppressWarnings("unchecked")
    private static void restoreSirModel(String model) throws Exception
    {
        long start = new Date().getTime();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        
        List<Object> sirModels = null;
        String hql = "from " + model;
        try
        {
            
          HibernateProxy.setCurrentUser("admin");
            sirModels = (List<Object>)  HibernateProxy.execute(() -> {
            	return GeneralHibernateUtil.executeHQLSelect(hql, 1, 0);
            });
        }
        catch(Exception ex)
        {
            Log.log.error("Could not read activities for restoring.", ex);
                      HibernateUtil.rethrowNestedTransaction(ex);
        }
        
        if (sirModels != null && sirModels.size() > 0)
        {
            Map<String, Object> modelMap = null;
            for (Object sirModel : sirModels)
            {
                
                modelMap = getSirModelMap(model, sirModel); //new ObjectMapper().convertValue(archiveActivity, Map.class);
                switch(model)
                {
//                    case "ArchiveSirActivity" :
//                    {
//                        indexSirActivity(modelMap, mapper);
//                        break;
//                    }
                    case "ArchiveSirNote" :
                    {
                        indexSirNote(modelMap, mapper);
                        break;
                    }
                    case "ArchiveSirArtifact" :
                    {
                        indexSirArtifact(modelMap, mapper);
                        break;
                    }
                    case "ArchiveSirAttachment" :
                    {
                        indexSirAttachment(modelMap, mapper);
                        break;
                    }
                    case "ArchiveSirAuditLog" :
                    {
                        indexSirAuditlog(modelMap, mapper);
                        break;
                    }
                }
            }
        }
        
        Log.log.info(String.format("%d %s record(s) got inserted in %d milliseconds", sirModels.size(), model, (new Date().getTime() - start)));
    }
    
    private static void indexSirNote(Map<String, Object> modelMap, ObjectMapper mapper) throws Exception
    {
        PBNotes note = mapper.convertValue(modelMap, PBNotes.class);
        PlaybookIndexAPI.indexPBNotes(note, "admin");
    }
    
    private static void indexSirArtifact(Map<String, Object> modelMap, ObjectMapper mapper) throws Exception
    {
        PBArtifacts artifact = mapper.convertValue(modelMap, PBArtifacts.class);
        PlaybookIndexAPI.indexPBArtifacts(artifact, "admin");
    }
    
    private static void indexSirAttachment(Map<String, Object> modelMap, ObjectMapper mapper) throws Exception
    {
        PBAttachments attachment = mapper.convertValue(modelMap, PBAttachments.class);
        PlaybookIndexAPI.indexPBAttachment(attachment, "admin");
    }
    
    private static void indexSirAuditlog(Map<String, Object> modelMap, ObjectMapper mapper) throws SearchException
    {
        PBAuditLog auditLog = mapper.convertValue(modelMap, PBAuditLog.class);
        PlaybookIndexAPI.indexPBAuditLog(auditLog, "admin");
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static Map<String, Object> getSirModelMap(String model, Object sirModel)
    {
        Map<String, Object> modelMap = null;
        
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        
        modelMap = mapper.convertValue(sirModel, Map.class);
        
        massageMapForES(modelMap, (BaseModel)sirModel);
        
        return modelMap;
    }
    
    @SuppressWarnings("rawtypes")
    private static void massageMapForES(Map<String, Object> arcitityMap, BaseModel archiveActivity)
    {
        arcitityMap.put("sysCreatedDt", archiveActivity.getSysCreatedOn());
        arcitityMap.put("sysUpdatedDt", archiveActivity.getSysUpdatedOn());
        arcitityMap.put("sysId", arcitityMap.get("sys_id"));
        arcitityMap.put("id", arcitityMap.get("sys_id"));
        arcitityMap.remove("sys_id");
    }
    
    /*
     * API to be called from update operation ONLY.
     * It will set all SIR components as modified so, during next run of the backup,
     * all will be picked up for backup and will be marked as 'un'modified after that.
     */
    public static void reindexSirComponents() throws SearchException
    {
        for (String index : sirComponentIndices)
        {
            long count = SearchAdminAPI.getClient().prepareSearch(index).setTypes(index)
                            .setSize(0).get().getHits().getTotalHits();
            
            SearchRequestBuilder srb = new SearchRequestBuilder(SearchAdminAPI.getClient(), SearchAction.INSTANCE);
            srb.setIndices(index).setTypes(index);
            srb.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
            srb.setSize((int) count);
            
            SearchResponse response = srb.execute().actionGet();
            
            if (response != null)
            {
                SearchHit[] result = response.getHits().getHits();
                if (result != null && result.length > 0)
                {
                    ObjectMapper mapper = new ObjectMapper();
                    //mapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                    
                    List<PBNotes> notes = new ArrayList<PBNotes>();
                    List<PBArtifacts> artifacts = new ArrayList<PBArtifacts>();
                    List<PBAttachments> attachments = new ArrayList<PBAttachments>();
                    List<PBAuditLog> auditLogs = new ArrayList<PBAuditLog>();
                    
                    for (SearchHit hit : result)
                    {
                        Map<String, Object> modelMap = hit.getSourceAsMap();
                        modelMap.put("modified", true);
                        
                        switch (index)
                        {
                            case SearchConstants.INDEX_NAME_PB_NOTES :
                            {
                                PBNotes model = mapper.convertValue(modelMap, PBNotes.class);
                                notes.add(model);
                                
                                break;
                            }
                            case SearchConstants.INDEX_NAME_PB_ARTIFACTS :
                            {
                                PBArtifacts model = mapper.convertValue(modelMap, PBArtifacts.class);
                                artifacts.add(model);
                                
                                break;
                            }
                            case SearchConstants.INDEX_NAME_PB_ATTACHMENT :
                            {
                                PBAttachments model = mapper.convertValue(modelMap, PBAttachments.class);
                                attachments.add(model);
                                
                                break;
                            }
                            case SearchConstants.INDEX_NAME_PB_AUDIT_LOG :
                            {
                                PBAuditLog model = mapper.convertValue(modelMap, PBAuditLog.class);
                                auditLogs.add(model);
                                
                                break;
                            }
                        }
                    }
                    
                    switch (index)
                    {
                        case SearchConstants.INDEX_NAME_PB_NOTES :
                        {
                            APIFactory.getPBNotesIndexAPI().indexDocuments(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, notes, false, "admin");
                            Log.log.info("All SIR Notes are marked as modified.");
                            break;
                        }
                        case SearchConstants.INDEX_NAME_PB_ARTIFACTS :
                        {
                            APIFactory.getPBArtifactsIndexAPI().indexDocuments(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, artifacts, false, "admin");
                            Log.log.info("All SIR Artifacts are marked as modified.");
                            break;
                        }
                        case SearchConstants.INDEX_NAME_PB_ATTACHMENT :
                        {
                            APIFactory.getPBAttachmentIndexAPI().indexDocuments(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, attachments, false, "admin");
                            Log.log.info("All SIR Attachments are marked as modified.");
                            break;
                        }
                        case SearchConstants.INDEX_NAME_PB_AUDIT_LOG :
                        {
                            APIFactory.getPBAuditLogIndexAPI().indexDocuments(SearchConstants.SYS_ID, SearchConstants.SYS_CREATED_ON, auditLogs, false, "admin");
                            Log.log.info("All SIR Auditlogs are marked as modified.");
                            break;
                        }
                    }
                }
            }
        }
    }
}
