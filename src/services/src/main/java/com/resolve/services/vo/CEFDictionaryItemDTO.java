package com.resolve.services.vo;

public class CEFDictionaryItemDTO {
	private String UShortName;
	private String UFullName;
	private String UDataType;
	private Integer ULength;
	private String UDescription;

	public String getUShortName() {
		return UShortName;
	}

	public void setUShortName(String uShortName) {
		UShortName = uShortName;
	}

	public String getUFullName() {
		return UFullName;
	}

	public void setUFullName(String uFullName) {
		UFullName = uFullName;
	}

	public String getUDataType() {
		return UDataType;
	}

	public void setUDataType(String uDataType) {
		UDataType = uDataType;
	}

	public Integer getULength() {
		return ULength;
	}

	public void setULength(Integer uLength) {
		ULength = uLength;
	}

	public String getUDescription() {
		return UDescription;
	}

	public void setUDescription(String uDescription) {
		UDescription = uDescription;
	}
}
