/******************************************************************************
* (C) Copyright 2018
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.sir.util;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.hibernate.Hibernate;

import com.resolve.persistence.model.ResolveSecurityIncident;
import com.resolve.persistence.model.SIRPhaseMetaData;
import com.resolve.persistence.model.SIRPhaseRuntimeData;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.PlaybookActivityUtils;
import com.resolve.services.hibernate.util.PropertiesUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.OrgsVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.sir.McAfeeSIRPhasesEnum;
import com.resolve.services.sir.NISTSIRPhasesEnum;
import com.resolve.util.Constants;
import com.resolve.util.GMTDate;
import com.resolve.util.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import static com.resolve.services.interfaces.VO.*;
import static com.resolve.services.util.UserUtils.*;
import static com.resolve.util.Log.*;
import static com.resolve.util.StringUtils.*;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * SIR Phase Utilities.
 * 
 * @author hemant.phanasgaonkar
 *
 */
public class PhaseUtils {
    
    public static final String RESOLVE_PHASE_STANDARDS = isNotBlank(PropertiesUtil.getPropertyString(Constants.SIR_PHASES_STANDARDS_SYSTEM_PROPERTY)) ?
                                                         PropertiesUtil.getPropertyString(Constants.SIR_PHASES_STANDARDS_SYSTEM_PROPERTY) : NISTSIRPhasesEnum.NIST_STANDARDS;
    
    private static final String PERSISTED_STANDARD_PHASES_LOG = "Persisted Standard %s Phase: %s with persistence id %s.";
    private static final String PHASE_IS_CUSTOM_PHASE_LOG = "Phase : [%s] is Custom phase.";
    private static final String PHASE_IS_STANDARDS_PHASE_LOG = "Phase : [%s] is standards phase.";
    private static final String UPSERTED_PHASE_RUNTIME_DATA_LOG = "Upserted Phase Runtime Data: %s.";
    private static final String FAILED_TO_FIND_PHASE_METADATA_FOR_PHASE_ID_LOG = 
    																"Failed to find phase meta data for meta phase id %s.";
    
    // Persist standard phases only if not already persisted

	static {

		if (RESOLVE_PHASE_STANDARDS.equals(NISTSIRPhasesEnum.NIST_STANDARDS)) {
			NISTSIRPhasesEnum[] nistSIRPahseEnums = NISTSIRPhasesEnum.values();

			try {
         HibernateProxy.setCurrentUser(SYSTEM);
				HibernateProxy.execute(() -> {
					for (int i = 0; i < nistSIRPahseEnums.length; i++) {

						SIRPhaseMetaData persistedSIRNISTPhaseMetaData = HibernateUtil.getDAOFactory()
								.getSIRPhaseMetaDataDAO().findById(nistSIRPahseEnums[i].getPersistenceId());

						if (persistedSIRNISTPhaseMetaData == null) {
							SIRPhaseMetaData sirNISTPhaseMetaData = setupSIRPhaseMetaData(
									nistSIRPahseEnums[i].getPhaseName(), null, NISTSIRPhasesEnum.NIST_STANDARDS,
									nistSIRPahseEnums[i].getPersistenceId());

							persistedSIRNISTPhaseMetaData = HibernateUtil.getDAOFactory().getSIRPhaseMetaDataDAO()
									.save(sirNISTPhaseMetaData);

							log.info(String.format(PERSISTED_STANDARD_PHASES_LOG, NISTSIRPhasesEnum.NIST_STANDARDS,
									persistedSIRNISTPhaseMetaData, persistedSIRNISTPhaseMetaData.getSys_id()));
						}
					}
				});
			} catch (Throwable t) {
				log.error(t.getMessage(), t);
                               HibernateUtil.rethrowNestedTransaction(t);
			}
		} else if (RESOLVE_PHASE_STANDARDS.equals(McAfeeSIRPhasesEnum.MCAFEE_STANDARDS)) {
			McAfeeSIRPhasesEnum[] macafeeSIRPahseEnums = McAfeeSIRPhasesEnum.values();

			try {
         HibernateProxy.setCurrentUser(SYSTEM);
				HibernateProxy.execute(() -> {
					for (int i = 0; i < macafeeSIRPahseEnums.length; i++) {

						SIRPhaseMetaData persistedSIRMcAfeePhaseMetaData = HibernateUtil.getDAOFactory().getSIRPhaseMetaDataDAO()
								.findById(macafeeSIRPahseEnums[i].getPersistenceId());

						if (persistedSIRMcAfeePhaseMetaData == null) {
							SIRPhaseMetaData sirMcAfeePhaseMetaData = setupSIRPhaseMetaData(
									macafeeSIRPahseEnums[i].getPhaseName(), null, McAfeeSIRPhasesEnum.MCAFEE_STANDARDS,
									macafeeSIRPahseEnums[i].getPersistenceId());

							persistedSIRMcAfeePhaseMetaData = HibernateUtil.getDAOFactory().getSIRPhaseMetaDataDAO()
									.save(sirMcAfeePhaseMetaData);

							log.info(String.format(PERSISTED_STANDARD_PHASES_LOG, McAfeeSIRPhasesEnum.MCAFEE_STANDARDS,
									persistedSIRMcAfeePhaseMetaData, persistedSIRMcAfeePhaseMetaData.getSys_id()));
						}
					}
				});
			} catch (Throwable t) {
				log.error(t.getMessage(), t);
                               HibernateUtil.rethrowNestedTransaction(t);
			}
		}
	}
    
    public static SIRPhaseMetaData createSIRPhaseMetaData(String phaseName, String orgId, String username) throws Exception {
        SIRPhaseMetaData sirPhaseMetaData = null;
        
        if (isNotBlank(phaseName)) {
            if (!isStandardsPhase(phaseName)) {
                log.debug(String.format(PHASE_IS_CUSTOM_PHASE_LOG, phaseName));
                
                // Check user's access to specified Org including None
                
                if (UserUtils.isOrgAccessible2(orgId, username)) {
                    sirPhaseMetaData = findSIRPhaseMetaData(phaseName, orgId, username);
                    
                    if (sirPhaseMetaData == null) {
                        sirPhaseMetaData = createCustomSIRPhaseMetaData(phaseName, orgId, username);
                    }
                } else {
                    throw new Exception("User does not have access to " + 
                                        (isNotBlank(orgId) ? "specified" : OrgsVO.NONE_ORG_NAME) + " Org.");
                }
            } else {
                log.debug(String.format(PHASE_IS_STANDARDS_PHASE_LOG,phaseName));
                
                String standardPhaseId = null;
                
                if (RESOLVE_PHASE_STANDARDS.equals(NISTSIRPhasesEnum.NIST_STANDARDS)) {
                    List<NISTSIRPhasesEnum> phaseEnumList = Arrays.asList(NISTSIRPhasesEnum.values());
                    
                    NISTSIRPhasesEnum matchingPahseEnum = phaseEnumList.stream()
                                                          .filter(phaseEnum -> (phaseEnum.getPhaseName().equalsIgnoreCase(phaseName) ||
                                                                                phaseEnum.getUIPhaseName().equalsIgnoreCase(phaseName)))
                                                          .findAny()
                                                          .orElse(null);
                    
                    if (matchingPahseEnum != null) {
                        standardPhaseId = matchingPahseEnum.getPersistenceId();
                    }
                } else if (RESOLVE_PHASE_STANDARDS.equals(McAfeeSIRPhasesEnum.MCAFEE_STANDARDS)) {
                    List<McAfeeSIRPhasesEnum> phaseEnumList = Arrays.asList(McAfeeSIRPhasesEnum.values());
                    
                    McAfeeSIRPhasesEnum matchingPahseEnum = phaseEnumList.stream()
                                                            .filter(phaseEnum -> phaseEnum.getPhaseName().equalsIgnoreCase(phaseName))
                                                            .findAny()
                                                            .orElse(null);
                    
                    if (matchingPahseEnum != null) {
                        standardPhaseId = matchingPahseEnum.getPersistenceId();
                    }
                }
                
                if (isNotBlank(standardPhaseId)) {
                    try {
                    	String finalStandardPhaseId = standardPhaseId;
                          HibernateProxy.setCurrentUser(username);
                    	sirPhaseMetaData = (SIRPhaseMetaData) HibernateProxy.execute(() -> {
                    		return HibernateUtil.getDAOFactory().getSIRPhaseMetaDataDAO().findById(finalStandardPhaseId);
                    	});
                    } catch (Throwable t) {
                        log.error(t.getMessage(), t);
                                                                    HibernateUtil.rethrowNestedTransaction(t);
                    }
                }
            }
        }
        
        return sirPhaseMetaData;
    }
    
    public static boolean isStandardsPhase(String phaseName) {
        
        boolean isStandardPhase = false;
                
        if (RESOLVE_PHASE_STANDARDS.equals(NISTSIRPhasesEnum.NIST_STANDARDS)) {            
            List<NISTSIRPhasesEnum> phaseEnumList = Arrays.asList(NISTSIRPhasesEnum.values());
            
            NISTSIRPhasesEnum matchingPahseEnum = phaseEnumList.stream()
                                                  .filter(phaseEnum -> (phaseEnum.getPhaseName().equalsIgnoreCase(phaseName) ||
                                                                        phaseEnum.getUIPhaseName().equalsIgnoreCase(phaseName)))
                                                  .findAny()
                                                  .orElse(null);
            
            isStandardPhase = matchingPahseEnum != null;
        } else if (RESOLVE_PHASE_STANDARDS.equals(McAfeeSIRPhasesEnum.MCAFEE_STANDARDS)) {
            List<McAfeeSIRPhasesEnum> phaseEnumList = Arrays.asList(McAfeeSIRPhasesEnum.values());
            
            McAfeeSIRPhasesEnum matchingPahseEnum = phaseEnumList.stream()
                                                    .filter(phaseEnum -> phaseEnum.getPhaseName().equalsIgnoreCase(phaseName))
                                                    .findAny()
                                                    .orElse(null);
            
            isStandardPhase = matchingPahseEnum != null;
        }
        
        return isStandardPhase;
    }
    
    private static SIRPhaseMetaData setupSIRPhaseMetaData(String phaseName, String orgId, String source) {
        return setupSIRPhaseMetaData(phaseName, orgId, source, null);
    }
    
    private static SIRPhaseMetaData setupSIRPhaseMetaData(String phaseName, String orgId, String source, String persistenceId) {
        
        SIRPhaseMetaData sirPhaseMetaData = new SIRPhaseMetaData();
        
        if (isNotBlank(phaseName)) {
        	sirPhaseMetaData.setName(phaseName);
        }
        
        sirPhaseMetaData.setSource(source);
        
        if (isNotBlank(orgId)) {
            sirPhaseMetaData.setSysOrg(orgId);
        }
        
        if (persistenceId != null/* && persistenceIds.length >= 1*/) {
            sirPhaseMetaData.setSys_id(persistenceId/*s[0]*/);
        }
            
        return sirPhaseMetaData;
    }
    
    public static SIRPhaseMetaData findSIRPhaseMetaData(String phaseName, String orgId, String username) {
        
        SIRPhaseMetaData sirPhaseMetaData = null;
        
        SIRPhaseMetaData example = setupSIRPhaseMetaData(phaseName, orgId, SIRPhaseMetaData.CUSTOM_SIR_PHASE_SOURCE);
                
        try {
              HibernateProxy.setCurrentUser(username);
        	sirPhaseMetaData = (SIRPhaseMetaData) HibernateProxy.execute(() -> {
        		return HibernateUtil.getDAOFactory().getSIRPhaseMetaDataDAO().findFirst(example);
        	});
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
                                            HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return sirPhaseMetaData;
    }
    
    public static SIRPhaseMetaData createCustomSIRPhaseMetaData(String phaseName, String orgId, String username) {
        
        SIRPhaseMetaData sirPhaseMetaData = setupSIRPhaseMetaData(phaseName, orgId, SIRPhaseMetaData.CUSTOM_SIR_PHASE_SOURCE);
        SIRPhaseMetaData newSIRPhaseMetaData = null;
        
        try {
              HibernateProxy.setCurrentUser(username);
        	newSIRPhaseMetaData = (SIRPhaseMetaData) HibernateProxy.execute(() -> {
        		return HibernateUtil.getDAOFactory().getSIRPhaseMetaDataDAO().insert(sirPhaseMetaData);
	            /*
	             * Persist phase meta data record immediately irrespective of success/failure of transactions 
	             * enclosing this transaction (if any). This avoids duplicate phase meta data (i.e. same phase name) 
	             * getting created when new phase name is repeated in more than one phase in
	             * SIR template.    
	             */
        	});
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
                                            HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return newSIRPhaseMetaData;
    }
    
    public static void createSIRPhasesRuntimeData(ResolveSecurityIncident incident, String username) throws Exception {
        JSONArray activityList = PlaybookActivityUtils.getPbActivities(incident.getSys_id(), incident.getPlaybook(), 
                                                                       incident.getPlaybookVersion(), incident.getSysOrg(),
                                                                       username);
        
        if (activityList != null && activityList.size() > 0) {
            
            try {
                  HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
            		int currPhaseIndex = 0;
            		String currMetaPhaseId = null;

            		for (int i=0; i<activityList.size(); i++) {
	                    JSONObject jsonActivity = (JSONObject)activityList.get(i);
	                    
	                    if (jsonActivity.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY) &&
	                        StringUtils.isNotBlank(jsonActivity.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY)) &&
	                        jsonActivity.containsKey(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY) &&
	                        StringUtils.isNotBlank(jsonActivity.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_ACTIVITY_ID_KEY)))
	                    {
	                        if (!jsonActivity.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY).
	                             equals(currMetaPhaseId))
	                        {
	                            currPhaseIndex++;
	                            currMetaPhaseId = jsonActivity.getString(WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY);
	                        }
	                        
	                        SIRPhaseRuntimeData dbSIRPhaseRuntimeData = 
	                                        findSIRPhaseRuntimeData(
	                                                        jsonActivity.getString(
	                                                                WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY),
	                                                         null,
	                                                         incident.getSys_id(),
	                                                         incident.getSysOrg(),
	                                                         username);
	                        
	                        boolean upserted = false;
	                        
	                        if (dbSIRPhaseRuntimeData != null) {
	                            if (!dbSIRPhaseRuntimeData.getPosition().equals(Integer.valueOf(currPhaseIndex))) {
	                                upserted = true;
	                                dbSIRPhaseRuntimeData.setPosition(Integer.valueOf(currPhaseIndex));
	                                
	                                dbSIRPhaseRuntimeData.setSysUpdatedBy(username);
	                                dbSIRPhaseRuntimeData.setSysUpdatedOn(GMTDate.getDate());
	                                dbSIRPhaseRuntimeData.setSysModCount(Integer.valueOf(
	                                                                        dbSIRPhaseRuntimeData.getSysModCount().intValue() + 1));
	                            }
	                        } else {
	                            upserted = true;
	                            dbSIRPhaseRuntimeData = 
	                                            setupSIRPhaseRuntimeData(incident,
	                                                                     jsonActivity.getString(
	                                                            WikiDocumentVO.PLAYBOOK_ACTIVITY_MAP_META_PHASE_ID_KEY),
	                                                                     Integer.valueOf(currPhaseIndex));                            
	                        }
	                        
	                        if (upserted) {
	                            SIRPhaseRuntimeData upsertedSIRPhaseRuntimeData = 
	                                        HibernateUtil.getDAOFactory().getSIRPhaseRuntimeDataDAO().insertOrUpdate(
	                                                        dbSIRPhaseRuntimeData);
	                            
	                            log.debug(String.format(UPSERTED_PHASE_RUNTIME_DATA_LOG, upsertedSIRPhaseRuntimeData));
	                        }
	                    }
	                }
                
            	});
            } catch (Throwable t) {
                log.error(t.getMessage(), t);
                                                    HibernateUtil.rethrowNestedTransaction(t);
            }
        }
    }
    
    public static SIRPhaseRuntimeData setupSIRPhaseRuntimeData(SIRPhaseMetaData eSPMD, 
                                                               ResolveSecurityIncident eRSI,
                                                               Integer sirPhaseIndex,
                                                               String orgId) {
        SIRPhaseRuntimeData sirPhaseRuntimeData = new SIRPhaseRuntimeData();

        if (eSPMD != null) {
            sirPhaseRuntimeData.setPhaseMetaData(eSPMD);
        }
        
        sirPhaseRuntimeData.setReferencedRSI(eRSI);

        if (sirPhaseIndex != null && sirPhaseIndex > NON_NEGATIVE_INTEGER_DEFAULT) {
            sirPhaseRuntimeData.setPosition(sirPhaseIndex);
        }

        if (isNotBlank(orgId)) {
            sirPhaseRuntimeData.setSysOrg(orgId);
        }

        return sirPhaseRuntimeData;
    }
    
    private static SIRPhaseRuntimeData setupSIRPhaseRuntimeData(ResolveSecurityIncident incident,
                                                                String sirPhaseMetaDataId,
                                                                Integer sirPhaseIndex)  throws Exception {
    	SIRPhaseMetaData sPMD = HibernateUtil.getDAOFactory().getSIRPhaseMetaDataDAO().findById(sirPhaseMetaDataId);
	        
        if (sPMD == null) {
            log.error(String.format(FAILED_TO_FIND_PHASE_METADATA_FOR_PHASE_ID_LOG, sirPhaseMetaDataId));
            throw new Exception("Failed to find phase meta data by meta phase id.");
        }
        
        return setupSIRPhaseRuntimeData(sPMD, incident, sirPhaseIndex, incident.getSysOrg());
    }
    
    public static SIRPhaseRuntimeData findSIRPhaseRuntimeData(String sirPhaseMetDataId, Integer sirPhaseIndex, 
                                                              String incidentId, String orgId, String username)
    {
        SIRPhaseRuntimeData dbSIRPhaseRuntimeData = null;

        SIRPhaseMetaData eSPMD = null;
        
        if (isNotBlank(sirPhaseMetDataId)) {
            eSPMD = new SIRPhaseMetaData();
            eSPMD.setSysOrg(orgId);
            eSPMD.setSys_id(sirPhaseMetDataId);
        }
        
        ResolveSecurityIncident eRSI = new ResolveSecurityIncident();
        eRSI.setSysOrg(orgId);
        eRSI.setSys_id(incidentId);

        SIRPhaseRuntimeData example = setupSIRPhaseRuntimeData(eSPMD, eRSI, sirPhaseIndex, orgId);

		try {
        HibernateProxy.setCurrentUser(username);
			dbSIRPhaseRuntimeData = (SIRPhaseRuntimeData) HibernateProxy.execute(() -> {

				if (sirPhaseIndex != null && sirPhaseIndex.intValue() > NON_NEGATIVE_INTEGER_DEFAULT) {
					return HibernateUtil.getDAOFactory().getSIRPhaseRuntimeDataDAO().findFirst(example);
				} else {
					return HibernateUtil.getDAOFactory().getSIRPhaseRuntimeDataDAO().findFirst(example, "position");
				}

			});
			
            if (dbSIRPhaseRuntimeData != null) {
                // Load lazyily initialized objects
                Hibernate.initialize(dbSIRPhaseRuntimeData.getPhaseMetaData());
                Hibernate.initialize(dbSIRPhaseRuntimeData.getReferencedRSI());
            }
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
                                            HibernateUtil.rethrowNestedTransaction(t);
        }

        return dbSIRPhaseRuntimeData;
    }
    
    @SuppressWarnings("unchecked")
	public static int getRuntimeDataPhaseCount(String incidentId, String orgId, String username) {
        
        List<SIRPhaseRuntimeData> sirRuntimeDataPhases = null;
        
        ResolveSecurityIncident referencedRSI = new ResolveSecurityIncident();
        
        referencedRSI.setSys_id(incidentId);
        referencedRSI.setPrimary(null);
        referencedRSI.setSirStarted(null);
        
        if (isNotBlank(orgId)) {
            referencedRSI.setSysOrg(orgId);
        }
        
        SIRPhaseRuntimeData example = new SIRPhaseRuntimeData();
        
        example.setReferencedRSI(referencedRSI);
        
        if (isNotBlank(orgId)) {
            example.setSysOrg(orgId);
        }
        
        try {
              HibernateProxy.setCurrentUser(username);
            sirRuntimeDataPhases = (List<SIRPhaseRuntimeData>) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getSIRPhaseRuntimeDataDAO().find(example, "position");
            });
                            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
                                            HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return sirRuntimeDataPhases != null ? sirRuntimeDataPhases.size() : 0;
    }
    
    public static boolean isStandardsPhaseByPersistenceId(String phasePersistenceId) {
        
        boolean isStandardPhase = false;
                
        if (RESOLVE_PHASE_STANDARDS.equals(NISTSIRPhasesEnum.NIST_STANDARDS)) {            
//            List<NISTSIRPhasesEnum> phaseEnumList = Arrays.asList(NISTSIRPhasesEnum.values());
            
            NISTSIRPhasesEnum matchingPahseEnum = /*phaseEnumList.stream()*/Stream.of(NISTSIRPhasesEnum.values()).parallel()
                                                  .filter(phaseEnum -> (phaseEnum.getPersistenceId().equalsIgnoreCase(phasePersistenceId)))
                                                  .findAny()
                                                  .orElse(null);
            
            isStandardPhase = matchingPahseEnum != null;
        } else if (RESOLVE_PHASE_STANDARDS.equals(McAfeeSIRPhasesEnum.MCAFEE_STANDARDS)) {
//            List<McAfeeSIRPhasesEnum> phaseEnumList = Arrays.asList(McAfeeSIRPhasesEnum.values());
            
            McAfeeSIRPhasesEnum matchingPahseEnum = /*phaseEnumList.stream()*/Stream.of(McAfeeSIRPhasesEnum.values()).parallel()
                                                    .filter(phaseEnum -> phaseEnum.getPersistenceId().equalsIgnoreCase(phasePersistenceId))
                                                    .findAny()
                                                    .orElse(null);
            
            isStandardPhase = matchingPahseEnum != null;
        }
        
        return isStandardPhase;
    }
}
