/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import java.util.List;
import java.util.Map;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWorksheet;
import com.resolve.util.ScriptDebug;
import com.resolve.util.XDoc;

/**
 * This class is an external facing facade.
 * 
 */
public class WorksheetUtils
{
    public final static String REFERENCE = "REFERENCE";
    public final static String ALERTID = "ALERTID";
    public final static String CORRELATIONID = "CORRELATIONID";
    public final static String PROBLEMID = "PROBLEMID";
    public final static String PROCESSID = "PROCESSID";
    public final static String NUMBER = "NUMBER";
    public final static String SERIAL = "SERIAL";
    public final static String IDENTIFIER = "IDENTIFIER";
    public final static String ASSIGNED_TO = "ASSIGNED_TO";
    public final static String WIKI = "WIKI";
    public final static String WORKSHEET_SUMMARY = "WORKSHEET_SUMMARY";
    public final static String WORKSHEET_DESCRIPTION = "WORKSHEET_DESCRIPTION";
    public final static String SIRID = "SIRID";

    @SuppressWarnings("rawtypes")
    public static String initWorksheet(String problemid, String reference, String alertid, String correlationid, String userid, String processid, boolean isEvent, Map params) throws Exception
    {
        return ServiceWorksheet.initWorksheet(problemid, reference, alertid, correlationid, userid, processid, isEvent, params);
    } // initWorksheet

    @Deprecated
    public static String getActiveWorksheet(String userid)
    {
        return ServiceHibernate.getActiveWorksheet(userid, null, null);
    } // getActiveWorksheet
    
    @Deprecated
    public static boolean isActiveArchiveWorksheet(String userid)
    {
        return ServiceHibernate.isActiveArchiveWorksheet(userid, null, null);
    }

    public static String setActiveWorksheet(String userid, String problemid, String orgId, String orgName) throws Exception
    {
        return ServiceHibernate.setActiveWorksheet(userid, problemid, orgId, orgName);
    } // setActiveWorksheet
    
    @Deprecated
    public static String setActiveWorksheet(String userid, String problemid) throws Exception
    {
        return ServiceHibernate.setActiveWorksheet(userid, problemid, null, null);
    } // setActiveWorksheet

    public static Map<String, String> getWorksheetReferencesCAS(String problemid) throws Exception
    {
        return ServiceWorksheet.getWorksheetReferences(problemid);
    } // getWorksheetReferenceCAS

    public static String getWorksheetByExampleCAS(Map<String, String> params)
    {
        return ServiceWorksheet.findWorksheetByColumnValues(params);
        
    }// getWorksheetByExampleCAS

    public static String getWorksheetByProcessID(String processId)
    {
        return ServiceWorksheet.getWorksheetIdByProcessId(processId);

    }// getWorksheetByProcessId

    public static String getWorksheetByExecuteID(String executeId)
    {
        throw new RuntimeException("This is obsolete method, change the calling code");
    } // getWorksheetByExecuteID

    public static void writeDebugCAS(ScriptDebug debug)
    {
        ServiceWorksheet.flushWorksheetDebug(debug);
    }

    /**
     * This method returns the task result in a worksheet.
     * 
     * @param sysId the worksheet's sysId. it takes precedence over the number below.
     * @param number the problem number (PRB number)
     * @param actionTasks array of full name (e.g., comment#resolve) to include in the response. null or empty array returns everything. 
     * @param username
     * @return
     * @throws Exception
     */
    public static List<Map<String, Object>> findTaskResultByWorksheet(String sysId, String number, String[] actionTasks, String username) throws Exception
    {
        return ServiceWorksheet.findTaskResultByWorksheet(sysId, number, actionTasks, username);
    }
    
    public static String getActiveWorksheet(String userid, String orgId, String orgName)
    {
        return ServiceHibernate.getActiveWorksheet(userid, orgId, orgName);
    } // getActiveWorksheet
    
    public static boolean isActiveArchiveWorksheet(String userid, String orgId, String orgName)
    {
        return ServiceHibernate.isActiveArchiveWorksheet(userid, orgId, orgName);
    }
} // WorksheetUtils
