package com.resolve.services.vo;

/**
 * The namespace response DTO is used to represent search response 
 * for namespaces or menupaths.
 */
public class NamespaceResponseDTO extends ResponseDTO<NamespaceResponseDTO> {
	
	private String name;
	
	private boolean leaf = false;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean isLeaf() {
		return leaf;
	}
	
	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}
	
}
