/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.migration.catalog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.codehaus.jackson.map.ObjectMapper;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.kernel.Traversal;

import com.resolve.persistence.model.ResolveCatalogNode;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceTag;
import com.resolve.services.catalog.Catalog;
import com.resolve.services.catalog.CatalogUtil;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.ResolveTagVO;
import com.resolve.services.migration.neo4j.GraphDBManager;
import com.resolve.services.migration.neo4j.SocialRelationshipTypes;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MigrateCatalogs
{

    public MigrateCatalogs()
    {

    }
    
    public static Collection<String> migrate()
    {
        MigrateCatalogs migrateCatalogs = new MigrateCatalogs();
        return migrateCatalogs.migrateInternal();

    }

    private Collection<String> migrateInternal()
    {
        Collection<String> pSysIds = new ArrayList<String>();
        
        try
        {
            //get all the catalogs
            List<ResolveCatalog> allCatalogs = getAllCatalogs();
            if(allCatalogs != null && allCatalogs.size() > 0)
            {
                Log.log.debug("Total # of Catalogs : " + allCatalogs.size());
                
                //for each catalog, persist in the SQL db
                for(ResolveCatalog graphCatalog : allCatalogs)
                {
                    try
                    {
                        graphCatalog = getCatalog(graphCatalog.getId());
                        
                        Catalog catalog = convertToCatalog(graphCatalog);
                        Catalog pCatalog = CatalogUtil.save(catalog, "admin");
                        
                        if(pCatalog != null)
                        {
                            pSysIds.add(pCatalog.getId());
                            WikiUtils.updateWikiDocCatalogRef(pCatalog.getParentSysId(), graphCatalog.getId());
                        }
                    }
                    catch (Exception e)
                    {
                        Log.log.error("Error for catalog:" + graphCatalog.getName(), e);
                    }
                }//end of for loop
                
                Log.log.debug("Total # of Catalogs Migrated : " + pSysIds.size());
                //update the relationships for catalog to catalog reference
                Set<String> sysIdsToUpdateReferences = GeneralHibernateUtil.getSysIdsFor("ResolveCatalogNode", "UType = 'CatalogReference' and UIsRootRef = true" , "system");
                if(sysIdsToUpdateReferences != null && sysIdsToUpdateReferences.size() > 0)
                {
                    Log.log.debug("Total # of Catalog to Catalog References : " + sysIdsToUpdateReferences.size());
                    for(String sysId : sysIdsToUpdateReferences)
                    {
                        try
                        {

                          HibernateProxy.setCurrentUser("system");
                        	HibernateProxy.execute(() -> {
                                ResolveCatalogNode model = HibernateUtil.getDAOFactory().getResolveCatalogNodeDAO().findById(sysId);
                                if(model != null)
                                {
                                    String path = model.getUPath();
                                    String refCatalogName = path.substring(path.lastIndexOf('/')+1);
                                    
                                    com.resolve.persistence.model.ResolveCatalog catalog = CatalogUtil.findCatalogModel(null, refCatalogName, "system");
                                    if(catalog != null)
                                    {
                                        model.setUReferenceCatalogSysId(catalog.getSys_id());
                                        
                                    }
                                }

                                
                        	});
                        }
                        catch (Exception e)
                        {
                            Log.log.warn(e.getMessage(), e);
                                                      HibernateUtil.rethrowNestedTransaction(e);
                        }
                    }//end of for loop
                    
                }//end of if
                
                
            }
            else
            {
                Log.log.debug("No Catalogs to Migrate.");
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in Catalog Migration", e);
        }

        return pSysIds;
    }

    private List<ResolveCatalog> getAllCatalogs() throws Exception
    {
        List<ResolveCatalog> results = new ArrayList<ResolveCatalog>();

        Node catalogRootNode = GraphDBManager.getInstance().findRootNodeFor(SocialRelationshipTypes.Catalog);
        if (catalogRootNode != null)
        {

            TraversalDescription trav = Traversal.description().breadthFirst()
                                        .relationships(SocialRelationshipTypes.Catalog, Direction.OUTGOING)
                                        .evaluator(Evaluators.excludeStartPosition());

            for (Node catalogNode : trav.traverse(catalogRootNode).nodes())
            {
                ResolveCatalog catalog = convertNodeToObject(catalogNode);
                results.add(catalog);
            }

        }

        return results;
    }
    
    private ResolveCatalog convertNodeToObject(Node catalogNode)  throws Exception
    {
        ResolveCatalog catalog = new ResolveCatalog();
        catalog.convertNodeToObject(catalogNode);
        
        //get the tags for this node
        List<ResolveTagVO> tags = getTagsForCatalogItemNode(catalog.getId());
        catalog.setTags(tags);
        
        //TODO: get the document ref displayType="wiki"
//        String displayType = catalog.getDisplayType();
        if (StringUtils.isNotBlank(catalog.getDisplayType()) 
                        && (catalog.getDisplayType().equalsIgnoreCase(ResolveCatalog.DISPLAY_TYPE_WIKI) || catalog.getDisplayType().equalsIgnoreCase(ResolveCatalog.DISPLAY_TYPE_LIST_OF_DOCUMENTS)))
        {
            String wiki = getDocumentFullNameReferedByCatalogItemNode(catalogNode);
            if(StringUtils.isNotBlank(wiki))
            {
                catalog.setWiki(wiki);
            }
        }
        
        return catalog;
    }
    
    private List<ResolveTagVO> getTagsForCatalogItemNode(String catalogNodeId) throws Exception
    {
        List<ResolveTagVO> tags = new ArrayList<ResolveTagVO>();
        
        if(StringUtils.isNotBlank(catalogNodeId))
        {
            Node catalogNode = GraphDBManager.getInstance().findNodeByIndexedID(catalogNodeId);
            if(catalogNode != null)
            {
                Iterable<Relationship> rels = catalogNode.getRelationships(SocialRelationshipTypes.TAG_OF_CAT);
                if (rels != null)
                {
                    for (Relationship rel : rels)
                    {
                        Node tagNode = rel.getOtherNode(catalogNode);
                        
//                        String sysId = (String) tagNode.getProperty(GraphDBManager.SYS_ID);
                        String tagName = (String) tagNode.getProperty(GraphDBManager.DISPLAYNAME);
                        try
                        {
                            ResolveTagVO tag = ServiceTag.getTag(null, tagName, "system");
                            if(tag != null)
                            {
                                tags.add(tag);
                            }
                        }
                        catch (Exception e)
                        {
                            Log.log.error("cannot find tag with name :" + tagName, e);
                        }
                    }
                }
            }
        }
        
        
        return tags;
    }
    
    
    private String getDocumentFullNameReferedByCatalogItemNode(Node catalogNode)
    {
        String docFullName = null;
        
        if(catalogNode != null)
        {
//            String path = (String) catalogNode.getProperty(ResolveCatalog.PATH);
            Iterable<Relationship> rels = catalogNode.getRelationships(SocialRelationshipTypes.CATALOG_DOCUMENT_REF);
            if(rels != null)
            {
                Set<String> docs = new HashSet<String>();
                for(Relationship rel : rels)
                {
                    Node docNode = rel.getOtherNode(catalogNode);
                    if(docNode != null && docNode.hasProperty(GraphDBManager.DISPLAYNAME))
                    {
                        docs.add((String) docNode.getProperty(GraphDBManager.DISPLAYNAME));
                    }
                }

                docFullName = StringUtils.convertCollectionToString(docs.iterator(), ",");
            }
        }

        return docFullName;
    }
    
    private Catalog convertToCatalog(ResolveCatalog graphCatalog) throws Exception
    {
        Catalog catalog = null;

        String json = new ObjectMapper().writeValueAsString(graphCatalog);
        catalog = new ObjectMapper().readValue(json, Catalog.class);
        
        return catalog;
    }
    
    
    private ResolveCatalog getCatalog(String sysID) throws Exception
    {
        ResolveCatalog cRoot = null;

        if (StringUtils.isNotEmpty(sysID))
        {
            Node catalogRootNode = GraphDBManager.getInstance().findNodeByIndexedID(sysID);

            if (catalogRootNode != null)
            {
                //catalog root
                cRoot = convertNodeToObject(catalogRootNode);

                if (cRoot.isRoot())
                {
                    TraversalDescription trave = Traversal.description()
                                    .breadthFirst()
                                    .relationships(SocialRelationshipTypes.CHILD_OF_CATALOG, Direction.INCOMING)
                                    .relationships(SocialRelationshipTypes.CATALOG_REF, Direction.INCOMING)
                                    .relationships(SocialRelationshipTypes.TAG_OF_CAT, Direction.INCOMING)
                                    .evaluator(Evaluators.excludeStartPosition())
                                    .evaluator(Evaluators.atDepth(1))
                                    .evaluator(new Evaluator()
                    {
                        
                        public Evaluation evaluate(Path path)
                        {
                            if (path.lastRelationship() != null && path.lastRelationship().getType().equals(SocialRelationshipTypes.CATALOG_REF))
                            {
                                return Evaluation.INCLUDE_AND_PRUNE;
                            }

                            return Evaluation.INCLUDE_AND_CONTINUE;
                        }
                    });

                    for (Node childNode : trave.traverse(catalogRootNode).nodes())
                    {
                        String childNodeDisplayName = (String) childNode.getProperty(ResolveCatalog.DISPLAYNAME);
                        String type = (String) childNode.getProperty(ResolveCatalog.TYPE);

//                        Log.log.info("-- the childNode -- " + childNodeDisplayName + " type:" + type);

                        if (StringUtils.isNotEmpty(type))
                        {
                            if (!type.equals(SocialRelationshipTypes.ResolveTag.name()))
                            {
                                ResolveCatalog childCatalog = convertNodeToObject(childNode);

                                // to get all child catalog
                                traverseChild(childNode, childCatalog, cRoot, trave);

                            }
//                            else if (type.equals(ResolveTag.NODE_TYPE.ResolveTag.name()))
//                            {
//                                ResolveTag tagLocal = new ResolveTag();
//                                tagLocal.convertNodeToObject(childNode);
//                                cRoot.getTags().add(tagLocal);
//
//                                Log.log.info("-- the  cRoot -- " + cRoot.getName() + "-- add tag of --" + tagLocal.getName());
//                            }
                        }
                    }
                }
            }
        }

        if (cRoot != null)
        {
            sortOnOrder(cRoot.getChildren());
        }
//        sortOnTag(cRoot.getTags());

//        Log.log.info("-- finished retrieve catalog " + ((cRoot == null) ? "" : cRoot.getName()));

        return cRoot;
    }
    
    private void traverseChild(Node childNode, ResolveCatalog childCatalog, ResolveCatalog parentCatalog, TraversalDescription travChild) throws Exception
    {

        List<ResolveCatalog> childChildCatalogList = new ArrayList<ResolveCatalog>();
        Set<ResolveCatalog> childChildCatalogSet = new HashSet<ResolveCatalog>();

        for (Node childChildNode : travChild.traverse(childNode).nodes())
        {
            String type = (String) childChildNode.getProperty(ResolveCatalog.TYPE);
            String childChildNodeName = (String) childChildNode.getProperty(ResolveCatalog.DISPLAYNAME);

//            Log.log.info("--- childChildNode -- " + childChildNodeName);

            

            if (StringUtils.isNotEmpty(type))
            {
                if (!type.equals(SocialRelationshipTypes.ResolveTag.name()))
                {
                    ResolveCatalog childChildCatalog = convertNodeToObject(childChildNode);

                    for (Node subChildNode : travChild.traverse(childChildNode).nodes())
                    {
                        String typeLocal = (String) subChildNode.getProperty(ResolveCatalog.TYPE);

                        if (StringUtils.isNotEmpty(typeLocal))
                        {
                            if (!typeLocal.equals(SocialRelationshipTypes.ResolveTag.name()))
                            {
                                ResolveCatalog subChildCatalog = convertNodeToObject(subChildNode);

                                //** RECURSIVE
                                traverseChild(subChildNode, subChildCatalog, childChildCatalog, travChild);

                            }
//                            else if (typeLocal.equals(ResolveTag.NODE_TYPE.ResolveTag.name()))
//                            {
//                                ResolveTag tagLocal = new ResolveTag();
//                                tagLocal.convertNodeToObject(subChildNode);
//                                childChildCatalog.getTags().add(tagLocal);
//
//                                Log.log.info("-- the childChildNode -- " + childChildCatalog.getName() + " --- add tag -- " + tagLocal.getName());
//                            }
                        }

                    }

                    sortOnOrder(childChildCatalog.getChildren());
//                    sortOnTag(childChildCatalog.getTags());

                    childChildCatalogSet.add(childChildCatalog);

                }
//                else if (type.equals(ResolveTag.NODE_TYPE.ResolveTag.name()))
//                {
//                    ResolveTag tagLocal = new ResolveTag();
//                    tagLocal.convertNodeToObject(childChildNode);
//                    childCatalog.getTags().add(tagLocal);
//                }
            }
        }

        childChildCatalogList.addAll(childChildCatalogSet);
        childCatalog.getChildren().addAll(childChildCatalogList);
        sortOnOrder(childCatalog.getChildren());
//        sortOnTag(childCatalog.getTags());

        if (parentCatalog != null)
        {
            parentCatalog.getChildren().add(childCatalog);
        }

    }
    
    private void sortOnOrder(List<? extends Object> catalog)
    {
        if (!catalog.isEmpty())
        {
            Collections.sort(catalog, new Comparator<Object>()
            {
                public int compare(final Object cat1, final Object cat2)
                {
                    return (((ResolveCatalog) cat1).getOrder() > ((ResolveCatalog) cat2).getOrder() ? 1 : -1);
                }
            });
        }
    }

}
