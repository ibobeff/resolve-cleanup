/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Lists;
import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceSocial;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.ResolveNamespaceUtil;
import com.resolve.services.hibernate.util.PropertiesUtil;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.AccessRightsVO;
import com.resolve.services.hibernate.vo.PropertiesVO;
import com.resolve.services.hibernate.vo.ResolveNamespaceVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.util.GMTDate;
import com.resolve.util.JavaUtils;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class AddRemoveRolesUtil
{

    /**
     * 
     * this is action from the Namespace Admin --> Set Roles 
     * 
     * @param namespaces - list of namespaces 
     * @param roles - selected roles from the UI
     * @param defaultRights - if this is true, get the default roles and apply to all the docs, if fa
     * @param username
     * @throws Exception
     */
	public static void setRolesForNamespaces(Set<String> namespaces, AccessRightsVO roles, boolean defaultRights,
			String username) throws Exception {
		setRolesForNamespaces(namespaces, roles, defaultRights, username, false);
    }

	public static void setRolesForNamespaces(Set<String> namespaces, AccessRightsVO roles, boolean defaultRights,
			String username, boolean useNamespaceEntity) throws Exception {
		if (namespaces != null && namespaces.size() > 0) {
			if (defaultRights) {
				// ignore the roles coming from UI, get the default roles from the properties
				List<PropertiesVO> lProps = PropertiesUtil.getPropertiesLikeName(ConstantValues.PROP_WIKI_RIGHTS);

				for (String ns : namespaces) {
					try {
						AccessRights ar = EvaluateAccessRights.evaluateAccessRights(null, lProps, ns, true);
						roles = ar.doGetVO();

						// get the sysIds for this namespace
						List<String> docSysIds = null;
						if (useNamespaceEntity) {
							ResolveNamespaceVO namespace = ResolveNamespaceUtil.getNamespaceByFullName(ns);
							docSysIds = Lists.newArrayList(namespace.getSys_id());
							updateAccessRightsToDocuments(docSysIds, roles, username, ResolveNamespaceUtil.RESOURCE_TYPE);
						} else {
							// set all the documents in the namespace to default=true
							updateDefaultRoleforNamespace(ns, true, username);
							docSysIds = findWikiDocumentSysIdsForNamespace(ns, true, username);
							updateAccessRightsToDocuments(docSysIds, roles, username);
						}

					} catch (Exception e) {
						Log.log.error("error assigning the roles for ns " + ns, e);
					}
				}
			} else {
				if (roles != null) {
					AccessRights ar = EvaluateAccessRights.evaluateAccessRights(new AccessRights(roles), null, null,
							false);
					roles = ar.doGetVO();

					for (String ns : namespaces) {
						try {
							// get the sysIds for this namespace
							List<String> docSysIds = null;
							if (useNamespaceEntity) {
								ResolveNamespaceVO namespace = ResolveNamespaceUtil.getNamespaceByFullName(ns);
								docSysIds = Lists.newArrayList(namespace.getSys_id());
								updateAccessRightsToDocuments(docSysIds, roles, username, ResolveNamespaceUtil.RESOURCE_TYPE);
							} else {
								// set all the documents in the namespace to default=false
								updateDefaultRoleforNamespace(ns, false, username);
								docSysIds = findWikiDocumentSysIdsForNamespace(ns, false, username);
								updateAccessRightsToDocuments(docSysIds, roles, username);
							}
						} catch (Exception e) {
							Log.log.error("error assigning the roles for ns " + ns, e);
						}
					}
				}
			}
		}
	}

    //this is action from the Namespace admin --> Set Namespace Default
	public static void setNamespaceDefaultRoles(Set<String> namespaces, AccessRightsVO roles, String username) throws Exception {
		setNamespaceDefaultRoles(namespaces, roles, username, false);
	}

	public static void setNamespaceDefaultRoles(Set<String> namespaces, AccessRightsVO roles, String username,
			boolean useNamespaceEntity) throws Exception {
		if (namespaces != null) {
			for (String ns : namespaces) {
				try {
					setNamespaceDefaultRoles(ns, roles, username, useNamespaceEntity);
				} catch (Exception e) {
					Log.log.error("error assigning the roles for ns " + ns, e);
				}
			}
		}
	}
    
    //this is action from Wiki Admin --> Set Roles
    public static void setRolesToWikiDocuments(Set<String> docSysIds, AccessRightsVO roles, boolean defaultRights, String username) throws Exception
    {
        if(docSysIds != null && docSysIds.size() > 0)
        {
            for(String docSysId : docSysIds)
            {
                updateWikiDocumentRoles(docSysId, null, roles, defaultRights, username);
            }
        }
    }

	private static void setNamespaceDefaultRoles(String namespace, AccessRightsVO roles, String username,
			boolean useNamespaceEntity) throws Exception {
		if (StringUtils.isNotBlank(namespace) && roles != null) {
			try {
				// update the system properties
				createOrUpdateSystemDefaultProperties(namespace, roles, username);

				// update the properties in the 'roles' object
				roles = WikiUtils.getDefaultRoles(namespace);

				List<String> docSysIds = null;
				if (useNamespaceEntity) {
					ResolveNamespaceVO ns = ResolveNamespaceUtil.getNamespaceByFullName(namespace);
					docSysIds = Lists.newArrayList(ns.getSys_id());
					updateAccessRights(docSysIds, roles, username, ResolveNamespaceUtil.RESOURCE_TYPE);
				} else {
					docSysIds = findWikiDocumentSysIdsForNamespace(namespace, true, username);
					updateAccessRightsToDocuments(docSysIds, roles, username);
				}
			} catch (Exception e) {
				Log.log.error("error assigning the roles for ns " + namespace, e);
			}
		}
	}
    
    private static void createOrUpdateSystemDefaultProperties(String namespace, AccessRightsVO roles, String username) throws Exception
    {
        //create ns prop names
        String viewPropsName = ConstantValues.PROP_WIKI_RIGHTS + ".view." + namespace;
        String editPropsName = ConstantValues.PROP_WIKI_RIGHTS + ".edit." + namespace;
        String adminPropsName = ConstantValues.PROP_WIKI_RIGHTS + ".admin." + namespace;
        String executePropsName = ConstantValues.PROP_WIKI_RIGHTS + ".execute." + namespace;

        //create prop objects
        saveProperty(viewPropsName, roles.getUReadAccess(), username);
        saveProperty(editPropsName, roles.getUWriteAccess(), username);
        saveProperty(adminPropsName, roles.getUAdminAccess(),  username);
        saveProperty(executePropsName, roles.getUExecuteAccess(), username);
    }
    

    private static void updateWikiDocumentRoles(String docSysId, String docFullName, AccessRightsVO roles, boolean defaultRole, String username) throws Exception
    {
        WikiDocumentVO docVO = WikiUtils.getWikiDoc(docSysId, docFullName, username);
        if (docVO != null)
        {
            AccessRightsVO docARVO = docVO.getAccessRights();
            if (docARVO == null)
            {
                throw new Exception("there is no Access right for the document " + docVO.getUFullname() + ". Please edit this doc and save it again.");
            }

            if (defaultRole)
            {
                docVO.setUIsDefaultRole(true);
            }
            else
            {
                docVO.setUIsDefaultRole(false);

                //get the new roles
                String readRoles = updatedRoles(docARVO.getUReadAccess(), roles.getUReadAccess());
                String writeRoles = updatedRoles(docARVO.getUWriteAccess(), roles.getUWriteAccess());
                String adminRoles = updatedRoles(docARVO.getUAdminAccess(), roles.getUAdminAccess());
                String executeRoles = updatedRoles(docARVO.getUExecuteAccess(), roles.getUExecuteAccess());

                //update the model
                docARVO.setUReadAccess(readRoles);
                docARVO.setUWriteAccess(writeRoles);
                docARVO.setUAdminAccess(adminRoles);
                docARVO.setUExecuteAccess(executeRoles);

                docVO.setAccessRights(docARVO);
            }

            try
            {
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {
                
	                WikiDocument doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(docVO.getSys_id());
	                if(doc != null)
	                {
	                    doc.setUIsDefaultRole(docVO.getUIsDefaultRole());
	                    
	                    AccessRights ar = doc.getAccessRights();
	                    if(ar != null)
	                    {
	                        ar.setUAdminAccess(docARVO.getUAdminAccess());
	                        ar.setUExecuteAccess(docARVO.getUExecuteAccess());
	                        ar.setUReadAccess(docARVO.getUReadAccess());
	                        ar.setUWriteAccess(docARVO.getUWriteAccess());
	                    }
	                    else
	                    {
	                        throw new Exception("Bad Data.Document "+ docVO.getUFullname() + " does not have Access Rights.");
	                    } 
	                }
                
                });
            }
            catch(Exception t)
            {
                Log.log.info("Error in updateWikiDocumentRoles", t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
    }
  
    
    private static void updateDefaultRoleforNamespace(String ns, boolean defaultRole, String username) throws Exception
    {
      //set all the documents in the namespace to default=false 
        StringBuilder updateDefaultFlagSql = new StringBuilder("update WikiDocument set UIsDefaultRole = ").append(defaultRole);
        
        Map<String, Object> updateParams = new HashMap<String, Object>();
        
        updateDefaultFlagSql.append(" where Lower(UNamespace) = :UNamespace");
        updateParams.put("UNamespace", ns.toLowerCase());
        
        //update the table
        GeneralHibernateUtil.executeHQLUpdate(updateDefaultFlagSql.toString(), updateParams, username);
    }

    private static List<String> findWikiDocumentSysIdsForNamespace(String ns, boolean defaultRole, String username) throws Exception
    {
        String parametrizedWhereClause = "Lower(wd.UNamespace) = :Unamespace and wd.UIsDefaultRole = :UIsDfaultRole";
        
        Map<String, Object> whereClauseParams = new HashMap<String, Object>();
        
        whereClauseParams.put("Unamespace", ns.toLowerCase());
        whereClauseParams.put("UIsDfaultRole", Boolean.valueOf(defaultRole));
        
        return  new ArrayList<String>(GeneralHibernateUtil.getSysIdsFor("WikiDocument", "wd", parametrizedWhereClause, whereClauseParams, username));
    }
    
    //this is a bulk operation. Ideally, we could have call the SaveHelper for each document but that will take a long time. Hence have to do this but note that any thing that needs 
    //document access rights need to be maintained in 2 places - one in SaveHelper and other one over here.
    private static void updateAccessRightsToDocuments(List<String> docSysIds, AccessRightsVO roles, String username) throws Exception
    {
    	updateAccessRightsToDocuments(docSysIds, roles, username, WikiDocument.RESOURCE_TYPE);
    }

    private static void updateAccessRightsToDocuments(List<String> docSysIds, AccessRightsVO roles, String username, String resourceType) throws Exception
    {
        if (docSysIds != null && docSysIds.size() > 0)
        {
            List<List<String>> batchSysIds = JavaUtils.chopped(docSysIds, 200);
            for (List<String> sysIds : batchSysIds)
            {
                try
                {
                	updateIndividualAccessRights(sysIds, roles, username, resourceType);
                    //update the graph
                    List<Document> listOfDocuments = SocialCompConversionUtil.getDocumentModelsForSysIds(sysIds);
                    ServiceSocial.bulkInsertOrUpdateDocumentGraphNodes(listOfDocuments, username);
                }
                catch (Throwable e)
                {
                    Log.log.error("error while executing the update for ResolveActionInvoc UDescription", e);
                }
            }//end of for loop
        }
    }

	private static void updateAccessRights(List<String> docSysIds, AccessRightsVO roles, String username,
			String resourceType) throws Exception {
		if (docSysIds != null && docSysIds.size() > 0) {
			List<List<String>> batchSysIds = JavaUtils.chopped(docSysIds, 200);
			for (List<String> sysIds : batchSysIds) {
				updateIndividualAccessRights(sysIds, roles, username, resourceType);
			}
		}
	}

	private static void updateIndividualAccessRights(List<String> sysIds, AccessRightsVO roles, String username,
			String resourceType) throws Exception {
		Map<String, Object> updateParams = new HashMap<String, Object>();

		StringBuilder sql = new StringBuilder("update AccessRights set ");
		sql.append(" UReadAccess = :UReadAccess").append(",");
		updateParams.put("UReadAccess", roles.getUReadAccess());
		sql.append(" UWriteAccess = :UWriteAccess").append(",");
		updateParams.put("UWriteAccess", roles.getUWriteAccess());
		sql.append(" UAdminAccess = :UAdminAccess").append(",");
		updateParams.put("UAdminAccess", roles.getUAdminAccess());
		sql.append(" UExecuteAccess = :UExecuteAccess").append(" ");
		updateParams.put("UExecuteAccess", roles.getUExecuteAccess());
		sql.append(" where UResourceId IN (:sysIds").append(") ");
		updateParams.put("sysIds", sysIds);
		sql.append(" and UResourceType = '").append(resourceType).append("'");

		try {
			GeneralHibernateUtil.executeHQLUpdate(sql.toString(), updateParams, username);
		} catch (Throwable e) {
			throw e;
		}
	}
	
    private static PropertiesVO saveProperty(String propertyName, String roles, String username)  throws Exception
    {
        PropertiesVO prop = null;
        
        if (StringUtils.isNotBlank(propertyName))
        {
            if (StringUtils.isNotBlank(roles))
            {
                prop = ServiceHibernate.findPropertyByName(propertyName);
                if (prop != null)
                {
                    String updatedRoles = updatedRoles(prop.getUValue(), roles);
                    prop.setUValue(updatedRoles);
                }
                else
                {
                    prop = new PropertiesVO(propertyName, roles, "string", "updated by admin", GMTDate.getDate(), username);
                }

                //persist it
                ServiceHibernate.saveProperties(prop, username);
            }
            else
            {
                //delete the property
                PropertiesUtil.deletePropertiesByName(propertyName, username);
            }
        }
        
        return prop;
    }
    
    private static String updatedRoles(String oldRoles, String newRoles)
    {
        String result = null;
        
        Set<String> roles = new HashSet<String>();
        roles.add("admin");
        roles.addAll(StringUtils.convertStringToList(newRoles, ","));
        
        result = StringUtils.convertCollectionToString(roles.iterator(), ",");
        return result;
    }
}
