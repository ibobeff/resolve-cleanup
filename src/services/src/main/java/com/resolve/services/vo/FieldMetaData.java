/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

public class FieldMetaData
{
    private String fieldName;
    private String datatype;
    private String uiType;
    
    
    public String getFieldName()
    {
        return fieldName;
    }
    public void setFieldName(String fieldName)
    {
        this.fieldName = fieldName;
    }
    public String getDatatype()
    {
        return datatype;
    }
    public void setDatatype(String datatype)
    {
        this.datatype = datatype;
    }
    public String getUiType()
    {
        return uiType;
    }
    public void setUiType(String uiType)
    {
        this.uiType = uiType;
    }
    
    
    
}
