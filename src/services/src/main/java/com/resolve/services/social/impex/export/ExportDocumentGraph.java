/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.social.impex.export;

import java.util.Collection;
import java.util.List;

import com.resolve.persistence.model.ResolveCatalogNode;
import com.resolve.persistence.model.ResolveCatalogNodeWikidocRel;
import com.resolve.persistence.model.ResolveTag;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikidocResolveTagRel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.catalog.Catalog;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.social.impex.GraphRelationshipDTO;
import com.resolve.services.social.impex.SocialImpexVO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;

public class ExportDocumentGraph  extends ExportComponentGraph
{
    
    public ExportDocumentGraph(String sysId, ImpexOptionsDTO options) throws Exception
    {
       super(sysId, options, NodeType.DOCUMENT);
    }
    
    @Override
    protected List<GraphRelationshipDTO> exportRelationships(ResolveNodeVO compNode, SocialImpexVO socialImpex) throws Exception
    {
        //its better to open a transaction and get the details as that will be better in performance
//        doc = ServiceWiki.getWikiDocWithGraphRelatedData(compNode.getUCompSysId(), compNode.getUCompName(), "admin");
        
        try
        {
          HibernateProxy.setCurrentUser("system");
        	HibernateProxy.execute(() -> {
            
	            WikiDocument doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(compNode.getUCompSysId()); 
	            if(doc != null)
	            {
	                if (options.wikiTags)
	                {
	                    //for documents, export the tags 
	                    exportTagsRelationship(doc, socialImpex);
	                }
	                
	                if (options.wikiCatalogs)
	                {
	                    //and catalog node relationship
	                    exportCatalogRelationship(doc, socialImpex);
	                }
	            }
            
        	});
        }
        catch(Throwable t)
        {
            Log.log.error("Error in exporting Document:" + compNode.getUCompName(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return relationships;
    }
    
    
    
    
    private void exportCatalogRelationship(WikiDocument doc, SocialImpexVO socialImpex) throws Exception
    {
        Collection<ResolveCatalogNodeWikidocRel> catalogNodeRels = doc.getCatalogNodeWikidocRels();
        if(catalogNodeRels != null && catalogNodeRels.size() > 0)
        {
            for(ResolveCatalogNodeWikidocRel catalogNodeRel : catalogNodeRels)
            {
                ResolveCatalogNode catalogNode = catalogNodeRel.getCatalogNode();
                if(catalogNode != null)
                {
                    GraphRelationshipDTO relation = new GraphRelationshipDTO();
                    relation.setSourceName(doc.getUFullname());
                    relation.setSourceType(NodeType.DOCUMENT);
                    relation.setTargetName(catalogNode.getUPath());
                    relation.setTargetType(NodeType.CatalogItem);
                    
                    Catalog catalog = new Catalog(catalogNode.doGetVO());
                    relation.setTargetObject(catalog);
                    
                    relationships.add(relation);
                }
            }
            
            
        }
    }
    
    private void exportTagsRelationship(WikiDocument doc, SocialImpexVO socialImpex) throws Exception
    {
        Collection<WikidocResolveTagRel> tagRels = doc.getWikidocResolveTagRels();
        if(tagRels != null && tagRels.size() > 0)
        {
            for(WikidocResolveTagRel rel : tagRels)
            {
                ResolveTag tag = rel.getTag();
                if(tag != null)
                {
                    GraphRelationshipDTO relation = new GraphRelationshipDTO();
                    relation.setSourceName(doc.getUFullname());
                    relation.setSourceType(NodeType.DOCUMENT);
                    relation.setTargetName(tag.getUName());
                    relation.setTargetType(NodeType.TAG);
                    
                    relationships.add(relation);
                }
            }
        }
    }
    
}
