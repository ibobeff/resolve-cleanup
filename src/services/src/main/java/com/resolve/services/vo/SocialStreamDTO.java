/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import com.resolve.services.interfaces.DTO;

public class SocialStreamDTO extends DTO
{

    private static final long serialVersionUID = -2146399667378822535L;
    
    private String id;      //same as sys_id
    private String name;    //name of the component - wiki or at or any other RSCOmponent
    private String type;    //type - Wiki, Runbook, Actiontask, etc
    
    public String getId()
    {
        return id;
    }
    public void setId(String id)
    {
        this.id = id;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    
    
    
    
}
