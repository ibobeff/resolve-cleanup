package com.resolve.services.rr.util;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import org.hibernate.query.Query;

import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceResolutionRouting;
import com.resolve.services.hibernate.vo.RRRuleVO;
import com.resolve.services.hibernate.vo.RRSchemaVO;
import com.resolve.services.rr.util.TransactionHelper.AtomicRawOperation;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;

public class QueryCursor extends Cursor
{
    private int total;
    private List<RRRuleVO> list;
    private int pos;
    private int count;
    private QueryDTO query;
    private String username;
    public QueryCursor(QueryDTO query, String username) throws Exception {
        this.query = query;
        this.query.setLimit(50);
        this.username = username;
        this.pos = 0;
        this.count = 0;
        this.total = -1;
    }
    @Override
    public boolean hasNext() throws Exception
    {   
        if(this.total==-1) {
            this.list = ServiceResolutionRouting.listRules(query, username);
            this.total = ServiceHibernate.getTotalHqlCount(query);
        }   
        return !this.list.isEmpty();
    }
    
    @Override
    public RRRuleVO next() throws ReadRuleException
    {
        RRRuleVO rule=  null;
        if(this.pos<this.list.size()) {
            rule = this.list.get(this.pos);
            this.pos++;
            this.count++;
            
            if(this.pos==this.list.size()) {
                this.query.setStart(this.count);
                this.pos=0;
                
                try
                {
                    this.list = ServiceResolutionRouting.listRules(query, username);
                }
                catch (Exception e)
                {
                    Log.log.error(e);
                    throw new ReadRuleException(e);
                }
            }
            return rule;
        }
        //just in case...theoretically it shoule never reach here
        return rule;
    }
    @Override
    public List<String> appendRuleHeaders() throws Exception
    {
        final QueryDTO schemaQuery = new QueryDTO();
        schemaQuery.setModelName("RRRule");
        schemaQuery.setFilter(this.query.getFilter());
        schemaQuery.setPrefixTableAlias("rule.");
        String where = schemaQuery.getFilterWhereClause();
        if(!StringUtils.isEmpty(where))
            where = " where " + where;
        final String hql = "select distinct rule.schema.id from RRRule rule " + where;
        schemaQuery.getParams();
        List<String> ids = new TransactionHelper<List<String>>(username).doAtomicOp(new AtomicRawOperation<List<String>>() {

            public List<String> action() throws Exception
            {
                Query hqlQuery = HibernateUtil.createQuery(hql);
                Object value = null;
                for(String key:schemaQuery.getParams().keySet()) {
                    value = schemaQuery.getParams().get(key);
                    hqlQuery.setParameter(key, value);
                }
                
                @SuppressWarnings("unchecked")
                List<String> list = hqlQuery.list();
                return list;
            }
            
        });
        if(ids.isEmpty())
            throw new Exception("No schema is found to form the headers.");
        List<RRSchemaVO> ruleSchemas = ResolutionRoutingUtil.listSchemasByIds(ids, username);
        List<String> headers = this.getDefaultHeader();
        for(RRSchemaVO schema:ruleSchemas) {
            
            JsonNode jsonFields = new ObjectMapper().readTree(schema.getJsonFields());
            for(JsonNode field:jsonFields) {
                if(headers.indexOf(field.get("name").asText())!=-1)
                    continue;
                headers.add(field.get("name").asText());
            }
        }
        
        return headers;
    }
    @Override
    public Double getPercentage()
    {
        return this.count*1.0/this.total;
    }
}
