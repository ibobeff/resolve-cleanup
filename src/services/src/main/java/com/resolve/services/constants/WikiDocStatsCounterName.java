/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.constants;

/**
 * These values are for the WikiDocStats
 * 
 * @author jeet.marwah
 *
 */
public enum WikiDocStatsCounterName
{
	VIEWED, EDITED, EXECUTED;
}
