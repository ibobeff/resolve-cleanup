/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WorkflowUtil
{

    private final static List<String> SYS_FIELDS = new ArrayList<String>();
    
    ///TODO : REMOVE THIS ONCE MIGRATED TO EXTJS - this is from WorkflowConstants 
    private final static String MODULE_RSADMIN = "rsadmin.jsp";
    private final static String MODULE_RSWORKSHEET = "rsworksheet.jsp";
    private final static String MODULE_RSACTIONTASK = "rsactiontask.jsp";
    private final static String MODULE_RSWIKI = "rswiki.jsp";
//    private final static String MODULE_RSNETCOOL = "rsnetcool.jsp";
//    private final static String MODULE_RSEXCHANGE = "rsexchange.jsp";
//    private final static String MODULE_RSCLIENT = "rsclient.jsp";
//    private final static String MODULE_RSEMAIL = "rsemail.jsp";
    /////////////////////////////////
    
    
    static
    {
        SYS_FIELDS.add("sys_id");
        SYS_FIELDS.add("sys_created_by");
        SYS_FIELDS.add("sys_created_on");
        SYS_FIELDS.add("sys_updated_by");
        SYS_FIELDS.add("sys_updated_on");
        SYS_FIELDS.add("sys_mod_count");
    }
    
    private final static Map<String, String> MODULE_TABLE_MAP = new HashMap<String, String>();

    static 
    {
        //rsadmin
        MODULE_TABLE_MAP.put("resolve_cron", MODULE_RSADMIN.toLowerCase());
        MODULE_TABLE_MAP.put("sys_user", MODULE_RSADMIN.toLowerCase());
        MODULE_TABLE_MAP.put("groups", MODULE_RSADMIN.toLowerCase());
        
        //rsworksheet
        MODULE_TABLE_MAP.put("worksheet", MODULE_RSWORKSHEET.toLowerCase());
        MODULE_TABLE_MAP.put("archive_worksheet", MODULE_RSWORKSHEET.toLowerCase());
        
        //rsactiontask
        MODULE_TABLE_MAP.put("resolve_preprocess", MODULE_RSACTIONTASK.toLowerCase());
        MODULE_TABLE_MAP.put("resolve_parser", MODULE_RSACTIONTASK.toLowerCase());
        MODULE_TABLE_MAP.put("resolve_assess", MODULE_RSACTIONTASK.toLowerCase());
        MODULE_TABLE_MAP.put("resolve_properties", MODULE_RSACTIONTASK.toLowerCase());
        MODULE_TABLE_MAP.put("resolve_action_pkg", MODULE_RSACTIONTASK.toLowerCase());
        MODULE_TABLE_MAP.put("resolve_action_task", MODULE_RSACTIONTASK.toLowerCase());
        
        //rswiki
        MODULE_TABLE_MAP.put("wikidoc", MODULE_RSWIKI.toLowerCase());
    }
    
    private final static Map<String, String> TABLE_LINK_MAP = new HashMap<String, String>();
    
    static 
    {
        //rsadmin
        TABLE_LINK_MAP.put("resolve_cron", "cron");
        TABLE_LINK_MAP.put("sys_user", "Users");
        TABLE_LINK_MAP.put("groups", "Groups");
        
        //rsworksheet
        TABLE_LINK_MAP.put("worksheet", "worksheet");
        TABLE_LINK_MAP.put("archive_worksheet", "archiveworksheet");
        
        //rsactiontask
        TABLE_LINK_MAP.put("resolve_preprocess", "Preprocess");
        TABLE_LINK_MAP.put("resolve_parser", "Parser");
        TABLE_LINK_MAP.put("resolve_assess", "Assess");
        TABLE_LINK_MAP.put("resolve_properties", "Properties");
        TABLE_LINK_MAP.put("resolve_action_pkg", "Tag");
        TABLE_LINK_MAP.put("resolve_action_task", "actiontask");
     
        //rswiki
        TABLE_LINK_MAP.put("wikidoc", "");
    }
   
    public static String getModuleName(String tableName)
    {
        return MODULE_TABLE_MAP.get(tableName);
    }
    
    public static String getMainName(String tableName)
    {
        return TABLE_LINK_MAP.get(tableName);
    }
    
}
