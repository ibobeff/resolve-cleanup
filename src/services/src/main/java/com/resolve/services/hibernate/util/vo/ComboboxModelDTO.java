/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util.vo;

public class ComboboxModelDTO implements Comparable<ComboboxModelDTO>
{
    private String name;
    private String value;
    private String type;

    public ComboboxModelDTO() {}
    
    public ComboboxModelDTO(String name, String value)
    {
        this.name = name;
        this.value = value;
    }

    public ComboboxModelDTO(String name, String value, String type)
    {
        this.name = name;
        this.value = value;
        this.type = type;
    }
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

//    @Override
    public int compareTo(ComboboxModelDTO obj)
    {
        return getName().compareTo(obj.getName());
    }
    
}
