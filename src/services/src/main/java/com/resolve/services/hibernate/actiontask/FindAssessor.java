/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.actiontask;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.hibernate.query.Query;

import com.resolve.persistence.model.ResolveActionInvoc;
import com.resolve.persistence.model.ResolveAssess;
import com.resolve.persistence.model.ResolveAssessRel;
import com.resolve.persistence.model.ResolveParser;
import com.resolve.persistence.util.DBCacheRegionConstants;import com.resolve.persistence.util.HibernateUtil;
import com.resolve.persistence.util.HibernateProxy;import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.util.AssessorUtil;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveAssessVO;
import com.resolve.services.vo.ATReferenceDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class FindAssessor
{
    private ResolveAssessVO vo = null;
    private ResolveAssess model = null;

    private String sysId = null;
    private String name = null;
    private String username = null;
    
    private Set<String> invocSysIds = new HashSet<String>();
    
    public FindAssessor(String sysId, String name, String username) throws Exception
    {
        if(StringUtils.isEmpty(sysId) && StringUtils.isEmpty(name))
        {
            throw new Exception("sysId or name of the assessor is mandatory");
        }
        
        this.sysId = sysId;
        this.name = name;
        this.username = username;
    }

    
    public ResolveAssessVO getWithoutRefs() throws Exception
    {
        //load the object first
        lookupAssessor();
        
        if (model != null)
        {
            //prepare the vo
            vo = model.doGetVO();
        }
        return vo;
    }

    
    public ResolveAssessVO get() throws Exception
    {
        //load the object first
        lookupAssessor();
        
        if (model != null)
        {
            //prepare the vo
            vo = model.doGetVO();
            
            //prepare the actiontask references
            prepareActionTaskReferences();
            
            //prepare the assessor refs in 
            prepareAssessorReferencing();
            
            //prepare the assessor refs by : 
            findAssessorReferencedBy();
        }
        return vo;
    }
    
    private void prepareActionTaskReferences() throws Exception
    {
        Collection<ResolveActionTaskVO> ats = ActionTaskUtil.findResolveActionTaskBasedOnInvoc(invocSysIds, username);
        if(ats.size() > 0)
        {
            for(ResolveActionTaskVO at : ats)
            {
                ATReferenceDTO ref = new ATReferenceDTO();
                ref.setDescription(at.getUSummary());
                ref.setName(at.getUFullName());
                ref.setType("Actiontask");
                ref.setRefInOrBy("Referenced By");
                
                //add the ref
                vo.getReferences().add(ref);
            }
            
        }
    }
    
    
    private void prepareAssessorReferencing() throws Exception
    {
        Set<String> assessorsReferenced = vo.getRefAssessors();
        if(assessorsReferenced.size() > 0)
        {
            Collection<ResolveAssessVO> assessors = AssessorUtil.findResolveAssessByNames(assessorsReferenced, username);
            for(ResolveAssessVO assess : assessors)
            {
                ATReferenceDTO ref = new ATReferenceDTO();
                ref.setDescription(assess.getUDescription());
                ref.setName(assess.getUName());
                ref.setType("Assessor");
                ref.setRefInOrBy("Referencing");
                
                //add the ref
                vo.getReferences().add(ref);
            }
        }
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void findAssessorReferencedBy()
    {
        String name = vo.getUName();
        String sql = "select a from ResolveAssessRel a where LOWER(a.URefAssessor) = '" + name + "' ";

        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            Query query = HibernateUtil.createQuery(sql);
	            List<ResolveAssessRel> list = query.list();
	            if (list != null && list.size() > 0)
	            {
	                for(ResolveAssessRel rel : list)
	                {
	                    ResolveAssess assess = rel.getAssessor();
	                    
	                    ATReferenceDTO ref = new ATReferenceDTO();
	                    ref.setDescription(assess.getUDescription());
	                    ref.setName(assess.getUName());
	                    ref.setType("Assessor");
	                    ref.setRefInOrBy("Referenced By");
	                    
	                    //add the ref
	                    vo.getReferences().add(ref);
	                }
	            }

            });
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }
    
    private void lookupAssessor() throws Exception
    {
        if(StringUtils.isNotBlank(sysId) && HibernateUtil.getCachedDBObject(DBCacheRegionConstants.ASSESSOR_CACHE_REGION, sysId) != null) {
            model = (ResolveAssess) HibernateUtil.getCachedDBObject(DBCacheRegionConstants.ASSESSOR_CACHE_REGION, sysId);
            return;
        }        
        
        try
        {

          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
                if (StringUtils.isNotBlank(sysId))
                {
                    model = HibernateUtil.getDAOFactory().getResolveAssessDAO().findById(sysId);
                }
                else if (StringUtils.isNotBlank(name))
                {
                    //to make this case-insensitive
                    //String sql = "select a from ResolveAssess a where LOWER(a.UName) = '" + name.toLowerCase().trim() + "'";
                    String sql = "select a from ResolveAssess a where LOWER(a.UName) = :UName";
                    
                    Map<String, Object> queryParams = new HashMap<String, Object>();
                    
                    queryParams.put("UName", name.trim().toLowerCase());
                    
                    List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                    if (list != null && list.size() > 0)
                    {
                        model = (ResolveAssess) list.get(0);
                    }
                }
                
                if(model != null)
                {
                    //load the references
                    if(model.getResolveAssessRels() != null)
                    {
                        model.getResolveAssessRels().size();
                    }
                    
                    if(model.getResolveActionInvocs() != null)
                    {
                        for(ResolveActionInvoc invoc : model.getResolveActionInvocs())
                        {
                            invocSysIds.add(invoc.getSys_id());
                        }
                    }
                    
                    if (StringUtils.isNotBlank(model.getSys_id())) 
                    {
                      HibernateUtil.cacheDBObject(DBCacheRegionConstants.ASSESSOR_CACHE_REGION, new String[] { "com.resolve.persistence.model.ResolveAssess"}, model.getSys_id(), model);
                    }
                }
        	});
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
            throw new Exception(e);
        }
    }
    
    
}
