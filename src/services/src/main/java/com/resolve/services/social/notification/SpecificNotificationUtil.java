/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.social.notification;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.resolve.persistence.model.ResolveNode;
import com.resolve.services.ServiceGraph;
import com.resolve.services.graph.social.model.ComponentNotification;
import com.resolve.services.graph.social.model.NotificationConfig;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.message.NotificationTypeEnum;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.hibernate.util.SocialNotificationUtil;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * 
 * same as the one from the graph folder
 * 
 * @author jeet.marwah
 *
 */
public class SpecificNotificationUtil
{
    public static final String GLOBAL_DEFAULT = "Global Default";
    public static final String SELECT_TRUE = "True";
    public static final String SELECT_FALSE = "False";
    
    private ResolveNodeVO streamNode = null;
    private ResolveNodeVO userNode = null;
    private NodeType streamType = null;
    private String streamDisplayName = null;
    private Map<UserGlobalNotificationContainerType, String> userCurrentSettings = null;
    private EnumSet<NotificationTypeEnum> enumSetNotificationType = null;
    private Set<UserGlobalNotificationContainerType> validNotificationTypes = new HashSet<UserGlobalNotificationContainerType>(); 
    
    public SpecificNotificationUtil(String streamId, User user) throws Exception
    {
        if(StringUtils.isEmpty(streamId) || user == null)
        {
            throw new Exception("StreamId and user are mandatory ");
        }
        
        this.streamNode = ServiceGraph.findNode(null, streamId, null, null, user.getCompName());
        if(streamNode == null)
        {
            throw new Exception("stream with id " + streamId + " does not exist.");
        }
        
        this.userNode = ServiceGraph.findNode(null, user.getSys_id(), null, NodeType.USER, user.getCompName());
        if(userNode == null)
        {
            throw new Exception("Node for User " + user.getDisplayName() + " does not exist.");
        }
        
        //type of stream
        streamType = streamNode.getUType();
        streamDisplayName = streamNode.getUCompName();
        enumSetNotificationType = SocialNotificationUtil.findNotificationTypesForType(streamType, true); 
        validNotificationTypes.addAll(NotificationTypeEnum.prepareNotificationTypes(enumSetNotificationType));
    }
    
    public ComponentNotification getSpecificNotifications()  throws Exception
    {
        //get the specific settings of the user with this node
        if(userCurrentSettings == null)
        {
            userCurrentSettings = getUserCurrentSpecificSettings();
        }

        //list of notifications available for this type of component
        List<NotificationConfig> notifyTypes = new ArrayList<NotificationConfig>();
        if (enumSetNotificationType != null && !enumSetNotificationType.isEmpty())
        {
            for (NotificationTypeEnum typeEnum : enumSetNotificationType)
            {
                notifyTypes.add(getSpecificNotification(typeEnum));
            }//end of for loop
        }


        //collect all the data and prepare the object
        ComponentNotification compNotify = new ComponentNotification();
        compNotify.setCompType(streamType);
        compNotify.setNotifyTypes(notifyTypes);
        compNotify.setComponentDisplayName(streamDisplayName);
        
        return compNotify;
    }
    
    
    /**
    This api is to set/update the value for a specific component type.
    * 
    * For eg, if user wants to receive email for all the Post for a Process 'P1', the values for this api will be
    * streamId = '8a9482e54185528801418568767a0009'  <== sysId of Process P1
    * type =  "PROCESS_FORWARD_EMAIL"  <== mapped to UserGlobalNotificationContainerType.PROCESS_FORWARD_EMAIL
    * value = true or false
    * 
    * @param selectedType
    * @param value
    * @throws Exception
    */
    public void updateType(UserGlobalNotificationContainerType type, boolean value) throws Exception
    {
        if (validNotificationTypes.contains(type))
        {
            
            SocialNotificationUtil.persistUserNotification(this.userNode.getUCompSysId(), this.userNode.getUCompName(), type, null, new ResolveNode(streamNode), value+"");
            
            //add the relationship and set the property
            //        ResolveGraphFactory.addRelation(userNode, streamNode, type);
            //        Relationship rel = ResolveGraphFactory.findRelationForType(userNode, streamNode, type);
            //        if(rel != null)
            //        {
            //            ResolveGraphFactory.updatePropertyOnRelationship(ResolveGraphFactory.NOTIFICATIONVALUE, value+"", rel);
            //        }
        }
        else
        {
            Log.log.warn("Type " + type + " is not valid for stream " + this.streamDisplayName + " which is of type " + this.streamType);
        }
    }
    
    
    
    //private apis
    private Map<UserGlobalNotificationContainerType, String> getUserCurrentSpecificSettings() throws Exception
    {
        Map<UserGlobalNotificationContainerType, String> result = new HashMap<UserGlobalNotificationContainerType, String>();
        
        
        Map<UserGlobalNotificationContainerType, String> userNotifications = SocialNotificationUtil.getUserNotificationsForNode(this.userNode.getUCompSysId(), this.streamNode.getUCompSysId(), 
                        this.streamNode.getUCompName(), this.streamNode.getUType(), this.userNode.getUCompName());
        Iterator<UserGlobalNotificationContainerType> it = userNotifications.keySet().iterator();
        while(it.hasNext())
        {
            UserGlobalNotificationContainerType notification = it.next();
            String value = userNotifications.get(notification);
            value = value.equalsIgnoreCase("true") ? SELECT_TRUE : SELECT_FALSE;
            
            //add it in the map
            result.put(notification, value);
        }
        
        return result;
    }
    
    private NotificationConfig getSpecificNotification(NotificationTypeEnum typeEnum) throws Exception
    {
        UserGlobalNotificationContainerType typeOfNotification = typeEnum.getType();
        if(userCurrentSettings == null)
        {
            userCurrentSettings = getUserCurrentSpecificSettings();
        }

        String value = userCurrentSettings.containsKey(typeOfNotification) ? userCurrentSettings.get(typeOfNotification) : GLOBAL_DEFAULT;
        
        NotificationConfig config = new NotificationConfig();
        config.setType(typeOfNotification);
        config.setDisplayName(typeEnum.getDisplayName());
        config.setValue(value);
        
        return config;
    }    

}
