/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.query.Query;

import com.resolve.persistence.dao.ContentWikidocActiontaskRelDAO;
import com.resolve.persistence.dao.ExceptionWikidocActiontaskRelDAO;
import com.resolve.persistence.dao.MainWikidocActiontaskRelDAO;
import com.resolve.persistence.dao.ResolveActionTaskDAO;
import com.resolve.persistence.dao.hibernate.HibernateDAOFactory;
import com.resolve.persistence.model.ActionTaskArchive;
import com.resolve.persistence.model.ContentWikidocActiontaskRel;
import com.resolve.persistence.model.ExceptionWikidocActiontaskRel;
import com.resolve.persistence.model.MainWikidocActiontaskRel;
import com.resolve.persistence.model.MetaFormAction;
import com.resolve.persistence.model.ResolveActionInvoc;
import com.resolve.persistence.model.ResolveActionParameter;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.ResolveActionTaskMockData;
import com.resolve.persistence.model.ResolveAssess;
import com.resolve.persistence.model.ResolveCompRel;
import com.resolve.persistence.model.ResolveParser;
import com.resolve.persistence.model.ResolveParserTemplate;
import com.resolve.persistence.model.ResolvePreprocess;
import com.resolve.persistence.model.ResolveTaskExpression;
import com.resolve.persistence.model.ResolveTaskOutputMapping;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.rsbase.MainBase;
import com.resolve.search.actiontask.ActionTaskIndexAPI;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.exception.MissingVersionException;
import com.resolve.services.hibernate.actiontask.DeleteActionTask;
import com.resolve.services.hibernate.actiontask.SaveActionTask;
import com.resolve.services.hibernate.actiontask.SearchActionTask;
import com.resolve.services.hibernate.actiontask.VersionPersistAction;
import com.resolve.services.hibernate.vo.AccessRightsVO;
import com.resolve.services.hibernate.vo.PropertiesVO;
import com.resolve.services.hibernate.vo.ResolveActionInvocOptionsVO;
import com.resolve.services.hibernate.vo.ResolveActionInvocVO;
import com.resolve.services.hibernate.vo.ResolveActionParameterVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskMockDataVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveAssessVO;
import com.resolve.services.hibernate.vo.ResolveParserVO;
import com.resolve.services.hibernate.vo.ResolvePreprocessVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.wiki.SaveHelper;
import com.resolve.services.hibernate.wiki.WikiArchiveUtil;
import com.resolve.services.migration.MigrateActionTask;
import com.resolve.services.util.ParseUtil;
import com.resolve.services.util.PropertiesUtil;
import com.resolve.services.util.UpdateChecksumUtil;
import com.resolve.services.util.UserUtils;
import com.resolve.services.vo.ActionTaskTreeDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QueryParameter;
import com.resolve.services.vo.ReferenceDTO;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstants;

public class ActionTaskUtil
{
    private final static Pattern VAR_REGEX_PROPERTY = Pattern.compile(Constants.VAR_REGEX_PROPERTY);
    
    private static final String ENCRYPTED_FIELD_DEFAULT_VALUE = StringUtils.repeat("*", 5);
    private static ConcurrentHashMap<String, String> cacheTaskId = new ConcurrentHashMap<String, String>();
    private static ConcurrentHashMap<String, String> cacheTaskName = new ConcurrentHashMap<String, String>();
    public static ResponseDTO<ResolveActionTaskVO> getResolveActionTasks(QueryDTO query, String username) throws Exception
    {
        SearchActionTask searchAt = new SearchActionTask(query, username);
        return searchAt.execute();
    }
    
    public static Collection<ResolveActionTaskVO> findResolveActionTaskBasedOnInvoc(Set<String> invocSysIds, String username)
    {
        Collection<ResolveActionTaskVO> result = new ArrayList<ResolveActionTaskVO>();
        
        if (invocSysIds != null && invocSysIds.size() > 0)
        {
            //prepare list of actiontasks based on invoc sysIds
            //String sql = "from ResolveActionTask where resolveActionInvoc IN (" + SQLUtils.prepareQueryString(new ArrayList<String>(invocSysIds)) + ")";
            String sql = "from ResolveActionTask where resolveActionInvoc.sys_id IN (:invocSysId)";
            
            Map<String, Collection<Object>> queryInParams = new HashMap<String, Collection<Object>>();
            
            queryInParams.put("invocSysId", new ArrayList<Object>(invocSysIds));
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelectInOnly(sql, queryInParams);
                for (Object o : list)
                {
                    ResolveActionTask instance = (ResolveActionTask) o;
                    result.add(instance.doGetVO());
                }
            }
            catch(Exception e)
            {
                Log.log.error("error in sql " + sql, e);
            }
        }
        
        return result;
    }
    
    public static Set<String> findAllResolveActionTaskModuleNames(String username)
    {
        Set<String> result = new HashSet<String>();
        
        //prepare list of actiontasks based on invoc sysIds
        String sql = "select DISTINCT UNamespace from ResolveActionTask order by UNamespace";
        
        
        try
        {
            List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, new HashMap<String, Object>());
            for (Object o : list)
            {
                String instance = (String) o;
                result.add(instance);
            }
        }
        catch(Exception e)
        {
            Log.log.error("error in hql " + sql, e);
        }
        
        return result;
        
    }
    
    @SuppressWarnings("unchecked")
      public static Map<String, Object> getAllActionTasks (QueryDTO queryDTO) 
      {
          List<? extends Object> resultList = null;
          List<Object> countList = null;
          
          try
          {
        	  
        		   String hqlQuery;
                   String countQuery;
                   String whereClause = queryDTO.getWhereClause();
                   queryDTO.getSelectHQL();
                   
                   int pageSize = queryDTO.getLimit();
                   int start = queryDTO.getStart();
                   if (StringUtils.isBlank(whereClause))
                   {
                       hqlQuery = " select sys_id, UName, UNamespace, USummary, sysUpdatedOn from ResolveActionTask order by UName" ;
                       countQuery = "select count(*) from ResolveActionTask";
                   }
                   else
                   {
                       hqlQuery = " select sys_id, UName, UNamespace, USummary, sysUpdatedOn from ResolveActionTask " + whereClause + " order by UName";
                       countQuery = "select count(*) from ResolveActionTask " + whereClause + " order by UName";
                   }
                   
                   queryDTO.setHql(hqlQuery);
                   
                   
                   
                   resultList = (List<? extends Object>) HibernateProxy.execute(() -> {
                	   return GeneralHibernateUtil.executeHQLSelectModel(queryDTO, start, pageSize, true);
                   });
                   
                   countList = (List<Object>) HibernateProxy.execute(() -> {
                	    return HibernateUtil.createQuery(countQuery).list();
                   });
          }
          catch (Throwable e)
          {
              Log.log.error(e.getMessage(), e);
                          HibernateUtil.rethrowNestedTransaction(e);
          }
          
          List<ResolveActionTaskVO> result = populateResolveActionTaskVO(resultList);
          
          Object obj = countList.get(0);
          Integer count = ((Long)obj).intValue();
          
          Map<String, Object> resultMap = new HashMap<String, Object> ();
          resultMap.put("ACTION_TASK_VO_LIST", result);
          resultMap.put("TOTAL_COUNT", count);
          
          return resultMap;
      }

    public static ResolveActionTaskVO getResolveActionTask(String task_sid, String atName, String username)
    {
        ResolveActionTask task = null;
        ResolveActionTaskVO taskVO = null;
    
        try
        {
            task = getResolveActionTaskModel(task_sid, atName, false, username);
        }
        catch (Throwable e)
        {
            Log.log.error("Error loading the actiontask ", e);
        }

        if (task != null)
        {
            taskVO = task.doGetVO();
        }
    
        return taskVO;
    } // checkTaskPermission
    
    public static ResolveActionTaskVO getActionTaskWithReferencesExceptGraph(String sysId, String fullName, String username)
    {
        ResolveActionTaskVO result = null;
        ResolveActionTask actionTask = null;

        try
        {
            actionTask = getResolveActionTaskModel(sysId, fullName, true, "system");
            if (actionTask != null)
            {
                result = actionTask.doGetVO();
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        return result;
    } // getActionIdFromFullname
    

    public static ResolveActionTaskVO getActionTaskWithReferences(String sysId, String fullName, String username)
    {
    	ResolveActionTaskVO result = null;
    	try
    	{
	    	result =  getActionTaskWithReferences(sysId, fullName, username, false);
    	}
    	catch (Throwable e)
    	{
    		Log.log.error(e.getMessage(), e);
    	}
    	
    	return result;
    }

    public static ResolveActionTaskVO getActionTaskWithReferences(String sysId, String fullName, String username, boolean validate) throws Exception {
    	return getActionTaskWithReferences(sysId, fullName, username, validate, null);
    }
    
    
    public static ResolveActionTaskVO getActionTaskWithReferences(String sysId, String fullName, String username, boolean validate, String version) throws Exception
    {
        ResolveActionTaskVO result = null;
        ResolveActionTask actionTask = null;

        if (sysId == null)
        {
            sysId = ActionTaskUtil.getActionIdFromFullname(fullName, username);
        }
        
        if (validate)
        {
        	validateUserForActionTaskNoCache(sysId, fullName, RightTypeEnum.view, username);
        }
        try
        {
            actionTask = getResolveActionTaskModel(sysId, fullName, true, "system");
            if (actionTask != null)
            {
            	try {
                   	result = getResolveActionTaskVersion(actionTask.getSys_id(), version);
            		// for migration purposes: if we don't have a proper version returned we return the current action task
                   	// TODO: should be removed post-6.4
            		if(result == null) {
            			result = actionTask.doGetVO();
            		}
            		
            	} catch (MissingVersionException e) {
            		throw e;
				}
            	
            	if(version == null) {
                    result.setReferencedIn(getActionTaskReferencedBy(null, result.getUFullName(), username));
            	}
                
    			// Clear the actual encrypted data from default values. 
    			clearEcryptedFieldDefaults(result);
            }
        } 
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
    } // getActionIdFromFullname

    private static ResolveActionTaskVO getResolveActionTaskVersion(String sysId, String version) throws Exception {


    	try {
	    	return (ResolveActionTaskVO) HibernateProxy.execute( () -> {
	    		
	    		ResolveActionTaskVO result = null;
	    		
	    		CriteriaBuilder queryBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
		    	CriteriaQuery<ActionTaskArchive> queryCriteria = queryBuilder.createQuery(ActionTaskArchive.class);
		    	Root<ActionTaskArchive> atArchiveRoot = queryCriteria.from(ActionTaskArchive.class);
		    	List<ActionTaskArchive> versionList = null;
		    	
	    	// latest 
	    	if(version == null || com.resolve.services.util.Constants.VERSION_LATEST.equals(version)) {
	    		queryCriteria.where(queryBuilder.equal(atArchiveRoot.get("UTableId"), sysId));
	        	queryCriteria.orderBy(queryBuilder.desc(atArchiveRoot.get("UVersion")));
	        	Query<ActionTaskArchive> query = HibernateUtil.getCurrentSession().createQuery(queryCriteria);
	        	versionList = query.setMaxResults(1).list();
	    	}
		    	// latest stable
	    	else if(com.resolve.services.util.Constants.VERSION_LATEST_STABLE.equals(version)) {
		        	queryCriteria.where(queryBuilder.equal(atArchiveRoot.get("UIsStable"), true),
		        			queryBuilder.equal(atArchiveRoot.get("UTableId"), sysId));
		        	queryCriteria.orderBy(queryBuilder.desc(atArchiveRoot.get("UVersion")));
		        	Query<ActionTaskArchive> query = HibernateUtil.getCurrentSession().createQuery(queryCriteria);
	        	versionList = query.setMaxResults(1).list();
		        // concrete version
	        } else {
		        	queryCriteria.where(queryBuilder.equal(atArchiveRoot.get("UTableId"), sysId),
		        			queryBuilder.equal(atArchiveRoot.get("UVersion"), version));
		        	Query<ActionTaskArchive> query = HibernateUtil.getCurrentSession().createQuery(queryCriteria);
	        	versionList = query.setMaxResults(1).list();
		        }
		        
	        if(versionList != null && versionList.size() >= 1) {
		        	ActionTaskArchive archive = versionList.get(0);
		        	String content = archive.getUContent();
		        	result = new ObjectMapper().readValue(
		        			content, ResolveActionTaskVO.class);
		        	Integer archiveVersion = archive.getUVersion();
		        	Boolean isStable = archive.isUIsStable();
		        	result.setUVersion(archiveVersion);
	        	result.setId(sysId);
		        	result.setUIsStable(isStable);
	    	} else {
	    		// for migration purposes: latest version might not have a record in the archive table
	    		// TODO: should be commented out post-6.4
	    		if(version == null || com.resolve.services.util.Constants.VERSION_LATEST.equals(version)) {
	    			result = null;
	    		} else {
		    		throw new MissingVersionException(String.format("Missing version: %s for wiki document with id: %s", version, sysId));	    			
		    	}
	    	}
		        
		        return result;
	    	});
	
	        
    	} catch (Exception ex) {
    		Log.log.info("Failed to retrieve action task version: " + ex.getMessage());
    		throw ex;
    	}
        
	}

	//whereClause = "root_menu_id,/", "root_module_id" --> select distinct UNamespace from ResolveActionTask
    ///resolve,/ --> select distinct UMenuPath from ResolveActionTask where (UMenuPath like 'resolve%' OR UMenuPath like '/resolve%')
    // /resolve,. --> select distinct UNamespace from ResolveActionTask where (UNamespace like 'resolve%' OR UNamespace like '/resolve%')
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<ActionTaskTreeDTO> getActionTaskTree(QueryDTO queryDTO, String username) throws Exception
    {
        List<ActionTaskTreeDTO> result = new ArrayList<ActionTaskTreeDTO>();
        String whereClause = queryDTO.getWhereClause();
        String sql = null;
        
        if(StringUtils.isNotBlank(whereClause))
        {
            // '/' is menu path
            // '.' is namespace/module name
            String delimiter = "/"; //by default, its the menu path
            String whereClauseArray[] = whereClause.split(",");
            
            if(whereClauseArray.length == 2)
            {
                whereClause = whereClauseArray[0];
                delimiter = whereClauseArray[1].trim();
            }
            else if(whereClauseArray.length == 1)
            {
                whereClause = whereClauseArray[0];
            }
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            String assignedToWhereClause = getWhereClauseForAssignedToFilter(queryDTO.getFilters(), queryParams);
            if(delimiter.equalsIgnoreCase("/"))
            {
                //this is for menu path
                sql = "select distinct at.UMenuPath from ResolveActionTask at where at.UMenuPath is not null ";
                if (!whereClause.contains("menu"))
                {
                    //sql = sql + " and at.UMenuPath like '/" + whereClause + "%' ";
                    sql = sql + " and at.UMenuPath like :UMenuPath ";
                    queryParams.put("UMenuPath", "/" + whereClause + "%");
                }
                else
                {
                    //sql = sql + " and at.UMenuPath  like '/%'";
                    sql = sql + " and at.UMenuPath  like :UMenuPath";
                    queryParams.put("UMenuPath", "/%");
                }
                
                if(StringUtils.isNotBlank(assignedToWhereClause))
                {
                    sql = sql +  " and " + assignedToWhereClause;
                }
                
                sql = sql + "  order by at.UMenuPath ";
            }
            else if(delimiter.equalsIgnoreCase("."))
            {
                //this is for namespace
                sql = "select distinct at.UNamespace from ResolveActionTask at ";
                if (!whereClause.contains("module"))
                {
                    //sql = sql + " where at.UNamespace like '" + whereClause + "%' ";
                    sql = sql + " where at.UNamespace like :UNamespace ";
                    queryParams.put("UNamespace", whereClause + "%");
                }
                
                if(StringUtils.isNotBlank(assignedToWhereClause))
                {
                    sql = sql + " and " + assignedToWhereClause;
                }

                sql = sql + "  order by at.UNamespace ";
            }
            
            /*String safeSQL =*/ SQLUtils.getSafeSQL(sql);
            
            //execute the sql
            List<String> list = null;
            try
            {
            	String sqlFinal = sql;
              HibernateProxy.setCurrentUser(username);
                list = (List<String>) HibernateProxy.execute(() -> {
                	  Query query = HibernateUtil.createQuery(sqlFinal);
                      
                      if (queryParams != null && !queryParams.isEmpty())
                      {
                          for (String queryParam : queryParams.keySet())
                          {
                              query.setParameter(queryParam, queryParams.get(queryParam));
                          }
                      }
                      
                      return query.list();
                });
                
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
            
            if(delimiter.equalsIgnoreCase("/"))
            {
                String root = whereClause;
                if(whereClause.equalsIgnoreCase("root_menu_id"))
                {
                    root = "";
                }
                result.addAll(prepareATTreeDTOForNamespace(list, "/" + root, "/"));
            }
            else if(delimiter.equalsIgnoreCase("."))
            {
                String root = whereClause;
                if(whereClause.equalsIgnoreCase("root_module_id"))
                {
                    root = "";
                }
                result.addAll(prepareATTreeDTOForNamespace(list, root, "."));
            }
        }
       
        return result;
    }
    
    public static String getActionIdFromFullname(String fullname, String username)
    {
    	String result = null;
    	String selectQuery = "select at.sys_id from ResolveActionTask at where at.UFullName = :UFullName";
    	if(cacheTaskId.containsKey(fullname)) {
    		result = cacheTaskId.get(fullname);
    	}
    	if(result == null) {
	    	try {
	    		final List<String> list = new ArrayList<String>();
				Consumer<HibernateUtil>block = (util) -> {
					try {
						@SuppressWarnings("unchecked")
						Query<String> q = HibernateUtil.createQuery(selectQuery);
						q.setParameter("UFullName", fullname);
						list.addAll(q.list());
					} catch(Exception e){
						Log.log.error(e.getMessage(),e);
					}
				};
				HibernateUtil.action(block, username);
				result = list.get(0);
				cacheTaskId.put(fullname, result);
				cacheTaskName.put(result, fullname);
			} catch (Exception e) {
				Log.log.error(e.getMessage(),e);
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
        return result;
    } // getActionIdFromFullname

    public static String getActionTaskNameFromId(String sysId, String username)
    {
    	String result = null;
    	String selectQuery = "select UName from resolve_action_task where sys_id=:task_sid";
    	if(cacheTaskName.containsKey(sysId)) {
    		result = cacheTaskName.get(sysId);
    	}
    	if(result == null) {
	    	try {
	    		final List<String> list = new ArrayList<String>();
				Consumer<HibernateUtil>block = (util) -> {
					try {
						@SuppressWarnings("unchecked")
						Query<String> q = HibernateUtil.createQuery(selectQuery);
						q.setParameter("task_sid", sysId);
						list.addAll(q.list());
					} catch(Exception e){
						Log.log.error(e.getMessage(),e);
					}
				};
				HibernateUtil.action(block, username);
				result = list.get(0);
				cacheTaskId.put(result, sysId);
				cacheTaskName.put(sysId, result);
			} catch (Exception e) {
				Log.log.error(e.getMessage(),e);
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
        return result;
    } // getActionIdFromFullname

    public static boolean hasRightsToActionTask(String actiontaskSysId, String actiontaskFullName, String username, RightTypeEnum rightType)
    {
        boolean hasRights = false;
        
        if(StringUtils.isNotBlank(username) && (StringUtils.isNotBlank(actiontaskSysId) || StringUtils.isNotBlank(actiontaskFullName)))
        {
            //get user roles
            Set<String> userRoles = UserUtils.getUserRoles(username);
            
            //get the actiontask rights
            ResolveActionTaskVO actionTask = getResolveActionTask(actiontaskSysId, actiontaskFullName, username);
            if(actionTask != null)
            {
                AccessRightsVO accessRights = actionTask.getAccessRights();
                if(accessRights != null)
                {
                    String rights = accessRights.getUAdminAccess();
                    if(rightType == RightTypeEnum.edit)
                    {
                        rights = rights + "," + accessRights.getUWriteAccess();
                    }
                    else if(rightType == RightTypeEnum.execute)
                    {
                        rights = rights + "," + accessRights.getUExecuteAccess();
                    }
                    else
                    {
                        rights = rights + "," + accessRights.getUReadAccess();
                    }
                    
                    hasRights = com.resolve.services.util.UserUtils.hasRole(username, userRoles, rights);
                }
            }
        }
        
        return hasRights;
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Object> findNamespaceForImpex(QueryDTO queryDTO, String username)
        {
            List<? extends Object> resultList = null;
            List<ResolveActionTaskVO> result = new ArrayList<ResolveActionTaskVO>();
            int pageSize = queryDTO.getLimit();
            int start = queryDTO.getStart();
            Integer count = null;
            
            StringBuilder sql = new StringBuilder();
            sql.append(" SELECT DISTINCT (UNamespace) from ResolveActionTask ");
            
            List<QueryFilter> qFilterList = QueryFilter.decodeJson(queryDTO.getFilter());
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            if (qFilterList.size() > 0)
            {
                sql.append(" where ");
                for (QueryFilter filter : qFilterList)
                {
                    QueryParameter l = filter.getParameters().get(0);
                    String params = filter.getField();
                    
                    //sql.append("LOWER (").append(params).append(") ").append(filter.getComp()).append(" '").append(l.getValue()).append("' and ");
                    sql.append("LOWER (").append(params).append(") ").append(filter.getComp()).append(" :").append(params).append(" and ");
                    queryParams.put(params, l.getValue());
                    
                    filter.getParameters();
                    Log.log.trace(sql);
                }
                sql.replace(sql.length() - 4, sql.length(), "");
                start = -1;
                pageSize = 0;
            }
            
            sql.append(" order by UNamespace ");
            
            if (StringUtils.isNotBlank(queryDTO.getSort()))
            {
                try
                {
                    String sort = queryDTO.getSort().substring(1, queryDTO.getSort().length() - 1);
                    Map<String,String> descMap = new ObjectMapper().readValue(sort, HashMap.class);
                    
                    List<String> dbOrderbyFunctionsBlackList = PropertiesUtil.getPropertyList(Constants.SECURITY_DB_ORDER_BY_FUNCTIONS_BLACKLIST_SYSTEM_PROPERTY);
                    
                    // Validate direction against order by functions black list
                    
                    if (dbOrderbyFunctionsBlackList != null && !dbOrderbyFunctionsBlackList.isEmpty() &&
                        dbOrderbyFunctionsBlackList.contains(descMap.get("direction").toLowerCase()))
                    {
                        throw new RuntimeException("Unsupported function in order by clause.");
                    }
                        
                    sql.append(descMap.get("direction"));
                    
                }
                catch (Exception e)
                {
                    Log.log.warn("Error " + e.getLocalizedMessage() + " occurred in  parsing Sort direction, setting it to DESC.");
                    sql.append(" DESC ");
                }
            }
            else
            {
                sql.append(" DESC ");
            }

			try {
				resultList = (List<? extends Object>) HibernateProxy.execute(() -> {
					return GeneralHibernateUtil.executeHQLSelect(sql.toString(), queryParams);
	
				});
				count = (Integer) HibernateProxy.execute(() -> {
					String countSql = "SELECT DISTINCT(UNamespace) AS COUNT from ResolveActionTask";
					List<? extends Object> countList = GeneralHibernateUtil.executeHQLSelect(countSql, -1, 0);
					return countList.size();
	
				});
			} catch (Exception e) {
				Log.log.error(e.getMessage(), e);
                               HibernateUtil.rethrowNestedTransaction(e);
			}
            
            if (resultList != null)
            {
                int i = (start-1) * pageSize ;
                for (Object obj : resultList)
                {
                    ResolveActionTaskVO actionTaskVO = new ResolveActionTaskVO();
                    actionTaskVO.setId(i++ + "");
    //                actionTaskVO.setSys_id((String)objArray[0]);
                    actionTaskVO.setUNamespace((String)obj);
                    result.add(actionTaskVO);
                }               
            }
            
            Map<String, Object> resultMap = new HashMap<String, Object> ();
            resultMap.put("ACTION_TASK_VO_LIST", result);
            resultMap.put("TOTAL_COUNT", count);
            
            return resultMap;
        }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<String> getAllActionTaskNames(/*String whereClause*/)
    {
        List<String> result = new ArrayList<String>();
        QueryDTO queryDTO = new QueryDTO();
        
        queryDTO.setSqlQuery("select u_fullname from resolve_action_task order by u_fullname asc");        
        queryDTO.setUseSql(true);
                
        try
        {
          HibernateProxy.setCurrentUser("system");
        	List<String> data = (List<String>) HibernateProxy.execute(() -> {
            	 Query query = HibernateUtil.createSQLQuery(queryDTO.getSelectSQLNonParameterizedSort());
            	 return query.list();
            });
            
            if(data != null)
            {
                for (String aRow : data)
                {
                    result.add(aRow);
                }
            }
            
                
        }
        catch (Throwable t)
        {
            Log.log.error(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
    
        return result;
    }

    public static ResolveActionTaskMockDataVO getActionTaskMockDataVO(String actionTaskId, String mockName)
    {
        // find mock name
        if (StringUtils.isNotBlank(mockName))
        {
            
            try
            {
                return (ResolveActionTaskMockDataVO) HibernateProxy.execute(() -> {
                	Collection<ResolveActionTaskMockData> collection = null;
                	ResolveActionTask actiontask = HibernateUtil.getDAOFactory().getResolveActionTaskDAO().findById(actionTaskId);
                    if (actiontask != null)
                    {
                        collection = actiontask.getResolveActionInvoc().getMockDataCollection();
                        
                        int size = collection.size();
                        if (size > 0)
                        {
                            for (ResolveActionTaskMockData mock : collection)
                            {
                                if (mock.getUName().equalsIgnoreCase(mockName))
                                {
                                    return mock.doGetVO();
                                }
                            }
                        }
                    }
                    
                    return null;
                    
                });
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
                
            }
        }
        return null;
        
    } // getActionTaskMockData

    public static boolean isActionTaskExist(String sysId, String fullname)
    {
        boolean exist = false;
        ResolveActionTask actionTask = null;

        try
        {
            actionTask = getResolveActionTaskModel(sysId, fullname, false, "system");
            if (actionTask != null)
            {
                exist = true;
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return exist;
    } // getActionIdFromFullname

    @SuppressWarnings("unchecked")
	public static Collection<ResolveActionParameterVO> getParamsForActionTaskSysId(String taskSysId, String username)
    {
        Collection<ResolveActionParameterVO> results = null;
        Collection<ResolveActionParameter> params = null;
        
        
        if (StringUtils.isNotEmpty(taskSysId))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
               params = (Collection<ResolveActionParameter>) HibernateProxy.execute(() -> {
                	
                	ResolveActionTask actionTask = null;
                	actionTask = HibernateUtil.getDAOFactory().getResolveActionTaskDAO().findById(taskSysId);
                    if (actionTask != null)
                    {
                        ResolveActionInvoc resolveActionInvoc = actionTask.getResolveActionInvoc();
                        if(resolveActionInvoc != null)
                        {
                            return resolveActionInvoc.getResolveActionParameters();
                        }
                    }
                    
                    return null;
                });
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
            
            if(params != null)
            {
                results = new ArrayList<ResolveActionParameterVO>();
                for(ResolveActionParameter param : params)
                {
                    results.add(param.doGetVO());
                }
            }
        }
        
        return results;
        
        
    }
    
	public static ResolveActionTaskVO saveResolveActionTask(ResolveActionTaskVO vo, String username) throws Exception {
		return saveActionTask(vo, null, username, null, VersionPersistAction.UPDATE);
	}
	
	/**
	 * Use this method in order to create version for the given AT. The version can be saved or committed. 
	 */
	public static ResolveActionTaskVO saveResolveActionTask(ResolveActionTaskVO vo, String rawContent, String username, String comment, VersionPersistAction versionPersistAction)
			throws Exception {
		return saveActionTask(vo, rawContent, username, comment, versionPersistAction);
	}

	private static ResolveActionTaskVO saveActionTask(ResolveActionTaskVO vo, String rawContent, String username, String comment, VersionPersistAction versionPersistAction) throws Exception {
		if (vo != null) {
			// Restore encrypted default values before saving in order not to overwrite
			// them.
			restoreEcryptedFieldDefaults(vo, username);

			setBlankValues(vo);

			SaveActionTask atUtil;
			if(rawContent == null) {
			    atUtil = new SaveActionTask(vo, username);
			    atUtil.setVersionPersistAction(versionPersistAction);
			} else {
			    atUtil = new SaveActionTask(vo, rawContent, username, comment, versionPersistAction);
			}

			vo = atUtil.save();
			if(vo != null) {
				Map<String, Object> params = new HashMap<String, Object>();
	            params.put(Constants.EXECUTE_ACTIONID, vo.getSys_id());
	            MainBase.getESB().sendInternalMessage(Constants.ESB_NAME_RSCONTROLS, "MAction.cacheFlush", params);
			}
			ActionTaskIndexAPI.indexActionTask(vo.getSys_id(), username);

			// Clear the actual encrypted data from default values.
			clearEcryptedFieldDefaults(vo);
		}

		return vo;
	}
    
    public static Set<String> deleteResolveActionTaskByIds(List<String> sysIds, boolean validate, String username) throws Exception
    {
        return new DeleteActionTask(sysIds, null, validate, username).delete();
    }    
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static ResolveActionTask getResolveActionTaskModel(String sysId, String atName, boolean loadReferences, String username)
    {

        try
        {
          HibernateProxy.setCurrentUser(username);
        	return (ResolveActionTask) HibernateProxy.execute(() -> {
        		ResolveActionTask actionTask = null;
        		
                if(StringUtils.isNotBlank(sysId))
                {
                    actionTask = HibernateUtil.getDAOFactory().getResolveActionTaskDAO().findById(sysId);
                }
                else if(StringUtils.isNotBlank(atName))
                {
                    String whereClause = null;
                    if(atName.indexOf('#') > -1)
                    {
                        //whereClause = " where LOWER(UFullName) = '" + atName.toLowerCase().trim() + "'" ;
                    	whereClause = " where LOWER(UFullName) = :fullName" ;
                    }
                    else
                    {
                        //whereClause = " where LOWER(UName) = '" + atName.toLowerCase().trim() + "'" ;
                        whereClause = " where LOWER(UName) = :name" ;
                    }
                    
                    //to make this case-insensitive
                    String sql = "from ResolveActionTask " + whereClause;
                    Query query = HibernateUtil.createQuery(sql.toString());
                    
                    if(atName.indexOf('#') > -1)
                    {
                    	query.setParameter("fullName", atName.toLowerCase().trim());
                    }
                    else
                    {
                    	query.setParameter("name", atName.toLowerCase().trim());
                    }
                    
                    List<Object> list = query.list();
                    if (list != null && list.size() > 0)
                    {
                        actionTask = (ResolveActionTask) list.get(0);
                    }
                }
                
                if (actionTask != null) {
                	
                    ResolveActionInvoc resolveActionInvoc = actionTask.getResolveActionInvoc();
                    if(resolveActionInvoc != null && loadReferences)
                    {
                        if(resolveActionInvoc.getParser() != null)
                        {
                            Collection<ResolveParserTemplate> resolveParserTemplates = resolveActionInvoc.getParser().getResolveParserTemplates();
                            if(resolveParserTemplates != null)
                            {
                                resolveParserTemplates.size();
                            }
                        }
                        
                        if(resolveActionInvoc.getResolveActionInvocOptions() != null)
                        {
                            resolveActionInvoc.getResolveActionInvocOptions().size();
                        }
                        
                        if(resolveActionInvoc.getResolveActionParameters() != null)
                        {
                            resolveActionInvoc.getResolveActionParameters().size();
                        }
                        if(resolveActionInvoc.getMockDataCollection() != null)
                        {
                            resolveActionInvoc.getMockDataCollection().size();
                        }
                        
                        if (resolveActionInvoc.getExpressions() != null)
                    	{
                        	resolveActionInvoc.getExpressions().size();
                    	}
                    	
                    	if (resolveActionInvoc.getOutputMappings() != null)
                    	{
                    		resolveActionInvoc.getOutputMappings().size();
                    	}
                    }
                    
                	//load the tags
                	if(actionTask.getAtTagRels() != null)
                	{
                		actionTask.getAtTagRels().size();
                	}                
                }
                
                return actionTask;
                
        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
            
            return null;
        }
        
        
    }

    public static ResolveActionTask validateUserForActionTaskNoCache(String sysId, String atFullName, RightTypeEnum right, String username) throws Exception
    {
        ResolveActionTask result = null;
        
        try 
        {
          HibernateProxy.setCurrentUser(username);
            result = (ResolveActionTask) HibernateProxy.executeNoCache(() -> {
            	return validateUserForActionTask(sysId, atFullName, right, username);
            });
        }
        catch(Exception e)
        {
            Log.log.warn(e.getMessage(), e);
            throw e;
        }
        
        return result;
    }
    
    //use this api whenever there is an 'Edit' of actiontask
    public static ResolveActionTask validateUserForActionTask(String sysId, String atFullName, RightTypeEnum right, String username) throws Exception
    {
      //make sure that actiontask does not exist with the same name
        ResolveActionTask dbAt = getResolveActionTaskModel(sysId, atFullName, true, username);
        if(dbAt == null)
        {
            throw new Exception("Actiontask with sysId '" + sysId + "', name : "+ atFullName +" does not exist.");
        }
        
//        if(dbAt.getAccessRights() == null)
//        {
//            throw new Exception("Actiontask " + atFullName + " , " + sysId + " has No access rights record in the DB.");
//        }

        if(dbAt.getAccessRights() != null)
        {
            //prepare the roles based on the right type
            String roles = dbAt.getAccessRights().getUAdminAccess();
            if(right == RightTypeEnum.view)
            {
                roles = roles + "," + dbAt.getAccessRights().getUReadAccess();
            }
            else if(right == RightTypeEnum.edit)
            {
                roles = roles + "," + dbAt.getAccessRights().getUWriteAccess();
            }
            else if(right == RightTypeEnum.execute)
            {
                roles = roles + "," + dbAt.getAccessRights().getUExecuteAccess();
            }
            
            //get user roles and validate
            Set<String> userRoles = UserUtils.getUserRoles(username);
            boolean userCanEdit = com.resolve.services.util.UserUtils.hasRole(username, userRoles, roles);
            if(!userCanEdit)
            {
                throw new Exception("User does not have rights for the actiontask " + dbAt.getUFullName());
            }
        }
        else
        {
            //so no rights mean that user has rights and when we save it , default rights will be added to it
        }
        return dbAt;
    }


    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static Set<String> getAllActionTaskSysIds()
    {
        Set<String> sysIds = new TreeSet<String>();
        try
        {
            HibernateProxy.execute(() -> {
            	// fetch the info based on the search criteria
                String sql = "select at.sys_id from ResolveActionTask as at";

                Query query = HibernateUtil.createQuery(sql);
                List<Object> actionTasks = query.list();
                if (actionTasks != null && actionTasks.size() > 0)
                {
                    for(Object sysId : actionTasks)
                    {
                        sysIds.add(sysId.toString());
                    }
                }
            });
            
        }
        catch (Throwable t)
        {
            Log.log.info(t.getMessage(), t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }

        return sysIds;
    }// getAllWikidocSysIds
    
    /**
     * API to populate ResolveActionTaskVO from the resultset.<p>
     * NOTE: Please make sure the 'select'ed fields in the query are sys_id, UName, UNamespace, USummary, sysUpdatedOn and in that order ONLY!.<p>
     * Any other field of more fields will either throw ClassCastException or IndexOutOfBoundException.<p>
     *  
     * @param list : List of Objects (list) containing the 'select' fields in the query (sys_id, UName, UNamespace, USummary, sysUpdatedOn) in that order
     * @return : List of ResolveActionTaskVO
     */
    public static List<ResolveActionTaskVO> populateResolveActionTaskVO(List<? extends Object> list)
    {
        List<ResolveActionTaskVO> result = new ArrayList<ResolveActionTaskVO>();
        
        if (CollectionUtils.isNotEmpty(list))
        {
            list.stream().forEach(obj -> {
                Object[] objArray = (Object[]) obj;
                ResolveActionTaskVO actionTaskVO = new ResolveActionTaskVO();
                
                actionTaskVO.setId((String)objArray[0]);
                actionTaskVO.setSys_id((String)objArray[0]);
                actionTaskVO.setUName((String)objArray[1]);
                actionTaskVO.setUNamespace((String)objArray[2]);
                actionTaskVO.setUSummary((String)objArray[3]);
                actionTaskVO.setSysUpdatedOn((Timestamp) objArray[4]);
                result.add(actionTaskVO);
            });           
        }
        
        return result;
    }
    
//    private static Set<String> getTagNamesForActionTask(String atSysId)
//    {
//        Set<String> result = new HashSet<String>();
//        
//        List<Tag> tags = getTagsForActiontask(atSysId);
//        if(tags != null)
//        {
//            for(Tag tag : tags)
//            {
//                //to protect from bad data we need this check 
//                if(tag != null && StringUtils.isNotBlank(tag.getName()))
//                {
//                    result.add(tag.getName());
//                }
//            }
//        }
//        
//        
//        return result;
//    }
//    
//    private static List<Tag> getTagsForActiontask(String atSysId)
//    {
//        List<Tag> result = new ArrayList<Tag>();
//        
//        if(StringUtils.isNotEmpty(atSysId))
//        {
//            try
//            {
//                List<ResolveTag> tags = ServiceTag.getTagsForComponent(atSysId);
//                if(tags != null)
//                {
//                    for(ResolveTag tag : tags)
//                    {
//                        result.add(ServiceTag.findTagById(tag.getSys_id(), "system"));
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                Log.log.error("error in getting tags for document:" + atSysId);
//            }
//        }
//        
//        return result;
//    }
//    

    public static List<String> moveOrRenameActionTask (List<String> ids, String newName, String newModuleName, String userName) throws Exception
    {
        List<String> result = new ArrayList<String>();
        Set<String> noRightsAT = new HashSet<String>();
        
        if(ids == null || ids.size() == 0 || StringUtils.isEmpty(newModuleName) || StringUtils.isBlank(userName))
        {
            throw new Exception("Actiontask Ids, Module Name and Username are mandatory for the Move operation.");
        }
        
        //get the userroles 
        Set<String> userRoles = UserUtils.getUserRoles(userName);
        if(ids.size() == 1)
        {
            String sysId = ids.get(0);
            if(StringUtils.isEmpty(newName))
            {
                throw new Exception("Missing Actiontask name to be moved to.");
            }
            
            ResolveActionTaskVO taskVO = getActionTaskWithReferences(sysId, null, userName);
            if (taskVO != null)
            {
                boolean hasAdminRights = com.resolve.services.util.UserUtils.hasRole(userName, userRoles, taskVO.getAccessRights().getUAdminAccess());
                if(hasAdminRights)
                {
                    String oldFullName = taskVO.getUFullName();
                    String newFullName = newName + "#" + newModuleName;
                    
                    if (getResolveActionTask(null, newFullName, userName) == null)
                    {
                        updateMoveOrRenameActionTask(taskVO, newModuleName, newName, userName, false);
                        updateWikiActionTaskRelation(oldFullName, newFullName, userName);

                        ActionTaskIndexAPI.indexActionTask(taskVO.getSys_id(), userName);
                    }
                    else
                    {
                        //if the AT with new name already exist.
                        result.add(newFullName);
                    }
                }
                else
                {
                    noRightsAT.add(taskVO.getUFullName());
                }
            }
        }
        else
        {           
            for(String sysId : ids)
            {
                ResolveActionTaskVO taskVO = getActionTaskWithReferences(sysId, null, userName);
                if (taskVO != null)
                {
                    boolean hasAdminRights = com.resolve.services.util.UserUtils.hasRole(userName, userRoles, taskVO.getAccessRights().getUAdminAccess());
                    if(hasAdminRights)
                    {
                        newName = taskVO.getUName(); //Names will be same 
                        String oldFullName = taskVO.getUFullName();
                        String newFullName = newName + "#" + newModuleName;
                        
                        if (getResolveActionTask(null, newFullName, userName) == null)
                        {
                            updateMoveOrRenameActionTask(taskVO, newModuleName, newName, userName, false);
                            updateWikiActionTaskRelation(oldFullName, newFullName, userName);
                            
                            ActionTaskIndexAPI.indexActionTask(taskVO.getSys_id(), userName);
                        }
                        else
                        {
                            //if the AT with new name already exist.
                            result.add(newFullName);
                        }
                    }
                    else
                    {
                        noRightsAT.add(taskVO.getUFullName());
                    }
                }
            }
        }
        
        if(noRightsAT.size() > 0)
        {
            throw new Exception("For Move operation, User needs Admin rights for " + StringUtils.convertCollectionToString(noRightsAT.iterator(), ","));
        } else {
        	Map<String, Object> params = new HashMap<String, Object>();
            params.put(Constants.EXECUTE_ACTIONID, ids);
            MainBase.getESB().sendInternalMessage(Constants.ESB_NAME_RSCONTROLS, "MAction.cacheFlush", params);
        }
        
        return result;
    }
    
    
    
    public static List<String> copyActionTasks(List<String> ids, String newName, String newModuleName, boolean overwrite, String userName) throws Exception
    {
        List<String> error = new ArrayList<String>();
        Set<String> noRightsAT = new HashSet<String>();
        
        if(ids == null || ids.size() == 0 || StringUtils.isEmpty(newModuleName) || StringUtils.isBlank(userName))
        {
            throw new Exception("Actiontask Ids, Module Name and Username are mandatory for the Copy operation.");
        }
        
        
        //get the userroles 
        Set<String> userRoles = UserUtils.getUserRoles(userName);
        if(ids.size() == 1)
        {
            String sysId = ids.get(0);
            if(StringUtils.isEmpty(newName))
            {
                throw new Exception("Missing Actiontask name to be copied to.");
            }
            
            ResolveActionTaskVO taskVO = getResolveActionTaskModel(sysId, null, true, userName).doGetVO();
            if (taskVO != null)
            {
                boolean hasAdminRights = com.resolve.services.util.UserUtils.hasRole(userName, userRoles, taskVO.getAccessRights().getUAdminAccess());
                if(hasAdminRights)
                {
//                    String oldFullName = taskVO.getUFullName();
                    String newFullName = newName + "#" + newModuleName;
                    
                    ResolveActionTaskVO localTaskVO = getResolveActionTask(null, newFullName, userName);
                    if ( localTaskVO == null)
                    {
                        updateMoveOrRenameActionTask(taskVO, newModuleName, newName, userName, true);
                    }
                    else
                    {
                        if (overwrite)
                        {
                            List<String> sysIds = new ArrayList<String>();
                            sysIds.add(localTaskVO.getSys_id());
                            ActionTaskUtil.deleteResolveActionTaskByIds(sysIds, false, userName);
                            updateMoveOrRenameActionTask(taskVO, newModuleName, newName, userName, true);
                        }
                        else
                        {
                            error.add(newFullName);
                        }
                    }
                    
                }
                else
                {
                    noRightsAT.add(taskVO.getUFullName());
                }
            }
        }
        else
        {
            for(String sysId : ids)
            {
                ResolveActionTaskVO taskVO = getActionTaskWithReferences(sysId, null, userName);
                if (taskVO != null)
                {
                    boolean hasAdminRights = com.resolve.services.util.UserUtils.hasRole(userName, userRoles, taskVO.getAccessRights().getUAdminAccess());
                    if(hasAdminRights)
                    {
                        newName = taskVO.getUName(); //Names will be same 
//                        String oldFullName = taskVO.getUFullName();
                        String newFullName = newName + "#" + newModuleName;
                        
                        ResolveActionTaskVO localTaskVO = getResolveActionTask(null, newFullName, userName);
                        if (localTaskVO == null)
                        {
                            updateMoveOrRenameActionTask(taskVO, newModuleName, taskVO.getUName(), userName, true);
                        }
                        else
                        {
                            if (overwrite)
                            {
                                List<String> sysIds = new ArrayList<String>();
                                sysIds.add(localTaskVO.getSys_id());
                                ActionTaskUtil.deleteResolveActionTaskByIds(sysIds, false, userName);
                                updateMoveOrRenameActionTask(taskVO, newModuleName, taskVO.getUName(), userName, true);
                            }
                            else
                            {
                                error.add(taskVO.getUName() + "#" + newModuleName);
                            }
                        }
                    }
                    else
                    {
                        noRightsAT.add(taskVO.getUFullName());
                    }
                }
            }

        }
        
        if(noRightsAT.size() > 0)
        {
            throw new Exception("For Copy operation, User needs Admin rights for " + StringUtils.convertCollectionToString(noRightsAT.iterator(), ","));
        }
       
        return error;
    }
    
    public static boolean getLogRawResultFlag()
    {
        return PropertiesUtil.getPropertyBoolean(Constants.ACTIONTASK_LOG_RAW_RESULT);
    }
    
    //TODO: note that sysId is not funtional but kept for future use
    private static List<ReferenceDTO> getActionTaskReferencedBy(String actiontaskSysId, String actionTaskFullName, String username)
    {
        List<ReferenceDTO> result = new ArrayList<ReferenceDTO>();
        
        if(StringUtils.isNotBlank(actionTaskFullName))
        {
            Map<String, ReferenceDTO> data = new HashMap<String, ReferenceDTO>();

            //prepare the data
            getActionTaskReferencedInWikiContent(data, actiontaskSysId, actionTaskFullName, username);
            getActionTaskReferencedInWikiMainModel(data, actiontaskSysId, actionTaskFullName, username);
            getActionTaskReferencedInWikiAbortModel(data, actiontaskSysId, actionTaskFullName, username);
            
            //add it to the list
            result.addAll(data.values());
        }
        
        return result;
        
    }
    
    @SuppressWarnings("unlikely-arg-type")
	private static void getActionTaskReferencedInWikiContent(Map<String, ReferenceDTO> data , String actiontaskSysId, String actionTaskFullName, String username)
    {
        ContentWikidocActiontaskRel rel = new ContentWikidocActiontaskRel();
        rel.setUActiontaskFullname(actionTaskFullName);
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
                ContentWikidocActiontaskRelDAO dao = HibernateUtil.getDAOFactory().getContentWikidocActiontaskRelDAO();
                List<ContentWikidocActiontaskRel> list = dao.find(rel);
                if (list != null && list.size() > 0)
                {
                    for (ContentWikidocActiontaskRel r : list)
                    {
                        if(r.getContentWikidoc() != null)
                        {
                            String docName = r.getContentWikidoc().getUFullname();
                            ReferenceDTO ref = null;
                            if(data.containsValue(docName))
                            {
                                ref = data.get(docName);
                            }
                            else
                            {
                                ref = new ReferenceDTO();
                                ref.setName(docName);
                            }
                            
                            //set the flag to true
                            ref.setRefInContent(true);
                            
                            //put the obj in map
                            if (data.containsKey(docName))
                            {
                                data.get(docName).setRefInContent(true);
                            }
                            else
                            {
                                data.put(docName, ref);
                            }
                        }
                    }
                }

        	});
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }
    
    @SuppressWarnings("unlikely-arg-type")
	private static void getActionTaskReferencedInWikiMainModel(Map<String, ReferenceDTO> data , String actiontaskSysId, String actionTaskFullName, String username)
    {
        MainWikidocActiontaskRel rel = new MainWikidocActiontaskRel();
        rel.setUActiontaskFullname(actionTaskFullName);

        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
                MainWikidocActiontaskRelDAO dao = HibernateUtil.getDAOFactory().getMainWikidocActiontaskRelDAO();
                List<MainWikidocActiontaskRel> list = dao.find(rel);
                if (list != null && list.size() > 0)
                {
                    for (MainWikidocActiontaskRel r : list)
                    {
                        if(r.getMainWikidoc() != null)
                        {
                            String docName = r.getMainWikidoc().getUFullname();
                            ReferenceDTO ref = null;
                            if(data.containsValue(docName))
                            {
                                ref = data.get(docName);
                            }
                            else
                            {
                                ref = new ReferenceDTO();
                                ref.setName(docName);
                            }
                            
                            //set the flag to true
                            ref.setRefInMain(true);
                            
                            //put the obj in map
                            if (data.containsKey(docName))
                            {
                                data.get(docName).setRefInMain(true);
                            }
                            else
                            {
                                data.put(docName, ref);
                            }
                        }
                    }
                }

        	});
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }
    
    @SuppressWarnings("unlikely-arg-type")
	private static void getActionTaskReferencedInWikiAbortModel(Map<String, ReferenceDTO> data , String actiontaskSysId, String actionTaskFullName, String username)
    {
        ExceptionWikidocActiontaskRel rel = new ExceptionWikidocActiontaskRel();
        rel.setUActiontaskFullname(actionTaskFullName);

        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
                ExceptionWikidocActiontaskRelDAO dao = HibernateUtil.getDAOFactory().getExceptionWikidocActiontaskRelDAO();
                List<ExceptionWikidocActiontaskRel> list = dao.find(rel);
                if (list != null && list.size() > 0)
                {
                    for (ExceptionWikidocActiontaskRel r : list)
                    {
                        if(r.getExceptionWikidoc() != null)
                        {
                            String docName = r.getExceptionWikidoc().getUFullname();
                            ReferenceDTO ref = null;
                            if(data.containsValue(docName))
                            {
                                ref = data.get(docName);
                            }
                            else
                            {
                                ref = new ReferenceDTO();
                                ref.setName(docName);
                            }
                            
                            //set the flag to true
                            ref.setRefInAbort(true);
                            
                            //put the obj in map
                            if (data.containsKey(docName))
                            {
                                data.get(docName).setRefInAbort(true);
                            }
                            else
                            {
                                data.put(docName, ref);
                            }
                        }
                    }
                }

        	});
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }
    
    private static List<ActionTaskTreeDTO> prepareATTreeDTOForNamespace(List<String> menupaths, String root, String delimiter)
    {
        List<ActionTaskTreeDTO> result = new ArrayList<ActionTaskTreeDTO>();
        
        //local copy for manipulation
        List<String> localMenupaths = new ArrayList<String>(menupaths);
        Set<String> childMenuPaths = new HashSet<String>();
        Set<String> menuPathsHavingLeafs = new HashSet<String>();
        
        //'/adaptors/session'
        for(String menupath : localMenupaths)
        {
            if(menupath.equals(root))
            {
                continue;
            }
            
            String childName = menupath; // '/adaptors/session'
            if(!root.equalsIgnoreCase("/")) // /adaptors
            {
                childName = childName.substring(root.length()); // '/session'
            }

            //remove the '/'
            if(childName.startsWith(delimiter))
            {
                childName = childName.substring(1);
            }
            
            //if there is more '/' than in position 0, it means there are more levels to it
            if(childName.indexOf(delimiter) >= 1)
            {
                // childName --> /session/test
                String regex = "/";
                if(delimiter.equalsIgnoreCase("."))
                {
                    regex = "\\.";
                }
                
                String[] arr = childName.split(regex);
                childName = arr[0];
                menuPathsHavingLeafs.add(childName);
            }
            
            //add it to the set
            childMenuPaths.add(childName);
        }
     
        //prepare the result set
        for(String childMenupath :  childMenuPaths)
        {
            ActionTaskTreeDTO treeDTO = new ActionTaskTreeDTO();
            treeDTO.setName(childMenupath);
            treeDTO.setId(childMenupath);
            
            if(menuPathsHavingLeafs.contains(childMenupath))
            {
                treeDTO.setLeaf(false);
            }
            else
            {
                treeDTO.setLeaf(true);
            }
            
            result.add(treeDTO);
        }//end of for loop
        
        //sort it
        Collections.sort(result, new Comparator<ActionTaskTreeDTO>()
        {
            public int compare(final ActionTaskTreeDTO dto1, final ActionTaskTreeDTO dto2)
            {
                return dto1.getName().compareToIgnoreCase(dto2.getName());
            }
        });
        
        return result;
    }
    
    
    private static void updateMoveOrRenameActionTask(ResolveActionTaskVO taskVO, String moduleName, String name, String userName, boolean copy) throws Exception
    {
        String taskFullName = name + "#" + moduleName;
        String oldFullName = taskVO.getUFullName();
        taskVO.setUName(name);
        taskVO.setUNamespace(moduleName);
        taskVO.setUFullName(taskFullName);
        
        if (taskVO.getResolveActionInvoc() != null)
        {
            ResolveActionInvocVO actionInvocVO = taskVO.getResolveActionInvoc();
            actionInvocVO.setUDescription(taskFullName);
            
            if (actionInvocVO.getParser() != null)
            {
                ResolveParserVO parser = actionInvocVO.getParser();
                if (parser.getUName().equals(oldFullName))
                {
                    if (copy)
                    {
                        resetParser(parser, taskFullName, userName);
                    }
                    else
                    {
                        parser.setUName(taskFullName);
                    }
                }
            }
            
            if (actionInvocVO.getPreprocess() != null)
            {
                ResolvePreprocessVO  preprocess = actionInvocVO.getPreprocess();
                if (preprocess.getUName().equals(oldFullName))
                {
                    if (copy)
                    {
                        resetPreprocesor(preprocess, taskFullName, userName);
                    }
                    else
                    {
                        preprocess.setUName(taskFullName);
                    }
                }
            }
            
            if (actionInvocVO.getAssess() != null)
            {
                ResolveAssessVO assess = actionInvocVO.getAssess();
                if (assess.getUName().equals(oldFullName))
                {
                    if (copy)
                    {
                        resetAssess(assess, taskFullName, userName);
                    }
                    else
                    {
                        assess.setUName(taskFullName);
                    }
                }
            }
        	
        	if (actionInvocVO.getExpressions() != null)
        	{
        		if (copy)
        		{
        			resetActionTaskCompColl(actionInvocVO.getExpressions(), userName);
        		}
        	}
        	
        	if (actionInvocVO.getOutputMappings() != null)
        	{
        		if (copy)
        		{
        			resetActionTaskCompColl(actionInvocVO.getOutputMappings(), userName);
        		}
        	}
            
            if (actionInvocVO.getResolveActionInvocOptions() != null)
            {
                Collection<ResolveActionInvocOptionsVO> invocOptionsColl = actionInvocVO.getResolveActionInvocOptions();
                if (copy)
                {
                    resetActionTaskCompColl(invocOptionsColl, userName);
                }
                else
                {
                    for (ResolveActionInvocOptionsVO optionsVO : invocOptionsColl)
                    {
                        if (optionsVO.getUName().equalsIgnoreCase("inputfile"))
                        {
                            optionsVO.setUDescription(taskFullName);
                        }
                    }
                }
            }
            
            if (actionInvocVO.getResolveActionParameters() != null)
            {
                Collection<ResolveActionParameterVO> paramVOColl = actionInvocVO.getResolveActionParameters();
                
                if(copy)
                {
                	resetActionTaskCompColl(paramVOColl, userName);
                }
            }
            if (actionInvocVO.getResolveActionTaskMockData() != null)
            {
                Collection<ResolveActionTaskMockDataVO> mockDataVO = actionInvocVO.getResolveActionTaskMockData();
                if(copy)
                {
                	resetActionTaskCompColl(mockDataVO, userName);
                }
            }
            if (taskVO.getAccessRights() != null)
            {
                if (copy)
                {
                    resetAccessRights(taskVO.getAccessRights(), userName);
                }
                else
                {
                    taskVO.getAccessRights().setUResourceName(taskFullName);
                }
            }
            
            if (copy)
            {
                resetActionInvoc(actionInvocVO, moduleName, name, userName);
                resetActionTask(taskVO, moduleName, name, userName);
            }
            
            try
            {
                saveResolveActionTask(taskVO, userName);
            }
            catch (Exception e)
            {
            	if (StringUtils.isBlank(e.getMessage()) || 
                    !(e.getMessage().startsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_PREFIX) &&
                      (e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_SUFFIX) ||
                       e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_HA_VIOLATED_SUFFIX)))) {
                    Log.log.error(StringUtils.isBlank(e.getMessage()) ? "Error in saving action task." : e.getMessage(), e);
                } else {
                	throw e;
                }
            }
        }
    }
    
    private static void resetActionTask(ResolveActionTaskVO resolveActionTask, String newNamespace, String newName, String userName)
    {
        resolveActionTask.setSys_id(null);
        resolveActionTask.setId(null);
        resolveActionTask.setUName(newName);
        resolveActionTask.setUNamespace(newNamespace);
        resolveActionTask.setUFullName(newName + "#" + newNamespace);
        resolveActionTask.setSysUpdatedOn(null);
        resolveActionTask.setSysCreatedOn(null);
        resolveActionTask.setSysUpdatedBy(null);
        resolveActionTask.setSysCreatedBy(userName);
    }
    
    private static void resetAccessRights(AccessRightsVO accessRights, String username)
    {
        accessRights.setSys_id(null);
        accessRights.setId(null);
        accessRights.setSysUpdatedOn(null);
        accessRights.setSysCreatedOn(null);
        accessRights.setSysUpdatedBy(null);
        accessRights.setSysCreatedBy(username);
        accessRights.setUResourceId(null);
    }
    
    private static void resetActionInvoc(ResolveActionInvocVO resolveActionInvoc, String newNamespace, String newName, String userName)
    {
        resolveActionInvoc.setSys_id(null);
        resolveActionInvoc.setId(null);
        resolveActionInvoc.setSysUpdatedOn(null);
        resolveActionInvoc.setSysCreatedOn(null);
        resolveActionInvoc.setSysUpdatedBy(null);
        resolveActionInvoc.setSysCreatedBy(userName);
        resolveActionInvoc.setUDescription(newName + "#" + newNamespace);
    }
    
    private static void resetParser(ResolveParserVO parser, String newFullName, String userName)
    {
        // if the action task name and the preprocessor names are same
        parser.setSys_id(null);
        parser.setId(null);
        parser.setUName(newFullName);
        parser.setResolveParserTemplates(null);// WHAT IS THIS
        parser.setSysUpdatedOn(null);
        parser.setSysCreatedOn(null);
        parser.setSysUpdatedBy(null);
        parser.setSysCreatedBy(userName);
    }
    
    private static void resetPreprocesor(ResolvePreprocessVO preprocess, String newFullName, String userName)
    {
        preprocess.setSys_id(null);
        preprocess.setId(null);
        preprocess.setUName(newFullName);
        preprocess.setSysUpdatedOn(null);
        preprocess.setSysCreatedOn(null);
        preprocess.setSysUpdatedBy(null);
        preprocess.setSysCreatedBy(userName);
    }
    
    private static void resetAssess(ResolveAssessVO assess, String newFullName, String userName)
    {
        assess.setSys_id(null);
        assess.setId(null);
        assess.setUName(newFullName);
        assess.setSysUpdatedOn(null);
        assess.setSysCreatedOn(null);
        assess.setSysUpdatedBy(null);
        assess.setSysCreatedBy(userName);
    }
    
    private static void resetActionTaskCompColl(Collection<? extends Object> actionTaskComponentColl, String userName)
    {
        if (actionTaskComponentColl.size() > 0)
        {
            for (Object obj : actionTaskComponentColl)
            {
                try
                {
                    PropertyUtils.setProperty(obj, HibernateConstants.SYS_ID, null);
                    PropertyUtils.setProperty(obj, "id", null);
                    PropertyUtils.setProperty(obj, "sysUpdatedBy", null);
                    PropertyUtils.setProperty(obj, "sysUpdatedOn", null);
                    PropertyUtils.setProperty(obj, "sysCreatedBy", userName);
                    PropertyUtils.setProperty(obj, "sysCreatedOn", null);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static void updateWikiActionTaskRelation(String oldFullName, String newFullName, String userName)
    {
        Set<WikiDocumentVO> wikiDocSet = new HashSet<WikiDocumentVO>();
        Map<String, WikiDocumentVO> wikiDocMap = new HashMap<String, WikiDocumentVO>();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("MainWikidocActiontaskRel");
        query.setWhereClause("UActiontaskFullname = '" + oldFullName + "'");
        
        List <? extends Object> data = null;
        try
        {
            data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, -1, false);
        }
        catch(Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        if (data != null && !data.isEmpty())
        {
            for (Object o : data)
            {
                MainWikidocActiontaskRel temp = (MainWikidocActiontaskRel)o;
                temp.setUActiontaskFullname(newFullName);
                
                // Find wikidoc record from main_wikidoc_task_rel table, i.e., 
                // find wiki automation that contains the action tasks to be renamed, 
                // and rename the action task names in u_model_process only
                WikiDocumentVO mainWikiDoc = temp.getMainWikidoc().doGetVO();
                String wikiSysId = mainWikiDoc.getSys_id();
                if(wikiDocMap.containsKey(wikiSysId))
                    mainWikiDoc = wikiDocMap.get(wikiSysId);
                if(changeWikiModel(oldFullName, newFullName, mainWikiDoc, "main"))
                    wikiDocMap.put(wikiSysId, mainWikiDoc);
                
                try
                {
                  HibernateProxy.setCurrentUser(userName);
                    HibernateProxy.execute(() -> HibernateUtil.getDAOFactory().getMainWikidocActiontaskRelDAO().update(temp));
                }
                catch(Exception e)
                {
                	Log.log.error(e.getMessage(), e);
                                   HibernateUtil.rethrowNestedTransaction(e);
                }
            }
        }
        
        query.setModelName("ContentWikidocActiontaskRel");
        query.setWhereClause("upper(UActiontaskFullname) = upper('" + oldFullName + "')");

        try
        {
            data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, -1, false);
        }
        catch(Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        if (data != null && !data.isEmpty())
        {
            Set<String> wikiSet = new HashSet<String>();
            
            for (Object o : data)
            {
                ContentWikidocActiontaskRel temp = (ContentWikidocActiontaskRel)o;
                temp.setUActiontaskFullname(newFullName);
                
                String wikiId = temp.getContentWikidoc().getSys_id();
                if(wikiSet.contains(wikiId))
                    continue;
                else
                    wikiSet.add(wikiId);
                
                // Find wikidoc record from content_wikidoc_task_rel table, i.e., 
                // find wiki automation that contains the action tasks to be renamed,
                // and rename the action task names in u_content only
                WikiDocumentVO contentWikiDoc = temp.getContentWikidoc().doGetVO();
                String wikiSysId = contentWikiDoc.getSys_id();
                if(wikiDocMap.containsKey(wikiSysId))
                    contentWikiDoc = wikiDocMap.get(wikiSysId);
                if(changeWikiModel(oldFullName, newFullName, contentWikiDoc, "content"))
                    wikiDocMap.put(wikiSysId, contentWikiDoc);
                
                try
                {
                  HibernateProxy.setCurrentUser(userName);
                	HibernateProxy.execute(() -> HibernateUtil.getDAOFactory().getContentWikidocActiontaskRelDAO().update(temp));
                }
                catch(Exception e)
                {
                	Log.log.error(e.getMessage(), e);
                                   HibernateUtil.rethrowNestedTransaction(e);
                }
            }
        }
        
        query.setModelName("ExceptionWikidocActiontaskRel");
        query.setWhereClause("UActiontaskFullname = '" + oldFullName + "'");

        try
        {
            data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, -1, false);
        }
        catch(Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        if (data != null && !data.isEmpty())
        {
            for (Object o : data)
            {
                ExceptionWikidocActiontaskRel temp = (ExceptionWikidocActiontaskRel)o;
                temp.setUActiontaskFullname(newFullName);
                
                // Find wikidoc record from exception_wikidoc_task_rel table, i.e., 
                // find wiki automation that contains the action tasks to be renamed, 
                // and rename the action task names in u_model_exception only
                WikiDocumentVO exceptionWikiDoc = temp.getExceptionWikidoc().doGetVO();
                
                String wikiSysId = exceptionWikiDoc.getSys_id();
                if(wikiDocMap.containsKey(wikiSysId))
                    exceptionWikiDoc = wikiDocMap.get(wikiSysId);
                if(changeWikiModel(oldFullName, newFullName, exceptionWikiDoc, "exception"))
                    wikiDocMap.put(wikiSysId, exceptionWikiDoc);
                
                try
                {
                  HibernateProxy.setCurrentUser(userName);
                	HibernateProxy.execute(() -> HibernateUtil.getDAOFactory().getExceptionWikidocActiontaskRelDAO().update(temp));
                }
                catch(Exception e)
                {
                	Log.log.error(e.getMessage(), e);
                                   HibernateUtil.rethrowNestedTransaction(e);
                }
            }
        }
        
        String taskId = getActionIdFromFullname(newFullName, "admin");
        String hql = "from ResolveCompRel where childId = :taskId and childType = 'Task'";
        data = null;
        
        try
        {
          HibernateProxy.setCurrentUser(userName);
        	data = (List<? extends Object>) HibernateProxy.execute(() -> {
	        	Query q = HibernateUtil.createQuery(hql);
	            q.setParameter("taskId", taskId);
	            return q.list();
	            });
        }
        catch(Exception e)
        {
            Log.log.error(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        if (data != null && data.size() > 0)
        {
        	for (Object o : data)
        	{
        		ResolveCompRel compRel = (ResolveCompRel) o;
        		String wikiId = compRel.getParentId();
        		
        		WikiDocumentVO wikiVO = WikiUtils.getWikiDoc(wikiId, null, userName);
        		
        		String dtModel = wikiVO.getUDecisionTree();
                if (StringUtils.isNotBlank(dtModel) && dtModel.contains(oldFullName))
                {
                	dtModel = dtModel.replace(oldFullName.substring(0, oldFullName.indexOf('#')), newFullName.substring(0, newFullName.indexOf('#')));
                	dtModel = dtModel.replace(oldFullName, newFullName);
                	WikiUtils.updateWikiModel("UDecisionTree", dtModel, wikiId);
                }
        	}
        }
        
        if(wikiDocMap.size() > 0)
        {
            Set<String> keySet = wikiDocMap.keySet();
            for(String key:keySet)
                wikiDocSet.add(wikiDocMap.get(key));
            
            ServiceHibernate.saveListOfWikiDocs(wikiDocSet, userName);
            
            // Wiki archive
            WikiArchiveUtil.updateWikiArchives(wikiDocSet, oldFullName, newFullName, userName);
        }
        
        // Custom Form
        updateMetaFormActionTaskName(oldFullName, newFullName, userName);
    }
    
    private static void updateMetaFormActionTaskName(String oldFullName, String newFullName, String userName) {
        
        QueryDTO query = new QueryDTO();
        List <? extends Object> data = null;
        
        query.setModelName("MetaFormAction");
        query.setWhereClause("UActionTaskName = '" + oldFullName + "'");

        try
        {
            data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, -1, false);
        }
        catch(Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        
        if (data != null && !data.isEmpty())
        {
            for (Object o : data)
            {
                MetaFormAction temp = (MetaFormAction)o;
                temp.setUActionTaskName(newFullName);
                
                try
                {
                  HibernateProxy.setCurrentUser(userName);
                    HibernateProxy.execute(() -> HibernateUtil.getDAOFactory().getMetaFormActionDAO().update(temp));
                }
                catch(Exception e)
                {
                	Log.log.error(e.getMessage(), e);
                                   HibernateUtil.rethrowNestedTransaction(e);
                }
            }
        }
    }
    
    protected static boolean changeWikiModel(String oldFullName, String newFullName, WikiDocumentVO wiki, String field)
    {
        boolean changed = false;
        
        String modelProcess = wiki.getUModelProcess();
        String modelFinal = wiki.getUModelFinal();
        String modelAbort = wiki.getUModelException();
        String wikiNamespace = wiki.getUNamespace();
        String content = wiki.getUContent();

        Log.log.debug("oldFullName = " + oldFullName);
        Log.log.debug("newFullName = " + newFullName);
        
        if(StringUtils.isNotBlank(modelProcess) && field.equals("main")) {
            String newModel = ParseUtil.replaceString(oldFullName, newFullName, modelProcess);
            if(newModel != null && modelProcess.compareTo(newModel) != 0) {
                wiki.setUModelProcess(newModel);
                changed = true;
                Log.log.debug("Changed!!!");
            }
        }
        
        if(StringUtils.isNotBlank(modelFinal)) {
            String newFinal = ParseUtil.replaceString(oldFullName, newFullName, modelFinal);
            if(modelFinal != null && modelFinal.compareTo(newFinal) != 0) {
                wiki.setUModelFinal(newFinal);
                changed = true;
            }
        }
        
        if(StringUtils.isNotBlank(modelAbort) && field.equals("exception")) {
            String newAbort = ParseUtil.replaceString(oldFullName, newFullName, modelAbort);
            if(newAbort != null && modelAbort.compareTo(newAbort) != 0) {
                wiki.setUModelException(newAbort);
                changed = true;
            }
        }
  
        if(StringUtils.isNotBlank(content) && field.equals("content")) {
            String newContent = content;
            
            if(newContent.indexOf("{action") != -1)
                newContent = ActionTaskUtil.replaceAction(oldFullName, newFullName, newContent, "{action", wikiNamespace);
            if(newContent.indexOf("{result2:") != -1)
                newContent = ActionTaskUtil.replaceAction(oldFullName, newFullName, newContent, "{result2", wikiNamespace);
            if(newContent.indexOf("{result:") != -1)
                newContent = ActionTaskUtil.replaceAction(oldFullName, newFullName, newContent, "{result", wikiNamespace);
            if(newContent.indexOf("{detail:") != -1)
                newContent = ActionTaskUtil.replaceAction(oldFullName, newFullName, newContent, "{detail", wikiNamespace);
    
            if(newContent.compareTo(content) != 0) {
                wiki.setUContent(newContent);
                changed = true;
            }
        }

        return changed;        
    }
    
    public static String replaceAction(String oldFullName, String newFullName, String content, String tag, String wikiNamespace) {
        
        ArrayList<String> actions = parseSections(content, tag);
        
        if(actions.size() == 0)
            return content;
        
        String[] oldTask = oldFullName.split("#");
        String[] newTask = newFullName.split("#");
        
        String oldName = oldTask[0];
        String newName = newTask[0];
        String oldNamespace = oldTask[1];
        String newNamespace = newTask[1];
        
        String oldTask1 = oldName;
        String oldTask2 = oldName + "&#35;" + oldNamespace;
        String oldTask3 = "namespace=" + oldNamespace + "|task=" + oldName;
        
        String newTask1 = newName;
        String newTask2 = newName + "&#35;" + newNamespace;
        String newTask3 = "namespace=" + newNamespace + "|task=" + newName;
        
        for(String action:actions) {
            String currentAction = action;
            ArrayList<String> tasks = parseTasks(action, tag);
            for(String task:tasks) {
                if(StringUtils.isBlank(task))
                    continue;
//                Log.log.debug("Old task line = " + task);
                
                String taskUpper = task.toUpperCase();
                String oldFullNameUpper = oldFullName.toUpperCase();
                
                if(taskUpper.indexOf(oldFullNameUpper) != -1)
                    action = replaceStringIgnoreCase(oldFullName, newFullName, action);
                else if(taskUpper.indexOf(oldTask2.toUpperCase()) != -1)
                    action = replaceStringIgnoreCase(oldTask2, newTask2, action);
                else if(taskUpper.indexOf(oldTask3.toUpperCase()+"}") != -1) 
                    action = replaceStringIgnoreCase(oldTask3, newTask3, action);
                else
                    ;
            } // end of for(tasks).
            
            if(!currentAction.equals(action)) {
                content = ParseUtil.replaceString(currentAction, action, content);
                Log.log.debug("New content = " + content);
            }
        } // end of for(actions).
        
        if(content.toUpperCase().indexOf("namespace=" + oldNamespace + "|task=" + oldTask1.toUpperCase()) == -1 
        && content.toUpperCase().indexOf(oldTask1.toUpperCase() + "#" + oldNamespace) == -1
        && wikiNamespace != null && oldNamespace.equalsIgnoreCase(wikiNamespace) && newNamespace.equalsIgnoreCase(wikiNamespace))
            content = handleMissingNamespace(oldTask1, newTask1, oldNamespace, content);
        
        return content;
    }
    
    private static String handleMissingNamespace(String oldTask, String newTask, String oldNamespace, String oldContent) {
        
        StringBuilder sb = new StringBuilder();
        String temp = new String(oldContent);
        String oldTaskUpper = oldTask.toUpperCase();
        String newTaskUpper = newTask.toUpperCase();
        
        while(temp.toUpperCase().contains(oldTaskUpper)) {
            int start = temp.toUpperCase().indexOf(oldTaskUpper);
            int startNew = temp.toUpperCase().indexOf(newTaskUpper);
            
            if(start > 6 && temp.charAt(start-6) == '|') {
                sb.append(temp.substring(0, start+oldTask.length()));
                temp = temp.substring(start+oldTask.length());
                continue;
            }
                
            else if(temp.charAt(start+oldTask.length()) == '#') {
                sb.append(temp.substring(0, start+oldTask.length()+oldNamespace.length()+1));
                temp = temp.substring(start+oldTask.length()+oldNamespace.length()+1);
                continue;
            }
            
            else if(temp.charAt(start+oldTask.length()) == '&') {
                sb.append(temp.substring(0, start+oldTask.length()+oldNamespace.length()+5));
                temp = temp.substring(start+oldTask.length()+oldNamespace.length()+5);
                continue;
            }
            
            else if(start == startNew) {
                String targetTask = temp.substring(start, start+newTask.length());
                if(oldTask.length() > newTask.length())
                    targetTask = temp.substring(start, start+oldTask.length());
                
                if(StringUtils.isNotEmpty(targetTask) && targetTask.equals(newTask)) {
                    sb.append(temp.substring(0, start+newTask.length()));
                    temp = temp.substring(start+newTask.length());
                    continue;
                }
                
                sb.append(temp.substring(0, start));
                sb.append(newTask);
                temp = temp.substring(start+oldTask.length());
            }
            
            else {
                sb.append(temp.substring(0, start));
                sb.append(newTask);
                temp = temp.substring((start+oldTask.length()));
            }            
        }
        
        return sb.toString() + temp;
    }
 
    private static String replaceStringIgnoreCase(String oldTask, String newTask, String oldAction)
    {
        if(oldAction == null || newTask == null || newTask.length() == 0)
            return oldAction;

         StringBuilder sb = new StringBuilder();
         String temp = new String(oldAction);
         
         while(temp.toUpperCase().contains(oldTask.toUpperCase())) {
             if(oldTask.contains("|task=")) {
                 String taskName = parseAction(temp);
                 int start = temp.toUpperCase().indexOf(taskName.toUpperCase());
                 
                 if(StringUtils.isNotEmpty(taskName) && taskName.equalsIgnoreCase(oldTask)) {
                     sb.append(temp.substring(0, start));
                     sb.append(newTask+"}");
                     temp = temp.substring(start+oldTask.length()+1);
                 }
                 
                 else {
                     sb.append(temp.substring(0, start+taskName.length()+1));
                     temp = temp.substring(start+taskName.length()+1);
                 }
             }
             
             else if(!temp.toUpperCase().contains(newTask.toUpperCase()) ||
                      temp.toUpperCase().contains(newTask.toUpperCase()) && oldTask.toUpperCase().contains(newTask.toUpperCase())) {
                 int start = temp.toUpperCase().indexOf(oldTask.toUpperCase());
                 sb.append(temp.substring(0, start));
                 sb.append(newTask);
                 temp = temp.substring(start+oldTask.length());
             }

             else {
                 int start = temp.lastIndexOf(newTask);
                 
                 if(start != -1) {
                     sb.append(temp.substring(0, start+newTask.length()));
                     temp = temp.substring(start+newTask.length());
                 }
                 
                 else
                     break;
             }
         }
             
         return sb.toString() + temp;
     }
    
    private static String parseAction(String action) {
        
        String taskFullName = "";
        int start = action.indexOf("{");
        int end = action.indexOf("}");
        
        if(action.startsWith("{description")) {
            start =  action.indexOf("{description");
            action = action.substring(start);
            start = action.indexOf("|");
            end = action.indexOf("}"); 
        }
        
        else if(action.startsWith("{namespace")) {
            start = action.indexOf("{namespace");
            action = action.substring(start);
            start = action.indexOf("{namespace");
            end = action.indexOf("}");
        }
        
        else
            return taskFullName;
        
        if(start != -1 && end != -1 && end > start)
            taskFullName = action.substring(start+1, end);
        
        return taskFullName;
    }
    
    public static ArrayList<String> parseSections(String content, String tag) {
        
        ArrayList<String> sections = new ArrayList<String>();
        
        while(content.indexOf(tag) != -1) {
            String section = "";
            if(tag.equals("{action"))
                section = StringUtils.substringBetween(content, tag, tag+"}");
            else
                section = StringUtils.substringBetween(content, tag+":", tag+"}");

            if(StringUtils.isBlank(section))
                break;
            
            String result = section;
            int index = section.indexOf("}");
            if(index != -1) {
                section = section.substring(index+1).trim();
                
                if(StringUtils.isNotEmpty(section)) {
                    if(tag.equals("{result2") && (section.startsWith("{") && !section.endsWith("}") || !section.startsWith("{") && section.endsWith("}") ) ||
                       tag.equals("{detail") && section.startsWith("{") && !section.endsWith("}")) {
                        Log.log.error("The following syntax error occurred in the content, the action tasks in the content may not be renamed properly.");
                        Log.log.error(tag + result);
                        index = content.indexOf(result);
                        content = content.substring(index+result.length() + tag.length() + 1);
                    }
                    
                    else {
                        if(tag.equals("{action"))
                            sections.add("{action}" + section + "{action}");
                        else if(tag.equals("{detail")) {
                            if(section.contains("#"))
                                sections.add("{detail:" + result + "{detail}");
                            else
                                sections.add(section);
                        }
                        else
                            sections.add(section);
                        
                        index = content.indexOf(section);
                        content = content.substring(index+section.length() + tag.length() + 1);
                    }
                }
                
                else {
                    index = content.indexOf(result);
                    content = content.substring(index+result.length() + tag.length() + 1);
                }
            }
            
            else
                break;
        } // end of while().
        
        return sections;
    }
    
    public static ArrayList<String> parseTasks(String action, String tag) {
        
        ArrayList<String> tasks = new ArrayList<String>();
        
        if(tag.equals("{action")) {
            tasks.add(action);
            return tasks;
        }
        
        if(tag.equals("{result2") || (tag.equals("{detail") && !action.contains("#"))) {
            StringTokenizer st = new StringTokenizer(action, "{");
            while(st.hasMoreTokens()) {
                tasks.add("{" + st.nextToken());
            }
        }
        
        else {
            BufferedReader reader = new BufferedReader(new StringReader(action));
            try {
                String task = reader.readLine();
                while(task != null) {
                    tasks.add(task);
                    task = reader.readLine();
                }
            } catch(Exception e) {
                Log.log.error(e.getMessage());
            } finally {
                if(reader != null) {
                    try {
                        reader.close();
                    } catch(Exception ee) {
                        Log.log.error(ee.getMessage());
                    }
                }
            }
        }
        
        return tasks;
    }
    
    private static String getWhereClauseForAssignedToFilter(List<QueryFilter> filters, Map<String, Object> queryParams)
    {
        String result = "";
         
        if(filters != null && filters.size() > 0)
        {
            for(QueryFilter filter : filters)
            {
                String field = filter.getField();
                if(field.equalsIgnoreCase("assignedToUsername"))
                {
                    String comp = filter.getComp();
                    if(filter.getParameters() != null && filter.getParameters().size() > 0)
                    {
                        String value = (String) filter.getParameters().get(0).getValue();
                        //result = " at.assignedTo.UUserName " + comp + " '" + value + "'";
                        result = " at.assignedTo.UUserName " + comp + ":UUserName";
                        queryParams.put("UUserName", value);
                    }
                    break;
                }
            }
        }
        
        return result;
    }
    
    private static Map<Integer, String> severityResultMap = new HashMap<Integer, String>();
    private static Map<Integer, String> conditionResultMap = new HashMap<Integer, String>();
    private static final Map<String, String> SOURCE_MAP = new HashMap<String, String>();
    
    static
    {
    	severityResultMap.put(0, "GOOD");
    	severityResultMap.put(1, "WARNING");
    	severityResultMap.put(2, "SEVERE");
    	severityResultMap.put(3, "CRITICAL");
    	
    	conditionResultMap.put(0, "GOOD");
    	conditionResultMap.put(1, "BAD");
    	
    	SOURCE_MAP.put("INPUT", "INPUTS");
    	SOURCE_MAP.put("OUTPUT", "OUTPUTS");
        SOURCE_MAP.put("FLOW", "FLOWS");
        SOURCE_MAP.put("PARAM", "PARAMS");
        SOURCE_MAP.put("WSDATA", "WSDATA");
        SOURCE_MAP.put("PROPERTY", "PROPERTIES");
    }
    
    public static String generateSeverityAndConditionScript(List<ResolveTaskExpression> expressions)
    {
    	StringBuilder script = new StringBuilder();
    	script.append("import static com.resolve.util.ComparisonUtils.*;\n\n");
    	script.append("RESULT.severity=\"GOOD\";\n");
    	script.append("RESULT.condition=\"GOOD\";\n");
//    	ResolveActionTaskVO taskVO = getActionTaskWithReferencesExceptGraph(taskId, null, "admin");
//    	if (taskVO != null)
//    	{
//    		ResolveActionInvocVO actionInvocVO = taskVO.getResolveActionInvoc();
//    		if (actionInvocVO != null)
//    		{
    			//Collection<ResolveTaskExpressionVO> expressions = actionInvocVO.getExpressions();
    			if (expressions != null)
    			{
    				Map<Integer, List<ResolveTaskExpression>> orderedResultLevelConditions = new TreeMap<Integer, List<ResolveTaskExpression>>();
    				Map<Integer, List<ResolveTaskExpression>> orderedResultLevelSeverity = new TreeMap<Integer, List<ResolveTaskExpression>>();
    				
    				for (ResolveTaskExpression expression : expressions)
    				{
    					List<ResolveTaskExpression> conditionList = null;
    					if(expression.getExpressionType().equalsIgnoreCase("condition"))
    					{
    						conditionList = orderedResultLevelConditions.get(expression.getResultLevel());
    					}
    					else
    					{
    						conditionList = orderedResultLevelSeverity.get(expression.getResultLevel());
    					}
    					
						if (conditionList == null)
						{
							conditionList = new ArrayList<ResolveTaskExpression>();
						}
						conditionList.add(expression);
						
						if (expression.getExpressionType().equalsIgnoreCase("condition"))
    					{
							orderedResultLevelConditions.put(expression.getResultLevel(), conditionList);
    					}
    					else
    					{
    						orderedResultLevelSeverity.put(expression.getResultLevel(), conditionList);
    					}
    				}
    				
    				script.append(generateScript(orderedResultLevelSeverity, "severity")).append("\n");
    				script.append(generateScript(orderedResultLevelConditions, "condition"));
    			}
    		//}
    	//}
    	//System.out.println(script.toString());
    	return script.toString();
    }
    
    private static String generateScript(Map<Integer, List<ResolveTaskExpression>> resuleLevelOrderedSeverityOrCondition, String expressionType)
    {
    	StringBuilder severityScript = new StringBuilder();
    	if (resuleLevelOrderedSeverityOrCondition.size() > 0)
		{
			for (Integer severityOrder : resuleLevelOrderedSeverityOrCondition.keySet())
			{
				// List of expressions whose result level is same. 3 -> Critical or 2 -> Severe etc...
				List<ResolveTaskExpression> severities = resuleLevelOrderedSeverityOrCondition.get(severityOrder);
				
				Map<Integer, List<ResolveTaskExpression>> orderedSeverities = new TreeMap<Integer, List<ResolveTaskExpression>>();
				for (ResolveTaskExpression severityExpression : severities)
				{
					List<ResolveTaskExpression> expressionVOSeverityList = orderedSeverities.get(severityExpression.getOrder());
					if (expressionVOSeverityList == null)
					{
						expressionVOSeverityList = new ArrayList<ResolveTaskExpression>();
					}
					expressionVOSeverityList.add(severityExpression);
					orderedSeverities.put(severityExpression.getOrder(), expressionVOSeverityList);
				}
				
				if (orderedSeverities.size() > 0)
				{
					severityScript.append("\nif( (");
					String resultLevel = null;
					for (Integer order : orderedSeverities.keySet())
					{
						List<ResolveTaskExpression> list = orderedSeverities.get(order);
						if (expressionType.equals("severity"))
						{
							resultLevel = severityResultMap.get(list.get(0).getResultLevel());
						}
						else
						{
							resultLevel = conditionResultMap.get(list.get(0).getResultLevel());
						}
						
						for (ResolveTaskExpression severityExpression : list)
						{
							severityScript.append(getExpression(severityExpression));
							severityScript.append(" || ");
						}
						severityScript.setLength(severityScript.length() - 4);
						severityScript.append(") && (");
					}
					severityScript.setLength(severityScript.length() - 6);
					severityScript.append(") )\n");
					
					severityScript.append("{\n");
					severityScript.append("	RESULT." + expressionType + "=\"").append(resultLevel).append("\"\n");
					severityScript.append("}");
				}
			}
		}
    	return severityScript.toString();
    }

	private static String getExpression(ResolveTaskExpression expressionVO)
    {
    	StringBuilder script = new StringBuilder();
    	String criteria = expressionVO.getCriteria(); //ANY, ALL
        String leftOperandType = expressionVO.getLeftOperandType();
        String leftOperandName = expressionVO.getLeftOperandName();
        String operator = expressionVO.getOperator(); //EQUALS, >, >= etc.
        String rightOperandType = expressionVO.getRightOperandType();
        String rightOperandName = expressionVO.getRightOperandName();
        //String expressionType = expressionVO.getExpressionType(); // condition or severity
        //Integer resultLevel = expressionVO.getResultLevel(); // 0-Good (both condition and severity), 1-Warning (Severify) and BAD (Condition), 2-Severe (severity), 3-Critical (severity)
        
        String variableString = null;
        String sourceString = null;

        variableString = getComparisonItem(leftOperandType, leftOperandName, "string");
        sourceString = getComparisonItem(rightOperandType, rightOperandName, "string");
        
        // these operators are borrowed from AB and UI '=' is mapped to '==' on server.
        if (operator.equals("="))
        {
        	operator = "==";
        }
        if ("all".equalsIgnoreCase(criteria))
        {
        	script.append("compareAll(").append(variableString).append(",").append(sourceString).append(",\"").append((operator)).append("\")");
        }
        else
        {
        	script.append("compareAny(").append(variableString).append(",").append(sourceString).append(",\"").append((operator)).append("\")");
        }
        
        return script.toString();
    }
    
    private static String getComparisonItem(String itemSource, String item, String itemType)
    {
        StringBuilder result = new StringBuilder();
        if ("constant".equalsIgnoreCase(itemSource))
        {
            if ("string".equalsIgnoreCase(itemType))
            {
                result.append("\"").append(item).append("\"");
            }
            else
            {
                result.append(item);
            }
        }
        else
        {
            result.append(itemSource).append("[\"").append(item).append("\"]");
        }
        return result.toString();
    }
    
    public static String generateOutputMappingScript(List<ResolveTaskOutputMapping> uiOutputMappingList)
    {
    	StringBuilder script = new StringBuilder();
    	
    	if (uiOutputMappingList != null && uiOutputMappingList.size() > 0)
    	{
    		for (ResolveTaskOutputMapping outputMapping : uiOutputMappingList)
    		{
    			script.append(outputMapping.getOutputType()).append(".").append(outputMapping.getOutputName()).append(" = ");
    			if (outputMapping.getSourceType().equalsIgnoreCase("constant"))
    			{
    				script.append("\"").append(outputMapping.getSourceName()).append("\";\n");
    			}
    			else
    			{
    				script.append(outputMapping.getSourceType()).append(".").append(outputMapping.getSourceName()).append(";\n");
    			}
    			
    		}
    	}
    	System.out.println(script.toString());
    	return script.toString();
    }
    
    public static boolean migrateComponent(ResolveActionTaskVO taskVO, String type, String username) {
        
        boolean completed = false;
        
        ResolveActionInvocVO actionInvocVO = taskVO.getResolveActionInvoc();
        String taskFullName = taskVO.getUFullName();
        
        
        if (actionInvocVO != null)
        {
            try
            {
        
              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
            		String newName = taskFullName;
            		
                    switch(type) {
                    case MigrateActionTask.ASSESSOR:
                        ResolveAssessVO assess = actionInvocVO.getAssess();
                        
                        if(assess != null) {
                            assess.setSys_id(null);
                            if(MigrateActionTask.assessNames.contains(taskFullName))
                                newName = taskFullName + System.nanoTime();
                            assess.setUName(newName);
                            Log.log.debug("new assess name = " + newName);
                            
                            ResolveAssess model = new ResolveAssess();
                            model.applyVOToModel(assess);
                            HibernateUtil.getDAOFactory().getResolveAssessDAO().persist(model);
                            assess = model.doGetVO();
                            Log.log.debug("new assess id = " + assess.getSys_id());
                            actionInvocVO.setAssess(assess);
                        }
                        
                        break;
                        
                    case MigrateActionTask.PARSER:
                        ResolveParserVO parser = actionInvocVO.getParser();
                        if(parser != null) {
                            parser.setSys_id(null);
                            if(MigrateActionTask.parserNames.contains(taskFullName))
                                newName = taskFullName + System.nanoTime();
                            parser.setUName(newName);
                            Log.log.debug("new parser name = " + newName);
                            
                            ResolveParser model = new ResolveParser();
                            model.applyVOToModel(parser);
                            HibernateUtil.getDAOFactory().getResolveParserDAO().persist(model);
                            parser = model.doGetVO();
                            Log.log.debug("new parser id = " + parser.getSys_id());
                            actionInvocVO.setParser(parser);
                        }
                        
                        break;
                        
                    case MigrateActionTask.PREPROCESS:
                        ResolvePreprocessVO preprocess = actionInvocVO.getPreprocess();
                        if(preprocess != null) {
                            preprocess.setSys_id(null);
                            if(MigrateActionTask.preprocessNames.contains(taskFullName))
                                newName = taskFullName + System.nanoTime();
                            preprocess.setUName(newName);
                            Log.log.debug("new preprocess name = " + newName);
                            
                            ResolvePreprocess model = new ResolvePreprocess();
                            model.applyVOToModel(preprocess);
                            HibernateUtil.getDAOFactory().getResolvePreprocessDAO().persist(model);
                            preprocess = model.doGetVO();
                            Log.log.debug("new preprocess id = " + preprocess.getSys_id());
                            actionInvocVO.setPreprocess(preprocess);
                        }
                        
                        break;
                        
                    default:
                        Log.log.debug("Unknown component type: " + type);
                        break;
                }
            
                ResolveActionInvoc actionInvoc = new ResolveActionInvoc(actionInvocVO);
                HibernateUtil.getDAOFactory().getResolveActionInvocDAO().update(actionInvoc);
                
            	});
                
                completed = true;
            }
            catch(Exception e)
            {
                e.printStackTrace();
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return completed;
    }
    
    public static String getContentAutoGenCode(String connectionType, String username) throws Exception
    {
    	String result = null;
    	String taskFullname = null;
    	
    	switch(connectionType.toLowerCase())
    	{
    		case "ssh" :
    		{
    			taskFullname = "sshTemplate#resolve";
    			break;
    		}
    		case "telnet" :
    		{
    			taskFullname = "telnetTemplate#resolve";
    			break;
    		}
    		case "http" :
    		{
    			taskFullname = "httpTemplate#resolve";
    			break;
    		}
    	}
    	
    	ResolveActionTaskVO taskVO = getActionTaskWithReferences(null, taskFullname, username);
    	if (taskVO == null)
    	{
    		throw new Exception("ActionTask with the name " + taskFullname + " could not be found. Please import automationbuilder package.");
    	}
    	
    	Collection<ResolveActionInvocOptionsVO> invocOptions = taskVO.getResolveActionInvoc().getResolveActionInvocOptions();
    	if (invocOptions != null)
    	{
    		for (ResolveActionInvocOptionsVO invocOption : invocOptions)
    		{
	    		if (invocOption != null && invocOption.getUName().equals("INPUTFILE"))
	    		{
	    			result = invocOption.getUValue();
	    			break;
	    		}
    		}
    	}
    	
    	return result;
    }
    
    public static Set<String> getWikiNamesReferringActionTask(String actionTaskFullName) throws Exception
    {
        QueryDTO query = new QueryDTO();
        Set<String> wikiFullNameSet = new HashSet<String>();
        
        query.setModelName("MainWikidocActiontaskRel");
        query.setWhereClause("UActiontaskFullname = '" + actionTaskFullName + "'");
        
        List <? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, -1, false);
        if (data != null && !data.isEmpty())
        {
            for (Object o : data)
            {
                wikiFullNameSet.add(((MainWikidocActiontaskRel)o).getMainWikidoc().getUFullname());
            }
        }
        
        query.setModelName("ContentWikidocActiontaskRel");
        query.setWhereClause("UActiontaskFullname = '" + actionTaskFullName + "'");
        
        data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, -1, false);
        
        if (data != null && !data.isEmpty())
        {
            for (Object o : data)
            {
                wikiFullNameSet.add(((ContentWikidocActiontaskRel)o).getContentWikidoc().getUFullname());
            }
        }
        
        
        query.setModelName("ExceptionWikidocActiontaskRel");
        query.setWhereClause("UActiontaskFullname = '" + actionTaskFullName + "'");
        
        data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, -1, false);
        if (data != null && !data.isEmpty())
        {
            for (Object o : data)
            {
                wikiFullNameSet.add(((ExceptionWikidocActiontaskRel)o).getExceptionWikidoc().getUFullname());
            }
        }
        
        String taskId = getActionIdFromFullname(actionTaskFullName, "admin");
        
        query.setModelName("ResolveCompRel");
        query.setWhereClause("childId = '" + taskId + "' and childType = 'Task'");
        data = GeneralHibernateUtil.executeHQLSelectModel(query, 0, -1, false);
        
        if (data != null)
        {
        	for (Object obj : data)
        	{
        		ResolveCompRel resolveCompRel = (ResolveCompRel) obj;
        		String wikiId = resolveCompRel.getParentId();
        		wikiFullNameSet.add(WikiUtils.getWikidocFullName(wikiId));
        	}
        }
        
        return wikiFullNameSet;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<String> getTaskMockList(String taskName, String username)
    {
    	List<String> mockList = new ArrayList<String>();
    	
        String sql = "from ResolveActionTask where LOWER(UFullName) = :fullName" ;
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
        		Query query = HibernateUtil.createQuery(sql);
    	        query.setParameter("fullName", taskName.toLowerCase());
    	        
    	        List<ResolveActionTask> list = query.list();
    	        
    	        if (list != null && list.size() > 0)
    	        {
    	        	ResolveActionTask task = list.get(0);
    	        	Collection<ResolveActionTaskMockData> mockCollection = task.getResolveActionInvoc().getMockDataCollection();
    	        	
    	        	if (mockCollection != null)
    	        	{
    	        		for (ResolveActionTaskMockData mockData : mockCollection)
    	        		{
    	        			mockList.add(mockData.getUName());
    	        		}
    	        	}
    	        }
    	        
        	});
        }
        catch(Exception e)
        {
        	Log.log.error("Error: Could not read mock for task: " + taskName, e);
                   HibernateUtil.rethrowNestedTransaction(e);
        }
    	
    	return mockList;
    }
    
    public static AccessRightsVO getDefaultRoles() throws Exception
    {
    	AccessRightsVO accessRight = new AccessRightsVO();
    	List<PropertiesVO> lProps = PropertiesUtil.getPropertiesLikeName(ConstantValues.PROP_ACTIONTASK_RIGHTS);
    	
    	for (PropertiesVO property : lProps)
    	{
    		String name = StringUtils.isBlank(property.getUName()) ? null : property.getUName().toLowerCase().replaceAll("\\s+", "");
    		String value = StringUtils.isBlank(property.getUValue()) ? null : property.getUValue().toLowerCase().replaceAll("\\s+", "");
    		
    		if (StringUtils.isNotBlank(name) && StringUtils.isNotBlank(value))
    		{
	    		if (name.endsWith("admin.all"))
	    		{
	    			accessRight.setUAdminAccess(value);
	    		}
	    		else if (name.endsWith("edit.all"))
	    		{
	    			accessRight.setUWriteAccess(value);
	    		}
	    		else if (name.endsWith("execute.all"))
	    		{
	    			accessRight.setUExecuteAccess(value);
	    		}
	    		else if (name.endsWith("view.all"))
	    		{
	    			accessRight.setUReadAccess(value);
	    		}
    		}
    	}
    	
    	return accessRight;
    }

	private static void restoreEcryptedFieldDefaults(ResolveActionTaskVO entity, String username) {
		if (entity.getResolveActionInvoc() == null
				|| entity.getResolveActionInvoc().getResolveActionParameters() == null) {
			return;
		}

		// Get the existing parameters from DB
		Collection<ResolveActionParameterVO> params = ServiceHibernate.getParamsForActionTaskSysId(entity.getSys_id(),
				username);

		if (params != null) {
			for (ResolveActionParameterVO paramVO : params) {
				// If existing parameter is encrypted restore its default value from DB
				// If incoming parameter is not encrypted, but DB parameter is encrypted, clear it out.
				if (paramVO.getUEncrypt() != null && paramVO.getUEncrypt()) {
					ResolveActionParameterVO inParamVO = getActionParameter(
							entity.getResolveActionInvoc().getResolveActionParameters(), paramVO.getSys_id());
					if (inParamVO != null) {
						if (inParamVO.getUEncrypt() != null && inParamVO.getUEncrypt()) {
							if (ENCRYPTED_FIELD_DEFAULT_VALUE.equalsIgnoreCase(inParamVO.getUDefaultValue())) {
								inParamVO.setUDefaultValue(paramVO.getUDefaultValue());
							}
						} else {
							inParamVO.setUDefaultValue(StringUtils.EMPTY);
						}
					}
				}
			}
		}
	}
	
	private static void setBlankValues(ResolveActionTaskVO entity) {
		if (entity.getResolveActionInvoc() == null
				|| entity.getResolveActionInvoc().getResolveActionParameters() == null) {
			return;
		}
		
		StringBuilder blankValues = new StringBuilder();
		
		//Check Default Parameters
		for (ResolveActionParameterVO param : entity.getResolveActionInvoc().getResolveActionParameters()) {
		    //If they are marked as encrypted, add to BLANKVALUES
		    if (param.getUEncrypt() != null && param.getUEncrypt()) {
		        if (blankValues.length() == 0) {
		            blankValues = blankValues.append("$INPUT{" + param.getUName() + "}"); 
		        } else {
		            blankValues.append(",$INPUT{" + param.getUName() + "}");
		        }
		    } else {
		        //If they contain a PROPERTY reference, check if it is an encrypted one, if so add the INPUT as BLANKVALUE
		        if (StringUtils.isNotBlank(param.getUDefaultValue())) {
			        Matcher matcher = VAR_REGEX_PROPERTY.matcher(param.getUDefaultValue());
		            while (matcher.find()) {
		                String name = matcher.group(1);
		                if (VariableUtil.isPropertyEncrypted(name)) {
					        if (blankValues.length() == 0) {
					            blankValues = blankValues.append("$INPUT{" + param.getUName() + "}"); 
					        } else {
					            blankValues.append(",$INPUT{" + param.getUName() + "}");
					        }
					        break;
		                }
		            }
		        }
		    }
		}
		
		//find existing blankvalues, remove if blank, set if value
		Collection<ResolveActionInvocOptionsVO> options = entity.getResolveActionInvoc().getResolveActionInvocOptions();
		ResolveActionInvocOptionsVO blankValuesOption = null;
		for (ResolveActionInvocOptionsVO option : options) {
		    if (option.getUName().equals("BLANKVALUES")) {
		        if (blankValues.length() == 0) {
		            options.remove(option);
		        } else {
			        blankValuesOption = option;
		        }
		        break;
		    }
		}
		if (blankValues.length() != 0)
		{
			if (blankValuesOption == null) {
			    blankValuesOption = new ResolveActionInvocOptionsVO();
			    blankValuesOption.setUName("BLANKVALUES");
			    options.add(blankValuesOption);
			}
			blankValuesOption.setUValue(blankValues.toString());
		}
	}

	private static void clearEcryptedFieldDefaults(ResolveActionTaskVO entity) {
		if (entity.getResolveActionInvoc() != null
				&& entity.getResolveActionInvoc().getResolveActionParameters() != null) {
			for (ResolveActionParameterVO paramVO : entity.getResolveActionInvoc().getResolveActionParameters()) {
				if (paramVO.getUEncrypt() != null && paramVO.getUEncrypt()) {
					paramVO.setUDefaultValue(ENCRYPTED_FIELD_DEFAULT_VALUE);
				}
			}
		}
	}

	private static ResolveActionParameterVO getActionParameter(Collection<ResolveActionParameterVO> params,
			String sysId) {
		for (ResolveActionParameterVO param : params) {
			if (param.getSys_id().equalsIgnoreCase(sysId)) {
				return param;
			}
		}

		return null;
	}
	
	private static final String COLON = ":";
	private static final String NEWLINE = "\n";
	
	private static int calculateTaskChecksum(ResolveActionTaskVO taskVO)
	{
	    int hash = 0;
	    
	    StringBuilder assess = new StringBuilder();
	    if (taskVO.getResolveActionInvoc().getAssess() != null)
	    {
	        ResolveAssessVO assessVO = taskVO.getResolveActionInvoc().getAssess();
	        assess.append(assessVO.getUScript());
	        assess.append(COLON + assessVO.ugetUOnlyCompleted());
	    }
	    
	    String parser = null;
	    if (taskVO.getResolveActionInvoc().getParser() != null)
	        parser = taskVO.getResolveActionInvoc().getParser().getUScript();
	    
	    String preprocess = null;
	    if (taskVO.getResolveActionInvoc().getPreprocess() != null)
	        preprocess = taskVO.getResolveActionInvoc().getPreprocess().getUScript();
	    
	    final String type = taskVO.getResolveActionInvoc().getUType();
	    
	    StringBuilder actionOptions = new StringBuilder();
	    if (taskVO.getResolveActionInvoc().getResolveActionInvocOptions() != null)
	    {
	        taskVO.getResolveActionInvoc().getResolveActionInvocOptions().stream().sorted((a, b) -> a.getUName().compareTo(b.getUName())).forEach(i -> {
	                String name = i.getUName();
	                // ignore INPUTFILE if task type is ASSESS.
                    if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_ASSESS) && !name.equalsIgnoreCase(Constants.ACTION_INVOCATION_OPTIONS_INPUTFILE))
                        actionOptions.append(i.getUName() + COLON + i.getUValue() + NEWLINE);
                    // For all other type of tasks, include everything.
                    else
                        actionOptions.append(i.getUName() + COLON + i.getUValue() + NEWLINE);
	            });
	    }
	    
	    StringBuilder resolveParams = new StringBuilder();
	    if (taskVO.getResolveActionInvoc().getResolveActionParameters() != null)
        {
	        // consider every INPUT or OUTPUT parameter, and if present, the default value for the same.
            taskVO.getResolveActionInvoc().getResolveActionParameters().stream().filter(p -> p.getUName() != null).sorted((a, b) -> a.getUName().compareTo(b.getUName())).forEach(i -> {
                resolveParams.append(i.getUName() + COLON + (StringUtils.isBlank(i.getUDefaultValue()) ? "" : i.getUDefaultValue()) +
                                COLON + BooleanUtils.isTrue(i.getURequired()) + 
                                COLON + BooleanUtils.isTrue(i.getUProtected()) + 
                                COLON + BooleanUtils.isTrue(i.getUEncrypt()) + NEWLINE);
                });
        }
	    String command = null;
	    if (type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_OS) || 
	                    type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_BASH) || 
	                    type.equalsIgnoreCase(Constants.ACTION_INVOCATION_TYPE_CMD))
	    {
	        // command is valid only if the task type is either OS, BASH or CMD.
	        command = taskVO.getResolveActionInvoc().getUCommand();
	    }
	    String taskInfo = type + COLON + taskVO.getUFullName();
	    
	    hash = Objects.hash(assess.toString(), parser, preprocess, actionOptions.toString(), resolveParams.toString(), command, taskInfo);
	    
	    return hash;
	}
	
	private static final String SYSID = "sysId";
	private static final String CHECKSUM = "checksum";
	private static final String UPDATE_CHECKSUM_SQL = "update ResolveActionTask set checksum = :" + CHECKSUM + " where sys_id = :" + SYSID;
	private static final String ERROR_TASK_CHECKSUM_UPDATE = "Error: Could not update checksum for task: %s";
	
	public static void updateTaskChecksum(ResolveActionTaskVO taskVO, String username)
	{
        Integer checksum = calculateTaskChecksum(taskVO);
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
            
            HibernateUtil.createQuery(UPDATE_CHECKSUM_SQL) 
                            .setParameter(SYSID, taskVO.getSys_id())
                            .setParameter(CHECKSUM, checksum)
                            .executeUpdate();
            });
        }
        catch(Exception e)
        {
            Log.log.error(String.format(ERROR_TASK_CHECKSUM_UPDATE, taskVO.getUFullName()), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
	}
	
	/*
     * API to be invoked by UpdateChecksumUtil.
     */
    public static Integer calculateChecksum(ResolveActionTask task)
    {
        return calculateTaskChecksum(task.doGetVO());
    }
	        
	/**
	 * API to be invoked from update/upgrade script only!
	 */
    public static void updateAllTasksForChecksum()
	{
        try {
            UpdateChecksumUtil.updateComponentChecksum("ResolveActionTask", ActionTaskUtil.class);
        }
        catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
	}
	
	public static Integer getTaskChecksum (String taskFullname, String username)
	{
	    Integer checksum = null;
	    ResolveActionTaskVO taskVO = getResolveActionTask(null, taskFullname, username);
	    if (taskVO != null)
	    {
	        checksum = taskVO.getChecksum();
	    }
	    return checksum;
	}
	
	public static void removeCacheById(String id) {
		if(id != null && cacheTaskName.containsKey(id)) {
			String cachedName = cacheTaskName.get(id);
			cacheTaskId.remove(cachedName);
			cacheTaskName.remove(id);
		}
		
	}
	
	public static void removeCacheByName(String name) {
		if(name != null && cacheTaskId.containsKey(name)) {
			String cachedId = cacheTaskId.get(name);
			cacheTaskId.remove(name);
			cacheTaskName.remove(cachedId);
		}
	}
	
	public static ConcurrentHashMap<String, String> getCacheTaskId() {
		return cacheTaskId;
	}

	public static void setCacheTaskId(ConcurrentHashMap<String, String> cacheTaskId) {
		ActionTaskUtil.cacheTaskId = cacheTaskId;
	}

	public static ConcurrentHashMap<String, String> getCacheTaskName() {
		return cacheTaskName;
	}

	public static void setCacheTaskName(ConcurrentHashMap<String, String> cacheTaskName) {
		ActionTaskUtil.cacheTaskName = cacheTaskName;
	}
	
} // ActionTaskUtil
