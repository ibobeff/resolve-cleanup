package com.resolve.services.rr.util;

import java.util.List;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import com.resolve.services.ServiceResolutionRouting;
import com.resolve.services.hibernate.vo.RRRuleVO;
import com.resolve.services.hibernate.vo.RRSchemaVO;
import com.resolve.util.Log;

public class IdCursor extends Cursor
{
    private List<String> ids;
    private String username;
    private int pos;
    public IdCursor(List<String> ids, String username) {
        this.ids = ids;
        this.username = username;
    }
    public RRRuleVO next() throws ReadRuleException
    {
        if(this.pos<ids.size()) {
            RRRuleVO rule;
            try
            {
                rule = ServiceResolutionRouting.getRule(ids.get(this.pos), username);
            }
            catch (Exception e)
            {
                Log.log.error(e);
                throw new ReadRuleException(ids.get(this.pos),e);
            }
            this.pos++;
            return rule;    
        }
        //just in case...theoretically it shoule never reach here
        return null;
    }
    public boolean hasNext() {
        return this.pos<ids.size();
    }
   
    @Override
    public List<String> appendRuleHeaders() throws Exception
    {
        List<String> headers = this.getDefaultHeader();
        List<String> ids = ResolutionRoutingUtil.listSchemaIdsByRuleId(this.ids, username);
        List<RRSchemaVO> list = ResolutionRoutingUtil.listSchemasByIds(ids, username);
        for(RRSchemaVO schema:list) {
            JsonNode jsonFields = new ObjectMapper().readTree(schema.getJsonFields());
            for(JsonNode field:jsonFields) {
                if(headers.indexOf(field.get("name").asText())!=-1)
                    continue;
                headers.add(field.get("name").asText());
            }
        }
        return headers;
    }
    @Override
    public Double getPercentage()
    {
        return this.pos*1.0/this.ids.size();
    }
}
