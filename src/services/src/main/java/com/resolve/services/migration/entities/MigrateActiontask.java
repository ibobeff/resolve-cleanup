/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.migration.entities;

import java.util.ArrayList;
import java.util.Collection;

import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.SocialUtil;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;

public class MigrateActiontask extends Entities
{
    private String sysId;

    public MigrateActiontask(String sysId)
    {
        this.sysId = sysId;
    }

    private ResolveActionTask migrateActionTask() throws Exception
    {
        ResolveActionTask at = findActiontaskById(sysId);
        
        if (at != null)
        {
            //                Collection<ResolveNodePropertiesVO> props = new HashSet<ResolveNodePropertiesVO>();
            //                props.add(createProp(Document.IS_DT, at.ugetUIsRoot() + ""));
            //                props.add(createProp(Document.IS_RUNBOOK, at.ugetUHasActiveModel() + ""));

            ResolveNodeVO node = new ResolveNodeVO();
            
            copyBaseModelToResolveNode(node, at);
            
            node.setUCompName(at.getUFullName());
            node.setUCompSysId(at.getSys_id());
            node.setUType(NodeType.ACTIONTASK);
            node.setUMarkDeleted(false);
            node.setULock(false);
            node.setUPinned(false);
            //                node.setProperties(props);

            //access rights
            AccessRights ar = at.getAccessRights();
            if(ar != null)
            {
                node.setUReadRoles(SocialUtil.convertRolesToSocialRolesString(ar.getUReadAccess() + "," + ar.getUAdminAccess()));
                node.setUEditRoles(SocialUtil.convertRolesToSocialRolesString(ar.getUWriteAccess() + "," + ar.getUAdminAccess()));
                node.setUPostRoles(SocialUtil.convertRolesToSocialRolesString(ar.getUReadAccess() + "," + ar.getUAdminAccess()));
            }
            
            //persist
            persistNode(node);
            Log.log.trace(Thread.currentThread().getName() + " : Migrated Actiontask:" + at.getUFullName());
        }
        
        return at;
    }
    
//    @Override
    public void run()
    {
        //there is no need to use the save api for the doc as its just fixing the wiki content.
        ResolveActionTask at = null;
        try
        {
            migrateActionTask();
        }
        catch (Exception e)
        {
            Log.log.error("Error saving the AT during the migration script. Please ignore this error as this will not affect the migration process." + 
                          (at != null ? " The AT is " + at.getUFullName() : ""), e);
        }
    }

    private ResolveActionTask findActiontaskById(String sysId) throws Exception
    {
        ResolveActionTask at = null;
        try
        {
          HibernateProxy.setCurrentUser("system");
            at = (ResolveActionTask) HibernateProxy.execute(() -> {

            	return HibernateUtil.getDAOFactory().getResolveActionTaskDAO().findById(sysId);

            });
        }
        catch (Exception e)
        {
            Log.log.error("Error getting AT with sysId: " + sysId, e);
            throw e;
        }

        return at;
    }
    
    @Override
    public Collection<String> call() throws Exception
    {
        ResolveActionTask at = null;
        Collection<String> pCompSysIds = new ArrayList<String>();
        
        try
        {
            at = migrateActionTask();
            
            if(at != null)
            {
                pCompSysIds.add(at.getSys_id());
            }
        }
        catch(Exception e)
        {
            Log.log.error("Error saving the AT during the migration script. Please ignore this error as this will not affect the migration process." + 
                          (at != null ? " The AT is " + at.getUFullName() : ""), e);
            throw e;
        }
        
        return pCompSysIds;
    }
}
