/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.exception;

/**
 * Error codes for WIKI
 * 
 * @author jeet.marwah
 *
 */
public interface WikiExceptionCodes
{
	// Module list
	public static final int MODULE_WIKI = 0;
	public static final int MODULE_WIKI_CONFIG = 1;
	public static final int MODULE_WIKI_DOC = 2;
	public static final int MODULE_WIKI_STORE = 3;
	public static final int MODULE_WIKI_RENDERING = 4;
	public static final int MODULE_WIKI_PLUGINS = 5;
	public static final int MODULE_WIKI_PERLPLUGINS = 6;
	public static final int MODULE_WIKI_CLASSES = 7;
	public static final int MODULE_WIKI_USER = 8;
	public static final int MODULE_WIKI_ACCESS = 9;
	public static final int MODULE_WIKI_EMAIL = 10;
	public static final int MODULE_WIKI_APP = 11;
	public static final int MODULE_WIKI_EXPORT = 12;
	public static final int MODULE_WIKI_DIFF = 13;
	public static final int MODULE_WIKI_GROOVY = 14;
	public static final int MODULE_WIKI_NOTIFICATION = 15;
	public static final int MODULE_WIKI_CACHE = 16;

	public static final int MODULE_PLUGIN_LASZLO = 21;

	// Error list
	public static final int ERROR_WIKI_UNKNOWN = 0;
	public static final int ERROR_WIKI_NOT_IMPLEMENTED = 1;
	public static final int ERROR_WIKI_DOES_NOT_EXIST = 2;
	public static final int ERROR_WIKI_INIT_FAILED = 3;
	public static final int ERROR_WIKI_MKDIR = 4;

	// Config
	public static final int ERROR_WIKI_CONFIG_FILENOTFOUND = 1001;
	public static final int ERROR_WIKI_CONFIG_FORMATERROR = 1002;

	// Doc
	public static final int ERROR_WIKI_DOC_EXPORT = 2001;
	public static final int ERROR_DOC_XML_PARSING = 2002;
	public static final int ERROR_DOC_RCS_PARSING = 2003;

	// Store
	public static final int ERROR_WIKI_STORE_CLASSINVOCATIONERROR = 3001;
	public static final int ERROR_WIKI_STORE_FILENOTFOUND = 3002;
	public static final int ERROR_WIKI_STORE_ARCHIVEFORMAT = 3003;
	public static final int ERROR_WIKI_STORE_ATTACHMENT_ARCHIVEFORMAT = 3004;

	public static final int ERROR_WIKI_STORE_RCS_SAVING_FILE = 3101;
	public static final int ERROR_WIKI_STORE_RCS_READING_FILE = 3102;
	public static final int ERROR_WIKI_STORE_RCS_DELETING_FILE = 3103;
	public static final int ERROR_WIKI_STORE_RCS_READING_REVISIONS = 3103;
	public static final int ERROR_WIKI_STORE_RCS_READING_VERSION = 3104;
	public static final int ERROR_WIKI_STORE_RCS_SEARCH = 3111;
	public static final int ERROR_WIKI_STORE_RCS_LOADING_ATTACHMENT = 3221;
	public static final int ERROR_WIKI_STORE_RCS_SAVING_ATTACHMENT = 3222;
	public static final int ERROR_WIKI_STORE_RCS_SEARCHING_ATTACHMENT = 3223;
	public static final int ERROR_WIKI_STORE_RCS_DELETING_ATTACHMENT = 3224;

	public static final int ERROR_WIKI_STORE_HIBERNATE_SAVING_DOC = 3201;
	public static final int ERROR_WIKI_STORE_HIBERNATE_READING_DOC = 3202;
	public static final int ERROR_WIKI_STORE_HIBERNATE_DELETING_DOC = 3203;
	public static final int ERROR_WIKI_STORE_HIBERNATE_CANNOT_DELETE_UNLOADED_DOC = 3204;
	public static final int ERROR_WIKI_STORE_HIBERNATE_READING_REVISIONS = 3203;
	public static final int ERROR_WIKI_STORE_HIBERNATE_READING_VERSION = 3204;
	public static final int ERROR_WIKI_STORE_HIBERNATE_UNEXISTANT_VERSION = 3205;
	public static final int ERROR_WIKI_STORE_HIBERNATE_SAVING_OBJECT = 3211;
	public static final int ERROR_WIKI_STORE_HIBERNATE_LOADING_OBJECT = 3212;
	public static final int ERROR_WIKI_STORE_HIBERNATE_DELETING_OBJECT = 3213;
	public static final int ERROR_WIKI_STORE_HIBERNATE_SAVING_CLASS = 3221;
	public static final int ERROR_WIKI_STORE_HIBERNATE_LOADING_CLASS = 3222;
	public static final int ERROR_WIKI_STORE_HIBERNATE_SEARCH = 3223;
	public static final int ERROR_WIKI_STORE_HIBERNATE_LOADING_ATTACHMENT = 3231;
	public static final int ERROR_WIKI_STORE_HIBERNATE_SAVING_ATTACHMENT = 3232;
	public static final int ERROR_WIKI_STORE_HIBERNATE_DELETING_ATTACHMENT = 3233;
	public static final int ERROR_WIKI_STORE_HIBERNATE_SAVING_ATTACHMENT_LIST = 3234;
	public static final int ERROR_WIKI_STORE_HIBERNATE_SEARCHING_ATTACHMENT = 3235;
	public static final int ERROR_WIKI_STORE_HIBERNATE_CHECK_EXISTS_DOC = 3236;
	public static final int ERROR_WIKI_STORE_HIBERNATE_SWITCH_DATABASE = 3301;
	public static final int ERROR_WIKI_STORE_HIBERNATE_CREATE_DATABASE = 3401;

	public static final int ERROR_WIKI_RENDERING_VELOCITY_EXCEPTION = 4001;
	public static final int ERROR_WIKI_RENDERING_GROOVY_EXCEPTION = 4002;

	public static final int ERROR_WIKI_PERLPLUGIN_START_EXCEPTION = 6001;
	public static final int ERROR_WIKI_PERLPLUGIN_START = 6002;
	public static final int ERROR_WIKI_PERLPLUGIN_PERLSERVER_EXCEPTION = 6003;

	public static final int ERROR_WIKI_CLASSES_FIELD_DOES_NOT_EXIST = 7001;
	public static final int ERROR_WIKI_CLASSES_FIELD_INVALID = 7002;
	public static final int ERROR_WIKI_CLASSES_DIFF = 7003;
	public static final int ERROR_WIKI_CLASSES_CUSTOMCLASSINVOCATIONERROR = 7004;
	public static final int ERROR_WIKI_CLASSES_PROPERTY_CLASS_INSTANCIATION = 7005;
	public static final int ERROR_WIKI_CLASSES_PROPERTY_CLASS_IN_METACLASS = 7006;

	public static final int ERROR_WIKI_USER_INIT = 8001;
	public static final int ERROR_WIKI_USER_CREATE = 8002;
	public static final int ERROR_WIKI_USER_INACTIVE = 8003;

	public static final int ERROR_WIKI_ACCESS_DENIED = 9001;
	public static final int ERROR_WIKI_ACCESS_TOKEN_INVALID = 9002;
	public static final int ERROR_WIKI_ACCESS_EXO_EXCEPTION_USERS = 9003;
	public static final int ERROR_WIKI_ACCESS_EXO_EXCEPTION_GROUPS = 9004;
	public static final int ERROR_WIKI_ACCESS_EXO_EXCEPTION_ADDING_USERS = 9005;
	public static final int ERROR_WIKI_ACCESS_EXO_EXCEPTION_LISTING_USERS = 9006;

	public static final int ERROR_WIKI_EMAIL_CANNOT_GET_VALIDATION_CONFIG = 10001;
	public static final int ERROR_WIKI_EMAIL_CANNOT_PREPARE_VALIDATION_EMAIL = 10002;
	public static final int ERROR_WIKI_EMAIL_ERROR_SENDING_EMAIL = 10003;
	public static final int ERROR_WIKI_EMAIL_CONNECT_FAILED = 10004;
	public static final int ERROR_WIKI_EMAIL_LOGIN_FAILED = 10005;
	public static final int ERROR_WIKI_EMAIL_SEND_FAILED = 10006;

	public static final int ERROR_WIKI_APP_TEMPLATE_DOES_NOT_EXIST = 11001;
	public static final int ERROR_WIKI_APP_DOCUMENT_NOT_EMPTY = 11002;
	public static final int ERROR_WIKI_APP_ATTACHMENT_NOT_FOUND = 11003;
	public static final int ERROR_WIKI_APP_CREATE_USER = 11004;
	public static final int ERROR_WIKI_APP_VALIDATE_USER = 11005;
	public static final int ERROR_WIKI_APP_INVALID_CHARS = 11006;
	public static final int ERROR_WIKI_APP_URL_EXCEPTION = 11007;
	public static final int ERROR_WIKI_APP_UPLOAD_PARSE_EXCEPTION = 11008;
	public static final int ERROR_WIKI_APP_UPLOAD_FILE_EXCEPTION = 11009;
	public static final int ERROR_WIKI_APP_REDIRECT_EXCEPTION = 11010;
	public static final int ERROR_WIKI_APP_SEND_RESPONSE_EXCEPTION = 11011;
	public static final int ERROR_WIKI_APP_SERVICE_NOT_FOUND = 11012;
	public static final int ERROR_WIKI_APP_FILE_EXCEPTION_MAXSIZE = 11013;
	public static final int ERROR_WIKI_APP_JAVA_HEAP_SPACE = 11014;

	public static final int ERROR_WIKI_EXPORT_XSL_FILE_NOT_FOUND = 12001;
	public static final int ERROR_WIKI_EXPORT_PDF_FOP_FAILED = 12002;
	public static final int ERROR_WIKI_EXPORT_XSL_FAILED = 12003;
	public static final int ERROR_WIKI_DIFF_CONTENT_ERROR = 13001;
	public static final int ERROR_WIKI_DIFF_RENDERED_ERROR = 13002;
	public static final int ERROR_WIKI_DIFF_METADATA_ERROR = 13003;
	public static final int ERROR_WIKI_DIFF_CLASS_ERROR = 13004;
	public static final int ERROR_WIKI_DIFF_OBJECT_ERROR = 13005;
	public static final int ERROR_WIKI_DIFF_XML_ERROR = 13005;
	public static final int ERROR_WIKI_STORE_HIBERNATE_SAVING_LOCK = 13006;
	public static final int ERROR_WIKI_STORE_HIBERNATE_LOADING_LOCK = 13007;
	public static final int ERROR_WIKI_STORE_HIBERNATE_DELETING_LOCK = 13008;
	public static final int ERROR_WIKI_STORE_HIBERNATE_INVALID_MAPPING = 13009;
	public static final int ERROR_WIKI_STORE_HIBERNATE_MAPPING_INJECTION_FAILED = 13010;
	public static final int ERROR_WIKI_STORE_HIBERNATE_LOADING_LINKS = 13011;
	public static final int ERROR_WIKI_STORE_HIBERNATE_SAVING_LINKS = 13012;
	public static final int ERROR_WIKI_STORE_HIBERNATE_DELETING_LINKS = 13013;
	public static final int ERROR_WIKI_STORE_HIBERNATE_LOADING_BACKLINKS = 13014;

	public static final int ERROR_WIKI_GROOVY_COMPILE_FAILED = 14001;
	public static final int ERROR_WIKI_GROOVY_EXECUTION_FAILED = 14002;

	public static final int ERROR_WIKI_NOTIFICATION = 15001;

	public static final int ERROR_CACHE_INITIALIZING = 16001;

	public static final int ERROR_LASZLO_INVALID_XML = 21001;
	public static final int ERROR_LASZLO_INVALID_DOTDOT = 21002;
}
