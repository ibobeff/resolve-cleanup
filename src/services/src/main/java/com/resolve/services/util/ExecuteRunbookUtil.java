/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.resolve.persistence.util.DBCacheRegionConstants;
import com.resolve.persistence.util.DataAPI;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWiki;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.util.Constants;
import com.resolve.util.ExecutableObject;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExecuteRunbookUtil
{
    public static String initExecuteProcess(Map<String, Object> params, String problemid, String processid, String userid, String reference, String alertid, String queryString) throws Exception
    {
        return WorksheetUtil.initExecuteProcess(params, problemid, processid, userid, reference, alertid, queryString);
    } // initExecuteProcess

    static String getWikiDocumentModelXML(String wiki, String modelType)
    {
        String result = null;
        WikiDocumentVO doc = findWikiDocumentByName(wiki);
        if (doc != null)
        {
            if (modelType.equals(Constants.MODEL_TYPE_MAIN))
            {
                result = doc.getUModelProcess();
            }
            else if (modelType.equals(Constants.MODEL_TYPE_ABORT))
            {
                result = doc.getUModelException();
            }
        }
        else
        {
            throw new RuntimeException("Missing WIKI document: " + wiki);
        }
        return result;
    } // getWikiDocumentModelXML

    static WikiDocumentVO findWikiDocumentByName(String name)
    {
        if (StringUtils.isBlank(name))
        {
            throw new RuntimeException("Missing wiki name: " + name);
        }

        Object obj = ServiceHibernate.getCachedWikiDocumentForRSControl(DBCacheRegionConstants.WIKI_DOCUMENT_CACHE_REGION, 
        																name);
        if (obj != null)
        {
            return (WikiDocumentVO) obj;
        }

        WikiDocumentVO doc = null;
        try
        {
            doc = ServiceWiki.getWikiDoc(null, name, "system");
            if (doc != null)
            {
                ServiceHibernate.cacheWikiDocumentForRSControl(name);
            }
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }
        return doc;
    }

    public static Map<String,String> actionTaskLock(Map<String,String> params)
    {        
        String lockName = params.get(Constants.ACTIONTASK_LOCK_NAME);
        if (StringUtils.isNotEmpty(lockName))
        {
            return taskNamedLock(params);
        }
        
        final Map<String,String> result = new HashMap<String,String>();
        final String wiki = params.get(Constants.WIKI);
        final String wikiCounter = wiki + Constants.ACTIONTASK_LOCK_TIMEOUT;
        final String processid = params.get(Constants.EXECUTE_PROCESSID);

        result.put(Constants.ACTIONTASK_LOCK_CONDITION, Constants.ASSESS_CONDITION_GOOD);
        result.put(Constants.ACTIONTASK_LOCK_SEVERITY, Constants.ASSESS_SEVERITY_GOOD);
        result.put(Constants.ACTIONTASK_LOCK_SUMMARY, "");
        result.put(Constants.ACTIONTASK_LOCK_DETAIL, "");

        int sleepTime = 10; // in secs
        int maxRetry = 18;
        
        ExecutableObject getRBLockCount = new ExecutableObject() {
            public Object execute()
            {
                Integer lockNum = com.resolve.persistence.util.HibernateUtil.getJavanNumberOnly(wikiCounter);
                return lockNum;
            }
        };
        
        boolean gotLock = false;
        Integer lockNum = (Integer)HibernateUtil.exclusiveClusterExecution(wiki, getRBLockCount);
        
        String info = "actionTaskLock: wiki= " + wiki + ", lock count= " + lockNum + ", processid= " + processid;
        Log.log.info(info);
        
        if (lockNum==null || lockNum==0)
        {
            ExecutableObject initRBLockCount = new ExecutableObject() {
                public Object execute()
                {
                    boolean hasLock = false;
                    Integer cur = HibernateUtil.getJavanNumberOnly(wikiCounter);
                    if (cur!=null && cur>0)
                    {
                        hasLock = false;
                    }
                    else
                    {
                        cur = 1;
                        HibernateUtil.saveJavaObject(wikiCounter, processid, cur);
                        //Integer lockNum = com.resolve.persistence.util.HibernateUtil.getJavanNumberOnly(wikiCounter);
                        hasLock = true;
                    }
                    return hasLock;
                }
            };
            gotLock= (Boolean)HibernateUtil.exclusiveClusterExecution(wiki, initRBLockCount);
            if (gotLock)
            {
                // We just got the lock, set up return value
                result.put(Constants.ACTIONTASK_LOCK_CONDITION, Constants.ASSESS_CONDITION_GOOD);
                result.put(Constants.ACTIONTASK_LOCK_SEVERITY, Constants.ASSESS_SEVERITY_GOOD);
                result.put(Constants.ACTIONTASK_LOCK_SUMMARY, "Lock successfully acquired.");
                result.put(Constants.ACTIONTASK_LOCK_DETAIL, "Acquired runbook lock.");
                return result;
            }
            else
            {
            	//make sure following loop does not throw Null Pointer Exception
            	lockNum = 1;
            }
        }

        ExecutableObject updateRBLockCount = new ExecutableObject() {
            public Object execute()
            {
                Map<String,Object> obj = HibernateUtil.getJavaObject(wikiCounter);
                String pid = (String)obj.get("OBJECT");
                Integer cur = (Integer)obj.get("NUMBER");
                if (cur<=0)
                {
                    HibernateUtil.saveJavaObject(wikiCounter, processid, 1);  // we grabs the lock
                    cur=0;
                }
                else
                {
                    if (pid.equalsIgnoreCase(processid))
                    {
                        // we own the lock, do nothing and return
                        
                    }
                    
                    
                    Map<String, Object> rows = null;
                    try
                    {
                        rows = DataAPI.findProcessRequestByIdAndStatus(pid,new ArrayList<String>());
                    }
                    catch (Exception ex)
                    {
                        Log.log.error(ex,ex);
                    }
                    if(rows==null || rows.isEmpty())
                    {
                    	String detail = "processrequest does not exist";
                    	detail += "\nFreeing lock and attempting to reacquire."; 
                    	
                        HibernateUtil.saveJavaObject(wikiCounter, processid, 1); //reset 
                        detail += "\nLock has been reacquired.";
                        result.put(Constants.ACTIONTASK_LOCK_DETAIL, detail);
                        cur = 0;
                    }
                    if (rows!=null&&!rows.isEmpty()&&(StringUtils.equalsIgnoreCase((String)rows.get("u_status"),"COMPLETED")||StringUtils.equalsIgnoreCase((String)rows.get("u_status"), "ABORTED")))
                    {
                        String detail = result.get(Constants.ACTIONTASK_LOCK_DETAIL) + "aborted or completed.";
                        detail += "\nStatus: " + rows.get("u_status");
                        detail += "\nFreeing lock and attempting to reacquire.";
                         
                        HibernateUtil.saveJavaObject(wikiCounter, processid, 1); //reset 
                        detail += "\nLock has been reacquired.";
                        result.put(Constants.ACTIONTASK_LOCK_DETAIL, detail);
                        cur = 0;
                    }
                }
                return cur;
            }
        };

        while (lockNum>0 && maxRetry>0)
        {
            try
            {
                Thread.sleep(sleepTime*1000);
                maxRetry--;
                lockNum = (Integer)HibernateUtil.exclusiveClusterExecution(wiki, updateRBLockCount);
            }
            catch (Exception ex)
            {
                //ignore
                Log.log.error(ex,ex);
            }
        }

        if (lockNum<=0)
        {
            //we got the lock from looping
            result.put(Constants.ACTIONTASK_LOCK_CONDITION, Constants.ASSESS_CONDITION_GOOD);
            result.put(Constants.ACTIONTASK_LOCK_SEVERITY, Constants.ASSESS_SEVERITY_GOOD);
            result.put(Constants.ACTIONTASK_LOCK_SUMMARY, "Lock successfully acquired.");
            result.put(Constants.ACTIONTASK_LOCK_DETAIL, "Acquired runbook lock.");
        }
        else
        {
            // failed to get the lock eventually
            result.put(Constants.ACTIONTASK_LOCK_CONDITION, Constants.ASSESS_CONDITION_BAD);
            result.put(Constants.ACTIONTASK_LOCK_SEVERITY, Constants.ASSESS_SEVERITY_WARNING);
            result.put(Constants.ACTIONTASK_LOCK_SUMMARY, "Lock timed out. Proceeding anyway.");
            result.put(Constants.ACTIONTASK_LOCK_DETAIL, "WARNING - Unable to acquire runbook lock.");
        }
        
        
        return result;
    }
    

    public static Map<String,String> actionTaskUnlock(Map<String,String> params)
    {
        String lockName = params.get(Constants.ACTIONTASK_LOCK_NAME);
        if (StringUtils.isNotEmpty(lockName))
        {
            return taskNamedUnlock(params);
        }
        
        final Map<String,String> result = new HashMap<String,String>();
        final String wiki = params.get(Constants.WIKI);
        final String wikiCounter = wiki + Constants.ACTIONTASK_LOCK_TIMEOUT;
        final String processid = params.get(Constants.EXECUTE_PROCESSID);

        result.put(Constants.ACTIONTASK_LOCK_CONDITION, Constants.ASSESS_CONDITION_GOOD);
        result.put(Constants.ACTIONTASK_LOCK_SEVERITY, Constants.ASSESS_SEVERITY_GOOD);
        result.put(Constants.ACTIONTASK_LOCK_SUMMARY, "");
        result.put(Constants.ACTIONTASK_LOCK_DETAIL, "");
        
        ExecutableObject unlockRB = new ExecutableObject() {
            public Object execute()
            {
                Map<String,Object> obj = HibernateUtil.getJavaObject(wikiCounter);
                if (obj==null)
                {
                    result.put(Constants.ACTIONTASK_LOCK_DETAIL, "Runbook isn't locked: " + wiki);
                    return result;
                }
                
                String pid = (String)obj.get("OBJECT");
                Integer cur = (Integer)obj.get("NUMBER");
                Log.log.info("Lock information wiki= " + wiki + ", owner processid= " +pid + ", lock count= " + cur + ", processid= " + processid);
                if (pid==null || cur ==null)
                {
                    String info = "Invalid lock, wiki= " + wiki + ", processid= " +pid + ", lock count= " + cur;
                    Log.log.info(info);
                    result.put(Constants.ACTIONTASK_LOCK_DETAIL, "Runbook isn't locked properly: " + wiki + "\n" + info);
                    result.put(Constants.ACTIONTASK_LOCK_SEVERITY, Constants.ASSESS_SEVERITY_WARNING);
                    return result;
                }
                
                if (!pid.equalsIgnoreCase(processid))
                {
                    String info = "Failed to unlock process. Lock is owned by other process: wiki= " + wiki + ", lock owner processid= " +pid + ", lock count= " + cur + ", processid= " + processid;
                    Log.log.info(info);
                    result.put(Constants.ACTIONTASK_LOCK_DETAIL, info);
                    result.put(Constants.ACTIONTASK_LOCK_SEVERITY, Constants.ASSESS_SEVERITY_WARNING);
                    return result;
                }
                
                //We have the lock
                HibernateUtil.saveJavaObject(wikiCounter, processid, 0);
                String info = "Unlocked runbook sucessfully: wiki= " + wiki + ", lock owner processid= " +pid + ", lock count= " + cur + ", processid= " + processid;
                Log.log.info(info);
                result.put(Constants.ACTIONTASK_LOCK_DETAIL, info);
                
                return result;
            }
        };
        
        HibernateUtil.exclusiveClusterExecution(wiki, unlockRB);
        
        return result;
    }

    public static Map<String,String> taskNamedLock(Map<String,String> params)
    {
        final Map<String,String> result = new HashMap<String,String>();
        String lockName = params.get(Constants.ACTIONTASK_LOCK_NAME);
        final String atLockName = StringUtils.isEmpty(lockName) ? params.get(Constants.WIKI) : lockName;
        final String lockCounter = atLockName + Constants.ACTIONTASK_LOCK_TIMEOUT;
        final String processid = params.get(Constants.EXECUTE_PROCESSID);

        if (StringUtils.isEmpty(atLockName))
        {
            throw new RuntimeException("Lock name has empty value in taskNamedLock() call");  
        }

        result.put(Constants.ACTIONTASK_LOCK_CONDITION, Constants.ASSESS_CONDITION_GOOD);
        result.put(Constants.ACTIONTASK_LOCK_SEVERITY, Constants.ASSESS_SEVERITY_GOOD);
        result.put(Constants.ACTIONTASK_LOCK_SUMMARY, "");
        result.put(Constants.ACTIONTASK_LOCK_DETAIL, "");

        int sleepTime = 10; // in secs
        int maxRetry = 18;
        
        ExecutableObject getRBLockCount = new ExecutableObject() {
            public Object execute()
            {
                Integer lockNum = com.resolve.persistence.util.HibernateUtil.getJavanNumberOnly(lockCounter);
                return lockNum;
            }
        };
        
        boolean gotLock = false;
        Integer lockNum = (Integer)HibernateUtil.exclusiveClusterExecution(atLockName, getRBLockCount);
        
        String info = "taskNamedLock: lock name= " + atLockName + ", lock count= " + lockNum + ", processid= " + processid;
        Log.log.info(info);
        
        if (lockNum==null || lockNum==0)
        {
            ExecutableObject initRBLockCount = new ExecutableObject() {
                public Object execute()
                {
                    boolean hasLock = false;
                    Integer cur = HibernateUtil.getJavanNumberOnly(lockCounter);
                    if (cur!=null && cur>0)
                    {
                        hasLock = false;
                    }
                    else
                    {
                        cur = 1;
                        HibernateUtil.saveJavaObject(lockCounter, processid, cur);
                        //Integer lockNum = com.resolve.persistence.util.HibernateUtil.getJavanNumberOnly(wikiCounter);
                        hasLock = true;
                    }
                    return hasLock;
                }
            };
            gotLock= (Boolean)HibernateUtil.exclusiveClusterExecution(atLockName, initRBLockCount);
            if (gotLock)
            {
                // We just got the lock, set up return value
                result.put(Constants.ACTIONTASK_LOCK_CONDITION, Constants.ASSESS_CONDITION_GOOD);
                result.put(Constants.ACTIONTASK_LOCK_SEVERITY, Constants.ASSESS_SEVERITY_GOOD);
                result.put(Constants.ACTIONTASK_LOCK_SUMMARY, "Lock successfully acquired.");
                result.put(Constants.ACTIONTASK_LOCK_DETAIL, "Acquired runbook lock. Lock=" + atLockName + ", processid=" + processid );
                return result;
            }
            else
            {
                //make sure following loop does not throw Null Pointer Exception
                lockNum = 1;
            }
        }

        ExecutableObject updateRBLockCount = new ExecutableObject() {
            public Object execute()
            {
                Map<String,Object> obj = HibernateUtil.getJavaObject(lockCounter);
                String pid = (String)obj.get("OBJECT");
                Integer cur = (Integer)obj.get("NUMBER");
                if (cur<=0)
                {
                    HibernateUtil.saveJavaObject(lockCounter, processid, 1);  // we grabs the lock
                    cur=0;
                    result.put(Constants.ACTIONTASK_LOCK_DETAIL, "Acquired runbook lock. Lock=" + atLockName + ", processid=" + processid);
                }
                else
                {
                    if (pid.equalsIgnoreCase(processid))
                    {
                        // we own the lock, do nothing and return
                        result.put(Constants.ACTIONTASK_LOCK_DETAIL, "Already acquired runbook lock. Lock=" + atLockName + ", processid=" + processid );
                    }
                    
                    Map<String, Object> rows = null;
                    try
                    {
                        rows = DataAPI.findProcessRequestByIdAndStatus(pid, Arrays.asList(new String[]{"COMPLETED", "ABORTED"}));
                    }
                    catch (Exception ex)
                    {
                        Log.log.error(ex,ex);
                    }
                    if (rows!=null&&!rows.isEmpty())
                    {
                        String detail = result.get(Constants.ACTIONTASK_LOCK_DETAIL) + "aborted or completed.";
                        detail += "\nStatus: " + rows.get("u_status");
                        detail += "\nFreeing lock and attempting to reacquire.";
                         
                        HibernateUtil.saveJavaObject(lockCounter, processid, 1); //reset 
                        detail += "\nLock has not been reacquired.";
                        result.put(Constants.ACTIONTASK_LOCK_DETAIL, detail);
                        cur = 0;
                    }
                }
                return cur;
            }
        };

        while (lockNum>0 && maxRetry>0)
        {
            try
            {
                Thread.sleep(sleepTime*1000);
                maxRetry--;
                lockNum = (Integer)HibernateUtil.exclusiveClusterExecution(atLockName, updateRBLockCount);
            }
            catch (Exception ex)
            {
                //ignore
                Log.log.error(ex,ex);
            }
        }

        if (lockNum<=0)
        {
            //we got the lock from looping
            result.put(Constants.ACTIONTASK_LOCK_CONDITION, Constants.ASSESS_CONDITION_GOOD);
            result.put(Constants.ACTIONTASK_LOCK_SEVERITY, Constants.ASSESS_SEVERITY_GOOD);
            result.put(Constants.ACTIONTASK_LOCK_SUMMARY, "Lock successfully acquired.");
            result.put(Constants.ACTIONTASK_LOCK_DETAIL, "Acquired runbook lock. Lock=" + atLockName + ", processid=" + processid);
        }
        else
        {
            // failed to get the lock eventually
            result.put(Constants.ACTIONTASK_LOCK_CONDITION, Constants.ASSESS_CONDITION_BAD);
            result.put(Constants.ACTIONTASK_LOCK_SEVERITY, Constants.ASSESS_SEVERITY_WARNING);
            result.put(Constants.ACTIONTASK_LOCK_SUMMARY, "Lock timed out. Proceeding anyway.");
            result.put(Constants.ACTIONTASK_LOCK_DETAIL, "WARNING - Unable to acquire runbook lock.");
        }
        
        return result;
    }

    public static Map<String,String> taskNamedUnlock(Map<String,String> params)
    {
        final Map<String,String> result = new HashMap<String,String>();
        String lockName = params.get(Constants.ACTIONTASK_LOCK_NAME);
        final String atLockName = StringUtils.isEmpty(lockName) ? params.get(Constants.WIKI) : lockName;
        final String lockCounter = atLockName + Constants.ACTIONTASK_LOCK_TIMEOUT;
        final String processid = params.get(Constants.EXECUTE_PROCESSID);

        if (StringUtils.isEmpty(atLockName))
        {
            throw new RuntimeException("Lock name should not be empty in taskNamedUnLock() call");  
        }
        
        result.put(Constants.ACTIONTASK_LOCK_CONDITION, Constants.ASSESS_CONDITION_GOOD);
        result.put(Constants.ACTIONTASK_LOCK_SEVERITY, Constants.ASSESS_SEVERITY_GOOD);
        result.put(Constants.ACTIONTASK_LOCK_SUMMARY, "Unlock lock " + atLockName);
        result.put(Constants.ACTIONTASK_LOCK_DETAIL, "");
        
        ExecutableObject unlockRB = new ExecutableObject() {
            public Object execute()
            {
                Map<String,Object> obj = HibernateUtil.getJavaObject(lockCounter);
                if (obj==null)
                {
                    result.put(Constants.ACTIONTASK_LOCK_DETAIL, "Process doesn't own lock: " + atLockName);
                    return result;
                }
                
                String pid = (String)obj.get("OBJECT");
                Integer cur = (Integer)obj.get("NUMBER");
                Log.log.info("Lock information: lock name = " + atLockName + ", owner processid= " +pid + ", lock count= " + cur + ", processid= " + processid);
                if (pid==null || cur ==null)
                {
                    String info = "Invalid lock: lock name = " + atLockName + ", processid= " +pid + ", lock count= " + cur;
                    Log.log.info(info);
                    result.put(Constants.ACTIONTASK_LOCK_DETAIL, "Runbook isn't locked properly: " + atLockName + "\n" + info);
                    result.put(Constants.ACTIONTASK_LOCK_SEVERITY, Constants.ASSESS_SEVERITY_WARNING);
                    return result;
                }
                
                if (!pid.equalsIgnoreCase(processid))
                {
                    String info = "Failed to unlock process. Lock is owned by other process: lock name= " + atLockName + ", lock owner processid= " +pid + ", lock count= " + cur + ", processid= " + processid;
                    Log.log.info(info);
                    result.put(Constants.ACTIONTASK_LOCK_DETAIL, info);
                    result.put(Constants.ACTIONTASK_LOCK_SEVERITY, Constants.ASSESS_SEVERITY_WARNING);
                    return result;
                }
                
                //We have the lock
                HibernateUtil.saveJavaObject(lockCounter, processid, 0);
                String info = "Unlocked runbook sucessfully: lock name = " + atLockName + ", lock owner processid= " +pid + ", lock count= " + cur + ", processid= " + processid;
                Log.log.info(info);
                result.put(Constants.ACTIONTASK_LOCK_DETAIL, info);
                
                return result;
            }
        };
        
        HibernateUtil.exclusiveClusterExecution(atLockName, unlockRB);
        
        return result;
    }

    
}
