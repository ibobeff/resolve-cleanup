/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.form;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;

import com.resolve.services.constants.CustomFormUIType;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.util.constants.HibernateConstantsEnum;
import com.resolve.util.StringUtils;

public class RsFormDTO extends RsUIComponent
{

    private String timezone;

    private String formName;
    private String viewName;
    private String windowTitle;
    private Boolean headerVisible;
    private Boolean wizard;

    // wiki on which this form will be displayed
    private String wikiName;

    // if the form is mapped to a table, if not, then it is an INPUT type form
    private String tableName;
    private String tableDisplayName;

    // this is only coming in from the UI..it is not persisted and is used for
    // the sole purpose of updating the meta tables
    private String tableSysId;
    private String referenceTableNames;

    // roles for a form - CSVs
    private String viewRoles;
    private String editRoles;
    private String adminRoles;

    private String username;

    // additional properties of for
    private RsFormProperties properties;

    // either 'layouts' or 'tabs' will be available - NOT sure if we want to
    // support both at the same time. But we can
    private List<RsPanelDTO> panels;
    // private List<RsTabDTO> tabs;

    // control panel - buttons, links, etc
    private RsButtonPanelDTO buttonPanel;

    // system hidden fields - default hidden fields added by the system property
    private List<RsUIField> systemFields;

    // //convenience API to get all the fields of a form
    // public List<RsUIField> getFormFields()
    // {
    // List<RsUIField> allFields = new Array
    // }

    public void validate() throws Exception
    {
        //table name validation
        String tableName = getTableName();
        if(StringUtils.isNotBlank(tableName))
        {
            //validate the name
            boolean valid = tableName.matches(HibernateConstants.REGEX_ALPHANUMERIC_WITH_UNDERSCORE_30CHARS);
            if(!valid)
            {
                throw new Exception("Table Name can have only alphanumerics,'_' and limited to 30 chars");
            }
        }

    }
    
    public RsCustomTableDTO u_getCustomTable()
    {
        RsCustomTableDTO customTable = new RsCustomTableDTO();
        customTable.setSys_id(getTableSysId());
        customTable.setUName(getTableName());
        customTable.setUDisplayName(getTableDisplayName());
        customTable.setUType(HibernateConstantsEnum.NORMAL_CUSTOM_TABLE.getTagName());
        customTable.setUreferenceTableNames(getReferenceTableNames());
        customTable.setUwikiName(getWikiName());
        customTable.setNewTable(StringUtils.isBlank(getTableSysId()) ? true : false);
        
        customTable.setFields(getTableFields());
        
        //populate the form roles - will be used if its a new table
        customTable.setViewRoles(getViewRoles());
        customTable.setEditRoles(getEditRoles());
        customTable.setAdminRoles(getAdminRoles());        
        
        return customTable;
    }
    
    public List<RsUIField> filterINPUTFields()
    {
        List<RsUIField> fields = new ArrayList<RsUIField>();
        
        if (panels != null)
        {
            for (RsPanelDTO panel : panels)
            {
                // just a panel
                if (panel.getColumns() != null)
                {
                    populateINPUTFields(fields, panel.getColumns());
                }
                // has tab panels
                else if (panel.getTabs() != null)
                {
                    for (RsTabDTO tab : panel.getTabs())
                    {
                        if (tab.getColumns() != null)
                        {
                            populateINPUTFields(fields, tab.getColumns());
                        }
                    }
                }
            }// end of for
        }
        
        return fields;
    }
    
    // for custom table - DEPRECATE THIS and REMOVE THE ResolveBaseModel
//    public ResolveBaseModel convertToBaseModel()
//    {
//        ResolveBaseModel bm = new ResolveBaseModel();
//        if (StringUtils.isNotEmpty(getTableSysId()))
//        {
//            bm.set("sys_id", getTableSysId());
//        }
//        bm.set(RSCustomTable.NAME, getTableName());
//        bm.set(RSCustomTable.DISPLAYNAME, getTableDisplayName());
//        bm.set(RSCustomTable.TYPE, WorkflowConstants.NORMAL_CUSTOM_TABLE.getTagName()); // WorkflowConstants.NORMAL_CUSTOM_TABLE
//        bm.set(RSCustomTable.REFERENCE_TABLE_NAMES, getReferenceTableNames());
//
//        bm.set(RSMetaFormView.WIKIPAGENAME, getWikiName());
//
//        bm.set("_Resolve_GDS_client_timezone", getTimezone());// "GMT-07:00"
//        bm.set("username", getUsername());
//        bm.set("isNew", StringUtils.isBlank(getTableSysId()) ? true : false);
//
//        // fields
//        List<ResolveBaseModel> fields = null;
//        if (panels != null)
//        {
//            fields = new ArrayList<ResolveBaseModel>();
//
//            for (RsPanelDTO panel : panels)
//            {
//                // just a panel
//                if (panel.getColumns() != null)
//                {
//                    convertDBFields(fields, panel.getColumns());
//                }
//                // has tab panels
//                else if (panel.getTabs() != null)
//                {
//                    for (RsTabDTO tab : panel.getTabs())
//                    {
//                        if (tab.getColumns() != null)
//                        {
//                            convertDBFields(fields, tab.getColumns());
//                        }
//                    }
//                }
//            }// end of for
//        }
//
//        // check if the sys fields are present, if not add them
//        // boolean hasSysFields = hasSysFields(fields);
//        // if(!hasSysFields)
//        // {
//        // fields.addAll(CustomTableUtil.getSysFields(getTableName(),
//        // getFormAccessRights()));
//        // }
//
//        bm.set("metaFields", fields);// fields
//
//        return bm;
//    }
    
    private List<RsUIField> getTableFields()
    {
        List<RsUIField> fields = null;
        
        if (panels != null)
        {
            fields = new ArrayList<RsUIField>();

            for (RsPanelDTO panel : panels)
            {
                // just a panel
                if (panel.getColumns() != null)
                {
                    populateDBFields(fields, panel.getColumns());
                }
                // has tab panels
                else if (panel.getTabs() != null)
                {
                    for (RsTabDTO tab : panel.getTabs())
                    {
                        if (tab.getColumns() != null)
                        {
                            populateDBFields(fields, tab.getColumns());
                        }
                    }
                }
            }// end of for
        }
        
        return fields;
        
    }
    
    
    private void populateINPUTFields(List<RsUIField> fields, List<RsColumnDTO> columns)
    {
        for (RsColumnDTO column : columns)
        {
            if (column.getFields() != null)
            {
                for (RsUIField field : column.getFields())
                {
                    String type = field.getSourceType();
                    if(type.equalsIgnoreCase(HibernateConstantsEnum.INPUT.name()))
                    {
                        fields.add(field);
                    }
                }//end of for
            }//end of if
        }//end of for loop
    }
    
    private void populateDBFields(List<RsUIField> fields, List<RsColumnDTO> columns)
    {
        
        for (RsColumnDTO column : columns)
        {
            if (column.getFields() != null)
            {
                for (RsUIField field : column.getFields())
                {
                    
                    if (CustomFormUIType.SectionContainerField.name().equalsIgnoreCase(field.getUiType()))
                    {
                        String jsonString = field.getSectionColumns();
                        // parse json to get the columns
                        try
                        {
                            JSONArray cols = StringUtils.stringToJSONArray(jsonString);
                            // iterate through the fields in the column an convert
                            // them to base models and add them to fields list
                            for (int i = 0; i < cols.size(); i++)
                            {
                                JSONObject col = cols.getJSONObject(i);
                                JSONArray columnFields = (JSONArray) col.get(HibernateConstants.META_FIELD_PROP_SECTION_COLUMN_CONTROLS);
                                for (int j = 0; j < columnFields.size(); j++)
                                {
                                    JsonConfig jsonConfig = new JsonConfig();
                                    jsonConfig.setRootClass(RsUIField.class);
                                    RsUIField rf = (RsUIField) JSONSerializer.toJava(columnFields.getJSONObject(j), jsonConfig);
                                    if (rf != null)
                                    {
                                        if (StringUtils.isNotEmpty(rf.getSourceType()) && rf.getSourceType().equalsIgnoreCase(HibernateConstantsEnum.DB.name()))
                                        {
                                            populateAdditionalFieldInfo(rf);

                                            fields.add(rf);
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                    }
                    // add only if this field is mapped to DB table.
                    // if(StringUtils.isNotBlank(field.getDbtable()) &&
                    // StringUtils.isNotBlank(field.getDbcolumn()))
                    if (StringUtils.isNotEmpty(field.getSourceType()) && field.getSourceType().equalsIgnoreCase(HibernateConstantsEnum.DB.name()))
                    {
                        populateAdditionalFieldInfo(field);

                        fields.add(field);
                    }
                }
            }
        }
    }
    
    private void populateAdditionalFieldInfo(RsUIField field)
    {
     // set the same roles as that of for to all the DB type fields
        field.setViewRoles(getViewRoles());
        field.setEditRoles(getEditRoles());
        field.setAdminRoles(getAdminRoles());
        
        //evaluate the DB type
        field.evaluateDbType();
    }
    

//    private void convertDBFields(List<ResolveBaseModel> fields, List<RsColumnDTO> columns)
//    {
//        for (RsColumnDTO column : columns)
//        {
//            if (column.getFields() != null)
//            {
//                for (RsUIField field : column.getFields())
//                {
//                    if (CustomFormUIType.SectionContainerField.name().equalsIgnoreCase(field.getUiType()))
//                    {
//                        String jsonString = field.getSectionColumns();
//                        // parse json to get the columns
//                        JSONArray cols = StringUtils.stringToJSONArray(jsonString);
//                        // iterate through the fields in the column an convert
//                        // them to base models and add them to fields list
//                        for (int i = 0; i < cols.size(); i++)
//                        {
//                            JSONObject col = cols.getJSONObject(i);
//                            JSONArray columnFields = (JSONArray) col.get("controls");
//                            for (int j = 0; j < columnFields.size(); j++)
//                            {
//                                JsonConfig jsonConfig = new JsonConfig();
//                                jsonConfig.setRootClass(RsUIField.class);
//                                RsUIField rf = (RsUIField) JSONSerializer.toJava(columnFields.getJSONObject(j), jsonConfig);
//                                if (rf != null)
//                                {
//                                    if (StringUtils.isNotEmpty(rf.getSourceType()) && rf.getSourceType().equalsIgnoreCase(DataSource.DB.name()))
//                                    {
//                                        // set the same roles as that of for to all the DB type fields
//                                        rf.setViewRoles(getViewRoles());
//                                        rf.setEditRoles(getEditRoles());
//                                        rf.setAdminRoles(getAdminRoles());
//
//                                        fields.add(rf.convertToBaseModel());
//                                    }
//                                }
//                            }
//                        }
//
//                    }
//                    // add only if this field is mapped to DB table.
//                    // if(StringUtils.isNotBlank(field.getDbtable()) &&
//                    // StringUtils.isNotBlank(field.getDbcolumn()))
//                    if (StringUtils.isNotEmpty(field.getSourceType()) && field.getSourceType().equalsIgnoreCase(DataSource.DB.name()))
//                    {
//                        // set the same roles as that of for to all the DB type fields
//                        field.setViewRoles(getViewRoles());
//                        field.setEditRoles(getEditRoles());
//                        field.setAdminRoles(getAdminRoles());
//
//                        fields.add(field.convertToBaseModel());
//                    }
//                }
//            }
//        }
//    }

//    private boolean hasSysFields(List<ResolveBaseModel> fields)
//    {
//        boolean hasSysFields = false;
//
//        // if the table is created, it has sys fields
//        if (StringUtils.isNotBlank(tableSysId))
//        {
//            hasSysFields = true;
//        }
//
//        // for(ResolveBaseModel field : fields)
//        // {
//        // if(((String)field.get(RSMetaField.COLUMN)).startsWith("sys_"))
//        // {
//        // hasSysFields = true;
//        // break;
//        // }
//        // }
//
//        return hasSysFields;
//    }

//    public ResolveBaseModel getFormAccessRights()
//    {
//        ResolveBaseModel rights = new ResolveBaseModel();
//        rights.set(RsUIField.READ_ACCESS_RIGHTS, getViewRoles());
//        rights.set(RsUIField.EDIT_ACCESS_RIGHTS, getEditRoles());
//        rights.set(RsUIField.ADMIN_ACCESS_RIGHTS, getAdminRoles());
//
//        return rights;
//    }

    public String getFormName()
    {
        return formName;
    }

    public void setFormName(String formName)
    {
        this.formName = formName;
    }

    public String getWikiName()
    {
        return wikiName;
    }

    public void setWikiName(String wikiName)
    {
        this.wikiName = wikiName;
    }

    public String getTableName()
    {
        return tableName;
    }

    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }

    public String getTableDisplayName()
    {
        return tableDisplayName;
    }

    public void setTableDisplayName(String tableDisplayName)
    {
        this.tableDisplayName = tableDisplayName;
    }

    public String getTableSysId()
    {
        return tableSysId;
    }

    public void setTableSysId(String tableSysId)
    {
        this.tableSysId = tableSysId;
    }

    public String getViewRoles()
    {
        return viewRoles;
    }

    public void setViewRoles(String viewRoles)
    {
        this.viewRoles = viewRoles;
    }

    public String getEditRoles()
    {
        return editRoles;
    }

    public void setEditRoles(String editRoles)
    {
        this.editRoles = editRoles;
    }

    // public List<RsTabDTO> getTabs()
    // {
    // return tabs;
    // }
    //
    // public void setTabs(List<RsTabDTO> tabs)
    // {
    // this.tabs = tabs;
    // }

    public RsButtonPanelDTO getButtonPanel()
    {
        return buttonPanel;
    }

    public void setButtonPanel(RsButtonPanelDTO buttonPanel)
    {
        this.buttonPanel = buttonPanel;
    }

    public List<RsUIField> getSystemFields()
    {
        return systemFields;
    }

    public List<RsPanelDTO> getPanels()
    {
        return panels;
    }

    public void setPanels(List<RsPanelDTO> panels)
    {
        this.panels = panels;
    }

    public void setSystemFields(List<RsUIField> systemFields)
    {
        this.systemFields = systemFields;
    }

    public String getViewName()
    {
        return viewName;
    }

    public void setViewName(String viewName)
    {
        this.viewName = viewName;
    }

    public String getWindowTitle()
    {
        return windowTitle;
    }

    public void setWindowTitle(String windowTitle)
    {
        this.windowTitle = windowTitle;
    }

    public Boolean isHeaderVisible()
    {
        return headerVisible;
    }

    public void setHeaderVisible(Boolean headerVisible)
    {
        this.headerVisible = headerVisible;
    }

    public Boolean isWizard()
    {
        return wizard;
    }

    public void setWizard(Boolean wizard)
    {
        this.wizard = wizard;
    }

    public RsFormProperties getProperties()
    {
        return properties;
    }

    public void setProperties(RsFormProperties properties)
    {
        this.properties = properties;
    }

    public String getAdminRoles()
    {
        return adminRoles;
    }

    public void setAdminRoles(String adminRoles)
    {
        this.adminRoles = adminRoles;
    }

    public String getTimezone()
    {
        return timezone;
    }

    public void setTimezone(String timezone)
    {
        this.timezone = timezone;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getReferenceTableNames()
    {
        return referenceTableNames;
    }

    public void setReferenceTableNames(String referenceTableNames)
    {
        this.referenceTableNames = referenceTableNames;
    }

}
