/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.customtable;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.query.Query;

import com.resolve.persistence.model.MetaFormView;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.MetaFormViewVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryParameter;
import com.resolve.services.vo.QuerySort.SortOrder;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.workflow.form.dto.CustomFormDTO;

import net.sf.json.JSONArray;
import net.sf.json.JSONNull;
import net.sf.json.JSONObject;

public class SearchCustomForm
{
    private static final String SORT_BY_JSON_PROPERTY = "property";
    private static final String SORT_BY_JSON_DIRECTION = "direction";
    
    private QueryDTO query = null; 
    private String username = null;
    private int start = -1;
    private int limit = -1;
    private Set<String> userRoles = null;
    
    public SearchCustomForm(QueryDTO query, String username) 
    {
        if(query == null || StringUtils.isEmpty(username))
        {
            throw new RuntimeException("Query and username are mandatory to search for custom forms");
        }
        
        this.query = query;
        this.username = username;
        this.start = query.getStart();
        this.limit = query.getLimit();
        userRoles = UserUtils.getUserRoles(this.username);
    }
    
    public List<MetaFormViewVO> execute() throws Exception
    {
        List<MetaFormViewVO> result = new ArrayList<MetaFormViewVO>();
        this.query.setPrefixTableAlias("mfv.");
        
        String hql = createHql();
        
        //set it in the query object
        this.query.setHql(hql.toString());
        
        try
        {
            //execute the qry
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, start, limit, true);
            if(data != null)
            {
                //grab the map of sysId-organizationname  
//                Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();

                String selectedColumns = query.getSelectColumns();
                if (StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");

                    //this will be the array of objects
                    for (Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        MetaFormView instance = new MetaFormView();

                        //set the attributes in the object
                        for (int x = 0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];

                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        result.add(instance.doGetVO());
                    }//end of for loop
                }
                else
                {
                    for (Object o : data)
                    {
                        MetaFormView instance = (MetaFormView) o;
                        result.add(instance.doGetVO());
                    }
                }
            }//end of if
            
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting data for sql:" + hql.toString(), e);
            throw e;
        }
        
        
        return result;
        
    }
        
    private String createHql()
    {
        StringBuilder hql = new StringBuilder("select ");
        
        if(StringUtils.isNotBlank(query.getSelectColumns()))
        {
            StringBuilder updatedCols = new StringBuilder();
            String[] selectedCols = query.getSelectColumns().split(",");
            for(String col : selectedCols)
            {
                updatedCols.append(this.query.getPrefixTableAlias()).append(col).append(",");
            }
            
            //remove the last comma
            updatedCols.setLength(updatedCols.length()-1);
            
            hql.append(updatedCols);
        }
        else
        {
            hql.append(" ").append(this.query.getPrefixTableAlias());
            //remove the last .
            hql.setLength(hql.length()-1);
        }
        
        
        hql.append(" from MetaFormView as mfv, MetaAccessRights as ar where ");
        hql.append(" ar.UResourceId = mfv.sys_id and ar.UResourceType ='" + MetaFormView.RESOURCE_TYPE + "' and ");

        //role filter 
        String roleFilter = "";
        //qry only if its not an 'admin', else show everything
        if (!this.userRoles.contains("admin") && !username.equals("resolve.maint"))
        {
            StringBuilder filters = new StringBuilder("(");
            for (String userRole : userRoles)
            {
                filters.append(" ar.UAdminAccess like '%" + userRole + "%' or ar.UReadAccess like '%" + userRole + "%' or ar.UWriteAccess like '%" + userRole + "%' or");
            }
            filters.setLength(filters.length() - 2);
            filters.append(")");  
            roleFilter = filters.toString();
        }
        else
        {
            roleFilter = " 1=1 ";
        }
        hql.append(roleFilter);
        
        //additional where clause
        String whereClause = this.query.getWhereClause();
        if(StringUtils.isNotBlank(whereClause))
        {
            StringBuilder updatedWhereClause = new StringBuilder();
            String[] clauses = whereClause.split("and");
            for(String clause : clauses)
            {
                String localClause = clause.toLowerCase();
                if (localClause.contains("lower") || localClause.contains("upper"))
                {
                    /*
                     * This is for custom queries when we need case insensitive searches.
                     * It's assumed that table prefix is taken care already.
                     */
                    updatedWhereClause.append(clause + " and ");
                }
                else
                {
                    updatedWhereClause.append(query.getPrefixTableAlias() + clause + " and ");
                }
            }
            
            updatedWhereClause.setLength(updatedWhereClause.length() - 4);
            
            hql.append(" and ").append(updatedWhereClause.toString());
        }
        
        //search filter
        String searchQry = this.query.getFilterWhereClause();
        if (StringUtils.isNotBlank(searchQry))
        {
            hql.append(" and ").append(searchQry);
        }
        
        //sort condition --> [{"property":"name","direction":"DESC"}]
        //comes from the UI...so will have to add the 'mfv.' in front of the properties
        this.query.parseSortClause();
        String sortBy = this.query.getOrderBy();
        if(StringUtils.isBlank(sortBy))
        {
            sortBy = this.query.getSort();
        }
        
        if(StringUtils.isNotBlank(sortBy))
        {
            hql.append(" order by ").append(sortBy.trim());
        }
        
        
        return hql.toString();
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public List<CustomFormDTO> executeForUI() throws Exception
    {
        List<CustomFormDTO> result = new ArrayList<CustomFormDTO>();
        
        QueryDTO queryDTO = new QueryDTO();
        
        String sqlQueryStr = createSqlForUI();
        
        queryDTO.setSqlQuery(sqlQueryStr);        
        queryDTO.setUseSql(true);
        
        try
        {
          HibernateProxy.setCurrentUser("system");
            HibernateProxy.execute(() -> {
            
	            Query query = HibernateUtil.createSQLQuery(queryDTO.getSelectSQLNonParameterizedSort());
	            
	            if (this.start > -1)
	            {
	                query.setFirstResult(start);
	            }
	            
	            if (this.limit > 0)
	            {
	                query.setMaxResults(limit);
	            }
	            
	            for (QueryParameter param : this.query.getWhereParameters())
	            {
	                if (sqlQueryStr.contains(param.getName()))
	                {
	                    query.setParameter(param.getName(), param.getValue());
	                }
	            }
				if (!this.userRoles.contains("admin") && !username.equals("resolve.maint")) {
					int roleCount = 1;
					for(String userRole : userRoles) {
						query.setParameter("userRole"+ roleCount, "%" + userRole+  "%");
						roleCount++;
					}
				}
	            
	            List<Object[]> data = query.list();
	            
	            if(data != null)
	            {
	                for (Object[] aRow : data)
	                {
	                    CustomFormDTO customFormDTO = new CustomFormDTO();
	                    
	                    customFormDTO.setId((String)aRow[0]);
	                    
	                    if (StringUtils.isNotBlank((String)aRow[1]))
	                    {
	                        customFormDTO.setUFormName((String)aRow[1]);
	                    }
	                    
	                    if (StringUtils.isNotBlank((String)aRow[2]))
	                    {
	                        customFormDTO.setUViewName((String)aRow[2]);
	                    }
	                    
	                    if (StringUtils.isNotBlank((String)aRow[3]))
	                    {
	                        customFormDTO.setUDisplayName((String)aRow[3]);
	                    }
	                    
	                    if (StringUtils.isNotBlank((String)aRow[4]))
	                    {
	                        customFormDTO.setUTableName((String)aRow[4]);
	                    }
	                    
	                    if (StringUtils.isNotBlank((String)aRow[5]))
	                    {
	                        customFormDTO.setUTableDisplayName((String)aRow[5]);
	                    }
	                    
	                    if (aRow[6] != null)
	                    {
	                        customFormDTO.setSysCreatedOn((java.sql.Timestamp)aRow[6]);
	                    }
	                    
	                    if (StringUtils.isNotBlank((String)aRow[7]))
	                    {
	                        customFormDTO.setSysCreatedBy((String)aRow[7]);
	                    }
	                    
	                    if (aRow[8] != null)
	                    {
	                        customFormDTO.setSysUpdatedOn((java.sql.Timestamp)aRow[8]);
	                    }
	                    
	                    if (StringUtils.isNotBlank((String)aRow[9]))
	                    {
	                        customFormDTO.setSysUpdatedBy((String)aRow[9]);
	                    }
	                    
	                    result.add(customFormDTO);
	                }
	            }
            
        	});            
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting data for sql:" + queryDTO.getSqlQuery(), e);
            throw e;
        }
        
        return result;
    }
    
    private String createSqlForUI()
    {
        StringBuilder sql = new StringBuilder("select mfv.sys_id, mfv.u_form_name, mfv.u_view_name, mfv.u_display_name, mfv.u_table_name, mfv.u_table_display_name, mfv.sys_created_on, mfv.sys_created_by, mfv.sys_updated_on, mfv.sys_updated_by "); 
        sql.append("from meta_form_view mfv, meta_access_rights ar ");
        sql.append("where ar.u_resource_sys_id = mfv.sys_id ");
        sql.append("and ar.u_resource_type = 'meta_form_view' ");
		if (!this.userRoles.contains("admin") && !username.equals("resolve.maint")) {                
		sql.append("and (");
			for(int roleCount = 1; roleCount <= userRoles.size(); roleCount++){
				sql.append("(ar.u_admin_access like :userRole");
				sql.append(roleCount);
				sql.append(" or ar.u_read_access like :userRole");
				sql.append(roleCount);
				if(roleCount != userRoles.size()) {
					sql.append(") or ");
				} else {
					sql.append(")");
				}
			}
			sql.append(") ");
        }
        //search filter
        String searchQry = this.query.getFilterWhereClause();
        
        if (StringUtils.isNotBlank(searchQry))
        {
            searchQry = searchQry.replaceAll("\\bUFormName\\b", "mfv.u_form_name");
            searchQry = searchQry.replaceAll("\\bUDisplayName\\b", "mfv.u_display_name");
            searchQry = searchQry.replaceAll("\\bUTableName\\b", "mfv.u_table_name");
            searchQry = searchQry.replaceAll("\\bsysCreatedOn\\b", "mfv.sys_created_on");
            searchQry = searchQry.replaceAll("\\bsysCreatedBy\\b", "mfv.sys_created_by");
            searchQry = searchQry.replaceAll("\\bsysUpdatedOn\\b", "mfv.sys_updated_on");
            searchQry = searchQry.replaceAll("\\bsysUpdatedBy\\b", "mfv.sys_updated_by");
            
            sql.append("and (" + searchQry + ") ");
        }
        
        String sortQry = this.query.getSort();
        String orderByClause = null;
        
        if (StringUtils.isNotBlank(sortQry))
        {
            JSONArray sortJSONArray = StringUtils.stringToJSONArray(sortQry);
            
            if (sortJSONArray != null && !sortJSONArray.isEmpty())
            {
                JSONObject sortJSON = sortJSONArray.getJSONObject(0);
                
                if (sortJSON != null && !JSONNull.getInstance().equals(sortJSON))
                {
                    String sortByPropVal = sortJSON.getString(SORT_BY_JSON_PROPERTY);
                    String sortDirVal = sortJSON.getString(SORT_BY_JSON_DIRECTION);
                    
                    if (StringUtils.isNotBlank(sortByPropVal) && StringUtils.isNotBlank(sortDirVal) && 
                        (sortByPropVal.equals("UFormName") || sortByPropVal.equals("UDisplayName") || 
                         sortByPropVal.equals("UTableName") || sortByPropVal.equals("sysCreatedOn") || 
                         sortByPropVal.equals("sysCreatedBy") || sortByPropVal.equals("sysUpdatedOn") || 
                         sortByPropVal.equals("sysUpdatedBy")) &&
                        (sortDirVal.equals(SortOrder.ASC.name()) || sortDirVal.equals(SortOrder.DESC.name())))
                    {
                        String orderByColumn = null;
                        
                        switch(sortByPropVal)
                        {
                            case "UFormName" :
                                orderByColumn = "mfv.u_form_name";
                                break;
                                
                            case "UDisplayName" :
                                orderByColumn = "mfv.u_display_name";
                                break;
                                
                            case "UTableName" :
                                orderByColumn = "mfv.u_table_name";
                                break;
                                
                            case "sysCreatedOn" :
                                orderByColumn = "mfv.sys_created_on";
                                break;
                                
                            case "sysCreatedBy" :
                                orderByColumn = "mfv.sys_created_by";
                                break;
                                
                            case "sysUpdatedOn" :
                                orderByColumn = "mfv.sys_updated_on";
                                break;
                                
                            case "sysUpdatedBy" :
                                orderByColumn = "mfv.sys_updated_by";
                                break;
                        }
                        
                        orderByClause = " " + orderByColumn + " " + sortDirVal;
                    }
                }               
            }
        }
        
        if (StringUtils.isNotBlank(orderByClause))
        {
            sql.append("order by " + orderByClause);
        }
        else
        {
            sql.append("order by mfv.u_form_name asc");
        }
        
        Log.log.debug("SQL for search custom form [" + sql.toString() + "]");
        
        return sql.toString();
    }
//    private static MetaFormViewVO convertModelToVO(MetaFormView model, Map<String, String> mapOfOrganization)
//    {
//        MetaFormViewVO vo = null;
//
//        if (model != null)
//        {
//            if (mapOfOrganization == null)
//            {
//                mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
//            }
//
//            vo = model.doGetVO();
//            if (StringUtils.isNotBlank(vo.getSysOrg()))
//            {
//                vo.setSysOrganizationName(mapOfOrganization.get(vo.getSysOrg()));
//            }
//            else
//            {
//                vo.setSysOrganizationName("");
//            }
//        }
//
//        return vo;
//    }        
    
    
}
