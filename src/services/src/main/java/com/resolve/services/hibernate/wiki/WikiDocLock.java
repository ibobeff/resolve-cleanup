/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

import com.resolve.util.Log;

/**
 * This class represents if a wiki doc is locked or not. If there is a rec of this type, that means that WIKI doc is in EDIT mode and no other user should be able to edit this doc. VIEW mode still be available to the other users
 * 
 * @author jeet.marwah
 * 
 */
public class WikiDocLock implements Serializable//DataSerializable
{
    private static final long serialVersionUID = -2200979551165316760L;
    
    private String username;
	private String wikiDocFullName;
	private Long startLockTime;

	public WikiDocLock()
	{
	}

	public WikiDocLock(String username, String wikiDocFullName)
	{
		this.username = username;
		this.wikiDocFullName = wikiDocFullName;
		this.startLockTime = System.currentTimeMillis();
	} // WikiDocLock

	
	public String getUsername()
	{
		return username;
	}

	public String getWikiDocFullName()
	{
		return wikiDocFullName;
	}

	public Long getStartLockTime()
	{
		return startLockTime;
	}

	
	public void setUsername(String username)
	{
		this.username = username;
	}

	public void setWikiDocFullName(String wikiDocFullName)
	{
		this.wikiDocFullName = wikiDocFullName;
	}

	public void setStartLockTime(Long startLockTime)
	{
		this.startLockTime = startLockTime;
	}
//	
//
//	public void readData(DataInput in) throws IOException
//	{
//		username = in.readUTF();
//		wikiDocFullName = in.readUTF();
//		startLockTime = in.readLong();
//	}
//
//	public void writeData(DataOutput out) throws IOException
//	{
//		out.writeUTF(username);
//		out.writeUTF(wikiDocFullName);
//		out.writeLong(startLockTime);
//	}
	
	@Override
	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    	sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
    	
    	return String.format("%s - User Name: %s , Wiki Doc Full Name: %s, Start Lock Time: %s",
    						 WikiDocLock.class.getSimpleName(), username, wikiDocFullName, 
    						 sdf.format(new Date(startLockTime)));
	}
}
