package com.resolve.services.hibernate.util;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.resolve.services.hibernate.vo.ArtifactConfigurationVO;
import com.resolve.services.hibernate.vo.ArtifactTypeVO;
import com.resolve.services.hibernate.vo.CEFDictionaryItemVO;
import com.resolve.services.hibernate.vo.CustomDictionaryItemVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.vo.ArtifactConfigurationDTO;
import com.resolve.services.vo.ArtifactTypeDTO;
import com.resolve.services.vo.CEFDictionaryItemDTO;
import com.resolve.services.vo.CustomDictionaryItemDTO;
import com.resolve.services.vo.GenericDictionaryItemDTO;
import com.resolve.services.vo.GenericDictionaryItemDTO.ArtifactTypeSource;
import com.resolve.services.vo.ResolveActionTaskDTO;
import com.resolve.services.vo.WikiDocumentDTO;
import com.resolve.util.cef.CEFParseException;

public class ConverterUtil {
    public static CustomDictionaryItemDTO convertCustomItemVOToDTO(CustomDictionaryItemVO vo) {
    	if (vo != null) {
    		CustomDictionaryItemDTO dto = new CustomDictionaryItemDTO();
        	dto.setUShortName(vo.getUShortName());
        	dto.setUFullName(vo.getUFullName());
        	dto.setUDescription(vo.getUDescription());
        	return dto;
    	}
    	throw new IllegalArgumentException("Unable to convert CEF item");
    }

    public static GenericDictionaryItemDTO convertCustomItemVOToGenericDTO(CustomDictionaryItemVO vo) {
    	if (vo != null) {
    		GenericDictionaryItemDTO dto = new GenericDictionaryItemDTO();
    		dto.setUArtifactTypeSource(ArtifactTypeSource.CUSTOM);
        	dto.setUShortName(vo.getUShortName());
        	dto.setUFullName(vo.getUFullName());
        	dto.setUDescription(vo.getUDescription());
        	return dto;
    	}
    	throw new IllegalArgumentException("Unable to convert CEF item");
    }

    public static CustomDictionaryItemDTO convertGenericDTOToCustomDictionaryItemDTO(GenericDictionaryItemDTO dto) {
    	if (dto != null && dto.getUArtifactTypeSource() == ArtifactTypeSource.CUSTOM) {
    		CustomDictionaryItemDTO cDto = new CustomDictionaryItemDTO();
    		cDto.setUShortName(dto.getUShortName());
    		cDto.setUFullName(dto.getUFullName());
    		cDto.setUDescription(dto.getUDescription());
        	return cDto;
    	}
    	throw new IllegalArgumentException("Unable to convert to custom item");
    }

	public static CEFDictionaryItemDTO convertCEFItemVOToDTO(CEFDictionaryItemVO vo) throws CEFParseException {
    	if (vo != null) {
    	CEFDictionaryItemDTO dto = new CEFDictionaryItemDTO();
        	dto.setUShortName(vo.getUShortName());
        	dto.setUFullName(vo.getUFullName());
        	dto.setUDataType(vo.getUDataType());
        	dto.setULength(vo.getULength());
        	dto.setUDescription(vo.getUDescription());
        	return dto;
    	}
    	throw new CEFParseException("Unable to convert CEF item");
    }

	public static GenericDictionaryItemDTO convertCEFVOToGenericDTO(CEFDictionaryItemVO vo) throws CEFParseException {
    	if (vo != null) {
    		GenericDictionaryItemDTO dto = new GenericDictionaryItemDTO();
    		dto.setUArtifactTypeSource(ArtifactTypeSource.CEF);
        	dto.setUShortName(vo.getUShortName());
        	dto.setUFullName(vo.getUFullName());
        	dto.setUDataType(vo.getUDataType());
        	dto.setULength(vo.getULength());
        	dto.setUDescription(vo.getUDescription());
        	return dto;
    	}
    	throw new CEFParseException("Unable to convert CEF item");
    }

    public static ArtifactTypeDTO convertArtifactTypeVOToDTO(ArtifactTypeVO vo) throws CEFParseException {
    	ArtifactTypeDTO dto = new ArtifactTypeDTO();
    	dto.setUName(vo.getUName());
    	dto.setUStandard(vo.getUStandard());

		Set<GenericDictionaryItemDTO> dtos = new HashSet<>();

    	if (CollectionUtils.isNotEmpty(vo.getCefItems())) {
    		for (CEFDictionaryItemVO item : vo.getCefItems()) {
    			dtos.add(convertCEFVOToGenericDTO(item));
    		}
		}

    	if (CollectionUtils.isNotEmpty(vo.getCustomItems())) {
    		for (CustomDictionaryItemVO item : vo.getCustomItems()) {
    			dtos.add(convertCustomItemVOToGenericDTO(item));
    		}
		}

		dto.setDictionaryItems(dtos);

    	return dto;
    }

    public static ArtifactConfigurationDTO convertArtifactVOToDTO(ArtifactConfigurationVO vo) throws CEFParseException {
    	ArtifactConfigurationDTO dto = new ArtifactConfigurationDTO();
    	dto.setUName(vo.getUName());
    	dto.setParam(vo.getParam());
    	if (vo.getArtifactType() != null) {
    	    dto.setArtifactType(convertArtifactTypeVOToDTO(vo.getArtifactType()));
    	} else {
    	    dto.setArtifactType(new ArtifactTypeDTO());
    	}
    	
    	if (vo.getAutomation() != null) {
    		dto.setAutomation(convertWikiDocumentVOToDTO(vo.getAutomation()));
    	}

    	if (vo.getTask() != null) {
    		dto.setTask(convertResolveActionTaskVOToDTO(vo.getTask()));
    	}
    		
    	return dto;
    }
    
    public static ResolveActionTaskDTO convertResolveActionTaskVOToDTO(ResolveActionTaskVO vo) {
    	ResolveActionTaskDTO dto = new ResolveActionTaskDTO();
    	dto.setId(vo.getId());
    	dto.setUName(vo.getUName());
    	dto.setUNamespace(vo.getUNamespace());
    	dto.setUFullName(vo.getUFullName());
    	dto.setUTypes(vo.getUTypes());
    	dto.setULogresult(vo.getULogresult());
    	dto.setUSummary(vo.getUSummary());
    	dto.setUDescription(vo.getUDescription());
    	dto.setURoles(vo.getURoles());
    	dto.setUActive(vo.getUActive());
    	dto.setUMenuPath(vo.getUMenuPath());
    	dto.setUIsDeleted(vo.getUIsDeleted());
    	dto.setUIsDefaultRole(vo.getUIsDefaultRole());

    	return dto;
    }

    public static WikiDocumentDTO convertWikiDocumentVOToDTO(WikiDocumentVO vo) {
    	WikiDocumentDTO dto = new WikiDocumentDTO();
    	dto.setId(vo.getId());
    	dto.setUName(vo.getUName());
    	dto.setUFullname(vo.getUFullname());
    	dto.setUTitle(vo.getUTitle());
    	dto.setUSummary(vo.getUSummary());
    	dto.setUContent(vo.getUContent());

    	return dto;
    }
}
