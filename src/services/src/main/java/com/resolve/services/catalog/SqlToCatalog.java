/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.catalog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.resolve.services.hibernate.vo.ResolveCatalogNodeVO;
import com.resolve.util.StringUtils;


/**
 * 
System.out.println(vo.getSys_id() + " :" + vo.getUName() + " :" + vo.getULft() + " :" + vo.getURgt() + " :" + vo.getUDepth());
17480.0 :Cherry :4 :5 :4
17479.0 :Red :3 :6 :3
17482.0 :Banana :8 :9 :4
17481.0 :Yellow :7 :10 :3
17478.0 :Fruit :2 :11 :2
17484.0 :Beef :13 :14 :3
17485.0 :Pork :15 :16 :3
17483.0 :Meat :12 :17 :2
17491.0 :Vegetables :19 :20 :3
17490.0 :Vegetables Reference Node :18 :21 :2
17472.0 :Food :1 :22 :1

 * 
 * @author jeet.marwah
 *
 */
public class SqlToCatalog
{
    private List<ResolveCatalogNodeVO> vos = null;
    private List<Catalog> allNodes = new ArrayList<Catalog>();
    private Catalog rootCatalog = null;

    public SqlToCatalog(List<ResolveCatalogNodeVO> vos)
    {
        this.vos = vos;
    }
    
    public Catalog generateCatalog()
    {
        if(this.vos != null && vos.size() > 0)
        {
            //prepare the Catalog objects from ResolveCatalogVO
            for(ResolveCatalogNodeVO vo : vos)
            {
                allNodes.add(transformVOToCatalog(vo));
            }
            
            //get the root catalog
            rootCatalog = findRoot();//findCatalogsOfDepth(1).get(0);
            
            //get the children 
            List<Catalog> children = getChildrenOf(rootCatalog.getLft(), rootCatalog.getRgt(), rootCatalog.getDepth());
            rootCatalog.setChildren(children);
        }
        
        return rootCatalog;
    }
    
    private List<Catalog> getChildrenOf(int parentLft, int parentRgt, int parentDepth)
    {
        List<Catalog> childs = new ArrayList<Catalog>();
        int childDepth = parentDepth + 1;
        
        //prepare child nodes list
        for(Catalog c : allNodes)
        {
            int depth = c.getDepth();
            int currLft = c.getLft();
            int currRgt = c.getRgt();
            
            if(depth == childDepth 
                    && parentLft < currLft 
                    && parentRgt > currRgt)
            {
                childs.add(c);
            }
        }
        
        //update the childs of the children
        for(Catalog child : childs)
        {
            //**RECURSIVE
            child.setChildren(getChildrenOf(child.getLft(), child.getRgt(), child.getDepth()));
        }
        
        //sort 
        sortOnOrder(childs);
        
        return childs;
    }
    
    //private apis
    private static void sortOnOrder(List<Catalog> catalog)
    {
        if (!catalog.isEmpty())
        {
            Collections.sort(catalog, new Comparator<Catalog>()
            {
                public int compare(final Catalog cat1, final Catalog cat2)
                {
                    return cat1.getOrder() > cat2.getOrder() ? 1 : -1;
                }
            });
        }
    }
    
//    private List<Catalog> findCatalogsOfDepth(int depth)
//    {
//        List<Catalog> vosOfThisDepth = new ArrayList<Catalog>();
//        
//        for(Catalog vo : allNodes)
//        {
//            int currDepth = vo.getDepth();
//            if(currDepth == depth)
//            {
//                vosOfThisDepth.add(vo);
//            }
//        }
//        
//        return vosOfThisDepth;
//    }
    
    private Catalog findRoot()
    {
        Catalog root = null;
        
        for(Catalog vo : allNodes)
        {
            int currDepth = vo.getDepth();
            if(currDepth == 1)
            {
                root = vo;
                break;
            }
        }
        
        return root;
    }
    
    private Catalog transformVOToCatalog(ResolveCatalogNodeVO vo)
    {
        Catalog c = new Catalog();
        c.setId(vo.getSys_id());
        c.setName(vo.getUName());
        c.setType(vo.getUType());
        c.setOperation(vo.getUOperation());
//        c.setOrder(vo.getUOrder());
        c.setSys_created_by(vo.getSysCreatedBy());
        c.setSys_created_on(vo.getSysCreatedOn() != null ? vo.getSysCreatedOn().getTime() : new Date().getTime());
        c.setSys_updated_by(vo.getSysUpdatedBy());
        c.setSys_updated_on(vo.getSysUpdatedOn() != null ? vo.getSysUpdatedOn().getTime() : new Date().getTime());
        
        c.setRoles(vo.getURoles());
        c.setEditRoles(vo.getUEditRoles());
        
        c.setTitle(vo.getUTitle());
        c.setDescription(vo.getUDescription());
        c.setImage(vo.getUImage());
        c.setImageName(vo.getUImageName());
        c.setTooltip(vo.getUTooltip());
        c.setWiki(vo.getUWiki());
        c.setLink(vo.getULink());
        c.setForm(vo.getUForm());
        c.setDisplayType(vo.getUDisplayType());
        c.setMaxImageWidth(vo.getUMaxImageWidth());
        c.setPath(vo.getUPath());
        c.setOpenInNewTab(vo.getUOpenInNewTab());
        c.setSideWidth(vo.getUSideWidth());
        
        c.setCatalogType(vo.getUCatalogType());
        c.setWikidocSysID(vo.getUWikidocSysID());
        
        c.setRoot(vo.getUIsRoot());
        c.setRootRef(vo.getUIsRootRef());
        
        c.setInternalName(vo.getUInternalName());
        c.setIcon(vo.getUIcon());
        
        c.setNamespace(vo.getUNamespace());
        
//        c.setLft(vo.getULft());
//        c.setRgt(vo.getURgt());
//        c.setDepth(vo.getUDepth());

        //tags
        c.setTags(vo.getTags());
        
        //for reference nodes
        if(c.getType().equalsIgnoreCase("CatalogReference") 
                        && !c.isRoot() 
                        && StringUtils.isNotEmpty(vo.getUReferenceCatalogSysId()))
        {
            try
            {
                Catalog refCatalog = CatalogUtil.getCatalog(vo.getUReferenceCatalogSysId(), null, "admin");
                if(refCatalog != null)
                {
                    List<Catalog> childs = new ArrayList<Catalog>();
                    childs.add(refCatalog);
                    
                    c.setChildren(childs);
                }
            }
            catch (Exception e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
        }
        
        return c;
    }
    
}
