package com.resolve.services.vo;

import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import com.resolve.services.constants.annotation.ContentTypeContraint;
import com.resolve.services.constants.annotation.IntegerContraint;
import com.resolve.services.validator.sequence.Level1;
import com.resolve.services.validator.sequence.Level2;
import com.resolve.services.validator.sequence.Level3;
import com.resolve.util.StringUtils;

/**
 * The search DTO is used to represent search criteria for requests to the
 * aggregated content search.
 * 
 */
public class SearchAttributesDTO {

	private String sysId;

	@Size(max = 128, message = "Term must not exceed 128 characters.", groups = Level1.class)
	private String term;

	private List<String> searchFields;

	@ContentTypeContraint(groups = Level1.class)
	private String type = "ALL";

	private List<String> authors;

	private List<String> tags;

	private String createdOn;

	private String updatedOn;

	private String sortBy;

	// asc, desc
	private String sortOrder;

	@IntegerContraint(message = "Size must be an integer.", groups = Level1.class)
	@Min(value = 1, message = "Size must be greater that 0.", groups = Level2.class)
	@Max(value = 200, message = "Size must not exceed 200.", groups = Level3.class)
	private String size = "20";

	@IntegerContraint(message = "Size must be an integer.", groups = Level1.class)
	@Min(value = 0, message = "From must be great or equal to 0.", groups = Level2.class)
	@Max(value = 2000000, message = "From must not exceed 2000000.", groups = Level3.class)
	private String from = "0";

	private List<String> namespaces;

	private List<String> menupaths;

	public String getSysId() {
		return sysId;
	}

	public void setSysId(String sysId) {
		this.sysId = sysId;
	}

	public String getTerm() {
		return this.term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public List<String> getSearchFields() {
		return this.searchFields;
	}

	public void setSearchFields(List<String> searchFields) {
		this.searchFields = searchFields;
	}

	public ContentType getType() {
		return ContentType.valueOf(this.type.toUpperCase());
	}

	public void setType(String type) {
		if (StringUtils.isNotBlank(type)) {
			this.type = type;
		}
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getSortBy() {
		return this.sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public int getSize() {
		return Integer.valueOf(this.size);
	}

	public void setSize(String size) {
		this.size = size;
	}

	public int getFrom() {
		return Integer.valueOf(this.from);
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public List<String> getNamespaces() {
		return namespaces;
	}

	public void setNamespaces(List<String> namespaces) {
		this.namespaces = namespaces;
	}

	public List<String> getMenupaths() {
		return menupaths;
	}

	public void setMenupaths(List<String> menupaths) {
		this.menupaths = menupaths;
	}

}
