/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author jeet.marwah
 *
 */
public class DbDTO
{
    private String table;
    private List<Map<String, Object>> records;
    
    public String getTable()
    {
        return table;
    }
    public void setTable(String table)
    {
        this.table = table;
    }
    public List<Map<String, Object>> getRecords()
    {
        return records;
    }
    public void setRecords(List<Map<String, Object>> records)
    {
        this.records = records;
    }
    
    
    
    

}
