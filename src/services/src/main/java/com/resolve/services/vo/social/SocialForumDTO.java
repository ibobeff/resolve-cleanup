/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.social;

import java.util.Map;

import com.resolve.services.interfaces.SocialDTO;

public class SocialForumDTO extends SocialDTO
{
    private static final long serialVersionUID = -3486490944305254955L;
    public final static String TABLE_NAME = "social_forum";
    
    public SocialForumDTO() {}
    
    public SocialForumDTO(Map<String, Object> data)
    {
        super(data);
    }
    
    
//    List<UserDTO> userList = new ArrayList<UserDTO>();
//
//    public List<UserDTO> getUserList()
//    {
//        return userList;
//    }
//
//    public void setUserList(List<UserDTO> userList)
//    {
//        this.userList = userList;
//    }
}
