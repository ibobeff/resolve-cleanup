/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.List;


public class WikidocRatingDTO
{
    private String docSysId;
    private String docFullName;
    
    private List<RatingDTO> ratingDtos;
    private List<ChartRatingDTO> chartRatingDtos;
    private double rating;
    
    public String getDocSysId()
    {
        return docSysId;
    }
    public void setDocSysId(String docSysId)
    {
        this.docSysId = docSysId;
    }
    public String getDocFullName()
    {
        return docFullName;
    }
    public void setDocFullName(String docFullName)
    {
        this.docFullName = docFullName;
    }
    public List<RatingDTO> getRatingDtos()
    {
        return ratingDtos;
    }
    public void setRatingDtos(List<RatingDTO> ratingDtos)
    {
        this.ratingDtos = ratingDtos;
    }
    public List<ChartRatingDTO> getChartRatingDtos()
    {
        return chartRatingDtos;
    }
    public void setChartRatingDtos(List<ChartRatingDTO> chartRatingDtos)
    {
        this.chartRatingDtos = chartRatingDtos;
    }
    public double getRating()
    {
        return rating;
    }
    public void setRating(double rating)
    {
        this.rating = rating;
    }

}
