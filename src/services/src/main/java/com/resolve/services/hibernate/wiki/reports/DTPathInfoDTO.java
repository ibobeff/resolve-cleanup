/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.hibernate.wiki.reports;

public class DTPathInfoDTO
{
    private String dtFullName;
    private double totalTimeInSecs;
    
    public DTPathInfoDTO()
    {
        
    }

    public String getDtFullName()
    {
        return dtFullName;
    }

    public void setDtFullName(String dtFullName)
    {
        this.dtFullName = dtFullName;
    }

    public double getTotalTimeInSecs()
    {
        return totalTimeInSecs;
    }

    public void setTotalTimeInSecs(double totalTime)
    {
        this.totalTimeInSecs = totalTime;
    }
    
    

}
