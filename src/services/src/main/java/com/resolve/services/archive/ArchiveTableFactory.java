/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.archive;

import com.resolve.util.Constants;
import com.resolve.util.Log;

public class ArchiveTableFactory
{
    private static BaseArchiveTable instance;
    
    private ArchiveTableFactory()
    {
        
    }
    
    public static BaseArchiveTable getTableArchiver(String dbType)
    {
    	Log.log.info("dyType = " + dbType + ", instance = " + instance);
    	
        if(instance == null)
        {
            if(dbType.equalsIgnoreCase(Constants.DB_TYPE_MYSQL))
            {
                instance = new ArchiveMySqlTable();
            }
            if(dbType.equalsIgnoreCase(Constants.DB_TYPE_ORACLE) || dbType.equalsIgnoreCase(Constants.DB_TYPE_ORACLE_12C))
            {
                instance = new ArchiveOracleTable();
            }
            if(dbType.equalsIgnoreCase(Constants.DB_TYPE_DB2))
            {
                instance = new ArchiveDB2Table();
            }
            
            if (instance==null)
            {
            	Log.log.error("dyType doesn't match any existing type! dbType= " + dbType + ", instance = " + instance);
            }            
        }
        return instance;
    }
} // archive

