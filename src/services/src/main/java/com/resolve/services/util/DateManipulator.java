/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import com.resolve.util.constants.HibernateConstants;
import com.resolve.services.graph.social.model.message.Post;
import com.resolve.util.Constants;
import com.resolve.util.DateUtils;
import com.resolve.util.StringUtils;

public class DateManipulator
{

    private static final SimpleDateFormat SOCIAL_DATE_FORMAT = new SimpleDateFormat("MMM dd, yyyy");
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("MMM dd, yyyy");
    private static final DateTimeFormatter TIME_FORMAT = DateTimeFormat.forPattern("hh:mma");

    public static final String TIMEZONE = "timezone";
    public static final String TIMEZONE_OFFSET = "timezoneOffset";
    
    
    public static long GetUTCDateLong()
    {
        return GetUTCDate().getMillis();
    }

    public static DateTime parseISODate(String dateString)
    {
        return DateTime.parse(dateString, ISODateTimeFormat.dateTimeParser()).toDateTime(DateTimeZone.UTC);
    }

    public static DateTime GetUTCDate()
    {
        return new DateTime(DateTimeZone.UTC);
    }

    public static String getSocialDate1(long localDate)
    {
        // TimeZone tz = TimeZone.getTimeZone(tzName.trim());
        // GregorianCalendar calendar = new GregorianCalendar(tz);
        // calendar.setTimeInMillis(timeInMillis);
        String result = SOCIAL_DATE_FORMAT.format(new Date(localDate));
        return result;
    }

    public static String getSocialIsoDateString(long localDate)
    {
        return getSocialIsoDate(localDate).toString();
    }

    public static DateTime getSocialIsoDate(long localDate)
    {
        return new DateTime(localDate, DateTimeZone.UTC);
    }

    /**
     * 4:34 AM (10 hours ago) Jan 11, 2012 (yesterday) Jan 10, 2012 (2 days ago)
     * 
     */
    public static String getSocialDate(long gmtDate, Map<String, Object> params)
    {
        String timezone = params.get(TIMEZONE) != null ? (String) params.get(TIMEZONE) : Post.TIMEZONE_DEFAULT;
        boolean offset = params.get(TIMEZONE_OFFSET) != null ? (Boolean) params.get(TIMEZONE_OFFSET) : Post.TIMEZONE_OFFSET_DEFAULT;

        return getSocialDate(gmtDate, timezone, offset);
    }

    public static String getSocialDate(long gmtDate, String timezone, boolean offset)
    {
        String dateStr = "";
        timezone = StringUtils.isNotBlank(timezone) ? timezone : Post.TIMEZONE_DEFAULT;
        // long localTimeInMills = DateUtils.convertGMTToLocalTime(new
        // Date(gmtDate), timezone, offset).getTime();
        long localTimeInMills = GetUTCDateLong();

        DateTime dateTime = new DateTime(localTimeInMills);
        DateTime today = new DateTime();
        DateTime yesterday = today.minusDays(1);

        if (dateTime.toLocalDate().equals(today.toLocalDate()))
        {
            int hours = Hours.hoursBetween(dateTime, today).getHours();
            if (hours == 0)
            {
                // this is in mins
                int mins = Minutes.minutesBetween(dateTime, today).getMinutes();
                if (mins == 0)
                {
                    int secs = Seconds.secondsBetween(dateTime, today).getSeconds();
                    dateStr = secs == 1 ? secs + " sec ago" : secs + " secs ago";
                }
                else
                {
                    dateStr = mins == 1 ? mins + " min ago" : mins + " mins ago";
                }
            }
            else
            {
                dateStr = hours == 1 ? TIME_FORMAT.print(dateTime) + " (" + hours + " hour ago)" : TIME_FORMAT.print(dateTime) + " (" + hours + " hours ago)";
            }
        }
        else if (dateTime.toLocalDate().equals(yesterday.toLocalDate()))
        {
            dateStr = DATE_FORMAT.print(dateTime) + " (yesterday)";
        }
        else
        {
            int days = Days.daysBetween(dateTime, today).getDays();
            dateStr = days == 1 ? DATE_FORMAT.print(dateTime) + " (" + days + " day ago)" : DATE_FORMAT.print(dateTime) + " (" + days + " days ago)";
        }

        return dateStr;
    }

    public static Date convertLocalToGMT(Date localDate)
    {
        DateTime dateTime = new DateTime(localDate);
        // System.out.println("Current Local Date :" + dateTime.toDate());
        // System.out.println("Current Local Date In Millis :" +
        // dateTime.toDate().getTime());

        DateTime gmt = dateTime.withZoneRetainFields(DateTimeZone.forID("GMT"));
        // System.out.println("GMT Date :" + gmt.toDate());
        // System.out.println("GMT Date In Millis :" + gmt.toDate().getTime());

        Date d = gmt.toDate();

        return d;
    }
    
    public static String getDocumentReviewedDate(Date lastReviewedOnGMT, String browserTimeZone)
    {
        String date = "Not Reviewed";
        
        if(StringUtils.isEmpty(browserTimeZone))
        {
            browserTimeZone = "GMT-07:00";
        }
        
        if(lastReviewedOnGMT != null)
        {
            DateTime dateTime = new DateTime(lastReviewedOnGMT);
            DateTime today = new DateTime();
            int days = Days.daysBetween(dateTime, today).getDays();
            if(days > 365)
            {
                date = HibernateConstants.dateWithoutTime.format(DateUtils.convertGMTDateToClientLocalDate(lastReviewedOnGMT, browserTimeZone));
            }
            else
            {
                date = HibernateConstants.dateWithoutYear.format(DateUtils.convertGMTDateToClientLocalDate(lastReviewedOnGMT, browserTimeZone));
            }
        }
        
        return date;
    }

    public static Date convertGMTToLocal(long gmtTimeInMillis, String timezoneID)
    {
        DateTime dateTime = new DateTime(gmtTimeInMillis, DateTimeZone.forID("GMT"));
        // System.out.println("Current Local Date :" + dateTime.toDate());
        // System.out.println("Current Local Date In Millis :" +
        // dateTime.toDate().getTime());

        DateTime gmt = dateTime.withZoneRetainFields(DateTimeZone.forID(timezoneID));
        // System.out.println("GMT Date :" + gmt.toDate());
        // System.out.println("GMT Date In Millis :" + gmt.toDate().getTime());

        Date d = gmt.toDate();

        return d;
    }
    
    public static String getUTCDefaultDate()
    {
        String utcdate = GetUTCDate().toString();
        String date = utcdate .substring(0, utcdate .length()-2) + ":" + utcdate .substring(utcdate .length()-2);
        return date;
    }
    
    public static String getDateInUTCFormat(Timestamp ts)
    {
        String dateStr = null;
        
        if(ts != null)
        {
            dateStr = new SimpleDateFormat(Constants.DEFAULT_SQL_DATE_FORMAT).format(ts) + "-0000";
        }
        
        return dateStr;
    }
    
    public static String getDateInUTCFormat(Date ts)
    {
        String dateStr = null;
        
        if(ts != null)
        {
            dateStr = new SimpleDateFormat(Constants.DEFAULT_SQL_DATE_FORMAT).format(ts) + "-0000";
        }
        
        return dateStr;
    }
    

//    private static void showTimezones()
//    {
//        String[] ids = TimeZone.getAvailableIDs();
//        for (String id : ids)
//        {
//            System.out.println(id);
//        }
//
//    }

    /*
    public static void main(String[] args)
    {
//        long time = 0;
        // System.out.println(getSocialDate(time, null));
        // showTimezones();
        convertLocalToGMT(new Date());
    }
    */
    
    

}// DateManipulator
