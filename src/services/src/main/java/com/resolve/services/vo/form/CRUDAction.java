/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.form;

import java.io.Serializable;
import java.util.ArrayList;

import com.resolve.dto.ComboBoxModel;

/**
 * Used in the META_FORM to decise where the data is coming from 
 * 
 * @author jeet.marwah
 *
 */

public enum CRUDAction implements Serializable
{
	//this is mapped to wikiarchive table
	INSERT("INSERT"),
	UPDATE("UPDATE"),
	SUBMIT("SUBMIT"),//for both insert and update
	DELETE("DELETE");
	
	//definition of the
	private final String tagName;
	
	private CRUDAction(String tagName)
	{
		this.tagName = tagName;
	}
	
	public String getTagName()
	{
		return this.tagName;
	}
	
	public static ArrayList<String> getDataSourceList()
	{
	    ArrayList<String> list = new ArrayList<String>();
	    list.add(CRUDAction.SUBMIT.getTagName());
        list.add(CRUDAction.DELETE.getTagName());
	    
//	    CRUDAction[] arr = CRUDAction.values();
//	    for(CRUDAction ds : arr)
//	    {
//	        list.add(ds.getTagName());
//	    }
	    
	    return list;
	}//getDataSourceList

    public static ArrayList<ComboBoxModel> getCrudActions()
    {
        ArrayList<ComboBoxModel> list = new ArrayList<ComboBoxModel>();
        list.add(new ComboBoxModel(CRUDAction.SUBMIT.getTagName(), CRUDAction.SUBMIT.getTagName()));
        list.add(new ComboBoxModel(CRUDAction.DELETE.getTagName(), CRUDAction.DELETE.getTagName()));
        
//        CRUDAction[] arr = CRUDAction.values();
//        for (CRUDAction ds : arr)
//        {
//            list.add(new ComboBoxModel(ds.getTagName(), ds.getTagName()));
//        }

        return list;
    }// getOperations
	
	public static CRUDAction getRevisionType(String tagName)
	{
		if(tagName == null)
		{
			return null;
		}
		else if (tagName.equals(INSERT.toString()))
		{
			return CRUDAction.INSERT;
		}
        else if (tagName.equals(UPDATE.toString()))
        {
            return CRUDAction.UPDATE;
        }
        else if (tagName.equals(SUBMIT.toString()))
        {
            return CRUDAction.SUBMIT;
        }
        else if (tagName.equals(DELETE.toString()))
		{
			return CRUDAction.DELETE;
		}
		else
		{
			return null;
		}
			
	}
}
