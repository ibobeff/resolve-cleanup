/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.catalog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.resolve.persistence.model.ResolveCatalog;
import com.resolve.persistence.model.ResolveCatalogNode;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ResolveCatalogEdgeVO;
import com.resolve.services.hibernate.vo.ResolveCatalogNodeVO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class GetCatalog
{
    private String catalogSysId = null;
    private String name = null;
    private String username = null;

    private ResolveCatalog catalog = null;
    private Catalog rootCatalog = null;

    public GetCatalog(String sysId, String catalogName, String username) throws Exception
    {
        if (StringUtils.isEmpty(sysId) && StringUtils.isEmpty(catalogName))
        {
            throw new Exception("Catalog Name or sysId is mandatory");
        }

        this.catalogSysId = sysId;
        this.name = catalogName;
        this.username = username;
    }

    public Catalog getCatalog() throws Exception
    {
        List<ResolveCatalogNodeVO> catalogNodesVO = getCatalogNodes();

        if(catalogNodesVO.size() > 0)
        {
            //get the root catalog
            ResolveCatalogNodeVO rootNode = findRoot(catalogNodesVO);
            rootCatalog = transformToCatalog(rootNode, 1, catalogNodesVO);
            rootCatalog.setParentSysId(catalog.getSys_id());
        }
        else
        {
            Log.log.info("Catalog with name '" + name + "' does not exist.");
        }

        return rootCatalog;
    }

    private Catalog transformToCatalog(ResolveCatalogNodeVO parentVO, int order, List<ResolveCatalogNodeVO> catalogNodesVO)
    {
        Catalog parentCatalog = transformVOToCatalog(parentVO, order);
        boolean evaluateChildren = true;
        
        //for reference nodes - do not evaluate children as its already done 
        if (parentCatalog.getType().equalsIgnoreCase(NodeType.CatalogReference.name()) && !parentCatalog.isRoot())
        {
            evaluateChildren = false;
        }
        
        //evaluate the child nodes for all except for the Reference Nodes
        if(evaluateChildren)
        {
            //filter out the children based on the edges
            List<String> childrenSysIds = new ArrayList<String>();
            Collection<ResolveCatalogEdgeVO> parentEdges = parentVO.getCatalogEdges();
            if (parentEdges != null && parentEdges.size() > 0)
            {
                //for sorting, we need a list.
                List<ResolveCatalogEdgeVO> parentEdgesList = new ArrayList<ResolveCatalogEdgeVO>(parentEdges);
                
                //sort it right here
                sortEdgesOnOrder(parentEdgesList);
    
                for (ResolveCatalogEdgeVO edgeVO : parentEdgesList)
                {
                    if (edgeVO.getCatalog().getSys_id().equalsIgnoreCase(catalog.getSys_id()) && edgeVO.getSourceNode().getSys_id().equalsIgnoreCase(parentVO.getSys_id()))
                    {
                        childrenSysIds.add(edgeVO.getDestinationNode().getSys_id());
                    }
                }
            }
            else
            {
                Log.log.info("No Edge. This is last node:" + parentVO.getUPath());
            }
    
            //get each children
            List<Catalog> children = new ArrayList<Catalog>();
            if (childrenSysIds.size() > 0)
            {
                int childOrder = 1;
                for (String childSysId : childrenSysIds)
                {
                    ResolveCatalogNodeVO childVO = getNode(childSysId, catalogNodesVO);
                    if(childVO != null)
                    {
	                    Catalog childCatalog = transformToCatalog(childVO, childOrder, catalogNodesVO); //**RECURSIVE
	    
	                    children.add(childCatalog);
	                    childOrder++;
                    }
                }//for loop
            }
            
            //add the children to the parent catalog
            parentCatalog.setChildren(children);
        }

        return parentCatalog;
    }

    private ResolveCatalogNodeVO getNode(String sysId, List<ResolveCatalogNodeVO> nodes)
    {
        ResolveCatalogNodeVO node = null;

        for (ResolveCatalogNodeVO vo : nodes)
        {
            if (vo.getSys_id().equalsIgnoreCase(sysId))
            {
                node = vo;
                break;
            }

        }

        return node;
    }

    private ResolveCatalogNodeVO findRoot(List<ResolveCatalogNodeVO> catalogNodesVO) throws Exception
    {
        ResolveCatalogNodeVO rootNode = null;

        //get the root node
        for (ResolveCatalogNodeVO node : catalogNodesVO)
        {
            if (node.getUIsRoot())
            {
                rootNode = node;
                break;
            }
        }//end of for loop

        if (rootNode == null)
        {
            throw new Exception("Bad Data !! This catalog does not have a Root node");
        }

        return rootNode;
    }

    private List<ResolveCatalogNodeVO> getCatalogNodes() throws Exception
    {
        List<ResolveCatalogNodeVO> result = new ArrayList<ResolveCatalogNodeVO>();

        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.executeNoCache(() -> {
            	 catalog = CatalogUtil.findCatalogModel(catalogSysId, name, username);
                 if (catalog != null)
                 {
                     Collection<ResolveCatalogNode> catalogNodes = catalog.getCatalogNodes();
                     if (catalogNodes == null)
                     {
                         throw new Exception("Bad Data !!! Catalog " + catalog.getUName() + " does not have nodes.");
                     }

                     for (ResolveCatalogNode catalogNode : catalogNodes)
                     {
                         catalogNode.getCatalogTagRels().size();
                         catalogNode.getCatalogWikiRels().size();
                         
                         ResolveCatalogNodeVO vo = catalogNode.doGetVO();
                         
                         result.add(vo);
                     }//end of for loop
                 }
            });
                   }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return result;
    }

    private static void sortEdgesOnOrder(List<ResolveCatalogEdgeVO> edges)
    {
        if (!edges.isEmpty())
        {
            Collections.sort(edges, new Comparator<ResolveCatalogEdgeVO>()
            {
                public int compare(final ResolveCatalogEdgeVO cat1, final ResolveCatalogEdgeVO cat2)
                {
                    return cat1.getUOrder() > cat2.getUOrder() ? 1 : -1;
                }
            });
        }
    }

    private Catalog transformVOToCatalog(ResolveCatalogNodeVO vo, int order)
    {
        Catalog c = new Catalog(vo);
        c.setOrder(order);
//        if(order == 1)
//        {
//            //this will be the root node
//            c.setRoot(true);
//        }

        //for reference nodes
        if (c.getType().equalsIgnoreCase(NodeType.CatalogReference.name()) && !c.isRoot() && StringUtils.isNotEmpty(vo.getUReferenceCatalogSysId()))
        {
            try
            {
                Catalog refCatalog = CatalogUtil.getCatalog(vo.getUReferenceCatalogSysId(), null, "admin");
                if (refCatalog != null)
                {
                    List<Catalog> childs = new ArrayList<Catalog>();
                    childs.add(refCatalog);

                    c.setChildren(childs);
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error in catalog references", e);
            }

        }

        return c;
    }
//    
//    private List<Tag> getTagsForNode(Collection<ResolveCatalogNodeTagRelVO> catalogTagRels)
//    {
//        List<Tag> tags = new ArrayList<Tag>();
//        
//        if(catalogTagRels != null && catalogTagRels.size() > 0)
//        {
//            for(ResolveCatalogNodeTagRelVO relVO : catalogTagRels)
//            {
//                ResolveTagVO tag = relVO.getTag();
//                
//                Tag t = new Tag();
//                t.setId(tag.getSys_id());
//                t.setName(tag.getUName());
//                
//                //add to the list
//                tags.add(t);
//            }//end of for loop
//        }
//        
//        return tags;
//        
//    }
//    
//    private List<WikiDocumentVO> getDocumentsForNode(Collection<ResolveCatalogNodeWikidocRelVO> rels)
//    {
//        List<WikiDocumentVO> docs = new ArrayList<WikiDocumentVO>();
//        
//        if(rels != null && rels.size() > 0)
//        {
//            for(ResolveCatalogNodeWikidocRelVO rel : rels)
//            {
//                docs.add(rel.getWikidoc());
//            }
//        }
//        
//        return docs;
//    }
//    
//    private String getDocumentNameForNode(Collection<ResolveCatalogNodeWikidocRelVO> rels)
//    {
//        String docName = "";
//        
//        if(rels != null && rels.size() > 0)
//        {
//            for(ResolveCatalogNodeWikidocRelVO rel : rels)
//            {
//                docName = rel.getWikidoc().getUFullname();
//                break;
//            }
//        }
//        
//        return docName;
//    }
    

}
