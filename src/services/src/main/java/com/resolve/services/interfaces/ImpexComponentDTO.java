/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.interfaces;

import java.util.List;

public class ImpexComponentDTO
{
    public static final String NEW_STATUS = "NEW";
    public static final String EXIST_SAME = "Exist, Same";
    public static final String EXIST_DIFFERENT = "Exist, Different";
    
    String type;//will NOT have NAMESPACE, TAG as they will be converted to DOCUMENT
    public String displayType;
    public String status;//NEW,SAME,CHANGE - used only for IMPORT
    public String tablename;//used for GLIDE components
    public String sys_id;
    public List<String> sys_ids;//used for DbVO which is a Glide component
    public String filename;//used for IMPORT
    public String operation;//DELETE OR INSERTORUPDATE , used for Import
    public Boolean exclude;
    public String error;
    public String name;
    public String fullName;
    public String module;
    public String group;
    public String defaultAction;
    
    public ImpexComponentDTO()
    {
        //set the defaults
        setExclude(false);
        setStatus("");
        setOperation("INSERT_OR_UPDATE");
        setDefaultAction("0");
    }//ImpexComponent
    
    public String getType()
    {
        return type;
    }
    public void setType(String type)
    {
        this.type = type;
    }
    public String getDisplayType()
    {
        return displayType;
    }
    public void setDisplayType(String displayType)
    {
        this.displayType = displayType;
    }
    public String getStatus()
    {
        return status;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }
    public String getTablename()
    {
        return tablename;
    }
    public void setTablename(String tablename)
    {
        this.tablename = tablename;
    }
    public String getSys_id()
    {
        return sys_id;
    }

    public void setSys_id(String sys_id)
    {
        this.sys_id = sys_id;
    }

    public List<String> getSys_ids()
    {
        return sys_ids;
    }
    public void setSys_ids(List<String> sys_ids)
    {
        this.sys_ids = sys_ids;
    }
    
    public String getFilename()
    {
        return filename;
    }
    public void setFilename(String filename)
    {
        this.filename = filename;
    }
    public String getOperation()
    {
        return operation;
    }
    public void setOperation(String operation)
    {
        this.operation = operation;
    }
   
    public Boolean getExclude()
    {
        return exclude;
    }
    public void setExclude(Boolean exclude)
    {
        this.exclude = exclude;
    }
    public String getError()
    {
        return error;
    }
    public void setError(String error)
    {
        this.error = error;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getModule()
    {
        return module;
    }

    public void setModule(String module)
    {
        this.module = module;
    }
    
    public void setGroup(String group)
    {
        this.group = group;
    }
    public String getGroup()
    {
        return this.group;
    }

    public String getDefaultAction()
    {
        return defaultAction;
    }

    public void setDefaultAction(String defaultAction)
    {
        this.defaultAction = defaultAction;
    }
    
    @Override
    public int hashCode()
    {
        int hashcode = 1;
        hashcode = 31 * hashcode + ((sys_id == null) ? (new java.util.Date().getTime() + "").hashCode() : sys_id.hashCode());
        return hashcode;
    }
    
    @Override
    public boolean equals (Object obj)
    {
        if (this == obj) return true;
        if (getClass() != obj.getClass()) return false;
        
        boolean value = false;
        
        ImpexComponentDTO other = (ImpexComponentDTO) obj;
        if (sys_id != null && other.sys_id != null)
        {
            value = sys_id.equalsIgnoreCase(other.sys_id);
        }
        else if (sys_ids != null && other.sys_ids != null)
        {
            value = sys_ids.toString().equalsIgnoreCase(other.sys_ids.toString());
        }
        return value;
    }
}
