/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.rb.util;

import com.resolve.util.StringUtils;

public class Params
{
    private String inputs; //renders <inputs><![CDATA[]]></inputs>
    private String outputs; //renders <outputs><![CDATA[]]></outputs>
    public Params(String inputs, String outputs)
    {
        this.inputs = inputs;
        this.outputs = outputs;
    }
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("      <params>\n");
        if(StringUtils.isBlank(inputs))
        {
            sb.append("        <inputs><![CDATA[{}]]>").append("</inputs>\n");
        }
        else
        {
            sb.append("        <inputs><![CDATA[{").append(inputs).append("}]]>").append("</inputs>\n");
        }
        if(StringUtils.isBlank(outputs))
        {
            sb.append("        <outputs><![CDATA[{}]]>").append("</outputs>\n");
        }
        else
        {
            sb.append("        <outputs><![CDATA[{").append(outputs).append("}]]>").append("</outputs>\n");
        }
        sb.append("      </params>");
        return sb.toString();
    }
}
