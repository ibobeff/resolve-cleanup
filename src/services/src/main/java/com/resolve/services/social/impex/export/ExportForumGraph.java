/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.social.impex.export;

import java.util.Collection;
import java.util.List;

import com.resolve.services.ServiceGraph;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.interfaces.ImpexOptionsDTO;
import com.resolve.services.social.impex.GraphRelationshipDTO;
import com.resolve.services.social.impex.SocialImpexVO;
import com.resolve.services.vo.social.NodeType;

public class ExportForumGraph extends ExportComponentGraph
{

    public ExportForumGraph(String sysId, ImpexOptionsDTO options) throws Exception
    {
       super(sysId, options, NodeType.FORUM);
    }
    
    @Override
    protected List<GraphRelationshipDTO> exportRelationships(ResolveNodeVO compNode, SocialImpexVO socialImpex) throws Exception
    {
        Collection<ResolveNodeVO> nodesThatThisForumContains = ServiceGraph.getNodesFollowingMe(compNode.getSys_id(), null, null, null, "admin");
        if (nodesThatThisForumContains != null)
        {
            for (ResolveNodeVO node : nodesThatThisForumContains)
            {
                NodeType type = node.getUType();
                if (type == NodeType.USER)
                {
                    addRelationship(compNode, node);
                    prepareReturnNode(node, socialImpex);
                }
            }
        }
        
        return relationships;
    }
    
    private void prepareReturnNode(ResolveNodeVO anyNode, SocialImpexVO socialImpex) throws Exception
    {
        if (options != null && options.getProcessUsers())
        {
            socialImpex.addReturnUser(new RSComponent(anyNode));
        }
        
    }
}
