/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import com.resolve.services.interfaces.VO;


public class ResolveActionTaskDTO  extends VO
{
    private static final long serialVersionUID = 3873125466087783924L;
    
    private String id;
    private String UName;
    private String UNamespace;
    private String UFullName;
 	private String UTypes;
    private Boolean ULogresult;
    private String USummary;
    private String UDescription;
    private String URoles;//this will be same as UExecuteRoles
    private Boolean UActive;
    private String UMenuPath;
    private Boolean UIsDeleted;
    private Boolean UIsDefaultRole;

    // transient
    private String UReadRoles;
    private String UWriteRoles;
    private String UAdminRoles;
    private String UExecuteRoles;

    public ResolveActionTaskDTO()
    {
    }

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUName()
    {
        return UName;
    }

    public void setUName(String uName)
    {
        UName = uName;
    }

    public String getUNamespace()
    {
        return UNamespace;
    }

    public void setUNamespace(String uNamespace)
    {
        UNamespace = uNamespace;
    }

    public String getUFullName()
    {
        return UFullName;
    }

    public void setUFullName(String uFullName)
    {
        UFullName = uFullName;
    }

    public String getUTypes()
    {
        return UTypes;
    }

    public void setUTypes(String uTypes)
    {
        UTypes = uTypes;
    }

    public Boolean getULogresult()
    {
        return ULogresult;
    }

    public void setULogresult(Boolean uLogresult)
    {
        ULogresult = uLogresult;
    }

    public String getUSummary()
    {
        return USummary;
    }

    public void setUSummary(String uSummary)
    {
        USummary = uSummary;
    }

    public String getUDescription()
    {
        return UDescription;
    }

    public void setUDescription(String uDescription)
    {
        UDescription = uDescription;
    }

    public String getURoles()
    {
        return URoles;
    }

    public void setURoles(String uRoles)
    {
        URoles = uRoles;
    }

    public Boolean getUActive()
    {
        return UActive;
    }

    public void setUActive(Boolean uActive)
    {
        UActive = uActive;
    }

    public String getUMenuPath()
    {
        return UMenuPath;
    }

    public void setUMenuPath(String uMenuPath)
    {
        UMenuPath = uMenuPath;
    }

    public Boolean getUIsDeleted()
    {
        return UIsDeleted;
    }

    public void setUIsDeleted(Boolean uIsDeleted)
    {
        UIsDeleted = uIsDeleted;
    }

    public Boolean getUIsDefaultRole()
    {
        return UIsDefaultRole;
    }

    public void setUIsDefaultRole(Boolean uIsDefaultRole)
    {
        UIsDefaultRole = uIsDefaultRole;
    }

    public String getUReadRoles()
    {
        return UReadRoles;
    }

    public void setUReadRoles(String uReadRoles)
    {
        UReadRoles = uReadRoles;
    }

    public String getUWriteRoles()
    {
        return UWriteRoles;
    }

    public void setUWriteRoles(String uWriteRoles)
    {
        UWriteRoles = uWriteRoles;
    }

    public String getUAdminRoles()
    {
        return UAdminRoles;
    }

    public void setUAdminRoles(String uAdminRoles)
    {
        UAdminRoles = uAdminRoles;
    }

    public String getUExecuteRoles()
    {
        return UExecuteRoles;
    }

    public void setUExecuteRoles(String uExecuteRoles)
    {
        UExecuteRoles = uExecuteRoles;
    }

    
    
} // ResolveActionTask
