/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.util.Collection;
import java.util.Set;

import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.WikiAttachment;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.services.ServiceWiki;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.util.WikiAttachmentUtil;
//import com.resolve.services.hibernate.util.WikiTemplateUtil;
import com.resolve.services.hibernate.util.WikiUtils;
//import com.resolve.services.hibernate.vo.WikiDocumentMetaFormRelVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.StringUtils;

public class CopyHelper
{
    private String fromDocument = null; 
    private String toDocument = null;
    private String username = null;
    private String fromDocResolutionBuilderId = null;
    private WikiDocument fromDoc;
    
    //switches
    private boolean overwrite = false; 
    private boolean validateUser = true;
    private boolean copyAttachments = true;
    private boolean userShouldFollow = false; //default set to false for the copy operation
    private boolean isCreateWikiDocFromTemplate = false; //default:false, true for create wikidoc from template
    
    public CopyHelper(String fromDocument, String toDocument, String username) throws Exception
    {
        if(StringUtils.isEmpty(username) || StringUtils.isEmpty(toDocument) || StringUtils.isEmpty(fromDocument))
        {
            throw new RuntimeException("Username, From and To Document names are mandatory");
        }
        
        //check if the names are not same
        if (fromDocument.equalsIgnoreCase(toDocument))
        {
            throw new RuntimeException("Old and New Document '" + toDocument + "' have the same name.");
        }
        
        //check for the doc name if its correct
        WikiUtils.validateWikiDocName(toDocument);
        
        
        this.fromDocument = fromDocument;
        this.toDocument = toDocument;
        this.username = username;
    }
    
    public CopyHelper(WikiDocument fromDoc, String toDocument, String username) throws Exception
    {
    	this.fromDoc = fromDoc;
    	this.toDocument = toDocument;
    	this.fromDocument = fromDoc.getUFullname();
    	this.username = username;
    	
    	if(StringUtils.isEmpty(username) || StringUtils.isEmpty(toDocument) || fromDoc == null)
        {
            throw new RuntimeException("Username, Original wikidic and To Document name are mandatory");
        }
    }

    public boolean isOverwrite()
    {
        return overwrite;
    }

    public void setOverwrite(boolean overwrite)
    {
        this.overwrite = overwrite;
    }

    public boolean isValidateUser()
    {
        return validateUser;
    }

    public void setValidateUser(boolean validateUser)
    {
        this.validateUser = validateUser;
    }
    
    public boolean isCopyAttachments()
    {
        return copyAttachments;
    }

    public void setCopyAttachments(boolean copyAttachments)
    {
        this.copyAttachments = copyAttachments;
    }
    
    public boolean isUserShouldFollow()
    {
        return userShouldFollow;
    }

    public void setUserShouldFollow(boolean userShouldFollow)
    {
        this.userShouldFollow = userShouldFollow;
    }

    public boolean isCreateWikiDocFromTemplate()
    {
        return isCreateWikiDocFromTemplate;
    }
    
    public void setIsCreateWikiDocFromTemplate(boolean isCreateWikiDocFromTemplate)
    {
        this.isCreateWikiDocFromTemplate = isCreateWikiDocFromTemplate;
    }
    
    public WikiDocumentVO copy() throws Exception
    {
        copyWiki();
        
        //get the new wiki created and return
        WikiDocumentVO newWiki = ServiceWiki.getWikiDocWithGraphRelatedData(null, toDocument, username);
        return newWiki;
        
    }
    
    public String getFromDocResolutionBuilderId()
    {
        return fromDocResolutionBuilderId;
    }
    
    //private apis
    private void copyWiki() throws Exception
    {
        //check for madatory fields
        if(StringUtils.isEmpty(fromDocument) || StringUtils.isEmpty(toDocument) || StringUtils.isEmpty(username))
        {
            throw new RuntimeException("From and To document names are mandatory.");
        }
        
        //check if the names are not same
        if (fromDocument.equalsIgnoreCase(toDocument))
        {
        	if (isOverwrite())
            {
                SaveHelper saveHelper = new SaveHelper(fromDoc, username);
                saveHelper.save();
                return;
            }
            else
            {
                throw new RuntimeException("Old and New Document '" + toDocument + "' have the same name.");
            }
        }
        
        //check if the new file exist
        WikiDocument toDoc = WikiUtils.getWikiDocumentModel(null,toDocument, username, false);
        if(toDoc != null && !overwrite)
        {
            String errorMessage = null;
            if (toDoc.getUFullname().startsWith("Template."))
            {
                /**
                 * Documents generated by wiki template has Template as their namespace. Most likely, this copy operation originated from there.
                 * And in template we don't have skip or overwrite flag. So, the message is different.
                 */
                errorMessage = "Document " + toDocument + " already exist";
            }
            else
            {
                errorMessage = "Document " + toDocument + " already exist and the flag to overwrite is set to false. So aborting the copying process.";
            }
            throw new Exception(errorMessage);
        }
        
        /*
         * fromDoc is populated from wiki 'Save As...' operation. If it's not, it must be copy operation.
         */
        if (fromDoc == null)
        {
        	fromDoc = WikiUtils.getWikiDocumentModel(null,fromDocument, username, true);
        }
        
        if(fromDoc == null)
        {
            throw new Exception("Document " + fromDocument + " does not exist. So there is nothing to copying from. Aborting.");
        }
        
        fromDocResolutionBuilderId = fromDoc.getUResolutionBuilderId();
        
        //validate if the user has rights to copy this document or not, only users with admin roles can do this operation
        //this is true when there is only 1 document to rename
        if(isValidateUser())
        {
            Set<String> userRoles = UserUtils.getUserRoles(username);
            String docRoles = fromDoc.getAccessRights().getUAdminAccess();
            boolean hasRights = com.resolve.services.util.UserUtils.hasRole(username, userRoles, docRoles);
            if(!hasRights)
            {
                throw new Exception("User " + username + " does not have role on the document " + fromDocument + " for Rename/Move operation.");
            }
        }
        
        //delete the toDocument if it exist
        if(toDoc != null)
        {
            DeleteHelper.deleteWikiDocument(null, toDocument, username);
        }
        
        //create a copy and save it
        toDoc = buildWikiDocToCopy(fromDoc, toDocument, isOverwrite(), username);
        SaveHelper saveHelper = new SaveHelper(toDoc, username);
        saveHelper.setUserShouldFollow(isUserShouldFollow());
        saveHelper.save();

        //if we need to copy attachments to the doc, do it now
        if(copyAttachments)
        {
            copyAttachments(this.fromDocument, this.toDocument);
        }
    }
    
    
    private WikiDocument buildWikiDocToCopy(WikiDocument doc, String toDocument, boolean force, String username)
    {
        /*String templateName = null;
        
        if (isCreateWikiDocFromTemplate())
        {
            templateName = doc.getUName();
        }*/
        
        //update the new name
        String[] arrNameNamespace = toDocument.split("\\.");
        doc.setUFullname(toDocument);
        doc.setUNamespace(arrNameNamespace[0]);
        doc.setUName(arrNameNamespace[1]);
        
        //update the values to create a new obj
        doc.setSys_id(null);//making it a new insert
        doc.setSysUpdatedBy(null);
        doc.setSysCreatedBy(null);
        doc.setSysUpdatedOn(null);
        doc.setSysCreatedOn(null);
        doc.setWikidocAttachmentRels(null);//handling attachments seperately
        doc.setWikidocStatistics(null);
        doc.setRoleWikidocHomepageRels(null);
        doc.setWikidocQualityRating(null);
        doc.setWikidocRatingResolution(null);
        doc.setUIsHidden(false);
        doc.setUIsDeleted(false);
        doc.setUIsActive(true);
        doc.setUIsLocked(false);
        doc.setUVersion(0);
        doc.setUResolutionBuilderId(null); // Set the resolution builder id to null to create new RB General record for new doc
        
        //nullify the tags as they are in the graph now
        doc.setWikidocResolveTagRels(null);
        
        //update the title to the document name.
        doc.setUTitle(toDocument);
        
        String newContent = DecisionTreeHelper.removeDtSectionsInContent(doc.getUContent(), null);
        doc.setUContent(newContent);
        
        doc.setUSIRRefCount(VO.NON_NEGATIVE_LONG_DEFAULT);
        
        AccessRights ar = null;
        
        /*
        if (isCreateWikiDocFromTemplate() && StringUtils.isNotBlank(templateName))
        {
            WikiDocumentMetaFormRelVO wikiDocMetaFormRel = 
                WikiTemplateUtil.findWikiTemplateByIdOrByName(null, templateName, username);
            
            if (wikiDocMetaFormRel != null)
            {
                ar = new AccessRights();
                ar.applyVOToModel(wikiDocMetaFormRel.getAccessRights());
            }
        }*/
        
        if (ar == null)
        {
            ar = doc.getAccessRights();
        }
        
        AccessRights newAr = new AccessRights();
        if(ar != null)
        {
            newAr.setUAdminAccess(ar.getUAdminAccess());
            newAr.setUExecuteAccess(ar.getUExecuteAccess());
            newAr.setUReadAccess(ar.getUReadAccess());
            newAr.setUWriteAccess(ar.getUWriteAccess());
        }  
        else
        {
            newAr.setUAdminAccess("admin");
            newAr.setUExecuteAccess("admin");
            newAr.setUReadAccess("admin");
            newAr.setUWriteAccess("admin");
        }
        doc.setAccessRights(newAr);
        
        return doc;
    }
    
    private void copyAttachments(String fromDoc, String toDoc) throws Exception
    {
        Collection<WikiAttachment> attachments = WikiAttachmentUtil.prepareAttachment(fromDoc, username);
        if(attachments != null)
        {
            AttachmentHelper attachHelper = new AttachmentHelper(null, toDoc, null, attachments, username);
            attachHelper.attach();
        }
    }
    
   
}
