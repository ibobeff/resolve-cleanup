/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.social.follow;

import java.util.Collection;
import java.util.Set;

import com.resolve.services.ServiceGraph;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;

/**
 * get list of components for a Process
 * 
 * @author jeet.marwah
 *
 */
public class ProcessFollow extends ContainerFollow
{

    public ProcessFollow(String processSysId, String processName, String username) throws Exception
    {
        super(ServiceGraph.findNode(null, processSysId, processName, NodeType.PROCESS, username), username);
    }

    @Override
    public Set<RSComponent> getStreams() throws Exception
    {
        Collection<ResolveNodeVO> nodesThatThisProcessContains = ServiceGraph.getNodesFollowedBy(rootNode.getSys_id(), null, null, null, username);
        if (nodesThatThisProcessContains != null)
        {
            for (ResolveNodeVO nodeFollowingMe : nodesThatThisProcessContains)
            {
               NodeType nodeFollowingMeType = nodeFollowingMe.getUType();
               if(nodeFollowingMeType == NodeType.TEAM)
               {
                   streams.addAll(convertNodeToRsComponent(nodeFollowingMe, isDirectFollow()));
                   
                   if(isRecurse())
                   {
                       TeamFollow searchChildTeam = new TeamFollow(nodeFollowingMe.getUCompSysId(), nodeFollowingMe.getUCompName(), username);
                       searchChildTeam.setDirectFollow(false);
                       searchChildTeam.setIgnoreUsers(true);
                       searchChildTeam.setRecurse(true);
                       
                       streams.addAll(searchChildTeam.getStreams());
                   }
                   
               }
//               else if(nodeFollowingMeType == NodeType.USER)
//               {
//                   if(!isIgnoreUsers())
//                   {
//                       streams.addAll(convertNodeToRsComponent(nodeFollowingMe, isDirectFollow()));
//                   }
//               }   
               else
               {
                   streams.addAll(convertNodeToRsComponent(nodeFollowingMe, isDirectFollow()));
               }

            }//end of for loop
        }
        
        //get the Users in a Team 
        if(!isIgnoreUsers())
        {
            Collection<ResolveNodeVO> nodesFollowingMe = ServiceGraph.getNodesFollowingMe(rootNode.getSys_id(), null, null, null, username);
            if (nodesFollowingMe != null)
            {
                for (ResolveNodeVO nodeFollowingMe : nodesFollowingMe)
                {
                    NodeType nodeFollowingMeType = nodeFollowingMe.getUType();

                    //if this a USER node, then add to the list 
                    if (nodeFollowingMeType == NodeType.USER)
                    {
                        streams.addAll(convertNodeToRsComponent(nodeFollowingMe, isDirectFollow()));
                    }
                }
            }
        }
        
        return streams;
    }

}
