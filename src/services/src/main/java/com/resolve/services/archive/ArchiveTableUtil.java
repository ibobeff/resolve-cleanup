/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.archive;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.resolve.persistence.model.ArchiveActionResult;
import com.resolve.persistence.model.ArchiveWorksheet;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ArchiveTableUtil
{
    public static Set<String> getAllArchiveTaskSysIdsForProblemId(String problemId)
    {
        Set<String> atNames = new HashSet<String>();

        if(StringUtils.isNotEmpty(problemId))
        {
            try
            {
              HibernateProxy.setCurrentUser("system");
                HibernateProxy.execute(() -> {
                	 // init problem
                    ArchiveWorksheet archiveProblem = HibernateUtil.getDAOFactory().getArchiveWorksheetDAO().findById(problemId);
                    if(archiveProblem != null)
                    {
                        Collection<ArchiveActionResult> archiveActionResults = archiveProblem.getArchiveActionResults();
                        if(archiveActionResults != null )
                        {
                            for(ArchiveActionResult archiveActionResult : archiveActionResults)
                            {
                                atNames.add(archiveActionResult.getTask().getSys_id());
                            }
                        }
                    }
                });
                
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }

        return atNames;
        
    }//getAllArchiveTaskForProblemSys_id

}
