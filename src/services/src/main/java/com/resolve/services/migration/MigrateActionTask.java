package com.resolve.services.migration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.actiontask.ActiontaskCleanupUtil;
import com.resolve.services.hibernate.util.ActionTaskUtil;
import com.resolve.services.hibernate.util.AssessorUtil;
import com.resolve.services.hibernate.util.ParserUtil;
import com.resolve.services.hibernate.util.PreprocessorUtil;
import com.resolve.services.hibernate.vo.ResolveActionInvocVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolveAssessVO;
import com.resolve.services.hibernate.vo.ResolveParserVO;
import com.resolve.services.hibernate.vo.ResolvePreprocessVO;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class MigrateActionTask
{
    public static final String ASSESSOR = "assess";
    public static final String PARSER = "parser";
    public static final String PREPROCESS = "preprocess";    
    
    private static ExecutorService executor = null;
    private static List<Future<Boolean>> futures = null;
    
    public static Set<String> assessNames = null;
    public static Set<String> parserNames = null;
    public static Set<String> preprocessNames = null;
    
    public static void migrate(int batchLoadSize) {
        
        Log.log.info("======= Start migration of Action Task components ========");
        Log.log.info("");
        long startAt = System.currentTimeMillis();
        
        removeSharedLinks(batchLoadSize);
        removeNameInconsistency();
        removeDanglingComponents();
        
        long endAt = System.currentTimeMillis();
        
        Log.log.info("Migration is completed in " + (endAt - startAt)/100 + " seconds");
        Log.log.info("");
        
        Log.log.info("======= Finish migration of Action Task components ========");
        Log.log.info("");
    }
    
    public static void removeSharedLinks(int batchLoadSize) 
    {
        Log.log.info("======= Start to remove shared component links ========");
        long startAt = System.currentTimeMillis();
        
        try  {
            executor = Executors.newFixedThreadPool(batchLoadSize);
            boolean isCompleted = false;
            
            ActiontaskCleanupUtil.updateActionTaskInvoDescription();
                            
            List<String> assessTaskNames = findActionTasksWithCommonAssess();
            List<String> parserTaskNames = findActionTasksWithCommonParser();
            List<String> preprocessTaskNames = findActionTasksWithCommonPreprocess();
            
            assessNames = findComponentNames(ASSESSOR);
            parserNames = findComponentNames(PARSER);
            preprocessNames = findComponentNames(PREPROCESS);
            
            Log.log.info("======= Starting to remove shared Assessor links ========");
            isCompleted = updateActionTask(assessTaskNames, batchLoadSize, ASSESSOR);
            if(!isCompleted)
                Log.log.warn("Removing shared Assessor links may not have completed properly.");
            Log.log.info("======= Finished removing shared Assessor links ========");
            Log.log.info("");
            
            Log.log.info("======= Starting to remove shared Parsers links ========");
            isCompleted = updateActionTask(parserTaskNames, batchLoadSize, PARSER);
            if(!isCompleted) 
                Log.log.warn("Removing shared Parsers links may not have completed properly.");
            Log.log.info("======= Finished removing shared Parsers links ========");
            Log.log.info("");
            
            Log.log.info("======= Starting to remove shared Preprocessors links ========");
            isCompleted = updateActionTask(preprocessTaskNames, batchLoadSize, PREPROCESS);
            if(!isCompleted)
                Log.log.warn("Removing shared Preprocessors links may not have completed properly.");
            Log.log.info("======= Finished removing shared Preprocessors links ========");
            Log.log.info("");
        } catch(Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
            Log.log.error("Removing shared component links process did not completed properly. Please try again.");
        } finally {
            try {
            if(executor != null)
                executor.shutdown();
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        long endAt = System.currentTimeMillis();
        
        Log.log.info("Removing shared component links is completed in " + (endAt - startAt)/100 + " seconds");
        
        Log.log.info("======= Finish removing shared component links of Action Task components ========");
        Log.log.info("");

    } // removeSharedLinks()
    
    public static void removeNameInconsistency() 
    {
        Log.log.info("======= Start to remove inconsistent names ========");
        Log.log.info("");
        
        long startAt = System.currentTimeMillis();
        
        try  {
            assessNames = findComponentNames(ASSESSOR);
            parserNames = findComponentNames(PARSER);
            preprocessNames = findComponentNames(PREPROCESS);
            
            Log.log.info("======= Starting removing inconsistent names of Assessors ========");
            
            long start = System.currentTimeMillis();
            
            Map<String, String> assessTaskNames = findActionTasksWithDifferentComponentNames(ASSESSOR);
            createNewComponentNames(assessTaskNames, ASSESSOR);
            
            assessTaskNames = findActionTasksWithDifferentComponentNames(ASSESSOR);
            deleteAndRenameComponents(assessTaskNames, ASSESSOR);
            
            long end = System.currentTimeMillis();
            
            Log.log.info("======= Finished removing inconsistent names of Assessors in " + (end - start)/100 + " seconds ========");
            Log.log.info("");
            
            Log.log.info("======= Starting removing inconsistent names of Parsers ========");
            
            start = System.currentTimeMillis();
            
            Map<String, String> parserTaskNames = findActionTasksWithDifferentComponentNames(PARSER);
            createNewComponentNames(parserTaskNames, PARSER);
            
            parserTaskNames = findActionTasksWithDifferentComponentNames(PARSER);
            deleteAndRenameComponents(parserTaskNames, PARSER);
            
            end = System.currentTimeMillis();
            
            Log.log.info("======= Finished removing inconsistent names of Parsers in " + (end - start)/100 + " seconds ========");
            Log.log.info("");
            
            Log.log.info("======= Starting removing inconsistent names of Preprocessors ========");

            start = System.currentTimeMillis();
            
            Map<String, String> preprocessTaskNames = findActionTasksWithDifferentComponentNames(PREPROCESS);
            createNewComponentNames(preprocessTaskNames, PREPROCESS);

            preprocessTaskNames = findActionTasksWithDifferentComponentNames(PREPROCESS);
            deleteAndRenameComponents(preprocessTaskNames, PREPROCESS);
            
            end = System.currentTimeMillis();
            Log.log.info("======= Finished removing inconsistent names of Preprocessors in " + (end - start)/100 + " seconds ========");
            Log.log.info("");
        } catch(Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
            Log.log.error("Removing inconsistent names did not completed properly. Please try again.");
        } 
        
        long endAt = System.currentTimeMillis();
        
        Log.log.info("Removing inconsistent names is completed in " + (endAt - startAt)/100 + " seconds");
        
        Log.log.info("======= Finish removing inconsistent names of Action Task components ========");
        Log.log.info("");

    } // removeNameInconsistency()
    
    public static void removeDanglingComponents() 
    {
        Log.log.info("======= Start to remove dangling components ========");
        Log.log.info("");
        
        long startAt = System.currentTimeMillis();
        
        try  {
            assessNames = findComponentNames(ASSESSOR);
            parserNames = findComponentNames(PARSER);
            preprocessNames = findComponentNames(PREPROCESS);
            
            Log.log.info("======= Starting removing dangling components of Assessors ========");
            
            List<String> assessIds = findDanglingComponents(ASSESSOR);
            deleteDanglingComponents(assessIds, ASSESSOR);
            
            Log.log.info("======= Finished removing dangling components of Assessors ========");
            Log.log.info("");
            
            Log.log.info("======= Starting removing dangling components of Parsers ========");
            
            List<String> parserIds = findDanglingComponents(PARSER);
            deleteDanglingComponents(parserIds, PARSER);
            Log.log.info("======= Finished removing dangling components of Parsers ========");
            Log.log.info("");
            
            Log.log.info("======= Starting removing dangling components of Preprocessors ========");

            List<String> preprocessIds = findDanglingComponents(PREPROCESS);
            deleteDanglingComponents(preprocessIds, PREPROCESS);
            
            Log.log.info("======= Finished removing dangling components of Preprocessors ========");
            Log.log.info("");
        } catch(Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
            Log.log.error("Removing dangling components did not completed properly. Please try again.");
        } finally {
            try {
            if(executor != null)
                executor.shutdown();
            } catch(Exception e) {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        long endAt = System.currentTimeMillis();
        
        Log.log.info("Removing inconsistent names is completed in " + (endAt - startAt)/100 + " seconds");
        
        Log.log.info("======= Finish dangling components of Action Task components ========");
        Log.log.info("");

    } // removeDanglingComponents()
    
    private static boolean isUpdateCompleted(String type) {

        boolean completed = true;
        int size = futures.size();
        int i=0;

        Log.log.info(type + " to be migrated = " + size);
        
        if(size != 0) {
            for(i=0; i<size; i++) {
                Future<Boolean> future = futures.get(i);
                
                while(!future.isDone()) {
                    try {
                        boolean result = future.get();
                        completed = completed & result;
                        break;
                    } catch(Exception e) {
                        completed = false;
                        Log.log.error(e.getMessage(), e);
                        break;
                    }
                }
            }
        }

        Log.log.info(type + " migrated = " + i);
        Log.log.info(type + " completed = " + completed);
        
        return completed;
    }
    
    private static boolean updateActionTask(List<String> taskNames, int batchLoadSize, String type) {
        
        boolean isCompleted = true;
        List<ResolveActionTaskVO> actionTaskList = null;
        
        futures = new ArrayList<Future<Boolean>>();
        
        try {           
            if(taskNames != null) {
                int size = taskNames.size();
                int batchSize = batchLoadSize;
                Log.log.debug("size = " + size);
                for(int i=0; i<size/batchLoadSize+1; i++) {
                    if(size - i*batchLoadSize < batchLoadSize)
                        batchSize = size - i*batchLoadSize;
                    Log.log.debug("i = " + i);
                    Log.log.debug("batchSize = " + batchSize);
                    actionTaskList = new ArrayList<ResolveActionTaskVO>();
                    
                    for(int j=0; j<batchSize; j++) {
                        String fullName = taskNames.get(i*batchLoadSize+j);
                        Log.log.debug("fullName = " + fullName);
                        ResolveActionTaskVO vo =  ActionTaskUtil.getActionTaskWithReferences(null, fullName, "admin");
                        if(vo != null) {
                            Log.log.debug("fullName = " + fullName);
                            actionTaskList.add(vo);
                        }
                    }
                    
                    if(actionTaskList != null)
                        handleBatch(actionTaskList, type);
                } // end of for(i)
            }
        } catch(Exception e) {
            e.printStackTrace();
            Log.log.error("Failed to start migration for the following reason: ");
            Log.log.error(e.getMessage());
            isCompleted = false;
        }
        
        if(isCompleted)
            isCompleted = isUpdateCompleted(type);
        
        return isCompleted;
    } // updateActionTask()
    
    private static void createNewComponentNames(Map<String, String> componentTaskNames, String type) throws Exception {
        
        Set<String> actionTaskNames = componentTaskNames.keySet();
        
        for(Iterator it=actionTaskNames.iterator(); it.hasNext();) {
            String actionTaskName = (String)it.next();
            String componentName = (String)componentTaskNames.get(actionTaskName);
            
            switch(type) {
                case ASSESSOR:
                    // if the assessor with the same name of the action task name exists and is used by any other action task
                    // then create a new assessor with a unique name and link to the current action task
                    if(componentTaskNames.containsValue(actionTaskName)) {
                        String conflictActionTaskName = findConflictActionTaskName(actionTaskName, type);
                        ResolveActionTaskVO conflictActionTask =  ActionTaskUtil.getActionTaskWithReferences(null, conflictActionTaskName, "admin");
                        ResolveAssessVO currentAssess = AssessorUtil.findResolveAssessNoCache(null, actionTaskName, "admin");
                        AssessorUtil.copyAssessor(conflictActionTask, currentAssess, "admin");
                    }
                    
                    break;
                    
                case PARSER:
                    // if the parser with the same name of the action task name exists and is used by any other action task
                    // then create a new parser with a unique name and link to the current action task
                    if(componentTaskNames.containsValue(actionTaskName)) {
                        String conflictActionTaskName = findConflictActionTaskName(actionTaskName, type);
                        ResolveActionTaskVO conflictActionTask =  ActionTaskUtil.getActionTaskWithReferences(null, conflictActionTaskName, "admin");
                        ResolveParserVO currentParser = ParserUtil.findResolveParserNoCache(null, actionTaskName, "admin");
                        ParserUtil.copyParser(conflictActionTask, currentParser, "admin");
                    }
                    
                    break;
                    
                case PREPROCESS:
                    // if the preprocessor with the same name of the action task name exists and is used by any other action task
                    // then create a new preprocessor with a unique name and link to the current action task
                    if(componentTaskNames.containsValue(actionTaskName)) {
                        String conflictActionTaskName = findConflictActionTaskName(actionTaskName, type);
                        ResolveActionTaskVO conflictActionTask =  ActionTaskUtil.getActionTaskWithReferences(null, conflictActionTaskName, "admin");
                        ResolvePreprocessVO currentPreprocess = PreprocessorUtil.findResolvePreprocessNoCache(null, actionTaskName, "admin");
                        PreprocessorUtil.copyPreprocessor(conflictActionTask, currentPreprocess, "admin");
                    }
                    
                    break;
                    
                default:
                    break;
            } // switch()
            
        } // for()

    } // createNewComponentNames()
    
    private static void deleteAndRenameComponents(Map<String, String> componentTaskNames, String type) throws Exception {
        
        Set<String> actionTaskNames = componentTaskNames.keySet();
        
        for(Iterator it=actionTaskNames.iterator(); it.hasNext();) {
            String actionTaskName = (String)it.next();
            String componentName = (String)componentTaskNames.get(actionTaskName);
            
            if(StringUtils.isBlank(actionTaskName))
                continue;
            
            switch(type) {
                case ASSESSOR:
                    // if the assessor with the same name of the action task name never exists
                    // then change current assessor name to the action task name
                    if(!assessNames.contains(actionTaskName)) {
                        ResolveAssessVO currentAssess = AssessorUtil.findResolveAssessNoCache(null, componentName, "admin");
                        
                        if(currentAssess == null) {
                            Log.log.error("Assessor not found: " + componentName);
                            continue;
                        }
                        
                        currentAssess.setUName(actionTaskName);
                        AssessorUtil.saveResolveAssess(currentAssess, "admin");
                    }
                    
                    // if the assessor with the same name of the action task name exists, but not used by any other action task
                    // then delete the existing assessor, and rename the current assessor to the action task name
                    else {
                        ResolveAssessVO currentAssess = AssessorUtil.findResolveAssessNoCache(null, componentName, "admin");
                        
                        if(currentAssess == null) {
                            Log.log.error("Assessor not found: " + componentName);
                            continue;
                        }
                        
                        ResolveAssessVO danglingAssess = AssessorUtil.findResolveAssessNoCache(null, actionTaskName, "admin");
                        
                        if(danglingAssess == null) {
                            Log.log.error("Assessor not found: " + actionTaskName);
                            continue;
                        }
                        
                        currentAssess.setUName(actionTaskName);
                        AssessorUtil.replaceResolveAssess(danglingAssess, currentAssess, "admin");
                    }
                    
                    break;
                    
                case PARSER:
                    // if the parser with the same name of the action task name never exists
                    // then change current assessor name to the action task name
                    if(!parserNames.contains(actionTaskName)) {
                        ResolveParserVO currentParser = ParserUtil.findResolveParserNoCache(null, componentName, "admin");
                        
                        if(currentParser == null) {
                            Log.log.error("Parser not found: " + componentName);
                            continue;
                        }
                        
                        currentParser.setUName(actionTaskName);
                        ParserUtil.saveResolveParser(currentParser, "admin");
                    }
                    
                    // if the assessor with the same name of the action task name exists, but not used by any other action task
                    // then delete the existing assessor, and rename the current assessor to the action task name
                    else {
                        ResolveParserVO currentParser = ParserUtil.findResolveParserNoCache(null, componentName, "admin");
                        
                        if(currentParser == null) {
                            Log.log.error("Parser not found: " + componentName);
                            continue;
                        }
                        
                        ResolveParserVO danglingParser = ParserUtil.findResolveParserNoCache(null, actionTaskName, "admin");
                        
                        if(danglingParser == null) {
                            Log.log.error("Parser not found: " + actionTaskName);
                            continue;
                        }
                        
                        currentParser.setUName(actionTaskName);
                        ParserUtil.replaceResolveParser(danglingParser, currentParser, "admin");
                    }
                    
                    break;
                    
                case PREPROCESS:
                    // if the preprocessor with the same name of the action task name never exists
                    // then change current preprocessor name to the action task name
                    if(!preprocessNames.contains(actionTaskName)) {
                        ResolvePreprocessVO currentPreprocess = PreprocessorUtil.findResolvePreprocessNoCache(null, componentName, "admin");
                        
                        if(currentPreprocess == null) {
                            Log.log.error("Preprocessor not found: " + componentName);
                            continue;
                        }
                        
                        currentPreprocess.setUName(actionTaskName);
                        PreprocessorUtil.saveResolvePreprocess(currentPreprocess, "admin");
                    }
                    
                    // if the assessor with the same name of the action task name exists, but not used by any other action task
                    // then delete the existing assessor, and rename the current assessor to the action task name
                    else {
                        ResolvePreprocessVO currentPreprocess = PreprocessorUtil.findResolvePreprocessNoCache(null, componentName, "admin");
                        
                        if(currentPreprocess == null) {
                            Log.log.error("Preprocessor not found: " + componentName);
                            continue;
                        }
                        
                        ResolvePreprocessVO danglingPreprocess = PreprocessorUtil.findResolvePreprocessNoCache(null, actionTaskName, "admin");
                        
                        if(danglingPreprocess == null) {
                            Log.log.error("Preprocessor not found: " + actionTaskName);
                            continue;
                        }
                        
                        currentPreprocess.setUName(actionTaskName);
                        PreprocessorUtil.replaceResolvePreprocess(danglingPreprocess, currentPreprocess, "admin");
                    }
                    
                    break;
                    
                default:
                    break;
            } // switch()
            
        } // for()
        
    } // deleteAndRenameComponents()
    
    private static void deleteDanglingComponents(List<String> componentIds, String type) throws Exception {
            
        SQLConnection conn = null;
        Statement stmt = null;
        
        try
        {
            conn = SQL.getConnection();
            
            int size = componentIds.size();
            if(size == 0) return;
            
            for(int i=0; i<size/100+1; i++) {
                stmt = conn.createStatement();
                
                for(int j=0; j<size%100; j++) {
                    String componentId = componentIds.get(i*100+j);
                    String sql = "DELETE FROM resolve_" + type + " WHERE sys_id='" + componentId + "'";
                    Log.log.debug(sql);
                    stmt.addBatch(HibernateUtil.getValidQuery(sql));
                }
                
                stmt.executeBatch();
            }
        } finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }
    } // deleteDanglingComponents()
    
    private static void removeBrokenReference(String sysId, String type) {
        
        SQLConnection conn = null;
        PreparedStatement psts = null;
        
        try
        {
            conn = SQL.getConnection();
            
            String sql = "UPDATE resolve_action_invoc SET u_" + type + "=null WHERE sys_id=?";
            Log.log.debug(sql);

            psts = conn.prepareStatement(sql);
            psts.setString(1, sysId);
            psts.executeUpdate();
        } catch(Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
        } finally
        {
            try {
                if (psts != null)
                {
                    psts.close();
                }
                if (conn != null)
                {
                    SQL.close(conn);
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    } // removeBrokenReference()
    
    private static void handleBatch(List<ResolveActionTaskVO> actionTaskList, String type) {

        String sysId = null;
        String uName = null;
        
        int size = actionTaskList.size();
        
        for(int i=0; i<size; i++) {
            try {
                sysId = getSysId(actionTaskList, i, type);
                if(sysId == null)
                    continue;
                
                uName = getUName(actionTaskList, i, type);
                if(uName == null)
                    continue;
                
                ResolveActionTaskVO taskVO = actionTaskList.get(i);
                Log.log.debug("action task = " + taskVO.getUFullName());
                Log.log.debug("sysId = " + sysId);
                
                if(uName.equalsIgnoreCase(taskVO.getUFullName()))
                    continue;
                
                else {
                    Log.log.debug("action task = " + taskVO.getUFullName());
                    Callable<Boolean> callable = new MigrateComponent(taskVO, type);
                    Future<Boolean> future = executor.submit(callable);
                    futures.add(future);
                }
            } catch(Exception e) {
                e.printStackTrace();
                Log.log.error(e.getMessage(), e);
            }
        } // end of for(i)
    }
    
    private static String getSysId(List<ResolveActionTaskVO> actionTaskList, int i, String type) {
        
        ResolveActionTaskVO taskVO = actionTaskList.get(i);
        ResolveActionInvocVO invocVO = taskVO.getResolveActionInvoc();
        
        String sysId= null;
        
        switch(type) {
            case MigrateActionTask.ASSESSOR:
                ResolveAssessVO assess = invocVO.getAssess();
                if(assess != null)
                    sysId = assess.getId();
                else
                    removeBrokenReference(invocVO.getSys_id(), MigrateActionTask.ASSESSOR);
                break;
            case MigrateActionTask.PARSER:
                ResolveParserVO parser = invocVO.getParser();
                if(parser != null)
                    sysId = parser.getId();
                else
                    removeBrokenReference(invocVO.getSys_id(), MigrateActionTask.PARSER);
                break;
            case MigrateActionTask.PREPROCESS:
                ResolvePreprocessVO preprocess = invocVO.getPreprocess();
                if(preprocess != null)
                    sysId = preprocess.getId();
                else
                    removeBrokenReference(invocVO.getSys_id(), MigrateActionTask.PREPROCESS);
                break;
            default:
                break;
        }
        
        return sysId;
    }
    
    private static String getUName(List<ResolveActionTaskVO> actionTaskList, int i, String type) {
        
        ResolveActionTaskVO taskVO = actionTaskList.get(i);
        ResolveActionInvocVO invocVO = taskVO.getResolveActionInvoc();
        
        String uName= null;
        
        switch(type) {
            case MigrateActionTask.ASSESSOR:
                uName = invocVO.getAssess().getUName();
                break;
            case MigrateActionTask.PARSER:
                uName = invocVO.getParser().getUName();
                break;
            case MigrateActionTask.PREPROCESS:
                uName = invocVO.getPreprocess().getUName();
                break;
            default:
                break;
        }
        
        return uName;
    }

    private static List<String> findActionTasksWithCommonAssess() throws Exception 
    {
        List<String> actionTaskNames = null;
        
        SQLConnection conn = null;
        ResultSet rs = null;
        
        try
        {
            conn = SQL.getConnection();
                
//            String sql = "SELECT RAT.U_FULLNAME TASK_NAME, RA.U_NAME ASSESS_NAME, RA.SYS_ID SYS_ID " + 
//                         "FROM resolve_assess RA, resolve_action_invoc RAI, resolve_action_task RAT " + 
//                         "WHERE RAT.U_INVOCATION = RAI.SYS_ID AND RAI.U_ASSESS = RA.SYS_ID AND RA.U_NAME != RAT.U_FULLNAME order by SYS_ID";
            
            String sql = "SELECT distinct(invoc1.u_description) " + 
                         "FROM resolve_action_invoc invoc1, resolve_action_invoc invoc2 " + 
                         "WHERE invoc1.u_assess=invoc2.u_assess AND invoc1.u_description<>invoc2.u_description " + 
                         "ORDER BY invoc1.u_assess";
            Log.log.debug(sql);
            
            PreparedStatement ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            actionTaskNames = new ArrayList<String>();

            while(rs.next()) {                
                String taskName = rs.getString(1);
                actionTaskNames.add(taskName);
            }
        } finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }
        
        return actionTaskNames;
        
    } // findActionTasksWithCommonAssess()
    
    private static List<String> findActionTasksWithCommonParser() throws Exception 
    {
        List<String> actionTaskNames = null;
        
        SQLConnection conn = null;
        ResultSet rs = null;
        
        try
        {
            conn = SQL.getConnection();
                
//            String sql = "SELECT RAT.U_FULLNAME TASK_NAME, RP.U_NAME AS PARSER_NAME, RP.SYS_ID AS SYS_ID " + 
//                         "FROM resolve_parser RP, resolve_action_invoc RAI, resolve_action_task RAT " + 
//                         "WHERE RAT.U_INVOCATION = RAI.SYS_ID AND RAI.U_PARSER = RP.SYS_ID AND RP.U_NAME != RAT.U_FULLNAME order by SYS_ID";
            
            String sql = "SELECT distinct(invoc1.u_description), invoc1.u_parser " + 
                         "FROM resolve_action_invoc invoc1, resolve_action_invoc invoc2 " + 
                         "WHERE invoc1.u_parser=invoc2.u_parser AND invoc1.u_description<>invoc2.u_description " + 
                         "ORDER BY invoc1.u_parser";
            Log.log.debug(sql);
            
            PreparedStatement ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            actionTaskNames = new ArrayList<String>();

            while(rs.next()) {                
                String taskName = rs.getString(1);
                actionTaskNames.add(taskName);
            }
        } finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }
        
        return actionTaskNames;
        
    } // findActionTasksWithCommonParser()
    
    private static List<String> findActionTasksWithCommonPreprocess() throws Exception 
    {
        List<String> actionTaskNames = null;
        
        SQLConnection conn = null;
        ResultSet rs = null;
        
        try
        {
            conn = SQL.getConnection();
                
//            String sql = "SELECT RAT.U_FULLNAME TASK_NAME, RPP.U_NAME PREPROCESS_NAME, RPP.SYS_ID SYS_ID " + 
//                         "FROM resolve_preprocess RPP, resolve_action_invoc RAI, resolve_action_task RAT " + 
//                         "WHERE RAT.U_INVOCATION = RAI.SYS_ID AND RAI.U_PREPROCESS = RPP.SYS_ID AND RPP.U_NAME != RAT.U_FULLNAME order by SYS_ID";
            
            String sql = "SELECT distinct(invoc1.u_description), invoc1.u_preprocess " + 
                         "FROM resolve_action_invoc invoc1, resolve_action_invoc invoc2 " + 
                         "WHERE invoc1.u_preprocess=invoc2.u_preprocess AND invoc1.u_description<>invoc2.u_description " + 
                         "ORDER BY invoc1.u_preprocess";
            Log.log.debug(sql);
            
            PreparedStatement ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            actionTaskNames = new ArrayList<String>();

            while(rs.next()) {                
                String taskName = rs.getString(1);
                actionTaskNames.add(taskName);
            }
        } finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }
        
        return actionTaskNames;
        
    } // findActionTasksWithCommonPreprocess()
    
    private static Set<String> findComponentNames(String type) throws Exception 
    {
        Set<String> names = null;
        
        SQLConnection conn = null;
        ResultSet rs = null;
        
        try
        {
            conn = SQL.getConnection();
                
//            String sql = "SELECT RAT.U_FULLNAME TASK_NAME, RPP.U_NAME PREPROCESS_NAME, RPP.SYS_ID SYS_ID " + 
//                         "FROM resolve_preprocess RPP, resolve_action_invoc RAI, resolve_action_task RAT " + 
//                         "WHERE RAT.U_INVOCATION = RAI.SYS_ID AND RAI.U_PREPROCESS = RPP.SYS_ID AND RPP.U_NAME != RAT.U_FULLNAME order by SYS_ID";
            
            String sql = "SELECT u_name FROM resolve_" + type;
            Log.log.debug(sql);
            
            PreparedStatement ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            names = new HashSet<String>();

            while(rs.next()) {                
                names.add(rs.getString(1));
            }
        } finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }
        
        return names;
        
    } // findComponentNames()
    
    private static List<String> findDanglingComponents(String type) throws Exception 
    {
        List<String> names = null;
        
        SQLConnection conn = null;
        ResultSet rs = null;
        
        try
        {
            conn = SQL.getConnection();
            
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT distinct(resolve_").append(type).append(".sys_id), resolve_").append(type).append(".u_name ");
            sb.append("FROM resolve_").append(type).append(", resolve_action_invoc ");
            sb.append("WHERE resolve_").append(type).append(".sys_id not in ");
            sb.append("(SELECT u_").append(type).append(" FROM resolve_action_invoc WHERE u_").append(type).append(" IS NOT NULL) ");
            if(type.equals(ASSESSOR))
                sb.append("AND resolve_").append(type).append(".u_name != 'DEFAULT#resolve'");
            
            String sql = sb.toString();
            Log.log.debug(sql);
            
            PreparedStatement ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            names = new ArrayList<String>();

            while(rs.next()) {                
                names.add(rs.getString(1));
            }
        } finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }
        
        return names;
        
    } // findDanglingComponents()
    
    private static Set<String> getUsedComponentNames(String type) throws Exception 
    {
        Set<String> names = null;
        
        SQLConnection conn = null;
        ResultSet rs = null;
        
        try
        {
            conn = SQL.getConnection();
            
            String sql = "SELECT " + type + ".u_name FROM resolve_action_invoc invoc, resolve_" + type + " " + type + " WHERE invoc.u_" + type + " = " + type + ".sys_id";
            Log.log.debug(sql);
            
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, type);
            ps.setString(2, type);
            ps.setString(3, type);
            ps.setString(4, type);
            ps.setString(5, type);

            rs = ps.executeQuery();
            
            names = new HashSet<String>();

            while(rs.next()) {
                names.add(rs.getString(1));
            }
        } finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }
        
        return names;
        
    } // getUsedComponentNames()
    
    private static String findConflictActionTaskName(String componentName, String type) throws Exception 
    {
        SQLConnection conn = null;
        ResultSet rs = null;
        String name = null;
        
        try
        {
            conn = SQL.getConnection();
            
            String sql = "SELECT invoc.u_description FROM resolve_action_invoc invoc, resolve_" + type + " " + type + 
                         " WHERE invoc.u_" + type + " = " + type + ".sys_id AND " + type + ".u_name = ?";
            Log.log.debug(sql);
            
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, componentName);
            
            rs = ps.executeQuery();

            while(rs.next()) {                
                name = rs.getString(1);
            }
        } finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }
        
        return name;
        
    } // findConflictActionTaskName()
    
    private static Map<String, String> findActionTasksWithDifferentComponentNames(String type) throws Exception {
        
        Map<String, String> map = new HashMap<String, String>();
        
        SQLConnection conn = null;
        ResultSet rs = null;
        
        try
        {
            conn = SQL.getConnection();
                
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT resolve_action_invoc.u_description, resolve_").append(type).append(".u_name ");
            sb.append("FROM resolve_action_invoc, resolve_").append(type).append(" "); 
            sb.append("WHERE resolve_action_invoc.u_").append(type).append(" = resolve_").append(type).append(".sys_id ");
            sb.append("AND resolve_action_invoc.u_description != resolve_").append(type).append(".u_name");
            
            String sql = sb.toString();
            Log.log.debug(sql);
            
            PreparedStatement ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();

            while(rs.next()) {                
                map.put(rs.getString(1), rs.getString(2)); // action_task name, component name
            }
        } finally
        {
            if (conn != null)
            {
                SQL.close(conn);
            }
        }
        
        return map;
    } // findActionTasksWithDifferentComponentNames()
    
    private static class MigrateComponent implements Callable<Boolean> {
        
        ResolveActionTaskVO actionTaskVO = null;
        String type = null;
        
        public MigrateComponent(ResolveActionTaskVO actionTaskVO, String type) {
            
            this.actionTaskVO = actionTaskVO;
            this.type = type;
        }
        
        @Override
        public Boolean call() throws Exception {
            
            boolean result = true;
            
            try {
                result = ActionTaskUtil.migrateComponent(actionTaskVO, type, "admin");
            } catch(Exception e) {
                result = false;
                Log.log.error(e.getMessage(), e);
            }
            
            return result;
        }
    } // MigrateComponent
    
} // MigrateActionTask
