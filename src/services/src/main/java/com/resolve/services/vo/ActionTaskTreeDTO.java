/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.ArrayList;
import java.util.List;

public class ActionTaskTreeDTO
{
    private String id;
    private String name;
    private Boolean leaf;
    private Boolean expanded;
    private List<ActionTaskTreeDTO> children = new ArrayList<ActionTaskTreeDTO>();
    
    public ActionTaskTreeDTO() {
        this.expanded = false;
    }
    
    public Boolean getExpanded()
    {
        return expanded;
    }

    public void setExpanded(Boolean expanded)
    {
        this.expanded = expanded;
    }

    public String getId()
    {
        return id;
    }
    public void setId(String id)
    {
        this.id = id;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public Boolean getLeaf()
    {
        return leaf;
    }
    public void setLeaf(Boolean leaf)
    {
        this.leaf = leaf;
    }
    
    public List<ActionTaskTreeDTO> getChildren()
    {
        return children;
    }
    public void setChildren(List<ActionTaskTreeDTO> children)
    {
        this.children = children;
    }
    
    @Override
    public String toString()
    {
        return name;
    }
    
    @Override
    public boolean equals(Object otherObj)
    {
        boolean isEquals = false;
        if (otherObj instanceof ActionTaskTreeDTO)
        {
            ActionTaskTreeDTO that = (ActionTaskTreeDTO) otherObj;
            // if sys_id are not equal, they are diff
            if (that.getName().equals(this.getName()))
            {
                isEquals = true;
            }
        }
        return isEquals;
    }

    @Override
    public int hashCode()
    {
        int hash = 1;
        hash = hash * 37 + this.getName().hashCode();
        hash = hash * 37 + (this.getName() == null ? 0 : this.getName().hashCode());
        return hash;
    }
}
