/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.hibernate.CacheMode;
import org.hibernate.query.Query;

import com.resolve.persistence.dao.AccessRightsDAO;
import com.resolve.persistence.dao.WikiDocumentDAO;
import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.CustomTable;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikidocStatistics;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.RightTypeEnum;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.customtable.CustomTableUtil;
import com.resolve.services.hibernate.vo.MetaViewLookupVO;
import com.resolve.services.hibernate.vo.PropertiesVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.util.UserUtils;
import com.resolve.services.util.WorksheetUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class StoreUtility
{
    public static void saveListOfWikiDocs(Collection<WikiDocumentVO> list, String username) 
    {
        for (WikiDocumentVO doc : list)
        {
            try
            {
                saveWikiDocument(doc, username);
            }
            catch (Exception e)
            {
                Log.log.error("Error saving doc:" + doc.getUFullname(), e);
            }
        }

    }
    
    public static WikiDocumentVO saveWikiDocument(WikiDocumentVO vo, String username) throws Exception
    {
        if(vo != null)
        {
            WikiDocument doc = null;
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equals(VO.STRING_DEFAULT))
            {
                doc = WikiUtils.getWikiDocumentModel(vo.getSys_id(), null, username, false);
                if(doc != null)
                {
                    doc.applyVOToModel(vo);
                }
                else
                {
                    doc = new WikiDocument(vo);
                }
            }
            else
            {
                doc = new WikiDocument(vo);
            }
            
            //save it
            SaveUtil.saveWikiDocument(doc, username);
        }
        
        return vo;
    }

    public static boolean hasUserAccessRight(String user, Set<String> userRoles, String entityRolesStr)
    {
        return UserUtils.hasRole(user, userRoles, entityRolesStr);
    }

    /**
     * This method is an overloaded method that returns true if a login user has access rights to a wikidoc
     * 
     * @param user
     * @param wikidoc
     * @return True if the user has access, false otherwise
     */
    public static boolean hasUserAccessRight(String user, WikiDocument wikidoc)
    {
        boolean hasRights = false;

        try
        {
            Set<String> userRoles = UserUtils.getUserRoles(user);

            if (user.equals(ConstantValues.WIKI_ADMIN) || user.equals(ConstantValues.WIKI_RESOLVE_MAINT))
            {
                hasRights = true;
            }
            else if (userRoles.contains("admin"))// if the user has the admin role, then no issues
            {
                hasRights = true;
            }
            else
            {
                String userWriteAccess = wikidoc.getAccessRights().getUWriteAccess();
                String userAdminAccess = wikidoc.getAccessRights().getUAdminAccess();
                String docRights = userWriteAccess + "," + userAdminAccess;

                hasRights = hasUserAccessRight(user, userRoles, docRights);
            }
        }
        catch (Throwable t)
        {
            Log.log.error("Error in hasUserAccessRight user -->" + user + " with Document -->" + wikidoc.getUFullname(), t);
        }

        return hasRights;

    } // hasUserAccessRight

    public static boolean hasUserAccessRight(String user, String docName)
    {
        boolean hasRights = false;

        try
        {
            Set<String> userRoles = UserUtils.getUserRoles(user);

            if (user.equals(ConstantValues.WIKI_ADMIN) || user.equals(ConstantValues.WIKI_RESOLVE_MAINT))
            {
                hasRights = true;
            }
            else if (userRoles.contains("admin"))// if the user has the admin role, then no issues
            {
                hasRights = true;
            }
            else
            {
                WikiDocument wikidoc = WikiUtils.getWikiDocumentModel(null, docName, user, false);
                String userWriteAccess = wikidoc.getAccessRights().getUWriteAccess();
                String userAdminAccess = wikidoc.getAccessRights().getUAdminAccess();
                String docRights = userWriteAccess + "," + userAdminAccess;

                hasRights = hasUserAccessRight(user, userRoles, docRights);
            }
        }
        catch (Throwable t)
        {
            Log.log.error("Error in hasUserAccessRight user -->" + user + " with Document -->" + docName, t);
        }

        return hasRights;
    }

    public static boolean hasUserAdminRight(String user, String docName)
    {
        boolean hasAdminRights = false;

        try
        {
            Set<String> userRoles = UserUtils.getUserRoles(user);

            if (user.equals(ConstantValues.WIKI_ADMIN) || user.equals(ConstantValues.WIKI_RESOLVE_MAINT))
            {
                hasAdminRights = true;
            }
            else if (userRoles.contains("admin"))// if the user has the admin role, then no issues
            {
                hasAdminRights = true;
            }
            else
            {
                WikiDocument wikidoc =  WikiUtils.getWikiDocumentModel(null, docName, user, false);
                String userAdminAccess = wikidoc.getAccessRights().getUAdminAccess();

                hasAdminRights = hasUserAccessRight(user, userRoles, userAdminAccess);
            }
        }
        catch (Throwable t)
        {
            Log.log.error("Error in hasUserAccessRight user -->" + user + " with Document -->" + docName, t);
        }

        return hasAdminRights;
    }
    
    public static boolean isCustomTable(String tableName)
    {
        boolean isCustomTable = false;

        if (StringUtils.isNotBlank(tableName))
        {
            try
            {
                CustomTable example = new CustomTable();
                example.setUModelName(tableName);

                @SuppressWarnings("unchecked")
				List<CustomTable> list = (List<CustomTable>) HibernateProxy.execute(() -> {
                	return HibernateUtil.getDAOFactory().getCustomTableDAO().find(example);
                });

                if (list != null && list.size() > 0)
                {
                    isCustomTable = true;
                }

            }
            catch (Throwable e)
            {
                Log.log.error("Error while looking for custom table:" + tableName, e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }

        return isCustomTable;
    }// isCustomTable
    
    //example --> ${view_lookup:CR}
    public static String evaluateViewLookup(String lookupString, String username)  throws Exception
    {
        String view = lookupString;

        if (StringUtils.isNotBlank(lookupString) && lookupString.indexOf(ConstantValues.VIEW_LOOKUP) > -1)
        {
            //if there are no recs in the DB, then use Default
            String viewName = "Default";

            //get user roles
            Set<String> userRoles = UserUtils.getUserRoles(username);

            //example --> ${view_lookup:CR}
            String appName = lookupString.substring(lookupString.indexOf(':') + 1, lookupString.indexOf('}'));

            //get list of recs for this app order by the order #
            List<MetaViewLookupVO> sortedList = CustomTableUtil.getViewLookupForApp(appName, username);

            for (MetaViewLookupVO lookup : sortedList)
            {
                String lookupRoles = lookup.getURoles();
                if (StringUtils.isNotBlank(lookupRoles))
                {
                    boolean hasRights = UserUtils.hasRole(username, userRoles, lookupRoles);
                    if (hasRights)
                    {
                        viewName = lookup.getUViewName();
                        break;
                    }
                }
            }//end of for loop

            //assign it
            view = viewName;
        }

        return view;
    }
    
    public static String getActionResultDetail(String problem_id, String task_id, String node_id)
    {
        return getActionResultDetail(problem_id, task_id, node_id, null, null, null);
    } // getActionResultDetail

    public static String getActionResultDetail(String problem_id, String task_id, String node_id, String separator, Integer max, Integer offset)
    {
        String value = "";

        if (StringUtils.isEmpty(separator))
        {
            separator = "\n<hr/>\n";
        }

        //by default, only show 1 detail
        if (max == null)
        {
            max = 1;
        }

        //task id is mandatory to get the results
        if (StringUtils.isNotBlank(task_id) && StringUtils.isNotEmpty(problem_id))
        {
            boolean isActiveWS = ServiceWorksheet.isThisWorksheetCurrent(problem_id);
            if (isActiveWS)
            {
                //this is cassandra
                value = WorksheetUtil.getActionResultDetail(problem_id, task_id, node_id, separator, max, offset);
            }
            else
            {
                boolean isArchived = ServiceHibernate.isThisWorksheetInArchive(problem_id);
                if (isArchived)
                {
                    //this is archive tables
                    value = ServiceHibernate.getActionResultDetailFromArchive(problem_id, task_id, node_id, separator, max, offset);
                }
            }

        }

        return value;
    } // getActionResultDetail
    
    public static Set<String> getRolesFor(RightTypeEnum rightType, List<PropertiesVO> lProps, String docNamespace)
    {
        Set<String> roles = new TreeSet<String>();
        boolean hasNamespaceDefaultSet = false;

        if (StringUtils.isNotBlank(docNamespace))
        {
            docNamespace = docNamespace.trim().toLowerCase();
        }
        else
        {
            docNamespace = null;
        }

        if (lProps != null && lProps.size() > 0)
        {
            for (PropertiesVO p : lProps)
            {
                String uname = p.getUName().toLowerCase();//make it lower case to make it case insensitive
                if (uname.indexOf(rightType.toString()) > 0)
                {
                    int startPos = uname.indexOf(docNamespace);
                    if (startPos > 0)
                    {
                        if (uname.substring(startPos, uname.length()).equalsIgnoreCase(docNamespace))
                        {
                            hasNamespaceDefaultSet = true;
                            String values = p.getUValue();
                            if (StringUtils.isNotEmpty(values))
                            {
                                String[] tempRoles = values.split(",");
                                for (String s : tempRoles)
                                {
                                    if (!StringUtils.isEmpty(s))
                                    {
                                        roles.add(s.trim());
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // if there is no setting for this namespace, then use ALL
            if (!hasNamespaceDefaultSet && roles.isEmpty())
            {
                for (PropertiesVO p : lProps)
                {
                    String uname = p.getUName();
                    if (uname.indexOf(rightType.toString()) > 0)
                    {
                        if (uname.indexOf("ALL") > 0)
                        {
                            String values = p.getUValue();
                            if (StringUtils.isNotEmpty(values))
                            {
                                String[] tempRoles = p.getUValue().split(",");
                                for (String s : tempRoles)
                                {
                                    roles.add(s.trim());
                                }
                            }
                        }
                    }
                }
            }
        }

        return roles;
    }
    
    public static WikiDocument findById(String id) throws WikiException
    {
        try
        {
          HibernateProxy.setCurrentUser("system");
        	return (WikiDocument) HibernateProxy.executeNoCache(() -> {
        		WikiDocument doc = null;
                AccessRights ar = null;
                
	            WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
	            doc = dao.findById(id);
	
	            if (doc != null)
	            {
	                // load the stats for the doc
	                if (doc.getWikidocStatistics() != null)
	                {
	                    for (WikidocStatistics stats : doc.getWikidocStatistics())
	                    {
	                        /*Integer in =*/ stats.getUViewCount();
	                    }
	                }
	
	                // populate the access rights
	                if (doc.getAccessRights() != null)
	                {
	                    ar = doc.getAccessRights();
	                }
	                else
	                {
	                    ar = new AccessRights();
	                    ar.setUResourceType(WikiDocument.RESOURCE_TYPE);
	                    ar.setUResourceName(doc.getUFullname());
	                    ar.setUResourceId(doc.getSys_id());
	
	                    AccessRightsDAO daoAR = HibernateUtil.getDAOFactory().getAccessRightsDAO();
	                    ar = daoAR.findFirst(ar);
	                }
	
	                doc.setUReadRoles(ar.getUReadAccess());
	                doc.setUWriteRoles(ar.getUWriteAccess());
	                doc.setUAdminRoles(ar.getUAdminAccess());
	                doc.setUExecuteRoles(ar.getUExecuteAccess());
	            }
	            
	            return doc;
        	});
        }
        catch (Throwable e)
        {
            Log.log.error("Error while looking for Wikidoc Sys_Id:" + id, e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return null;
    }
    
    /**
     * Find a doc
     * 
     * @param docFullName
     * @return
     * @throws WikiException
     */
    public static WikiDocument find(String docFullName) throws WikiException
    {
        return find(docFullName, true);
    }
    
    /**
     * Find a doc
     * 
     * @param docFullName
     * @param throwException
     * @return
     * @throws WikiException
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static WikiDocument find(String docFullName, boolean throwException) throws WikiException
    {

        if (StringUtils.isNotBlank(docFullName))
        {
            try
            {
              HibernateProxy.setCurrentUser("system");
                return (WikiDocument) HibernateProxy.executeNoCache(() -> {
                	AccessRights ar = null;
                	WikiDocument doc = null;

	                List<WikiDocument> lWiki = new ArrayList<WikiDocument>();
	                String hql = "from WikiDocument wd where LOWER(wd.UFullname) like '" + StringUtils.escapeSql(docFullName.toLowerCase()) + "'";
	                Query query = HibernateUtil.createQuery(hql);
	                lWiki = query.list();
	
	                if (lWiki.size() == 1)
	                {
	                    doc = lWiki.get(0);
	                }
	                else if (lWiki.size() > 1)
	                {
	                    throw new WikiException(new Exception("There are more then 1 copy of <" + docFullName + "> document. Please contact the Administrator."));
	                }
	                else
	                {
	                    if (throwException)
	                    {
	                        throw new WikiException(new Exception("There is no document by name <" + docFullName + ">."));
	                    }
	                }
	
	                // populate the access rights
	                if (doc != null)
	                {
	                    ar = doc.getAccessRights();
	                }

	                if (ar != null)
	                {
	                	doc.setUReadRoles(ar.getUReadAccess());
	                	doc.setUWriteRoles(ar.getUWriteAccess());
	                	doc.setUAdminRoles(ar.getUAdminAccess());
	                	doc.setUExecuteRoles(ar.getUExecuteAccess());
	                }
	                
	                return doc;
                });

            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage());
                              HibernateUtil.rethrowNestedTransaction(e);
                return null;
            }
        }
        
        return null;
    }
}
