/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.form;

//mapped to meta_form_action table
public class RsButtonAction
{

    private String id;
    private String crudAction;
    private String operation;
    private String runbook;
    private String actionTask;
    private String script;
    private Integer orderNumber;
    private String additionalParam;
    private String redirectUrl;
    private String redirectTarget;
    
    public String getId()
    {
        return id;
    }
    public void setId(String id)
    {
        this.id = id;
    }
    public String getCrudAction()
    {
        return crudAction;
    }
    public void setCrudAction(String crudAction)
    {
        this.crudAction = crudAction;
    }
    public String getOperation()
    {
        return operation;
    }
    public void setOperation(String operation)
    {
        this.operation = operation;
    }
    public String getRunbook()
    {
        return runbook;
    }
    public void setRunbook(String runbook)
    {
        this.runbook = runbook;
    }
    public String getScript()
    {
        return script;
    }
    public void setScript(String script)
    {
        this.script = script;
    }
    public Integer getOrderNumber()
    {
        return orderNumber;
    }
    public void setOrderNumber(Integer sequence)
    {
        this.orderNumber = sequence;
    }
    public String getAdditionalParam()
    {
        return additionalParam;
    }
    public void setAdditionalParam(String additionalParam)
    {
        this.additionalParam = additionalParam;
    }
    public String getRedirectUrl()
    {
        return redirectUrl;
    }
    public void setRedirectUrl(String redirectUrl)
    {
        this.redirectUrl = redirectUrl;
    }
    public String getRedirectTarget()
    {
        return redirectTarget;
    }
    public void setRedirectTarget(String redirectTarget)
    {
        this.redirectTarget = redirectTarget;
    }
    public String getActionTask()
    {
        return actionTask;
    }
    public void setActionTask(String actionTask)
    {
        this.actionTask = actionTask;
    }
    

}
