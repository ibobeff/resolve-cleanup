/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/
package com.resolve.services.archive;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Level;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.admin.indices.stats.IndicesStatsResponse;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.IndexNotFoundException;
import org.elasticsearch.index.query.QueryBuilder;

import com.google.common.collect.Lists;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.search.APIFactory;
import com.resolve.search.SearchAdminAPI;
import com.resolve.search.SearchConstants;
import com.resolve.search.SearchException;
import com.resolve.search.SearchUtils;
import com.resolve.search.model.ProcessRequest;
import com.resolve.search.model.TaskResult;
import com.resolve.search.model.Worksheet;
import com.resolve.search.model.WorksheetData;
import com.resolve.search.playbook.PlaybookIndexAPI;
import com.resolve.search.worksheet.WorksheetIndexAPI;
import com.resolve.search.worksheet.WorksheetSearchAPI;
import com.resolve.services.util.PlaybookUtils;
import com.resolve.services.util.UserUtils;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryFilter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.sql.SQL;
import com.resolve.sql.SQLConnection;
import com.resolve.util.Guid;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;

public class BaseArchiveTable
{
	public static final String START_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	public static final String ALL_DOCUMENT_IDS_IN_INDEX_ID = "ALL_DOCUMENT_IDS_IN_INDEX_ID";
	
	private static final String UNDER_SCORE = "_";
	private static final String SIR = "SIR";
	private static final String NON_SIR = "Non-" + SIR;
		
    private static String WORKSHEET_INSERT_STATEMENT = "";
    private static String PROCESS_REQUEST_INSERT_STATEMENT = "";
    private static String TASK_RESULT_INSERT_STATEMENT = "";
    private static String TASK_RESULT_LOB_INSERT_STATEMENT = "";
    private static String WORKSHEET_DATA_INSERT_STATEMENT = "";
    private static String WORKSHEET_DATA_DELETE_STATEMENT = "";
    private static String PROCESS_REQUEST_DELETE_STATEMENT = "";
    private static String WORKSHEET_DELETE_STATEMENT = "";
    private static String TASK_RESULT_DELETE_STATEMENT = "";
    private static String TASK_RESULT_LOB_DELETE_STATEMENT = "";
    private static String SIR_UPDATE_STATEMENT = "";
    private static String DUPLICATE_SIR_UPDATE_STATEMENT = "";
    
    private static final SimpleDateFormat sdf = new SimpleDateFormat(START_DATE_FORMAT);
    
    protected int MIN_BLOCK_SIZE = 50; 
    public static String RSARCHIVE_CHECK = "RSARCHIVE_CHECK"; 
    public static String CONCURRENT_ARCHIVE_NUMBER = "CONCURRENT_ARCHIVE_NUMBER";
    public static volatile boolean isArchiving = false; 
        
    static 
    {
        StringBuilder worksheetInsertStatement = new StringBuilder(" insert into archive_worksheet (sys_id, u_alert_id, u_condition, u_correlation_id, u_debug, u_description, u_number, u_reference, u_severity, u_summary, u_work_notes, u_worksheet, u_assigned_to, u_assigned_to_name, sys_created_by, sys_created_on, sys_mod_count, sys_updated_by, sys_updated_on, u_sir_id, sys_org) ");
        worksheetInsertStatement.append(" values(?, "); // sys_id
        worksheetInsertStatement.append(" ?, "); // u_alert_id
        worksheetInsertStatement.append(" ?, "); // u_condition
        worksheetInsertStatement.append(" ?, "); // u_correlation_id
        worksheetInsertStatement.append(" ?, "); // u_debug
        worksheetInsertStatement.append(" ?, "); // u_description
        worksheetInsertStatement.append(" ?, "); // u_number
        worksheetInsertStatement.append(" ?, "); // u_reference
        worksheetInsertStatement.append(" ?, "); // u_severity
        worksheetInsertStatement.append(" ?, "); // u_summary
        worksheetInsertStatement.append(" ?, "); // u_work_notes
        worksheetInsertStatement.append(" ?, "); // u_worksheet
        worksheetInsertStatement.append(" ?, "); // u_assigned_to
        worksheetInsertStatement.append(" ?, "); // u_assigned_to_name
        worksheetInsertStatement.append(" ?, "); // sys_created_by
        worksheetInsertStatement.append(" ?, "); // sys_created_on
        worksheetInsertStatement.append(" ?, "); // sys_mod_count
        worksheetInsertStatement.append(" ?, "); // sys_updated_by
        worksheetInsertStatement.append(" ?, "); // sys_updated_on
        worksheetInsertStatement.append(" ?, "); // u_sir_id
        worksheetInsertStatement.append(" ?) "); // sys_org
        WORKSHEET_INSERT_STATEMENT = worksheetInsertStatement.toString();
        
        StringBuilder processRequestInsertSql = new StringBuilder(" insert into archive_process_request (sys_id, u_duration, u_number, u_status, u_timeout, u_wiki, sys_created_by, sys_created_on, sys_mod_count, sys_updated_by, sys_updated_on, u_problem, u_atexeccount) ");
        processRequestInsertSql.append(" values(?, "); // sys_id
        processRequestInsertSql.append(" ?, "); // u_duration
        processRequestInsertSql.append(" ?, "); // u_number
        processRequestInsertSql.append(" ?, "); // u_status
        processRequestInsertSql.append(" ?, "); // u_timeout
        processRequestInsertSql.append(" ?, "); // u_wiki
        processRequestInsertSql.append(" ?, "); // sys_created_by
        processRequestInsertSql.append(" ?, "); // sys_created_on
        processRequestInsertSql.append(" ?, "); // sys_mod_count
        processRequestInsertSql.append(" ?, "); // sys_updated_by
        processRequestInsertSql.append(" ?, "); // sys_updated_on
        processRequestInsertSql.append(" ?, "); // u_problem
        processRequestInsertSql.append(" ?) "); // u_atexeccount
        PROCESS_REQUEST_INSERT_STATEMENT = processRequestInsertSql.toString();
        
        StringBuilder taskResultInsertSql = new StringBuilder(" insert into archive_action_result (sys_id, u_address, u_completion, u_condition, u_duration, u_esbaddr, u_severity, u_timestamp, sys_created_by, sys_created_on," + " sys_mod_count, sys_updated_by, sys_updated_on, u_action_result_lob, u_execute_request, u_execute_result, u_problem, u_process, u_target, u_target_guid, u_actiontask, u_hidden)");
        taskResultInsertSql.append(" values(?, "); // sys_id
        taskResultInsertSql.append(" ?, "); // u_address
        taskResultInsertSql.append(" ?, "); // u_completion
        taskResultInsertSql.append(" ?, "); // u_condition
        taskResultInsertSql.append(" ?, "); // u_duration
        taskResultInsertSql.append(" ?, "); // u_esbaddr
        taskResultInsertSql.append(" ?, "); // u_severity
        taskResultInsertSql.append(" ?, "); // u_timestamp
        taskResultInsertSql.append(" ?, "); // sys_created_by
        taskResultInsertSql.append(" ?, "); // sys_created_on
        taskResultInsertSql.append(" ?, "); // sys_mod_count
        taskResultInsertSql.append(" ?, "); // sys_updated_by
        taskResultInsertSql.append(" ?, "); // sys_updated_on
        taskResultInsertSql.append(" ?, "); // u_action_result_lob
        taskResultInsertSql.append(" ?, "); // u_execute_request
        taskResultInsertSql.append(" ?, "); // u_execute_result
        taskResultInsertSql.append(" ?, "); // u_problem
        taskResultInsertSql.append(" ?, "); // u_process
        taskResultInsertSql.append(" ?, "); // u_target
        taskResultInsertSql.append(" ?, "); // u_target_guid
        taskResultInsertSql.append(" ?, "); // u_actiontask
        taskResultInsertSql.append(" ?) "); // u_hidden
        TASK_RESULT_INSERT_STATEMENT = taskResultInsertSql.toString();
        
        StringBuilder taskResultLobInsertSqlLob = new StringBuilder(" insert into archive_action_result_lob (sys_id, u_detail, u_summary, " + "sys_created_by, sys_created_on, sys_mod_count, sys_updated_by, sys_updated_on)");
        taskResultLobInsertSqlLob.append(" values(?, "); // sys_id
        taskResultLobInsertSqlLob.append(" ?, "); // u_detail
        taskResultLobInsertSqlLob.append(" ?, "); // u_summary
        taskResultLobInsertSqlLob.append(" ?, "); // sys_created_by
        taskResultLobInsertSqlLob.append(" ?, "); // sys_created_on
        taskResultLobInsertSqlLob.append(" ?, "); // sys_mod_count
        taskResultLobInsertSqlLob.append(" ?, "); // sys_updated_by
        taskResultLobInsertSqlLob.append(" ?) "); // sys_updated_on
        TASK_RESULT_LOB_INSERT_STATEMENT = taskResultLobInsertSqlLob.toString();

        StringBuilder worksheetDataInsertStatement = 
        					new StringBuilder(" insert into archive_worksheet_data (" +
        											"wsid, " + // #1 Worksheet Id
        											"propname, " + // #2 Property Name
        											"propvalue, " + // #3 Property Value (byte[])
        											"sys_created_by, " + // #4 Create By
        											"sys_created_on, " + // #5 Created On
        											"sys_mod_count, " + // #6 Modification Count
        											"sys_updated_by, " + // #7 Updated By
        											"sys_updated_on, " + // #8 Updated On
        											"sys_org, " + // #9 Sys Org
        											"sys_id) "); // #10 Sys Id
        worksheetDataInsertStatement.append(" values(?, "); // wsid
        worksheetDataInsertStatement.append(" ?, "); // propname
        worksheetDataInsertStatement.append(" ?, "); // propvalue
        worksheetDataInsertStatement.append(" ?, "); // sys_created_by
        worksheetDataInsertStatement.append(" ?, "); // sys_created_on
        worksheetDataInsertStatement.append(" ?, "); // sys_mod_count
        worksheetDataInsertStatement.append(" ?, "); // sys_updated_by
        worksheetDataInsertStatement.append(" ?, "); // sys_updated_on
        worksheetDataInsertStatement.append(" ?, "); // sys_org
        worksheetDataInsertStatement.append(" ?) "); // sys_org
        WORKSHEET_DATA_INSERT_STATEMENT = worksheetDataInsertStatement.toString();
        
        StringBuilder worksheetDataDeleteStatement = 
				new StringBuilder(" delete from archive_worksheet_data where " +
								  " wsid = ? " + // Worksheet Id
								  " and propname = ? "); // Property Name
        WORKSHEET_DATA_DELETE_STATEMENT = worksheetDataDeleteStatement.toString();
        
        StringBuilder processRequestDeleteStatement =
        		new StringBuilder(" delete from archive_process_request where sys_id = ? "); // sys_id
        PROCESS_REQUEST_DELETE_STATEMENT = processRequestDeleteStatement.toString();
        
        StringBuilder worksheetDeleteStatement = 
        		new StringBuilder(" delete from archive_worksheet where sys_id = ? "); // sys_id
        WORKSHEET_DELETE_STATEMENT = worksheetDeleteStatement.toString();
        
        StringBuilder taskResultDeleteStatement =
        		new StringBuilder(" delete from archive_action_result where sys_id = ? "); // sys_id
        TASK_RESULT_DELETE_STATEMENT = taskResultDeleteStatement.toString();
        
        StringBuilder taskResultLobDeleteStatement = 
        		new StringBuilder(" delete from archive_action_result_lob where sys_id = ? "); // sys_id
        TASK_RESULT_LOB_DELETE_STATEMENT = taskResultLobDeleteStatement.toString();
        
        StringBuilder sirUpdateStatement = 
        		new StringBuilder(" update resolve_security_incident " +
        						  " set sys_is_deleted = 'Y', " +
        						  " sys_updated_by = 'system', " +
        						  " sys_updated_on = ? " +
        						  " where u_problem_id = ? ");
        SIR_UPDATE_STATEMENT = sirUpdateStatement.toString();
        
        StringBuilder duplicateSIRUpdateStatement = 
        		new StringBuilder(" update resolve_security_incident " +
        						  " set u_master_sir = '' " +
        						  " where u_master_sir = ? ");
        DUPLICATE_SIR_UPDATE_STATEMENT = duplicateSIRUpdateStatement.toString();
        
        sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.of("UTC")));
    }

//    static
//    {
//        startUpdateThread();    
//    }
    
    protected String archive_process_request_cols = null;
    protected String archive_action_result_cols = null;
    protected String archive_execute_dependency_cols = null;
    protected String archive_execute_request_cols = null;
    protected String archive_execute_result_cols = null;
    protected String archive_worksheet_cols = null;
    protected String archive_worksheet_debug_cols = null;

    protected String dbType;
    protected long expiry = 0; // In Days
    protected long cleanup = 0; // In Days
    protected Duration expiryDuration = Duration.ZERO; // In java Duration
    protected Duration cleanupDuration = Duration.ofDays(7l); // In java Duration
    protected Duration sirExpiryDuration = Duration.ZERO;
    protected Duration sirCleanupDuration = Duration.ofDays(7l);
    protected boolean lock = false;
    protected int blockSize = 10000;
    protected int smallTableBlockSize = 2000;
    
    protected long startTime;
    public static volatile boolean isSystemBusy = false;
    
    public void setExpiryDuration(Duration expiryDuration) {
    	this.expiryDuration = expiryDuration;
    }
    
    public void setBlocksize(int blockSize) {
    	this.blockSize = blockSize;
    }
    
    public void setSmallTableBlocksize(int smallTableBlockSize) {
    	this.smallTableBlockSize = smallTableBlockSize;
    }
    
    protected BaseArchiveTable()
    {
        blockSize = ConfigArchive.getBlocksize();
        smallTableBlockSize = ConfigArchive.getSmalltableblocksize();
    }

    public synchronized void archive(boolean force, boolean isSIRWSArchive)
    {
        long methodStartTime = Log.start(String.format("Starting %s Worksheet Archive [force is %b]: ", 
        											   (isSIRWSArchive ? SIR : NON_SIR), force), Level.INFO);
        
        isArchiving = true;
        
        // NOTE: This startTime will be used throughout the Archive process so
        // we kind of get a lock in the start time.
        startTime = System.currentTimeMillis();

        // set expiry for this execution
        expiry = force ? 0 : (isSIRWSArchive ? ConfigArchive.getSirExpiry() : ConfigArchive.getExpiry());
        expiryDuration = force ? Duration.ZERO : (isSIRWSArchive ? ConfigArchive.getSirExpiryDuration() : 
        														   ConfigArchive.getExpiryDuration());
        
        cleanup = force ? Duration.ofDays(7l).toMillis() : (isSIRWSArchive ? ConfigArchive.getSirCleanup() : ConfigArchive.getCleanup());
        cleanupDuration = force ? Duration.ofDays(7l) : (isSIRWSArchive ? ConfigArchive.getSirCleanupDuration() : 
        															ConfigArchive.getCleanupDuration());
        
        // archive
        long expiryTime = getTimeInMillis(expiryDuration);
        long cleanupTime = getTimeInMillis(expiryDuration.plus(cleanupDuration));
        
        Log.log.info(String.format("%s Worksheet Archive start time: %s, expiry (in days/Duration): %d/%s, " +
				   				   "cleanup (in days/Duration): %d/%s, expiry (Timestamp): %s, " +
				   				   "cleanup (Timestamp): %s", (isSIRWSArchive ? SIR : NON_SIR),
				   				   sdf.format(new Date(startTime)), expiry, expiryDuration, cleanup, cleanupDuration, 
				   				   sdf.format(new Date(expiryTime)), sdf.format(new Date(cleanupTime))));
        
    	Set<String> ignoreIds = null;
    	
        ignoreIds = WorksheetSearchAPI.getOpenProcessRequests(expiryTime,cleanupTime);
        try {
			ignoreIds.addAll(WorksheetSearchAPI.getRecentlyUsedWorksheets(expiryTime));
			ignoreIds.addAll(WorksheetSearchAPI.getRecentlyUsedWSDATAWorksheets(expiryTime));
		} catch (SearchException e1) {
			Log.log.error(e1.getMessage(),e1);
		}
        
        if (CollectionUtils.isNotEmpty(ignoreIds)) {
        	try {
        		List<String> filteredWorksheetIds = WorksheetSearchAPI.filterWorksheetIds(ignoreIds, null, UserUtils.SYSTEM, 
        																			 isSIRWSArchive);
        		
        		if (Log.log.isDebugEnabled() && CollectionUtils.isNotEmpty(filteredWorksheetIds)) {
        			Log.log.debug(String.format("Size of ignore worksheet ids (%s + %s) is %d out of which " +
        										"%s worksheet ids size is %d", SIR, NON_SIR, ignoreIds.size(),
        										(isSIRWSArchive ? SIR : NON_SIR), filteredWorksheetIds.size()));
        		}
        		
        		// Reset ignoreIds to contain SIR worksheet ids only instead of both (SIR + Non-SIR) worksheet ids
        		ignoreIds.clear();
        		ignoreIds.addAll(filteredWorksheetIds);
        	} catch (SearchException e) {
        		Log.log.error(String.format("Error %sin finding %s worksheet ids for %d ignore (%s + %s) worksheet ids",
											(StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
											(isSIRWSArchive ? SIR : NON_SIR), ignoreIds.size(), SIR, NON_SIR), e);
        		throw new RuntimeException(e);
        	}
        }
        
        Map<Pair<String, String>, List<String>> delArchivedProcReqs = 
        											archiveProcessRequests(expiryTime, cleanupTime, ignoreIds, isSIRWSArchive);
        
        Map<Pair<String, String>, List<String>> delArchivedTaskResultReqs = 
													archiveTaskResults(expiryTime, cleanupTime, ignoreIds,
																	   isSIRWSArchive);
        
        Pair<Map<Pair<String, String>, List<String>>, List<String>> wsDeleteAndArchivedDocs = 
        																archiveWorksheets(expiryTime, cleanupTime, 
        																				  ignoreIds, isSIRWSArchive);
        
        Map<Pair<String, String>, List<String>> delArchivedWSDATAReqs = new HashMap<Pair<String, String>, List<String>>();
        
        if (CollectionUtils.isNotEmpty(wsDeleteAndArchivedDocs.getRight())) {
        	delArchivedWSDATAReqs = archiveWorksheetsData(wsDeleteAndArchivedDocs.getRight(), isSIRWSArchive);
        }
        
        if (isSIRWSArchive && CollectionUtils.isNotEmpty(wsDeleteAndArchivedDocs.getRight())) {
        	try {
				archiveSIRData(wsDeleteAndArchivedDocs.getRight());
			} catch (SearchException e) {
				Log.log.error(String.format("Error %sin archiving %d SIR worksheet ids",
											(StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
											wsDeleteAndArchivedDocs.getRight().size()), e);
				throw new RuntimeException(e);
			}
        }
        
        //we've removed all the indexes we can now delete the singular docs
        Map<Pair<String, String>, List<String>> delArchivedDocReqs = new HashMap<Pair<String, String>, List<String>>();
        
        if (MapUtils.isNotEmpty(delArchivedProcReqs)) {
        	Log.log.info(String.format("Adding %d %s process request ids from %d daily process request indices to bulk " +
        							   "delete documents by ids",
        							   delArchivedProcReqs.values().stream()
        							   .mapToInt(list -> {
        								   	if (list.contains(ALL_DOCUMENT_IDS_IN_INDEX_ID)) {
        								   		return list.size() - 1;
        								   	} else {
        								   		return list.size();
        								   	}
        							   }).sum(),
        							   (isSIRWSArchive ? SIR : NON_SIR), delArchivedProcReqs.keySet().size()));
        	delArchivedDocReqs.putAll(delArchivedProcReqs);
        }

        if (MapUtils.isNotEmpty(wsDeleteAndArchivedDocs.getLeft())) {
        	Log.log.info(String.format("Adding %d %s worksheet ids from %d daily worksheet indices to bulk " +
        							   "delete documents by ids",
        							   wsDeleteAndArchivedDocs.getLeft().values().stream()
        							   .mapToInt(list -> {
						   					if (list.contains(ALL_DOCUMENT_IDS_IN_INDEX_ID)) {
						   						return list.size() - 1;
						   					} else {
						   						return list.size();
						   					}
        							   }).sum(),
        							   (isSIRWSArchive ? SIR : NON_SIR), wsDeleteAndArchivedDocs.getLeft().keySet().size()));
        	delArchivedDocReqs.putAll(wsDeleteAndArchivedDocs.getLeft());
        }
        
        if (MapUtils.isNotEmpty(delArchivedWSDATAReqs)) {
        	Log.log.info(String.format("Adding %d %s worksheet data ids from worksheetdata index to bulk " +
        							   "delete documents by ids", 
        							   delArchivedWSDATAReqs.values().stream().mapToInt(list -> list.size()).sum(), 
        							   (isSIRWSArchive ? SIR : NON_SIR)));
        	delArchivedDocReqs.putAll(delArchivedWSDATAReqs);
        }
        
        if (MapUtils.isNotEmpty(delArchivedTaskResultReqs)) {
        	Log.log.info(String.format("Adding %d %s task result ids from %d daily task result indices to bulk " +
        							   "delete documents by ids", 
        							   delArchivedTaskResultReqs.values().stream()
        							   .mapToInt(list -> {
						   					if (list.contains(ALL_DOCUMENT_IDS_IN_INDEX_ID)) {
						   						return list.size() - 1;
						   					} else {
						   						return list.size();
						   					}
        							   }).sum(),
        							   (isSIRWSArchive ? SIR : NON_SIR), delArchivedTaskResultReqs.keySet().size()));
        	delArchivedDocReqs.putAll(delArchivedTaskResultReqs);
        }
        
        if (MapUtils.isNotEmpty(delArchivedDocReqs)) {
        	try {
        		APIFactory.getGenericIndexAPI().bulkDeleteByIds(delArchivedDocReqs, false);
        	} catch (SearchException e) {
        		Log.log.error(String.format("Error %sin bulk deleting documents by ids from main ES",
        									StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""), e);
        		throw new RuntimeException(e);
        	}
        }
        
        isArchiving = false;
        
        Log.duration(String.format("Completed %s Worksheet Archive [force is %b]: ", 
        						   (isSIRWSArchive ? SIR : NON_SIR), force), Level.INFO, methodStartTime);
        
    } // archive
    
    public Map<Pair<String, String>, List<String>> archiveProcessRequests(long expiryTime, long cleanupTime, 
    															   		  Set<String> ignoreIds, boolean isSIRWSArchive) {
    	long methodStartTime = Log.start(String.format("Starting %s Archive Process Requests [Expiry Time %s (%d ms), " +
    												   "Cleanup Time %s (%d ms), Ignore Worksheet Ids count %d]: ",
    												   (isSIRWSArchive ? "SIR" : "Non-SIR"), sdf.format(new Date(expiryTime)), 
    												   expiryTime, sdf.format(new Date(cleanupTime)), cleanupTime, 
    												   ignoreIds.size()), 
    									 Level.INFO);
    	
        ResponseDTO<String> prIndxNamesResp = WorksheetSearchAPI.getAllProcessRequestIndexNames(expiryTime);
        List<String> indexes = prIndxNamesResp.getRecords();
        final Map<Pair<String, String>, List<String>> delProcReqs = 
        													new ConcurrentHashMap<Pair<String, String>, List<String>>();
		QueryDTO queryDTO = new QueryDTO();
        queryDTO.addSortItem(new QuerySort("sysUpdatedOn",QuerySort.SortOrder.ASC));
        
		indexes.parallelStream().forEach(index -> {
			List<String> deleteDocs = archiveProcessRequestIndex(expiryTime, cleanupTime, ignoreIds, index, queryDTO, 
																 isSIRWSArchive);
			
			if (CollectionUtils.isNotEmpty(deleteDocs)) {
				delProcReqs.put(Pair.of(index, SearchConstants.DOCUMENT_TYPE_PROCESSREQUEST), deleteDocs);
			}
		});
		
		Log.duration(String.format("Completed %s Archive Process Requests [Expiry Time %s (%d ms), Cleanup Time %s (%d ms), " +
								   "Ignore Worksheet Ids count %d]: %s", (isSIRWSArchive ? "SIR" : "Non-SIR"), 
								   sdf.format(new Date(expiryTime)), expiryTime, sdf.format(new Date(cleanupTime)), 
								   cleanupTime, ignoreIds.size(),
								   (MapUtils.isNotEmpty(delProcReqs) ? 
									delProcReqs.values().stream()
									.filter(list -> !list.contains(ALL_DOCUMENT_IDS_IN_INDEX_ID))
									.mapToInt(list -> list.size()).sum() + 
									" Process Request Documents From " + delProcReqs.keySet().size() + 
									" Daily Process Request Indices Need To Be Deleted" : "")), 
					 Level.INFO, methodStartTime);
		
		return delProcReqs;
    }
    
    public List<String> archiveProcessRequestIndex(long expiryTime, long cleanupTime, Set<String> ignoreIds, String index, 
    											   QueryDTO queryDTO, boolean isSIRWSArchive)
    {
    	long methodStartTime = Log.start(String.format("Starting Archive %s Process Requests from Index %s [Expiry Time %s " +
    												   "(%d ms), Cleanup Time %s (%d ms), Ignore Worksheet Ids count %d]: ", 
    												   (isSIRWSArchive ? SIR : NON_SIR), index, 
    												   sdf.format(new Date(expiryTime)), expiryTime, 
    												   sdf.format(new Date(cleanupTime)), cleanupTime, ignoreIds.size()), 
    									 Level.INFO);
    	
    	List<String> deleteDocs = new ArrayList<String>();
    	String scrollId = null;
    	long indexDocTotal = getTotalforIndex(index);
    	
    	try {	
    		long searchStartTime = getSearchStartTimeBasedOnIndex(index);
    		
    		if(indexDocTotal != 0) {
    			List<String> archivedProcReqsids = new ArrayList<String>();
    			final Set<String> procReqWorksheetIds = new ConcurrentSkipListSet<String>();
    			TimeValue scrollTimeOut = new TimeValue(WorksheetSearchAPI.getScrollCtxKeepAliveDur(), TimeUnit.MINUTES);
    			
    			Pair<List<ProcessRequest>, String> scrollProcReqs = findProcessRequestsByUpdatedOn(
    																			queryDTO, ignoreIds, searchStartTime, 
    																			expiryTime, cleanupTime, new String[] {index}, 
    																			scrollId, scrollTimeOut);
    			
				while(scrollProcReqs != null && scrollProcReqs.getLeft() != null &&
	    			  CollectionUtils.isNotEmpty(scrollProcReqs.getLeft())) {										
					scrollId = StringUtils.isNotBlank(scrollProcReqs.getRight()) ? scrollProcReqs.getRight() : null;
					
					scrollProcReqs.getLeft().parallelStream()
					.forEach(procReq -> {
						if (procReq != null && StringUtils.isNotBlank(procReq.getProblem())) {
							procReqWorksheetIds.add(procReq.getProblem());
						}
					});
					
					final List<String> filteredWorksheetIds = new CopyOnWriteArrayList<>();
					
					if (CollectionUtils.isNotEmpty(procReqWorksheetIds)) {
						filteredWorksheetIds.addAll(WorksheetSearchAPI.filterWorksheetIds(procReqWorksheetIds, 
												  									 	  null, 
												  									 	  UserUtils.SYSTEM, 
												  									 	  isSIRWSArchive));
					}
					
					List<ProcessRequest> sirProcReqs = new ArrayList<ProcessRequest>();
					
					if (CollectionUtils.isNotEmpty(filteredWorksheetIds) && isSIRWSArchive) {
						// Filter out SIR worksheets updated after expiry based on updated time of SIR in DB
							
						List<String> updatedSIRWorksheetIds = PlaybookUtils
															  .identifySIRsUpdatedAfterExpiry(expiryTime, 
																	  						  filteredWorksheetIds, 
																							  UserUtils.SYSTEM);
							
						if (CollectionUtils.isNotEmpty(updatedSIRWorksheetIds)) {
							filteredWorksheetIds.removeAll(updatedSIRWorksheetIds);
						}
					}
					
					if (CollectionUtils.isNotEmpty(filteredWorksheetIds)) {
						sirProcReqs = scrollProcReqs.getLeft().parallelStream()
								  	  .filter(procReq -> filteredWorksheetIds.contains(procReq.getProblem()))
								  	  .collect(Collectors.toList());
					}
					
					scrollProcReqs = Pair.of(sirProcReqs, scrollId);
					
					if (Log.log.isDebugEnabled()) {
						Log.log.debug(String.format("Archiving %d %s process requests from index %s", 
													scrollProcReqs.getLeft().size(), (isSIRWSArchive ? SIR : NON_SIR), index));
					}
					
					if (CollectionUtils.isNotEmpty(scrollProcReqs.getLeft())) {
						archivedProcReqsids.addAll(archiveProcessInternal(scrollProcReqs.getLeft(), new String[] {index},
																		  isSIRWSArchive));
					}
					
					procReqWorksheetIds.clear();					
					scrollProcReqs = null;
					
					if (StringUtils.isNotBlank(scrollId)) {
						scrollProcReqs = findProcessRequestsByUpdatedOn(queryDTO, ignoreIds, searchStartTime, expiryTime, 
																		cleanupTime, new String[] {index}, scrollId,
																		scrollTimeOut);
					}
				}
				
				if (scrollProcReqs != null) {
    				scrollId = StringUtils.isNotBlank(scrollProcReqs.getRight()) ? scrollProcReqs.getRight() : null;
    				
    				if (StringUtils.isNotBlank(scrollId)) {
    					WorksheetSearchAPI.clearProcessRequestScroll(scrollId);
    					scrollId = null;
    				}
    			}
				
				if (Log.log.isDebugEnabled()) {
					Log.log.debug(String.format("Archived total %d %s process requests from index %s containing total %d " +
											 	"process requests", archivedProcReqsids.size(), 
											 	(isSIRWSArchive ? SIR : NON_SIR), index, indexDocTotal));
				}
    			
    			if(indexDocTotal == archivedProcReqsids.size()) {
    				if(deleteIndex(index) && Log.log.isDebugEnabled()) {
            			Log.log.debug(String.format("Deleted process requests index %s containing %d %s process requests", 
            										index, archivedProcReqsids.size(), (isSIRWSArchive ? SIR : NON_SIR)));
    				} else {
    					Log.log.warn(String.format("Failed to delete process requests index %s containing %d %s process " +
    												"requests, adding %d process requests to delete from " +
    												"process request index alias", index, indexDocTotal, 
    												(isSIRWSArchive ? SIR : NON_SIR), archivedProcReqsids.size()));
            			deleteDocs.addAll(archivedProcReqsids);
    				}
    				deleteDocs.add(ALL_DOCUMENT_IDS_IN_INDEX_ID);
            	} else {
            		deleteDocs.addAll(archivedProcReqsids);
            	}
    		} else {
    			deleteIndex(index);
    			deleteDocs.add(ALL_DOCUMENT_IDS_IN_INDEX_ID);
    		}
        } catch (Throwable t) {
			Log.log.error(String.format("Error %sin archiving %s process requests from index %s", 
										(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
										(isSIRWSArchive ? SIR : NON_SIR), index), t);
		} finally {
			if (StringUtils.isNotBlank(scrollId)) {
				WorksheetSearchAPI.clearProcessRequestScroll(scrollId);
			}
		}
    	
    	Log.duration(String.format("Completed Archive %s Process Requests from Index %s [Expiry Time %s (%d ms), " +
    							   "Cleanup Time %s (%d ms), Ignore Worksheet Ids count %d]: Archived %s %s Process Request " +
    							   "Documents out of total %d Process Request Documents", (isSIRWSArchive ? SIR : NON_SIR), 
    							   index, sdf.format(new Date(expiryTime)), expiryTime, sdf.format(new Date(cleanupTime)), 
								   cleanupTime, ignoreIds.size(), 
								   (deleteDocs.contains(ALL_DOCUMENT_IDS_IN_INDEX_ID) ? 
								    (deleteDocs.size() == 1 ? "All" : "" + (deleteDocs.size() - 1)) : 
								    "" + deleteDocs.size()), (isSIRWSArchive ? SIR : NON_SIR), indexDocTotal), 
    				 Level.INFO, methodStartTime);
    	
    	return deleteDocs;
    }
    
    public Pair<Map<Pair<String, String>, List<String>>, List<String>> 
    												archiveWorksheets(long expiryTime, long cleanupTime, 
    																  Set<String> ignoreIds, boolean isSIRWSArchive) {
    	long methodStartTime = Log.start(String.format("Starting Archive %s Worksheets [Expiry Time %s (%d ms), " +
    												   "Cleanup Time %s (%d ms), Ignore Worksheet Ids count %d]: ",
    												   (isSIRWSArchive ? SIR : NON_SIR), sdf.format(new Date(expiryTime)), 
    												   expiryTime, sdf.format(new Date(cleanupTime)), cleanupTime, 
    												   ignoreIds.size()), Level.INFO);

        ResponseDTO<String> wsIndxNamesResp = WorksheetSearchAPI.getAllWorksheetIndexNames(expiryTime);
        List<String> indexes = wsIndxNamesResp.getRecords();
        final List<String> archivedDocs = new CopyOnWriteArrayList<String>();
        final Map<Pair<String, String>, List<String>> delWSReqs = 
															new ConcurrentHashMap<Pair<String, String>, List<String>>();
    	
    	QueryDTO queryDTO = new QueryDTO(0, blockSize);
        queryDTO.addSortItem(new QuerySort("sysUpdatedOn",QuerySort.SortOrder.ASC));
        queryDTO.setSelectColumns("sysUpdatedOn,sysId,sirId");
        queryDTO.setExcludeColumns("summary");
    
        indexes.parallelStream().forEach(index -> {
        	Pair<List<String>, List<String>> deletedAndArchivedWSIds = archiveWorksheetIndex(expiryTime, cleanupTime, 
        																					 ignoreIds, index, queryDTO,
        																					 isSIRWSArchive);
        	
        	if (Log.log.isDebugEnabled()) {
        		Log.log.debug(String.format("Worksheet Index %s: Total %s worksheets archived %d, %s Worksheets to be " +
        									"deleted from index %d", index, (isSIRWSArchive ? SIR : NON_SIR), 
        									deletedAndArchivedWSIds.getRight().size(), (isSIRWSArchive ? SIR : NON_SIR),
        									deletedAndArchivedWSIds.getLeft().size()));
        	}
        	
        	if (CollectionUtils.isNotEmpty(deletedAndArchivedWSIds.getLeft())) {
        		delWSReqs.put(Pair.of(index, SearchConstants.DOCUMENT_TYPE_WORKSHEET), deletedAndArchivedWSIds.getLeft());
			}

        	archivedDocs.addAll(deletedAndArchivedWSIds.getRight());
        	
        	if (Log.log.isDebugEnabled()) {
        		Log.log.debug(String.format("After processing %s Overall archiving %s worksheets progress: " +
        									"Total %s worksheets archived %d", index, (isSIRWSArchive ? SIR : NON_SIR),
        									(isSIRWSArchive ? SIR : NON_SIR), archivedDocs.size()));
        	}
        	

        });
        
        Log.duration(String.format("Completed Archive %s Worksheets [Expiry Time %s (%d ms), Cleanup Time %s (%d ms), " +
								   "Ignore Worksheet Ids count %d]: Archived %d Worksheet Documents, %d Worksheet Documents" +
								   " From %d Daily Worksheet Indices Need To Be Deleted", (isSIRWSArchive ? SIR : NON_SIR),
								   sdf.format(new Date(expiryTime)), expiryTime, sdf.format(new Date(cleanupTime)), 
								   cleanupTime, ignoreIds.size(), archivedDocs.size(), 
								   delWSReqs.values().stream()
								   .filter(list -> !list.contains(ALL_DOCUMENT_IDS_IN_INDEX_ID))
								   .mapToInt(list -> list.size()).sum(),
								   delWSReqs.keySet().size()), Level.INFO, methodStartTime);
        
    	return Pair.of(delWSReqs, archivedDocs);
    }
    
    private long getSearchStartTimeBasedOnIndex(String index) {
    	long searchStartTime = 0;
    	
    	if (StringUtils.isNotBlank(index)) {
    		String yearMonthDay = StringUtils.substringAfter(index, UNDER_SCORE);
    		
    		if (StringUtils.isNotBlank(yearMonthDay) && yearMonthDay.length() == 8) {
				String year = yearMonthDay.substring(0, 4).trim();
				String month = yearMonthDay.substring(4, 6).trim();
				String day = yearMonthDay.substring(6, 8).trim();
				
				if (StringUtils.isNotBlank(year) && year.length() == 4 &&
					StringUtils.isNotBlank(month) && month.length() == 2 &&
					StringUtils.isNotBlank(day) && day.length() == 2) {
					searchStartTime = Instant
									  .parse(String.format("%s-%s-%sT00:00:00Z", 
 										 				   year, month, day))
									  .toEpochMilli();
				}
			}
    	}
    	
    	return searchStartTime;
    }
    
    private Pair<List<String>, List<String>> archiveWorksheetIndex(long expiryTime, long cleanupTime, Set<String> ignoreIds, 
    															   String index, QueryDTO queryDTO, boolean isSIRWSArchive) {
    	long methodStartTime = Log.start(String.format("Starting Archive %s Worksheets from Index %s [Expiry Time %s " +
    												   "(%d ms), Cleanup Time %s (%d ms), Ignore Worksheet Ids count %d]: ", 
				   									   (isSIRWSArchive ? SIR : NON_SIR), index, 
				   									   sdf.format(new Date(expiryTime)), expiryTime, 
				   									   sdf.format(new Date(cleanupTime)), cleanupTime, ignoreIds.size()), 
    									 Level.INFO);
    	
    	List<String> deleteDocs = new ArrayList<String>();
    	List<String>archivedWorksheetIds = new ArrayList<String>();
    	
    	try
		{	
    		long searchStartTime = getSearchStartTimeBasedOnIndex(index);    		
    		long indexDocTotal = getTotalforIndex(index);
    		
    		if(indexDocTotal != 0)
    		{
    			List<String> worksheetIds = new ArrayList<String>();
    			List<Worksheet> worksheets = findWorksheetsByUpdatedOn(queryDTO, searchStartTime, expiryTime, 
    																   cleanupTime, smallTableBlockSize, 0, ignoreIds,
    																   new String[] {index}, isSIRWSArchive);
    			
    			if (CollectionUtils.isNotEmpty(worksheets)) {
					worksheets.forEach(ws -> worksheetIds.add(ws.getSysId()));
					
					if (isSIRWSArchive) {
						// Filter out SIR worksheets updated after expiry based on updated time of SIR in DB
						
						List<String> updatedSIRWorksheetIds = new ArrayList<String>();
						
						try {
							updatedSIRWorksheetIds = PlaybookUtils.identifySIRsUpdatedAfterExpiry(expiryTime, 
																		  						  worksheetIds, 
																								  UserUtils.SYSTEM);
						} catch (Exception e) {
							Log.log.error(String.format("Error %sin identifying worksheet ids of SIRs updated after %s " +
														"(%d ms) from given %d SIR worksheet ids", 
														(StringUtils.isNotBlank(e.getMessage()) ? 
														 "[" + e.getMessage() + "] " : ""), sdf.format(new Date(expiryTime)),
														expiryTime, worksheetIds.size()));
						}
													
						if (CollectionUtils.isNotEmpty(updatedSIRWorksheetIds)) {
							worksheetIds.removeAll(updatedSIRWorksheetIds);
						}
					}
					
					if (Log.log.isDebugEnabled()) {
						Log.log.debug(String.format("archiving %d %s worksheet documents from index %s", worksheetIds.size(), 
													(isSIRWSArchive ? SIR : NON_SIR), index));
					}
					
					if (CollectionUtils.isNotEmpty(worksheetIds)) {
						archivedWorksheetIds.addAll(archiveWorksheet(worksheetIds, index));
					}
	        		
	        		if (Log.log.isDebugEnabled()) {
	        			Log.log.debug(String.format("archived %d %s worksheet documents from index %s", worksheetIds.size(), 
													(isSIRWSArchive ? SIR : NON_SIR), index));
	        		}
	    			
	            	if(indexDocTotal == archivedWorksheetIds.size()) {
	            		if(deleteIndex(index) && Log.log.isDebugEnabled()) {
	            			Log.log.debug(String.format("Deleted worksheet index %s containing %d worksheets", index,
	            										archivedWorksheetIds.size()));
	    				} else {
	    					Log.log.warn(String.format("Failed to delete worksheet index %s containing %d worksheets " +
	    											   ", adding %d worksheets to delete from worksheet " +
	    											   "index alias", index, indexDocTotal, archivedWorksheetIds.size()));
	            			deleteDocs.addAll(archivedWorksheetIds);
	    				}
	    				deleteDocs.add(ALL_DOCUMENT_IDS_IN_INDEX_ID);
	            	} else {
	            		deleteDocs.addAll(archivedWorksheetIds);
	            		deleteDocs.add(ALL_DOCUMENT_IDS_IN_INDEX_ID);
	            	}
    			}
    		}
    		else {
    			deleteIndex(index);
    		}
        } catch (SearchException e) {
			Log.log.error(e.getMessage(),e);
		}
    	
    	Log.duration(String.format("Completed Archive %s Worksheet from Index %s [Expiry Time %s (%d ms), " +
				   				   "Cleanup Time %s (%d ms), Ignore Worksheet Ids count %d]: Archived %d %s Worksheet " +
				   				   "Documents, %d %s Worksheet Documents From %s Worksheet Daily Index Need To Be Deleted", 
				   				   (isSIRWSArchive ? SIR : NON_SIR), index, sdf.format(new Date(expiryTime)), expiryTime, 
				   				   sdf.format(new Date(cleanupTime)), cleanupTime, ignoreIds.size(), 
				   				   archivedWorksheetIds.size(), (isSIRWSArchive ? SIR : NON_SIR), 
				   				   (deleteDocs.contains(ALL_DOCUMENT_IDS_IN_INDEX_ID) ? 
				   					deleteDocs.size() - 1 : deleteDocs.size()), (isSIRWSArchive ? SIR : NON_SIR), index), 
    				 Level.INFO, methodStartTime);
    	
    	return Pair.of(deleteDocs, archivedWorksheetIds);
    }
    
    public Map<Pair<String, String>, List<String>> 
    											archiveTaskResults(long expiryTime, long cleanupTime, Set<String> ignoreIds,
    															   boolean isSIRWSArchive) {
    	long methodStartTime = Log.start(String.format("Starting Archive %s Task Results [Expiry Time %s (%d ms), " +
				   									   "Cleanup Time %s (%d ms), Ignore Worksheet Ids count %d]: ", 
				   									   (isSIRWSArchive ? SIR : NON_SIR), sdf.format(new Date(expiryTime)), 
				   									   expiryTime, sdf.format(new Date(cleanupTime)), cleanupTime, 
				   									   ignoreIds.size()), Level.INFO);
    	
        ResponseDTO<String> indices = WorksheetSearchAPI.getAllTaskResultIndexNames(expiryTime);
        List<String> indexes = indices.getRecords();
    	final Map<Pair<String, String>, List<String>> delTaskResultReqs = 
															new ConcurrentHashMap<Pair<String, String>, List<String>>();
    	QueryDTO queryDTO = new QueryDTO(0, blockSize);
        queryDTO.setSelectColumns("sysUpdatedOn,sysId,problemId");
        queryDTO.addSortItem(new QuerySort("sysUpdatedOn",QuerySort.SortOrder.ASC));
    	
        indexes.parallelStream().forEach(index -> {
        	List<String> deleteDocs = archiveTaskResultIndex(expiryTime, cleanupTime, ignoreIds, index, queryDTO,
        													 isSIRWSArchive);
        	
        	if (CollectionUtils.isNotEmpty(deleteDocs)) {
        		delTaskResultReqs.put(Pair.of(index, SearchConstants.DOCUMENT_TYPE_TASKRESULT), deleteDocs);
			}
        });
    	
        Log.duration(String.format("Completed Archive %s Task Results [Expiry Time %s (%d ms), Cleanup Time %s (%d ms), " +
				   				   "Ignore Worksheet Ids count %d]: %s", (isSIRWSArchive ? SIR : NON_SIR), 
				   				   sdf.format(new Date(expiryTime)), expiryTime, sdf.format(new Date(cleanupTime)), 
				   				   cleanupTime, ignoreIds.size(),
				   				   (MapUtils.isNotEmpty(delTaskResultReqs) ? 
				   					delTaskResultReqs.values().stream()
				   					.filter(list -> !list.contains(ALL_DOCUMENT_IDS_IN_INDEX_ID))
				   					.mapToInt(list -> list.size()).sum() + 
				   					" Task Result Documents From " + delTaskResultReqs.keySet().size() + 
				   					" Daily Task Result Indices Need To Be Deleted" : "")), 
        			 Level.INFO, methodStartTime);
        
    	return delTaskResultReqs;
    }
    
    private List<String> archiveTaskResultIndex(long expiryTime, long cleanupTime, Set<String> ignoreIds, 
    											String index, QueryDTO queryDTO, boolean isSIRWSArchive) {
    	long methodStartTime = Log.start(String.format("Starting Archive %s Task Result from Index %s [Expiry Time %s " +
    												   "(%d ms), Cleanup Time %s (%d ms), Ignore Worksheet Ids count %d]: ", 
				   									   (isSIRWSArchive ? SIR : NON_SIR), index, 
				   									   sdf.format(new Date(expiryTime)), expiryTime, 
				   									   sdf.format(new Date(cleanupTime)), cleanupTime, ignoreIds.size()), 
    									 Level.INFO);
    	
    	List<String> deleteDocs = new ArrayList<String>();
    	
    	String scrollId = null;
    	long indexDocTotal = getTotalforIndex(index);
    	
    	try {	
    		long searchStartTime = getSearchStartTimeBasedOnIndex(index);
    		
    		if(indexDocTotal != 0) {
    			Set<String> taskResultIds = new HashSet<String>();
    			final Set<String> taskResultWorksheetIds = new ConcurrentSkipListSet<String>();
    			List<String> archivedTaskResultsids = new ArrayList<String>();
    			TimeValue scrollTimeOut = new TimeValue(WorksheetSearchAPI.getScrollCtxKeepAliveDur(), TimeUnit.MINUTES);
    			
    			Pair<List<TaskResult>, String> scrollTaskResults = findTaskResultsByUpdatedOn(queryDTO, ignoreIds, 
    																						  searchStartTime, expiryTime, 
    																						  cleanupTime, 
    																						  new String[] {index}, scrollId,
    																						  scrollTimeOut);
    			
				while(scrollTaskResults != null && scrollTaskResults.getLeft() != null &&
	    			  CollectionUtils.isNotEmpty(scrollTaskResults.getLeft())) {
					scrollId = StringUtils.isNotBlank(scrollTaskResults.getRight()) ? scrollTaskResults.getRight() : null;
					
					scrollTaskResults.getLeft().parallelStream()
					.forEach(taskResult -> {
						if (taskResult != null && StringUtils.isNotBlank(taskResult.getProblemId())) {
							taskResultWorksheetIds.add(taskResult.getProblemId());
						}
					});
					
					final List<String> filteredWorksheetIds = new CopyOnWriteArrayList<>();
					
					if (CollectionUtils.isNotEmpty(taskResultWorksheetIds)) {
						filteredWorksheetIds.addAll(WorksheetSearchAPI.filterWorksheetIds(taskResultWorksheetIds, 
												  									 	  null, 
												  									 	  UserUtils.SYSTEM,
												  									 	  isSIRWSArchive));
					}
					
					List<TaskResult> sirTaskResults = new ArrayList<TaskResult>();
					
					if (CollectionUtils.isNotEmpty(filteredWorksheetIds) && isSIRWSArchive) {
						// Filter out SIR worksheets updated after expiry based on updated time of SIR in DB
							
						List<String> updatedSIRWorksheetIds = PlaybookUtils
															  .identifySIRsUpdatedAfterExpiry(expiryTime, 
																	  						  filteredWorksheetIds, 
																							  UserUtils.SYSTEM);
														
						if (CollectionUtils.isNotEmpty(updatedSIRWorksheetIds)) {
							filteredWorksheetIds.removeAll(updatedSIRWorksheetIds);
						}
					}
					
					if (CollectionUtils.isNotEmpty(filteredWorksheetIds)) {
						sirTaskResults = scrollTaskResults.getLeft().parallelStream()
								  	 	 .filter(taskResult -> filteredWorksheetIds.contains(taskResult.getProblemId()))
								  	 	 .collect(Collectors.toList());
					}
					
					scrollTaskResults = Pair.of(sirTaskResults, scrollId);
					
					if (Log.log.isDebugEnabled()) {
						Log.log.debug(String.format("Archiving %d %s task results from index %s", 
													scrollTaskResults.getLeft().size(), 
													(isSIRWSArchive ? SIR : NON_SIR), index));
					}
					
					if (scrollTaskResults != null && scrollTaskResults.getLeft() != null &&
						CollectionUtils.isNotEmpty(scrollTaskResults.getLeft())) {
						scrollTaskResults.getLeft()
						.forEach(taskResult -> {
							taskResultIds.add(taskResult.getSysId());
						});
					}
					
					if (CollectionUtils.isNotEmpty(taskResultIds)) {
						archivedTaskResultsids.addAll(archiveTaskResult(new ArrayList<String>(taskResultIds), 
																		new String[] {index}, isSIRWSArchive));
					}
					
					taskResultIds.clear();					
					taskResultWorksheetIds.clear();
					scrollTaskResults = null;
					
					if (StringUtils.isNotBlank(scrollId)) {
						scrollTaskResults = findTaskResultsByUpdatedOn(queryDTO, ignoreIds, searchStartTime, expiryTime, 
																   	   cleanupTime, new String[] {index}, scrollId, 
																   	   scrollTimeOut);
					}
				}
				
				if (scrollTaskResults != null) {
					scrollId = StringUtils.isNotBlank(scrollTaskResults.getRight()) ? scrollTaskResults.getRight() : null;
					
					if (StringUtils.isNotBlank(scrollId)) {
						WorksheetSearchAPI.clearTaskResultScroll(scrollId);
						scrollId = null;
					}
				}
				
				if (Log.log.isDebugEnabled()) {
					Log.log.debug(String.format("Archived total %d %s task results from index %s containing %d task results", 
    											archivedTaskResultsids.size(), (isSIRWSArchive ? SIR : NON_SIR), index, 
    											indexDocTotal));
				}
    			
    			if(indexDocTotal == archivedTaskResultsids.size()) {
    				if(deleteIndex(index) && Log.log.isDebugEnabled()) {
            			Log.log.debug(String.format("Deleted task result index %s containing %d task results", index,
            										archivedTaskResultsids.size()));
    				} else {
    					Log.log.warn(String.format("Failed to delete task results index %s containing %d task results " +
    											   ", adding %d task results to delete from task results " +
    											   "index alias", index, indexDocTotal, archivedTaskResultsids.size()));
            			deleteDocs.addAll(archivedTaskResultsids);
    				}
    				deleteDocs.add(ALL_DOCUMENT_IDS_IN_INDEX_ID);
            	} else {
            		deleteDocs.addAll(archivedTaskResultsids);
            	}
    		} else {
    			deleteIndex(index);
    			deleteDocs.add(ALL_DOCUMENT_IDS_IN_INDEX_ID);
    		}
        } catch (Throwable t) {
			Log.log.error(String.format("Error %sin archiving task results for index %s", 
										(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
										index), t);
		} finally {
			if (StringUtils.isNotBlank(scrollId)) {
				WorksheetSearchAPI.clearTaskResultScroll(scrollId);
			}
		}
    	
    	Log.duration(String.format("Completed Archive %s Task Results Index %s [Expiry Time %s (%d ms), Cleanup Time %s " +
				   				   "(%d ms), Ignore Worksheet Ids count %d]: Archived %s %s Task Result Documents out of " +
				   				   "total %d Task Result Documents", (isSIRWSArchive ? SIR : NON_SIR), index, 
				   				   sdf.format(new Date(expiryTime)), expiryTime, sdf.format(new Date(cleanupTime)), 
				   				   cleanupTime, ignoreIds.size(), 
				   				   (deleteDocs.contains(ALL_DOCUMENT_IDS_IN_INDEX_ID) ? 
									(deleteDocs.size() == 1 ? "All" : "" + (deleteDocs.size() - 1)) : 
									"" + deleteDocs.size()), (isSIRWSArchive ? SIR : NON_SIR), indexDocTotal), 
    				 Level.INFO, methodStartTime);
    	
    	return deleteDocs;
    }
    
    public long getTotalforIndex(String index)
    {
    	IndicesStatsResponse response = SearchAdminAPI.getClient().admin().indices().prepareStats(index).execute().actionGet();
        return response.getIndex(index).getTotal().getDocs().getCount();	
    }
    public static boolean deleteIndex(String index)
    {
    	boolean result = false;
    	try {
    		// don't delete the current days index
    		if(!StringUtils.contains(index, new SimpleDateFormat("yyyyMMdd").format(new Date()))) {
    			DeleteIndexRequest delIndxReq = new DeleteIndexRequest(index);
    			
    			delIndxReq.timeout(TimeValue.timeValueMinutes(10)); // Timeout to wait for all nodes to acknowledge deletion
    			delIndxReq.masterNodeTimeout(TimeValue.timeValueMinutes(4)); // Timeout to connect to master node
    			delIndxReq.indicesOptions(IndicesOptions.strictSingleIndexNoExpandForbidClosed());
    			
    			DeleteIndexResponse deleteResponse = SearchAdminAPI.getClient().admin().indices()
    												 .delete(delIndxReq).actionGet();
    			result = deleteResponse.isAcknowledged();
    		}
    	} catch (IndexNotFoundException ie) {
    		Log.log.warn(ie.getMessage());
    	}
    	
    	return result;
    }

    protected void adjustBlockSize()
    {
        if (smallTableBlockSize > MIN_BLOCK_SIZE)
        {
            smallTableBlockSize/=10;
            if (smallTableBlockSize<MIN_BLOCK_SIZE)
            {
                smallTableBlockSize = MIN_BLOCK_SIZE;
            }
        }
    }
    
    protected List<String> archiveTaskResults()
    {
    	throw new RuntimeException("Must be implemented by subclass.");
    }
    
    private List<String> archiveTaskResult(List<String> sysIds, String[] indexNames, boolean isSIRWSArchive) {
    	long methodStartTime = Log.start(String.format("Starting Archive %d %s Task Result Documents From Indices [%s]: ",
    												   sysIds.size(), (isSIRWSArchive ? SIR : NON_SIR), 
    												   StringUtils.listToString(Arrays.asList(indexNames))), Level.INFO);
    	
    	SQLConnection sqlConn = null;
    	List<String> archivedRecords = new ArrayList<String>();
    	int orginalTxIsolationLevel = -1;
    	
    	try {
    		sqlConn = SQL.getConnection();
    		orginalTxIsolationLevel = setTxIsolationLevelToReadCommitted(sqlConn);
    		
    		SQL.setAutoCommit(sqlConn, false);    		
    		archivedRecords = insertActionResult(sqlConn, sysIds, indexNames);
    		SQL.commit(sqlConn);
    		
    		if (Log.log.isDebugEnabled()) {
    			Log.log.debug(String.format("Inserted %d out of requested %d %s action task results into " +
    										"archive_action_result table from task result indices [%s]", 
    										archivedRecords.size(), sysIds.size(), (isSIRWSArchive ? SIR : NON_SIR),
    										StringUtils.listToString(Arrays.asList(indexNames), ", ")));
    		}
    	} catch (Throwable t) {
    		String errMsg = String.format("Error [%s]in inserting requested %d %s action task results into " +
										  "archive_action_result table from task result indices [%s]", 
										  (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
										  sysIds.size(), (isSIRWSArchive ? SIR : NON_SIR), 
										  StringUtils.listToString(Arrays.asList(indexNames), ", "));
    		Log.log.error(errMsg, t);
    		throw new RuntimeException(errMsg, t);
    	} finally {
    		if(orginalTxIsolationLevel != -1) {
                try {
                    resetTxIsolationLevelToOriginal(sqlConn, orginalTxIsolationLevel);
                } catch (SQLException e) {
                    //ignore.
                }
            }
    		SQL.close(sqlConn);
    	}
    	
    	Log.duration(String.format("Completed Archive %d %s Task Result Documents From Indices [%s]: Archived %d Task " +
    							   "Result Documents", sysIds.size(), (isSIRWSArchive ? SIR : NON_SIR), 
    							   StringUtils.listToString(Arrays.asList(indexNames)), archivedRecords.size()), 
    				 Level.INFO, methodStartTime);
    	
    	return archivedRecords;
    }    
    
    private List<String> archiveWorksheet(List<String> worksheetIds, String index) {
    	long methodStartTime = Log.start(String.format("Starting Archive %d Worksheet Ids In Index %s: ", worksheetIds.size(),
    												   index), Level.INFO);
    	
        SQLConnection sqlConn = null;
        int orginalTxIsolationLevel = -1;
        List<String> archivedWorksheetIds = new ArrayList<String>();
        
        try {
        	sqlConn = SQL.getConnection();
        	orginalTxIsolationLevel = setTxIsolationLevelToReadCommitted(sqlConn);
        	
            // Insert all the relevant sys_id and u_problem ids into the
            // archive_worksheet table.
            int start = 0;
            int limit = 10000;
            int end = start + limit;

            int totalRecords = worksheetIds.size();
            List<Worksheet> worksheets = null;
            
            while (start < totalRecords) {
                if (end > totalRecords) {
                    end = totalRecords;
                }
                
                List<String> subList = worksheetIds.subList(start, end);                
                worksheets =  WorksheetSearchAPI.findWorksheetByIds(subList, "system").getRecords();
                
                if (CollectionUtils.isNotEmpty(worksheets) && worksheets.size() == subList.size()) {	                
	                SQL.setAutoCommit(sqlConn, false);
	                archivedWorksheetIds.addAll(insertWorksheet(sqlConn, worksheets));
	                SQL.commit(sqlConn);
	                
	                if (worksheets != null) {
	                	worksheets.clear();
	                }
                } else {
                	throw new Exception(String.format("Failed to find %d worksheet documents corresponding to %d worksheet" +
                									  "ids from index %s", subList.size(), subList.size(), index));
                }
                
                start += limit;
                end = start + limit;
            }
        } catch (Throwable t) {
        	String errMsg = String.format("Error [%s]in inserting requested %d worksheets into archive_worksheet table", 
					  					  (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
					  					  worksheetIds.size());
        	Log.log.error(errMsg, t);
        	throw new RuntimeException(errMsg, t);
        } finally {
        	if(orginalTxIsolationLevel != -1) {
                try {
                    resetTxIsolationLevelToOriginal(sqlConn, orginalTxIsolationLevel);
                } catch (SQLException e) {
                    //ignore.
                }
            }
            SQL.close(sqlConn);
        }
        
        Log.duration(String.format("Completed Archive %d Worksheet Ids In Index %s: Archived %d Worksheet Documents", 
        						   worksheetIds.size(), index, archivedWorksheetIds.size()), Level.INFO, methodStartTime);
        
        return archivedWorksheetIds;
    } // archiveWorksheet
    
    protected int executeSQL(PreparedStatement stmt) throws SQLException {
        int result = -1;
        try {
            long time = System.currentTimeMillis();
            result = stmt.executeUpdate();
            time = System.currentTimeMillis() - time;
            
            if (Log.log.isDebugEnabled()) {
                Log.log.debug(String.format("%d seconds spent on %d records", (time / 1000), result));
            }
        } catch (SQLException e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        } finally {
            try{
                stmt.close();
            } catch (Exception e) {
                Log.log.warn(e.getMessage(), e);
            }
        }
        
        return result;
    } // executeSQL

    protected long getTimeInMillis(long timeInMillis)
    {
    	/*
    	 * Granularity of all ES daily indices is day.
    	 * 
    	 * ("Start time truncated to start of day"  ms - duration in ms) truncated to start of day ms - 1 ms
    	 */
    	return (Instant.ofEpochMilli((Instant.ofEpochMilli(startTime).truncatedTo(ChronoUnit.DAYS).getEpochSecond() * 1000) - 
    						 		 timeInMillis).truncatedTo(ChronoUnit.DAYS).getEpochSecond() * 1000) - 1;
    }
    
    protected long getTimeInMillis(Duration duration) {
    	/*
    	 * Granularity of all ES daily indices is day.
    	 * 
    	 * "Start time truncated to day" ms - duration in ms - 1 ms
    	 */
    	return (Instant.ofEpochMilli(startTime).truncatedTo(ChronoUnit.DAYS).minus(duration.toMillis(), ChronoUnit.MILLIS)
    			.getEpochSecond() * 1000) - 1;
    }
    
    public boolean isSirOrCorruptedWorksheet(String worksheetId) throws SearchException
    {
        boolean result = true;
        
        try
        {
        	ResponseDTO<Worksheet> wsResponse = WorksheetSearchAPI.findWorksheetById(worksheetId, "system");
        	if (StringUtils.isNotBlank(wsResponse.getData().getSirId()))
        	{
        		result = true;
        	}
        	else
        	{
        		result = false;
        	}
        }
        catch (Throwable e)
        {
        	Log.log.warn("Skip archive corrupted worksheet: " + worksheetId, e);
        }
        
        return result;
    }
    
    /**
     * Selects data from ElasticSearch and inserts into the database.
     *
     * @param conn
     * @param cqlQueries
     * @return
     * @throws SearchException 
     * @throws InterruptedException 
     */
    protected List<String> archiveProcessRequests(SQLConnection conn, List<ProcessRequest> processRequests) throws SearchException, InterruptedException
    {        
    	int count = processRequests.size();
        Log.log.debug("Total " + count + " process requests found for archiving.");
        long processRequestCount = 0;
        //insert PROCESS REQUESTS
        List<String> archivedRecords = insertProcessRequest(conn, processRequests);
        processRequestCount += archivedRecords.size();
        Log.log.debug("Running total of archive_process_request inserted " + processRequestCount + ", total " + count);

        return archivedRecords;
    }
    protected void deleteProcessRequests(List<String> processRequests)
    {
    	List<List<String>> deleteLists = Lists.partition(processRequests, 1000);
    	deleteLists.parallelStream().forEach(docs -> deleteProcessRequestDocs(docs) );
    }
    
    private void deleteProcessRequestDocs(List<String> docs)
    {
    	try
    	{
    		WorksheetIndexAPI.deleteProcessRequests(docs, true, "system");
    	}catch (SearchException e) {
			Log.log.error(e.getMessage(),e);
		}
    }
    
    protected void deleteTaskResults(List<String> taskResultIds)
    {
    	List<List<String>> deleteLists = Lists.partition(taskResultIds, 1000);
    	deleteLists.parallelStream().forEach(docs -> deleteTaskResultDocs(docs) );
    }
    
    private void deleteTaskResultDocs(List <String> docs)
    {
    	try
    	{
    		WorksheetIndexAPI.deleteTaskResults(docs, true, "system");
    	}catch (SearchException e) {
			Log.log.error(e.getMessage(),e);
		}
    }
    
    protected void deleteWorksheet(List<String> worksheetids)
    {
    	List<List<String>> deleteLists = Lists.partition(worksheetids, 1000);
    	deleteLists.parallelStream().forEach(docs -> deleteWorksheetDocs(docs) );
    }

    private void deleteWorksheetDocs(List<String> docs)
    {
    	try
    	{
    		WorksheetIndexAPI.deleteWorksheets(docs, true, "system",false);
    	}catch (SearchException e) {
			Log.log.error(e.getMessage(),e);
		}
    }
    
    protected List<String> insertProcessRequest(SQLConnection conn, List<ProcessRequest> processRequests) {
        int totalRecords = 0;
        List<String> archivedRecords = new ArrayList<String>();
        PreparedStatement insertStatement = null;
        PreparedStatement deleteStatement = null;
        
        try {
            insertStatement = conn.prepareStatement(PROCESS_REQUEST_INSERT_STATEMENT);
            deleteStatement = conn.prepareStatement(PROCESS_REQUEST_DELETE_STATEMENT);
            
            int limit = 1000;
            int start = 0;
            int end = start + limit;
            totalRecords = processRequests.size();
            
            while (start < totalRecords) {
                if (end > totalRecords) {
                    end = totalRecords;
                }
                
                List<ProcessRequest> subList = processRequests.subList(start, end);
                archivedRecords.addAll(safeInsertProcessRequest(conn, insertStatement, subList, deleteStatement));
                insertStatement.clearBatch();
                start += limit;
                end = start + limit;
            }
        } catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        } finally {
            try {
                if (insertStatement != null && !insertStatement.isClosed()) {
                    insertStatement.close();
                }
            } catch (SQLException sqle) {
                Log.log.warn(String.format("Error %sin closing process request insert statement", 
                						   StringUtils.isNotBlank(sqle.getMessage()) ? "[" + sqle.getMessage() + "] ": ""));
            }
            
            try {
                if (deleteStatement != null && !deleteStatement.isClosed()) {
                    deleteStatement.close();
                }
            } catch (SQLException sqle) {
                Log.log.warn(String.format("Error %sin closing process request delete statement", 
                						   StringUtils.isNotBlank(sqle.getMessage()) ? "[" + sqle.getMessage() + "] ": ""));
            }
        }
        
        return archivedRecords;
    }
    
    private void prepareArchivedProcssRequestDeleteStatement(PreparedStatement deleteProcReqStatement, 
			 												 List<ProcessRequest> processRequests) throws SQLException {
    	if(CollectionUtils.isNotEmpty(processRequests)) {
    		for (ProcessRequest processRequest : processRequests) {
    			try {
    				deleteProcReqStatement.setString(1, processRequest.getSysId());

    				deleteProcReqStatement.addBatch();
    			} catch (Exception e) {
    				Log.log.error(String.format("Error %sin preparing delete archived process prepared statement " +
    											"for archived process request with sys id %s and adding to batch",
    											(StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
    											processRequest.getSysId()));
    				throw e;
    			} 
    		}
    	}
    }
    
    private void safeDeleteArchivedProcessRequests(PreparedStatement deleteProcReqStatement, 
			 									   List<ProcessRequest> processRequests) throws SQLException {
    	prepareArchivedProcssRequestDeleteStatement(deleteProcReqStatement, processRequests);
    	
		try {
			deleteProcReqStatement.executeBatch();
		} catch (SQLException e) {
			Log.log.error(String.format("Error %ssafely deleting %d archived process requests from archive_proces_request " +
										"table", (StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""), 
										processRequests.size()));
			throw e;
		}
    }
    
    private void deleteExistingArchivedProcessRequests(List<ProcessRequest> processRequests, 
    												   PreparedStatement deleteStatement) throws SQLException {
    	if (CollectionUtils.isNotEmpty(processRequests)) {
    		try {
    			safeDeleteArchivedProcessRequests(deleteStatement, processRequests);
    		} catch (SQLException e) {
    			Log.log.error(String.format("Error %sin deleting %d existing archived process request records from DB", 
	  					  					(StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
	  					  					processRequests.size()));
    			throw e;
    		}
    	}
    }
    
    /**
     * This method inserts process requests into archive table but if there is duplicate record error then it retries
     * the insert by deleting the batch.
     *
     * @param conn
     * @param totalRecords
     * @param insertStatement
     * @param keys
     * @return
     * @throws SQLException
     * @throws SearchException 
     */
    private List<String> safeInsertProcessRequest(SQLConnection conn, PreparedStatement insertStatement, 
    											  List<ProcessRequest> processRequests,
    											  PreparedStatement deleteStatement) throws SQLException {
        int retry = 0;
        
        List<String> keyList = prepareProcessRequestInsertStatement(insertStatement, processRequests);
        
        while (retry <= 1) {
            if (retry == 1) {
            	conn.setAutoCommit(false);
            	deleteExistingArchivedProcessRequests(processRequests, deleteStatement);
            	conn.commit();
            }

            if (retry <= 1) {
                try {               	
                    insertStatement.executeBatch();
                    break;
                } catch (SQLException e) {
                	
                	String errMsg = String.format("Error %sin inserting %d process requests into archive_process_request " +
                								  "table, retry %d", 
                								  (StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
                								  processRequests.size(), (retry + 1));
                	
                	if (retry == 1) {
                		Log.log.error(errMsg, e);
                		throw e;
                	}
                	
                    Log.log.warn(errMsg);
                    retry++;
                }
            }
        }
        
        return keyList;
    }

    /**
     * This method reads process requests from cassandra and prepare a
     * {@link PreparedStatement} with data that will be inserted into MySQL.
     *
     * @param insertStatement
     * @param keys
     * @throws SQLException
     * @throws SearchException 
     */
    private List<String> prepareProcessRequestInsertStatement(PreparedStatement insertStatement, List<ProcessRequest> processRequests) throws SQLException
    {
    	List<String> keyList = new ArrayList<String>();
        for (ProcessRequest processRequest : processRequests)
        {
            try
            {
                insertStatement.setString(1, processRequest.getSysId());
                // duration was null for some records, is it genuine.
                if (processRequest.getDuration() == null) 
                {
                    insertStatement.setInt(2, 0);
                }
                else
                {
                    insertStatement.setInt(2, processRequest.getDuration());
                }
    
                insertStatement.setString(3, processRequest.getNumber());
                insertStatement.setString(4, processRequest.getStatus());
                insertStatement.setString(5, processRequest.getTimeout());
                insertStatement.setString(6, processRequest.getWiki());
                insertStatement.setString(7, processRequest.getSysCreatedBy());
                insertStatement.setTimestamp(8, new Timestamp(processRequest.getSysCreatedOn()));
                insertStatement.setInt(9, -1); // mod count is unused
                insertStatement.setString(10, processRequest.getSysUpdatedBy());
                insertStatement.setTimestamp(11, new Timestamp(processRequest.getSysUpdatedOn()));
                insertStatement.setString(12, processRequest.getProblem());
                insertStatement.setInt(13, processRequest.getuATExecCount().intValue());
                
                insertStatement.addBatch();
                keyList.add(processRequest.getSysId());
            }
            catch (Exception e)
            {
                Log.log.error("Unable to insert process request record: " + processRequest.getSysId(), e);
            } 
        }
        return keyList;
    }
    
    protected List<String> insertActionResult(SQLConnection conn, List<String> atResultIds, String[] indexNames) {
        List<String> archivedTaskResults = new ArrayList<String>();

        PreparedStatement insertStatement = null;
        PreparedStatement insertStatementLob = null;
        PreparedStatement deleteStatement = null;
        PreparedStatement deleteLobStatement = null;
        
        try {
            insertStatement = conn.prepareStatement(TASK_RESULT_INSERT_STATEMENT);
            insertStatementLob = conn.prepareStatement(TASK_RESULT_LOB_INSERT_STATEMENT);
            deleteStatement = conn.prepareStatement(TASK_RESULT_DELETE_STATEMENT);
            deleteLobStatement = conn.prepareStatement(TASK_RESULT_LOB_DELETE_STATEMENT);

            int limit = 1000;
            int start = 0;
            int end = start + limit;
            int totalRecords = atResultIds.size();
            
            while(start < totalRecords) {
                if(end > totalRecords) {
                    end = totalRecords;
                }
                List<String> subList = atResultIds.subList(start, end);
                archivedTaskResults.addAll(safeInsertActionResult(conn, insertStatement, insertStatementLob, subList, 
                												  indexNames, deleteStatement, deleteLobStatement));
                insertStatement.clearBatch();
                insertStatementLob.clearBatch();
                start += limit;
                end = start + limit;
            }
        } catch (Throwable t) {
        	Log.log.error(String.format("Error [%s]in inserting requested %d action task results into " +
										"archive_action_result and archived_action_result_lob tables from task result " +
										"indices [%s]", 
										(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
										atResultIds, StringUtils.listToString(Arrays.asList(indexNames), ", ")), t);
        } finally {
            try {
                if (insertStatement != null && !insertStatement.isClosed()) {
                    insertStatement.close();
                }
            } catch (SQLException sqle) {
                Log.log.warn(String.format("Error %sin closing task result insert statement",
                						   StringUtils.isNotBlank(sqle.getMessage()) ? "[" + sqle.getMessage() + "] " : ""));
            }
            
            try {
                if (insertStatementLob != null && !insertStatementLob.isClosed()) {
                    insertStatementLob.close();
                }
            }  catch (SQLException sqle) {
                Log.log.warn(String.format("Error %sin closing task result lob insert statement",
						   				   StringUtils.isNotBlank(sqle.getMessage()) ? "[" + sqle.getMessage() + "] " : ""));
            }
            
            try {
                if (deleteStatement != null && !deleteStatement.isClosed()) {
                	deleteStatement.close();
                }
            } catch (SQLException sqle) {
                Log.log.warn(String.format("Error %sin closing task result delete statement",
						   				   StringUtils.isNotBlank(sqle.getMessage()) ? "[" + sqle.getMessage() + "] " : ""));
            }
            
            try {
                if (deleteLobStatement != null && !deleteLobStatement.isClosed()) {
                	deleteLobStatement.close();
                }
            } catch (SQLException sqle) {
                Log.log.warn(String.format("Error %sin closing task result lob delete statement",
		   				   				   StringUtils.isNotBlank(sqle.getMessage()) ? "[" + sqle.getMessage() + "] " : ""));
            }
        }
        
        return archivedTaskResults;
    }
    
    private void prepareArchivedTaskResultDeleteStatement(PreparedStatement deleteTaskResultStatement,
    													  PreparedStatement deleteTaskResultLobStatement, 
    													  List<String> taskResultIds) throws SQLException {
    	if(CollectionUtils.isNotEmpty(taskResultIds)) {
    		for (String taskResultId : taskResultIds) {
    			try {
    				deleteTaskResultStatement.setString(1, taskResultId);
    				deleteTaskResultStatement.addBatch();
    				deleteTaskResultLobStatement.setString(1, taskResultId);
    				deleteTaskResultLobStatement.addBatch();
    			} catch (Exception e) {
    				Log.log.error(String.format("Error %sin preparing delete archived task result statement or delete " +
    											"archived task result lob statement for archived task result with task " +
    											"result id %s and adding to batch",
    											(StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
    											taskResultId));
    				throw e;
    			} 
    		}
    	}
    }
    
    private void safeDeleteArchivedTaskResults(PreparedStatement deleteTaskResulStatement, 
    										   PreparedStatement deleteTaskResulLobStatement,
			   								   List<String> taskResultIds) throws SQLException {
    	prepareArchivedTaskResultDeleteStatement(deleteTaskResulStatement, deleteTaskResulLobStatement, taskResultIds);

    	try {
    		deleteTaskResulStatement.executeBatch();
    		deleteTaskResulLobStatement.executeBatch();
    	} catch (SQLException e) {
    		Log.log.error(String.format("Error %ssafely deleting %d archived task results from archive_action_result or " +
    									"archive_action_result_lob tables", 
    									(StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""), 
    									taskResultIds.size()));
    		throw e;
    	}
    }
    
    private void deleteExistingArchivedTaskResults(List<String> taskResultIds, 
			   									   PreparedStatement deleteStatement,
			   									   PreparedStatement deleteLobStatement) throws SQLException {
    	if (CollectionUtils.isNotEmpty(taskResultIds)) {
    		try {
    			safeDeleteArchivedTaskResults(deleteStatement, deleteLobStatement, taskResultIds);
    		} catch (SQLException e) {
    			Log.log.error(String.format("Error %sin deleting %d existing archived task result records from DB", 
    						  				(StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
    						  				taskResultIds.size()));
    			throw e;
    		}
    	}
    }
    
    /**
     * This method inserts action results into archive table but if there is duplicate record error then it retries
     * the insert by deleting the batch.
     *
     * @param conn
     * @param insertStatement
     * @param processIds
     * @return
     * @throws SQLException
     * @throws SearchException 
     */
    private List<String> safeInsertActionResult(SQLConnection conn, PreparedStatement insertStatement, 
    											PreparedStatement insertStatementLob, List<String> taskResultIds, 
    											String[] indexNames, PreparedStatement deleteStatement,
    											PreparedStatement deleteLobStatement) throws SQLException, SearchException {
        // now get the record from ES we can also make a chunk and call, for now let's not
        // overoptimize it
        List<String> actionResultIds = prepareActionResultInsertStatement(insertStatement, insertStatementLob, taskResultIds, 
        																  indexNames);
        int retry = 0;

        while (retry <= 1) {
            if (retry == 1) {
            	Log.log.info(String.format("Deleting %d records from archive_action_result and archive_action_result_lob" +
                    					   " tables, retry count is %d", actionResultIds.size(), (retry + 1)));
            	conn.setAutoCommit(false);
            	deleteExistingArchivedTaskResults(taskResultIds, deleteStatement, deleteLobStatement);
            	conn.commit();
            }

            if (retry <= 1) {
                try {
                    insertStatement.executeBatch();
                    insertStatementLob.executeBatch();
                    
                    Log.log.info(String.format("Inserted %d records into archive_action_result and " +
                    						   "archive_action_result_lob tables, retry count is %d", 
                    						   actionResultIds.size(), (retry + 1)));
                    break;
                } catch (SQLException e) {
                	String errMsg = String.format("Error %sin inserting %d task results into archive_action_result " +
		  					  					  "or archive_action_result_lob tables, retry %d", 
		  					  					  (StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
		  					  					  taskResultIds.size(), (retry + 1));

                	if (retry == 1) {
                		Log.log.error(errMsg, e);
                		throw e;
                	}

                	Log.log.warn(errMsg);
                	retry++;
                }
            }
        }
        
        return actionResultIds;
    }
    
    @Deprecated
    protected List<String> archiveTaskResultsWS(List<String> worksheetIds) {
    	throw new RuntimeException("this should be implemented in a subclass");
    }

    /**
     * This method inserts action results into archive table but if there is duplicate record error then it retries
     * the insert by deleting the batch.
     *
     * @param conn
     * @param insertStatement
     * @param worksheetIds
     * @return
     * @throws SQLException
     * @throws SearchException 
     */
    @Deprecated
    private List<String> safeInsertActionResultWS(SQLConnection conn, PreparedStatement insertStatement, PreparedStatement insertStatementLob, List<String> worksheetIds) throws SQLException, SearchException
    {
        List<String> result = new ArrayList<String>();
        // now get the record from ES we can also make a chunk and call, for now let's not
        // overoptimize it
        List<String> actionResultIds = prepareActionResultInsertStatementWS(insertStatement, insertStatementLob, worksheetIds);
        int retry = 0;
        PreparedStatement stmt = null;
        while(true)
        {
            if(retry == 1)
            {
                insertStatement.clearBatch();
                insertStatementLob.clearBatch();

                //delete all the records from this batch and retry
                //we will chunk it to be 1000 at a time, some DB like Oracle
                //cannot handle more than 1000 in a IN clause.
                int limit = 100;
                int start = 0;
                int end = start + limit;
                int totalRecords = actionResultIds.size();
                while(start < totalRecords)
                {
                    if(end > totalRecords)
                    {
                        end = totalRecords;
                    }

                    List<String> subResultIds = actionResultIds.subList(start, end);

                    String queryString = SQLUtils.prepareQueryString(subResultIds, dbType);
                    
                    Log.log.info("Deleting archive_action_result ids.. retry: " + retry);
                    Log.log.info("Deleting archive_action_result ids: " + queryString);
                    
                    stmt = conn.prepareStatement("delete from archive_action_result where sys_id in (" + queryString + ")");
                    executeSQL(stmt);

                    stmt = conn.prepareStatement("delete from archive_action_result_lob where sys_id in (" + queryString + ")");
                    executeSQL(stmt);
                    start += limit;
                    end = start + limit;
                }
                
                //remake
                prepareActionResultInsertStatementWS(insertStatement, insertStatementLob, worksheetIds);
            }

            if(retry <= 1)
            {
                try
                {
                    insertStatement.executeBatch();
                    insertStatementLob.executeBatch();
                    // successfully inserted intot eh DB add ids to the result
                    result.addAll(actionResultIds);
                    break;
                }
                catch (SQLException e)
                {
                    Log.log.warn("Error during insert into the action result table: " + e.getMessage());
                    Log.log.warn(e, e);
                    retry++;
                }
            }
            else
            {
                break;
            }
        }
        result.addAll(actionResultIds);
        return result;
    }
    
    @Deprecated
    protected List<String> insertActionResultWS(SQLConnection conn, List<String> worksheetIds)
    {
        List<String> results = new ArrayList<String>();        
        PreparedStatement insertStatement = null;
        PreparedStatement insertStatementLob = null;
        try
        {
            insertStatement = conn.prepareStatement(TASK_RESULT_INSERT_STATEMENT);
            insertStatementLob = conn.prepareStatement(TASK_RESULT_LOB_INSERT_STATEMENT);

            //do not change increase this number because some DB 
            //cannot handle more than this
            int limit = 100;
            int start = 0;
            int end = start + limit;
            int totalRecords = worksheetIds.size();
            while(start < totalRecords)
            {
                if(end > totalRecords)
                {
                    end = totalRecords;
                }
                List<String> subList = worksheetIds.subList(start, end);
                results.addAll(safeInsertActionResultWS(conn, insertStatement, insertStatementLob, subList));
                insertStatement.clearBatch();
                insertStatementLob.clearBatch();
                start += limit;
                end = start + limit;
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                if (insertStatement != null && !insertStatement.isClosed())
                {
                    insertStatement.close();
                }
            }
            catch (SQLException sqle)
            {
                Log.log.warn("Failed to Close Statement: " + sqle.getMessage());
            }
            try
            {
                if (insertStatementLob != null && !insertStatementLob.isClosed())
                {
                    insertStatementLob.close();
                }
            }
            catch (SQLException sqle)
            {
                Log.log.warn("Failed to Close Statement: " + sqle.getMessage());
            }
        }
        return results;
    }
    
    @Deprecated
    private List<String> prepareActionResultInsertStatementWS(PreparedStatement insertStatement, PreparedStatement insertStatementLob, List<String> worksheetIds) throws SQLException, SearchException
    {
        List<String> result = new ArrayList<String>();
        
        for(String processId : worksheetIds)
        {
            QueryDTO queryDTO = new QueryDTO(0, smallTableBlockSize);
            queryDTO.addFilterItem(new QueryFilter("problemId", QueryFilter.EQUALS, processId));
            
            while(true)
            {
                ResponseDTO<TaskResult> response = WorksheetSearchAPI.searchTaskResults(queryDTO, "system");
                List<TaskResult> actionResults = response.getRecords();
                if(actionResults == null || actionResults.size() <= 0)
                {
                    break; //no more record left
                }
                else
                {
                    for (TaskResult actionResult : actionResults)
                    {
                        try
                        {
                            result.add(actionResult.getSysId());
                            insertStatement.setString(1, actionResult.getSysId());
                            insertStatement.setString(2, actionResult.getAddress());
                            insertStatement.setString(3, actionResult.getCompletion());
                            insertStatement.setString(4, actionResult.getCondition());
                            if (actionResult.getDuration() == null) // duration was null for
                                                                    // some records, is it
                                                                    // genuine.
                            {
                                insertStatement.setInt(5, 0);
                            }
                            else
                            {
                                insertStatement.setInt(5, actionResult.getDuration());
                            }
                            insertStatement.setString(6, actionResult.getEsbaddr() == null ? "" : actionResult.getEsbaddr());
                            insertStatement.setString(7, actionResult.getSeverity());
                            if (actionResult.getTimestamp() == null)
                            {
                                insertStatement.setLong(8, 0L);
                            }
                            else
                            {
                                insertStatement.setLong(8, actionResult.getTimestamp());
                            }
                
                            insertStatement.setString(9, actionResult.getSysCreatedBy());
                            long defaultCreatedOn = (actionResult.getSysCreatedOn() == null ? 0 : actionResult.getSysCreatedOn());
                            insertStatement.setTimestamp(10, new Timestamp(defaultCreatedOn));
                            insertStatement.setInt(11, -1); // mod count is unused
                            insertStatement.setString(12, actionResult.getSysUpdatedBy());
                            long defaultUpdatedOn = (actionResult.getSysUpdatedOn() == null ? 0 : actionResult.getSysUpdatedOn());
                            insertStatement.setTimestamp(13, new Timestamp(defaultUpdatedOn));
                            // use the same id as action_result
                            insertStatement.setString(14, actionResult.getSysId()); 
                            //insertStatement.setString(15, actionResult.getExecuteRequestId());
                            insertStatement.setString(15, ""); //no need anymore
                            //insertStatement.setString(16, actionResult.getExecuteResultId());
                            insertStatement.setString(16, ""); //no need anymore
                            insertStatement.setString(17, actionResult.getProblemId());
                            insertStatement.setString(18, actionResult.getProcessId());
                            insertStatement.setString(19, actionResult.getAddress());
                            insertStatement.setString(20, actionResult.getTargetGUID());
                            insertStatement.setString(21, actionResult.getTaskId());
                            insertStatement.setBoolean(22, actionResult.isHidden());
                
                            insertStatement.addBatch();
            
                            // Now the LOB table
                            insertStatementLob.setString(1, actionResult.getSysId()); // use the
                                                                                      // same id
                                                                                      // as
                                                                                      // execute_result
                            //insertStatementLob.setString(2, StringUtils.utf8Conversion(actionResult.getDetail(), false));
                            //insertStatementLob.setString(3, StringUtils.utf8Conversion(actionResult.getSummary(), false));
                            if(actionResult.getDetail() != null)
                            {
                                //since detail is a clob we're going to insert it as bytes because
                                //the characters could be some special unicode characters.
                                if(Log.log.isTraceEnabled())
                                {
                                    Log.log.trace("Detail for : " + actionResult.getSysId() + " >>> : " + actionResult.getDetail());
                                }
        //                        insertStatementLob.setBytes(2, actionResult.getDetail().getBytes());
                                insertStatementLob.setString(2, StringUtils.utf8Conversion(actionResult.getDetail(), false));        
                            }
                            if(actionResult.getSummary() != null)
                            {
                                //since summary is a clob we're going to insert it as bytes because
                                //the characters could be some special unicode characters.
                                if(Log.log.isTraceEnabled())
                                {
                                    Log.log.trace("Summary for : " + actionResult.getSysId() + " >>> : " + actionResult.getSummary());
                                }
        //                        insertStatementLob.setBytes(3, actionResult.getSummary().getBytes());
                                insertStatementLob.setString(3, StringUtils.utf8Conversion(actionResult.getSummary(), false));        
                            }
                            insertStatementLob.setString(4, actionResult.getSysCreatedBy());
                            insertStatementLob.setTimestamp(5, new Timestamp(defaultCreatedOn));
                            insertStatementLob.setInt(6, -1); // mod count is unused
                            insertStatementLob.setString(7, actionResult.getSysUpdatedBy());
                            insertStatementLob.setTimestamp(8, new Timestamp(defaultUpdatedOn));
                            insertStatementLob.addBatch();
                        }
                        catch (Exception e)
                        {
                            Log.log.error("Unable to insert action result record: " + actionResult.getSysId(), e);
                            Log.log.error(e,e);
                        } 
                    }
                    
                    //go to next page
                    queryDTO.setStart(queryDTO.getStart() + queryDTO.getLimit());
                }
            }
        }
        return result;
    }
    
    private List<String> prepareActionResultInsertStatement(PreparedStatement insertStatement, 
    														PreparedStatement insertStatementLob, List<String> sysIds, 
    														String[] indexNames) throws SQLException, SearchException
    {
        List<String> result = new ArrayList<String>();       
        int loopCount = sysIds.size() / smallTableBlockSize + ((sysIds.size() % smallTableBlockSize) != 0 ? 1 : 0);
        
        if (Log.log.isDebugEnabled()) {
        	Log.log.debug(String.format("%d action results in daily action result indices [%s], loop count is %d",
        							    sysIds.size(), StringUtils.listToString(Arrays.asList(indexNames), ", "),
        							    loopCount));
        }
        
        for (int i = 0; i < loopCount; i++) {
	        QueryDTO queryDTO = new QueryDTO(0, smallTableBlockSize);
	        String scrollId = null;
	        TimeValue scrollTimeOut = new TimeValue(WorksheetSearchAPI.getScrollCtxKeepAliveDur(), TimeUnit.MINUTES);
	            
	        try {
	        	QueryFilter termsQryFltr = new QueryFilter(QueryFilter.TERMS_TYPE, 
	        											   StringUtils.listToString(
	        													   sysIds.subList(i * smallTableBlockSize, 
	        															   		  ((((i+1) * smallTableBlockSize) > 
	        															   		    sysIds.size()) ? 
	        															   		   (sysIds.size() % smallTableBlockSize) :
	        															   		   ((i+1) * smallTableBlockSize))), ","), 
													   	   "sysId", QueryFilter.EQUALS);
	        	QueryBuilder termsQryBldr = SearchUtils.createTermsQueryBuilder(termsQryFltr);
	        	
	        	Pair<ResponseDTO<TaskResult>, String> scrollTaskResults = 
	        												WorksheetSearchAPI.searchTaskResults(queryDTO, termsQryBldr, 
	        																					 indexNames, UserUtils.SYSTEM,
	        																					 scrollId, scrollTimeOut);
	        	
	            while(scrollTaskResults != null && scrollTaskResults.getLeft() != null && 
	            	  scrollTaskResults.getLeft().isSuccess() && 
	            	  CollectionUtils.isNotEmpty(scrollTaskResults.getLeft().getRecords())) {
	            	scrollId = StringUtils.isNotBlank(scrollTaskResults.getRight()) ? scrollTaskResults.getRight() : null;
	            	
	                List<TaskResult> actionResults = scrollTaskResults.getLeft().getRecords();
	                
	                if(actionResults != null && actionResults.size() > 0) {
	                    for (TaskResult actionResult : actionResults) {
	                        try {
	                            insertStatement.setString(1, actionResult.getSysId());
	                            insertStatement.setString(2, actionResult.getAddress());
	                            insertStatement.setString(3, actionResult.getCompletion());
	                            insertStatement.setString(4, actionResult.getCondition());
	                            
	                            // duration was null for some records, is it genuine.
	                            
	                            if (actionResult.getDuration() == null) {
	                                insertStatement.setInt(5, 0);
	                            } else {
	                                insertStatement.setInt(5, actionResult.getDuration());
	                            }
	                            
	                            insertStatement.setString(6, actionResult.getEsbaddr() == null ? 
	                            						  "" : actionResult.getEsbaddr());
	                            insertStatement.setString(7, actionResult.getSeverity());
	                            
	                            if (actionResult.getTimestamp() == null) {
	                                insertStatement.setLong(8, 0L);
	                            } else {
	                                insertStatement.setLong(8, actionResult.getTimestamp());
	                            }
	                
	                            insertStatement.setString(9, actionResult.getSysCreatedBy());
	                            long defaultCreatedOn = (actionResult.getSysCreatedOn() == null ? 
	                            						 0 : actionResult.getSysCreatedOn());
	                            insertStatement.setTimestamp(10, new Timestamp(defaultCreatedOn));
	                            insertStatement.setInt(11, -1); // mod count is unused
	                            insertStatement.setString(12, actionResult.getSysUpdatedBy());
	                            long defaultUpdatedOn = (actionResult.getSysUpdatedOn() == null ? 
	                            						 0 : actionResult.getSysUpdatedOn());
	                            insertStatement.setTimestamp(13, new Timestamp(defaultUpdatedOn));
	                            // use the same id as action_result
	                            insertStatement.setString(14, actionResult.getSysId()); 
	                            //insertStatement.setString(15, actionResult.getExecuteRequestId());
	                            insertStatement.setString(15, ""); //no need anymore
	                            //insertStatement.setString(16, actionResult.getExecuteResultId());
	                            insertStatement.setString(16, ""); //no need anymore
	                            insertStatement.setString(17, actionResult.getProblemId());
	                            insertStatement.setString(18, actionResult.getProcessId());
	                            String truncatedAddress = StringUtils.isNotBlank(actionResult.getAddress()) && 
	                            						  actionResult.getAddress().length() > 32 ?
	                            						  actionResult.getAddress().substring(0, 32) : 
	                            						  actionResult.getAddress();
	                            insertStatement.setString(19, truncatedAddress);
	                            insertStatement.setString(20, actionResult.getTargetGUID());
	                            insertStatement.setString(21, actionResult.getTaskId());
	                            insertStatement.setBoolean(22, actionResult.isHidden());
	                
	                            insertStatement.addBatch();
	            
	                            // Now the LOB table
	                            insertStatementLob.setString(1, actionResult.getSysId()); // use the
	                                                                                      // same id
	                                                                                      // as
	                                                                                      // execute_result
	                            //insertStatementLob.setString(2, StringUtils.utf8Conversion(actionResult.getDetail(), false));
	                            //insertStatementLob.setString(3, StringUtils.utf8Conversion(actionResult.getSummary(), false));
	                            if(actionResult.getDetail() != null) {
	                                //since detail is a clob we're going to insert it as bytes because
	                                //the characters could be some special unicode characters.
	                                if(Log.log.isTraceEnabled()) {
	                                    Log.log.trace("Detail for : " + actionResult.getSysId() + " >>> : " + 
	                                    			  actionResult.getDetail());
	                                }
	                                
	                                insertStatementLob.setString(2, 
	                                							 StringUtils.utf8Conversion(actionResult.getDetail(), false));        
	                            }
	                            
	                            if(actionResult.getSummary() != null) {
	                                //since summary is a clob we're going to insert it as bytes because
	                                //the characters could be some special unicode characters.
	                                if(Log.log.isTraceEnabled()) {
	                                    Log.log.trace("Summary for : " + actionResult.getSysId() + " >>> : " + 
	                                    			  actionResult.getSummary());
	                                }
	                                
	                                insertStatementLob.setString(3, 
	                                							 StringUtils.utf8Conversion(actionResult.getSummary(), false));        
	                            }
	                            
	                            insertStatementLob.setString(4, actionResult.getSysCreatedBy());
	                            insertStatementLob.setTimestamp(5, new Timestamp(defaultCreatedOn));
	                            insertStatementLob.setInt(6, -1); // mod count is unused
	                            insertStatementLob.setString(7, actionResult.getSysUpdatedBy());
	                            insertStatementLob.setTimestamp(8, new Timestamp(defaultUpdatedOn));
	                            insertStatementLob.addBatch();
	                            result.add(actionResult.getSysId());
	                        } catch (Throwable t) {
	                            Log.log.error(String.format("Error %sin inserting action task result record for sys_id %s",
	                            							(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""), 
	                            							actionResult.getSysId()), t);
	                        } 
	                    }
	                }
	                
	                scrollTaskResults = null;
	                
	                if (StringUtils.isNotBlank(scrollId)) {
	                	//Scroll
	                	scrollTaskResults = WorksheetSearchAPI.searchTaskResults(queryDTO, termsQryBldr, 
																 				indexNames, UserUtils.SYSTEM,
																 				scrollId, scrollTimeOut);
	                }
	            }
	            
	            if (scrollTaskResults != null) {
	            	scrollId = StringUtils.isNotBlank(scrollTaskResults.getRight()) ? scrollTaskResults.getRight() : null;
	            	
	            	if (StringUtils.isNotBlank(scrollId)) {
						WorksheetSearchAPI.clearTaskResultScroll(scrollId);
						scrollId = null;
					}
	            }
	        } catch (Throwable t) {
	        	Log.log.error(String.format("Error %sin preparing batch statement for archiving %d task results from " +
	        								" task result indices [%s]", 
	        								(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
	        								sysIds.size(), StringUtils.listToString(Arrays.asList(indexNames), ", "), indexNames), 
	        								t);
	        } finally {
	        	if (StringUtils.isNotBlank(scrollId)) {
					WorksheetSearchAPI.clearTaskResultScroll(scrollId);
				}
	        }
        }
        
        return result;
    }

    protected List<String> insertWorksheet(SQLConnection conn, List<Worksheet> worksheets) throws SearchException
    {
        int totalRecords = 0;
        long cleanupTime = getTimeInMillis(cleanup);
        List<String> archivedWorksheets = new ArrayList<String>();

        if (CollectionUtils.isEmpty(worksheets)) {
        	return archivedWorksheets;
        }
        
        if (worksheets.size() > 0) {
            for(int i = worksheets.size()-1; i>=0; i--) {
                long openProcessRequestsCount = getOpenProcessRequestsCountByWorksheetId(worksheets.get(i),cleanupTime);  
                if(openProcessRequestsCount > 0) {
                    Log.log.info("There are open process requests for worksheet id: " + worksheets.get(i).getSysId());
                    worksheets.remove(i);
                }
            }
            
            PreparedStatement insertStatement = null;
            PreparedStatement deleteStatement = null;
            
            try
            {
                insertStatement = conn.prepareStatement(WORKSHEET_INSERT_STATEMENT);
                deleteStatement = conn.prepareStatement(WORKSHEET_DELETE_STATEMENT);
                
                int limit = 1000;
                int start = 0;
                int end = start + limit;
                totalRecords = worksheets.size();
                
                while(start < totalRecords) {
                    if(end > totalRecords) {
                        end = totalRecords;
                    }
                    List<Worksheet> subList = worksheets.subList(start, end);
                    archivedWorksheets.addAll(safeInsertWorksheet(conn, insertStatement, subList, deleteStatement));
                    insertStatement.clearBatch();
                    
                    start += limit;
                    end = start + limit;
                }
            } catch (Exception e) {
                Log.log.error(e.getMessage(), e);
            } finally {
                try {
                    if (insertStatement != null && !insertStatement.isClosed())  {
                        insertStatement.close();
                    }
                } catch (SQLException sqle) {
                	Log.log.warn(String.format("Error %sin closing worksheet insert statement", 
 						   					   StringUtils.isNotBlank(sqle.getMessage()) ? 
 						   					   "[" + sqle.getMessage() + "] ": ""));
                }
                
                try {
                    if (deleteStatement != null && !deleteStatement.isClosed()) {
                        deleteStatement.close();
                    }
                } catch (SQLException sqle) {
                    Log.log.warn(String.format("Error %sin closing worksheet delete statement", 
                    						   StringUtils.isNotBlank(sqle.getMessage()) ? 
                    						   "[" + sqle.getMessage() + "] ": ""));
                }
            }
        }
        
        return archivedWorksheets;
    }

    private void prepareArchivedWorksheetDeleteStatement(PreparedStatement deleteWorksheetStatement, 
			 											 List<Worksheet> worksheets) throws SQLException {
    	if(CollectionUtils.isNotEmpty(worksheets)) {
    		for (Worksheet worksheet : worksheets) {
    			try {
    				deleteWorksheetStatement.setString(1, worksheet.getSysId());

    				deleteWorksheetStatement.addBatch();
    			} catch (Exception e) {
    				Log.log.error(String.format("Error %sin preparing delete worksheet prepared statement " +
    											"for archived worksheet with sys id %s and adding to batch",
    											(StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
    											worksheet.getSysId()));
    				throw e;
    			} 
    		}
    	}
    }

    private void safeDeleteArchivedWorksheets(PreparedStatement deleteWorksheetStatement, 
    										  List<Worksheet> worksheets) throws SQLException {
    	prepareArchivedWorksheetDeleteStatement(deleteWorksheetStatement, worksheets);

    	try {
    		deleteWorksheetStatement.executeBatch();
    	} catch (SQLException e) {
    		Log.log.error(String.format("Error %ssafely deleting %d archived worksheets from archive_worksheet " +
    									"table", (StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""), 
    									worksheets.size()));
    		throw e;
    	}
    }

	private void deleteExistingArchivedWorksheets(List<Worksheet> worksheets, 
												  PreparedStatement deleteStatement) throws SQLException {
		if (CollectionUtils.isNotEmpty(worksheets)) {
			try {
				safeDeleteArchivedWorksheets(deleteStatement, worksheets);
			} catch (SQLException e) {
				Log.log.error(String.format("Error %sin deleting %d existing archived worksheet records from DB", 
											(StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
											worksheets.size()));
				throw e;
			}
		}
	}
    
    /**
     * This method inserts worksheet into archive table but if there is duplicate record error then it retries
     * the insert by deleting the batch.
     *
     * @param conn
     * @param totalRecords
     * @param insertStatement
     * @param worksheets
     * @return
     * @throws SQLException
     * @throws SearchException 
     */
    private List<String> safeInsertWorksheet(SQLConnection conn, PreparedStatement insertStatement, 
    										 List<Worksheet> worksheets, 
    										 PreparedStatement deleteStatement) throws SQLException {
        int retry = 0;
   	 	
   	 	List<String> keys = prepareWorksheetInsertStatement(insertStatement, worksheets);
   	 
        while (retry <= 1) {
            if (retry == 1) {
            	conn.setAutoCommit(false);
            	deleteExistingArchivedWorksheets(worksheets, deleteStatement);
            	conn.commit();
            }

            if (retry <= 1) {
                try {
                    insertStatement.executeBatch();
                    break;
                } catch (SQLException e) {
                	String errMsg = String.format("Error %sin inserting %d worksheets into archive_worksheet " +
							  					  "table, retry %d", 
							  					  (StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
							  					  worksheets.size(), (retry + 1));

                	if (retry == 1) {
                		Log.log.error(errMsg, e);
                		throw e;
                	}

                	Log.log.warn(errMsg);
                	retry++;
                }
            }
        }
        
        return keys;
    }

    /**
     * This method reads process requests from cassandra and prepare a
     * {@link PreparedStatement} with data that will be inserted into MySQL.
     *
     * @param insertStatement
     * @param worksheetIds
     * @throws SQLException
     * @throws SearchException 
     */
    private List<String> prepareWorksheetInsertStatement(PreparedStatement insertStatement, List<Worksheet> worksheets) throws SQLException
    {
    	List<String> keys = new ArrayList<String>();
    	
        if(worksheets != null)
        {
            for (Worksheet worksheet : worksheets)
            {
                try
                {
                    insertStatement.setString(1, worksheet.getSysId());
                    insertStatement.setString(2, worksheet.getAlertId());
                    insertStatement.setString(3, worksheet.getCondition());
                    insertStatement.setString(4, worksheet.getCorrelationId());
                    insertStatement.setString(5, worksheet.getDebug());
                    insertStatement.setString(6, worksheet.getDescription());
                    insertStatement.setString(7, worksheet.getNumber());
                    insertStatement.setString(8, worksheet.getReference());
                    insertStatement.setString(9, worksheet.getSeverity());
                    insertStatement.setString(10, worksheet.getSummary());
                    insertStatement.setString(11, worksheet.getWorkNotes());
                    insertStatement.setString(12, worksheet.getWorksheet());
                    insertStatement.setString(13, worksheet.getAssignedTo());
                    insertStatement.setString(14, worksheet.getAssignedToName());
                    insertStatement.setString(15, worksheet.getSysCreatedBy());
                    if (worksheet.getSysCreatedOn() == null)
                    {
                        insertStatement.setTimestamp(16, new Timestamp(0L));
                    }
                    else
                    {
                        insertStatement.setTimestamp(16, new Timestamp(worksheet.getSysCreatedOn()));
                    }
                    insertStatement.setInt(17, -1); // mod count is unused
                    insertStatement.setString(18, worksheet.getSysUpdatedBy());
                    if (worksheet.getSysUpdatedOn() == null)
                    {
                        insertStatement.setTimestamp(19, new Timestamp(0L));
                    }
                    else
                    {
                        insertStatement.setTimestamp(19, new Timestamp(worksheet.getSysUpdatedOn()));
                    }
                    insertStatement.setString(20, worksheet.getSirId());
                    String worksheetOrgId = worksheet.getSysOrg();
                    insertStatement.setString(21, (worksheetOrgId != null)? worksheetOrgId: "");
                    insertStatement.addBatch();
                    keys.add(worksheet.getSysId());
                }
                catch (Exception e)
                {
                    Log.log.error("Unable to insert worksheet record: " + worksheet.getSysId(), e);
                } 
            }
        }
        
        return keys;
    }
    
    protected long getOpenProcessRequestsCountByWorksheetId(Worksheet ws,long cleanupTime) throws SearchException
    {
        return WorksheetSearchAPI.getOpenProcessRequestsCount(ws, null, "system",cleanupTime);
    }
    
    protected long getOpenProcessRequestsCountByWorksheetId(String worksheetId, Worksheet ws) throws SearchException
    {
        return WorksheetSearchAPI.getOpenProcessRequestsCount(worksheetId, null, "system");
    }
    
    protected List<TaskResult> findTaskResultsByUpdatedOn(QueryDTO queryDTO, Set<String> ignoreIds, long starttime, long endTime, long cleanupTime, String[] indexNames) throws SearchException
    {
    	return WorksheetSearchAPI.findArchiveTaskResultIdsByUpdatedOn(queryDTO, ignoreIds, starttime, endTime, cleanupTime, "system", indexNames);
    }
    
    protected List<Worksheet> findWorksheetsByUpdatedOn(QueryDTO queryDTO, long startTime, long endTime, long cleanupTime, 
    													int blockSize, int start, Set<String> ignoreIds, String[] indexNames,
    													boolean isSIRWSArchive) throws SearchException {
        return WorksheetSearchAPI.findArchiveWorksheetsByUpdatedOn(startTime, endTime, cleanupTime, "system", queryDTO, 
        														   ignoreIds, indexNames, isSIRWSArchive);
    }
    
    private List<String> archiveProcessInternal(List<ProcessRequest> processRequests, String[] indexNames,
    											boolean isSIRWSArchive) {
    	long methodStartTime = Log.start(String.format("Starting Archive %d %s Process Request Documents From Indices [%s]: ", 
		    										   processRequests.size(), (isSIRWSArchive ? SIR : NON_SIR),
		    										   StringUtils.listToString(Arrays.asList(indexNames))), Level.INFO);
    	
    	List<String> archivedProcessRequests = new ArrayList<String>();       
    	SQLConnection sqlConn = null;
    	int orginalTxIsolationLevel = -1;

    	try {
    		sqlConn = SQL.getConnection();
    		orginalTxIsolationLevel = setTxIsolationLevelToReadCommitted(sqlConn);

    		SQL.setAutoCommit(sqlConn, false);
    		archivedProcessRequests = archiveProcessRequests(sqlConn, processRequests);
    		SQL.commit(sqlConn);

    		if (Log.log.isDebugEnabled()) {
    			Log.log.debug(String.format("Inserted %d out of requested %d %s process requests into " +
											"archive_process_request table from process request indices [%s]", 
											archivedProcessRequests.size(), processRequests.size(), 
											(isSIRWSArchive ? SIR : NON_SIR), 
											StringUtils.listToString(Arrays.asList(indexNames), ", ")));
    		}
    	} catch (Throwable t) {
    		String errMsg = String.format("Error [%s]in inserting requested %d %s process requests into " +
						  				  "archive_process_request table from process request indices [%s]", 
						  				  (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
						  				  processRequests.size(), (isSIRWSArchive ? SIR : NON_SIR), 
						  				  StringUtils.listToString(Arrays.asList(indexNames), ", "));
    		Log.log.error(errMsg, t);
    		throw new RuntimeException(errMsg, t);
    	} finally {
    		if(orginalTxIsolationLevel != -1) {
    			try {
    				resetTxIsolationLevelToOriginal(sqlConn, orginalTxIsolationLevel);
    			} catch (SQLException e) {
    				//ignore.
    			}
    		}
    		SQL.close(sqlConn);
    	}

    	Log.duration(String.format("Completed Archive %d %s Process Request Documents From Indices [%s]: Archived %d " +
    							   "Process Request Documents", processRequests.size(), (isSIRWSArchive ? SIR : NON_SIR), 
				   				   StringUtils.listToString(Arrays.asList(indexNames)), archivedProcessRequests.size()), 
    				 Level.INFO, methodStartTime);
    	
    	return archivedProcessRequests;
    }

    @SuppressWarnings("unchecked")
	public static Integer getArchiveCount()
    {
        String mapName = CONCURRENT_ARCHIVE_NUMBER;
        Map<String,Object> obj = (Map<String,Object>)HibernateUtil.getJavaObjectOnly(mapName);
        if (obj==null)
        {
            return null;
        }
        long time = (Long)obj.get("TIME");
        int num = (Integer)obj.get("COUNT");
        long now = System.currentTimeMillis();
        if (now - time > 1000*60*2 || num < 1)  // 2 minutes
        {
            return null; // over two minutes, assume it's dead
        }
        
        return num;
    }


//    private static List<String> splitIndexByNames(List<String> indexNames,long expirytime,String prefix)
//    {
//    	Pattern p = Pattern.compile("[a-zA-Z]+_[0-9]+");
//    	Date date = new Date(expirytime);
//    	String sdfDate= new SimpleDateFormat("yyyyMMdd").format(date);
//    	int intDate = Integer.valueOf(sdfDate);
//    	return indexNames.stream()
//    			.filter(p.asPredicate())
//    			.filter(index -> index.contains(prefix))
//    			.filter(index -> intDate >= Integer.valueOf(index.split("_")[1]))
//    			.sorted()
//    			.collect(Collectors.toList());
//    }
    
    /*
     * This method prepares a Worksheet Data
     * {@link PreparedStatement} with data that will be inserted into archive_worksheet_data table.
     *
     * @param insertWSDATAStatement
     * @param worksheetIds
     * @throws SQLException
     * @throws SearchException 
     */
    private void prepareWorksheetDataInsertStatement(PreparedStatement insertWSDATAStatement, 
    												 List<WorksheetData> worksheetDatas) throws SQLException
    {
        if(worksheetDatas != null)
        {
            for (WorksheetData worksheetData : worksheetDatas)
            {
                try
                {
                    insertWSDATAStatement.setString(1, worksheetData.getWorksheetId());
                    insertWSDATAStatement.setString(2, worksheetData.getPropertyName());
                    insertWSDATAStatement.setBytes(3, worksheetData.getPropertyValue());
                    insertWSDATAStatement.setString(4, worksheetData.getSysCreatedBy());
                    Long createdOnTime = worksheetData.getSysCreatedOn() == null ? 0L : worksheetData.getSysCreatedOn();
                    insertWSDATAStatement.setTimestamp(5, new Timestamp(createdOnTime));
                    insertWSDATAStatement.setInt(6, 0); // mod count is unused
                    insertWSDATAStatement.setString(7, worksheetData.getSysUpdatedBy());
                    Long updatedOnTime = worksheetData.getSysUpdatedOn() == null ? 0L : worksheetData.getSysUpdatedOn();
                    insertWSDATAStatement.setTimestamp(8, new Timestamp(updatedOnTime));
                    insertWSDATAStatement.setString(9, StringUtils.isNotBlank(worksheetData.getSysOrg()) ? 
                    														  worksheetData.getSysOrg() : "");
                    insertWSDATAStatement.setString(10, Guid.getGuid(true));
                    
                    insertWSDATAStatement.addBatch();
                } catch (Exception e) {
                    Log.log.error(String.format("Unable to insert worksheet data record: %s", worksheetData.getSysId()), e);
                } 
            }
        }
    }
    
    private void prepareWorksheetDataDeleteStatement(PreparedStatement deleteWSDATAStatement, 
			 										 List<WorksheetData> worksheetDatas) throws SQLException {
    	if(CollectionUtils.isNotEmpty(worksheetDatas)) {
    		for (WorksheetData worksheetData : worksheetDatas) {
    			try {
    				deleteWSDATAStatement.setString(1, worksheetData.getWorksheetId());
    				deleteWSDATAStatement.setString(2, worksheetData.getPropertyName());

    				deleteWSDATAStatement.addBatch();
    			} catch (Exception e) {
    				Log.log.error(String.format("Unable to delete worksheet data record: %s", worksheetData.getSysId()), e);
    			} 
    		}
    	}
    }
    
    private void safeDeleteWorksheetData(SQLConnection conn, PreparedStatement deleteWSDATAStatement, 
			 									 List<WorksheetData> worksheetDatas) throws SQLException, SearchException {
    	prepareWorksheetDataDeleteStatement(deleteWSDATAStatement, worksheetDatas);
    	int retry = 0;

    	while(retry <= 1) {
			try {
				deleteWSDATAStatement.executeBatch();
				break;
			} catch (SQLException e) {
				Log.log.warn(String.format("Error [%s] during delete from archive_worksheet_data table retry %d",
										   e.getMessage(), (retry + 1)));
				retry++;
			}
    	}
    }
    
    private void deleteExistingArchivedWorksheetData(List<WorksheetData> worksheetDatas, SQLConnection conn) throws Throwable {
    	if (CollectionUtils.isNotEmpty(worksheetDatas)) {
    		PreparedStatement deleteStatement = null;
    		
    		try {
    			deleteStatement = conn.prepareStatement(WORKSHEET_DATA_DELETE_STATEMENT);
    			
    			int limit = 1000;
                int start = 0;
                int end = start + limit;
                
    			while(start < worksheetDatas.size()) {
                    if(end > worksheetDatas.size()) {
                        end = worksheetDatas.size();
                    }
                    
                    List<WorksheetData> subList = worksheetDatas.subList(start, end);
                    
                    conn.setAutoCommit(false);
                    safeDeleteWorksheetData(conn, deleteStatement, subList);
                    conn.commit();
                    
                    deleteStatement.clearBatch();
                    
                    start += limit;
                    end = start + limit;
                }
    		} catch (Throwable t) {
    			Log.log.error(String.format("Error %sin deleting %d worksheet data records from DB", 
	  					  					(StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
	  					  					worksheetDatas.size()), t);
    			throw t;
    		} finally {
                try {
                    if (deleteStatement != null && !deleteStatement.isClosed()) {
                    	deleteStatement.close();
                    }
                } catch (SQLException sqle) {
                    Log.log.warn(String.format("Failed to close delete worksheet data statement: %s", sqle.getMessage()));
                }
            }
    	}
    }
    
    /*
     * This method inserts worksheet data into archive table but if there is duplicate record error then it retries
     * the insert by deleting the batch.
     *
     * @param conn
     * @param totalRecords
     * @param insertStatement
     * @param worksheets
     * @return
     * @throws SQLException
     * @throws SearchException 
     */
    private List<String> safeInsertWorksheetData(SQLConnection conn, PreparedStatement insertWSDATAStatement, 
    											 List<WorksheetData> worksheetDatas) throws SQLException, SearchException
    {
        prepareWorksheetDataInsertStatement(insertWSDATAStatement, worksheetDatas);
        int retry = 0;
        
   	 	final List<String> wsDataESPKs = new CopyOnWriteArrayList<String>();
   	 	   	 	
        while (retry <= 1) {
        	try {
        		deleteExistingArchivedWorksheetData(worksheetDatas, conn);
        	} catch (Throwable t) {
        		throw new SearchException("Error in deleting archived worksheet data for one or more worsheet datas passed" +
        								  " for safe insert");
        	}

            if(retry <= 1) {
                try {
                	insertWSDATAStatement.executeBatch();
                    //successfully inserted into DB return ES worksheet ids 
                	worksheetDatas.parallelStream()
                	.forEach(worksheetData -> wsDataESPKs.add(worksheetData.getWorksheetId() + 
                											  worksheetData.getPropertyName()));
                    break;
                } catch (SQLException e) {
                    Log.log.warn(String.format("Error during insert into the archive_worksheet_data table: %s",
                    						   e.getMessage()));
                    retry++;
                }
            }
        }
        
        return wsDataESPKs;
    }
       
    protected List<String> insertWorksheetData(SQLConnection conn, 
    										   List<WorksheetData> worksheetDatas) throws SearchException {
        int totalRecords = 0;
        List<String> archivedWorksheetDataIds = new ArrayList<String>(); // Worksheet Id + Property Name

        if (CollectionUtils.isNotEmpty(worksheetDatas)) {
            PreparedStatement insertStatement = null;
            
            try
            {
                insertStatement = conn.prepareStatement(WORKSHEET_DATA_INSERT_STATEMENT);
                
                int limit = 1000;
                int start = 0;
                int end = start + limit;
                totalRecords = worksheetDatas.size();
                
                while(start < totalRecords) {
                    if(end > totalRecords) {
                        end = totalRecords;
                    }
                    
                    List<WorksheetData> subList = worksheetDatas.subList(start, end);
                    archivedWorksheetDataIds.addAll(safeInsertWorksheetData(conn, insertStatement, subList));
                    insertStatement.clearBatch();
                    
                    start += limit;
                    end = start + limit;
                }
            } catch (Throwable t) {
            	String errMsg = String.format("Error %sin inserting %d worksheet data records into DB", 
											  (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
											  worksheetDatas.size());
            	
                Log.log.error(errMsg, t);
                throw new SearchException(errMsg, t);
            } finally {
                try {
                    if (insertStatement != null && !insertStatement.isClosed()) {
                        insertStatement.close();
                    }
                } catch (SQLException sqle) {
                    Log.log.warn("Failed to Close Insert Worksheet Data Statement: " + sqle.getMessage());
                }
            }
        }
        
        return archivedWorksheetDataIds;
    }
    
    private Map<Pair<String, String>, List<String>> archiveWorksheetsData(List<String> worksheetIds,
    																	  boolean isSIRWSArchive) {
        long methodStartTime = Log.start(String.format("Starting Archive Worksheet Data for %d %s Worksheet Ids: ", 
        											   worksheetIds.size(), (isSIRWSArchive ? SIR : NON_SIR)), Level.INFO);
        
        SQLConnection sqlConn = null;
        int orginalTxIsolationLevel = -1;
        List<String> archivedWorksheetDataIds = new ArrayList<String>();
        
        try {
            int start = 0;
            int limit = 2000;
            int end = start + limit;

            int totalRecords = worksheetIds.size();
            List<WorksheetData> worksheetDatas = null;
            sqlConn = SQL.getConnection();
            orginalTxIsolationLevel = setTxIsolationLevelToReadCommitted(sqlConn);
            
            while(start < totalRecords) {
                if(end > totalRecords) {
                    end = totalRecords;
                }
                
                List<String> subList = worksheetIds.subList(start, end);
                worksheetDatas =  WorksheetSearchAPI.findWorksheetDataByWorksheetIds(subList).getRecords();
                SQL.setAutoCommit(sqlConn, false);
                archivedWorksheetDataIds.addAll(insertWorksheetData(sqlConn, worksheetDatas));
                SQL.commit(sqlConn);
                worksheetDatas.clear();
                start += limit;
                end = start + limit;
            }
        } catch (Throwable t) {
        	String errMsg = String.format("Error [%s]in inserting requested worksheet data for %d %s worksheet ids into " +
        								  "archive_worksheet_data table", 
					  					  (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
					  					  worksheetIds.size(), (isSIRWSArchive ? SIR : NON_SIR));
        	Log.log.error(errMsg, t);
        	throw new RuntimeException(errMsg, t);
        } finally {
        	if(orginalTxIsolationLevel != -1) {
                try {
                    resetTxIsolationLevelToOriginal(sqlConn, orginalTxIsolationLevel);
                } catch (SQLException e) {
                    //ignore.
                }
            }
        	
            SQL.close(sqlConn);
        }
        
        Log.duration(String.format("Completed Archive Worksheet Data for %d %s Worksheet Ids: Archived %d Worksheet Data " +
        						   "Documents", worksheetIds.size(), (isSIRWSArchive ? SIR : NON_SIR), 
        						   archivedWorksheetDataIds.size()), 
        			 Level.INFO, methodStartTime);
        
        Map<Pair<String, String>, List<String>> delArchivedWSDATAReqs = new HashMap<Pair<String, String>, List<String>>();
        
        if (CollectionUtils.isNotEmpty(archivedWorksheetDataIds)) {
        	delArchivedWSDATAReqs.put(Pair.of(SearchConstants.INDEX_NAME_WORKSHEET_DATA, 
        								      SearchConstants.DOCUMENT_TYPE_WORKSHEET_DATA), archivedWorksheetDataIds);
        }
        
        return delArchivedWSDATAReqs;
    } // archiveWorksheetData
    
    protected Pair<List<TaskResult>, String> findTaskResultsByUpdatedOn(QueryDTO queryDTO, Set<String> ignoreIds, 
    																	long starttime, long endTime, long cleanupTime, 
    																	String[] indexNames, String scrollId,
    																	TimeValue scrollTimeOut) throws SearchException {
    	if ((scrollTimeOut == null) || (scrollTimeOut.getMillis() <= 0l)) {
    		throw new SearchException("Scroll time out has to be positive, non-zero value");
    	}
    	
    	return WorksheetSearchAPI.findArchiveTaskResultIdsByUpdatedOn(queryDTO, ignoreIds, starttime, endTime, cleanupTime, 
    																  "system", indexNames, scrollId, scrollTimeOut);
    }
    
    protected Pair<List<ProcessRequest>, String> findProcessRequestsByUpdatedOn(QueryDTO queryDTO, Set<String> ignoreIds, 
																				long starttime, long endTime, long cleanupTime, 
																				String[] indexNames, String scrollId,
																				TimeValue scrollTimeOut) 
																				throws SearchException {
    	if ((scrollTimeOut == null) || (scrollTimeOut.getMillis() <= 0l)) {
    		throw new SearchException("Scroll time out has to be positive, non-zero value");
    	}
    	
    	return WorksheetSearchAPI.findArchiveProcessRequestsByUpdatedOn(queryDTO, ignoreIds, starttime, endTime, cleanupTime, 
				  													  "system", indexNames, scrollId, scrollTimeOut);
    }
    
    protected int setTxIsolationLevelToReadCommitted(SQLConnection sqlConn) throws SQLException {
    	int originalTransIsolationLevel = -1;
    	
    	if (sqlConn != null) {
    		Connection conn = sqlConn.getConnection();
    		originalTransIsolationLevel = conn.getTransactionIsolation();
    		
    		if (originalTransIsolationLevel != Connection.TRANSACTION_READ_COMMITTED) {
    			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
    		}
    	}
    	
    	return originalTransIsolationLevel;
    }
    
    protected void resetTxIsolationLevelToOriginal(SQLConnection sqlConn, int orginalTxIsolationLevel ) throws SQLException {
    	if (sqlConn != null) {
    		Connection conn = sqlConn.getConnection();
    		
    		if (orginalTxIsolationLevel != conn.getTransactionIsolation()) {
    			conn.setTransactionIsolation(orginalTxIsolationLevel);
    		}
    	}
    }
    
    
    private void archiveSIRData(List<String> worksheetIds) throws SearchException {
    	long methodStartTime = Log.start(String.format("Starting Archive SIR Data for %d SIR Worksheet Ids: ", 
    												   worksheetIds.size()), Level.INFO);

    	SQLConnection sqlConn = null;
    	int orginalTxIsolationLevel = -1;
    	
    	try {
    		int start = 0;
    		int limit = 2000;
    		int end = start + limit;

    		int totalRecords = worksheetIds.size();
    		Map<String, Set<String>> wsIdsToSIRIds = new ConcurrentHashMap<>();
    		Map<String, Set<String>> wsIdsToPSIRIds = new ConcurrentHashMap<>();
    		List<String> updWsIds = new ArrayList<String>();
    		List<String> updIncidentIds = new CopyOnWriteArrayList<String>();
    		List<String> updatedWorksheetIds = new ArrayList<String>();
        	List<String> archivedArtifactIds = new ArrayList<String>();
        	List<String> archivedAttachmentIds = new ArrayList<String>();
        	List<String> archivedAuditLogIds = new ArrayList<String>();
        	List<String> archivedNoteIds = new ArrayList<String>();
    		Map<Pair<String, String>, List<String>> delArchivedSIRDataReqs = 
    													new HashMap<Pair<String, String>, List<String>>(); 
    		sqlConn = SQL.getConnection();
    		orginalTxIsolationLevel = setTxIsolationLevelToReadCommitted(sqlConn);

    		while(start < totalRecords) {
    			if(end > totalRecords) {
    				end = totalRecords;
    			}

    			List<String> subList = worksheetIds.subList(start, end);
    			Map<String, Pair<Set<String>, Set<String>>> wsIdToSIRAndPSIRIds = 
    															PlaybookUtils
    															.getSIRAndPrimarySIRIdsForWorksheetIds("problemId", subList,
    																					   			   UserUtils.SYSTEM, 
    																					   			   null);
    			
    			if (MapUtils.isNotEmpty(wsIdToSIRAndPSIRIds)) {
    				wsIdToSIRAndPSIRIds.keySet().parallelStream().forEach(wsId -> {
    					if (CollectionUtils.isNotEmpty(wsIdToSIRAndPSIRIds.get(wsId).getLeft())) {
    						wsIdsToSIRIds.put(wsId, wsIdToSIRAndPSIRIds.get(wsId).getLeft());
    					}
    					
    					if (CollectionUtils.isNotEmpty(wsIdToSIRAndPSIRIds.get(wsId).getRight())) {
    						wsIdsToPSIRIds.put(wsId, wsIdToSIRAndPSIRIds.get(wsId).getRight());
    					}
    				});
    			}
    			
    			SQL.setAutoCommit(sqlConn, false);
    			updWsIds = updateSIR(sqlConn, subList, wsIdsToPSIRIds);
    			
    			if (CollectionUtils.isNotEmpty(updWsIds)) {
    				updatedWorksheetIds.addAll(updWsIds);
    				
    				updWsIds.parallelStream().forEach(updWsId -> {
    					if (wsIdsToSIRIds.containsKey(updWsId) && CollectionUtils.isNotEmpty(wsIdsToSIRIds.get(updWsId))) {
    						updIncidentIds.addAll(wsIdsToSIRIds.get(updWsId));
    					}
    				});
    				
	    			/*
	    			 * Remove artifacts, attachments, audit logs, and notes before committing batch.
	    			 * This will make sure that when Logstash updates the combined index SIR has
	    			 * no artifacts, attachments, audit logs, and notes.
	    			 */
	    			List<String> archivedArtifactBatchIds = PlaybookIndexAPI.findArtifactIdsByWorksheetIds(updWsIds);
	    			List<String> archivedAttachmentBatchIds = PlaybookIndexAPI.findAttachmentIdsByWorksheetIds(updWsIds);
	    			List<String> archivedAuditLogBatchIds = PlaybookIndexAPI.findAuditLogIdsByIncidentIds(updIncidentIds);
	    			List<String> archivedNoteBatchIds = PlaybookIndexAPI.findNoteIdsByWorksheetIds(updWsIds);
	    			
	    			if (CollectionUtils.isNotEmpty(archivedArtifactBatchIds)) {
	    	    		delArchivedSIRDataReqs.put(Pair.of(SearchConstants.INDEX_NAME_PB_ARTIFACTS, 
	    	    										   SearchConstants.DOCUMENT_TYPE_PB_ARTIFACTS), 
	    	    								   archivedArtifactBatchIds);
	    	    	}
	    			
	    			if (CollectionUtils.isNotEmpty(archivedAttachmentBatchIds)) {
	    	    		delArchivedSIRDataReqs.put(Pair.of(SearchConstants.INDEX_NAME_PB_ATTACHMENT, 
	    	    										   SearchConstants.DOCUMENT_TYPE_PB_ATTACHMENT), 
	    	    								   archivedAttachmentBatchIds);
	    	    	}
	    			
	    			if (CollectionUtils.isNotEmpty(archivedAuditLogBatchIds)) {
	    	    		delArchivedSIRDataReqs.put(Pair.of(SearchConstants.INDEX_NAME_PB_AUDIT_LOG, 
	    	    										   SearchConstants.DOCUMENT_TYPE_PB_AUDIT_LOG), 
	    	    								   archivedAuditLogBatchIds);
	    	    	}
	    			
	    			if (CollectionUtils.isNotEmpty(archivedNoteBatchIds)) {
	    	    		delArchivedSIRDataReqs.put(Pair.of(SearchConstants.INDEX_NAME_PB_NOTES, 
	    	    										   SearchConstants.DOCUMENT_TYPE_PB_NOTES), archivedNoteBatchIds);
	    	    	}
	    			
	    			if (MapUtils.isNotEmpty(delArchivedSIRDataReqs)) {
	    	        	try {
	    	        		APIFactory.getGenericIndexAPI().bulkDeleteByIds(delArchivedSIRDataReqs, false);
	    	        	} catch (SearchException e) {
	    	        		Log.log.error(String.format("Error %sin bulk deleting documents by ids",
	    	        									StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
	    	        					  e);
	    	        		throw e;
	    	        	}
	    	        }
	    			
	    			archivedArtifactIds.addAll(archivedArtifactBatchIds);
	    			archivedAttachmentIds.addAll(archivedAttachmentBatchIds);
	    			archivedAuditLogIds.addAll(archivedAuditLogBatchIds);
	    			archivedNoteIds.addAll(archivedNoteBatchIds);
    			}
    			
    			SQL.commit(sqlConn);
    			wsIdsToSIRIds.clear();
    			wsIdsToPSIRIds.clear();
    			
    			updWsIds.clear();
    			updIncidentIds.clear();
    			archivedArtifactIds.clear();
    			archivedAttachmentIds.clear();
    			archivedAuditLogIds.clear();
    			archivedNoteIds.clear();
    			delArchivedSIRDataReqs.clear();
    			
    			start += limit;
    			end = start + limit;
    		}
    	} catch (Throwable t) {
    		String errMsg = String.format("Error [%s]in updating requested SIRs for %d SIR worksheet ids from " +
    									  "resolve_security_incident table", 
    									  (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
    									  worksheetIds.size());
    		Log.log.error(errMsg, t);
    		throw new RuntimeException(errMsg, t);
    	} finally {
    		if(orginalTxIsolationLevel != -1) {
    			try {
    				resetTxIsolationLevelToOriginal(sqlConn, orginalTxIsolationLevel);
    			} catch (SQLException e) {
    				//ignore.
    			}
    		}

    		SQL.close(sqlConn);
    	}
    	
    	Log.duration(String.format("Completed Archive SIR Data for %d SIR Worksheet Ids: ", worksheetIds.size()), 
    				 Level.INFO, methodStartTime);
    } // archiveSIRData
    
    private List<String> prepareSIRUpdateStatement(PreparedStatement updateStatement, 
    											   List<String> worksheetIds,
    											   PreparedStatement updateDuplicateSIRStatement,
    						 					   Map<String, Set<String>> wsIdsToPSIRIds) throws SQLException {
    	List<String> keys = new ArrayList<String>();
    	
    	Timestamp currentTimeStamp = new Timestamp(System.currentTimeMillis());
    	
        if(CollectionUtils.isNotEmpty(worksheetIds)) {
            for (String worksheetId : worksheetIds) {
                try {
                	updateStatement.setTimestamp(1, currentTimeStamp);
                	updateStatement.setString(2, worksheetId);
                	updateStatement.addBatch();
                	
                	if (MapUtils.isNotEmpty(wsIdsToPSIRIds) && 
                		CollectionUtils.isNotEmpty(wsIdsToPSIRIds.get(worksheetId))) {
                		final List<SQLException> sqles = new ArrayList<>();
                		wsIdsToPSIRIds.get(worksheetId).stream().forEach(pSIRId -> {
                			try {
								updateDuplicateSIRStatement.setString(1, pSIRId);
								updateDuplicateSIRStatement.addBatch();
							} catch (SQLException e) {
								Log.log.error(String.format("Unable to reset duplicate SIR's having %s as master SIR sys id",
															pSIRId), e);
								sqles.add(e);
							}
                		});
                		
                		if (CollectionUtils.isNotEmpty(sqles)) {
                			throw sqles.get(0);
                		}
                	}
                	
                    keys.add(worksheetId);
                } catch (Exception e) {
                    Log.log.error(String.format("Unable to update SIR record with worksheet id: %s", worksheetId), e);
                    throw e;
                } 
            }
        }
        
        return keys;
    }
    
    private List<String> safeUpdateSIR(SQLConnection conn, PreparedStatement updateStatement, 
			 						   List<String> worksheetIds,
			 						  PreparedStatement updateDuplicateSIRStatement,
			 						  Map<String, Set<String>> wsIdsToPSIRIds) throws SQLException {
    	int retry = 0;

    	List<String> keys = prepareSIRUpdateStatement(updateStatement, worksheetIds, 
    												  updateDuplicateSIRStatement, wsIdsToPSIRIds);
    	
    	while (retry <= 1) {
			try {
				updateStatement.executeBatch();
				break;
			} catch (SQLException e) {
				String errMsg = String.format("Error %sin resetting %d SIRs from resolve_security_incident table, " +
											  " retry %d", 
											  (StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
											  worksheetIds.size(), (retry + 1));

				if (retry == 1) {
					Log.log.error(errMsg, e);
					throw e;
				}

				Log.log.warn(errMsg);
				retry++;
			}
    	}
    	
    	if (MapUtils.isNotEmpty(wsIdsToPSIRIds)) {
	    	retry = 0;
	    	
	    	while (retry <= 1) {
				try {
					updateDuplicateSIRStatement.executeBatch();
					break;
				} catch (SQLException e) {
					String errMsg = String.format("Error %sin resetting master SIR id of duplicate SIRs for %d master SIR " +
												  "ids in resolve_security_incident table, retry %d", 
												  (StringUtils.isNotBlank(e.getMessage()) ? "[" + e.getMessage() + "] " : ""),
												  wsIdsToPSIRIds.size(), (retry + 1));
	
					if (retry == 1) {
						Log.log.error(errMsg, e);
						throw e;
					}
	
					Log.log.warn(errMsg);
					retry++;
				}
	    	}
    	}

    	return keys;
    }
    
    protected List<String> updateSIR(SQLConnection conn, 
			   						 List<String> worksheetIds,
			   						 Map<String, Set<String>> wsIdsToPSIRIds) throws SearchException {
    	int totalRecords = 0;
    	List<String> updatedWorksheetIds = new ArrayList<String>(); // Worksheet Id

    	if (CollectionUtils.isNotEmpty(worksheetIds)) {
    		PreparedStatement updateStatement = null;
    		PreparedStatement updateDuplicateSIRStatement = null;

    		try {
    			updateStatement = conn.prepareStatement(SIR_UPDATE_STATEMENT);
    			updateDuplicateSIRStatement = conn.prepareStatement(DUPLICATE_SIR_UPDATE_STATEMENT);

    			int limit = 1000;
    			int start = 0;
    			int end = start + limit;
    			totalRecords = worksheetIds.size();

    			while(start < totalRecords) {
    				if(end > totalRecords) {
    					end = totalRecords;
    				}

    				List<String> subList = worksheetIds.subList(start, end);
    				updatedWorksheetIds.addAll(safeUpdateSIR(conn, updateStatement, subList, 
    														 updateDuplicateSIRStatement, wsIdsToPSIRIds));
    				updateStatement.clearBatch();

    				start += limit;
    				end = start + limit;
    			}
    		} catch (Throwable t) {
    			String errMsg = String.format("Error %sin updating %d SIR records in DB", 
    										  (StringUtils.isNotBlank(t.getMessage()) ? "[" + t.getMessage() + "] " : ""),
    										  worksheetIds.size());

    			Log.log.error(errMsg, t);
    			throw new SearchException(errMsg, t);
    		} finally {
    			try {
    				if (updateStatement != null && !updateStatement.isClosed()) {
    					updateStatement.close();
    				}
    			} catch (SQLException sqle) {
    				Log.log.warn("Failed to Close Update SIR Statement: " + sqle.getMessage());
    			}
    		}
    	}

    	return updatedWorksheetIds;
    }
} // archive
