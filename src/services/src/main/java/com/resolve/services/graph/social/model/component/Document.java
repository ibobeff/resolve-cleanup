/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.component;

import java.util.Map;

import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.RSPublisher;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;

public class Document extends RSComponent implements RSPublisher
{
    private static final long serialVersionUID = 1869574353432118145L;
    
//    public static final String TYPE = "document";
    public static final String IS_RUNBOOK = "isRunbook";
    public static final String IS_DT = "isDecisionTree";
    public static final String DISPLAY_MODE = "DisplayMode";
    public static final String IS_TEMPLATE = "isTemplate";
    public static final String IS_DELETABLE = "isDeletable";
    public static final String SIR_REF_COUNT = "sirRefCount";
    
    private boolean runbook;
    private boolean decisionTree;
    

    public Document(String sysId, String name) 
    {
        super(NodeType.DOCUMENT);
        setId(sysId);
        setSys_id(sysId);
        setName(name);
    }
    
    public Document() 
    {
        super(NodeType.DOCUMENT);
    }
    
    public Document(ResolveNodeVO node) throws Exception
    {
        super(node);
        this.setId(node.getUCompSysId());// + "-" + NodeType.DOCUMENT.name());
        this.setSys_id(node.getUCompSysId());// + "-" + NodeType.DOCUMENT.name());
        
        if(this.getProperties() != null)
        {
            Map<String, String> props = this.getProperties();
            if(props.containsKey(IS_RUNBOOK) && props.get(IS_RUNBOOK).equalsIgnoreCase("true"))
            {
                runbook = true;
            }
            if(props.containsKey(IS_DT) && props.get(IS_DT).equalsIgnoreCase("true"))
            {
                decisionTree = true;
            }
        }           
    }

    public boolean isRunbook()
    {
        return runbook;
    }

    public void setRunbook(boolean runbook)
    {
        this.runbook = runbook;
    }

    public boolean isDecisionTree()
    {
        return decisionTree;
    }

    public void setDecisionTree(boolean decisionTree)
    {
        this.decisionTree = decisionTree;
    }
}
