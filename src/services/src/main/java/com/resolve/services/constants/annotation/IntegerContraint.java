package com.resolve.services.constants.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.resolve.services.validator.IntegerValidator;

@Documented
@Constraint(validatedBy = IntegerValidator.class)
@Retention(RUNTIME)
@Target({ TYPE, FIELD, METHOD, ANNOTATION_TYPE })
public @interface IntegerContraint {
	String message() default "Not valid number.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
