/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.component.container;

import java.util.Collection;

import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.RSContainer;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;
/**
 * Forum is only a publisher . It is a CONTAINER
 * 
 * @author jeet.marwah
 *
 */
public class Forum extends RSComponent implements RSContainer
{
    private static final long serialVersionUID = 5625216702514073726L;

    public static final String TYPE = "forum";

    private Collection<User> users = null;
   
    public Forum()
    {
        super(NodeType.FORUM);
        setName("Forum");
    }
    
    public Forum(String sysId, String name) 
    {
        super(NodeType.FORUM);
        setSys_id(sysId);
        setName(name);
    }
    
    public Forum(ResolveNodeVO node) throws Exception
    {
        super(node);
    }
    
    public void setUsers(Collection<User> users)
    {
        this.users = users;
    }
    
    public Collection<User> getUsers()
    {
        return users;
    }

}
