package com.resolve.services.hibernate.util;

import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.query.Query;

import com.resolve.persistence.model.PullGatewayFilter;
import com.resolve.persistence.model.PullGatewayFilterAttr;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.GatewayFilterVO;
import com.resolve.services.hibernate.vo.GatewayVO;
import com.resolve.services.hibernate.vo.PullGatewayFilterVO;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.Log;
import com.resolve.util.MappingAnnotation;
import com.resolve.util.StringUtils;

public class PullGatewayFilterUtil extends GatewayFilterUtil
{
    private static final String pullGatewayFilterVOClassName = "com.resolve.services.hibernate.vo.PullGatewayFilterVO";
     
    
    public static Map<String, Map<String, Object>> loadPullGatewayFilters(String gatewayName, 
                                                    String queueName, Boolean deployed) throws Exception {
        
        //Response 
        Map<String, Map<String, Object>> filterMap = null;
        //hibernate query results
        List<PullGatewayFilter> results = new ArrayList<PullGatewayFilter>();
        try {
            PullGatewayFilter gatewayFilter = new PullGatewayFilter();
            gatewayFilter.setUGatewayName(gatewayName);
            gatewayFilter.setUQueue(queueName);
            
            results = (List<PullGatewayFilter>) HibernateProxy.execute(() -> {
            	return  HibernateUtil.getDAOFactory().getPullGatewayFilterDAO().find(gatewayFilter);
            });
            
            filterMap = new HashMap<String, Map<String, Object>>();
//             Map<String, Object> filter = new HashMap<String, Object>();
             
             if(results.size() > 0) {
                 for(PullGatewayFilter gwFilter: results) {
                	 Map<String, Object> filter = new HashMap<String, Object>();
                     filter.put("SYS_ID",gwFilter.getSys_id());
                     filter.put("ACTIVE", gwFilter.getUActive());
                     filter.put("DEPLOYED", gwFilter.getUDeployed());
                     filter.put("UPDATED", gwFilter.getUUpdated());
                     filter.put("QUEUE", gwFilter.getUQueue());
                     filter.put("ORDER", gwFilter.getUOrder());
                     filter.put("INTERVAL", gwFilter.getUInterval());
                     filter.put("ID", gwFilter.getUName());
                     filter.put("GATEWAY_NAME", gwFilter.getUGatewayName());
                     filter.put("EVENT_ID", gwFilter.getUEventEventId());
                     filter.put("RUNBOOK", gwFilter.getURunbook());
                     filter.put("SCRIPT", gwFilter.getUScript());
                     filter.put("QUERY", gwFilter.getUQuery());
                     filter.put("TIME_RANGE", gwFilter.getUTimeRange());
                     filter.put("LAST_ID", gwFilter.getULastId());
                     filter.put("LAST_VALUE", gwFilter.getULastValue());
                     filter.put("LAST_TIMESTAMP", gwFilter.getULastTimestamp());
                     filterMap.put(gwFilter.getUName(), filter);
                 }
            }
        }catch(Exception ex) {
            Log.log.error(ex.getMessage(), ex);
            throw ex;
        }
        return filterMap;
        
    }

  public static GatewayFilterVO insertPullGatewayFilter(Map<String, String>params, String gatewayName) throws Exception {

      try {
    	  return (GatewayFilterVO) HibernateProxy.execute(() -> {
    		  PullGatewayFilterVO filter = null;
    		  String filterId;
	          PullGatewayFilter insertedPullGateway = insertPullGatewayFilterFields(params, gatewayName);
	          if(insertedPullGateway==null) {
	                Log.log.error("Failed to insert pull gateway fields.");
	                throw new Exception("Failed to insert pull gateway fields.");
	          }else {
	                 filterId = insertedPullGateway.getSys_id() ;            
	          }
	          if(filterId == null) {
	              Log.log.error("Failed to insert pull gateway fields for " + insertedPullGateway.getUName() + ".");
	              throw new Exception("Failed to insert pull gateway fields for " + insertedPullGateway.getUName() + ".");
	          }
	
	          String filterName = params.get("uname");
	          String queueName = params.get("uqueue");
	          String username = params.get("username");
	          Map<String, String> attrs = new HashMap<String, String>();
	          if(filterId != null) {
	               attrs = findPullGatewayFilterAttributes(params);
	              
	              if(attrs != null) {
	                  for(Iterator<String> it=attrs.keySet().iterator(); it.hasNext();) {
	                      String attrName = it.next();
	                      String value = attrs.get(attrName);
	                      if (StringUtils.isNotBlank(attrName) && attrName.equals("upassword") && StringUtils.isNotBlank(value)) {
	                          try {
	                              value = CryptUtils.encrypt(value);
	                          } catch(Exception e) {
	                              Log.log.error(e.getMessage(), e);
	                          }
	                      }
	  
	                      insertPullGatewayFilterAttribute(attrName, value, filterId, username);
	                  }
	              }
	          }
	          
	          filter = getPullFilterByName(gatewayName, filterName, queueName);
	          filter.setAttrs(attrs);
	          
	          return filter;
    	  });
      } catch(Exception e) {
          Log.log.error(e.getMessage(), e);
          throw e;
      } 
      
  }
    
public static PullGatewayFilter insertPullGatewayFilterFields(Map<String, String>params, 
                                                String gatewayName) throws Exception {
        boolean inserted = false;
        PullGatewayFilter pullGatewayFilter;
        PullGatewayFilter result;
        try
        {
            pullGatewayFilter = new PullGatewayFilter();
           // pullGatewayFilter.setSys_id(new Long(System.nanoTime()).toString());
            pullGatewayFilter.setUActive(new Boolean(params.get("uactive")));
            pullGatewayFilter.setUDeployed(new Boolean(params.get("udeployed")));
            pullGatewayFilter.setUUpdated(new Boolean(params.get("uupdated")));
            pullGatewayFilter.setUOrder(new Integer(params.get("uorder")));   
            pullGatewayFilter.setUInterval(new Integer(params.get("uinterval")));
            pullGatewayFilter.setUName(params.get("uname"));
            pullGatewayFilter.setUGatewayName(gatewayName);
            String queueName = params.get("uqueue");
            if(StringUtils.isBlank(queueName))
                pullGatewayFilter.setUQueue(Constants.DEFAULT_GATEWAY_QUEUE_NAME);
            else
                pullGatewayFilter.setUQueue(queueName);
            
             pullGatewayFilter.setUEventEventId(params.get("ueventEventId"));
             pullGatewayFilter.setURunbook(params.get("urunbook"));
             pullGatewayFilter.setUScript(params.get("uscript"));
             pullGatewayFilter.setUQuery(params.get("uquery"));
             pullGatewayFilter.setULastId(params.get("ulastId"));
             pullGatewayFilter.setULastValue(params.get("ulastValue"));
             pullGatewayFilter.setULastTimestamp(params.get("ulastTimestamp"));
             pullGatewayFilter.setUTimeRange(params.get("utimeRange"));
             pullGatewayFilter.setSysCreatedBy(params.get("username"));
             pullGatewayFilter.setSysCreatedOn(new Timestamp(System.currentTimeMillis()));
             
             pullGatewayFilter.setSys_id(null);
            
             result = (PullGatewayFilter) HibernateProxy.execute(() -> {
            	 return HibernateUtil.getDAOFactory().getPullGatewayFilterDAO().persist(pullGatewayFilter);
             });
             
             inserted = true;
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return result;
    }
    
public static void updatePullGatewayFilter(Map<String, String>params, String gatewayName, boolean isUpdated) throws Exception {
    
    try {
        
        HibernateProxy.execute(() -> {
        	String filterName = params.get("uname");
            String username = params.get("username");
            String queueName = params.get("uqueue");
            //Field to identify where the update call is being made from
            String isDeploying = params.get("DACTION") == null ? "false" : params.get("DACTION") ;
            if(filterName == null)
                throw new Exception("Filter name is not available.");
            
            if(StringUtils.isBlank(queueName)) {
                queueName = Constants.DEFAULT_GATEWAY_QUEUE_NAME;
                params.put("uqueue", queueName);
            }
        	
        	String filterId = null;
            boolean filterInserted = false;
            PullGatewayFilter filter = new PullGatewayFilter();
            
        	filter.setUGatewayName(gatewayName);
            filter.setUName(filterName);
            filter.setUQueue(queueName);
            filterId = getPullGatewayFilterId(filter);
            
            if(filterId == null) {
                filter =  insertPullGatewayFilterFields(params, gatewayName);
                filterInserted = true;
                filterId = filter.getSys_id();
            }
            else if(isDeploying.equalsIgnoreCase("true")) {
            	updatePullGatewayFilterFields(params, gatewayName, isUpdated);
            }
            else {
            	updatePullGatewayFilterLastVals(params, gatewayName, isUpdated);
            }
            
            Map<String, String> attrs = findPullGatewayFilterAttributes(params);
            
            if(attrs != null) {
                HashMap<String, String> attributes = getPullGatewayFilterAttributes(filterId);
                
                for(Iterator<String> it=attrs.keySet().iterator(); it.hasNext();) {
                    String attrName = it.next();
                    String value = attrs.get(attrName);
                    if (StringUtils.isNotBlank(attrName) && attrName.equals("upassword") && StringUtils.isNotBlank(value) 
                                                        && !value.startsWith("ENC:")) {
                        try {
                            value = CryptUtils.encrypt(value);
                        } catch(Exception e) {
                            Log.log.error(e.getMessage(), e);
                        }
                    }
                    
                    if(attributes != null && attributes.containsKey(attrName) && filterInserted == false) {
                        updatePullGatewayFilterAttribute(attrName, value, filterId, username);
                    }
//                    else if(isDeploying.equalsIgnoreCase("true")) {
                    else {
                        insertPullGatewayFilterAttribute(attrName, value, filterId, username);
                    }
                }
            }
        });
    } catch(Exception e) {
        Log.log.error(e.getMessage(), e);
        throw e;
    }
}

    private static Map<String, String> findPullGatewayFilterAttributes(Map<String, String> params) throws Exception {
        
        Map<String, String> attrs = new HashMap<String, String>();
        
        try {
            Method[] methods = Class.forName(pullGatewayFilterVOClassName).newInstance().getClass().getMethods();
            
            for(Iterator<String> it=params.keySet().iterator(); it.hasNext();) {
                String key = it.next();
                if(key == null)
                    continue;
                
                boolean found= false;
                
                for(Method method:methods) {
                    String name = method.getName();
                    if(method.getAnnotation(MappingAnnotation.class) != null) {
                        String attr = name.substring(3);
                        if(key.equalsIgnoreCase(attr)) {
                            found = true;
                            break;
                        }
                    }
                }
                
                if(found)
                    continue;
                else if(!key.startsWith("u") || key.equals("username"))
                    continue;
                else
                    attrs.put(key,  params.get(key));
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return attrs;
    }
    
   
    @SuppressWarnings("unchecked")
	public static HashMap<String, String> getPullGatewayFilterAttributes(String filterId) throws Exception {

        HashMap<String, String> attributes = new HashMap<String, String>();
        List<PullGatewayFilterAttr> filterAttrs = new ArrayList<PullGatewayFilterAttr>();
        PullGatewayFilterAttr filterAttr = new PullGatewayFilterAttr();
        try {

            filterAttr.setUPullFilterId(filterId);
            filterAttrs = (List<PullGatewayFilterAttr>) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getPullGatewayFilterAttrDAO().find(filterAttr);
            });
            
            if(filterAttrs!= null && filterAttrs.size() >= 0) {
                for(PullGatewayFilterAttr pullFilterAttr: filterAttrs) {
                    if(pullFilterAttr != null) {
                        attributes.put(pullFilterAttr.getUName(), pullFilterAttr.getUValue());
                    }
                }
            }
           
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return attributes;
    }
    
    @SuppressWarnings("unchecked")
	public static List<String> getPullGatewayFilterAttributeIds(String filterId) {

        PullGatewayFilterAttr filterAttr = new PullGatewayFilterAttr();
        List<PullGatewayFilterAttr> filterAttrs = new ArrayList<PullGatewayFilterAttr>();
        List<String> attributeIds = new ArrayList<String>();
        try {
            filterAttr.setUPullFilterId(filterId);
            filterAttrs =  (List<PullGatewayFilterAttr>) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getPullGatewayFilterAttrDAO().find(filterAttr);
            });
            if(filterAttrs != null && filterAttrs.size() >= 0) {
                for(PullGatewayFilterAttr filter: filterAttrs) {
                    attributeIds.add(filter.getSys_id());
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
            Log.log.error(e.getMessage(), e);
                                           HibernateUtil.rethrowNestedTransaction(e);
        } 
        return attributeIds;
    }
    
    private static void insertPullGatewayFilterAttribute(String attrName, String value, 
                                                    String filterId, String username) throws Exception {
        
        try {
                
                PullGatewayFilterAttr pullGatewayFilterAttr = new PullGatewayFilterAttr();
            
                pullGatewayFilterAttr.setSysCreatedBy(username);
                pullGatewayFilterAttr.setSysCreatedOn(new Timestamp(System.currentTimeMillis()));
                pullGatewayFilterAttr.setUName(attrName);
                pullGatewayFilterAttr.setUValue(value);  
                pullGatewayFilterAttr.setUPullFilterId(filterId);
                pullGatewayFilterAttr.setSys_id(null);
                
                HibernateProxy.execute(() -> {
                	HibernateUtil.getDAOFactory().getPullGatewayFilterAttrDAO().persist(pullGatewayFilterAttr);
                });
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        } 
    }
    
    
private static void updatePullGatewayFilterAttribute(String attrName, String value, 
                                                     String filterId, String username) throws Exception {
        try {
                        
            HibernateProxy.execute(() -> {
            	Query query = HibernateUtil.getCurrentSession().createQuery("update PullGatewayFilterAttr "
                        + " set UValue = :uvalue, sysUpdatedBy= :sysupdatedby, sysUpdatedOn= :sysupdatedon" +
                            " where UPullFilterId = :pullfilterid and UName= :uname ");
	           query.setParameter("uvalue", value);
	           query.setParameter("sysupdatedby", username);
	           query.setParameter("sysupdatedon", (new Timestamp(System.currentTimeMillis())));
	           query.setParameter("pullfilterid", filterId);
	           query.setParameter("uname", attrName);

	           return query.executeUpdate();
            });
            
            
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        } 
    }
    
/**
 *   Query query = HibernateUtil.getSessionFactory().getCurrentSession().createQuery("update PullGatewayFilter"
                    + " set UActive = :uactive, UDeployed= :udeployed, UOrder = :uorder, UInterval = :uinterval, " +
                     "  UEventEventId = :ueventid, URunbook= :urunbook, UScript= :uscript, UQuery = :uquery, UTimeRange = :utimerange" +
                     " ,ULastId = :ulastid, ULastValue= :ulastval, ULastTimestamp= :ulasttimestamp, sysUpdatedBy = :updtBy, " +
                     "sysUpdatedOn = :updtOn, UUpdated = :isUpdated  where UQueue = :uqueue and UName= :uname and UGatewayName= :ugatewayname");
 * @param params
 * @param gatewayName
 * @param isUpdated
 * @throws Exception
 */
    private static void updatePullGatewayFilterFields(Map<String, String>params, 
                                                String gatewayName, boolean isUpdated) throws Exception {
        
//        PullGatewayFilter pullGatewayFilter = new PullGatewayFilter();
        try {
                        
            HibernateProxy.execute(() -> {
            	 Query query = HibernateUtil.getCurrentSession().createQuery("update PullGatewayFilter"
                         + " set UActive = :uactive, UDeployed= :udeployed, UOrder = :uorder, UInterval = :uinterval, " +
                          "  UEventEventId = :ueventid, URunbook= :urunbook, UScript= :uscript, UQuery = :uquery, sysUpdatedBy = :updtBy " +
                          ",sysUpdatedOn = :updtOn, UUpdated = :isUpdated  where UQueue = :uqueue and UName= :uname and UGatewayName= :ugatewayname");

                     query.setParameter("uactive", new Boolean(params.get("uactive")));
                     query.setParameter("udeployed", new Boolean(params.get("udeployed")));
                     query.setParameter("uorder", new Integer(params.get("uorder")));
                     query.setParameter("uinterval",new Integer(params.get("uinterval")));
                     query.setParameter("ueventid", params.get("ueventEventId"));
                     query.setParameter("urunbook", params.get("urunbook"));
                     query.setParameter("uscript", params.get("uscript"));
                     query.setParameter("uquery", params.get("uquery"));
//                     query.setParameter("utimerange", params.get("utimeRange"));
//                     query.setParameter("ulastid", params.get("ulastId"));
//                     query.setParameter("ulastval", params.get("ulastValue"));
//                     query.setParameter("ulasttimestamp", params.get("ulastTimestamp"));
                     query.setParameter("updtBy", params.get("username"));
                     query.setParameter("updtOn", new Timestamp(System.currentTimeMillis()));
                     if(isUpdated)
                         query.setParameter("isUpdated", new Boolean(true));
                     else
                         query.setParameter("isUpdated", new Boolean(false));
                     query.setParameter("uname",params.get("uname"));
                     query.setParameter("ugatewayname", gatewayName);
                     query.setParameter("uqueue", params.get("uqueue"));
                     
                     return query.executeUpdate();
            });
           

        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    private static void updatePullGatewayFilterLastVals(Map<String, String>params, 
            String gatewayName, boolean isUpdated) throws Exception {

		//PullGatewayFilter pullGatewayFilter = new PullGatewayFilter();
		try {
						
			HibernateProxy.execute(() -> {
				 Query query = HibernateUtil.getCurrentSession().createQuery("update PullGatewayFilter"
		                    + " set UActive = :uactive, UDeployed= :udeployed, UOrder = :uorder, UInterval = :uinterval, " +
		                     "  UEventEventId = :ueventid, URunbook= :urunbook, UScript= :uscript, UQuery = :uquery, UTimeRange = :utimerange" +
		                     " ,ULastId = :ulastid, ULastValue= :ulastval, ULastTimestamp= :ulasttimestamp, sysUpdatedBy = :updtBy, " +
		                     "sysUpdatedOn = :updtOn, UUpdated = :isUpdated  where UQueue = :uqueue and UName= :uname and UGatewayName= :ugatewayname");
				
				query.setParameter("uactive", new Boolean(params.get("uactive")));
				query.setParameter("udeployed", new Boolean(params.get("udeployed")));
				query.setParameter("uorder", new Integer(params.get("uorder")));
				query.setParameter("uinterval",new Integer(params.get("uinterval")));
				query.setParameter("ueventid", params.get("ueventEventId"));
				query.setParameter("urunbook", params.get("urunbook"));
				query.setParameter("uscript", params.get("uscript"));
				query.setParameter("uquery", params.get("uquery"));
				query.setParameter("utimerange", params.get("utimeRange"));
				query.setParameter("ulastid", params.get("ulastId"));
				query.setParameter("ulastval", params.get("ulastValue"));
				query.setParameter("ulasttimestamp", params.get("ulastTimestamp"));
				query.setParameter("updtBy", params.get("username"));
				query.setParameter("updtOn", new Timestamp(System.currentTimeMillis()));
				if(isUpdated)
				query.setParameter("isUpdated", new Boolean(true));
				else
				query.setParameter("isUpdated", new Boolean(false));
				query.setParameter("uname",params.get("uname"));
				query.setParameter("ugatewayname", gatewayName);
				query.setParameter("uqueue", params.get("uqueue"));
				
				return query.executeUpdate();
			});
			
			
		
		} catch(Exception e) {
			Log.log.error(e.getMessage(), e);
			throw e;
		}
}
    
    
    public static PullGatewayFilterVO getPullFilterByName(String gatewayName,
                                    String filterName, String queueName) throws Exception {
        
        List<PullGatewayFilterVO> filters = getPullFiltersByName(gatewayName, filterName, queueName);
        if(filters.size() > 0)
            return filters.get(0);
        
        return null;
    }
    
    @SuppressWarnings("unchecked")
	public static List<PullGatewayFilterVO> getPullFiltersByName(String gatewayName, 
                    String filterName, String queueName) throws Exception {
        if (Log.log.isDebugEnabled()) {
        	Log.log.debug(String.format("getPullFiltersByName(%s,%s,%s)", gatewayName, 
        								(StringUtils.isNotBlank(filterName) ? filterName : ""),
        								(StringUtils.isNotBlank(queueName) ? queueName : "")));
        }
        
        PullGatewayFilterVO filter = null;
        String sysId = null;
        List<PullGatewayFilterVO> filters = new ArrayList<PullGatewayFilterVO>();
        List<PullGatewayFilter> pullFilters = new ArrayList<PullGatewayFilter>();
        PullGatewayFilter pullFilter = new PullGatewayFilter();
        
        try {
        	if (StringUtils.isNotBlank(filterName)) {
        		pullFilter.setUName(filterName);
        	}
            pullFilter.setUGatewayName(StringUtils.toCamelCase(gatewayName));
            if(StringUtils.isNotBlank(queueName))
                pullFilter.setUQueue(queueName);
            pullFilters =  (List<PullGatewayFilter>) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getPullGatewayFilterDAO().find(pullFilter);
            });
            
            if (Log.log.isDebugEnabled()) {
            	Log.log.debug(String.format("PullGatewayFilterDAO.find(%s,%s,%s) returned %d records", gatewayName, 
        									(StringUtils.isNotBlank(filterName) ? filterName : ""),
        									(StringUtils.isNotBlank(queueName) ? queueName : ""),
        									(CollectionUtils.isNotEmpty(pullFilters) ? pullFilters.size() : 0)));
            }
            
            if(pullFilters != null && pullFilters.size() >= 0) {
                for(PullGatewayFilter gatewayPullFilter: pullFilters) {
                    filter = new PullGatewayFilterVO();
                    
                    sysId = gatewayPullFilter.getSys_id();
                    filter.setId(sysId);
                    filter.setSys_id(sysId);
                    filter.setUName(gatewayPullFilter.getUName());
                    filter.setUActive(gatewayPullFilter.getUActive());
                    filter.setUDeployed(gatewayPullFilter.getUDeployed());
                    filter.setUUpdated(gatewayPullFilter.getUUpdated());
                    filter.setUOrder(gatewayPullFilter.getUOrder());
                    filter.setUInterval(gatewayPullFilter.getUInterval());
                    filter.setUName(gatewayPullFilter.getUName());
                    filter.setUQueue(gatewayPullFilter.getUQueue());
                    filter.setUGatewayName(gatewayPullFilter.getUGatewayName());
                    filter.setUEventEventId(gatewayPullFilter.getUEventEventId());
                    filter.setURunbook(gatewayPullFilter.getURunbook());
                    filter.setUScript(gatewayPullFilter.getUScript());
                    filter.setUQuery(gatewayPullFilter.getUQuery());
                    filter.setULastId(gatewayPullFilter.getULastId());
                    filter.setULastValue(gatewayPullFilter.getULastValue());
                    filter.setUTimeRange(gatewayPullFilter.getUTimeRange());
                    filter.setULastTimestamp(gatewayPullFilter.getULastTimestamp());
                    filter.setChecksum(gatewayPullFilter.getChecksum());
                    filter.setAttrs(getPullGatewayFilterAttributes(sysId));
                    filters.add(filter);
                }
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        
        return filters;
    }
    
    public static PullGatewayFilterVO getPullFilter(String id) throws Exception {
        PullGatewayFilterVO filter = getPullFilterById(id);
        return filter;
    }
    
    
@SuppressWarnings("unchecked")
private static PullGatewayFilterVO getPullFilterById(String id) throws Exception {
        
        PullGatewayFilter pullGateFilter = new PullGatewayFilter();
        List<PullGatewayFilter> filters = new ArrayList<PullGatewayFilter>();
        PullGatewayFilterVO filter = null;
        try {
            pullGateFilter.setSys_id(id);
            filters = (List<PullGatewayFilter>) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getPullGatewayFilterDAO().find(pullGateFilter);
            });
            if(filters != null && filters.size() >= 0) {
                for(PullGatewayFilter pullGatewayFilter: filters) {
                    filter = new PullGatewayFilterVO();
                    filter.setId(pullGatewayFilter.getSys_id());
                    filter.setSys_id(pullGatewayFilter.getSys_id());
                    filter.setSysCreatedOn(pullGatewayFilter.getSysCreatedOn());
                    filter.setSysUpdatedOn(pullGatewayFilter.getSysUpdatedOn());
                    filter.setSysCreatedBy(pullGatewayFilter.getSysCreatedBy());
                    filter.setSysUpdatedBy(pullGatewayFilter.getSysUpdatedBy());
                    filter.setUActive(pullGatewayFilter.getUActive());
                    filter.setUDeployed(pullGatewayFilter.getUDeployed());
                    filter.setUUpdated(pullGatewayFilter.getUUpdated());
                    filter.setUOrder(pullGatewayFilter.getUOrder()); 
                    filter.setUInterval(pullGatewayFilter.getUInterval());
                    filter.setUName(pullGatewayFilter.getUName());
                    filter.setUGatewayName(pullGatewayFilter.getUGatewayName() );
                    filter.setUQueue(pullGatewayFilter.getUQuery());
                    filter.setUEventEventId(pullGatewayFilter.getUEventEventId());
                    filter.setURunbook(pullGatewayFilter.getURunbook());
                    filter.setUScript(pullGatewayFilter.getUScript());
                    filter.setUQuery(pullGatewayFilter.getUQuery());
                    filter.setULastId(pullGatewayFilter.getULastId());
                    filter.setULastValue(pullGatewayFilter.getULastValue());
                    filter.setUTimeRange(pullGatewayFilter.getUTimeRange());
                    filter.setULastTimestamp(pullGatewayFilter.getULastTimestamp());
                    filter.setAttrs(getPullGatewayFilterAttributes(pullGatewayFilter.getSys_id())); 
                }
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        } 
        return filter;
    }

    
@SuppressWarnings("unchecked")
public static List<GatewayVO> listPullFilters(String gatewayName, String queueName, Boolean deployed) throws Exception {
        
        List<GatewayVO> filters = new ArrayList<GatewayVO>();
        PullGatewayFilter pullGatewayFilter = new PullGatewayFilter();
        List<PullGatewayFilter> pullGatewayFilters = new ArrayList<PullGatewayFilter>();
        
        try {
            pullGatewayFilter.setUGatewayName(gatewayName);
            if(deployed != null) {
                if(!deployed)
                    pullGatewayFilter.setUQueue(Constants.DEFAULT_GATEWAY_QUEUE_NAME);
                else
                    pullGatewayFilter.setUQueue(queueName.toUpperCase());
            }
            
            pullGatewayFilters = (List<PullGatewayFilter>) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getPullGatewayFilterDAO().find(pullGatewayFilter);
            });            
          
            for(PullGatewayFilter pullFilter: pullGatewayFilters) {
                
                PullGatewayFilterVO filter = new PullGatewayFilterVO();

                filter.setId(pullFilter.getSys_id());
                filter.setSys_id(pullFilter.getSys_id());
                filter.setSysCreatedOn(pullFilter.getSysCreatedOn());
                filter.setSysUpdatedOn(pullFilter.getSysUpdatedOn());
                filter.setSysCreatedBy(pullFilter.getSysCreatedBy());
                filter.setSysUpdatedBy(pullFilter.getSysUpdatedBy());
                
                filter.setUActive(pullFilter.getUActive());
                filter.setUActive(pullFilter.getUDeployed());
                filter.setUUpdated(pullFilter.getUUpdated());
                filter.setUOrder(pullFilter.getUOrder()); 
                filter.setUInterval(pullFilter.getUInterval());
                filter.setUName(pullFilter.getUName());
                filter.setUGatewayName(pullFilter.getUGatewayName() );
                filter.setUQueue(pullFilter.getUQueue());
                filter.setUEventEventId(pullFilter.getUEventEventId());
                filter.setURunbook(pullFilter.getURunbook());
                filter.setUScript(pullFilter.getUScript());
                filter.setUQuery(pullFilter.getUQuery());
                filter.setULastId(pullFilter.getULastId());
                filter.setULastValue(pullFilter.getULastValue());
                filter.setUTimeRange(pullFilter.getUTimeRange());
                filter.setULastTimestamp(pullFilter.getULastTimestamp());
                filter.setAttrs(getPullGatewayFilterAttributes(pullFilter.getSys_id())); 
                filters.add(filter);
            }
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
        return filters;
    }


public static boolean deletePullFilters(String gatewayName, String[] ids) {
        
        int num = ids.length;
        boolean result = false;
        try {
            
            List<String> idList = new ArrayList<String>();
            for(int i=0; i<num; i++) {
                idList.add(ids[i]);
            }

			result = (boolean) HibernateProxy.execute(() -> {
				HibernateUtil.deleteQuery("PullGatewayFilter", "sys_id", idList);
				HibernateUtil.deleteQuery("PullGatewayFilterAttr", "UPullFilterId", idList);
				return true;
			});
        } catch(Exception e) {
            Log.log.error("++++++++++++++++++++++++++++++++++++++++");
            Log.log.error("Error thrown when deleting pull filters, the delete may have failed");
            Log.log.error("++++++++++++++++++++++++++++++++++++++++");
            Log.log.error(e.getMessage(), e);
                                           HibernateUtil.rethrowNestedTransaction(e);
        } 
        return result;
    }
    
    
    // This method is called by RSView to update filter to refresh the updated flag
    public static void updatePullGatewayFilter(PullGatewayFilterVO pull, String filterId) throws Exception {
  
        try {
            
            HibernateProxy.execute(()-> {
            	 Query query = HibernateUtil.getCurrentSession().createQuery("update PullGatewayFilter"
                         + " set u_active = :uactive, u_deployed= :udeployed, u_updated= :uupdated, u_order = :uorder, " +
                          " u_interval = :uinterval, u_name= :uname, u_gateway_name= :ugatewayname, u_queue = :uqueue," +
                          " u_event_eventid = :ueventid, u_runbook= :urunbook, u_script= :uscript, u_query = :uquery, u_time_range = :utimerange," +
                          " u_last_id = :ulastid, u_last_value= :ulastval, u_last_timestamp= :ulasttimestamp where sys_id = :sysid");

                query.setParameter("uactive", pull.getUActive());
                query.setParameter("udeployed", pull.getUDeployed());
                query.setParameter("uupdated", pull.getUUpdated());
                query.setParameter("uorder", pull.getUOrder());
                query.setParameter("uinterval", pull.getUInterval());
                query.setParameter("uname", pull.getUName());
                query.setParameter("ugatewayname", pull.getUGatewayName());
                query.setParameter("uqueue", pull.getUQueue());
                query.setParameter("ueventid", pull.getUEventEventId());
                query.setParameter("urunbook", pull.getURunbook());
                query.setParameter("uscript", pull.getUScript());
                query.setParameter("uquery", pull.getUQuery());
                query.setParameter("utimerange", pull.getUTimeRange());
                query.setParameter("ulastid", pull.getULastId());
                query.setParameter("ulastval", pull.getULastValue());
                query.setParameter("ulasttimestamp", pull.getULastTimestamp());
                query.setParameter("sysid", filterId);
                return query.executeUpdate();
            });
           
           
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
            throw e;
        }
    }
    
    @SuppressWarnings("unchecked")
	public static String getPullGatewayFilterId(PullGatewayFilter passedPullGateway) throws Exception {
        
    try {
            List<PullGatewayFilter> gatewayFilters = new ArrayList<PullGatewayFilter>();
            gatewayFilters =  (List<PullGatewayFilter>) HibernateProxy.execute(() -> {
            	return HibernateUtil.getDAOFactory().getPullGatewayFilterDAO().find(passedPullGateway);
            });
            
            if(gatewayFilters != null && gatewayFilters.size() >= 0) {
                for(PullGatewayFilter pullGatewayFilter: gatewayFilters) {
                    return pullGatewayFilter.getSys_id();
                }
            }
        }catch(Exception ex) {
                Log.log.error(ex.getMessage(), ex);
                throw ex;
        }
    	return null;
        
    }
    
    public static List<? extends GatewayVO> getPullFiltersForQueue(String gatewayName, String queueName) throws Exception {
    	return getPullFiltersByName(gatewayName, null, queueName);
    }
    
} // class PullGatewayFilterUtil
