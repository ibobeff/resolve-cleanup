/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.rsbase.MainBase;
import com.resolve.persistence.model.ConfigRADIUS;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ConfigRADIUSVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ConfigRADIUSUtil
{
    public static List<ConfigRADIUSVO> getConfigRADIUS(QueryDTO query, String username)
    {
        List<ConfigRADIUSVO> result = new ArrayList<ConfigRADIUSVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        ConfigRADIUS instance = new ConfigRADIUS();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        result.add(convertModelToVO(instance));
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        ConfigRADIUS instance = (ConfigRADIUS) o;
                        result.add(convertModelToVO(instance));
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
    public static ConfigRADIUSVO findConfigRADIUSById(String sys_id, String username)
    {
        ConfigRADIUS model = null;
        ConfigRADIUSVO result = null;
        
        if(!StringUtils.isEmpty(sys_id))
        {
            try
            {
                model = findConfigRADIUSModelById(sys_id);
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
        
        if(model != null)
        {
            result = convertModelToVO(model);
        }
        
        return result;
    }
    
    public static ConfigRADIUSVO saveConfigRADIUS(ConfigRADIUSVO vo, String username)
    {
        if(vo != null)
        {
            ConfigRADIUS model = null;
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equalsIgnoreCase(VO.STRING_DEFAULT))
            {
                model = findConfigRADIUSModelById(vo.getSys_id());
            }
            else
            {
                model = new ConfigRADIUS();
            }
            model.applyVOToModel(vo);
            model = SaveUtil.saveConfigRADIUS(model, username);
            
            //invalidate the cache by sending the JMS to the gateway
            sendJmsMessageToInvalidate(model);
            
            vo = convertModelToVO(model);
        }
        
        return vo;
    }
    
    public static ConfigRADIUSVO findConfigRADIUSByDomainName(String domainName)
    {
        ConfigRADIUSVO result = null;
        
        if(StringUtils.isNotBlank(domainName))
        {
            ConfigRADIUS model = findConfigRADIUSModelByDomainName(domainName);
            if(model != null)
            {
                result = convertModelToVO(model);
            }
        }
        
        return result;
    }
    
    public static ConfigRADIUSVO findDefaultConfigRADIUS()
    {
        ConfigRADIUSVO result = null;
        ConfigRADIUS model = null;
        
        try
        {
            model = (ConfigRADIUS) HibernateProxy.execute(() -> {
            	ConfigRADIUS searchedModel = new ConfigRADIUS();
            	searchedModel.setUIsDefault(true);
            	
            	return HibernateUtil.getDAOFactory().getConfigRADIUSDAO().findFirst(searchedModel);
            });
        }
        catch (Throwable e)
        {
        	Log.log.warn(e.getMessage(), e);
                   HibernateUtil.rethrowNestedTransaction(e);
        }
        
        if(model != null)
        {
            result = convertModelToVO(model);
        }
        
        return result;
    }
    
    public static void deleteConfigRADIUSByIds(String[] sysIds, String username) throws Exception
    {
        if(sysIds != null)
        {
            for(String sysId : sysIds)
            {
                deleteConfigRADIUSById(sysId, username);
            }
        }
    }
    
    private static void deleteConfigRADIUSById(String sysId, String username) throws Exception
    {
        ConfigRADIUS model = null;
        try
        {
          HibernateProxy.setCurrentUser(username);
        	model = (ConfigRADIUS) HibernateProxy.execute(() -> {
	            
        		ConfigRADIUS modelResult = HibernateUtil.getDAOFactory().getConfigRADIUSDAO().findById(sysId);
	            if(modelResult != null)
	            {
	                HibernateUtil.getDAOFactory().getConfigRADIUSDAO().delete(modelResult);
	            }

	            return modelResult;
        	});
            
            //invalidate the cache by sending the JMS to the gateway
            sendJmsMessageToInvalidate(model);
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new Exception(e); 
        }
    }
    
    
    //private apis
    private static ConfigRADIUS findConfigRADIUSModelByDomainName(String domainName)
    {
        ConfigRADIUS result = null;
        if(StringUtils.isNotBlank(domainName))
        {
            ConfigRADIUS example = new ConfigRADIUS();
            example.setUDomain(domainName);
            
            try
            {
				result = (ConfigRADIUS) HibernateProxy.execute(() -> {
					return HibernateUtil.getDAOFactory().getConfigRADIUSDAO().findFirst(example);
				});
            }
            catch (Throwable e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return result;
    }
    
    private static ConfigRADIUS findConfigRADIUSModelById(String sys_id)
    {
        ConfigRADIUS result = null;
        
        if(!StringUtils.isEmpty(sys_id))
        {
            try
            {
            	result = (ConfigRADIUS) HibernateProxy.execute(() -> {
            		return HibernateUtil.getDAOFactory().getConfigRADIUSDAO().findById(sys_id);
            	});
                
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        
        return result;
    }
    
    private static ConfigRADIUSVO convertModelToVO(ConfigRADIUS model)
    {
        ConfigRADIUSVO vo = null;
        
        if(model != null)
        {
            vo = model.doGetVO();
            if(vo.getBelongsToOrganization() != null)
            {
                vo.setSysOrganizationName(vo.getBelongsToOrganization().getUOrganizationName());
            }
            else
            {
                vo.setSysOrganizationName("");
            }            
        }
        
        return vo;
    }
    
    private static void sendJmsMessageToInvalidate(ConfigRADIUS model)
    {
        // invalidate the cache by sending the JMS to the gateway - this is RSREMOTE
        String gateway = model.getUGateway();
        if(StringUtils.isNotBlank(gateway))
        {
            /*
             * Currently RADIUS gateway does not support domain.
             * Enable following once it supports domain and MRADIUS
             * implements invalidateDomain().
             *
            Map<String, String> params = new HashMap<String, String>();
            params.put(Constants.AUTH_DOMAIN, model.getUDomain());
            
            //jms message
            MainBase.getESB().sendMessage(gateway, "MRADIUS.invalidateDomain", params);
            */
        }  
        
        //this is to invalidate the RSVIEWS cache
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(Constants.AUTH_DOMAIN, model.doGetVO()); 
        
        MainBase.getESB().sendMessage(Constants.ESB_NAME_RSVIEWS, "MAction.invalidateRADIUS", params);
       
        
    }

    /*
    private static void sendMessageToImportSSLCertificate(ConfigRADIUS model)
    {
        //inform rsremote to import certificate from the RADIUS server.
        String gateway = model.getUGateway();
        if(StringUtils.isNotBlank(gateway))
        {
            Map<String, String> params = new HashMap<String, String>();
            
            //message
            MainBase.getESB().sendMessage(gateway, "MRADIUS.importSSLCertificate", params);
        }  
    }
    */
}
