/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.hibernate.CacheMode;
import org.hibernate.query.Query;
import org.owasp.esapi.ESAPI;

import com.resolve.rsbase.MainBase;
import com.resolve.search.metric.MetricIndexAPI;
import com.resolve.search.model.MetricTimer;
import com.resolve.persistence.model.BaseModel;
import com.resolve.persistence.model.MetricThreshold;
import com.resolve.persistence.model.ResolveMCPCluster;
import com.resolve.persistence.model.ResolveMCPLogin;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.MetricThresholdVO;
import com.resolve.services.hibernate.vo.ResolveMCPClusterVO;
import com.resolve.services.hibernate.vo.ResolveMCPComponentVO;
import com.resolve.services.hibernate.vo.ResolveMCPGroupVO;
import com.resolve.services.hibernate.vo.ResolveMCPHostVO;
import com.resolve.services.hibernate.vo.ResolveMCPLoginVO;
import com.resolve.services.hibernate.vo.ResolveRegistrationVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.jdbc.util.GenericJDBCHandler;
import com.resolve.services.util.CSRFUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.QueryParameter;
import com.resolve.services.vo.QuerySort;
import com.resolve.services.vo.ResponseDTO;
import com.resolve.util.Constants;
import com.resolve.util.CryptUtils;
import com.resolve.util.GMTDate;
import com.resolve.util.Log;
import com.resolve.util.MetricMsg;
import com.resolve.util.MetricThresholdType;
import com.resolve.util.StringUtils;
//import com.resolve.util.restclient.RestCaller;

public class MetricUtil
{
    public static String UNIQUE_THRESHOLDS = "UNIQUE_THRESHOLDS";
    public static String UNIQUE_THRESHOLDS_TOTAL = "UNIQUE_THRESHOLDS_TOTAL";
    public static String REMOTE_AUTO_DEPLOY_USERNAME="mcp_admin";
    public static String REMOTE_AUTO_DEPLOY_P_ASSWORD="Mcp_resolve1";
//    public static boolean isMCP = true;
    
    public static void initMetricThresholds(String fileName) throws Exception
    {
        List<MetricThreshold> lDBProps = getDefaultThresholdValuesFromDB();
        List<MetricThreshold> lNewDBProps = new ArrayList<MetricThreshold>();
        java.util.Properties properties = null;
        try
        {
            InputStream is = new FileInputStream(fileName);
            properties = new java.util.Properties();
            properties.load(is);
            is.close();
        }
        catch (Exception e)
        {
            throw new Exception("Failed to read from " + fileName + " file.");
        }

        Enumeration<Object> enu = properties.keys();
        while (enu.hasMoreElements())
        {
            String key = (String) enu.nextElement(); // eg.
                                                     // jvm.memory<list>
            String tmpkey = key; // eg. jvm.memory<list>
            String value = properties.getProperty(key);
//            String type = "string"; // must reset it back to string

            // remove comments as description value
//            String desc = "";
            int descPos = value.indexOf("##");
            if (descPos >= 0)
            {
//                desc = value.substring(descPos + 2).trim();
                value = value.substring(0, descPos).trim();
            }
            
            // get type
            if (key.indexOf("<") > 0)
            {
                key = tmpkey.substring(0, tmpkey.indexOf("<"));
//                type = tmpkey.substring(tmpkey.indexOf("<") + 1, tmpkey.indexOf(">"));
            }
            
            String highVal = value.split(",")[0];
            String lowVal = value.split(",")[1];
            String alertStatus = value.split(",")[2];
            
            String group = key.split("\\.")[0]; 
            String name = key.split("\\.")[1];
            
            // if the key is already there in DB table, then don't update it
            if (!isAvailableInDB(group, name, lDBProps))
            {
                MetricThreshold metricthreshold = new MetricThreshold();
                metricthreshold.setUName(name);
                metricthreshold.setUGroup(group);
                metricthreshold.setUGuid(""); //default empty string when not deployed
                metricthreshold.setUType("default");
                metricthreshold.setUHigh(highVal);
                metricthreshold.setULow(lowVal);
                metricthreshold.setUAlertStatus(alertStatus);
                metricthreshold.setSysCreatedBy("system");
                metricthreshold.setSysCreatedOn(GMTDate.getDate());
                metricthreshold.setSysModCount(0);

                lNewDBProps.add(metricthreshold);
            }
        }

        // insert it in the DB
        uploadMetricThresholdsInDB(lNewDBProps);

    } // initMetricThresholds
    
    public static void processValuesInMap(MetricThresholdVO metric, Map<String, MetricThresholdType> thresholdProperties) 
    {
        String metricname = metric.getUGroup() + "." + metric.getUName();
        MetricThresholdType metrictype = new MetricThresholdType();
        String regex = "[0-9]+";
        String high_value = metric.getUHigh();
        String low_value = metric.getULow();
        if(high_value.matches(regex) && low_value.matches(regex))
        {
            metrictype.setHigh(metric.getUHigh());
            metrictype.setLow(metric.getULow());
        }
        else if(high_value.matches(regex))
        {
            metrictype.setHigh(metric.getUHigh());
            metrictype.setLow("undef");
        }
        else if(low_value.matches(regex))
        {
            metrictype.setLow(metric.getULow());
            metrictype.setHigh("undef");
        }
        else
        {
            metrictype.setHigh("undef");
            metrictype.setLow("undef");
        }
        metrictype.setAlertstatus(metric.getUAlertStatus());
        
        thresholdProperties.put(metricname, metrictype);
   
    } // processValuesInMap
    
    ///////////////
    //private apis
    ///////////////
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static List<MetricThreshold> getDefaultThresholdValuesFromDB()
    {
        List<MetricThreshold> list = new ArrayList<MetricThreshold>();
        try
        {
            list = (List<MetricThreshold>) HibernateProxy.execute(() -> {
            	String sql = " from MetricThreshold ";
                Query q = HibernateUtil.createQuery(sql);
                return q.list();
            });            
        }
        catch (Throwable e)
        {
            e.printStackTrace();
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return list;
    } // getMetricThresholdsFromDB
    
    private static boolean isAvailableInDB(String group, String name, List<MetricThreshold> lDBProps)
    {
        boolean isAvailableInDB = false;

        forLoop: for (MetricThreshold p : lDBProps)
        {
            if (p.getUGroup().equals(group) && p.getUName().equals(name))
            {
                isAvailableInDB = true;
                break forLoop;
            }
        }

        return isAvailableInDB;
    } // isAvailableInDB
    
    private static void uploadMetricThresholdsInDB(List<MetricThreshold> lNewDBProps)
    {
        if (lNewDBProps != null && lNewDBProps.size() > 0)
        {
            String username = null;
            for (MetricThreshold model : lNewDBProps)
            {
                if(StringUtils.isEmpty(username))
                {
                    username = "system";
                }
                
                // persist it
                try
                {
                  HibernateProxy.setCurrentUser(username);
                	HibernateProxy.execute(() -> {

	                    if (StringUtils.isNotBlank(model.getSys_id()))
	                    {
	                        HibernateUtil.getDAOFactory().getMetricThresholdDAO().update(model);
	                    }
	                    else
	                    {
	                        model.setSys_id(null);
	                        HibernateUtil.getDAOFactory().getMetricThresholdDAO().persist(model);
	                    }
	                    

                	});
                }
                catch (Throwable e)
                {
                    Log.log.warn(e.getMessage());
                                      HibernateUtil.rethrowNestedTransaction(e);
                }
                
            }
        }

    } // uploadMetricThresholdsInDB
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public static int findHighestVersionNumber(String ruleModule, String ruleName, boolean includeMCP)
    {
        int result = -1;
        String hql = " select max(mt.UVersion) from MetricThreshold mt where lower(mt.URuleModule) = :namespace and lower(mt.URuleName) = :rulename ";
        
        if (!includeMCP)
        {
            hql += " and ( mt.UFromMCP <> 'true' or mt.UFromMCP is NULL ) ";
        }
        else
        {
            hql += " and mt.UFromMCP = 'true' ";
        }
        
        List<? extends Object> results = null;
		try {
			String hqlFinal = hql;
			results = (List<? extends Object>) HibernateProxy.execute(() -> {

				Query query = HibernateUtil.createQuery(hqlFinal);
				query.setParameter("namespace", ruleModule.toLowerCase());
				query.setParameter("rulename", ruleName.toLowerCase());
				return query.list();

			});
		} catch (Exception e) {
			Log.log.error("Error in import executing SQL: " + hql, e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}

        if (results!=null && results.size()>0)
        {
        	Object obj = results.get(0);
        	if (obj == null)
        	{
        		result = 0;
        	}
        	else
        	{
        		result = (Integer)obj;
        	}
        }
        return result;
    }    
    
    public static  List<MetricThreshold> getAutoDeploymentMetricThresholds()
    {
        List<MetricThreshold> result = new ArrayList<MetricThreshold>();
        QueryDTO query = new QueryDTO();
        query.setModelName("MetricThreshold");
        query.setStart(0);
        query.setLimit(-1);
        query.setWhereClause("UVersion > 0 and UType = 'default' and UActive = 'true' and ( UFromMCP <> 'true' or UFromMCP is NULL ) ");
        
        try
        {
            List<? extends Object> data = executeHQLSelectModel(query, 0, -1, true);  // find all rules first
            if(data != null)
            {
                Map<String, MetricThreshold> rulesMap = new HashMap<String, MetricThreshold>();
                for(Object o : data)
                {
                    MetricThreshold instance = (MetricThreshold) o;
                    
                    String key = "" + instance.getURuleModule() + instance.getURuleName();
                    Integer version = instance.getUVersion();
                    MetricThreshold current = rulesMap.get(key);
                    if (version!=null && StringUtils.isNotEmpty(instance.getURuleModule()) && StringUtils.isNotEmpty(instance.getURuleName()))
                    {
                        if (current==null)
                        {
                            rulesMap.put(key, instance);
                        }
                        else
                        {
                            if (instance.getUVersion()>current.getUVersion())
                            {
                                rulesMap.put(key, instance);
                            }
                        }
                    }
                }

                for(Object o : data)
                {
                    MetricThreshold instance = (MetricThreshold) o;
                    String key = "" + instance.getURuleModule() + instance.getURuleName();
//                    Integer version = instance.getUVersion();
                    MetricThreshold current = rulesMap.get(key);
                    if (instance.getUVersion()>=current.getUVersion())
                    {
                        result.add(instance);
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static void  mcpManualDeployMetricThresholds(String data, String action) throws JsonParseException, JsonMappingException, IOException
    {   
        Log.log.info("MCP manual deployment call. Action: " + action);
        Object dataObj = null;
        if (data!=null)
        {
            dataObj = StringUtils.stringToObj(data);
        }
        else
        {
            throw new RuntimeException("mcpapicall error: data is null. action=" + action);
        }
        Map<String, Object> mMap = (Map<String, Object>) dataObj; 
        
        List<MetricThreshold> thresholds = (ArrayList<MetricThreshold>) mMap.get("rules");
        String deployData = (String)mMap.get("deploy");
        
        for (MetricThreshold m : thresholds)
        {
            if (isManagedMCP())
            {
                m.setUFromMCP("true");
            }
            
            if (StringUtils.isNotEmpty(deployData))
            {
                if (Constants.MCP_MANUAL_DEPLOY.equals(action))
                {
                    m.setUIpaddress(deployData);
                }
                else if (Constants.MCP_UNDEPLOY.equalsIgnoreCase(action) || Constants.MCP_UNDEPLOY_ALL.equalsIgnoreCase(action))
                {
                    m.setUIpaddress("");
                }
            }
            SaveUtil.saveMetricThreshold(m, null);
        }
        if (thresholds!=null)
        {
            Log.log.info("MCP manual deployment call. Update current MetricThresholds. ");
            MainBase.getESB().sendMessage(Constants.ESB_NAME_METRIC,"MMetric.updateThresholds", null);
        }
    }
    

    @SuppressWarnings("unchecked")
    public static void  mcpAutoDeployMetricThresholds(String data, String action)
    {   
        if (isCentralMCP() || isLocalMCP())
        {
            Log.log.debug("Component is not a managed MCP. Bypass auto deploy call");
            return;
        }
        
        Log.log.info("MCP auto deployment call on action: " + action + ", MCP mode: " + MetricMsg.mcpMode);
        Object dataObj = null;
        if (data!=null)
        {
        	data = ESAPI.encoder().decodeForHTML(data);
            dataObj = StringUtils.stringToObj(data);
        }
        else
        {
            throw new RuntimeException("mcpapicall error: data is null. action=" + action);
        }
        
        deleteAllAutoDeploymentMetricThresholds(null, true);
        
        List<MetricThreshold> thresholds = (ArrayList<MetricThreshold>) dataObj; 
        for (MetricThreshold m : thresholds)
        {
            m.setUFromMCP("true");
            SaveUtil.saveMetricThreshold(m, null);
        }
        if (thresholds!=null)
        {
            Log.log.info("MCP auto deployment call. Update current MetricThresholds. ");
            MainBase.getESB().sendMessage(Constants.ESB_NAME_METRIC,"MMetric.updateThresholds", null);
        }
        
    }
    
    @SuppressWarnings("unchecked")
    public static void  mcpUndeployMetricThresholds(String data, String action)
    {   
        Log.log.info("MCP un-deploy call. Action: " + action);
        Object dataObj = null;
        if (data!=null)
        {
            dataObj = StringUtils.stringToObj(data);
        }
        else
        {
            throw new RuntimeException("mcpapicall error: data is null. action=" + action);
        }
        
        List<MetricThreshold> thresholds = (ArrayList<MetricThreshold>) dataObj; 
        for (MetricThreshold m : thresholds)
        {
            try
            {
                deleteMetricThresholdById(m.getSys_id(), "system", false);
            }
            catch (Exception e)
            {
                Log.log.error("Error unemploy MetricThreshold", e);
            }
        }
        if (thresholds!=null&&thresholds.size()>0)
        {
            Log.log.info("Undeploy metric threshold " + thresholds.get(0).getURuleModule() + thresholds.get(0).getURuleName());
            MainBase.getESB().sendMessage(Constants.ESB_NAME_METRIC,"MMetric.updateThresholds", null);
        }
    }
    
    public static Map<String, Object> getUIEditableMetricThresholds(QueryDTO query)
    {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        List<MetricThresholdVO> result = new ArrayList<MetricThresholdVO>();
        int totalSize = 0;
        int limit = query.getLimit();
        int offset = query.getStart();
        
        if (StringUtils.isNotBlank(query.getWhereClause()))
        {
            query.setWhereClause(query.getWhereClause() + " and ( UFromMCP != 'true' or UFromMCP is null ) " );
        }
        else
        {
            query.setWhereClause(" UFromMCP != 'true' or UFromMCP is null " );
        }
                        
        
        try
        {
            List<? extends Object> data = executeHQLSelectModel(query, offset, -1, true);  // find all rules first
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    MetricThreshold instance = new MetricThreshold();
                    if (columns.length > 1)
                    {
                        for(Object o : data)
                        {
                            Object[] record = (Object[]) o;
                                
                            //set the attributes in the object
                            for(int x=0; x < columns.length; x++)
                            {
                                String column = columns[x].trim().replace(" ", "");
                                if (column.contains("("))
                                {
                                    // there could be a column 'distinct (URuleModule)'
                                    column = StringUtils.substringBetween(column, "(", ")");
                                }
                                Object value = record[x];
                                
                                PropertyUtils.setProperty(instance, column, value);
                            }
                            result.add(instance.doGetVO());
                        }//end of for loop
                    }
                    else if (columns.length == 1)
                    {
                        for(Object obj : data)
                        {
                            String column = columns[0].trim().replace(" ", "");
                            if (column.contains("("))
                            {
                                // there could be a column 'distinct (URuleModule)'
                                column = StringUtils.substringBetween(column, "(", ")");
                            }
                            PropertyUtils.setProperty(instance, column, obj);
                            result.add(instance.doGetVO());
                        }
                    }
                }
                else
                {
                    Map<String, MetricThreshold> rulesMap = new HashMap<String, MetricThreshold>();
                    for(Object o : data)
                    {
                        MetricThreshold instance = (MetricThreshold) o;
                        
                        String key = "" + instance.getURuleModule() + instance.getURuleName();
                        Integer version = instance.getUVersion();
                        MetricThreshold current = rulesMap.get(key);
                        if (version!=null && StringUtils.isNotEmpty(instance.getURuleModule()) && StringUtils.isNotEmpty(instance.getURuleName()))
                        {
                            if (current==null)
                            {
                                rulesMap.put(key, instance);
                            }
                            else
                            {
                                if (instance.getUVersion()>current.getUVersion())
                                {
                                    rulesMap.put(key, instance);
                                }
                            }
                        }
                    }

                    List<MetricThresholdVO> tmpResult = new ArrayList<MetricThresholdVO>();

                    for(Object o : data)
                    {
                        MetricThreshold instance = (MetricThreshold) o;
                        if (instance.getUVersion()==null || StringUtils.isEmpty(instance.getURuleModule()) || StringUtils.isEmpty(instance.getURuleName()))
                        {
                            tmpResult.add(instance.doGetVO());
                        }
                        else
                        {
                            String key = "" + instance.getURuleModule() + instance.getURuleName();
                            //Integer version = instance.getUVersion();
                            MetricThreshold current = rulesMap.get(key);
                            if (instance.getUVersion()>=current.getUVersion())
                            {
                                tmpResult.add(instance.doGetVO());
                            }
                        }
                    }
                    
                    //paging
                    if (limit>-1 && offset>-1)
                    {
                        result = tmpResult.subList(offset, Math.min(offset + limit, tmpResult.size()));
                        totalSize = tmpResult.size();
                    }
                    else
                    {
                        result = tmpResult;
                        totalSize = result.size();
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        resultMap.put(UNIQUE_THRESHOLDS, result);
        resultMap.put(UNIQUE_THRESHOLDS_TOTAL, Integer.valueOf(totalSize));
        return resultMap;
    } //getUniqueMetricThreshold
    
    
    public static List<MetricThresholdVO> getMetricThreshold(QueryDTO query)
    {
        List<MetricThresholdVO> result = new ArrayList<MetricThresholdVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        try
        {
            List<? extends Object> data = executeHQLSelectModel(query, offset, limit, true);
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        MetricThreshold instance = new MetricThreshold();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        result.add(instance.doGetVO());
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        MetricThreshold instance = (MetricThreshold) o;
                        result.add(instance.doGetVO());
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    } //getMetricThreshold
    
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static List<? extends Object> executeHQLSelectModel(QueryDTO queryDTO, int start, int limit, boolean forUI) throws Exception
    {
        List result = null;

        if (StringUtils.isEmpty(queryDTO.getHql()) && StringUtils.isEmpty(queryDTO.getModelName()))
        {
            throw new Exception("Modelname is mandatory to execute this generic query.");
        }

      HibernateProxy.setCurrentUser("admin");
        result = (List) HibernateProxy.executeNoCache(() -> {
        	String hql = "";
	        try
	        {
	            //########## READ THIS #############/
	            //Make sure this condition is never removed from this method otherwise there will
	            //be catastrophic disaster at runtime. BD
	            if (StringUtils.isNotEmpty(queryDTO.getHql()))
	            {
	                //this case is when we have to create complex query using joins to more than 1 table
	                hql = queryDTO.getHql();
	            }
	            else
	            {
	                if (StringUtils.isEmpty(queryDTO.getModelName()))
	                {
	                    String tempTableName = queryDTO.getTableName();
	                    String modelName = HibernateUtil.table2Class(tempTableName);
	                    queryDTO.setModelName(modelName);
	                }
	
	                hql = queryDTO.getSelectHQL();
	//                hql += " order by tableData.UVersion desc ";
	                
	//                hql += " group by tableData.URuleName order by tableData.UVersion ";
	
	            }
	
	            Query query = HibernateUtil.createQuery(hql);
	
	            Log.log.trace("HQL to be Executed:" + hql);
	            //KEPT IT HERE - IN Case there is a need to find out the type of field using Introspection
	            //            String modelName = queryDTO.getModelName();
	            //            Class clazz = Class.forName("com.resolve.persistence.model." + modelName);
	            //            for (Field field : clazz.getDeclaredFields()) {
	            //                System.out.print("Field: " + field.getName() + " - ");
	            //                java.lang.reflect.Type type = field.getGenericType();
	            //                if (!(type instanceof ParameterizedType)) {
	            //
	            //                    System.out.println("Type: " + field.getType());
	            //                }
	            //            }
	
	            // Add in all the filtering parameters
	            for (QueryParameter param : queryDTO.getWhereParameters())
	            {
	                if (hql.contains(param.getName()))
	                {
	                    query.setParameter(param.getName(), param.getValue());
	                }
	            }
	            
	            // Add in all sort parameters
	            for (QuerySort qsort : queryDTO.getParsedSorts())
	            {
	                if (hql.contains(QuerySort.SORT_PROP_NAME_PREFIX + qsort.getProperty().replaceAll("\\.", "_")))
	                {
	                    query.setParameter(QuerySort.SORT_PROP_NAME_PREFIX + qsort.getProperty().replaceAll("\\.", "_"),
	                                       qsort.getProperty());
	                }
	            }
	
	            if (limit > -1)
	            {
	                query.setFirstResult(start);
	                query.setMaxResults(limit);
	            }
	
	            return query.list();

           
	        }
	        catch (Throwable t)
	        {
	            Log.log.error("Error in executing HQL for VO: " + hql, t);
	            throw new Exception(t);
	        }
        });
        //for UI, massage the data
        if (result != null && forUI && !(result instanceof List) )
        {
            List newResult = new ArrayList();
            for (Object o : result)
            {
                if (o instanceof Object[])
                {
                    Object[] oldArray = (Object[]) o;
                    Object[] newArray = new Object[oldArray.length];
                    for (int pos = 0; pos < oldArray.length; pos++)
                    {
                        Object oldValue = oldArray[pos];
                        Object newValue = GenericJDBCHandler.getMassagedData(oldValue, forUI);
                        newArray[pos] = newValue;
                    }

                    newResult.add(newArray);
                }
                else if (o instanceof BaseModel)
                {
                    newResult.add(o);
                    //                    ((BaseModel) o).massageDataForUI();
                    //                    ((BaseModel) o).doGetVO();
                }
            }
            result = newResult;
        }

        return result;
    }

    
    public static List<MetricThresholdVO> getMetricThreshold(QueryDTO query, String[] sysIds)
    {
        List<MetricThresholdVO> result = new ArrayList<MetricThresholdVO>();
        
        try
        {
            if(sysIds != null)
            {
                for(String sysId : sysIds)
                {
                    MetricThresholdVO mvo = findMetricThresholdById(sysId);
                    result.add(mvo);
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    } //getMetricThreshold
    
    //deploy or save records into  metric_threshold DB
    /**
     * This method created a Map of GUIDS and map of thresholds to be deployed to these GUID 
     * Then sends an ESB message to all the components and GUIDs involved.
     * 
     * @param guids
     * @param sys_ids
     * @param username
     * @return Map<String, HashMap<String, Object>> i.e. Map(GUID, HashMap<metricname, metricthresholdtype> Note: 
     * metricthresholdtype object has an string high value and string low value)
     */
    public static Map<String, Map<String, Object>> deployMetricThreshold(String[] guids, String[] sys_ids, String[] highs, String[] lows, String username)
    {
        Map<String, Map<String, Object>> mapGuidAndMetricThresholds = new HashMap<String, Map<String, Object>>();
        
      
        // deploying an already exisiting metric_threshold in the DB to a list of guids
        int index = 0;
        for(String sys_id : sys_ids)
        {
            MetricThresholdVO metricvo = findMetricThresholdById(sys_id);
            if(metricvo != null)
            {
                //Create the composite key for checking if an update is necessary
                String metricName = metricvo.getUName();
                String metricGroup = metricvo.getUGroup();
//                Integer metricHighValue = metricvo.getUHigh();
//                Integer metricLowValue = metricvo.getULow();
                for(String guid : guids)
                {
                    MetricThresholdVO getMetricComponent = findMetricThresholdDeployed(metricName, metricGroup, guid);
                    if(getMetricComponent != null)
                    {
                        // Found such a deployed metric so update its high and low values
                        getMetricComponent.setUHigh(highs[index]);
                        getMetricComponent.setULow(lows[index]);
                    }
                    else
                    {
                        ResolveRegistrationVO reg = ResolveRegistrationUtil.findResolveRegistrationByGuid(guid);
                        String ipaddress = null;
                        String componenttype = null;
                        if(reg != null)
                        {
                            ipaddress = reg.getUIpaddress();
                            componenttype = reg.getUType();
                        }
                        getMetricComponent = new MetricThresholdVO(metricName, metricGroup, guid, highs[index], lows[index], metricvo.getUAlertStatus(), metricvo.getUType(), ipaddress, componenttype, GMTDate.getDate(), username, metricvo.getUActive(), metricvo.getUAction(), metricvo.getUSource(), metricvo.getURuleModule(), metricvo.getURuleName(), metricvo.getUConditionValue(), metricvo.getUConditionOperator());
                    }
                    
                    // Save to the database
                    saveMetricThreshold(getMetricComponent, username);
                    
                    // Add it to the list of guid and metricThresholds
                    addToGuidMap(guid, metricName, metricGroup, highs[index], lows[index], metricvo.getUAlertStatus(), mapGuidAndMetricThresholds);
                }
            }
            else
            {
                try
                {
                    throw new Exception("Metric threshold deployment - no record with this sys_id available: " + sys_id);
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }            
            }
            index++;
        }
        
        //send ESB message to MMetric.updateMetricThresholdProperties in order to update the mthreshold.thresholdProperties map 
        for(String guid: mapGuidAndMetricThresholds.keySet())
        {
            Map<String, Object> params = mapGuidAndMetricThresholds.get(guid);
            params.put("GUID", guid);
            MainBase.getESB().sendMessage(Constants.ESB_NAME_METRIC,"MMetric.updateMetricThresholdProperties", params);
            Log.log.debug("Sending request to update metric threshold properties: ");
        }
 
        return mapGuidAndMetricThresholds;
    } // deployMetricThreshold
    
    /**
     * 
     * @param guid
     * @param metricname
     * @param metricgroup
     * @param metrichigh
     * @param metriclow
     * @param metrictype
     * @param username
     * @return
     */
    public static boolean addAndDeployMetricThreshold(String guid, String metricname, String metricgroup, String metrichigh, String metriclow, String metrictype, String username, String uActive, String action, String source, String module, String ruleName )
    {
        boolean status = false;
        String[] guids = { guid };
        MetricThresholdVO saveMTVO = new MetricThresholdVO( metricname, 
                                                        metricgroup,
                                                        "",
                                                        metrichigh,
                                                        metriclow,
                                                        "Warning",
                                                        metrictype,
                                                        null, // ipaddress of machine deployed to. This value will get filled during deployMetricThreshold()
                                                        null, // component type where deployed to. This value will get filled during deployMetricThreshold()
                                                        GMTDate.getDate(),
                                                        username,
                                                        uActive,
                                                        action,
                                                        source,
                                                        module,
                                                        ruleName,
                                                        null,
                                                        null);
        
        //Check if metric entry is deployed in a server and hence exists in database 
        MetricThresholdVO getDeployedMetricComponent = findMetricThresholdDeployed(metricname, metricgroup, guid);
        //Check if default metric threshold entry is available in database but not yet deployed
        MetricThresholdVO getDefaultMetricComponent = findMetricThresholdDeployed(metricname, metricgroup, "");
        
        // Note: If default high and low thresholds are already deployed => getDefaultMetricComponent!= null or getDeployedMetricComponent !=null
        // then no need to change the defaults
        // deploy the newly added metric threshold to the guids
        if(getDeployedMetricComponent == null && (getDefaultMetricComponent == null))
        {
            // Create an entry in the database
            saveMTVO = saveMetricThreshold(saveMTVO, username);
            String[] sys_ids = { saveMTVO.getSys_id() };
            String[] highs = { saveMTVO.getUHigh() };
            String[] lows = {saveMTVO.getULow() };
            deployMetricThreshold(guids, sys_ids, highs, lows, username);
            status = true;
        }

        return status;
    } //addAndDeployMetricThreshold
    
    
    private static void addToGuidMap(String guid, String metricName, String metricGroup, String metricHighValue, String metricLowValue, String metricalertstatus, Map<String, Map<String, Object>> mapGuidAndMetricThresholds)
    {
        MetricThresholdType mtype = new MetricThresholdType(metricHighValue, metricLowValue, metricalertstatus);
        Map<String, Object> metricthreshold = null;
        
        if(mapGuidAndMetricThresholds.keySet().contains(guid))
        {
            metricthreshold = mapGuidAndMetricThresholds.get(guid);
            if(metricthreshold == null || metricthreshold.isEmpty())
            {
                try
                {
                    throw new Exception();
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }       
            }
        }
        else
        {
            //New Guid Entry
            metricthreshold = new HashMap<String, Object>();
        }
        String fullMetricName = metricGroup+"."+metricName;
        metricthreshold.put(fullMetricName, mtype);
        mapGuidAndMetricThresholds.put(guid, metricthreshold);
        
    } //addToGuidMap

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static boolean hasMetricThresholdWithSameName(String module, String name, boolean includeMCP)
    {
        boolean result = false;
        try
        {
            result = (boolean) HibernateProxy.execute(() -> {
            	String hql = " from MetricThreshold mt where lower(mt.URuleModule) = :namespace and lower(mt.URuleName) = :rulename ";

                if (!includeMCP)
                {
                    hql += " and ( mt.UFromMCP <> 'true' or mt.UFromMCP is NULL )";
                }
                else
                {
                    hql += " and mt.UFromMCP = 'true' ";
                }            
                
                List<MetricThreshold> results = null;
                Query query = HibernateUtil.createQuery(hql);
                query.setParameter("namespace", module.toLowerCase());
                query.setParameter("rulename", name.toLowerCase());
                results = query.list();
                if (results!=null && results.size()>0)
                {
                    return true;
                }
                
                return false;
            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new RuntimeException(e); 
        }
        
        return result;
    } // findMetricThresholdById
    
    public static List<String> getRegisteredClusters(String group)
    {
        if (StringUtils.isEmpty(group))
        {
            group = Constants.MCP_DEFAULT_GROUP;
        }
        return findRegisteredClusters(group);
    }
    
    
    public static ResolveMCPLoginVO findMCPLoginVO(String group, String cluster)
    {    
        ResolveMCPLoginVO result = null;
        ResolveMCPLogin rmcp = findMCPLogin(group, cluster);
        if (rmcp!=null)
        {
            result = rmcp.doGetVO();
        }
        
        return result;
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static ResolveMCPLogin findMCPLogin(String group, String cluster)
    {    
        ResolveMCPLogin result = null;
        try
        {
			result = (ResolveMCPLogin) HibernateProxy.execute(() -> {
				String hql = " from ResolveMCPLogin mt where mt.UGroupName = :group and mt.UClusterName = :cluster ";

				List<ResolveMCPLogin> results = null;
				Query query = HibernateUtil.createQuery(hql);
				query.setParameter("group", group);
				query.setParameter("cluster", cluster);

				results = query.list();
				if (results != null && results.size() > 0) {
					return (ResolveMCPLogin) results.get(0);
				} else {
					return null;
				}
			});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new RuntimeException(e); 
        }
        
        return result;
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static MetricThresholdVO findMetricThreshold(String module, String name, Integer version, boolean includeMCP)
    {
        MetricThresholdVO result = null;
        try
        {
			result = (MetricThresholdVO) HibernateProxy.execute(() -> {

				String hql = " from MetricThreshold mt where mt.URuleModule = :namespace and mt.URuleName = :rulename ";
				if (version != null && version != 0) {
					hql = hql + " and mt.UVersion = :version ";
				} else {
					// for the revision, version could either be 0 or null.
					hql = hql + " and (UVersion is null OR UVersion = 0) ";
				}

				if (!includeMCP) {
					hql += " and ( mt.UFromMCP <> 'true' or mt.UFromMCP is NULL ) ";
				} else {
					hql += " and mt.UFromMCP = 'true' ";
				}

				List<MetricThreshold> results = null;
				Query query = HibernateUtil.createQuery(hql);
				query.setParameter("namespace", module);
				query.setParameter("rulename", name);
				
				if (version != null && version != 0) {
					query.setParameter("version", version);
				}
				
				results = query.list();

				if (results != null && results.size() > 0) {
					return results.get(0).doGetVO();
				} else {
					return null;
				}
			});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new RuntimeException(e); 
        }
        
        return result;
    } // findMetricThreshold
    
    public static MetricThresholdVO findMetricThresholdById(String sys_id)
    {
        MetricThreshold model = null;
        MetricThresholdVO result = null;
        
        if(!StringUtils.isEmpty(sys_id))
        {
            try
            {
                model = findMetricThresholdByIdPrivate(sys_id);
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }
        }
        
        if(model != null)
        {
            result = model.doGetVO();
        }
        
        return result;
    } // findMetricThresholdById
    
    public static Map<String, Object> findMetricThresholdByIdAndHighestEditableVersion(String sysId)
    {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	MetricThresholdVO metricThresholdVO = findMetricThresholdById(sysId);
    	if (metricThresholdVO != null)
    	{
    		Integer version = findHighestVersionNumber(metricThresholdVO.getURuleModule(), metricThresholdVO.getURuleName(), false);
    		if (version == -1)
    		{
    			result.put("ERROR", "Could not get the version number for this rule.");
    		}
    		
    		result.put("METRIC_THRESHOLD", metricThresholdVO);
    		result.put("VERSION", version);
    	}
    	else
    	{
    		result.put("ERROR", "Could not get the Metric Threshold.");
    	}
    	
    	return result;
    }
    
    public static Map<String, Object> getRevisionWithVersionCount(String module, String name)
    {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	MetricThresholdVO metricThresholdVO = findMetricThreshold(module, name, null, false);
    	if (metricThresholdVO != null)
    	{
    		Integer version = findHighestVersionNumber(module, name, false);
    		if (version == -1)
    		{
    			result.put("ERROR", "Could not get the version number for this rule.");
    		}
    		
    		result.put("METRIC_THRESHOLD", metricThresholdVO);
    		result.put("VERSION", version);
    	}
    	else
    	{
    		result.put("ERROR", "Could not get the Metric Threshold.");
    	}
    	
    	return result;
    }
    
    public static MetricThresholdVO saveMetricThreshold(MetricThresholdVO vo, String username)
    {
        if(vo != null)
        {
        	vo.setUVersion(0);
            MetricThreshold model = null;
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equals(VO.STRING_DEFAULT))
            {
                model = findMetricThresholdByIdPrivate(vo.getSys_id());
            }
            else
            {
                model = new MetricThreshold();
                if (hasMetricThresholdWithSameName(vo.getURuleModule(), vo.getURuleName(), false))
                {
                    throw new RuntimeException("Failed to create new threshold rule. Metric threshold with the same name already exists");
                }
            }
            
            // check if we already have same named module and use it. This will preserve the original case.
            Map<String, String> moduleNameMap = getUniqueModuleNames(false);
            if (moduleNameMap.get(vo.getURuleModule().toLowerCase()) != null)
            {
            	vo.setURuleModule(moduleNameMap.get(vo.getURuleModule().toLowerCase()));
            }
            model.applyVOToModel(vo);
            model = SaveUtil.saveMetricThreshold(model, username);
            
            updateLatestVersionAsOutdated(model.getURuleModule(), model.getURuleName(), username);

            vo = model.doGetVO();
        }
        
        return vo;
    } // saveMetricThreshold
    
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static boolean IsClusterRegistered(String group, String cluster)
    {
        ResolveMCPCluster result = null;
        try
        {
            result = (ResolveMCPCluster) HibernateProxy.execute(() -> {
            	 String hql = " select mt from ResolveMCPCluster mt left join mt.clusterGroup cg where mt.UClusterName = :cluster and cg.UGroupName = :group";
                 
                 List<ResolveMCPCluster> results = null;
                 Query query = HibernateUtil.createQuery(hql);
                 query.setParameter("group", group);
                 query.setParameter("cluster", cluster);
                 
                 results = query.list();
                 if (results!=null && results.size()>0)
                 {
                     return (ResolveMCPCluster)results.get(0);
                 }
                 return null;
            });
           
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new RuntimeException(e); 
        }
        
        if (result==null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static List<String> findRegisteredClusters(String group)
    {
        List<String> result = null;
        try
        {
        	List<ResolveMCPCluster> results = null;
        	results = (List<ResolveMCPCluster>) HibernateProxy.execute(() -> {
	            String hql = " select mt from ResolveMCPCluster mt left join mt.clusterGroup cg where cg.UGroupName = :group";
	            
	            Query query = HibernateUtil.createQuery(hql);
	            query.setParameter("group", group);
	            
	            return query.list();
        	});
        	
            if (results!=null && results.size()>0)
            {
            	result = new ArrayList<String>();
            	for (ResolveMCPCluster r : results)
            	{
            		result.add(r.getUClusterName());
            	}
            }
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new RuntimeException(e); 
        }
        return result;
    }
    
    public static ResolveMCPLoginVO saveMCPLogin(ResolveMCPLoginVO vo, String username)
    {
        if(vo != null)
        {
            if (StringUtils.isEmpty(vo.getUGroupName()))
            {
                vo.setUGroupName(Constants.MCP_DEFAULT_GROUP);
            }
            
            if (!IsClusterRegistered(vo.getUGroupName(), vo.getUClusterName()))
            {
                throw new RuntimeException("Cluster is not registered. cluster name: " + vo.getUClusterName() + ", group name: " + vo.getUGroupName());  
            }
        
            ResolveMCPLogin model = null;
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equals(VO.STRING_DEFAULT))
            {
                model = findMCPLoginByIdPrivate(vo.getSys_id());
            }
            else
            {
                model = findMCPLogin(vo.getUGroupName(), vo.getUClusterName());
                if (model==null)
                {
                    model = new ResolveMCPLogin();
                }
//                else
//                {
//                    try
//                    {
//                        System.out.println("Existing password is : " + CryptUtils.decrypt(model.getUPWDValue()));
//                    }
//                    catch(Exception ex)
//                    {
//                    }
//                }
            }
            
            model.setUClusterName(vo.getUClusterName());
            model.setUGroupName(vo.getUGroupName());
            
            String value = vo.getUPWDValue();
            if (!CryptUtils.isEncrypted(value))
            {
                try
                {
                    value = CryptUtils.encrypt(value);
                }
                catch(Exception ex)
                {
                    Log.log.error(ex);
                    throw new RuntimeException("Failed to save MCP login data", ex);
                }
            }
            model.setUPWDValue(value);
            
            try
            {
                if(StringUtils.isEmpty(username))
                {
                    username = "system";
                }

                ResolveMCPLogin modelFinal = model;
              HibernateProxy.setCurrentUser(username);
                HibernateProxy.execute(() -> {
	                if (modelFinal.getSys_id()==null)
	                {
	                    HibernateUtil.getCurrentSession().persist(modelFinal);
	                }
	                else
	                {
	                    HibernateUtil.getCurrentSession().replicate(modelFinal, org.hibernate.ReplicationMode.OVERWRITE);
	                }
                });
            }
            catch (Exception e)
            {
                Log.log.error("Failed to persist MCPLogin data: " + e.getMessage(), e);                
                              HibernateUtil.rethrowNestedTransaction(e);
            }
            
            vo = model.doGetVO();
        }
        
        return vo;
    } // saveMCPLogin
    
    /*
     * After saving a revision, the latest version of that rule, if present, is marked as outdated. 
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static void updateLatestVersionAsOutdated(String namespace, String ruleName, String username)
    {
        String hql = " from MetricThreshold mt where lower(mt.URuleModule) = :namespace and lower(mt.URuleName) = :rulename order by sysCreatedOn desc";
        List<? extends Object> results = null;
        try
        {
            results = (List<? extends Object>) HibernateProxy.execute(() -> {
            	 Query query = HibernateUtil.createQuery(hql);
                 query.setParameter("namespace", namespace.toLowerCase());
                 query.setParameter("rulename", ruleName.toLowerCase());
                 return query.list();
            });
           
        }
        catch (Exception e)
        {
            Log.log.error("Error in import executing SQL: " + hql, e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        if (results != null && results.size() > 0)
        {
            MetricThreshold model = (MetricThreshold)results.get(0);
            if (model.getUVersion() != null && model.getUVersion() > 0) // make sure, it's not a the revision
            {
                model.setIsOutdated(true); 
                SaveUtil.saveMetricThreshold(model, username);
            }
        }
    }
    
    /*
     * Get the map of unique modulenames where, key is lowercased and value is original
     * cased namespace
     */
	@SuppressWarnings("rawtypes")
	private static Map<String, String> getUniqueModuleNames(boolean includeMCP)
    {
    	QueryDTO query = new QueryDTO();
    	query.setModelName("MetricThreshold");
    	query.setSelectColumns("distinct(URuleModule)");
    
    	String hql = " URuleModule is not null ";
        if (!includeMCP)
        {
            hql += " and UFromMCP <> 'true' or UFromMCP is NULL ";
        }
        else
        {
            hql += " and UFromMCP = 'true' ";
        }
    	
        query.setWhereClause(hql);
        
    	Map<String, String> result = new HashMap<String, String>();
    	List list = null;
    	try
    	{
    		list = executeHQLSelectModel(query, -1, -1, false);
    	}
    	catch(Exception e)
    	{
    		Log.log.error(e.getMessage(), e);
    	}
    	
    	if (list != null)
    	{
    		for (Object obj : list)
    		{
    			String module = (String)obj;
    			if (module != null)
    			{
    				result.put(module.toLowerCase(), module);
    			}
    		}
    	}
    	
    	return result;
    }
    
    /**
     * An API to be called on the latest revision of the rule. If there are no versions,
     * a new rule will be created with version 1. If the versions are present, a new rule will be
     * created by setting version as previous version + 1
     * 
     * @param vo : MetricThresholdVO Latest revision.
     * @param username
     * @return
     */
    public static MetricThresholdVO saveVersionMetricThreshold(MetricThresholdVO vo, String username)
    {
        Integer version = null;
        if(vo != null)
        {
            MetricThreshold model = null;
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equals(VO.STRING_DEFAULT))
            {
                model = findMetricThresholdByIdPrivate(vo.getSys_id());
                
                if (model != null)  
                {
                	// Find the highest version number of the latest revision
                	version = findHighestVersionNumber(model.getURuleModule(), model.getURuleName(), false);
                    version++;
                    
                    // Save version will always create a new record wrt latest revision
                    model = new MetricThreshold();
                    vo.setSys_id(null);
                }
            }
            else
            {
            	// This condition should never occure.
                model = new MetricThreshold();
            }
            Log.log.debug("saveVersionMetricThreshold on threshold " + model.getURuleModule() + "." + model.getURuleName() + " on version " + model.getUVersion());
            vo.setUVersion(version);
            model.applyVOToModel(vo);
            model = SaveUtil.saveMetricThreshold(model, username);
            afterMCPRuleUpdate();
            //MainBase.getESB().sendMessage(Constants.ESB_NAME_METRIC,"MMetric.updateThresholds", null);
            vo = model.doGetVO();
        }
        
        return vo;
    } // saveMetricThreshold
    
    public static MetricThresholdVO copyMetricThreshold(MetricThresholdVO vo, String username)
    {
        if(vo != null)
        {
            MetricThreshold model = new MetricThreshold();
            model.applyVOToModel(vo);
            model.setUVersion(0);
            model.setSys_id(null);
            
            if (hasMetricThresholdWithSameName(vo.getURuleModule(), vo.getURuleName(), false))
            {
                throw new RuntimeException("Failed to create new threshold rule. Metric threshold with the same name already exists");
            }
            
            model = SaveUtil.saveMetricThreshold(model, username);
//            MainBase.getESB().sendMessage(Constants.ESB_NAME_METRIC,"MMetric.updateThresholds", null);
            vo = model.doGetVO();
        }
        
        return vo;
    } // saveMetricThreshold

    public static void mcpAllUndeployMetricThreshold(String id, String deployData)
    {
        mcpInstallMetricThreshold(id, deployData, Constants.MCP_UNDEPLOY_ALL);
    }
    
    public static void mcpUndeployMetricThreshold(String id, String deployData)
    {
        mcpInstallMetricThreshold(id, deployData, Constants.MCP_UNDEPLOY);
    } 

    public static void mcpDeployMetricThreshold(String id, String deployData)
    {
        mcpInstallMetricThreshold(id, deployData, Constants.MCP_MANUAL_DEPLOY);
    }
    
    @SuppressWarnings("unchecked")
    public static void mcpInstallMetricThreshold(String id, String deployData, String action)
    {
        Map<String,Map<String, List<String>>> deployMap = null;
        try
        {
            List<Map<String,Map<String,String>>> targets  = findAutoDeploymentTargets();
            List<Map<String,Map<String,String>>> newTargets = new ArrayList<Map<String,Map<String,String>>>();
            
            if (!Constants.MCP_UNDEPLOY_ALL.equals(action))
            {                
                if (deployData!=null)
                {
                    deployMap = (Map<String,Map<String, List<String>>>) (new ObjectMapper().readValue(deployData, new TypeReference<Map<String,Map<String, List<String>>>>() { }));
                }
            
                if (deployMap==null || deployMap.isEmpty())
                    return;
            
                for (Map<String,Map<String,String>> cluster : targets)
                {
                    for (String name : cluster.keySet())
                    {
                        if (deployMap.containsKey(name))
                        {
                            newTargets.add(cluster);
                        }
                    }
                }
            }
            else
            {
                newTargets = targets;
            }
                
            if (newTargets.isEmpty())
            {
                String msg = "No registered cluster can be found for deployment";
                Log.log.error(msg);
                throw new RuntimeException(msg);
            }
            
            MetricThreshold model = null;
            if(!StringUtils.isEmpty(id))
            {
                try
                {
                    model = findMetricThresholdByIdPrivate(id);
                }
                catch(Exception e)
                {
                    Log.log.warn(e.getMessage(), e);
                }
            }
            if (model==null)
            {
                String msg = "No threshold can be found with id: " + id;
                Log.log.error(msg);
                throw new RuntimeException(msg);
            }

            List<MetricThreshold> mtds = new ArrayList<MetricThreshold>();
            mtds.add(model);
            
            for (Map<String,Map<String,String>> clusterMap : newTargets)
            {
                if (clusterMap.isEmpty())
                    continue;
                
//                boolean success = false;
//                String currentCluster = null;
                for (String cluster : clusterMap.keySet())
                {
//                    currentCluster = cluster;
                    Map<String,String> hostMap = clusterMap.get(cluster);
                    String host = hostMap.get("__HOST__");
                    String port = hostMap.get("__PORT__");
                    
                    String username = REMOTE_AUTO_DEPLOY_USERNAME;
                    String pwd = REMOTE_AUTO_DEPLOY_P_ASSWORD;
                    ResolveMCPLoginVO login = findMCPLoginVO(hostMap.get(Constants.MCP_GROUP_NAME), cluster);
                    if (login!=null && StringUtils.isNotEmpty(login.getUPWDValue()))
                    {                
                        String val = login.getUPWDValue();
                        try
                        {
                            pwd = CryptUtils.decrypt(val);
                        }
                        catch(Exception ex)
                        {
                            Log.log.error(ex);
                        }
                    }
                    
                    if (mcpAPICall(host, port, username, pwd,  mtds, action, deployData))
                    {
//                        success = true;
                        break;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Log.log.error(ex,ex);
            throw new RuntimeException(ex);
        }
    }
    
    public static void deleteMetricThresholdByIds(String[] sysIds, String username, boolean includeMCP) throws Exception
    {
        boolean ruleDeleted = false;
        if(sysIds != null)
        {
            for(String sysId : sysIds)
            {
                ruleDeleted = deleteMetricThresholdById(sysId, username, includeMCP);
            }
        }
        if (ruleDeleted)
        {
            afterMCPRuleUpdate();
        }

    } // saveMetricThreshold

    
    public static List<Map<String,Map<String,String>>> findAutoDeploymentTargets()
    {
        List<Map<String,Map<String,String>>> result = new ArrayList<Map<String,Map<String,String>>>();
        List<ResolveMCPGroupVO> lmcps = MCPUtils.getRegisteredInstances(false);
        if (lmcps!=null && !lmcps.isEmpty())
        {
            for (ResolveMCPGroupVO rmpg : lmcps)
            {
                Collection<ResolveMCPClusterVO> rmcpcs =  rmpg.getChildren();
                for (ResolveMCPClusterVO rmcpc : rmcpcs)
                {
                    Map<String,Map<String,String>> clusterMap = new HashMap<String, Map<String,String>>(); 
                    Collection<ResolveMCPHostVO> rmcphs = rmcpc.getChildren();
                    for (ResolveMCPHostVO rmcph : rmcphs)
                    {
                        Map<String,String> map = new HashMap<String,String>();
                        Collection<ResolveMCPComponentVO> rmcpcpts = rmcph.getMcpComponents();
                        for (ResolveMCPComponentVO rmcpcpt : rmcpcpts)
                        {
                            String cmpName = rmcpcpt.getUComponentName();
                            if (cmpName.contains("RSView"))
                            {
                                String port = cmpName.substring(cmpName.indexOf(Constants.MCP_NAMEPORT_SEPARATOR) + Constants.MCP_NAMEPORT_SEPARATOR.length());
                                if (StringUtils.isNotEmpty(port))
                                {
                                    map.put("__HOST__", rmcph.getUHostName());
                                    map.put("__PORT__", port);
                                    map.put(Constants.MCP_GROUP_NAME, rmpg.getUGroupName());
                                }
                            }
                        }
                        if (!map.isEmpty())
                        {
                            clusterMap.put(rmcpc.getUClusterName(), map);
                        }
                    }
                    if (!clusterMap.isEmpty())
                    {
                        result.add(clusterMap);
                    }
                }
            }
        }
        
        return result;
    }

    public static void afterMCPRuleUpdate()
    {
        
        MainBase.getESB().sendMessage(Constants.ESB_NAME_METRIC,"MMetric.updateThresholds", null);
        Log.log.debug("afterMCPRuleUpdate, isCentralMCP is " + isCentralMCP());
        if (isCentralMCP())
        {
            sendMCPAutoDeployRules();
        }
    }
    
    public static void sendMCPAutoDeployRules()
    {
        try
        {
            if (REMOTE_AUTO_DEPLOY_USERNAME==null || REMOTE_AUTO_DEPLOY_P_ASSWORD == null)
            {
                sendMCPAutoDeployRules("admin", "resolve");
            }
            else
            {
                sendMCPAutoDeployRules(REMOTE_AUTO_DEPLOY_USERNAME, REMOTE_AUTO_DEPLOY_P_ASSWORD);
            }
        }
        catch (Throwable ex)
        {
            Log.log.info(ex,ex);
        }
    }
    
    public static void sendMCPAutoDeployRules(String username, String password)
    {
        List<Map<String,Map<String,String>>> clusterMaps =  findAutoDeploymentTargets();
        if (clusterMaps==null || clusterMaps.isEmpty())
        {
            return;
        }
        List<MetricThreshold> mtds = MetricUtil.getAutoDeploymentMetricThresholds();
        
        for (Map<String,Map<String,String>> clusterMap : clusterMaps)
        {
            if (clusterMap.isEmpty())
                continue;
            
            boolean success = false;
            String currentCluster = null;
            for (String cluster : clusterMap.keySet())
            {
                currentCluster = cluster;
                Map<String,String> hostMap = clusterMap.get(cluster);
                String host = hostMap.get("__HOST__");
                String port = hostMap.get("__PORT__");
                
                ResolveMCPLoginVO login = findMCPLoginVO(hostMap.get(Constants.MCP_GROUP_NAME), cluster);
                if (login!=null && StringUtils.isNotEmpty(login.getUPWDValue()))
                {                
                    String val = login.getUPWDValue();
                    try
                    {
                        password = CryptUtils.decrypt(val);
                    }
                    catch(Exception ex)
                    {
                        Log.log.error(ex);
                    }
                }
                
                Log.log.debug("Sending "  + mtds.size() + " auto deploy rules to cluster: " + cluster + ", host: " + host + ", port: " + port); 
//                if (mcpAPICall(host, port, "admin", "resolve",  mtds, Constants.MCP_AUTO_DEPLOY, null))
                if (mcpAPICall(host, port, username, password,  mtds, Constants.MCP_AUTO_DEPLOY, null))
                {
                    success = true;
                    Log.log.debug("Sucess: Sending "  + mtds.size() + " auto deploy rules to cluster: " + cluster + ", host: " + host + ", port: " + port); 
                    break;
                }
                else
                {
                    Log.log.debug("Failed: Sending "  + mtds.size() + " auto deploy rules to cluster: " + cluster + ", host: " + host + ", port: " + port); 
                }
            }
            Log.log.info("Complete auto deployment to cluster " + currentCluster + ". Success= " + success);
        }
    }    
    
    
    @SuppressWarnings("rawtypes")
    private static boolean mcpAPICall(String host, String portValue, String username, String p_assword,  List<MetricThreshold> rules, String action, String deployData)
    {
        boolean success = true;

        try
        {
            ESAPI.validator().getValidInput("host", host, "ResolveText", 255, false);
            ESAPI.validator().getValidInput("portValue", portValue, "ResolveText", 10, false);
            ESAPI.validator().getValidInput("username", username, "ResolveText", 255, false);
            ESAPI.validator().getValidInput("p_assword", p_assword, "ResolveText", 100, false);
            ESAPI.validator().getValidInput("action", action, "ResolveText", 100, false);
            
            if (StringUtils.isNotBlank(deployData))
            {
                ESAPI.validator().getValidInput("deployData", deployData, "ResolveText", 100, false);
            }
            
            // Protocol (http/https) should be based on blueprint property 
            String protocol = "http://";
            String mcpURI = host; //"localhost";
            String port = ":" + portValue; //":8080";

            String serviceUrl = new StringBuilder(protocol).append(mcpURI).append(port).toString();
            String restURL = protocol + mcpURI + port + "/resolve/service/metric/mcpapicall";
            
            Log.log.debug("Rest URL: " + restURL);
            
            //TODO: Update session init when starting support for https
			HttpClient httpClient = CSRFUtil.initSession(serviceUrl);

			String initialToken = CSRFUtil.fetchInitialToken(httpClient, serviceUrl);
			CSRFUtil.login(httpClient, serviceUrl, username, p_assword);
			
			String token = CSRFUtil.fetchToken(httpClient, serviceUrl, initialToken);

            HttpPost httppost = new HttpPost(restURL);
            httppost.setHeader("X-Requested-With", "XMLHttpRequest");
            httppost.setHeader(HttpHeaders.REFERER, serviceUrl);
            
            String[] tokens = CSRFUtil.parseCSRFToken(token);
            httppost.setHeader(tokens[0], tokens[1]);

            ArrayList<NameValuePair> postParameters;
            
            String dataStr = null;
            if (Constants.MCP_AUTO_DEPLOY.equals(action))
            {
                dataStr = StringUtils.objToString(rules);
            }
            else
            {
                Map<String, Object> mMap = new HashMap<String, Object>();
                mMap.put("rules", rules);
                mMap.put("deploy", deployData);
                dataStr = StringUtils.objToString(mMap);
            }
            
            ESAPI.validator().getValidInput("dataStr", dataStr, "ResolveText", 65536, false, false);
            
            postParameters = new ArrayList<NameValuePair>();
            postParameters.add(new BasicNameValuePair("_username_", /*ESAPI.encoder().decodeForHTML(*/ESAPI.encoder().encodeForHTML(username))/*)*/);
            postParameters.add(new BasicNameValuePair("_password_", /*ESAPI.encoder().decodeForHTML(*/ESAPI.encoder().encodeForHTML(p_assword))/*)*/);
            postParameters.add(new BasicNameValuePair("data", /*ESAPI.encoder().decodeForHTML(*/ESAPI.encoder().encodeForHTML(dataStr))/*)*/);
            postParameters.add(new BasicNameValuePair("action", /*ESAPI.encoder().decodeForHTML(*/ESAPI.encoder().encodeForHTML(action))/*)*/);
            
            httppost.setEntity(new UrlEncodedFormEntity(postParameters, Charset.forName("UTF-8")));
            
            HttpResponse response = httpClient.execute(httppost);
            
            Log.log.debug("HttpResponse status line: " + response.getStatusLine());
            Log.log.debug("HttpResponse entity: " + response.getEntity());
            
            String jsonStr = EntityUtils.toString(response.getEntity());
            
            Log.log.debug("HttpResponse JSON: " + jsonStr);
            
            ResponseDTO result = new ObjectMapper().readValue(jsonStr, ResponseDTO.class);
            success = result.isSuccess();
            Log.log.info("Completed auto deploy rules call to server: " + host + ", port: " + port);
        }
        catch (Exception e)
        {
            String err = "Failed sending auto deploy rules call to server: " + host + ". " + e.getMessage();
            Log.log.error(err, e);
            success = false;
        }
        
        return success;
    }
    
    /**
     * HQL:
     * select tableData from MetricThreshold tableData  where UGuid not in ('') order by  UComponenttype ASC , UName ASC
     * 
     * 
     * SQL:
     *  select  
        where ... 
        order by ...

     * @param query
     * @return
     */
    public static List<MetricThresholdVO> getListOfMetricServers(QueryDTO query)
    {
        List<MetricThresholdVO> result = new ArrayList<MetricThresholdVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        query.setModelName("MetricThreshold");
        query.setWhereClause("UGuid not in ('')");      
        String hql = query.getSelectHQL();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelect(hql, offset, limit);
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        MetricThresholdVO instance = new MetricThresholdVO();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        result.add(instance);
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        MetricThreshold instance = (MetricThreshold) o;
                        result.add(instance.doGetVO());
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    } // MetricThresholdVO
    
    public static List<MetricThresholdVO> getListOfDeployedMetricServers(QueryDTO query)
    {
        List<MetricThresholdVO> result = new ArrayList<MetricThresholdVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        String hql = query.getSelectHQL();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelect(hql, offset, limit);
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        MetricThresholdVO instance = new MetricThresholdVO();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        result.add(instance);
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        MetricThreshold instance = (MetricThreshold) o;
                        result.add(instance.doGetVO());
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    } // getListOfDeployedMetricServers
    
    
    //private apis
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static boolean deleteMetricThresholdById(String sysId, String username, boolean includeMCP) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	return (boolean) HibernateProxy.execute(() -> {
            	boolean modelDeleted = false;
            
	            MetricThreshold model = HibernateUtil.getDAOFactory().getMetricThresholdDAO().findById(sysId);
	            if(model != null)
	            {
	                
	                String hql = " from MetricThreshold mt where mt.URuleModule = :namespace and mt.URuleName = :rulename ";
	                
	                if (!includeMCP)
	                {
	                    hql += " and ( mt.UFromMCP <> 'true' or mt.UFromMCP is NULL )";
	                }
	                else
	                {
	                    hql += " and mt.UFromMCP = 'true' ";
	                }
	                
	                List<MetricThreshold> results = null;
	                if (model.getURuleModule()!=null && model.getURuleName()!=null)
	                {
	                    Query query = HibernateUtil.createQuery(hql);
	                    query.setParameter("namespace", model.getURuleModule());
	                    query.setParameter("rulename", model.getURuleName());
	                    results = query.list();
	                    for (MetricThreshold mtd : results)
	                    {
	                        HibernateUtil.getDAOFactory().getMetricThresholdDAO().delete(mtd);
	                        modelDeleted = true;
	                    }
	                }
	                else
	                {
	                    HibernateUtil.getDAOFactory().getMetricThresholdDAO().delete(model);
	                    modelDeleted = true;
	                }
	            }
	            
	            return modelDeleted;
            });
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new Exception(e); 
        }
    } //deleteMetricThresholdById
    
    private static MetricThreshold findMetricThresholdByIdPrivate(String sys_id)
    {
        MetricThreshold model = null;
        if(!StringUtils.isEmpty(sys_id))
        {
            try
            {
				model = (MetricThreshold) HibernateProxy.execute(() -> {
					return HibernateUtil.getDAOFactory().getMetricThresholdDAO().findById(sys_id);
				});
                
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        return model;
    } // findMetricThresholdByIdPrivate
 
    private static ResolveMCPLogin findMCPLoginByIdPrivate(String sys_id)
    {
        ResolveMCPLogin model = null;
        if(!StringUtils.isEmpty(sys_id))
        {
            try
            {
				model =  (ResolveMCPLogin) HibernateProxy.execute(() -> {
					return HibernateUtil.getCurrentSession().get(ResolveMCPLogin.class, sys_id);
				});
            }
            catch(Exception e)
            {
                Log.log.warn(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        return model;
    } // findMetricThresholdByIdPrivate

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static MetricThresholdVO findMetricThresholdDeployed(String metricname, String metricgroup, String guid)
    {
        MetricThresholdVO result = null;
        
//        if(StringUtils.isNotEmpty(guid))
//        {
            MetricThreshold model = null;

            // query metric_threshold database to find a match
		try {
			List<MetricThreshold> temp = (List<MetricThreshold>) HibernateProxy.execute(() -> {
				String hql = "from MetricThreshold where UName='" + metricname + "' and UGroup='" + metricgroup
						+ "' and upper(UGuid) = '" + guid.toUpperCase() + "'";

				Query query = HibernateUtil.createQuery(hql);

				return query.list();
			});
			
			if (temp.size() == 1) {
				model = (MetricThreshold) temp.get(0);
			} else if (temp.size() > 1) {
				throw new Exception("This will be a unique composite key violation for guid: " + guid);
			}

		} catch (Exception e) {
			Log.log.warn(e.getMessage(), e);
                             HibernateUtil.rethrowNestedTransaction(e);
		}
            
            if(model != null)
            {
                result = model.doGetVO(); 
            }
            
            
//        }
        
        return result;
    } //findMetricThresholdDeployed

    public static boolean isCentralMCP()
    {
        return MetricMsg.mcpMode.equals(Constants.MCP_MODE_CENTRAL);
    }
    
    public static boolean isLocalMCP()
    {
        return MetricMsg.mcpMode.equals(Constants.MCP_MODE_LOCAL);
    }

    public static boolean isManagedMCP()
    {
        return MetricMsg.mcpMode.equals(Constants.MCP_MODE_MANAGED);    
    }

    public static boolean isLocalCentral()
    {
        return MetricMsg.mcpMode.equals(Constants.MCP_MODE_LOCAL) || MetricMsg.mcpMode.equals(Constants.MCP_MODE_CENTRAL);
    }

    
    @SuppressWarnings("rawtypes")
    private static void deleteAllAutoDeploymentMetricThresholds(String username, boolean onlyMCP) 
    {
        Log.log.debug("delete all auto deploy thresholds with onlyMCP: " + onlyMCP);
        try
        {
            if (username == null)
            {
                username = "system";
            }
            
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {

	            String hql = " delete MetricThreshold where UType = 'default' ";
	            if (onlyMCP)
	            {
	                hql += " and UFromMCP = 'true' "; 
	            }
	            Query query = HibernateUtil.createQuery(hql);
	            query.executeUpdate();
        	});
            HibernateUtil.evictHibernateRegion("com.resolve.persistence.model.MetricThreshold");
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    } //deleteAllMetricThresholds
    

    public static void saveMetricTimer(MetricTimer metricTimer)
    {
        MetricIndexAPI.indexMetricTimer(metricTimer, "system");
    }
    
} //MetricUtil
