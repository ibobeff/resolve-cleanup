/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.persistence.model.WikiDocumentMetaFormRel;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.OrganizationUtil;
import com.resolve.services.hibernate.util.UserUtils;
import com.resolve.services.hibernate.vo.WikiDocumentMetaFormRelVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SearchWikiTemplates
{
    private QueryDTO query = null; 
    private String username = null;
    
    private int start = -1;
    private int limit = -1;
    private Set<String> userRoles = null;

    public SearchWikiTemplates(QueryDTO query, String username) 
    {
        if(query == null || StringUtils.isEmpty(username))
        {
            throw new RuntimeException("Query and username are mandatory to search for wiki template");
        }
        
        this.query = query;
        this.username = username;
        
        this.start = query.getStart();
        this.limit = query.getLimit();
        userRoles = UserUtils.getUserRoles(this.username);
    }
    
    public List<WikiDocumentMetaFormRelVO> execute()
    {
        List<WikiDocumentMetaFormRelVO> result = new ArrayList<WikiDocumentMetaFormRelVO>();
        this.query.setPrefixTableAlias("wd.");
        
        String hql = createHql();
        
        //set it in the query object
        this.query.setHql(hql.toString());

        try
        {
            //execute the qry
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, start, limit, true);
            if(data != null)
            {
                //grab the map of sysId-organizationname  
                Map<String, String> mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();

                String selectedColumns = query.getSelectColumns();
                if (StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");

                    //this will be the array of objects
                    for (Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        WikiDocumentMetaFormRel instance = new WikiDocumentMetaFormRel();

                        //set the attributes in the object
                        for (int x = 0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];

                            PropertyUtils.setProperty(instance, column, value);
                        }

                        result.add(convertModelToVO(instance, mapOfOrganization));
                    }//end of for loop
                }
                else
                {
                    for (Object o : data)
                    {
                        WikiDocumentMetaFormRel instance = (WikiDocumentMetaFormRel) o;
                        result.add(convertModelToVO(instance, mapOfOrganization));
                    }
                }
            }//end of if
            
        }
        catch (Exception e)
        {
            Log.log.error("Error in getting data for sqk:" + hql.toString(), e);
        }
        
        
        return result;
    }
    
    private String createHql()
    {
        StringBuilder hql = new StringBuilder("select ");
        
        if(StringUtils.isNotBlank(query.getSelectColumns()))
        {
            StringBuilder updatedCols = new StringBuilder();
            String[] selectedCols = query.getSelectColumns().split(",");
            for(String col : selectedCols)
            {
                updatedCols.append("wd.").append(col).append(",");
            }
            
            //remove the last comma
            updatedCols.setLength(updatedCols.length()-1);
            
            hql.append(updatedCols);
        }
        else
        {
            hql.append(" wd ");
        }
        
        hql.append(" from WikiDocumentMetaFormRel as wd ");
        //NOTE- somehow left outer join is not working...so this is a crude way of returning all the recs on the UI as this app is
        //mostly be used by 'admin'
        if(!this.userRoles.contains("admin"))
        {
            hql.append(" left join wd.accessRights as ar ");
        }
        hql.append(" where  ");
        if(!this.userRoles.contains("admin"))
        {
            hql.append(" ar.UResourceId = wd.sys_id and ar.UResourceType ='" + WikiDocumentMetaFormRel.RESOURCE_TYPE + "' and ");
        }

        
//        hql.append(" from WikiDocumentMetaFormRel as wd, AccessRights as ar where ");
//        hql.append(" ar.UResourceId = wd.sys_id and ar.UResourceType ='" + WikiDocumentMetaFormRel.RESOURCE_TYPE + "' and ");

        //role filter 
        String roleFilter = "";
        //qry only if its not an 'admin', else show everything
        if (!this.userRoles.contains("admin") && !username.equals("resolve.maint"))
        {
            StringBuilder filters = new StringBuilder("(");
            for (String userRole : userRoles)
            {
                filters.append(" ar.UAdminAccess like '%" + userRole + "%' or ar.UReadAccess like '%" + userRole + "%' or");
            }
            filters.setLength(filters.length() - 2);
            filters.append(")");  
            roleFilter = filters.toString();
        }
        else
        {
            roleFilter = " 1=1 ";
        }
        hql.append(roleFilter);
        
        //additional where clause
        String whereClause = this.query.getWhereClause();
        if(StringUtils.isNotBlank(whereClause))
        {
            hql.append(" and ").append(whereClause);
        }
        
        //search filter
        String searchQry = this.query.getFilterWhereClause();
        if (StringUtils.isNotBlank(searchQry))
        {
            hql.append(" and ").append(searchQry);
        }
        
        //sort condition --> [{"property":"name","direction":"DESC"}]
        //comes from the UI...so will have to add the 'wd.' in front of the properties
        this.query.parseSortClause();
        String sortBy = this.query.getOrderBy();
        if(StringUtils.isBlank(sortBy))
        {
            sortBy = this.query.getSort();
        }
        
        if(StringUtils.isNotBlank(sortBy))
        {
            hql.append(" order by ").append(sortBy.trim());
        }
        
        
        return hql.toString();
    }
    
    
    
    
    //private apis    
    private static WikiDocumentMetaFormRelVO convertModelToVO(WikiDocumentMetaFormRel model, Map<String, String> mapOfOrganization)
    {
        WikiDocumentMetaFormRelVO vo = null;

        if (model != null)
        {
            if (mapOfOrganization == null)
            {
                mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
            }

            vo = model.doGetVO();
            if (StringUtils.isNotBlank(vo.getSysOrg()))
            {
                vo.setSysOrganizationName(mapOfOrganization.get(vo.getSysOrg()));
            }
            else
            {
                vo.setSysOrganizationName("");
            }

        }

        return vo;
    }    
    
}
