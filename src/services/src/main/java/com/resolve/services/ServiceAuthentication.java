/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services;

import java.util.List;

import com.resolve.services.hibernate.util.AuthenticationUtil;
import com.resolve.services.hibernate.vo.ConfigActiveDirectoryVO;
import com.resolve.services.hibernate.vo.ConfigLDAPVO;
import com.resolve.services.hibernate.vo.ConfigRADIUSVO;
import com.resolve.util.StringUtils;

public class ServiceAuthentication
{
    public static boolean isDomainExist(String domain)
    {
        boolean result = false;
        
        if (StringUtils.isNotBlank(domain))
        {
            if (isLDAPDomainExist(domain))
            {
                result = true;
            }
            else if (isActiveDirectoryDomainExist(domain))
            {
                result = true;
            }
        }
        
        return result;
    }

    public static boolean isActiveDirectoryDomainExist(String domain)
    {
        return AuthenticationUtil.isActiveDirectoryDomainExist(domain);
    }
    
    public static ConfigActiveDirectoryVO getActiveDirectoryFor(String domain)
    {
        return AuthenticationUtil.getActiveDirectoryFor(domain);
    }
    
//    public static ConfigActiveDirectoryVO setDefaultActiveDirectory(ConfigActiveDirectoryVO configActiveDirectoryVO)
//    {
//        return AuthenticationUtil.setDefaultActiveDirectory(configActiveDirectoryVO);
//    }
    
    public static boolean isLDAPDomainExist(String domain)
    {
        return AuthenticationUtil.isLDAPDomainExist(domain);
    }

    public static ConfigLDAPVO getLDAPFor(String domain)
    {
        return AuthenticationUtil.getLDAPFor(domain);
    }
    
//    public static ConfigLDAPVO setDefaultLDAP(ConfigLDAPVO vo)
//    {
//        return AuthenticationUtil.setDefaultLDAP(vo);
//    }
    
    public static boolean isOrgEnabled(String sys_id)
    {
        return AuthenticationUtil.isOrgEnabled(sys_id);
    }
    
    public static boolean updateAndCheckLogin(String username, String sourceAddr)
    {
        return AuthenticationUtil.updateAndCheckLogin(username, sourceAddr);
    }

    public static void updateLoginFailure(String username)
    {
        AuthenticationUtil.updateLoginFailure(username);
    }
    
    public static void validateIfGroupRequiredForUser(String username, List<String> memberOf, String authType) throws Exception
    {
        AuthenticationUtil.validateIfGroupRequiredForUser(username, memberOf, authType);
    }

    public static void assignUserGroups(String username, List<String> memberOf, String authType)
    {
        AuthenticationUtil.assignUserGroups(username, memberOf, authType);
    }

    public static void invalidateLDAPCache(ConfigLDAPVO configLDAPVO, String username)
    {
        AuthenticationUtil.invalidateLDAPCache(configLDAPVO, username);
    }

    public static void invalidateADCache(ConfigActiveDirectoryVO configActiveDirectoryVO, String username)
    {
        AuthenticationUtil.invalidateADCache(configActiveDirectoryVO, username);
    }
    
    public static void invalidateRADIUSCache(ConfigRADIUSVO configRADIUSVO, String username)
    {
        AuthenticationUtil.invalidateRADIUSCache(configRADIUSVO, username);
    }
    
    public static boolean isRADIUSDomainExist(String domain)
    {
        return AuthenticationUtil.isRADIUSDomainExist(domain);
    }
    
    public static ConfigRADIUSVO getRADIUSFor(String domain)
    {
        return AuthenticationUtil.getRADIUSFor(domain);
    }
}
