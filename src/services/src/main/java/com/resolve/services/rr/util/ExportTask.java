package com.resolve.services.rr.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.resolve.rsbase.MainBase;
import com.resolve.services.ServiceResolutionRouting;
import com.resolve.services.hibernate.vo.RRRuleFieldVO;
import com.resolve.services.hibernate.vo.RRRuleVO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class ExportTask extends BaseBulkTask{
    private String filePath;
    private List<String> headers;
    private ExportTaskResponseWriter writer;
 
    public ExportTask(final QueryDTO query,final String username) throws Exception {
        this.init(query,username,BaseBulkTask.EXPORT);
        this.filePath = MainBase.getTmpDir() + "/rr_tmp/" + this.username + (new Date()).getTime() + ".csv";
    }
    public ExportTask(final List<String> ids,final String username)
    {
        this.init(ids,username,BaseBulkTask.EXPORT);
        this.filePath = MainBase.getTmpDir() + "/rr_tmp/" + this.username + (new Date()).getTime() + ".csv";
    } 
   
    public String getFilePath()
    {
        return filePath;
    }

    private String ruleToLine(RRRuleVO rule){
        List<String> csv = new ArrayList<String>();
        for(int i = 0;i<this.headers.size();i++) {
            String header = this.headers.get(i);
            if(header.equals("module"))
                csv.add(rule.getModule());
            else if(header.equals("schema"))
                csv.add(rule.getSchema().getName());
            else if(header.equals("wiki"))
                csv.add(rule.getWiki());
            else if(header.equals("automation"))
                csv.add(rule.getAutomation());
            else if(header.equals("runbook"))
                csv.add(rule.getRunbook());
            else if(header.equals("eventid"))
                csv.add(rule.getEventId());
            else if(header.equals("active"))
                csv.add(rule.getActive().toString());
            else if(header.equals("newWorksheetOnly"))
                csv.add(rule.getNewWorksheetOnly().toString());
            else if(header.equals("sir")) {
            	csv.add(rule.getSir() != null ? rule.getSir().toString() : "");
            }
            else if(header.equals("source")) {
            	csv.add(StringUtils.isNotBlank(rule.getSirSource()) ? rule.getSirSource() : "");
            }
            else if(header.equals("title")) {
            	csv.add(StringUtils.isNotBlank(rule.getSirTitle()) ? rule.getSirTitle() : "");
            }
            else if(header.equals("type")) {
            	csv.add(StringUtils.isNotBlank(rule.getSirType()) ? rule.getSirType() : "");
            }
            else if(header.equals("playbook")) {
            	csv.add(StringUtils.isNotBlank(rule.getSirPlaybook()) ? rule.getSirPlaybook() : "");
            }
            else if(header.equals("severity")) {
            	csv.add(StringUtils.isNotBlank(rule.getSirSeverity()) ? rule.getSirSeverity() : "");
            }
            else if(header.equals("owner")) {
            	csv.add(StringUtils.isNotBlank(rule.getSirOwner()) ? rule.getSirOwner() : "");
            }
            else {
                String value = "";
                for(RRRuleFieldVO field:rule.getRidFields()) {
                    if(field.getName().equals(header)) 
                        value = field.getValue();
                }
                csv.add(value);
            }
        }
        return StringUtils.join(csv,",");
    }
    
    public void startRealExport(ExportTaskResponseWriter writer) {
        this.writer = writer;
        this.start();
    }
    /**
     * 
     * @param errorOccured if true, it means the task stop because of an error. so this thread will not terminate immediately
     *                     it will wait a little bit so that the polling can get the error status
     */
    private void cleanUp(boolean errorOccured) {
      //this takes out the thread from the map
        try
        {
            int count = 0;
            while(count++<=5&&errorOccured)
                Thread.sleep(1000);//give the client side a chance to check the final state
            ServiceResolutionRouting.poll(this.getTid(),this.username);
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(), e);
        }
    }
   
    public void run()
    {
        long start = System.currentTimeMillis();
        long current = -1;
        try
        {
            this.headers = this.cursor.appendRuleHeaders();
            this.writer.writeLine(StringUtils.join(headers,","));
        }
        catch (Exception e)
        {
            this.recorder.globalError("Error creating csv header!");
            this.recorder.finish();
            Log.log.error(e.getMessage(), e);
            this.cleanUp(true);
            return;
        }
        try
        {
            String line = "";
            while(this.cursor.hasNext()&&!this.isStoping()) {
                current = System.currentTimeMillis();
                if((current - start)/Locker.MINUTE_IN_MILLIS>Locker.TIMEOUT_IN_MINUTE) {
                    start = current;
                    Locker.updateLock(this.getTid(),this.username, current);
                }
                RRRuleVO rule = null;
                try
                {
                    rule = this.cursor.next();
                }
                catch (ReadRuleException e)
                {
                    this.recorder.exportFailure(e.getId(), "", "", "Error reading mapping from database!");
                    Log.log.error(e);
                    continue;
                }
                try {
                    line = this.ruleToLine(rule);
                }catch(Exception e) {
                    this.recorder.exportFailure(rule.getId(), rule.getRid(), rule.getSchema()!=null?rule.getSchema().getName():"", "Error converting mapping to csv!");
                    Log.log.error(e);
                    continue;
                }
                
                try
                {
//                    Thread.sleep(1000);
                    this.writer.writeLine(line);
                }
                catch (IOException e)
                {
                    this.recorder.exportFailure(rule.getId(), rule.getRid(), rule.getSchema().getName(), "Error writing csv to output!");
                    Log.log.error(e);
                    continue;
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error(e.getMessage(),e);
        }
        this.writer.cleanUp(); 
        this.recorder.finish();
        this.cleanUp(false);
    }
}