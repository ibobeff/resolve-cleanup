/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.tuple.Pair;

import com.resolve.services.ServiceHibernate;
import com.resolve.util.Constants;

public class SequenceUtils
{
    private static Integer initSeq = -1;
    static Map<String, AtomicInteger> numberMap = new ConcurrentHashMap<String, AtomicInteger>();

    // get init sequence for this process
    static
    {
        synchronized (initSeq)
        {
            if (initSeq == -1)
            {
                initSeq = ServiceHibernate.getNextSequence(Constants.PERSISTENCE_SEQ_INIT);
            }
        }
    }

    public static String getNextSequence(String seqName)
    {
        String result = null;

        // get atomic integer
        AtomicInteger number = numberMap.get(seqName);
        if (number == null)
        {
            synchronized (numberMap)
            {
                number = numberMap.get(seqName);
                if (number == null)
                {
                    number = new AtomicInteger(0);
                    numberMap.put(seqName, number);
                }
            }
        }

        // increment number
        int value = number.incrementAndGet();

        // set result
        result = seqName + initSeq + "-" + value;

        return result;
    } // getNextSequence

    public static Pair<String, Integer> getNextXSequenceNumbers(String seqName, int delta)
    {
        Pair<String, Integer> result = null;

        if (delta >= 1)
        {
            // get atomic integer
            AtomicInteger number = numberMap.get(seqName);
            if (number == null)
            {
                synchronized (numberMap)
                {
                    number = numberMap.get(seqName);
                    if (number == null)
                    {
                        number = new AtomicInteger(0);
                        numberMap.put(seqName, number);
                    }
                }
            }
    
            // increment number
            int value = number.addAndGet(delta);
    
            // set result
            result = Pair.of(seqName + initSeq + "-", value - (delta - 1));
        }

        return result;
    } // getNextXSequenceNumbers
} // SequenceUtils
