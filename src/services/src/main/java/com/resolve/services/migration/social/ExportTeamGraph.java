/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.migration.social;

import java.util.List;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.kernel.Traversal;
import org.neo4j.kernel.Uniqueness;

import com.resolve.services.migration.neo4j.GraphDBManager;
import com.resolve.services.migration.neo4j.SocialRelationshipTypes;

public class ExportTeamGraph  extends ExportComponentGraph
{
    private boolean recurse = false;

    public ExportTeamGraph(String sysId, boolean recurse) throws Exception
    {
        super(sysId, SocialRelationshipTypes.TEAM);
        this.recurse = recurse;
    }
    
    @Override
    protected List<GraphRelationshipDTO> exportRelationships(Node compNode) throws Exception
    {
      //get all the nodes that this process node is refering too
        exportTeamsForTeam(compNode);
        
        return relationships;
    }
    
    private void exportTeamsForTeam(Node compNode) throws Exception
    {
        //get all the nodes that this process node is refering too
        Iterable<Node> usersBelongingToThisTeam = findUsersBelongingToTeam(compNode);
        if(usersBelongingToThisTeam != null)
        {
            for(Node anyNode : usersBelongingToThisTeam)
            {
                addRelationship(compNode, anyNode, RelationType.FOLLOWER.name());                
            }//end of for
        }
        
        //find teams that belong to this team
        if(recurse)
        {
            Iterable<Node> teamsBelongingToThisTeam = findTeamsBelongingToTeam(compNode);
            if(teamsBelongingToThisTeam != null)
            {
                for(Node anyNode : teamsBelongingToThisTeam)
                {
                    addRelationship(compNode, anyNode, RelationType.FOLLOWER.name());                    
                    
                    //**RECURSIVE
                    exportTeamsForTeam(anyNode);
                }//end of for loop
            }
        }
    }
    
    private Iterable<Node> findUsersBelongingToTeam(Node teamNode)
    {
        TraversalDescription traversalTeamForMemberTeams =
                        Traversal.description().breadthFirst() 
                        .relationships(RelationType.MEMBER, Direction.INCOMING) 
                        .uniqueness(Uniqueness.NODE_GLOBAL) 
                        .evaluator(Evaluators.atDepth(1))
                        .evaluator( new Evaluator()
                        {
                                public Evaluation evaluate( Path path )
                                {
                                    Node node = path.endNode();
                                    String type = (String) node.getProperty(GraphDBManager.TYPE);
                                    
                                    //if its a USER node, than include it, else ignore
                                    if(type.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
                                    {
                                        return Evaluation.INCLUDE_AND_CONTINUE;
                                    }
                                    else
                                    {
                                        return Evaluation.EXCLUDE_AND_CONTINUE;
                                    }
                                }
                            } );
        
        return traversalTeamForMemberTeams.traverse(teamNode).nodes();
    }
    
    private Iterable<Node> findTeamsBelongingToTeam(Node teamNode)
    {
        TraversalDescription traversalTeamForMemberTeams =
                        Traversal.description().breadthFirst() 
                        .relationships(RelationType.MEMBER, Direction.INCOMING) 
                        .uniqueness(Uniqueness.NODE_GLOBAL) 
                        .evaluator(Evaluators.atDepth(1))
                        .evaluator( new Evaluator()
                        {
                                public Evaluation evaluate( Path path )
                                {
                                    Node node = path.endNode();
                                    String type = (String) node.getProperty(GraphDBManager.TYPE);
                                    
                                    //if its a TEAM node, than include it, else ignore
                                    if(type.equalsIgnoreCase(SocialRelationshipTypes.TEAM.name()))
                                    {
                                        return Evaluation.INCLUDE_AND_CONTINUE;
                                    }
                                    else
                                    {
                                        return Evaluation.EXCLUDE_AND_CONTINUE;
                                    }
                                }
                            } );
        
        return traversalTeamForMemberTeams.traverse(teamNode).nodes();
    }
    
}
