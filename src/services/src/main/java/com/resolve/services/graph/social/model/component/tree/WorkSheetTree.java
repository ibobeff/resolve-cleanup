/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.component.tree;

import java.util.Collection;

import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.Worksheet;

public class WorkSheetTree
{
    private Collection<Worksheet> worksheets;
    private User owner;

    public void setWorkSheets(Collection<Worksheet> worksheets)
    {
        this.worksheets = worksheets;
    }

    public Collection<Worksheet> getWorkSheets()
    {
        return this.worksheets;
    }

    public void setOwner(User user)
    {
        this.owner = user;
    }

    public User getOwner()
    {
        return this.owner;
    }
}
