/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.rb.util;

import java.util.Set;
import java.util.TreeSet;

public class Model
{
    private Start start;
    private End end;
    private Set<Object> tasks = new TreeSet<Object>();
    private Set<Edge> edges = new TreeSet<Edge>();
    
    public Model(Start start, End end, Set<Object> tasks, Set<Edge> edges)
    {
        super();
        this.start = start;
        this.end = end;
        this.tasks = tasks;
        this.edges = edges;
    }
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("<mxGraphModel>\n")
            .append("  <root version=\"5.0\">\n")
            .append("    <mxCell id=\"0\"/>\n")
            .append("    <mxCell id=\"1\" parent=\"0\"/>\n");
        //let's get over with th start and end nodes
        sb.append(start.toString()).append("\n");
        sb.append(end.toString()).append("\n");

        //tasks
        for(Object task : tasks)
        {
            sb.append(task.toString()).append("\n");
        }

        //edges
        for(Edge edge : edges)
        {
            sb.append(edge.toString()).append("\n");
        }
        
        sb.append("  </root>\n");
        sb.append("</mxGraphModel>\n");

        return sb.toString();
    }
}
