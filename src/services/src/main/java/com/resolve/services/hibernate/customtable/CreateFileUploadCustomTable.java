/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.customtable;

import java.util.ArrayList;
import java.util.List;

import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.CustomFormUIType;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.util.constants.HibernateConstantsEnum;
import com.resolve.services.vo.form.RsCustomTableDTO;
import com.resolve.services.vo.form.RsUIField;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;


/**
 * used to create the FileUpload table for form 
 * the creation is based on sql
 * 
 * @author jeet.marwah
 *
 */
public class CreateFileUploadCustomTable
{
    protected String tableName;
    protected String referenceColumnName;
//    private String timezone = null; 
    private String username = null;

    public CreateFileUploadCustomTable(String tableName, String referenceColumnName, String timezone, String username)//, ResolveBaseModel formAccessRights)
    {
        if(StringUtils.isEmpty(tableName))
        {
            throw new RuntimeException("Table Name is mandatory");
        }

        this.tableName = tableName;
        this.referenceColumnName = StringUtils.isNotEmpty(referenceColumnName) ? referenceColumnName.trim() : "U_REF_SYS_ID".toLowerCase();
//        this.timezone = StringUtils.isNotEmpty(timezone) ? timezone : GMTDate.getDefaultTimezone();
        this.username = StringUtils.isNotEmpty(username) ? username : "admin";
    }
    
    public void create() throws Exception
    {
        if(!CustomTableUtil.isTableExist(this.tableName))
        {
            //New implementation
            List<RsUIField> fields = new ArrayList<RsUIField>();
            fields.add(prepareFileNameField(tableName));
            fields.add(prepareSizeField(tableName));
            fields.add(prepareContentField(tableName));
            fields.add(prepareReferenceField(tableName, referenceColumnName));
            
            RsCustomTableDTO customTable = new RsCustomTableDTO();
            customTable.setUName(this.tableName);
            customTable.setUDisplayName(this.tableName);
            customTable.setUType(HibernateConstantsEnum.NORMAL_CUSTOM_TABLE.getTagName());
//            customTable.setUreferenceTableNames(getReferenceTableNames());
//            customTable.setUwikiName(getWikiName());
            customTable.setNewTable(true);
            
            customTable.setFields(fields);
            
            try
            {
                ServiceHibernate.createCustomTable(customTable, username, null);
            }
            catch (Throwable e)
            {
                Log.log.error("Error in creating file upload table " + tableName, e);
                throw new Exception(e.getMessage());
            }
            
        }
    }
    
    private static RsUIField prepareFileNameField(String tableName)
    {
        RsUIField field = new RsUIField();
        field.setDbtable(tableName);
        field.setColumnModelName(HibernateConstants.FU_UFILENAME);
        field.setName(HibernateConstants.FU_UFILENAME);
        field.setDisplayName(HibernateConstants.FU_UFILENAME);
        field.setDbcolumn(HibernateConstants.FU_UFILENAME);
        field.setUiType(CustomFormUIType.TextField.name());
//        field.setRstype(CustomFormUIType.TextField.name());
        field.setDbtype(HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        
        field.setEncrypted(false);
        field.setMandatory(false);
        field.setHidden(false);
        field.setReadOnly(false);
        field.setOrderNumber(1);
        field.setWidth(0);
        field.setHeight(22);
        field.setLabelAlign("left");
        
        field.setStringMinLength(0);
        field.setStringMaxLength(333);
        field.setUiStringMaxLength(333);
        
        field.setViewRoles("admin");
        field.setEditRoles("admin");
        field.setAdminRoles("admin");

        return field;
    }
    
    private static RsUIField prepareSizeField(String tableName)
    {
        RsUIField field = new RsUIField();
        field.setDbtable(tableName);
        field.setColumnModelName(HibernateConstants.FU_USIZE);
        field.setName(HibernateConstants.FU_USIZE);
        field.setDisplayName(HibernateConstants.FU_USIZE);
        field.setDbcolumn(HibernateConstants.FU_USIZE);
        field.setUiType(CustomFormUIType.NumberTextField.name());
//        field.setRstype(CustomFormUIType.NumberTextField.name());
        field.setDbtype(HibernateConstantsEnum.CUSTOMTABLE_TYPE_LONG.getTagName());
        
        field.setEncrypted(false);
        field.setMandatory(false);
        field.setHidden(false);
        field.setReadOnly(false);
        field.setOrderNumber(2);
        field.setWidth(0);
        field.setHeight(22);
        field.setLabelAlign("left");
        
        field.setViewRoles("admin");
        field.setEditRoles("admin");
        field.setAdminRoles("admin");

        return field;
    }
    
    private static RsUIField prepareContentField(String tableName)
    {
        RsUIField field = new RsUIField();
        field.setDbtable(tableName);
        field.setColumnModelName(HibernateConstants.FU_UCONTENT);
        field.setName(HibernateConstants.FU_UCONTENT);
        field.setDisplayName(HibernateConstants.FU_UCONTENT);
        field.setDbcolumn(HibernateConstants.FU_UCONTENT);
        field.setUiType(CustomFormUIType.TextArea.name());
//        field.setRstype(CustomFormUIType.TextArea.name());
        field.setDbtype(HibernateConstantsEnum.CUSTOMTABLE_TYPE_BLOB.getTagName());
        
        field.setEncrypted(false);
        field.setMandatory(false);
        field.setHidden(false);
        field.setReadOnly(false);
        field.setOrderNumber(3);
        field.setWidth(0);
        field.setHeight(22);
        field.setLabelAlign("left");
        
//        field.setStringMinLength(0);
//        field.setStringMaxLength(4001);
//        field.setUiStringMaxLength(4001);
        
        field.setViewRoles("admin");
        field.setEditRoles("admin");
        field.setAdminRoles("admin");

        return field;
    }
    

    private static RsUIField prepareReferenceField(String tableName, String refColumnName)
    {
        RsUIField field = new RsUIField();
        field.setDbtable(tableName);
        field.setColumnModelName(refColumnName);
        field.setName(refColumnName);
        field.setDisplayName(refColumnName);
        field.setDbcolumn(refColumnName);
        field.setUiType(CustomFormUIType.TextField.name());
//        field.setRstype(CustomFormUIType.TextField.name());
        field.setDbtype(HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName());
        
        field.setEncrypted(false);
        field.setMandatory(false);
        field.setHidden(false);
        field.setReadOnly(false);
        field.setOrderNumber(4);
        field.setWidth(0);
        field.setHeight(22);
        field.setLabelAlign("left");
        
        field.setStringMinLength(0);
        field.setStringMaxLength(40);
        field.setUiStringMaxLength(40);
        
        field.setViewRoles("admin");
        field.setEditRoles("admin");
        field.setAdminRoles("admin");

        return field;
    }
       
}
