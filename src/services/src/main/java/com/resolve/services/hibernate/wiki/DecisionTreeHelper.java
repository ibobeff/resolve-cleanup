/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.hibernate.query.Query;
import org.xml.sax.SAXException;

import com.resolve.dto.SectionModel;
import com.resolve.dto.SectionType;
import com.resolve.persistence.dao.WikiDocumentDAO;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceWorksheet;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.constants.WikiDocStatsCounterName;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.util.StoreUtility;
import com.resolve.services.util.WikiUtils;
import com.resolve.services.util.GenericSectionUtil;
import com.resolve.services.util.UserUtils;
import com.resolve.util.Constants;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.vo.DTMetricLogList;
import com.resolve.vo.DTMetricLogVO;

public class DecisionTreeHelper
{
	@SuppressWarnings({ "unchecked", "rawtypes" })
    public static List<WikiDocument> getDocumentsInDecisionTree(String rootDocument)
    {        
        List<WikiDocument> res = new ArrayList<>();
		try
        {
           res =  (List<WikiDocument>) HibernateProxy.execute(() -> {
            	List<WikiDocument> result = new ArrayList<WikiDocument>();
            	
            	 StringBuffer sql = new StringBuffer("select wd1 from WikiDocument wd1, DTWikidocWikidocRel rel, WikiDocument wd2 ");
                 sql.append("where lower(wd1.UFullname) = lower(rel.UWikidocFullname) ");
                 sql.append("and rel.dtWikidoc = wd2.sys_id ");
                 sql.append("and lower(wd2.UFullname) = :UFullname");
                 
                 Query query = HibernateUtil.createQuery(sql.toString());
                 query.setParameter("UFullname", rootDocument.toLowerCase());
                 result = query.list();
                 if(Log.log.isTraceEnabled())
                 {
                     Log.log.trace("Found " + result.size() + " Documents in " + rootDocument + " Decision Tree");
                 }
                 
                 WikiDocument example = new WikiDocument();
                 example.setUFullname(rootDocument);
                     
                 WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
                 WikiDocument rootDoc = dao.findFirst(example, "pbActivities");
                 result.add(rootDoc);
                 
                 return result;
            });
        }
        catch(Throwable t)
        {
            Log.log.error("Unable to Search for Documents in Decision Tree", t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
        
        return res;
        
    } //getDocumentsInDecisionTree
    
    private static Map<String, String> getDocumentSectionsInDecisionTree(String rootDoc)
    {
        Map<String,String> result = new TreeMap<String,String>();
        List<WikiDocument> docsInTree = getDocumentsInDecisionTree(rootDoc);
        
        String regex = "(?msi)(\\{section:type=" + SectionType.SOURCE + "\\|title=dt_" + rootDoc + "_(\\d+).*?\\{section\\}\n?)";
        Pattern pattern = Pattern.compile(regex); 
        for (WikiDocument doc : docsInTree)
        {
            String content = doc.getUContent();
            if (StringUtils.isNotEmpty(content))
            {
                Matcher matcher = pattern.matcher(content);
                while (matcher.find())
                {
                    String node = matcher.group(2);
                    String key = doc.getUFullname() + "_" + node;
                    result.put(key, matcher.group(1));
                    Log.log.trace("Found Section " + key);
                }
            }
        }
        if(Log.log.isTraceEnabled())
        {
            Log.log.trace("Found " + result.size() + " sections for " + rootDoc);
        }
        return result;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static void removeDecisionTrees(String rootDocumentIds)
    {
        StringBuffer sql = new StringBuffer("select  wd.UFullname") 
            .append(" from WikiDocument as wd ") 
            .append(" where wd.sys_id in (")
            .append(rootDocumentIds + ")")
            .append(" and wd.UIsRoot = true");
        try
        {
        	HibernateProxy.execute(() -> {
        		 Query query = HibernateUtil.createQuery(sql.toString());
                 List<Object> page = query.list();
                 for (Object obj : page)
                 {
                     String fullName = (String) obj;
                     removeDecisionTree(fullName, UserUtils.SYSTEM);
                 }
        	});           
        }
        catch (Throwable t)
        {
            Log.log.error("Failed to pull Fullname of doc to delete", t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
    }//removeDecisionTrees
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private static void removeDecisionTree(String rootDocument, String user)
    {
        Log.log.trace("Remove Decision Tree Sections for " + rootDocument);
        try
        {
            HibernateProxy.execute(() -> {
            	String sql = "select wd from WikiDocument wd where wd.UFullname = :UFullname";
                
                Query query = HibernateUtil.createQuery(sql);
                query.setParameter("UFullname", rootDocument);
                List<Object> sysIDs = query.list();
                String sysID = null;
                WikiDocument doc = null;
                
                for (Object obj : sysIDs)
                {
                    doc = (WikiDocument) obj;
                    sysID = doc.getSys_id();
                }
                if (StringUtils.isNotEmpty(sysID))
                {
                    List<WikiDocument> dtDocuments = getDocumentsInDecisionTree(doc.getUFullname());
                    if (dtDocuments.size() > 0)
                    {
                        removeDecisionTreeSections(dtDocuments, rootDocument, user);
                    }
                }
                else
                {
                    Log.log.error("No Root Document " + rootDocument + " Found");
                }
                //HibernateUtil.deleteQuery("DTWikidocWikidocRel", "dtWikidoc = '" + sysID + "'");
                HibernateUtil.deleteQuery("DTWikidocWikidocRel", "dtWikidoc", sysID);
            });
        }
        catch(Throwable t)
        {
            Log.log.error("Unable to Remove All Documents in Decision Tree", t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
    } //removeDecisionTree
    
    private static void removeDecisionTreeSections(List<WikiDocument> documents, String rootDocument, String user)
    {

        try
        {

        	HibernateProxy.execute(() -> {
        		   WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
                   
                   String regex = "(?ms)(\\{section:type=" + SectionType.SOURCE + "\\|title=dt_" + rootDocument + "_.*?\\{section\\}\n?)";
                   Pattern pattern = Pattern.compile(regex); 
                   
                   for (WikiDocument document : documents)
                   {
                       String editRights = document.getAccessRights() != null ? document.getAccessRights().getUWriteAccess() : "";
                       if (UserUtils.hasRole(user, editRights))
                       {
                           if(Log.log.isTraceEnabled())
                           {
                               Log.log.trace("Removing {section} with title like dt_" + rootDocument + " from " + document.getUFullname());
                           }
                           //matcher to remove section
                           
                           //matcher to remove section
                           String content = document.getUContent();
                           if (StringUtils.isNotEmpty(content))
                           {
                               Matcher matcher = pattern.matcher(content);
                               String tmpContent = "";
                               int last = 0;
                               while (matcher.find())
                               {
                                   Log.log.trace("Removing Section from " + document.getUFullname() + ":\n" + matcher.group(1));
                                   tmpContent = tmpContent + content.substring(last, matcher.start());
                                   last = matcher.end();
                               }
                               content = tmpContent + content.substring(last);
                               Log.log.trace("Final Document Content: " + content.trim());
                               document.setUContent(content.trim());
                               dao.persist(document);
                           }
                           
                       }
                       else
                       {
                           Log.log.error("Missing Edit Rights for " + document.getUFullname() + ", Cannot Clear Sections");
                       }
                   }
        	});
        }
        catch(Throwable t)
        {
            Log.log.error("Unable to Remove All Documents in Decision Tree", t);
                      HibernateUtil.rethrowNestedTransaction(t);
        }
    } //removeDecisionTreeSections
    
    public static String removeDtSectionsInContent(String content, String rootDocument)
    {
        String result = content;
        
        if(StringUtils.isNotBlank(content))
        {
            try
            {
                String regex = "(?ms)(\\{section:type=(?i)(source)\\|title=dt_";
                if (StringUtils.isNotEmpty(rootDocument))
                {
                    regex += rootDocument + "_";
                }
                regex += ".*?\\{section\\}\n?)";
                    
                rootDocument = rootDocument != null ? rootDocument : "";
                if(Log.log.isTraceEnabled())
                {
                    Log.log.trace("Removing {section}s with title like dt_" + rootDocument + " from:\n" + content);
                }
                //matcher to remove section
                Pattern pattern = Pattern.compile(regex); 
                Matcher matcher = pattern.matcher(content);
                String tmpContent = "";
                int last = 0;
                while (matcher.find())
                {
                    Log.log.trace("Removing Section:\n" + matcher.group(1));
                    tmpContent = tmpContent + content.substring(last, matcher.start());
                    last = matcher.end();
                }
                result = tmpContent + content.substring(last);
                result = result.trim();
                Log.log.trace("Final Document Content: " + result);
            }
            catch (Exception e)
            {
                Log.log.error("Failed to Remove " + rootDocument + " DT Sections from content", e);
            }
        }
        
        return result;
    } // removeDtSectionsInContent

    // Graph used to detect loop in DT 
    private static final class Graph {
        
        final int vertexNum;
        private final List<List<Integer>> vertexChildren;

        boolean[] visited;
        boolean[] recStack;
        int stopNode = -1;
        
        public Graph(int V) 
        {
            this.vertexNum = V;
            vertexChildren = new ArrayList<>(V);
             
            for (int i = 0; i < V; i++)
                vertexChildren.add(new LinkedList<>());
            
            visited = new boolean[V];
            recStack = new boolean[V];
        }
         
        private boolean isCyclicUtil(int i, boolean[] visited, boolean[] recStack) 
        {
            if (recStack[i])
            {
            	stopNode = i;
                return true;
            }
     
            if (visited[i])
                return false;
                 
            visited[i] = true;
     
            recStack[i] = true;
            List<Integer> children = vertexChildren.get(i);
             
            for (Integer c: children)
                if (isCyclicUtil(c, visited, recStack))
                    return true;
                     
            recStack[i] = false;
     
            return false;
        }
     
        private void addEdge(int source, int dest) {
            vertexChildren.get(source).add(dest);
        }
     
        private boolean isCyclic() 
        {
            for (int i = 0; i < vertexNum; i++)
            {
                if (isCyclicUtil(i, visited, recStack))
                {	
                    return true;
                }
             }
            return false;
        }
    }
        
    @SuppressWarnings("unchecked")
    static Optional<String> hasLoopInDT(Document document, Map<String, String> dtModel)
    {
    	Optional<String> result = Optional.empty();
        List<Node> list = document.selectNodes("/mxGraphModel/root/Edge/mxCell");
        Map<String, Integer> id2GraphNode = new HashMap<>();
        Map<Integer, String> graphNode2Id = new HashMap<>();
        int nodeCount = 0;
        for (Node edge : list)
        {
            String srcId = edge.valueOf("@source");
            if (id2GraphNode.get(srcId)==null)
            {
            	id2GraphNode.put(srcId, nodeCount);
            	graphNode2Id.put(nodeCount, srcId);
            	nodeCount++;
            }
            
            String dstId = edge.valueOf("@target");
            if (id2GraphNode.get(dstId)==null)
            {
            	id2GraphNode.put(dstId, nodeCount);
            	graphNode2Id.put(nodeCount, dstId);
            	nodeCount++;
            }
        }

        Graph graph = new Graph(nodeCount);
        
        for (Node edge : list)
        {
            String srcId = edge.valueOf("@source");
            String dstId = edge.valueOf("@target");
            graph.addEdge(id2GraphNode.get(srcId), id2GraphNode.get(dstId));
        }

        if (graph.isCyclic())
        {
        	int i = graph.stopNode;
        	StringBuilder errMsg = new StringBuilder("Decision tree is invalid. Found a loop in decision tree model: ");
        	for (int j = i; j < graph.vertexNum; ++j)
        	{
        		if (graph.recStack[j])
        		{
        			errMsg.append(String.format(" Node (%s) ==> ", dtModel.get(graphNode2Id.get(j))));
        		}
        	}

        	errMsg.append(String.format(" Node (%s) ", dtModel.get(graphNode2Id.get(i))));
        	result = Optional.of(errMsg.toString());
        }
        
    	return result;
    }
    
    /**
     * applying decision tree to list of documents using sysIds. Used mainly for the Import/Export
     *  
     * @param sysIds
     * @param user
     */
    public static void applyDecisionTreeXML(Set<String> sysIds, String user)
    {
        if(sysIds != null && sysIds.size() > 0)
        {
            for(String sysId : sysIds)
            {
                try
                {
                    WikiDocument doc = StoreUtility.findById(sysId);
                    if(doc != null)
                    {
                        applyDecisionTreeXML(doc, user);
                    }
                }
                catch(Throwable t)
                {
                    Log.log.error("Error applying decision tree to :" + sysId, t);
                }
            }
        }
    }
    
    /**
     * This method will add/remove/edit sections from wikidocs
     * based on the saved decision tree model
     * 
     * @param wikiDoc
     * @param user
     * @return
     * @throws Exception 
     */
    @SuppressWarnings("unchecked")
    public static void applyDecisionTreeXML(WikiDocument wikiDoc, String user) throws Exception
    {
        String docName = wikiDoc.getUFullname();
        String xml = wikiDoc.getUDecisionTree();
        int answerCount=0;
        
        if(Log.log.isTraceEnabled())
        {
            Log.log.trace("Applying Decision Tree for " + docName);
            Log.log.trace("Attempting to Apply " + docName + " XML:\n" + xml);
        }
        try
        {
            if (validateDTXML(docName, xml, user))
            {
                SAXReader reader = new SAXReader();
                reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
                reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
                reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
                Document document = reader.read(new ByteArrayInputStream(xml.getBytes()));
                //Document document = DocumentHelper.parseText(xml);
                
                Map<String, String> dtModel = new HashMap<String, String>(); //model of decision tree
                Map<String, String> dtRunbookMap = new HashMap<String, String>();//map documents to node(s) in dtModel
                
                //Get Root Node
                Log.log.trace("Get Root Node");
                Node root = document.selectSingleNode("/mxGraphModel/root/Start");
                
                // set result start node
                String rootId = root.valueOf("@id");
                dtModel.put(rootId, "Root::Root");
                dtRunbookMap.put(docName, rootId);
                Log.log.trace("Root Document " + docName + " id " + rootId);
                
                List<String> runbookNameList = new ArrayList<String>();
                
                //get all Documents in model
                Log.log.trace("Get Document List");
                List<Node> list = document.selectNodes("/mxGraphModel/root/Document");
                
                for (Node runbook : list)
                {
                    String id = runbook.valueOf("@id").trim();
                    String wiki = runbook.valueOf("@description");
                    String label = runbook.valueOf("@label");

                    if (StringUtils.isEmpty(wiki))
                    {
                        wiki = label;
                    }
                    Log.log.trace("Document " + wiki + " with id " + id);
                    
                    // remove carriage returns
                    wiki = wiki.replaceAll("\\n", " ");
                    wiki = wiki.replaceAll("\\r", " ");
                    wiki = wiki.trim();
                    
                    wiki = com.resolve.services.hibernate.util.WikiUtils.getCorrectCaseWikiName(wiki, user);
                    
                    if(wiki != null && !wiki.equals(""))
                    {
                        if (!runbookNameList.contains(wiki))
                        {
                            runbookNameList.add(wiki);
                        }
                    }
                    String tmpId = dtRunbookMap.get(wiki);
                    if (tmpId != null)
                    {
                        tmpId += "," + id;
                    }
                    else
                    {
                        tmpId = id;
                    }
                    dtRunbookMap.put(wiki, tmpId + "::" + label);
                }
                //Remove all Documents from model that don't exist
                List<String> notFoundRunbook = WikiUtils.runbooksNotFound(runbookNameList);
                /*
                 *  TODO find out why the notFoundRunbook gettting cleared before 
                 *  processing the list of runbooks not found
                 */
                notFoundRunbook.clear();
                Log.log.warn("Runbooks Not Found: " + notFoundRunbook.size());
                for (String runbook : notFoundRunbook)
                {
                    if(Log.log.isTraceEnabled())
                    {
                        Log.log.trace("Document Not Found for Decision Tree: " + runbook);
                    }
//                    runbookNameList.remove(runbook);
//                    dtRunbookMap.remove(runbook);
                }
                //Remove all Documents from model that can't be edited
                List<String> unableToEdit = WikiUtils.runbooksEditNotAvailable(runbookNameList, user);
                Log.log.warn("Runbooks Missing Edit Permissions: " + unableToEdit.size());
                for (String runbook : unableToEdit)
                {
                    if(Log.log.isTraceEnabled())
                    {
                        Log.log.trace("Unable to Edit Document for Decision Tree: " + runbook);
                    }
                    runbookNameList.remove(runbook);
                    dtRunbookMap.remove(runbook);
                }
                //If no valid documents remain
                if (runbookNameList.size() == 0)
                {
                    Log.log.error("DT Model Does Not Have Valid/Editable Documents");
                }
                //for all valid/editable documents, add them to the dt model
                for (String runbook : runbookNameList)
                {
                    String[] nodeIdSet = dtRunbookMap.get(runbook).split(",");
                    for (int i=0; i<nodeIdSet.length; i++)
                    {
                        String nodeId = nodeIdSet[i];
                        String label = "";
                        int splitIndex = nodeId.indexOf("::");
                        if (splitIndex > -1)
                        {
                            nodeId = nodeIdSet[i].substring(0, splitIndex);
                            label = nodeIdSet[i].substring(splitIndex + 2);
                        }
                        dtModel.put(nodeId, "Document::" + runbook + "::" + label);
                        
                        //TODO: create a new list with node ids which is deleted one by one when we process it for the {section} code
                        //whatever is remaining will be treated as LEAFs of this DT
                        
                        Log.log.trace("Added to Model " + nodeId + " -> " + dtModel.get(nodeId));
                    }
                }
                
                //get all Questions into model
                list = document.selectNodes("/mxGraphModel/root/Question");
                Log.log.trace("Questions Found: " + list.size());
                
                for (Node question : list)
                {
                    String id = question.valueOf("@id");
                    
                    
                    // Validate whether a question has 2 or more same answers. If yes, error out. 
                    List<Node> tmpNodes = document.selectNodes("/mxGraphModel/root/Edge/mxCell[@source='" + id + "']");
                    Set<String> answerSet = new HashSet<String>();
                    for (Node tmpNode : tmpNodes)
                    {
                    	String answer = tmpNode.getParent().valueOf("@label");
                    	if (StringUtils.isNotBlank(answer))
                    	{
                    		if (!answerSet.add(answer.toLowerCase()))
                    		{
                    			throw new Exception(" Question '" + question.valueOf("@label") + "' has 2 or more same answers, '" + answer + "', leading to an ambiguous situation.");
                    		}
                    	}
                    }
                    // End of validation.
                    
                    String selectType = question.valueOf("@select");
                    if (StringUtils.isEmpty(selectType))
                    {
                        selectType = "DEFAULT";
                    }
                    else
                    {
                        selectType = selectType.trim();
                        if (!selectType.equalsIgnoreCase("RADIO")
                                && !selectType.equalsIgnoreCase("DROPDOWN"))
                        {
                            selectType = "DEFAULT";
                        }
                    }
                    String questionStr = question.valueOf("@label");
                    if (questionStr == null)
                    {
                        questionStr = "";
                    }
                    else if (questionStr.endsWith(":"))
                    {
                        questionStr += " ";
                    }
                    String recommendationStr = question.valueOf("@recommendation");
                    if (recommendationStr == null)
                    {
                        recommendationStr = "";
                    }
                    else if (recommendationStr.endsWith(":"))
                    {
                        recommendationStr += " ";
                    }
                    dtModel.put(id, "Question::" + questionStr + "::Recommendation::" + recommendationStr + "::" + selectType);
                    Log.log.trace("Added to Model " + id + " -> " + dtModel.get(id));
                }
                
                //get all Action Tasks into model
                list = document.selectNodes("/mxGraphModel/root/Task");
                Log.log.trace("# of Action Tasks Found: " + list.size());
                
                for (Node question : list)
                {
                    String id = question.valueOf("@id"); 
                    String taskName = question.valueOf("@description");
                    List<Node> paramNode = document.selectNodes("/mxGraphModel/root/Task[@id = " + id + "]/params/inputs");
                    String params = "{}";
                    if(paramNode.size() > 0) {
                        params = paramNode.get(0).getText();
                        params = params.trim().replaceAll("\n ", "").replace("$", "");
                    }
                    if (StringUtils.isBlank(taskName))
                    {
                        taskName = question.valueOf("@label");
                        
                        if (StringUtils.isBlank(taskName))
                        {
                            taskName = "";
                        }
                    }
                    else if (taskName.endsWith("?"))
                    {
                        taskName = taskName.substring(0, taskName.length()-1);
                    }
                    dtModel.put(id, "Task::" + taskName + "::" + taskName + "::" + params);
                    Log.log.trace("Added to Model " + id + " -> " + dtModel.get(id));
                }
                
                // get all Runbooks into model
                list = document.selectNodes("/mxGraphModel/root/Subprocess");
                Log.log.trace("# of Action Runbooks Found: " + list.size());
                
                for (Node runBookNode : list)
                {
                    String id = runBookNode.valueOf("@id");                                        
                    String runBookName = runBookNode.valueOf("@description");
                    List<Node> paramNode = document.selectNodes("/mxGraphModel/root/Subprocess[@id = " + id + "]/params/inputs");
                    String params = "{}";
                    if(paramNode.size() > 0) {
                        params = paramNode.get(0).getText();
                        params = params.trim().replaceAll("\n $", "").replace("$", "");;
                    }
                    if (StringUtils.isBlank(runBookName))
                    {
                        runBookName = runBookNode.valueOf("@label");
                        
                        if (StringUtils.isBlank(runBookName))
                        {
                            runBookName = "";
                        }
                    }
                    else if (runBookName.endsWith("?"))
                    {
                        runBookName = runBookName.substring(0, runBookName.length()-1);
                    }
                    dtModel.put(id, "Runbook::" + runBookName + "::" + runBookName + "::" + params);
                    Log.log.trace("Added to Model " + id + " -> " + dtModel.get(id));
                }
                
                //get all connectors/answers
                list = document.selectNodes("/mxGraphModel/root/Edge/mxCell");
                //model of all connectors
                //keys: id of origin node
                //value list: destination type::destination node id::answer text
                Map<String, List<String>> connectors = new HashMap<String, List<String>>();
                //List of all question ids that have answers into other questions instead of Documents
                //List<String> questionToQuestionList = new ArrayList<String>();
                Map<String, String> expressions = new HashMap<String, String>();
                List<String> executionList = new ArrayList<String>();
                for (Node edge : list)
                {
                    String srcId = edge.valueOf("@source");
                    String dstId = edge.valueOf("@target");
                    
                    if (dtModel.get(srcId) != null && dtModel.get(dstId) != null)
                    {
                        String srcType = dtModel.get(srcId).split("::")[0];
                        String dstType = dtModel.get(dstId).split("::")[0];
                        //if connector source is a Document
                        if ("Root".equalsIgnoreCase(srcType) || "Document".equalsIgnoreCase(srcType))
                        {
                            //ignore Document to Document connectors, they are not valid
                            if (!"Question".equalsIgnoreCase(dstType))
                            {
                                //String dstLabel = dtModel.get(dstId).split("::")[1];
                                //Log.log.warn(srcType + "s Can only Connect to a Question (" + dstLabel + ")");
                                Log.log.trace("Edge : " + dtModel.get(srcId) + "->" + dtModel.get(dstId));
                            }
                            else
                            {
                            	//add connector to list with srcId as origin
                                List<String> connector = connectors.get(srcId);
                                if (connector == null)
                                {
                                    connector = new ArrayList<String>();
                                    connectors.put(srcId, connector);
                                }
                                String orderStr = getOrderValue(edge.getParent().valueOf("@order"));
                                Log.log.trace("Add Document Connector->" + dstType + "::" + dstId + "::" + edge.getParent().valueOf("@label"));
                                connector.add(dstType + "::" + dstId + "::" + edge.getParent().valueOf("@label") + "::" + orderStr);
                            }
                        }
                        else if ("Question".equalsIgnoreCase(srcType))
                        {
                            // Get Edge expressions if specified
                            
                            String expressionTxt = edge.getParent().valueOf("./expressions");
                            
                            if (StringUtils.isNotBlank(expressionTxt.trim()))
                            {
                                Log.log.trace("Expression Text : [" + expressionTxt.trim() + "]");
                                expressions.put(srcId + "->" + dstId, expressionTxt.trim());
                            }
                                                
                        	if ("Task".equalsIgnoreCase(dstType) || "Runbook".equalsIgnoreCase(dstType))
                        	{
                        		
                        		//listTasks(dstId, srcId, taskList, dtModel, list, connectors);
                        		String localDstId = dstId;
                        		while(true)
                        		{
                        			boolean breakFromInnerFor = false;
	                        		for (Node node : list)
	                        		{
	                        			String tmpSrcId = node.valueOf("@source");
	                                    String tmpDstId = node.valueOf("@target");
	                                    
	                                    if (tmpSrcId.equals(localDstId))
	                                    {
	                                    	executionList.add("{\"name\":\"" + dtModel.get(localDstId).split("::")[1] + "\",\"params\":" + dtModel.get(localDstId).split("::")[3] + "}");
	                                    	if (dtModel.get(tmpSrcId) != null && dtModel.get(tmpDstId) != null)
	                                        {
	                                            //String tmpSrcType = dtModel.get(tmpSrcId).split("::")[0];
	                                            String tmpDstType = dtModel.get(tmpDstId).split("::")[0];
	                                            
	                                            if ("Task".equalsIgnoreCase(tmpDstType) || "Runbook".equalsIgnoreCase(tmpDstType))
	                                            {
	                                            	localDstId = tmpDstId;
	                                            	breakFromInnerFor = true;
	                                            	break;
	                                            }
	                                            //add connector to list with srcId as origin
	        	                                List<String> connector = connectors.get(srcId);
	        	                                if (connector == null)
	        	                                {
	        	                                    connector = new ArrayList<String>();
	        	                                    connectors.put(srcId, connector);
	        	                                }
	        	                                String orderStr = getOrderValue(node.getParent().valueOf("@order"));
	        	                                Log.log.trace("Adding " + tmpDstType + " Connector->" + tmpDstType + "::" + tmpDstId + "::" + node.getParent().valueOf("@label"));
	        	                                connector.add(tmpDstType + "::" + tmpDstId + "::" + edge.getParent().valueOf("@label") + "::" + orderStr + "::" + executionList);
	        	                                
	        	                                if (expressions.containsKey(srcId + "->" + dstId) &&
	        	                                    !expressions.containsKey(srcId + "->" + tmpDstId))
	        	                                {
	        	                                    expressions.put(srcId + "->" + tmpDstId, expressions.get(srcId + "->" + dstId));
	        	                                }
	        	                                
	        	                                executionList.clear();
	                                        }
	                                    }
	                        		}
	                        		if (breakFromInnerFor == true)
	                        		{
	                        			continue;
	                        		}
	                        		else
	                        		{
	                        			// come out of while(true)
	                        			break;
	                        		}
                        		}
                        	}
                        	else
                        	{
	                            //note down question into question connectors
	                            //add connector to list with srcId as origin
	                            List<String> connector = connectors.get(srcId);
	                            if (connector == null)
	                            {
	                                connector = new ArrayList<String>();
	                                connectors.put(srcId, connector);
	                            }
	                            String orderStr = getOrderValue(edge.getParent().valueOf("@order"));
	                            Log.log.trace("Adding " + dstType + " Connector->" + dstType + "::" + dstId + "::" + edge.getParent().valueOf("@label"));
	                            connector.add(dstType + "::" + dstId + "::" + edge.getParent().valueOf("@label") + "::" + orderStr);
                        	}
                        }
                    }
                }
                
                Optional<String> loopError = hasLoopInDT(document, dtModel);
                if (loopError.isPresent())
                {
                	throw new Exception(loopError.get());
                }
                
                //Wiki Page for Question -> Question mappings
                String decisionHolderName = docName + "_Decision";
                StringBuilder decisionHolder = new StringBuilder();
                
                Map<String,String> decisionSections = new TreeMap<String,String>();
                Set<String> sectionTitleSet = new HashSet<String>();
                
                boolean sectionStart = false;
                boolean ifTag = false;
                
                //ArrayList<String> alreadyProcessedLinkId = new ArrayList<String>();
                Map<String, Set<String>> alreadyProcessedLinkIdsByNodeId = new HashMap<String, Set<String>>();
                
                for (String nodeId : connectors.keySet())
                {
                    alreadyProcessedLinkIdsByNodeId.put(nodeId, new HashSet<String>());
                    
                    Set<String> alreadyProcessedLinkId = alreadyProcessedLinkIdsByNodeId.get(nodeId);
                    
                    Log.log.trace("Processing nodeId " + nodeId + " with model value " + dtModel.get(nodeId));
                    
                    if (dtModel.get(nodeId).indexOf("::") != -1)
                    {
                        String[] node = dtModel.get(nodeId).split("::");
                        String dtType = node[0];
                        String nodeDocName = "";
                        StringBuilder dt = new StringBuilder();
                        
                        if (dtType.equalsIgnoreCase("Task") || dtType.equalsIgnoreCase("Runbook"))
                        {
                        	continue;
                        }
                        
                        if (dtType.equalsIgnoreCase("Question"))
                        {
                            nodeDocName = decisionHolderName;
                        }
                        else if (dtType.equalsIgnoreCase("Root"))
                        {
                            nodeDocName = docName;
                        }
                        else
                        {
                            nodeDocName = node[1];
                        }
                        
                        //Get a list of all questions branching off of this Node
                        List<String> questionList = connectors.get(nodeId);
                        Log.log.trace("There are " + questionList.size() + " connectors off of this node");
  
                        if (questionList != null)
                        {
                            ArrayList<String> linksToProcess = new ArrayList<String>();
                          
                            for (String link : questionList)
                            {
                                String[] linkValues = link.split("::");
                                //String linkType = linkValues[0];
                                String linkId = linkValues[1];
                                linksToProcess.add(linkId);
                                
                            }
                            
                            linksToProcess.removeAll(alreadyProcessedLinkId);
                            
                            if(linksToProcess.size() > 0) 
                            {
                                if (sectionTitleSet.contains("dt_" + docName + "_" + nodeId))
                                {
                                    continue;
                                }
                                else
                                {
                                    sectionTitleSet.add("dt_" + docName + "_" + nodeId);
                                }
                                 
                                 //Start Section and Velocity #if Statements
                                 sectionStart = true;
                                 dt.append("{section:" + SectionModel.TYPE + "=" + SectionType.SOURCE.getTagName() + "|");
                                 dt.append(SectionModel.TITLE + "=dt_" + docName + "_" + nodeId);
                                 dt.append("|" + SectionModel.ID + "=" + GenericSectionUtil.createSectionId());
                                 dt.append("}\n");
//                                 dt.append("|" + SectionModel.HEIGHT + "=" + SectionModel.DEFAULT_IFRAME_HEIGHT + "}\n");
                                 dt.append("##GENERATED CODE: DO NOT MODIFY\n");
                                 if (dtType.equalsIgnoreCase("Document"))
                                 {
                                     ifTag = true;
                                     dt.append("#if ($!wikiContext.getAttribute(\"ROOT\") == \"" + docName + "\""); 
                                     dt.append(" && $!wikiContext.getAttribute(\"NODE_ID\") == \"" + nodeId + "\")\n");
                                 }
                                 else if (dtType.equalsIgnoreCase("Task") || dtType.equalsIgnoreCase("Runbook"))
                                 {
//                                	 dt.append("#if ($!wikiContext.getAttribute(\"ROOT\") == \"" + docName + "\""); 
//                                    dt.append(" && $!wikiContext.getAttribute(\"NODE_ID\") == \"" + nodeId + "\")\n");                                	 
                                 }
                                 
                                 answerCount = 0;
                                 
                                 for (String link : questionList)
                                 {
                                     //Question formatted as Question::NodeID
                                     String[] linkValues = link.split("::");
                                     String linkType = linkValues[0];
                                     String linkId = linkValues[1];
                                     StringBuilder dtTmp = new StringBuilder();
                                     
                                     if(!linksToProcess.contains(linkId)) {  //if linkId is already processed skip it.
                                         continue;
                                     }
                                     Log.log.trace("Processing link " + link);
                                     
                                     if (dtType.equalsIgnoreCase("Question") || dtType.equalsIgnoreCase("Task") ||
                                         dtType.equalsIgnoreCase("Runbook"))
                                     {
                                         ifTag = true;
                                         dtTmp.append("#if ($!wikiContext.getAttribute(\"ROOT\") == \"" + docName + "\""); 
                                         dtTmp.append(" && $!wikiContext.getAttribute(\"NODE_ID\") == \"" + linkId + "\")\n");
                                         alreadyProcessedLinkId.add(linkId);
                                     }
                                     
                                     //Only Add Decision Tags for connectors into questions
                                     if (linkType.equalsIgnoreCase("Question"))
                                     {
                                         answerCount++;
                                         String question = "Question";
                                         String recommendationText = "";
                                         String dtQuestion = dtModel.get(linkId);
                                         String selectType = "DEFAULT";
                                         if (dtQuestion != null)
                                         {
                                             String[] dtQuestionValues = dtQuestion.split("::");
                                             if (dtQuestionValues.length == 5)
                                             {
                                                 question = dtQuestionValues[1];
                                                 recommendationText = dtQuestionValues[3];
                                                 selectType = dtQuestionValues[4];
                                             }
                                         }
                                         question = question.replaceAll("\\s+", " ").trim(); 
                                         question = StringEscapeUtils.escapeHtml(question);
                                         //start dt append with {decision} dialog using question
                                         dtTmp.append("{decision:");
                                         if ("DROPDOWN".equalsIgnoreCase(selectType))
                                         {
                                             dtTmp.append("type=combobox|");
                                         }
                                         else if ("RADIO".equalsIgnoreCase(selectType))
                                         {
                                             dtTmp.append("type=radio|");
                                         }
                                         dtTmp.append("question=" + question + "|");
                                         dtTmp.append("recommendation=" + recommendationText + "}\n");
                                         
                                         //Get Connectors out of Question
                                         List<String> answerList = connectors.get(linkId);
                                         if (answerList != null)
                                         {
                                             Set<String> answerTextSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
//                                             int orderBy = 1;
                                             for (String answerEdge : answerList)
                                             {
                                                 String[] answerValues = answerEdge.split("::");
                                                 String answerType = answerValues[0];
                                                 String answerNodeId = answerValues[1];
                                                 String answerText = dtModel.get(answerNodeId);
                                                 String orderByStr = answerValues[3];//"9999";//String.format("%04d", orderBy);
//                                                 String orderByStr = String.format("%04d", orderBy);
                                                 String hiddenDiv = "<div class=\"x-hidden\" name=\""+orderByStr+"\"></div>";
                                                 Log.log.trace("Processing Answer " + answerEdge);
                                                 int textIndex = answerText.indexOf("::");
                                                 if (answerValues.length >= 4 && StringUtils.isNotBlank(answerValues[2]))
                                                 {
                                                     answerText = answerValues[2];
                                                 }
                                                 else if (textIndex != -1)
                                                 {
                                                     answerText = answerText.substring(textIndex + 2);
                                                     textIndex = answerText.indexOf("::");
                                                     if (textIndex != -1)
                                                     {
                                                         if (answerType.equalsIgnoreCase("Document") && textIndex != answerText.length()-2)
                                                         {
                                                             answerText = answerText.substring(textIndex + 2);
                                                         }
                                                         else
                                                         {
                                                             answerText = answerText.substring(0, textIndex);
                                                         }
                                                     }
                                                 }
                                                 answerText = answerText.replaceAll("\\s+", " ").trim(); 
                                                 answerText = StringEscapeUtils.escapeHtml(answerText);
                                                 //be sure to add parameter ROOT NODE and target id (answerValues[1]) to link
                                                 if (answerType.equalsIgnoreCase("Document"))
                                                 {
                                                     String answer = "{pre}" + hiddenDiv + answerText + "{pre}="
                                                     + "{pre}"+ dtModel.get(answerNodeId).split("::")[1];
                                                     if( !notFoundRunbook.contains(dtModel.get(answerNodeId).split("::")[1]))
                                                     {
	                                                     answer += "?ROOT=" + docName + "&NODE_ID=" + answerNodeId
	                                                     + "&ANSWER_TEXT=" + answerText;
	                                                     
	                                                     if (answerValues.length == 5)
	                                                     {
	                                                    	 // action tasks/runbooks to be executed before this document getting displayed.
	                                                    	 answer = answer + "&EXECUTION_LIST=" + answerValues[4];
	                                                     }
                                                     }
                                                     
                                                     // Add Expression if it exists
                                                     
                                                     if (expressions.containsKey(linkId + "->" + answerNodeId))
                                                     {
                                                         answer = answer + "&EXPRESSION=" + expressions.get(linkId + "->" + answerNodeId);
                                                     }
                                                     
                                                     answer+= "{pre}\n";
//                                                     answerTextSet.put(answerNodeId, answer);
                                                     answerTextSet.add(answer);
                                                 }
                                                 else if (answerType.equalsIgnoreCase("Question"))
                                                 {
                                                     //append decisionHolder: tmpValue
                                                     String answer = "{pre}" + hiddenDiv + answerText + "{pre}="
                                                     + "{pre}" + decisionHolderName
                                                     + "?ROOT=" + docName + "&NODE_ID=" + answerNodeId
                                                     + "&ANSWER_TEXT=" + answerText;
                                                     
                                                     if (answerValues.length == 5)
                                                     {
                                                         // action tasks/runbooks to be executed before this document getting displayed.
                                                         answer = answer + "&EXECUTION_LIST=" + answerValues[4];
                                                     }
                                                     
                                                     // Add Expression if it exists
                                                     
                                                     if (expressions.containsKey(linkId + "->" + answerNodeId))
                                                     {
                                                         answer = answer + "&EXPRESSION=" + expressions.get(linkId + "->" + answerNodeId);
                                                     }
                                                     
                                                     answer += "{pre}\n";
                                                     answerTextSet.add(answer);
                                                 }
                                                 
//                                                 orderBy++;
                                             }
                                             for (String answer : answerTextSet)
                                             {
                                                 dtTmp.append(answer);
                                             }
                                         }
                                         dtTmp.append("{decision}\n");
                                     }
                                     if (dtType.equalsIgnoreCase("Question") || dtType.equalsIgnoreCase("Task") ||
                                         dtType.equalsIgnoreCase("Runbook"))
                                     {
                                         if (ifTag)
                                         {
                                             dtTmp.append("#end\n");
                                             ifTag = false;
                                         }
                                     }
                                     
                                     if (!dt.toString().contains(dtTmp.toString()))
                                     {
                                         dt.append(dtTmp);
                                     }
                                 }
                             }
                       
                             if (dtType.equalsIgnoreCase("Document"))
                             {
                                 if (ifTag)
                                 {
                                     dt.append("#end\n");
                                     ifTag = false;
                                 }
                             }
                             
                             if (sectionStart)
                             {                                
                                 dt.append("{section}\n");
                                 sectionStart = false;
                             }
                             
                             Log.log.trace("Final section text to add to " + nodeDocName + ":\n" + dt.toString());
                             //If there were valid questions leading from Node
                             if (answerCount > 0)
                             {
                                 if (!dtType.equalsIgnoreCase("Question"))
                                 {
                                     decisionSections.put(nodeDocName + "_" + nodeId, dt.toString());
                                 }
                                 else
                                 {
                                     decisionHolder.append(dt);
                                 }
                             }
                        }
                    }
                    else
                    {
                        Log.log.error("Malformed Node Data: " + dtModel.get(nodeId));
                    }
                }
                
                saveDecisionTreeSections(docName, decisionSections, decisionHolder.toString(), user);
            }
        }
        catch (DocumentException de)
        {
            Log.log.error("Failed to Parse Decision Tree Model", de);
        }
        catch (Exception e)
        {
            Log.log.error("Failed to Create Decision Tree", e);
            throw e;
        }
    } //applyDecisionTreeXML
    
//    private static void listTasks(String dstId, String srcId, List<String> taskList, Map<String, String>dtModel, List<Node> list, Map<String, List<String>> connectors)
//    {
//    	String localDstId = dstId;
//		
//		for (Node node : list)
//		{
//			String tmpSrcId = node.valueOf("@source");
//            String tmpDstId = node.valueOf("@target");
//            
//            if (tmpSrcId.equals(localDstId))
//            {
//            	taskList.add(dtModel.get(localDstId).split("::")[1]);
//            	if (dtModel.get(tmpSrcId) != null && dtModel.get(tmpDstId) != null)
//                {
//                    //String tmpSrcType = dtModel.get(tmpSrcId).split("::")[0];
//                    String tmpDstType = dtModel.get(tmpDstId).split("::")[0];
//                    
//                    if ("Task".equals(tmpDstType))
//                    {
//                    	localDstId = tmpDstId;
//                    	continue;
//                    }
//                    //add connector to list with srcId as origin
//                    List<String> connector = connectors.get(srcId);
//                    if (connector == null)
//                    {
//                        connector = new ArrayList<String>();
//                        connectors.put(srcId, connector);
//                    }
//                    String orderStr = getOrderValue(node.getParent().valueOf("@order"));
//                    Log.log.trace("Add Document Connector->" + tmpDstType + "::" + tmpDstId + "::" + node.getParent().valueOf("@label"));
//                    connector.add(tmpDstType + "::" + tmpDstId + "::" + edge.getParent().valueOf("@label") + "::" + orderStr + "::" + taskList);
//                }
//            }
//		}
//    }
    
    private static WikiDocument saveDecisionTreeSections(String rootDocName, Map<String, String> decisionSections, String decisionWikiContent, String user)
    {
        String decisionHolderName = rootDocName + "_Decision";
        
        WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
            
        //list of sections currently applied by DT
        Map<String,String> currentDTSections = getDocumentSectionsInDecisionTree(rootDocName);
        //list of documents to be updated
        Map<String,WikiDocument> editedDTDocs = new HashMap<String,WikiDocument>();
        //current wiki name, doc, and content being edited
        String currentWikiName = null;
        WikiDocument currentWiki = null;
        String content = null;
        //for all sections that need to be applied
        Log.log.trace("Applying Decision Tree Sections");
        for (String wikiDocSection : decisionSections.keySet())
        {
            int index = wikiDocSection.lastIndexOf("_");
            String wiki = "";
            if (index > 0)
            {
                wiki = wikiDocSection.substring(0, index);
                //if not the current "edited" wiki load the wiki doc and content
                if (!wiki.equals(currentWikiName))
                {
                    try
                    {
                        WikiDocument example = new WikiDocument();
                        example.setUFullname(wiki);

                        currentWiki = (WikiDocument) HibernateProxy.execute(() -> {
                        	  return dao.findFirst(example, "pbActivities");
                        });
                        
                      currentWikiName = wiki;
                
                      
                      if(currentWiki != null)
                      {
                          content = currentWiki.getUContent();
                          editedDTDocs.put(wiki,currentWiki);
                      }
                       
                      
                    }
                    catch (Throwable t)
                    {
                        Log.log.error("Failed to Look up Wiki Document: " + wiki, t);
                                              HibernateUtil.rethrowNestedTransaction(t);
                    }
                }
                
                //make sure that null is not a part of the content. 
                if(StringUtils.isBlank(content))
                {
                    content = "";
                }
                    
                if (currentWiki != null)
                {
                    //check if current section already exists
                    String currentText = currentDTSections.get(wikiDocSection);
                    if (currentText != null)
                    {
                        //if it does replace it with new section content
                        content = content.replace(currentText, decisionSections.get(wikiDocSection));
                        currentWiki.setUContent(content);
                        //remove for current DT sections
                        currentDTSections.remove(wikiDocSection);
                    }
                    else
                    {
                        //if it does not append to the document content
                        content = content + "\n" + decisionSections.get(wikiDocSection);
                        currentWiki.setUContent(content);
                    }
                }
                else
                {
                    Log.log.warn("Current Wiki is Null");
                }
            }
            else
            {
                Log.log.error("Wiki Doc Name Section Missing Node Id " + wikiDocSection);
            }
        }
        //for all current sections left over, they need to be removed
        Log.log.trace("Removing Old Decision Tree Sections");
        for (String wikiDocSection : currentDTSections.keySet())
        {
            int index = wikiDocSection.lastIndexOf("_");
            String wiki = "";
            if (index > 0)
            {
                wiki = wikiDocSection.substring(0, index);
                //check if document already it "edited" list
                WikiDocument wikiDoc = editedDTDocs.get(wiki);
                if (wikiDoc == null)
                {
                    try
                    {
                    	
                        //if not load it
                        WikiDocument example = new WikiDocument();
                        example.setUFullname(wiki);
                        wikiDoc = (WikiDocument) HibernateProxy.execute(() -> {
                        	return dao.findFirst(example, "pbActivities");
                        });                         
                        
                        editedDTDocs.put(wiki,wikiDoc);
                    }
                    catch (Throwable t)
                    {
                        Log.log.error("Failed to Look up Wiki Document: " + wiki, t);
                                              HibernateUtil.rethrowNestedTransaction(t);
                    }
                }
                    
                //remove section from current content
                content = wikiDoc.getUContent();
                content = content.replace(currentDTSections.get(wikiDocSection), "").trim();
                wikiDoc.setUContent(content);
            }
            else
            {
                Log.log.error("Wiki Doc Name Section For Removal Missing Node Id " + wikiDocSection);
            }
        }
        //save all edited documents
        Log.log.trace("Saving All Changes");
        WikiDocument returnDoc = null;
        for (String editedWiki : editedDTDocs.keySet())
        {
            try
            {
                returnDoc = (WikiDocument) HibernateProxy.execute(() -> {
                	 WikiDocument doc = dao.persist(editedDTDocs.get(editedWiki));
                     if( doc.getUFullname().equals(rootDocName)) {
                         return doc;
                     }
                     return null;
                });
            }
            catch (Throwable t)
            {
                Log.log.error("Failed to Persist Edited DT Document: " + editedWiki, t);
                              HibernateUtil.rethrowNestedTransaction(t);
            }
        }
        //create or update _Decision wiki document
        if (StringUtils.isNotEmpty(decisionWikiContent))
        {
            try
            {
                buildDecisionDocument(rootDocName, decisionHolderName, user, decisionWikiContent);
                editedDTDocs.put(decisionHolderName, null);
            }
            catch (Throwable t)
            {
                Log.log.error("Failed to Build Decision Document " + decisionHolderName, t);
            }
        }
            
        for (String wikiName : editedDTDocs.keySet())
        {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put(ConstantValues.WIKI_DOCUMENT_NAME, wikiName); 
            params.put(ConstantValues.WIKIDOC_STATS_COUNTER_NAME_KEY, WikiDocStatsCounterName.EDITED);
            params.put(ConstantValues.USERNAME, user); 
            
            com.resolve.services.hibernate.util.WikiUtils.sendESBForUpdatingWikiRelations(params);
//            ServiceHibernate.executeSystemExecutor("com.resolve.services.hibernate.util.UpdateWikiDocRelations", "updateContentWikiDocRelationship", params); 
        }
        
        Log.log.trace("Finished with Applying Decision Tree");
        return returnDoc;
    }//saveDecisionTreeSections
    
    @SuppressWarnings("unchecked")
    private static boolean validateDTXML(String docName, String xml, String user) throws DocumentException, SAXException
    {
        boolean result = true;
        if (StringUtils.isEmpty(xml))
        {
            Log.log.info("Model is Empty, Removing all Decision Tree Sections");
            removeDecisionTree(docName, user);
            result = false;
        }
        else
        {
            SAXReader reader = new SAXReader();
            reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
            reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            Document document = reader.read(new ByteArrayInputStream(xml.getBytes()));
            //Document document = DocumentHelper.parseText(xml);
            
            Node root = document.selectSingleNode("/mxGraphModel/root/Start");
            if (root == null)
            {
                Log.log.warn("Model is Missing the Root Node, Removing all Decision Tree Sections");
                removeDecisionTree(docName, user);
                result = false;
            }
            else
            {
                List<Node> list = document.selectNodes("/mxGraphModel/root/Document");
                if (list == null || list.size() == 0)
                {
                    Log.log.error("DT Model Does Not Have any Documents");
                    result = false;
                }
                list = document.selectNodes("/mxGraphModel/root/Question");
                if (list == null || list.size() == 0)
                {
                    Log.log.error("Model Does Not Have any Questions");
                    result = false;
                }
                
                list = document.selectNodes("/mxGraphModel/root/Edge/mxCell");
                if (list == null || list.size() == 0)
                {
                    Log.log.error("Model is Missing Connector(s)");
                    result = false;
                }
            }
        }
        return result;
    } //validateDTXML
    
    private static boolean buildDecisionDocument(String rootFullName, String decisionFullName, String username, String content) throws WikiException
    {
        boolean result = false;
        
        Log.log.trace("Getting Wiki Instance");
        
        String decisionNamespace = decisionFullName.substring(0, decisionFullName.indexOf("."));
        String decisionName = decisionFullName.substring(decisionFullName.indexOf(".")+1);
        
        boolean isWikiDocExist = WikiUtils.isWikidocExist(decisionNamespace + "." + decisionName);
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            result = (boolean) HibernateProxy.execute(() -> {
            	WikiDocument decisionDoc = null;
            	WikiDocument rootDoc = com.resolve.services.hibernate.util.WikiUtils.getWikiDocumentModel(null, rootFullName, username, true);//StoreUtility.findAndLoadReferences(rootFullName);
	            if(isWikiDocExist)
	            {
	                decisionDoc = com.resolve.services.hibernate.util.WikiUtils.getWikiDocumentModel(null, decisionFullName, username, true);//StoreUtility.findAndLoadReferences(decisionFullName);
	            }
	            else
	            {
	                decisionDoc = new WikiDocument();
	                decisionDoc.setUFullname(decisionFullName);
	                decisionDoc.setUNamespace(decisionNamespace);
	                decisionDoc.setUName(decisionName);
	                decisionDoc.setUIsHidden(false);
	                decisionDoc.setUIsDeleted(false);
	                decisionDoc.setUIsActive(true);
	                decisionDoc.setUIsLocked(false);
	            }
	            
	            decisionDoc.setUContent(content);
	            decisionDoc.setUIsDefaultRole(rootDoc.ugetUIsDefaultRole());
	            decisionDoc.setUReadRoles(rootDoc.getUReadRoles());
	            decisionDoc.setUWriteRoles(rootDoc.getUWriteRoles());
	            decisionDoc.setUExecuteRoles(rootDoc.getUExecuteRoles());
	            decisionDoc.setUAdminRoles(rootDoc.getUAdminRoles());
	            
	            SaveHelper docSave = new SaveHelper(decisionDoc, username);
	            docSave.setUserShouldFollow(false);
	            docSave.save();
	            
	            return true;
            
            });
        }
        catch(Throwable t)
        {
            result = false;
            
            if (StringUtils.isBlank(t.getMessage()) || 
            	!(t.getMessage().startsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_PREFIX) &&
            	  (t.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_SUFFIX) ||
            	   t.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_HA_VIOLATED_SUFFIX)))) {
            	Log.log.error("Failed to Save Decision Document", t);
            }
                      HibernateUtil.rethrowNestedTransaction(t);
            
        } 
        
        return result;
    }//buildDecisionDocument
    
    private static String getOrderValue(String orderStr)
    {
        String result = "9999";

        if (StringUtils.isNotEmpty(orderStr))
        {
            try
            {
                Integer order = Integer.parseInt(orderStr);
                result = String.format("%04d", order);
            }
            catch (Throwable t)
            {
                result = "9999";
            }
        }

        return result;

    }
    
    @SuppressWarnings("unchecked")
	public static Set<String> getAllDTComponents(WikiDocument wiki, String component)
    {
    	Set<String> componentNames = new HashSet<String>();
    	
    	try
		{
    		String xml = wiki.getUDecisionTree();
    		if (StringUtils.isNotBlank(xml))
    		{
    		    SAXReader reader = new SAXReader();
                reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
                reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
                reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
                Document document = reader.read(new ByteArrayInputStream(xml.getBytes()));
				//Document document = DocumentHelper.parseText(xml);
				List<Node> listWikiDocNode = document.selectNodes("/mxGraphModel/root/" + component);
				if (listWikiDocNode.size() > 0)
				{
					
					for (Iterator<Node> i = listWikiDocNode.iterator(); i.hasNext();)
					{
						Node xmlNode = i.next();
						String componentName = null;
						
						if (component.equals("Task"))
						{
							List<Node> taskNodes = xmlNode.selectNodes("name");
							if (taskNodes != null)
							{
								componentName = taskNodes.get(0).getText().trim();
							}
						}
						else
						{
							/*
							 * All new DT's will populate description attribute with the name
							 * of the component. If it's blank, fall back on the label attribute.
							 */
							componentName = xmlNode.valueOf("@description");
							if (StringUtils.isBlank(componentName))
							{
								componentName = xmlNode.valueOf("@label");
							}
						}
						
						if (StringUtils.isBlank(componentName))
						{
							Log.log.error("Could not gather " + component + " for DT" + wiki.getUFullname());
						}
						else
						{
							componentNames.add(componentName);
						}
					}
				}
				
				if (component.equals("Task"))
				{
					String dtOptionsJson = wiki.getUDTOptions();
					if (StringUtils.isNotBlank(dtOptionsJson))
					{
						Map<String, Object> dtOptionsMap = new ObjectMapper().readValue(dtOptionsJson, Map.class);
						if (dtOptionsMap != null)
						{
							List<Map<String, String>> dtVariablesMapList = (List<Map<String, String>>)dtOptionsMap.get("dtVariables");
							if (dtVariablesMapList != null && dtVariablesMapList.size() > 0)
							{
								for (Map<String, String> dtVariableMap : dtVariablesMapList)
								{
									String taskName = dtVariableMap.get("taskName");
									if (StringUtils.isNotBlank(taskName))
									{
										componentNames.add(taskName.trim());
									}
								}
							}
						}
					}
				}
    		}
		}
    	catch(Throwable t)
    	{
    		Log.log.error("Could not gather " + component + "(s) from DT " + wiki.getUFullname(), t);
    	}
    	
    	return componentNames;
    }
    @SuppressWarnings("unchecked")
	public static void resetDTData(String problemId, String dtFullName, String username)
    {
    	// clean dt_metric
    	try
    	{
	    	Object property = ServiceWorksheet.getWorksheetWSDATAProperty(problemId, Constants.DT_METRIC, username);
	    	if (property != null)
	    	{
	    		DTMetricLogList dtMetricLogList = (DTMetricLogList) new ObjectMapper().readValue((String) property, DTMetricLogList.class);
	    		List<DTMetricLogVO> dtMetricLogVOList = dtMetricLogList.getDtMetricLogs();
	    		
	    		if (dtMetricLogVOList != null && dtMetricLogVOList.size() > 0)
	    		{
	    			Iterator<DTMetricLogVO> dtMetricLogVOIterator = dtMetricLogVOList.iterator();
	    			while (dtMetricLogVOIterator.hasNext())
	    			{
	    				DTMetricLogVO dtMetricLogVO = dtMetricLogVOIterator.next();
	    				if (dtMetricLogVO.getRootDtFullName().toLowerCase().equals(dtFullName.toLowerCase()))
	    				{
	    					dtMetricLogVOIterator.remove();
	    					ServiceWorksheet.setWorksheetWSDATAProperty(problemId, Constants.DT_METRIC, new ObjectMapper().writeValueAsString(dtMetricLogList), username);
	    					break;
	    				}
	    			}
	    		}
	    	}
    	}
    	catch(Exception e)
    	{
    		Log.log.error("Could not parse and clean dt_matric for DT: " + dtFullName + ", problemId: " + problemId, e);
    	}
    	
    	// clean answer_params_map and all the corresponding key-value pairs.
    	try
    	{
	    	String answerParamsMapJson = (String) ServiceWorksheet.getWorksheetWSDATAProperty(problemId, ConstantValues.ANSWER_PARAMS_MAP, username);
	    	if (StringUtils.isNotBlank(answerParamsMapJson))
	    	{
	    		Map<String, String> answerParamsMap = new ObjectMapper().readValue(answerParamsMapJson, LinkedHashMap.class);
	    		Set<String> keys = answerParamsMap.keySet();
	    		boolean keyFound = false;
	    		if (keys.size() > 0)
	    		{
	    			List<String> keysToBeRemoved = new ArrayList<String>();
	    			for (String key : keys)
	    			{
	    				if (key.toLowerCase().contains(dtFullName.toLowerCase()))
	    				{
	    					keyFound = true;
	    					String keyValueJson = answerParamsMap.get(key);
	    					if (StringUtils.isNotBlank(keyValueJson))
	    					{
	    						Map<String, String> answerMap = new ObjectMapper().readValue(keyValueJson, Map.class);
	    						keysToBeRemoved.addAll(answerMap.keySet());
	    					}
	    					answerParamsMap.remove(key);
	    				}
	    			}
	    			
	    			if (keysToBeRemoved.size() > 0)
	    			{
	    				ServiceWorksheet.deleteWorksheetWSDATA(problemId, keysToBeRemoved.toArray(new String[0]), username);
	    			}
	    			
	    			if (keyFound)
	    			{
	    				String mapJson = new ObjectMapper().writeValueAsString(answerParamsMap);
	    				ServiceWorksheet.setWorksheetWSDATAProperty(problemId, ConstantValues.ANSWER_PARAMS_MAP, mapJson, username);
	    			}
	    		}
	    	}
    	}
    	catch(Exception e)
    	{
    		Log.log.error("Could not parse and clean answer_params_map for DT: " + dtFullName + ", problemId: " + problemId, e);
    	}
    }
}
