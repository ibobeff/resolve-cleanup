/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.graph.social.model.component;

import com.resolve.services.graph.social.model.RSPublisher;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.vo.social.NodeType;

/**
 * Runbook is only a publisher  
 * 
 * @author jeet.marwah
 *
 */
public class Runbook extends Document implements RSPublisher
{
    private static final long serialVersionUID = 3556107679214461045L;
    
//    public static final String TYPE = "runbook";
    
    public Runbook()
    {
       this.setType(NodeType.RUNBOOK);
    }
    
    public Runbook(String sysId, String name) 
    {
        this.setType(NodeType.RUNBOOK);
        this.setId(sysId + "-" + NodeType.RUNBOOK.name());
        this.setSys_id(sysId + "-" + NodeType.RUNBOOK.name());
        this.setName(name);
    }

    public Runbook(ResolveNodeVO node) throws Exception
    {
        super(node);
        this.setId(node.getUCompSysId() + "-" + NodeType.RUNBOOK.name());
        this.setSys_id(node.getUCompSysId() + "-" + NodeType.RUNBOOK.name());
        this.setType(NodeType.RUNBOOK);
    }
}
