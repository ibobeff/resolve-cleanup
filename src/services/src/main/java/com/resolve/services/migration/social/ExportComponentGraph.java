/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.migration.social;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.graphdb.Node;

import com.resolve.services.migration.neo4j.GraphDBManager;
import com.resolve.services.migration.neo4j.SocialRelationshipTypes;
import com.resolve.util.StringUtils;

public abstract class ExportComponentGraph
{
    protected String sysId = null;
    protected SocialRelationshipTypes type = null;

    protected List<GraphRelationshipDTO> relationships = new ArrayList<GraphRelationshipDTO>();
    
    public ExportComponentGraph(String sysId, SocialRelationshipTypes type) throws Exception
    {
        if(StringUtils.isEmpty(sysId) || type == null)
        {
            throw new Exception("sysId and type are mandatory.");
        }
        
        this.sysId = sysId;
        this.type = type;
    }
    
    public List<GraphRelationshipDTO> export() throws Exception
    {
        Node compNode = validate();
        return exportRelationships(compNode);
    }
    
    protected abstract List<GraphRelationshipDTO> exportRelationships(Node compNode) throws Exception;
    
    protected Node validate() throws Exception
    {
        Node compNode = GraphDBManager.getInstance().findNodeByIndexedID(sysId);
        if(compNode == null)
        {
            throw new Exception(type + " with sysId " + sysId + " does not exist");
        }
        
        String sourceName = (String) compNode.getProperty(GraphDBManager.DISPLAYNAME);
        if(compNode.hasProperty("NODE_TYPE"))
        {
            String sourceType = (String) compNode.getProperty("NODE_TYPE");
            if(!sourceType.equalsIgnoreCase(type.name()))
            {
                throw new Exception("This node is not of a " + type + " but of " + sourceType + " with name " + sourceName);
            }
        }
        
        return compNode;
    }
    
    protected void addRelationship(Node compNode, Node anyOtherNode, String relationshipTypeStr) throws Exception
    {
        String sourceSysId = (String) compNode.getProperty(GraphDBManager.SYS_ID);
        String sourceName = (String) compNode.getProperty(GraphDBManager.DISPLAYNAME);
        String sourceType = (String) compNode.getProperty("NODE_TYPE");
        if(compNode.hasProperty("username") && sourceType.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
        {
            sourceName =  (String) compNode.getProperty("username");
        }
        
        String targetSysId = (String) anyOtherNode.getProperty(GraphDBManager.SYS_ID);
        String targetType = (String) anyOtherNode.getProperty("NODE_TYPE");
        
        String targetName = anyOtherNode.hasProperty(GraphDBManager.DISPLAYNAME) ? (String) anyOtherNode.getProperty(GraphDBManager.DISPLAYNAME) : null;
                
        if(targetType.equalsIgnoreCase(SocialRelationshipTypes.USER.name()))
        {
            targetName =  (String) anyOtherNode.getProperty("username");
        }
        
        GraphRelationshipDTO relation = new GraphRelationshipDTO();
        relation.setSourceSysId(sourceSysId);
        relation.setSourceName(sourceName);
        relation.setSourceType(SocialRelationshipTypes.valueOf(sourceType));
        relation.setTargetSysId(targetSysId);
        
        if(targetName != null && !targetName.isEmpty())
        {
            relation.setTargetName(targetName);
        }
        
        relation.setTargetType(SocialRelationshipTypes.valueOf(targetType));
        
        RelationType relationType = null;
        
        if(relationshipTypeStr != null && !relationshipTypeStr.isEmpty())
        {
            try
            {
                relationType = RelationType.valueOf(relationshipTypeStr);
            }
            catch(Exception e)
            {
                ;
            }
        }
        
        if(relationType != null)
        {
            relation.setRelationType(relationType);
        }
        
        relation.setGraphRelationType(relationshipTypeStr);
        
        relationships.add(relation);
    }
}
