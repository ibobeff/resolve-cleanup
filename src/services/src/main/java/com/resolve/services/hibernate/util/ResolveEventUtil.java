/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.LockOptions;
import org.hibernate.query.Query;

import com.resolve.persistence.model.ResolveEvent;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.ResolveEventVO;
import com.resolve.util.Log;

public class ResolveEventUtil
{
    public static void insertResolveEvent (ResolveEventVO eventVO) throws Exception
    {
        ResolveEvent event = new ResolveEvent(eventVO);
        try
        {
          HibernateProxy.setCurrentUser("admin");
            HibernateProxy.executeNoCache( () -> {
            	HibernateUtil.getDAOFactory().getResolveEventDAO().insertOrUpdate(event);
            });

        }
        catch (Throwable e)
        {
            Log.log.error("Error while insertResolveEvent. UValue=" + eventVO.getUValue(), e);
            
            throw e;
        }
    }
    
    @SuppressWarnings("rawtypes")
	public static void clearAllResolveEventByValue(String eventValue) throws Exception
    {
        String sqlQuery = "delete from ResolveEvent where UValue like :eventValue";
        try
        {
        	HibernateProxy.execute(() -> {
	            Query query = HibernateUtil.createQuery(sqlQuery);
	            query.setParameter("eventValue", eventValue + "%");
	            query.executeUpdate();
        	});
        }
        catch (Throwable e)
        {
            Log.log.error("Error while clearAllResolveEventByValue. UValue=" + eventValue, e); 
            throw e;
        }
    }
    
    @SuppressWarnings("unchecked")
    public static ResolveEventVO getLatestEventByValue(String eventValue) throws Exception
    {
        ResolveEventVO eventVO = null;
        List<ResolveEvent> list = null;
        String sqlQuery = "from ResolveEvent where UValue like '" + eventValue + "%' order by UValue DESC";
        
        try
        {
        	while (true)
            {
                /*
                 * During import operation, if custom table is imported,
                 * we re-initialize hibernate and any call during this time to DB may throw an exception.
                 * So, wait until hibernate initializes. 
                 */
                if (HibernateUtil.isHibernateInitializing2())
                {
                    Thread.sleep(1000);
                }
                else
                {
                    break;
                }
            }
        	
            list = (List<ResolveEvent>) HibernateProxy.executeNoCache(() -> {
            	Query<ResolveEvent> query = HibernateUtil.createQuery(sqlQuery).setMaxResults(1);
                return query.list();
            });           
            
        }
        catch (Throwable e)
        {
            Log.log.error("Error while getLatestEventByValue. UValue=" + eventValue, e);
            
            throw e;
        }
        
        if (list != null && !list.isEmpty())
        {
            eventVO = list.get(0).doGetVO();
        }
        
        return eventVO;
    }
    
    @SuppressWarnings("unchecked")
    public static ResolveEventVO getLatestImpexEventByValue() throws Exception
    {
        ResolveEventVO eventVO = null;
        List<ResolveEvent> list = null;
        String sqlQuery = "from ResolveEvent where UValue like 'IMPEX%' order by sysUpdatedOn DESC";
        
        try
        {
            while (true)
            {
                /*
                 * During import operation, if custom table is imported,
                 * we re-initialize hibernate and any call during this time to DB may throw an exception.
                 * So, wait until hibernate initializes. 
                 */
                if (HibernateUtil.isHibernateInitializing2())
                {
                    Thread.sleep(1000);
                }
                else
                {
                    break;
                }
            }
            
          HibernateProxy.setCurrentUser("admin");
            list = (List<ResolveEvent>) HibernateProxy.executeNoCache(() -> {
            	Query<ResolveEvent> query = HibernateUtil.createQuery(sqlQuery).setMaxResults(1);
                return  query.list();
            });            
            
        }
        catch (Throwable e)
        {
            Log.log.error("Error while getLatestEventByValue of Impex", e);
            
            throw e;
        }
        
        if (list != null && !list.isEmpty())
        {
            eventVO = list.get(0).doGetVO();
        }
        
        return eventVO;
    }
    
    @SuppressWarnings("unchecked")
    public static ResolveEventVO getLatestImpexEventByValue(long waitForSecs) throws Exception
    {
        ResolveEventVO eventVO = null;
        List<ResolveEvent> list = null;
        String sqlQuery = "from ResolveEvent where UValue like 'IMPEX%' order by sysUpdatedOn DESC";
        long waitedSecs = 0;
        
        try
        {
            while (true)
            {
                /*
                 * During import operation, if custom table is imported,
                 * we re-initialize hibernate and any call during this time to DB may throw an exception.
                 * So, wait until hibernate initializes. 
                 */
                if (HibernateUtil.isHibernateInitializing2())
                {
                	if (waitForSecs > 0l && waitedSecs < waitForSecs) {
                		break;
                	}
                	
            		Thread.sleep(1000);
            		++waitedSecs;
                }
                else
                {
                    break;
                }
            }
            
            if (!HibernateUtil.isHibernateInitializing2()) {
            	
                                                             HibernateProxy.setCurrentUser("admin");
            	list = (List<ResolveEvent>) HibernateProxy.executeNoCache(() -> {
            		Query<ResolveEvent> query = HibernateUtil.createQuery(sqlQuery).setMaxResults(1);
    	            return query.list();
            	});
            }
        }
        catch (Throwable e)
        {
            Log.log.error("Error while getLatestEventByValue of Impex", e);
            
            throw e;
        }
        
        if (list != null && !list.isEmpty())
        {
            eventVO = list.get(0).doGetVO();
        }
        
        return eventVO;
    }
    
    /*
     * Returns previous value if one exists and and has last updated on time less than 5 minutes.
     * If no value or last updated time is more than 5 minutes then inserts the new record 
     * and return the newly inserted record  
     */
    @SuppressWarnings("unchecked")
    public static synchronized Pair<ResolveEventVO, Boolean> getAndSetImpexEventValue(String operation, long waitForSecs, 
    																				  String moduleName,
    																				  String userName) throws Exception
    {
        
        
        String sqlQuery = "from ResolveEvent where UValue like 'IMPEX%' order by sysUpdatedOn DESC";
        long waitedSecs = 0;
        long current = new Date().getTime();
        
        if (StringUtils.isBlank(moduleName)) {
        	throw new IllegalArgumentException("Module Name");
        }
        
        if (StringUtils.isBlank(userName)) {
        	throw new IllegalArgumentException("User Name");
        }
        
        try
        {
            while (true)
            {
                /*
                 * During import operation, if custom table is imported,
                 * hibernate is re-initialized and any call during this time to DB may throw an exception.
                 * So, wait until hibernate initializes. 
                 */
                if (HibernateUtil.isHibernateInitializing2())
                {
                	if (waitForSecs > 0l && waitedSecs < waitForSecs) {
                		throw new Exception(String.format("Operation timed out after waiting for %d seconds as Hibernate is " +
								  						  "still initializing.", waitForSecs));
                	}
                	
                	Thread.sleep(1000);
                	++waitedSecs;
                }
                else
                {
                    break;
                }
            }
            
            if (!HibernateUtil.isHibernateInitializing2()) {
                                                             HibernateProxy.setCurrentUser(userName);
	            return (Pair<ResolveEventVO, Boolean>) HibernateProxy.executeNoCache(() -> {
	            	List<ResolveEvent> list = null;
	            	ResolveEventVO eventVO = null;
	                Boolean isNew = Boolean.FALSE;
	            	Query<ResolveEvent> query = HibernateUtil.createQuery(sqlQuery).setMaxResults(1);
	  	            list = query.list();
	  	            
	  	            boolean locked = false;
	  	            
	  	            if (list != null && !list.isEmpty()) {
	  	            	LockOptions lockOpt = new LockOptions();
	  	            	lockOpt.setLockMode(LockMode.UPGRADE_NOWAIT);
	  	            	
	  	            	try {
	  	            		HibernateUtil.getCurrentSession().buildLockRequest(lockOpt).lock(list.get(0));
	  	            		locked = true;
	  	            	} catch (HibernateException he) {
	  	            		Log.log.info(String.format("Error%sin locking Resolve Event entity for %s", 
	  	            								   (StringUtils.isNotBlank(he.getMessage()) ? 
	  	            									" " + he.getMessage() + " " : " "), LockMode.UPGRADE_NOWAIT));
	  	            	}
	  	            }
	  	            
	  	            if (list != null && !list.isEmpty()) {
	  	            	eventVO = list.get(0).doGetVO();
	  	            }
	  	            
	  	            if ( eventVO == null || 
	  	            	 (eventVO != null && locked &&
	  	            	  (eventVO.getUValue().contains(",-2,") || eventVO.getUValue().contains(",-1,") ||
	  	            	   (!eventVO.getUValue().contains("IMPEX-UPLOAD") && !eventVO.getUValue().contains("IMPEX-URLUPLOAD") && 
	  	            		!eventVO.getUValue().contains("IMPEX-UPLOADANDINSTALL") && 
	  	            		(current - eventVO.getSysUpdatedOn().getTime()) > 300000) ||
	  	            	   ((eventVO.getUValue().contains("IMPEX-UPLOAD") || eventVO.getUValue().contains("IMPEX-URLUPLOAD") ||
	  	            		 eventVO.getUValue().contains("IMPEX-UPLOADANDINSTALL")) && 
	  	   	            	(current - eventVO.getSysUpdatedOn().getTime()) > 600000)))) {
	  	            	ResolveEvent newResolveEvent = new ResolveEvent(String.format("IMPEX-%s Init:%s,1,0", operation, moduleName));
	  	            	
	  	            	/*
	  	            	 * For MySQL if event value contains -2 (i.e. previous operation is complete) or -1 (i.e. previous operation
	  	            	 * resulted in error), next operation will start after 1.5 seconds delay. MySQL time stamp does not store 
	  	            	 * milliseconds which breaks the logic for retrieving latest event value. If two sub-second events are saved 
	  	            	 * in MySQL DB, it cannot order them properly to return latest event.
	  	            	 */
	  	            	
	  	            	if (HibernateUtil.isMySQL()) {
	  	            		Thread.sleep(1500);
	  	            	}
	  	            	
	  	            	ResolveEvent insertedRSEvent = HibernateUtil.getDAOFactory().getResolveEventDAO().insert(newResolveEvent);
	  	            	HibernateUtil.getCurrentSession().flush();
	  	            	
	  	            	if (insertedRSEvent != null) {
	  	            		isNew = Boolean.TRUE;
	  	            		eventVO = insertedRSEvent.doGetVO();
	  	            		Log.log.info(String.format("Inserted New Resolve Event [%s].", eventVO));
	  	            	}
	  	            }
	  	            
	  	          return Pair.of(eventVO, isNew);
	            });  
            }
            return null;
        }
        catch (Throwable e)
        {
            Log.log.error(String.format("Error%sin get and set resolve event Value for %s operation, %s module",
            							(StringUtils.isNotBlank(e.getMessage()) ? " [" + e.getMessage() + "] " : " "),
            							operation, moduleName), e);
            
            throw e;
        }        
        
    }
    
    public static void updateResolveEvent (String operation, String oldModuleName, String newModuleName) throws Exception
    {
    	if (StringUtils.isBlank(oldModuleName)) {
        	throw new IllegalArgumentException("Old Module Name");
        }
        
        if (StringUtils.isBlank(newModuleName)) {
        	throw new IllegalArgumentException("New Module Name");
        }
        
        ResolveEventVO oldEventVO = getLatestEventByValue(String.format("IMPEX-%s%%:%s", operation, oldModuleName));
        
        if (oldEventVO == null) {
        	throw new Exception (String.format("Resolve Event with old module name %s not found", oldModuleName));
        }
        
        Log.log.debug(String.format("Resolve Event with Old Module Name [%s]", oldEventVO));
        
        oldEventVO.setUValue(oldEventVO.getUValue().replaceFirst(oldModuleName, newModuleName + "|" + oldModuleName));
        
        Log.log.debug(String.format("Resolve Event with New Module Name [%s]", oldEventVO));
        
        try
        {
          HibernateProxy.setCurrentUser("admin");
            HibernateProxy.executeNoCache( () -> {
            	HibernateUtil.getDAOFactory().getResolveEventDAO().insertOrUpdate(new ResolveEvent(oldEventVO));
            });

        }
        catch (Throwable e)
        {
            Log.log.error(String.format("Error%swhile updating module name from %s to %s in Resolve Event.",
            							(StringUtils.isNotBlank(e.getMessage()) ? " " + e.getMessage() + " " : " "),
            							oldModuleName, newModuleName), e);
            
            throw e;
        }
    }
}
