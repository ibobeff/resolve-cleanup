/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied, 
 * modified, or distributed without the express written permission of 
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.migration.entities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.util.GeneralHibernateUtil;
import com.resolve.services.hibernate.util.GraphUtil;
import com.resolve.services.hibernate.util.SocialUtil;
import com.resolve.services.hibernate.vo.ResolveNodeVO;
import com.resolve.services.interfaces.SocialDTO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.services.vo.social.NodeType;
import com.resolve.services.vo.social.SocialForumDTO;
import com.resolve.services.vo.social.SocialProcessDTO;
import com.resolve.services.vo.social.SocialRssDTO;
import com.resolve.services.vo.social.SocialTeamDTO;
import com.resolve.util.JavaUtils;
import com.resolve.util.Log;

/**
 * main purpose of this utility class is to make sure that social component entities are available and the social notification master table is also
 * updated 
 * 
 * This is  multithreaded version of MIgrateSocialEntities
 * 
 * @author jeet.marwah
 *
 */
public class MigrateEntities
{
    private final int FIXED_POOL_THREAD_COUNT = 40;
    private final int MAX_WIKIDOC_BATCH_SIZE = 500;
    
    //thread pool service
    private ExecutorService service = Executors.newFixedThreadPool(FIXED_POOL_THREAD_COUNT);
    
    public static Collection<String> startMigration()
    {
        MigrateEntities migrateEntities = new MigrateEntities();
        return migrateEntities.migrate();
    }
    
    private Collection<String> migrate()
    {
        long startTime = System.currentTimeMillis();
        Collection<String> pCompSysIds = new ArrayList<String>();
        
        if(doMigration())
        {
            Log.log.info("Hibernate JDBC Batch Size is set to " + HibernateUtil.getJdbcBatchSize() + ".");
            
            //migrate docs
            pCompSysIds.addAll(migrateWikidocs());
            
            //namespace
            pCompSysIds.addAll(migrateNamespaces());
    
            //actiontask
            pCompSysIds.addAll(migrateActiontasks());
            
            //users
            pCompSysIds.addAll(migrateUsers());
    
            //process
            pCompSysIds.addAll(migrateProcesses());
    
            //team
            pCompSysIds.addAll(migrateTeams());
    
            //forum
            pCompSysIds.addAll(migrateForums());
            
            //rss
            pCompSysIds.addAll(migrateRss());
    
            //tags
    //        migrateTags();
        }
        else
        {
            Log.log.debug("Entities Migration was already done.");
        }
        
        try
        {
            //shutdown the service
            service.shutdown();
            
            //wait till all the records are executed 
            // for 200k recs, it took more then 5 hours, so just adding a big time to make sure that is completed in case that is the case
            service.awaitTermination(10, TimeUnit.HOURS);
        }
        catch (Exception e)
        {
            Log.log.error("Error in shutdown of migration thread pool");
        }
        
        Log.log.debug(">>>>>>>>>>>>>>> Total time for migrating " + pCompSysIds.size() + ":" + ( (System.currentTimeMillis() - startTime)/ 1000d));
        
        return pCompSysIds;
    }
    
    private boolean doMigration()
    {
        return !GeneralHibernateUtil.modelExists("ResolveNode", "system");
    }
    
    //Executes tasks and waits for them to finish before returning
    private Collection<String> executeTasks(Collection<Callable<Collection<String>>> tasks) throws Exception
    {
        //long total = 0l;
        Collection<String> pSysIds = new ArrayList<String>();
        List<Future<Collection<String>>> futures = service.invokeAll(tasks);
        
        if(futures != null && !futures.isEmpty())
        {
            for(Future<Collection<String>> future : futures)
            {
                if(future.isDone())
                {
                    try
                    {
                        //total += ((Integer)future.get()).intValue();
                        pSysIds.addAll(future.get());
                    }
                    catch (ExecutionException e)
                    {
                        Log.log.error(e.getMessage(), e);
                    }
                }
            }
        }
        
        return pSysIds;
    }
    
    private Collection<String> migrateWikidocs()
    {
        Log.log.debug("*** START UPDATE DOC TO GRAPH   ***");
        long startTime = System.currentTimeMillis();

        Collection<String> pCompSysIds = new ArrayList<String>();
        List<String> sysIds = new ArrayList<String>(GeneralHibernateUtil.getSysIdsForWithTimeout("WikiDocument", null, "system", 480));
        if (sysIds != null && sysIds.size() > 0)
        {
            Log.log.debug("Total # of Wikis : " + sysIds.size());
            int batchCount = 0;
            
            //////////////////////////////////////////
            //DOING 1 rec at a time - takes more than 5 hours for 200K docs
//            for (String sysId : sysIds)
//            {
//                Log.log.debug("Processed " + count++ + " Documents");
//                service.execute(new MigrateWikiDoc(sysId));
//
//                
//            }//end of for
            ////////////////////////////////////////////
            
            
            ///////////DOING A BATCH OF RECS - takes 36 secs for 5504 recs - This 
            
            int batchSize = 1;
            
            if(sysIds.size() > FIXED_POOL_THREAD_COUNT)
                batchSize = sysIds.size() / FIXED_POOL_THREAD_COUNT;
            
            if(batchSize > MAX_WIKIDOC_BATCH_SIZE)
                batchSize = MAX_WIKIDOC_BATCH_SIZE;
            
            List<List<String>> batchSysIds = JavaUtils.chopped(sysIds, batchSize);
            Collection<Callable<Collection<String>>> migrateWikiDocsTasks = new ArrayList<Callable<Collection<String>>>();
            for(List<String> batchSysId : batchSysIds)
            {
                migrateWikiDocsTasks.add(new MigrateWikiDocs(new HashSet<String>(batchSysId)));
                Log.log.trace("Prepared Batch # " + (++batchCount) + " of " + batchSysId.size());
            }
            
            try
            {                
                pCompSysIds = executeTasks(migrateWikiDocsTasks);
                
                Log.log.debug("Total # of Wikis Migrated: " + pCompSysIds.size());
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
            
            //all the recs at one shot - 98 secs for 5504 recs
//            new MigrateWikiDocs(null).run();
        }

        Log.log.debug("*** END OF UPDATE DOC TO GRAPH  *** " + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
        
        return pCompSysIds;
    }
    
    private Collection<String> migrateNamespaces()
    {
        Log.log.debug("*** START NAMESPACES TO GRAPH   ***");
        long startTime = System.currentTimeMillis();
        Collection<String> pCompSysIds = new ArrayList<String>();
        
        Set<String> namespaces = new HashSet<String>();
        String sql = "select DISTINCT UNamespace from WikiDocument order by UNamespace";
        try
        {
            List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, new HashMap<String, Object>());
            for (Object o : list)
            {
                String instance = (String) o;
                /*
                 *  Note: if Neo4j contains wiki docs with case sensititive namespaces
                 *  then migrated data will also contain case sensitive namespaces in 
                 *  wikidoc and resolve_node tables.
                 */
                namespaces.add(instance);
            }
        }
        catch(Exception e)
        {
            Log.log.error("error in sql " + sql, e);
        }
                
        Log.log.debug("Total # of Namespaces : " + namespaces.size());

        if(!namespaces.isEmpty())
        {
            //Create task for processing
            Collection<Callable<Collection<String>>> migrateNamespaceTasks = new ArrayList<Callable<Collection<String>>>();
            for (String namespace : namespaces)
            {
                migrateNamespaceTasks.add(new MigrateNamespace(namespace));
            }
            
            try
            {                
                pCompSysIds = executeTasks(migrateNamespaceTasks);
                
                Log.log.debug("Total # of Namespaces Migrated: " + pCompSysIds.size());
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        
        Log.log.debug("*** END OF UPDATE NAMESPACES TO GRAPH  *** " + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
        
        return pCompSysIds;
    }
    
    
    private Collection<String> migrateActiontasks()
    {
        Log.log.debug("*** START UPDATE AT TO GRAPH   ***");
        long startTime = System.currentTimeMillis();
        Collection<String> pCompSysIds = new ArrayList<String>();

        List<String> sysIds = new ArrayList<String>(GeneralHibernateUtil.getSysIdsFor("ResolveActionTask", null, "system"));
        if (sysIds != null && sysIds.size() > 0)
        {
            Log.log.debug("Total # of Action Tasks : " + sysIds.size());

            //Create task for processing
            Collection<Callable<Collection<String>>> migrateActiontaskTasks = new ArrayList<Callable<Collection<String>>>();
            
            for (String sysId : sysIds)
            {
                migrateActiontaskTasks.add(new MigrateActiontask(sysId));
            }//end of for
            
            try
            {                
                pCompSysIds = executeTasks(migrateActiontaskTasks);
                
                Log.log.debug("Total # of Action Tasks Migrated: " + pCompSysIds.size());
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        Log.log.debug("*** END OF UPDATE AT TO GRAPH  *** " + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
        
        return pCompSysIds;
    }
    
    private Collection<String> migrateUsers()
    {
        Log.log.debug("*** START UPDATE USERS TO GRAPH   ***");
        long startTime = System.currentTimeMillis();
        Collection<String> pCompSysIds = new ArrayList<String>();

        List<String> sysIds = new ArrayList<String>(GeneralHibernateUtil.getSysIdsFor("Users", null, "system"));
        if (sysIds != null && sysIds.size() > 0)
        {
            Log.log.debug("Total # of recs : " + sysIds.size());
            
            //Create task for processing
            Collection<Callable<Collection<String>>> migrateUserskTasks = new ArrayList<Callable<Collection<String>>>();
            
            for (String sysId : sysIds)
            {
                migrateUserskTasks.add(new MigrateUser(sysId));
            }//end of for
            
            try
            {                
                pCompSysIds = executeTasks(migrateUserskTasks);
                
                Log.log.debug("Total # of Users Migrated: " + pCompSysIds.size());
            }
            catch (Exception e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }

        Log.log.debug("*** END OF UPDATE AT TO GRAPH  *** " + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
        
        return pCompSysIds;
    }
    
    protected void copySocialDToToResolveNode(ResolveNodeVO nodeVO, SocialDTO socialDTO)
    {
        nodeVO.setSysCreatedOn(socialDTO.getSysCreatedOn());
        nodeVO.setSysCreatedBy(socialDTO.getSysCreatedBy());
        nodeVO.setSysUpdatedOn(socialDTO.getSysUpdatedOn());
        nodeVO.setSysUpdatedBy(socialDTO.getSysUpdatedBy());
        nodeVO.setSysModCount(socialDTO.getSys_mod_count());
        nodeVO.setSysOrg(socialDTO.getSysOrg());
    }
    
    @SuppressWarnings("unchecked")
    private Collection<String> migrateProcesses()
    {
        Log.log.debug("*** START UPDATE PROCESS TO GRAPH   ***");
        long startTime = System.currentTimeMillis();
        Collection<String> pCompSysIds = new ArrayList<String>();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("social_process"); 
        query.setStart(0);
        query.setPage(1);
        
        try
        {
            Map<String, Object> data = ServiceHibernate.getSocialComponents(query, "admin");
            List<SocialProcessDTO> list = (List<SocialProcessDTO>) data.get("DATA");
            if(list != null && list.size() > 0)
            {
                Log.log.debug("Total # of Processes : " + list.size());

                //Create task for processing
                Collection<Callable<Collection<String>>> migrateProcessTasks = new ArrayList<Callable<Collection<String>>>();
                
                for(final SocialProcessDTO dto : list)
                {
                    //migrateProcessTasks.add(new MigrateProcessTask(dto));
                    migrateProcessTasks.add(new Callable<Collection<String>>()
                        {
                            public Collection<String> call() throws Exception
                            {
                                ResolveNodeVO node = new ResolveNodeVO();
                                
                                copySocialDToToResolveNode(node, dto);
                                
                                node.setUCompName(dto.getU_display_name());
                                node.setUCompSysId(dto.getSys_id());
                                node.setUType(NodeType.PROCESS);
                                node.setUMarkDeleted(false);
                                node.setULock(false);
                                node.setUPinned(false);
//                                node.setProperties(props);

                                node.setUReadRoles(SocialUtil.convertRolesToSocialRolesString(dto.getU_read_roles() + "," + dto.getU_edit_roles()));
                                node.setUEditRoles(SocialUtil.convertRolesToSocialRolesString(dto.getU_edit_roles()));
                                node.setUPostRoles(SocialUtil.convertRolesToSocialRolesString(dto.getU_post_roles() + "," + dto.getU_edit_roles()));

                                //persist
                                ResolveNodeVO pnode = persistNode(node);

                                Log.log.trace(Thread.currentThread().getName() + " : Migrated Process:" + node.getUCompName());
                                
                                Collection<String> pCompSysIds = new ArrayList<String>();
                                pCompSysIds.add(pnode.getUCompSysId());
                                
                                return pCompSysIds;
                            }
                        });
                }
                
                try
                {                
                    pCompSysIds = executeTasks(migrateProcessTasks);
                    
                    Log.log.debug("Total # of Processes Migrated: " + pCompSysIds.size());
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in migrating Process:", e);
        }
        
        Log.log.debug("*** END OF UPDATE OF PROCESS TO GRAPH  *** " + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
        
        return pCompSysIds;
    }
    
    @SuppressWarnings("unchecked")
    private Collection<String> migrateTeams()
    {
        Log.log.debug("*** START UPDATE TEAMS TO GRAPH   ***");
        long startTime = System.currentTimeMillis();
        Collection<String> pCompSysIds = new ArrayList<String>();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("social_team"); 
        query.setStart(0);
        query.setPage(1);
        
        try
        {
            Map<String, Object> data = ServiceHibernate.getSocialComponents(query, "admin");
            List<SocialTeamDTO> list = (List<SocialTeamDTO>) data.get("DATA");
            if(list != null && list.size() > 0)
            {
                Log.log.debug("Total # of Teams : " + list.size());
                
                //Create task for processing
                Collection<Callable<Collection<String>>> migrateTeamTasks = new ArrayList<Callable<Collection<String>>>();

                for(final SocialTeamDTO dto : list)
                {
                    migrateTeamTasks.add(new Callable<Collection<String>>()
                        {
                            public Collection<String> call() throws Exception
                            {
                                ResolveNodeVO node = new ResolveNodeVO();
                                
                                copySocialDToToResolveNode(node, dto);
                                
                                node.setUCompName(dto.getU_display_name());
                                node.setUCompSysId(dto.getSys_id());
                                node.setUType(NodeType.TEAM);
                                node.setUMarkDeleted(false);
                                node.setULock(false);
                                node.setUPinned(false);
//                                node.setProperties(props);
                                
                                node.setUReadRoles(SocialUtil.convertRolesToSocialRolesString(dto.getU_read_roles() + "," + dto.getU_edit_roles()));
                                node.setUEditRoles(SocialUtil.convertRolesToSocialRolesString(dto.getU_edit_roles()));
                                node.setUPostRoles(SocialUtil.convertRolesToSocialRolesString(dto.getU_post_roles() + "," + dto.getU_edit_roles()));

                                //persist
                                ResolveNodeVO pnode = persistNode(node);
                                
                                Log.log.trace(Thread.currentThread().getName() + " : Migrated Team:" + node.getUCompName());
                                
                                Collection<String> pCompSysIds = new ArrayList<String>();
                                pCompSysIds.add(pnode.getUCompSysId());
                                
                                return pCompSysIds;
                            }                        
                        });
                }
                
                try
                {                
                    pCompSysIds = executeTasks(migrateTeamTasks);
                    
                    Log.log.debug("Total # of Teams Migrated: " + pCompSysIds.size());
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in migrating Team:", e);
        }
        
        Log.log.debug("*** END OF UPDATE OF TEAMS TO GRAPH  *** " + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
        
        return pCompSysIds;
    }
    
    @SuppressWarnings("unchecked")
    private Collection<String> migrateForums()
    {
        Log.log.debug("*** START UPDATE FORUMS TO GRAPH   ***");
        long startTime = System.currentTimeMillis();
        Collection<String> pCompSysIds = new ArrayList<String>();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("social_forum"); 
        query.setStart(0);
        query.setPage(1);
        
        try
        {
            Map<String, Object> data = ServiceHibernate.getSocialComponents(query, "admin");
            List<SocialForumDTO> list = (List<SocialForumDTO>) data.get("DATA");
            if(list != null && list.size() > 0)
            {
                Log.log.debug("Total # of Forums : " + list.size());
                
                //Create task for processing
                Collection<Callable<Collection<String>>> migrateForumTasks = new ArrayList<Callable<Collection<String>>>();
                
                for(final SocialForumDTO dto : list)
                {
                    migrateForumTasks.add(new Callable<Collection<String>>()
                        {

                            public Collection<String> call() throws Exception
                            {
                                ResolveNodeVO node = new ResolveNodeVO();
                                
                                copySocialDToToResolveNode(node, dto);
                                
                                node.setUCompName(dto.getU_display_name());
                                node.setUCompSysId(dto.getSys_id());
                                node.setUType(NodeType.FORUM);
                                node.setUMarkDeleted(false);
                                node.setULock(false);
                                node.setUPinned(false);
//                                node.setProperties(props);

                                node.setUReadRoles(SocialUtil.convertRolesToSocialRolesString(dto.getU_read_roles() + "," + dto.getU_edit_roles()));
                                node.setUEditRoles(SocialUtil.convertRolesToSocialRolesString(dto.getU_edit_roles()));
                                node.setUPostRoles(SocialUtil.convertRolesToSocialRolesString(dto.getU_post_roles() + "," + dto.getU_edit_roles()));

                                //persist
                                ResolveNodeVO pnode = persistNode(node);
                                
                                Log.log.trace(Thread.currentThread().getName() + " : Migrated Forum:" + node.getUCompName());
                                
                                Collection<String> pCompSysIds = new ArrayList<String>();
                                pCompSysIds.add(pnode.getUCompSysId());
                                
                                return pCompSysIds;
                            }                    
                        });
                }
                
                try
                {                
                    pCompSysIds = executeTasks(migrateForumTasks);
                    
                    Log.log.debug("Total # of Forums Migrated: " + pCompSysIds.size());
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in migrating Forums:", e);
        }
        
        Log.log.debug("*** END OF UPDATE OF FORUMS TO GRAPH  *** " + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
        
        return pCompSysIds;
    }
    
    @SuppressWarnings("unchecked")
    private Collection<String> migrateRss()
    {
        Log.log.debug("*** START UPDATE RSS TO GRAPH   ***");
        long startTime = System.currentTimeMillis();
        Collection<String> pCompSysIds = new ArrayList<String>();
        
        QueryDTO query = new QueryDTO();
        query.setModelName("rss_subscription"); 
        query.setStart(0);
        query.setPage(1);
        
        try
        {
            Map<String, Object> data = ServiceHibernate.getSocialComponents(query, "admin");
            List<SocialRssDTO> list = (List<SocialRssDTO>) data.get("DATA");
            if(list != null && list.size() > 0)
            {
                Log.log.debug("Total # of RSSs : " + list.size());

                //Create task for processing
                Collection<Callable<Collection<String>>> migrateRSSTasks = new ArrayList<Callable<Collection<String>>>();
                
                for(final SocialRssDTO dto : list)
                {
                    migrateRSSTasks.add(new Callable<Collection<String>>()
                        {

                            public Collection<String> call() throws Exception
                            {
                                ResolveNodeVO node = new ResolveNodeVO();
                                
                                copySocialDToToResolveNode(node, dto);
                                
                                node.setUCompName(dto.getU_display_name());
                                node.setUCompSysId(dto.getSys_id());
                                node.setUType(NodeType.RSS);
                                node.setUMarkDeleted(false);
                                node.setULock(false);
                                node.setUPinned(false);
//                                node.setProperties(props);

                                node.setUReadRoles(SocialUtil.convertRolesToSocialRolesString(dto.getU_read_roles() + "," + dto.getU_edit_roles()));
                                node.setUEditRoles(SocialUtil.convertRolesToSocialRolesString(dto.getU_edit_roles()));
                                node.setUPostRoles(SocialUtil.convertRolesToSocialRolesString(dto.getU_post_roles() + "," + dto.getU_edit_roles()));
                                
                                //persist
                                ResolveNodeVO pnode = persistNode(node);
                                
                                Log.log.trace(Thread.currentThread().getName() + " : Migrated RSS:" + node.getUCompName());
                                
                                Collection<String> pCompSysIds = new ArrayList<String>();
                                pCompSysIds.add(pnode.getUCompSysId());
                                
                                return pCompSysIds;
                            }
                        });
                }
                
                try
                {                
                    pCompSysIds = executeTasks(migrateRSSTasks);
                    
                    Log.log.debug("Total # of RSSs Migrated: " + pCompSysIds.size());
                }
                catch (Exception e)
                {
                    Log.log.error(e.getMessage(), e);
                }
            }
        }
        catch (Exception e)
        {
            Log.log.error("Error in migrating RSS:", e);
        }
        
        Log.log.debug("*** END OF UPDATE OF RSS TO GRAPH  *** " + ((System.currentTimeMillis() - startTime) * 0.001) + " secs");
        
        return pCompSysIds;
    }

    protected ResolveNodeVO persistNode(ResolveNodeVO node)
    {
        ResolveNodeVO pnode = null;
        
        try
        {
            pnode = GraphUtil.persistNode(node, "admin");
        }
        catch (Exception e)
        {
           Log.log.error(Thread.currentThread().getName() + " : Error persisting node:" + node.getUCompName() + ": Msg:" + e.getMessage());
        }
        
        return pnode;
    }

}
