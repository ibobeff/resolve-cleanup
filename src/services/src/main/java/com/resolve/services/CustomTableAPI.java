/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.resolve.persistence.util.EntityUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.StringUtils;

public class CustomTableAPI
{
    public static List<Map<String, Object>> getRecordData(QueryDTO query) throws Exception
    {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

        if (StringUtils.isNotBlank(query.getTableName()) || StringUtils.isNotBlank(query.getModelName()) || StringUtils.isNotBlank(query.getSqlQuery()) || StringUtils.isNotBlank(query.getHql()))
        {
            if (query.isUseSql())
            {
                String sql = query.getSelectSQLNonParameterizedSort();
                result = EntityUtil.executeSQLSelect(sql, query.getStart(), query.getLimit());
            }
            else
            {
                result = EntityUtil.executeHQLSelect(query, query.getStart(), query.getLimit());
            }
        }

        return result;
    }
}
