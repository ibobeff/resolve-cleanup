package com.resolve.services.rb.util;

import java.util.ArrayList;
import java.util.List;

/******
 * This class is mainly to organize the nodes.
 * A tree structure is not necessary but it allows me to implement methods that 
 * are easier to use than traversing back and forth through a list
 * @author jesse.puente
 * @param <T>
 */
public class ABMarkupTree<T>
{

    
    private Node<T> root;
    
    public ABMarkupTree(T rootData)
    {
        root = new Node<T>();
        root.data = rootData;
        root.children = new ArrayList<Node<T>>();
    }
    
    public static class Node<T>
    {
        public static final int CAPTURE = 0;
        public static int LITERAL = 1;
        public static int TYPE = 2;
        
        private int presidence = LITERAL;
        private T data;
        private Node<T> parent;
        private List<Node<T>> children;
    }
}
