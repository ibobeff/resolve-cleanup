/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.social.notification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.resolve.dto.SocialComponentType;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.services.ServiceSocial;
import com.resolve.services.graph.social.SocialCompConversionUtil;
import com.resolve.services.graph.social.model.RSComponent;
import com.resolve.services.graph.social.model.component.ActionTask;
import com.resolve.services.graph.social.model.component.Document;
import com.resolve.services.graph.social.model.component.Rss;
import com.resolve.services.graph.social.model.component.Runbook;
import com.resolve.services.graph.social.model.component.User;
import com.resolve.services.graph.social.model.component.container.Forum;
import com.resolve.services.graph.social.model.component.container.Process;
import com.resolve.services.graph.social.model.component.container.Team;
import com.resolve.services.graph.social.model.message.SubmitNotification;
import com.resolve.services.graph.social.model.message.UserGlobalNotificationContainerType;
import com.resolve.services.hibernate.util.SocialUtil;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

/**
 * This is a convenience class that helps generating/sending the social
 * notification.
 * 
 */
public class NotificationHelper
{
    public final static Map<UserGlobalNotificationContainerType, String> messages;

    static
    {
        messages = new HashMap<UserGlobalNotificationContainerType, String>();

        messages.put(UserGlobalNotificationContainerType.DOCUMENT_CREATE, "Wiki %s has been created by %s.");
        messages.put(UserGlobalNotificationContainerType.DOCUMENT_UPDATE, "Wiki %s has been updated by %s.");
        messages.put(UserGlobalNotificationContainerType.DOCUMENT_DELETE, "Wiki %s has been marked for deletion by %s.");
        messages.put(UserGlobalNotificationContainerType.DOCUMENT_UNDELETE, "Wiki %s has been un-deleted by %s.");
        messages.put(UserGlobalNotificationContainerType.DOCUMENT_ACTIVE, "Wiki %s has been made active by %s.");
        messages.put(UserGlobalNotificationContainerType.DOCUMENT_INACTIVE, "Wiki %s has been made inactive by %s.");
        messages.put(UserGlobalNotificationContainerType.DOCUMENT_LOCKED, "Wiki %s has been locked by %s.");
        messages.put(UserGlobalNotificationContainerType.DOCUMENT_UNLOCKED, "Wiki %s has been un-locked by %s.");
        messages.put(UserGlobalNotificationContainerType.DOCUMENT_HIDE, "Wiki %s has been hidden by %s.");
        messages.put(UserGlobalNotificationContainerType.DOCUMENT_UNHIDE, "Wiki %s has been un-hide by %s.");
        messages.put(UserGlobalNotificationContainerType.DOCUMENT_PURGED, "Wiki %s has been purged by %s.");
        messages.put(UserGlobalNotificationContainerType.DOCUMENT_COMMIT, "Wiki %s has been committed by %s. ");
        messages.put(UserGlobalNotificationContainerType.DOCUMENT_ADDED_TO_PROCESS, "Wiki %s has been added to process %s by %s.");
        messages.put(UserGlobalNotificationContainerType.DOCUMENT_REMOVED_FROM_PROCESS, "Wiki %s has been removed from process %s by %s.");

        messages.put(UserGlobalNotificationContainerType.ACTIONTASK_CREATE, "ActionTask %s has been created by %s.");
        messages.put(UserGlobalNotificationContainerType.ACTIONTASK_UPDATE, "ActionTask %s has been updated by %s.");
        messages.put(UserGlobalNotificationContainerType.ACTIONTASK_PURGED, "ActionTask %s has been purged by %s. ");
        messages.put(UserGlobalNotificationContainerType.ACTIONTASK_ADDED_TO_PROCESS, "ActionTask %s has been added to process %s by %s.");
        messages.put(UserGlobalNotificationContainerType.ACTIONTASK_REMOVED_FROM_PROCESS, "ActionTask %s has been removed from process %s by %s.");

        messages.put(UserGlobalNotificationContainerType.RSS_CREATE, "RSS %s has been created by %s.");
        messages.put(UserGlobalNotificationContainerType.RSS_UPDATE, "RSS %s has been updated by %s.");
        messages.put(UserGlobalNotificationContainerType.RSS_PURGED, "RSS %s has been purged by %s.");
        messages.put(UserGlobalNotificationContainerType.RSS_ADDED_TO_PROCESS, "RSS %s has been added to process %s by %s.");
        messages.put(UserGlobalNotificationContainerType.RSS_REMOVED_FROM_PROCESS, "RSS %s has been removed from process %s by %s.");

        messages.put(UserGlobalNotificationContainerType.FORUM_CREATE, "Forum %s has been created by %s.");
        messages.put(UserGlobalNotificationContainerType.FORUM_UPDATE, "Forum %s has been updated by %s.");
        messages.put(UserGlobalNotificationContainerType.FORUM_PURGED, "Forum %s has been purged by %s.");
        messages.put(UserGlobalNotificationContainerType.FORUM_ADDED_TO_PROCESS, "Forum %s has been added to process %s by %s.");
        messages.put(UserGlobalNotificationContainerType.FORUM_REMOVED_FROM_PROCESS, "Forum %s has been removed from process %s by %s.");
        messages.put(UserGlobalNotificationContainerType.FORUM_USER_ADDED, "Forum %s has added User %s by %s.");
        messages.put(UserGlobalNotificationContainerType.FORUM_USER_REMOVED, "Forum %s has removed User %s by %s.");

        messages.put(UserGlobalNotificationContainerType.TEAM_CREATE, "Team %s has been created by %s.");
        messages.put(UserGlobalNotificationContainerType.TEAM_UPDATE, "Team %s has been updated by %s.");
        messages.put(UserGlobalNotificationContainerType.TEAM_PURGED, "Team %s has been purged by %s.");
        messages.put(UserGlobalNotificationContainerType.TEAM_ADDED_TO_PROCESS, "Team %s has been added to process %s by %s.");
        messages.put(UserGlobalNotificationContainerType.TEAM_REMOVED_FROM_PROCESS, "Team %s has been removed from process %s by %s.");
        messages.put(UserGlobalNotificationContainerType.TEAM_ADDED_TO_TEAM, "Team %s has been added to team %s by %s.");
        messages.put(UserGlobalNotificationContainerType.TEAM_REMOVED_FROM_TEAM, "Team %s has been removed from team %s by %s.");
        messages.put(UserGlobalNotificationContainerType.TEAM_USER_ADDED, "Team %s has added User %s by %s.");
//        messages.put(UserGlobalNotificationContainerType.TEAM_USER_ADDED, "User %s has been added to Team %s by %s.");
        messages.put(UserGlobalNotificationContainerType.TEAM_USER_REMOVED, "Team %s has removed User %s.");
//        messages.put(UserGlobalNotificationContainerType.TEAM_USER_REMOVED, "User %s has left Team %s by %s.");

        messages.put(UserGlobalNotificationContainerType.PROCESS_CREATE, "Process %s has been created by %s.");
        messages.put(UserGlobalNotificationContainerType.PROCESS_UPDATE, "Process %s has been updated by %s.");
        messages.put(UserGlobalNotificationContainerType.PROCESS_PURGED, "Process %s has been purged by %s.");
        messages.put(UserGlobalNotificationContainerType.PROCESS_DOCUMENT_ADDED, "Process %s has added Document %s by %s.");
        messages.put(UserGlobalNotificationContainerType.PROCESS_DOCUMENT_REMOVED, "Process %s has removed Document %s by %s.");
        messages.put(UserGlobalNotificationContainerType.PROCESS_ACTIONTASK_ADDED, "Process %s has added ActionTask %s by %s.");
        messages.put(UserGlobalNotificationContainerType.PROCESS_ACTIONTASK_REMOVED, "Process %s has removed ActionTask %s by %s.");
        messages.put(UserGlobalNotificationContainerType.PROCESS_RSS_ADDED, "Process %s has added RSS %s by %s.");
        messages.put(UserGlobalNotificationContainerType.PROCESS_RSS_REMOVED, "Process %s has removed RSS %s by %s.");
        messages.put(UserGlobalNotificationContainerType.PROCESS_FORUM_ADDED, "Process %s has added Forum %s by %s.");
        messages.put(UserGlobalNotificationContainerType.PROCESS_FORUM_REMOVED, "Process %s has removed Forum %s by %s.");
        messages.put(UserGlobalNotificationContainerType.PROCESS_TEAM_ADDED, "Process %s has added Team %s by %s.");
        messages.put(UserGlobalNotificationContainerType.PROCESS_TEAM_REMOVED, "Process %s has removed Team %s by %s.");
        messages.put(UserGlobalNotificationContainerType.PROCESS_USER_ADDED, "Process %s has added User %s by %s.");
        messages.put(UserGlobalNotificationContainerType.PROCESS_USER_REMOVED, "Process %s has removed User %s by %s.");

        messages.put(UserGlobalNotificationContainerType.USER_ADDED_TO_PROCESS, "User %s has been added to process %s by %s.");
        messages.put(UserGlobalNotificationContainerType.USER_ADDED_TO_TEAM, "User %s has been added to team %s by %s.");
        messages.put(UserGlobalNotificationContainerType.USER_REMOVED_FROM_PROCESS, "User %s has been removed from process %s by %s.");
        messages.put(UserGlobalNotificationContainerType.USER_REMOVED_FROM_TEAM, "User %s has been removed from team %s by %s.");
        messages.put(UserGlobalNotificationContainerType.USER_ADDED_TO_FORUM, "User %s has been added to forum %s by %s.");
        messages.put(UserGlobalNotificationContainerType.USER_REMOVED_FROM_FORUM, "User %s has been removed from forum %s by %s.");
        
        messages.put(UserGlobalNotificationContainerType.USER_FOLLOW_ME, "User %s is now following %s.");
        messages.put(UserGlobalNotificationContainerType.USER_UNFOLLOW_ME, "User %s has stopped following %s.");
        messages.put(UserGlobalNotificationContainerType.USER_PROFILE_CHANGE, "User profile of %s has been changed by %s.");

    }

    /**
     * The read roles must be space separated, however sometime they're coming
     * as comma separated, hence this conversion.
     * 
     * @param readRoles
     * @return
     */
    private static String spacifyReadRoles(String readRoles)
    {
        String result = readRoles;
        if (StringUtils.isNotEmpty(readRoles) && readRoles.indexOf(',') >= 0)
        {
            result = readRoles.replaceAll(",", " ");
        }
        return result;
    }

    public static void submitNotification(SubmitNotification notification, boolean isAsync)
    {
        if (notification != null)
        {
            SocialUtil.submitNotification(notification, isAsync);
        }
    }

    public static void submitNotifications(List<SubmitNotification> notifications, boolean isAsync)
    {
        if (notifications != null && notifications.size() > 0)
        {
            SocialUtil.submitNotifications(notifications, isAsync);
        }
    }

    public static SubmitNotification getSocialNotification(String componentSysId, String componentDisplayName, String componentReadRoles, String componentWriteRoles,SocialComponentType componentType, UserGlobalNotificationContainerType notificationType, String post, String username, boolean doSubmit, boolean isAsync)
    {
        SubmitNotification submitNotification = new SubmitNotification();

        submitNotification.setEventType(notificationType);
        submitNotification.setCompType(componentType);
        submitNotification.setContent(post);

        // for notification
        submitNotification.setCompSysId(componentSysId);
        submitNotification.setCompDisplayName(componentDisplayName);
        submitNotification.setCompReadRoles(spacifyReadRoles(componentReadRoles));
        submitNotification.setCompEditRoles(spacifyReadRoles(componentWriteRoles));
        submitNotification.setUsername(username);

        if (Log.log.isTraceEnabled())
        {
            Log.log.trace("Notification for " + componentType);
            Log.log.trace(submitNotification.toString());
        }

        if (StringUtils.isEmpty(componentReadRoles))
        {
            throw new RuntimeException("Read roles must be provided for social notification to work.");
        }
        if (StringUtils.isEmpty(username))
        {
            throw new RuntimeException("Username must be provided for social notification to work.");
        }

        //TODO temp hack until we officially have UNDELETE option in notification
        if(submitNotification.getEventType().equals(UserGlobalNotificationContainerType.DOCUMENT_UNDELETE))
        {
            submitNotification.setEventType(UserGlobalNotificationContainerType.DOCUMENT_DELETE);
        }

        // If need to send then send it.
        if (doSubmit)
        {
            SocialUtil.submitNotification(submitNotification, isAsync);
        }

        return submitNotification;
    }
    
    private static SubmitNotification getWikiDocumentNotification(WikiDocument doc, SocialComponentType componentType, UserGlobalNotificationContainerType notificationType, String post, String username, boolean doSubmit, boolean isAsync)
    {
        if (doc != null && doc.getAccessRights() != null)
        {
            return getSocialNotification(doc.getSys_id(), doc.getUFullname(), doc.getAccessRights().getUReadAccess(), doc.getAccessRights().getUWriteAccess(), componentType, notificationType, post, username, doSubmit, isAsync);
        }
        else
        {
            throw new RuntimeException("AccessRights must be provided for the WikiDocument.");
        }
    }

    private static SubmitNotification getWikiDocumentNotification(WikiDocument doc, SocialComponentType componentType, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        return getWikiDocumentNotification(doc, componentType, notificationType, String.format(messages.get(notificationType), doc.getUFullname(), username), username, doSubmit, isAsync);
    }

    // Runbook notifications
    public static SubmitNotification getRunbookNotification(WikiDocument doc, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        return getWikiDocumentNotification(doc, SocialComponentType.RUNBOOKS, notificationType, username, doSubmit, isAsync);
    }

    public static SubmitNotification getRunbookNotification(Runbook doc, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        String post = String.format(messages.get(notificationType), doc.getDisplayName(), username);
        return getSocialNotification(doc.getSys_id(), doc.getDisplayName(), doc.getRoles(), doc.getEditRoles(), SocialComponentType.RUNBOOKS, notificationType, post, username, doSubmit, isAsync);
    }

    public static SubmitNotification getRunbookNotification(Runbook doc, Process process, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        String post = String.format(messages.get(notificationType), doc.getDisplayName(), process.getDisplayName(), username);
        return getSocialNotification(doc.getSys_id(), doc.getDisplayName(), doc.getRoles(), doc.getEditRoles(), SocialComponentType.RUNBOOKS, notificationType, post, username, doSubmit, isAsync);
    }

    // Document notifications
    public static SubmitNotification getDocumentNotification(WikiDocument doc, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        return getWikiDocumentNotification(doc, SocialComponentType.DOCUMENTS, notificationType, username, doSubmit, isAsync);
    }

    public static SubmitNotification getDocumentNotification(String docId, String processName, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        Document doc = SocialCompConversionUtil.createDocument(docId);
        String post = String.format(messages.get(notificationType), doc.getDisplayName(), processName, username);
        return getSocialNotification(doc.getSys_id(), doc.getDisplayName(), doc.getRoles(), doc.getEditRoles(), SocialComponentType.DOCUMENTS, notificationType, post, username, doSubmit, isAsync);
    }

    // ActionTask notifications
    public static SubmitNotification getActionTaskNotification(ResolveActionTask task, UserGlobalNotificationContainerType notificationType, String post, String username, boolean doSubmit, boolean isAsync)
    {
        if (task != null)
        {
            String readAccess = "";
            String writeAccess = "";
            if (task.getAccessRights() != null)
            {
                readAccess = task.getAccessRights().getUReadAccess();
                writeAccess = task.getAccessRights().getUWriteAccess();
            }
            else
            {
                readAccess = writeAccess = task.getURoles();
            }
            return getSocialNotification(task.getSys_id(), task.getUFullName(), readAccess, writeAccess, SocialComponentType.ACTIONTASKS, notificationType, post, username, doSubmit, isAsync);
        }
        else
        {
            throw new RuntimeException("AccessRights must be provided for the ActionTask.");
        }
    }

    public static SubmitNotification getActionTaskNotification(ResolveActionTask task, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        return getActionTaskNotification(task, notificationType, String.format(messages.get(notificationType), task.getUFullName(), username), username, doSubmit, isAsync);
    }

    public static SubmitNotification getActionTaskNotification(String taskSysId, String parentCompName, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        ActionTask task = SocialCompConversionUtil.createActionTask(taskSysId);
        String post = String.format(messages.get(notificationType), task.getDisplayName(), parentCompName, username);
        return getSocialNotification(task.getSys_id(), task.getDisplayName(), task.getRoles(), task.getEditRoles(), SocialComponentType.ACTIONTASKS, notificationType, post, username, doSubmit, isAsync);
    }

    // RSS notifications
    public static SubmitNotification getRssNotification(String rssSysId, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        Rss rss = SocialCompConversionUtil.createRss(rssSysId);
        String post = String.format(messages.get(notificationType), rss.getDisplayName(), username);
        return getSocialNotification(rss.getSys_id(), rss.getDisplayName(), rss.getRoles(), rss.getEditRoles(), SocialComponentType.RSS, notificationType, post, username, doSubmit, isAsync);
    }

    public static SubmitNotification getRssNotification(String rssSysId, String parentCompName, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        Rss rss = SocialCompConversionUtil.createRss(rssSysId);
        String post = String.format(messages.get(notificationType), rss.getDisplayName(), parentCompName, username);
        return getSocialNotification(rss.getSys_id(), rss.getDisplayName(), rss.getRoles(), rss.getEditRoles(), SocialComponentType.RSS, notificationType, post, username, doSubmit, isAsync);
    }

    // Forum notifications
    public static SubmitNotification getForumNotification(String forumId, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        Forum forum = SocialCompConversionUtil.createForum(forumId);
        String post = String.format(messages.get(notificationType), forum.getDisplayName(), username);
        return getSocialNotification(forum.getSys_id(), forum.getDisplayName(), forum.getRoles(), forum.getEditRoles(), SocialComponentType.TEAMS, notificationType, post, username, doSubmit, isAsync);
    }
    
    public static SubmitNotification getForumNotification(Forum forum, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        String post = String.format(messages.get(notificationType), forum.getDisplayName(), username);
        return getSocialNotification(forum.getSys_id(), forum.getDisplayName(), forum.getRoles(), forum.getEditRoles(), SocialComponentType.FORUMS, notificationType, post, username, doSubmit, isAsync);
    }

    public static SubmitNotification getForumNotification(String forumId, String processName, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        Forum forum = SocialCompConversionUtil.createForum(forumId);
        String post = String.format(messages.get(notificationType), forum.getDisplayName(), processName, username);
        return getSocialNotification(forum.getSys_id(), forum.getDisplayName(), forum.getRoles(), forum.getEditRoles(), SocialComponentType.FORUMS, notificationType, post, username, doSubmit, isAsync);
    }

    public static SubmitNotification getForumNotification(Forum forum, User user, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        String post = String.format(messages.get(notificationType),forum.getDisplayName(), user.getDisplayName(), username);
        return getSocialNotification(forum.getSys_id(), forum.getDisplayName(), forum.getRoles(), forum.getEditRoles(), SocialComponentType.FORUMS, notificationType, post, username, doSubmit, isAsync);
    }

    // Team notifications
    public static SubmitNotification getTeamNotification(String teamId, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        Team team = SocialCompConversionUtil.createTeam(teamId);
        String post = String.format(messages.get(notificationType), team.getDisplayName(), username);
        return getSocialNotification(team.getSys_id(), team.getDisplayName(), team.getRoles(), team.getEditRoles(), SocialComponentType.TEAMS, notificationType, post, username, doSubmit, isAsync);
    }

    public static SubmitNotification getTeamNotification(String teamId, Team parentTeam, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        Team team = SocialCompConversionUtil.createTeam(teamId);
        String post = String.format(messages.get(notificationType), team.getDisplayName(), parentTeam.getDisplayName(), username);
        return getSocialNotification(team.getSys_id(), team.getDisplayName(), team.getRoles(), team.getEditRoles(), SocialComponentType.TEAMS, notificationType, post, username, doSubmit, isAsync);
    }

    public static SubmitNotification getTeamNotification(String teamSysId, String parentCompName, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        Team team = SocialCompConversionUtil.createTeam(teamSysId);
        String post = String.format(messages.get(notificationType), team.getDisplayName(), parentCompName, username);
        return getSocialNotification(team.getSys_id(), team.getDisplayName(), team.getRoles(), team.getEditRoles(), SocialComponentType.TEAMS, notificationType, post, username, doSubmit, isAsync);
    }

    public static SubmitNotification getTeamNotification(Team team, User user, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        user = SocialCompConversionUtil.getSocialUserById(user.getSys_id());
        String post = String.format(messages.get(notificationType), team.getDisplayName(), user.getDisplayName(), username);
        return getSocialNotification(team.getSys_id(), team.getDisplayName(), team.getRoles(), team.getEditRoles(), SocialComponentType.TEAMS, notificationType, post, username, doSubmit, isAsync);
    }
    
    public static SubmitNotification getTeamNotificationUserLeftTeam(Team team, User user, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        user = SocialCompConversionUtil.getSocialUserById(user.getSys_id());
        String post = String.format(messages.get(notificationType), user.getDisplayName(), team.getDisplayName(), username);
        return getSocialNotification(team.getSys_id(), team.getDisplayName(), team.getRoles(), team.getEditRoles(), SocialComponentType.TEAMS, notificationType, post, username, doSubmit, isAsync);
    }

    // User notifications
    public static SubmitNotification getUserNotification(User user, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        user = SocialCompConversionUtil.getSocialUserById(user.getSys_id());
        String post = String.format(messages.get(notificationType), user.getDisplayName(), username);
        return getSocialNotification(user.getSys_id(), user.getDisplayName(), user.getRoles(), user.getRoles(),SocialComponentType.USERS, notificationType, post, username, doSubmit, isAsync);
    }

    public static SubmitNotification getUserFollowNotification(String userSysId, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        User user = SocialCompConversionUtil.getSocialUserById(userSysId);
        String post = String.format(messages.get(notificationType), username, user.getName());
        return getSocialNotification(user.getSys_id(), user.getDisplayName(), user.getRoles(), user.getRoles(), SocialComponentType.USERS, notificationType, post, username, doSubmit, isAsync);
    }

    public static SubmitNotification getUserNotification(String userSysId, String compName, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        User user = SocialCompConversionUtil.getSocialUserById(userSysId);
        String post = String.format(messages.get(notificationType), user.getName(), compName, username);
        return getSocialNotification(user.getSys_id(), user.getDisplayName(), user.getRoles(), user.getRoles(), SocialComponentType.USERS, notificationType, post, username, doSubmit, isAsync);
    }

    // Process notifications
    public static SubmitNotification getProcessNotification(String processId, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        Process process = SocialCompConversionUtil.createProcess(processId);
        String post = String.format(messages.get(notificationType), process.getDisplayName(), username);
        return getSocialNotification(process.getSys_id(), process.getDisplayName(), process.getRoles(), process.getEditRoles(), SocialComponentType.PROCESS, notificationType, post, username, doSubmit, isAsync);
    }
    public static SubmitNotification getProcessNotification(String processSysId, String socialCompName, UserGlobalNotificationContainerType notificationType, String username, boolean doSubmit, boolean isAsync)
    {
        Process process = SocialCompConversionUtil.createProcess(processSysId);
        String post = String.format(messages.get(notificationType), process.getDisplayName(), socialCompName, username);
        return getSocialNotification(process.getSys_id(), process.getDisplayName(), process.getRoles(), process.getEditRoles(), SocialComponentType.PROCESS, notificationType, post, username, doSubmit, isAsync);
    }
}
