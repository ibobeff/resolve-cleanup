/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;

import com.resolve.persistence.model.ResolveActionInvoc;
import com.resolve.persistence.model.ResolvePreprocess;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.hibernate.actiontask.FindPreprocessor;
import com.resolve.services.hibernate.vo.ResolveActionInvocVO;
import com.resolve.services.hibernate.vo.ResolveActionTaskVO;
import com.resolve.services.hibernate.vo.ResolvePreprocessVO;
import com.resolve.services.interfaces.VO;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.SQLUtils;
import com.resolve.util.StringUtils;


public class PreprocessorUtil
{
    public static List<ResolvePreprocessVO> getResolvePreprocess(QueryDTO query, String username)
    {
        List<ResolvePreprocessVO> result = new ArrayList<ResolvePreprocessVO>();
        
        int limit = query.getLimit();
        int offset = query.getStart();
        
        try
        {
            List<? extends Object> data = GeneralHibernateUtil.executeHQLSelectModel(query, offset, limit, true);
            if(data != null)
            {
                String selectedColumns = query.getSelectColumns();
                if(StringUtils.isNotBlank(selectedColumns))
                {
                    String[] columns = selectedColumns.split(",");
                    
                    //this will be the array of objects
                    for(Object o : data)
                    {
                        Object[] record = (Object[]) o;
                        ResolvePreprocess instance = new ResolvePreprocess();
                        
                        //set the attributes in the object
                        for(int x=0; x < columns.length; x++)
                        {
                            String column = columns[x].trim();
                            Object value = record[x];
                            
                            PropertyUtils.setProperty(instance, column, value);
                        }
                        
                        result.add(instance.doGetVO());
                    }//end of for loop
                }
                else
                {
                    for(Object o : data)
                    {
                        ResolvePreprocess instance = (ResolvePreprocess) o;
                        result.add(instance.doGetVO());
                    }
                }
            }
        }
        catch (Exception t)
        {
            Log.log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
    public static ResolvePreprocessVO saveResolvePreprocess(ResolvePreprocessVO vo, String username) throws Exception
    {
        if(vo != null && vo.validate())
        {
            ResolvePreprocess model = null;
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equals(VO.STRING_DEFAULT))
            {
                model = findResolvePreprocessModel(vo.getSys_id(), null, username);
                if(model == null)
                {
                    throw new Exception("Preprocessor with sysId " + vo.getSys_id() + " does not exist.");
                }
            }
            else
            {
                model = findResolvePreprocessModel(null, vo.getUName(), username);
                if(model != null)
                {
                    throw new Exception("Preprocessor with name " + vo.getUName() + " already exist");
                }
                
                model = new ResolvePreprocess();
            }
            model.applyVOToModel(vo);
            model = SaveUtil.saveResolvePreprocess(model, username);
            
            //submit a request to archive it
            ResolveArchiveUtils.submitArchiveRequest("ResolvePreprocess", "UScript", model.getSys_id(), username);
            
            vo = model.doGetVO();
        }
        
        return vo;
    }
    
    @SuppressWarnings("unchecked")
    public static Set<String> findActiontaskReferencesForPreprocessors(Set<String> preprocessorSysIds, String username)   throws Exception
    {
        Set<String> result = new HashSet<String>();
        
        if(preprocessorSysIds != null && preprocessorSysIds.size() > 0)
        {
            //String qrySysIds = SQLUtils.prepareQueryString(new ArrayList<String>(preprocessorSysIds));
            //String sql = "select UFullName from ResolveActionTask where resolveActionInvoc.sys_id in (select sys_id from ResolveActionInvoc where preprocess.sys_id IN ("+ qrySysIds +"))";
            String sql = "select UFullName from ResolveActionTask where resolveActionInvoc.sys_id in (select sys_id from ResolveActionInvoc where preprocess.sys_id IN (:qrySysIds))";
            
            Map<String, Collection<Object>> queryInParams = new HashMap<String, Collection<Object>>();
            
            queryInParams.put("qrySysIds", new ArrayList<Object>(preprocessorSysIds));
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelectInOnly(sql, queryInParams);
                result.addAll((List<String>) list);
            }
            catch(Exception e)
            {
                Log.log.error("error in sql " + sql, e);
            }
        }
        
        return result;
    }
    
    public static void deleteResolvePreprocessByIds(String[] sysIds, boolean deleteAll, String username) throws Exception
    {

        if (deleteAll)
        {
            GeneralHibernateUtil.purgeDBTable("ResolvePreprocess", username);
            
            //delete all the archives related to this table
            ResolveArchiveUtils.purgeArchive("ResolvePreprocess", "all", username);
        }
        else
        {
            if (sysIds != null)
            {
                //can be used as its 1 table only
            	ServiceHibernate.deleteDBRow(username, "ResolvePreprocess", (List<String>) Arrays.asList(sysIds), false);
                
                //delete all the archives related to these sysIds
                String csvSysIds = StringUtils.convertCollectionToString(Arrays.asList(sysIds).iterator(), ",");
                ResolveArchiveUtils.submitPurgeArchiveRequest("ResolvePreprocess", csvSysIds, username);
            }
        }
    }
    
    public static String getPreprocessorScript(String name, String username) throws Exception
    {
        String result = null;

        if (StringUtils.isNotBlank(name))
        {
            try
            {
                
                ResolvePreprocessVO preprocessor = findResolvePreprocess(null, name, username);
                if(preprocessor != null)
                {
                    result = preprocessor.getUScript();
                    if (StringUtils.isEmpty(result))
                    {
                        throw new Exception("FAILED: Missing script content - preprocessor: " + name);
                    }
                }
                else
                {
                    throw new Exception("FAILED: Unable to find preprocessor: " + name);
                }
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
            }
        }
        return result;
    } // getPreprocessorScript
    
    //The no-cache version of the preprocessor
    public static ResolvePreprocessVO findResolvePreprocessNoCache(String sysId, String name, String username)
    {
        ResolvePreprocessVO result = null;

        try 
        {

          HibernateProxy.setCurrentUser(username);
            result = (ResolvePreprocessVO) HibernateProxy.executeNoCache(() -> {
            	return findResolvePreprocess(sysId, name, username);
            }); 
            
        }
        catch(Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
        return result;
    }
    
    public static ResolvePreprocessVO findResolvePreprocess(String sysId, String name, String username)
    {
        ResolvePreprocessVO result = null;

        try
        {
            FindPreprocessor findPreprocess = new FindPreprocessor(sysId, name, username);
            result = findPreprocess.get();
            
//            preprocess = findResolvePreprocessModel(sysId, name, username);
        }
        catch (Throwable e)
        {
            Log.log.error(e.getMessage(), e);
        }

        return result;
        
    } // findResolvePreprocess
    
    public static Collection<ResolvePreprocessVO> findResolvePreprocessByNames(Set<String> assessorNames, String username)
    {
        Collection<ResolvePreprocessVO> result = new ArrayList<ResolvePreprocessVO>();
        
        if (assessorNames != null && assessorNames.size() > 0)
        {
            //prepare list of actiontasks based on invoc sysIds
//            String sql = "select sys_id,UName,UDescription,sysUpdatedOn from ResolvePreprocessVO where UName IN (" + SQLUtils.prepareQueryString(new ArrayList<String>(assessorNames)) + ")";
            QueryDTO query = new QueryDTO();
            try
            {
                query.setModelName("ResolvePreprocess");
                query.setSelectColumns("sys_id,UName,UDescription,sysUpdatedOn");
                query.setWhereClause("UName IN (" + SQLUtils.prepareQueryString(new ArrayList<String>(assessorNames)) + ")");
                
                result.addAll(getResolvePreprocess(query, username));

                //            List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql);
                //            for (Object o : list)
                //            {
                //                ResolveAssess instance = (ResolveAssess) o;
                //                result.add(instance.doGetVO());
                //            }
            }
            catch (Exception e)
            {
                Log.log.error("error in sql " + query.getSelectSQL(), e);
            }
        }
        return result;
        
    }
    
    private static ResolvePreprocess findResolvePreprocessModel(String sysId, String name, String username)
    {
        ResolvePreprocess result = null;

        if(StringUtils.isNotBlank(sysId))
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	result = (ResolvePreprocess) HibernateProxy.execute(() -> {
    
            		return HibernateUtil.getDAOFactory().getResolvePreprocessDAO().findById(sysId);
    
            	});
            }
            catch (Throwable e)
            {
                Log.log.error(e.getMessage(), e);
                              HibernateUtil.rethrowNestedTransaction(e);
            }
        }
        else if(StringUtils.isNotBlank(name))
        {
            //to make this case-insensitive
            //String sql = "select a from ResolvePreprocess a where LOWER(a.UName) = '" + name.toLowerCase().trim() + "'" ;
            String sql = "select a from ResolvePreprocess a where LOWER(a.UName) = :UName" ;
            
            Map<String, Object> queryParams = new HashMap<String, Object>();
            
            queryParams.put("UName", name.toLowerCase().trim());
            
            try
            {
                List<? extends Object> list = GeneralHibernateUtil.executeHQLSelect(sql, queryParams);
                if(list != null && list.size() > 0)
                {
                    result = (ResolvePreprocess) list.get(0);
                }
            }
            catch (Exception e)
            {
                Log.log.error("Error while executing sql:" + sql, e);
            }
        }
        
        return result;
    }
    
    public static void copyPreprocessor(ResolveActionTaskVO taskVO, ResolvePreprocessVO preprocessVO, String username) {
        
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {
            
	            ResolveActionInvocVO actionInvocVO = taskVO.getResolveActionInvoc();
	            String taskFullName = taskVO.getUFullName();
	            String newName = taskFullName + System.nanoTime();
	            
	            ResolvePreprocessVO preprocess = actionInvocVO.getPreprocess();
	            
	            if(preprocess != null) {
	                preprocess.setSys_id(null);
	                preprocess.setUName(newName);
	                Log.log.debug("new preprocess name = " + newName);
	                
	                ResolvePreprocess model = new ResolvePreprocess();
	                model.applyVOToModel(preprocess);
	                HibernateUtil.getDAOFactory().getResolvePreprocessDAO().persist(model);
	                preprocess = model.doGetVO();
	                Log.log.debug("new preprocess id = " + preprocess.getSys_id());
	                actionInvocVO.setPreprocess(preprocess);
	            }
	    
	            ResolveActionInvoc actionInvoc = new ResolveActionInvoc(actionInvocVO);
	            HibernateUtil.getDAOFactory().getResolveActionInvocDAO().update(actionInvoc);
            
        	});
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
                                           HibernateUtil.rethrowNestedTransaction(e);
        }
    } // copyPreprocessor()
    
    public static void replaceResolvePreprocess(ResolvePreprocessVO toDelete, ResolvePreprocessVO renameTo, String username) {
        
        try
        {
          HibernateProxy.setCurrentUser(username);
            HibernateProxy.execute(() -> {
            	 ResolvePreprocess model = new ResolvePreprocess();
                 model.applyVOToModel(toDelete);
                 HibernateUtil.getDAOFactory().getResolvePreprocessDAO().delete(model);
            });
                
            
            HibernateProxy.execute(() -> {
            	ResolvePreprocess m = new ResolvePreprocess();
                m.applyVOToModel(renameTo);
                HibernateUtil.getDAOFactory().getResolvePreprocessDAO().persist(m);
            });
        } catch(Exception e) {
            Log.log.error(e.getMessage(), e);
                                           HibernateUtil.rethrowNestedTransaction(e);
        }
    } // replaceResolvePreprocess()

} // PreprocessorUtil
