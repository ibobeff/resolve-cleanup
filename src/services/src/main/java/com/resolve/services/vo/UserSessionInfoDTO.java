/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo;

import java.util.List;

public class UserSessionInfoDTO
{
    private Integer activeSessions;
    private Integer maxActiveSessions;
    private Integer maxSessions;
    private Integer timeout;
    
    private List<UserSessionDetailDTO> sessionDetails;

    public Integer getActiveSessions()
    {
        return activeSessions;
    }

    public void setActiveSessions(Integer activeSessions)
    {
        this.activeSessions = activeSessions;
    }

    public Integer getMaxActiveSessions()
    {
        return maxActiveSessions;
    }

    public void setMaxActiveSessions(Integer maxActiveSessions)
    {
        this.maxActiveSessions = maxActiveSessions;
    }

    public Integer getMaxSessions()
    {
        return maxSessions;
    }

    public void setMaxSessions(Integer maxSessions)
    {
        this.maxSessions = maxSessions;
    }

    public Integer getTimeout()
    {
        return timeout;
    }

    public void setTimeout(Integer timeout)
    {
        this.timeout = timeout;
    }

    public List<UserSessionDetailDTO> getSessionDetails()
    {
        return sessionDetails;
    }

    public void setSessionDetails(List<UserSessionDetailDTO> sessionDetails)
    {
        this.sessionDetails = sessionDetails;
    }
    
    
    
}
