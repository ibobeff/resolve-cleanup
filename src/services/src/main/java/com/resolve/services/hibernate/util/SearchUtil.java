package com.resolve.services.hibernate.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.resolve.persistence.model.MetaFormView;
import com.resolve.persistence.model.RecentItem;
import com.resolve.persistence.model.RecentSearch;
import com.resolve.persistence.model.ResolveActionTask;
import com.resolve.persistence.model.ResolveProperties;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.vo.ContentType;
import com.resolve.services.vo.SearchAttributesDTO;
import com.resolve.services.vo.SearchRecordDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class SearchUtil {

	private static final String ITEM_DELIMITER = ",";

	public static final int MAX_RECENT_ITEMS = 15;

	private static String FIND_RECENT_ITEM_BY_USERNAME = "from RecentItem where sysCreatedBy = '%s' order by sysUpdatedOn desc";
	private static String FIND_RECENT_SEARCH_BY_USERNAME = "from RecentSearch where sysCreatedBy = '%s' order by sysUpdatedOn desc";

	@SuppressWarnings("unchecked")
	public static SearchRecordDTO saveRecentlyOpenedItem(SearchRecordDTO searchRecordDTO, String username)
			throws Exception {
		if (StringUtils.isEmpty(username)) {
			username = "system";
		}
		
		String usernameFinal = username;

		try {
        HibernateProxy.setCurrentUser(username);
			HibernateProxy.executeNoCache(() -> {
				RecentItem item = null;

				String findByIdAndTypeQuery = "from RecentItem where itemSysId = '" + searchRecordDTO.getSysId()
						+ "' and itemType = '" + searchRecordDTO.getType() + "' and sysCreatedBy = '" + usernameFinal + "'";

				List<RecentItem> existingItem = HibernateUtil.createQuery(findByIdAndTypeQuery).list();

				if (existingItem.size() == 0) {
					item = new RecentItem();
					item.setSysCreatedBy(usernameFinal);
					item.setItemSysId(searchRecordDTO.getSysId());
					item.setItemType(searchRecordDTO.getType() != null ? searchRecordDTO.getType().name() : null);
					item.setSysCreatedOn(new Date());

					List<RecentItem> recentItems = HibernateUtil
							.createQuery(String.format(FIND_RECENT_ITEM_BY_USERNAME, usernameFinal)).list();
					if (recentItems != null && recentItems.size() >= MAX_RECENT_ITEMS) {
						for (int index = MAX_RECENT_ITEMS - 1; index < recentItems.size(); index++) {
							HibernateUtil.getDAOFactory().getRecentItemDAO().delete(recentItems.get(index));
						}
					}
				} else {
					item = existingItem.get(0);
					item.setSysUpdatedOn(new Date());

				}

				HibernateUtil.getDAOFactory().getRecentItemDAO().insertOrUpdate(item);
			});

		} catch (Throwable e) {
			Log.log.warn(e.getMessage(), e);
			throw new Exception();
		}

		return searchRecordDTO;
	}

	@SuppressWarnings("unchecked")
	public static List<SearchRecordDTO> listRecentlyOpenedItems(String username) throws Exception {
		List<SearchRecordDTO> results = new LinkedList<>();

		try {
        HibernateProxy.setCurrentUser(username);
			HibernateProxy.executeNoCache( () -> {
				List<RecentItem> recentItems = HibernateUtil
						.createQuery(String.format(FIND_RECENT_ITEM_BY_USERNAME, username)).list();
				
				List<String> deletionList = null;
				if (recentItems != null) {
					for (RecentItem item : recentItems) {
						ContentType type = ContentType.valueOf(item.getItemType());
						SearchRecordDTO recordDTO = new SearchRecordDTO();
						recordDTO.setSysId(item.getItemSysId());
						recordDTO.setType(item.getItemType());
						boolean found = false;

						switch (type) {
						case ACTIONTASK:
							ResolveActionTask task = HibernateUtil.getDAOFactory().getResolveActionTaskDAO()
									.findById(item.getItemSysId());
							if (task != null) {
								recordDTO.setName(task.getUName());
								recordDTO.setNamespace(task.getUNamespace());
								found = true;
							}
							break;
						case AUTOMATION:
						case DECISIONTREE:
						case DOCUMENT:
						case PLAYBOOK:
							WikiDocument wikidoc = HibernateUtil.getDAOFactory().getWikiDocumentDAO()
									.findById(item.getItemSysId());
							if (wikidoc != null) {
								recordDTO.setName(wikidoc.getUName());
								recordDTO.setNamespace(wikidoc.getUNamespace());
								found = true;
							}
							break;
						case PROPERTY:
							ResolveProperties properties = HibernateUtil.getDAOFactory().getResolvePropertiesDAO()
									.findById(item.getItemSysId());
							if (properties != null) {
								recordDTO.setName(properties.getUName());
								recordDTO.setNamespace(properties.getUModule());
								found = true;
							}
							break;
						case CUSTOMFORM:
							MetaFormView form = HibernateUtil.getDAOFactory().getMetaFormViewDAO()
									.findById(item.getItemSysId());
							if (form != null) {
								recordDTO.setName(form.getUFormName());
								found = true;
							}
							break;
						default:
							Log.log.error("Unhandled type for recent item: " + type);
							break;
						}

						if (found) {
							results.add(recordDTO);
						} else {
							if (deletionList == null) {
								deletionList = new ArrayList<>();
							}
							deletionList.add(item.getItemSysId());
						}
					}
				}
				
				// remove deleted content from recently opened items
				HibernateUtil.delete("RecentItem", deletionList);
			});

			
		} catch (Throwable e) {
			Log.log.warn(e.getMessage(), e);
			throw new Exception();
		}

		return results;
	}

	@SuppressWarnings("unchecked")
	public static SearchAttributesDTO saveRecentSearch(SearchAttributesDTO searchDTO, String username)
			throws Exception {
		if (StringUtils.isEmpty(username)) {
			username = "system";
		}
		
		String usernameFinal = username;

		try {
        HibernateProxy.setCurrentUser(username);
			HibernateProxy.executeNoCache( () -> {
				String escapedTerm = searchDTO.getTerm().replace("\\", "\\\\");
				String findSearch = "from RecentSearch where term = '" + escapedTerm + "' and sysCreatedBy = '" + usernameFinal
						+ "'";
				List<RecentSearch> existingSearches = HibernateUtil.createHQLQuery(findSearch).list();
				RecentSearch item = null;

				if (existingSearches.size() == 0) {

					item = new RecentSearch();
					item.setTerm(searchDTO.getTerm());
					item.setSearchFields(
							searchDTO.getSearchFields() != null ? String.join(ITEM_DELIMITER, searchDTO.getSearchFields())
									: null);
					item.setType(searchDTO.getType().name());
					item.setAuthors(
							searchDTO.getAuthors() != null ? String.join(ITEM_DELIMITER, searchDTO.getAuthors()) : null);
					item.setTags(searchDTO.getTags() != null ? String.join(ITEM_DELIMITER, searchDTO.getTags()) : null);
					item.setCreatedOn(searchDTO.getCreatedOn());
					item.setUpdatedOn(searchDTO.getUpdatedOn());
					item.setSortBy(searchDTO.getSortBy());
					item.setSize(searchDTO.getSize());
					item.setFrom(searchDTO.getFrom());
					item.setNamespaces(
							searchDTO.getNamespaces() != null ? String.join(ITEM_DELIMITER, searchDTO.getNamespaces())
									: null);
					item.setMenupaths(
							searchDTO.getMenupaths() != null ? String.join(ITEM_DELIMITER, searchDTO.getMenupaths())
									: null);
					item.setSysCreatedOn(new Date());
					item.setSysCreatedBy(usernameFinal);

					List<RecentSearch> recentSearches = HibernateUtil
							.createQuery(String.format(FIND_RECENT_SEARCH_BY_USERNAME, usernameFinal)).list();
					if (recentSearches != null && recentSearches.size() >= MAX_RECENT_ITEMS) {
						for (int index = MAX_RECENT_ITEMS - 1; index < recentSearches.size(); index++) {
							HibernateUtil.getDAOFactory().getRecentSearchDAO().delete(recentSearches.get(index));
						}
					}
				} else {
					item = existingSearches.get(0);
					item.setSysUpdatedOn(new Date());
				}

				HibernateUtil.getDAOFactory().getRecentSearchDAO().insertOrUpdate(item);
				
			});
			
		} catch (Throwable e) {
			Log.log.warn(e.getMessage(), e);
			throw new Exception();
		}

		return searchDTO;
	}

	@SuppressWarnings("unchecked")
	public static List<SearchAttributesDTO> listRecentSearches(String username) throws Exception {

		List<SearchAttributesDTO> results = new LinkedList<>();

		try {
        HibernateProxy.setCurrentUser(username);
			HibernateProxy.executeNoCache(() -> {
				
				List<RecentSearch> recentSearches = HibernateUtil
						.createQuery(String.format(FIND_RECENT_SEARCH_BY_USERNAME, username)).list();
				if (recentSearches != null) {
					for (RecentSearch recentSearch : recentSearches) {
						SearchAttributesDTO searchDTO = new SearchAttributesDTO();
						searchDTO.setSysId(recentSearch.getSys_id());
						searchDTO.setTerm(recentSearch.getTerm());
						searchDTO.setSearchFields(recentSearch.getSearchFields() != null
								? Arrays.asList(recentSearch.getSearchFields().split(ITEM_DELIMITER))
								: null);
						searchDTO.setType(recentSearch.getType());
						searchDTO.setAuthors(recentSearch.getAuthors() != null
								? Arrays.asList(recentSearch.getAuthors().split(ITEM_DELIMITER))
								: null);
						searchDTO.setTags(
								recentSearch.getTags() != null ? Arrays.asList(recentSearch.getTags().split(ITEM_DELIMITER))
										: null);
						searchDTO.setCreatedOn(recentSearch.getCreatedOn());
						searchDTO.setUpdatedOn(recentSearch.getUpdatedOn());
						searchDTO.setSortBy(recentSearch.getSortBy());
						searchDTO.setSize(String.valueOf(recentSearch.getSize()));
						searchDTO.setFrom(String.valueOf(recentSearch.getFrom()));
						searchDTO.setNamespaces(recentSearch.getNamespaces() != null
								? Arrays.asList(recentSearch.getNamespaces().split(ITEM_DELIMITER))
								: null);
						searchDTO.setMenupaths(recentSearch.getMenupaths() != null
								? Arrays.asList(recentSearch.getMenupaths().split(ITEM_DELIMITER))
								: null);
						results.add(searchDTO);
					}
				}
			});
			
		} catch (Throwable e) {
			Log.log.warn(e.getMessage(), e);
			throw new Exception();
		}

		return results;
	}

}
