package com.resolve.services.rr.util;

import com.resolve.services.hibernate.vo.RRRuleVO;

public interface BulkDBTaskDataManipulator
{
    public void manipulate(RRRuleVO rule,String username) throws Exception;
}
