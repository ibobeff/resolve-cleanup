/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.util;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Query;

import com.bmc.thirdparty.org.apache.commons.collections.CollectionUtils;
import com.resolve.dto.SectionType;
import com.resolve.persistence.model.AccessRights;
import com.resolve.persistence.model.MetaFormView;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikiDocumentMetaFormRel;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceHibernate;
import com.resolve.services.constants.ConstantValues;
import com.resolve.services.hibernate.vo.WikiDocumentMetaFormRelVO;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.wiki.CopyHelper;
import com.resolve.services.hibernate.wiki.CreateFormForTemplate;
import com.resolve.services.hibernate.wiki.DeleteHelper;
import com.resolve.services.hibernate.wiki.EvaluateAccessRights;
import com.resolve.services.hibernate.wiki.SaveHelper;
import com.resolve.services.hibernate.wiki.SearchWikiTemplates;
import com.resolve.services.hibernate.wiki.WikiStatusEnum;
import com.resolve.services.hibernate.wiki.WikiStatusUtil;
import com.resolve.services.impex.util.ImpexEnum;
import com.resolve.services.interfaces.VO;
import com.resolve.services.util.GenericSectionUtil;
import com.resolve.services.util.UpdateChecksumUtil;
import com.resolve.services.vo.QueryDTO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstants;
import com.resolve.workflow.form.CopyOrRenameCustomForm;

/**
 * TODO: remove the StoreUtility, WikiStore, and others that are in RSVIEW. This class should be free of RSVIEW classes
 * 
 * @author jeet.marwah
 *
 */
public class WikiTemplateUtil
{

    public static List<WikiDocumentMetaFormRelVO> getWikiTemplate(QueryDTO query, String username)
    {
        return new SearchWikiTemplates(query, username).execute();
    }

    public static WikiDocumentMetaFormRelVO findWikiTemplateByIdOrByName(String sys_id, String templateName, String username)
    {
        WikiDocumentMetaFormRel model = null;
        WikiDocumentMetaFormRelVO result = null;

        
            try
            {
                if (StringUtils.isNotBlank(sys_id))
                {
                    model = findWikiTemplateModel(sys_id, null);
                }
                else if (StringUtils.isNotBlank(templateName))
                {
                    model = findWikiTemplateModel(null, templateName);
                }
            }
            catch (Exception e)
            {
                Log.log.warn(e.getMessage(), e);
            }

        if (model != null)
        {
            result = convertModelToVO(model, null);
        }

        return result;
    }

    //Refer to GeneralTabWidget.java for the UI logic
    public static WikiDocumentMetaFormRelVO saveWikiTemplate(WikiDocumentMetaFormRelVO vo, String username) throws Exception
    {
        if (vo != null && vo.validate())
        {
            WikiDocumentMetaFormRel model = null;
            WikiDocumentMetaFormRel localModel = findWikiTemplateModel(null, vo.getUName());
            
            if (StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equalsIgnoreCase(VO.STRING_DEFAULT))
            {
                model = findWikiTemplateModel(vo.getSys_id(), null);
                if (model != null && localModel != null)
                {
                    if (localModel.getSys_id().equals(model.getSys_id()))
                    {
                        if (!localModel.getUName().equals(model.getUName()))
                        {
                            throwExceptionAndQuit("Template with same sys_id and different name");
                        }
                    }
                    else
                    {
                        throwExceptionAndQuit("Template with different sys_id and same name");
                    }
                }
            }
            else
            {
                if (localModel != null)
                {
                    // New tamplate with the existing template name.
                    throwExceptionAndQuit("The template '" + vo.getUName() + "' already exists.");
                }
                
                model = new WikiDocumentMetaFormRel();
                
                //make sure that the name is in CAPS
                vo.setUName(vo.getUName().toUpperCase());

                //set the names
                vo.setUWikiDocName(HibernateConstants.WIKI_TEMPLATE_NAMESPACE + "." + vo.getUName());
                vo.setUFormName(HibernateConstants.FORM_TEMPLATE_NAMESPACE + vo.getUName());
            }
            
            //create a rec in the template table - WikiDocumentMetaFormRel
            model.applyVOToModel(vo);
            model.setChecksum(calculateChecksum(model));
            //update the access rights
            EvaluateAccessRights.updateAccessRightsFor(model, username);

            //persist it
            model = SaveUtil.saveWikiDocumentMetaFormRel(model, username);

            //create a wikidoc in the Template namespace - create a template
            WikiDocument templateDocument = createTemplateDocument(model.getUWikiDocName(), model.getAccessRights(), username);

            //create a custom form place holder
            MetaFormView form = createMetaForm(model.getUFormName().toUpperCase(), model.getAccessRights(), username);
            
            //rename if this condition is true
            if(StringUtils.isNotBlank(vo.getSys_id()) && !vo.getSys_id().equalsIgnoreCase(VO.STRING_DEFAULT)&&localModel==null) {
                String newFormName = (HibernateConstants.FORM_TEMPLATE_NAMESPACE + vo.getUName()).toUpperCase();
                String newWikiName = HibernateConstants.WIKI_TEMPLATE_NAMESPACE + "." +  vo.getUName();
                
                //create new wiki
                WikiDocumentVO templateDocumentVO = WikiUtils.moveRenameDocument(model.getUWikiDocName(), newWikiName, false, true, username);
                templateDocument = new WikiDocument(templateDocumentVO);
                //create new form
                form = new CopyOrRenameCustomForm(model.getUFormName(), newFormName, username).rename();
                model.setUWikiDocName(newWikiName);
                model.setUFormName(newFormName);
            }

            //update the template definition
            model.setWikidoc(templateDocument);
            model.setForm(form);
            model = SaveUtil.saveWikiDocumentMetaFormRel(model, username);

            //convert to VO
            vo = convertModelToVO(model, null);
        }

        return vo;
    }
    
    private static void throwExceptionAndQuit(String message) throws Exception
    {
        throw new Exception(message);
    }

    public static void deleteWikiTemplateByIds(String[] sysIds, String username) throws Exception
    {
        if (sysIds != null)
        {
            for (String sysId : sysIds)
            {
                deleteWikiTemplateById(sysId, username);
            }
        }
    }

    public static WikiDocumentMetaFormRelVO copyTemplate(String fromTemplate, String toTemplate, String username) throws Exception
    {

        WikiDocumentMetaFormRelVO newTemplate = null;

        if (StringUtils.isNotBlank(fromTemplate) && StringUtils.isNotBlank(toTemplate))
        {

            WikiDocumentMetaFormRel model = findWikiTemplateModel(null, fromTemplate);
            if (model == null)
            {
                throw new Exception("The template '" + fromTemplate + "' does not exists.");
            }
            
            toTemplate = toTemplate.trim().toUpperCase();
            String newFormName = (HibernateConstants.FORM_TEMPLATE_NAMESPACE + toTemplate).toUpperCase();
            String newWikiName = HibernateConstants.WIKI_TEMPLATE_NAMESPACE + "." + toTemplate;
            
            //create new wiki
            WikiDocumentVO doc = WikiUtils.copyDocument(model.getUWikiDocName(), newWikiName, false, true, username);
            
            //create new form
            MetaFormView form = new CopyOrRenameCustomForm(model.getUFormName(), newFormName, username).copy();

            //update the values
            model.setSys_id(null);
            model.setSysCreatedBy(null);
            model.setSysCreatedOn(null);
            model.setSysUpdatedBy(null);
            model.setSysUpdatedOn(null);
            model.setUName(toTemplate);
            model.setUWikiDocName(newWikiName);
            model.setUFormName(newFormName);
            model.setForm(form);
            model.setWikidoc(new WikiDocument(doc));

            if (model.getAccessRights() != null)
            {
                AccessRights ar = model.getAccessRights();
                ar.setSys_id(null);
                ar.setSysCreatedBy(null);
                ar.setSysCreatedOn(null);
                ar.setSysUpdatedBy(null);
                ar.setSysUpdatedOn(null);
            }

            //save it
            newTemplate = saveWikiTemplate(model.doGetVO(), username);

        }

        return newTemplate;

    }
    
    public static WikiDocumentMetaFormRelVO renameTemplate(String fromTemplate, String toTemplate, String username) throws Exception
    {
        WikiDocumentMetaFormRelVO newTemplate = null;

        if (StringUtils.isNotBlank(fromTemplate) && StringUtils.isNotBlank(toTemplate))
        {

            WikiDocumentMetaFormRel model = findWikiTemplateModel(null, fromTemplate);
            if (model == null)
            {
                throw new Exception("The template '" + fromTemplate + "' does not exists.");
            }
            
            toTemplate = toTemplate.trim().toUpperCase();
            boolean validName = toTemplate.matches(HibernateConstants.REGEX_ALPHANUMERIC_NO_SPACE);
            if(!validName)
            {
                throw new Exception("Template name can have only Alpha numeric and _.");
            }
            String newFormName = (HibernateConstants.FORM_TEMPLATE_NAMESPACE + toTemplate).toUpperCase();
            String newWikiName = HibernateConstants.WIKI_TEMPLATE_NAMESPACE + "." + toTemplate;
            
            //create new wiki
            WikiDocumentVO doc = WikiUtils.moveRenameDocument(model.getUWikiDocName(), newWikiName, false, true, username);
            
            
            //create new form
            MetaFormView form = new CopyOrRenameCustomForm(model.getUFormName(), newFormName, username).rename();

            //update the values
            model.setUName(toTemplate);
            model.setUWikiDocName(newWikiName);
            model.setUFormName(newFormName);
            model.setForm(form);
            model.setWikidoc(new WikiDocument(doc));
            
            //save it
            model = SaveUtil.saveWikiDocumentMetaFormRel(model, username);
            newTemplate = convertModelToVO(model, null);
        }

        return newTemplate;
    }
    
    /**
     * API to create a wiki document with Template
     * 
     * List of keys that this API expects:
         * MANDATORY 
             * USERNAME  
             * namespace --> namespace of the wikidocument that will be created with 
             * docname --> wiki document name
             * template_namespace 
             * template_docname
         * OPTIONAL - THESE ARE NOT REQUIRED ANY MORE AS WE GET ALL THE STUFF FROM THE TEMPLATE NOW
             * summary
             * tags
             * mainXML
             * finalXML
             * abortXML
             * readRoles
             * writeRoles
             * adminRoles
             * executeRoles
             * template_values --> map of key-value pairs that will be substituted 
             * 
     * @param params
     * @param username
     */
    @SuppressWarnings("unchecked")
    public static void createWikiUsingTemplate(Map<String, Object> params, String username) throws Exception
    {
        String templateNamespace = (String)params.get(ConstantValues.WIKI_TEMPLATE_NAMESPACE_KEY);
        String templateDocumentName = (String)params.get(ConstantValues.WIKI_TEMPLATE_DOCNAME_KEY);
        String namespace = (String)params.get(ConstantValues.WIKI_NAMESPACE_KEY);
        String docname = (String)params.get(ConstantValues.WIKI_DOCNAME_KEY);
        Map<String, String> templateValues = (HashMap<String,String>)params.get(ConstantValues.WIKI_TEMPLATE_VALUES_KEY);
        
        String fullName = namespace+"."+docname;
        String templateFullName = templateNamespace+"."+templateDocumentName;
        WikiDocumentVO doc = null;
        
        try
        {
            //get the template
            WikiDocumentVO templateVO = WikiUtils.getWikiDoc(null, templateFullName, username);
            if(templateVO != null)
            {
                //new content
                String newWikiDocumentContent = replaceTemplateValues(fullName, templateVO.getUContent(), templateValues, false);
                String newWikiDocumentModelProcess = replaceTemplateValues(fullName, templateVO.getUModelProcess(), templateValues, true);
                String newWikiDocumentModelException = replaceTemplateValues(fullName, templateVO.getUModelException(), templateValues, true);
                String newWikiDocumentModelDT = replaceTemplateValues(fullName, templateVO.getUDecisionTree(), templateValues, true);
                String newWikiDocumentDTOptions = replaceTemplateValues(fullName, templateVO.getUDTOptions(), templateValues, false);
                
                if(templateVO.ugetUIsRoot())
                {
                    //so this Template is a DT. we have to remove the section for the Template one as when a Document gets created, it will have its own DT associated with it
                    String regex = "(?ms)(\\{section:type=" + SectionType.SOURCE + "\\|title=dt_" + templateFullName + "_(\\d+).*?\\{section\\}\n?)";
                    Pattern pattern = Pattern.compile(regex); 
                    Matcher matcher = pattern.matcher(newWikiDocumentContent);
                    while (matcher.find())
                    {
                        String dtSection = matcher.group(1);
                        newWikiDocumentContent = newWikiDocumentContent.replace(dtSection, "");
                    }//end of while loop
                }
                
                //make copy of the template 
                //this will copy the attachments, tags, rights, etc 
                CopyHelper copyHelper = new CopyHelper(templateFullName, fullName, username);
                copyHelper.setOverwrite(false);
                copyHelper.setValidateUser(false);//no validation as its a template copy
                copyHelper.setUserShouldFollow(true);
                copyHelper.setIsCreateWikiDocFromTemplate(true);
                
                doc = copyHelper.copy();//WikiUtils.getWikiDoc(null, templateNamespace+"."+templateDocumentName, username);
                doc = WikiUtils.updateWikidocumentFromMap(doc, params);
                
                //update with new content
                doc.setUContent(newWikiDocumentContent);
                doc.setUModelProcess(newWikiDocumentModelProcess);
                doc.setUModelException(newWikiDocumentModelException);
                doc.setUDecisionTree(newWikiDocumentModelDT);
                doc.setUDTOptions(newWikiDocumentDTOptions);
                
                //save the document now
                doc = WikiUtils.saveWikiDoc(doc, null, username);
            }
            else
            {
                throw new Exception("Wiki Template with name " + templateFullName + " does not exist.");
            }
            
            //content
//            String newWikiDocumentContent = replaceTemplateValues(templateContent, templateValues);
//            
//            //put that in the hashmap
//            params.put(ConstantValues.WIKI_CONTENT_KEY, newWikiDocumentContent);
//            
//            //create the wiki
//            WikiUtils.createWiki(params, username);
        }
        catch(Exception e)
        {
            String error = "Error while creating document " + fullName + " from template " + templateFullName;
            
            if (StringUtils.isBlank(e.getMessage()) || 
                !(e.getMessage().startsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_PREFIX) &&
                  (e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_SUFFIX) ||
                   e.getMessage().endsWith(SaveHelper.FAILED_TO_SAVE_OBJECT_HA_VIOLATED_SUFFIX)))) {
            	Log.log.error(error, e);
            	throw new Exception(error);
            } else {
            	throw e;
            }
        }
    }

    //private apis
    private static WikiDocumentMetaFormRelVO convertModelToVO(WikiDocumentMetaFormRel model, Map<String, String> mapOfOrganization)
    {
        WikiDocumentMetaFormRelVO vo = null;

        if (model != null)
        {
            if (mapOfOrganization == null)
            {
                mapOfOrganization = OrganizationUtil.getListOfAllOraganizationNames();
            }

            vo = model.doGetVO();
            if (StringUtils.isNotBlank(vo.getSysOrg()))
            {
                vo.setSysOrganizationName(mapOfOrganization.get(vo.getSysOrg()));
            }
            else
            {
                vo.setSysOrganizationName("");
            }
        }

        return vo;
    }

    private static WikiDocumentMetaFormRel findWikiTemplateModel(String sys_id, String name)
    {
        WikiDocumentMetaFormRel result = null;

        try
        {
            result = (WikiDocumentMetaFormRel) HibernateProxy.execute(() -> {

	            if (StringUtils.isNotBlank(sys_id))
	            {
	                return HibernateUtil.getDAOFactory().getWikiDocumentMetaFormRelDAO().findById(sys_id);
	            }
	            else if (StringUtils.isNotBlank(name))
	            {
	                String hql = " from WikiDocumentMetaFormRel where upper(UName) = :name";
	                List<?> temp = HibernateUtil.createQuery(hql)
	                                .setParameter("name", name.toUpperCase())
	                                .list();
	                if (temp.size() > 0)
	                {
	                    return (WikiDocumentMetaFormRel) temp.get(0);
	                }
	            }

	            return null;
            });
        }
        catch (Exception e)
        {
            Log.log.warn(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }

        return result;
    }

    private static void deleteWikiTemplateById(String sysId, String username) throws Exception
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {

	            WikiDocumentMetaFormRel model = HibernateUtil.getDAOFactory().getWikiDocumentMetaFormRelDAO().findById(sysId);
	            if (model != null)
	            {
	                try
	                {
	                    // delete the wikidocument
	                    delete(model.getUWikiDocName(), true, username);
	                }
	                catch (Exception e)
	                {
	                    Log.log.error(e);
	                }
	
	                try
	                {
	                    // delete the form
	                    ServiceHibernate.deleteMetaFormByName(model.getUFormName());
	                }
	                catch (Exception e)
	                {
	                    Log.log.error(e);
	                } catch (Throwable e) {
						throw new Exception(e);
					}
	
	                // now delete the record
	                if(model.getAccessRights() != null)
	                {
	                    HibernateUtil.getDAOFactory().getAccessRightsDAO().delete(model.getAccessRights());
	                }
	                HibernateUtil.getDAOFactory().getWikiDocumentMetaFormRelDAO().delete(model);
	            }

        	});
        }
        catch (Throwable e)
        {
            Log.log.warn(e.getMessage(), e);
            throw new Exception(e);
        }
    }

    private static void delete(String docFullName, boolean purge, String username) throws Exception
    {
        if (purge)
        {
            DeleteHelper.deleteWikiDocument(null, docFullName, username);
        }
        else
        {
            WikiStatusUtil.updateWikiStatusFlag(null, docFullName, WikiStatusEnum.DELETED, true, true, username);
//            DeleteHelper.updateDelete(null, docFullName, true, username);
        }
    }

    private static WikiDocument createTemplateDocument(String docName, AccessRights rights, String username)
    {
        WikiDocument templateDocument = null;

        if (StringUtils.isNotBlank(docName))
        {
            String fullName = docName.trim();

            //find the template
            try
            {
                templateDocument = StoreUtility.find(fullName);
            }
            catch (Exception e)
            {
                templateDocument = null;
            }

            //create a new one if it does not exist
            if (templateDocument == null)
            {
                //create doc with fullName
                templateDocument = createDocumentWithName(fullName, rights, username);
            }
            else
            {
                //edit it - only update the access rights of template to this document
                AccessRights docRights = templateDocument.getAccessRights();
                docRights.setUReadAccess(rights.getUReadAccess());
                docRights.setUWriteAccess(rights.getUWriteAccess());
                docRights.setUExecuteAccess(rights.getUExecuteAccess());
                docRights.setUAdminAccess(rights.getUAdminAccess());
                
                templateDocument.setAccessRights(docRights);
                
                try
                {
                    saveWikiDocument(templateDocument, username);
                }
                catch (Exception e)
                {
                    //set it to null, if anything goes wrong
                    Log.log.error("Cannot update doc:" + templateDocument.getUFullname(), e);
                    templateDocument = null;
                }
            }

        }

        return templateDocument;
    }

    private static WikiDocument createDocumentWithName(String docName, AccessRights rights, String username)
    {
        WikiDocument doc = new WikiDocument();
        doc.setUFullname(docName);

        StringBuffer content = new StringBuffer();
        content.append("1 Template Heading").append("\n\n");
        content.append("Use the Source Editor or the Wiki Builder to define the layout of the template.").append("\n");
        content.append("The values from the form will substitute any matching fields defined by \\[fieldname\\].").append("\n");
        content.append("For example: \\[name\\] will be replaced when with the actual values from the form for the fieldname: name").append("\n");

        try
        {
            String[] arr = docName.split("\\.");
            doc.setUNamespace(arr[0]);
            doc.setUName(arr[1]);
            doc.setUContent(GenericSectionUtil.validateSectionStr(content.toString(), null));
            doc.setUIsActive(true);
            doc.setUIsDeleted(false);
            doc.setUIsHidden(false);
            doc.setUIsLocked(false);
            doc.setUIsRoot(false);
            doc.setUIsDefaultRole(false);

            doc.setUReadRoles(rights.getUReadAccess());
            doc.setUWriteRoles(rights.getUWriteAccess());
            doc.setUExecuteRoles(rights.getUExecuteAccess());
            doc.setUAdminRoles(rights.getUAdminAccess());

            saveWikiDocument(doc, username);
        }
        catch (Exception e)
        {
            //set it to null, if anything goes wrong
            Log.log.error("Cannot create doc:" + docName, e);
            doc = null;
        }

        return doc;
    }

    private static void saveWikiDocument(WikiDocument doc, String username) throws Exception
    {
        if(doc != null)
        {
            doc.setWikidocAttachmentRels(null);
            doc.setWikidocQualityRating(null);
            doc.setWikidocRatingResolution(null);
            doc.setWikidocResolveTagRels(null);
            doc.setWikidocStatistics(null);
            
            SaveHelper save = new SaveHelper(doc, username);
            save.setUserShouldFollow(false);
            save.save();
        }
    }

    private static MetaFormView createMetaForm(String formViewName, AccessRights rights, String username) throws Exception
    {
        MetaFormView form = null;

        try
        {
            form = new CreateFormForTemplate(formViewName, rights, username).createForm();
        }
        catch (Exception e)
        {
            Log.log.error("Error creating the form from Template", e);
            throw e;
        }

        return form;
    }
    
    private static String replaceTemplateValues(String docName, String templateContent, Map<String, String> templateValues, boolean isModel)
    {
        String content = templateContent;
        
        if(StringUtils.isNotEmpty(templateContent) && templateValues != null && templateValues.size() > 0)
        {
            Iterator<String> it = templateValues.keySet().iterator();
            while(it.hasNext())
            {
                String originalKey = it.next();
                String value = templateValues.get(originalKey);
                
                String key = originalKey.replace("[", "\\[");
                key = key.replace("]", "\\]");
                
                value = Matcher.quoteReplacement(value);
                content = content.replaceAll(key, value);
                
                //model params has double encoding. So replacing that
                if(isModel)
                {
                    try
                    {
                        //have to double encode as that is how the automation.js has the params 
                        String encodedKey = URLEncoder.encode(URLEncoder.encode(originalKey, "UTF-8"), "UTF-8");
                        String encodedValue = URLEncoder.encode(URLEncoder.encode(value, "UTF-8"), "UTF-8");
                        content = content.replaceAll(encodedKey, encodedValue);
                    }
                    catch(Exception e)
                    {
                        Log.log.error("Error in replacing the encoding for the document " + docName, e);
                    }
                }
            }
            
        }//if clause
        
        return content;
    }//replaceTemplateValues
    
    public static Map<String, Object> getTemplateComponents(String templateId, String username) {
        Map<String, Object> result = new HashMap<>();
        
        WikiDocumentMetaFormRel templateVO = findWikiTemplateModel(templateId, username);
        if (templateVO != null) {
            WikiDocument wikiVo = templateVO.getWikidoc();
            if (wikiVo != null) {
                List<String> wikis = new ArrayList<>();
                wikis.add(wikiVo.getUFullname() + ImpexEnum.COLON.getValue() + wikiVo.getSys_id());
                result.put(ImpexEnum.WIKI.getValue(), wikis);
            } else {
                Log.log.warn(String.format("The Wiki belongs to the wiki template, %s, does not exist.", templateVO.getUName()));
            }
            MetaFormView formVO = templateVO.getForm();
            if (formVO != null) {
                List<String> forms = new ArrayList<>();
                forms.add(formVO.getUFormName() + ImpexEnum.COLON.getValue() + formVO.getSys_id());
                result.put(ImpexEnum.FORM.getValue(), forms);
            } else {
                Log.log.warn(String.format("The Form belongs to the wiki template, %s, does not exist.", templateVO.getUName()));
            }
        }
        
        return result;
    }
    
    /**
     * API to be invoked from update/upgrade script only!
     */
    public static void updateAllWikiTemplatesForChecksum() throws Exception
    {
        try {
            UpdateChecksumUtil.updateComponentChecksum("WikiDocumentMetaFormRel", WikiTemplateUtil.class);
        }
        catch (Exception e) {
            Log.log.error(e.getMessage(), e);
        }
    }
    
    public static Integer calculateChecksum(WikiDocumentMetaFormRel wikiTemplate)
    {
        return Objects.hash(wikiTemplate.getUName(), wikiTemplate.getUWikiDocName(), wikiTemplate.getUFormName());
    }
    
    public static Integer getTemplateChecksum(String name, String username)
    {
        Integer checksum = null;
        WikiDocumentMetaFormRel template = findWikiTemplateModel(null, name);
        if (template != null)
        {
            checksum = template.getChecksum();
        }
        return checksum;
    }
    
    private static int calculateTemplateChecksum(WikiDocumentMetaFormRel wikiTemplate)
    {
        return Objects.hash(wikiTemplate.getUName(), wikiTemplate.getUWikiDocName(), wikiTemplate.getUFormName());
    }
}
