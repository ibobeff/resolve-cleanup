/******************************************************************************
 * (C) Copyright 2014
 *
 * Resolve Systems, LLC
 *
 * All rights reserved
 * This software is distributed under license and may not be used, copied,
 * modified, or distributed without the express written permission of
 * Resolve Systems, LLC.
 *
 ******************************************************************************/

package com.resolve.services.vo.form;

import java.util.List;

public class RsUIButton extends RsUIComponent
{
    private String type;
    private boolean customTableDisplay;
    private List<RsButtonAction> actions;
    private List<RsFieldDependency> dependencies;
    // RBA-13999 : adding extra properties, like font, color, etc.
    private String backgroundColor;
    private String font;
    private String fontColor;
    private String fontSize;

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public List<RsButtonAction> getActions()
    {
        return actions;
    }

    public void setActions(List<RsButtonAction> actions)
    {
        this.actions = actions;
    }

    public List<RsFieldDependency> getDependencies()
    {
        return dependencies;
    }

    public void setDependencies(List<RsFieldDependency> dependencies)
    {
        this.dependencies = dependencies;
    }

    public boolean isCustomTableDisplay()
    {
        return customTableDisplay;
    }

    public void setCustomTableDisplay(boolean customTableDisplay)
    {
        this.customTableDisplay = customTableDisplay;
    }

    public String getBackgroundColor()
    {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor)
    {
        this.backgroundColor = backgroundColor;
    }

    public String getFont()
    {
        return font;
    }

    public void setFont(String font)
    {
        this.font = font;
    }

    public String getFontColor()
    {
        return fontColor;
    }

    public void setFontColor(String fontColor)
    {
        this.fontColor = fontColor;
    }

    public String getFontSize()
    {
        return fontSize;
    }

    public void setFontSize(String fontSize)
    {
        this.fontSize = fontSize;
    }

}
