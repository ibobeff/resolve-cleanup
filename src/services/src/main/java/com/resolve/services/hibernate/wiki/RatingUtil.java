/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.hibernate.wiki;

import java.util.Collection;
import java.util.Set;

import com.resolve.persistence.dao.WikiDocumentDAO;
import com.resolve.persistence.dao.WikidocResolutionRatingDAO;
import com.resolve.persistence.model.WikiDocument;
import com.resolve.persistence.model.WikidocResolutionRating;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.ServiceWiki;
import com.resolve.services.exception.WikiException;
import com.resolve.services.hibernate.util.WikiUtils;
import com.resolve.services.hibernate.vo.WikiDocumentVO;
import com.resolve.services.hibernate.vo.WikidocResolutionRatingVO;
import com.resolve.util.Log;
import com.resolve.wiki.stats.StarRating;

public class RatingUtil
{
    public static WikidocResolutionRatingVO getResolutionRatingForDocument(String docSysId, String docFullName, String username) throws Exception
    {
        WikidocResolutionRatingVO result = null;
        
        WikiDocument doc = WikiUtils.getWikiDocumentModel(docSysId, docFullName, username, true);
        if(doc == null)
        {
            throw new Exception("Document with sysId " + docSysId + " or docFullName " + docFullName + " does not exist.");
        }
        
        if(doc.getWikidocRatingResolution() != null && doc.getWikidocRatingResolution().size() > 0)
        {
            for(WikidocResolutionRating rating : doc.getWikidocRatingResolution())
            {
                result = rating.doGetVO();
            }
        }
        
        return result;
    }
    
    public static void updateResolutionRatingCountForNamespaces(Set<String> namespaces, WikidocResolutionRatingVO rating, String username) throws Exception
    {
        if(namespaces != null)
        {
            for(String ns : namespaces)
            {
                try
                {
                    Set<String> sysIds = NamespaceUtil.findAllDocumentsSysIdsForNamespace(ns, username);
                    updateResolutionRatingCount(sysIds, rating, username); 
                }
                catch (Exception e)
                {
                    Log.log.error("error setting the rating for ns " + ns, e);
                }
            }
        }
    }
    
    
    public static void updateResolutionRatingCount(Set<String> wikidocumentId, WikidocResolutionRatingVO rating, String username)  throws WikiException
    {
        for(String sys_id : wikidocumentId)
        {
            updateWikidocResolutionRating( sys_id, rating, username);
        }
    }//updateResolutionRatingCount
    
    public static void updateWeightForNamespaces(Set<String> namespaces, double weight, String username)  throws WikiException
    {
        if(namespaces != null)
        {
            for(String ns : namespaces)
            {
                try
                {
                    Set<String> sysIds = NamespaceUtil.findAllDocumentsSysIdsForNamespace(ns, username);
                    updateWikidocsWeight(sysIds, weight, username); 
                }
                catch (Exception e)
                {
                    Log.log.error("error setting the rating for ns " + ns, e);
                }
            }
        }
    }
    
    public static void updateWikidocsWeight(Set<String> wikidocumentId, double weight, String username)  throws WikiException
    {
        for(String sys_id : wikidocumentId)
        {
            updateWikidocWeight(sys_id, weight, username);
        }
    }//updateWikidocsWeight
    
    public static void incrementRating(String docSysId, String docFullName, int rating, long multiplier, String username) throws Exception
    {
        WikiDocumentVO doc = null;
        if(rating < 1 || rating > 5 || multiplier == 0)
        {
            throw new Exception("Rating and Multiplier should be a positive number and rating should be between 1 and 5.");
        }
        
        doc = ServiceWiki.getWikiDoc(docSysId, docFullName, username);
        if(doc != null)
        {
            StarRating starRating = StarRating.STAR_1;
            if(rating == 2)
            {
                starRating = StarRating.STAR_2;
            }
            else if(rating == 3)
            {
                starRating = StarRating.STAR_3;
            }
            else if(rating == 4)
            {
                starRating = StarRating.STAR_4;
            }
            else if(rating == 5)
            {
                starRating = StarRating.STAR_5;
            }
            
            updateWikidocResolutionRating(doc.getSys_id(), starRating, multiplier, username);
           
        }
        else
        {
           throw new Exception("Document does not exist - so will not be able to update the rating for it : Name :" + docFullName + " : sysid : " + docSysId);
        }
        
    }//incrementRating
    
    public static void initRating(String docSysId, String docFullName, int total, int count, String username) throws Exception
    {
        if(total < 1 || count < 1 || (total/count > 5))
        {
            throw new Exception("Total and Count should be a positive number whose average should be between 1 and 5.");
        }

        WikiDocumentVO docVO = ServiceWiki.getWikiDoc(docSysId, docFullName, username);
        if(docVO != null)
        {
            try
            {
              HibernateProxy.setCurrentUser(username);
            	HibernateProxy.execute(() -> {
                    //get the handle of wikidocument
                    WikiDocument doc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(docVO.getSys_id());
                    if(doc != null)
                    {
                        WikidocResolutionRatingDAO daoWR = HibernateUtil.getDAOFactory().getWikidocResolutionRatingDAO();

                        //get the handle to the resolution object
                        WikidocResolutionRating wikiResolution = null;
                        Collection<WikidocResolutionRating> wikidocResolutions = doc.getWikidocRatingResolution();
                        if(wikidocResolutions != null && wikidocResolutions.size() == 1)
                        {
                            wikiResolution = wikidocResolutions.iterator().next();
                        }
                        else
                        {
                            wikiResolution = new WikidocResolutionRating();
                            wikiResolution.setWikidoc(doc);
                        }
                        
                        wikiResolution.setU1StarCount(0L);
                        wikiResolution.setU2StarCount(0L);
                        wikiResolution.setU3StarCount(0L);
                        wikiResolution.setU4StarCount(0L);
                        wikiResolution.setU5StarCount(0L);
                        wikiResolution.setUinitTotal(new Long(total));
                        wikiResolution.setUinitCount(new Long(count));
                        
                        daoWR.persist(wikiResolution);
                    }
            	});
            }
            catch (Exception e)
            {
                Log.log.debug(e.getMessage(), e);
                throw e;
            }
        }
        else
        {
           throw new Exception("Document does not exist - so will not be able to update the rating for it : Name :" + docFullName + " : sysid : " + docSysId);
        }
    }//initRating
    
    
    
    /**
     * 
     * increment/decrement the count of the starRating for this document
     * 
     * @param wikiDocument
     * @param starRating 
     * @param ratingCount - can be +ve or -ve #
     * @param username
     */
    private static void updateWikidocResolutionRating(String wikidocumentId, WikidocResolutionRatingVO rating, String username) throws WikiException
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	WikiDocument wikiDoc = (WikiDocument) HibernateProxy.execute(() -> {
	            //get the handle of wikidocument
	            WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
	            WikiDocument wikiDocument = dao.findById(wikidocumentId);
	            
	            WikidocResolutionRatingDAO daoWR = HibernateUtil.getDAOFactory().getWikidocResolutionRatingDAO();
	
	            //get the handle to the resolution object
	            WikidocResolutionRating wikiResolution = null;
	            Collection<WikidocResolutionRating> wikidocResolutions = wikiDocument.getWikidocRatingResolution();
	            if(wikidocResolutions != null && wikidocResolutions.size() == 1)
	            {
	                wikiResolution = wikidocResolutions.iterator().next();
	            }
	            else
	            {
	                wikiResolution = new WikidocResolutionRating();
	                wikiResolution.setWikidoc(wikiDocument);
	            }
	            
	            //increment/decrement the count
	            wikiResolution.setU1StarCount(rating.getU1StarCount());
	            wikiResolution.setU2StarCount(rating.getU2StarCount());
	            wikiResolution.setU3StarCount(rating.getU3StarCount());
	            wikiResolution.setU4StarCount(rating.getU4StarCount());
	            wikiResolution.setU5StarCount(rating.getU5StarCount());
	
	            //persist
	            daoWR.persist(wikiResolution);
	            return wikiDocument;
            });
            
            //index the document
            ServiceWiki.indexWikiDocument(wikiDoc.getSys_id(), username);
            
        }
        catch (Exception e)
        {
            Log.log.debug(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
       

    }//updateRatingCount
    
    private static void updateWikidocWeight(String sysId, double weight, String username)
    {
    	WikiDocument wikiDocument =  null;
        try
        {
          HibernateProxy.setCurrentUser(username);
        	wikiDocument = (WikiDocument) HibernateProxy.execute(() -> {

	            //get the handle of wikidocument and update the value
        		WikiDocument wikiDoc = HibernateUtil.getDAOFactory().getWikiDocumentDAO().findById(sysId);
	            if(wikiDoc != null)
	            {
	                wikiDoc.setURatingBoost(weight);
	            }
	            
	            HibernateUtil.getCurrentSession().flush();
	            return wikiDoc;
            });
            
            //index the document
            ServiceWiki.indexWikiDocument(wikiDocument.getSys_id(), username);
        }
        catch (Exception e)
        {
            Log.log.debug(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
    }
    
    private static void updateWikidocResolutionRating(String wikidocumentId, StarRating starRating, long ratingCount, String username) throws WikiException
    {
        try
        {
          HibernateProxy.setCurrentUser(username);
        	HibernateProxy.execute(() -> {

	            //get the handle of wikidocument
	            WikiDocumentDAO dao = HibernateUtil.getDAOFactory().getWikiDocumentDAO();
	            WikiDocument wikiDocument = dao.findById(wikidocumentId);
	            
	            WikidocResolutionRatingDAO daoWR = HibernateUtil.getDAOFactory().getWikidocResolutionRatingDAO();
	
	            //get the handle to the resolution object
	            WikidocResolutionRating wikiResolution = null;
	            Collection<WikidocResolutionRating> wikidocResolutions = wikiDocument.getWikidocRatingResolution();
	            if(wikidocResolutions != null && wikidocResolutions.size() == 1)
	            {
	                wikiResolution = wikidocResolutions.iterator().next();
	            }
	            else
	            {
	                wikiResolution = new WikidocResolutionRating();
	                wikiResolution.setWikidoc(wikiDocument);
	            }
	            
	            //increment/decrement the count
	            if(starRating == StarRating.STAR_1)
	            {
	                wikiResolution.setU1StarCount(calculateNewCount(wikiResolution.getU1StarCount() != null ? wikiResolution.getU1StarCount() : 0, ratingCount));
	            }
	            else if(starRating == StarRating.STAR_2)
	            {
	                wikiResolution.setU2StarCount(calculateNewCount(wikiResolution.getU2StarCount() != null ? wikiResolution.getU2StarCount() : 0, ratingCount));
	            }
	            else if(starRating == StarRating.STAR_3)
	            {
	                wikiResolution.setU3StarCount(calculateNewCount(wikiResolution.getU3StarCount() != null ? wikiResolution.getU3StarCount() : 0, ratingCount));
	            }
	            else if(starRating == StarRating.STAR_4)
	            {
	                wikiResolution.setU4StarCount(calculateNewCount(wikiResolution.getU4StarCount() != null ? wikiResolution.getU4StarCount() : 0, ratingCount));
	            }
	            else if(starRating == StarRating.STAR_5)
	            {
	                wikiResolution.setU5StarCount(calculateNewCount(wikiResolution.getU5StarCount() != null ? wikiResolution.getU5StarCount() : 0, ratingCount));
	            }
	
	            //persist
	            daoWR.persist(wikiResolution);
        	});
            
        }
        catch (Exception e)
        {
            Log.log.debug(e.getMessage(), e);
                      HibernateUtil.rethrowNestedTransaction(e);
        }
        
    }//updateRatingCount

    private static long calculateNewCount(long currentCount, long incrementOrDecrementByCount)
    {
        long newCount = currentCount + incrementOrDecrementByCount;
        return newCount;
    }//calculateNewCount
    
}
