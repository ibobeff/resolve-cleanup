/******************************************************************************
* (C) Copyright 2014
*
* Resolve Systems, LLC
*
* All rights reserved
* This software is distributed under license and may not be used, copied,
* modified, or distributed without the express written permission of
* Resolve Systems, LLC.
*
******************************************************************************/

package com.resolve.services.vo.form;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.resolve.services.constants.CustomFormUIType;
import com.resolve.services.hibernate.customtable.CustomTableUtil;
import com.resolve.util.StringUtils;
import com.resolve.util.constants.HibernateConstantsEnum;
import com.resolve.util.Log;

/**
 * represents a field on the UI
 * 
 * @author jeet.marwah
 *
 */
public class RsUIField extends RsUIComponent
{
    public static final String DEPENDENCIES = "dependencies";
    public static final String ACCESS_RIGHTS = "access_rights";
    public static final String READ_ACCESS_RIGHTS = "read_access_rights";
    public static final String EDIT_ACCESS_RIGHTS = "edit_access_rights";
    public static final String ADMIN_ACCESS_RIGHTS = "admin_access_rights";
    
    
//    private String label;
    private String value;//value that will be populated for this field
    private Integer colNumber;
    private String sourceType; //mapped to DataSource.java

    //map with MetaField 
    private String dbtable;
    private String dbcolumn;
    private String dbcolumnId;
    private String columnModelName;
    private String dbtype;
    private String rstype;
    
    //map with MetaFieldProperties
    private String uiType;//for gxt
//    private String uiXtype;//for extjs
    private String js;
    private Integer size;
    private String defaultValue;
    private Integer width;
    private Integer height;
    private String labelAlign;

    private Integer booleanMaxLength;
    private Integer stringMinLength;
    private Integer stringMaxLength;
    private Integer uiStringMaxLength;
    private Boolean isMultiSelect;
    private Boolean selectUserInput;
    private Integer selectMaxDisplay;
    private String selectValues;
    private String choiceValues;
    private String checkboxValues;
    private Boolean dateTimeHasCalendar;
    private Integer integerMinValue;
    private Integer integerMaxValue;
    private Double decimalMinValue;
    private Double decimalMaxValue;
    private Integer journalRows;
    private Integer journalColumns;
    private Integer journalMinValue;
    private Integer journalMaxValue;
    private Integer listRows;
    private Integer listColumns;
    private Integer listMaxDisplay;  
    private String listValues;
    private String referenceTable;
    private String referenceDisplayColumn;
    private String referenceDisplayColumnList;
    private String referenceTarget;
    private String referenceParams;
    private String linkTarget;
    private String linkParams;
    private String linkTemplate;
    private String sequencePrefix;
    private String hiddenValue;
    
    //for user picker
    private String groups;
    private String usersOfTeams;
    private Boolean recurseUsersOfTeam;
    private String teamsOfTeams;
    private Boolean recurseTeamsOfTeam;
    private Boolean autoAssignToMe;
    private Boolean allowAssignToMe;
    
    //for teampicker
    private String teamPickerTeamsOfTeams;
    
    private String fileUploadTableName;
    private String referenceColumnName;
    private Boolean allowUpload;
    private Boolean allowDownload;
    private Boolean allowRemove;
    private String allowedFileTypes;
    
    private Integer widgetColumns;
    
    //for grid reference
    private String refGridReferenceByTable;
    private String refGridSelectedReferenceColumns;
    private String refGridReferenceColumnName;
    private String allowReferenceTableAdd;
    private String allowReferenceTableRemove;
    private String allowReferenceTableView;
    private String referenceTableUrl;
    private String refGridViewPopup;
    private String refGridViewPopupWidth;
    private String refGridViewPopupHeight;
    private String refGridViewTitle;
    
    //for choice additions
    private String choiceTable;
    private String choiceDisplayField;
    private String choiceValueField;
    private String choiceWhere;
    private String choiceField;
    private String choiceSql;
    private int choiceOptionSelection;
    
    //for section control
    private int sectionNumberOfColumns;
    private String sectionColumns;
    private String sectionType;
    private String sectionTitle;
    private String sectionLayout;
    
    //for spacer control
    private Boolean showHorizontalRule;
    
    //for journal control
    private Boolean allowJournalEdit;
    
    private String tooltip;
    
    private static List<Method> getMethodList = null;
    private static List<Method> setMethodList = null;
    
    //map to MetaSytle --> ??? TODO
    
    private List<RsFieldDependency> dependencies;
    
    //this is for meta_field - DB type - set on the server side and is same as form roles
    private String viewRoles;
    private String editRoles;
    private String adminRoles;    
    
    //New fields - field validation
    private String regex;
    
    static {
        loadMethods();
    }
    
    //used to create the custom table
//    public ResolveBaseModel convertToBaseModel()
//    {
//        //set the DB type
//        Map m = CustomTableUtil.rsUITypeToDbTypeMapping;
//        String test = CustomTableUtil.rsUITypeToDbTypeMapping.get(CustomFormUIType.valueOf(getUiType()));
//        setDbtype(CustomTableUtil.rsUITypeToDbTypeMapping.get(CustomFormUIType.valueOf(getUiType())));
//        if(WorkflowConstants.CUSTOMTABLE_TYPE_STRING.getTagName().equalsIgnoreCase(getDbtype()) && getStringMaxLength() > 4000)
//        {
//            setDbtype(WorkflowConstants.CUSTOMTABLE_TYPE_CLOB.getTagName());
//        }
//
//        //TODO - create a MAP for the old coponents to get the RsType/UType - for new widgets, we will make it same as UIType
////        setRstype(getUiType());
//        
//        ResolveBaseModel bm = new ResolveBaseModel();
//        
//        //common
//        bm.set(RSMetaField.TABLE, getDbtable());//tablename
//        bm.set(RSMetaField.TYPE, getUiType()); //String, Checkbox - NOTE- this is same as UIType
//        bm.set(RSMetaField.DBTYPE, getDbtype());//string, clob 
//        
//        bm.set(RSMetaField.COLUMNMODELNAME, getColumnModelName());//u_col1
//        bm.set(RSMetaField.NAME, getName());
//        bm.set(RSMetaField.DISPLAYNAME, getDisplayName());
//        bm.set(RSMetaField.COLUMN, getDbcolumn());//u_col1
//        bm.set(RSMetaField.COLUMN_ID, getDbcolumnId());//ucolumnId
//        bm.set(RSMetaField.UITYPE, getUiType());//TextField, MultipleCheckBox
//        bm.set(RSMetaField.ISCRYPT, isEncrypted());
//        bm.set(RSMetaField.ISMANDATORY, isMandatory());
//        bm.set(RSMetaField.ORDER, getOrderNumber());
//        bm.set(RSMetaField.SIZE, getSize());
//        bm.set(RSMetaField.DEFAULTVALUE, getDefaultValue());
//        bm.set(RSMetaField.WIDTH, getWidth());
//        bm.set(RSMetaField.HEIGHT, getHeight());
//        bm.set(RSMetaField.LABEL_ALIGN, getLabelAlign());
//        
//        //for String - 40 length
//        // for UType / UUIType / UDbType --> String / TextField / string
//        bm.set(RSMetaField.STRINGMINLENGTH, getStringMinLength());//40, 333
//        bm.set(RSMetaField.STRINGMAXLENGTH, getStringMaxLength());//40, 333
//        bm.set(RSMetaField.UISTRINGMAXLENGTH, getUiStringMaxLength());//this is used for max validation on the form
//        
//        //for UType / UUIType / UDbType --> Select / ComboBox / string(4000)
//        bm.set(RSMetaField.SELECT_VALUES, getSelectValues());
//        bm.set(RSMetaField.SELECT_USER_INPUT, getSelectUserInput());
//        bm.set(RSMetaField.SELECT_MAX_DISPLAY, getSelectMaxDisplay());
//        bm.set(RSMetaField.SELECT_IS_MULTISELECT, getIsMultiSelect());
//        
//        //for UType / UUIType  / UDbType --> Choice / RadioButton / string(333)
//        bm.set(RSMetaField.CHOICE_VALUES, getChoiceValues());
//        
//        //for UType / UUIType / UDbType --> Checkbox / MultipleCheckBox / string(333)
//        bm.set(RSMetaField.CHECKBOX_VALUES, getCheckboxValues());
//        
//        // for UType / UUIType / UDbType --> Link / Link / string(333)
//        bm.set(RSMetaField.LINK_TEMPLATE, getLinkTemplate());
//        bm.set(RSMetaField.LINK_TARGET, getLinkTarget());        
//        
//        // for UType / UUIType / UDbType --> Journal / Journal / clob
//        bm.set(RSMetaField.JOURNAL_ROWS, getJournalRows());
//        bm.set(RSMetaField.JOURNAL_COLUMNS, getJournalColumns());
//
//        // for UType / UUIType / UDbType --> List / List / clob
//        bm.set(RSMetaField.LIST_COLUMNS, getListColumns());//5
//        bm.set(RSMetaField.LIST_ROWS, getListRows());//10
//        bm.set(RSMetaField.LIST_MAX_DISPLAY, getListMaxDisplay());//50
//        bm.set(RSMetaField.LIST_VALUES, getListValues());
//        
//        // for UType / UUIType / UDbType --> Reference / Reference / string(333)
//        bm.set(RSMetaField.REFERENCE_TABLE, getReferenceTable());//table name
//        bm.set(RSMetaField.REFERENCE_DISPLAY_COLUMN, getReferenceDisplayColumn());
//        bm.set(RSMetaField.REFERENCE_PARAMS, getReferenceParams());
//        bm.set(RSMetaField.REFERENCE_TARGET, getReferenceTarget());
//        
//        // for UType / UUIType / UDbType --> Sequence / Sequence / string(333)
//        bm.set(RSMetaField.SEQUENCE_PREFIX, getSequencePrefix());
//        
//        // for UType / UUIType / UDbType --> Integer / NumberTextField / long
//        bm.set(RSMetaField.INTEGER_MIN_VALUE, getIntegerMinValue());
//        bm.set(RSMetaField.INTEGER_MAX_VALUE, getIntegerMaxValue());
//        
//        // for UType / UUIType / UDbType --> Boolean / CheckBox / boolean
//        bm.set(RSMetaField.BOOLEANMAXLENGTH, getBooleanMaxLength());
//
//        // for UType / UUIType / UDbType --> Decimal / DecimalTextField / double
//        bm.set(RSMetaField.DECIMAL_MIN_VALUE, getDecimalMinValue());
//        bm.set(RSMetaField.DECIMAL_MAX_VALUE, getDecimalMaxValue());
//
//        // for UType / UUIType / UDbType --> String / TextField / string
//        // for UType / UUIType / UDbType --> Date / Date / timestamp
//        // for UType / UUIType / UDbType --> Date Time / DateTime / timestamp
//        // for UType / UUIType / UDbType --> Timestamp / Timestamp / long
//        // for UType / UUIType / UDbType --> Password / Password / string(333)
//        
//        //for new xtypes
//        bm.set(RSMetaField.GROUPS, getGroups());
//        bm.set(RSMetaField.USERS_OF_TEAMS, getUsersOfTeams());
//        bm.set(RSMetaField.RECURSE_USERS_OF_TEAMS, getRecurseUsersOfTeam());
//        bm.set(RSMetaField.TEAMS_OF_TEAMS, getTeamsOfTeams());
//        bm.set(RSMetaField.RECURSE_TEAMS_OF_TEAMS, getRecurseTeamsOfTeam());
//        bm.set(RSMetaField.ALLOW_ASSIGN_TO_ME, getAllowAssignToMe());
//        bm.set(RSMetaField.AUTO_ASSIGN_TO_ME, getAutoAssignToMe());
//        
//        //for team picker
//        bm.set(RSMetaField.TP_TEAMS_OF_TEAMS, getTeamPickerTeamsOfTeams());
//        
//        //for spacer
//        bm.set(RSMetaField.SHOW_HORIZONTAL_RULE, getShowHorizontalRule());
//        
//        //for journal
//        bm.set(RSMetaField.ALLOW_JOURNAL_EDIT, getAllowJournalEdit());
//        
//        //for file upload widget
//        bm.set(RSMetaField.FILE_UPLOAD_TABLE_NAME, getFileUploadTableName());
//        bm.set(RSMetaField.ALLOW_UPLOAD, getAllowUpload());
//        bm.set(RSMetaField.ALLOW_DOWNLOAD, getAllowDownload());
//        bm.set(RSMetaField.ALLOW_REMOVE, getAllowRemove());
//        bm.set(RSMetaField.ALLOW_FILE_TYPES, getAllowedFileTypes());
//        
//        bm.set(RSMetaField.WIDGET_COLUMNS, getWidgetColumns());
//        
//        //for grid ref widget
//        bm.set(RSMetaField.REF_GRID_REF_BY_TABLE, getRefGridReferenceByTable());
//        bm.set(RSMetaField.REF_GRID_SELECTED_REF_COL, getRefGridSelectedReferenceColumns());
//        bm.set(RSMetaField.REF_GRID_REF_COL_NAME, getRefGridReferenceColumnName());
//        bm.set(RSMetaField.REF_GRID_ALLOW_REF_TABLE_ADD, getAllowReferenceTableAdd());
//        bm.set(RSMetaField.REF_GRID_ALLOW_REF_TABLE_REMOVE, getAllowReferenceTableRemove());
//        bm.set(RSMetaField.REF_GRID_REF_ALLOW_REF_TABLE_VIEW, getAllowReferenceTableView());
//        bm.set(RSMetaField.REF_GRID_REF_TABLE_URL, getReferenceTableUrl());
//        bm.set(RSMetaField.REF_GRID_VIEW_POPUP, getRefGridViewPopup());
//        bm.set(RSMetaField.REF_GRID_VIEW_POPUP_WIDTH, getRefGridViewPopupWidth());
//        bm.set(RSMetaField.REF_GRID_VIEW_POPUP_HEIGHT, getRefGridViewPopupHeight());
//        bm.set(RSMetaField.REF_GRID_VIEW_POPUP_TITLE, getRefGridViewTitle());
//        
//        //for hidden and readonly
//        bm.set(RSMetaField.IS_HIDDEN, isHidden());
//        bm.set(RSMetaField.IS_READONLY, isReadOnly());
//        
//        bm.set(RSMetaField.TOOLTIP, getTooltip());
//        
//        //for dependencies - not required when custom table is created using the custom form
//        bm.set(DEPENDENCIES, getDependencies());
//        
//        //access rights
//        ResolveBaseModel rights = new ResolveBaseModel();
//        rights.set(READ_ACCESS_RIGHTS, getViewRoles());
//        rights.set(EDIT_ACCESS_RIGHTS, getEditRoles());
//        rights.set(ADMIN_ACCESS_RIGHTS, getAdminRoles());
//        bm.set(ACCESS_RIGHTS, rights);
//        
//        //system 
//        if(StringUtils.isNotBlank(getDbcolumnId()))
//        {
//            bm.set(RSMetaField.SYS_ID, getDbcolumnId());
////            bm.set("isNew", false);    
//        }
//        else
//        {
//            bm.set(RSMetaField.SYS_ID, null);
////            bm.set("isNew", true);
//        }
////        bm.set("sysCreatedOn", "");
////        bm.set("sysUpdatedOn", "");
////        bm.set("sysCreatedBy", "");
////        bm.set("sysUpdatedBy", "");
////        bm.set("UValue", "");
////        bm.set("UStyle", "");
////        bm.set(RSMetaField.JAVASCRIPT, "");
//        
//        return bm;
//    }
    
    
    //    public String getLabel()
//    {
//        return label;
//    }
//    public void setLabel(String label)
//    {
//        this.label = label;
//    }
    public String getValue()
    {
        return value;
    }
    public void setValue(String value)
    {
        this.value = value;
    }
    public String getDefaultValue()
    {
        return defaultValue;
    }
    public void setDefaultValue(String defaultValue)
    {
        this.defaultValue = defaultValue;
    }
    public String getRegex()
    {
        return regex;
    }
    public void setRegex(String regex)
    {
        this.regex = regex;
    }
    public Integer getColNumber()
    {
        return colNumber;
    }
    public void setColNumber(Integer colNumber)
    {
        this.colNumber = colNumber;
    }
    public String getDbtable()
    {
        return dbtable;
    }
    public void setDbtable(String dbtable)
    {
        this.dbtable = dbtable;
    }
    public String getDbcolumn()
    {
        return dbcolumn;
    }
    public void setDbcolumn(String dbcolumn)
    {
        this.dbcolumn = dbcolumn;
    }
    public String getColumnModelName()
    {
        return columnModelName;
    }
    public void setColumnModelName(String columnModelName)
    {
        this.columnModelName = columnModelName;
    }
    
    public void evaluateDbType()
    {
        if(StringUtils.isBlank(dbtype))
        {
            this.dbtype = (CustomTableUtil.rsUITypeToDbTypeMapping.get(CustomFormUIType.valueOf(getUiType())));
            if(HibernateConstantsEnum.CUSTOMTABLE_TYPE_STRING.getTagName().equalsIgnoreCase(this.dbtype) && getStringMaxLength() > 4000)
            {
                this.dbtype = HibernateConstantsEnum.CUSTOMTABLE_TYPE_CLOB.getTagName();
            }
        }
    }
    
    public String getDbtype()
    {
        return dbtype;
    }
    public void setDbtype(String dbtype)
    {
        this.dbtype = dbtype;
    }
//    public String getRstype()
//    {
//        return rstype;
//    }
//    public void setRstype(String rstype)
//    {
//        this.rstype = rstype;
//    }
    public String getUiType()
    {
        return uiType;
    }
    public void setUiType(String uiType)
    {
        this.uiType = uiType;
    }
//    public String getUiXtype()
//    {
//        return uiXtype;
//    }
//    public void setUiXtype(String uiXtype)
//    {
//        this.uiXtype = uiXtype;
//    }
    public String getJs()
    {
        return js;
    }
    public void setJs(String js)
    {
        this.js = js;
    }
    public Integer getSize()
    {
        return size;
    }
    public void setSize(Integer size)
    {
        this.size = size;
    }

    public Integer getBooleanMaxLength()
    {
        return booleanMaxLength;
    }
    public void setBooleanMaxLength(Integer booleanMaxLength)
    {
        this.booleanMaxLength = booleanMaxLength;
    }
    public Integer getStringMaxLength()
    {
        return stringMaxLength;
    }
    public void setStringMaxLength(Integer stringMaxLength)
    {
        this.stringMaxLength = stringMaxLength;
    }
    public Integer getUiStringMaxLength()
    {
        return uiStringMaxLength;
    }
    public void setUiStringMaxLength(Integer uiStringMaxLength)
    {
        this.uiStringMaxLength = uiStringMaxLength;
    }
    public Boolean getIsMultiSelect()
    {
        return isMultiSelect;
    }
    public void setIsMultiSelect(Boolean isMultiSelect)
    {
        this.isMultiSelect = isMultiSelect;
    }
    public Boolean getSelectUserInput()
    {
        return selectUserInput;
    }
    public void setSelectUserInput(Boolean selectUserInput)
    {
        this.selectUserInput = selectUserInput;
    }
    public Integer getSelectMaxDisplay()
    {
        return selectMaxDisplay;
    }
    public void setSelectMaxDisplay(Integer selectMaxDisplay)
    {
        this.selectMaxDisplay = selectMaxDisplay;
    }
    public String getSelectValues()
    {
        return selectValues;
    }
    public void setSelectValues(String selectValues)
    {
        this.selectValues = selectValues;
    }
    public String getChoiceValues()
    {
        return choiceValues;
    }
    public void setChoiceValues(String choiceValues)
    {
        this.choiceValues = choiceValues;
    }
    public String getCheckboxValues()
    {
        return checkboxValues;
    }
    public void setCheckboxValues(String checkboxValues)
    {
        this.checkboxValues = checkboxValues;
    }
    public Boolean getDateTimeHasCalendar()
    {
        return dateTimeHasCalendar;
    }
    public void setDateTimeHasCalendar(Boolean dateTimeHasCalendar)
    {
        this.dateTimeHasCalendar = dateTimeHasCalendar;
    }
    public Integer getIntegerMinValue()
    {
        return integerMinValue;
    }
    public void setIntegerMinValue(Integer integerMinValue)
    {
        this.integerMinValue = integerMinValue;
    }
    public Integer getIntegerMaxValue()
    {
        return integerMaxValue;
    }
    public void setIntegerMaxValue(Integer integerMaxValue)
    {
        this.integerMaxValue = integerMaxValue;
    }
    public Double getDecimalMinValue()
    {
        return decimalMinValue;
    }
    public void setDecimalMinValue(Double decimalMinValue)
    {
        this.decimalMinValue = decimalMinValue;
    }
    public Double getDecimalMaxValue()
    {
        return decimalMaxValue;
    }
    public void setDecimalMaxValue(Double decimalMaxValue)
    {
        this.decimalMaxValue = decimalMaxValue;
    }
    public Integer getJournalRows()
    {
        return journalRows;
    }
    public void setJournalRows(Integer journalRows)
    {
        this.journalRows = journalRows;
    }
    public Integer getJournalColumns()
    {
        return journalColumns;
    }
    public void setJournalColumns(Integer journalColumns)
    {
        this.journalColumns = journalColumns;
    }
    public Integer getJournalMinValue()
    {
        return journalMinValue;
    }
    public void setJournalMinValue(Integer journalMinValue)
    {
        this.journalMinValue = journalMinValue;
    }
    public Integer getJournalMaxValue()
    {
        return journalMaxValue;
    }
    public void setJournalMaxValue(Integer journalMaxValue)
    {
        this.journalMaxValue = journalMaxValue;
    }
    public Integer getListRows()
    {
        return listRows;
    }
    public void setListRows(Integer listRows)
    {
        this.listRows = listRows;
    }
    public Integer getListColumns()
    {
        return listColumns;
    }
    public void setListColumns(Integer listColumns)
    {
        this.listColumns = listColumns;
    }
    public Integer getListMaxDisplay()
    {
        return listMaxDisplay;
    }
    public void setListMaxDisplay(Integer listMaxDisplay)
    {
        this.listMaxDisplay = listMaxDisplay;
    }
    public String getListValues()
    {
        return listValues;
    }
    public void setListValues(String listValues)
    {
        this.listValues = listValues;
    }
    public String getReferenceTable()
    {
        return referenceTable;
    }
    public void setReferenceTable(String referenceTable)
    {
        this.referenceTable = referenceTable;
    }
    public String getReferenceDisplayColumn()
    {
        return referenceDisplayColumn;
    }
    public void setReferenceDisplayColumn(String referenceDisplayColumn)
    {
        this.referenceDisplayColumn = referenceDisplayColumn;
    }
    public String getReferenceDisplayColumnList()
    {
        return referenceDisplayColumnList;
    }
    public void setReferenceDisplayColumnList(String referenceDisplayColumnList)
    {
        this.referenceDisplayColumnList = referenceDisplayColumnList;
    }
    public String getReferenceTarget()
    {
        return referenceTarget;
    }
    public void setReferenceTarget(String referenceTarget)
    {
        this.referenceTarget = referenceTarget;
    }
    public String getReferenceParams()
    {
        return referenceParams;
    }
    public void setReferenceParams(String rReferenceParams)
    {
        this.referenceParams = rReferenceParams;
    }
    public String getLinkTarget()
    {
        return linkTarget;
    }
    public void setLinkTarget(String linkTarget)
    {
        this.linkTarget = linkTarget;
    }
    public String getLinkParams()
    {
        return linkParams;
    }
    public void setLinkParams(String linkParams)
    {
        this.linkParams = linkParams;
    }
    public String getLinkTemplate()
    {
        return linkTemplate;
    }
    public void setLinkTemplate(String linkTemplate)
    {
        this.linkTemplate = linkTemplate;
    }
    public String getSequencePrefix()
    {
        return sequencePrefix;
    }
    public void setSequencePrefix(String sequencePrefix)
    {
        this.sequencePrefix = sequencePrefix;
    }
    public String getHiddenValue()
    {
        return hiddenValue;
    }
    public void setHiddenValue(String hiddenValue)
    {
        this.hiddenValue = hiddenValue;
    }
    public String getSourceType()
    {
        return sourceType;
    }
    public void setSourceType(String sourceType)
    {
        this.sourceType = sourceType;
    }

    public Integer getWidth()
    {
        return width;
    }

    public void setWidth(Integer width)
    {
        this.width = width;
    }

    public Integer getHeight()
    {
        return height;
    }

    public void setHeight(Integer height)
    {
        this.height = height;
    }

    public String getLabelAlign()
    {
        return labelAlign;
    }

    public void setLabelAlign(String labelAlign)
    {
        this.labelAlign = labelAlign;
    }

    public Integer getStringMinLength()
    {
        return stringMinLength;
    }

    public void setStringMinLength(Integer stringMinLength)
    {
        this.stringMinLength = stringMinLength;
    }


    public String getDbcolumnId()
    {
        return dbcolumnId;
    }


    public void setDbcolumnId(String dbcolumnId)
    {
        this.dbcolumnId = dbcolumnId;
    }


    public String getGroups()
    {
        return groups;
    }


    public void setGroups(String groups)
    {
        this.groups = groups;
    }


    public String getUsersOfTeams()
    {
        return usersOfTeams;
    }


    public void setUsersOfTeams(String usersOfTeams)
    {
        this.usersOfTeams = usersOfTeams;
    }


    public Boolean getRecurseUsersOfTeam()
    {
        return recurseUsersOfTeam;
    }


    public void setRecurseUsersOfTeam(Boolean recurseUsersOfTeam)
    {
        this.recurseUsersOfTeam = recurseUsersOfTeam;
    }


    public String getTeamsOfTeams()
    {
        return teamsOfTeams;
    }


    public void setTeamsOfTeams(String teamsOfTeams)
    {
        this.teamsOfTeams = teamsOfTeams;
    }


    public Boolean getRecurseTeamsOfTeam()
    {
        return recurseTeamsOfTeam;
    }


    public void setRecurseTeamsOfTeam(Boolean recurseTeamsOfTeam)
    {
        this.recurseTeamsOfTeam = recurseTeamsOfTeam;
    }


    public Boolean getAutoAssignToMe()
    {
        return autoAssignToMe;
    }


    public void setAutoAssignToMe(Boolean autoAssignToMe)
    {
        this.autoAssignToMe = autoAssignToMe;
    }


    public Boolean getAllowAssignToMe()
    {
        return allowAssignToMe;
    }


    public void setAllowAssignToMe(Boolean allowAssignToMe)
    {
        this.allowAssignToMe = allowAssignToMe;
    }

    public List<RsFieldDependency> getDependencies()
    {
        return dependencies;
    }

    public void setDependencies(List<RsFieldDependency> dependencies)
    {
        this.dependencies = dependencies;
    }


    public String getViewRoles()
    {
        return viewRoles;
    }


    public void setViewRoles(String viewRoles)
    {
        this.viewRoles = viewRoles;
    }


    public String getEditRoles()
    {
        return editRoles;
    }


    public void setEditRoles(String editRoles)
    {
        this.editRoles = editRoles;
    }


    public String getAdminRoles()
    {
        return adminRoles;
    }


    public void setAdminRoles(String adminRoles)
    {
        this.adminRoles = adminRoles;
    }


    public Boolean getAllowUpload()
    {
        return allowUpload;
    }


    public void setAllowUpload(Boolean allowUpload)
    {
        this.allowUpload = allowUpload;
    }


    public Boolean getAllowDownload()
    {
        return allowDownload;
    }


    public void setAllowDownload(Boolean allowDownload)
    {
        this.allowDownload = allowDownload;
    }


    public Boolean getAllowRemove()
    {
        return allowRemove;
    }


    public void setAllowRemove(Boolean allowRemove)
    {
        this.allowRemove = allowRemove;
    }


    public String getAllowedFileTypes()
    {
        return allowedFileTypes;
    }


    public void setAllowedFileTypes(String allowedFileTypes)
    {
        this.allowedFileTypes = allowedFileTypes;
    }


    public Integer getWidgetColumns()
    {
        return widgetColumns;
    }


    public void setWidgetColumns(Integer widgetColumns)
    {
        this.widgetColumns = widgetColumns;
    }


    public String getFileUploadTableName()
    {
        return fileUploadTableName;
    }


    public void setFileUploadTableName(String fileUploadTableName)
    {
        this.fileUploadTableName = fileUploadTableName;
    }


    public String getReferenceColumnName()
    {
        return referenceColumnName;
    }


    public void setReferenceColumnName(String referenceColumnName)
    {
        this.referenceColumnName = referenceColumnName;
    }


    public String getRefGridReferenceByTable()
    {
        return refGridReferenceByTable;
    }


    public void setRefGridReferenceByTable(String refGridReferenceByTable)
    {
        this.refGridReferenceByTable = refGridReferenceByTable;
    }


    public String getRefGridSelectedReferenceColumns()
    {
        return refGridSelectedReferenceColumns;
    }


    public void setRefGridSelectedReferenceColumns(String refGridSelectedReferenceColumns)
    {
        this.refGridSelectedReferenceColumns = refGridSelectedReferenceColumns;
    }


    public String getRefGridReferenceColumnName()
    {
        return refGridReferenceColumnName;
    }


    public void setRefGridReferenceColumnName(String refGridReferenceColumnName)
    {
        this.refGridReferenceColumnName = refGridReferenceColumnName;
    }


    public String getAllowReferenceTableAdd()
    {
        return allowReferenceTableAdd;
    }


    public void setAllowReferenceTableAdd(String allowReferenceTableAdd)
    {
        this.allowReferenceTableAdd = allowReferenceTableAdd;
    }


    public String getAllowReferenceTableRemove()
    {
        return allowReferenceTableRemove;
    }


    public void setAllowReferenceTableRemove(String allowReferenceTableRemove)
    {
        this.allowReferenceTableRemove = allowReferenceTableRemove;
    }


    public String getAllowReferenceTableView()
    {
        return allowReferenceTableView;
    }


    public void setAllowReferenceTableView(String allowReferenceTableView)
    {
        this.allowReferenceTableView = allowReferenceTableView;
    }


    public String getReferenceTableUrl()
    {
        return referenceTableUrl;
    }


    public void setReferenceTableUrl(String referenceTableUrl)
    {
        this.referenceTableUrl = referenceTableUrl;
    }


    public String getRefGridViewPopup()
    {
        return refGridViewPopup;
    }


    public void setRefGridViewPopup(String refGridViewPopup)
    {
        this.refGridViewPopup = refGridViewPopup;
    }


    public String getRefGridViewPopupWidth()
    {
        return refGridViewPopupWidth;
    }


    public void setRefGridViewPopupWidth(String refGridViewPopupWidth)
    {
        this.refGridViewPopupWidth = refGridViewPopupWidth;
    }


    public String getRefGridViewPopupHeight()
    {
        return refGridViewPopupHeight;
    }


    public void setRefGridViewPopupHeight(String refGridViewPopupHeight)
    {
        this.refGridViewPopupHeight = refGridViewPopupHeight;
    }


    public String getRefGridViewTitle()
    {
        return refGridViewTitle;
    }


    public void setRefGridViewTitle(String refGridViewTitle)
    {
        this.refGridViewTitle = refGridViewTitle;
    }


    public String getTeamPickerTeamsOfTeams()
    {
        return teamPickerTeamsOfTeams;
    }


    public void setTeamPickerTeamsOfTeams(String teamPickerTeamsOfTeams)
    {
        this.teamPickerTeamsOfTeams = teamPickerTeamsOfTeams;
    }


    public String getTooltip()
    {
        return tooltip;
    }


    public void setTooltip(String tooltip)
    {
        this.tooltip = tooltip;
    }


    public String getChoiceTable()
    {
        return choiceTable;
    }


    public void setChoiceTable(String choiceTable)
    {
        this.choiceTable = choiceTable;
    }


    public String getChoiceDisplayField()
    {
        return choiceDisplayField;
    }


    public void setChoiceDisplayField(String choiceDisplayField)
    {
        this.choiceDisplayField = choiceDisplayField;
    }


    public String getChoiceValueField()
    {
        return choiceValueField;
    }


    public void setChoiceValueField(String choiceValueField)
    {
        this.choiceValueField = choiceValueField;
    }


    public String getChoiceWhere()
    {
        return choiceWhere;
    }


    public void setChoiceWhere(String choiceWhere)
    {
        this.choiceWhere = choiceWhere;
    }


    public String getChoiceField()
    {
        return choiceField;
    }


    public void setChoiceField(String choiceField)
    {
        this.choiceField = choiceField;
    }


    public String getChoiceSql()
    {
        return choiceSql;
    }


    public void setChoiceSql(String choiceSql)
    {
        this.choiceSql = choiceSql;
    }


    public int getChoiceOptionSelection()
    {
        return choiceOptionSelection;
    }


    public void setChoiceOptionSelection(int choiceOptionSelection)
    {
        this.choiceOptionSelection = choiceOptionSelection;
    }


    public int getSectionNumberOfColumns()
    {
        return sectionNumberOfColumns;
    }


    public void setSectionNumberOfColumns(int sectionNumberOfColumns)
    {
        this.sectionNumberOfColumns = sectionNumberOfColumns;
    }


    public String getSectionColumns()
    {
        return sectionColumns;
    }


    public void setSectionColumns(String sectionColumns)
    {
        this.sectionColumns = sectionColumns;
    }


    public String getSectionType()
    {
        return sectionType;
    }


    public void setSectionType(String sectionType)
    {
        this.sectionType = sectionType;
    }


    public String getSectionTitle()
    {
        return sectionTitle;
    }


    public void setSectionTitle(String sectionTitle)
    {
        this.sectionTitle = sectionTitle;
    }


    public String getSectionLayout()
    {
        return sectionLayout;
    }


    public void setSectionLayout(String sectionLayout)
    {
        this.sectionLayout = sectionLayout;
    }


    public Boolean getShowHorizontalRule()
    {
        return showHorizontalRule;
    }


    public void setShowHorizontalRule(Boolean showHorizontalRule)
    {
        this.showHorizontalRule = showHorizontalRule;
    }


    public Boolean getAllowJournalEdit()
    {
        return allowJournalEdit;
    }


    public void setAllowJournalEdit(Boolean allowJournalEdit)
    {
        this.allowJournalEdit = allowJournalEdit;
    }
    
    public static List<Method> getGetMethodList()
    {
        return getMethodList;
    }
    public static void setGetMethodList(List<Method> getMethodList)
    {
        RsUIField.getMethodList = getMethodList;
    }
    public static List<Method> getSetMethodList()
    {
        return setMethodList;
    }
    public static void setSetMethodList(List<Method> setMethodList)
    {
        RsUIField.setMethodList = setMethodList;
    }
    
    public void setTransientFields(String jsonStr)
    {
        if(StringUtils.isBlank(jsonStr) || jsonStr.equals("UNDEFINED"))
            return;
        
        try {
            List<Method> methods = getSetMethodList();
            if(methods == null || methods.size() == 0)
                return;
            
            JSONObject json = JSONObject.fromObject(jsonStr);
            
            Iterator<?> it = json.keys();
            for(String field; it.hasNext();) {
                field = (String) it.next();
                if(StringUtils.isBlank(field))
                    continue;
                
                Object value = null;
                if(json.has(field))
                    value = json.get(field);
                else
                    continue;
                
                String setMethodStr = getSetMethodName(field);
                if(setMethodStr != null)
                {
                    Method method = lookupMethod(setMethodStr, methods);
                    if(method != null) {
                        Object[] params = null;
                        if(value instanceof String) {
                            params = new String[1];
                            params[0] = (String)value;
                            method.invoke(this, (Object[])params);
                        }
                        else if(value instanceof Integer) {
                            params = new Integer[1];
                            params[0] = (Integer)value;
                            method.invoke(this, ((Integer)params[0]).intValue());
                        }
                        else if(value instanceof Double) {
                            params = new Double[1];
                            params[0] = (Double)value;
                            method.invoke(this, ((Double)params[0]).doubleValue());
                        }
                        else if(value instanceof JSONArray)
                        {
                            params = new String[1];
                            params[0] = StringUtils.jsonArrayToString((JSONArray)value);
                            method.invoke(this, (Object[])params);
                        }
                        else if(value instanceof JSONObject)
                        {
                            params = new String[1];
                            params[0] = StringUtils.jsonObjectToString((JSONObject)value);
                            method.invoke(this, (Object[])params);
                        }
                        else {
                            params = new Object[1];
                            params[0] = value;
                            method.invoke(this, (Object[])params);
                        }
/*                      else
                            throw new RuntimeException("New transient object type found: " + value.getClass()); */
                    }
                }
            } // end of for(method).
        } catch(Exception e) {
            Log.log.error("Failed to set transient fields from properties.", e);
        }
    }
    
    private static String getSetMethodName(String fieldName) {
        
        String methodName = null;
        
        if(StringUtils.isBlank(fieldName))
            return null;

        methodName = "set" + fieldName.substring(1);

        return methodName;
    }
    
    private static void loadMethods() {
        
        getMethodList = new ArrayList<Method>();
        setMethodList = new ArrayList<Method>();
        
        try {
            Class<?> currentClass = Class.forName("com.resolve.services.vo.form.RsUIField");
            Method[] methods = currentClass.getDeclaredMethods();
            for(Method method:methods) {
                String methodName = method.getName();
                if(methodName.startsWith("get"))
                    getMethodList.add(method);
                else if(methodName.startsWith("set"))
                    setMethodList.add(method);
            }
        }
        catch(Exception e)
        {
            Log.log.error("Failed to load get or set method list.", e);
            throw new RuntimeException("Failed to load get or set method list.", e);
        }
    }
    
    public static Method lookupMethod(String methodStr, List<Method> methods) {

        for(Method method:methods) {
            if(methodStr.equals(method.getName()))
                return method;
        }
        
        return null;
    }

}
