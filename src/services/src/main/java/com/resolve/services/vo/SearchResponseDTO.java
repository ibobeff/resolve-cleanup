package com.resolve.services.vo;

/**
 * The search response DTO is used to represent search response for the
 * aggregated content search.
 * 
 */
public class SearchResponseDTO extends ResponseDTO<SearchRecordDTO> {

	private long totalWikis = 0;

	private long totalAutomations = 0;

	private long totalDecisionTrees = 0;

	private long totalActionTasks = 0;

	private long totalCustomForms = 0;

	private long totalProperties = 0;

	private long totalPlaybooks = 0;

	public long getTotalWikis() {
		return totalWikis;
	}

	public void setTotalWikis(long totalWikis) {
		this.totalWikis = totalWikis;
	}

	public long getTotalAutomations() {
		return totalAutomations;
	}

	public void setTotalAutomations(long totalAutomations) {
		this.totalAutomations = totalAutomations;
	}

	public long getTotalDecisionTrees() {
		return totalDecisionTrees;
	}

	public void setTotalDecisionTrees(long totalDecisionTrees) {
		this.totalDecisionTrees = totalDecisionTrees;
	}

	public long getTotalActionTasks() {
		return totalActionTasks;
	}

	public void setTotalActionTasks(long totalActionTasks) {
		this.totalActionTasks = totalActionTasks;
	}

	public long getTotalCustomForms() {
		return totalCustomForms;
	}

	public void setTotalCustomForms(long totalCustomForms) {
		this.totalCustomForms = totalCustomForms;
	}

	public long getTotalProperties() {
		return totalProperties;
	}

	public void setTotalProperties(long totalProperties) {
		this.totalProperties = totalProperties;
	}

	public long getTotalPlaybooks() {
		return totalPlaybooks;
	}

	public void setTotalPlaybooks(long totalPlaybooks) {
		this.totalPlaybooks = totalPlaybooks;
	}

	public void setCount(ContentType type, long count) {
		switch (type) {
		case DOCUMENT: {
			setTotalWikis(count);
			break;
		}
		case AUTOMATION: {
			setTotalAutomations(count);
			break;
		}
		case DECISIONTREE: {
			setTotalDecisionTrees(count);
			break;
		}
		case PLAYBOOK: {
			setTotalPlaybooks(count);
			break;
		}
		case ACTIONTASK: {
			setTotalActionTasks(count);
			break;
		}
		case PROPERTY: {
			setTotalProperties(count);
			break;
		}
		case CUSTOMFORM: {
			setTotalCustomForms(count);
			break;
		}
		default:
			break;
		}
	}
}
