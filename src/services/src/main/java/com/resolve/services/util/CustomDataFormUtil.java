package com.resolve.services.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.ejb.HibernateQuery;

import com.resolve.persistence.model.CustomDataForm;
import com.resolve.persistence.model.ResolveSecurityIncident;
import com.resolve.persistence.util.HibernateProxy;
import com.resolve.persistence.util.HibernateUtil;
import com.resolve.services.hibernate.vo.CustomDataFormVO;
import com.resolve.services.hibernate.vo.ResolveSecurityIncidentVO;
import com.resolve.services.interfaces.VO;
import com.resolve.util.Log;
import com.resolve.util.StringUtils;

public class CustomDataFormUtil {
	private static final String ERROR_GET_CUSTOM_DATA = "Failed to get incident custom data";
	private static final String ERROR_INPUT_NULL_OR_EMPTY = "Input parameter is null or empty";
	private static final String ERROR_CUSTOM_FORM_NOT_FOUND = "Incident custom data form not found";
	private static final String ERROR_SAVING_CUSTOM_DATA = "Failed to save custom data form";
	private static final String ERROR_INCIDENT_NOT_FOUND = "Target incident not found";
	private static final String CUSTOM_DATA_FORM_FROM_SIR = "custom data form from SIR";
	
	public static List<CustomDataFormVO> getSIRCustomDataForms(String incidentId, String username) throws Exception {
		if (StringUtils.isBlank(incidentId)) {
			throw new IllegalArgumentException(ERROR_INPUT_NULL_OR_EMPTY);
		}

		List<CustomDataFormVO> customVOs = new ArrayList<>();

		try {

        HibernateProxy.setCurrentUser(username);
			HibernateProxy.execute(() -> {
				CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
				CriteriaQuery<CustomDataForm> criteriaQuery = criteriaBuilder.createQuery(CustomDataForm.class);					
				Root<CustomDataForm> from = criteriaQuery.from(CustomDataForm.class);
				criteriaQuery.select(from).where(criteriaBuilder.equal(from.get("incident").get("sys_id"), incidentId));
				
				List<CustomDataForm> result = HibernateUtil.getCurrentSession().createQuery(criteriaQuery).getResultList();
				for (CustomDataForm ca : result) {
					customVOs.add(ca.doGetVO());
				}

			});
		} catch (Throwable t) {
			Log.log.error(ERROR_GET_CUSTOM_DATA, t);

			throw t;
		}
		return customVOs;
	}

	public static CustomDataFormVO getSIRCustomDataForm(String incidentId, String wikiId, String username) throws Exception {
		if (StringUtils.isBlank(incidentId) || StringUtils.isBlank(wikiId)) {
			throw new IllegalArgumentException(ERROR_INPUT_NULL_OR_EMPTY);
		}

		try {

        HibernateProxy.setCurrentUser(username);
			return (CustomDataFormVO) HibernateProxy.execute(() -> {
				CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
				CriteriaQuery<CustomDataForm> criteriaQuery = criteriaBuilder.createQuery(CustomDataForm.class);					
				Root<CustomDataForm> from = criteriaQuery.from(CustomDataForm.class);
				Predicate pred = criteriaBuilder.and(
						criteriaBuilder.equal(from.get("incident").get("sys_id"), incidentId),
						criteriaBuilder.equal(from.get("wikiId"), wikiId));
				criteriaQuery.select(from).where(pred);
				
				CustomDataForm cdf = HibernateUtil.getCurrentSession().createQuery(criteriaQuery).uniqueResult();

				CustomDataFormVO result = null;
				if (cdf != null) {
					result = cdf.doGetVO();
				}
				
				return result;
			});			
		} catch (Throwable t) {
			Log.log.error(ERROR_GET_CUSTOM_DATA, t);

			throw t;
		}
	}

	public static CustomDataFormVO saveSIRCustomDataForm(CustomDataFormVO vo, String username)
			throws Exception {
		if (vo == null || StringUtils.isBlank(vo.getIncidentId()) || StringUtils.isBlank(vo.getWikiId())
				|| VO.STRING_DEFAULT.equalsIgnoreCase(vo.getIncidentId())
				|| VO.STRING_DEFAULT.equalsIgnoreCase(vo.getWikiId())) {
			throw new IllegalArgumentException(ERROR_INPUT_NULL_OR_EMPTY);
		}

		try {
        HibernateProxy.setCurrentUser(username);
			return (CustomDataFormVO) HibernateProxy.execute(() -> {

				CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
				CriteriaQuery<CustomDataForm> criteriaQuery = criteriaBuilder.createQuery(CustomDataForm.class);					
				Root<CustomDataForm> from = criteriaQuery.from(CustomDataForm.class);
				Predicate pred = criteriaBuilder.and(
						criteriaBuilder.equal(from.get("incident").get("sys_id"), vo.getIncidentId()),
						criteriaBuilder.equal(from.get("wikiId"), vo.getWikiId()));
				criteriaQuery.select(from).where(pred);
				
				CustomDataForm cdf = HibernateUtil.getCurrentSession().createQuery(criteriaQuery).uniqueResult();
	
				if (cdf == null) {
					cdf = new CustomDataForm();
				}
	
				cdf = fillCustomDataForm(cdf, vo);
				
				if (cdf.getIncident() == null) {
					ResolveSecurityIncidentVO incidentVo = PlaybookUtils.getSecurityIncident(vo.getIncidentId(), null, username);
	
					if (incidentVo == null) {
						throw new IllegalArgumentException(ERROR_INCIDENT_NOT_FOUND);
					}
	
					ResolveSecurityIncident incident = new ResolveSecurityIncident();
					incident.applyVOToModel(incidentVo);
					cdf.setIncident(incident);
				}
	
				CustomDataForm result = HibernateUtil.getDAOFactory().getCustomDataFormDAO().insertOrUpdate(cdf);
				return result.doGetVO();

			});			
		} catch (Throwable t) {
			Log.log.error(ERROR_SAVING_CUSTOM_DATA, t);
			throw t;
		}
	}

	public static boolean deleteSIRCustomDataForm(String incidentId, String wikiId, String username) throws Exception {
		
		PlaybookUtils.v2LicenseViolationsAndEnvCheck(CUSTOM_DATA_FORM_FROM_SIR);
		
		try {
			if (StringUtils.isBlank(incidentId) || StringUtils.isBlank(wikiId)) {
				throw new IllegalArgumentException(ERROR_INPUT_NULL_OR_EMPTY);
			}

			return (boolean) HibernateProxy.execute(() -> {

				CriteriaBuilder criteriaBuilder = HibernateUtil.getCurrentSession().getCriteriaBuilder();
				CriteriaQuery<CustomDataForm> criteriaQuery = criteriaBuilder.createQuery(CustomDataForm.class);
				Root<CustomDataForm> from = criteriaQuery.from(CustomDataForm.class);

				Predicate pred = criteriaBuilder.and(
						criteriaBuilder.equal(from.get("incident").get("sys_id"), incidentId),
						criteriaBuilder.equal(from.get("wikiId"), wikiId));

				criteriaQuery.select(from).where(pred);

				CustomDataForm oldForm = HibernateUtil.getCurrentSession().createQuery(criteriaQuery).uniqueResult();

				if (oldForm == null) {
					throw new IllegalArgumentException(ERROR_CUSTOM_FORM_NOT_FOUND);
				}

				HibernateUtil.getDAOFactory().getCustomDataFormDAO().delete(oldForm);

				return true;
			});

		} catch (Throwable t) {
			Log.log.error(t.getMessage(), t);
                             HibernateUtil.rethrowNestedTransaction(t);
		}
		
		return false;
	}

	private static CustomDataForm fillCustomDataForm(CustomDataForm oldCdf, CustomDataFormVO vo) throws Exception {
		oldCdf.setWikiId(vo.getWikiId());

		if (vo.getFormRecord() != null) {
			oldCdf.getFormRecord().clear();
			for (String key : vo.getFormRecord().keySet()) {
				oldCdf.getFormRecord().put(key, vo.getFormRecord().get(key));
			}
		}
		
		return oldCdf;
	}

}
